<?xml version="1.0"?>
<material fx="UberCar" mutable="true">
  <param type="texture" name="p_ambientOcclusion">Wheel_AO</param>
  <param type="texture" name="p_diffuse">Tires_A</param>
  <param type="texture" name="t_normalMap">Tires_N</param>
  <param type="texture" name="p_specMap">Tires_S</param>
  <param type="bool" name="s_legacySpecular">true</param>
  <param type="bool" name="s_glossFromTexture">true</param>
  <param type="float" name="p_shadowRender_Offset">0.0001</param>
  <param type="float" name="p_legacySpecularReflectivity">0.8300</param>
  <param type="float" name="p_legacySpecularPower">5.3700</param>
  <param type="float" name="p_fresnelSide">0.3100</param>
  <param type="float2" name="p_bumpTile">1.0000 1.0000</param>
  <param type="float" name="p_bumpiness">1.5000</param>
  <param type="float" name="p_lerp">0.0000</param>
</material>