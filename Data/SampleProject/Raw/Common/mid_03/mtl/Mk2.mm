<?xml version="1.0"?>
<material fx="UberCar" mutable="true">
  <param type="bool" name="s_legacySpecular">true</param>
  <param type="bool" name="s_glossFromTexture">true</param>
  <param type="float" name="p_shadowRender_Offset">0.0001</param>
  <param type="float" name="p_lerp">0.0000</param>
  <param type="float" name="p_legacySpecularReflectivity">1.0000</param>
  <param type="float" name="p_legacySpecularPower">7.7600</param>
  <param type="float" name="p_fresnelSide">0.9600</param>
  <param type="float2" name="p_bumpTile">1.0000 1.0000</param>
  <param type="float" name="p_bumpiness">1.0000</param>
  <param type="texture" name="p_ambientOcclusion">MK2_AO</param>
  <param type="texture" name="p_diffuse">Mk2_A</param>
  <param type="texture" name="t_normalMap">Mk2_N</param>
  <param type="texture" name="p_specMap">Mk2_S</param>
</material>