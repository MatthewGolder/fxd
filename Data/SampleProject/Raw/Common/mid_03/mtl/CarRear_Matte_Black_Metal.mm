<?xml version="1.0"?>
<material fx="UberCar" mutable="true">
  <param type="texture" name="p_ambientOcclusion">Car_AO</param>
  <param type="texture" name="p_diffuse">Black_Matte_Metal_Diffuse</param>
  <param type="texture" name="t_normalMap">Car_Normal</param>
  <param type="texture" name="p_specMap">Black_Matte_Metal_Spec</param>
  <param type="bool" name="s_legacySpecular">true</param>
  <param type="bool" name="s_glossFromTexture">true</param>
  <param type="float" name="p_shadowRender_Offset">0.0001</param>
  <param type="float" name="p_legacySpecularReflectivity">2.7900</param>
  <param type="float" name="p_legacySpecularPower">10.0000</param>
  <param type="float" name="p_fresnelSide">1.4500</param>
  <param type="float2" name="p_bumpTile">1.0000 1.0000</param>
  <param type="float" name="p_bumpiness">0.0000</param>
</material>