<?xml version="1.0"?>
<material fx="UberCar" mutable="true">
  <param type="texture" name="p_diffuse">Car_Paint_Diffuse</param>
  <param type="texture" name="t_normalMap">Car_Normal</param>
  <param type="texture" name="p_specMap">Car_Paint_Spec</param>
  <param type="float" name="p_fresnelSide">0.8600</param>
  <param type="bool" name="s_legacySpecular">true</param>
  <param type="float" name="p_legacySpecularPower">25.6400</param>
  <param type="bool" name="s_glossFromTexture">true</param>
  <param type="texture" name="p_ambientOcclusion">Car_AO</param>
  <param type="float" name="p_bumpiness">0.0000</param>
  <param type="float" name="p_legacySpecularReflectivity">1.6900</param>
</material>