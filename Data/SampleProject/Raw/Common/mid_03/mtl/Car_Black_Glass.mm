<?xml version="1.0"?>
<material fx="UberCar" mutable="true">
  <param type="texture" name="p_diffuse">Glass_Diffuse</param>
  <param type="texture" name="t_normalMap">Car_Normal</param>
  <param type="texture" name="p_specMap">Glass_Diffuse</param>
  <param type="bool" name="s_glossFromTexture">true</param>
  <param type="float" name="p_fresnelSide">1.3900</param>
  <param type="bool" name="s_legacySpecular">true</param>
  <param type="float" name="p_legacySpecularPower">25.4200</param>
  <param type="texture" name="p_ambientOcclusion">Car_AO</param>
  <param type="float" name="p_legacySpecularReflectivity">5.3000</param>
</material>