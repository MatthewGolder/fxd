<?xml version="1.0"?>
<material fx="UberCar" mutable="true">
  <param type="texture" name="p_specMap">Tire_Spec</param>
  <param type="texture" name="t_normalMap">Car_Normal</param>
  <param type="texture" name="p_diffuse">Tire_Diffuse</param>
  <param type="float" name="p_fresnelSide">0.1100</param>
  <param type="bool" name="s_glossFromTexture">true</param>
  <param type="bool" name="s_legacySpecular">true</param>
  <param type="float" name="p_legacySpecularPower">31.3600</param>
  <param type="texture" name="p_ambientOcclusion">Wheel_AO</param>
  <param type="float" name="p_bumpiness">0.0000</param>
  <param type="float" name="p_legacySpecularReflectivity">3.8100</param>
</material>