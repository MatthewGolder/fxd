<?xml version="1.0"?>
<material fx="UberCar" mutable="true">
  <param type="texture" name="p_diffuse">Black_Matte_Metal_Diffuse</param>
  <param type="texture" name="t_normalMap">Car_Normal</param>
  <param type="texture" name="p_specMap">Black_Matte_Metal_Spec</param>
  <param type="bool" name="s_glossFromTexture">true</param>
  <param type="float" name="p_fresnelSide">0.2100</param>
  <param type="float" name="p_bumpiness">0.0000</param>
  <param type="bool" name="s_legacySpecular">true</param>
  <param type="float" name="p_legacySpecularPower">31.5700</param>
  <param type="texture" name="p_ambientOcclusion">Car_AO</param>
  <param type="float" name="p_legacySpecularReflectivity">1.0000</param>
</material>