<?xml version="1.0"?>
<material fx="UberCar" mutable="true">
  <param type="texture" name="p_ambientOcclusion">Wheel_AO</param>
  <param type="texture" name="p_diffuse">Matte_Silver_Metal_Diffuse</param>
  <param type="texture" name="t_normalMap">Car_Normal</param>
  <param type="texture" name="p_specMap">Matte_Silver_Metal_Spec</param>
  <param type="float" name="p_bumpiness">0.0000</param>
  <param type="float" name="p_fresnelSide">1.4500</param>
  <param type="bool" name="s_glossFromTexture">true</param>
  <param type="bool" name="s_legacySpecular">true</param>
  <param type="float" name="p_legacySpecularReflectivity">2.7900</param>
</material>