<Shader name="vertColour" type="Shader">
	<Constant_Buffer name="CB" register="0">
		<Param type="float4x4" name="World" />
		<Param type="float4x4" name="View" />
		<Param type="float4x4" name="Projection" />
	</Constant_Buffer>
	<Vertex_Shader_Input name="VS_IN">
		<Param type="float4" name="Position" semantic="Position" />
		<Param type="float4" name="Color" semantic="Color0" />
	</Vertex_Shader_Input>
	<Vertex_Shader_Output name="VS_OUT">
		<Param type="float4" name="Position" semantic="SV_Position" />
		<Param type="float4" name="Color" semantic="Color0" />
	</Vertex_Shader_Output>
	<Pixel_Shader_Output name="PS_OUT">
		<Param type="float4" name="Color" semantic="Color0" />
	</Pixel_Shader_Output>
	<Vertex_Shader>
		float4 worldPosition = mul(Input.Position, CB.World);
		float4 viewPosition = mul(worldPosition, CB.View);
		Output.Position = mul(viewPosition, CB.Projection);
		Output.Color = Input.Color;
	</Vertex_Shader>
	<Pixel_Shader>
		Output.Color = Input.Color;
	</Pixel_Shader>
</Shader>