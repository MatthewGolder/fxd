<Shader name="vertTexUV" type="Shader">
	<texture name="p_texture" sampler="PointClamp" type="Two" register="0"/>

	<Constant_Buffer name="CB" register="0">
		<Param type="float4x4" name="World" />
		<Param type="float4x4" name="View" />
		<Param type="float4x4" name="Projection" />
	</Constant_Buffer>

	<Vertex_Shader_Input name="VS_IN">
		<Param type="float4" name="Position" semantic="Position" />
		<Param type="float2" name="TexUV" semantic="TexCoord0" />
	</Vertex_Shader_Input>

	<Vertex_Shader_Output name="VS_OUT">
		<Param type="float4" name="Position" semantic="SV_Position" />
		<Param type="float2" name="TexUV" semantic="TexCoord0" />
	</Vertex_Shader_Output>

	<Pixel_Shader_Output name="PS_OUT">
		<Param type="float4" name="Color" semantic="Color0" />
	</Pixel_Shader_Output>

	<Vertex_Shader>
		float4 worldPosition = mul(Input.Position, CB.World);
		float4 viewPosition = mul(worldPosition, CB.View);
		Output.Position = mul(viewPosition, CB.Projection);
		Output.TexUV = Input.TexUV;
	</Vertex_Shader>

	<Pixel_Shader>
		float4 texCol = SampleTexture2D(p_texture, Input.TexUV);
		Output.Color = texCol;
	</Pixel_Shader>
</Shader>