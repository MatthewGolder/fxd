// Creator - MatthewGolder
#ifndef TESTBED_GAMEFLOW_MAINMENU_H
#define TESTBED_GAMEFLOW_MAINMENU_H

#include "FXDEngine/App/DisplayLayer.h"
#include "FXDEngine/Accounts/Types.h"

namespace Game
{
	class GameData;

	// ------
	// MainMenuLayer
	// -
	// ------
	class MainMenuLayer FINAL : public FXD::App::IDisplayLayer
	{
	public:
		MainMenuLayer(FXD::Job::AsyncCancel& cancel, Game::GameData& gameData, const FXD::Accounts::PlayerLocal& playerLocal);
		~MainMenuLayer(void);

		FXD::Core::ColourRGBA colour(void) FINAL;

	protected:
		void _load(void) FINAL;
		void _unload(void) FINAL;
		void _update(FXD::F32 dt) FINAL;
		void _render(const FXD::GFX::IViewport* pDisplay) const FINAL;

		void _on_focus_gain(void) FINAL;
		void _on_focus_lost(void) FINAL;

	private:
		FXD::Job::AsyncCancel& m_cancel;
		Game::GameData& m_gameData;
		FXD::Accounts::PlayerLocal m_playerLocal;
	};
} //namespace Game
#endif //TESTBED_GAMEFLOW_STARTUP_H