// Creator - MatthewGolder
#include "SampleProject/GameFlow/StartUp.h"
#include "SampleProject/GameFlow/MainMenu.h"
#include "SampleProject/GameData/GameData.h"
#include "SampleProject/Camera/Camera.h"
#include "FXDEngine/Accounts/AccountJPThread.h"
#include "FXDEngine/Accounts/AccountSystem.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/HWBuffer.h"
#include "FXDEngine/GFX/FX/ShaderModule.h"
#include "FXDEngine/GFX/VertexDec.h"
#include "FXDEngine/Input/Events.h"
#include "FXDEngine/Job/EventHandler.h"
#include "FXDEngine/Math/Constants.h"
#include "FXDEngine/SND/SndNode.h"

using namespace Game;

// ------
// StartUpLayer
// -
// ------
StartUpLayer::StartUpLayer(FXD::Job::AsyncCancel& cancel, Game::GameData& gameData)
	: IDisplayLayer(FXD::App::E_DisplayLayerType::Opaque)
	, m_gameData(gameData)
	, m_cancel(cancel)
	, OnPlayerJoinedEvent(nullptr)
	, OnKeyboardButton(nullptr)
	, OnMouseButton(nullptr)
{
	FXD::FXDAccounts()->account_system()->player_joined(FXD::Accounts::E_PlayerJoinButton::Start);

	OnPlayerJoinedEvent = FXD::FXDEvent()->attach< FXD::Accounts::OnPlayerJoinedEvent >([&](auto evt)
	{
		m_playerJoin = evt.m_player;
	});
	OnKeyboardButton = FXD::FXDEvent()->attach< FXD::Input::OnKeyboardButton >([&](auto evt)
	{
		if ((evt.Pressed) && (evt.Name == "VK_RIGHT"))
		{
			PRINT_INFO << evt.Name;
		}
		if ((evt.Pressed) && (evt.Name == "VK_LEFT"))
		{
			PRINT_INFO << evt.Name;
		}
	});
	OnMouseButton = FXD::FXDEvent()->attach< FXD::Input::OnMouseButton >([&](auto evt)
	{
	});
}

StartUpLayer::~StartUpLayer(void)
{
	FXD::FXDEvent()->detach< FXD::Accounts::OnPlayerJoinedEvent >(OnPlayerJoinedEvent);
	FXD::FXDEvent()->detach< FXD::Input::OnKeyboardButton >(OnKeyboardButton);
	FXD::FXDEvent()->detach< FXD::Input::OnMouseButton >(OnMouseButton);
}

FXD::Core::ColourRGBA StartUpLayer::colour(void)
{
	return FXD::Core::ColourRGBA(FXD::Core::ColourRGB::Red, m_fTransitionValue);
}

void StartUpLayer::_load(void)
{
}

void StartUpLayer::_unload(void)
{
}

Game::Camera g_camera;
FXD::Math::Matrix4F g_Projection;
FXD::Math::Matrix4F g_View;
FXD::Math::Matrix4F g_World;
FXD::F32 g_Time2 = 0.0f;
FXD::F32 g_Blue2 = 0.0f;
extern bool bPress;

void StartUpLayer::_update(FXD::F32 dt)
{
	if (m_playerJoin != nullptr)
	{
		request_remove();
		FXD::App::RunDisplayLayerAsync< Game::MainMenuLayer >(FXD::FXDAsync(), m_cancel, *m_pDisplayEngine, m_gameData, m_playerJoin);
	}

	g_Time2 += dt;
	g_Blue2 = (1.0f + FXD::Math::Sin(g_Time2)) * 0.5f;
	g_World = FXD::Math::Matrix4F::create_rotationY(0.0f/*g_Time2*/);
	g_World *= FXD::Math::Matrix4F::create_scale(g_Blue2, g_Blue2, g_Blue2);

	FXD::Math::Vector3F Eye(0.0f, 1.0f, -5.0f);
	FXD::Math::Vector3F At(0.0f, 1.0f, 0.0f);
	FXD::Math::Vector3F Up(0.0f, 1.0f, 0.0f);
	g_View.lookat_lh(Eye, At, Up);
	g_camera.update(dt);
}

void StartUpLayer::_render(const FXD::GFX::IViewport* pDisplay) const
{
	/*
	vertColour[0].Color = FXD::Core::ColourRGBA(0.0f, 0.0f, g_Blue2, 1.0f);
	vertColour[1].Color = FXD::Core::ColourRGBA::Green;
	vertColour[2].Color = FXD::Core::ColourRGBA::Cyan;
	vertColour[3].Color = FXD::Core::ColourRGBA::Red;
	vertColour[4].Color = FXD::Core::ColourRGBA::Purple;
	vertColour[5].Color = FXD::Core::ColourRGBA::Yellow;
	vertColour[6].Color = FXD::Core::ColourRGBA::White;
	vertColour[7].Color = FXD::Core::ColourRGBA::Black;

	{
		FXD::U32 nWidth = pDisplay->size().x();
		FXD::U32 nHeight = pDisplay->size().y();
		g_Projection.perspective_fov_lh(FXD::Math::PI_HALF, (nWidth / (FXD::F32)nHeight), 0.01f, 100.0f);

		m_cb.mWorld = g_World.get_transpose() * FXD::Math::Matrix4F::create_translation(0.3f, 0.0f, 0.0f);
		m_cb.mView = g_View.get_transpose();
		m_cb.mProjection = g_Projection.get_transpose();
		m_gameData.gfxData().progCol()->uniform()->write_data(0, sizeof(FXD::GFX::FX::TEMP_VS_CONSTANT_BUFFER), &m_cb);
	}

	{
		VertColour in1;
		in1.Pos = FXD::Math::Vector3F(9.0f, 7.0f, 2.0f);
		in1.Color = FXD::Core::ColourRGBA::White;
		VertColour in2;
		in2.Pos = FXD::Math::Vector3F(-2.0f, -11.0f, 0.0f);
		in2.Color = FXD::Core::ColourRGBA::Yellow;
		VertColour out1;
		VertColour out2;

		FXD::GFX::VertexBuffer vbo = m_gameData.gfxData().vbo1();

		vbo->read_data(0, (1 * sizeof(VertColour)), &out1);
		vbo->read_data(sizeof(VertColour), (1 * sizeof(VertColour)), &out2);
		vbo->write_data(0, (1 * sizeof(VertColour)), &in1);
		vbo->write_data(sizeof(VertColour), (1 * sizeof(VertColour)), &in2);
		vbo->read_data(0, (1 * sizeof(VertColour)), &out1);
		vbo->read_data(sizeof(VertColour), (1 * sizeof(VertColour)), &out2);
		float f = 0;
	}

	/*
	g_pImmediateContext->VSSetShader(g_pVertexShader, NULL, 0);
	g_pImmediateContext->VSSetConstantBuffers(0, 1, &g_pCBNeverChanges);
	g_pImmediateContext->VSSetConstantBuffers(1, 1, &g_pCBChangeOnResize);
	g_pImmediateContext->VSSetConstantBuffers(2, 1, &g_pCBChangesEveryFrame);
	g_pImmediateContext->PSSetShader(g_pPixelShader, NULL, 0);
	g_pImmediateContext->PSSetConstantBuffers(2, 1, &g_pCBChangesEveryFrame);
	g_pImmediateContext->PSSetShaderResources(0, 1, &g_pTextureRV);
	g_pImmediateContext->PSSetSamplers(0, 1, &g_pSamplerLinear);
	g_pImmediateContext->DrawIndexed(36, 0, 0);
	*/

	{
		//m_gameData.gfxData().vbo1()->activate(0);
		//m_gameData.gfxData().ibo()->activate(0);

		//m_gameData.gfxData().progCol()->vertexShader()->attach();
		//m_gameData.gfxData().progCol()->vertexShader()->set_uniform(m_gameData.gfxData().progCol()->uniform());
	
		//m_gameData.gfxData().progCol()->pixelShader()->attach();
		//m_gameData.gfxData().progCol()->pixelShader()->set_uniform(m_gameData.gfxData().progCol()->uniform());
		//m_gameData.gfxData().progCol()->vertex_decleration()->attach();
	}
	{
		/*
		FXD::U32 nWidth = pDisplay->size().x();
		FXD::U32 nHeight = pDisplay->size().y();
		g_Projection.perspective_fov_lh(FXD::Math::PI_HALF, (nWidth / (FXD::F32)nHeight), 0.01f, 100.0f);

		m_cb.mWorld = g_World.get_transpose() * FXD::Math::Matrix4F::create_translation(-0.3f, 0.0f, 0.0f);
		m_cb.mView = g_View.get_transpose();
		m_cb.mProjection = g_Projection.get_transpose();
		m_gameData.gfxData().progUV()->uniform()->write_data(0, sizeof(FXD::GFX::FX::TEMP_VS_CONSTANT_BUFFER), &m_cb);
		*/
	}
	{
		/*
		m_gameData.gfxData().vbo2()->write_data(0, (8 * sizeof(VertTexUV)), &vertTexUV);
		m_gameData.gfxData().vbo2()->activate(0);
		m_gameData.gfxData().ibo()->activate(0);

		m_gameData.gfxData().progUV()->vertexShader()->attach();
		m_gameData.gfxData().progUV()->pixelShader()->attach();
		m_gameData.gfxData().progUV()->vertexShader()->set_uniform(m_gameData.gfxData().progUV()->uniform());
		m_gameData.gfxData().progUV()->pixelShader()->set_uniform(m_gameData.gfxData().progUV()->uniform());
		m_gameData.gfxData().progUV()->vertex_decleration()->attach();
		*/
	}
}

void StartUpLayer::_on_focus_gain(void)
{
}

void StartUpLayer::_on_focus_lost(void)
{
}