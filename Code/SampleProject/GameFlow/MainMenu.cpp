// Creator - MatthewGolder
#include "SampleProject/GameFlow/MainMenu.h"
#include "SampleProject/GameData/GameData.h"
#include "FXDEngine/Accounts/PlayerLocal.h"
#include "FXDEngine/Input/InputMapDesc.h"

using namespace Game;

// ------
// MainMenuLayer
// -
// ------
MainMenuLayer::MainMenuLayer(FXD::Job::AsyncCancel& cancel, Game::GameData& gameData, const FXD::Accounts::PlayerLocal& playerLocal)
	: IDisplayLayer(FXD::App::E_DisplayLayerType::Opaque)
	, m_cancel(cancel)
	, m_gameData(gameData)
	, m_playerLocal(playerLocal)
{
	m_playerLocal->player_input().set_inputmap_def(gameData.inputData().inputMapDesc(), "UI");

	PRINT_INFO << m_playerLocal->persona();
	PRINT_INFO << m_playerLocal->is_local();
	PRINT_INFO << m_playerLocal->globalID();
	PRINT_INFO << m_playerLocal->platform_id();
}

MainMenuLayer::~MainMenuLayer(void)
{
}

FXD::Core::ColourRGBA MainMenuLayer::colour(void)
{
	return FXD::Core::ColourRGBA(FXD::Core::ColourRGB::Blue, m_fTransitionValue);
}

void MainMenuLayer::_load(void)
{
}

void MainMenuLayer::_unload(void)
{
}

void MainMenuLayer::_update(FXD::F32 /*dt*/)
{
	FXD::U32 nID = m_gameData.inputData().inputMapDesc()->getControlID("Back");
	bool bBool = m_playerLocal->player_input().m_inputMap.get_bool(nID);
	if (bBool)
	{
		PRINT_INFO << bBool;
	}
}

void MainMenuLayer::_render(const FXD::GFX::IViewport* /*pDisplay*/) const
{
}

void MainMenuLayer::_on_focus_gain(void)
{
}

void MainMenuLayer::_on_focus_lost(void)
{
}