// Creator - MatthewGolder
#ifndef SAMPLEPROJECT_GAMEFLOW_STARTUP_H
#define SAMPLEPROJECT_GAMEFLOW_STARTUP_H

#include "SampleProject/GameData/GFXData.h"
#include "FXDEngine/Accounts/Types.h"
#include "FXDEngine/Accounts/Events.h"
#include "FXDEngine/App/DisplayLayer.h"
#include "FXDEngine/Input/Events.h"
#include "FXDEngine/GFX/FX/ShaderModule.h"

namespace Game
{
	class GameData;

	// ------
	// StartUpLayer
	// -
	// ------
	class StartUpLayer FINAL : public FXD::App::IDisplayLayer
	{
	public:
		StartUpLayer(FXD::Job::AsyncCancel& cancel, Game::GameData& gameData);
		~StartUpLayer(void);

		FXD::Core::ColourRGBA colour(void) FINAL;

	protected:
		void _load(void) FINAL;
		void _unload(void) FINAL;
		void _update(FXD::F32 dt) FINAL;
		void _render(const FXD::GFX::IViewport* pDisplay) const FINAL;

		void _on_focus_gain(void) FINAL;
		void _on_focus_lost(void) FINAL;

	private:
		Game::GameData& m_gameData;
		FXD::Job::AsyncCancel& m_cancel;
		FXD::Accounts::PlayerLocal m_playerJoin;

		mutable FXD::GFX::FX::TEMP_VS_CONSTANT_BUFFER m_cb;

		const FXD::Job::Handler< FXD::Accounts::OnPlayerJoinedEvent >* OnPlayerJoinedEvent;
		const FXD::Job::Handler< FXD::Input::OnKeyboardButton >* OnKeyboardButton;
		const FXD::Job::Handler< FXD::Input::OnMouseButton >* OnMouseButton;
	};
} //namespace Game
#endif //SAMPLEPROJECT_GAMEFLOW_STARTUP_H