// Creator - MatthewGolder
#ifndef TESTBED_CAMERA_CAMERA_H
#define TESTBED_CAMERA_CAMERA_H

#include "FXDEngine/Math/Matrix4.h"
#include "FXDEngine/Math/Quaternion.h"
#include "FXDEngine/Math/Vector3F.h"

namespace Game
{
	// ------
	// Camera
	// -
	// ------
	class Camera
	{
	public:
		Camera(void);
		~Camera(void);

		void update(FXD::F32 dt);

		FXD::Math::Vector3F m_position; // Camera position
		FXD::Math::Vector3F m_rotation; // Camera rotation
	private:

		static const FXD::F32 ROTATION_SPEED; // Determines speed for rotation, in degrees per second.
	};
} //namespace Game
#endif //TESTBED_CAMERA_CAMERA_H