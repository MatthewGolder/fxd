// Creator - MatthewGolder
#include "SampleProject/Camera/Camera.h"
#include "FXDEngine/App/FXDApi.h"

using namespace Game;

// ------
// Camera
// -
// ------
Camera::Camera(void)
	: m_position(FXD::Math::Vector3F::kZero)
	, m_rotation(FXD::Math::Vector3F::kZero)
{}

Camera::~Camera(void)
{}

void Camera::update(FXD::F32 dt)
{
	m_rotation += FXD::Math::Vector3F(10.0f * dt, 0.0f, 0.0f);

	// Check if any movement or rotation keys are being held
	//bool goingForward = gVirtualInput().isButtonHeld(mMoveForward);
	//bool goingBack = gVirtualInput().isButtonHeld(mMoveBack);
	//bool goingLeft = gVirtualInput().isButtonHeld(mMoveLeft);
	//bool goingRight = gVirtualInput().isButtonHeld(mMoveRight);
	//bool fastMove = gVirtualInput().isButtonHeld(mFastMove);
	//bool camRotating = gVirtualInput().isButtonHeld(mRotateCam);


	//FXD::F32 mYaw; mYaw += FXD::Math::Degree(gVirtualInput().getAxisValue(mHorizontalAxis) * ROTATION_SPEED);
	//FXD::F32 mPitch; mPitch += FXD::Math::Degree(gVirtualInput().getAxisValue(mVerticalAxis) * ROTATION_SPEED);


	// If the movement button is pressed, determine direction to move in
	//FXD::Math::Vector3F direction = FXD::Math::Vector3F::kZero;
	//if (goingForward) direction += tfrm.get_forward();
	//if (goingBack) direction -= tfrm.get_forward();
	//if (goingRight) direction += tfrm.get_right();
	//if (goingLeft) direction -= tfrm.get_right();
}