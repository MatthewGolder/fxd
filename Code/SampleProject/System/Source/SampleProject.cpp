// Creator - MatthewGolder
#include "SampleProject/System/SampleProject.h"
#include "SampleProject/GameFlow/StartUp.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Thread/ThreadPerformance.h"

#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOSystem.h"
#include "FXDEngine/IO/IODevice.h"
#include "FXDEngine/IO/IOStreamFile.h"
#include "FXDEngine/IO/ZipDirectoryIterator.h"

using namespace Game;
using namespace FXD;

// ------
// FXDTest
// -
// ------
FXDTest::FXDTest(void)
{
}

FXDTest::~FXDTest(void)
{
}

void FXDTest::run_game(void)
{
	IEngineApp::run_game();

	// 1. Start the game timer 
	//m_timer.start_counter();
	FXD::U32 nCounter = 0;
	while (m_eGameState != App::E_GameState::Stopped)
	{
		nCounter++;
		if (nCounter > 100)
		{
			nCounter = 0;
		}
		if (nCounter < 50)
		{
			m_clearColour = FXD::Core::ColourRGBA::Blue;
		}
		if (nCounter >= 50)
		{
			m_clearColour = FXD::Core::ColourRGBA::Red;
		}

		m_gameData.gfxData().load2();

		m_gameData.gfxData().gfxAdapter()->RHIBeginDrawingViewport(m_gameData.gfxData().gfxViewport());
		m_gameData.gfxData().gfxAdapter()->RHIClearViewport(m_gameData.gfxData().gfxViewport(), m_clearColour, (GFX::E_ClearFlag::Colour | GFX::E_ClearFlag::Depth), 1.0f, 0.0f, 0);
		/*
		m_gameData.gfxData().gfxAdapter()->RHIFrame(this);
		*/
		m_gameData.gfxData().gfxAdapter()->RHIEndDrawingViewport(m_gameData.gfxData().gfxViewport());

		m_gameData.gfxData().unload2();
		Thread::ThreadPerformance::process_thread_counters();
		//FXD::Memory::Log::PrintTotalStats();
	}
}

void FXDTest::load(void)
{
	/*
	unzFile unzFile = unzOpen64(UTF_8("C:\\Media\\Code\\FXD\\Data\\SampleProject\\Compress\\Android\\Common\\.zip"));
	Core::StreamOut fileStream = FXDIO()->io_system()->io_device()->file_open_zip_stream(unzFile, "Common/Audio/AudioGraph.xml", IO::E_FileMode::Read).get();

	FXD::IO::Path path(UTF_8("C:\\Media\\Code\\FXD\\Data\\SampleProject\\Compress\\Android\\Common\\.zip"));
	{
		for (auto itrPath = FXD::IO::zip_directory_iterator(path, "*"); itrPath != FXD::IO::zip_directory_iterator(); ++itrPath)
		{
			FXD::IO::FileStatus s = FXD::IO::FS::Status((*itrPath).path());
			PRINT_INFO << (*itrPath).path();
		}
	}
	*/

	IEngineApp::load();
	m_gameData.audioData().load();
	m_gameData.gfxData().load();
	m_gameData.inputData().load();

	Job::Future< void > layer = App::RunDisplayLayerAsync< Game::StartUpLayer >(FXDAsync(), m_appCancel, m_displayEngine, m_gameData);
}

void FXDTest::unload(void)
{
	m_gameData.inputData().unload();
	m_gameData.gfxData().unload();
	m_gameData.audioData().unload();
	m_appCancel.cancel();
	IEngineApp::unload();
}