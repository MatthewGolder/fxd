// Creator - MatthewGolder
#include "SampleProject/System/SampleProject.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/Thread/ThreadLocalStorage.h"
#include "FXDEngine/Thread/ThreadPerformance.h"

#include "SampleProject/System/addressbook.pb.h"

// ------
// 
// -
// ------
FXD::S32 fxd_main(FXD::S32 argc, FXD::UTF8* argv[])
{
	{
		FXD::AddressBook ab1;
		FXD::Person* p1 = ab1.add_people();
		FXD::Person* p2 = ab1.add_people();
		p1->set_name("adad");
		p1->set_id(111);
		p1->set_email("adad@adad.com");
		p2->set_name("wong");
		p2->set_id(222);
		p2->set_email("wong@wong.com");

		std::string str = ab1.SerializeAsString();
		{
			FXD::AddressBook ab2;
			ab2.ParseFromString(str);
			auto& people = ab2.people();
			fxd_for(const auto& person, people)
			{
				PRINT_INFO << person.name().c_str();
				PRINT_INFO << person.email().c_str();
				PRINT_INFO << person.id();
			}
			float f = 0;
		}
	}

	FXD::Thread::Funcs::SetCurrentThreadName(UTF_8("Main Thread"));
	FXD::Thread::ThreadLocalData::initialise();
	FXD::Thread::ThreadPerformance::initialise(UTF_8("Main Thread"));

	{
		Game::FXDTest game;
		if (game.init_game(argc, argv, FXD::App::E_Api::Accounts | FXD::App::E_Api::Input | FXD::App::E_Api::SND | FXD::App::E_Api::GFX))
		{
			game.run_game();
		}
		game.shutdown_game();
	}

	FXD::Thread::ThreadPerformance::shutdown();
	FXD::Thread::ThreadLocalData::shutdown();

	FXD::Memory::Log::PrintTotalStats();
	FXD::Memory::Log::DumpMemory(UTF_8("memorydump.txt"));
	return 0;
}

// ------
// FXDGlobals
// -
// ------
FXD::Core::String8 FXD::App::GetGameNameUTF8(void)
{
	return UTF_8("FXDTest");
}

FXD::Core::String16 FXD::App::GetGameNameUTF16(void)
{
	return UTF_16("FXDTest");
}

FXD::Core::String8 FXD::App::GetDisplayGameNameUTF8(void)
{
	return UTF_8("FXDTest");
}

FXD::Core::String16 FXD::App::GetDisplayGameNameUTF16(void)
{
	return UTF_16("FXDTest");
}

FXD::Core::String8 FXD::App::GetShortNameUTF8(void)
{
	return UTF_8("FXDTest");
}

FXD::Core::String16 FXD::App::GetShortNameUTF16(void)
{
	return UTF_16("FXDTest");
}