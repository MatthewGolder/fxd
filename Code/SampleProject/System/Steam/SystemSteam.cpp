// Creator - MatthewGolder
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Accounts/Types.h"

#if IsOSPC() && IsAccountSteam()
#include "FXDEngine/App/Steam/GlobalsSteam.h"
#include "FXDEngine/App/Platform.h"

// ------
// 
// -
// ------
extern FXD::S32 fxd_main(FXD::S32 argc, FXD::UTF8* argv[]);

FXD::S32 main(FXD::S32 argc, FXD::UTF8* argv[])
{
	return fxd_main(argc, argv);
}

// ------
// GlobalData
// -
// ------
FXD::App::GlobalData& FXD::App::GetGlobalData(void)
{
	static FXD::App::GlobalData globalData(480, false, k_EPositionBottomRight);
	return globalData;
}
#endif //IsOSPC() && IsAccountSteam()