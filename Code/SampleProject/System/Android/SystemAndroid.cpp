// Creator - MatthewGolder
#include "FXDEngine/Accounts/Types.h"

#if IsOSAndroid()
#include "FXDEngine/App/Android/GlobalsAndroid.h"
#include "FXDEngine/App/Android/GlueAndroid.h"
#include "FXDEngine/App/Platform.h"

// ------
// 
// -
// ------
extern "C"
{
	void AndroidMain(ANativeActivity* pActivity, void* pSavedState, size_t nSavedStateSize)
	{
		FXD::App::FXDAndroidApp2 app(pActivity, pSavedState, nSavedStateSize);
	}
}

// ------
// GlobalData
// -
// ------
FXD::App::GlobalData& FXD::App::GetGlobalData(void)
{
	static FXD::App::GlobalData globalData(480, false, nullptr);
	return globalData;
}

// ------
// extern
// -
// ------
extern "C"
{
	/*
	static pthread_t g_mainThread;
	JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* jvm, void* pBReserved)
	{
		FXD::App::GlobalData().m_lockable.get_quick_write_lock()->m_pJavaVM = jvm;
		return JNI_VERSION_1_6;
	}
	JNIEXPORT void JNICALL JNI_OnUnload(JavaVM* jvm, void* pBReserved)
	{
		FXD::App::GlobalData().m_lockable.get_quick_write_lock()->m_pJavaVM = nullptr;
	}

	JNIEXPORT void JNICALL Java_com_GolderGames_SOA_FXDWrapper_nativeOnCreate(JNIEnv* pEnv, jobject thiz, FXD::F32 dpiX, FXD::F32 dpiY)
	{
		FXD::App::GlobalData().m_lockable.get_quick_write_lock()->m_fxdWrapper = pEnv->NewGlobalRef(thiz);
		pthread_create(&g_mainThread, 0, mainThreadProc, 0);
	}
	JNIEXPORT void JNICALL Java_com_GolderGames_SOA_FXDWrapper_nativeOnStart(JNIEnv* pEnv, jobject thiz)
	{
		float f = 0;
	}
	JNIEXPORT void JNICALL Java_com_GolderGames_SOA_FXDWrapper_nativeOnResume(JNIEnv* pEnv, jobject thiz)
	{
		float f = 0;
	}
	JNIEXPORT void JNICALL Java_com_GolderGames_SOA_FXDWrapper_nativeOnPause(JNIEnv* pEnv, jobject thiz)
	{
		float f = 0;
	}
	JNIEXPORT void JNICALL Java_com_GolderGames_SOA_FXDWrapper_nativeOnStop(JNIEnv* pEnv, jobject thiz)
	{
		float f = 0;
	}
	JNIEXPORT void JNICALL Java_com_GolderGames_SOA_FXDWrapper_nativeOnRestart(JNIEnv* pEnv, jobject thiz)
	{
		float f = 0;
	}
	JNIEXPORT void JNICALL Java_com_GolderGames_SOA_FXDWrapper_nativeOnDestroy(JNIEnv* pEnv, jobject thiz)
	{
		pEnv->DeleteGlobalRef(FXD::App::GlobalData().m_lockable.get_quick_write_lock()->m_fxdWrapper);
		float f = 0;
	}
	*/
}
#endif //IsOSAndroid()