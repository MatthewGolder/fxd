// Creator - MatthewGolder
#include "FXDEngine/App/Types.h"

#if IsOSAndroid()
/*
#include "FXDEngine/App/Android/GlueAndroid.h"
#include "FXDEngine/App/Android/GlobalsAndroid.h"
#include "FXDEngine/App/Android/PlatformAndroid.h"
#include <android/sensor.h>

struct saved_state
{
	float angle;
	int32_t x;
	int32_t y;
};

struct FXDAndroidEngine
{
	struct FXDAndroidApp* m_pApp;
	//ASensorManager* m_pSensorManager;
	//const ASensor* m_pAccelerometerSensor;
	//ASensorEventQueue* m_pSensorEventQueue;

	int animating;
	struct saved_state state;
};

static int32_t engine_handle_input(struct FXDAndroidApp* pApp, AInputEvent* pEvent)
{
	struct engine* pEngine = (struct engine*)pApp->m_pUserData;
}

static void engine_handle_cmd(struct FXDAndroidApp* pApp, int32_t cmd)
{
	struct engine* pEngine = (struct engine*)pApp->m_pUserData;
}

void android_main(struct FXDAndroidApp* pApp)
{
	FXD::Core::String8 str;

	struct FXDAndroidEngine engine;
	memset(&engine, 0x00, sizeof(engine));
	engine.m_pApp = pApp;
	pApp->m_pUserData = &engine;
	pApp->onAppCmd = engine_handle_cmd;
	pApp->onInputEvent = engine_handle_input;

	// Prepare to monitor accelerometer
	//engine.m_pSensorManager = ASensorManager_getInstance();
	//engine.m_pAccelerometerSensor = ASensorManager_getDefaultSensor(engine.m_pSensorManager, ASENSOR_TYPE_ACCELEROMETER);
	//engine.m_pSensorEventQueue = ASensorManager_createEventQueue(engine.m_pSensorManager, pApp->m_pLooper, LOOPER_ID_USER, NULL, NULL);

	if (pApp->m_pSavedState != NULL)
	{
		// We are starting with a previous saved state; restore from it.
		engine.state = *(struct saved_state*)pApp->m_pSavedState;
	}

	engine.animating = 1;

	while (1)
	{
		// Read all pending events.
		int ident;
		int events;
		struct FXDAndroidPollSource* pPollSource;

		while ((ident = ALooper_pollAll(engine.animating ? 0 : -1, NULL, &events, (void**)&pPollSource)) >= 0)
		{
			// Process this event.
			//if (pPollSource != NULL)
			//{
			//	pPollSource->process(pApp, pPollSource);
			//}
			// If a sensor has data, process it now.
			//if (ident == LOOPER_ID_USER)
			//{
			//	if (engine.m_pAccelerometerSensor != NULL)
			//	{
			//		ASensorEvent pSensorEvent;
			//		while (ASensorEventQueue_getEvents(engine.m_pSensorEventQueue, &pSensorEvent, 1) > 0)
			//		{
			//			LOGI("accelerometer: x=%f y=%f z=%f", pSensorEvent.acceleration.x, pSensorEvent.acceleration.y, pSensorEvent.acceleration.z);
			//		}
			//	}
			//}
			if (pApp->m_nDestroyRequested != 0)
			{
				//engine_term_display(&engine);
				return;
			}
		}
	}
}
*/
#endif //IsOSAndroid()