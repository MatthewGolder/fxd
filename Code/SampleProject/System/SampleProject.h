// Creator - MatthewGolder
#ifndef SAMPLEPROJECT_SYSTEM_SAMPLEPROJECT_H
#define SAMPLEPROJECT_SYSTEM_SAMPLEPROJECT_H

#include "FXDEngine/App/EngineApp.h"
#include "SampleProject/GameData/GameData.h"

namespace Game
{
	// ------
	// FXDTest
	// -
	// ------
	class FXDTest : public FXD::App::IEngineApp
	{
	public:
		FXDTest(void);
		~FXDTest(void);

		void run_game(void) FINAL;
	
		void load(void) FINAL;
		void unload(void) FINAL;

	private:
		FXD::Job::AsyncCancel m_appCancel;
		Game::GameData m_gameData;
		FXD::Core::ColourRGBA m_clearColour = FXD::Core::ColourRGBA::Blue;
	};

} //namespace Game
#endif //SAMPLEPROJECT_SYSTEM_SAMPLEPROJECT_H