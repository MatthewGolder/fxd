// Creator - MatthewGolder
#ifndef TESTBED_GAMEDATA_INPUTDATA_H
#define TESTBED_GAMEDATA_INPUTDATA_H

#include "FXDEngine/Input/InputMap.h"
#include "FXDEngine/IO/FileCache.h"

namespace Game
{
	// ------
	// InputData
	// -
	// ------
	class InputData
	{
	public:
		InputData(void);
		~InputData(void);

		void load(void);
		void unload(void);
		
		GET_REF_W(FXD::Input::InputMapDesc, inputMapDesc, inputMapDesc);
		GET_REF_W(FXD::IO::FileCache< FXD::Input::InputMapDesc >, inputMapDescCache, inputMapDescCache);

	private:
		FXD::Input::InputMapDesc m_inputMapDesc;
		FXD::IO::FileCache< FXD::Input::InputMapDesc > m_inputMapDescCache;
	};
} //namespace Game
#endif //TESTBED_GAMEDATA_INPUTDATA_H