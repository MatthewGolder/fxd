// Creator - MatthewGolder
#include "SampleProject/GameData/InputData.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Job/Async.h"
#include "FXDEngine/Input/InputMapDesc.h"
#include "FXDEngine/IO/IODevice.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOSystem.h"
#include "FXDEngine/Memory/StreamMemHandle.h"

using namespace Game;

FXD::Input::InputMapDesc InputMapDefLoadFunc(const FXD::IO::Path& ioPath)
{
	FXD::Core::StreamIn stream = FXD::FXDIO()->io_system()->io_device()->file_open_memory_stream(ioPath, false).get();
	return FXD::Input::IInputMapDesc::FromStream(stream, ioPath.filename());
}

// ------
// InputData
// -
// ------
InputData::InputData(void)
{
}

InputData::~InputData(void)
{
}

void InputData::load(void)
{
	auto funcRef = [&]()
	{
		FXD::IO::Path ioPath = FXD::IO::FS::GetCWD() / UTF_8("Common") / UTF_8("Input") / UTF_8("Controls.xml");
		m_inputMapDesc = inputMapDescCache().load(ioPath, InputMapDefLoadFunc);
	};

	FXD::Job::Async(FXD::FXDAsync(), [=]() { funcRef(); }).wait();
}

void InputData::unload(void)
{
}