// Creator - MatthewGolder
#ifndef TESTBED_GAMEDATA_GFXDATA_H
#define TESTBED_GAMEDATA_GFXDATA_H

#include "FXDEngine/GFX/GfxAdapter.h"
#include "FXDEngine/GFX/Viewport.h"
#include "FXDEngine/Math/Matrix4.h"
#include "FXDEngine/Math/Vector2F.h"
#include "FXDEngine/Math/Vector3F.h"
#include "FXDEngine/Scene/Types.h"

struct VertColour
{
	FXD::Math::Vector3F Pos;
	FXD::Core::ColourRGBA Color;
};
struct VertTexUV
{
	FXD::Math::Vector3F Pos;
	FXD::Math::Vector2F UV;
};

extern VertColour vertColour[];
extern VertTexUV vertTexUV[];
extern FXD::U16 indices[];

namespace Game
{
	// ------
	// GFXData
	// -
	// ------
	class GFXData
	{
	public:
	public:
		GFXData(void);
		~GFXData(void);

		void load(void);
		void load2(void);
		void unload(void);
		void unload2(void);

		GET_REF_W(FXD::GFX::GfxAdapter, gfxAdapter, gfxAdapter);
		GET_REF_W(FXD::GFX::Window, window, window);
		GET_REF_W(FXD::GFX::Viewport, gfxViewport, gfxViewport);
		//GET_REF_W(FXD::GFX::Viewport, gfxDisplay2, gfxDisplay2);

		GET_REF_W(FXD::GFX::VertexBuffer, vbo1, vbo1);
		GET_REF_W(FXD::GFX::VertexBuffer, vbo2, vbo2);
		GET_REF_W(FXD::GFX::IndexBuffer, ibo, ibo);
		GET_REF_W(FXD::GFX::FX::ShaderResource, progCol, progCol);
		//GET_REF_W(FXD::GFX::FX::ShaderResource, progUV, progUV);

	private:
		FXD::GFX::Window m_window;
		FXD::GFX::GfxAdapter m_gfxAdapter;
		FXD::GFX::Viewport m_gfxViewport;
		//FXD::GFX::Viewport m_gfxDisplay2;

		FXD::GFX::VertexBuffer m_vbo1, m_vbo2;
		FXD::GFX::IndexBuffer m_ibo;
		FXD::GFX::FX::Pipeline m_pipeCol;
		FXD::GFX::FX::ShaderResource m_progCol; //, m_progUV;
		//FXD::GFX::Texture m_textureTGA, m_texturePNG;
		//FXD::GFX::Image m_imageTGA, m_imagePNG;
		//FXD::Scene::Geo m_geo;
	};
} //namespace Game
#endif //TESTBED_GAMEDATA_AUDIODATA_H