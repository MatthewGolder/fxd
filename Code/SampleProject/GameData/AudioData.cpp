// Creator - MatthewGolder
#include "SampleProject/GameData/AudioData.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/SndNode.h"
#include "FXDEngine/SND/SndSystem.h"

using namespace Game;

// ------
// AudioData
// -
// ------
AudioData::AudioData(void)
{
}

AudioData::~AudioData(void)
{
}

void AudioData::load(void)
{
	// 1. Create the adapters
	auto adapterDesc = FXD::FXDSND()->snd_system()->snd_api()->find_default_adapter();
	m_sndAdapter = FXD::FXDSND()->snd_system()->create_adapter(adapterDesc).get();

	// 2. Create the audio graph
	FXD::IO::Path audioPath = FXD::IO::FS::GetCWD() / UTF_8("Common") / UTF_8("Audio");
	bool bRet = m_sndAdapter->master_node()->load_nodes(audioPath / UTF_8("AudioGraph.xml"));

	m_loopGroup = m_sndAdapter->load_loop_points(audioPath / UTF_8("Source") / UTF_8("Loop.xml")).get();
	m_audioWav = FXD::FXDSND()->snd_system()->load_audio(audioPath / UTF_8("Source") / UTF_8("Devil_Trigger.wav")).get();
	m_audioOgg = FXD::FXDSND()->snd_system()->load_audio(audioPath / UTF_8("Source") / UTF_8("CountCue.ogg")).get();
	
	FXD::SND::NODE_VOICE_DESC nodeVoiceDesc = {};
	nodeVoiceDesc.Flags = FXD::SND::E_CreateFlag::Streamed;
	nodeVoiceDesc.Audio = m_audioWav;
	nodeVoiceDesc.LoopGroup = m_loopGroup;
	m_voice = m_sndAdapter->master_node()->add_voice(nodeVoiceDesc);
}

void AudioData::unload(void)
{
	if (m_sndAdapter)
	{
		FXD::FXDSND()->snd_system()->release_adapter(m_sndAdapter).get();
		m_sndAdapter = nullptr;
	}
}