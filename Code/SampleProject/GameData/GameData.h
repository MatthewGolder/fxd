// Creator - MatthewGolder
#ifndef TESTBED_GAMEDATA_GAMEDATA_H
#define TESTBED_GAMEDATA_GAMEDATA_H

#include "SampleProject/GameData/AudioData.h"
#include "SampleProject/GameData/GFXData.h"
#include "SampleProject/GameData/InputData.h"

namespace Game
{
	// ------
	// GameData
	// -
	// ------
	class GameData
	{
	public:
		GameData(void);
		~GameData(void);

		GET_REF_W(Game::AudioData, audioData, audioData);
		GET_REF_W(Game::GFXData, gfxData, gfxData);
		GET_REF_W(Game::InputData, inputData, inputData);

	private:
		Game::AudioData m_audioData;
		Game::GFXData m_gfxData;
		Game::InputData m_inputData;
	};
} //namespace Game
#endif //TESTBED_GAMEDATA_GAMEDATA_H