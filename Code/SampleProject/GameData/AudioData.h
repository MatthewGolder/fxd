// Creator - MatthewGolder
#ifndef TESTBED_GAMEDATA_AUDIODATA_H
#define TESTBED_GAMEDATA_AUDIODATA_H

#include "FXDEngine/SND/SndAdapter.h"

namespace Game
{
	// ------
	// AudioData
	// -
	// ------
	class AudioData
	{
	public:
		AudioData(void);
		~AudioData(void);

		void load(void);
		void unload(void);

		GET_REF_W(FXD::SND::SndAdapter, sndAdapter, snd_adapter);
		GET_REF_W(FXD::SND::Audio, audioWav, audio_wav);
		GET_REF_W(FXD::SND::Audio, audioOgg, audio_ogg);

	private:
		FXD::SND::SndAdapter m_sndAdapter;
		FXD::SND::SndLoopGroup m_loopGroup;
		FXD::SND::Audio m_audioWav, m_audioOgg;
		FXD::SND::NodeVoice m_voice;
	};
} //namespace Game
#endif //TESTBED_GAMEDATA_AUDIODATA_H