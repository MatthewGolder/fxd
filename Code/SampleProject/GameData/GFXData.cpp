// Creator - MatthewGolder
#include "SampleProject/GameData/GFXData.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/GfxAdapter.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/GfxSystem.h"
#include "FXDEngine/GFX/HWBuffer.h"
#include "FXDEngine/GFX/Window.h"
#include "FXDEngine/GFX/WindowManager.h"
#include "FXDEngine/GFX/FX/FXLibrary.h"
#include "FXDEngine/GFX/Decoders/Texture.h"

using namespace Game;

VertColour vertColour[] =
{
	{ FXD::Math::Vector3F(-1.0f, 1.0f, -1.0f), FXD::Core::ColourRGBA::Blue },
	{ FXD::Math::Vector3F(1.0f, 1.0f, -1.0f), FXD::Core::ColourRGBA::Green },
	{ FXD::Math::Vector3F(1.0f, 1.0f, 1.0f), FXD::Core::ColourRGBA::Cyan },
	{ FXD::Math::Vector3F(-1.0f, 1.0f, 1.0f), FXD::Core::ColourRGBA::Red },

	{ FXD::Math::Vector3F(-1.0f, -1.0f, -1.0f), FXD::Core::ColourRGBA::Purple },
	{ FXD::Math::Vector3F(1.0f, -1.0f, -1.0f), FXD::Core::ColourRGBA::Yellow },
	{ FXD::Math::Vector3F(1.0f, -1.0f, 1.0f), FXD::Core::ColourRGBA::White },
	{ FXD::Math::Vector3F(-1.0f, -1.0f, 1.0f), FXD::Core::ColourRGBA::Black }
};

VertTexUV vertTexUV[] =
{
	{ FXD::Math::Vector3F(-1.0f, 1.0f, -1.0f), FXD::Math::Vector2F(0.0f, 0.0f) },
	{ FXD::Math::Vector3F(1.0f, 1.0f, -1.0f), FXD::Math::Vector2F(0.0f, 0.0f) },
	{ FXD::Math::Vector3F(1.0f, 1.0f, 1.0f), FXD::Math::Vector2F(0.0f, 0.0f) },
	{ FXD::Math::Vector3F(-1.0f, 1.0f, 1.0f), FXD::Math::Vector2F(0.0f, 0.0f) },

	{ FXD::Math::Vector3F(-1.0f, -1.0f, -1.0f), FXD::Math::Vector2F(0.0f, 0.0f) },
	{ FXD::Math::Vector3F(1.0f, -1.0f, -1.0f), FXD::Math::Vector2F(0.0f, 0.0f) },
	{ FXD::Math::Vector3F(1.0f, -1.0f, 1.0f), FXD::Math::Vector2F(0.0f, 0.0f) },
	{ FXD::Math::Vector3F(-1.0f, -1.0f, 1.0f), FXD::Math::Vector2F(0.0f, 0.0f) }
};

FXD::U16 indices[] =
{
	3, 1, 0,
	2, 1, 3,

	0, 5, 4,
	1, 5, 0,

	3, 4, 7,
	0, 4, 3,

	1, 6, 5,
	2, 6, 1,

	2, 7, 6,
	3, 7, 2,

	6, 4, 5,
	7, 4, 6,
};


// ------
// GFXData
// -
// ------
GFXData::GFXData(void)
{
}

GFXData::~GFXData(void)
{
}

void GFXData::load(void)
{
	// 1. Create the Window
	FXD::GFX::WINDOW_DESC windowDesc = {};
	windowDesc.bVSync = false;
	windowDesc.bFocused = false;
	windowDesc.bResizable = true;
	windowDesc.bFullscreen = false;
	windowDesc.bVisible = true;
	windowDesc.bDepthBuffer = true;
	m_window = FXD::FXDGFX()->window_manager()->create_window(windowDesc);

	// 2. Create the adapter
	auto adapterDesc = FXD::FXDGFX()->gfx_system()->gfx_api()->find_default_adapter();
	m_gfxAdapter = FXD::FXDGFX()->gfx_system()->create_adapter(adapterDesc).get();
	m_gfxViewport = m_gfxAdapter->create_viewport(m_window.get()).get();

	FXD::IO::Path rootPath = FXD::IO::FS::GetCWD();
	//m_progCol = m_gfxAdapter->fx_library()->load_shader(rootPath / UTF_8("Common") / UTF_8("Effects") / UTF_8("VertColour.shader")).get();
	//m_progUV = m_gfxAdapter->fx_library()->load_shader(rootPath / UTF_8("Common") / UTF_8("Effects") / UTF_8("VertTexUV.shader")).get();

	//m_geo = FXD::FXDGFX()->gfx_system()->load_geo(rootPath / UTF_8("Common") / UTF_8("mid_03") / UTF_8("mesh") / UTF_8("Engine.dae")).get();
	//m_texturePNG = FXD::FXDGFX()->gfx_system()->load_texture(rootPath / UTF_8("Common") / UTF_8("Textures") / UTF_8("PNG") / UTF_8("tileAbove-32.png")).get();
	//m_textureTGA = FXD::FXDGFX()->gfx_system()->load_texture(rootPath / UTF_8("Common") / UTF_8("Textures") / UTF_8("TGA") / UTF_8("tileAbove-32.tga")).get();

	//m_imagePNG = m_gfxAdapter->create_image(m_texturePNG, FXD::GFX::BIND_SHADER_RESOURCE).get();
	//m_imageTGA = m_gfxAdapter->create_image(m_textureTGA, FXD::GFX::BIND_SHADER_RESOURCE).get();

	m_vbo1 = m_gfxAdapter->create_vertex_buffer(FXD::GFX::BIND_VERTEX_BUFFER, 8, sizeof(VertColour), &vertColour).get();
	m_vbo2 = m_gfxAdapter->create_vertex_buffer(FXD::GFX::BIND_VERTEX_BUFFER, 8, sizeof(VertTexUV), &vertColour).get();
	m_ibo = m_gfxAdapter->create_index_buffer(FXD::GFX::BIND_INDEX_BUFFER, 36, FXD::GFX::E_IndexType::Bit_16, &indices).get();
}

void GFXData::load2(void)
{
}

void GFXData::unload(void)
{
	FXD_RELEASE(m_vbo1, release().get());
	FXD_RELEASE(m_vbo2, release().get());
	FXD_RELEASE(m_ibo, release().get());

	if (m_gfxAdapter)
	{
		//FXD_RELEASE(m_program, release().get());
		FXD_RELEASE(m_gfxViewport, release().get());

		FXD::FXDGFX()->window_manager()->destroy_window(m_window);
		m_window = nullptr;

		FXD::FXDGFX()->gfx_system()->release_adapter(m_gfxAdapter).get();
		m_gfxAdapter = nullptr;
	}
}

void GFXData::unload2(void)
{
}