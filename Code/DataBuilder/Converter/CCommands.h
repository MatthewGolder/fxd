// Creator - MatthewGolder
#pragma once
#ifndef DATABUILDER_CONVERTER_CCOMMANDS_H
#define DATABUILDER_CONVERTER_CCOMMANDS_H

#include "DataBuilder/Converter/Types.h"
#include "FXDEngine/Container/Map.h"
#include "FXDEngine/Core/OS.h"
#include "FXDEngine/IO/Path.h"
#include "FXDEngine/IO/XML/XMLSaxParser.h"

using namespace FXD;

namespace DataBuilder
{
	namespace Converter
	{
		// ------
		// CCommand
		// -
		// ------
		class CCommand
		{
		public:
			CCommand(void)
				: m_eTargetPlatform(Core::E_OS::Unknown)
			{}
			~CCommand(void)
			{}

		public:
			Core::String8 m_strFileType;
			Core::E_OS m_eTargetPlatform;
			Converter::E_ConverterType m_eConverterType;
			Container::Map< Core::String8, Core::String8 > m_convertParams;
		};

		// ------
		// CCommandGroup
		// -
		// ------
		class CCommandGroup
		{
		public:
			friend class CFileParser;

		public:
			CCommandGroup(const IO::Path& pathRaw, const IO::Path& pathConvert, const IO::Path& pathCompress, const IO::Path& pathTmp);
			~CCommandGroup(void);

			const IO::Path& get_path_raw(void) const;
			const IO::Path& get_path_convert(void) const;
			const IO::Path& get_path_compress(void) const;
			const IO::Path& get_path_temp(void) const;

		public:
			IO::Path m_pathRaw;
			IO::Path m_pathConvert;
			IO::Path m_pathCompress;
			IO::Path m_pathTmp;
			Container::Vector< Converter::CCommand > m_commands;
		};

	} //namespace Converter
} //namespace DataBuilder
#endif //DATABUILDER_CONVERTER_CCOMMANDS_H