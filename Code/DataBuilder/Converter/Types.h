// Creator - MatthewGolder
#pragma once
#ifndef DATABUILDER_CONVERTER_TYPES_H
#define DATABUILDER_CONVERTER_TYPES_H

#include "FXDEngine/Core/StringView.h"

using namespace FXD;

namespace DataBuilder
{
	namespace Converter
	{
		enum class E_ConverterType : FXD::U16
		{
			Audio = 0,
			Copy,
			Material,
			Shader,
			Texture,
			Compress,
			Count,
			Unknown = 0xffff
		};

		extern const Core::StringView8 ToString(const Converter::E_ConverterType eType);
		extern const Converter::E_ConverterType ToType(const Core::StringView8 strType);

	} //namespace Converter
} //namespace DataBuilder
#endif //DATABUILDER_CONVERTER_TYPES_H