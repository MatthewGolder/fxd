// Creator - MatthewGolder
#pragma once
#ifndef DATABUILDER_CONVERTER_PLATFORMWIN32_H
#define DATABUILDER_CONVERTER_PLATFORMWIN32_H

#include "DataBuilder/Converter/Audio/Win32/AudioConverterWin32.h"
#include "DataBuilder/Converter/Copy/CopyConverter.h"
#include "DataBuilder/Converter/Material/MaterialConverter.h"
#include "DataBuilder/Converter/Shader/Win32/ShaderConverterWin32.h"
#include "DataBuilder/Converter/Texture/TextureConverter.h"
#include "DataBuilder/Converter/Compress/CompressConverter.h"

using namespace FXD;
using namespace DataBuilder;
using namespace Converter;

static bool handle_platform_windows(Converter::CRecord& record, const Converter::CCommand& command, const IO::Path& pathFile, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp)
{
	bool bRetVal = false;
	switch (command.m_eConverterType)
	{
		case Converter::E_ConverterType::Audio:
		{
			Converter::AudioConverterWin32 conv(command.m_convertParams, pathFile, pathSrc, pathDst, pathTmp);
			bRetVal = conv.start_conversion(record);
			break;
		}
		case Converter::E_ConverterType::Copy:
		{
			Converter::CopyConverter conv(command.m_convertParams, pathFile, pathSrc, pathDst, pathTmp);
			bRetVal = conv.start_conversion(record);
			break;
		}
		case Converter::E_ConverterType::Material:
		{
			Converter::MaterialConverter conv(command.m_convertParams, pathFile, pathSrc, pathDst, pathTmp);
			bRetVal = conv.start_conversion(record);
			break;
		}
		case Converter::E_ConverterType::Shader:
		{
			Converter::ShaderConverterWin32 conv(command.m_convertParams, pathFile, pathSrc, pathDst, pathTmp);
			bRetVal = conv.start_conversion(record);
			break;
		}
		case Converter::E_ConverterType::Texture:
		{
			Converter::TextureConverter conv(command.m_convertParams, pathFile, pathSrc, pathDst, pathTmp);
			bRetVal = conv.start_conversion(record);
			break;
		}
		case Converter::E_ConverterType::Compress:
		{
			Converter::CompressConverter conv(command.m_convertParams, pathFile, pathSrc, pathDst, pathTmp);
			bRetVal = conv.start_conversion(record);
			break;
		}
	}
	return bRetVal;
}

#endif //DATABUILDER_CONVERTER_PLATFORMWIN32_H