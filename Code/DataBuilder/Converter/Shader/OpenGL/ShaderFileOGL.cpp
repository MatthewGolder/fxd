// Creator - MatthewGolder
#include "DataBuilder/Converter/Shader/OpenGL/ShaderFileOGL.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/App/Win32/FuncsWin32.h"
#include "FXDEngine/GFX/Mapping.h"
#include "FXDEngine/GFX/FX/MappingFX.h"
#include "FXDEngine/IO/IODevice.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOStreamFile.h"
#include "FXDEngine/IO/IOSystem.h"
#include "FXDEngine/IO/Path.h"

using namespace DataBuilder;
using namespace Converter;
using namespace FXD;

namespace
{
	Core::StringBuilder8 genTextures(const Container::Vector< GFX::FX::TextureDef >& textureDefs)
	{
		Core::StringBuilder8 strBuff;
		fxd_for(const auto& texDef, textureDefs)
		{
			if (texDef.Type == GFX::E_ImageType::Two)
			{
				strBuff << UTF_8("uniform sampler2D ") << texDef.Name << UTF_8(";\n");
			}
		}
		if (textureDefs.not_empty())
		{
			strBuff << UTF_8("\n");
		}
		return strBuff;
	}

	Core::StringBuilder8 genConstantBuffersStructs(const Container::Vector< GFX::FX::ConstantBufferDef >& constantBuffers)
	{
		Core::StringBuilder8 strBuff;
		fxd_for(const auto& constantBuffer, constantBuffers)
		{
			strBuff << "struct " << constantBuffer.Name << constantBuffer.RegisterSlot << "\n";
			strBuff << "{\n";
			fxd_for(const auto& param, constantBuffer.Params)
			{
				strBuff << "\t" << GFX::FX::Funcs::ToString(param.DataType) << " " << param.Name << ";\n";
			}
			strBuff << "};\n";
		}
		return strBuff;
	}
	Core::StringBuilder8 genVaryingListStruct(const GFX::FX::VaryingListDef& varyingList)
	{
		Core::StringBuilder8 strBuff;
		strBuff << "struct " << varyingList.Name << "\n";
		strBuff << "{\n";
		fxd_for(const auto& param, varyingList.Params)
		{
			if (param.DataType == GFX::FX::E_FXDataType::Unknown)
			{
				break;
			}
			strBuff << "\t" << GFX::FX::Funcs::ToString(param.DataType) << " " << param.Name << ";\n";
		}
		strBuff << "};\n";
		return strBuff;
	}
	Core::StringBuilder8 genConstantBuffersLayouts(const Container::Vector< GFX::FX::ConstantBufferDef >& constantBuffers)
	{
		Core::StringBuilder8 strBuff;
		fxd_for(const auto& constantBuffer, constantBuffers)
		{
			strBuff << "layout(std140) uniform " << "_" << constantBuffer.Name << constantBuffer.RegisterSlot;
			strBuff << "{ " << constantBuffer.Name << constantBuffer.RegisterSlot << " " << constantBuffer.Name << ";" << "};\n";
		}
		return strBuff;
	}
	Core::StringBuilder8 genVaryingLayouts(const GFX::FX::VaryingListDef& varyingList, const Core::StringView8& type)
	{
		FXD::U32 nID = 0;
		Core::StringBuilder8 strBuff;
		fxd_for(const auto& param, varyingList.Params)
		{
			if (param.DataType == GFX::FX::E_FXDataType::Unknown)
			{
				break;
			}
			strBuff << "layout(location = " << nID << ") " << type << " " << GFX::FX::Funcs::ToString(param.DataType) << " " << param.Name << ";\n";
			nID++;
		}
		return strBuff;
	}
	Core::StringBuilder8 genVarying(const GFX::FX::VaryingListDef& varyingList, const Core::StringView8& op, const Core::StringView8& type, const Core::StringView8& name)
	{
		Core::StringBuilder8 strBuff;
		strBuff << op << " " << type << " " << name << ";\n";
		return strBuff;
	}

	Core::StringBuilder8 genVertexShader(const GFX::FX::VaryingListDef& vListIn, const GFX::FX::VaryingListDef& vListOut, const Core::String8& strShader)
	{
		Core::StringBuilder8 strBuff;
		strBuff << UTF_8("\n");
		strBuff << UTF_8("void main()\n");
		strBuff << UTF_8("{\n");
		strBuff << UTF_8("\t") << vListIn.Name << " Input;\n";
		fxd_for(const auto& param, vListIn.Params)
		{
			if (param.DataType == GFX::FX::E_FXDataType::Unknown)
			{
				break;
			}
			strBuff << UTF_8("\tInput.") << param.Name << " = " << param.Name << ";\n";
		}
		strBuff << UTF_8("\t") << strShader;
		strBuff << UTF_8("\t") << UTF_8("gl_Position = Output.Position;\n");
		strBuff << UTF_8("}\n");
		return strBuff;
	}

	Core::StringBuilder8 genPixelShader(const GFX::FX::VaryingListDef& vListIn, const GFX::FX::VaryingListDef& vListOut, const Core::String8& strShader)
	{
		Core::StringBuilder8 strBuff;
		strBuff << UTF_8("\n");
		strBuff << UTF_8("void main()\n");
		strBuff << UTF_8("{\n");
		strBuff << UTF_8("\t") << vListOut.Name << UTF_8(" Output;\n");
		strBuff << UTF_8("\t") << strShader;
		strBuff << UTF_8("\t") << UTF_8("_Color = Output.Color;\n");
		strBuff << UTF_8("}\n");
		return strBuff;
	}

	void genShaderOGL(const GFX::FX::ShaderFileDef& sfd, Core::StringBuilder8& srcVS, Core::StringBuilder8& srcPS)
	{
		Core::StringBuilder8 strTD = genTextures(sfd.m_textures);
	
		Core::StringBuilder8 strCB_Struct = genConstantBuffersStructs(sfd.m_constantBuffers);
		Core::StringBuilder8 strCB_Layout = genConstantBuffersLayouts(sfd.m_constantBuffers);

		Core::StringBuilder8 strVI_Struct = genVaryingListStruct(sfd.m_vShaderIn);
		Core::StringBuilder8 strVO_Struct = genVaryingListStruct(sfd.m_vShaderOut);
		Core::StringBuilder8 strVI_Layout = genVaryingLayouts(sfd.m_vShaderIn, "in");
		Core::StringBuilder8 strVO_Layout = genVarying(sfd.m_vShaderOut, "out", sfd.m_vShaderOut.Name.c_str(), "Output");
		Core::StringBuilder8 strVS = genVertexShader(sfd.m_vShaderIn, sfd.m_vShaderOut, sfd.m_vertexShader);

		Core::StringBuilder8 strPI_Struct = genVaryingListStruct(sfd.m_vShaderOut);
		Core::StringBuilder8 strPO_Struct = genVaryingListStruct(sfd.m_pShaderOut);
		Core::StringBuilder8 strPI_Layout = genVarying(sfd.m_vShaderOut, "in", sfd.m_vShaderOut.Name.c_str(), "Input");
		Core::StringBuilder8 strPO_Layout = genVarying(sfd.m_vShaderOut, "out", GFX::FX::Funcs::ToString(GFX::FX::E_FXDataType::Float4), "_Color");
		Core::StringBuilder8 strPS = genPixelShader(sfd.m_vShaderOut, sfd.m_pShaderOut, sfd.m_pixelShader);

		srcVS << strTD.str() << strCB_Struct.str() << strVI_Struct.str() << strVO_Struct.str() << strCB_Layout.str() << strVI_Layout.str() << strVO_Layout.str() << strVS.str();
		srcPS << strTD.str() << strCB_Struct.str() << strPI_Struct.str() << strPO_Struct.str() << strCB_Layout.str() << strPI_Layout.str() << strPO_Layout.str() << strPS.str();
	}

	void createTemp(const IO::Path& ioPath, const Core::String8& src)
	{
		Core::StreamOut fileStream = FXDIO()->io_system()->io_device()->file_open_stream(ioPath, { IO::E_FileMode::Write, IO::E_SerilizeMode::String, true }).get();
		(*fileStream) << src;
	}

	Memory::MemHandle createBinary(const IO::Path& ioIn, const IO::Path& ioOut, const Core::String8& src)
	{
		IO::Path ioOutVK = ioIn.string() + GFX::Api::ToString(GFX::E_GfxApi::OpenGL).c_str();

		// 1. Run process
		Core::StringBuilder8 strStream;
		strStream
			<< "glslangValidator.exe"
			<< " -e main "
			<< " -G " << ioIn.c_str()
			<< " -o " << ioOut.c_str();

		IO::Path pathModule = IO::FS::GetModuleDirectory();
		if (!App::FuncsWin32::runProcess(pathModule, UTF_8("glslangValidator.exe"), strStream.str()))
		{
			return Memory::MemHandle();
		}

		// 2. Check if the file actually was converted
		if (ioOut.file_exists())
		{
			// 2.1 Read memory and delete temp file
			IO::FS::FileRemame(ioOut, ioOutVK);
			IO::FS::FileDelete(ioIn);

			Memory::MemHandle retMem = FXDIO()->io_system()->io_device()->file_open_memhandle(ioOutVK, false, { }).get();
			ioOutVK.file_delete();
			return retMem;
		}
		return Memory::MemHandle();
	}
	void appendShaderMacros(Core::StringBuilder8& strBuf, const Core::String8& strVersion)
	{
		strBuf << UTF_8("#version ") << strVersion << UTF_8(" core\n");
		strBuf << UTF_8("#define OGL\n");

		strBuf << "#define float2 vec2\n";
		strBuf << "#define float3 vec3\n";
		strBuf << "#define float4 vec4\n";
		strBuf << "#define float3x4 mat3x4\n";
		strBuf << "#define float4x4 mat4x4\n";

		strBuf << "vec3 mul(in vec3 lhs, in vec3 rhs) { return lhs * rhs; }\n";
		strBuf << "vec4 mul(in vec4 lhs, in vec4 rhs) { return lhs * rhs; }\n";
		strBuf << "vec4 mul(in vec4 lhs, in mat4x4 rhs) { return lhs * rhs; }\n";

		strBuf << "float4 SampleTexture2D(in sampler2D s, in float2 uv)\n";
		strBuf << "{\n";
		strBuf << "  return texture(s, uv);\n";
		strBuf << "}\n";

		strBuf << UTF_8("\n");
	}
}

// ------
// ShaderFileOGL
// -
// ------
void ShaderFileOGL::convert(GFX::FX::ShaderFileDef& sfd, const Core::String8& strVersion)
{
	// 1. Set up macros
	Core::StringBuilder8 strVS;
	appendShaderMacros(strVS, strVersion);

	Core::StringBuilder8 strPS;
	appendShaderMacros(strPS, strVersion);

	// 2. Convert the file
	genShaderOGL(sfd, strVS, strPS);

	// 3. Create Temp files
	IO::Path pathTemp = IO::FS::GetTempDirectory();
	createTemp(pathTemp / (sfd.m_strFileName + UTF_8(".vert")).c_str(), strVS.str());
	createTemp(pathTemp / (sfd.m_strFileName + UTF_8(".frag")).c_str(), strPS.str());

	// 4. Convert File
	sfd.m_eType = GFX::E_GfxApi::OpenGL;
	sfd.m_vertexShader = Core::encode_string(createBinary(pathTemp / (sfd.m_strFileName + UTF_8(".vert")).c_str(), pathTemp / (sfd.m_strFileName + UTF_8(".spv")).c_str(), strVS.str()));
	sfd.m_pixelShader = Core::encode_string(createBinary(pathTemp / (sfd.m_strFileName + UTF_8(".frag")).c_str(), pathTemp / (sfd.m_strFileName + UTF_8(".spv")).c_str(), strPS.str()));
}