// Creator - MatthewGolder
#pragma once
#ifndef DATABUILDER_CONVERTER_SHADER_OPENGL_SHADERFILEOGL_H
#define DATABUILDER_CONVERTER_SHADER_OPENGL_SHADERFILEOGL_H

#include "FXDEngine/GFX/FX/ShaderFileDef.h"

namespace DataBuilder
{
	namespace Converter
	{
		// ------
		// ShaderFileOGL
		// -
		// ------
		struct ShaderFileOGL
		{
			static void convert(FXD::GFX::FX::ShaderFileDef& sfd, const FXD::Core::String8& strVersion);
		};

	} //namespace Converter
} //namespace DataBuilder
#endif //DATABUILDER_CONVERTER_SHADER_OPENGL_SHADERFILEOGL_H