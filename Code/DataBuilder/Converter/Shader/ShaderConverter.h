// Creator - MatthewGolder
#pragma once
#ifndef DATABUILDER_CONVERTER_SHADER_SHADERCONVERTER_H
#define DATABUILDER_CONVERTER_SHADER_SHADERCONVERTER_H

#include "DataBuilder/Converter/Converter.h"
#include "FXDEngine/GFX/FX/Types.h"

namespace DataBuilder
{
	namespace Converter
	{
		// ------
		// IShaderConverter
		// -
		// ------
		class IShaderConverter : public Converter::IConverter
		{
		public:
			IShaderConverter(const Container::Map< Core::String8, Core::String8 >& params, const IO::Path& pathFile, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp);
			virtual ~IShaderConverter(void);

			virtual const Core::E_OS get_platform(void) const PURE;
			virtual const Converter::E_ConverterType get_type(void) const FINAL;

			const Core::String8 get_converter_name(void) const FINAL;
			const Core::String8 get_convert_file_name(void) const FINAL;
			const Core::String8 get_convert_file_extension(void) const FINAL;

		protected:
			virtual FXD::Memory::MemHandle convert(const Core::StreamIn& stream) PURE;

			GFX::E_GfxApi m_eSourceType;
			GFX::E_GfxApi m_eTarget_type;
			Core::String8 m_strVersion;
		};

	} //namespace Converter
} //namespace DataBuilder
#endif //DATABUILDER_CONVERTER_SHADER_SHADERCONVERTER_H