// Creator - MatthewGolder
#include "DataBuilder/Converter/Shader/ShaderConverter.h"
#include "FXDEngine/GFX/GfxApi.h"
#include "FXDEngine/GFX/Mapping.h"
#include "FXDEngine/IO/DirectoryEntry.h"
#include "FXDEngine/IO/DirectoryIterator.h"

using namespace FXD;
using namespace DataBuilder;
using namespace Converter;

// ------
// IShaderConverter
// -
// ------
IShaderConverter::IShaderConverter(const Container::Map< Core::String8, Core::String8 >& params, const IO::Path& pathFile, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp)
	: IConverter(params, pathFile, pathSrc, pathDst, pathTmp)
	, m_eSourceType(GFX::E_GfxApi::Unknown)
	, m_eTarget_type(GFX::E_GfxApi::Unknown)
	, m_strVersion()
{
}

IShaderConverter::~IShaderConverter(void)
{
}

const Core::E_OS IShaderConverter::get_platform(void) const
{
	return Core::E_OS::Windows;
}

const Converter::E_ConverterType IShaderConverter::get_type(void) const
{
	return Converter::E_ConverterType::Shader;
}

const Core::String8 IShaderConverter::get_converter_name(void) const
{
	return UTF_8("Shader");
}

const Core::String8 IShaderConverter::get_convert_file_name(void) const
{
	return "";//IO::Path(m_inFile).replace_extension(get_convert_file_extension()).filename();
}

const Core::String8 IShaderConverter::get_convert_file_extension(void) const
{
	//Core::String8 strFileExtension = m_inFile.extension();
	//strFileExtension.append(GFX::Api::ToString(m_eTarget_type).c_str());
	//return strFileExtension;
	return "";
}