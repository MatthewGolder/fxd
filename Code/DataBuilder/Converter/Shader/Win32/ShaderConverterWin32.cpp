// Creator - MatthewGolder
#include "DataBuilder/Converter/Shader/Win32/ShaderConverterWin32.h"
#include "DataBuilder/Converter/Shader/DX11/ShaderFileDX11.h"
#include "DataBuilder/Converter/Shader/OpenGL/ShaderFileOGL.h"
#include "DataBuilder/Converter/Shader/Vulkan/ShaderFileVulkan.h"
#include "DataBuilder/System/DataBuilder.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/IO/IODevice.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOStreamFile.h"
#include "FXDEngine/IO/IOSystem.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/Mapping.h"

using namespace FXD;
using namespace DataBuilder;
using namespace Converter;

// ------
// ShaderConverterWin32
// -
// ------
ShaderConverterWin32::ShaderConverterWin32(const Container::Map< Core::String8, Core::String8 >& params, const IO::Path& pathFile, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp)
	: IShaderConverter(params, pathFile, pathSrc, pathDst, pathTmp)
{
	Core::String8 strOutType;
	if (m_convertParams.try_get_value(UTF_8("OutType"), strOutType))
	{
		m_eTarget_type = FXD::GFX::Api::ToApiType(strOutType.c_str());
	}
	if (m_convertParams.try_get_value(UTF_8("version"), strOutType))
	{
		m_strVersion = strOutType;
	}
}

ShaderConverterWin32::~ShaderConverterWin32(void)
{
}

const Core::E_OS ShaderConverterWin32::get_platform(void) const
{
	return Core::E_OS::Windows;
}

Memory::MemHandle ShaderConverterWin32::convert(const Core::StreamIn& stream)
{
	// 1. Check Target Type
	if (m_eTarget_type == GFX::E_GfxApi::Unknown)
	{
		return Memory::MemHandle();
	}

	GFX::FX::ShaderFileTagCB callback;
	IO::StreamParser streamParser(stream);
	IO::XMLSaxParser::parse(streamParser, callback);

	// 2. Convert and get memory
	switch (m_eTarget_type)
	{
		case GFX::E_GfxApi::DX11:
		{
			return convert_DX11(callback.m_FXFileDef);
		}
		case GFX::E_GfxApi::OpenGL:
		{
			return convert_opengl(callback.m_FXFileDef);
		}
		case GFX::E_GfxApi::Vulkan:
		{
			return convert_vulkan(callback.m_FXFileDef);
		}
		default:
		{
			return Memory::MemHandle();
		}
	}
}

FXD::Memory::MemHandle ShaderConverterWin32::convert_DX11(GFX::FX::ShaderFileDef& sfd)
{
	switch (m_eSourceType)
	{
		case GFX::E_GfxApi::Unknown:
		{
			// 1. Get setup parameters
			IO::Path pathTemp = IO::FS::GetTempDirectory() / get_convert_file_name().c_str();

			// 2. Run process
			{
				ShaderFileDX11::convert(sfd);
				Core::StreamOut fileStream = FXD::FXDIO()->io_system()->io_device()->file_open_stream(pathTemp, { IO::E_FileMode::Write, IO::E_SerilizeMode::String, true }).get();
				sfd.writeXml(fileStream);
			}

			// 3. Check if the file actually was converted
			if (!pathTemp.file_exists())
			{
				return Memory::MemHandle();
			}

			// 4. Read memory and delete temp file
			Memory::MemHandle retMemHandle = FXD::FXDIO()->io_system()->io_device()->file_open_memhandle(pathTemp, false).get();
			pathTemp.file_delete();
			return retMemHandle;
		}
		default:
		{
			return Memory::MemHandle();
		}
	}
}

FXD::Memory::MemHandle ShaderConverterWin32::convert_opengl(GFX::FX::ShaderFileDef& sfd)
{
	switch (m_eSourceType)
	{
		case GFX::E_GfxApi::Unknown:
		{
			// 1. Get setup parameters
			IO::Path pathTemp = IO::FS::GetTempDirectory() / get_convert_file_name().c_str();

			// 2. Run process
			{
				ShaderFileOGL::convert(sfd, m_strVersion);
				if (sfd.m_vertexShader.not_empty() && sfd.m_pixelShader.not_empty())
				{
					Core::StreamOut fileStream = FXDIO()->io_system()->io_device()->file_open_stream(pathTemp, { IO::E_FileMode::Write, IO::E_SerilizeMode::String, true }, { }).get();
					sfd.writeXml(fileStream);
				}
			}

			// 3. Check if the file actually was converted
			Memory::MemHandle retMemHandle;
			if (pathTemp.file_exists())
			{
				// 3.1. Read memory and delete temp file
				retMemHandle = FXDIO()->io_system()->io_device()->file_open_memhandle(pathTemp, false, { }).get();
				pathTemp.file_delete();
			}
			return retMemHandle;
		}
		default:
		{
			return Memory::MemHandle();
		}
	}
}

Memory::MemHandle ShaderConverterWin32::convert_vulkan(GFX::FX::ShaderFileDef& sfd)
{
	switch (m_eSourceType)
	{
		case GFX::E_GfxApi::Unknown:
		{
			// 1. Get setup parameters
			IO::Path pathTemp = IO::FS::GetTempDirectory() / get_convert_file_name().c_str();

			// 2. Run process
			{
				ShaderFileVulkan::convert(sfd, m_strVersion);
				Core::StreamOut fileStream = FXDIO()->io_system()->io_device()->file_open_stream(pathTemp, { IO::E_FileMode::Write, IO::E_SerilizeMode::String, true }).get();
				sfd.writeXml(fileStream);
			}

			// 3. Check if the file actually was converted
			Memory::MemHandle retMemHandle;
			if (pathTemp.file_exists())
			{
				// 3.1 Read memory and delete temp file
				retMemHandle = FXDIO()->io_system()->io_device()->file_open_memhandle(pathTemp, false).get();
				pathTemp.file_delete();
			}
			return retMemHandle;
		}
		default:
		{
			return Memory::MemHandle();
		}
	}
}
