// Creator - MatthewGolder
#pragma once
#ifndef DATABUILDER_CONVERTER_SHADER_WIN32_SHADERCONVERTERWIN32_H
#define DATABUILDER_CONVERTER_SHADER_WIN32_SHADERCONVERTERWIN32_H

#include "DataBuilder/Converter/Shader/ShaderConverter.h"
#include "FXDEngine/GFX/FX/ShaderFileDef.h"

namespace DataBuilder
{
	namespace Converter
	{
		// ------
		// ShaderConverterWin32
		// -
		// ------
		class ShaderConverterWin32 : public Converter::IShaderConverter
		{
		public:
			ShaderConverterWin32(const Container::Map< Core::String8, Core::String8 >& params, const IO::Path& pathFile, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp);
			~ShaderConverterWin32(void);

			const Core::E_OS get_platform(void) const FINAL;

		private:
			Memory::MemHandle convert(const Core::StreamIn& stream);
			Memory::MemHandle convert_DX11(GFX::FX::ShaderFileDef& sfd);
			Memory::MemHandle convert_opengl(GFX::FX::ShaderFileDef& sfd);
			Memory::MemHandle convert_vulkan(GFX::FX::ShaderFileDef& sfd);
		};

	} //namespace Converter
} //namespace DataBuilder
#endif //DATABUILDER_CONVERTER_SHADER_WIN32_SHADERCONVERTERWIN32_H