// Creator - MatthewGolder
#include "DataBuilder/Converter/Shader/DX11/ShaderFileDX11.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/App/Win32/FuncsWin32.h"
#include "FXDEngine/GFX/Mapping.h"
#include "FXDEngine/GFX/FX/MappingFX.h"
#include "FXDEngine/IO/IODevice.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOStreamFile.h"
#include "FXDEngine/IO/IOSystem.h"
#include "FXDEngine/IO/Path.h"

#include <d3d11.h>
#include <d3dcompiler.h>

using namespace DataBuilder;
using namespace Converter;
using namespace FXD;

namespace
{
	const FXD::Core::StringView8 kVSMain = "VSMain";
	const FXD::Core::StringView8 kPSMain = "PSMain";
	const FXD::Core::StringView8 kVSVersion = "vs_4_0";
	const FXD::Core::StringView8 kPSVersion = "ps_4_0";

	const FXD::Core::StringView8 kDX11SemanticTypes[] =
	{
		"Position",		// FX::E_SemanticType::Position
		"SV_Position",	// FX::E_SemanticType::SV_Position
		"Color0",		// FX::E_SemanticType::Color0
		"Color1",		// FX::E_SemanticType::Color1
		"Normal0",		// FX::E_SemanticType::Normal0,
		"Normal1",		// FX::E_SemanticType::Normal1
		"Binormal0",	// FX::E_SemanticType::Binormal0
		"Binormal1",	// FX::E_SemanticType::Binormal01
		"Tangent0",		// FX::E_SemanticType::Tangent0
		"Tangent1",		// FX::E_SemanticType::Tangent1
		"TexCoord0",	// FX::E_SemanticType::TexCoord0
		"TexCoord1",	// FX::E_SemanticType::TexCoord1
		"TexCoord2",	// FX::E_SemanticType::TexCoord2
		"TexCoord3",	// FX::E_SemanticType::TexCoord3
		"TexCoord4",	// FX::E_SemanticType::TexCoord4
		"TexCoord5",	// FX::E_SemanticType::TexCoord5
		"TexCoord6",	// FX::E_SemanticType::TexCoord6
		"TexCoord7",	// FX::E_SemanticType::TexCoord7
		"TexCoord8"		// FX::E_SemanticType::TexCoord8
	};
	Core::StringView8 ToString(const GFX::FX::E_SemanticType eType)
	{
		return kDX11SemanticTypes[FXD::STD::to_underlying(eType)];
	}

	Core::StringBuilder8 genTextures(const Container::Vector< GFX::FX::TextureDef >& textureDefs)
	{
		Core::StringBuilder8 strBuff;
		fxd_for(const auto& texDef, textureDefs)
		{
			if (texDef.Type == FXD::GFX::E_ImageType::Two)
			{
				strBuff << UTF_8("Texture2D texture_") << texDef.Name << " : register(t" << texDef.RegisterSlot << ");\n";
			}
		}
		if (textureDefs.not_empty())
		{
			strBuff << UTF_8("\n");
		}
		return strBuff;
	}

	Core::StringBuilder8 genSamplers(const Container::Vector< GFX::FX::TextureDef >& samplerDefs)
	{
		Core::StringBuilder8 strBuff;
		fxd_for(const auto& samplerDef, samplerDefs)
		{
			strBuff << "SamplerState sampler_" << samplerDef.Name << " : register(s" << samplerDef.RegisterSlot << ");\n";
		}
		if (samplerDefs.not_empty())
		{
			strBuff << UTF_8("\n");
		}
		return strBuff;
	}

	Core::StringBuilder8 genConstantBuffers(const Container::Vector< GFX::FX::ConstantBufferDef >& constantBuffers)
	{
		Core::StringBuilder8 strBuff;
		fxd_for(const auto& buffer, constantBuffers)
		{
			// Write the constant buffer struct
			strBuff << "struct b" << buffer.RegisterSlot << "\n";
			strBuff << "{\n";
			fxd_for(const auto& param, buffer.Params)
			{
				strBuff << UTF_8("\t") << GFX::FX::Funcs::ToString(param.DataType) << " " << param.Name << ";\n";
			}
			strBuff << UTF_8("};\n");

			// Write the constant buffer declaration
			strBuff << "cbuffer b" << buffer.RegisterSlot << " : register(b" << buffer.RegisterSlot << ")\n";
			strBuff << "{\n";
			strBuff << "\tb" << buffer.RegisterSlot << " " << buffer.Name << ";\n";
			strBuff << "};\n";
		}
		if (constantBuffers.not_empty())
		{
			strBuff << UTF_8("\n");
		}
		return strBuff;
	}

	Core::StringBuilder8 genVaryingList(const GFX::FX::VaryingListDef& varyingList)
	{
		Core::StringBuilder8 strBuff;
		strBuff << UTF_8("struct ") << varyingList.Name << UTF_8("\n");
		strBuff << UTF_8("{\n");
		fxd_for(const auto& param, varyingList.Params)
		{
			if (param.DataType == FXD::GFX::FX::E_FXDataType::Unknown)
			{
				break;
			}
			strBuff << UTF_8("\t") << GFX::FX::Funcs::ToString(param.DataType) << UTF_8(" ") << param.Name << UTF_8(" : ") << ToString(param.SemanticType) << UTF_8(";\n");
		}
		strBuff << UTF_8("};\n");
		strBuff << UTF_8("\n");
		return strBuff;
	}

	Core::StringBuilder8 genVertexShader(const GFX::FX::VaryingListDef& vListIn, const GFX::FX::VaryingListDef& vListOut, const Core::String8& strShader)
	{
		Core::StringBuilder8 strBuff;
		strBuff << vListOut.Name << UTF_8(" ") << kVSMain << UTF_8("(") << vListIn.Name << UTF_8(" Input") << UTF_8(")\n");

		strBuff << UTF_8("{\n");
		strBuff << UTF_8("\t") << vListOut.Name << UTF_8(" Output;\n");
		strBuff << UTF_8("\t") << strShader;
		strBuff << UTF_8("\t") << UTF_8("return Output;\n");
		strBuff << UTF_8("}\n");
		return strBuff;
	}

	Core::StringBuilder8 genPixelShader(const GFX::FX::VaryingListDef& vListIn, const GFX::FX::VaryingListDef& vListOut, const Core::String8& strShader)
	{
		Core::StringBuilder8 strBuff;
		strBuff << UTF_8("float4 ") << kPSMain << UTF_8("(") << vListIn.Name << UTF_8(" Input) : SV_TARGET\n");
		strBuff << UTF_8("{\n");
		strBuff << UTF_8("\t") << vListOut.Name << UTF_8(" Output;\n");
		strBuff << UTF_8("\t") << strShader;
		strBuff << UTF_8("\t") << UTF_8("return Output.Color;\n");
		strBuff << UTF_8("}\n");
		return strBuff;
	}

	void genShaderDX11(const GFX::FX::ShaderFileDef& sfd, Core::StringBuilder8& srcVS, Core::StringBuilder8& srcPS)
	{
		Core::StringBuilder8 strTD = genTextures(sfd.m_textures);
		Core::StringBuilder8 strSD = genSamplers(sfd.m_textures);
		Core::StringBuilder8 strCB = genConstantBuffers(sfd.m_constantBuffers);

		Core::StringBuilder8 strVI = genVaryingList(sfd.m_vShaderIn);
		Core::StringBuilder8 strVO = genVaryingList(sfd.m_vShaderOut);
		Core::StringBuilder8 strVS = genVertexShader(sfd.m_vShaderIn, sfd.m_vShaderOut, sfd.m_vertexShader);

		Core::StringBuilder8 strPI = genVaryingList(sfd.m_vShaderOut);
		Core::StringBuilder8 strPO = genVaryingList(sfd.m_pShaderOut);
		Core::StringBuilder8 strPS = genPixelShader(sfd.m_vShaderOut, sfd.m_pShaderOut, sfd.m_pixelShader);

		srcVS << strCB.str() << strVI.str() << strVO.str() << strVS.str();
		srcPS << strTD.str() << strSD.str() << strCB.str() << strPI.str() << strPO.str() << strPS.str();
	}

	void createTemp(const IO::Path& ioPath, const Core::String8& src)
	{
		Core::StreamOut fileStream = FXD::FXDIO()->io_system()->io_device()->file_open_stream(ioPath, { IO::E_FileMode::Write, IO::E_SerilizeMode::String, true }).get();
		(*fileStream) << src;
	}

	Memory::MemHandle createBinary(const Core::String8& src, const FXD::Core::StringView8 profile, const FXD::Core::StringView8 entry)
	{
		FXD::U32 nFlags = (D3DCOMPILE_OPTIMIZATION_LEVEL3 | D3DCOMPILE_WARNINGS_ARE_ERRORS);
		ID3DBlob* pShader = nullptr;
		ID3DBlob* pErrors = nullptr;
		HRESULT hr = D3DCompile(src.c_str(), src.length(), entry.c_str(), nullptr, nullptr, entry.c_str(), profile.c_str(), nFlags, 0, &pShader, &pErrors);
		if (pErrors != nullptr)
		{
			PRINT_INFO << "Shader Compiler messages:\n" << (FXD::UTF8*)pErrors->GetBufferPointer() << "\n";
			FXD_RELEASE(pErrors, Release());
			return Memory::MemHandle();
		}

		Memory::MemHandle retMem;
		if (pShader != nullptr)
		{
			retMem = Memory::MemHandle::allocate(pShader->GetBufferSize());
			Memory::MemCopy(Memory::MemHandle::LockGuard(retMem).get_mem(), pShader->GetBufferPointer(), retMem.get_size());
			FXD_RELEASE(pShader, Release());
		}
		return retMem;
	}
	void appendShaderMacros(Core::StringBuilder8& strBuf)
	{
		strBuf << UTF_8("#define DX11\n");

		strBuf << "#define SampleTexture2D(t, uv) texture_##t.Sample(sampler_##t, uv)\n";

		strBuf << UTF_8("\n");
	}
}

// ------
// ShaderFileDX11
// -
// ------
void ShaderFileDX11::convert(GFX::FX::ShaderFileDef& sfd)
{
	// 1. Set up macros
	Core::StringBuilder8 strVS;
	appendShaderMacros(strVS);

	Core::StringBuilder8 strPS;
	appendShaderMacros(strPS);

	// 2. Convert the file
	genShaderDX11(sfd, strVS, strPS);

	// 3. Create Temp files
	IO::Path pathTemp = IO::FS::GetTempDirectory();
	createTemp(pathTemp / (sfd.m_strFileName + UTF_8(".vert") + GFX::Api::ToString(GFX::E_GfxApi::DX11).c_str()).c_str(), strVS.str());
	createTemp(pathTemp / (sfd.m_strFileName + UTF_8(".frag") + GFX::Api::ToString(GFX::E_GfxApi::DX11).c_str()).c_str(), strPS.str());

	// 4. Create the binaries
	sfd.m_eType = GFX::E_GfxApi::DX11;
	sfd.m_vertexShader = Core::encode_string(createBinary(strVS.str(), kVSVersion, kVSMain));
	sfd.m_pixelShader = Core::encode_string(createBinary(strPS.str(), kPSVersion, kPSMain));
}