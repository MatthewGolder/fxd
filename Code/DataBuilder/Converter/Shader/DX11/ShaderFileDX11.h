// Creator - MatthewGolder
#pragma once
#ifndef DATABUILDER_CONVERTER_SHADER_DX11_SHADERFILEDX11_H
#define DATABUILDER_CONVERTER_SHADER_DX11_SHADERFILEDX11_H

#include "FXDEngine/GFX/FX/ShaderFileDef.h"

namespace DataBuilder
{
	namespace Converter
	{
		// ------
		// ShaderFileDX11
		// -
		// ------
		struct ShaderFileDX11
		{
			static void convert(FXD::GFX::FX::ShaderFileDef& sfd);
		};

	} //namespace Converter
} //namespace DataBuilder
#endif //DATABUILDER_CONVERTER_SHADER_DX11_SHADERFILEDX11_H
