// Creator - MatthewGolder
#pragma once
#ifndef DATABUILDER_CONVERTER_CPROCESSOR_H
#define DATABUILDER_CONVERTER_CPROCESSOR_H

#include "DataBuilder/Converter/CCommands.h"
#include "DataBuilder/System/DataBuilder.h"
#include "FXDEngine/Core/OS.h"
#include "FXDEngine/IO/Path.h"

#include <3rdParty/minizip/mz_compat.h>
#include <3rdParty/zlib/zlib.h>

using namespace FXD;

namespace DataBuilder
{
	namespace Converter
	{
		class CRecord;

		// ------
		// CProcessor
		// -
		// ------
		class CProcessor : public Core::NonCopyable
		{
		public:
			CProcessor(Converter::CCommandGroup& commandGroup, Core::E_OS ePlatformType);
			~CProcessor(void);

			FXD::U32 convert(Converter::CRecord& record);
			FXD::U32 compress(Converter::CRecord& record);
			FXD::U32 pack(Converter::CRecord& record);

		private:
			void iterate_convert(Converter::CRecord& record, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp);
			void process_convert(Converter::CRecord& record, const Converter::CCommand& command, const IO::Path& pathFile, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp);

			void iterate_compress(Converter::CRecord& record, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp);
			void process_compress(Converter::CRecord& record, const IO::Path& pathFile, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp);

		private:
			Converter::CCommandGroup m_commandGroup;
			Core::E_OS m_ePlatformType;
		};

	} //namespace Converter
} //namespace DataBuilder
#endif //DATABUILDER_CONVERTER_CPROCESSOR_H