// Creator - MatthewGolder
#include "DataBuilder/Converter/Material/MaterialConverter.h"
#include "FXDEngine/IO/DirectoryEntry.h"
#include "FXDEngine/IO/DirectoryIterator.h"
#include "FXDEngine/IO/Path.h"

using namespace FXD;
using namespace DataBuilder;
using namespace Converter;

// ------
// MaterialConverter
// -
// ------
MaterialConverter::MaterialConverter(const Container::Map< Core::String8, Core::String8 >& params, const IO::Path& pathFile, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp)
	: IConverter(params, pathFile, pathSrc, pathDst, pathTmp)
{
}

MaterialConverter::~MaterialConverter(void)
{
}

const Core::E_OS MaterialConverter::get_platform(void) const
{
	return Core::E_OS::Windows;
}

const Converter::E_ConverterType MaterialConverter::get_type(void) const
{
	return Converter::E_ConverterType::Material;
}

const Core::String8 MaterialConverter::get_converter_name(void) const
{
	return UTF_8("Material");
}

const Core::String8 MaterialConverter::get_convert_file_name(void) const
{
	return m_pathFile.filename();
}

const Core::String8 MaterialConverter::get_convert_file_extension(void) const
{
	return m_pathFile.extension();
}

FXD::Memory::MemHandle MaterialConverter::convert(const Core::StreamIn& stream)
{
	return Memory::MemHandle::read_to_end((*stream), true);
}