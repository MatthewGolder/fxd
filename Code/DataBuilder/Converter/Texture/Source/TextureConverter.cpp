// Creator - MatthewGolder
#include "DataBuilder/Converter/Texture/TextureConverter.h"
#include "FXDEngine/IO/DirectoryEntry.h"
#include "FXDEngine/IO/DirectoryIterator.h"
#include "FXDEngine/IO/Path.h"

using namespace FXD;
using namespace DataBuilder;
using namespace Converter;

// ------
// TextureConverter
// -
// ------
TextureConverter::TextureConverter(const Container::Map< Core::String8, Core::String8 >& params, const IO::Path& pathFile, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp)
	: IConverter(params, pathFile, pathSrc, pathDst, pathTmp)
{
}

TextureConverter::~TextureConverter(void)
{
}

const Core::E_OS TextureConverter::get_platform(void) const
{
	return Core::E_OS::Windows;
}

const Converter::E_ConverterType TextureConverter::get_type(void) const
{
	return Converter::E_ConverterType::Copy;
}

const Core::String8 TextureConverter::get_converter_name(void) const
{
	return UTF_8("Texture");
}

const Core::String8 TextureConverter::get_convert_file_name(void) const
{
	return m_pathFile.filename();
}

const Core::String8 TextureConverter::get_convert_file_extension(void) const
{
	return m_pathFile.extension();
}

FXD::Memory::MemHandle TextureConverter::convert(const Core::StreamIn& stream)
{
	return Memory::MemHandle::read_to_end((*stream), true);
}