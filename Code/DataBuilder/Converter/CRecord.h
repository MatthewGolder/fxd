// Creator - MatthewGolder
#pragma once
#ifndef DATABUILDER_CONVERTER_CRECORD_H
#define DATABUILDER_CONVERTER_CRECORD_H

#include "FXDEngine/IO/Path.h"
#include "FXDEngine/Container/Vector.h"
#include "FXDEngine/Container/Map.h"
#include "DataBuilder/Converter/Types.h"

using namespace FXD;

namespace DataBuilder
{
	namespace Converter
	{
		// ------
		// CRecord
		// -
		// ------
		class CRecord
		{
		public:
			CRecord(void);
			~CRecord(void);

			void add(Converter::E_ConverterType eConverterType, const Core::String8& strSrcName, const IO::Path& pathSrc, const Core::String8& strDstName, const IO::Path& pathDst);

		public:
			Container::Vector< Container::Map< Core::String8, Core::String8 > > m_convertInfo;
		};
	} //namespace Converter
} //namespace DataBuilder
#endif //DATABUILDER_CONVERTER_CRECORD_H