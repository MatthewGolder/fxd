// Creator - MatthewGolder
#pragma once
#ifndef DATABUILDER_CONVERTER_CFILEPARSER_H
#define DATABUILDER_CONVERTER_CFILEPARSER_H

#include "DataBuilder/Converter/CCommands.h"

using namespace FXD;

namespace DataBuilder
{
	namespace Converter
	{
		// ------
		// CFileParser
		// -
		// ------
		class CFileParser : public FXD::IO::XMLSaxParser::ITagCB
		{
		public:
			// ------
			// E_Tag
			// -
			// ------
			enum class E_Tag
			{
				DataConverter = 0,
				ConverterInfo = 1,
				Command = 2,
				Count,
				Unknown = 0xffff
			};

		public:
			CFileParser(const IO::Path& pathRaw, const IO::Path& pathConvert, const IO::Path& pathCompress, const IO::Path& pathTmp);
			~CFileParser(void);

			void handle_tag(const Core::String8& strName, ITagCB*& pTagCB) FINAL;
			void end_tag(const Core::String8& strName) FINAL;
			void handle_attribute(const Core::String8& strName, const Core::String8& strValue) FINAL;

		public:
			IO::Path m_pathRaw;
			IO::Path m_pathConvert;
			IO::Path m_pathCompress;
			IO::Path m_pathTmp;
			Container::Vector< Converter::CCommandGroup > m_commandGroups;

		private:
			CFileParser::E_Tag m_eTag;
		};

	} //namespace Converter
} //namespace DataBuilder
#endif //DATABUILDER_CONVERTER_CFILEPARSER_H