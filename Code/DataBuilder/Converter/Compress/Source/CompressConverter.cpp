// Creator - MatthewGolder
#include "DataBuilder/Converter/Compress/CompressConverter.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/IO/DirectoryEntry.h"
#include "FXDEngine/IO/DirectoryIterator.h"
#include "FXDEngine/IO/Path.h"
#include "FXDEngine/IO/IOStreamFile.h"
#include <FXDEngine/IO/IOJPThread.h>
#include <FXDEngine/IO/IOSystem.h>
#include <FXDEngine/IO/IODevice.h>
#include "FXDEngine/Memory/StreamMemHandle.h"
#include "FXDEngine/Memory/StreamMemory.h"
#include "FXDEngine/App/FXDApi.h"

#include <3rdParty/lz4/lz4.h>
#include <3rdParty/lz4/lz4hc.h>
#include <3rdParty/lz4/lz4frame.h>
#include <stdio.h>

using namespace FXD;
using namespace DataBuilder;
using namespace Converter;

int GetMaxCompressedLen(int nLenSrc)
{
	int n16kBlocks = (nLenSrc + 16383) / 16384; // round up any fraction of a block
	return (nLenSrc + 6 + (n16kBlocks * 5));
}

#	define IN_CHUNK_SIZE (16*1024)
static const LZ4F_preferences_t kPrefs =
{
 { LZ4F_max256KB, LZ4F_blockLinked, LZ4F_noContentChecksum, LZ4F_frame, 0 /* unknown content size */, 0 /* no dictID */ , LZ4F_noBlockChecksum },
	LZ4HC_CLEVEL_MAX,			/* compression level; 0 == default */
	0,								/* autoflush */
	0,								/* favor decompression speed */
	{ 0, 0, 0 },				/* reserved, must be set to 0 */
};
typedef struct
{
	int error;
	unsigned long long size_in;
	unsigned long long size_out;
} compressResult_t;

static compressResult_t compress_file_internal(Core::StreamIn& streamI, Core::StreamOut& streamO, LZ4F_compressionContext_t ctx, void* inBuff, size_t inChunkSize, void* outBuff, size_t outCapacity)
{
	compressResult_t result = { 1, 0, 0 };  /* result for an error */
	unsigned long long count_in = 0, count_out;

	//assert(f_in != NULL);
	//assert(f_out != NULL);
	PRINT_COND_ASSERT(ctx != NULL, "");
	PRINT_COND_ASSERT(outCapacity >= LZ4F_HEADER_SIZE_MAX, "");
	PRINT_COND_ASSERT(outCapacity >= LZ4F_compressBound(inChunkSize, &kPrefs), "");

	//write frame header
	{
		size_t const headerSize = LZ4F_compressBegin(ctx, outBuff, outCapacity, &kPrefs);
		if (LZ4F_isError(headerSize))
		{
			//printf("Failed to start compression: error %u \n", (unsigned)headerSize);
			return result;
		}
		count_out = headerSize;
		//printf("Buffer size is %u bytes, header size %u bytes \n", (unsigned)outCapacity, (unsigned)headerSize);
		streamO->write_size(outBuff, headerSize);
	}

	// stream file
	for (;;)
	{
		size_t const readSize = streamI->read_size(inBuff, IN_CHUNK_SIZE);
		if (readSize == 0)
		{
			break; // nothing left to read from input file
		}
		count_in += readSize;

		size_t const compressedSize = LZ4F_compressUpdate(ctx, outBuff, outCapacity, inBuff, readSize, NULL);
		if (LZ4F_isError(compressedSize))
		{
			//printf("Compression failed: error %u \n", (unsigned)compressedSize);
			return result;
		}

		//printf("Writing %u bytes\n", (unsigned)compressedSize);
		streamO->write_size(outBuff, compressedSize);
		count_out += compressedSize;
	}

	// flush whatever remains within internal buffers
	{
		size_t const compressedSize = LZ4F_compressEnd(ctx, outBuff, outCapacity, NULL);
		if (LZ4F_isError(compressedSize))
		{
			//printf("Failed to end compression: error %u \n", (unsigned)compressedSize);
			return result;
		}

		//printf("Writing %u bytes \n", (unsigned)compressedSize);
		streamO->write_size(outBuff, compressedSize);
		count_out += compressedSize;
	}

	result.size_in = count_in;
	result.size_out = count_out;
	result.error = 0;
	return result;
}

void compress_normal(const IO::Path& iPath, const IO::Path& oPath, Core::CompressParams compressParams)
{
	Memory::MemHandle streamI = FXDIO()->io_system()->io_device()->file_open_memhandle(iPath.c_str(), false).get();
	Memory::StreamMemory streamO(compressParams);

	Memory::MemHandle::LockGuard lock(streamI);
	FXD::U32 nUnCompSize = streamI.get_size();
	FXD::U8* pUnCompData = (FXD::U8*)lock.get_mem();
	FXD::U32 nCompSize = streamO.compress_size(pUnCompData, nUnCompSize);

	{
		Memory::MemHandle mem = Memory::MemHandle::read_to_end(streamO, true);
		Memory::MemHandle::LockGuard lockO(mem);

		Core::StreamOut fileOut = FXDIO()->io_system()->io_device()->file_open_stream(oPath.c_str(), { IO::E_FileMode::Write, IO::E_SerilizeMode::Binary, true }).get();
		fileOut->write_size(lockO.get_mem(), mem.get_size());
	}
	PRINT_INFO << "   Compression - " << ((FXD::F32)nCompSize / (FXD::F32)nUnCompSize);
}

void compress_zs(const IO::Path& iPath, const IO::Path& oPath, int level)
{
	{
		/*
		Memory::StreamMemHandle fileStreamI = FXD::FXDIO()->io_system()->io_device()->file_open_memory_stream(iPath.c_str(), false).get();
		//IO::StreamFile fileStreamO = FXD::FXDIO()->io_system()->io_device()->file_open_stream(oPath.c_str(), { IO::E_FileMode::Write, IO::E_SerilizeMode::Binary, true }).get();

		FXD::U32 nUnCompSize = fileStreamI->length();
		int nOutSize = 0;

		//LZ4_stream_t lStream;
		//LZ4_stream_t* pLStream = &lStream;
		int inpBufIndex = 0;
		char inpBuf[2][BLOCK_BYTES];
		Memory::MemZero(inpBuf[0], BLOCK_BYTES);
		Memory::MemZero(inpBuf[1], BLOCK_BYTES);

		int nCompSize = GetMaxCompressedLen(BLOCK_BYTES);
		FXD::U8* pCompData = (FXD::U8*)malloc((size_t)nCompSize);

		z_stream zInfo = { 0 };
		//zInfo.next_out = pCompData;
		//zInfo.avail_out = nCompSize;
		//zInfo.next_in = pUnCompData;
		//zInfo.avail_in = nUnCompSize;

		//LZ4_initStream(pLStream, sizeof(*pLStream));
		int nErr = deflateInit(&zInfo, level);
		for (;;)
		{
			char* const inpPtr = inpBuf[inpBufIndex];
			const FXD::U32 inpBytes = fileStreamI->read_size(inpPtr, BLOCK_BYTES);
			if (0 == inpBytes)
			{
				break;
			}
			{
				zInfo.next_in = (FXD::U8*)inpPtr;
				zInfo.avail_in = inpBytes;
				zInfo.next_out = pCompData;
				zInfo.avail_out = nCompSize;

				nErr = deflate(&zInfo, Z_FINISH);
				if (nErr == Z_STREAM_END)
				{
					nOutSize += sizeof(FXD::S32);
					nOutSize += zInfo.total_out;
					//fileStreamO->write_size(&cmpBytes, sizeof(FXD::S32));
					//fileStreamO->write_size(&cmpBuf, cmpBytes);
				}
			}

			inpBufIndex = (inpBufIndex + 1) % 2;
		}
		deflateEnd(&zInfo);

		FXD::S32 nFinal = 0;
		//fileStreamO->write_size(&nFinal, sizeof(FXD::S32));
		nOutSize += sizeof(FXD::S32);
		//const int nOutSize = fileStreamO->length();

		PRINT_INFO << "   Compression ZS - " << ((FXD::F32)nOutSize / (FXD::F32)nUnCompSize) << "%  " << timer.elapsed_time();
		*/
	}
}

void compress_ls(const IO::Path& iPath, const IO::Path& oPath, int level)
{
	Core::Timer timer;
	timer.start_counter();

	{
		Memory::StreamMemHandle fileStreamI = FXD::FXDIO()->io_system()->io_device()->file_open_memory_stream(iPath.c_str(), false).get();
		//IO::StreamFile fileStreamO = FXD::FXDIO()->io_system()->io_device()->file_open_stream(oPath.c_str(), { IO::E_FileMode::Write, IO::E_SerilizeMode::Binary, true }).get();

		FXD::U32 nUnCompSize = fileStreamI->length();
		int nOutSize = 0;

		/*
		LZ4_stream_t lStream;
		int inpBufIndex = 0;
		char inpBuf[2][BLOCK_BYTES];
		Memory::MemZero(inpBuf[0], BLOCK_BYTES);
		Memory::MemZero(inpBuf[1], BLOCK_BYTES);

		LZ4_initStream(&lStream, sizeof(lStream));

		for (;;)
		{
			char* const inpPtr = inpBuf[inpBufIndex];
			const FXD::U32 inpBytes = fileStreamI->read_size(inpPtr, BLOCK_BYTES);
			if (0 == inpBytes)
			{
				break;
			}

			{
				char cmpBuf[LZ4_COMPRESSBOUND(BLOCK_BYTES)];
				const int cmpBytes = LZ4_compress_fast_continue(&lStream, inpPtr, cmpBuf, inpBytes, sizeof(cmpBuf), level);
				if (cmpBytes <= 0)
				{
					break;
				}

				nOutSize += sizeof(FXD::S32);
				nOutSize += cmpBytes;
				//fileStreamO->write_size(&cmpBytes, sizeof(FXD::S32));
				//fileStreamO->write_size(&cmpBuf, cmpBytes);
			}

			inpBufIndex = (inpBufIndex + 1) % 2;
		}

		FXD::S32 nFinal = 0;
		//fileStreamO->write_size(&nFinal, sizeof(FXD::S32));
		nOutSize += sizeof(FXD::S32);
		//const int nOutSize = fileStreamO->length();

		PRINT_INFO << "   Compression LS - " << ((FXD::F32)nOutSize / (FXD::F32)nUnCompSize) << "%  " << timer.elapsed_time();
		*/
	}
}

void compress_lf(const IO::Path& iPath, const IO::Path& oPath)
{
	Core::Timer timer;
	timer.start_counter();

	{
		Core::StreamIn fileStreamI = FXD::FXDIO()->io_system()->io_device()->file_open_stream(iPath.c_str(), { IO::E_FileMode::Read, IO::E_SerilizeMode::Binary, true }).get();
		Core::StreamOut fileStreamO = FXD::FXDIO()->io_system()->io_device()->file_open_stream(oPath.c_str(), { IO::E_FileMode::Write, IO::E_SerilizeMode::Binary, true }).get();

		// ressource allocation
		LZ4F_compressionContext_t ctx;
		size_t const ctxCreation = LZ4F_createCompressionContext(&ctx, LZ4F_VERSION);
		void* const src = malloc(IN_CHUNK_SIZE);
		size_t const outbufCapacity = LZ4F_compressBound(IN_CHUNK_SIZE, &kPrefs);   // large enough for any input <= IN_CHUNK_SIZE
		void* const outbuff = malloc(outbufCapacity);

		compressResult_t result = { 1, 0, 0 };  // == error (default)
		if (!LZ4F_isError(ctxCreation) && src && outbuff)
		{
			result = compress_file_internal(fileStreamI, fileStreamO,
				ctx,
				src, IN_CHUNK_SIZE,
				outbuff, outbufCapacity);
		}
		else
		{
			//printf("error : ressource allocation failed \n");
		}

		LZ4F_freeCompressionContext(ctx);   // supports free on NULL
		free(src);
		free(outbuff);
		PRINT_INFO << "   Compression LF - " << ((FXD::F32)result.size_out / (FXD::F32)result.size_in) << "%  " << timer.elapsed_time();
	}
}

void decompress(const IO::Path& iPath, const IO::Path& oPath)
{
	/*
	Core::StreamIn fileStreamI = FXD::FXDIO()->io_system()->io_device()->file_open_stream(iPath.c_str(), { IO::E_FileMode::Read, IO::E_SerilizeMode::Binary, false }).get();
	//Core::StreamOut fileStreamO = FXD::FXDIO()->io_system()->io_device()->file_open_stream(oPath.c_str(), { IO::E_FileMode::Write, IO::E_SerilizeMode::Binary, true }).get();

	LZ4_streamDecode_t lz4StreamDecode_body;
	LZ4_streamDecode_t* lz4StreamDecode = &lz4StreamDecode_body;

	char decBuf[2][BLOCK_BYTES];
	int decBufIndex = 0;

	LZ4_setStreamDecode(lz4StreamDecode, NULL, 0);


	for (;;)
	{
		char cmpBuf[LZ4_COMPRESSBOUND(BLOCK_BYTES)];
		int  cmpBytes = 0;

		{
			const size_t readCount0 = fileStreamI->read_size(&cmpBytes, sizeof(cmpBytes));
			if (readCount0 != 4 || cmpBytes <= 0)
			{
				break;
			}

			const size_t readCount1 = fileStreamI->read_size(cmpBuf, cmpBytes);
			if (readCount1 != (size_t)cmpBytes)
			{
				break;
			}
		}
		{
			char* const decPtr = decBuf[decBufIndex];
			const int decBytes = LZ4_decompress_safe_continue(lz4StreamDecode, cmpBuf, decPtr, cmpBytes, BLOCK_BYTES);
			if (decBytes <= 0)
			{
				break;
			}
			//fileStreamO->write_size(decPtr, (size_t)decBytes);
		}
		decBufIndex = (decBufIndex + 1) % 2;
	}
	*/
}

int compare(const IO::Path& iPath, const IO::Path& oPath, Core::CompressParams compressParams)
{
	Core::StreamIn fileStreamI1 = FXD::FXDIO()->io_system()->io_device()->file_open_stream(iPath.c_str(), { IO::E_FileMode::Read, IO::E_SerilizeMode::Binary, false }).get();
	Core::StreamIn fileStreamI2 = FXD::FXDIO()->io_system()->io_device()->file_open_stream(oPath.c_str(), { IO::E_FileMode::Read, IO::E_SerilizeMode::Binary, false }, compressParams).get();
	int result = 0;

	FXD::S32 nLen1 = fileStreamI1->length();
	FXD::S32 nLen2 = fileStreamI2->length();
	void* b1 = FXD::Memory::Alloc::Default.allocate(nLen1);
	void* b2 = FXD::Memory::Alloc::Default.allocate(nLen1);

	while (0 == result)
	{
		const size_t r1 = fileStreamI1->decompress_size(b1, nLen1);
		const size_t r2 = fileStreamI2->decompress_size(b2, nLen1);

		result = (int)r1 - (int)r2;

		if (0 == r1 || 0 == r2)
		{
			break;
		}
		if (0 == result)
		{
			result = memcmp(b1, b2, r1);
		}

	}
	FXD::Memory::Alloc::Default.deallocate(b1);
	FXD::Memory::Alloc::Default.deallocate(b2);

	return result;
}

/*
void compress_one_file(zipFile zfile, const IO::Path& iPath, const IO::Path& oPath)
{
	{
		Memory::StreamMemHandle fileStream = FXD::FXDIO()->io_system()->io_device()->file_open_memory_stream(iPath.c_str(), false, { }).get();

		zip_fileinfo zfi = { 0 };
		int ii = zipOpenNewFileInZip(zfile, oPath.c_str(), &zfi, NULL, 0, NULL, 0, NULL, Z_DEFLATED, Z_DEFAULT_COMPRESSION);

		size_t ss = fileStream->length();
		FXD::Container::Vector<char> buffer(ss);
		fileStream->read_size(&buffer[0], ss);
		ii = zipWriteInFileInZip(zfile, ss == 0 ? "" : &buffer[0], ss);
		ii = zipCloseFileInZip(zfile);
	}

	{
		unzFile zfile = unzOpen64(outfilename.c_str());
		if (!zfile)
		{
			return -1;
		}
		unz_global_info64 gi;
		int ii = unzGetGlobalInfo64(zfile, &gi);
		ii = unzGoToFirstFile(zfile);
		ii = unzGoToNextFile(zfile);

		FXD::Container::Vector<char> buffer(124);
		unz_file_info file_info;
		unzGetCurrentFileInfo(zfile, &file_info, &buffer[0], 124, 0, 0, 0, 0);

		unzClose(zfile);
	}
}

void zipper_add_dir(zipFile zfile, const IO::Path& oPath)
{
	FXD::Core::String8 str = oPath.c_str();
	str += UTF_8("/");

	zip_fileinfo zfi = { 0 };
	int ii = zipOpenNewFileInZip(zfile, str.c_str(), &zfi, NULL, 0, NULL, 0, NULL, Z_DEFLATED, Z_DEFAULT_COMPRESSION);
	ii = zipWriteInFileInZip(zfile, 0, 0);
	ii = zipCloseFileInZip(zfile);
}
*/

// ------
// CompressConverter
// -
// ------
CompressConverter::CompressConverter(const Container::Map< Core::String8, Core::String8 >& params, const IO::Path& pathFile, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp)
	: IConverter(params, pathFile, pathSrc, pathDst, pathTmp)
{
}

CompressConverter::~CompressConverter(void)
{
}

const Core::E_OS CompressConverter::get_platform(void) const
{
	return Core::E_OS::Windows;
}

const Converter::E_ConverterType CompressConverter::get_type(void) const
{
	return Converter::E_ConverterType::Compress;
}

const Core::String8 CompressConverter::get_converter_name(void) const
{
	return UTF_8("Compress");
}

const Core::String8 CompressConverter::get_convert_file_name(void) const
{
	return m_pathFile.filename();
}

const Core::String8 CompressConverter::get_convert_file_extension(void) const
{
	return m_pathFile.extension();
}

FXD::Memory::MemHandle CompressConverter::convert(const Core::StreamIn& stream)
{
	//PRINT_INFO << inRawPath;
	/*
	{
		IO::Path inPath = m_inFile;
		IO::Path outPath = m_inFile;
		outPath.replace_extension(outPath.extension() + "c");

		compress_normal(inPath, outPath, { Core::E_CompressionType::LZ4, Core::E_CompressionQuality::Default });
		PRINT_COND_ASSERT(compare(inPath, outPath, { Core::E_CompressionType::LZ4, Core::E_CompressionQuality::Default }) == 0, "");

		PRINT_LINE;
		//decompress(inDec, inCompAp2;
		//compare(inConvAp, inCompAp);
		//compress_one_file(zfile, inConvAp, inRelPath);
	}
	*/

	/*
	Memory::StreamMemory streamO({ Core::E_CompressionType::LZ4, Core::E_CompressionQuality::Default });

	{
		Memory::MemHandle mem = Memory::MemHandle::read_to_end((*stream), true);
		Memory::MemHandle::LockGuard lock(mem);
		FXD::U32 nUnCompSize = mem.get_size();
		FXD::U8* pUnCompData = (FXD::U8*)lock.get_mem();
		FXD::U32 nCompSize = streamO.compress_size(pUnCompData, nUnCompSize);
	}
	return Memory::MemHandle::read_to_end(streamO, true);
	*/
	return Memory::MemHandle::read_to_end(*stream, true);;
}