// Creator - MatthewGolder
#include "DataBuilder/Converter/CRecord.h"

using namespace FXD;
using namespace DataBuilder;
using namespace Converter;

// ------
// CRecord
// -
// ------
CRecord::CRecord(void)
{
}

CRecord::~CRecord(void)
{
}

void CRecord::add(Converter::E_ConverterType eConverterType, const Core::String8& strSrcName, const IO::Path& pathSrc, const Core::String8& strDstName, const IO::Path& pathDst)
{
	Container::Map< Core::String8, Core::String8 > map =
	{
		{ UTF_8("ConverterType"), Converter::ToString(eConverterType).c_str() },
		{ UTF_8("SrcName"), strSrcName },
		{ UTF_8("SrcPath"), strSrcName.c_str() },
		{ UTF_8("DstName"), strDstName },
		{ UTF_8("DstPath"), pathDst.c_str() }
	};
	m_convertInfo.push_back(map);
}