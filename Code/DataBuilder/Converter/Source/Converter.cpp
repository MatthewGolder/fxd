// Creator - MatthewGolder
#include "DataBuilder/Converter/Converter.h"
#include "DataBuilder/Converter/CRecord.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOStreamFile.h"
#include "FXDEngine/IO/IOSystem.h"
#include "FXDEngine/IO/IODevice.h"
#include "FXDEngine/IO/Path.h"
#include "FXDEngine/IO/IOFileInfo.h"
#include "FXDEngine/IO/DirectoryEntry.h"
#include "FXDEngine/IO/DirectoryIterator.h"

using namespace FXD;
using namespace DataBuilder;
using namespace Converter;

// ------
// IConverter
// -
// ------
IConverter::IConverter(const Container::Map< Core::String8, Core::String8 >& params, const IO::Path& pathFile, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp)
	: m_convertParams(params)
	, m_pathFile(pathFile)
	, m_pathSrc(pathSrc)
	, m_pathDst(pathDst)
	, m_pathTmp(pathTmp)
{
}

IConverter::~IConverter(void)
{
}

bool IConverter::start_conversion(Converter::CRecord& record)
{
	const Core::String8 strFileNameSrc = get_input_file_name();
	const Core::String8 strFileNameDst = get_convert_file_name();
	const IO::Path pathSrc = m_pathSrc / strFileNameSrc;
	const IO::Path pathDst = m_pathDst / strFileNameDst;

	// Convert the in file
	bool bConvertSuccess = false;
	bool bAlreadyCreated = false;
	Memory::MemHandle memHandle;
	{
		// Open the input file
		IO::StreamFile streamSrc = FXDIO()->io_system()->io_device()->file_open_stream(pathSrc, { IO::E_FileMode::Read, IO::E_SerilizeMode::Binary, false }).get();
		if (streamSrc)
		{
			// Is there already an up to date version that has been converted
			{
				IO::StreamFile streamDst = FXDIO()->io_system()->io_device()->file_open_stream(pathDst, { IO::E_FileMode::Read, IO::E_SerilizeMode::Binary, true }).get();
				if (streamDst)
				{
					IO::FileInfo fileInfoSrc = streamSrc->get_file_info();
					IO::FileInfo fileInfoDst = streamDst->get_file_info();

					FXD::S64 nOutSize = fileInfoDst.file_size();
					Core::Date dateSrc = fileInfoSrc.last_modify_date();
					Core::Date dateDst = fileInfoDst.last_modify_date();
					bAlreadyCreated = ((nOutSize != 0) && (dateDst > dateSrc));
				}
			}

			// If we need to process the file the do it
			if (bAlreadyCreated == false)
			{
				// Print the input file information
				PRINT_INFO << "Start DataBuilder: " << get_converter_name();
				IO::FileInfo fileInfoIn = streamSrc->get_file_info();
				PRINT_INFO << "DataBuilder: Source File - " << pathSrc.c_str();
				PRINT_INFO << "DataBuilder:	Size - " << fileInfoIn.file_size() << UTF_8(" bytes");
				PRINT_INFO << "DataBuilder:	Last Accessed - " << fileInfoIn.last_access_date();

				// Convert the input stream
				Core::StreamIn streamConv(streamSrc);
				memHandle = convert(streamConv);
				if (memHandle.is_vaild() == false)
				{
					PRINT_ERROR << "Failed Conversion!\n\n";
				}
			}
			else
			{
				bConvertSuccess = true;
			}
		}
		else
		{
			PRINT_ERROR << "Failed to find the input stream.\n\n";
		}
	}

	// 2. Write out converted file
	if (memHandle.get_size() > 0)
	{
		// 2.1. Open the convert file
		IO::StreamFile streamDst = FXDIO()->io_system()->io_device()->file_open_stream(pathDst, { IO::E_FileMode::Write, IO::E_SerilizeMode::Binary, false }).get();
		if (!streamDst)
		{
			PRINT_ERROR << "Failed to write Converted File!\n\n";
		}
		else
		{
			// 2.2.1 Write to the convert file
			streamDst->write_size(Memory::MemHandle::LockGuard(memHandle).get_mem(), memHandle.get_size());

			// 2.2.2 Print the convert file information
			IO::FileInfo fileInfoDst = streamDst->get_file_info();
			PRINT_INFO << "DataBuilder: Out File - " << pathDst.c_str();
			PRINT_INFO << "DataBuilder:	Size - " << fileInfoDst.file_size() << UTF_8(" bytes");
			PRINT_INFO << "[SUCCESS]!\n";
			bConvertSuccess = true;
		}
	}

	record.add(get_type(), strFileNameSrc, m_pathSrc, strFileNameDst, m_pathDst);
	return true;
}

const Core::String8 IConverter::get_input_file_name(void) const
{
	return m_pathFile;
}

const Core::String8 IConverter::get_input_file_extension(void) const
{
	return m_pathFile.extension();
}