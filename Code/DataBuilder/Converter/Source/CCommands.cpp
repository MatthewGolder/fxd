// Creator - MatthewGolder
#include "DataBuilder/Converter/CCommands.h"
#include "FXDEngine/Core/OS.h"

using namespace FXD;
using namespace DataBuilder;
using namespace Converter;

// ------
// CCommandGroup
// -
// ------
CCommandGroup::CCommandGroup(const IO::Path& pathRaw, const IO::Path& pathConvert, const IO::Path& pathCompress, const IO::Path& pathTmp)
	: m_pathRaw(pathRaw)
	, m_pathConvert(pathConvert)
	, m_pathCompress(pathCompress)
	, m_pathTmp(pathTmp)
{
}

CCommandGroup::~CCommandGroup(void)
{
}

const IO::Path& CCommandGroup::get_path_raw(void) const
{
	return m_pathRaw;
}

const IO::Path& CCommandGroup::get_path_convert(void) const
{
	return m_pathConvert;
}

const IO::Path& CCommandGroup::get_path_compress(void) const
{
	return m_pathCompress;
}

const IO::Path& CCommandGroup::get_path_temp(void) const
{
	return m_pathTmp;
}