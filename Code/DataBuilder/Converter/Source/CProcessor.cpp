// Creator - MatthewGolder
#include "DataBuilder/Converter/CProcessor.h"
#include "DataBuilder/Converter/PlatformAndroid.h"
#include "DataBuilder/Converter/PlatformWin32.h"

#include "FXDEngine/App/FXDApi.h"
#include <FXDEngine/IO/IOJPThread.h>
#include <FXDEngine/IO/IOSystem.h>
#include <FXDEngine/IO/IODevice.h>
#include "FXDEngine/IO/DirectoryEntry.h"
#include "FXDEngine/IO/DirectoryIterator.h"

using namespace FXD;
using namespace DataBuilder;
using namespace Converter;

// ------
// CProcessor
// -
// ------
CProcessor::CProcessor(Converter::CCommandGroup& commandGroup, Core::E_OS ePlatformType)
	: m_commandGroup(commandGroup)
	, m_ePlatformType(ePlatformType)
{
}

CProcessor::~CProcessor(void)
{
}

FXD::U32 CProcessor::convert(Converter::CRecord& record)
{
	IO::Path pathSrc = m_commandGroup.get_path_raw();
	IO::Path pathDst = m_commandGroup.get_path_convert();
	IO::Path pathTmp = m_commandGroup.get_path_temp();
	iterate_convert(record, pathSrc, pathDst, pathTmp);
	return 0;
}

extern int test_zlib();

FXD::U32 CProcessor::compress(Converter::CRecord& record)
{
	//test_zlib();

	IO::Path pathSrc = m_commandGroup.get_path_convert();
	IO::Path pathDst = m_commandGroup.get_path_compress();
	IO::Path pathTmp = m_commandGroup.get_path_temp();
	iterate_compress(record, pathSrc, pathDst, pathTmp);
	return 0;
}

FXD::U32 CProcessor::pack(Converter::CRecord& record)
{
	return 0;
}

void CProcessor::iterate_convert(Converter::CRecord & record, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp)
{
	for (auto& itr : IO::directory_iterator(pathSrc))
	{
		if (itr.is_directory())
		{
			// 1. Expand directories
			IO::Path pathSrcNew = pathSrc;
			IO::Path pathDstNew = pathDst;
			IO::Path pathTmpNew = pathTmp;
			pathSrcNew.append_create(itr.path().filename().string());
			pathDstNew.append_create(itr.path().filename().string());
			pathTmpNew.append_create(itr.path().filename().string());

			// 2. Iterate Directories
			iterate_convert(record, pathSrcNew, pathDstNew, pathTmpNew);
		}
		else if (itr.is_regular_file())
		{
			fxd_for(const auto& command, m_commandGroup.m_commands)
			{
				bool bDoConvert = ((command.m_eTargetPlatform == Core::E_OS::Unknown) || (command.m_eTargetPlatform == m_ePlatformType));
				if (bDoConvert)
				{
					Core::String8 strFileName = itr.path().extension();
					if (strFileName.ends_with_no_case(command.m_strFileType))
					{
						process_convert(record, command, itr.path().filename(), pathSrc, pathDst, pathTmp);
					}
				}
			}
		}
	}
}

void CProcessor::process_convert(Converter::CRecord& record, const Converter::CCommand& command, const IO::Path& pathFile, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp)
{
	bool bRetVal = false;
	switch(m_ePlatformType)
	{
		case Core::E_OS::Windows:
		{
			bRetVal = handle_platform_windows(record, command, pathFile, pathSrc, pathDst, pathTmp);
			break;
		}
		case Core::E_OS::Android:
		{
			bRetVal = handle_platform_android(record, command, pathFile, pathSrc, pathDst, pathTmp);
			break;
		}
	}
}

void CProcessor::iterate_compress(Converter::CRecord& record, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp)
{
	//PRINT_INFO << compPath;
	//zipper_add_dir(zfile, compPath);

	for (auto& itr : IO::directory_iterator(pathSrc))
	{
		if (itr.is_directory())
		{
			// 1. Expand directories
			IO::Path pathSrcNew = pathSrc;
			IO::Path pathDstNew = pathDst;
			IO::Path pathTmpNew = pathTmp;
			pathSrcNew.append_create(itr.path().filename());
			pathDstNew.append_create(itr.path().filename());
			pathTmpNew.append_create(itr.path().filename());

			// 2. Iterate Directories
			iterate_compress(record, pathSrcNew, pathDstNew, pathTmpNew);
		}
		else if (itr.is_regular_file())
		{
			process_compress(record, itr.path().filename(), pathSrc, pathDst, pathTmp);
		}
	}
}

void CProcessor::process_compress(Converter::CRecord& record, const IO::Path& pathFile, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp)
{
	Converter::CCommand command;
	command.m_eConverterType = Converter::E_ConverterType::Compress;
	process_convert(record, command, pathFile, pathSrc, pathDst, pathTmp);
}