// Creator - MatthewGolder
#include "DataBuilder/Converter/CFileParser.h"
#include "FXDEngine/Core/OS.h"

using namespace FXD;
using namespace DataBuilder;
using namespace Converter;

// ------
// CFileParser
// -
// ------
CFileParser::CFileParser(const IO::Path& pathRaw, const IO::Path& pathConvert, const IO::Path& pathCompress, const IO::Path& pathTmp)
	: m_eTag(E_Tag::Unknown)
	, m_pathRaw(pathRaw)
	, m_pathConvert(pathConvert)
	, m_pathCompress(pathCompress)
	, m_pathTmp(pathTmp)
{
}

CFileParser::~CFileParser(void)
{
}

void CFileParser::handle_tag(const Core::String8& strName, ITagCB*& pTagCB)
{
	pTagCB = this;
	switch (m_eTag)
	{
		case E_Tag::Unknown:
		{
			if (strName == UTF_8("DataConverter"))
			{
				m_eTag = E_Tag::DataConverter;
			}
			break;
		}
		case E_Tag::DataConverter:
		{
			if (strName == Core::String8("CommandGroup"))
			{
				m_commandGroups.emplace_back(m_pathRaw, m_pathConvert, m_pathCompress, m_pathTmp);
				m_eTag = E_Tag::ConverterInfo;
			}
			break;
		}
		case E_Tag::ConverterInfo:
		{
			if (strName == Core::String8("Command"))
			{
				m_eTag = E_Tag::Command;
				m_commandGroups.back().m_commands.emplace_back();
			}
			break;
		}
		default:
		{
			PRINT_ASSERT << "DataBuilder: Unknown XML Tag in CFileParser";
			break;
		}
	}
}

void CFileParser::end_tag(const Core::String8& strName)
{
	switch (m_eTag)
	{
		case E_Tag::DataConverter:
		{
			m_eTag = E_Tag::Unknown;
			break;
		}
		case E_Tag::ConverterInfo:
		{
			m_eTag = E_Tag::DataConverter;
			break;
		}
		case E_Tag::Command:
		{
			m_eTag = E_Tag::ConverterInfo;
			break;
		}
		default:
		{
			break;
		}
	}
}

void CFileParser::handle_attribute(const Core::String8& strName, const Core::String8& strValue)
{
	switch (m_eTag)
	{
		case E_Tag::ConverterInfo:
		{
			if (strName == Core::String8("directory"))
			{
				m_commandGroups.back().m_pathRaw.append_create(strValue);
				m_commandGroups.back().m_pathConvert.append_create(strValue);
				m_commandGroups.back().m_pathCompress.append_create(strValue);
				m_commandGroups.back().m_pathTmp.append_create(strValue);
			}
			break;
		}
		case E_Tag::Command:
		{
			if (strName == Core::String8("file_extension"))
			{
				m_commandGroups.back().m_commands.back().m_strFileType = strValue;
			}
			else if (strName == Core::String8("target_platform"))
			{
				m_commandGroups.back().m_commands.back().m_eTargetPlatform = FXD::Core::OS::ToType(strValue.c_str());
			}
			else if(strName == Core::String8("converter_type"))
			{
				m_commandGroups.back().m_commands.back().m_eConverterType = Converter::ToType(strValue.c_str());
			}
			else if (strName == Core::String8("params"))
			{
				Container::Vector< Core::String8 > vecStr = strValue.to_str_array(FXD::UTF8(','));
				for (size_t n1 = 0; n1 < vecStr.size(); n1++)
				{
					Core::String8 strs[2];
					vecStr[n1].to_str_array(FXD::UTF8('='), &strs[0], 2);
					strs[0].trim();
					strs[1].trim();

					m_commandGroups.back().m_commands.back().m_convertParams[strs[0]] = strs[1];
				}
			}
			break;
		}
		default:
		{
			break;
		}
	}
}