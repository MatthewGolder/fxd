// Creator - MatthewGolder
#include "DataBuilder/Converter/Types.h"

using namespace FXD;
using namespace DataBuilder;
using namespace Converter;

// ------
// Converter
// -
// ------
const Core::StringView8 sConverts[] =
{
	"Audio",		// Converter::E_ConverterType::Audio
	"Copy",		// Converter::E_ConverterType::Copy
	"Material",	// Converter::E_ConverterType::Material
	"Shader",	// Converter::E_ConverterType::Shader
	"Texture",	// Converter::E_ConverterType::Texture
	"Compress",	// Converter::E_ConverterType::Compress
	"Count"		// Converter::E_ConverterType::Count
};

const Core::StringView8 DataBuilder::Converter::ToString(const Converter::E_ConverterType eType)
{
	return sConverts[FXD::STD::to_underlying(eType)].c_str();
}

const Converter::E_ConverterType DataBuilder::Converter::ToType(const Core::StringView8 strType)
{
	for (FXD::U16 n1 = 0; n1 < FXD::STD::to_underlying(Converter::E_ConverterType::Count); ++n1)
	{
		if (strType.compare_no_case(sConverts[n1]) == 0)
		{
			return (Converter::E_ConverterType)n1;
		}
	}
	return Converter::E_ConverterType::Unknown;
}