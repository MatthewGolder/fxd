// Creator - MatthewGolder
#pragma once
#ifndef DATABUILDER_CONVERTER_CONVERTER_H
#define DATABUILDER_CONVERTER_CONVERTER_H

#include "DataBuilder/Converter/Types.h"
#include "FXDEngine/Core/OS.h"
#include "FXDEngine/Container/Map.h"
#include "FXDEngine/IO/Path.h"
#include "FXDEngine/Memory/MemHandle.h"

using namespace FXD;

namespace DataBuilder
{
	class BuilderApp;

	namespace Converter
	{
		class CRecord;

		// ------
		// IConverter
		// -
		// ------
		class IConverter : public Core::NonCopyable
		{
		public:
			IConverter(const Container::Map< Core::String8, Core::String8 >& params, const IO::Path& pathFile, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp);
			virtual ~IConverter(void);

			bool start_conversion(Converter::CRecord& record);

			virtual const Core::E_OS get_platform(void) const PURE;
			virtual const Converter::E_ConverterType get_type(void) const PURE;

			const Core::String8 get_input_file_name(void) const;
			const Core::String8 get_input_file_extension(void) const;
			virtual const Core::String8 get_converter_name(void) const PURE;
			virtual const Core::String8 get_convert_file_name(void) const PURE;
			virtual const Core::String8 get_convert_file_extension(void) const PURE;

		private:
			virtual Memory::MemHandle convert(const Core::StreamIn& stream) PURE;

		protected:
			Container::Map< Core::String8, Core::String8 > m_convertParams;
			const IO::Path m_pathFile;
			const IO::Path m_pathSrc;
			const IO::Path m_pathDst;
			const IO::Path m_pathTmp;
		};

	} //namespace Converter
} //namespace DataBuilder
#endif //DATABUILDER_CONVERTER_CONVERTER_H