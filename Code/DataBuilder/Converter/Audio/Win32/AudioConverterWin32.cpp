// Creator - MatthewGolder
#include "DataBuilder/Converter/Audio/Win32/AudioConverterWin32.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/App/Win32/FuncsWin32.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOSystem.h"
#include "FXDEngine/IO/IODevice.h"
#include "FXDEngine/IO/DirectoryEntry.h"
#include "FXDEngine/IO/DirectoryIterator.h"

using namespace FXD;
using namespace DataBuilder;
using namespace Converter;

// ------
// AudioConverterWin32
// -
// ------
AudioConverterWin32::AudioConverterWin32(const Container::Map< Core::String8, Core::String8 >& params, const IO::Path& pathFile, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp)
	: IAudioConverter(params, pathFile, pathSrc, pathDst, pathTmp)
{
}

AudioConverterWin32::~AudioConverterWin32(void)
{
}

const Core::E_OS AudioConverterWin32::get_platform(void) const
{
	return Core::E_OS::Windows;
}

Memory::MemHandle AudioConverterWin32::convert(const Core::StreamIn& stream)
{
	// 1. Check Target Type
	if (m_eTarget_type == SND::E_AudioAsset::Count)
	{
		return Memory::MemHandle();
	}

	// 2. Convert and get memory
	switch (m_eTarget_type)
	{
		case SND::E_AudioAsset::Ogg:
		{
			return convert_ogg(stream);
		}
		case SND::E_AudioAsset::Wav:
		{
			return convert_wav(stream);
		}
		default:
		{
			return Memory::MemHandle();
		}
	}
}

Memory::MemHandle AudioConverterWin32::convert_ogg(const Core::StreamIn& /*stream*/)
{
	switch (m_eSourceType)
	{
		case SND::E_AudioAsset::Ogg:
		case SND::E_AudioAsset::Wav:
		{
			// 1. Get setup parameters
			IO::Path pathIn = m_pathDst / get_input_file_name();
			IO::Path pathTemp = m_pathTmp / get_convert_file_name().c_str();

			Core::StringBuilder8 strStream;
			strStream
				<< "sox --show "
				<< pathIn.c_str() << " "
				<< pathTemp.c_str() << " "
				<< "rate " << m_nBitRate;

			// 2. Run process
			IO::Path pathModule = IO::FS::GetModuleDirectory();
			if (!App::FuncsWin32::runProcess(pathModule, UTF_8("sox.exe"), strStream.str()))
			{
				return Memory::MemHandle();
			}

			// 3. Check if the file actually was converted
			if (!pathTemp.file_exists())
			{
				return Memory::MemHandle();
			}

			// 4. Read memory and delete temp file
			Memory::MemHandle retMemHandle = FXDIO()->io_system()->io_device()->file_open_memhandle(pathTemp, false, { }).get();
			pathTemp.file_delete();
			return retMemHandle;
		}
		default:
		{
			return Memory::MemHandle();
		}
	}
}

Memory::MemHandle AudioConverterWin32::convert_wav(const Core::StreamIn& /*stream*/)
{
	switch (m_eSourceType)
	{
		case SND::E_AudioAsset::Ogg:
		case SND::E_AudioAsset::Wav:
		{
			// 1. Get setup parameters
			IO::Path pathIn = m_pathDst / get_input_file_name();
			IO::Path pathTemp = m_pathTmp / get_convert_file_name().c_str();

			Core::StringBuilder8 strStream;
			strStream
				<< "sox --show "
				<< pathIn.c_str() << " "
				<< pathTemp.c_str() << " "
				<< "rate " << m_nBitRate;

			// 2. Run process
			IO::Path pathModule = IO::FS::GetModuleDirectory();
			if (!App::FuncsWin32::runProcess(pathModule, UTF_8("sox.exe"), strStream.str()))
			{
				return Memory::MemHandle();
			}

			// 3. Check if the file actually was converted
			if (!pathTemp.file_exists())
			{
				return Memory::MemHandle();
			}

			// 4. Read memory and delete temp file
			Memory::MemHandle retMemHandle = FXDIO()->io_system()->io_device()->file_open_memhandle(pathTemp, false, { }).get();
			pathTemp.file_delete();
			return retMemHandle;
		}
		default:
		{
			return Memory::MemHandle();
		}
	}
}