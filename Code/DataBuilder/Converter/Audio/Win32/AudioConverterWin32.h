// Creator - MatthewGolder
#pragma once
#ifndef DATABUILDER_CONVERTER_AUDIO_WIN32_AUDIOCONVERTERWIN32_H
#define DATABUILDER_CONVERTER_AUDIO_WIN32_AUDIOCONVERTERWIN32_H

#include "DataBuilder/Converter/Audio/AudioConverter.h"

namespace DataBuilder
{
	namespace Converter
	{
		// ------
		// AudioConverterWin32
		// -
		// ------
		class AudioConverterWin32 FINAL : public Converter::IAudioConverter
		{
		public:
			AudioConverterWin32(const Container::Map< Core::String8, Core::String8 >& params, const IO::Path& pathFile, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp);
			~AudioConverterWin32(void);
				
			const Core::E_OS get_platform(void) const FINAL;
				
		private:
			Memory::MemHandle convert(const Core::StreamIn& stream);
			FXD::Memory::MemHandle convert_ogg(const Core::StreamIn& stream);
			FXD::Memory::MemHandle convert_wav(const Core::StreamIn& stream);
		};

	} //namespace Converter
} //namespace DataBuilder
#endif //DATABUILDER_CONVERTER_AUDIO_WIN32_AUDIOCONVERTERWIN32_H