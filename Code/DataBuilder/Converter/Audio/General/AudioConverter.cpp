// Creator - MatthewGolder
#include "DataBuilder/Converter/Audio/AudioConverter.h"
#include "FXDEngine/IO/DirectoryEntry.h"
#include "FXDEngine/IO/DirectoryIterator.h"
#include "FXDEngine/IO/Path.h"

using namespace FXD;
using namespace DataBuilder;
using namespace Converter;

// ------
// IAudioConverter
// -
// ------
IAudioConverter::IAudioConverter(const Container::Map< Core::String8, Core::String8 >& params, const IO::Path& pathFile, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp)
	: IConverter(params, pathFile, pathSrc, pathDst, pathTmp)
	, m_nBitRate(48000)
	, m_eSourceType(SND::E_AudioAsset::Count)
	, m_eTarget_type(SND::E_AudioAsset::Count)
{
	m_eSourceType = FXD::SND::File::ToFileType(pathFile.extension().c_str());
	m_eTarget_type = m_eSourceType;
	
	Core::String8 strOutType;
	if (m_convertParams.try_get_value(UTF_8("OutType"), strOutType))
	{
		m_eTarget_type = FXD::SND::File::ToFileType(strOutType.c_str());
	}
	if (m_convertParams.try_get_value(UTF_8("BitRate"), strOutType))
	{
		m_nBitRate = strOutType.to_S32();
	}
}

IAudioConverter::~IAudioConverter(void)
{
}

const Converter::E_ConverterType IAudioConverter::get_type(void) const
{
	return Converter::E_ConverterType::Audio;
}

const Core::String8 IAudioConverter::get_converter_name(void) const
{
	return UTF_8("Audio");
}

const Core::String8 IAudioConverter::get_convert_file_name(void) const
{
	Core::String8 ext = get_convert_file_extension();
	IO::Path path(m_pathFile);
	return path.replace_extension(ext);
}

const Core::String8 IAudioConverter::get_convert_file_extension(void) const
{
	return FXD::SND::File::ToString(m_eTarget_type).c_str();
}