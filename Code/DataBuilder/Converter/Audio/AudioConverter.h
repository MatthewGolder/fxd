// Creator - MatthewGolder
#pragma once
#ifndef DATABUILDER_CONVERTER_AUDIO_AUDIOCONVERTER_H
#define DATABUILDER_CONVERTER_AUDIO_AUDIOCONVERTER_H

#include "DataBuilder/Converter/Converter.h"
#include "FXDEngine/SND/Types.h"
#include "FXDEngine/SND/Decoders/Audio.h"

namespace DataBuilder
{
	namespace Converter
	{
		// ------
		// IAudioConverter
		// -
		// ------
		class IAudioConverter : public Converter::IConverter
		{
		public:
			IAudioConverter(const Container::Map< Core::String8, Core::String8 >& params, const IO::Path& pathFile, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp);
			virtual ~IAudioConverter(void);

			virtual const Core::E_OS get_platform(void) const PURE;
			virtual const Converter::E_ConverterType get_type(void) const FINAL;

			const Core::String8 get_converter_name(void) const FINAL;
			const Core::String8 get_convert_file_name(void) const FINAL;
			const Core::String8 get_convert_file_extension(void) const FINAL;
				
		protected:
			virtual FXD::Memory::MemHandle convert(const Core::StreamIn& stream) PURE;
			virtual FXD::Memory::MemHandle convert_ogg(const Core::StreamIn& stream) PURE;
			virtual FXD::Memory::MemHandle convert_wav(const Core::StreamIn& stream) PURE;

			FXD::S32 m_nBitRate;
			SND::E_AudioAsset m_eSourceType;
			SND::E_AudioAsset m_eTarget_type;
		};
			
	} //namespace Converter
} //namespace DataBuilder
#endif //DATABUILDER_CONVERTER_AUDIO_AUDIOCONVERTER_H