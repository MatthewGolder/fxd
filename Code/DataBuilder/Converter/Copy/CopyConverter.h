// Creator - MatthewGolder
#pragma once
#ifndef DATABUILDER_CONVERTER_COPY_COPYCONVERTER_H
#define DATABUILDER_CONVERTER_COPY_COPYCONVERTER_H

#include "DataBuilder/Converter/Converter.h"

namespace DataBuilder
{
	namespace Converter
	{
		// ------
		// CopyConverter
		// -
		// ------
		class CopyConverter FINAL : public Converter::IConverter
		{
		public:
			CopyConverter(const Container::Map< Core::String8, Core::String8 >& params, const IO::Path& pathFile, const IO::Path& pathSrc, const IO::Path& pathDst, const IO::Path& pathTmp);
			~CopyConverter(void);

			const Core::E_OS get_platform(void) const FINAL;
			const Converter::E_ConverterType get_type(void) const FINAL;

			const Core::String8 get_converter_name(void) const FINAL;
			const Core::String8 get_convert_file_name(void) const FINAL;
			const Core::String8 get_convert_file_extension(void) const FINAL;

		private:
			FXD::Memory::MemHandle convert(const Core::StreamIn& stream);
		};

	} //namespace Converter
} //namespace DataBuilder
#endif //DATABUILDER_CONVERTER_COPY_COPYCONVERTER_H