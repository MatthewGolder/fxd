#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "FXDEngine/IO/IOStreamFile.h"
#include "FXDEngine/Memory/StreamMemHandle.h"

#include "FXDEngine/App/FXDApi.h"
#include <FXDEngine/IO/IOJPThread.h>
#include <FXDEngine/IO/IOSystem.h>
#include <FXDEngine/IO/IODevice.h>
#include "FXDEngine/IO/DirectoryEntry.h"
#include "FXDEngine/IO/DirectoryIterator.h"
#include <3rdParty/zlib/zlib.h>

#if defined(MSDOS) || defined(OS2) || defined(WIN32) || defined(__CYGWIN__)
#  include <fcntl.h>
#  include <io.h>
#  define SET_BINARY_MODE(file) setmode(fileno(file), O_BINARY)
#else
#  define SET_BINARY_MODE(file)
#endif
#include <string>

#define CHUNK 16384

namespace
{
	FXD::U32 GetMaxCompressedLen(int nLenSrc)
	{
		FXD::U32 n16kBlocks = ((nLenSrc + 16383) / 16384); // round up any fraction of a block
		return (nLenSrc + 6 + (n16kBlocks * 5));
	}
}

int test_compress_all(FXD::Core::StreamOut sout, FXD::Core::StreamIn sin, int level)
{
	FXD::Memory::MemHandle mem = FXD::Memory::MemHandle::read_to_end(*sin, true);
	const FXD::U64 nLen = mem.get_size();
	//const FXD::U64 nCmpSize = GetMaxCompressedLen(nLen);
	//FXD::U8* pCmpPtr = (FXD::U8*)FXD::Memory::Alloc::Default.allocate(nCmpSize);

	// allocate deflate state
	z_stream zstrm;
	zstrm.zalloc = Z_NULL;
	zstrm.zfree = Z_NULL;
	zstrm.opaque = Z_NULL;
	int ret = deflateInit(&zstrm, level);
	if (ret != Z_OK)
	{
		return ret;
	}

	zstrm.next_in = (Bytef*)FXD::Memory::MemHandle::LockGuard(mem).get_mem();
	zstrm.avail_in = mem.get_size();

	char outbuffer[32768];
	std::string outstring;

	// retrieve the compressed bytes blockwise
	do
	{
		zstrm.next_out = reinterpret_cast<Bytef*>(outbuffer);
		zstrm.avail_out = sizeof(outbuffer);
		ret = deflate(&zstrm, Z_FINISH);

		if (outstring.size() < zstrm.total_out)
		{
			// append the block to the output string
			outstring.append(outbuffer, zstrm.total_out - outstring.size());
		}
	} while (ret == Z_OK);

	deflateEnd(&zstrm);

	if (ret != Z_STREAM_END)
	{
		// an error occurred that was not EOF
		//std::ostringstream oss;
		//oss << "Exception during zlib compression: (" << ret << ") " << zs.msg;
		//throw(std::runtime_error(oss.str()));
	}

	return 0;
}

int test_compress_stream(FXD::Core::StreamOut sout, FXD::Core::StreamIn sin, int level)
{
	// allocate deflate state
	z_stream zstrm;
	zstrm.zalloc = Z_NULL;
	zstrm.zfree = Z_NULL;
	zstrm.opaque = Z_NULL;
	int ret = deflateInit(&zstrm, level);
	if (ret != Z_OK)
	{
		return ret;
	}

	const FXD::U64 nCmpSize = sin->length();
	FXD::U8* pCmpPtr = (FXD::U8*)FXD::Memory::Alloc::Default.allocate(nCmpSize);

	// compress until end of file
	int flush;
	unsigned char in[CHUNK];
	unsigned char out[CHUNK];
	do
	{
		zstrm.avail_in = sin->read_size(in, CHUNK);
		PRINT_INFO << "R " << zstrm.avail_in;

		flush = (zstrm.avail_in == 0) ? Z_FINISH : Z_NO_FLUSH;
		if (zstrm.avail_in == 0)
		{
			float f = 0;
		}
		zstrm.next_in = in;

		// run deflate() on input until output buffer not full, finish compression if all of source has been read in
		do
		{
			zstrm.avail_out = CHUNK;
			zstrm.next_out = out;
			ret = deflate(&zstrm, flush);    // no bad return value
			assert(ret != Z_STREAM_ERROR);  // state not clobbered
			unsigned have = (CHUNK - zstrm.avail_out);

			PRINT_INFO << "H " << have;
			if (sout->write_size(out, have) != have)
			{
				(void)deflateEnd(&zstrm);
				return Z_ERRNO;
			}

		} while (zstrm.avail_out == 0);
		assert(zstrm.avail_in == 0);     // all input will be used

		// done when last data in file processed
	} while (flush != Z_FINISH);
	assert(ret == Z_STREAM_END);	// stream will be complete

	// clean up and return
	(void)deflateEnd(&zstrm);
	return Z_OK;
}

/* Decompress from file source to file dest until stream ends or EOF.
	inf() returns Z_OK on success, Z_MEM_ERROR if memory could not be
	allocated for processing, Z_DATA_ERROR if the deflate data is
	invalid or incomplete, Z_VERSION_ERROR if the version of zlib.h and
	the version of the library linked do not match, or Z_ERRNO if there
	is an error reading or writing the files. */
int test_decompress(FXD::Core::StreamOut sout, FXD::Core::StreamIn sin)
{
	unsigned have;
	unsigned char in[CHUNK];
	unsigned char out[CHUNK];

	// allocate inflate state
	z_stream strm;
	strm.zalloc = Z_NULL;
	strm.zfree = Z_NULL;
	strm.opaque = Z_NULL;
	strm.avail_in = 0;
	strm.next_in = Z_NULL;
	int ret = inflateInit(&strm);
	if (ret != Z_OK)
	{
		return ret;
	}

	// decompress until deflate stream ends or end of file
	do
	{
		strm.avail_in = sin->read_size(in, CHUNK);
		if (strm.avail_in == 0)
		{
			break;
		}
		strm.next_in = in;

		// run inflate() on input until output buffer not full
		do
		{
			strm.avail_out = CHUNK;
			strm.next_out = out;
			ret = inflate(&strm, Z_NO_FLUSH);
			assert(ret != Z_STREAM_ERROR);  // state not clobbered
			switch (ret)
			{
				case Z_NEED_DICT:
				{
					ret = Z_DATA_ERROR;     // and fall through
				}
				case Z_DATA_ERROR:
				case Z_MEM_ERROR:
				{
					(void)inflateEnd(&strm);
					return ret;
				}
			}
			have = CHUNK - strm.avail_out;
			if (sout->write_size(out, have) != have)
			{
				(void)inflateEnd(&strm);
				return Z_ERRNO;
			}
		} while (strm.avail_out == 0);

		// done when inflate() says it's done
	} while (ret != Z_STREAM_END);

	// clean up and return
	(void)inflateEnd(&strm);
	return (ret == Z_STREAM_END) ? Z_OK : Z_DATA_ERROR;
}

// report a zlib or i/o error
void zerr(int ret)
{
	fputs("zpipe: ", stderr);
	switch (ret)
	{
		case Z_ERRNO:
		{
			if (ferror(stdin))
			{
				fputs("error reading stdin\n", stderr);
			}
			if (ferror(stdout))
			{
				fputs("error writing stdout\n", stderr);
			}
			break;
		}
		case Z_STREAM_ERROR:
		{
			fputs("invalid compression level\n", stderr);
			break;
		}
		case Z_DATA_ERROR:
		{
			fputs("invalid or incomplete deflate data\n", stderr);
			break;
		}
		case Z_MEM_ERROR:
		{
			fputs("out of memory\n", stderr);
			break;
		}
		case Z_VERSION_ERROR:
		{
			fputs("zlib version mismatch!\n", stderr);
		}
	}
}

int compare_zlib(FXD::Core::StreamIn fp0, FXD::Core::StreamIn fp1)
{
	int result = 0;

	while (0 == result)
	{
		char b0[65536];
		char b1[65536];
		const size_t r0 = fp0->read_size(b0, sizeof(b0));
		const size_t r1 = fp1->read_size(b1, sizeof(b1));

		result = (int)r0 - (int)r1;

		if (0 == r0 || 0 == r1)
		{
			break;
		}
		if (0 == result)
		{
			result = memcmp(b0, b1, r0);
		}
	}

	return result;
}

int test_zlib()
{
	char* pConst = "C:\\Media\\Code\\FXD\\Data\\SampleProject\\Raw\\Common\\Textures\\BMP\\tileAbove-32.bmp";
	//char* pConst = "C:\\Media\\Code\\FXD\\Data\\SampleProject\\Raw\\Common\\Audio\\Source\\Devil_Trigger.ogg";
	char inpFilename[256] = { 0 };
	char cmpFilename[256] = { 0 };
	char decFilename[256] = { 0 };

	snprintf(inpFilename, 256, "%s", pConst);
	snprintf(cmpFilename, 256, "%s-%s", pConst, "C");
	snprintf(decFilename, 256, "%s-%s", pConst, "D");

	printf("inp = [%s]\n", inpFilename);
	printf("lz4 = [%s]\n", cmpFilename);
	printf("dec = [%s]\n", decFilename);

	// compress
	{
		FXD::Core::StreamIn inpFp = FXD::FXDIO()->io_system()->io_device()->file_open_stream(inpFilename, { FXD::IO::E_FileMode::Read, FXD::IO::E_SerilizeMode::Binary, true }).get();
		FXD::Core::StreamOut outFp = FXD::FXDIO()->io_system()->io_device()->file_open_stream(cmpFilename, { FXD::IO::E_FileMode::Write, FXD::IO::E_SerilizeMode::Binary, true }).get();

		printf("compress : %s -> %s\n", inpFilename, cmpFilename);
		test_compress_all(outFp, inpFp, Z_DEFAULT_COMPRESSION);
		printf("compress : done\n");
	}

	// decompress
	{
		FXD::Core::StreamIn inpFp = FXD::FXDIO()->io_system()->io_device()->file_open_stream(cmpFilename, { FXD::IO::E_FileMode::Read, FXD::IO::E_SerilizeMode::Binary, true }).get();
		FXD::Core::StreamOut outFp = FXD::FXDIO()->io_system()->io_device()->file_open_stream(decFilename, { FXD::IO::E_FileMode::Write, FXD::IO::E_SerilizeMode::Binary, true }).get();

		printf("decompress : %s -> %s\n", cmpFilename, decFilename);
		test_decompress(outFp, inpFp);
		printf("decompress : done\n");
	}

	// verify
	{
		FXD::Core::StreamIn inpFp = FXD::FXDIO()->io_system()->io_device()->file_open_stream(inpFilename, { FXD::IO::E_FileMode::Read, FXD::IO::E_SerilizeMode::Binary, true }).get();
		FXD::Core::StreamIn decFp = FXD::FXDIO()->io_system()->io_device()->file_open_stream(decFilename, { FXD::IO::E_FileMode::Read, FXD::IO::E_SerilizeMode::Binary, true }).get();

		printf("verify : %s <-> %s\n", inpFilename, decFilename);
		const int cmp = compare_zlib(inpFp, decFp);
		if (0 == cmp)
		{
			printf("verify : OK\n");
		}
		else
		{
			printf("verify : NO\n");
		}
	}

	return 0;
}