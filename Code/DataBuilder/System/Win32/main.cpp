#if defined(_MSC_VER) && (_MSC_VER <= 1800)  /* Visual Studio <= 2013 */
#  define _CRT_SECURE_NO_WARNINGS
#  define snprintf sprintf_s
#endif

#include "FXDEngine/IO/IOStreamFile.h"
#include "FXDEngine/Memory/StreamMemHandle.h"

#include "FXDEngine/App/FXDApi.h"
#include <FXDEngine/IO/IOJPThread.h>
#include <FXDEngine/IO/IOSystem.h>
#include <FXDEngine/IO/IODevice.h>
#include "FXDEngine/IO/DirectoryEntry.h"
#include "FXDEngine/IO/DirectoryIterator.h"
#include <3rdParty/lz4/lz4.h>
#include <3rdParty/lz4/lz4frame.h>

enum
{
	BLOCK_BYTES = 1024 * 8,
	//BLOCK_BYTES = 1024 * 64,
};

void test_compress(FXD::Core::StreamOut out, FXD::Core::StreamIn in)
{
	LZ4_stream_t lz4Stream_body;
	LZ4_initStream(&lz4Stream_body, sizeof(lz4Stream_body));

	FXD::U64 nRemainingBytes = in->length();
	FXD::U64 nBytesOffset = 0;
	FXD::U64 nWrittenBytes = 0;
	FXD::S8 nBufferIdx = 0;
	FXD::U64 nCount = 0;

	char inpBuf[2][BLOCK_BYTES];
	while (nRemainingBytes != 0)
	{
		FXD::U64 nBytesToWrite = 0;
		if (nRemainingBytes > BLOCK_BYTES)
		{
			nBytesToWrite = BLOCK_BYTES;
			nRemainingBytes -= BLOCK_BYTES;
		}
		else
		{
			nBytesToWrite = nRemainingBytes;
			nRemainingBytes -= nRemainingBytes;
		}
		char* const inpPtr = inpBuf[nBufferIdx];
		const FXD::U64 inpBytes = in->read_size(inpPtr, nBytesToWrite);
		if (0 == inpBytes)
		{
			break;
		}
		{
			char cmpBuf[LZ4_COMPRESSBOUND(BLOCK_BYTES)];
			const FXD::S64 nErr = LZ4_compress_fast_continue(&lz4Stream_body, inpPtr, cmpBuf, inpBytes, sizeof(cmpBuf), 1);
			if (nErr <= 0)
			{
				PRINT_ASSERT << "Core: LZ4 " << LZ4F_getErrorName(nErr);
			}
			else
			{
				const FXD::U64 nCompBuffSize = nErr;
				out->write_size(&nCompBuffSize, sizeof(nCompBuffSize));
				nWrittenBytes += out->write_size(&cmpBuf, nCompBuffSize);
				nCount++;
			}
		}

		nBytesOffset += nBytesToWrite;
		nBufferIdx = (nBufferIdx + 1) % 2;
	}
	//write_int(outFp, 0);
}


void test_decompress(FXD::Core::StreamOut out, FXD::Core::StreamIn in)
{
	LZ4_streamDecode_t lz4StreamDecode_body;
	LZ4_setStreamDecode(&lz4StreamDecode_body, NULL, 0);

	char decBuf[2][BLOCK_BYTES];
	FXD::S8 nBufferIdx = 0;
	FXD::U64 nBytesOffset = 0;
	FXD::U64 nDecompressedBytes = 0;
	FXD::U64 nCount = 0;

	for (;;)
	{
		char cmpBuf[LZ4_COMPRESSBOUND(BLOCK_BYTES)];
		FXD::U64 cmpBytes = 0;
		{
			const FXD::U64 readCount0 = in->read_size(&cmpBytes, sizeof(cmpBytes));
			if (cmpBytes <= 0)
			{
				break;
			}
			const FXD::U64 readCount1 = in->read_size(cmpBuf, cmpBytes);
			if (readCount1 != (size_t)cmpBytes)
			{
				break;
			}
		}
		{
			char* const decPtr = decBuf[nBufferIdx];
			const FXD::S64 nErr = LZ4_decompress_safe_continue(&lz4StreamDecode_body, cmpBuf, decPtr, cmpBytes, BLOCK_BYTES);
			if (nErr <= 0)
			{
				PRINT_ASSERT << "Core: LZ4 " << LZ4F_getErrorName(nErr);
			}
			else
			{
				const FXD::U64 nCompBuffSize = nErr;
				nDecompressedBytes += nCompBuffSize;
				nBytesOffset += nCompBuffSize;

				out->write_size(decPtr, nCompBuffSize);
				nCount++;
			}
		}

		nBufferIdx = (nBufferIdx + 1) % 2;
	}
}

int compare(FXD::Core::StreamIn fp0, FXD::Core::StreamIn fp1)
{
	int result = 0;

	while (0 == result)
	{
		char b0[65536];
		char b1[65536];
		const size_t r0 = fp0->read_size(b0, sizeof(b0));
		const size_t r1 = fp1->read_size(b1, sizeof(b1));

		result = (int)r0 - (int)r1;

		if (0 == r0 || 0 == r1)
		{
			break;
		}
		if (0 == result)
		{
			result = memcmp(b0, b1, r0);
		}
	}

	return result;
}

int test_lz4()
{
	char* pConst = "C:\\Media\\Code\\FXD\\Data\\SampleProject\\Raw\\Common\\Textures\\BMP\\tileAbove-256.bmp";
	char inpFilename[256] = { 0 };
	char lz4Filename[256] = { 0 };
	char decFilename[256] = { 0 };

	snprintf(inpFilename, 256, "%s", pConst);
	snprintf(lz4Filename, 256, "%s.lz4s-%d", pConst, BLOCK_BYTES);
	snprintf(decFilename, 256, "%s.lz4s-%d.dec", pConst, BLOCK_BYTES);

	printf("inp = [%s]\n", inpFilename);
	printf("lz4 = [%s]\n", lz4Filename);
	printf("dec = [%s]\n", decFilename);

	// compress
	{
		FXD::Core::StreamIn inpFp = FXD::FXDIO()->io_system()->io_device()->file_open_stream(inpFilename, { FXD::IO::E_FileMode::Read, FXD::IO::E_SerilizeMode::Binary, true }).get();
		FXD::Core::StreamOut outFp = FXD::FXDIO()->io_system()->io_device()->file_open_stream(lz4Filename, { FXD::IO::E_FileMode::Write, FXD::IO::E_SerilizeMode::Binary, true }).get();

		printf("compress : %s -> %s\n", inpFilename, lz4Filename);
		test_compress(outFp, inpFp);
		printf("compress : done\n");
	}

	// decompress
	{
		FXD::Core::StreamIn inpFp = FXD::FXDIO()->io_system()->io_device()->file_open_stream(lz4Filename, { FXD::IO::E_FileMode::Read, FXD::IO::E_SerilizeMode::Binary, true }).get();
		FXD::Core::StreamOut outFp = FXD::FXDIO()->io_system()->io_device()->file_open_stream(decFilename, { FXD::IO::E_FileMode::Write, FXD::IO::E_SerilizeMode::Binary, true }).get();
		
		printf("decompress : %s -> %s\n", lz4Filename, decFilename);
		test_decompress(outFp, inpFp);
		printf("decompress : done\n");
	}

	// verify
	{
		FXD::Core::StreamIn inpFp = FXD::FXDIO()->io_system()->io_device()->file_open_stream(inpFilename, { FXD::IO::E_FileMode::Read, FXD::IO::E_SerilizeMode::Binary, true }).get();
		FXD::Core::StreamIn decFp = FXD::FXDIO()->io_system()->io_device()->file_open_stream(decFilename, { FXD::IO::E_FileMode::Read, FXD::IO::E_SerilizeMode::Binary, true }).get();

		printf("verify : %s <-> %s\n", inpFilename, decFilename);
		const int cmp = compare(inpFp, decFp);
		if (0 == cmp)
		{
			printf("verify : OK\n");
		}
		else
		{
			printf("verify : NG\n");
		}
	}

	return 0;
}