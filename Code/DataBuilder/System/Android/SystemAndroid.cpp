// Creator - MatthewGolder
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Accounts/Types.h"

#if IsOSAndroid()
#include "FXDEngine/App/Android/GlobalsAndroid.h"
#include "FXDEngine/App/Platform.h"
#include <android/native_activity.h>

// ------
// 
// -
// ------
extern FXD::S32 fxd_main(FXD::S32 argc, FXD::UTF8* argv[]);
FXD::S32 main(FXD::S32 argc, FXD::UTF8* argv[])
{
	return fxd_main(argc, argv);
}

extern "C"
{
	void AndroidMain(ANativeActivity* pActivity, void* pSavedState, size_t nSavedStateSize)
	{
		main(0, 0);
	}
}

// ------
// GlobalData
// -
// ------
FXD::App::GlobalData& FXD::App::GetGlobalData(void)
{
	static FXD::App::GlobalData globalData(480, false, nullptr);
	return globalData;
}

// ------
// extern
// -
// ------
extern "C"
{
	/*
	static pthread_t g_mainThread;
	JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* jvm, void* pBReserved)
	{
		FXD::App::GlobalData().m_lockable.get_quick_write_lock()->m_pJavaVM = jvm;
		return JNI_VERSION_1_6;
	}
	JNIEXPORT void JNICALL JNI_OnUnload(JavaVM* jvm, void* pBReserved)
	{
		FXD::App::GlobalData().m_lockable.get_quick_write_lock()->m_pJavaVM = nullptr;
	}

	JNIEXPORT void JNICALL Java_com_GolderGames_SOA_FXDWrapper_nativeOnCreate(JNIEnv* pEnv, jobject thiz, FXD::F32 dpiX, FXD::F32 dpiY)
	{
		FXD::App::GlobalData().m_lockable.get_quick_write_lock()->m_fxdWrapper = pEnv->NewGlobalRef(thiz);
		//static pthread_t g_mainThread;
		//pthread_create(&g_mainThread, 0, mainThreadProc, 0);
	}
	JNIEXPORT void JNICALL Java_com_GolderGames_SOA_FXDWrapper_nativeOnStart(JNIEnv* pEnv, jobject thiz)
	{
		float f = 0;
	}
	JNIEXPORT void JNICALL Java_com_GolderGames_SOA_FXDWrapper_nativeOnResume(JNIEnv* pEnv, jobject thiz)
	{
		float f = 0;
	}
	JNIEXPORT void JNICALL Java_com_GolderGames_SOA_FXDWrapper_nativeOnPause(JNIEnv* pEnv, jobject thiz)
	{
		float f = 0;
	}
	JNIEXPORT void JNICALL Java_com_GolderGames_SOA_FXDWrapper_nativeOnStop(JNIEnv* pEnv, jobject thiz)
	{
		float f = 0;
	}
	JNIEXPORT void JNICALL Java_com_GolderGames_SOA_FXDWrapper_nativeOnRestart(JNIEnv* pEnv, jobject thiz)
	{
		float f = 0;
	}
	JNIEXPORT void JNICALL Java_com_GolderGames_SOA_FXDWrapper_nativeOnDestroy(JNIEnv* pEnv, jobject thiz)
	{
		pEnv->DeleteGlobalRef(FXD::App::GlobalData().m_lockable.get_quick_write_lock()->m_fxdWrapper);
	}
	*/
}
#endif //IsOSAndroid()