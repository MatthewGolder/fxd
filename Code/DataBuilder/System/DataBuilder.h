// Creator - MatthewGolder
#pragma once
#ifndef DATABUILDER_SYSTEM_DATABUILDER_H
#define DATABUILDER_SYSTEM_DATABUILDER_H

#include "FXDEngine/App/EngineApp.h"

namespace DataBuilder
{
	// ------
	// BuilderApp
	// -
	// ------
	class BuilderApp : public FXD::App::IEngineApp
	{
	public:
		BuilderApp(void);
		~BuilderApp(void);

		void run_game(void) FINAL;

		void load(void) FINAL;
		void unload(void) FINAL;
	};
} //namespace DataBuilder
#endif //DATABUILDER_SYSTEM_DATABUILDER_H