// Creator - MatthewGolder
#include "DataBuilder/System/DataBuilder.h"
#include "DataBuilder/Converter/CFileParser.h"
#include "DataBuilder/Converter/CProcessor.h"
#include "DataBuilder/Converter/CRecord.h"
#include "FXDEngine/Core/OS.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOStreamFile.h"
#include "FXDEngine/IO/IOSystem.h"
#include "FXDEngine/IO/IODevice.h"
#include "FXDEngine/IO/DirectoryEntry.h"
#include "FXDEngine/IO/DirectoryIterator.h"

using namespace FXD;
using namespace DataBuilder;

// ------
// BuilderApp
// -
// ------
BuilderApp::BuilderApp(void)
{
}

BuilderApp::~BuilderApp(void)
{
}

void BuilderApp::run_game(void)
{
	IEngineApp::run_game();

	Core::E_OS ePlatform = FXD::Core::OS::ToType(FXDPlatform()->cmd_args().get_value(UTF_8("platform")).c_str());
	if (ePlatform != Core::E_OS::Unknown)
	{
		IO::Path pathRaw = IO::Path(UTF_8("Raw"));
		IO::Path pathConvert = IO::Path(UTF_8("Convert")).append_create(FXD::Core::OS::ToString(ePlatform).c_str());
		IO::Path pathCompress = IO::Path(UTF_8("Compress")).append_create(FXD::Core::OS::ToString(ePlatform).c_str());
		IO::Path pathTmp = FXD::IO::FS::GetTempDirectory().append_create(FXD::Core::OS::ToString(ePlatform).c_str());

		for (auto& itr : IO::directory_iterator(pathRaw, UTF_8("*.FXD-DataConvert")))
		{
			Converter::CFileParser parser(pathRaw, pathConvert, pathCompress, pathTmp);
			{
				IO::StreamFile stream = FXDIO()->io_system()->io_device()->file_open_stream(itr.path(), { IO::E_FileMode::Read, IO::E_SerilizeMode::Binary, false }).get();
				IO::StreamParser streamParser(stream);
				IO::XMLSaxParser::parse(streamParser, parser);
			}

			fxd_for(auto& commandGroup, parser.m_commandGroups)
			{
				Converter::CRecord recordConvert;
				Converter::CRecord recordCompress;

				Converter::CProcessor cprocessor(commandGroup, ePlatform);
				cprocessor.convert(recordConvert);
				cprocessor.compress(recordCompress);
			}
			//convertManager.pack(record);
		}

		/*
		if (ePlatform == Core::E_OS::Android)
		{
			-d C : \Media\Code\FXD\Data\SampleProject\Convert\Android 

			Core::StringBuilder8 strStream;
			strStream
				<<  "jobb.bat " << " "
				<< "-k secretkey" << " "
				<< "-pn com.my.app.package" << " "
				<< "pv 11";
		*/
	}
	else
	{
		PRINT_INFO << UTF_8("Unknown platform type.");
	}
	PRINT_INFO << UTF_8("Press any key to continue.");
	getchar();
}

void BuilderApp::load(void)
{
	IEngineApp::load();
}

void BuilderApp::unload(void)
{
	IEngineApp::unload();
}