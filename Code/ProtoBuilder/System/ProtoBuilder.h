// Creator - MatthewGolder
#pragma once
#ifndef PROTOBUILDER_SYSTEM_PROTOBUILDER_H
#define PROTOBUILDER_SYSTEM_PROTOBUILDER_H

#include "FXDEngine/App/EngineApp.h"

namespace ProtoBuilder
{
	// ------
	// BuilderApp
	// -
	// ------
	class BuilderApp : public FXD::App::IEngineApp
	{
	public:
		BuilderApp(void);
		~BuilderApp(void);

		void run_game(void) FINAL;

		void load(void) FINAL;
		void unload(void) FINAL;
	};
} //namespace ProtoBuilder
#endif //PROTOBUILDER_SYSTEM_PROTOBUILDER_H