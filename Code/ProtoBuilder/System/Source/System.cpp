// Creator - MatthewGolder
#include "ProtoBuilder/System/ProtoBuilder.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/Thread/ThreadLocalStorage.h"
#include "FXDEngine/Thread/ThreadPerformance.h"

// ------
// 
// -
// ------
FXD::S32 fxd_main(FXD::S32 argc, FXD::UTF8* argv[])
{
	FXD::Thread::Funcs::SetCurrentThreadName(UTF_8("Main Thread"));
	FXD::Thread::ThreadLocalData::initialise();
	FXD::Thread::ThreadPerformance::initialise(UTF_8("Main Thread"));
	{
		ProtoBuilder::BuilderApp app;
		if (app.init_game(argc, argv, FXD::App::E_ApiFlags()))
		{
			app.run_game();
		}
		app.shutdown_game();
	}
	FXD::Thread::ThreadPerformance::shutdown();
	FXD::Thread::ThreadLocalData::shutdown();

	FXD::Memory::Log::PrintTotalStats();
	FXD::Memory::Log::DumpMemory(UTF_8("memorydump.txt"));
	return 0;
}

// ------
// GlobalData
// -
// ------
FXD::Core::String8 FXD::App::GetGameNameUTF8(void)
{
	return UTF_8("ProtoBuilder");
}

FXD::Core::String16 FXD::App::GetGameNameUTF16(void)
{
	return UTF_16("ProtoBuilder");
}

FXD::Core::String8 FXD::App::GetDisplayGameNameUTF8(void)
{
	return UTF_8("ProtoBuilder");
}

FXD::Core::String16 FXD::App::GetDisplayGameNameUTF16(void)
{
	return UTF_16("ProtoBuilder");
}

FXD::Core::String8 FXD::App::GetShortNameUTF8(void)
{
	return UTF_8("PB");
}

FXD::Core::String16 FXD::App::GetShortNameUTF16(void)
{
	return UTF_16("PB");
}