// Creator - MatthewGolder
#include "ProtoBuilder/System/ProtoBuilder.h"
#include "FXDEngine/Core/OS.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/App/Win32/FuncsWin32.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOSystem.h"
#include "FXDEngine/IO/IODevice.h"
#include "FXDEngine/IO/Path.h"
#include "FXDEngine/IO/DirectoryEntry.h"
#include "FXDEngine/IO/DirectoryIterator.h"

using namespace FXD;
using namespace ProtoBuilder;

// ------
// BuilderApp
// -
// ------
BuilderApp::BuilderApp(void)
{
}

BuilderApp::~BuilderApp(void)
{
}

void convert_file(const IO::Path& pathFilename, const IO::Path& pathSrc)
{
	if (pathFilename.extension().string().ends_with_no_case(".proto"))
	{
		IO::Path pathFull = pathSrc / pathFilename;
		PRINT_INFO << "Start ProtoBuilder:";
		PRINT_INFO << "ProtoBuilder:	Input File - " << pathFull;

		{
			Core::StringBuilder8 strStream;
			strStream
				<< "protoc " << pathFilename << " "
				<< "-I=" << pathSrc.c_str() << " "
				<< "--cpp_out=" << pathSrc.c_str();

			IO::Path pathModule = IO::FS::GetModuleDirectory();
			if (!App::FuncsWin32::runProcess(pathModule, UTF_8("protoc.exe"), strStream.str()))
			{
				return;
			}
		}
	}
}

void convert_iterate_converters(IO::Path& pathSrc)
{
	for (auto& itr : IO::directory_iterator(pathSrc))
	{
		if (itr.is_directory())
		{
			// 1. Expand directories
			IO::Path pathSrcNew = pathSrc;
			pathSrcNew.append(itr.path().filename().string(), true);

			// 2. Iterate Directories
			convert_iterate_converters(pathSrcNew);
		}
		else if (itr.is_regular_file())
		{
			convert_file(itr.path().filename(), pathSrc);
		}
	}
}

void BuilderApp::run_game(void)
{
	IEngineApp::run_game();

	IO::Path pathRoot = IO::FS::GetCWD();
	convert_iterate_converters(pathRoot);

	PRINT_INFO << UTF_8("Press any key to continue.");
	getchar();
}

void BuilderApp::load(void)
{
	IEngineApp::load();
}

void BuilderApp::unload(void)
{
	IEngineApp::unload();
}