﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace ScriptLauncher
{
    public class SysTrayApp : Form
    {
        [STAThread]
        static void Main()
        {
            Application.Run(new SysTrayApp());
        }

        private NotifyIcon m_trayIcon;
        private ContextMenu m_trayMenu;
        private BuildFile m_bf;

        public SysTrayApp()
        {
            using (var archive = System.IO.Compression.ZipFile.OpenRead("C:\\Media\\Code\\FXD\\Data\\SampleProject\\Compress\\Android\\Common\\.zip"))
            {
                foreach (var s in archive.Entries)
                {
                }
                Bitmap newImage = null;
                using (var image = new Bitmap(archive.Entries[53].Open()))
                {
                    newImage = new Bitmap(image);
                }
                float f = 0;
            }

            // Create a simple tray menu with only one item.
            m_trayMenu = new ContextMenu();

            // Create a tray icon.
            m_trayIcon = new NotifyIcon
            {
                Text = "FXDBuilder",
                Icon = ScriptLauncher.Properties.Resources.Icon,
                ContextMenu = m_trayMenu, // Add menu to tray icon and show it.
                Visible = true
            };

            String strIn = Directory.GetCurrentDirectory();
            strIn += "\\Build.xml";
            m_bf = new BuildFile();
            m_bf.ReadFileIn(strIn);

            GenerateTrayMenu();
        }

        protected override void OnLoad(EventArgs e)
        {
            this.Visible = false;       // Hide form window.
            this.ShowInTaskbar = false; // Remove from taskbar.
            base.OnLoad(e);
        }

        private void OnImport(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                // Set filter options and filter index.
                Filter = "XML Files (.xml)|*.xml|All Files (*.*)|*.*",
                FilterIndex = 1,
                Multiselect = true
            };

            // Call the ShowDialog method to show the dialog box.
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                String strIn = openFileDialog1.FileName.ToString();
                m_bf.ReadFileIn(strIn);

                GenerateTrayMenu();

                String strOut = Directory.GetCurrentDirectory();
                strOut += "\\Build.xml";
                m_bf.WriteFileOut(strOut);
            }
        }

        private void OnExit(object sender, EventArgs e)
        {
            Application.Exit();
        }

        protected override void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                m_trayIcon.Dispose();
            }
            base.Dispose(isDisposing);
        }

        private void GenerateTrayMenu()
        {
            m_trayMenu.MenuItems.Clear();

            foreach (var game in m_bf.m_games)
            {
                MenuItem gameItem = m_trayMenu.MenuItems.Cast<MenuItem>().FirstOrDefault(n => n.Text == game.Key);
                if (gameItem == null)
                {
                    gameItem = new MenuItem(game.Key);
                    m_trayMenu.MenuItems.Add(gameItem);
                }

                foreach (var command in game.Value.m_commands)
                {
                    MenuItem platformItem = gameItem.MenuItems.Cast<MenuItem>().FirstOrDefault(n => n.Text == command.m_strPlatform);
                    if (platformItem == null)
                    {
                        platformItem = new MenuItem(command.m_strPlatform);
                        gameItem.MenuItems.Add(platformItem);
                    }
                    if (platformItem.MenuItems.Cast<MenuItem>().FirstOrDefault(n => n.Text == command.m_strTitle) != null)
                    {
                        Debug.Write("");
                    }

                    MenuItem commandItem = new MenuItem(command.m_strTitle, new EventHandler((Object o, EventArgs a) =>
                    {
                        try
                        {
                            String strCommandLine = Helper.ReplaceEnviromentVariables(command.m_variables["command_line"]);
                            String strExePath = Helper.ReplaceEnviromentVariables(command.m_variables["executable_path"]);
                            String strWorkPath = Helper.ReplaceEnviromentVariables(command.m_variables["working_path"]);

                            bool bExePath = File.Exists(strExePath);
                            bool bWorkPath = Directory.Exists(strWorkPath);
                            if (!bExePath)
                            {
                                MessageBox.Show(strExePath + " - cannot be found.");
                                return;
                            }
                            if (!bWorkPath)
                            {
                                Directory.CreateDirectory(strWorkPath);
                            }

                            ProcessStartInfo startInfo = new ProcessStartInfo
                            {
                                CreateNoWindow = false,
                                UseShellExecute = false,
                                FileName = strExePath,
                                WindowStyle = ProcessWindowStyle.Hidden,
                                WorkingDirectory = strWorkPath,
                                Arguments = strCommandLine
                            };
                            Process proc = Process.Start(startInfo);
                            proc.WaitForExit();
                        }
                        catch (System.IO.IOException e)
                        {
                            MessageBox.Show(e.Message);
                        }
                    })
                    );
                    platformItem.MenuItems.Add(commandItem);
                }
            }
            m_trayMenu.MenuItems.Add("-");
            m_trayMenu.MenuItems.Add("Import", OnImport);
            m_trayMenu.MenuItems.Add("Exit", OnExit);
        }
    }
}
