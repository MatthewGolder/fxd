﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace ScriptLauncher
{
    class Helper
    {
        public static String ReplaceEnviromentVariables(String strIn)
        {
            int start = strIn.IndexOf("{");
            int end = strIn.IndexOf("}");
            while (start != -1 && end != -1)
            {
                String result1 = strIn.Substring(start + 1, end - start - 1);
                String result2 = strIn.Substring(start, end - start + 1);
                String envVal = Environment.GetEnvironmentVariable(result1);

                strIn = strIn.Replace(result2, envVal);
                start = strIn.IndexOf("{");
                end = strIn.IndexOf("}");
            }
            return strIn;
        }
    }

    // ------
    // BuildCommand
    // -
    // ------
    class BuildCommand
    {
        public String m_strTitle;
        public String m_strPlatform;
        public SortedDictionary<String, String> m_variables = new SortedDictionary<String, String>();

        public BuildCommand(String strTitle, String strPlatform)
        {
            m_strTitle = strTitle;
            m_strPlatform = strPlatform;
        }
        ~BuildCommand()
        {
        }

        public void ReadFileIn(XmlReader xmlReader)
        {
            while (xmlReader.Read())
            {
                switch (xmlReader.NodeType)
                {
                    case XmlNodeType.Element:
                    {
                        if (xmlReader.Name == "Variable")
                        {
                            String strName = xmlReader.GetAttribute("name");
                            String strValue = xmlReader.GetAttribute("value");
                            if (m_variables.ContainsKey(strName))
                            {
                                m_variables.Remove(strName);
                            }
                            m_variables.Add(strName, strValue);
                        }
                        break;
                    }
                    case XmlNodeType.EndElement:
                    {
                        if (xmlReader.Name == "Command")
                        {
                            return;
                        }
                        break;
                    }
                }
            }
        }
        public void WriteFileOut(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("Command");
            xmlWriter.WriteAttributeString("title", m_strTitle);
            xmlWriter.WriteAttributeString("platform", m_strPlatform);
            foreach (KeyValuePair<String, String> entry in m_variables)
            {
                xmlWriter.WriteStartElement("Variable");
                xmlWriter.WriteAttributeString("name", entry.Key);
                xmlWriter.WriteAttributeString("value", entry.Value);
                xmlWriter.WriteEndElement();
            }
            xmlWriter.WriteEndElement();
        }
    }

    // ------
    // BuildGame
    // -
    // ------
    class BuildGame
    {
        public String m_strGameName;
        public List<BuildCommand> m_commands = new List<BuildCommand>();

        public BuildGame(String strGameName)
        {
            m_strGameName = strGameName;
        }

        ~BuildGame()
        {
        }

        public void ReadFileIn(XmlReader xmlReader)
        {
            while (xmlReader.Read())
            {
                switch (xmlReader.NodeType)
                {
                    case XmlNodeType.Element:
                    {
                        if (xmlReader.Name == "Command")
                        {
                            String strTitle = xmlReader.GetAttribute("title");
                            String strPlatform = xmlReader.GetAttribute("platform");
                            BuildCommand bc = new BuildCommand(strTitle, strPlatform);
                            bc.ReadFileIn(xmlReader);
                            m_commands.Add(bc);
                        }
                        break;
                    }
                    case XmlNodeType.EndElement:
                    {
                        if (xmlReader.Name == "Game")
                        {
                            return;
                        }
                        break;
                    }
                }
            }
        }

        public void WriteFileOut(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("Game");
            xmlWriter.WriteAttributeString("name", m_strGameName);
            foreach (var entry in m_commands)
            {
                entry.WriteFileOut(xmlWriter);
            }
            xmlWriter.WriteEndElement();
        }
    }

    // ------
    // BuildFile
    // -
    // ------
    class BuildFile
    {
        public SortedDictionary<String, BuildGame> m_games = new SortedDictionary<String, BuildGame>();

        public BuildFile()
        {
        }

        ~BuildFile()
        {
        }

        public void ReadFileIn(String strFilePath)
        {
            if (System.IO.File.Exists(strFilePath))
            {
                XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
                XmlReader xmlReader = XmlReader.Create(strFilePath, xmlReaderSettings);
                while (xmlReader.Read())
                {
                    switch (xmlReader.NodeType)
                    {
                        case XmlNodeType.Element:
                        {
                            if (xmlReader.Name == "Game")
                            {
                                String strGameName = xmlReader.GetAttribute("name");
                                if (m_games.ContainsKey(strGameName))
                                {
                                    m_games.Remove(strGameName);
                                }
                                BuildGame bg = new BuildGame(strGameName);
                                bg.ReadFileIn(xmlReader);
                                m_games.Add(strGameName, bg);
                            }
                            break;
                        }
                    }
                }
                if (xmlReader != null)
                {
                    xmlReader.Close();
                }
            }
        }

        public void WriteFileOut(String strFilePath)
        {
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;
            xmlWriterSettings.IndentChars = "\t";
            XmlWriter xmlWriter = XmlWriter.Create(strFilePath, xmlWriterSettings);
            xmlWriter.WriteStartElement("Build");
            foreach (var entry in m_games)
            {
                entry.Value.WriteFileOut(xmlWriter);
            }
            xmlWriter.WriteEndElement();
            if (xmlWriter != null)
            {
                xmlWriter.Close();
            }
        }
    }
}
