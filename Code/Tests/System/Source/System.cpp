// Creator - MatthewGolder
#include "Tests/System/Tests.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/Thread/ThreadLocalStorage.h"
#include "FXDEngine/Thread/ThreadPerformance.h"
#include "Tests/System/Tests/FXDTest.h"

// ------
// 
// -
// ------
FXD::S32 fxd_main(FXD::S32 argc, FXD::UTF8* argv[])
{
	/*
	TestFunctional();
	TestTuple();

	TestTypeTraits();
	TestMemory();
	TestAlgorithm();
	TestAny();
	TestArray();
	TestBitset();
	TestDeque();
	TestIterator();
	TestList();
	TestMap();
	TestNumericLimits();
	TestPair();
	TestSet();
	TestString();
	TestStringView();
	TestUtility();
	TestVector();
	*/
	FXD::Thread::Funcs::SetCurrentThreadName(UTF_8("Main Thread"));
	FXD::Thread::ThreadLocalData::initialise();
	FXD::Thread::ThreadPerformance::initialise(UTF_8("Main Thread"));

	{
		Game::FXDTests game;
		if (game.init_game(argc, argv, FXD::App::E_Api::Accounts | FXD::App::E_Api::Input | FXD::App::E_Api::SND | FXD::App::E_Api::GFX))
		{
			game.run_game();
		}
		game.shutdown_game();
	}

	FXD::Thread::ThreadPerformance::shutdown();
	FXD::Thread::ThreadLocalData::shutdown();

	FXD::Memory::Log::PrintTotalStats();
	FXD::Memory::Log::DumpMemory(UTF_8("memorydump.txt"));
	return 0;
}

// ------
// FXDGlobals
// -
// ------
FXD::Core::String8 FXD::App::GetGameNameUTF8(void)
{
	return UTF_8("FXDTests");
}

FXD::Core::String16 FXD::App::GetGameNameUTF16(void)
{
	return UTF_16("FXDTests");
}

FXD::Core::String8 FXD::App::GetDisplayGameNameUTF8(void)
{
	return UTF_8("FXDTests");
}

FXD::Core::String16 FXD::App::GetDisplayGameNameUTF16(void)
{
	return UTF_16("FXDTests");
}

FXD::Core::String8 FXD::App::GetShortNameUTF8(void)
{
	return UTF_8("FXDTests");
}

FXD::Core::String16 FXD::App::GetShortNameUTF16(void)
{
	return UTF_16("FXDTests");
}