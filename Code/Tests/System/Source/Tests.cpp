// Creator - MatthewGolder
#include "Tests/System/Tests.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Thread/ThreadPerformance.h"

using namespace Game;
using namespace FXD;

// ------
// FXDTests
// -
// ------
FXDTests::FXDTests(void)
{
}

FXDTests::~FXDTests(void)
{
}

void FXDTests::run_game(void)
{
	IEngineApp::run_game();

	// 1. Start the game timer 
	//m_timer.start_counter();
	FXD::U32 nCounter = 0;
	while (m_eGameState != App::E_GameState::Stopped)
	{
		nCounter++;
		if (nCounter > 100)
		{
			nCounter = 0;
		}
		if (nCounter < 50)
		{
			m_clearColour = FXD::Core::ColourRGBA::Blue;
		}
		if (nCounter >= 50)
		{
			m_clearColour = FXD::Core::ColourRGBA::Red;
		}
		Thread::ThreadPerformance::process_thread_counters();
	}
}

void FXDTests::load(void)
{
	IEngineApp::load();
}

void FXDTests::unload(void)
{
	m_appCancel.cancel();
	IEngineApp::unload();
}