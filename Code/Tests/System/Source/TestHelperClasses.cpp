// Creator - MatthewGolder
#include "Tests/System/TestHelperClasses.h"

using namespace Game;

///////////////////////////////////////////////////////////////////////////////
// MallocAllocator
///////////////////////////////////////////////////////////////////////////////
FXD::S32 MallocAllocator::m_nAllocCountAll = 0;
FXD::S32 MallocAllocator::m_nFreeCountAll = 0;
FXD::S32 MallocAllocator::m_nAllocVolumeAll = 0;
void* MallocAllocator::m_pLastAllocation = nullptr;

MallocAllocator::MallocAllocator(const FXD::UTF8* /*pStr*/)
	: m_nAllocCount(0)
	, m_nFreeCount(0)
	, m_nAllocVolume(0)
{}

MallocAllocator::MallocAllocator(const MallocAllocator& rhs)
	: m_nAllocCount(rhs.m_nAllocCount)
	, m_nFreeCount(rhs.m_nFreeCount)
	, m_nAllocVolume(rhs.m_nAllocVolume)
{}

MallocAllocator::MallocAllocator(const MallocAllocator&, const FXD::UTF8* /*pStr*/)
{}

MallocAllocator& MallocAllocator::operator=(const MallocAllocator& rhs)
{
	m_nAllocCount = rhs.m_nAllocCount;
	m_nFreeCount = rhs.m_nFreeCount;
	m_nAllocVolume = rhs.m_nAllocVolume;
	return (*this);
}

void MallocAllocator::deallocate(void* pMem, size_t nSize)
{
	m_nFreeCount++;
	m_nAllocVolume -= nSize;
	m_nFreeCountAll++;
	m_nAllocVolumeAll -= nSize;

	return free(pMem);
}

void* MallocAllocator::allocate(size_t nSize, FXD::S32 nFlags)
{
	m_nAllocCount++;
	m_nAllocVolume += nSize;
	m_nAllocCountAll++;
	m_nAllocVolumeAll += nSize;
	m_pLastAllocation = malloc(nSize);
	return m_pLastAllocation;
}

const FXD::UTF8* MallocAllocator::get_name(void) const
{
	return "MallocAllocator";
}

void MallocAllocator::set_name(const FXD::UTF8* /*pStr*/)
{
}

void MallocAllocator::reset_all(void)
{
	m_nAllocCountAll = 0;
	m_nFreeCountAll = 0;
	m_nAllocVolumeAll = 0;
	m_pLastAllocation = nullptr;
}