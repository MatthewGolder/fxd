// Creator - MatthewGolder
#ifndef TESTS_SYSTEM_TESTS_H
#define TESTS_SYSTEM_TESTS_H

#include "FXDEngine/App/EngineApp.h"

namespace Game
{
	// ------
	// FXDTests
	// -
	// ------
	class FXDTests : public FXD::App::IEngineApp
	{
	public:
		FXDTests(void);
		~FXDTests(void);

		void run_game(void) FINAL;

		void load(void) FINAL;
		void unload(void) FINAL;

	private:
		FXD::Job::AsyncCancel m_appCancel;
		FXD::Core::ColourRGBA m_clearColour = FXD::Core::ColourRGBA::Blue;
	};

} //namespace Game
#endif //TESTS_SYSTEM_TESTS_H