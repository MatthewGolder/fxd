// Creator - MatthewGolder
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Accounts/Types.h"

#if IsOSPC() && IsAccountWindows()
#include "FXDEngine/App/Win32/GlobalsWin32.h"
#include "FXDEngine/App/Platform.h"

extern FXD::S32 fxd_main(FXD::S32 argc, FXD::UTF8* argv[]);

FXD::S32 main(FXD::S32 argc, FXD::UTF8* argv[])
{
	return fxd_main(argc, argv);
}

// ------
// GlobalData
// -
// ------
FXD::App::GlobalData& FXD::App::GetGlobalData(void)
{
	static FXD::App::GlobalData globalData(false);
	return globalData;
}
#endif //IsOSPC() && IsAccountWindows()