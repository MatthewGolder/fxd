// Creator - MatthewGolder
#include "Tests/System/Tests/FXDTest.h"
#include "Tests/System/TestHelperClasses.h"
#include "FXDEngine/Container/Deque.h"
#include "FXDEngine/Container/LinkedList.h"

#include <deque>
#include <list>

namespace
{
	// _DequeObject
	class _DequeObject
	{
	public:
		_DequeObject(FXD::S32 nX = 0)
			: m_nX(nX)
			, m_nVal(kMagicValue)
		{
			++sDOCount;
		}

		_DequeObject(const _DequeObject& rhs)
			: m_nX(rhs.m_nX)
			, m_nVal(kMagicValue)
		{
			++sDOCount;
		}

		_DequeObject& operator=(const _DequeObject& rhs)
		{
			m_nX = rhs.m_nX;
			return (*this);
		}

		~_DequeObject(void)
		{
			if (m_nVal != kMagicValue)
			{
				++sMagicErrorCount;
			}
			m_nVal = 0;
			--sDOCount;
		}

	public:
		FXD::S32 m_nX;								// Value for the _DequeObject.
		FXD::U32 m_nVal;							// 
		static FXD::S32 sDOCount;				// Count of all current existing DequeObjects.
		static FXD::S32 sMagicErrorCount;	// Number of magic number mismatch errors.
	};

	FXD::S32 _DequeObject::sDOCount = 0;
	FXD::S32 _DequeObject::sMagicErrorCount = 0;

	bool operator==(const _DequeObject& lhs, const _DequeObject& rhs)
	{
		return (lhs.m_nX == rhs.m_nX);
	}

	bool operator<(const _DequeObject& lhs, const _DequeObject& rhs)
	{
		return (lhs.m_nX < rhs.m_nX);
	}
}


// Template instantations.
// These tell the compiler to compile all the functions for the given class.
template class FXD::Container::Deque< FXD::S32 >;
template class FXD::Container::Deque< _DequeObject >;

///////////////////////////////////////////////////////////////////////////////
using FXDIntDeque = FXD::Container::Deque< FXD::S32 >;
using FXDDODeque = FXD::Container::Deque< _DequeObject >;

using SIntDeque = std::deque< FXD::S32 >;
using SDODeque = std::deque< _DequeObject >;
///////////////////////////////////////////////////////////////////////////////

template < typename D1, typename D2 >
FXD::S32 CompareDeques(const D1& d1, const D2& d2, const FXD::UTF8* pTestName)
{
	FXD::S32 nErrorCount = 0;
	const size_t nSize1 = d1.size();
	const size_t nSize2 = d2.size();

	// bool operator==(const Deque< T, Allocator, kDequeSubarraySize >& lhs, const Deque< T, Allocator, kDequeSubarraySize >& rhs)
	{
		PRINT_COND_ASSERT((d1.empty() == d2.empty()), "Deque - bool operator==(const Deque< T, Allocator, kDequeSubarraySize >& lhs, const Deque< T, Allocator, kDequeSubarraySize >& rhs)");
		PRINT_COND_ASSERT((nSize1 == nSize2), "Deque - bool operator==(const Deque< T, Allocator, kDequeSubarraySize >& lhs, const Deque< T, Allocator, kDequeSubarraySize >& rhs)");
		if (nSize1 != nSize2)
		{
			PRINT_WARN << pTestName << ": Deque size difference: " << nSize1 << " " << nSize2;
		}
	}

	if (nSize1 == nSize2)
	{
		// reference operator[](size_type nID)
		{
			for (FXD::U32 n1 = 0; n1 < nSize1; n1++)
			{
				const typename D1::value_type& t1 = d1[n1];
				const typename D2::value_type& t2 = d2[n1];

				PRINT_COND_ASSERT((t1 == t2), "Deque - reference operator[](size_type nID)");
				if (!(t1 == t2))
				{
					PRINT_WARN << pTestName << ": Deque index difference at index " << n1;
					break;
				}
			}
		}

		// iterator begin(void) NOEXCEPT
		// iterator end(void) NOEXCEPT
		{
			typename D1::const_iterator itr1 = d1.begin();
			typename D2::const_iterator itr2 = d2.begin();
			for (FXD::U32 n1 = 0; itr1 != d1.end(); ++itr1, ++itr2, ++n1)
			{
				const typename D1::value_type& t1 = *itr1;
				const typename D2::value_type& t2 = *itr2;

				PRINT_COND_ASSERT((t1 == t2), "Deque - iterator end(void) NOEXCEPT");
				if (!(t1 == t2))
				{
					PRINT_WARN << pTestName << ": Deque iterator difference at index: " << n1;
					break;
				}
			}
		}

		// reverse_iterator rbegin(void) NOEXCEPT
		// reverse_iterator rend(void) NOEXCEPT
		{
			typename D1::const_reverse_iterator itrRev1 = d1.rbegin();
			typename D2::const_reverse_iterator itrRev2 = d2.rbegin();
			for (typename D1::size_type n1 = d1.size() - 1; itrRev1 != d1.rend(); ++itrRev1, ++itrRev2, --n1)
			{
				const typename D1::value_type& t1 = *itrRev1;
				const typename D2::value_type& t2 = *itrRev2;
				PRINT_COND_ASSERT((t1 == t2), "CompareDeques: Failed a test");
				if (!(t1 == t2))
				{
					PRINT_WARN << pTestName << ": Deque reverse iterator difference at index: " << n1;
					break;
				}
			}
		}
	}

	return nErrorCount;
}

// TestDequeConstruction
template < typename D1, typename D2 >
FXD::S32 TestDequeConstruction(void)
{
	FXD::S32 nErrorCount = 0;

	{
		D1 d1A;
		D2 d2A;
		nErrorCount += CompareDeques(d1A, d2A, "Deque ctor");

		D1 d1B((typename D1::size_type)0);
		D2 d2B((typename D2::size_type)0);
		nErrorCount += CompareDeques(d1B, d2B, "Deque ctor");

		D1 d1C(1000);
		D2 d2C(1000);
		nErrorCount += CompareDeques(d1C, d2C, "Deque ctor");

		D1 d1D(2000, 1);
		D2 d2D(2000, 1);
		nErrorCount += CompareDeques(d1D, d2D, "Deque ctor");

		D1 d1E(d1C);
		D2 d2E(d2C);
		nErrorCount += CompareDeques(d1E, d2E, "Deque ctor");

		D1 d1F(d1C.begin(), d1C.end());
		D2 d2F(d2C.begin(), d2C.end());
		nErrorCount += CompareDeques(d1F, d2F, "Deque ctor");

		d1E = d1D;
		d2E = d2D;
		nErrorCount += CompareDeques(d1D, d2D, "Deque operator=");
		nErrorCount += CompareDeques(d1E, d2E, "Deque operator=");

		d1E.swap(d1D);
		d2E.swap(d2D);
		nErrorCount += CompareDeques(d1D, d2D, "Deque swap");
		nErrorCount += CompareDeques(d1E, d2E, "Deque swap");

		d1A.clear();
		d2A.clear();
		nErrorCount += CompareDeques(d1A, d2A, "Deque clear");

		d1B.clear();
		d2B.clear();
		nErrorCount += CompareDeques(d1B, d2B, "Deque clear");
	}

	PRINT_COND_ASSERT((_DequeObject::sDOCount == 0), "Deque - Failed");
	PRINT_COND_ASSERT((_DequeObject::sMagicErrorCount == 0), "Deque - Failed");

	return nErrorCount;
}

// TestDequeSimpleMutation
template < typename D1, typename D2 >
FXD::S32 TestDequeSimpleMutation(void)
{
	FXD::S32 nErrorCount = 0;
	{
		D1 d1;
		D2 d2;

		// void push_back(const value_type& val)
		// reference front(void)
		// reference back(void)
		{
			for (FXD::S32 n1 = 0; n1 < 1000; n1++)
			{
				d1.push_back(n1);
				d2.push_back(n1);

				PRINT_COND_ASSERT(d1.front() == d2.front(), "Deque - reference front(void)");
				PRINT_COND_ASSERT(d1.back() == d2.back(), "Deque - reference front(void)");
			}
			nErrorCount += CompareDeques(d1, d2, "Deque - void push_back(const value_type& val)");
		}

		// reference operator[](size_type nID)
		// reference at(size_type nID)
		{
			for (typename D1::size_type n1 = 0, iEnd = d1.size(); n1 < iEnd; n1++)
			{
				PRINT_COND_ASSERT(d1[(FXD::U32)n1] == d2[(FXD::U32)n1], "Deque - reference operator[](size_type nID)");
				PRINT_COND_ASSERT(d1.at((FXD::U32)n1) == d2.at((FXD::U32)n1), "Deque - reference at(size_type nID)");
			}
		}

		// reference push_back(void)
		{
			for (FXD::S32 n1 = 0; n1 < 1000; n1++)
			{
				d1.push_back(FXD::S32());
				typename D2::value_type& ref = d2.push_back();
				PRINT_COND_ASSERT(d1.front() == d2.front(), "Deque - reference push_back(void)");
				PRINT_COND_ASSERT(d1.back() == d2.back(), "Deque - reference push_back(void)");
				PRINT_COND_ASSERT(&ref == &d2.back(), "Deque - reference push_back(void)");
			}
			nErrorCount += CompareDeques(d1, d2, "Deque - reference push_back(void)");
		}

		// reference operator[](size_type nID)
		// reference at(size_type nID)
		{
			for (typename D1::size_type n1 = 0, iEnd = d1.size(); n1 < iEnd; n1++)
			{
				PRINT_COND_ASSERT(d1[(FXD::U32)n1] == d2[(FXD::U32)n1], "Deque - reference operator[](size_type nID)");
				PRINT_COND_ASSERT(d1.at((FXD::U32)n1) == d2.at((FXD::U32)n1), "Deque - reference at(size_type nID)");
			}
		}

		// void push_front(const value_type& val)
		{
			for (FXD::S32 n1 = 0; n1 < 1000; n1++)
			{
				d1.push_front(n1);
				d2.push_front(n1);
				PRINT_COND_ASSERT(d1.front() == d2.front(), "Deque - void push_front(const value_type& val)");
				PRINT_COND_ASSERT(d1.back() == d2.back(), "Deque - void push_front(const value_type& val)");
			}
			nErrorCount += CompareDeques(d1, d2, "Deque - void push_front(const value_type& val)");
		}

		// reference operator[](size_type nID)
		// reference at(size_type nID)
		{
			for (typename D1::size_type n1 = 0, iEnd = d1.size(); n1 < iEnd; n1++)
			{
				PRINT_COND_ASSERT(d1[(FXD::U32)n1] == d2[(FXD::U32)n1], "Deque - reference operator[](size_type nID)");
				PRINT_COND_ASSERT(d1.at((FXD::U32)n1) == d2.at((FXD::U32)n1), "Deque - reference at(size_type nID)");
			}
		}

		// reference push_front(void)
		{
			for (FXD::S32 n1 = 0; n1 < 1000; n1++)
			{
				d1.push_front(FXD::S32());
				typename D2::value_type& ref = d2.push_front();
				PRINT_COND_ASSERT(d1.front() == d2.front(), "Deque - reference push_front(void)");
				PRINT_COND_ASSERT(d1.back() == d2.back(), "Deque - reference push_front(void)");
				PRINT_COND_ASSERT(&ref == &d2.front(), "Deque - reference push_front(void)");
			}
			nErrorCount += CompareDeques(d1, d2, "Deque - reference push_front(void)");
		}

		// reference operator[](size_type nID)
		// reference at(size_type nID)
		{
			for (typename D1::size_type n1 = 0, iEnd = d1.size(); n1 < iEnd; n1++)
			{
				PRINT_COND_ASSERT(d1[(FXD::U32)n1] == d2[(FXD::U32)n1], "Deque - reference operator[](size_type nID)");
				PRINT_COND_ASSERT(d1.at((FXD::U32)n1) == d2.at((FXD::U32)n1), "Deque - reference at(size_type nID)");
			}
		}

		// void pop_back(void)
		{
			for (FXD::S32 n1 = 0; n1 < 500; n1++)
			{
				d1.pop_back();
				d2.pop_back();
				PRINT_COND_ASSERT(d1.front() == d2.front(), "Deque - void pop_back(void)");
				PRINT_COND_ASSERT(d1.back() == d2.back(), "Deque - void pop_back(void)");
			}
			nErrorCount += CompareDeques(d1, d2, "Deque - void pop_back(void)");
		}

		// reference operator[](size_type nID)
		// reference at(size_type nID)
		{
			for (typename D1::size_type n1 = 0, iEnd = d1.size(); n1 < iEnd; n1++)
			{
				PRINT_COND_ASSERT(d1[(FXD::U32)n1] == d2[(FXD::U32)n1], "Deque - reference operator[](size_type nID)");
				PRINT_COND_ASSERT(d1.at((FXD::U32)n1) == d2.at((FXD::U32)n1), "Deque - reference at(size_type nID)");
			}
		}

		// void pop_front(void)
		{
			for (FXD::S32 n1 = 0; n1 < 500; n1++)
			{
				d1.pop_front();
				d2.pop_front();
				PRINT_COND_ASSERT(d1.front() == d2.front(), "Deque - void pop_front(void))");
				PRINT_COND_ASSERT(d1.back() == d2.back(), "Deque - void pop_front(void))");
			}
			nErrorCount += CompareDeques(d1, d2, "Deque - void pop_front(void))");
		}

		// reference operator[](size_type nID)
		// reference at(size_type nID)
		{
			for (typename D1::size_type n1 = 0, iEnd = d1.size(); n1 < iEnd; n1++)
			{
				PRINT_COND_ASSERT(d1[(FXD::U32)n1] == d2[(FXD::U32)n1], "Deque - reference operator[](size_type nID)");
				PRINT_COND_ASSERT(d1.at((FXD::U32)n1) == d2.at((FXD::U32)n1), "Deque - reference at(size_type nID)");
			}
		}

		// void resize(size_type nSize, const value_type& val)
		{
			for (FXD::S32 n1 = 0; n1 < 500; n1++)
			{
				d1.resize(d1.size() + 3, n1);
				d2.resize(d2.size() + 3, n1);

				PRINT_COND_ASSERT(d1.front() == d2.front(), "Deque - void resize(size_type nSize, const value_type& val)");
				PRINT_COND_ASSERT(d1.back() == d2.back(), "Deque - void resize(size_type nSize, const value_type& val)");
			}
			nErrorCount += CompareDeques(d1, d2, "Deque - void resize(size_type nSize, const value_type& val)");
		}

		// reference operator[](size_type nID)
		// reference at(size_type nID)
		{
			for (typename D1::size_type n1 = 0, iEnd = d1.size(); n1 < iEnd; n1++)
			{
				PRINT_COND_ASSERT(d1[(FXD::U32)n1] == d2[(FXD::U32)n1], "Deque - reference operator[](size_type nID)");
				PRINT_COND_ASSERT(d1.at((FXD::U32)n1) == d2.at((FXD::U32)n1), "Deque - reference at(size_type nID)");
			}
		}

		// void resize(size_type nSize)
		{
			for (FXD::S32 n1 = 0; n1 < 500; n1++)
			{
				typename D1::size_type t1 = d1.size();
				typename D1::size_type t2 = d2.size();

				d1.resize(d1.size() - 2);
				d2.resize(d2.size() - 2);

				t1 = d1.size();
				t2 = d2.size();

				PRINT_COND_ASSERT(d1.front() == d2.front(), "void resize(size_type nSize)");
				PRINT_COND_ASSERT(d1.back() == d2.back(), "void resize(size_type nSize)");
			}
			nErrorCount += CompareDeques(d1, d2, "void resize(size_type nSize)");
		}

		// reference operator[](size_type nID)
		// reference at(size_type nID)
		{
			for (typename D1::size_type n1 = 0, iEnd = d1.size(); n1 < iEnd; n1++)
			{
				PRINT_COND_ASSERT(d1[(FXD::U32)n1] == d2[(FXD::U32)n1], "Deque - reference operator[](size_type nID)");
				PRINT_COND_ASSERT(d1.at((FXD::U32)n1) == d2.at((FXD::U32)n1), "Deque - reference at(size_type nID)");
			}
		}
	}

	PRINT_COND_ASSERT(_DequeObject::sDOCount == 0, "Deque - Failed");
	PRINT_COND_ASSERT(_DequeObject::sMagicErrorCount == 0, "Deque - Failed");

	return nErrorCount;
}

// TestDequeComplexMutation
template < typename D1, typename D2 >
FXD::S32 TestDequeComplexMutation(void)
{
	FXD::S32 nErrorCount = 0;

	{
		D1 d1;
		D2 d2;
		std::list< FXD::S32 > listIntSTD;
		FXD::Container::LinkedList< FXD::S32 > listIntFXD;

		// void assign(size_type nCount, const value_type& val)
		{
			d1.assign(100, 1);
			d2.assign(100, 1);
			nErrorCount += CompareDeques(d1, d2, "Deque - void assign(size_type nCount, const value_type& val)");

			d1.assign(50, 2);
			d2.assign(50, 2);
			nErrorCount += CompareDeques(d1, d2, "Deque - void assign(size_type nCount, const value_type& val)");

			d1.assign(150, 2);
			d2.assign(150, 2);
			nErrorCount += CompareDeques(d1, d2, "Deque - void assign(size_type nCount, const value_type& val)");
		}

		// void assign(InputItr first, InputItr last)
		{
			for (FXD::S32 n1 = 0; n1 < 100; n1++)
			{
				listIntSTD.push_back(n1);
			}

			for (FXD::S32 n1 = 0; n1 < 100; n1++)
			{
				listIntFXD.push_back(n1);
			}

			d1.assign(listIntSTD.begin(), listIntSTD.end());
			d2.assign(listIntFXD.begin(), listIntFXD.end());
			nErrorCount += CompareDeques(d1, d2, "Deque - void assign(InputItr first, InputItr last)");
		}

		// iterator insert(const_iterator pos, const value_type& val);
		{
			d1.insert(d1.begin(), d1[1]);
			d2.insert(d2.begin(), d2[1]);
			nErrorCount += CompareDeques(d1, d2, "Deque - iterator insert(const_iterator pos, const value_type& val)");

			d1.insert(d1.end(), d1[d1.size() - 2]);
			d2.insert(d2.end(), d2[d2.size() - 2]);
			nErrorCount += CompareDeques(d1, d2, "Deque - iterator insert(const_iterator pos, const value_type& val)");

			typename D1::iterator itrNearBeginSTD = d1.begin();
			typename D2::iterator itrNearBeginFXD = d2.begin();

			std::advance(itrNearBeginSTD, 1);
			FXD::STD::advance(itrNearBeginFXD, 1);

			d1.insert(itrNearBeginSTD, d1[3]);
			d2.insert(itrNearBeginFXD, d2[3]);
			nErrorCount += CompareDeques(d1, d2, "Deque - iterator insert(const_iterator pos, const value_type& val)");

			typename D1::iterator itD1NearEnd = d1.begin();
			typename D2::iterator itD2NearEnd = d2.begin();

			std::advance(itD1NearEnd, d1.size() - 1);
			FXD::STD::advance(itD2NearEnd, d2.size() - 1);

			d1.insert(itD1NearEnd, d1[d1.size() - 2]);
			d2.insert(itD2NearEnd, d2[d2.size() - 2]);
			nErrorCount += CompareDeques(d1, d2, "Deque - iterator insert(const_iterator pos, const value_type& val)");

			// void insert(const_iterator pos, size_type nCount, const value_type& val);
			{
				d1.insert(d1.begin(), d1.size() * 2, 3); // Insert a large number of items at the front.
				d2.insert(d2.begin(), d2.size() * 2, 3);
				nErrorCount += CompareDeques(d1, d2, "Deque - void insert(const_iterator pos, size_type nCount, const value_type& val)");

				d1.insert(d1.end(), d1.size() * 2, 3); // Insert a large number of items at the end.
				d2.insert(d2.end(), d2.size() * 2, 3);
				nErrorCount += CompareDeques(d1, d2, "Deque - void insert(const_iterator pos, size_type nCount, const value_type& val)");

				itrNearBeginSTD = d1.begin();
				itrNearBeginFXD = d2.begin();

				std::advance(itrNearBeginSTD, 3);
				FXD::STD::advance(itrNearBeginFXD, 3);

				d1.insert(itrNearBeginSTD, 3, 4);
				d2.insert(itrNearBeginFXD, 3, 4);
				nErrorCount += CompareDeques(d1, d2, "Deque - void insert(const_iterator pos, size_type nCount, const value_type& val)");

				itD1NearEnd = d1.begin();
				itD2NearEnd = d2.begin();

				std::advance(itD1NearEnd, d1.size() - 1);
				FXD::STD::advance(itD2NearEnd, d2.size() - 1);

				d1.insert(d1.end(), 5, 6);
				d2.insert(d2.end(), 5, 6);
				nErrorCount += CompareDeques(d1, d2, "Deque - void insert(const_iterator pos, size_type nCount, const value_type& val)");
			}

			// void insert(const_iterator pos, InputItr first, InputItr last)
			{
				itrNearBeginSTD = d1.begin();
				itrNearBeginFXD = d2.begin();

				std::advance(itrNearBeginSTD, 3);
				FXD::STD::advance(itrNearBeginFXD, 3);

				d1.insert(itrNearBeginSTD, listIntSTD.begin(), listIntSTD.end());
				d2.insert(itrNearBeginFXD, listIntFXD.begin(), listIntFXD.end());
				nErrorCount += CompareDeques(d1, d2, "Deque - void insert(const_iterator pos, InputItr first, InputItr last)");
			}

			// iterator erase(const_iterator pos);
			{
				itrNearBeginSTD = d1.begin();
				itrNearBeginFXD = d2.begin();

				while (itrNearBeginSTD != d1.end()) // Run a loop whereby we erase every third element.
				{
					for (FXD::S32 n1 = 0; (n1 < 3) && (itrNearBeginSTD != d1.end()); ++n1)
					{
						++itrNearBeginSTD;
						++itrNearBeginFXD;
					}

					if (itrNearBeginSTD != d1.end())
					{
						itrNearBeginSTD = d1.erase(itrNearBeginSTD);
						itrNearBeginFXD = d2.erase(itrNearBeginFXD);
						nErrorCount += CompareDeques(d1, d2, "Deque - iterator erase(const_iterator pos)");
					}
				}
			}

			// iterator erase(const_iterator first, const_iterator last);
			{
				itrNearBeginSTD = d1.begin();
				itrNearBeginFXD = d2.begin();

				while (itrNearBeginSTD != d1.end()) // Run a loop whereby we erase spans of elements.
				{
					typename D1::iterator itD1Saved = itrNearBeginSTD;
					typename D2::iterator itD2Saved = itrNearBeginFXD;

					for (FXD::S32 n1 = 0; (n1 < 11) && (itrNearBeginSTD != d1.end()); ++n1)
					{
						++itrNearBeginSTD;
						++itrNearBeginFXD;
					}

					if (itrNearBeginSTD != d1.end())
					{
						itrNearBeginSTD = d1.erase(itD1Saved, itrNearBeginSTD);
						itrNearBeginFXD = d2.erase(itD2Saved, itrNearBeginFXD);
						nErrorCount += CompareDeques(d1, d2, "Deque - iterator erase(const_iterator first, const_iterator last)");
					}

					for (FXD::S32 n1 = 0; (n1 < 17) && (itrNearBeginSTD != d1.end()); ++n1)
					{
						++itrNearBeginSTD;
						++itrNearBeginFXD;
					}
				}
			}
		}

		// reverse_iterator erase(reverse_iterator pos);
		// reverse_iterator erase(reverse_iterator first, reverse_iterator last);
		{
			D2 d1Erase;

			for (FXD::S32 n1 = 0; n1 < 20; n1++)
			{
				typename D2::value_type val(n1);
				d1Erase.push_back(val);
			}
			PRINT_COND_ASSERT((d1Erase.size() == 20) && (d1Erase[0] == 0) && (d1Erase[19] == 19), "Deque - Failed");

			typename D2::reverse_iterator r2A = d1Erase.rbegin();
			typename D2::reverse_iterator r2B = r2A + 3;
			d1Erase.erase(r2A, r2B);
			PRINT_COND_ASSERT((d1Erase.size() == 17), "Deque - iterator erase(reverse_iterator first, reverse_iterator last)");
			PRINT_COND_ASSERT((d1Erase[0] == 0), "Deque - iterator erase(reverse_iterator first, reverse_iterator last)");
			PRINT_COND_ASSERT((d1Erase[16] == 16), "Deque - iterator erase(reverse_iterator first, reverse_iterator last)");


			r2B = d1Erase.rend();
			r2A = r2B - 3;
			d1Erase.erase(r2A, r2B);
			PRINT_COND_ASSERT((d1Erase.size() == 14), "Deque - iterator erase(reverse_iterator first, reverse_iterator last)");
			PRINT_COND_ASSERT((d1Erase[0] == 3), "Deque - iterator erase(reverse_iterator first, reverse_iterator last)");
			PRINT_COND_ASSERT((d1Erase[13] == 16), "Deque - iterator erase(reverse_iterator first, reverse_iterator last)");


			r2B = d1Erase.rend() - 1;
			d1Erase.erase(r2B);
			PRINT_COND_ASSERT((d1Erase.size() == 13), "Deque - iterator erase(reverse_iterator pos)");
			PRINT_COND_ASSERT((d1Erase[0] == 4), "Deque - iterator erase(reverse_iterator pos)");
			PRINT_COND_ASSERT((d1Erase[12] == 16), "Deque - iterator erase(reverse_iterator pos)");


			r2B = d1Erase.rbegin();
			d1Erase.erase(r2B);
			PRINT_COND_ASSERT((d1Erase.size() == 12), "Deque - iterator erase(reverse_iterator pos)");
			PRINT_COND_ASSERT((d1Erase[0] == 4), "Deque - iterator erase(reverse_iterator pos)");
			PRINT_COND_ASSERT((d1Erase[11] == 15), "Deque - iterator erase(reverse_iterator pos)");


			r2A = d1Erase.rbegin();
			r2B = d1Erase.rend();
			d1Erase.erase(r2A, r2B);
			PRINT_COND_ASSERT(d1Erase.size() == 0, "Deque - iterator erase(reverse_iterator first, reverse_iterator last)");
		}
	}

	PRINT_COND_ASSERT(_DequeObject::sDOCount == 0, "Deque - Failed");
	PRINT_COND_ASSERT(_DequeObject::sMagicErrorCount == 0, "Deque - Failed");

	return nErrorCount;
}

FXD::S32 TestDeque(void)
{
	FXD::S32 nErrorCount = 0;
	// Test construction
	{
		nErrorCount += TestDequeConstruction< SIntDeque, FXDIntDeque >();
		nErrorCount += TestDequeConstruction< SDODeque, FXDDODeque >();
	}

	// Test simple mutating functionality.
	{
		nErrorCount += TestDequeSimpleMutation< SIntDeque, FXDIntDeque >();
		nErrorCount += TestDequeSimpleMutation< SDODeque, FXDDODeque >();
	}

	// Test complex mutating functionality.
	{
		nErrorCount += TestDequeComplexMutation< SIntDeque, FXDIntDeque >();
		nErrorCount += TestDequeComplexMutation< SDODeque, FXDDODeque >();
	}

	// reference emplace_back(Args&&... args)
	// reference emplace_back(Args&&... args)
	{
		{
			FXD::Container::Deque< Game::MoveAssignable > dequeMove;
			dequeMove.emplace_back(Game::MoveAssignable::Create());
			dequeMove.emplace_front(Game::MoveAssignable::Create());

			auto cd = std::move(dequeMove);
			PRINT_COND_ASSERT(dequeMove.size() == 0, "Deque - reference emplace_back(Args&&... args)");
			PRINT_COND_ASSERT(cd.size() == 2, "Deque - reference emplace_back(Args&&... args)");
		}

		// simply test the basic api of deque with a move-only type
		{
			FXD::Container::Deque< Game::MoveAssignable > dequeMove;

			// emplace_back
			dequeMove.emplace_back(Game::MoveAssignable::Create());
			dequeMove.emplace_back(Game::MoveAssignable::Create());
			dequeMove.emplace_back(Game::MoveAssignable::Create());

			// iterator erase(const_iterator pos)
			dequeMove.erase(dequeMove.begin());
			PRINT_COND_ASSERT(dequeMove.size() == 2, "Deque - iterator erase(const_iterator pos)");

			// reference operator[](size_type nID)
			// reference at(size_type nID)
			// reference front(void)
			// reference back(void)
			PRINT_COND_ASSERT(dequeMove[0].m_nVal == 42, "Deque - reference operator[](size_type nID)");
			PRINT_COND_ASSERT(dequeMove.at(0).m_nVal == 42, "Deque - reference at(size_type nID)");
			PRINT_COND_ASSERT(dequeMove.front().m_nVal == 42, "Deque - reference front(void)");
			PRINT_COND_ASSERT(dequeMove.back().m_nVal == 42, "Deque - reference back(void)");

			// void clear(void)
			dequeMove.clear();
			PRINT_COND_ASSERT(dequeMove.size() == 0, "Deque - void clear(void)");

			// iterator emplace(const_iterator pos, Args&&... args)
			dequeMove.emplace(dequeMove.begin(), Game::MoveAssignable::Create());
			dequeMove.emplace(dequeMove.begin(), Game::MoveAssignable::Create());
			PRINT_COND_ASSERT(dequeMove.size() == 2, "Deque - iterator emplace(const_iterator pos, Args&&... args)");

			// void pop_back(void)
			dequeMove.pop_back();
			PRINT_COND_ASSERT(dequeMove.size() == 1, "Deque - void pop_back(void)");

			// reference emplace_front(Args&&... args)
			{
				FXD::Container::Deque< Game::MoveAssignable > dequeSwap;

				dequeSwap.emplace_front(Game::MoveAssignable::Create());
				dequeSwap.emplace_front(Game::MoveAssignable::Create());
				dequeSwap.emplace_front(Game::MoveAssignable::Create());

				// swap
				dequeSwap.swap(dequeMove);
				PRINT_COND_ASSERT(dequeSwap.size() == 1, "Deque - reference emplace_front(Args&&... args)");
				PRINT_COND_ASSERT(dequeMove.size() == 3, "Deque - reference emplace_front(Args&&... args)");
			}

			// void pop_front(void)
			// iterator insert(const_iterator pos, value_type&& val)
			{
				dequeMove.pop_front();
				PRINT_COND_ASSERT(dequeMove.size() == 2, "Deque - void pop_front(void)");

				dequeMove.insert(dequeMove.end(), Game::MoveAssignable::Create());
				PRINT_COND_ASSERT(dequeMove.size() == 3, "Deque - iterator insert(const_iterator pos, value_type&& val)");
			}
		}
	}

	// Deque(std::initializer_list< value_type > iList, const allocator_type& alloc = allocator_type())
	// my_type& operator=(std::initializer_list< value_type > iList)
	// void assign(std::initializer_list< value_type > iList)
	// iterator insert(const_iterator pos, std::initializer_list< value_type > iList)
	{
		FXD::Container::Deque< FXD::S32 > dequeInt = { 0, 1, 2 };
		PRINT_COND_ASSERT(VerifySequence(dequeInt.begin(), dequeInt.end(), FXD::S32(), "deque std::initializer_list", 0, 1, 2, -1), "Deque - Deque(std::initializer_list< value_type > iList, const allocator_type& alloc = allocator_type())");

		dequeInt = { 13, 14, 15 };
		PRINT_COND_ASSERT(VerifySequence(dequeInt.begin(), dequeInt.end(), FXD::S32(), "deque std::initializer_list", 13, 14, 15, -1), "Deque - my_type& operator=(std::initializer_list< value_type > iList)");

		dequeInt.assign({ 16, 17, 18 });
		PRINT_COND_ASSERT(VerifySequence(dequeInt.begin(), dequeInt.end(), FXD::S32(), "deque std::initializer_list", 16, 17, 18, -1), "Deque - void assign(std::initializer_list< value_type > iList)");

		FXD::Container::Deque< FXD::S32 >::iterator itr = dequeInt.insert(dequeInt.begin(), { 14, 15 });
		PRINT_COND_ASSERT(VerifySequence(dequeInt.begin(), dequeInt.end(), FXD::S32(), "deque std::initializer_list", 14, 15, 16, 17, 18, -1), "Deque - iterator insert(const_iterator pos, std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT(*itr == 14, "Deque - Failed");
	}

	// C++11 functionality

	// Deque(my_type&& rhs);
	// Deque(my_type&& rhs, const allocator_type& allocator);
	// my_type& operator=(my_type&& rhs);
	{
		FXD::Container::Deque< TestObject > deque3TO33(3, TestObject(33));
		FXD::Container::Deque< TestObject > dequeTOA(std::move(deque3TO33));
		PRINT_COND_ASSERT((dequeTOA.size() == 3) && (dequeTOA.front().m_nX == 33) && (deque3TO33.size() == 0), "Deque - Deque(my_type&& rhs)");

		// The following is not as strong a test of this ctor as it could be. A stronger test would be to use IntanceAllocator with different instances.
		FXD::Container::Deque< TestObject, Game::MallocAllocator > deque4TO44(4, TestObject(44));
		FXD::Container::Deque< TestObject, Game::MallocAllocator > toDequeB(std::move(deque4TO44), Game::MallocAllocator());
		PRINT_COND_ASSERT((toDequeB.size() == 4) && (toDequeB.front().m_nX == 44) && (deque4TO44.size() == 0), "Deque(my_type&& rhs, const allocator_type& allocator)");

		FXD::Container::Deque< TestObject, Game::MallocAllocator > deque5TO55(5, TestObject(55));
		toDequeB = std::move(deque5TO55);
		PRINT_COND_ASSERT((toDequeB.size() == 5) && (toDequeB.front().m_nX == 55) && (deque5TO55.size() == 0), "Deque - my_type& operator=(my_type&& rhs)");
	}

	// C++11 functionality
	{
		// iterator emplace(const_iterator pos, Args&&... args);
		// void emplace_front(Args&&... args);
		// void emplace_back(Args&&... args);
		{
			TestObject::reset();

			FXD::Container::Deque< TestObject, FXD::Memory::Alloc::AlignedType< FXD::Memory::Alloc::Heap, TestObject >, 16 > dequeTOA;

			dequeTOA.emplace_back(2, 3, 4);
			PRINT_COND_ASSERT((dequeTOA.size() == 1) && (dequeTOA.back().m_nX == (2 + 3 + 4)) && (TestObject::sTOCtorCount == 1), "void emplace_front(Args&&... args)");

			dequeTOA.emplace(dequeTOA.begin(), 3, 4, 5);
			PRINT_COND_ASSERT((dequeTOA.size() == 2) && (dequeTOA.front().m_nX == (3 + 4 + 5)) && (TestObject::sTOCtorCount == 3), "iterator emplace(const_iterator pos, Args&&... args)");

			dequeTOA.emplace_front(6, 7, 8);
			PRINT_COND_ASSERT((dequeTOA.size() == 3) && (dequeTOA.front().m_nX == (6 + 7 + 8)) && (TestObject::sTOCtorCount == 4), "void emplace_front(Args&&... args)");
		}

		// void push_front(value_type&& val);
		// void push_back(value_type&& val);
		// iterator insert(const_iterator pos, value_type&& val);
		{
			TestObject::reset();

			FXD::Container::Deque< TestObject, FXD::Memory::Alloc::AlignedType< FXD::Memory::Alloc::Heap, TestObject >, 16 > dequeTOA;

			dequeTOA.push_back(TestObject(2, 3, 4));
			PRINT_COND_ASSERT((dequeTOA.size() == 1) && (dequeTOA.back().m_nX == (2 + 3 + 4)) && (TestObject::sTOMoveCtorCount == 1), "Deque - void push_front(value_type&& val)");

			dequeTOA.insert(dequeTOA.begin(), TestObject(3, 4, 5));
			PRINT_COND_ASSERT((dequeTOA.size() == 2) && (dequeTOA.front().m_nX == (3 + 4 + 5)) && (TestObject::sTOMoveCtorCount == 3), "Deque - void push_back(value_type&& val)");

			dequeTOA.push_front(TestObject(6, 7, 8));
			PRINT_COND_ASSERT((dequeTOA.size() == 3) && (dequeTOA.front().m_nX == (6 + 7 + 8)) && (TestObject::sTOMoveCtorCount == 4), "Deque - iterator insert(const_iterator pos, value_type&& val)");
		}
	}

	// Regression of kDequeSubarraySize calculations
	{
		PRINT_COND_ASSERT(FXDIntDeque::kSubarraySize >= 4, "Deque - Failed");
		PRINT_COND_ASSERT(FXDDODeque::kSubarraySize >= 2, "Deque - Failed");
	}

	return nErrorCount;
}