// Creator - MatthewGolder

template < typename StringViewT >
FXD::S32 TEST_STRING_NAME(void)
{
	FXD::S32 nErrorCount = 0;
	{
		// CONSTEXPR StringViewBase(void) 
		{
			StringViewT sw;
			PRINT_COND_ASSERT(sw.empty(), "StringViewBase - CONSTEXPR StringViewBase(void)");
			PRINT_COND_ASSERT(sw.data() == nullptr, "StringViewBase - CONSTEXPR StringViewBase(void)");
			PRINT_COND_ASSERT(sw.size() == 0, "StringViewBase - CONSTEXPR StringViewBase(void)");
			PRINT_COND_ASSERT(sw.size() == sw.length(), "StringViewBase - CONSTEXPR StringViewBase(void)");
		}

		// CONSTEXPR StringViewBase(const my_type& rhs);
		{
			auto* pLiteral = LITERAL("Hello, World");
			StringViewT sw1(pLiteral);
			StringViewT sw2(sw1);
			PRINT_COND_ASSERT(sw1.size() == sw2.size(), "StringViewBase - CONSTEXPR StringViewBase(const my_type& rhs)");
			PRINT_COND_ASSERT(FXD::Memory::MemCompare(sw1.data(), sw2.data(), sw1.size()) == 0, "StringViewBase - CONSTEXPR StringViewBase(const my_type& rhs)");
		}

		// CONSTEXPR StringViewBase(const_pointer pStr, size_type nCount)
		{
			{
				StringViewT sw(LITERAL("Hello, World"), 12);
				PRINT_COND_ASSERT(!sw.empty(), "StringViewBase - CONSTEXPR StringViewBase(const_pointer pStr, size_type nCount)");
				PRINT_COND_ASSERT(sw.data() != nullptr, "StringViewBase - CONSTEXPR StringViewBase(const_pointer pStr, size_type nCount)");
				PRINT_COND_ASSERT(sw.size() == 12, "StringViewBase - CONSTEXPR StringViewBase(const_pointer pStr, size_type nCount)");
				PRINT_COND_ASSERT(sw.size() == sw.length(), "StringViewBase - CONSTEXPR StringViewBase(const_pointer pStr, size_type nCount)");
			}

			{
				StringViewT sw(LITERAL("Hello, World"), 5);
				PRINT_COND_ASSERT(!sw.empty(), "StringViewBase - CONSTEXPR StringViewBase(const_pointer pStr, size_type nCount)");
				PRINT_COND_ASSERT(sw.data() != nullptr, "StringViewBase - CONSTEXPR StringViewBase(const_pointer pStr, size_type nCount)");
				PRINT_COND_ASSERT(sw.size() == 5, "StringViewBase - CONSTEXPR StringViewBase(const_pointer pStr, size_type nCount)");
				PRINT_COND_ASSERT(sw.size() == sw.length(), "StringViewBase - CONSTEXPR StringViewBase(const_pointer pStr, size_type nCount)");
				PRINT_COND_ASSERT(FXD::Memory::MemCompare(sw.data(), LITERAL("Hello"), sw.size()) == 0, "StringViewBase - CONSTEXPR StringViewBase(const_pointer pStr, size_type nCount)");
			}
		}

		// CONSTEXPR StringViewBase(const_pointer pStr)
		{
			auto* pLiteral = LITERAL("England, London");
			StringViewT sw(pLiteral);
			PRINT_COND_ASSERT(!sw.empty(), "StringViewBase - CONSTEXPR StringViewBase(const_pointer pStr)");
			PRINT_COND_ASSERT(sw.data() != nullptr, "StringViewBase - CONSTEXPR StringViewBase(const_pointer pStr)");
			PRINT_COND_ASSERT(sw.size() == 15, "StringViewBase - CONSTEXPR StringViewBase(const_pointer pStr)");
			PRINT_COND_ASSERT(sw.size() == sw.length(), "CONSTEXPR StringViewBase(const_pointer pStr)");
			PRINT_COND_ASSERT(FXD::Memory::MemCompare(sw.data(), pLiteral, sw.size()) == 0, "StringViewBase - CONSTEXPR StringViewBase(const_pointer pStr)");
		}

		// my_type& operator=(const my_type& rhs);
		{
			auto* pLiteral = LITERAL("Hello, World");
			StringViewT sw1(pLiteral);
			StringViewT sw2;
			PRINT_COND_ASSERT(!sw1.empty(), "StringViewBase - my_type& operator=(const my_type& rhs)");
			PRINT_COND_ASSERT( sw2.empty(), "StringViewBase - my_type& operator=(const my_type& rhs)");

			sw2 = sw1;

			PRINT_COND_ASSERT(!sw1.empty(), "StringViewBase - my_type& operator=(const my_type& rhs)");
			PRINT_COND_ASSERT(!sw2.empty(), "StringViewBase - my_type& operator=(const my_type& rhs)");
			PRINT_COND_ASSERT(sw1.size() == sw2.size(), "StringViewBase - my_type& operator=(const my_type& rhs)");
			PRINT_COND_ASSERT(FXD::Memory::MemCompare(sw1.data(), pLiteral, sw1.size()) == 0, "StringViewBase - my_type& operator=(const my_type& rhs)");
			PRINT_COND_ASSERT(FXD::Memory::MemCompare(sw2.data(), pLiteral, sw2.size()) == 0, "StringViewBase - my_type& operator=(const my_type& rhs)");
		}

		{
			// CONSTEXPR const_iterator begin(void) const NOEXCEPT
			// CONSTEXPR const_iterator cbegin(void) const NOEXCEPT
			StringViewT sw(LITERAL("abcdefg"));
			{
				auto itr = sw.begin();

				PRINT_COND_ASSERT(*itr++ == LITERAL('a'), "StringViewBase - CONSTEXPR const_iterator begin(void) const NOEXCEPT");
				PRINT_COND_ASSERT(*itr++ == LITERAL('b'), "StringViewBase - CONSTEXPR const_iterator begin(void) const NOEXCEPT");

				auto citr = sw.cbegin();
				PRINT_COND_ASSERT(*citr++ == LITERAL('a'), "StringViewBase - CONSTEXPR const_iterator cbegin(void) const NOEXCEPT");
				PRINT_COND_ASSERT(*citr++ == LITERAL('b'), "StringViewBase - CONSTEXPR const_iterator cbegin(void) const NOEXCEPT");
			}

			// CONSTEXPR const_iterator end(void) const NOEXCEPT
			// CONSTEXPR const_iterator cend(void) const NOEXCEPT
			{
				auto itr = sw.end();
				PRINT_COND_ASSERT(*itr-- == LITERAL('\0'), "StringViewBase - CONSTEXPR const_iterator end(void) const NOEXCEPT");
				PRINT_COND_ASSERT(*itr-- == LITERAL('g'), "StringViewBase - CONSTEXPR const_iterator end(void) const NOEXCEPTT");

				auto citr = sw.cend();
				PRINT_COND_ASSERT(*citr-- == LITERAL('\0'), "StringViewBase - CONSTEXPR const_iterator cend(void) const NOEXCEPTT");
				PRINT_COND_ASSERT(*citr-- == LITERAL('g'), "StringViewBase - CONSTEXPR const_iterator cend(void) const NOEXCEPTT");
			}

			// CONSTEXPR const_reverse_iterator rbegin(void) const NOEXCEPT
			// CONSTEXPR const_reverse_iterator crbegin(void) const NOEXCEPT
			{
				auto itr = sw.rbegin();

				PRINT_COND_ASSERT(*itr++ == LITERAL('g'), "StringViewBase - CONSTEXPR const_reverse_iterator rbegin(void) const NOEXCEPT");
				PRINT_COND_ASSERT(*itr++ == LITERAL('f'), "StringViewBase - CONSTEXPR const_reverse_iterator rbegin(void) const NOEXCEPT");

				auto citr = sw.crbegin();
				PRINT_COND_ASSERT(*citr++ == LITERAL('g'), "StringViewBase - CONSTEXPR const_reverse_iterator crbegin(void) const NOEXCEPT");
				PRINT_COND_ASSERT(*citr++ == LITERAL('f'), "StringViewBase - CONSTEXPR const_reverse_iterator crbegin(void) const NOEXCEPT");
			}

			// CONSTEXPR const_reverse_iterator rend(void) const NOEXCEPT
			// CONSTEXPR const_reverse_iterator crend(void) const NOEXCEPT
			{
				auto itr = sw.rend();
				itr--;
				PRINT_COND_ASSERT(*itr-- == LITERAL('a'), "StringViewBase - CONSTEXPR const_reverse_iterator rend(void) const NOEXCEPT");
				PRINT_COND_ASSERT(*itr-- == LITERAL('b'), "StringViewBase - CONSTEXPR const_reverse_iterator rend(void) const NOEXCEPT");


				auto citr = sw.crend();
				citr--;
				PRINT_COND_ASSERT(*citr-- == LITERAL('a'), "StringViewBase - CONSTEXPR const_reverse_iterator crend(void) const NOEXCEPT");
				PRINT_COND_ASSERT(*citr-- == LITERAL('b'), "StringViewBase - CONSTEXPR const_reverse_iterator crend(void) const NOEXCEPT");
			}
		}

		// CONSTEXPR const_pointer data(void) const
		{
			auto* pLiteral = LITERAL("England, London");
			StringViewT sw(pLiteral);
			PRINT_COND_ASSERT(sw.data() != nullptr, "");
			PRINT_COND_ASSERT(FXD::Memory::MemCompare(sw.data(), pLiteral, sw.size()) == 0, "StringViewBase - CONSTEXPR const_pointer data(void) const");
			PRINT_COND_ASSERT(FXD::Memory::MemCompare(sw.data() + 9, LITERAL("London"), 6) == 0, "StringViewBase - CONSTEXPR const_pointer data(void) const");
		}

		// CONSTEXPR const_reference front(void) const
		// CONSTEXPR const_reference back(void) const
		{
			{
				StringViewT sw(LITERAL("England, London"));
				PRINT_COND_ASSERT(sw.front() == LITERAL('E'), "StringViewBase - CONSTEXPR const_reference front(void) const");
				PRINT_COND_ASSERT(sw.back() == LITERAL('n'), "StringViewBase - CONSTEXPR const_reference back(void) const");

			}
			{
				StringViewT sw(LITERAL("London"));
				PRINT_COND_ASSERT(sw.front() == LITERAL('L'), "StringViewBase - CONSTEXPR const_reference front(void) const");
				PRINT_COND_ASSERT(sw.back() == LITERAL('n'), "StringViewBase - CONSTEXPR const_reference back(void) const");
			}
		}

		// CONSTEXPR const_reference operator[](size_type nOffSet) const
		{
			StringViewT sw(LITERAL("London"));
			PRINT_COND_ASSERT(sw[0] == LITERAL('L'), "StringViewBase - CONSTEXPR const_reference operator[](size_type nOffSet) const");
			PRINT_COND_ASSERT(sw[1] == LITERAL('o'), "StringViewBase - CONSTEXPR const_reference operator[](size_type nOffSet) const");
			PRINT_COND_ASSERT(sw[2] == LITERAL('n'), "StringViewBase - CONSTEXPR const_reference operator[](size_type nOffSet) const");
			PRINT_COND_ASSERT(sw[3] == LITERAL('d'), "StringViewBase - CONSTEXPR const_reference operator[](size_type nOffSet) const");
			PRINT_COND_ASSERT(sw[4] == LITERAL('o'), "StringViewBase - CONSTEXPR const_reference operator[](size_type nOffSet) const");
			PRINT_COND_ASSERT(sw[5] == LITERAL('n'), "StringViewBase - CONSTEXPR const_reference operator[](size_type nOffSet) const");
		}

		// CONSTEXPR size_type size(void const NOEXCEPT 
		// CONSTEXPR size_type length(void) const NOEXCEPT
		// CONSTEXPR size_type max_size(void) const NOEXCEPT
		// CONSTEXPR bool empty(void) const NOEXCEPT 
		{
			StringViewT sw(LITERAL("http://en.cppreference.com/w/cpp/header/string_view"));
			PRINT_COND_ASSERT(sw.size() == 51, "StringViewBase - CONSTEXPR size_type size(void const NOEXCEPT");
			PRINT_COND_ASSERT(sw.length() == 51, "StringViewBase - CONSTEXPR size_type length(void) const NOEXCEPT");
			PRINT_COND_ASSERT(sw.max_size() == FXD::Math::NumericLimits< typename StringViewT::size_type >::max(), "StringViewBase - size_type max_size(void) const NOEXCEPT");
			PRINT_COND_ASSERT(!sw.empty(), "StringViewBase - CONSTEXPR bool empty(void) const NOEXCEPT");
		}

		// CONSTEXPR void swap(my_type& rhs)
		{
			auto* pV = LITERAL("England");
			auto* pC = LITERAL("London");
			StringViewT sw1(pV);
			StringViewT sw2(pC);
			sw1.swap(sw2);
			PRINT_COND_ASSERT(FXD::Memory::MemCompare(sw1.data(), pC, sw1.size()) == 0, "StringViewBase - CONSTEXPR void swap(my_type& rhs)");
			PRINT_COND_ASSERT(FXD::Memory::MemCompare(sw2.data(), pV, sw2.size()) == 0, "StringViewBase - CONSTEXPR void swap(my_type& rhs)");
		}

		// CONSTEXPR void remove_prefix(size_type nCount)
		// CONSTEXPR void remove_suffix(size_type nCount)
		{
			StringViewT sw(LITERAL("London"));
			sw.remove_prefix(2);
			PRINT_COND_ASSERT(FXD::Memory::MemCompare(sw.data(), LITERAL("ndon"), sw.size()) == 0, "StringViewBase - CONSTEXPR void remove_prefix(size_type nCount)");
			PRINT_COND_ASSERT(sw.size() == 4, "StringViewBase - CONSTEXPR void remove_prefix(size_type nCount)");

			sw.remove_prefix(2);
			PRINT_COND_ASSERT(FXD::Memory::MemCompare(sw.data(), LITERAL("on"), sw.size()) == 0, "StringViewBase - CONSTEXPR void remove_prefix(size_type nCount)");
			PRINT_COND_ASSERT(sw.size() == 2, "StringViewBase - CONSTEXPR void remove_prefix(size_type nCount)");

			sw.remove_suffix(1);
			PRINT_COND_ASSERT(FXD::Memory::MemCompare(sw.data(), LITERAL("o"), sw.size()) == 0, "StringViewBase - CONSTEXPR void remove_prefix(size_type nCount)");
			PRINT_COND_ASSERT(sw.size() == 1, "StringViewBase - CONSTEXPR void remove_suffix(size_type nCount)");

			sw.remove_suffix(1);
			PRINT_COND_ASSERT(FXD::Memory::MemCompare(sw.data(), LITERAL(""), sw.size()) == 0, "StringViewBase - CONSTEXPR void remove_prefix(size_type nCount)");
			PRINT_COND_ASSERT(sw.size() == 0, "StringViewBase - CONSTEXPR void remove_suffix(size_type nCount)");
		}

		// size_type copy(pointer pStr, size_type nCount, size_type nOffSet = 0) const;
		{
			typename StringViewT::value_type buf[256];
			StringViewT sw(LITERAL("**Hello, World"));
			auto cnt = sw.copy(buf, 5, 2);
			PRINT_COND_ASSERT(FXD::Memory::MemCompare(buf, LITERAL("Hello"), 5) == 0, "StringViewBase - size_type copy(pointer pStr, size_type nCount, size_type nOffSet = 0) const");
			PRINT_COND_ASSERT(cnt == 5, "StringViewBase - size_type copy(pointer pStr, size_type nCount, size_type nOffSet = 0) const");
		}

		// CONSTEXPR StringViewBase substr(size_type nOffSet = 0, size_type nCount = npos) const;
		{
			StringViewT sw(LITERAL("**Hello, World"));
			auto sw2 = sw.substr(2, 5);
			PRINT_COND_ASSERT(FXD::Memory::MemCompare(sw2.data(), LITERAL("Hello"), sw2.size()) == 0, "StringViewBase - CONSTEXPR StringViewBase substr(size_type nOffSet = 0, size_type nCount = npos) const");
		}

		// CONSTEXPR14 FXD::S32 compare(my_type rhs) const NOEXCEPT;
		{
			{
				PRINT_COND_ASSERT(StringViewT(LITERAL("A")).compare(StringViewT(LITERAL("A"))) == 0, "StringViewBase - CONSTEXPR14 FXD::S32 compare(my_type rhs) const NOEXCEPT");
				PRINT_COND_ASSERT(StringViewT(LITERAL("a")).compare(StringViewT(LITERAL("a"))) == 0, "StringViewBase - CONSTEXPR14 FXD::S32 compare(my_type rhs) const NOEXCEPT");
				PRINT_COND_ASSERT(StringViewT(LITERAL("A")).compare(StringViewT(LITERAL("a"))) != 0, "StringViewBase - CONSTEXPR14 FXD::S32 compare(my_type rhs) const NOEXCEPT");
				PRINT_COND_ASSERT(StringViewT(LITERAL("A")).compare(StringViewT(LITERAL("a"))) < 0, "StringViewBase - CONSTEXPR14 FXD::S32 compare(my_type rhs) const NOEXCEPT");
				PRINT_COND_ASSERT(StringViewT(LITERAL("A")).compare(StringViewT(LITERAL("A"))) <= 0, "StringViewBase - CONSTEXPR14 FXD::S32 compare(my_type rhs) const NOEXCEPT");
				PRINT_COND_ASSERT(StringViewT(LITERAL("a")).compare(StringViewT(LITERAL("A"))) > 0, "StringViewBase - CONSTEXPR14 FXD::S32 compare(my_type rhs) const NOEXCEPT");
				PRINT_COND_ASSERT(StringViewT(LITERAL("A")).compare(StringViewT(LITERAL("A"))) >= 0, "StringViewBase - CONSTEXPR14 FXD::S32 compare(my_type rhs) const NOEXCEPT");
			}

			{
				PRINT_COND_ASSERT(StringViewT(LITERAL("Aa")).compare(StringViewT(LITERAL("A"))) > 0, "StringViewBase - CONSTEXPR14 FXD::S32 compare(my_type rhs) const NOEXCEPT");
				PRINT_COND_ASSERT(StringViewT(LITERAL("A")).compare(StringViewT(LITERAL("Aa"))) < 0, "StringViewBase - CONSTEXPR14 FXD::S32 compare(my_type rhs) const NOEXCEPT");
			}

			{
				StringViewT sw1(LITERAL("Hello, World"));
				StringViewT sw2(LITERAL("Hello, WWorld"));
				StringViewT sw3(LITERAL("Hello, Wzorld"));
				PRINT_COND_ASSERT(sw1.compare(sw1) == 0, "StringViewBase - CONSTEXPR14 FXD::S32 compare(my_type rhs) const NOEXCEPT");
				PRINT_COND_ASSERT(sw1.compare(sw2) > 0, "StringViewBase - CONSTEXPR14 FXD::S32 compare(my_type rhs) const NOEXCEPT");
				PRINT_COND_ASSERT(sw1.compare(sw3) < 0, "StringViewBase - CONSTEXPR14 FXD::S32 compare(my_type rhs) const NOEXCEPT");
			}
		}

		// CONSTEXPR FXD::S32 compare(size_type nOffSet, size_type nLength, my_type str) const;
		{
			StringViewT sw1(LITERAL("*** Hello ***"));
			StringViewT sw2(LITERAL("Hello"));
			PRINT_COND_ASSERT(sw1.compare(4, 5, sw2) == 0, "StringViewBase - CONSTEXPR FXD::S32 compare(size_type nOffSet, size_type nLength, my_type str) const");
		}

		// CONSTEXPR FXD::S32 compare(size_type nOffSet, size_type nLength, my_type str, size_type nSubpos, size_type nSublen) const;
		{
			StringViewT sw(LITERAL("London"));
			PRINT_COND_ASSERT(sw.compare(0, 2, StringViewT(LITERAL("Lo")), 0, 2) == 0, "StringViewBase - CONSTEXPR FXD::S32 compare(size_type nOffSet, size_type nLength, my_type str, size_type nSubpos, size_type nSublen) const");
			PRINT_COND_ASSERT(sw.compare(4, 2, StringViewT(LITERAL("on")), 0, 2) == 0, "StringViewBase - CONSTEXPR FXD::S32 compare(size_type nOffSet, size_type nLength, my_type str, size_type nSubpos, size_type nSublen) const");
			PRINT_COND_ASSERT(sw.compare(0, 3, StringViewT(LITERAL("Ta")), 0, 2) != 0, "StringViewBase - CONSTEXPR FXD::S32 compare(size_type nOffSet, size_type nLength, my_type str, size_type nSubpos, size_type nSublen) const");
		}

		// CONSTEXPR FXD::S32 compare(const_pointer pStr) const;
		{
			StringViewT sw(LITERAL("Hello"));
			PRINT_COND_ASSERT(sw.compare(LITERAL("London")) != 0, "StringViewBase - CONSTEXPR FXD::S32 compare(const_pointer pStr) const");
			PRINT_COND_ASSERT(sw.compare(LITERAL("London!")) != 0, "StringViewBase - CONSTEXPR FXD::S32 compare(const_pointer pStr) const");
			PRINT_COND_ASSERT(sw.compare(LITERAL("Hello")) == 0, "StringViewBase - CONSTEXPR FXD::S32 compare(const_pointer pStr) const");
		}

		// CONSTEXPR FXD::S32 compare(size_type nOffSet, size_type nLength, const_pointer pStr) const;
		{
			StringViewT sw(LITERAL("*** Hello"));
			PRINT_COND_ASSERT(sw.compare(4, 5, LITERAL("Hello")) == 0, "StringViewBase - CONSTEXPR FXD::S32 compare(size_type nOffSet, size_type nLength, const_pointer pStr) const");
			PRINT_COND_ASSERT(sw.compare(4, 5, LITERAL("Hello 555")) != 0, "StringViewBase - CONSTEXPR FXD::S32 compare(size_type nOffSet, size_type nLength, const_pointer pStr) const");
			PRINT_COND_ASSERT(sw.compare(4, 5, LITERAL("hello")) != 0, "StringViewBase - CONSTEXPR FXD::S32 compare(size_type nOffSet, size_type nLength, const_pointer pStr) const");
		}

		// CONSTEXPR FXD::S32 compare(size_type nOffSet, size_type nLength, const_pointer pPtr, size_type nCount) const;
		{
			StringViewT sw(LITERAL("*** Hello ***"));
			PRINT_COND_ASSERT(sw.compare(4, 5, LITERAL("Hello"), 5) == 0, "StringViewBase - CONSTEXPR FXD::S32 compare(size_type nOffSet, size_type nLength, const_pointer pPtr, size_type nCount) const");
			PRINT_COND_ASSERT(sw.compare(0, 1, LITERAL("*"), 1) == 0, "StringViewBase - CONSTEXPR FXD::S32 compare(size_type nOffSet, size_type nLength, const_pointer pPtr, size_type nCount) const");
			PRINT_COND_ASSERT(sw.compare(0, 2, LITERAL("**"), 1) != 0, "StringViewBase - CONSTEXPR FXD::S32 compare(size_type nOffSet, size_type nLength, const_pointer pPtr, size_type nCount) const");
			PRINT_COND_ASSERT(sw.compare(0, 2, LITERAL("**"), 2) == 0, "StringViewBase - CONSTEXPR FXD::S32 compare(size_type nOffSet, size_type nLength, const_pointer pPtr, size_type nCount) const");
			PRINT_COND_ASSERT(sw.compare(0, 2, LITERAL("^^"), 2) != 0, "StringViewBase - CONSTEXPR FXD::S32 compare(size_type nOffSet, size_type nLength, const_pointer pPtr, size_type nCount) const");
		}

		// CONSTEXPR size_type find(my_type str, size_type nOffSet = 0) const NOEXCEPT;
		{
			StringViewT sw(LITERAL("*** Hello ***"));
			PRINT_COND_ASSERT(sw.find(StringViewT(LITERAL("Hello"))) != StringViewT::npos, "StringViewBase - CONSTEXPR size_type find(my_type str, size_type nOffSet = 0) const NOEXCEPT");
			PRINT_COND_ASSERT(sw.find(StringViewT(LITERAL("ell"))) != StringViewT::npos, "StringViewBase - CONSTEXPR size_type find(my_type str, size_type nOffSet = 0) const NOEXCEPT");
			PRINT_COND_ASSERT(sw.find(StringViewT(LITERAL("FailToFindMe"))) == StringViewT::npos, "StringViewBase - CONSTEXPR size_type find(my_type str, size_type nOffSet = 0) const NOEXCEPT");
		}

		// CONSTEXPR14 size_type find(charT ch, size_type nOffSet = 0) const NOEXCEPT;
		{
			StringViewT sw(LITERAL("*** Hello ***"));
			PRINT_COND_ASSERT(sw.find(LITERAL("H")) == 4, "StringViewBase - CONSTEXPR14 size_type find(charT ch, size_type nOffSet = 0) const NOEXCEPT");
			PRINT_COND_ASSERT(sw.find(LITERAL("e")) == 5, "StringViewBase - CONSTEXPR14 size_type find(charT ch, size_type nOffSet = 0) const NOEXCEPT");
			PRINT_COND_ASSERT(sw.find(LITERAL("l")) == 6, "StringViewBase - CONSTEXPR14 size_type find(charT ch, size_type nOffSet = 0) const NOEXCEPT");
			PRINT_COND_ASSERT(sw.find(LITERAL("o")) == 8, "StringViewBase - CONSTEXPR14 size_type find(charT ch, size_type nOffSet = 0) const NOEXCEPT");
			PRINT_COND_ASSERT(sw.find(LITERAL("&")) == StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type find(charT ch, size_type nOffSet = 0) const NOEXCEPT");
			PRINT_COND_ASSERT(sw.find(LITERAL("@")) == StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type find(charT ch, size_type nOffSet = 0) const NOEXCEPT");
		}

		// CONSTEXPR14 size_type find(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT;
		{
			StringViewT sw(LITERAL("Hello, London"));
			PRINT_COND_ASSERT(sw.find(LITERAL("Hello"), 0, 3) != StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type find(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT");
			PRINT_COND_ASSERT(sw.find(LITERAL("Hello"), 3, 3) == StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type find(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT");
			PRINT_COND_ASSERT(sw.find(LITERAL("Londo"), 7, 5) != StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type find(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT");
		}

		// CONSTEXPR14 size_type find(const_pointer pStr, size_type nOffSet = 0) const;
		{
			StringViewT sw(LITERAL("Hello, London"));
			PRINT_COND_ASSERT(sw.find(LITERAL("Hello"), 0) != StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type find(const_pointer pStr, size_type nOffSet = 0) const");
			PRINT_COND_ASSERT(sw.find(LITERAL("Hello"), 3) == StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type find(const_pointer pStr, size_type nOffSet = 0) const");
			PRINT_COND_ASSERT(sw.find(LITERAL("Londo"), 5) != StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type find(const_pointer pStr, size_type nOffSet = 0) const");
		}


		// CONSTEXPR14 size_type rfind(my_type str, size_type nOffSet = npos) const NOEXCEPT;
		// CONSTEXPR14 size_type rfind(const_pointer pStr, size_type nOffSet = npos) const NOEXCEPT;
		// CONSTEXPR14 size_type rfind(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT;
		// CONSTEXPR14 size_type rfind(charT ch, size_type nOffSet = npos) const NOEXCEPT;
		{
			StringViewT str(LITERAL("abcdefghijklmnopqrstuvwxyz"));

			PRINT_COND_ASSERT(str.rfind(StringViewT(LITERAL("d"))) != StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type rfind(my_type str, size_type nOffSet = npos) const NOEXCEPT");
			PRINT_COND_ASSERT(str.rfind(StringViewT(LITERAL("tuv"))) != StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type rfind(my_type str, size_type nOffSet = npos) const NOEXCEPT");
			PRINT_COND_ASSERT(str.rfind(StringViewT(LITERAL("123r"))) == StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type rfind(my_type str, size_type nOffSet = npos) const NOEXCEPT");
			PRINT_COND_ASSERT(str.rfind(LITERAL("d")) != StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type rfind(const_pointer pStr, size_type nOffSet = npos) const NOEXCEPT");
			PRINT_COND_ASSERT(str.rfind(LITERAL("tuv")) != StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type rfind(const_pointer pStr, size_type nOffSet = npos) const NOEXCEPT");
			PRINT_COND_ASSERT(str.rfind(LITERAL("123r")) == StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type rfind(const_pointer pStr, size_type nOffSet = npos) const NOEXCEPT");
			PRINT_COND_ASSERT(str.rfind(LITERAL("d"), str.length()) != StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type rfind(const_pointer pStr, size_type nOffSet = npos) const NOEXCEPT");
			PRINT_COND_ASSERT(str.rfind(LITERAL("tuv"), str.length() - 2) != StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type rfind(const_pointer pStr, size_type nOffSet = npos) const NOEXCEPT");
			PRINT_COND_ASSERT(str.rfind(LITERAL("123r"), str.length() - 2) == StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type rfind(const_pointer pStr, size_type nOffSet = npos) const NOEXCEPT");
			PRINT_COND_ASSERT(str.rfind(LITERAL('d'), str.length() - 0) != StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type rfind(charT ch, size_type nOffSet = npos) const NOEXCEPT");
			PRINT_COND_ASSERT(str.rfind(LITERAL('t'), str.length() - 2) != StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type rfind(charT ch, size_type nOffSet = npos) const NOEXCEPT");
			PRINT_COND_ASSERT(str.rfind(LITERAL('1'), str.length() - 2) == StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type rfind(charT ch, size_type nOffSet = npos) const NOEXCEPT");
		}

		// CONSTEXPR14 size_type find_last_of(my_type str, size_type nOffSet = npos) const NOEXCEPT;
		// CONSTEXPR14 size_type find_last_of(const_pointer pStr, size_type nOffSet = npos) const NOEXCEPT;
		// CONSTEXPR14 size_type find_last_of(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT;
		// CONSTEXPR14 size_type find_last_of(charT ch, size_type nOffSet = npos) const NOEXCEPT;
		{
			StringViewT str(LITERAL("aaaaabbbbbcccdddddeeeeefffggh"));

			PRINT_COND_ASSERT(str.find_first_of(StringViewT(LITERAL("aaa"))) == 0, "StringViewBase - CONSTEXPR14 size_type find_first_of(my_type str, size_type nOffSet = 0) const NOEXCEPT");
			PRINT_COND_ASSERT(str.find_first_of(LITERAL("aab")) == 0, "StringViewBase - CONSTEXPR14 size_type find_first_of(my_type str, size_type nOffSet = 0) const NOEXCEPT");

			PRINT_COND_ASSERT(str.find_first_of(LITERAL("baab")) == 0, "StringViewBase - CONSTEXPR14 size_type find_first_of(const_pointer pStr, size_type nOffSet = 0) const NOEXCEPT");
			PRINT_COND_ASSERT(str.find_first_of(LITERAL("ceg")) == 10, "StringViewBase - CONSTEXPR14 size_type find_first_of(const_pointer pStr, size_type nOffSet = 0) const NOEXCEPT");

			PRINT_COND_ASSERT(str.find_first_of(LITERAL("eeef"), 1, 2) == 18, "StringViewBase - CONSTEXPR14 size_type find_first_of(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT");
			PRINT_COND_ASSERT(str.find_first_of(LITERAL("eeef"), 1, 4) == 18, "StringViewBase - CONSTEXPR14 size_type find_first_of(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT");

			PRINT_COND_ASSERT(str.find_first_of(LITERAL('g')) == 26, "StringViewBase - CONSTEXPR14 size_type find_first_of(charT ch, size_type nOffSet = 0) const NOEXCEPT");
			PRINT_COND_ASSERT(str.find_first_of(LITERAL('$')) == StringViewT::npos, "StringViewBase - CONSTEXPR14 size_type find_first_of(charT ch, size_type nOffSet = 0) const NOEXCEPT");
		}

		// CONSTEXPR14 size_type find_last_of(my_type str, size_type nOffSet = npos) const NOEXCEPT;
		// CONSTEXPR14 size_type find_last_of(const_pointer pStr, size_type nOffSet = npos) const NOEXCEPT;
		// CONSTEXPR14 size_type find_last_of(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT;
		// CONSTEXPR14 size_type find_last_of(charT ch, size_type nOffSet = npos) const NOEXCEPT;
		{
			StringViewT str(LITERAL("aaaaabbbbbcccdddddeeeeefffggh"));

			PRINT_COND_ASSERT(str.find_last_of(StringViewT(LITERAL("aaa"))) == 4, "StringViewBase - CONSTEXPR14 find_last_of(my_type str, size_type nOffSet = npos) const NOEXCEPT");
			PRINT_COND_ASSERT(str.find_last_of(LITERAL("aab")) == 9, "StringViewBase - CONSTEXPR14 find_last_of(my_type str, size_type nOffSet = npos) const NOEXCEPT");

			PRINT_COND_ASSERT(str.find_last_of(LITERAL("baab")) == 9, "StringViewBase - CONSTEXPR14 find_last_of(const_pointer pStr, size_type nOffSet = npos) const NOEXCEPT");
			PRINT_COND_ASSERT(str.find_last_of(LITERAL("ceg")) == 27, "StringViewBase - CONSTEXPR14 find_last_of(const_pointer pStr, size_type nOffSet = npos) const NOEXCEPT");

			PRINT_COND_ASSERT(str.find_last_of(LITERAL('g')) == 27, "StringViewBase - CONSTEXPR14 find_last_of(charT ch, size_type nOffSet = npos) const NOEXCEPT");
			PRINT_COND_ASSERT(str.find_last_of(LITERAL('$')) == StringViewT::npos, "StringViewBase - CONSTEXPR14 find_last_of(charT ch, size_type nOffSet = npos) const NOEXCEPT");
		}

		// CONSTEXPR14 size_type find_first_not_of(my_type str, size_type nOffSet = npos) const NOEXCEPT;
		// CONSTEXPR14 size_type find_first_not_of(const_pointer pStr, size_type nOffSet = npos) const;
		// CONSTEXPR14 size_type find_first_not_of(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT;
		// CONSTEXPR14 size_type find_first_not_of(charT ch, size_type nOffSet = npos) const NOEXCEPT;
		{
			StringViewT str(LITERAL("aaaaabbbbbcccdddddeeeeefffggh"));

			PRINT_COND_ASSERT(str.find_first_not_of(StringViewT(LITERAL("abcdfg"))) == 18, "StringViewBase - CONSTEXPR14 find_first_not_of(my_type str, size_type nOffSet = npos) const NOEXCEPT");
			PRINT_COND_ASSERT(str.find_first_not_of(LITERAL("abcdfg")) == 18, "StringViewBase - CONSTEXPR14 find_first_not_of(const_pointer pStr, size_type nOffSet = npos) const");
			PRINT_COND_ASSERT(str.find_first_not_of(LITERAL('a')) == 5, "StringViewBase - CONSTEXPR14 find_first_not_of(charT ch, size_type nOffSet = npos) const NOEXCEPT");
		}


		// CONSTEXPR14 size_type find_last_not_of(my_type str, size_type nOffSet = npos) const NOEXCEPT;
		// CONSTEXPR14 size_type find_last_not_of(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT;
		// CONSTEXPR14 size_type find_last_not_of(const_pointer pStr, size_type nOffSet = npos) const NOEXCEPT;
		// CONSTEXPR14 size_type find_last_not_of(charT ch, size_type nOffSet = npos) const NOEXCEPT;
		{
			StringViewT str(LITERAL("aaaaabbbbbcccdddddeeeeefffggh"));

			PRINT_COND_ASSERT(str.find_last_not_of(StringViewT(LITERAL("a"))) == 28, "StringViewBase - CONSTEXPR14 find_last_not_of(my_type str, size_type nOffSet = npos) const NOEXCEPT");
			PRINT_COND_ASSERT(str.find_last_not_of(StringViewT(LITERAL("abcdfg"))) == 28, "StringViewBase - CONSTEXPR14 find_last_not_of(my_type str, size_type nOffSet = npos) const NOEXCEPT");
			PRINT_COND_ASSERT(str.find_last_not_of(StringViewT(LITERAL("abcdfgh"))) == 22, "StringViewBase - CONSTEXPR14 find_last_not_of(my_type str, size_type nOffSet = npos) const NOEXCEPT");
			PRINT_COND_ASSERT(str.find_last_not_of(LITERAL("abcdfgh")) == 22, "StringViewBase - CONSTEXPR14 find_last_not_of(const_pointer pStr, size_type nOffSet = npos) const NOEXCEPT");
			PRINT_COND_ASSERT(str.find_last_not_of(LITERAL('a')) == 28, "StringViewBase - CONSTEXPR14 find_last_not_of(charT ch, size_type nOffSet = npos) const NOEXCEPT");
		}

		// template < class CharT, class Traits >
		// CONSTEXPR bool operator==(StringViewBase< CharT, Traits > lhs, StringViewBase< CharT, Traits > rhs);
		// template < class CharT, class Traits >
		// CONSTEXPR bool operator!=(StringViewBase< CharT, Traits > lhs, StringViewBase< CharT, Traits > rhs);
		// template < class CharT, class Traits >
		// CONSTEXPR bool operator<(StringViewBase< CharT, Traits > lhs, StringViewBase< CharT, Traits > rhs);
		// template < class CharT, class Traits >
		// CONSTEXPR bool operator<=(StringViewBase< CharT, Traits > lhs, StringViewBase< CharT, Traits > rhs);
		// template < class CharT, class Traits >
		// CONSTEXPR bool operator>(StringViewBase< CharT, Traits > lhs, StringViewBase< CharT, Traits > rhs);
		// template < class CharT, class Traits >
		// CONSTEXPR bool operator>=(StringViewBase< CharT, Traits > lhs, StringViewBase< CharT, Traits > rhs);
		{
			StringViewT sw1(LITERAL("AAAAABBBBBCCCDDDDDEEEEEFFFGGH"));
			StringViewT sw2(LITERAL("aaaaabbbbbcccdddddeeeeefffggh"));
			StringViewT sw3(LITERAL("0123456789!@#$%^&*()"));

			PRINT_COND_ASSERT(sw1 == StringViewT(LITERAL("AAAAABBBBBCCCDDDDDEEEEEFFFGGH")), "");
			PRINT_COND_ASSERT(sw1 != StringViewT(LITERAL("abcdefghijklmnopqrstuvwxyz")), "");
			PRINT_COND_ASSERT(sw1 < sw2, "");
			PRINT_COND_ASSERT(sw1 <= sw2, "");
			PRINT_COND_ASSERT(sw2 > sw1, "");
			PRINT_COND_ASSERT(sw2 >= sw1, "");
		}
	}

	{
		StringViewT sw1(LITERAL("AAAAABBBBBCCCDDDDDEEEEEFFFGGH"));

		PRINT_COND_ASSERT(sw1.starts_with(LITERAL('A')), "");
		PRINT_COND_ASSERT(!sw1.starts_with(LITERAL('X')), "");
		PRINT_COND_ASSERT(sw1.starts_with(LITERAL("AAAA")), "");
		PRINT_COND_ASSERT(sw1.starts_with(StringViewT(LITERAL("AAAA"))), "");
		PRINT_COND_ASSERT(!sw1.starts_with(LITERAL("AAAB")), "");

		PRINT_COND_ASSERT(sw1.ends_with(LITERAL('H')), "");
		PRINT_COND_ASSERT(!sw1.ends_with(LITERAL('X')), "");
		PRINT_COND_ASSERT(sw1.ends_with(LITERAL("FGGH")), "");
		PRINT_COND_ASSERT(sw1.ends_with(StringViewT(LITERAL("FGGH"))), "");
		PRINT_COND_ASSERT(!sw1.ends_with(LITERAL("FGGH$")), "");
	}

	return nErrorCount;
}

#undef TEST_STRING_NAME 
#undef LITERAL