// Creator - MatthewGolder
#include "FXDEngine/Memory/Allocator/Alloc.h"
#include "Tests/System/Tests/FXDTest.h"

///////////////////////////////////////////////////////////////////////////////
// TestObject
///////////////////////////////////////////////////////////////////////////////
int64_t TestObject::sTOCount = 0;
int64_t TestObject::sTOCtorCount = 0;
int64_t TestObject::sTODtorCount = 0;
int64_t TestObject::sTODefaultCtorCount = 0;
int64_t TestObject::sTOArgCtorCount = 0;
int64_t TestObject::sTOCopyCtorCount = 0;
int64_t TestObject::sTOMoveCtorCount = 0;
int64_t TestObject::sTOCopyAssignCount = 0;
int64_t TestObject::sTOMoveAssignCount = 0;
FXD::S32 TestObject::sMagicErrorCount = 0;

TestObject::TestObject(FXD::S32 nX, bool bThrowOnCopy)
	: m_nX(nX)
	, m_bThrowOnCopy(bThrowOnCopy)
	, m_nMagicValue(kMagicValue)
{
	++sTOCount;
	++sTOCtorCount;
	++sTODefaultCtorCount;
	m_nID = sTOCtorCount;
}

TestObject::TestObject(FXD::S32 x0, FXD::S32 x1, FXD::S32 x2, bool bThrowOnCopy)
	: m_nX(x0 + x1 + x2)
	, m_bThrowOnCopy(bThrowOnCopy)
	, m_nMagicValue(kMagicValue)
{
	++sTOCount;
	++sTOCtorCount;
	++sTOArgCtorCount;
	m_nID = sTOCtorCount;
}

TestObject::~TestObject(void)
{
	if (m_nMagicValue != kMagicValue)
	{
		++sMagicErrorCount;
	}
	m_nMagicValue = 0;
	--sTOCount;
	++sTODtorCount;
}

TestObject::TestObject(const TestObject& rhs)
	: m_nX(rhs.m_nX)
	, m_bThrowOnCopy(rhs.m_bThrowOnCopy)
	, m_nMagicValue(rhs.m_nMagicValue)
{
	++sTOCount;
	++sTOCtorCount;
	++sTOCopyCtorCount;
	m_nID = sTOCtorCount;
	if (m_bThrowOnCopy)
	{
		PRINT_ASSERT << "Disallowed TestObject copy";
	}
}

TestObject::TestObject(TestObject&& rhs)
	: m_nX(rhs.m_nX)
	, m_bThrowOnCopy(rhs.m_bThrowOnCopy)
	, m_nMagicValue(rhs.m_nMagicValue)
{
	++sTOCount;
	++sTOCtorCount;
	++sTOMoveCtorCount;
	m_nID = sTOCtorCount;  // testObject keeps its m_nID, and we assign ours anew.
	rhs.m_nX = 0;   // We are swapping our contents with the TestObject, so give it our "previous" value.
	if (m_bThrowOnCopy)
	{
		//PRINT_ASSERT << "Disallowed TestObject copy";
	}
}

TestObject& TestObject::operator=(const TestObject& rhs)
{
	++sTOCopyAssignCount;

	if (&rhs != this)
	{
		m_nX = rhs.m_nX;
		// Leave m_nID alone.
		m_nMagicValue = rhs.m_nMagicValue;
		m_bThrowOnCopy = rhs.m_bThrowOnCopy;
		if (m_bThrowOnCopy)
		{
			PRINT_ASSERT << "Disallowed TestObject copy";
		}
	}
	return (*this);
}

TestObject& TestObject::operator=(TestObject&& rhs)
{
	++sTOMoveAssignCount;

	if (&rhs != this)
	{
		FXD::STD::swap(m_nX, rhs.m_nX);
		// Leave m_nID alone.
		FXD::STD::swap(m_nMagicValue, rhs.m_nMagicValue);
		FXD::STD::swap(m_bThrowOnCopy, rhs.m_bThrowOnCopy);
		if (m_bThrowOnCopy)
		{
			//PRINT_ASSERT << "Disallowed TestObject copy";
		}
	}
	return (*this);
}

void TestObject::reset(void)
{
	sTOCount = 0;
	sTOCtorCount = 0;
	sTODtorCount = 0;
	sTODefaultCtorCount = 0;
	sTOArgCtorCount = 0;
	sTOCopyCtorCount = 0;
	sTOMoveCtorCount = 0;
	sTOCopyAssignCount = 0;
	sTOMoveAssignCount = 0;
	sMagicErrorCount = 0;
}

bool TestObject::is_clear(void) // Returns true if there are no existing TestObjects and the sanity checks related to that test OK.
{
	return (sTOCount == 0) && (sTODtorCount == sTOCtorCount) && (sMagicErrorCount == 0);
}

///////////////////////////////////////////////////////////////////////////////
// gFXD_TestLevel
///////////////////////////////////////////////////////////////////////////////
FXD_TestLevel gFXD_TestLevel = FXD_TestLevel::Medium;


FXD::U32 gRandSeed = 0;
FXD::U32 GetRandSeed(void)
{
	return gRandSeed;
}

void SetRandSeed(FXD::U32 nSeed)
{
	gRandSeed = nSeed;
}