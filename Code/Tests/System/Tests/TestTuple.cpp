// Creator - MatthewGolder
#include "Tests/System/Tests/FXDTest.h"
#include "Tests/System/TestHelperClasses.h"
#include "FXDEngine/Container/Tuple.h"
#include "FXDEngine/Core/Utility.h"

namespace TestTupleInternal
{
	struct DefaultConstructibleType
	{
		static const FXD::S32 kDefaultVal = 0x1EE7C0DE;
		
		DefaultConstructibleType(void)
			: m_nVal(kDefaultVal)
		{}
		
		FXD::S32 m_nVal;
	};

	class OperationCountingType
	{
	public:
		OperationCountingType(void)
			: m_nVal()
		{
			++s_nDefaultConstructorCalls;
		}
		OperationCountingType(FXD::S32 nVal)
			: m_nVal(nVal)
		{
			++s_nIntConstructorCalls;
		}
		OperationCountingType(const OperationCountingType& rhs)
			: m_nVal(rhs.m_nVal)
		{
			++s_nCopyConstructorCalls;
		}
		OperationCountingType(OperationCountingType&& rhs)
			: m_nVal(rhs.m_nVal)
		{
			++s_nMoveConstructorCalls;
			rhs.m_nVal = 0;
		}
		OperationCountingType& operator=(const OperationCountingType& rhs)
		{
			m_nVal = rhs.m_nVal;
			++s_nCopyAssignmentCalls;
			return (*this);
		}
		OperationCountingType& operator=(OperationCountingType&& rhs)
		{
			m_nVal = rhs.m_nVal;
			rhs.m_nVal = 0;
			++s_nMoveAssignmentCalls;
			return (*this);
		}
		~OperationCountingType(void)
		{
			++s_nDestructorCalls;
		}

		static void reset_counters(void)
		{
			s_nDefaultConstructorCalls = 0;
			s_nIntConstructorCalls = 0;
			s_nCopyConstructorCalls = 0;
			s_nMoveConstructorCalls = 0;
			s_nCopyAssignmentCalls = 0;
			s_nMoveAssignmentCalls = 0;
			s_nDestructorCalls = 0;
		}

	public:
		FXD::S32 m_nVal;
		static FXD::S32 s_nDefaultConstructorCalls;
		static FXD::S32 s_nIntConstructorCalls;
		static FXD::S32 s_nCopyConstructorCalls;
		static FXD::S32 s_nMoveConstructorCalls;
		static FXD::S32 s_nCopyAssignmentCalls;
		static FXD::S32 s_nMoveAssignmentCalls;
		static FXD::S32 s_nDestructorCalls;
	};

	FXD::S32 OperationCountingType::s_nDefaultConstructorCalls = 0;
	FXD::S32 OperationCountingType::s_nIntConstructorCalls = 0;
	FXD::S32 OperationCountingType::s_nCopyConstructorCalls = 0;
	FXD::S32 OperationCountingType::s_nMoveConstructorCalls = 0;
	FXD::S32 OperationCountingType::s_nCopyAssignmentCalls = 0;
	FXD::S32 OperationCountingType::s_nMoveAssignmentCalls = 0;
	FXD::S32 OperationCountingType::s_nDestructorCalls = 0;

}  // namespace TestTupleInternal

FXD::S32 TestTuple(void)
{
	FXD::S32 nErrorCount = 0;

	{
		CTC_ASSERT((FXD::tuple_size< FXD::Container::Tuple< FXD::S32 > >::value == 1), "Tuple - tuple_size< FXD::Container::Tuple< FXD::S32 > >.");
		CTC_ASSERT((FXD::tuple_size< const FXD::Container::Tuple< FXD::S32 > >::value == 1), "Tuple - tuple_size< const FXD::Container::Tuple< FXD::S32 > >.");
		CTC_ASSERT((FXD::tuple_size< const FXD::Container::Tuple< const FXD::S32 > >::value == 1), "Tuple - tuple_size< const FXD::Container::Tuple< const FXD::S32 > >:.");
		CTC_ASSERT((FXD::tuple_size< volatile FXD::Container::Tuple< FXD::S32 > >::value == 1), "Tuple - tuple_size< volatile FXD::Container::Tuple< FXD::S32 > >.");
		CTC_ASSERT((FXD::tuple_size< const volatile FXD::Container::Tuple< FXD::S32 > >::value == 1), "Tuple - tuple_size< const volatile FXD::Container::Tuple< FXD::S32 > >.");
		CTC_ASSERT((FXD::tuple_size< FXD::Container::Tuple< FXD::S32, FXD::F32, bool > >::value == 3), "Tuple - tuple_size< FXD::Container::Tuple< FXD::S32, FXD::F32, bool > >.");

		CTC_ASSERT((FXD::STD::is_same< FXD::tuple_element_t< 0, FXD::Container::Tuple< FXD::S32 > >, FXD::S32 >::value), "Tuple - FXD::tuple_element_t< 0, FXD::Container::Tuple< FXD::S32 > >, FXD::S32 >.");
		CTC_ASSERT((FXD::STD::is_same< FXD::tuple_element_t< 1, FXD::Container::Tuple< FXD::F32, FXD::S32 > >, FXD::S32 >::value), "Tuple - FXD::tuple_element_t< 1, FXD::Container::Tuple< FXD::F32, FXD::S32 > >, FXD::S32 >.");
		CTC_ASSERT((FXD::STD::is_same< FXD::tuple_element_t< 1, FXD::Container::Tuple< FXD::F32, const FXD::S32 > >, const FXD::S32 >::value), "Tuple - FXD::tuple_element_t< 1, FXD::Container::Tuple< FXD::F32, const FXD::S32 > >, const FXD::S32 >.");
		CTC_ASSERT((FXD::STD::is_same< FXD::tuple_element_t< 1, FXD::Container::Tuple< FXD::F32, volatile FXD::S32 > >, volatile FXD::S32 >::value), "Tuple - FXD::tuple_element_t< 1, FXD::Container::Tuple< FXD::F32, volatile FXD::S32 > >, volatile FXD::S32 >.");
		CTC_ASSERT((FXD::STD::is_same< FXD::tuple_element_t< 1, FXD::Container::Tuple< FXD::F32, const volatile FXD::S32 > >, const volatile FXD::S32 >::value), "Tuple - FXD::tuple_element_t< 1, FXD::Container::Tuple< FXD::F32, const volatile FXD::S32 > >, const volatile FXD::S32 >.");
		CTC_ASSERT((FXD::STD::is_same< FXD::tuple_element_t< 1, FXD::Container::Tuple< FXD::F32, FXD::S32& > >, FXD::S32& >::value), "Tuple - FXD::tuple_element_t< 1, FXD::Container::Tuple< FXD::F32, FXD::S32& > >, FXD::S32& >.");
	}

	{
		{
			FXD::Container::Tuple< FXD::S32 > tupSingleElement(1);
			PRINT_COND_ASSERT(FXD::get< 0 >(tupSingleElement) == 1, "Tuple");
			FXD::get< 0 >(tupSingleElement) = 2;
			PRINT_COND_ASSERT(FXD::get< 0 >(tupSingleElement) == 2, "Tuple");
			FXD::get< FXD::S32 >(tupSingleElement) = 3;
			PRINT_COND_ASSERT(FXD::get< FXD::S32 >(tupSingleElement) == 3, "Tuple");
		}

		{
			const FXD::Container::Tuple< FXD::S32 > tupSingleConstElement(3);
			PRINT_COND_ASSERT(FXD::get< 0 >(tupSingleConstElement) == 3, "Tuple");
			PRINT_COND_ASSERT(FXD::get< FXD::S32 >(tupSingleConstElement) == 3, "Tuple");
		}

		{
			FXD::Container::Tuple< TestTupleInternal::DefaultConstructibleType > tupDefaultConstructed;
			PRINT_COND_ASSERT(FXD::get< 0 >(tupDefaultConstructed).m_nVal == TestTupleInternal::DefaultConstructibleType::kDefaultVal, "Tuple");
		}

		PRINT_COND_ASSERT(TestTupleInternal::OperationCountingType::s_nDestructorCalls == 0, "Tuple");
		TestTupleInternal::OperationCountingType::reset_counters();

		{
			FXD::Container::Tuple< TestTupleInternal::OperationCountingType > tupDefaultConstructed1;
			PRINT_COND_ASSERT(TestTupleInternal::OperationCountingType::s_nDefaultConstructorCalls == 1 && FXD::get< 0 >(tupDefaultConstructed1).m_nVal == 0, "Tuple");
			FXD::get< 0 >(tupDefaultConstructed1).m_nVal = 1;

			FXD::Container::Tuple< TestTupleInternal::OperationCountingType > tupDefaultConstructed2(tupDefaultConstructed1);
			PRINT_COND_ASSERT(TestTupleInternal::OperationCountingType::s_nDefaultConstructorCalls == 1 && TestTupleInternal::OperationCountingType::s_nCopyConstructorCalls == 1 && FXD::get< 0 >(tupDefaultConstructed2).m_nVal == 1, "Tuple");
			FXD::get< 0 >(tupDefaultConstructed1).m_nVal = 2;

			tupDefaultConstructed2 = tupDefaultConstructed1;
			PRINT_COND_ASSERT(TestTupleInternal::OperationCountingType::s_nDefaultConstructorCalls == 1 && TestTupleInternal::OperationCountingType::s_nCopyConstructorCalls == 1 && TestTupleInternal::OperationCountingType::s_nCopyAssignmentCalls == 1 && FXD::get< 0 >(tupDefaultConstructed2).m_nVal == 2, "Tuple");
		}
	
		PRINT_COND_ASSERT(TestTupleInternal::OperationCountingType::s_nDestructorCalls == 2, "Tuple");
		TestTupleInternal::OperationCountingType::reset_counters();
		
		{
			FXD::Container::Tuple< TestTupleInternal::OperationCountingType > tupDefaultConstructed(TestTupleInternal::OperationCountingType(5));
			PRINT_COND_ASSERT(TestTupleInternal::OperationCountingType::s_nMoveConstructorCalls == 1 && TestTupleInternal::OperationCountingType::s_nDefaultConstructorCalls == 0 && TestTupleInternal::OperationCountingType::s_nCopyConstructorCalls == 0 && FXD::get< 0 >(tupDefaultConstructed).m_nVal == 5, "Tuple");
		}

		PRINT_COND_ASSERT(TestTupleInternal::OperationCountingType::s_nDestructorCalls == 2, "Tuple");
	}


	{
		// Test constructor
		FXD::Container::Tuple< FXD::S32, FXD::F32, bool > tupConstruct(1, 1.0f, true);
		PRINT_COND_ASSERT(FXD::get< 0 >(tupConstruct) == 1, "Tuple");
		PRINT_COND_ASSERT(FXD::get< 1 >(tupConstruct) == 1.0f, "Tuple");
		PRINT_COND_ASSERT(FXD::get< 2 >(tupConstruct) == true, "Tuple");
		PRINT_COND_ASSERT(FXD::get< FXD::S32 >(tupConstruct) == 1, "Tuple");
		PRINT_COND_ASSERT(FXD::get< FXD::F32 >(tupConstruct) == 1.0f, "Tuple");
		PRINT_COND_ASSERT(FXD::get< bool >(tupConstruct) == true, "Tuple");

		FXD::get< 1 >(tupConstruct) = 2.0f;
		PRINT_COND_ASSERT(FXD::get< 1 >(tupConstruct) == 2.0f, "Tuple");

		// Test copy constructor
		FXD::Container::Tuple< FXD::S32, FXD::F32, bool > tupCopyConstruct(tupConstruct);
		PRINT_COND_ASSERT(FXD::get< 0 >(tupCopyConstruct) == 1 && FXD::get< 1 >(tupCopyConstruct) == 2.0f && FXD::get< 2 >(tupCopyConstruct) == true, "Tuple");

		// Test copy assignment
		FXD::Container::Tuple< FXD::S32, FXD::F32, bool > tupCopyAssignment(2, 3.0f, true);
		PRINT_COND_ASSERT(FXD::get< 0 >(tupCopyAssignment) == 2 && FXD::get< 1 >(tupCopyAssignment) == 3.0f && FXD::get< 2 >(tupCopyAssignment) == true, "Tuple");
		tupCopyAssignment = tupCopyConstruct;
		PRINT_COND_ASSERT(FXD::get< 0 >(tupCopyAssignment) == 1 && FXD::get< 1 >(tupCopyAssignment) == 2.0f && FXD::get< 2 >(tupCopyAssignment) == true, "Tuple");

		// Test converting 'copy' constructor (from a tuple of different type whose members are each convertible)
		FXD::Container::Tuple< double, double, bool > tupCopyConvertable(tupConstruct);
		PRINT_COND_ASSERT(FXD::get< 0 >(tupCopyConvertable) == 1.0 && FXD::get< 1 >(tupCopyConvertable) == 2.0 && FXD::get< 2 >(tupCopyConvertable) == true, "Tuple");

		// Test converting assignment operator (from a tuple of different type whose members are each convertible)
		FXD::Container::Tuple< double, double, bool > tupAssignmentConvertable;
		PRINT_COND_ASSERT(FXD::get< 0 >(tupAssignmentConvertable) == 0.0 && FXD::get< 1 >(tupAssignmentConvertable) == 0.0 && FXD::get< 2 >(tupAssignmentConvertable) == false, "Tuple");
		tupAssignmentConvertable = tupCopyConstruct;
		PRINT_COND_ASSERT(FXD::get< 0 >(tupAssignmentConvertable) == 1.0 && FXD::get< 1 >(tupAssignmentConvertable) == 2.0 && FXD::get< 2 >(tupAssignmentConvertable) == true, "Tuple");
	}
	
	{
		// Test default initialization (built in types should be value initialized rather than default initialized)
		FXD::Container::Tuple< FXD::S32, FXD::F32, bool > tupDefaultInitialized;
		PRINT_COND_ASSERT(FXD::get< 0 >(tupDefaultInitialized) == 0 && FXD::get< 1 >(tupDefaultInitialized) == 0.0f && FXD::get< 2 >(tupDefaultInitialized) == false, "Tuple");
	}

	{
		// Test some other cases with typed-getter
		{
			FXD::Container::Tuple< double, double, bool > tupRepeatedType1(1.0f, 2.0f, true);
			PRINT_COND_ASSERT(FXD::get< bool >(tupRepeatedType1) == true, "Tuple");

			FXD::Container::Tuple< double, bool, double > tupRepeatedType2(1.0f, true, 2.0f);
			PRINT_COND_ASSERT(FXD::get< bool >(tupRepeatedType2) == true, "Tuple");

			FXD::Container::Tuple< bool, double, double > tupRepeatedType3(true, 1.0f, 2.0f);
			PRINT_COND_ASSERT(FXD::get< bool >(tupRepeatedType2) == true, "Tuple");

			struct FloatOne { FXD::F32 val; };
			struct FloatTwo { FXD::F32 val; };
			FXD::Container::Tuple< FloatOne, FloatTwo > tupOfStructs({ 1.0f }, { 2.0f });
			PRINT_COND_ASSERT(FXD::get< FloatOne >(tupOfStructs).val == 1.0f, "Tuple");
			PRINT_COND_ASSERT(FXD::get< FloatTwo >(tupOfStructs).val == 2.0f, "Tuple");

			const FXD::Container::Tuple< double, double, bool > tupConst(tupRepeatedType1);
			const bool& constRefVal = FXD::get< bool >(tupConst);
			PRINT_COND_ASSERT(constRefVal == true, "Tuple");

			const bool&& constRRefVal = FXD::get< bool >(std::move(tupRepeatedType1));
			PRINT_COND_ASSERT(constRRefVal == true, "Tuple");
		}
	}

	{
		{
			FXD::Container::Tuple< FXD::S32, FXD::F32 > tuphDefaultInitList(1, {});
		}

		// tuple construction from pair
		{
			FXD::Core::Pair< FXD::S32, FXD::F32 > tupPair(1, 2.0f);
			FXD::Container::Tuple< FXD::S32, FXD::F32 > tupPairCopy1(tupPair);
			PRINT_COND_ASSERT(FXD::get< 0 >(tupPair) == 1 && FXD::get< 1 >(tupPair) == 2.0f, "Tuple");

			FXD::Container::Tuple< double, double > tupPairCopy2(tupPairCopy1);
			PRINT_COND_ASSERT(FXD::get< 0 >(tupPairCopy2) == 1.0 && FXD::get< 1 >(tupPairCopy2) == 2.0, "Tuple");

			tupPairCopy2 = FXD::Core::make_pair(2, 3);
			PRINT_COND_ASSERT(FXD::get< 0 >(tupPairCopy2) == 2.0 && FXD::get< 1 >(tupPairCopy2) == 3.0, "Tuple");
		}

		// operators: ==, !=, <
		{
			FXD::Container::Tuple< double, double > tup1(3, 4);
			FXD::Container::Tuple< double, double > tup2 = tup1;
			PRINT_COND_ASSERT(tup1 == tup2, "Tuple");
			PRINT_COND_ASSERT(!(tup1 < tup2) && !(tup2 < tup1), "Tuple");

			FXD::Container::Tuple< double, double > tupDefaultInit;
			PRINT_COND_ASSERT(tup1 != tupDefaultInit, "Tuple");
			PRINT_COND_ASSERT(tupDefaultInit < tup1, "Tuple");

			FXD::Container::Tuple< FXD::S32, FXD::S32, FXD::S32 > tupLess(1, 2, 3);
			FXD::Container::Tuple< FXD::S32, FXD::S32, FXD::S32 > tupGreater(1, 2, 4);
			PRINT_COND_ASSERT(tupLess < tupGreater && !(tupGreater < tupLess) && tupGreater > tupLess && !(tupLess > tupGreater), "Tuple");
		}
		{
			FXD::Container::Tuple< FXD::S32, FXD::F32, TestObject > tup1(2, 2.0f, TestObject(2));
			FXD::Container::Tuple< FXD::S32, FXD::F32, TestObject > tupCopy(tup1);
			FXD::Container::Tuple< const FXD::S32, const FXD::F32, const TestObject > tupConstCopy(tup1);

			PRINT_COND_ASSERT(FXD::get< 0 >(tupCopy) == FXD::get< 0 >(tup1), "Tuple");
			PRINT_COND_ASSERT(FXD::get< 1 >(tupCopy) == FXD::get< 1 >(tup1), "Tuple");
			PRINT_COND_ASSERT(tupCopy == tup1, "Tuple");
			PRINT_COND_ASSERT(FXD::get< 0 >(tupCopy) == FXD::get< 0 >(tupConstCopy), "Tuple");
			PRINT_COND_ASSERT(FXD::get< 1 >(tupCopy) == FXD::get< 1 >(tupConstCopy), "Tuple");
			PRINT_COND_ASSERT(tupConstCopy == tup1, "Tuple");
			PRINT_COND_ASSERT(tupConstCopy == tupCopy, "Tuple");
		}

		// swap
		{
			FXD::Container::Tuple< FXD::S32, FXD::S32, FXD::S32 > tupLess(1, 2, 3);
			FXD::Container::Tuple< FXD::S32, FXD::S32, FXD::S32 > tupGreater(1, 2, 4);

			FXD::STD::swap(tupLess, tupGreater);
			PRINT_COND_ASSERT(FXD::get< 2 >(tupLess) == 4 && FXD::get< 2 >(tupGreater) == 3, "Tuple");
			FXD::STD::swap(tupGreater, tupLess);
			PRINT_COND_ASSERT(tupLess < tupGreater, "Tuple");
		}
	}

	{
		// Test construction of tuple containing a move only type
		CTC_ASSERT((FXD::STD::is_constructible< Game::MoveOnlyType, Game::MoveOnlyType >::value), "Tuple - FXD::STD::is_constructible< Game::MoveOnlyType, Game::MoveOnlyType >.");
		CTC_ASSERT((FXD::STD::is_constructible< Game::MoveOnlyType, Game::MoveOnlyType&& >::value), "Tuple - FXD::STD::is_constructible< Game::MoveOnlyType, Game::MoveOnlyType&& >.");
		CTC_ASSERT((FXD::STD::is_constructible< Game::MoveOnlyType&&, Game::MoveOnlyType&& >::value), "Tuple - FXD::STD::is_constructible< Game::MoveOnlyType&&, Game::MoveOnlyType&& >.");

		FXD::Container::Tuple< Game::MoveOnlyType > tupMoveOnly(1);
		PRINT_COND_ASSERT(FXD::get< 0 >(tupMoveOnly).m_nVal == 1, "Tuple");

		FXD::get< 0 >(tupMoveOnly) = Game::MoveOnlyType(2);
		PRINT_COND_ASSERT(FXD::get< 0 >(tupMoveOnly).m_nVal == 2, "Tuple");

		FXD::Container::Tuple< const Game::MoveOnlyType& > tupMoveOnlyConstRef1(tupMoveOnly);
		PRINT_COND_ASSERT(FXD::get< 0 >(tupMoveOnlyConstRef1).m_nVal == 2, "Tuple");

		FXD::Container::Tuple< const Game::MoveOnlyType& > tupMoveOnlyConstRef2(FXD::get< 0 >(tupMoveOnly));
		PRINT_COND_ASSERT(FXD::get< 0 >(tupMoveOnlyConstRef2).m_nVal == 2, "Tuple");

		FXD::Container::Tuple< Game::MoveOnlyType& > tupMoveOnlyRef(FXD::get< 0 >(tupMoveOnly));
		PRINT_COND_ASSERT(FXD::get< 0 >(tupMoveOnlyRef).m_nVal == 2, "Tuple");
	}

	{
		// Tuple helpers

		// make_tuple
		{
			auto tup = FXD::make_tuple(1, 2.0, true);
			PRINT_COND_ASSERT(FXD::get< 0 >(tup) == 1 && FXD::get< 1 >(tup) == 2.0 && FXD::get< 2 >(tup) == true, "Tuple");
		}

		// forward_as_tuple
		{
			auto forwardTest = [](FXD::Container::Tuple< Game::MoveOnlyType&&, Game::MoveOnlyType&& > x) -> FXD::Container::Tuple< Game::MoveOnlyType, Game::MoveOnlyType >
			{
				return FXD::Container::Tuple< Game::MoveOnlyType, Game::MoveOnlyType >(std::move(x));
			};

			FXD::Container::Tuple< Game::MoveOnlyType, Game::MoveOnlyType > tupForward(forwardTest(FXD::forward_as_tuple(Game::MoveOnlyType(1), Game::MoveOnlyType(2))));

			PRINT_COND_ASSERT(FXD::get< 0 >(tupForward).m_nVal == 1 && FXD::get< 1 >(tupForward).m_nVal == 2, "Tuple");
		}

		{
			// tie
			// tuple_cat
			{
				FXD::S32 nVal = 0;
				double fVal = 0.0f;
				FXD::tie(nVal, FXD::Container::ignore, fVal) = FXD::make_tuple(1, 3, 5);
				PRINT_COND_ASSERT(nVal == 1 && fVal == 5.0f, "Tuple");

				auto tupCat1 = FXD::tuple_cat(FXD::make_tuple(1, 2.0f), FXD::make_tuple(3.0, true));
				PRINT_COND_ASSERT(FXD::get< 0 >(tupCat1) == 1 && FXD::get< 1 >(tupCat1) == 2.0f && FXD::get< 2 >(tupCat1) == 3.0 && FXD::get< 3 >(tupCat1) == true, "Tuple");

				auto tupCat2 = FXD::tuple_cat(FXD::make_tuple(1, 2.0f), FXD::make_tuple(3.0, true), FXD::make_tuple(5u, '6'));
				PRINT_COND_ASSERT(FXD::get< 0 >(tupCat2) == 1 && FXD::get< 1 >(tupCat2) == 2.0f && FXD::get< 2 >(tupCat2) == 3.0 && FXD::get< 3 >(tupCat2) == true && FXD::get< 4 >(tupCat2) == 5u && FXD::get< 5 >(tupCat2) == '6', "Tuple");

				auto tupCat3 = FXD::tuple_cat(FXD::make_tuple(1), FXD::tie(nVal, FXD::Container::ignore, fVal));
				FXD::get< 1 >(tupCat3) = 2;
				PRINT_COND_ASSERT(nVal == 2, "Tuple");
			}
		}

		{
			// Empty tuple
			FXD::Container::Tuple<> tupleEmpty;
			PRINT_COND_ASSERT(FXD::tuple_size< decltype(tupleEmpty) >::value == 0, "Tuple");
			tupleEmpty = FXD::make_tuple();
			auto tupleEmpty2 = FXD::make_tuple();
			FXD::STD::swap(tupleEmpty2, tupleEmpty);
		}
	}

	// test piecewise_construct
	{
		{
			struct _Local
			{
				_Local(void) = default;
				_Local(FXD::S32 nVal1, FXD::S32 nVal2)
					: m_nVal1(nVal1)
					, m_nVal2(nVal2)
				{}

				FXD::S32 m_nVal1 = 0;
				FXD::S32 m_nVal2 = 0;
			};

			auto t = FXD::make_tuple(42, 43);

			FXD::Core::Pair< _Local, _Local > pair(FXD::STD::piecewise_construct, t, t);

			PRINT_COND_ASSERT(pair.first.m_nVal1 == 42, "Tuple");
			PRINT_COND_ASSERT(pair.second.m_nVal1 == 42, "Tuple");
			PRINT_COND_ASSERT(pair.first.m_nVal2 == 43, "Tuple");
			PRINT_COND_ASSERT(pair.second.m_nVal2 == 43, "Tuple");
		}

		{
			struct _Local
			{
				_Local(void) = default;
				_Local(FXD::S32 nVal1, FXD::S32 nVal2, FXD::S32 nVal3, FXD::S32 nVal4)
					: m_nVal1(nVal1)
					, m_nVal2(nVal2)
					, m_nVal3(nVal3)
					, m_nVal4(nVal4)
				{}

				FXD::S32 m_nVal1 = 0;
				FXD::S32 m_nVal2 = 0;
				FXD::S32 m_nVal3 = 0;
				FXD::S32 m_nVal4 = 0;
			};

			auto t = FXD::make_tuple(42, 43, 44, 45);

			FXD::Core::Pair< _Local, _Local > pair(FXD::STD::piecewise_construct, t, t);

			PRINT_COND_ASSERT(pair.first.m_nVal1 == 42, "Tuple");
			PRINT_COND_ASSERT(pair.second.m_nVal1 == 42, "Tuple");

			PRINT_COND_ASSERT(pair.first.m_nVal2 == 43, "Tuple");
			PRINT_COND_ASSERT(pair.second.m_nVal2 == 43, "Tuple");

			PRINT_COND_ASSERT(pair.first.m_nVal3 == 44, "Tuple");
			PRINT_COND_ASSERT(pair.second.m_nVal3 == 44, "Tuple");

			PRINT_COND_ASSERT(pair.first.m_nVal4 == 45, "Tuple");
			PRINT_COND_ASSERT(pair.second.m_nVal4 == 45, "Tuple");
		}

		{
			struct _Local1
			{
				_Local1(void) = default;
				_Local1(FXD::S32 nVal)
					: m_nVal(nVal)
				{}
				FXD::S32 m_nVal = 0;
			};

			struct _Local2
			{
				_Local2(void) = default;
				_Local2(FXD::S8 nVal)
					: m_val(nVal)
				{}
				FXD::S8 m_val = 0;
			};

			auto t1 = FXD::make_tuple(42);
			auto t2 = FXD::make_tuple('a');

			FXD::Core::Pair< _Local1, _Local2 > pair(FXD::STD::piecewise_construct, t1, t2);

			PRINT_COND_ASSERT(pair.first.m_nVal == 42, "Tuple");
			PRINT_COND_ASSERT(pair.second.m_val == 'a', "Tuple");
		}
	}

	// apply
	{
		/*
		// test with tuples
		{
			{
				auto result = FXD::apply([](FXD::S32 i) { return i; }, FXD::make_tuple(1));
				PRINT_COND_ASSERT(result == 1, "Tuple");
			}

			{
				auto result = FXD::apply([](FXD::S32 i, FXD::S32 j) { return i + j; }, FXD::make_tuple(1, 2));
				PRINT_COND_ASSERT(result == 3, "Tuple");
			}

			{
				auto result = FXD::apply([](FXD::S32 i, FXD::S32 j, FXD::S32 k, FXD::S32 m) { return i + j + k + m; }, FXD::make_tuple(1, 2, 3, 4));
				PRINT_COND_ASSERT(result == 10, "Tuple");
			}
		}
		*/

		/*
		// test with pair
		{
			auto result = FXD::apply([](FXD::S32 i, FXD::S32 j) { return i + j; }, FXD::make_pair(1, 2));
			PRINT_COND_ASSERT(result == 3, "Tuple");
		}
		*/

		// test with array
		{
			// TODO(rparolin):
			// FXD::array requires FXD::get support before we can support unpacking FXD::arrays with FXD::apply.
			//
			// {
			//     auto result = FXD::apply([](FXD::S32 i) { return i; }, FXD::array<FXD::S32, 1>{1});
			//     PRINT_COND_ASSERT(result == 1);
			// }
			// {
			//     auto result = FXD::apply([](FXD::S32 i, FXD::S32 j) { return i + j; }, FXD::array<FXD::S32, 2>{1,2});
			//     PRINT_COND_ASSERT(result == 3);
			// }
			// {
			//     auto result = FXD::apply([](FXD::S32 i, FXD::S32 j, FXD::S32 k, FXD::S32 m) { return i + j + k + m; }, FXD::array<FXD::S32, 4>{1, 2, 3, 4});
			//     PRINT_COND_ASSERT(result == 10);
			// }
		}
	}

	// user regression for tuple_cat
	{
		void* pEmpty = nullptr;
		auto tup1 = FXD::make_tuple(pEmpty, true);
		auto tupCat = FXD::tuple_cat(FXD::make_tuple("asd", 1), tup1);

		CTC_ASSERT((FXD::STD::is_same< decltype(tupCat), FXD::Container::Tuple< const FXD::UTF8*, FXD::S32, void*, bool > >::value), "Tuple - is_same_v< decltype(tupCat), FXD::Container::Tuple< const FXD::UTF8*, FXD::S32, void*, bool > >.");

		PRINT_COND_ASSERT(FXD::Core::String8("asd") == FXD::get< 0 >(tupCat), "Tuple");
		PRINT_COND_ASSERT(FXD::get< 1 >(tupCat) == 1, "Tuple");
		PRINT_COND_ASSERT(FXD::get< 2 >(tupCat) == nullptr, "Tuple");
		PRINT_COND_ASSERT(FXD::get< 3 >(tupCat) == true, "Tuple");
	}

	return nErrorCount;
}