// Creator - MatthewGolder
#include "Tests/System/Tests/FXDTest.h"
#include "FXDEngine/Core/StringView.h"
#include "FXDEngine/Math/Limits.h"

#define TEST_STRING_NAME TestBasicStringView
#define LITERAL(x) x
#include "TestStringView.inl"

#define TEST_STRING_NAME TestBasicStringView16
#define LITERAL(x) (u##x)
#include "TestStringView.inl"

FXD::S32 TestStringView(void)
{
    FXD::S32 nErrorCount = 0;

	nErrorCount += TestBasicStringView< FXD::Core::StringViewBase< FXD::UTF8 > >();
	nErrorCount += TestBasicStringView< FXD::Core::StringView8 >();

	nErrorCount += TestBasicStringView16< FXD::Core::StringViewBase< FXD::UTF16 > >();
	nErrorCount += TestBasicStringView16< FXD::Core::StringView16 >();

	return nErrorCount;
}