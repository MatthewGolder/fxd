// Creator - MatthewGolder
#include "Tests/System/Tests/TestSet.h"
#include "FXDEngine/Container/Array.h"
#include "FXDEngine/Container/Iterator.h"
#include "FXDEngine/Container/LinkedList.h"
#include "FXDEngine/Container/Set.h"
#include "FXDEngine/Container/Vector.h"

FXD::S32 TestIteratorAdvance(void)
{
	FXD::S32 nErrorCount = 0;

	// void advance(InputItr& itr, NumType nDist)
	{
		const FXD::S32 nNumElements = 10;

		FXD::Container::Vector< FXD::S32 > vecInt;
		for (FXD::S32 n1 = 0; n1 < nNumElements; n1++)
		{
			vecInt.push_back(n1);
		}

		// test forward advancement
		FXD::Container::Vector< FXD::S32 >::iterator itr = vecInt.begin();
		for (FXD::S32 n1 = 0; n1 < nNumElements; n1++)
		{
			PRINT_COND_ASSERT(*itr == vecInt[n1], "Iterator - void advance(InputItr& itr, NumType nDist)");
			FXD::STD::advance(itr, 1);
		}

		// test backwards advancement
		FXD::Container::Vector< FXD::S32 >::iterator itr2 = vecInt.end();
		FXD::S32 n1 = nNumElements - 1;
		do
		{
			FXD::STD::advance(itr2, -1);
			PRINT_COND_ASSERT(*itr2 == vecInt[n1], "Iterator - void advance(InputItr& itr, NumType nDist)");
		} while (n1-- != 0);
	}

	// void advance(InputItr& itr, NumType nDist)
	{
		FXD::Container::LinkedList< FXD::S32 > listInt;
		listInt.push_back(0);
		listInt.push_back(1);
		listInt.push_back(42);
		listInt.push_back(2);

		FXD::Container::LinkedList< FXD::S32 >::iterator itr = listInt.begin();
		FXD::STD::advance(itr, listInt.size());
		PRINT_COND_ASSERT(itr == listInt.end(), "Iterator - void advance(InputItr& itr, NumType nDist)");

		// Exercise advance with an signed Distance type.
		itr = listInt.begin();
		FXD::STD::advance(itr, (size_t)listInt.size());
		PRINT_COND_ASSERT(itr == listInt.end(), "Iterator - void advance(InputItr& itr, NumType nDist)");
	}

	// void next(InputItr itr, typename FXD::STD::iterator_traits< InputItr >::difference_type nDist = 1)
	{
		FXD::Container::Vector< FXD::S32 > vecInt;
		vecInt.push_back(0);
		vecInt.push_back(1);
		vecInt.push_back(42);
		vecInt.push_back(2);

		FXD::Container::Vector< FXD::S32 >::iterator itr = vecInt.begin();
		PRINT_COND_ASSERT(*FXD::STD::next(itr, 0) == 0, "Iterator - void next(InputItr itr, typename FXD::STD::iterator_traits< InputItr >::difference_type nDist = 1)");
		PRINT_COND_ASSERT(*FXD::STD::next(itr) == 1, "Iterator - void next(InputItr itr, typename FXD::STD::iterator_traits< InputItr >::difference_type nDist = 1)");
		PRINT_COND_ASSERT(*FXD::STD::next(itr, 2) == 42, "Iterator - void next(InputItr itr, typename FXD::STD::iterator_traits< InputItr >::difference_type nDist = 1)");
	}

	// void prev(InputItr itr, typename FXD::STD::iterator_traits< InputItr >::difference_type nDist = 1)
	{
		FXD::Container::Vector< FXD::S32 > vecInt;
		vecInt.push_back(0);
		vecInt.push_back(1);
		vecInt.push_back(42);
		vecInt.push_back(2);

		FXD::Container::Vector< FXD::S32 >::iterator itr = vecInt.end();
		PRINT_COND_ASSERT(*FXD::STD::prev(itr, 2) == 42, "Iterator - void prev(InputItr itr, typename FXD::STD::iterator_traits< InputItr >::difference_type nDist = 1)");
		PRINT_COND_ASSERT(*FXD::STD::prev(itr) == 2, "Iterator - void prev(InputItr itr, typename FXD::STD::iterator_traits< InputItr >::difference_type nDist = 1)");
	}

	return nErrorCount;
}

FXD::S32 TestIteratorMoveIterator(void)
{
	FXD::S32 nErrorCount = 0;

	// operator++(FXD::S32)
	// operator--(FXD::S32)
	{
		FXD::Container::Vector< FXD::S32 > arrayInt = { 0, 1, 42, 2 };
		const auto itrConst = FXD::STD::make_move_iterator(arrayInt.begin());

		auto itrMove = itrConst;
		itrMove++; // the result of the expression is the incremented value, we need this test to read the existing state of the iterator.
		PRINT_COND_ASSERT(*itrMove != *itrConst, "");

		itrMove = itrConst + 2; // points to '42'
		itrMove--; // the result of the expression is the incremented value, we need this test to read the existing state of the iterator.
		PRINT_COND_ASSERT(*itrMove != *(itrConst + 2), "");
	}
	return nErrorCount;
}


///////////////////////////////////////////////////////////////////////////////
// TestIterator
///////////////////////////////////////////////////////////////////////////////
FXD::S32 TestIterator(void)
{
	FXD::S32 nErrorCount = 0;
	nErrorCount += TestIteratorAdvance();
	nErrorCount += TestIteratorMoveIterator();

	// reverse_iterator< Iterator > make_reverse_iterator(Iterator itr)
	{
		FXD::Container::Vector< FXD::S32 > vecSrc;
		for (FXD::U32 n1 = 0; n1 < 10; n1++)
		{
			vecSrc.push_back(n1); // vecSrc should become {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
		}

		auto itr = FXD::STD::make_reverse_iterator(vecSrc.end());
		PRINT_COND_ASSERT(*itr == 9, "Iterator - reverse_iterator< Iterator > make_reverse_iterator(Iterator itr)"); ++itr;
		PRINT_COND_ASSERT(*itr == 8, "Iterator - reverse_iterator< Iterator > make_reverse_iterator(Iterator itr)"); ++itr;
		PRINT_COND_ASSERT(*itr == 7, "Iterator - reverse_iterator< Iterator > make_reverse_iterator(Iterator itr)"); ++itr;
		PRINT_COND_ASSERT(*itr == 6, "Iterator - reverse_iterator< Iterator > make_reverse_iterator(Iterator itr)"); ++itr;
		PRINT_COND_ASSERT(*itr == 5, "Iterator - reverse_iterator< Iterator > make_reverse_iterator(Iterator itr)"); ++itr;
		PRINT_COND_ASSERT(*itr == 4, "Iterator - reverse_iterator< Iterator > make_reverse_iterator(Iterator itr)"); ++itr;
		PRINT_COND_ASSERT(*itr == 3, "Iterator - reverse_iterator< Iterator > make_reverse_iterator(Iterator itr)"); ++itr;
		PRINT_COND_ASSERT(*itr == 2, "Iterator - reverse_iterator< Iterator > make_reverse_iterator(Iterator itr)"); ++itr;
		PRINT_COND_ASSERT(*itr == 1, "Iterator - reverse_iterator< Iterator > make_reverse_iterator(Iterator itr)"); ++itr;
		PRINT_COND_ASSERT(*itr == 0, "Iterator - reverse_iterator< Iterator > make_reverse_iterator(Iterator itr)"); ++itr;
		PRINT_COND_ASSERT(itr == vecSrc.rend(), "Iterator - reverse_iterator< Iterator > make_reverse_iterator(Iterator itr)");;
		PRINT_COND_ASSERT(itr == FXD::STD::make_reverse_iterator(vecSrc.begin()), "Iterator - reverse_iterator< Iterator > make_reverse_iterator(Iterator itr)");
	}

	// move_iterator< Iterator > make_move_iterator(Iterator itr)
	{
		FXD::Container::Vector< FXD::Core::String8 > vecSrc;
		for (FXD::U32 n1 = 0; n1 < 4; n1++)
		{
			vecSrc.push_back(FXD::Core::String8(1, (FXD::Core::String8::value_type)('0' + n1))); // v should become {"0", "1", "2", "3"};
		}

		// Moves the values out of the string array and into the result.
		auto be = FXD::STD::make_move_iterator(vecSrc.begin());
		auto en = FXD::STD::make_move_iterator(vecSrc.end());
		std::ptrdiff_t d = FXD::STD::distance(be, en);

		FXD::Container::Vector< FXD::Core::String8 > vecDst(FXD::STD::make_move_iterator(vecSrc.begin()), FXD::STD::make_move_iterator(vecSrc.end()));

		PRINT_COND_ASSERT((vecSrc.size() == 4) && (vecSrc[0] == "") && (vecSrc[3] == ""), "Iterator - move_iterator< Iterator > make_move_iterator(Iterator itr)");
		PRINT_COND_ASSERT((vecDst.size() == 4) && (vecDst[0] == "0") && (vecDst[3] == "3"), "Iterator - move_iterator< Iterator > make_move_iterator(Iterator itr)");
	}

	// CONSTEXPR14 auto inline begin(Container& cont)->decltype(cont.begin())
	// CONSTEXPR14 auto inline end(Container& cont)->decltype(cont.end())
	{
		FXD::Container::Vector< FXD::S32 > vecInt;
		PRINT_COND_ASSERT(FXD::STD::begin(vecInt) == FXD::STD::end(vecInt), "Iterator - CONSTEXPR14 auto inline begin(Container& cont)->decltype(cont.begin())");

		FXD::Container::LinkedList< FXD::S32 > listInt;
		PRINT_COND_ASSERT(FXD::STD::begin(listInt) == FXD::STD::end(listInt), "Iterator - CONSTEXPR14 auto inline begin(Container& cont)->decltype(cont.begin())");

		FXD::Container::Set< FXD::S32 > setInt;
		PRINT_COND_ASSERT(FXD::STD::begin(setInt) == FXD::STD::end(setInt), "Iterator - CONSTEXPR14 auto inline begin(Container& cont)->decltype(cont.begin())");

		FXD::Container::Array< FXD::S32, 0 > arrayInt;
		PRINT_COND_ASSERT(FXD::STD::begin(arrayInt) == FXD::STD::end(arrayInt), "Iterator - CONSTEXPR14 auto inline begin(Container& cont)->decltype(cont.begin())");

		FXD::Core::String8 str8;
		PRINT_COND_ASSERT(FXD::STD::begin(str8) == FXD::STD::end(str8), "Iterator - CONSTEXPR14 auto inline begin(Container& cont)->decltype(cont.begin())");
	}

	// CONSTEXPR14 auto data(Container& cont)->decltype(cont.data())
	// CONSTEXPR14 T* data(T(&arr)[SIZE]) NOEXCEPT
	// CONSTEXPR14 const T* data(std::initializer_list< T > iList) NOEXCEPT
	{
		FXD::Container::Array< FXD::S32, 0 > arrayInt;
		FXD::S32* parrayIntData = FXD::STD::data(arrayInt);
		PRINT_COND_ASSERT(parrayIntData == arrayInt.data(), "Iterator - CONSTEXPR14 auto data(Container& cont)->decltype(cont.data())");

		FXD::Container::Vector< FXD::S32 > vecInt;
		FXD::S32* pvecInttorData = FXD::STD::data(vecInt);
		PRINT_COND_ASSERT(pvecInttorData == vecInt.data(), "Iterator - CONSTEXPR14 auto data(Container& cont)->decltype(cont.data())");

		FXD::S32 carrayInt[34];
		FXD::S32* pIntCArray = FXD::STD::data(carrayInt);
		PRINT_COND_ASSERT(pIntCArray == carrayInt, "Iterator - CONSTEXPR14 T* data(T(&arr)[SIZE]) NOEXCEPT)");

		std::initializer_list< FXD::S32 > iListInt;
		const FXD::S32* pIListInt = FXD::STD::data(iListInt);
		PRINT_COND_ASSERT(pIListInt == iListInt.begin(), "Iterator - CONSTEXPR14 const T* data(std::initializer_list< T > iList) NOEXCEPT)");
	}

	// CONSTEXPR14 auto size(const C& c)->decltype(c.size())
	{
		FXD::Container::Vector< FXD::S32 > vecInt;
		vecInt.push_back();
		vecInt.push_back();
		vecInt.push_back();
		PRINT_COND_ASSERT(FXD::STD::size(vecInt) == 3, "Iterator - CONSTEXPR14 auto size(const C& c)->decltype(c.size())");

		FXD::S32 carrayInt[34];
		PRINT_COND_ASSERT(FXD::STD::size(carrayInt) == 34, "Iterator - CONSTEXPR14 auto size(const C& c)->decltype(c.size())");
	}

	// CONSTEXPR14 auto empty(const Container& cont)->decltype(cont.empty())
	// CONSTEXPR14 bool empty(std::initializer_list< T > iList) NOEXCEPT
	{
		FXD::Container::Vector< FXD::S32 > vecInt;
		PRINT_COND_ASSERT(FXD::STD::empty(vecInt), "Iterator - CONSTEXPR14 auto empty(const Container& cont)->decltype(cont.empty())");

		vecInt.push_back();
		PRINT_COND_ASSERT(!FXD::STD::empty(vecInt), "Iterator - CONSTEXPR14 auto empty(const Container& cont)->decltype(cont.empty())");

		std::initializer_list< FXD::S32 > iListInt;
		PRINT_COND_ASSERT(FXD::STD::empty(iListInt), "Iterator - CONSTEXPR14 bool empty(std::initializer_list< T > iList) NOEXCEPT");

		PRINT_COND_ASSERT(!FXD::STD::empty({ 1, 2, 3, 4, 5, 6 }), "Iterator - CONSTEXPR14 bool empty(std::initializer_list< T > iList) NOEXCEPT");
	}

	// Range-based for loops
	{
		FXD::Container::Vector< FXD::S32 > vecInt;
		FXD::S32 nIdx = 0;

		vecInt.push_back(0);
		vecInt.push_back(1);

		for (FXD::S32 n1 : vecInt)
		{
			PRINT_COND_ASSERT(n1 == nIdx++, "Iterator - Range-based for loops");
		}
	}

	// Range-based for loops
	{
		FXD::Core::String8 str8;
		FXD::Core::String8::value_type C = 'a';

		str8.push_back('a');
		str8.push_back('b');

		for (FXD::Core::String8::value_type n1 : str8)
		{
			PRINT_COND_ASSERT(n1 == C++, "Iterator - Range-based for loops");
		}
	}

	// is_iterator_wrapper
	{
		CTC_ASSERT((FXD::STD::is_iterator_wrapper< void >::value == false), "Iterator - is_iterator_wrapper");
		CTC_ASSERT((FXD::STD::is_iterator_wrapper< FXD::S32 >::value == false), "Iterator - is_iterator_wrapper");
		CTC_ASSERT((FXD::STD::is_iterator_wrapper< FXD::S32* >::value == false), "Iterator - is_iterator_wrapper");
		CTC_ASSERT((FXD::STD::is_iterator_wrapper< FXD::Container::Array< FXD::S8, 0 >* >::value == false), "Iterator - is_iterator_wrapper");
		CTC_ASSERT((FXD::STD::is_iterator_wrapper< FXD::Container::Vector< FXD::S8 > >::value == false), "Iterator - is_iterator_wrapper");
		CTC_ASSERT((FXD::STD::is_iterator_wrapper< FXD::STD::generic_iterator< FXD::S32* > >::value == true), "Iterator - is_iterator_wrapper");
		CTC_ASSERT((FXD::STD::is_iterator_wrapper< FXD::STD::move_iterator< FXD::Container::Array< FXD::S32, 0 >::iterator> >::value == true), "Iterator - is_iterator_wrapper");
	}

	// unwrap_iterator
	{
		FXD::S32 arrayInt[2];
		FXD::S32* pInt = FXD::STD::unwrap_iterator(&arrayInt[0]);
		arrayInt[0] = 17;
		PRINT_COND_ASSERT(*pInt == 17, "");
		CTC_ASSERT((FXD::STD::is_same< decltype(FXD::STD::unwrap_iterator(&arrayInt[0])), FXD::S32* >::value == true), "Iterator - unwrap_iterator");

		FXD::STD::generic_iterator< FXD::S32* > itrGenericInt(arrayInt);
		pInt = FXD::STD::unwrap_iterator(itrGenericInt);
		arrayInt[0] = 18;
		PRINT_COND_ASSERT(*pInt == 18, "Iterator - unwrap_iterator");
		CTC_ASSERT((FXD::STD::is_same< decltype(FXD::STD::unwrap_iterator(itrGenericInt)), FXD::S32* >::value == true), "Iterator - unwrap_iterator");

		FXD::Container::Vector< FXD::S32 > vecInt(4, 19);
		FXD::Container::Vector< FXD::S32 >::iterator itrVec = FXD::STD::unwrap_iterator(vecInt.begin());
		PRINT_COND_ASSERT(*itrVec == 19, "");
		CTC_ASSERT((FXD::STD::is_same< decltype(FXD::STD::unwrap_iterator(vecInt.begin())), FXD::Container::Vector< FXD::S32 >::iterator >::value == true), "Iterator - unwrap_iterator");

		FXD::STD::move_iterator< FXD::Container::Vector< FXD::S32 >::iterator > itrMoveVec(vecInt.begin());
		itrVec = FXD::STD::unwrap_iterator(itrMoveVec);
		vecInt[0] = 20;
		PRINT_COND_ASSERT(*itrVec == 20, "Iterator - unwrap_iterator");
		CTC_ASSERT((FXD::STD::is_same< decltype(FXD::STD::unwrap_iterator(itrMoveVec)), FXD::Container::Vector< FXD::S32 >::iterator>::value == true), "Iterator - unwrap_iterator");
	}

	return nErrorCount;
}