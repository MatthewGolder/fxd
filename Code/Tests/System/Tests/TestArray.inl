// Creator - MatthewGolder

template < typename ArrayT >
FXD::S32 TestArray(void)
{
	FXD::S32 nErrorCount = 0;

	{
		ArrayT array1 = { { 0, 1, 2, 3, 4 } };
		ArrayT array2 = { { 0, 1, 2, 3 } };
		ArrayT array3 = { { 4, 3, 2, 1, 0 } };
		//CLANGG
		//FXD::Container::Array< FXD::S32, 0 > array4 = ({ 0 });

		PRINT_COND_ASSERT(!array1.empty(), "Array - CONSTEXPR14 bool empty(void) const NOEXCEPT");
		PRINT_COND_ASSERT(array1.size() == 5, "Array - CONSTEXPR14 size_type size(void) const NOEXCEPT");
		PRINT_COND_ASSERT(array1[0] == 0, "Array - CONSTEXPR14 reference operator[](size_type nPos)");
		PRINT_COND_ASSERT(array1[4] == 4, "Array - CONSTEXPR14 reference operator[](size_type nPos)");

		PRINT_COND_ASSERT(!array2.empty(), "Array - CONSTEXPR14 bool empty(void) const NOEXCEPT");
		PRINT_COND_ASSERT(array2.size() == 5, "Array - CONSTEXPR14 size_type size(void) const NOEXCEPT");
		PRINT_COND_ASSERT(array2[0] == 0, "Array - CONSTEXPR14 reference operator[](size_type nPos)");
		PRINT_COND_ASSERT(array2[3] == 3, "Array - CONSTEXPR14 reference operator[](size_type nPos)");

		//PRINT_COND_ASSERT(array4.empty(), "Array - CONSTEXPR14 bool empty(void) const NOEXCEPT");
		//PRINT_COND_ASSERT(array4.size() == 0, "Array - CONSTEXPR14 size_type size(void) const NOEXCEPT");

		// void swap(my_type& rhs)
		{
			array1.swap(array3);
			PRINT_COND_ASSERT(array1[0] == 4, "Array - void swap(my_type& rhs)");
			PRINT_COND_ASSERT(array3[0] == 0, "Array - void swap(my_type& rhs)");
		}

		// CONSTEXPR14 iterator begin(void) NOEXCEPT
		// CONSTEXPR14 iterator end(void) NOEXCEPT
		{
			typename ArrayT::iterator itr = array1.begin();
			PRINT_COND_ASSERT((*itr == 4), "Array - CONSTEXPR14 iterator begin(void) NOEXCEPT");

			++itr;
			PRINT_COND_ASSERT((*itr == 3), "Array - CONSTEXPR14 iterator begin(void) NOEXCEPT");

			++itr;
			PRINT_COND_ASSERT((*itr == 2), "Array - CONSTEXPR14 iterator begin(void) NOEXCEPT");

			--itr;
			PRINT_COND_ASSERT((*itr == 3), "Array - CONSTEXPR14 iterator begin(void) NOEXCEPT");

			itr += 3;
			PRINT_COND_ASSERT((*itr == 0), "Array - CONSTEXPR14 iterator begin(void) NOEXCEPT");

			++itr;
			PRINT_COND_ASSERT((itr == array1.end()), "Array - CONSTEXPR14 iterator end(void) NOEXCEPT");
		}

		// CONSTEXPR14 reverse_iterator rbegin(void) NOEXCEPT
		// CONSTEXPR14 reverse_iterator rend(void) NOEXCEPT
		{
			typename ArrayT::reverse_iterator itr = array1.rbegin();
			PRINT_COND_ASSERT((*itr == 0), "Array - CONSTEXPR14 reverse_iterator rbegin(void) NOEXCEPT");

			itr++;
			PRINT_COND_ASSERT((*itr == 1), "Array - CONSTEXPR14 reverse_iterator rbegin(void) NOEXCEPT");
		}

		// CONSTEXPR14 const_pointer data(void) const NOEXCEPT
		{
			FXD::S32* pArray = array1.data();
			PRINT_COND_ASSERT((pArray == array1.data()), "Array - CONSTEXPR14 const_pointer data(void) const NOEXCEPT");
		}

		// CONSTEXPR14 reference front(void)
		{
			FXD::S32& nFront = array1.front();
			PRINT_COND_ASSERT((nFront == 4), "Array - CONSTEXPR14 reference front(void)");
		}

		// CONSTEXPR14 const_reference back(void) const
		{
			FXD::S32& nBack = array1.back();
			PRINT_COND_ASSERT((nBack == 0), "Array - CONSTEXPR14 const_reference back(void) const");
		}


		// CONSTEXPR14 bool operator==(const Array< Type, SIZE >& lhs, const Array< Type, SIZE >& rhs)
		// CONSTEXPR14 bool operator!=(const Array< Type, SIZE >& lhs, const Array< Type, SIZE >& rhs)
		// CONSTEXPR14 bool operator<=(const Array< Type, SIZE >& lhs, const Array< Type, SIZE >& rhs)
		// CONSTEXPR14 bool operator>=(const Array< Type, SIZE >& lhs, const Array< Type, SIZE >& rhs)
		// CONSTEXPR14 bool operator<(const Array< Type, SIZE >& lhs, const Array< Type, SIZE >& rhs)
		// CONSTEXPR14 bool operator>(const Array< Type, SIZE >& lhs, const Array< Type, SIZE >& rhs)
		{
			array1[0] = 0; array1[1] = 1; array1[2] = 2; array1[3] = 3; array1[4] = 4; // 01234
			array2[0] = 0; array2[1] = 1; array2[2] = 2; array2[3] = 3; array2[4] = 4; // 01234
			array3[0] = 0; array3[1] = 1; array3[2] = 2; array3[3] = 3; array3[4] = 9; // 01239

			PRINT_COND_ASSERT( (array1 == array2), "Array - CONSTEXPR14 bool operator==(const Array< Type, SIZE >& lhs, const Array< Type, SIZE >& rhs)");
			PRINT_COND_ASSERT(!(array1 != array2), "Array - CONSTEXPR14 bool operato!=(const Array< Type, SIZE >& lhs, const Array< Type, SIZE >& rhs)");
			PRINT_COND_ASSERT( (array1 <= array2), "Array - CONSTEXPR14 bool operator<=(const Array< Type, SIZE >& lhs, const Array< Type, SIZE >& rhs)");
			PRINT_COND_ASSERT( (array1 >= array2), "Array - CONSTEXPR14 bool operator>=(const Array< Type, SIZE >& lhs, const Array< Type, SIZE >& rhs)");
			PRINT_COND_ASSERT(!(array1 < array2), "Array - CONSTEXPR14 bool operator<(const Array< Type, SIZE >& lhs, const Array< Type, SIZE >& rhs)");
			PRINT_COND_ASSERT(!(array1 > array2), "Array - CONSTEXPR14 bool operator>(const Array< Type, SIZE >& lhs, const Array< Type, SIZE >& rhs)");

			PRINT_COND_ASSERT(!(array1 == array3), "Array - CONSTEXPR14 bool operator==(const Array< Type, SIZE >& lhs, const Array< Type, SIZE >& rhs)");
			PRINT_COND_ASSERT( (array1 != array3), "Array - CONSTEXPR14 bool operator!=(const Array< Type, SIZE >& lhs, const Array< Type, SIZE >& rhs)");
			PRINT_COND_ASSERT( (array1 <= array3), "Array - CONSTEXPR14 bool operator<=(const Array< Type, SIZE >& lhs, const Array< Type, SIZE >& rhs)");
			PRINT_COND_ASSERT(!(array1 >= array3), "Array - CONSTEXPR14 bool operator>=(const Array< Type, SIZE >& lhs, const Array< Type, SIZE >& rhs)");
			PRINT_COND_ASSERT( (array1 < array3), "Array - CONSTEXPR14 bool operator>(const Array< Type, SIZE >& lhs, const Array< Type, SIZE >& rhs)");
			PRINT_COND_ASSERT(!(array1 > array3), "Array - CONSTEXPR14 bool operator>(const Array< Type, SIZE >& lhs, const Array< Type, SIZE >& rhs)");
		}
	}

	// NO_CONSTEXPR14 tests
	{
#ifndef NO_CONSTEXPR14
		/*
		ArrayT array1 = { { 0, 1, 2, 3, 4 } };
		{
			CTC_ASSERT((array1 == array1), "Array - CONSTEXPR14 bool operator==(const FXD::Container::Array< Type, SIZE >& lhs, const FXD::Container::Array< Type, SIZE >& rhs)");
		}

		// CONSTEXPR14 const_reference operator[](size_type nPos) const
		{
			CTC_ASSERT((array1[0] == 0), "Array - CONSTEXPR14 const_reference operator[](size_type nPos) const");
			CTC_ASSERT((array1[1] == 1), "Array - CONSTEXPR14 const_reference operator[](size_type nPos) const");
			CTC_ASSERT((array1[2] == 2), "Array - CONSTEXPR14 const_reference operator[](size_type nPos) const");
			CTC_ASSERT((array1[3] == 3), "Array - CONSTEXPR14 const_reference operator[](size_type nPos) const");
			CTC_ASSERT((array1[4] == 4), "Array - CONSTEXPR14 const_reference operator[](size_type nPos) const");
		}

		// CONSTEXPR14 const_reference at(size_type nPos) const
		{
			CTC_ASSERT(array1.at(0) == 0, "Array - CONSTEXPR14 const_reference at(size_type nPos) const");
			CTC_ASSERT(array1.at(1) == 1, "Array - CONSTEXPR14 const_reference at(size_type nPos) const");
			CTC_ASSERT(array1.at(2) == 2, "Array - CONSTEXPR14 const_reference at(size_type nPos) const");
			CTC_ASSERT(array1.at(3) == 3, "Array - CONSTEXPR14 const_reference at(size_type nPos) const");
			CTC_ASSERT(array1.at(4) == 4, "Array - CONSTEXPR14 const_reference at(size_type nPos) const");
		}

		// CONSTEXPR14 const_pointer data(void) const NOEXCEPT
		{
			CTC_ASSERT(array1.data()[0] == 0, "Array - CONSTEXPR14 const_pointer data(void) const NOEXCEPT");
			CTC_ASSERT(array1.data()[1] == 1, "Array - CONSTEXPR14 const_pointer data(void) const NOEXCEPT");
			CTC_ASSERT(array1.data()[2] == 2, "Array - CONSTEXPR14 const_pointer data(void) const NOEXCEPT");
			CTC_ASSERT(array1.data()[3] == 3, "Array - CONSTEXPR14 const_pointer data(void) const NOEXCEPT");
			CTC_ASSERT(array1.data()[4] == 4, "Array - CONSTEXPR14 const_pointer data(void) const NOEXCEPT");
		}

		// CONSTEXPR14 bool empty(void) const NOEXCEPT
		// CONSTEXPR14 bool not_empty(void) const NOEXCEPT
		// CONSTEXPR14 size_type size(void) const NOEXCEPT
		{
			CTC_ASSERT(!array1.empty(), "Array - CONSTEXPR14 bool empty(void) const NOEXCEPT");
			CTC_ASSERT(array1.not_empty(), "Array - CONSTEXPR14 bool not_empty(void) const NOEXCEPT");
			CTC_ASSERT(array1.size() == 5, "Array - CONSTEXPR14 size_type size(void) const NOEXCEPT");
		}

		// CONSTEXPR14 const_reference front(void) const
		{
			CTC_ASSERT(array1.front() == 0, "Array - CONSTEXPR14 const_reference front(void) const");
			CTC_ASSERT(array1.back() == 3, "Array - CONSTEXPR14 const_reference back(void) const");
		}

		// CONSTEXPR14 const_iterator begin(void) const NOEXCEPT`
		{
			CTC_ASSERT((array1.begin()[0] == 0), "Array - CONSTEXPR14 const_iterator begin(void) const NOEXCEPT");
			CTC_ASSERT((array1.begin()[1] == 1), "Array - CONSTEXPR14 const_iterator begin(void) const NOEXCEPT");
			CTC_ASSERT((array1.begin()[2] == 2), "Array - CONSTEXPR14 const_iterator begin(void) const NOEXCEPT");
			CTC_ASSERT((array1.begin()[3] == 3), "Array - CONSTEXPR14 const_iterator begin(void) const NOEXCEPT");
			CTC_ASSERT((array1.begin()[4] == 4), "Array - CONSTEXPR14 const_iterator begin(void) const NOEXCEPT");
		}

		// CONSTEXPR14 const_iterator cbegin(void) const NOEXCEPT
		{
			CTC_ASSERT((array1.cbegin()[0] == 0), "Array - CONSTEXPR14 const_iterator cbegin(void) const NOEXCEPT");
			CTC_ASSERT((array1.cbegin()[1] == 1), "Array - CONSTEXPR14 const_iterator cbegin(void) const NOEXCEPT");
			CTC_ASSERT((array1.cbegin()[2] == 2), "Array - CONSTEXPR14 const_iterator cbegin(void) const NOEXCEPT");
			CTC_ASSERT((array1.cbegin()[3] == 3), "Array - CONSTEXPR14 const_iterator cbegin(void) const NOEXCEPT");
			CTC_ASSERT((array1.cbegin()[4] == 4), "Array - CONSTEXPR14 const_iterator cbegin(void) const NOEXCEPT");
		}

		// CONSTEXPR14 const_reverse_iterator crbegin(void) const NOEXCEPT
		{
			CTC_ASSERT((array1.crbegin()[0] == 4), "Array - CONSTEXPR14 const_reverse_iterator crbegin(void) const NOEXCEPT");
			CTC_ASSERT((array1.crbegin()[1] == 3), "Array - CONSTEXPR14 const_reverse_iterator crbegin(void) const NOEXCEPT");
			CTC_ASSERT((array1.crbegin()[2] == 2), "Array - CONSTEXPR14 const_reverse_iterator crbegin(void) const NOEXCEPT");
			CTC_ASSERT((array1.crbegin()[3] == 1), "Array - CONSTEXPR14 const_reverse_iterator crbegin(void) const NOEXCEPT");
			CTC_ASSERT((array1.crbegin()[4] == 0), "Array - CONSTEXPR14 const_reverse_iterator crbegin(void) const NOEXCEPT");
		}

		// CONSTEXPR14 const_iterator end(void) const NOEXCEPT
		{
			CTC_ASSERT((array1.end()[-1] == 4), "Array - CONSTEXPR14 const_iterator end(void) const NOEXCEPT");
			CTC_ASSERT((array1.end()[-2] == 3), "Array - CONSTEXPR14 const_iterator end(void) const NOEXCEPT");
			CTC_ASSERT((array1.end()[-3] == 2), "Array - CONSTEXPR14 const_iterator end(void) const NOEXCEPT");
			CTC_ASSERT((array1.end()[-4] == 1), "Array - CONSTEXPR14 const_iterator end(void) const NOEXCEPT");
			CTC_ASSERT((array1.end()[-5] == 0), "Array - CONSTEXPR14 const_iterator end(void) const NOEXCEPT");
		}

		// CONSTEXPR14 const_iterator cend(void) const NOEXCEPT
		{
			CTC_ASSERT((array1.cend()[-1] == 4), "Array - CONSTEXPR14 const_iterator cend(void) const NOEXCEPT");
			CTC_ASSERT((array1.cend()[-2] == 3), "Array - CONSTEXPR14 const_iterator cend(void) const NOEXCEPT");
			CTC_ASSERT((array1.cend()[-3] == 2), "Array - CONSTEXPR14 const_iterator cend(void) const NOEXCEPT");
			CTC_ASSERT((array1.cend()[-4] == 1), "Array - CONSTEXPR14 const_iterator cend(void) const NOEXCEPT");
			CTC_ASSERT((array1.cend()[-5] == 0), "Array - CONSTEXPR14 const_iterator cend(void) const NOEXCEPT");
		}

		// CONSTEXPR14 const_reverse_iterator crend(void) const NOEXCEPT
		{
			CTC_ASSERT((array1.crend()[-1] == 0), "Array - CONSTEXPR14 const_reverse_iterator crend(void) const NOEXCEPT");
			CTC_ASSERT((array1.crend()[-2] == 1), "Array - CONSTEXPR14 const_reverse_iterator crend(void) const NOEXCEPT");
			CTC_ASSERT((array1.crend()[-3] == 2), "Array - CONSTEXPR14 const_reverse_iterator crend(void) const NOEXCEPT");
			CTC_ASSERT((array1.crend()[-4] == 3), "Array - CONSTEXPR14 const_reverse_iterator crend(void) const NOEXCEPT");
			CTC_ASSERT((array1.crend()[-5] == 4), "Array - CONSTEXPR14 const_reverse_iterator crend(void) const NOEXCEPT");
		}
		*/
#endif // NO_CONSTEXPR14
	}

	return nErrorCount;
}

FXD::S32 TestArray20(void)
{
	FXD::S32 nErrorCount = 0;

	// to_array
	{
		{
			CONSTEXPR FXD::S32 array1[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
			CONSTEXPR auto arr = FXD::STD::to_array(array1);

			CTC_ASSERT((FXD::STD::is_same< typename FXD::STD::remove_cv< decltype(arr) >::type, FXD::Container::Array< FXD::S32, 10 > >::value), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[0] == 0), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[1] == 1), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[2] == 2), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[3] == 3), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[4] == 4), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[5] == 5), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[6] == 6), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[7] == 7), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[8] == 8), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[9] == 9), "CONSTEXPR to_array(Type(&a)[N])");
		}

		{
			CONSTEXPR auto arr = FXD::STD::to_array({ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });

			CTC_ASSERT((FXD::STD::is_same< typename FXD::STD::remove_cv< decltype(arr) >::type, FXD::Container::Array< FXD::S32, 10 > >::value), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[0] == 0), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[1] == 1), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[2] == 2), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[3] == 3), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[4] == 4), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[5] == 5), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[6] == 6), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[7] == 7), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[8] == 8), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[9] == 9), "CONSTEXPR to_array(Type(&a)[N])");
		}

		{
			/*CLANGG
			CONSTEXPR auto arr = FXD::STD::to_array< FXD::S64 >({ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });

			CTC_ASSERT((FXD::STD::is_same< typename FXD::STD::remove_cv< decltype(arr) >::type, FXD::Container::Array< FXD::S64, 10 > >::value), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[0] == 0), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[1] == 1), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[2] == 2), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[3] == 3), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[4] == 4), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[5] == 5), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[6] == 6), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[7] == 7), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[8] == 8), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[9] == 9), "CONSTEXPR to_array(Type(&a)[N])");
			*/
		}

		{
			/*CLANGG
			CONSTEXPR auto arr = FXD::STD::to_array< FXD::U64 >({ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });

			CTC_ASSERT((FXD::STD::is_same< typename FXD::STD::remove_cv< decltype(arr) >::type, FXD::Container::Array< FXD::U64, 10 > >::value), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[0] == 0), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[1] == 1), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[2] == 2), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[3] == 3), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[4] == 4), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[5] == 5), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[6] == 6), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[7] == 7), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[8] == 8), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT((arr[9] == 9), "CONSTEXPR to_array(Type(&a)[N])");
			*/
		}

		{
			CONSTEXPR auto arr = FXD::STD::to_array("FUNKY");

			CTC_ASSERT((FXD::STD::is_same< typename FXD::STD::remove_cv< decltype(arr) >::type, FXD::Container::Array< FXD::UTF8, 6 > >::value), "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT(arr[0] == 'F', "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT(arr[1] == 'U', "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT(arr[2] == 'N', "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT(arr[3] == 'K', "CONSTEXPR to_array(Type(&a)[N])");
			CTC_ASSERT(arr[4] == 'Y', "CONSTEXPR to_array(Type(&a)[N])");
		}
	}

	return nErrorCount;
}