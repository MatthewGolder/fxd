// Creator - MatthewGolder
#include "Tests/System/Tests/FXDTest.h"
#include "Tests/System/TestHelperClasses.h"
#include "FXDEngine/Container/LinkedList.h"

// Template instantations.
// These tell the compiler to compile all the functions for the given class.
template class FXD::Container::LinkedList< bool >;
template class FXD::Container::LinkedList< FXD::S32 >;
template class FXD::Container::LinkedList< Game::Align64 >;
template class FXD::Container::LinkedList< TestObject >;
//template class FXD::Container::LinkedList< std::unique_ptr< FXD::S32 > >;


FXD::S32 TestList(void)
{
	FXD::S32 nErrorCount = 0;

	// LinkedList(void);
	{
		FXD::Container::LinkedList< FXD::S32 > list;
		PRINT_COND_ASSERT((list.size() == 0), "LinkedList - LinkedList(void)");
		PRINT_COND_ASSERT(list.empty(), "LinkedList - LinkedList(void)");
		PRINT_COND_ASSERT(list.validate(), "LinkedList - LinkedList(void)");
		PRINT_COND_ASSERT((list.begin() == list.end()), "LinkedList - LinkedList(void)");
	}

	Game::MallocAllocator::reset_all();

	// LinkedList(const allocator_type& allocator);
	{
		Game::MallocAllocator mallocator;
		{
			FXD::Container::LinkedList< FXD::S32, Game::MallocAllocator > list(mallocator);
			PRINT_COND_ASSERT((list.allocator() == mallocator), "LinkedList - LinkedList(const allocator_type& alloc)");
			list.push_front(42);
			PRINT_COND_ASSERT((Game::MallocAllocator::m_nAllocCountAll != 0), "LinkedList - LinkedList(const allocator_type& alloc)");
		}
		PRINT_COND_ASSERT(Game::MallocAllocator::m_nAllocCountAll == Game::MallocAllocator::m_nFreeCountAll, "LinkedList - LinkedList(const allocator_type& alloc)");
	}

	// LinkedList(size_type nCount, const allocator_type& alloc = allocator_type());
	{
		const FXD::S32 nTestSize = 42;
		FXD::Container::LinkedList< FXD::S32 > list(nTestSize);
		PRINT_COND_ASSERT(!list.empty(), "LinkedList - LinkedList(size_type nCount, const allocator_type& alloc = allocator_type())");
		PRINT_COND_ASSERT((list.size() == nTestSize), "LinkedList - LinkedList(size_type nCount, const allocator_type& alloc = allocator_type())");
		PRINT_COND_ASSERT(list.validate(), "LinkedList - LinkedList(size_type nCount, const allocator_type& alloc = allocator_type())");
		PRINT_COND_ASSERT(FXD::STD::all_of(list.begin(), list.end(), [](FXD::S32 e) { return e == 0; }), "LinkedList - LinkedList(size_type nCount, const allocator_type& alloc = allocator_type())");
	}

	// LinkedList(size_type nCount, const T& val, const allocator_type& alloc = allocator_type());
	{
		const FXD::S32 nTestSize = 42;
		const FXD::S32 nTestVal = 435;

		FXD::Container::LinkedList< FXD::S32 > list(42, nTestVal);
		PRINT_COND_ASSERT(!list.empty(), "LinkedList - LinkedList(size_type nCount, const T& val, const allocator_type& alloc = allocator_type())");
		PRINT_COND_ASSERT((list.size() == nTestSize), "LinkedList - LinkedList(size_type nCount, const T& val, const allocator_type& alloc = allocator_type())");
		PRINT_COND_ASSERT(list.validate(), "LinkedList - LinkedList(size_type nCount, const T& val, const allocator_type& alloc = allocator_type())");
		PRINT_COND_ASSERT(FXD::STD::all_of(list.begin(), list.end(), [=](FXD::S32 nVal)
		{
			return (nVal == nTestVal);
		}), "LinkedList - LinkedList(size_type nCount, const T& val, const allocator_type& alloc = allocator_type())");
	}

	// LinkedList(const my_type& rhs);
	{
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list2(list1);
		PRINT_COND_ASSERT((list1 == list2), "LinkedList - LinkedList(const my_type& rhs)");
		PRINT_COND_ASSERT(list1.validate(), "LinkedList - LinkedList(const my_type& rhs)");
		PRINT_COND_ASSERT((list1.size() == list2.size()), "LinkedList - LinkedList(const my_type& rhs)");
		PRINT_COND_ASSERT(list2.validate(), "LinkedList - LinkedList(const my_type& rhs)");
	}

	// LinkedList(const my_type& rhs, const allocator_type& alloc);
	{
		Game::MallocAllocator mallocator;
		FXD::Container::LinkedList< FXD::S32, Game::MallocAllocator > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32, Game::MallocAllocator > list2(list1, mallocator);
		PRINT_COND_ASSERT((list1 == list2), "LinkedList - LinkedList(const my_type& rhs, const allocator_type& alloc)");
		PRINT_COND_ASSERT(list1.validate(), "LinkedList - LinkedList(const my_type& rhs, const allocator_type& alloc)");
		PRINT_COND_ASSERT((list1.size() == list2.size()), "LinkedList - LinkedList(const my_type& rhs, const allocator_type& alloc)");
		PRINT_COND_ASSERT(list2.validate(), "LinkedList - LinkedList(const my_type& rhs, const allocator_type& alloc)");
		PRINT_COND_ASSERT((list1.allocator() == list2.allocator()), "LinkedList - LinkedList(const my_type& rhs, const allocator_type& alloc)");
	}

	// LinkedList(std::initializer_list< value_type > iList, const allocator_type& alloc = allocator_type());
	// LinkedList(my_type&& rhs);
	{
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		PRINT_COND_ASSERT(!list1.empty(), "LinkedList - LinkedList(std::initializer_list< value_type > iList, const allocator_type& alloc = allocator_type())");
		PRINT_COND_ASSERT((list1.size() == 10), "LinkedList - LinkedList(std::initializer_list< value_type > iList, const allocator_type& alloc = allocator_type())");
		PRINT_COND_ASSERT(list1.validate(), "LinkedList - LinkedList(std::initializer_list< value_type > iList, const allocator_type& alloc = allocator_type())");

		FXD::Container::LinkedList< FXD::S32 > list2(std::move(list1));
		PRINT_COND_ASSERT(list1.empty(), "LinkedList - LinkedList(my_type&& rhs)");
		PRINT_COND_ASSERT(!list2.empty(), "LinkedList - LinkedList(my_type&& rhs)");
		PRINT_COND_ASSERT((list1.size() == 0), "LinkedList - LinkedList(my_type&& rhs)");
		PRINT_COND_ASSERT((list2.size() == 10), "LinkedList - LinkedList(my_type&& rhs)");
		PRINT_COND_ASSERT((list1 != list2), "LinkedList - LinkedList(my_type&& rhs)");
		PRINT_COND_ASSERT((list1.size() != list2.size()), "LinkedList - LinkedList(my_type&& rhs)");
		PRINT_COND_ASSERT(list1.validate(), "LinkedList - LinkedList(my_type&& rhs)");
		PRINT_COND_ASSERT(list2.validate(), "LinkedList - LinkedList(my_type&& rhs)");
	}

	// LinkedList(std::initializer_list< value_type > ilist);
	{
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::STD::for_each(list1.begin(), list1.end(), [&](FXD::S32 e)
		{
			static FXD::S32 nInc = 0;
			PRINT_COND_ASSERT((nInc++ == e), "LinkedList - LinkedList(std::initializer_list< value_type > iList, const allocator_type& alloc = allocator_type())");
		});
	}

	// LinkedList(InputItr first, InputItr last); 
	{
		FXD::Container::LinkedList< FXD::S32 > listRef = { 3, 4, 5, 6, 7 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

		auto start = list1.begin();
		FXD::STD::advance(start, 3);

		auto end = start;
		FXD::STD::advance(end, 5);

		FXD::Container::LinkedList< FXD::S32 > list2(start, end);

		PRINT_COND_ASSERT((list2 == listRef), "LinkedList - LinkedList(InputItr first, InputItr last)");
		PRINT_COND_ASSERT(list1.validate(), "LinkedList - LinkedList(InputItr first, InputItr last)");
		PRINT_COND_ASSERT(list2.validate(), "LinkedList - LinkedList(InputItr first, InputItr last)");

		PRINT_COND_ASSERT((list1.size() == 10), "LinkedList - LinkedList(InputItr first, InputItr last)");
		PRINT_COND_ASSERT((list2.size() == 5), "LinkedList - LinkedList(InputItr first, InputItr last)");

		PRINT_COND_ASSERT(!list2.empty(), "LinkedList - LinkedList(InputItr first, InputItr last)");
		PRINT_COND_ASSERT(!list1.empty(), "LinkedList - LinkedList(InputItr first, InputItr last)");
	}

	// my_type& operator=(const my_type& rhs);
	// my_type& operator=(std::initializer_list< value_type > iList);
	// my_type& operator=(my_type&& rhs);
	{
		const FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list2 = list1;
		PRINT_COND_ASSERT(list1.validate(), "LinkedList - my_type& operator=(std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT(list2.validate(), "LinkedList - my_type& operator=(std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT((list1.size() == 10), "LinkedList - my_type& operator=(std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT((list2.size() == 10), "LinkedList - my_type& operator=(std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT(!list1.empty(), "LinkedList - my_type& operator=(std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT(!list2.empty(), "LinkedList - my_type& operator=(std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT((list2 == list1), "LinkedList - my_type& operator=(std::initializer_list< value_type > iList)");

		FXD::Container::LinkedList< FXD::S32 > list3 = std::move(list2);
		PRINT_COND_ASSERT(list2.empty(), "LinkedList - my_type& operator=(my_type&& rhs)");
		PRINT_COND_ASSERT((list3 == list1), "LinkedList - my_type& operator=(my_type&& rhs)");
		PRINT_COND_ASSERT((list3.size() == 10), "LinkedList - my_type& operator=(my_type&& rhs)");
		PRINT_COND_ASSERT(list3.validate(), "LinkedList - my_type& operator=(my_type&& rhs)");
	}

	// void swap(my_type& rhs);
	{
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > array2 = {};

		PRINT_COND_ASSERT(list1.validate(), "LinkedList - void swap(my_type& rhs)");
		PRINT_COND_ASSERT(array2.validate(), "LinkedList - void swap(my_type& rhs)");
		PRINT_COND_ASSERT(!list1.empty(), "LinkedList - void swap(my_type& rhs)");
		PRINT_COND_ASSERT(array2.empty(), "LinkedList - void swap(my_type& rhs)");

		array2.swap(list1);

		PRINT_COND_ASSERT(list1.validate(), "LinkedList - void swap(my_type& rhs)");
		PRINT_COND_ASSERT(array2.validate(), "LinkedList - void swap(my_type& rhs)");
		PRINT_COND_ASSERT(list1.empty(), "LinkedList - void swap(my_type& rhs)");
		PRINT_COND_ASSERT(!array2.empty(), "LinkedList - void swap(my_type& rhs)");
	}

	// void assign(size_type nCount, const value_type& val);
	{
		FXD::Container::LinkedList< FXD::S32 > listRef = { 42, 42, 42, 42 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3 };
		list1.assign(4, 42);
		PRINT_COND_ASSERT((list1 == listRef), "LinkedList - void assign(size_type nCount, const value_type& val)");
		PRINT_COND_ASSERT(list1.validate(), "LinkedList - void assign(size_type nCount, const value_type& val)");
		PRINT_COND_ASSERT(!list1.empty(), "LinkedList - void assign(size_type nCount, const value_type& val)");
		PRINT_COND_ASSERT((list1.size() == 4), "LinkedList - void assign(size_type nCount, const value_type& val)");
	}

	// void assign(InputItr first, InputItr last);
	{
		FXD::Container::LinkedList< FXD::S32 > listRef = FXD::Container::LinkedList< FXD::S32 >{ 3, 4, 5, 6, 7 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > array2;

		auto start = list1.begin();
		FXD::STD::advance(start, 3);

		auto end = start;
		FXD::STD::advance(end, 5);

		array2.assign(start, end);

		PRINT_COND_ASSERT((array2 == listRef), "LinkedList - void assign(InputItr first, InputItr last)");
		PRINT_COND_ASSERT(list1.validate(), "LinkedList - void assign(InputItr first, InputItr last)");
		PRINT_COND_ASSERT(array2.validate(), "LinkedList - void assign(InputItr first, InputItr last)");

		PRINT_COND_ASSERT((list1.size() == 10), "LinkedList - void assign(InputItr first, InputItr last)");
		PRINT_COND_ASSERT((array2.size() == 5), "LinkedList - void assign(InputItr first, InputItr last)");

		PRINT_COND_ASSERT(!array2.empty(), "LinkedList - void assign(InputItr first, InputItr last)");
		PRINT_COND_ASSERT(!list1.empty(), "LinkedList - void assign(InputItr first, InputItr last)");
	}

	// void assign(std::initializer_list< value_type > iList);
	{
		FXD::Container::LinkedList< FXD::S32 > listRef = FXD::Container::LinkedList< FXD::S32 >{ 3, 4, 5, 6, 7 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > array2;

		auto start = list1.begin();
		FXD::STD::advance(start, 3);

		auto end = start;
		FXD::STD::advance(end, 5);

		array2.assign(start, end);

		PRINT_COND_ASSERT((array2 == listRef), "LinkedList - void assign(std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT(list1.validate(), "LinkedList - void assign(std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT(array2.validate(), "LinkedList - void assign(std::initializer_list< value_type > iList)");

		PRINT_COND_ASSERT((list1.size() == 10), "LinkedList - void assign(std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT((array2.size() == 5), "LinkedList - void assign(std::initializer_list< value_type > iList)");

		PRINT_COND_ASSERT(!array2.empty(), "LinkedList - void assign(std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT(!list1.empty(), "LinkedList - void assign(std::initializer_list< value_type > iList)");
	}

	// iterator begin(void) 
	// const_iterator begin(void) const 
	// const_iterator cbegin(void) const 
	// iterator end(void) 
	// const_iterator end(void) const 
	// const_iterator cend(void) const 
	{
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		{
			static FXD::S32 nInc = 0;
			auto itr = list1.begin();
			while (itr != list1.end())
			{
				PRINT_COND_ASSERT((*itr++ == nInc++), "LinkedList - const_iterator begin(void) const");
			}
		}
		{
			static FXD::S32 nInc = 0;
			auto itr = list1.cbegin();
			while (itr != list1.cend())
			{
				PRINT_COND_ASSERT((*itr++ == nInc++), "LinkedList - const_iterator cbegin(void) const");
			}
		}
	}

	// reverse_iterator rbegin(void)
	// const_reverse_iterator rbegin(void) const
	// const_reverse_iterator crbegin(void) const
	// reverse_iterator rend(void)
	// const_reverse_iterator rend(void) const
	// const_reverse_iterator crend(void) const 
	{
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		{
			static FXD::S32 inc = 9;
			auto itr = list1.rbegin();
			while (itr != list1.rend())
			{
				PRINT_COND_ASSERT((*itr == inc--), "LinkedList - reverse_iterator rbegin(void) const");
				itr++;
			}
		}
		{
			static FXD::S32 inc = 9;
			auto itr = list1.crbegin();
			while (itr != list1.crend())
			{
				PRINT_COND_ASSERT((*itr == inc--), "LinkedList - const_reverse_iterator crbegin(void) const");
				itr++;
			}
		}
	}

	// bool empty(void) const
	{
		{
			FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
			PRINT_COND_ASSERT(!list1.empty(), "LinkedList - bool empty(void) const");
		}
		{
			FXD::Container::LinkedList< FXD::S32 > list1 = {};
			PRINT_COND_ASSERT(list1.empty(), "LinkedList - bool empty(void) const");
		}
	}

	// size_type size(void) const
	{
		{
			FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
			PRINT_COND_ASSERT((list1.size() == 10), "LinkedList - size_type size(void) const");
		}
		{
			FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4 };
			PRINT_COND_ASSERT((list1.size() == 5), "LinkedList - size_type size(void) const");
		}
		{
			FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1 };
			PRINT_COND_ASSERT((list1.size() == 2), "LinkedList - size_type size(void) const");
		}
		{
			FXD::Container::LinkedList< FXD::S32 > list1 = {};
			PRINT_COND_ASSERT((list1.size() == 0), "LinkedList - size_type size(void) const");
		}
	}

	// void resize(size_type nCount);
	// void resize(size_type nCount, const value_type& val);
	{
		{
			FXD::Container::LinkedList< FXD::S32 > list1;
			list1.resize(10);
			PRINT_COND_ASSERT((list1.size() == 10), "LinkedList - void resize(size_type nCount)");
			PRINT_COND_ASSERT(!list1.empty(), "LinkedList - void resize(size_type nCount)");
			PRINT_COND_ASSERT(FXD::STD::all_of(list1.begin(), list1.end(), [](FXD::S32 nIdx) { return (nIdx == 0); }), "LinkedList - void resize(size_type nCount)");
		}
		{
			FXD::Container::LinkedList< FXD::S32 > list1;
			list1.resize(10, 42);
			PRINT_COND_ASSERT((list1.size() == 10), "LinkedList - void resize(size_type nCount, const value_type& val)");
			PRINT_COND_ASSERT(!list1.empty(), "LinkedList - void resize(size_type nCount, const value_type& val)");
			PRINT_COND_ASSERT(FXD::STD::all_of(list1.begin(), list1.end(), [](FXD::S32 nIdx) { return (nIdx == 42); }), "LinkedList - void resize(size_type nCount, const value_type& val)");
		}
	}

	// reference front(void);
	// const_reference front(void) const;
	{
		{
			FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
			PRINT_COND_ASSERT((list1.front() == 0), "LinkedList - reference front(void)");
			list1.front() = 42;
			PRINT_COND_ASSERT((list1.front() == 42), "LinkedList - reference front(void)");
		}
		{
			const FXD::Container::LinkedList< FXD::S32 > list1 = { 5, 6, 7, 8, 9 };
			PRINT_COND_ASSERT((list1.front() == 5), "LinkedList - reference front(void)");
		}
		{
			FXD::Container::LinkedList< FXD::S32 > list1 = { 9 };
			PRINT_COND_ASSERT((list1.front() == 9), "LinkedList - reference front(void)");
			list1.front() = 42;
			PRINT_COND_ASSERT((list1.front() == 42), "LinkedList - reference front(void)");
		}
	}

	// reference back(void);
	// const_reference back(void) const;
	{
		{
			FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
			PRINT_COND_ASSERT(list1.back() == 9, "LinkedList - reference back(void)");
			list1.back() = 42;
			PRINT_COND_ASSERT(list1.back() == 42, "LinkedList - reference back(void)");
		}
		{
			const FXD::Container::LinkedList< FXD::S32 > list1 = { 5, 6, 7, 8, 9 };
			PRINT_COND_ASSERT(list1.back() == 9, "LinkedList - reference back(void)");
		}
		{
			FXD::Container::LinkedList< FXD::S32 > list1 = { 9 };
			PRINT_COND_ASSERT(list1.back() == 9, "LinkedList - reference back(void)");
			list1.back() = 42;
			PRINT_COND_ASSERT(list1.back() == 42, "LinkedList - reference back(void)");
		}
	}

	// void emplace_front(Args&&... args);
	// void emplace_front(value_type&& value);
	// void emplace_front(const value_type& value);
	{
		FXD::Container::LinkedList< FXD::S32 > listRef = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
		FXD::Container::LinkedList< FXD::S32 > list1;

		for (FXD::U32 n1 = 0; n1 < 10; n1++)
		{
			list1.emplace_front(n1);
		}
		PRINT_COND_ASSERT((list1 == listRef), "LinkedList - void emplace_front(Args&&... args)");
	}

	// template < typename... Args >
	// void emplace_back(Args&&... args);
	// void emplace_back(value_type&& value);
	// void emplace_back(const value_type& value);
	{
		{
			FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
			FXD::Container::LinkedList< FXD::S32 > list1;

			for (FXD::U32 n1 = 0; n1 < 10; n1++)
			{
				list1.emplace_back(n1);
			}
			PRINT_COND_ASSERT((list1 == listRef), "LinkedList - void emplace_back(Args&&... args)");
		}

		{
			// ------
			// A
			// -
			// ------
			class A
			{
			public:
				A(void)
					: m_nVal(0)
				{}
				A(FXD::S32 nVal)
					: m_nVal(nVal)
				{}
				bool operator==(const A& rhs) const
				{
					return (m_nVal == rhs.m_nVal);
				}

			public:
				FXD::S32 m_nVal;
			};

			{
				FXD::Container::LinkedList< A > listRef = { { 1 },{ 2 },{ 3 } };
				FXD::Container::LinkedList< A > list1;

				list1.emplace_back(1);
				list1.emplace_back(2);
				list1.emplace_back(3);

				PRINT_COND_ASSERT((list1 == listRef), "LinkedList - void emplace_back(Args&&... args)");
			}
			{
				FXD::Container::LinkedList< A > listRef = { { 1 },{ 2 },{ 3 } };
				FXD::Container::LinkedList< A > list1;

				list1.emplace_back(A(1));
				list1.emplace_back(A(2));
				list1.emplace_back(A(3));

				PRINT_COND_ASSERT((list1 == listRef), "LinkedList - void emplace_back(Args&&... args)");
			}
			{
				FXD::Container::LinkedList< A > listRef = { { 1 },{ 2 },{ 3 } };
				FXD::Container::LinkedList< A > list1;

				A a1(1);
				A a2(2);
				A a3(3);

				list1.emplace_back(a1);
				list1.emplace_back(a2);
				list1.emplace_back(a3);

				PRINT_COND_ASSERT((list1 == listRef), "LinkedList - void emplace_back(Args&&... args)");
			}
		}
	}

	// reference push_front(void);
	// void push_front(const value_type& val);
	// void push_front(value_type&& rhs);
	{
		{
			FXD::Container::LinkedList< FXD::S32 > listRef = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
			FXD::Container::LinkedList< FXD::S32 > list1;

			for (FXD::U32 n1 = 0; n1 < 10; n1++)
			{
				list1.push_front(n1);
			}
			PRINT_COND_ASSERT((list1 == listRef), "LinkedList - void push_front(const value_type& val)");
		}
		{
			struct A
			{
				FXD::S32 m_nVal;
			};
			FXD::Container::LinkedList< A > list1;
			list1.push_front(A{ 42 });
			PRINT_COND_ASSERT(list1.back().m_nVal == 42, "LinkedList - void push_front(T&& val)");
		}
	}

	// void push_back(const value_type& val);
	// void push_back(value_type&& rhs);
	{
		{
			FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
			FXD::Container::LinkedList< FXD::S32 > list1;

			for (FXD::U32 n1 = 0; n1 < 10; n1++)
			{
				list1.push_back(n1);
			}
			PRINT_COND_ASSERT(list1 == listRef, "LinkedList - void push_back(const value_type& val)");
		}
		{
			struct A
			{
				FXD::S32 m_nVal;
			};
			FXD::Container::LinkedList< A > list1;
			list1.push_back(A{ 42 });
			PRINT_COND_ASSERT(list1.back().m_nVal == 42, "LinkedList - void push_back(T&& val)");
		}
	}

	// void pop_front(void);
	{
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		for (FXD::U32 n1 = 0; n1 < 10; n1++)
		{
			PRINT_COND_ASSERT(FXD::U32(list1.front()) == n1, "LinkedList - void pop_front(void)");
			list1.pop_front();
		}
	}

	// void pop_back(void);
	{
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		for (FXD::U32 n1 = 0; n1 < 10; n1++)
		{
			PRINT_COND_ASSERT(FXD::U32(list1.back()) == (9 - n1), "LinkedList - void pop_back(void)");
			list1.pop_back();
		}
	}

	// iterator emplace(const_iterator pos, Args&&... args);
	{
		FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 1, 2, 3, 4, 42, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

		auto insert_pos = list1.begin();
		FXD::STD::advance(insert_pos, 5);

		list1.emplace(insert_pos, 42);
		PRINT_COND_ASSERT(list1 == listRef, "LinkedList - iterator emplace(const_iterator pos, Args&&... args)");
	}

	// iterator insert(const_iterator pos, value_type&& val);
	{
		FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 1, 2, 3, 4, 42, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

		auto insert_pos = list1.begin();
		FXD::STD::advance(insert_pos, 5);

		list1.insert(insert_pos, 42);
		PRINT_COND_ASSERT((list1 == listRef), "LinkedList - iterator insert(const_iterator pos, value_type&& val)");
	}

	// iterator insert(const_iterator pos, size_type nCount, const T& val);
	{
		FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 1, 2, 3, 4, 42, 42, 42, 42, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

		auto insert_pos = list1.begin();
		FXD::STD::advance(insert_pos, 5);

		list1.insert(insert_pos, 4, 42);
		PRINT_COND_ASSERT((list1 == listRef), "LinkedList - iterator insert(const_iterator pos, size_type nCount, const T& val)");
	}

	// iterator insert(const_iterator pos, InputItr first, InputItr last);
	{
		FXD::Container::LinkedList< FXD::S32 > to_insert = { 42, 42, 42, 42 };
		FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 1, 2, 3, 4, 42, 42, 42, 42, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

		auto insert_pos = list1.begin();
		FXD::STD::advance(insert_pos, 5);

		list1.insert(insert_pos, to_insert.begin(), to_insert.end());
		PRINT_COND_ASSERT((list1 == listRef), "LinkedList - iterator insert(const_iterator pos, InputItr first, InputItr last)");
	}

	// iterator insert(const_iterator pos, std::initializer_list< value_type > iList);
	{
		FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 1, 2, 3, 4, 42, 42, 42, 42, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

		auto insert_pos = list1.begin();
		FXD::STD::advance(insert_pos, 5);

		list1.insert(insert_pos, { 42, 42, 42, 42 });
		PRINT_COND_ASSERT((list1 == listRef), "LinkedList - iterator insert(const_iterator pos, std::initializer_list< value_type > iList)");
	}

	// iterator erase(const_iterator pos);
	{
		FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 42, 5, 6, 7, 8, 9 };

		auto erase_pos = list1.begin();
		FXD::STD::advance(erase_pos, 5);

		auto iter_after_removed = list1.erase(erase_pos);
		PRINT_COND_ASSERT(*iter_after_removed == 5, "");
		PRINT_COND_ASSERT((list1 == listRef), "LinkedList - iterator erase(const_iterator pos)");
	}

	// iterator erase(const_iterator first, const_iterator last);
	{
		FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 42, 42, 42, 42, 5, 6, 7, 8, 9 };

		auto erase_begin = list1.begin();
		FXD::STD::advance(erase_begin, 5);

		auto erase_end = erase_begin;
		FXD::STD::advance(erase_end, 4);

		list1.erase(erase_begin, erase_end);
		PRINT_COND_ASSERT((list1 == listRef), "LinkedList - iterator erase(const_iterator first, const_iterator last)");
	}

	// reverse_iterator erase(const_reverse_iterator pos);
	{
		FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 42, 5, 6, 7, 8, 9 };

		auto erase_rbegin = list1.rbegin();
		FXD::STD::advance(erase_rbegin, 5);

		auto iter_after_remove = list1.erase(erase_rbegin);
		PRINT_COND_ASSERT(*iter_after_remove == 4, "LinkedList - reverse_iterator erase(const_reverse_iterator pos)");
		PRINT_COND_ASSERT((list1 == listRef), "LinkedList - reverse_iterator erase(const_reverse_iterator pos)");
	}

	// reverse_iterator erase(const_reverse_iterator first, const_reverse_iterator last);
	{
		FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 42, 42, 42, 42, 5, 6, 7, 8, 9 };

		auto erase_crbegin = list1.crbegin();
		auto erase_crend = list1.crbegin();
		FXD::STD::advance(erase_crbegin, 4);
		FXD::STD::advance(erase_crend, 8);

		auto iter_after_removed = list1.erase(erase_crbegin, erase_crend);
		PRINT_COND_ASSERT(*iter_after_removed == 4, "LinkedList - reverse_iterator erase(const_reverse_iterator first, const_reverse_iterator last)");
		PRINT_COND_ASSERT((list1 == listRef), "LinkedList - reverse_iterator erase(const_reverse_iterator first, const_reverse_iterator last)");
	}

	// void clear(void)
	{
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		list1.clear();
		PRINT_COND_ASSERT(list1.empty(), "LinkedList - void clear(void)");
		PRINT_COND_ASSERT(list1.size() == 0, "LinkedList - void clear(void)");
	}

	// void remove(const T& x);
	{
		FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 1, 2, 3, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		list1.remove(4);
		PRINT_COND_ASSERT((list1 == listRef), "LinkedList - void remove(const T& val)");
	}

	// void remove_if(Predicate);
	{
		FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 1, 2, 3, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		list1.remove_if([](FXD::S32 e) { return e == 4; });
		PRINT_COND_ASSERT((list1 == listRef), "LinkedList - void remove_if(Predicate pred)");
	}

	// void reverse(void)
	{
		FXD::Container::LinkedList< FXD::S32 > listRef = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		list1.reverse();
		PRINT_COND_ASSERT((list1 == listRef), "LinkedList - void reverse(void) NOEXCEPT");
	}

	// void splice(const_iterator pos, my_type& rhs)
	{
		const FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4 };
		FXD::Container::LinkedList< FXD::S32 > list2 = { 5, 6, 7, 8, 9 };

		FXD::Container::LinkedList< FXD::S32 > listInt;
		listInt.splice(listInt.begin(), list2);
		listInt.splice(listInt.begin(), list1);

		PRINT_COND_ASSERT((listInt == listRef), "LinkedList - LinkedList splice");
		PRINT_COND_ASSERT(list1.empty(), "LinkedList - void splice(const_iterator pos, my_type& rhs)");
		PRINT_COND_ASSERT(list2.empty(), "LinkedList - void splice(const_iterator pos, my_type& rhs)");
	}

	// void splice(const_iterator pos, my_type& rhs, const_iterator first);
	{
		const FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 5 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { -1, -1, 0 };
		FXD::Container::LinkedList< FXD::S32 > list2 = { -1, -1, 5 };

		auto itrList1 = list1.begin();
		auto itrList2 = list2.begin();

		FXD::STD::advance(itrList1, 2);
		FXD::STD::advance(itrList2, 2);

		FXD::Container::LinkedList< FXD::S32 > listint;
		listint.splice(listint.begin(), list2, itrList2);
		listint.splice(listint.begin(), list1, itrList1);

		PRINT_COND_ASSERT((listint == listRef), "LinkedList - void splice(const_iterator pos, my_type& rhs, const_iterator first)");
		PRINT_COND_ASSERT(!list1.empty(), "LinkedList - void splice(const_iterator pos, my_type& rhs, const_iterator first)");
		PRINT_COND_ASSERT(!list2.empty(), "LinkedList - void splice(const_iterator pos, my_type& rhs, const_iterator first)");
	}

	// void splice(const_iterator pos, my_type& rhs, const_iterator first, const_iterator last)
	{
		const FXD::Container::LinkedList< FXD::S32 > listRef1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { -1, -1, 0, 1, 2, 3, 4, -1, -1 };
		FXD::Container::LinkedList< FXD::S32 > list2 = { -1, -1, 5, 6, 7, 8, 9, -1, -1 };

		auto itrList1Begin = list1.begin();
		auto itrList2Begin = list2.begin();
		auto itrList1End = list1.end();
		auto itrList2End = list2.end();

		FXD::STD::advance(itrList1Begin, 2);
		FXD::STD::advance(itrList2Begin, 2);
		FXD::STD::advance(itrList1End, -2);
		FXD::STD::advance(itrList2End, -2);

		FXD::Container::LinkedList< FXD::S32 > listInt;
		listInt.splice(listInt.begin(), list2, itrList2Begin, itrList2End);
		listInt.splice(listInt.begin(), list1, itrList1Begin, itrList1End);

		const FXD::Container::LinkedList< FXD::S32 > listRef2 = { -1, -1, -1, -1 };
		PRINT_COND_ASSERT(listInt == listRef1, "LinkedList - void splice(const_iterator pos, my_type& rhs, const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT(list1 == listRef2, "LinkedList - void splice(const_iterator pos, my_type& rhs, const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT(list2 == listRef2, "LinkedList - void splice(const_iterator pos, my_type& rhs, const_iterator first, const_iterator last)");
	}

	// void splice(const_iterator pos, my_type&& rhs)
	{
		const FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4 };
		FXD::Container::LinkedList< FXD::S32 > list2 = { 5, 6, 7, 8, 9 };

		FXD::Container::LinkedList< FXD::S32 > listInt;
		listInt.splice(listInt.begin(), std::move(list2));
		listInt.splice(listInt.begin(), std::move(list1));

		PRINT_COND_ASSERT((listInt == listRef), "LinkedList - void splice(const_iterator pos, my_type&& rhs)");
		PRINT_COND_ASSERT(list1.empty(), "LinkedList - void splice(const_iterator pos, my_type&& rhs)");
		PRINT_COND_ASSERT(list2.empty(), "LinkedList - void splice(const_iterator pos, my_type&& rhs)");
	}

	// void splice(const_iterator pos, my_type&& rhs, const_iterator first)
	{
		const FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 5 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { -1, -1, 0 };
		FXD::Container::LinkedList< FXD::S32 > list2 = { -1, -1, 5 };

		auto itrList1Begin = list1.begin();
		auto itrList2Begin = list2.begin();

		FXD::STD::advance(itrList1Begin, 2);
		FXD::STD::advance(itrList2Begin, 2);

		FXD::Container::LinkedList< FXD::S32 > listInt;
		listInt.splice(listInt.begin(), std::move(list2), itrList2Begin);
		listInt.splice(listInt.begin(), std::move(list1), itrList1Begin);

		PRINT_COND_ASSERT((listInt == listRef), "LinkedList - void splice(const_iterator pos, my_type&& rhs, const_iterator first)");
		PRINT_COND_ASSERT(!list1.empty(), "LinkedList - void splice(const_iterator pos, my_type&& rhs, const_iterator first)");
		PRINT_COND_ASSERT(!list2.empty(), "LinkedList - void splice(const_iterator pos, my_type&& rhs, const_iterator first)");
	}

	// void splice(const_iterator pos, my_type&& rhs, const_iterator first, const_iterator last)
	{
		const FXD::Container::LinkedList< FXD::S32 > listRef1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { -1, -1, 0, 1, 2, 3, 4, -1, -1 };
		FXD::Container::LinkedList< FXD::S32 > list2 = { -1, -1, 5, 6, 7, 8, 9, -1, -1 };

		auto itrList1Begin = list1.begin();
		auto itrList2Begin = list2.begin();
		auto itrList1End = list1.end();
		auto itrList2End = list2.end();

		FXD::STD::advance(itrList1Begin, 2);
		FXD::STD::advance(itrList2Begin, 2);
		FXD::STD::advance(itrList1End, -2);
		FXD::STD::advance(itrList2End, -2);

		FXD::Container::LinkedList< FXD::S32 > listInt;
		listInt.splice(listInt.begin(), std::move(list2), itrList2Begin, itrList2End);
		listInt.splice(listInt.begin(), std::move(list1), itrList1Begin, itrList1End);

		const FXD::Container::LinkedList< FXD::S32 > listRef2 = { -1, -1, -1, -1 };
		PRINT_COND_ASSERT((listInt == listRef1), "LinkedList - void splice(const_iterator pos, my_type&& rhs, const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT((list1 == listRef2), "LinkedList - void splice(const_iterator pos, my_type&& rhs, const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT((list2 == listRef2), "LinkedList - void splice(const_iterator pos, my_type&& rhs, const_iterator first, const_iterator last)");
	}

	// void merge(my_type& rhs)
	// void merge(my_type&& rhs)
	// void merge(my_type& rhs, Compare compare)
	{
		FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4 };
		FXD::Container::LinkedList< FXD::S32 > list2 = { 5, 6, 7, 8, 9 };
		list1.merge(list2);
		PRINT_COND_ASSERT((list1 == listRef), "LinkedList - void merge(my_type& rhs)");
	}

	// void merge(my_type&& rhs, Compare compare)
	{
		FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list1 = { 0, 1, 2, 3, 4 };
		FXD::Container::LinkedList< FXD::S32 > list2 = { 5, 6, 7, 8, 9 };
		list1.merge(list2, [](FXD::S32 lhs, FXD::S32 rhs)
		{
			return lhs < rhs;
		});
		PRINT_COND_ASSERT((list1 == listRef), "LinkedList - void merge(my_type&& rhs, Compare compare)");
	}

	// void unique(void)
	{
		FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 2, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 7, 8, 9, 9, 9, 9, 9, 9, 9, 9 };
		list.unique();
		PRINT_COND_ASSERT((list == listRef), "LinkedList - void unique(void)");
	}

	// void unique(BinaryPredicate pred)
	{
		static bool bBreakComparison;
		struct A
		{
			bool operator==(const A& rhs) const
			{
				return bBreakComparison ? false : (m_nVal == rhs.m_nVal);
			}
			FXD::S32 m_nVal;
		};

		FXD::Container::LinkedList< A > listRef = { { 0 },{ 1 },{ 2 },{ 3 },{ 4 },{ 5 },{ 6 },{ 7 },{ 8 },{ 9 } };
		FXD::Container::LinkedList< A > list = { { 0 },{ 0 },{ 0 },{ 0 },{ 0 },{ 0 },{ 1 },{ 2 },{ 2 },{ 2 },{ 2 },{ 3 },{ 4 },{ 5 },
		{ 5 },{ 5 },{ 5 },{ 5 },{ 6 },{ 7 },{ 7 },{ 7 },{ 7 },{ 8 },{ 9 },{ 9 },{ 9 } };

		bBreakComparison = true;
		list.unique(); // noop because broken comparison operator
		PRINT_COND_ASSERT(list != listRef, "LinkedList - void unique(BinaryPredicate pred)");

		list.unique([](const A& lhs, const A& rhs)
		{
			return (lhs.m_nVal == rhs.m_nVal);
		});

		bBreakComparison = false;
		PRINT_COND_ASSERT((list == listRef), "LinkedList - void unique(BinaryPredicate pred)");
	}

	// void sort(void)
	{
		FXD::Container::LinkedList< FXD::S32 > listRef = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::Container::LinkedList< FXD::S32 > list = { 9, 4, 5, 3, 1, 0, 6, 2, 7, 8 };

		list.sort();
		PRINT_COND_ASSERT((list == listRef), "LinkedList - void sort(void)");
	}

	// void sort(Compare compare)
	{
		struct A
		{
			FXD::S32 m_nVal;
			bool operator==(const A& rhs) const
			{
				return m_nVal == rhs.m_nVal;
			}
		};

		FXD::Container::LinkedList< A > listRef = { { 0 },{ 1 },{ 2 },{ 3 },{ 4 },{ 5 },{ 6 },{ 7 },{ 8 },{ 9 } };
		FXD::Container::LinkedList< A > list1 = { { 1 },{ 0 },{ 2 },{ 9 },{ 4 },{ 5 },{ 6 },{ 7 },{ 3 },{ 8 } };

		list1.sort([](const A& lhs, const A& rhs)
		{
			return lhs.m_nVal < rhs.m_nVal;
		});
		PRINT_COND_ASSERT((list1 == listRef), "LinkedList - void sort(Compare compare)");
	}

	return nErrorCount;
}