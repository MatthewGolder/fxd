// Creator - MatthewGolder
#include "Tests/System/Tests/FXDTest.h"
#include "Tests/System/TestHelperClasses.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/Core/Functional.h"
#include "FXDEngine/Core/Random.h"

/*
namespace
{
	// Used for eastl::function tests
	static int TestIntRet(int* p)
	{
		int ret = *p;
		*p += 1;
		return ret;
	}

	// Used for str_less tests below.
	template <typename T>
	struct Results
	{
		const T* p1;
		const T* p2;
		bool expectedResult; // The expected result of the expression (p1 < p2)
	};


	// Used for const_mem_fun_t below.
	struct X
	{
		X()
		{}
		void DoNothing() const
		{}
	};

	template <typename T>
	void foo(typename T::argument_type arg)
	{
		typename T::result_type(T::*pFunction)(typename T::argument_type) const = &T::operator();
		T t(&X::DoNothing);
		(t.*pFunction)(arg);
	}


	// Used for equal_to_2 tests below.
	struct N1
	{
		N1(int x)
		: mX(x)
		{}
		int mX;
	};

	struct N2
	{
		N2(int x)
		: mX(x)
		{}
		int mX;
	};

	bool operator==(const N1& n1, const N1& n1a) { return (n1.mX == n1a.mX); }
	bool operator==(const N1& n1, const N2& n2) { return (n1.mX == n2.mX); }
	bool operator==(const N2& n2, const N1& n1) { return (n2.mX == n1.mX); }

	bool operator!=(const N1& n1, const N1& n1a) { return (n1.mX != n1a.mX); }
	bool operator!=(const N1& n1, const N2& n2) { return (n1.mX != n2.mX); }
	bool operator!=(const N2& n2, const N1& n1) { return (n2.mX != n1.mX); }

	bool operator< (const N1& n1, const N1& n1a) { return (n1.mX  < n1a.mX); }
	bool operator< (const N1& n1, const N2& n2) { return (n1.mX  < n2.mX); }
	bool operator< (const N2& n2, const N1& n1) { return (n2.mX  < n1.mX); }


	// Used for mem_fun tests below.
	struct TestClass
	{
		mutable int mX;

		TestClass() : mX(37) { }

		void Increment()
		{
			mX++;
		}

		void IncrementConst() const
		{
			mX++;
		}

		int MultiplyBy(int x)
		{
			return mX * x;
		}

		int MultiplyByConst(int x) const
		{
			return mX * x;
		}
	};
}


// Template instantations.
// These tell the compiler to compile all the functions for the given class.
typedef eastl::basic_string<char8_t, MallocAllocator> String8MA;
typedef eastl::basic_string<char16_t, MallocAllocator> String16MA;

template struct eastl::string_hash<String8MA>;
template struct eastl::string_hash<String16MA>;

template class eastl::hash_set<String8MA, eastl::string_hash<String8MA> >;
template class eastl::hash_set<String16MA, eastl::string_hash<String16MA> >;


// Helper function for testing our default hash implementations for pod types which
// simply returns the static_cast<size_t> of the val passed in
template < typename T >
int TestHashHelper(T val)
{
	int nErrorCount = 0;

	EATEST_VERIFY(eastl::hash<T>()(val) == static_cast<size_t>(val));

	return nErrorCount;
}
*/

FXD::S32 FunctionTest(FXD::S32 nIdx)
{
	return nIdx;
}

static FXD::S32 TestClasses(void)
{
	FXD::S32 nErrorCount = 0;

	// function
	{
		struct FunctionClassOperatorTest
		{
			FXD::S32 operator()(FXD::S32 nIdx) const
			{
				return nIdx;
			}
		};

		class FunctionClassTest
		{
		public:
			FunctionClassTest(FXD::S32 nIdx)
				: m_nIdx(nIdx)
			{}

			FXD::S32 add(FXD::S32 nIdx) const
			{
				m_nIdx += nIdx;
				return m_nIdx;
			}

		public:
			mutable FXD::S32 m_nIdx;
		};

		// Store a free function
		FXD::STD::function< FXD::S32(FXD::S32) > funcFree = FunctionTest;
		PRINT_COND_ASSERT(funcFree(12) == 12, "Functional - inline FXD::STD::function< FXD::S32(FXD::S32) >");

		// Store a lambda
		FXD::STD::function< FXD::S32() > funcLambda = []() { return FunctionTest(42); };
		PRINT_COND_ASSERT(funcLambda() == 42, "Functional - inline FXD::STD::function< FXD::S32(FXD::S32) >");

		// Store the result of a call to std::bind
		FXD::STD::function< FXD::S32() > funcBind = std::bind(FunctionTest, 31337);
		PRINT_COND_ASSERT(funcBind() == 31337, "Functional - inline FXD::STD::function< FXD::S32(FXD::S32) >");

		// Store a call to a member function
		FXD::STD::function< FXD::S32(const FunctionClassTest&, FXD::S32) > funcMemberFunc = &FunctionClassTest::add;

		const FunctionClassTest foo(314159);
		PRINT_COND_ASSERT(funcMemberFunc(foo, 1) == 314160, "Functional - inline FXD::STD::function< FXD::S32(FXD::S32) >");
		PRINT_COND_ASSERT(funcMemberFunc(314159, 1) == 314160, "Functional - inline FXD::STD::function< FXD::S32(FXD::S32) >");

		// Store a call to a data member accessor
		FXD::STD::function< FXD::S32(FunctionClassTest const&) > funcMemberData = &FunctionClassTest::m_nIdx;
		PRINT_COND_ASSERT(funcMemberData(foo) == 314160, "Functional - inline FXD::STD::function< FXD::S32(FXD::S32) >");

		// Store a call to a member function and object
		using std::placeholders::_1;
		FXD::STD::function< FXD::S32(FXD::S32) > funcMemberObject = std::bind(&FunctionClassTest::add, foo, _1);
		PRINT_COND_ASSERT(funcMemberObject(2) == 314162, "Functional - inline FXD::STD::function< FXD::S32(FXD::S32) >");

		// Store a call to a member function and object ptr
		FXD::STD::function< FXD::S32(FXD::S32) > funcMemberObjectPtr = std::bind(&FunctionClassTest::add, &foo, _1);
		PRINT_COND_ASSERT(funcMemberObjectPtr(3) == 314163, "Functional - inline FXD::STD::function< FXD::S32(FXD::S32) >");

		// Store a call to a function object
		FXD::STD::function< FXD::S32(FXD::S32) > funcObject = FunctionClassOperatorTest();
		PRINT_COND_ASSERT(funcObject(18) == 18, "Functional - inline FXD::STD::function< FXD::S32(FXD::S32) >");
	}

	// mem_fn
	{
		class MenFnClassTest
		{
		public:
			void increment(void)
			{
				m_nIdx *= 2;
			}
			void set_value(FXD::S32 nIdx)
			{
				m_nIdx = nIdx;
			}
			FXD::S32 get_value(void)
			{
				return m_nIdx;
			}

		public:
			FXD::S32 m_nIdx = 2;
		};

		MenFnClassTest f;
		auto memFnGet = FXD::STD::mem_fn(&MenFnClassTest::get_value);
		PRINT_COND_ASSERT(memFnGet(f) == 2, "Functional - inline mem_fn< MenFnClassTest >");

		auto memFnSet = FXD::STD::mem_fn(&MenFnClassTest::set_value);
		memFnSet(f, 5);
		PRINT_COND_ASSERT(memFnGet(f) == 5, "Functional - inline mem_fn< MenFnClassTest >");

		auto memFnIncrement = FXD::STD::mem_fn(&MenFnClassTest::increment);
		memFnIncrement(f);
		PRINT_COND_ASSERT(memFnGet(f) == 10, "Functional - inline mem_fn< MenFnClassTest >");
	}

	// is_bind_expression TODO
	{
		struct BindExpressionTest
		{
			FXD::S32 operator()() const
			{
				return 0;
			}
			FXD::S32 operator()() volatile
			{
				return 1;
			}
			FXD::S32 operator()() const volatile
			{
				return 2;
			}
			void operator()()
			{};
		};

		/*
		const auto b0 = FXD::bind(BindExpressionTest());
		CTC_ASSERT(FXD::STD::is_bind_expression< decltype(b0) >::value, "const-qualified wrapper is a bind expression");

		volatile auto b1 = std::bind(BindExpressionTest());
		CTC_ASSERT(FXD::STD::is_bind_expression<decltype(b1)>::value, "volatile-qualified wrapper is a bind expression");

		const volatile auto b2 = std::bind(BindExpressionTest());
		CTC_ASSERT(FXD::STD::is_bind_expression<decltype(b2)>::value, "const-volatile-qualified wrapper is a bind expression");

		const auto b3 = std::bind< FXD::S32 >(BindExpressionTest());
		CTC_ASSERT(FXD::STD::is_bind_expression<decltype(b3)>::value, "const-qualified wrapper is a bind expression");

		volatile auto b4 = std::bind< FXD::S32 >(BindExpressionTest());
		CTC_ASSERT(FXD::STD::is_bind_expression<decltype(b4)>::value, "volatile-qualified wrapper is a bind expression");

		const volatile auto b5 = std::bind< FXD::S32 >(BindExpressionTest());
		CTC_ASSERT(FXD::STD::is_bind_expression<decltype(b5)>::value, "const-volatile-qualified wrapper is a bind expression");
		*/
	}

	// is_placeholder TODO
	{
		CTC_ASSERT(FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_1) >::value == 1, "Functional - FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_1) >");
		CTC_ASSERT(FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_2) >::value == 2, "Functional - FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_2) >");
		CTC_ASSERT(FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_3) >::value == 3, "Functional - FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_3) >");
		CTC_ASSERT(FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_4) >::value == 4, "Functional - FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_4) >");
		CTC_ASSERT(FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_5) >::value == 5, "Functional - FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_5) >");
		CTC_ASSERT(FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_6) >::value == 6, "Functional - FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_6) >");
		CTC_ASSERT(FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_7) >::value == 7, "Functional - FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_7) >");
		CTC_ASSERT(FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_8) >::value == 8, "Functional - FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_8) >");
		CTC_ASSERT(FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_9) >::value == 9, "Functional - FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_9) >");
		CTC_ASSERT(FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_10) >::value == 10, "Functional - FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_10) >");
		CTC_ASSERT(FXD::STD::is_placeholder< decltype(4) >::value == 0, "Functional - FXD::STD::is_placeholder< decltype(4) >");
		CTC_ASSERT(FXD::STD::is_placeholder< decltype(5.5f) >::value == 0, "Functional - FXD::STD::is_placeholder< decltype(5.5f) >");
		CTC_ASSERT(FXD::STD::is_placeholder< decltype('a') >::value == 0, "Functional - FXD::STD::is_placeholder< decltype('a') >");
		CTC_ASSERT(FXD::STD::is_placeholder< decltype(Game::PodEmpty()) >::value == 0, "Functional - FXD::STD::is_placeholder< decltype(Game::PodEmpty()) >");

#if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED
		CTC_ASSERT(FXD::STD::is_placeholder_v< decltype(FXD::STD::placeholders::_1) > == 1, "Functional - FXD::STD::is_placeholder_v< decltype(FXD::STD::placeholders::_1) >");
		CTC_ASSERT(FXD::STD::is_placeholder_v< decltype(FXD::STD::placeholders::_2) > == 2, "Functional - FXD::STD::is_placeholder_v< decltype(FXD::STD::placeholders::_2) >");
		CTC_ASSERT(FXD::STD::is_placeholder_v< decltype(FXD::STD::placeholders::_3) > == 3, "Functional - FXD::STD::is_placeholder_v< decltype(FXD::STD::placeholders::_3) >");
		CTC_ASSERT(FXD::STD::is_placeholder_v< decltype(FXD::STD::placeholders::_4) > == 4, "Functional - FXD::STD::is_placeholder_v< decltype(FXD::STD::placeholders::_4) >");
		CTC_ASSERT(FXD::STD::is_placeholder_v< decltype(FXD::STD::placeholders::_5) > == 5, "Functional - FXD::STD::is_placeholder_v< decltype(FXD::STD::placeholders::_5) >");
		CTC_ASSERT(FXD::STD::is_placeholder_v< decltype(FXD::STD::placeholders::_6) > == 6, "Functional - FXD::STD::is_placeholder_v< decltype(FXD::STD::placeholders::_6) >");
		CTC_ASSERT(FXD::STD::is_placeholder_v< decltype(FXD::STD::placeholders::_7) > == 7, "Functional - FXD::STD::is_placeholder_v< decltype(FXD::STD::placeholders::_7) >");
		CTC_ASSERT(FXD::STD::is_placeholder_v< decltype(FXD::STD::placeholders::_8) > == 8, "Functional - FXD::STD::is_placeholder_v< decltype(FXD::STD::placeholders::_8) >");
		CTC_ASSERT(FXD::STD::is_placeholder_v< decltype(FXD::STD::placeholders::_9) > == 9, "Functional - FXD::STD::is_placeholder_v< decltype(FXD::STD::placeholders::_9) >");
		CTC_ASSERT(FXD::STD::is_placeholder_v< decltype(FXD::STD::placeholders::_10) > == 10, "Functional - FXD::STD::is_placeholder_v< decltype(FXD::STD::placeholders::_10) >");
		CTC_ASSERT(FXD::STD::is_placeholder_v< decltype(4) > == 0, "Functional - FXD::STD::is_placeholder_v< decltype(4) >");
		CTC_ASSERT(FXD::STD::is_placeholder_v< decltype(5.5f) > == 0, "Functional - FXD::STD::is_placeholder_v< decltype(5.5f) >");
		CTC_ASSERT(FXD::STD::is_placeholder_v< decltype('a') > == 0, "Functional - FXD::STD::is_placeholder_v< decltype('a') >");
		CTC_ASSERT(FXD::STD::is_placeholder_v< decltype(Game::PodEmpty()) > == 0, "Functional - FXD::STD::is_placeholder_v< decltype(Game::PodEmpty()) >");
#endif
	
		PRINT_COND_ASSERT(FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_1) >::value == 1, "Functional");
		PRINT_COND_ASSERT(FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_2) >::value == 2, "Functional");
		PRINT_COND_ASSERT(FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_3) >::value == 3, "Functional");
		PRINT_COND_ASSERT(FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_4) >::value == 4, "Functional");
		PRINT_COND_ASSERT(FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_5) >::value == 5, "Functional");
		PRINT_COND_ASSERT(FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_6) >::value == 6, "Functional");
		PRINT_COND_ASSERT(FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_7) >::value == 7, "Functional");
		PRINT_COND_ASSERT(FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_8) >::value == 8, "Functional");
		PRINT_COND_ASSERT(FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_9) >::value == 9, "Functional");
		PRINT_COND_ASSERT(FXD::STD::is_placeholder< decltype(FXD::STD::placeholders::_10) >::value == 10, "Functional");
		PRINT_COND_ASSERT(FXD::STD::is_placeholder< decltype(4) >::value == 0, "Functional");
		PRINT_COND_ASSERT(FXD::STD::is_placeholder< decltype(5.5f) >::value == 0, "Functional");
		PRINT_COND_ASSERT(FXD::STD::is_placeholder< decltype('a') >::value == 0, "Functional");
		PRINT_COND_ASSERT(FXD::STD::is_placeholder< decltype(Game::PodEmpty()) >::value == 0, "Functional");
	}

	// reference_wrapper TODO
	{
		// operator T&
		{
			FXD::S32 nVal = 0;
			FXD::STD::reference_wrapper< FXD::S32 > ref(nVal);
			FXD::S32& nValRef = ref;
			nValRef = 42;

			PRINT_COND_ASSERT(nVal == 42, "Functional - inline FXD::STD::reference_wrapper< T > reference_wrapper(T& val) NOEXCEPT");
		}

		// get
		{
			FXD::S32 nVal = 0;
			FXD::STD::reference_wrapper< FXD::S32 > ref(nVal);
			ref.get() = 42;

			PRINT_COND_ASSERT(nVal == 42, "Functional - inline FXD::STD::reference_wrapper< T > reference_wrapper(T& val) NOEXCEPT");
		}

		// copy constructor
		{
			FXD::S32 nVal = 0;
			FXD::STD::reference_wrapper< FXD::S32 > ref(nVal);
			FXD::STD::reference_wrapper< FXD::S32 > refCopy(ref);
			refCopy.get() = 42;

			PRINT_COND_ASSERT(nVal == 42, "Functional - inline FXD::STD::reference_wrapper< T > reference_wrapper(T& val) NOEXCEPT");
		}

		// assignment
		{
			FXD::S32 nVal1 = 0;
			FXD::S32 nVal2 = 0;

			FXD::STD::reference_wrapper< FXD::S32 > ref1(nVal1);
			FXD::STD::reference_wrapper< FXD::S32 > ref2(nVal2);

			ref2 = ref1; // rebind ref2 refer to nVal1
			ref2.get() = 42;

			PRINT_COND_ASSERT(nVal1 == 42, "Functional - inline FXD::STD::reference_wrapper< T > reference_wrapper(T& val) NOEXCEPT");
			PRINT_COND_ASSERT(nVal2 == 0, "Functional - inline FXD::STD::reference_wrapper< T > reference_wrapper(T& val) NOEXCEPT");
		}

		// invoke
		{
			struct Functor
			{
				bool bCalled = false;
				void operator()()
				{
					bCalled = true;
				}
			};

			Functor func;
			FXD::STD::reference_wrapper< Functor > ref(func);
			ref();

			PRINT_COND_ASSERT(func.bCalled, "Functional - inline FXD::STD::reference_wrapper< T > reference_wrapper(T& val) NOEXCEPT");
		}
	}

	return nErrorCount;
}

static FXD::S32 TestHashing(void)
{
	FXD::S32 nErrorCount = 0;

	{
	}

	return nErrorCount;
}

static FXD::S32 TestFunctions(void)
{
	FXD::S32 nErrorCount = 0;

	// bind TODO
	{
	}

	// inline FXD::STD::reference_wrapper< T > ref(T& val) NOEXCEPT
	// inline FXD::STD::reference_wrapper< T > ref(FXD::STD::reference_wrapper< T > val) NOEXCEPT
	{
		FXD::S32 nVal = 0;
		FXD::STD::reference_wrapper< FXD::S32 > ref1 = FXD::STD::ref(nVal);
		ref1.get() = 42;
		FXD::STD::reference_wrapper< FXD::S32 > ref2 = FXD::STD::ref(ref1);

		PRINT_COND_ASSERT(nVal == 42, "Functional - inline FXD::STD::reference_wrapper< T > ref(T& val) NOEXCEPT");
		PRINT_COND_ASSERT(ref1 == 42, "Functional - inline FXD::STD::reference_wrapper< T > ref(T& val) NOEXCEPT");
		PRINT_COND_ASSERT(ref2 == 42, "Functional - inline FXD::STD::reference_wrapper< T > ref(FXD::STD::reference_wrapper< T > val) NOEXCEPT");
	}

	// inline FXD::STD::reference_wrapper< const T > cref(const T& val) NOEXCEPT
	// inline FXD::STD::reference_wrapper< const T > cref(FXD::STD::reference_wrapper< T > val) NOEXCEPT
	{
		FXD::S32 nVal = 1337;
		FXD::STD::reference_wrapper< const FXD::S32 > ref1 = FXD::STD::cref(nVal);
		FXD::STD::reference_wrapper< const FXD::S32 > ref2 = FXD::STD::cref(ref1);

		PRINT_COND_ASSERT(nVal == 1337, "Functional - inline FXD::STD::reference_wrapper< const T > cref(const T& val) NOEXCEPT");
		PRINT_COND_ASSERT(ref1 == 1337, "Functional - inline FXD::STD::reference_wrapper< const T > cref(const T& val) NOEXCEPT");
		PRINT_COND_ASSERT(ref2 == 1337, "Functional - inline FXD::STD::reference_wrapper< const T > cref(FXD::STD::reference_wrapper< T > val) NOEXCEPT");
	}

	// inline decltype(auto) invoke(F&& func, Args&&... args)
	{
		{
			class _TestStruct
			{
			public:
				_TestStruct(FXD::S32 nVal)
					: m_nVal(nVal)
				{}
				void add(FXD::S32 nVal)
				{
					m_nVal += nVal;
				}
				FXD::S32 get_value(void)
				{
					return m_nVal;
				}
				FXD::S32& get_value_reference(void)
				{
					return m_nVal;
				}

			public:
				FXD::S32 m_nVal;
			};

			{
				_TestStruct test(42);
				FXD::invoke(&_TestStruct::add, test, 10);

				PRINT_COND_ASSERT(test.m_nVal == 52, "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_invocable< decltype(&_TestStruct::add), _TestStruct, FXD::S32 >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_same< typename FXD::STD::invoke_result< decltype(&_TestStruct::add), _TestStruct, FXD::S32 >::type, void >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
			}
			{
				_TestStruct test(42);
				FXD::invoke(&_TestStruct::add, &test, 10);

				PRINT_COND_ASSERT(test.m_nVal == 52, "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_invocable< decltype(&_TestStruct::add), _TestStruct*, FXD::S32 >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_same< typename FXD::STD::invoke_result< decltype(&_TestStruct::add), _TestStruct*, FXD::S32 >::type, void >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
			}
			{
				_TestStruct test(42);
				FXD::STD::reference_wrapper< _TestStruct > ref(test);
				FXD::invoke(&_TestStruct::add, ref, 10);

				PRINT_COND_ASSERT(test.m_nVal == 52, "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_invocable< decltype(&_TestStruct::add), FXD::STD::reference_wrapper< _TestStruct >, FXD::S32 >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_same< typename FXD::STD::invoke_result< decltype(&_TestStruct::add), FXD::STD::reference_wrapper< _TestStruct >, FXD::S32 >::type, void >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
			}
			{
				_TestStruct test(42);
				FXD::STD::reference_wrapper< _TestStruct > ref(test);
				FXD::invoke(&_TestStruct::m_nVal, ref) = 43;

				PRINT_COND_ASSERT(test.get_value_reference() == 43, "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_invocable< decltype(&_TestStruct::get_value_reference), FXD::STD::reference_wrapper< _TestStruct > >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_same< typename FXD::STD::invoke_result< decltype(&_TestStruct::get_value_reference), FXD::STD::reference_wrapper< _TestStruct > >::type, FXD::S32& >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
			}
			{
				_TestStruct test(42);
				FXD::invoke(&_TestStruct::get_value_reference, test) = 43;

				PRINT_COND_ASSERT(test.m_nVal == 43, "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_invocable< decltype(&_TestStruct::get_value_reference), _TestStruct& >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_same< typename FXD::STD::invoke_result< decltype(&_TestStruct::get_value_reference), _TestStruct& >::type, FXD::S32& >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
			}
			{
				_TestStruct test(42);

				PRINT_COND_ASSERT(FXD::invoke(&_TestStruct::m_nVal, test) == 42, "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_invocable< decltype(&_TestStruct::m_nVal), _TestStruct& >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_same< typename FXD::STD::invoke_result< decltype(&_TestStruct::m_nVal), _TestStruct& >::type, FXD::S32& >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
			}
			{
				_TestStruct test(42);
				FXD::invoke(&_TestStruct::m_nVal, test) = 43;

				PRINT_COND_ASSERT(test.m_nVal == 43, "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_invocable< decltype(&_TestStruct::m_nVal), _TestStruct&>::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_same< typename FXD::STD::invoke_result< decltype(&_TestStruct::m_nVal), _TestStruct& >::type, FXD::S32& >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
			}
			{
				_TestStruct test(42);
				FXD::invoke(&_TestStruct::m_nVal, &test) = 43;

				PRINT_COND_ASSERT(test.m_nVal == 43, "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_invocable< decltype(&_TestStruct::m_nVal), _TestStruct* >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_same< typename FXD::STD::invoke_result< decltype(&_TestStruct::m_nVal), _TestStruct* >::type, FXD::S32& >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
			}
			{
				struct _TestFunctor
				{
					void operator()()
					{
						m_bCalled = true;
					}
					bool m_bCalled = false;
				};

				_TestFunctor func;
				FXD::invoke(func);

				PRINT_COND_ASSERT(func.m_bCalled, "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_invocable< decltype(func) >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_same< typename FXD::STD::invoke_result< decltype(func) >::type, void >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
			}
			{
				struct _TestFunctorArguments
				{
					void operator()(FXD::S32 nVal)
					{
						m_nVal = nVal;
					}
					FXD::S32 m_nVal = 0;
				};

				_TestFunctorArguments func;
				FXD::invoke(func, 42);

				PRINT_COND_ASSERT(func.m_nVal == 42, "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_invocable< decltype(func), FXD::S32 >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_same< typename FXD::STD::invoke_result< decltype(func), FXD::S32 >::type, void >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
			}
			{
				static bool bCalled = false;
				auto func = [] { bCalled = true; };
				FXD::invoke(func);

				PRINT_COND_ASSERT(bCalled, "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_invocable< decltype(func) >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_same< typename FXD::STD::invoke_result< decltype(func) >::type, void >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
			}
			{
				static FXD::S32 nVal = 0;
				auto func = [](FXD::S32 nRes) { nVal = nRes; };
				FXD::invoke(func, 42);

				PRINT_COND_ASSERT(nVal == 42, "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_invocable< decltype(func), FXD::S32 >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
				CTC_ASSERT((FXD::STD::is_same< typename FXD::STD::invoke_result< decltype(func), FXD::S32 >::type, void >::value), "Functional - inline decltype(auto) invoke(F&& func, Args&&... args)");
			}
		}
	}

	return nErrorCount;
}

static FXD::S32 TestArithmeticOperations(void)
{
	FXD::S32 nErrorCount = 0;

	// plus <>
	{
		CTC_ASSERT(FXD::STD::plus<>()(40, 2) == 42, "Functional - FXD::STD::plus< FXD::S32 >()");
		CTC_ASSERT(FXD::STD::plus<>()(40.0f, 2.0f) == 42.0f, "Functional - FXD::STD::plus< FXD::F32 >()");

		PRINT_COND_ASSERT(FXD::STD::plus<>()(40, 2) == 42, "Functional");
		PRINT_COND_ASSERT(FXD::STD::plus<>()(40.0f, 2.0f) == 42.0f, "Functional");
		PRINT_COND_ASSERT(FXD::STD::plus<>()(FXD::Core::String8("4"), "2") == "42", "Functional");
	}

	// minus <>
	{
		CTC_ASSERT(FXD::STD::minus<>()(6, 2) == 4, "Functional - FXD::STD::minus< FXD::S32 >()");
		CTC_ASSERT(FXD::STD::minus<>()(6.0f, 2.0f) == 4.0f, "Functional - FXD::STD::minus< FXD::F32 >()");
		
		PRINT_COND_ASSERT(FXD::STD::minus<>()(6, 2) == 4, "Functional");
		PRINT_COND_ASSERT(FXD::STD::minus<>()(6.0f, 2.0f) == 4.0f, "Functional");
	}

	// multiplies <>
	{
		CTC_ASSERT(FXD::STD::multiplies<>()(6, 2) == 12, "Functional - FXD::STD::multiplies< FXD::S32 >()");
		CTC_ASSERT(FXD::STD::multiplies<>()(6.0f, 2.0f) == 12.0f, "Functional - FXD::STD::multiplies< FXD::F32 >()");

		PRINT_COND_ASSERT(FXD::STD::multiplies<>()(6, 2) == 12, "Functional");
		PRINT_COND_ASSERT(FXD::STD::multiplies<>()(6.0f, 2.0f) == 12.0f, "Functional");
	}

	// divides <>
	{
		CTC_ASSERT(FXD::STD::divides<>()(6, 2) == 3, "Functional - FXD::STD::divides< FXD::S32 >()");
		CTC_ASSERT(FXD::STD::divides<>()(6.0f, 2.0f) == 3.0f, "Functional - FXD::STD::divides< FXD::F32 >()");

		PRINT_COND_ASSERT(FXD::STD::divides<>()(6, 2) == 3, "Functional");
		PRINT_COND_ASSERT(FXD::STD::divides<>()(6.0f, 2.0f) == 3.0f, "Functional");
	}

	// modulus <>
	{
		CTC_ASSERT(FXD::STD::modulus<>()(6, 2) == 0, "Functional - FXD::STD::modulus< FXD::S32 >()");
		CTC_ASSERT(FXD::STD::modulus<>()(7, 2) == 1, "Functional - FXD::STD::modulus< FXD::S32 >()");

		PRINT_COND_ASSERT(FXD::STD::modulus<>()(6, 2) == 0, "Functional");
		PRINT_COND_ASSERT(FXD::STD::modulus<>()(7, 2) == 1, "Functional");
	}

	// negate <>
	{
		CTC_ASSERT(FXD::STD::negate<>()(42) == -42, "Functional - FXD::STD::negate< FXD::S32 >()");
		CTC_ASSERT(FXD::STD::negate<>()(42.0f) == -42.0f, "Functional - FXD::STD::negate< FXD::F32 >()");
		CTC_ASSERT(FXD::STD::negate<>()(-42) == 42, "Functional - FXD::STD::negate< FXD::S32 >()");
		CTC_ASSERT(FXD::STD::negate<>()(-42.0f) == 42.0f, "Functional - FXD::STD::negate< FXD::F32 >()");

		PRINT_COND_ASSERT(FXD::STD::negate<>()(42) == -42, "Functional");
		PRINT_COND_ASSERT(FXD::STD::negate<>()(42.0f) == -42.0f, "Functional");
		PRINT_COND_ASSERT(FXD::STD::negate<>()(-42) == 42, "Functional");
		PRINT_COND_ASSERT(FXD::STD::negate<>()(-42.0f) == 42.0f, "Functional");
	}

	return nErrorCount;
}

static FXD::S32 TestComparisons(void)
{
	FXD::S32 nErrorCount = 0;

	// equal_to <>
	{
		CTC_ASSERT(!FXD::STD::equal_to<>()(40, 2), "Functional - FXD::STD::equal_to< FXD::S32 >()");
		CTC_ASSERT(!FXD::STD::equal_to<>()(2, 40), "Functional - FXD::STD::equal_to< FXD::S32 >()");
		CTC_ASSERT( FXD::STD::equal_to<>()(40, 40), "Functional - FXD::STD::equal_to< FXD::S32 >()");
		CTC_ASSERT(!FXD::STD::equal_to<>()(40.0f, 2.0f), "Functional - FXD::STD::equal_to< FXD::F32 >()");
		CTC_ASSERT(!FXD::STD::equal_to<>()(2.0f, 40.0f), "Functional - FXD::STD::equal_to< FXD::F32 >()");
		CTC_ASSERT( FXD::STD::equal_to<>()(40.0f, 40.0f), "Functional - FXD::STD::equal_to< FXD::F32 >()");

		PRINT_COND_ASSERT(!FXD::STD::equal_to<>()(40, 2), "Functional");
		PRINT_COND_ASSERT(!FXD::STD::equal_to<>()(2, 40), "Functional");
		PRINT_COND_ASSERT( FXD::STD::equal_to<>()(40, 40), "Functional");
		PRINT_COND_ASSERT(!FXD::STD::equal_to<>()(40.0f, 2.0f), "Functional");
		PRINT_COND_ASSERT(!FXD::STD::equal_to<>()(2.0f, 40.0f), "Functional");
		PRINT_COND_ASSERT( FXD::STD::equal_to<>()(40.0f, 40.0f), "Functional");
	}

	// not_equal_to <>
	{
		CTC_ASSERT( FXD::STD::not_equal_to<>()(40, 2), "Functional - FXD::STD::not_equal_to< FXD::S32 >()");
		CTC_ASSERT( FXD::STD::not_equal_to<>()(2, 40), "Functional - FXD::STD::not_equal_to< FXD::S32 >()");
		CTC_ASSERT(!FXD::STD::not_equal_to<>()(40, 40), "Functional - FXD::STD::not_equal_to< FXD::S32 >()");
		CTC_ASSERT( FXD::STD::not_equal_to<>()(40.0f, 2.0f), "Functional - FXD::STD::not_equal_to< FXD::F32 >()");
		CTC_ASSERT( FXD::STD::not_equal_to<>()(2.0f, 40.0f), "Functional - FXD::STD::not_equal_to< FXD::F32 >()");
		CTC_ASSERT(!FXD::STD::not_equal_to<>()(40.0f, 40.0f), "Functional - FXD::STD::not_equal_to< FXD::F32 >()");

		PRINT_COND_ASSERT( FXD::STD::not_equal_to<>()(40, 2), "Functional");
		PRINT_COND_ASSERT( FXD::STD::not_equal_to<>()(2, 40), "Functional");
		PRINT_COND_ASSERT(!FXD::STD::not_equal_to<>()(40, 40), "Functional");
		PRINT_COND_ASSERT( FXD::STD::not_equal_to<>()(40.0f, 2.0f), "Functional");
		PRINT_COND_ASSERT( FXD::STD::not_equal_to<>()(2.0f, 40.0f), "Functional");
		PRINT_COND_ASSERT(!FXD::STD::not_equal_to<>()(40.0f, 40.0f), "Functional");
	}

	// greater <>
	{
		CTC_ASSERT( FXD::STD::greater<>()(40, 2), "Functional - FXD::STD::greater< FXD::S32 >()");
		CTC_ASSERT(!FXD::STD::greater<>()(2, 40), "Functional - FXD::STD::greater< FXD::S32 >()");
		CTC_ASSERT(!FXD::STD::greater<>()(40, 40), "Functional - FXD::STD::greater< FXD::S32 >()");
		CTC_ASSERT( FXD::STD::greater<>()(40.0f, 2.0f), "Functional - FXD::STD::greater< FXD::F32 >()");
		CTC_ASSERT(!FXD::STD::greater<>()(2.0f, 40.0f), "Functional - FXD::STD::greater< FXD::F32 >()");
		CTC_ASSERT(!FXD::STD::greater<>()(40.0f, 40.0f), "Functional - FXD::STD::greater< FXD::F32 >()");

		PRINT_COND_ASSERT( FXD::STD::greater<>()(40, 2), "Functional");
		PRINT_COND_ASSERT(!FXD::STD::greater<>()(2, 40), "Functional");
		PRINT_COND_ASSERT(!FXD::STD::greater<>()(40, 40), "Functional");
		PRINT_COND_ASSERT( FXD::STD::greater<>()(40.0f, 2.0f), "Functional");
		PRINT_COND_ASSERT(!FXD::STD::greater<>()(2.0f, 40.0f), "Functional");
		PRINT_COND_ASSERT(!FXD::STD::greater<>()(40.0f, 40.0f), "Functional");
	}

	// less <>
	{
		CTC_ASSERT(!FXD::STD::less<>()(40, 2), "Functional - FXD::STD::less< FXD::S32 >()");
		CTC_ASSERT( FXD::STD::less<>()(2, 40), "Functional - FXD::STD::less< FXD::S32 >()");
		CTC_ASSERT(!FXD::STD::less<>()(40, 40), "Functional - FXD::STD::less< FXD::S32 >()");
		CTC_ASSERT(!FXD::STD::less<>()(40.0f, 2.0f), "Functional - FXD::STD::less< FXD::F32 >()");
		CTC_ASSERT( FXD::STD::less<>()(2.0f, 40.0f), "Functional - FXD::STD::less< FXD::F32 >()");
		CTC_ASSERT(!FXD::STD::less<>()(40.0f, 40.0f), "Functional - FXD::STD::less< FXD::F32 >()");

		PRINT_COND_ASSERT(!FXD::STD::less<>()(40, 2), "Functional");
		PRINT_COND_ASSERT( FXD::STD::less<>()(2, 40), "Functional");
		PRINT_COND_ASSERT(!FXD::STD::less<>()(40, 40), "Functional");
		PRINT_COND_ASSERT(!FXD::STD::less<>()(40.0f, 2.0f), "Functional");
		PRINT_COND_ASSERT( FXD::STD::less<>()(2.0f, 40.0f), "Functional");
		PRINT_COND_ASSERT(!FXD::STD::less<>()(40.0f, 40.0f), "Functional");
	}

	// greater_equal <>
	{
		CTC_ASSERT( FXD::STD::greater_equal<>()(40, 2), "Functional - FXD::STD::greater_equal< FXD::S32 >()");
		CTC_ASSERT(!FXD::STD::greater_equal<>()(2, 40), "Functional - FXD::STD::greater_equal< FXD::S32 >()");
		CTC_ASSERT( FXD::STD::greater_equal<>()(40, 40), "Functional - FXD::STD::greater_equal< FXD::S32 >()");
		CTC_ASSERT( FXD::STD::greater_equal<>()(40.0f, 2.0f), "Functional - FXD::STD::greater_equal< FXD::F32 >()");
		CTC_ASSERT(!FXD::STD::greater_equal<>()(2.0f, 40.0f), "Functional - FXD::STD::greater_equal< FXD::F32 >()");
		CTC_ASSERT( FXD::STD::greater_equal<>()(40.0f, 40.0f), "Functional - FXD::STD::greater_equal< FXD::F32 >()");

		PRINT_COND_ASSERT( FXD::STD::greater_equal<>()(40, 2), "Functional");
		PRINT_COND_ASSERT(!FXD::STD::greater_equal<>()(2, 40), "Functional");
		PRINT_COND_ASSERT( FXD::STD::greater_equal<>()(40, 40), "Functional");
		PRINT_COND_ASSERT( FXD::STD::greater_equal<>()(40.0f, 2.0f), "Functional");
		PRINT_COND_ASSERT(!FXD::STD::greater_equal<>()(2.0f, 40.0f), "Functional");
		PRINT_COND_ASSERT( FXD::STD::greater_equal<>()(40.0f, 40.0f), "Functional");
	}

	// less_equal <>
	{
		CTC_ASSERT(!FXD::STD::less_equal<>()(40, 2), "Functional - FXD::STD::less_equal< FXD::S32 >()");
		CTC_ASSERT( FXD::STD::less_equal<>()(2, 40), "Functional - FXD::STD::less_equal< FXD::S32 >()");
		CTC_ASSERT( FXD::STD::less_equal<>()(40, 40), "Functional - FXD::STD::less_equal< FXD::S32 >()");
		CTC_ASSERT(!FXD::STD::less_equal<>()(40.0f, 2.0f), "Functional - FXD::STD::less_equal< FXD::F32 >()");
		CTC_ASSERT( FXD::STD::less_equal<>()(2.0f, 40.0f), "Functional - FXD::STD::less_equal< FXD::F32 >()");
		CTC_ASSERT( FXD::STD::less_equal<>()(40.0f, 40.0f), "Functional - FXD::STD::less_equal< FXD::F32 >()");

		PRINT_COND_ASSERT(!FXD::STD::less_equal<>()(40, 2), "Functional");
		PRINT_COND_ASSERT( FXD::STD::less_equal<>()(2, 40), "Functional");
		PRINT_COND_ASSERT( FXD::STD::less_equal<>()(40, 40), "Functional");
		PRINT_COND_ASSERT(!FXD::STD::less_equal<>()(40.0f, 2.0f), "Functional");
		PRINT_COND_ASSERT( FXD::STD::less_equal<>()(2.0f, 40.0f), "Functional");
		PRINT_COND_ASSERT( FXD::STD::less_equal<>()(40.0f, 40.0f), "Functional");
	}

	return nErrorCount;
}

static FXD::S32 TestLogicalOperations(void)
{
	FXD::S32 nErrorCount = 0;

	// logical_and <>
	{
		CTC_ASSERT( FXD::STD::logical_and<>()(true, true), "Functional - FXD::STD::logical_and<>()(true, true)");
		CTC_ASSERT(!FXD::STD::logical_and<>()(true, false), "Functional - FXD::STD::logical_and<>()(true, false)");
		CTC_ASSERT(!FXD::STD::logical_and<>()(false, true), "Functional - FXD::STD::logical_and<>()(false, true)");
		CTC_ASSERT(!FXD::STD::logical_and<>()(false, false), "Functional - FXD::STD::logical_and<>()(false, false)");
	
		PRINT_COND_ASSERT( FXD::STD::logical_and<>()(true, true), "Functional");
		PRINT_COND_ASSERT(!FXD::STD::logical_and<>()(true, false), "Functional");
		PRINT_COND_ASSERT(!FXD::STD::logical_and<>()(false, true), "Functional");
		PRINT_COND_ASSERT(!FXD::STD::logical_and<>()(false, false), "Functional");
	}

	// logical_or <>
	{
		CTC_ASSERT( FXD::STD::logical_or<>()(true, true), "Functional - FXD::STD::logical_or<>()(true, true)");
		CTC_ASSERT( FXD::STD::logical_or<>()(true, false), "Functional - FXD::STD::logical_or<>()(true, false)");
		CTC_ASSERT( FXD::STD::logical_or<>()(false, true), "Functional - FXD::STD::logical_or<>()(false, true)");
		CTC_ASSERT(!FXD::STD::logical_or<>()(false, false), "Functional - FXD::STD::logical_or<>()(false, false)");
	
		PRINT_COND_ASSERT( FXD::STD::logical_or<>()(true, true), "Functional");
		PRINT_COND_ASSERT( FXD::STD::logical_or<>()(true, false), "Functional");
		PRINT_COND_ASSERT( FXD::STD::logical_or<>()(false, true), "Functional");
		PRINT_COND_ASSERT(!FXD::STD::logical_or<>()(false, false), "Functional");
	}

	// logical_not <>
	{
		CTC_ASSERT(!FXD::STD::logical_not<>()(true), "Functional - FXD::STD::logical_not<>()(true)");
		CTC_ASSERT( FXD::STD::logical_not<>()(false), "Functional - FXD::STD::logical_not<>()(false)");

		PRINT_COND_ASSERT(!FXD::STD::logical_not<>()(true), "Functional");
		PRINT_COND_ASSERT( FXD::STD::logical_not<>()(false), "Functional");
	}

	return nErrorCount;
}

static FXD::S32 TestBitwiseOperations(void)
{
	FXD::S32 nErrorCount = 0;

	// bit_and <>
	{
		CTC_ASSERT(FXD::STD::bit_and<>()(3, 2) == 2, "Functional -FXD::STD::bit_and< FXD::S32 >()");

		PRINT_COND_ASSERT(FXD::STD::bit_and<>()(3, 2) == 2, "Functional");
	}

	// bit_or <>
	{
		CTC_ASSERT(FXD::STD::bit_or<>()(1, 2) == 3, "Functional - FXD::STD::bit_or< FXD::S32 >()");

		PRINT_COND_ASSERT(FXD::STD::bit_or<>()(1, 2) == 3, "Functional");
	}

	// bit_xor <>
	{
		CTC_ASSERT(FXD::STD::bit_xor<>()(1, 1) == 0, "Functional - FXD::STD::bit_xor< FXD::S32 >()");

		PRINT_COND_ASSERT(FXD::STD::bit_xor<>()(1, 1) == 0, "Functional");
	}

	// bit_xor <>
	{
		CTC_ASSERT(FXD::STD::bit_not< FXD::S32 >()(FXD::STD::bit_not<>()(0)) == 0, "Functional - FXD::STD::bit_not< FXD::S32 >()");

		PRINT_COND_ASSERT(FXD::STD::bit_not< FXD::S32 >()(FXD::STD::bit_not<>()(0)) == 0, "Functional");
	}

	return nErrorCount;
}

static FXD::S32 TestNegators(void)
{
	FXD::S32 nErrorCount = 0;

	// not_fn <>
	{
		FXD::Container::Vector< FXD::S32 > vecInt = { 99, 6264, 41, 18467, 6334, 26500, 19169 };
		auto divisible_by_3 = [](FXD::S32 nVal) { return nVal % 3 == 0; };

		PRINT_COND_ASSERT(FXD::STD::count_if(vecInt.begin(), vecInt.end(), divisible_by_3) == 2, "Functional");
		PRINT_COND_ASSERT(FXD::STD::count_if(vecInt.begin(), vecInt.end(), FXD::STD::not_fn(divisible_by_3)) == 5, "Functional");
	}

	// unary_negate <>
	{
		struct _LessThan7 : FXD::STD::unary_function< FXD::S32, bool >
		{
			bool operator()(FXD::S32 nVal) const
			{
				return (nVal < 7);
			}
		};
		FXD::STD::unary_negate< _LessThan7 > not_less_than_7((_LessThan7()));

		FXD::Container::Vector< FXD::S32 > vecInt = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

		PRINT_COND_ASSERT(FXD::STD::count_if(vecInt.begin(), vecInt.end(), not_less_than_7) == 3, "Functional");
	}

	// binary_negate <>
	{
		struct _Same : FXD::STD::binary_function< FXD::S32, FXD::S32, bool >
		{
			bool operator()(FXD::S32 lhs, FXD::S32 rhs) const
			{
				return (lhs == rhs);
			}
		};
		FXD::STD::binary_negate< _Same > notSame((_Same()));

		FXD::Container::Vector< FXD::S32 > vecInt1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::Container::Vector< FXD::S32 > vecInt2 = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
		FXD::Container::Vector< bool > vecInt3(vecInt1.size());
		FXD::STD::transform(vecInt1.begin(), vecInt1.end(), vecInt2.begin(), vecInt3.begin(), notSame);

		PRINT_COND_ASSERT(FXD::STD::count_if(vecInt3.begin(), vecInt3.end(), [](bool bVal) { return bVal; }) == 9, "Functional");
	}

	return nErrorCount;
}

static FXD::S32 TestSearchers(void)
{
	FXD::S32 nErrorCount = 0;

	// default_searcher TODO
	{
	}

	// default_searcher TODO
	{
	}

	// boyer_moore_horspool_searcher TODO
	{
	}

	return nErrorCount;
}

static FXD::S32 TestBase(void)
{
	FXD::S32 nErrorCount = 0;

	// unary_function <>
	{
		CTC_ASSERT((FXD::STD::is_same< FXD::STD::unary_function< FXD::S32, bool >::argument_type, FXD::S32 >::value), "Functional - FXD::STD::unary_function< FXD::S32, bool >::argument_type");
		CTC_ASSERT((FXD::STD::is_same< FXD::STD::unary_function< FXD::S32, bool >::result_type, bool >::value), "Functional - FXD::STD::unary_function< FXD::S32, bool >::result_type");
	
		PRINT_COND_ASSERT((FXD::STD::is_same< FXD::STD::unary_function< FXD::S32, bool >::argument_type, FXD::S32 >::value), "Functional");
		PRINT_COND_ASSERT((FXD::STD::is_same< FXD::STD::unary_function< FXD::S32, bool >::result_type, bool >::value), "Functional");
	}

	// binary_function <>
	{
		CTC_ASSERT((FXD::STD::is_same< FXD::STD::binary_function< FXD::S32, FXD::S16, bool >::first_argument_type, FXD::S32 >::value), "Functional - FXD::STD::binary_function< FXD::S32, FXD::S16, bool >::first_argument_type");
		CTC_ASSERT((FXD::STD::is_same< FXD::STD::binary_function< FXD::S32, FXD::S16, bool >::second_argument_type, FXD::S16 >::value), "Functional - FXD::STD::binary_function< FXD::S32, FXD::S16, bool >::second_argument_type");
		CTC_ASSERT((FXD::STD::is_same< FXD::STD::binary_function< FXD::S32, FXD::S16, bool >::result_type, bool >::value), "Functional - FXD::STD::binary_function< FXD::S32, FXD::S16, bool >::result_type");

		PRINT_COND_ASSERT((FXD::STD::is_same< FXD::STD::binary_function< FXD::S32, FXD::S16, bool >::first_argument_type, FXD::S32 >::value), "Functional");
		PRINT_COND_ASSERT((FXD::STD::is_same< FXD::STD::binary_function< FXD::S32, FXD::S16, bool >::second_argument_type, FXD::S16 >::value), "Functional");
		PRINT_COND_ASSERT((FXD::STD::is_same< FXD::STD::binary_function< FXD::S32, FXD::S16, bool >::result_type, bool >::value), "Functional");
	}

	return nErrorCount;
}

FXD::S32 TestFunctional(void)
{
	FXD::S32 nErrorCount = 0;

	TestObject::reset();

	{
		nErrorCount += TestClasses();
		nErrorCount += TestHashing();
		nErrorCount += TestFunctions();
		nErrorCount += TestArithmeticOperations();
		nErrorCount += TestComparisons();
		nErrorCount += TestLogicalOperations();
		nErrorCount += TestBitwiseOperations();
		nErrorCount += TestNegators();
		nErrorCount += TestSearchers();
		nErrorCount += TestBase();
	}
	
	// identity
	/*
	{
		CTC_ASSERT((sizeof(FXD::STD::identity< FXD::S32 >::type) == sizeof(FXD::S32)), "Functional - identity< FXD::S32 >");
		CTC_ASSERT((FXD::STD::is_same< FXD::S32, FXD::STD::identity< FXD::S32 >::type >::value), "Functional - identity< FXD::S32 >");

		CTC_ASSERT((sizeof(FXD::STD::identity_t< FXD::S32 >) == sizeof(FXD::S32)), "Functional - identity_t< FXD::S32 >");
		CTC_ASSERT((FXD::STD::is_same< FXD::S32, FXD::STD::identity_t< FXD::S32 > >::value), "Functional - identity_t< FXD::S32 >");
	}

	{
		// str_equal_to
		char p0[] = "";
		char p1[] = "hello";
		char p2[] = "world";
		char p3[] = "helllllo";
		char p4[] = "hello"; // Intentionally the same value as p1.

							 // str_equal_to
		typedef hash_set<const char*, hash<const char*>, str_equal_to<const char*> > StringHashSet;
		StringHashSet shs;

		shs.insert(p1);
		shs.insert(p2);
		shs.insert(p3);

		StringHashSet::iterator it = shs.find(p0);
		EATEST_VERIFY(it == shs.end());

		it = shs.find(p1);
		EATEST_VERIFY(it != shs.end());

		it = shs.find(p2);
		EATEST_VERIFY(it != shs.end());

		it = shs.find(p4);
		EATEST_VERIFY(it != shs.end());
	}

	{
		// str_less<const char8_t*>
		Results<char> results8[] =
		{
			{ "",          "", false },
			{ "",         "a",  true },
			{ "a",         "", false },
			{ "a",        "a", false },
			{ "a",        "b",  true },
			{ "____a",    "____a", false },
			{ "____a",    "____b",  true },
			{ "____b",    "____a", false },
			{ "_\xff",       "_a", false },    // Test high values, which exercises the signed/unsiged comparison behavior.
			{ "_a",    "_\xff",  true }
		};

		str_less<const char*> sl8;
		for (size_t i = 0; i < EAArrayCount(results8); i++)
		{
			// Verify that our test is in line with the strcmp function.
			bool bResult = (EA::StdC::Strcmp(results8[i].p1, results8[i].p2) < 0);
			EATEST_VERIFY_F(bResult == results8[i].expectedResult, "Strcmp failure, test %zu. Expected \"%s\" to be %sless than \"%s\"", i, results8[i].p1, results8[i].expectedResult ? "" : "not ", results8[i].p2);

			// Verify that str_less achieves the expected results.
			bResult = sl8(results8[i].p1, results8[i].p2);
			EATEST_VERIFY_F(bResult == results8[i].expectedResult, "str_less test failure, test %zu. Expected \"%s\" to be %sless than \"%s\"", i, results8[i].p1, results8[i].expectedResult ? "" : "not ", results8[i].p2);
		}

		// str_less<const wchar_t*>
		Results<wchar_t> resultsW[] =
		{
			{ L"",            L"", false },
			{ L"",           L"a",  true },
			{ L"a",           L"", false },
			{ L"a",          L"a", false },
			{ L"a",          L"b",  true },
			{ L"____a",      L"____a", false },
			{ L"____a",      L"____b",  true },
			{ L"____b",      L"____a", false },
			{ L"_\xffff",         L"_a", false },   // Test high values, which exercises the signed/unsiged comparison behavior.
			{ L"_a",    L"_\xffff",  true }
		};

		str_less<const wchar_t*> slW;
		for (size_t i = 0; i < EAArrayCount(resultsW); i++)
		{
			// Verify that our test is in line with the strcmp function.
			bool bResult = (EA::StdC::Strcmp(resultsW[i].p1, resultsW[i].p2) < 0);
			EATEST_VERIFY_F(bResult == resultsW[i].expectedResult, "Strcmp failure, test %zu. Expected \"%s\" to be %sless than \"%s\"", i, results8[i].p1, results8[i].expectedResult ? "" : "not ", results8[i].p2);

			// Verify that str_less achieves the expected results.
			bResult = slW(resultsW[i].p1, resultsW[i].p2);
			EATEST_VERIFY_F(bResult == resultsW[i].expectedResult, "str_less test failure, test %zu. Expected \"%ls\" to be %sless than \"%ls\"", i, resultsW[i].p1, resultsW[i].expectedResult ? "" : "not ", resultsW[i].p2);
		}
	}

	{
		// str_less
		char p0[] = "";
		char p1[] = "hello";
		char p2[] = "world";
		char p3[] = "helllllo";
		char p4[] = "hello"; // Intentionally the same value as p1.

		typedef set<const char*, str_less<const char*> > StringSet;
		StringSet ss;

		ss.insert(p1);
		ss.insert(p2);
		ss.insert(p3);

		StringSet::iterator it = ss.find(p0);
		EATEST_VERIFY(it == ss.end());

		it = ss.find(p1);
		EATEST_VERIFY(it != ss.end());

		it = ss.find(p2);
		EATEST_VERIFY(it != ss.end());

		it = ss.find(p4);
		EATEST_VERIFY(it != ss.end());
	}

	{
		// equal_to_2
		N1 n11(1);
		N1 n13(3);
		N2 n21(1);
		N2 n22(2);
		//const N1 cn11(1);
		//const N1 cn13(3);

		equal_to_2<N1, N2> e;
		EATEST_VERIFY(e(n11, n21));
		EATEST_VERIFY(e(n21, n11));

		equal_to_2<N1, N1> es;
		EATEST_VERIFY(es(n11, n11));

		//equal_to_2<const N1, N1> ec; // To do: Make this case work.
		//EATEST_VERIFY(e(cn11, n11));

		// not_equal_to_2
		not_equal_to_2<N1, N2> n;
		EATEST_VERIFY(n(n11, n22));
		EATEST_VERIFY(n(n22, n11));

		not_equal_to_2<N1, N1> ns;
		EATEST_VERIFY(ns(n11, n13));

		// less_2
		less_2<N1, N2> le;
		EATEST_VERIFY(le(n11, n22));
		EATEST_VERIFY(le(n22, n13));

		less_2<N1, N1> les;
		EATEST_VERIFY(les(n11, n13));
	}

	{
		// Test defect report entry #297.
		const X x;
		foo< const_mem_fun_t<void, X> >(&x);
	}

	{
		// mem_fun (no argument version)
		TestClass  tc0, tc1, tc2;
		TestClass* tcArray[3] = { &tc0, &tc1, &tc2 };

		for_each(tcArray, tcArray + 3, mem_fun(&TestClass::Increment));
		EATEST_VERIFY((tc0.mX == 38) && (tc1.mX == 38) && (tc2.mX == 38));

		for_each(tcArray, tcArray + 3, mem_fun(&TestClass::IncrementConst));
		EATEST_VERIFY((tc0.mX == 39) && (tc1.mX == 39) && (tc2.mX == 39));
	}

	{
		// mem_fun (one argument version)
		TestClass  tc0, tc1, tc2;
		TestClass* tcArray[3] = { &tc0, &tc1, &tc2 };
		int        intArray1[3] = { -1,  0,  2 };
		int        intArray2[3] = { -9, -9, -9 };

		transform(tcArray, tcArray + 3, intArray1, intArray2, mem_fun(&TestClass::MultiplyBy));
		EATEST_VERIFY((intArray2[0] == -37) && (intArray2[1] == 0) && (intArray2[2] == 74));

		intArray2[0] = intArray2[1] = intArray2[2] = -9;
		transform(tcArray, tcArray + 3, intArray1, intArray2, mem_fun(&TestClass::MultiplyByConst));
		EATEST_VERIFY((intArray2[0] == -37) && (intArray2[1] == 0) && (intArray2[2] == 74));
	}

	{
		// mem_fun_ref (no argument version)
		TestClass tcArray[3];

		for_each(tcArray, tcArray + 3, mem_fun_ref(&TestClass::Increment));
		EATEST_VERIFY((tcArray[0].mX == 38) && (tcArray[1].mX == 38) && (tcArray[2].mX == 38));

		for_each(tcArray, tcArray + 3, mem_fun_ref(&TestClass::IncrementConst));
		EATEST_VERIFY((tcArray[0].mX == 39) && (tcArray[1].mX == 39) && (tcArray[2].mX == 39));
	}


	{
		// mem_fun_ref (one argument version)
		TestClass tcArray[3];
		int       intArray1[3] = { -1,  0,  2 };
		int       intArray2[3] = { -9, -9, -9 };

		transform(tcArray, tcArray + 3, intArray1, intArray2, mem_fun_ref(&TestClass::MultiplyBy));
		EATEST_VERIFY((intArray2[0] == -37) && (intArray2[1] == 0) && (intArray2[2] == 74));

		intArray2[0] = intArray2[1] = intArray2[2] = -9;
		transform(tcArray, tcArray + 3, intArray1, intArray2, mem_fun_ref(&TestClass::MultiplyByConst));
		EATEST_VERIFY((intArray2[0] == -37) && (intArray2[1] == 0) && (intArray2[2] == 74));
	}


	{
		// Template instantations.
		// These tell the compiler to compile all the functions for the given class.
		eastl::hash_set<String8MA, eastl::string_hash<String8MA> >  hs8;
		eastl::hash_set<String16MA, eastl::string_hash<String16MA> > hs16;

		EATEST_VERIFY(hs8.empty());
		EATEST_VERIFY(hs16.empty());
	}

	{
		// unary_compose
		//eastl::vector<double> angles;
		//eastl::vector<double> sines;

		//eastl::transform(angles.begin(), angles.end(), sines.begin(),
		//eastl::compose1(eastl::negate<double>(),
		//eastl::compose1(eastl::ptr_fun(sin),
		//eastl::bind2nd(eastl::multiplies<double>(), 3.14159 / 180.0))));

		// binary_compose
		list<int> L;

		eastl::list<int>::iterator in_range =
			eastl::find_if(L.begin(), L.end(),
				eastl::compose2(eastl::logical_and<bool>(),
					eastl::bind2nd(eastl::greater_equal<int>(), 1),
					eastl::bind2nd(eastl::less_equal<int>(), 10)));
		EATEST_VERIFY(in_range == L.end());
	}

	{
		nErrorCount += TestHashHelper<int>(4330);
		nErrorCount += TestHashHelper<bool>(true);
		nErrorCount += TestHashHelper<char>('E');
		nErrorCount += TestHashHelper<signed char>('E');
		nErrorCount += TestHashHelper<unsigned char>('E');
		nErrorCount += TestHashHelper<char8_t>('E');
		nErrorCount += TestHashHelper<char16_t>(0xEAEA);
		nErrorCount += TestHashHelper<char32_t>(0x00EA4330);
#if !defined(EA_WCHAR_T_NON_NATIVE)
		nErrorCount += TestHashHelper<wchar_t>(L'E');
#endif
		nErrorCount += TestHashHelper<signed short>(4330);
		nErrorCount += TestHashHelper<unsigned short>(4330u);
		nErrorCount += TestHashHelper<signed int>(4330);
		nErrorCount += TestHashHelper<unsigned int>(4330u);
		nErrorCount += TestHashHelper<signed long>(4330l);
		nErrorCount += TestHashHelper<unsigned long>(4330ul);
		nErrorCount += TestHashHelper<signed long long>(4330ll);
		nErrorCount += TestHashHelper<unsigned long long>(4330ll);
		nErrorCount += TestHashHelper<float>(4330.099999f);
		nErrorCount += TestHashHelper<double>(4330.055);
		nErrorCount += TestHashHelper<long double>(4330.0654l);

		{
			enum hash_enum_test { e1, e2, e3 };
			nErrorCount += TestHashHelper<hash_enum_test>(e1);
			nErrorCount += TestHashHelper<hash_enum_test>(e2);
			nErrorCount += TestHashHelper<hash_enum_test>(e3);
		}
	}


#if defined(EA_COMPILER_CPP11_ENABLED) && EASTL_VARIADIC_TEMPLATES_ENABLED
	// On platforms do not support variadic templates the eastl::invoke (FXD::STD::mem_fn is built on eastl::invoke)
	// implementation is extremely basic and does not hold up.  A significant amount of code would have to be written
	// and I don't believe the investment is justified at this point.  If you require this functionality on older
	// compilers please contact us.
	//


	// mem_fn
	{
		struct AddingStruct
		{
			AddingStruct(int inValue) : value(inValue) {}
			void Add(int addAmount) { value += addAmount; }
			void Add2(int add1, int add2) { value += (add1 + add2); }
			int value;
		};

		struct OverloadedStruct
		{
			OverloadedStruct(int inValue) : value(inValue) {}
			int &Value() { return value; }
			const int &Value() const { return value; }
			int value;
		};

		{
			AddingStruct a(42);
			FXD::STD::mem_fn(&AddingStruct::Add)(a, 6);
			EATEST_VERIFY(a.value == 48);
		}
		{
			AddingStruct a(42);
			FXD::STD::mem_fn(&AddingStruct::Add2)(a, 3, 3);
			EATEST_VERIFY(a.value == 48);
		}
		{
			AddingStruct a(42);
			auto fStructAdd = FXD::STD::mem_fn(&AddingStruct::Add);
			fStructAdd(a, 6);
			EATEST_VERIFY(a.value == 48);
		}
		{
			OverloadedStruct a(42);
			EATEST_VERIFY(FXD::STD::mem_fn<int &()>(&OverloadedStruct::Value)(a) == 42);
			EATEST_VERIFY(FXD::STD::mem_fn<const int &() const>(&OverloadedStruct::Value)(a) == 42);
		}
	}
#endif

	// eastl::function
	{
		{
			{
				struct Functor { int operator()() { return 42; } };
				eastl::function<int(void)> fn = Functor();
				EATEST_VERIFY(fn() == 42);
			}

			{
				struct Functor { int operator()(int in) { return in; } };
				eastl::function<int(int)> fn = Functor();
				EATEST_VERIFY(fn(24) == 24);
			}
		}

		{
			int val = 0;
			auto lambda = [&val] { ++val; };
			{
				eastl::function<void(void)> ff = std::bind(lambda);
				ff();
				VERIFY(val == 1);
			}
			{
				eastl::function<void(void)> ff = nullptr;
				ff = std::bind(lambda);
				ff();
				VERIFY(val == 2);
			}
		}

		{
			int val = 0;
			{
				eastl::function<int(int*)> ff = &TestIntRet;
				int ret = ff(&val);
				EATEST_VERIFY(ret == 0);
				EATEST_VERIFY(val == 1);
			}
			{
				eastl::function<int(int*)> ff;
				ff = &TestIntRet;
				int ret = ff(&val);
				EATEST_VERIFY(ret == 1);
				EATEST_VERIFY(val == 2);
			}
		}

		{
			struct Test { int x = 1; };
			Test t;
			const Test ct;

			{
				eastl::function<int(const Test&)> ff = &Test::x;
				int ret = ff(t);
				EATEST_VERIFY(ret == 1);
			}
			{
				eastl::function<int(const Test&)> ff = &Test::x;
				int ret = ff(ct);
				EATEST_VERIFY(ret == 1);
			}
			{
				eastl::function<int(const Test&)> ff;
				ff = &Test::x;
				int ret = ff(t);
				EATEST_VERIFY(ret == 1);
			}
			{
				eastl::function<int(const Test&)> ff;
				ff = &Test::x;
				int ret = ff(ct);
				EATEST_VERIFY(ret == 1);
			}
		}

		{
			struct TestVoidRet
			{
				void IncX() const
				{
					++x;
				}

				void IncX()
				{
					++x;
				}

				mutable int x = 0;
			};

			TestVoidRet voidRet;
			const TestVoidRet cvoidRet;

			{
				eastl::function<void(const TestVoidRet&)> ff = static_cast<void(TestVoidRet::*)() const>(&TestVoidRet::IncX);
				ff(cvoidRet);
				VERIFY(cvoidRet.x == 1);
			}
			{
				eastl::function<void(const TestVoidRet&)> ff = static_cast<void(TestVoidRet::*)() const>(&TestVoidRet::IncX);
				ff(voidRet);
				VERIFY(voidRet.x == 1);
			}
			{
				eastl::function<void(TestVoidRet&)> ff = static_cast<void(TestVoidRet::*)()>(&TestVoidRet::IncX);
				ff(voidRet);
				VERIFY(voidRet.x == 2);
			}
		}

		{
			int val = 0;
			struct Functor { void operator()(int* p) { *p += 1; } };
			Functor functor;
			{
				eastl::function<void(int*)> ff = eastl::reference_wrapper<Functor>(functor);
				ff(&val);
				EATEST_VERIFY(val == 1);
			}

			{
				eastl::function<void(int*)> ff;
				ff = eastl::reference_wrapper<Functor>(functor);
				ff(&val);
				EATEST_VERIFY(val == 2);
			}
		}

		{
			{
				auto lambda = [] {};
				EA_UNUSED(lambda);
				CTC_ASSERT(internal::is_functor_inplace_allocatable<decltype(lambda), EASTL_FUNCTION_DEFAULT_CAPTURE_SSO_SIZE>::value == true, "lambda equivalent to function pointer does not fit in eastl::function local memory.");
			}

			{
				eastl::function<void(void)> fn;

				EATEST_VERIFY(!fn);
				fn = [] {};
				EATEST_VERIFY(!!fn);
			}

			{
				eastl::function<int(int)> fn = [](int param) { return param; };
				EATEST_VERIFY(fn(42) == 42);
			}

			{
				eastl::function<int(int)> fn = ReturnVal;
				EATEST_VERIFY(fn(42) == 42);
			}

			{
				eastl::function<int()> fn0 = ReturnZero;
				eastl::function<int()> fn1 = ReturnOne;

				EATEST_VERIFY(fn0() == 0 && fn1() == 1);
				swap(fn0, fn1);
				EATEST_VERIFY(fn0() == 1 && fn1() == 0);
			}

			{
				eastl::function<int()> fn0 = ReturnZero;
				eastl::function<int()> fn1 = ReturnOne;

				EATEST_VERIFY(fn0() == 0 && fn1() == 1);
				fn0 = fn1;
				EATEST_VERIFY(fn0() == 1 && fn1() == 1);
			}

			{
				eastl::function<int()> fn0 = ReturnZero;
				eastl::function<int()> fn1 = ReturnOne;

				EATEST_VERIFY(fn0() == 0 && fn1() == 1);
				fn0 = eastl::move(fn1);
				EATEST_VERIFY(fn0() == 1 && fn1 == nullptr);
			}

			{
				eastl::function<int(int)> f1(nullptr);
				EATEST_VERIFY(!f1);

				eastl::function<int(int)> f2 = nullptr;
				EATEST_VERIFY(!f2);
			}
		}

		{
			// test the default allocator path by using a lambda capture too large to fit into the eastl::function local
			// storage.
			uint64_t a = 1, b = 2, c = 3, d = 4, e = 5, f = 6;
			eastl::function<uint64_t(void)> fn = [=] { return a + b + c + d + e + f; };
			auto result = fn();
			EATEST_VERIFY(result == 21);
		}

		{
			struct Functor { void operator()() { return; } };
			eastl::function<void(void)> fn;
			eastl::function<void(void)> fn2 = nullptr;
			EATEST_VERIFY(!fn);
			EATEST_VERIFY(!fn2);
			EATEST_VERIFY(fn == nullptr);
			EATEST_VERIFY(fn2 == nullptr);
			EATEST_VERIFY(nullptr == fn);
			EATEST_VERIFY(nullptr == fn2);
			fn = Functor();
			fn2 = Functor();
			EATEST_VERIFY(!!fn);
			EATEST_VERIFY(!!fn2);
			EATEST_VERIFY(fn != nullptr);
			EATEST_VERIFY(fn2 != nullptr);
			EATEST_VERIFY(nullptr != fn);
			EATEST_VERIFY(nullptr != fn2);
			fn = nullptr;
			fn2 = fn;
			EATEST_VERIFY(!fn);
			EATEST_VERIFY(!fn2);
			EATEST_VERIFY(fn == nullptr);
			EATEST_VERIFY(fn2 == nullptr);
			EATEST_VERIFY(nullptr == fn);
			EATEST_VERIFY(nullptr == fn2);
		}

		{
			using eastl::swap;
			struct Functor { int operator()() { return 5; } };
			eastl::function<int(void)> fn = Functor();
			eastl::function<int(void)> fn2;
			EATEST_VERIFY(fn() == 5);
			EATEST_VERIFY(!fn2);
			fn.swap(fn2);
			EATEST_VERIFY(!fn);
			EATEST_VERIFY(fn2() == 5);
			swap(fn, fn2);
			EATEST_VERIFY(fn() == 5);
			EATEST_VERIFY(!fn2);
		}

		{
			uint64_t a = 1, b = 2, c = 3, d = 4, e = 5, f = 6;
			eastl::function<uint64_t(void)> fn([=] { return a + b + c + d + e + f; });

			auto result = fn();
			EATEST_VERIFY(result == 21);
		}

		// user regression "self assigment" tests
		{
			eastl::function<int(void)> fn = [cache = 0]() mutable  { return cache++; };

			EATEST_VERIFY(fn() == 0);
			EATEST_VERIFY(fn() == 1);
			EATEST_VERIFY(fn() == 2);

			EA_DISABLE_CLANG_WARNING(-Wunknown - pragmas)
				EA_DISABLE_CLANG_WARNING(-Wunknown - warning - option)
				EA_DISABLE_CLANG_WARNING(-Wself - assign - overloaded)
				fn = fn;
			EA_RESTORE_CLANG_WARNING()
				EA_RESTORE_CLANG_WARNING()
				EA_RESTORE_CLANG_WARNING()

				EATEST_VERIFY(fn() == 3);
			EATEST_VERIFY(fn() == 4);
			EATEST_VERIFY(fn() == 5);

			fn = eastl::move(fn);

			EATEST_VERIFY(fn() == 6);
			EATEST_VERIFY(fn() == 7);
			EATEST_VERIFY(fn() == 8);
		}

		// user regression for memory leak when re-assigning an eastl::function which already holds a large closure.
		{
			static int sCtorCount = 0;
			static int sDtorCount = 0;

			{
				struct local
				{
					local() { sCtorCount++; }
					local(const local&) { sCtorCount++; }
					local(local&&) { sCtorCount++; }
					~local() { sDtorCount++; }

					void operator=(const local&) = delete; // suppress msvc warning
				} l;

				eastl::function<bool()> f;

				f = [l]() { return false; };

				// ensure closure resources are cleaned up when assigning to a non-null eastl::function.
				f = [l]() { return true; };
			}

			EATEST_VERIFY(sCtorCount == sDtorCount);
		}
	}

	// Checking _MSC_EXTENSIONS is required because the Microsoft calling convention classifiers are only available when
	// compiler specific C/C++ language extensions are enabled.
#if defined(EA_PLATFORM_MICROSOFT) && defined(_MSC_EXTENSIONS)
	{
		// no arguments
		typedef void(__stdcall * StdCallFunction)();
		typedef void(__cdecl * CDeclFunction)();

		// only varargs
		typedef void(__stdcall * StdCallFunctionWithVarargs)(...);
		typedef void(__cdecl * CDeclFunctionWithVarargs)(...);

		// arguments and varargs
		typedef void(__stdcall * StdCallFunctionWithVarargsAtEnd)(int, int, int, ...);
		typedef void(__cdecl * CDeclFunctionWithVarargsAtEnd)(int, short, long, ...);

		CTC_ASSERT(!eastl::is_function<StdCallFunction>::value, "is_function failure");
		CTC_ASSERT(!eastl::is_function<CDeclFunction>::value, "is_function failure");
		CTC_ASSERT(eastl::is_function<typename eastl::remove_pointer<StdCallFunction>::type>::value, "is_function failure");
		CTC_ASSERT(eastl::is_function<typename eastl::remove_pointer<CDeclFunction>::type>::value, "is_function failure");
		CTC_ASSERT(eastl::is_function<typename eastl::remove_pointer<StdCallFunctionWithVarargs>::type>::value, "is_function failure");
		CTC_ASSERT(eastl::is_function<typename eastl::remove_pointer<CDeclFunctionWithVarargs>::type>::value, "is_function failure");
		CTC_ASSERT(eastl::is_function<typename eastl::remove_pointer<StdCallFunctionWithVarargsAtEnd>::type>::value, "is_function failure");
		CTC_ASSERT(eastl::is_function<typename eastl::remove_pointer<CDeclFunctionWithVarargsAtEnd>::type>::value, "is_function failure");
	}
#endif

	// Test Function Objects
#if defined(EA_COMPILER_CPP14_ENABLED)
	{
	}
#endif
	*/
	return nErrorCount;
}

/*
// Test that we can instantiate invoke_result with incorrect argument types.
// This should be instantiable, but should not have a `type` typedef.
struct TestInvokeResult
{
	int f(int i) { return i; }
};

template struct eastl::invoke_result<decltype(&TestInvokeResult::f), TestInvokeResult, void>;
CTC_ASSERT(!eastl::is_invocable<decltype(&TestInvokeResult::f), TestInvokeResult, void>::value, "incorrect value for is_invocable");
CTC_ASSERT(eastl::is_invocable<decltype(&TestInvokeResult::f), TestInvokeResult, int>::value, "incorrect value for is_invocable");
*/