// Creator - MatthewGolder

template < typename StringType >
FXD::S32 TEST_STRING_NAME(void)
{
	FXD::S32 nErrorCount = 0;

	// StringBase(void);
	{
		StringType str;
		PRINT_COND_ASSERT(str.empty(), "StringBase - StringBase(void)");
		PRINT_COND_ASSERT(str.length() == 0, "StringBase - StringBase(void)");
		PRINT_COND_ASSERT(str.validate(), "StringBase - StringBase(void)");
	}

	// StringBase(const_pointer pStr, size_type nCount);
	{
		{
			StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"), 26);
			PRINT_COND_ASSERT(str[5] == LITERAL('f'), "StringBase - StringBase(const_pointer pStr, size_type nCount)");
			PRINT_COND_ASSERT(str.empty() == false, "StringBase - StringBase(const_pointer pStr, size_type nCount)");
			PRINT_COND_ASSERT(str.length() == 26, "StringBase - StringBase(const_pointer pStr, size_type nCount)");
			PRINT_COND_ASSERT(str.validate(), "StringBase - StringBase(const_pointer pStr, size_type nCount)");
		}
		{
			StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));
			PRINT_COND_ASSERT(str[5] == LITERAL('f'), "StringBase - StringBase(const_pointer pStr, size_type nCount)");
			PRINT_COND_ASSERT(str.empty() == false, "StringBase - StringBase(const_pointer pStr, size_type nCount)");
			PRINT_COND_ASSERT(str.length() == 26, "StringBase - StringBase(const_pointer pStr, size_type nCount)");
			PRINT_COND_ASSERT(str.validate(), "StringBase - StringBase(const_pointer pStr, size_type nCount)");
		}
	}

	// StringBase(const my_type& str, size_type nSubpos, size_type nSublen = npos);
	{
		StringType str1(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		StringType str2(str1, 3, 3);
		PRINT_COND_ASSERT(str2 == LITERAL("def"), "StringBase - StringBase(const my_type& str, size_type nSubpos, size_type nSublen = npos)");
		PRINT_COND_ASSERT(str2.size() == 3, "StringBase - StringBase(const my_type& str, size_type nSubpos, size_type nSublen = npos)");
		PRINT_COND_ASSERT(str2.length() == 3, "StringBase - StringBase(const my_type& str, size_type nSubpos, size_type nSublen = npos)");
		PRINT_COND_ASSERT(str2.capacity() >= 3, "StringBase - StringBase(const my_type& str, size_type nSubpos, size_type nSublen = npos)");

		StringType str3(str1, 25, 3);
		PRINT_COND_ASSERT(str3 == LITERAL("z"), "StringBase - StringBase(const my_type& str, size_type nSubpos, size_type nSublen = npos)");
		PRINT_COND_ASSERT(str3.size() == 1, "StringBase - StringBase(const my_type& str, size_type nSubpos, size_type nSublen = npos)");
		PRINT_COND_ASSERT(str3.length() == 1, "StringBase - StringBase(const my_type& str, size_type nSubpos, size_type nSublen = npos)");
		PRINT_COND_ASSERT(str3.capacity() >= 1, "StringBase - StringBase(const my_type& str, size_type nSubpos, size_type nSublen = npos)");

		PRINT_COND_ASSERT(str1.validate(), "StringBase - StringBase(const my_type& str, size_type nSubpos, size_type nSublen = npos)");
		PRINT_COND_ASSERT(str2.validate(), "StringBase - StringBase(const my_type& str, size_type nSubpos, size_type nSublen = npos)");
		PRINT_COND_ASSERT(str3.validate(), "StringBase - StringBase(const my_type& str, size_type nSubpos, size_type nSublen = npos)");
	}

	// StringBase(const_pointer pStr);
	{
		auto* pLiteral = LITERAL("abcdefghijklmnopqrstuvwxyz");
		StringType str(pLiteral);
		PRINT_COND_ASSERT(str == pLiteral, "StringBase - StringBase(const_pointer pStr)");
	}

	// StringBase(size_type nCount, const value_type ch);
	{
		StringType str(32, LITERAL('a'));
		PRINT_COND_ASSERT(!str.empty(), "StringBase - StringBase(size_type nCount, const value_type ch)");
		PRINT_COND_ASSERT(str.size() == 32, "StringBase - StringBase(size_type nCount, const value_type ch)");
		PRINT_COND_ASSERT(str.length() == 32, "StringBase - StringBase(size_type nCount, const value_type ch)");
		PRINT_COND_ASSERT(str == LITERAL("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"), "StringBase - StringBase(size_type nCount, const value_type ch)");
		PRINT_COND_ASSERT(str.validate(), "StringBase - StringBase(size_type nCount, const value_type ch)");
	}

	// StringBase(const my_type& rhs);
	{
		StringType str1(LITERAL("abcdefghijklmnopqrstuvwxyz"));
		StringType str2(str1);

		PRINT_COND_ASSERT(str1 == str2, "StringBase - StringBase(const my_type& rhs)");
		PRINT_COND_ASSERT(str1.size() == str2.size(), "StringBase - StringBase(const my_type& rhs)");
		PRINT_COND_ASSERT(str1.empty() == str2.empty(), "StringBase - StringBase(const my_type& rhs)");
		PRINT_COND_ASSERT(str1.length() == str2.length(), "StringBase - StringBase(const my_type& rhs)");
		PRINT_COND_ASSERT(FXD::Memory::MemCompare(str1.data(), str2.data(), str1.size()) == 0, "StringBase - StringBase(const my_type& rhs)");
		PRINT_COND_ASSERT(str1.validate(), "StringBase - StringBase(const my_type& rhs)");
		PRINT_COND_ASSERT(str2.validate(), "StringBase - StringBase(const my_type& rhs)");
	}

	// StringBase(InputItr first, InputItr last);
	{
		StringType str1(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		auto* pStart = str1.c_str() + 5;
		auto* pEnd = str1.c_str() + 20;

		StringType str(pStart, pEnd);
		PRINT_COND_ASSERT(str == LITERAL("fghijklmnopqrst"), "StringBase - StringBase(InputItr first, InputItr last)");
		PRINT_COND_ASSERT(!str.empty(), "StringBase - StringBase(InputItr first, InputItr last)");
		PRINT_COND_ASSERT(str.size() == 15, "StringBase - StringBase(InputItr first, InputItr last)");
	}

	// StringBase(std::initializer_list< value_type > iList);
	{
		StringType str({ 'a','b','c','d','e','f' });
		PRINT_COND_ASSERT(str == LITERAL("abcdef"), "StringBase - StringBase(std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT(!str.empty(), "StringBase - StringBase(std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT(str.length() == 6, "StringBase - StringBase(std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT(str.size() == 6, "StringBase - StringBase(std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT(str.validate(), "StringBase - StringBase(std::initializer_list< value_type > iList)");;
	}

	// StringBase(my_type&& rhs);
	{
		StringType str1(LITERAL("abcdefghijklmnopqrstuvwxyz"));
		StringType str2(std::move(str1));

		PRINT_COND_ASSERT(str1 != LITERAL("abcdefghijklmnopqrstuvwxyz"), "StringBase - StringBase(my_type&& rhs)");
		PRINT_COND_ASSERT(str2 == LITERAL("abcdefghijklmnopqrstuvwxyz"), "StringBase - StringBase(my_type&& rhs)");

		PRINT_COND_ASSERT(str1.empty(), "StringBase - StringBase(my_type&& rhs)");
		PRINT_COND_ASSERT(!str2.empty(), "StringBase - StringBase(my_type&& rhs)");

		PRINT_COND_ASSERT(str1.length() == 0, "StringBase - StringBase(my_type&& rhs)");
		PRINT_COND_ASSERT(str2.length() == 26, "StringBase - StringBase(my_type&& rhs)");

		PRINT_COND_ASSERT(str1.size() == 0, "StringBase - StringBase(my_type&& rhs)");
		PRINT_COND_ASSERT(str2.size() == 26, "StringBase - StringBase(my_type&& rhs)");

		PRINT_COND_ASSERT(str1.validate(), "StringBase - StringBase(my_type&& rhs)");
		PRINT_COND_ASSERT(str2.validate(), "StringBase - StringBase(my_type&& rhs)");
	}
	{  // test sso string
		StringType str1(LITERAL("a"));
		StringType str2(std::move(str1));

		PRINT_COND_ASSERT(str1 != LITERAL("a"), "StringBase -");
		PRINT_COND_ASSERT(str2 == LITERAL("a"), "StringBase -");

		PRINT_COND_ASSERT(str1.empty(), "StringBase -");
		PRINT_COND_ASSERT(!str2.empty(), "StringBase -");

		PRINT_COND_ASSERT(str1.length() == 0, "StringBase -");
		PRINT_COND_ASSERT(str2.length() == 1, "StringBase -");

		PRINT_COND_ASSERT(str1.size() == 0, "StringBase -");
		PRINT_COND_ASSERT(str2.size() == 1, "StringBase -");

		PRINT_COND_ASSERT(str1.validate(), "StringBase -");
		PRINT_COND_ASSERT(str2.validate(), "StringBase -");
	}

	// my_type& operator=(const my_type& rhs);
	{
		StringType str1(LITERAL("abcdefghijklmnopqrstuvwxyz"));
		StringType str1_copy(LITERAL(""));

		PRINT_COND_ASSERT(str1_copy.empty(), "StringBase - my_type& operator=(const my_type& rhs)");

		str1_copy = str1;

		PRINT_COND_ASSERT(str1 == str1_copy, "StringBase - my_type& operator=(const my_type& rhs)");
		PRINT_COND_ASSERT(!str1_copy.empty(), "StringBase - my_type& operator=(const my_type& rhs)");
		PRINT_COND_ASSERT(str1.validate(), "StringBase - my_type& operator=(const my_type& rhs)");
		PRINT_COND_ASSERT(str1_copy.validate(), "StringBase - my_type& operator=(const my_type& rhs)");
	}

	// my_type& operator=(const_pointer pStr);
	{
		StringType str;
		str = LITERAL("abcdefghijklmnopqrstuvwxyz");

		PRINT_COND_ASSERT(str[5] == LITERAL('f'), "StringBase - my_type& operator=(const_pointer pStr)");
		PRINT_COND_ASSERT(str == LITERAL("abcdefghijklmnopqrstuvwxyz"), "StringBase - my_type& operator=(const_pointer pStr)");
		PRINT_COND_ASSERT(!str.empty(), "StringBase - my_type& operator=(const_pointer pStr)");
		PRINT_COND_ASSERT(str.length() == 26, "StringBase - my_type& operator=(const_pointer pStr)");
		PRINT_COND_ASSERT(str.validate(), "StringBase - my_type& operator=(const_pointer pStr;)");
	}

	// my_type& operator=(const value_type ch);
	{
		StringType str;
		str = LITERAL('a');

		PRINT_COND_ASSERT(str == LITERAL("a"), "StringBase - my_type& operator=(const value_type ch)");
		PRINT_COND_ASSERT(!str.empty(), "StringBase - my_type& operator=(const value_type ch)");
		PRINT_COND_ASSERT(str.length() == 1, "StringBase - my_type& operator=(const value_type ch)");
		PRINT_COND_ASSERT(str.size() == 1, "StringBase - my_type& operator=(const value_type ch)");
		PRINT_COND_ASSERT(str.validate(), "StringBase - my_type& operator=(const value_type ch)");
	}

	// my_type& operator=(std::initializer_list< value_type > iList);
	{
		StringType str = { 'a','b','c','d','e','f' };

		PRINT_COND_ASSERT(str == LITERAL("abcdef"), "StringBase - my_type& operator=(std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT(!str.empty(), "StringBase - my_type& operator=(std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT(str.length() == 6, "StringBase - my_type& operator=(std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT(str.size() == 6, "StringBase - my_type& operator=(std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT(str.validate(), "StringBase - my_type& operator=(std::initializer_list< value_type > iList)");
	}

	// my_type& operator=(my_type&& rhs);
	{
		StringType str1(LITERAL("abcdefghijklmnopqrstuvwxyz"));
		StringType str2 = std::move(str1);

		PRINT_COND_ASSERT(str1 != LITERAL("abcdefghijklmnopqrstuvwxyz"), "StringBase - my_type& operator=(my_type&& rhs)");
		PRINT_COND_ASSERT(str2 == LITERAL("abcdefghijklmnopqrstuvwxyz"), "StringBase - my_type& operator=(my_type&& rhs)");

		PRINT_COND_ASSERT(str1.empty(), "StringBase - my_type& operator=(my_type&& rhs)");
		PRINT_COND_ASSERT(!str2.empty(), "StringBase - my_type& operator=(my_type&& rhs)");

		PRINT_COND_ASSERT(str1.length() == 0, "StringBase - my_type& operator=(my_type&& rhs)");
		PRINT_COND_ASSERT(str2.length() == 26, "StringBase - my_type& operator=(my_type&& rhs)");

		PRINT_COND_ASSERT(str1.size() == 0, "StringBase - my_type& operator=(my_type&& rhs)");
		PRINT_COND_ASSERT(str2.size() == 26, "StringBase - my_type& operator=(my_type&& rhs)");

		PRINT_COND_ASSERT(str1.validate(), "StringBase - my_type& operator=(my_type&& rhs)");
		PRINT_COND_ASSERT(str2.validate(), "StringBase - my_type& operator=(my_type&& rhs)");
	}
	{
		StringType str1(LITERAL("a"));
		StringType str2 = std::move(str1);

		PRINT_COND_ASSERT(str1 != LITERAL("a"), "StringBase - my_type& operator=(my_type&& rhs)");
		PRINT_COND_ASSERT(str2 == LITERAL("a"), "StringBase - my_type& operator=(my_type&& rhs)");

		PRINT_COND_ASSERT(str1.empty(), "StringBase - my_type& operator=(my_type&& rhs)");
		PRINT_COND_ASSERT(!str2.empty(), "StringBase - my_type& operator=(my_type&& rhs)");

		PRINT_COND_ASSERT(str1.length() == 0, "StringBase - my_type& operator=(my_type&& rhs)");
		PRINT_COND_ASSERT(str2.length() == 1, "StringBase - my_type& operator=(my_type&& rhs)");

		PRINT_COND_ASSERT(str1.size() == 0, "StringBase - my_type& operator=(my_type&& rhs)");
		PRINT_COND_ASSERT(str2.size() == 1, "StringBase - my_type& operator=(my_type&& rhs)");

		PRINT_COND_ASSERT(str1.validate(), "StringBase - my_type& operator=(my_type&& rhs)");
		PRINT_COND_ASSERT(str2.validate(), "StringBase - my_type& operator=(my_type&& rhs)");
	}

	// void swap(my_type& str);
	{
		StringType str1(LITERAL("abcdefghijklmnopqrstuvwxyz"));
		StringType str2;

		str1.swap(str2);

		PRINT_COND_ASSERT(str1 != LITERAL("abcdefghijklmnopqrstuvwxyz"), "StringBase - void swap(my_type& str)");
		PRINT_COND_ASSERT(str2 == LITERAL("abcdefghijklmnopqrstuvwxyz"), "StringBase - void swap(my_type& str)");

		PRINT_COND_ASSERT(str1.empty(), "StringBase - void swap(my_type& str)");
		PRINT_COND_ASSERT(!str2.empty(), "StringBase - void swap(my_type& str)");

		PRINT_COND_ASSERT(str1.length() == 0, "StringBase - void swap(my_type& str)");
		PRINT_COND_ASSERT(str2.length() == 26, "StringBase - void swap(my_type& str)");
		PRINT_COND_ASSERT(str1.size() == 0, "StringBase - void swap(my_type& str)");
		PRINT_COND_ASSERT(str2.size() == 26, "StringBase - void swap(my_type& str)");

		PRINT_COND_ASSERT(str1.validate(), "StringBase - void swap(my_type& str)");
		PRINT_COND_ASSERT(str2.validate(), "StringBase - void swap(my_type& str)");
	}

	// my_type& assign(const my_type& rhs);
	{
		StringType str1(LITERAL("abcdefghijklmnopqrstuvwxyz"));
		StringType str2;

		str2.assign(str1);

		PRINT_COND_ASSERT(str1 == LITERAL("abcdefghijklmnopqrstuvwxyz"), "StringBase - my_type& assign(const my_type& rhs)");
		PRINT_COND_ASSERT(str2 == LITERAL("abcdefghijklmnopqrstuvwxyz"), "StringBase - my_type& assign(const my_type& rhs)");

		PRINT_COND_ASSERT(!str1.empty(), "StringBase - my_type& assign(const my_type& rhs)");
		PRINT_COND_ASSERT(!str2.empty(), "StringBase - my_type& assign(const my_type& rhs)");

		PRINT_COND_ASSERT(str1.length() == 26, "StringBase - my_type& assign(const my_type& rhs)");
		PRINT_COND_ASSERT(str2.length() == 26, "StringBase - my_type& assign(const my_type& rhs)");
		PRINT_COND_ASSERT(str1.size() == 26, "StringBase - my_type& assign(const my_type& rhs)");
		PRINT_COND_ASSERT(str2.size() == 26, "StringBase - my_type& assign(const my_type& rhs)");

		PRINT_COND_ASSERT(str1.validate(), "StringBase - my_type& assign(const my_type& rhs)");
		PRINT_COND_ASSERT(str2.validate(), "StringBase - my_type& assign(const my_type& rhs)");
	}

	// my_type& assign(const my_type& rhs, size_type nPos, size_type nCount = my_type::npos);
	{
		StringType str1(LITERAL("abcdefghijklmnopqrstuvwxyz"));
		StringType str2(LITERAL("123456789"));

		str1.assign(str2, 3, 3);

		PRINT_COND_ASSERT(str1 == LITERAL("456"), "StringBase - my_type& assign(const my_type& rhs, size_type nPos, size_type nCount = my_type::npos)");
		PRINT_COND_ASSERT(str1.validate(), "StringBase - my_type& assign(const my_type& rhs, size_type nPos, size_type nCount = my_type::npos)");
		PRINT_COND_ASSERT(str2.validate(), "StringBase - my_type& assign(const my_type& rhs, size_type nPos, size_type nCount = my_type::npos)");
	}

	// my_type& assign(const_pointer pStr, size_type nCount);
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));
		str.assign(LITERAL("123456789"), 5);

		PRINT_COND_ASSERT(str == LITERAL("12345"), "StringBase - my_type& assign(const_pointer pStr, size_type nCount)");
		PRINT_COND_ASSERT(str.validate(), "StringBase - my_type& assign(const_pointer pStr, size_type nCount)");
	}

	// my_type& assign(const_pointer pStr);
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));
		str.assign(LITERAL("123"));

		PRINT_COND_ASSERT(str == LITERAL("123"), "StringBase - my_type& assign(const_pointer pStr)");
		PRINT_COND_ASSERT(str.validate(), "StringBase - my_type& assign(const_pointer pStr)");
	}

	// my_type& assign(size_type nCount, const value_type ch);
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));
		str.assign(32, LITERAL('c'));

		PRINT_COND_ASSERT(str == LITERAL("cccccccccccccccccccccccccccccccc"), "StringBase - my_type& assign(size_type nCount, const value_type ch)");
		PRINT_COND_ASSERT(str.validate(), "StringBase - my_type& assign(size_type nCount, const value_type ch)");
	}

	// my_type& assign(InputItr first, InputItr last);
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		auto* pLiteral = LITERAL("0123456789");
		auto* pBegin = pLiteral + 4;
		auto* pEnd = pLiteral + 7;

		str.assign(pBegin, pEnd);

		PRINT_COND_ASSERT(str == LITERAL("456"), "StringBase - my_type& assign(InputItr first, InputItr last)");
		PRINT_COND_ASSERT(str.validate(), "StringBase - my_type& assign(InputItr first, InputItr last)");
	}

	// my_type& assign(my_type&& rhs);
	{
		StringType str1;
		StringType str2(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		str1.assign(std::move(str2));
		str2.clear();

		PRINT_COND_ASSERT(str1 == LITERAL("abcdefghijklmnopqrstuvwxyz"), "StringBase - my_type& assign(my_type&& rhs)");
		PRINT_COND_ASSERT(str2 != LITERAL("abcdefghijklmnopqrstuvwxyz"), "StringBase - my_type& assign(my_type&& rhs)");

		PRINT_COND_ASSERT(!str1.empty(), "StringBase - my_type& assign(my_type&& rhs)");
		PRINT_COND_ASSERT(str2.empty(), "StringBase - my_type& assign(my_type&& rhs)");

		PRINT_COND_ASSERT(str1.length() == 26, "StringBase - my_type& assign(my_type&& rhs)");
		PRINT_COND_ASSERT(str2.length() == 0, "StringBase - my_type& assign(my_type&& rhs)");
		PRINT_COND_ASSERT(str1.size() == 26, "StringBase - my_type& assign(my_type&& rhs)");
		PRINT_COND_ASSERT(str2.size() == 0, "StringBase - my_type& assign(my_type&& rhs)");

		PRINT_COND_ASSERT(str1.validate(), "StringBase - my_type& assign(my_type&& rhs)");
		PRINT_COND_ASSERT(str2.validate(), "StringBase - my_type& assign(my_type&& rhs)");
	}

	// my_type& assign(std::initializer_list< value_type > iList);
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));
		str.assign({ '1','2','3' });

		PRINT_COND_ASSERT(str == LITERAL("123"), "StringBase - my_type& assign(std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT(str.validate(), "StringBase - my_type& assign(std::initializer_list< value_type > iList)");
	}

	// iterator begin(void) NOEXCEPT;
	// const_iterator begin(void) const NOEXCEPT;
	// const_iterator cbegin(void) const NOEXCEPT;
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		auto iBegin = str.begin();

		PRINT_COND_ASSERT(*iBegin++ == LITERAL('a'), "StringBase - const_iterator begin(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*iBegin++ == LITERAL('b'), "StringBase - const_iterator begin(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*iBegin++ == LITERAL('c'), "StringBase - const_iterator begin(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*iBegin++ == LITERAL('d'), "StringBase - const_iterator begin(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*iBegin++ == LITERAL('e'), "StringBase - const_iterator begin(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*iBegin++ == LITERAL('f'), "StringBase - const_iterator begin(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*(str.begin() + 25) == LITERAL('z'), "StringBase - const_iterator begin(void) const NOEXCEPT");

	}

	// iterator end(void) NOEXCEPT;
	// const_iterator end(void) const NOEXCEPT;
	// const_iterator cend(void) const NOEXCEPT;
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		auto iEnd = str.end() - 1;

		PRINT_COND_ASSERT(*iEnd-- == LITERAL('z'), "StringBase - const_iterator end(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*iEnd-- == LITERAL('y'), "StringBase - const_iterator end(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*iEnd-- == LITERAL('x'), "StringBase - const_iterator end(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*iEnd-- == LITERAL('w'), "StringBase - const_iterator end(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*iEnd-- == LITERAL('v'), "StringBase - const_iterator end(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*iEnd-- == LITERAL('u'), "StringBase - const_iterator end(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*(str.end() - 26) == LITERAL('a'), "StringBase - const_iterator end(void) const NOEXCEPT");
	}

	// reverse_iterator rbegin(void) NOEXCEPT;
	// const_reverse_iterator rbegin(void) const NOEXCEPT;
	// const_reverse_iterator crbegin(void) const NOEXCEPT;
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		auto iRBegin = str.rbegin();

		PRINT_COND_ASSERT(*iRBegin++ == LITERAL('z'), "StringBase - const_reverse_iterator rbegin(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*iRBegin++ == LITERAL('y'), "StringBase - const_reverse_iterator rbegin(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*iRBegin++ == LITERAL('x'), "StringBase - const_reverse_iterator rbegin(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*iRBegin++ == LITERAL('w'), "StringBase - const_reverse_iterator rbegin(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*iRBegin++ == LITERAL('v'), "StringBase - const_reverse_iterator rbegin(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*iRBegin++ == LITERAL('u'), "StringBase - const_reverse_iterator rbegin(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*(str.rbegin() + 25) == LITERAL('a'), "StringBase - const_reverse_iterator rbegin(void) const NOEXCEPT");
	}

	// reverse_iterator rend(void) NOEXCEPT;
	// const_reverse_iterator rend(void) const NOEXCEPT;
	// const_reverse_iterator crend(void) const NOEXCEPT;
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		auto iREnd = str.rend() - 1;

		PRINT_COND_ASSERT(*iREnd-- == LITERAL('a'), "StringBase - const_reverse_iterator rend(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*iREnd-- == LITERAL('b'), "StringBase - const_reverse_iterator rend(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*iREnd-- == LITERAL('c'), "StringBase - const_reverse_iterator rend(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*iREnd-- == LITERAL('d'), "StringBase - const_reverse_iterator rend(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*iREnd-- == LITERAL('e'), "StringBase - const_reverse_iterator rend(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*iREnd-- == LITERAL('f'), "StringBase - const_reverse_iterator rend(void) const NOEXCEPT");
		PRINT_COND_ASSERT(*(str.rend() - 26) == LITERAL('z'), "StringBase - const_reverse_iterator rend(void) const NOEXCEPT");
	}

	// bool empty(void) const NOEXCEPT;
	// size_type size(void) const NOEXCEPT;
	// size_type length(void) const NOEXCEPT;
	// size_type capacity(void) const NOEXCEPT;
	// void resize(size_type nNewsize, const value_type ch);
	// void resize(size_type nNewsize);
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));
		PRINT_COND_ASSERT(!str.empty(), "StringBase - ");
		PRINT_COND_ASSERT(str.size() == 26, "StringBase - ");
		PRINT_COND_ASSERT(str.length() == 26, "StringBase - ");
		PRINT_COND_ASSERT(str.capacity() >= 26, "StringBase - ");

		str.assign(LITERAL(""));
		PRINT_COND_ASSERT(str.empty(), "StringBase - my_type& assign(const_pointer pStr)");
		PRINT_COND_ASSERT(str.size() == 0, "StringBase - my_type& assign(const_pointer pStr)");
		PRINT_COND_ASSERT(str.length() == 0, "StringBase - my_type& assign(const_pointer pStr)");
		PRINT_COND_ASSERT(str.capacity() >= 26, "StringBase - my_type& assign(const_pointer pStr)");

		str.resize(0);
		PRINT_COND_ASSERT(str.empty(), "StringBase - void resize(size_type nNewsize)");
		PRINT_COND_ASSERT(str.size() == 0, "StringBase - void resize(size_type nNewsize)");
		PRINT_COND_ASSERT(str.length() == 0, "StringBase - void resize(size_type nNewsize)");
		PRINT_COND_ASSERT(str.capacity() >= 26, "StringBase - void resize(size_type nNewsize)");

		str.resize(32, LITERAL('c'));
		PRINT_COND_ASSERT(!str.empty(), "StringBase - void resize(size_type nNewsize, const value_type ch)");
		PRINT_COND_ASSERT(str.size() == 32, "StringBase - void resize(size_type nNewsize, const value_type ch)");
		PRINT_COND_ASSERT(str.length() == 32, "StringBase - void resize(size_type nNewsize, const value_type ch)");
		PRINT_COND_ASSERT(str.capacity() >= 32, "StringBase - void resize(size_type nNewsize, const value_type ch)");
		PRINT_COND_ASSERT(str == LITERAL("cccccccccccccccccccccccccccccccc"), "StringBase - void resize(size_type nNewsize, const value_type ch)");
	}

	// void reserve(size_type nNewcap = 0);
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));
		PRINT_COND_ASSERT(!str.empty(), "StringBase - ");
		PRINT_COND_ASSERT(str.size() == 26, "StringBase - ");
		PRINT_COND_ASSERT(str.length() == 26, "StringBase - ");
		PRINT_COND_ASSERT(str.capacity() >= 26, "StringBase - ");

		// verifies that we allocate memory
		str.reserve(64);
		PRINT_COND_ASSERT(!str.empty(), "StringBase - void reserve(size_type nNewcap = 0)");
		PRINT_COND_ASSERT(str.size() == 26, "StringBase - void reserve(size_type nNewcap = 0)");
		PRINT_COND_ASSERT(str.length() == 26, "StringBase - void reserve(size_type nNewcap = 0)");
		PRINT_COND_ASSERT(str.capacity() >= 64, "StringBase - void reserve(size_type nNewcap = 0)");

		// verifies that we do not free memory
		str.reserve(32);
		PRINT_COND_ASSERT(!str.empty(), "StringBase - void reserve(size_type nNewcap = 0)");
		PRINT_COND_ASSERT(str.size() == 26, "StringBase - void reserve(size_type nNewcap = 0)");
		PRINT_COND_ASSERT(str.length() == 26, "StringBase - void reserve(size_type nNewcap = 0)");
		PRINT_COND_ASSERT(str.capacity() >= 64, "StringBase - void reserve(size_type nNewcap = 0)");
	}

	// const value_type* data(void) const NOEXCEPT;
	// const value_type* c_str(void) const NOEXCEPT;
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		auto* pData = str.data();
		auto* pCStr = str.c_str();

		PRINT_COND_ASSERT(pData != nullptr, "StringBase - const value_type* data(void) const NOEXCEPT");
		PRINT_COND_ASSERT(pCStr != nullptr, "StringBase - const value_type* data(void) const NOEXCEPT");
		PRINT_COND_ASSERT(pData == pCStr, "StringBase - const value_type* data(void) const NOEXCEPT");
		PRINT_COND_ASSERT(FXD::Memory::MemCompare(pData, pCStr, str.size()) == 0, "StringBase - const value_type* data(void) const NOEXCEPT");
	}

	// reference operator[](size_type nOffSet);
	// const_reference operator[](size_type nOffSet) const;
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		PRINT_COND_ASSERT(str[0] == LITERAL('a'), "StringBase - const_reference operator[](size_type nOffSet)");
		PRINT_COND_ASSERT(str[14] == LITERAL('o'), "StringBase - const_reference operator[](size_type nOffSet)");
		PRINT_COND_ASSERT(str[25] == LITERAL('z'), "StringBase - const_reference operator[](size_type nOffSet)");
	}

	// reference at(size_type nOffSet);
	// const_reference at(size_type nOffSet) const;
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		PRINT_COND_ASSERT(str.at(0) == LITERAL('a'), "StringBase - const_reference at(size_type nOffSet)");
		PRINT_COND_ASSERT(str.at(14) == LITERAL('o'), "StringBase - const_reference at(size_type nOffSet)");
		PRINT_COND_ASSERT(str.at(25) == LITERAL('z'), "StringBase - const_reference at(size_type nOffSet)");
	}

	// reference front(void);
	// const_reference front(void) const;
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));
		PRINT_COND_ASSERT(str.front() == LITERAL('a'), "StringBase - const_reference front(void) const");
	}

	// reference back(void);
	// const_reference back(void) const;
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));
		PRINT_COND_ASSERT(str.back() == LITERAL('z'), "StringBase - const_reference back(void) const");
	}

	// my_type& operator+=(const my_type& rhs);
	// my_type& operator+=(const_pointer pStr);
	// my_type& operator+=(value_type ch);
	{
		StringType str1(LITERAL("abcdefghijklmnopqrstuvwxyz"));
		StringType str2(LITERAL("123"));
		str1 += str2;
		PRINT_COND_ASSERT(str1 == LITERAL("abcdefghijklmnopqrstuvwxyz123"), "StringBase - my_type& operator+=(const my_type& rhs)");

		str1 += LITERAL("456");
		PRINT_COND_ASSERT(str1 == LITERAL("abcdefghijklmnopqrstuvwxyz123456"), "StringBase - my_type& operator+=(const_pointer pStr)");

		str1 += LITERAL('7');
		PRINT_COND_ASSERT(str1 == LITERAL("abcdefghijklmnopqrstuvwxyz1234567"), "StringBase - my_type& operator+=(value_type ch)");
	}

	// my_type& append(const my_type& str);
	// my_type& append(const my_type& str, size_type nPos, size_type nCount = my_type::npos);
	// my_type& append(const_pointer pStr, size_type nCount);
	// my_type& append(const_pointer pStr);
	// my_type& append(size_type nCount, const value_type ch);
	// my_type& append(InputItr first, InputItr last);
	{
		const StringType src(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		StringType str;
		str.append(StringType(LITERAL("abcd")));
		PRINT_COND_ASSERT(str == LITERAL("abcd"), "StringBase - my_type& append(const my_type& str)");

		str.append(src, 4, 4);
		PRINT_COND_ASSERT(str == LITERAL("abcdefgh"), "StringBase - my_type& append(const my_type& str, size_type nPos, size_type nCount)");

		str.append(src.data() + 8, 4);
		PRINT_COND_ASSERT(str == LITERAL("abcdefghijkl"), "StringBase - my_type& append(const_pointer pStr, size_type nCount)");

		str.append(LITERAL("mnop"));
		PRINT_COND_ASSERT(str == LITERAL("abcdefghijklmnop"), "StringBase - my_type& append(const_pointer pStr)");

		str.append(1, LITERAL('q'));
		PRINT_COND_ASSERT(str == LITERAL("abcdefghijklmnopq"), "StringBase - my_type& append(size_type nCount, const value_type ch)");

		str.append(src.data() + 17, src.data() + 26);   // "abcdefghijklmnopqrstuvwxyz"
		PRINT_COND_ASSERT(str == src, "StringBase - my_type& append(InputItr first, InputItr last)");
	}

	// void push_back(value_type ch);
	{
		StringType str;
		const StringType src(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		FXD::STD::for_each(FXD::STD::begin(src), FXD::STD::end(src), [&str](const typename StringType::value_type& c)
		{
			str.push_back(c);
		});

		PRINT_COND_ASSERT(str == src, "StringBase - void push_back(value_type ch)");
		PRINT_COND_ASSERT(str.validate(), "StringBase - void push_back(value_type ch)");
	}

	// void pop_back(void);
	{
		StringType str(LITERAL("123456789"));
		PRINT_COND_ASSERT(str == LITERAL("123456789"), "");

		str.pop_back(); PRINT_COND_ASSERT(str == LITERAL("12345678"), "StringBase - void pop_back(void)");
		str.pop_back(); PRINT_COND_ASSERT(str == LITERAL("1234567"), "StringBase - void pop_back(void)");
		str.pop_back(); PRINT_COND_ASSERT(str == LITERAL("123456"), "StringBase - void pop_back(void)");
		str.pop_back(); PRINT_COND_ASSERT(str == LITERAL("12345"), "StringBase - void pop_back(void)");
		str.pop_back(); PRINT_COND_ASSERT(str == LITERAL("1234"), "StringBase - void pop_back(void)");
		str.pop_back(); PRINT_COND_ASSERT(str == LITERAL("123"), "StringBase - void pop_back(void)");
		str.pop_back(); PRINT_COND_ASSERT(str == LITERAL("12"), "StringBase - void pop_back(void)");
		str.pop_back(); PRINT_COND_ASSERT(str == LITERAL("1"), "StringBase - void pop_back(void)");
		str.pop_back(); PRINT_COND_ASSERT(str == LITERAL(""), "StringBase - void pop_back(void)");

		PRINT_COND_ASSERT(str.validate(), "StringBase - void pop_back(void)");
	}

	// my_type& insert(size_type nPos, const_pointer pStr);
	// my_type& insert(size_type nPos, const my_type& str, size_type nPos2, size_type nCount = my_type::npos);
	// my_type& insert(size_type nPos, const_pointer pStr, size_type nCount);
	// my_type& insert(size_type nPos, const my_type& str);
	// my_type& insert(size_type nPos, const_pointer pStr, size_type nCount);
	// iterator insert(const_iterator pos, size_type nCount, const value_type ch);
	// iterator insert(const_iterator pos, InputItr first, InputItr last);
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		str.insert((typename StringType::size_type)0, (typename StringType::size_type)1, LITERAL('1'));
		PRINT_COND_ASSERT(str == LITERAL("1abcdefghijklmnopqrstuvwxyz"), "StringBase - my_type& insert(size_type nPos, size_type nCount, const value_type ch)");

		str.insert(2, LITERAL("234"));
		PRINT_COND_ASSERT(str == LITERAL("1a234bcdefghijklmnopqrstuvwxyz"), "StringBase - my_type& insert(size_type nPos, const_pointer pStr)");

		str.insert(15, StringType(LITERAL("567")));
		PRINT_COND_ASSERT(str == LITERAL("1a234bcdefghijk567lmnopqrstuvwxyz"), "StringBase - my_type& insert(size_type nPos, const my_type& str)");

		str.insert(30, StringType(LITERAL(" is an example of a substring")), 1, 14);
		PRINT_COND_ASSERT(str == LITERAL("1a234bcdefghijk567lmnopqrstuvwis an example xyz"), "StringBase - my_type& insert(size_type nPos, const my_type& str, size_type nPos2, size_type nCount)");

		{
			StringType strSSO;
			auto nSSOCap = strSSO.capacity();
			StringType strCheck;
			strCheck.append(nSSOCap, LITERAL('a'));
			strSSO.append(nSSOCap - 1, LITERAL('a'));

			strSSO.insert(strSSO.size() - 1, LITERAL("a"));
			PRINT_COND_ASSERT(strSSO.validate(), "StringBase - my_type& insert(size_type nPos, const_pointer pStr)");
			PRINT_COND_ASSERT(strSSO == strCheck, "StringBase - my_type& insert(size_type nPos, const_pointer pStr)");
		}
		{
			StringType strSSO;
			auto nSSOCap = strSSO.capacity();

			// 32 bit platform with char32_t can only hold 2 characters in SSO
			if (nSSOCap - 2 > 0)
			{
				StringType strCheck;
				strCheck.append(nSSOCap, LITERAL('a'));
				strSSO.append(nSSOCap - 2, LITERAL('a'));

				strSSO.insert(strSSO.size() - 1, LITERAL("aa"));
				PRINT_COND_ASSERT(strSSO.validate(), "StringBase - void insert(size_type nPos, const_pointer pStr)");
				PRINT_COND_ASSERT(strSSO == strCheck, "StringBase - void insert(size_type nPos, const_pointer pStr)");
			}
		}
	}

	// iterator insert(const_iterator pos, std::initializer_list< value_type > iList);
	{
		StringType str;
		str.insert(str.begin(), { 'a','b','c' });
		str.insert(str.end(), { 'd','e','f' });
		str.insert(str.begin() + 3, { '1','2','3' });

		PRINT_COND_ASSERT(str == LITERAL("abc123def"), "StringBase - iterator insert(const_iterator pos, std::initializer_list< value_type > iList)");
		PRINT_COND_ASSERT(str.validate(), "StringBase - iterator insert(const_iterator pos, std::initializer_list< value_type > iList)");
	}

	// iterator insert(const_iterator pos, const value_type ch)
	{
		StringType str = LITERAL("aaa");
		auto itr = str.insert(str.end(), 'b');
		PRINT_COND_ASSERT(*itr == LITERAL('b'), "StringBase - iterator insert(const_iterator pos, const value_type ch)");
		PRINT_COND_ASSERT(str == LITERAL("aaab"), "StringBase - iterator insert(const_iterator pos, const value_type ch)");

		itr = str.insert(str.begin(), 'c');
		PRINT_COND_ASSERT(*itr == LITERAL('c'), "StringBase - iterator insert(const_iterator pos, const value_type ch)");
		PRINT_COND_ASSERT(str == LITERAL("caaab"), "StringBase - iterator insert(const_iterator pos, const value_type ch)");

		itr = str.insert(str.begin() + 2, 'd');
		PRINT_COND_ASSERT(*itr == LITERAL('d'), "StringBase - iterator insert(const_iterator pos, const value_type ch)");
		PRINT_COND_ASSERT(str == LITERAL("cadaab"), "StringBase - iterator insert(const_iterator pos, const value_type ch)");
	}

	// my_type& erase(size_type nPos = 0, size_type nCount = my_type::npos);
	// iterator erase(const_iterator pos);
	// iterator erase(const_iterator first, const_iterator last);
	// reverse_iterator erase(reverse_iterator pos);
	// reverse_iterator erase(reverse_iterator first, reverse_iterator last);
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		str.erase(0, 5);
		PRINT_COND_ASSERT(str == LITERAL("fghijklmnopqrstuvwxyz"), "StringBase - my_type& erase(size_type nPos = 0, size_type nCount = my_type::npos)");

		str.erase(5, 10);
		PRINT_COND_ASSERT(str == LITERAL("fghijuvwxyz"), "StringBase - my_type& erase(size_type nPos = 0, size_type nCount = my_type::npos)");

		str.erase(str.find(LITERAL('v')));
		PRINT_COND_ASSERT(str == LITERAL("fghiju"), "StringBase - iterator erase(const_iterator pos)");

		str.erase(str.find(LITERAL('g')), str.find(LITERAL('i')));
		PRINT_COND_ASSERT(str == LITERAL("fju"), "StringBase - iterator erase(const_iterator first, const_iterator last)");

		typename StringType::const_iterator itr = str.begin() + 1; // 'j'
		str.erase(itr);
		PRINT_COND_ASSERT(str == LITERAL("fu"), "StringBase - iterator erase(const_iterator pos)");

	}

	// void clear(void) NOEXCEPT;
	{
		StringType str(LITERAL("123456789"));
		PRINT_COND_ASSERT(str == LITERAL("123456789"), "");

		str.clear();
		PRINT_COND_ASSERT(str == LITERAL(""), "StringBase - void clear(void) NOEXCEPT");
		PRINT_COND_ASSERT(str.empty(), "StringBase - void clear(void) NOEXCEPT");
		PRINT_COND_ASSERT(str.validate(), "StringBase - void clear(void) NOEXCEPT");
	}

	// my_type& replace(size_type nPos, size_type nCount, const my_type& str);
	// my_type& replace(size_type nPos, size_type nCount, const my_type& str, size_type nPos2, size_type nCount2 = my_type::npos);
	// my_type& replace(size_type nPos, size_type nCount, const pointer pStr, size_type nCount2);
	// my_type& replace(size_type nPos, size_type nCount, const pointer pStr);
	// my_type& replace(size_type nPos, size_type nCount, size_type nCount2, const value_type ch);
	// my_type& replace(const_iterator first, const_iterator last, const my_type& str);
	// my_type& replace(const_iterator first, const_iterator last, const const_pointer pStr, size_type nCount);
	// my_type& replace(const_iterator first, const_iterator last, const const_pointer pStr);
	// my_type& replace(const_iterator first, const_iterator last, size_type nCount2, const value_type ch);
	// my_type& replace(const_iterator first, const_iterator last, InputItr first2, InputItr last2);
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		str.replace(5, 10, StringType(LITERAL("123")));
		PRINT_COND_ASSERT(str == LITERAL("abcde123pqrstuvwxyz"), "StringBase - my_type& replace(size_type nPos, size_type nCount, const my_type& str)");

		str.replace(13, 1, StringType(LITERAL("0123456789")), 4, 6);
		PRINT_COND_ASSERT(str == LITERAL("abcde123pqrst456789vwxyz"), "StringBase - my_type& replace(size_type nPos, size_type nCount, const my_type& str, size_type nPos2, size_type nCount2)");

		str.replace(24, 1, LITERAL("0123456789"));
		PRINT_COND_ASSERT(str == LITERAL("abcde123pqrst456789vwxyz0123456789"), "StringBase - my_type& replace(size_type nPos, size_type nCount, const_pointer pStr)");

		str.replace(16, 4, 4, LITERAL('@'));
		PRINT_COND_ASSERT(str == LITERAL("abcde123pqrst456@@@@wxyz0123456789"), "StringBase - my_type& replace(size_type nPos, size_type nCount, size_type nCount2, const value_type ch)");
	}

	// size_type copy(value_type* p, size_type n, size_type position = 0) const;
	{
		typename StringType::value_type buf[64];

		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));
		str.copy(buf, 10, 10);

		PRINT_COND_ASSERT(FXD::Memory::MemCompare(buf, LITERAL("klmnopqrst"), 10) == 0, "StringBase - size_type copy(value_type* p, size_type n, size_type position = 0) const");
	}

	// size_type find(const my_type& str, size_type nPos = 0) const NOEXCEPT;
	// size_type find(const_pointer pStr, size_type nPos = 0) const;
	// size_type find(const_pointer pStr, size_type nPos, size_type nCount) const;
	// size_type find(const value_type ch, size_type nPos = 0) const NOEXCEPT;
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		PRINT_COND_ASSERT(str.find(StringType(LITERAL("d"))) != StringType::npos, "StringBase - size_type find(const my_type& str, size_type nPos = 0) const NOEXCEPT");
		PRINT_COND_ASSERT(str.find(StringType(LITERAL("tuv"))) != StringType::npos, "StringBase - size_type find(const my_type& str, size_type nPos = 0) const NOEXCEPT");
		PRINT_COND_ASSERT(str.find(StringType(LITERAL("123r"))) == StringType::npos, "StringBase - size_type find(const my_type& str, size_type nPos = 0) const NOEXCEPT; ");

		PRINT_COND_ASSERT(str.find(LITERAL("d")) != StringType::npos, "StringBase - size_type find(const_pointer pStr, size_type nPos = 0) const");
		PRINT_COND_ASSERT(str.find(LITERAL("tuv")) != StringType::npos, "StringBase - size_type find(const_pointer pStr, size_type nPos = 0) const");
		PRINT_COND_ASSERT(str.find(LITERAL("123r")) == StringType::npos, "StringBase - size_type find(const_pointer pStr, size_type nPos = 0) const");

		PRINT_COND_ASSERT(str.find(LITERAL("d"), 0) != StringType::npos, "StringBase - size_type find(const_pointer pStr, size_type nPos, size_type nCount) const");
		PRINT_COND_ASSERT(str.find(LITERAL("tuv"), 2) != StringType::npos, "StringBase - size_type find(const_pointer pStr, size_type nPos, size_type nCount) const");
		PRINT_COND_ASSERT(str.find(LITERAL("123r"), 2) == StringType::npos, "StringBase - size_type find(const_pointer pStr, size_type nPos, size_type nCount) const");

		PRINT_COND_ASSERT(str.find(LITERAL('d'), 0) != StringType::npos, "StringBase - size_type find(const value_type ch, size_type nPos = 0) const NOEXCEPT");
		PRINT_COND_ASSERT(str.find(LITERAL('t'), 2) != StringType::npos, "StringBase - size_type find(const value_type ch, size_type nPos = 0) const NOEXCEPT");
		PRINT_COND_ASSERT(str.find(LITERAL('1'), 2) == StringType::npos, "StringBase - size_type find(const value_type ch, size_type nPos = 0) const NOEXCEPT");
	}

	// size_type rfind(const my_type& str, size_type nPos = my_type::npos) const NOEXCEPT;
	// size_type rfind(const_pointer pStr, size_type nPos = my_type::npos) const;
	// size_type rfind(const_pointer pStr, size_type nPos, size_type nCount) const;
	// size_type rfind(const value_type ch, size_type nPos = my_type::npos) const NOEXCEPT;
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		PRINT_COND_ASSERT(str.rfind(StringType(LITERAL("d"))) != StringType::npos, "StringBase - size_type rfind(const my_type& str, size_type nPos = my_type::npos) const NOEXCEPT");
		PRINT_COND_ASSERT(str.rfind(StringType(LITERAL("tuv"))) != StringType::npos, "StringBase - size_type rfind(const my_type& str, size_type nPos = my_type::npos) const NOEXCEPT");
		PRINT_COND_ASSERT(str.rfind(StringType(LITERAL("123r"))) == StringType::npos, "StringBase - size_type rfind(const my_type& str, size_type nPos = my_type::npos) const NOEXCEPT");

		PRINT_COND_ASSERT(str.rfind(LITERAL("d")) != StringType::npos, "StringBase - size_type rfind(const_pointer pStr, size_type nPos = my_type::npos) const");
		PRINT_COND_ASSERT(str.rfind(LITERAL("tuv")) != StringType::npos, "StringBase - size_type rfind(const_pointer pStr, size_type nPos = my_type::npos) const");
		PRINT_COND_ASSERT(str.rfind(LITERAL("123r")) == StringType::npos, "StringBase - size_type rfind(const_pointer pStr, size_type nPos = my_type::npos) const");

		PRINT_COND_ASSERT(str.rfind(LITERAL("d"), 20) != StringType::npos, "StringBase - size_type rfind(const_pointer pStr, size_type nPos, size_type nCount) const");
		PRINT_COND_ASSERT(str.rfind(LITERAL("tuv"), 20) != StringType::npos, "StringBase - size_type rfind(const_pointer pStr, size_type nPos, size_type nCount) const");
		PRINT_COND_ASSERT(str.rfind(LITERAL("123r"), 20) == StringType::npos, "StringBase - size_type rfind(const_pointer pStr, size_type nPos, size_type nCount) const");

		PRINT_COND_ASSERT(str.rfind(LITERAL('d'), 20) != StringType::npos, "StringBase - size_type rfind(const value_type ch, size_type nPos = my_type::npos) const NOEXCEPT");
		PRINT_COND_ASSERT(str.rfind(LITERAL('t'), 20) != StringType::npos, "StringBase - size_type rfind(const value_type ch, size_type nPos = my_type::npos) const NOEXCEPT");
		PRINT_COND_ASSERT(str.rfind(LITERAL('1'), 20) == StringType::npos, "StringBase - size_type rfind(const value_type ch, size_type nPos = my_type::npos) const NOEXCEPT");
	}

	// size_type find_first_of(const my_type& str, size_type nPos = 0) const NOEXCEPT;
	// size_type find_first_of(const_pointer pStr, size_type nPos = 0) const;
	// size_type find_first_of(const_pointer pStr, size_type nPos, size_type nCount) const;
	// size_type find_first_of(const value_type ch, size_type nPos = 0) const NOEXCEPT;
	{
		StringType str(LITERAL("aaaaabbbbbcccdddddeeeeefffggh"));

		PRINT_COND_ASSERT(str.find_first_of(StringType(LITERAL("aaa"))) == 0, "StringBase - size_type find_first_of(const my_type& str, size_type nPos = 0) const NOEXCEPT");
		PRINT_COND_ASSERT(str.find_first_of(LITERAL("aab")) == 0, "StringBase - size_type find_first_of(const_pointer pStr, size_type nPos = 0) const");
		PRINT_COND_ASSERT(str.find_first_of(LITERAL("baab")) == 0, "StringBase - size_type find_first_of(const_pointer pStr, size_type nPos = 0) const");
		PRINT_COND_ASSERT(str.find_first_of(LITERAL("ceg")) == 10, "StringBase - size_type find_first_of(const_pointer pStr, size_type nPos = 0) const");
		PRINT_COND_ASSERT(str.find_first_of(LITERAL("eeef"), 1, 2) == 18, "StringBase - size_type find_first_of(const_pointer pStr, size_type nPos, size_type nCount) const");
		PRINT_COND_ASSERT(str.find_first_of(LITERAL("eeef"), 1, 4) == 18, "StringBase - size_type find_first_of(const_pointer pStr, size_type nPos, size_type nCount) const");
		PRINT_COND_ASSERT(str.find_first_of(LITERAL('g')) == 26, "StringBase - size_type find_first_of(const value_type ch, size_type nPos = 0) const NOEXCEPT");
		PRINT_COND_ASSERT(str.find_first_of(LITERAL('$')) == StringType::npos, "StringBase - size_type find_first_of(const value_type ch, size_type nPos = 0) const NOEXCEPT");
	}

	// size_type find_last_of(const my_type& str, size_type nPos = my_type::npos) const NOEXCEPT;
	// size_type find_last_of(const_pointer pStr, size_type nPos = my_type::npos) const;
	// size_type find_last_of(const_pointer pStr, size_type nPos, size_type nCount) const;
	// size_type find_last_of(const value_type ch, size_type nPos = my_type::npos) const NOEXCEPT;
	{
		StringType str(LITERAL("aaaaabbbbbcccdddddeeeeefffggh"));

		PRINT_COND_ASSERT(str.find_last_of(StringType(LITERAL("aaa"))) == 4, "StringBase - size_type find_last_of(const my_type& str, size_type nPos = my_type::npos) const NOEXCEPT");
		PRINT_COND_ASSERT(str.find_last_of(LITERAL("aab")) == 9, "StringBase - size_type find_last_of(const_pointer pStr, size_type nPos = my_type::npos) const");
		PRINT_COND_ASSERT(str.find_last_of(LITERAL("baab")) == 9, "StringBase - size_type find_last_of(const_pointer pStr, size_type nPos = my_type::npos) const");
		PRINT_COND_ASSERT(str.find_last_of(LITERAL("ceg")) == 27, "StringBase - size_type find_last_of(const_pointer pStr, size_type nPos = my_type::npos) const");
		PRINT_COND_ASSERT(str.find_last_of(LITERAL('g')) == 27, "StringBase - size_type find_last_of(const value_type ch, size_type nPos = my_type::npos) const NOEXCEPT");
		PRINT_COND_ASSERT(str.find_last_of(LITERAL('$')) == StringType::npos, "StringBase - size_type find_last_of(const value_type ch, size_type nPos = my_type::npos) const NOEXCEPT");
	}

	// size_type find_first_not_of(const my_type& str, size_type nPos = 0) const NOEXCEPT;
	// size_type find_first_not_of(const_pointer pStr, size_type nPos = 0) const;
	// size_type find_first_not_of(const_pointer pStr, size_type nPos, size_type nCount) const;
	// size_type find_first_not_of(const value_type ch, size_type nPos = 0) const NOEXCEPT;
	{
		StringType str(LITERAL("aaaaabbbbbcccdddddeeeeefffggh"));

		PRINT_COND_ASSERT(str.find_first_not_of(StringType(LITERAL("abcdfg"))) == 18, "StringBase - size_type find_last_of(const my_type& str, size_type nPos = my_type::npos) const NOEXCEPT");
		PRINT_COND_ASSERT(str.find_first_not_of(LITERAL("abcdfg")) == 18, "StringBase - size_type find_first_not_of(const_pointer pStr, size_type nPos = 0) const");
		PRINT_COND_ASSERT(str.find_first_not_of(LITERAL('a')) == 5, "StringBase - size_type find_first_not_of(const value_type ch, size_type nPos = 0) const NOEXCEPT");
	}

	// size_type find_last_not_of(my_type str, size_type nOffSet = npos) const NOEXCEPT;
	// size_type find_last_not_of(const_pointer pStr, size_type nOffSet = npos) const;
	// size_type find_last_not_of(const_pointer pStr, size_type nOffSet, size_type nCount) const;
	// size_type find_last_not_of(charT ch, size_type nOffSet = npos) const NOEXCEPT;
	{
		StringType str(LITERAL("aaaaabbbbbcccdddddeeeeefffggh"));

		PRINT_COND_ASSERT(str.find_last_not_of(StringType(LITERAL("a"))) == 28, "StringBase - size_type find_last_not_of(my_type str, size_type nOffSet = npos) const NOEXCEPT");
		PRINT_COND_ASSERT(str.find_last_not_of(StringType(LITERAL("abcdfg"))) == 28, "StringBase - size_type find_last_not_of(my_type str, size_type nOffSet = npos) const NOEXCEPT");
		PRINT_COND_ASSERT(str.find_last_not_of(StringType(LITERAL("abcdfgh"))) == 22, "StringBase - size_type find_last_not_of(my_type str, size_type nOffSet = npos) const NOEXCEPT");
		PRINT_COND_ASSERT(str.find_last_not_of(LITERAL("abcdfgh")) == 22, "StringBase - size_type find_last_not_of(const_pointer pStr, size_type nOffSet = npos) const");
		PRINT_COND_ASSERT(str.find_last_not_of(LITERAL('a')) == 28, "StringBase - size_type find_last_not_of(charT ch, size_type nOffSet = npos) const NOEXCEPT");
	}

	// my_type substr(size_type nPos = 0, size_type nCount = my_type::npos) const;
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		auto substring = str.substr(0, 6);
		PRINT_COND_ASSERT(substring == LITERAL("abcdef"), "StringBase - my_type substr(size_type nPos = 0, size_type nCount = my_type::npos) const");

		substring = str.substr(0, 0);
		PRINT_COND_ASSERT(substring == LITERAL(""), "StringBase - my_type substr(size_type nPos = 0, size_type nCount = my_type::npos) const");

		substring = str.substr(16, 0);
		PRINT_COND_ASSERT(substring == LITERAL(""), "StringBase - my_type substr(size_type nPos = 0, size_type nCount = my_type::npos) const");

		substring = str.substr(16, 42);
		PRINT_COND_ASSERT(substring == LITERAL("qrstuvwxyz"), "StringBase - my_type substr(size_type nPos = 0, size_type nCount = my_type::npos) const");
	}

	// FXD::S32 compare(const my_type& str) const;
	// FXD::S32 compare(size_type nPos, size_type nCount, const my_type& str) const;
	// FXD::S32 compare(size_type nPos, size_type nCount, const my_type& str, size_type nPos2, size_type nCount2) const;
	// FXD::S32 compare(const_pointer pStr) const;
	// FXD::S32 compare(size_type nPos, size_type nCount, const_pointer pStr) const;
	// FXD::S32 compare(size_type nPos, size_type nCount, const_pointer pStr, size_type nCount2) const;
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		PRINT_COND_ASSERT(str.compare(StringType(LITERAL("abcdefghijklmnopqrstuvwxyz"))) == 0, "StringBase - FXD::S32 compare(const my_type& str) const");
		PRINT_COND_ASSERT(str.compare(StringType(LITERAL("ABCDEFGHIJKLMNOPQRSTUVWXYZ"))) != 0, "StringBase - FXD::S32 compare(const my_type& str) const");
		PRINT_COND_ASSERT(str.compare(StringType(LITERAL("abcdefghijklmnopqrstuvwxyz123"))) != 0, "StringBase - FXD::S32 compare(const my_type& str) const");
		PRINT_COND_ASSERT(str.compare(1, 5, StringType(LITERAL("bcdef"))) == 0, "StringBase - FXD::S32 compare(size_type nPos, size_type nCount, const my_type& str) const");

		PRINT_COND_ASSERT(str.compare(LITERAL("abcdefghijklmnopqrstuvwxyz")) == 0, "StringBase - FXD::S32 compare(const_pointer pStr) const");
		PRINT_COND_ASSERT(str.compare(LITERAL("abcdefghijklmnopqrstuvwxyz123")) != 0, "StringBase - FXD::S32 compare(const_pointer pStr) const");
		PRINT_COND_ASSERT(str.compare(LITERAL("ABCDEFGHIJKLMNOPQRSTUVWXYZ123")) != 0, "StringBase - FXD::S32 compare(const_pointer pStr) const");
		PRINT_COND_ASSERT(str.compare(1, 5, LITERAL("bcdef")) == 0, "StringBase - FXD::S32 compare(size_type nPos, size_type nCount, const_pointer pStr) const");
	}

	// FXD::S32 compare_no_case(const my_type& str) const;
	// FXD::S32 compare_no_case(size_type nPos, size_type nCount, const my_type& str) const;
	// FXD::S32 compare_no_case(size_type nPos, size_type nCount, const my_type& str, size_type nPos2, size_type nCount2) const;
	// FXD::S32 compare_no_case(const_pointer pStr) const;
	// FXD::S32 compare_no_case(size_type nPos, size_type nCount, const_pointer pStr) const;
	// FXD::S32 compare_no_case(size_type nPos, size_type nCount, const_pointer pStr, size_type nCount2) const;
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));

		PRINT_COND_ASSERT(str.compare_no_case(StringType(LITERAL("abcdefghijklmnopqrstuvwxyz"))) == 0, "StringBase - FXD::S32 compare_no_case(const my_type& str) const");
		PRINT_COND_ASSERT(str.compare_no_case(StringType(LITERAL("ABCDEFGHIJKLMNOPQRSTUVWXYZ"))) == 0, "StringBase - FXD::S32 compare_no_case(const my_type& str) const");
		PRINT_COND_ASSERT(str.compare_no_case(StringType(LITERAL("abcdefghijklmnopqrstuvwxyz123"))) != 0, "StringBase - FXD::S32 compare_no_case(const my_type& str) const");
		PRINT_COND_ASSERT(str.compare_no_case(1, 5, StringType(LITERAL("BCDEF"))) == 0, "StringBase - FXD::S32 compare_no_case(size_type nPos, size_type nCount, const my_type& str) const");

		PRINT_COND_ASSERT(str.compare_no_case(LITERAL("abcdefghijklmnopqrstuvwxyz")) == 0, "StringBase - FXD::S32 compare_no_case(const_pointer pStr) const");
		PRINT_COND_ASSERT(str.compare_no_case(LITERAL("ABCDEFGHIJKLMNOPQRSTUVWXYZ")) == 0, "StringBase - FXD::S32 compare_no_case(const_pointer pStr) const");
		PRINT_COND_ASSERT(str.compare_no_case(LITERAL("abcdefghijklmnopqrstuvwxyz123")) != 0, "StringBase - FXD::S32 compare_no_case(const_pointer pStr) const");
		PRINT_COND_ASSERT(str.compare_no_case(1, 5, LITERAL("BCDEF")) == 0, "StringBase - FXD::S32 compare_no_case(size_type nPos, size_type nCount, const my_type& str) const");
	}

	// void to_lower(void);
	{
		{
			StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));
			str.to_lower();
			PRINT_COND_ASSERT(str == LITERAL("abcdefghijklmnopqrstuvwxyz"), "StringBase - void to_lower(void)");
		}
		{
			StringType str(LITERAL("ABCDEFGHIJKLMNOPQRSTUVWXYZ"));
			str.to_lower();
			PRINT_COND_ASSERT(str == LITERAL("abcdefghijklmnopqrstuvwxyz"), "StringBase - void to_lower(void)");
		}
		{
			StringType str(LITERAL("123456789~!@#$%^&*()_+"));
			str.to_lower();
			PRINT_COND_ASSERT(str == LITERAL("123456789~!@#$%^&*()_+"), "StringBase - void to_lower(void)");
		}
	}

	// void to_upper(void);
	{
		{
			StringType str(LITERAL("ABCDEFGHIJKLMNOPQRSTUVWXYZ"));
			str.to_upper();
			PRINT_COND_ASSERT(str == LITERAL("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), "StringBase - void to_upper(void)");
		}
		{
			StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));
			str.to_upper();
			PRINT_COND_ASSERT(str == LITERAL("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), "StringBase - void to_upper(void)");
		}
		{
			StringType str(LITERAL("123456789~!@#$%^&*()_+"));
			str.to_upper();
			PRINT_COND_ASSERT(str == LITERAL("123456789~!@#$%^&*()_+"), "StringBase - void to_upper(void)");
		}
	}

	// void ltrim(void);
	// void rtrim(void);
	// void trim(void);
	{
		StringType str(LITERAL("abcdefghijklmnopqrstuvwxyz"));
		{
			StringType rstr(LITERAL("abcdefghijklmnopqrstuvwxyz			\t		\t\t\t		"));
			rstr.ltrim();
			PRINT_COND_ASSERT(str != rstr, "StringBase - void ltrim(void)");
		}
		{
			StringType lstr(LITERAL("	\t						abcdefghijklmnopqrstuvwxyz"));
			lstr.ltrim();
			PRINT_COND_ASSERT(str == lstr, "StringBase - void ltrim(void)");
		}
		{
			StringType rstr(LITERAL("abcdefghijklmnopqrstuvwxyz		\t\t\t					"));
			rstr.rtrim();
			PRINT_COND_ASSERT(str == rstr, "StringBase - void rtrim(void)");
		}
		{
			StringType lstr(LITERAL("	\t						abcdefghijklmnopqrstuvwxyz"));
			lstr.rtrim();
			PRINT_COND_ASSERT(str != lstr, "StringBase - void rtrim(void)");
		}
		{
			StringType lrstr(LITERAL("   \t                abcdefghijklmnopqrstuvwxyz		\t					"));
			lrstr.trim();
			PRINT_COND_ASSERT(str == lrstr, "StringBase - void trim(void)");
		}
		{
			auto* pLiteral = LITERAL("abcdefghijklmn          opqrstuvwxyz");
			StringType mstr(pLiteral);
			mstr.trim();
			PRINT_COND_ASSERT(mstr == pLiteral, "StringBase - void trim(void)");
		}
	}
	return nErrorCount;
}

// Required to prevent manual undef of macros when 'TestString.inl' preprocessed at the top of the unit test cpp file.
#undef TEST_STRING_NAME
#undef LITERAL