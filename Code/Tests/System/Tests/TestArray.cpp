// Creator - MatthewGolder
#include "Tests/System/Tests/TestSet.h"
#include "Tests/System/TestHelperClasses.h"
#include "FXDEngine/Container/Array.h"
#include <array>

#include "TestArray.inl"


// Template instantations.
template class FXD::Container::Array< FXD::S32, 32 >;
template class FXD::Container::Array< Game::Align32, 32 >;

FXD::S32 TestArray(void)
{
	FXD::S32 nErrorCount = 0;

	nErrorCount += TestArray< std::array< FXD::S32, 5 > >();
	nErrorCount += TestArray< FXD::Container::Array< FXD::S32, 5 > >();
	nErrorCount += TestArray20();

	return nErrorCount;
}