// Creator - MatthewGolder
#ifndef TESTS_SYSTEM_TESTS_TESTMAP_H
#define TESTS_SYSTEM_TESTS_TESTMAP_H

#include "Tests/System/Tests/FXDTest.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/Core/Algorithm.h"
#include "FXDEngine/Core/Random.h"
#include "FXDEngine/Core/Utility.h"
#include "FXDEngine/Container/Vector.h"

///////////////////////////////////////////////////////////////////////////////
// TestMapConstruction
//
// This test compares FXD::Container::Map/FXD::Container::MultiMap to std::map/multimap.
//
// Requires a container that can hold at least 1000 items.
///////////////////////////////////////////////////////////////////////////////
template < typename T1, typename T2, bool bMultimap >
FXD::S32 TestMapConstruction(void)
{
	FXD::S32 nErrorCount = 0;
	TestObject::reset();
	{
		std::unique_ptr< T1 > pt1A = std::make_unique< T1 >();
		std::unique_ptr< T2 > pt2A = std::make_unique< T2 >();
		T1& t1A = *pt1A;
		T2& t2A = *pt2A;
		nErrorCount += CompareContainers(t1A, t2A, "Map - Map(void)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());
		PRINT_COND_ASSERT(t1A.validate(), "Map - Map(void)");

		std::unique_ptr< T1 > pt1B = std::make_unique< T1 >();
		std::unique_ptr< T2 > pt2B = std::make_unique< T2 >();
		T1& t1B = *pt1B;
		T2& t2B = *pt2B;
		nErrorCount += CompareContainers(t1B, t2B, "Map - Map(void)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());

		std::unique_ptr< T1 > pt1C = std::make_unique< T1 >();
		std::unique_ptr< T2 > pt2C = std::make_unique< T2 >();
		T1& t1C = *pt1C;
		T2& t2C = *pt2C;
		for (FXD::U32 n1 = 0; n1 < 1000; n1++)
		{
			t1C.insert(typename T1::value_type(typename T1::key_type(n1), typename T1::mapped_type(n1)));
			t2C.insert(typename T2::value_type(typename T2::key_type(n1), typename T2::mapped_type(n1)));
			PRINT_COND_ASSERT(t1C.validate(), "Map - insert_return_type insert(value_type&& val)");
			nErrorCount += CompareContainers(t1C, t2C, "Map - insert_return_type insert(value_type&& val)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());
		}

		std::unique_ptr< T1 > pt1D = std::make_unique< T1 >();
		std::unique_ptr< T2 > pt2D = std::make_unique< T2 >();
		T1& t1D = *pt1D;
		T2& t2D = *pt2D;
		nErrorCount += CompareContainers(t1D, t2D, "Map - Map(void)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());

		std::unique_ptr< T1 > pt1E = std::make_unique< T1 >(t1C);
		std::unique_ptr< T2 > pt2E = std::make_unique< T2 >(t2C);
		T1& t1E = *pt1E;
		T2& t2E = *pt2E;
		PRINT_COND_ASSERT(t1E.validate(), "Map - Map(void)");
		nErrorCount += CompareContainers(t1E, t2E, "Map - Map(void)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());

		std::unique_ptr< T1 > pt1F = std::make_unique< T1 >(t1C.begin(), t1C.end());
		std::unique_ptr< T2 > pt2F = std::make_unique< T2 >(t2C.begin(), t2C.end());
		T1& t1F = *pt1F;
		T2& t2F = *pt2F;
		PRINT_COND_ASSERT(t1F.validate(), "Map - Map(InputItr first, InputItr last)");
		nErrorCount += CompareContainers(t1F, t2F, "Map - Map(InputItr first, InputItr last)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());

		// my_type& operator=(const my_type& rhs);
		{
			t1E = t1D;
			t2E = t2D;
			nErrorCount += CompareContainers(t1D, t2D, "Map - my_type& operator=(const my_type& rhs)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());
			nErrorCount += CompareContainers(t1E, t2E, "Map - my_type& operator=(const my_type& rhs)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());
		}

		// insert_return_type insert(const value_type& val);
		// insert_return_type insert(P&& val);
		// insert_return_type insert(value_type&& val);
		{
			std::unique_ptr< T1 > pT1P = std::make_unique< T1 >();
			std::unique_ptr< T1 > pT1Q = std::make_unique< T1 >();
			T1& t1P = *pT1P;
			T1& t1Q = *pT1Q;

			typename T1::key_type k10(0);
			typename T1::key_type k11(1);
			typename T1::key_type k12(2);
			typename T1::key_type k13(3);
			typename T1::key_type k14(4);
			typename T1::key_type k15(5);

			typename T1::value_type v10(k10, typename T1::mapped_type(0));
			typename T1::value_type v11(k11, typename T1::mapped_type(1));
			typename T1::value_type v12(k12, typename T1::mapped_type(2));
			typename T1::value_type v13(k13, typename T1::mapped_type(3));
			typename T1::value_type v14(k14, typename T1::mapped_type(4));
			typename T1::value_type v15(k15, typename T1::mapped_type(5));

			t1P.insert(v10);
			t1P.insert(v11);
			t1P.insert(v12);

			t1Q.insert(v13);
			t1Q.insert(v14);
			t1Q.insert(v15);

			t1Q = std::move(t1P);
			PRINT_COND_ASSERT((t1Q.size() == 3) && (t1Q.find(k10) != t1Q.end()) && (t1Q.find(k11) != t1Q.end()) && (t1Q.find(k12) != t1Q.end()), "Map - insert_return_type insert(P&& val)");
		}

		// void swap(my_type& rhs)
		{
			t1E.swap(t1D);
			t2E.swap(t2D);
			PRINT_COND_ASSERT(t1D.validate(), "Map - void swap(my_type& rhs)");
			PRINT_COND_ASSERT(t1E.validate(), "Map - void swap(my_type& rhs)");
			nErrorCount += CompareContainers(t1D, t2D, "Map - void swap(my_type& rhs)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());
			nErrorCount += CompareContainers(t1E, t2E, "Map - void swap(my_type& rhs)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());
		}

		// void clear(void)
		{
			t1A.clear();
			t2A.clear();
			PRINT_COND_ASSERT(t1A.validate(), "Map - void clear(void)");
			nErrorCount += CompareContainers(t1A, t2A, "Map - void clear(void)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());

			t1B.clear();
			t2B.clear();
			PRINT_COND_ASSERT(t1B.validate(), "Map - void clear(void)");
			nErrorCount += CompareContainers(t1B, t2B, "Map - void clear(void)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());
		}
	}

	PRINT_COND_ASSERT(TestObject::is_clear(), "Map - Failed");
	TestObject::reset();

	return nErrorCount;
}

///////////////////////////////////////////////////////////////////////////////
// TestMapMutation
//
// Requires a container that can hold at least 1000 items.
///////////////////////////////////////////////////////////////////////////////
template < typename T1, typename T2, bool bMultimap >
FXD::S32 TestMapMutation(void)
{
	FXD::S32 nErrorCount = 0;
	TestObject::reset();
	{
		std::unique_ptr< T1 > pt1A = std::make_unique< T1 >();
		std::unique_ptr< T2 > pt2A = std::make_unique< T2 >();
		T1& t1A = *pt1A;
		T2& t2A = *pt2A;

		if (gFXD_TestLevel != FXD_TestLevel::Low)
		{
			// Set up an array of values to randomize / permute.
			FXD::Container::Vector< typename T1::key_type > vecInsert;

			FXD::Core::RandomGen rng(FXD::App::GetRandSeed());

			for (FXD::U32 n1 = 0; n1 < 1000; n1++)
			{
				vecInsert.push_back(typename T1::key_type(n1));

				// Occasionally attempt to duplicate an element, both for map and multimap.
				if (((n1 + 1) < 1000) && (rng.rand_limit(4) == 0))
				{
					vecInsert.push_back(typename T1::key_type(n1));
					n1++;
				}
			}

			for (FXD::U32 n1 = 0; n1 < FXD::STD::to_underlying(gFXD_TestLevel) * 20; n1++) // For each permutation...
			{
				FXD::STD::random_shuffle(vecInsert.begin(), vecInsert.end(), rng);

				// insert_return_type insert(value_type&& val);
				for (FXD::U32 n2 = 0, iEnd = (FXD::U32)vecInsert.size(); n2 < iEnd; n2++)
				{
					typename T1::key_type& key = vecInsert[n2];

					t1A.insert(typename T1::value_type(key, key)); // We expect that both arguments are the same.
					t2A.insert(typename T2::value_type(key, key));

					PRINT_COND_ASSERT(t1A.validate(), "Map - insert_return_type insert(value_type&& val)");
					nErrorCount += CompareContainers(t1A, t2A, "Map - void insert_return_type insert(value_type&& val)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());
				}

				// reverse_iterator rbegin(void) NOEXCEPT
				// reverse_iterator rend(void) NOEXCEPT
				typename T1::reverse_iterator r1 = t1A.rbegin();
				typename T2::reverse_iterator r2 = t2A.rbegin();

				while (r1 != t1A.rend())
				{
					typename T1::key_type k1 = (*r1).first;
					typename T2::key_type k2 = (*r2).first;
					PRINT_COND_ASSERT((k1 == k2), "Map - reverse_iterator rbegin(void) NOEXCEPT");

					r1++;
					r2++;
				}

				// size_type find_erase(const key_type& key)
				// bool contains(const key_type& key) const
				for (FXD::U32 n1 = 0, iEnd = (FXD::U32)vecInsert.size(); n1 < iEnd; n1++)
				{
					typename T1::key_type& key = vecInsert[n1];

					typename T1::size_type nRet1 = t1A.find_erase(key);
					typename T2::size_type nRet2 = t2A.erase(key);

					PRINT_COND_ASSERT((nRet1 == nRet2), "Map - size_type find_erase(const key_type& key)");
					PRINT_COND_ASSERT(t1A.validate(), "Map - size_type find_erase(const key_type& key)");
					nErrorCount += CompareContainers(t1A, t2A, "Map - bool find_erase(const key_type& key)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());
				}

				PRINT_COND_ASSERT((TestObject::sTOCount == 0) || (TestObject::sTOCount == (int64_t)vecInsert.size()), "Map - Failed"); // This test will only have meaning when T1 contains TestObject.
			}
		}

		PRINT_COND_ASSERT(TestObject::is_clear(), "Map - Failed");
		TestObject::reset();

		// Possibly do extended testing.
		if (gFXD_TestLevel == FXD_TestLevel::High)
		{
			// Set up an array of values to randomize / permute.
			FXD::Container::Vector< typename T1::key_type > vecInsert;

			for (FXD::U32 n1 = 0; n1 < 9; n1++) // Much more than this count would take too long to test all permutations.
			{
				vecInsert.push_back(typename T1::key_type(n1));
			}

			for (FXD::U32 n1 = 0; FXD::STD::next_permutation(vecInsert.begin(), vecInsert.end()); n1++) // For each permutation...
			{
				// insert_return_type insert(value_type&& val);
				for (FXD::U32 n2 = 0, iEnd = (FXD::U32)vecInsert.size(); n2 < iEnd; n2++)
				{
					typename T1::key_type& key = vecInsert[n2];

					t1A.insert(typename T1::value_type(key, key)); // We expect that both arguments are the same.
					t2A.insert(typename T2::value_type(key, key));

					PRINT_COND_ASSERT(t1A.validate(), "Map - insert_return_type insert(value_type&& val)");
					nErrorCount += CompareContainers(t1A, t2A, "Map - insert_return_type insert(value_type&& val)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());
				}

				// bool find_erase(const key_type& key)
				// bool contains(const key_type& key) const
				for (FXD::U32 n2 = 0, iEnd = (FXD::U32)vecInsert.size(); n2 < iEnd; n2++)
				{
					typename T1::key_type& key = vecInsert[n2];

					t1A.find_erase(key);
					t2A.erase(key);

					PRINT_COND_ASSERT(t1A.validate(), "Map - bool find_erase(const key_type& key)");
					nErrorCount += CompareContainers(t1A, t2A, "Map - bool find_erase(const key_type& key)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());
				}
				PRINT_COND_ASSERT((TestObject::sTOCount == 0) || (TestObject::sTOCount == (int64_t)vecInsert.size()), "Map - Failed"); // This test will only have meaning when T1 contains TestObject.
			}
		}
	}

	PRINT_COND_ASSERT(TestObject::is_clear(), "Map - Failed");
	TestObject::reset();

	{ // Other insert and erase operations
		std::unique_ptr< T1 > pt1A = std::make_unique< T1 >();
		std::unique_ptr< T2 > pt2A = std::make_unique< T2 >();
		T1& t1A = *pt1A;
		T2& t2A = *pt2A;

		// Set up an array of values to randomize / permute.
		FXD::Container::Vector< FXD::Core::Pair< typename T1::key_type, typename T1::mapped_type > > vecInsert1;
		std::vector< std::pair< typename T2::key_type, typename T2::mapped_type > > vecInsert2;

		FXD::Core::RandomGen rng(FXD::App::GetRandSeed());

		{
			for (FXD::U32 n1 = 0; n1 < 100; n1++)
			{
				vecInsert1.push_back(typename T1::value_type(typename T1::key_type(n1), typename T1::mapped_type(n1)));
				vecInsert2.push_back(typename T2::value_type(typename T2::key_type(n1), typename T2::mapped_type(n1)));

				if (rng.rand_limit(3) == 0)
				{
					vecInsert1.push_back(typename T1::value_type(typename T1::key_type(n1), typename T1::mapped_type(n1)));
					vecInsert2.push_back(typename T2::value_type(typename T2::key_type(n1), typename T2::mapped_type(n1)));
				}
			}
		}

		// void insert(InputItr first, InputItr last)
		{
			t1A.insert(vecInsert1.begin(), vecInsert1.end());
			t2A.insert(vecInsert2.begin(), vecInsert2.end());
			PRINT_COND_ASSERT(t1A.validate(), "Map - void insert(InputItr first, InputItr last)");
			nErrorCount += CompareContainers(t1A, t2A, "Map - void insert(InputItr first, InputItr last)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());
		}

		// insert_return_type insert(const Key& key);
		{
			t1A.insert(typename T1::key_type(8888));
			t2A.insert(typename T2::value_type(typename T2::key_type(8888), typename T2::mapped_type(0)));
			PRINT_COND_ASSERT(t1A.validate(), "Map - insert_return_type insert(const Key& key)");
			nErrorCount += CompareContainers(t1A, t2A, "Map - insert_return_type insert(const Key& key)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());
		}

		// iterator insert(const_iterator hint, const value_type& val);
		{
			typename T1::iterator itr1 = t1A.insert(t1A.find(typename T1::key_type(2)), typename T1::value_type(typename T1::key_type(1), typename T1::mapped_type(1)));
			typename T2::iterator itr2 = t2A.insert(t2A.find(typename T2::key_type(2)), typename T2::value_type(typename T2::key_type(1), typename T2::mapped_type(1)));
			PRINT_COND_ASSERT(t1A.validate(), "Map - iterator insert(const_iterator hint, const value_type& val)");
			PRINT_COND_ASSERT(itr1->first == typename T1::key_type(1), "Map - iterator insert(const_iterator hint, const value_type& val)");
			PRINT_COND_ASSERT(itr2->first == typename T2::key_type(1), "Map - iterator insert(const_iterator hint, const value_type& val)");
			nErrorCount += CompareContainers(t1A, t2A, "Map - iterator insert(const_iterator hint, const value_type& val)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());

			itr1 = t1A.insert(t1A.end(), typename T1::value_type(typename T1::key_type(5), typename T1::mapped_type(5)));
			itr2 = t2A.insert(t2A.end(), typename T2::value_type(typename T2::key_type(5), typename T2::mapped_type(5)));
			PRINT_COND_ASSERT(t1A.validate(), "Map - iterator insert(const_iterator hint, const value_type& val)");
			PRINT_COND_ASSERT(itr1->first == typename T1::key_type(5), "Map - iterator insert(const_iterator hint, const value_type& val)");
			PRINT_COND_ASSERT(itr2->first == typename T2::key_type(5), "Map - iterator insert(const_iterator hint, const value_type& val)");
			nErrorCount += CompareContainers(t1A, t2A, "Map - iterator insert(const_iterator hint, const value_type& val)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());

			// Now we remove these items so that the insertions above can succeed.
			t1A.erase(t1A.find(typename T1::key_type(1)));
			t2A.erase(t2A.find(typename T2::key_type(1)));
			itr1 = t1A.insert(t1A.find(typename T1::key_type(2)), typename T1::value_type(typename T1::key_type(1), typename T1::mapped_type(1)));
			itr2 = t2A.insert(t2A.find(typename T2::key_type(2)), typename T2::value_type(typename T2::key_type(1), typename T2::mapped_type(1)));
			PRINT_COND_ASSERT(t1A.validate(), "Map - iterator insert(const_iterator hint, const value_type& val)");
			PRINT_COND_ASSERT(itr1->first == typename T1::key_type(1), "Map - iterator insert(const_iterator hint, const value_type& val)");
			PRINT_COND_ASSERT(itr2->first == typename T2::key_type(1), "Map - iterator insert(const_iterator hint, const value_type& val)");
			nErrorCount += CompareContainers(t1A, t2A, "Map - iterator insert(const_iterator hint, const value_type& val)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());

			t1A.erase(t1A.find(typename T1::key_type(5)));
			t2A.erase(t2A.find(typename T2::key_type(5)));
			itr1 = t1A.insert(t1A.end(), typename T1::value_type(typename T1::key_type(5), typename T1::mapped_type(5)));
			itr2 = t2A.insert(t2A.end(), typename T2::value_type(typename T2::key_type(5), typename T2::mapped_type(5)));
			PRINT_COND_ASSERT(t1A.validate(), "Map - iterator insert(const_iterator hint, const value_type& val)");
			PRINT_COND_ASSERT(itr1->first == typename T1::key_type(5), "Map - iterator insert(const_iterator hint, const value_type& val)");
			PRINT_COND_ASSERT(itr2->first == typename T2::key_type(5), "Map - iterator insert(const_iterator hint, const value_type& val)");
			nErrorCount += CompareContainers(t1A, t2A, "Map - iterator insert(const_iterator hint, const value_type& val)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());
		}

		// iterator erase(const_iterator first, const_iterator last);
		{
			typename T1::iterator it11 = t1A.find(typename T1::key_type(17));
			typename T1::iterator it12 = t1A.find(typename T2::key_type(37));
			t1A.erase(it11, it12);

			typename T2::iterator it21 = t2A.find(typename T1::key_type(17));
			typename T2::iterator it22 = t2A.find(typename T2::key_type(37));
			t2A.erase(it21, it22);

			PRINT_COND_ASSERT(t1A.validate(), "Map - iterator erase(const_iterator first, const_iterator last)");
			nErrorCount += CompareContainers(t1A, t2A, "Map - iterator erase(const_iterator first, const_iterator last)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());
		}

		// iterator erase(const_iterator pos);
		{
			t1A.erase(t1A.find(typename T1::key_type(60)));
			t2A.erase(t2A.find(typename T1::key_type(60)));
			PRINT_COND_ASSERT(t1A.validate(), "Map - iterator erase(const_iterator pos)");
			nErrorCount += CompareContainers(t1A, t2A, "Map - iterator erase(const_iterator pos)", FXD::STD::UseFirst< typename T1::value_type >(), FXD::STD::UseFirst< typename T2::value_type >());
		}
	}

	PRINT_COND_ASSERT(TestObject::is_clear(), "Map - Failed");
	TestObject::reset();

	return nErrorCount;
}

template < typename T1 >
FXD::S32 TestMapSpecific(T1& t1A, FXD::STD::false_type) // FXD::STD::false_type means this is a map and not a multimap.
{
	FXD::S32 nErrorCount = 0;

	// T& operator[](const Key& key);
	// T& operator[](Key&& key);
	{
		typename T1::mapped_type mapped = t1A[typename T1::key_type(0)];
		PRINT_COND_ASSERT((mapped == typename T1::mapped_type(0)), "Map - T& operator[](Key&& key)");

		mapped = t1A[typename T1::key_type(999)];
		PRINT_COND_ASSERT((mapped == typename T1::mapped_type(999)), "Map - T& operator[](Key&& key)");

		mapped = t1A[typename T1::key_type(10000000)];
		PRINT_COND_ASSERT((mapped == typename T1::mapped_type(0)), "Map - T& operator[](Key&& key)");
	}

	return nErrorCount;
}

template < typename T1 >
FXD::S32 TestMapSpecific(T1& t1A, FXD::STD::true_type) // FXD::STD::true_type means this is a multimap and not a map.
{
	FXD::S32 nErrorCount = 0;

	// Core::Pair< iterator, iterator > equal_range_small(const Key& key)
	// Core::Pair< const_iterator, const_iterator > equal_range_small(const Key& key) const
	{
		FXD::Core::Pair< typename T1::iterator, typename T1::iterator > er = t1A.equal_range(typename T1::key_type(499));
		PRINT_COND_ASSERT((er.first->first == typename T1::key_type(499)), "Map - Core::Pair< const_iterator, const_iterator > equal_range_small(const Key& key) const");
		PRINT_COND_ASSERT((er.second->first == typename T1::key_type(501)), "Map - Core::Pair< const_iterator, const_iterator > equal_range_small(const Key& key) const");

		er = t1A.equal_range(typename T1::key_type(-1));
		PRINT_COND_ASSERT((er.first == er.second), "Map - Core::Pair< const_iterator, const_iterator > equal_range_small(const Key& key) const");
		PRINT_COND_ASSERT((er.first == t1A.begin()), "Map - Core::Pair< const_iterator, const_iterator > equal_range_small(const Key& key) const");
	}

	return nErrorCount;
}

///////////////////////////////////////////////////////////////////////////////
// TestMapSearch
//
// This function is designed to work with map, fixed_map (and not hash containers).
// Requires a container that can hold at least 1000 items.
///////////////////////////////////////////////////////////////////////////////
template < typename T1, bool bMultimap >
FXD::S32 TestMapSearch(void)
{
	FXD::S32 nErrorCount = 0;
	TestObject::reset();
	{
		std::unique_ptr< T1 > pt1A = std::make_unique< T1 >();
		T1& t1A = *pt1A;
		typename T1::iterator itr;

		FXD::Container::Vector< typename T1::key_type > vecInsert;
		for (FXD::U32 n1 = 0; n1 < 1000; n1++)
		{
			vecInsert.push_back(typename T1::key_type(n1));
		}

		FXD::Core::RandomGen rng(FXD::App::GetRandSeed());
		FXD::STD::random_shuffle(vecInsert.begin(), vecInsert.end(), rng);

		// insert_return_type insert(value_type&& val)
		{
			for (FXD::U32 n1 = 0, iEnd = (FXD::U32)vecInsert.size(); n1 < iEnd; n1++)
			{
				typename T1::key_type key(n1);
				t1A.insert(typename T1::value_type(key, key));

				itr = t1A.find(key);
				PRINT_COND_ASSERT(itr != t1A.end(), "Map - insert_return_type insert(value_type&& val)");
			}
		}

		// iterator find(const key_type& key)
		// const_iterator find(const key_type& key) const
		{
			for (FXD::U32 n1 = 0; n1 < 1000; n1++)
			{
				typename T1::key_type key(n1);
				itr = t1A.find(key);

				PRINT_COND_ASSERT(itr != t1A.end(), "Map - const_iterator find(const key_type& key) const");
				PRINT_COND_ASSERT(itr->first == key, "Map - const_iterator find(const key_type& key) const");
				PRINT_COND_ASSERT(itr->second == key, "Map - const_iterator find(const key_type& key) const");
			}

			itr = t1A.find(typename T1::key_type(-1));
			PRINT_COND_ASSERT(itr == t1A.end(), "Map - const_iterator find(const key_type& key) const");
			itr = t1A.find(typename T1::key_type(1001));
			PRINT_COND_ASSERT(itr == t1A.end(), "Map - const_iterator find(const key_type& key) const");
		}

		// iterator lower_bound(const key_type& key)
		// const_iterator lower_bound(const key_type& key) const
		{
			itr = t1A.lower_bound(typename T1::key_type(0));
			PRINT_COND_ASSERT(itr == t1A.begin(), "Map - const_iterator lower_bound(const key_type& key) const");

			itr = t1A.lower_bound(typename T1::key_type(-1));
			PRINT_COND_ASSERT(itr == t1A.begin(), "Map - const_iterator lower_bound(const key_type& key) const");

			itr = t1A.lower_bound(typename T1::key_type(1001));
			PRINT_COND_ASSERT(itr == t1A.end(), "Map - const_iterator lower_bound(const key_type& key) const");

			t1A.find_erase(typename T1::key_type(500));
			itr = t1A.lower_bound(typename T1::key_type(500));
			PRINT_COND_ASSERT(itr->first == typename T1::key_type(501), "Map - const_iterator lower_bound(const key_type& key) const");
		}

		// iterator upper_bound(const key_type& key)
		// const_iterator upper_bound(const key_type& key) const
		{
			itr = t1A.upper_bound(typename T1::key_type(-1));
			PRINT_COND_ASSERT(itr == t1A.begin(), "Map - const_iterator upper_bound(const key_type& key) const");

			itr = t1A.upper_bound(typename T1::key_type(499));
			PRINT_COND_ASSERT(itr->first == typename T1::key_type(501), "Map - const_iterator upper_bound(const key_type& key) const");

			itr = t1A.upper_bound(typename T1::key_type(-1));
			PRINT_COND_ASSERT(itr->first == typename T1::key_type(0), "Map - const_iterator upper_bound(const key_type& key) const");

			itr = t1A.upper_bound(typename T1::key_type(1000));
			PRINT_COND_ASSERT(itr == t1A.end(), "Map - const_iterator upper_bound(const key_type& key) const");
		}

		// size_type count(const Key& key) const
		{
			typename T1::size_type nCount = t1A.count(typename T1::key_type(-1));
			PRINT_COND_ASSERT((nCount == 0), "Map - size_type count(const Key& key) const");
			nCount = t1A.count(typename T1::key_type(0));
			PRINT_COND_ASSERT((nCount == 1), "Map - size_type count(const Key& key) const");
			nCount = t1A.count(typename T1::key_type(500));
			PRINT_COND_ASSERT((nCount == 0), "Map - size_type count(const Key& key) const");
			nCount = t1A.count(typename T1::key_type(1001));
			PRINT_COND_ASSERT((nCount == 0), "Map - size_type count(const Key& key) const");
		}

		// Core::Pair< iterator, iterator > equal_range(const Key& key)
		// Core::Pair< const_iterator, const_iterator > equal_range(const Key& key) const
		{
			FXD::Core::Pair< typename T1::iterator, typename T1::iterator > er = t1A.equal_range(typename T1::key_type(200));
			PRINT_COND_ASSERT(er.first->first == typename T1::key_type(200), "Map - Core::Pair< const_iterator, const_iterator > equal_range(const Key& key) const");
			PRINT_COND_ASSERT(er.first->second == typename T1::key_type(200), "Map - Core::Pair< const_iterator, const_iterator > equal_range(const Key& key) const");

			er = t1A.equal_range(typename T1::key_type(499));
			PRINT_COND_ASSERT(er.first->first == typename T1::key_type(499), "Map - Core::Pair< const_iterator, const_iterator > equal_range(const Key& key) const");
			PRINT_COND_ASSERT(er.second->first == typename T1::key_type(501), "Map - Core::Pair< const_iterator, const_iterator > equal_range(const Key& key) const");

			er = t1A.equal_range(typename T1::key_type(-1));
			PRINT_COND_ASSERT(er.first == er.second, "Map - Core::Pair< const_iterator, const_iterator > equal_range(const Key& key) const");
			PRINT_COND_ASSERT(er.first == t1A.begin(), "Map - Core::Pair< const_iterator, const_iterator > equal_range(const Key& key) const");
		}

		// Some tests need to be differently between map and multimap.
		nErrorCount += TestMapSpecific(t1A, FXD::STD::integral_constant< bool, bMultimap >());
	}

	PRINT_COND_ASSERT(TestObject::is_clear(), "Map - Failed");
	TestObject::reset();

	return nErrorCount;
}

///////////////////////////////////////////////////////////////////////////////
// TestMapCpp11
//
// This function is designed to work with map, fixed_map, hash_map, fixed_hash_map.
///////////////////////////////////////////////////////////////////////////////
template < typename T1 >
FXD::S32 TestMapCpp11(void)
{
	FXD::S32 nErrorCount = 0;
	TestObject::reset();

	// insert_return_type emplace(Args&&... args);
	// iterator emplace_hint(const_iterator pos, Args&&... args);
	// iterator insert(const_iterator pos, value_type&& val);
	{
		using TOMap = T1;
		using value_type = typename TOMap::value_type;
		typename TOMap::insert_return_type toMapInsertResult;
		typename TOMap::iterator itrMap;

		TOMap mapTO;
		TestObject to1(0);
		TestObject to2(1);

		{
			toMapInsertResult = mapTO.emplace(value_type(0, to1));
			PRINT_COND_ASSERT(toMapInsertResult.second, "Map - insert_return_type emplace(Args&&... args)");

			toMapInsertResult = mapTO.emplace(value_type(1, std::move(to2)));
			PRINT_COND_ASSERT(toMapInsertResult.second == true, "Map - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to4(4);
			value_type value40(4, to4);
			PRINT_COND_ASSERT(mapTO.find(4) == mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(value40.second.m_nX == 4, "Map - insert_return_type emplace(Args&&... args)");
			toMapInsertResult = mapTO.emplace(std::move(value40));
			PRINT_COND_ASSERT(toMapInsertResult.second == true, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(4) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(value40.second.m_nX == 0, "Map - insert_return_type emplace(Args&&... args)");

			value_type value41(4, TestObject(41));
			toMapInsertResult = mapTO.emplace(std::move(value41));
			PRINT_COND_ASSERT(toMapInsertResult.second == false, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(toMapInsertResult.first->second.m_nX == 4, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(4) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to5(5);
			value_type value50(5, to5);
			PRINT_COND_ASSERT(mapTO.find(5) == mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
			toMapInsertResult = mapTO.emplace(std::move(value50));
			PRINT_COND_ASSERT(toMapInsertResult.second == true, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(5) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");

			value_type value51(5, TestObject(51));
			itrMap = mapTO.emplace_hint(toMapInsertResult.first, std::move(value51));
			PRINT_COND_ASSERT(itrMap->first == 5, "Map - iterator emplace_hint(const_iterator hint, value_type&& args)");
			PRINT_COND_ASSERT(itrMap->second.m_nX == 5, "Map - iterator emplace_hint(const_iterator hint, value_type&& args)");
			PRINT_COND_ASSERT(mapTO.find(5) != mapTO.end(), "Map - iterator emplace_hint(const_iterator hint, value_type&& args)");
		}
		{
			TestObject to(6);
			value_type value1(6, to);
			PRINT_COND_ASSERT(mapTO.find(6) == mapTO.end(), "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
			itrMap = mapTO.emplace_hint(mapTO.begin(), std::move(value1));
			PRINT_COND_ASSERT(itrMap->first == 6, "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(6) != mapTO.end(), "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
		}
		{
			TestObject to(2);
			PRINT_COND_ASSERT(mapTO.find(2) == mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
			toMapInsertResult = mapTO.emplace(value_type(2, to));
			PRINT_COND_ASSERT(toMapInsertResult.second, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(2) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
			toMapInsertResult = mapTO.emplace(value_type(2, to));
			PRINT_COND_ASSERT(!toMapInsertResult.second, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(2) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to7(7);
			value_type value70(7, to7);
			PRINT_COND_ASSERT(mapTO.find(7) == mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
			toMapInsertResult = mapTO.emplace(value70);
			PRINT_COND_ASSERT(toMapInsertResult.second == true, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(7) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");

			value_type value71(7, TestObject(71));
			itrMap = mapTO.emplace_hint(toMapInsertResult.first, value71);
			PRINT_COND_ASSERT(itrMap->first == 7, "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
			PRINT_COND_ASSERT(itrMap->second.m_nX == 7, "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(7) != mapTO.end(), "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
		}
		{
			TestObject to8(8);
			value_type value8(8, to8);
			PRINT_COND_ASSERT(mapTO.find(8) == mapTO.end(), "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
			itrMap = mapTO.emplace_hint(mapTO.begin(), value8); // specify a bad hint. Insertion should still work.
			PRINT_COND_ASSERT(itrMap->first == 8, "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(8) != mapTO.end(), "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
		}
		{
			TestObject to(3);
			PRINT_COND_ASSERT(mapTO.find(3) == mapTO.end(), "Map - insert_return_type insert(value_type&& val)");
			toMapInsertResult = mapTO.insert(value_type(3, to));
			PRINT_COND_ASSERT(toMapInsertResult.second, "Map - insert_return_type insert(value_type&& val)");
			PRINT_COND_ASSERT(mapTO.find(3) != mapTO.end(), "Map - insert_return_type insert(value_type&& val)");
			toMapInsertResult = mapTO.insert(value_type(3, to));
			PRINT_COND_ASSERT(!toMapInsertResult.second, "Map - insert_return_type insert(value_type&& val)");
			PRINT_COND_ASSERT(mapTO.find(3) != mapTO.end(), "Map - insert_return_type insert(value_type&& val)");
		}
		{
			TestObject to9(9);
			value_type value90(9, to9);
			PRINT_COND_ASSERT(mapTO.find(9) == mapTO.end(), "Map - iterator emplace(Args&&... args)");
			toMapInsertResult = mapTO.emplace(std::move(value90));
			PRINT_COND_ASSERT(toMapInsertResult.second == true, "Map - iterator emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(9) != mapTO.end(), "Map - iterator emplace(Args&&... args)");

			value_type value91(9, TestObject(91));
			itrMap = mapTO.insert(toMapInsertResult.first, std::move(value91));
			PRINT_COND_ASSERT(itrMap->first == 9, "Map - iterator insert(const_iterator hint, value_type&& val)");
			PRINT_COND_ASSERT(itrMap->second.m_nX == 9, "Map - iterator insert(const_iterator hint, value_type&& val)");
			PRINT_COND_ASSERT(mapTO.find(9) != mapTO.end(), "Map - iterator insert(const_iterator hint, value_type&& val)");
		}
		{
			TestObject to10(10);
			value_type value10(10, to10);
			PRINT_COND_ASSERT(mapTO.find(10) == mapTO.end(), "Map - insert(const_iterator hint, value_type&& val)");
			itrMap = mapTO.insert(mapTO.begin(), std::move(value10)); // specify a bad hint. Insertion should still work.
			PRINT_COND_ASSERT(itrMap->first == 10, "Map - insert(const_iterator hint, value_type&& val)");
			PRINT_COND_ASSERT(mapTO.find(10) != mapTO.end(), "Map - insert(const_iterator hint, value_type&& val)");
		}
		{
			TestObject to11(11);
			PRINT_COND_ASSERT(mapTO.find(11) == mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
			toMapInsertResult = mapTO.emplace(11, to11);
			PRINT_COND_ASSERT(toMapInsertResult.second == true, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(toMapInsertResult.first->first == 11, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(11) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to111(111);
			toMapInsertResult = mapTO.emplace(11, to111);
			PRINT_COND_ASSERT(toMapInsertResult.second == false, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(toMapInsertResult.first->first == 11, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(toMapInsertResult.first->second.m_nX == 11, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(11) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to12(12);
			PRINT_COND_ASSERT(mapTO.find(12) == mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
			toMapInsertResult = mapTO.emplace(12, std::move(to12));
			PRINT_COND_ASSERT(toMapInsertResult.second == true, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(toMapInsertResult.first->first == 12, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(12) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to121(121);
			toMapInsertResult = mapTO.emplace(12, std::move(to121));
			PRINT_COND_ASSERT(toMapInsertResult.second == false, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(toMapInsertResult.first->first == 12, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(toMapInsertResult.first->second.m_nX == 12, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(12) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
		}
		{
			PRINT_COND_ASSERT(mapTO.find(13) == mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
			toMapInsertResult = mapTO.emplace(FXD::STD::piecewise_construct, FXD::make_tuple(13), FXD::make_tuple(1, 2, 10)); // 1 + 2 + 10 = 13
			PRINT_COND_ASSERT(toMapInsertResult.second == true, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(toMapInsertResult.first->first == 13, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(13) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
		}
		{
			toMapInsertResult = mapTO.emplace(FXD::STD::piecewise_construct, FXD::make_tuple(13), FXD::make_tuple(1, 30, 100)); // 1 + 30 + 100 = 131
			PRINT_COND_ASSERT(toMapInsertResult.second == false, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(toMapInsertResult.first->first == 13, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(toMapInsertResult.first->second.m_nX == 13, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(13) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to14(14);
			PRINT_COND_ASSERT(mapTO.find(14) == mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
			toMapInsertResult = mapTO.emplace(14, to14);
			PRINT_COND_ASSERT(toMapInsertResult.second == true, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(14) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to141(141);
			itrMap = mapTO.emplace_hint(toMapInsertResult.first, 14, to141);
			PRINT_COND_ASSERT(itrMap->first == 14, "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
			PRINT_COND_ASSERT(itrMap->second.m_nX == 14, "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(14) != mapTO.end(), "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
		}
		{
			TestObject to15(15);
			PRINT_COND_ASSERT(mapTO.find(15) == mapTO.end(), "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
			itrMap = mapTO.emplace_hint(mapTO.begin(), 15, to15); // specify a bad hint. Insertion should still work.
			PRINT_COND_ASSERT(itrMap->first == 15, "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(15) != mapTO.end(), "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
		}
		{
			TestObject to16(16);
			PRINT_COND_ASSERT(mapTO.find(16) == mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
			toMapInsertResult = mapTO.emplace(16, std::move(to16));
			PRINT_COND_ASSERT(toMapInsertResult.second == true, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(16) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to161(161);
			itrMap = mapTO.emplace_hint(toMapInsertResult.first, 16, std::move(to161));
			PRINT_COND_ASSERT(itrMap->first == 16, "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
			PRINT_COND_ASSERT(itrMap->second.m_nX == 16, "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(16) != mapTO.end(), "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
		}
		{
			TestObject to17(17);
			PRINT_COND_ASSERT(mapTO.find(17) == mapTO.end(), "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
			itrMap = mapTO.emplace_hint(mapTO.begin(), 17, std::move(to17)); // specify a bad hint. Insertion should still work.
			PRINT_COND_ASSERT(itrMap->first == 17, "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(17) != mapTO.end(), "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
		}
		{
			PRINT_COND_ASSERT(mapTO.find(18) == mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
			toMapInsertResult = mapTO.emplace(FXD::STD::piecewise_construct, FXD::make_tuple(18), FXD::make_tuple(3, 5, 10)); // 3 + 5 + 10 = 18
			PRINT_COND_ASSERT(toMapInsertResult.second == true, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(18) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
		}
		{
			itrMap = mapTO.emplace_hint(toMapInsertResult.first, FXD::STD::piecewise_construct, FXD::make_tuple(18), FXD::make_tuple(1, 80, 100)); // 1 + 80 + 100 = 181
			PRINT_COND_ASSERT(itrMap->first == 18, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(itrMap->second.m_nX == 18, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(18) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
		}
		{
			PRINT_COND_ASSERT(mapTO.find(19) == mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
			itrMap = mapTO.emplace_hint(mapTO.begin(), FXD::STD::piecewise_construct, FXD::make_tuple(19), FXD::make_tuple(4, 5, 10)); // 4 + 5 + 10 = 19 // specify a bad hint. Insertion should still work.
			PRINT_COND_ASSERT(itrMap->first == 19, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(19) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to20(20);
			value_type value20(20, to20);
			PRINT_COND_ASSERT(mapTO.find(20) == mapTO.end(), "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
			toMapInsertResult = mapTO.emplace(value20);
			PRINT_COND_ASSERT(toMapInsertResult.second == true, "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(20) != mapTO.end(), "Map - iterator emplace_hint(const_iterator hint, Args&&... args)");
		}
		{
			value_type value201(20, TestObject(201));
			itrMap = mapTO.insert(toMapInsertResult.first, value201);
			PRINT_COND_ASSERT(itrMap->first == 20, "Map - iterator insert(const_iterator hint, P&& val)");
			PRINT_COND_ASSERT(itrMap->second.m_nX == 20, "Map - iterator insert(const_iterator hint, P&& val)");
			PRINT_COND_ASSERT(mapTO.find(20) != mapTO.end(), "Map - iterator insert(const_iterator hint, P&& val)");
		}
		{
			TestObject to21(21);
			value_type value21(21, to21);
			PRINT_COND_ASSERT(mapTO.find(21) == mapTO.end(), "Map - iterator insert(const_iterator hint, P&& val)");
			itrMap = mapTO.insert(mapTO.begin(), value21); // specify a bad hint. Insertion should still work.
			PRINT_COND_ASSERT(itrMap->first == 21, "Map - iterator insert(const_iterator hint, P&& val)");
			PRINT_COND_ASSERT(mapTO.find(21) != mapTO.end(), "Map - iterator insert(const_iterator hint, P&& val)");
		}
		{
			mapTO.insert({ value_type(22, TestObject(22)), value_type(23, TestObject(23)), value_type(24, TestObject(24)) });
			PRINT_COND_ASSERT(mapTO.find(22) != mapTO.end(), "Map - void insert(std::initializer_list< value_type > iList)");
			PRINT_COND_ASSERT(mapTO.find(23) != mapTO.end(), "Map - void insert(std::initializer_list< value_type > iList)");
			PRINT_COND_ASSERT(mapTO.find(24) != mapTO.end(), "Map - void insert(std::initializer_list< value_type > iList)");
		}
	}

	// key_compare& key_comp(void);
	// const key_compare& key_comp(void) const;
	{
		/*CLANGG
		VM1 vm;
		const VM1 vmc;
		const VM1::key_compare& kc = vmc.key_comp();
		vm.key_comp() = kc;
		*/
	}

	return nErrorCount;
}

template < typename T >
FXD::S32 TestMapCpp11NonCopyable(void)
{
	FXD::S32 nErrorCount = 0;

	// T& operator[](Key&& key);
	{
		T ncMap;
		ncMap[1].m_nVal = 1;
		PRINT_COND_ASSERT(ncMap[1].m_nVal == 1, "Map - T& operator[](Key&& key)");
	}

	return nErrorCount;
}

///////////////////////////////////////////////////////////////////////////////
// TestMultimapCpp11
//
// This function is designed to work with multimap, fixed_multimap, hash_multimap, fixed_hash_multimap
//
// This is similar to the TestSetCpp11 function, with some differences related
// to handling of duplicate entries.
///////////////////////////////////////////////////////////////////////////////
template < typename T1 >
FXD::S32 TestMultimapCpp11(void)
{
	FXD::S32 nErrorCount = 0;
	TestObject::reset();

	// insert_return_type emplace(Args&&... args);
	// iterator emplace_hint(const_iterator pos, Args&&... args);
	// iterator insert(const_iterator pos, value_type&& val);
	{
		using TOMap = T1;
		using value_type = typename TOMap::value_type;
		typename TOMap::iterator itrMap;

		TOMap mapTO;
		{
			TestObject to1(0);
			TestObject to2(1);
			itrMap = mapTO.emplace(value_type(0, to1));
			PRINT_COND_ASSERT(itrMap->first == 0, "Map - insert_return_type emplace(Args&&... args)");

			itrMap = mapTO.emplace(value_type(1, std::move(to2)));
			PRINT_COND_ASSERT(itrMap->first == 1, "Map - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to(4);
			value_type value1(4, to);
			PRINT_COND_ASSERT(mapTO.find(4) == mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(value1.second.m_nX == 4, "Map - insert_return_type emplace(Args&&... args)");

			itrMap = mapTO.emplace(std::move(value1));
			PRINT_COND_ASSERT(itrMap->first == 4, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(4) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(value1.second.m_nX == 0, "Map - insert_return_type emplace(Args&&... args)");

			value_type value2(4, to);
			itrMap = mapTO.emplace(std::move(value2));
			PRINT_COND_ASSERT(itrMap->first == 4, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(4) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to(5);
			value_type value1(5, to);
			itrMap = mapTO.emplace(std::move(value1));
			PRINT_COND_ASSERT(itrMap->first == 5, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(5) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");

			value_type value2(5, to);
			itrMap = mapTO.emplace_hint(itrMap, std::move(value2));
			PRINT_COND_ASSERT(itrMap->first == 5, "Map - iterator emplace_hint(const_iterator pos, Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(5) != mapTO.end(), "Map - iterator emplace_hint(const_iterator pos, Args&&... args)");
		}
		{
			TestObject to(6);
			value_type value1(6, to);
			itrMap = mapTO.emplace_hint(mapTO.begin(), std::move(value1));
			PRINT_COND_ASSERT(itrMap->first == 6, "Map - iterator emplace_hint(const_iterator pos, Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(6) != mapTO.end(), "Map - iterator emplace_hint(const_iterator pos, Args&&... args)");
		}
		{
			TestObject to(2);
			PRINT_COND_ASSERT(mapTO.find(2) == mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");

			itrMap = mapTO.emplace(value_type(2, to));
			PRINT_COND_ASSERT(itrMap->first == 2, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(2) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");

			itrMap = mapTO.emplace(value_type(2, to));
			PRINT_COND_ASSERT(itrMap->first == 2, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(2) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to(7);
			value_type value1(7, to);
			itrMap = mapTO.emplace(value1);
			PRINT_COND_ASSERT(itrMap->first == 7, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(7) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");

			value_type value2(7, to);
			itrMap = mapTO.emplace_hint(itrMap, value2);
			PRINT_COND_ASSERT(itrMap->first == 7, "Map - iterator emplace_hint(const_iterator pos, Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(7) != mapTO.end(), "Map - iterator emplace_hint(const_iterator pos, Args&&... args)");;
		}
		{
			TestObject to(8);
			value_type value8(8, to);
			itrMap = mapTO.emplace_hint(mapTO.begin(), value8);
			PRINT_COND_ASSERT(itrMap->first == 8, "Map - iterator emplace_hint(const_iterator pos, Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(8) != mapTO.end(), "Map - iterator emplace_hint(const_iterator pos, Args&&... args)");
		}
		{
			TestObject to(3);
			PRINT_COND_ASSERT(mapTO.find(3) == mapTO.end(), "Map - iterator insert(const_iterator pos, value_type&& val)");

			itrMap = mapTO.insert(value_type(3, to));
			PRINT_COND_ASSERT(itrMap->first == 3, "Map - iterator insert(const_iterator pos, value_type&& val)");
			PRINT_COND_ASSERT(mapTO.find(3) != mapTO.end(), "Map - iterator insert(const_iterator pos, value_type&& val)");

			itrMap = mapTO.insert(value_type(3, to));
			PRINT_COND_ASSERT(itrMap->first == 3, "Map - iterator insert(const_iterator pos, value_type&& val)");
			PRINT_COND_ASSERT(mapTO.find(3) != mapTO.end(), "Map - iterator insert(const_iterator pos, value_type&& val)");
		}
		{
			TestObject to(9);
			value_type value1(9, to);
			itrMap = mapTO.emplace(std::move(value1));
			PRINT_COND_ASSERT(itrMap->first == 9, "Map - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(9) != mapTO.end(), "Map - insert_return_type emplace(Args&&... args)");

			value_type value2(9, to);
			itrMap = mapTO.emplace_hint(itrMap, std::move(value2));
			PRINT_COND_ASSERT(itrMap->first == 9, "Map - iterator emplace_hint(const_iterator pos, Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(9) != mapTO.end(), "Map - iterator emplace_hint(const_iterator pos, Args&&... args)");
		}
		{
			TestObject to(10);
			value_type value1(10, to);
			itrMap = mapTO.emplace_hint(mapTO.begin(), std::move(value1));
			PRINT_COND_ASSERT(itrMap->first == 10, "Map - iterator emplace_hint(const_iterator pos, Args&&... args)");
			PRINT_COND_ASSERT(mapTO.find(10) != mapTO.end(), "Map - iterator emplace_hint(const_iterator pos, Args&&... args)");
		}
	}

	return nErrorCount;
}

///////////////////////////////////////////////////////////////////////////////
// TestMapCpp17
//
// This function is designed to work with map, fixed_map, hash_map, fixed_hash_map, unordered_map.
///////////////////////////////////////////////////////////////////////////////
template < typename T1 >
FXD::S32 TestMapCpp17(void)
{
	FXD::S32 nErrorCount = 0;
	TestObject::reset();

	using TOMap = T1;
	using mapped_type = typename TOMap::mapped_type;
	typename TOMap::iterator itrMap;

	// Core::Pair< iterator, bool > try_emplace(const key_type& key, Args&&... args);
	// Core::Pair< iterator, bool > try_emplace(key_type&& key, Args&&... args);
	// iterator try_emplace(const_iterator hint, const key_type& key, Args&&... args);
	// iterator try_emplace(const_iterator hint, key_type&& key, Args&&... args);
	{
		TOMap mapTO;
		{
			auto result = mapTO.try_emplace(7, 7);
			PRINT_COND_ASSERT((result.second), "Map - Core::Pair< iterator, bool > try_emplace(key_type&& key, Args&&... args)");
			PRINT_COND_ASSERT((result.first->second == mapped_type(7)), "Map - Core::Pair< iterator, bool > try_emplace(key_type&& key, Args&&... args)");
			PRINT_COND_ASSERT((mapTO.size() == 1), "Map - Core::Pair< iterator, bool > try_emplace(key_type&& key, Args&&... args)");
		}
		{
			// verify duplicate not inserted
			auto result = mapTO.try_emplace(7, mapped_type(7));
			PRINT_COND_ASSERT((!result.second), "Map - Core::Pair< iterator, bool > try_emplace(key_type&& key, Args&&... args)");
			PRINT_COND_ASSERT((result.first->second == mapped_type(7)), "Map - Core::Pair< iterator, bool > try_emplace(key_type&& key, Args&&... args)");
			PRINT_COND_ASSERT((mapTO.size() == 1), "Map - Core::Pair< iterator, bool > try_emplace(key_type&& key, Args&&... args)");
		}
		{
			// verify duplicate not inserted
			auto hint = mapTO.find(7);
			auto result = mapTO.try_emplace(hint, 7, 7);
			PRINT_COND_ASSERT((result->first == 7), "Map - iterator try_emplace(const_iterator hint, key_type&& key, Args&&... args)");
			PRINT_COND_ASSERT((result->second == mapped_type(7)), "Map - iterator try_emplace(const_iterator hint, key_type&& key, Args&&... args)");
			PRINT_COND_ASSERT((mapTO.size() == 1), "Map - iterator try_emplace(const_iterator hint, key_type&& key, Args&&... args)");
		}
		{
			// verify duplicate not inserted
			auto hint = mapTO.find(7);
			auto result = mapTO.try_emplace(hint, 7, mapped_type(7));
			PRINT_COND_ASSERT(result->first == 7, "Map - iterator try_emplace(const_iterator hint, key_type&& key, Args&&... args)");
			PRINT_COND_ASSERT(result->second == mapped_type(7), "Map - iterator try_emplace(const_iterator hint, key_type&& key, Args&&... args)");
			PRINT_COND_ASSERT(mapTO.size() == 1, "Map - iterator try_emplace(const_iterator hint, key_type&& key, Args&&... args)");
		}
		{
			auto result = mapTO.try_emplace(8, 8);
			PRINT_COND_ASSERT((result.second), "Map - Core::Pair< iterator, bool > try_emplace(key_type&& key, Args&&... args)");
			PRINT_COND_ASSERT((result.first->second == mapped_type(8)), "Map - Core::Pair< iterator, bool > try_emplace(key_type&& key, Args&&... args)");
			PRINT_COND_ASSERT((mapTO.size() == 2), "Map - Core::Pair< iterator, bool > try_emplace(key_type&& key, Args&&... args)");
		}
		{
			auto result = mapTO.try_emplace(9, mapped_type(9));
			PRINT_COND_ASSERT((result.second), "Map - Core::Pair< iterator, bool > try_emplace(key_type&& key, Args&&... args)");
			PRINT_COND_ASSERT((result.first->second == mapped_type(9)), "Map - Core::Pair< iterator, bool > try_emplace(key_type&& key, Args&&... args)");
			PRINT_COND_ASSERT((mapTO.size() == 3), "Map - Core::Pair< iterator, bool > try_emplace(key_type&& key, Args&&... args)");
		}
	}
	{
		// Core::Pair< iterator, bool > insert_or_assign(const key_type& key, M&& obj);
		// Core::Pair< iterator, bool > insert_or_assign(key_type&& key, M&& obj);
		// iterator insert_or_assign(const_iterator hint, const key_type& key, M&& obj);
		// iterator insert_or_assign(const_iterator hint, key_type&& key, M&& obj);

		TOMap mapTO;
		{
			// initial rvalue insert
			auto result = mapTO.insert_or_assign(3, mapped_type(3));
			PRINT_COND_ASSERT((result.second), "Map - Core::Pair< iterator, bool > insert_or_assign(key_type&& key, M&& obj)");
			PRINT_COND_ASSERT((mapTO.size() == 1), "Map - Core::Pair< iterator, bool > insert_or_assign(key_type&& key, M&& obj)");
			PRINT_COND_ASSERT((result.first->first == 3), "Map - Core::Pair< iterator, bool > insert_or_assign(key_type&& key, M&& obj)");
			PRINT_COND_ASSERT((result.first->second == mapped_type(3)), "Map - Core::Pair< iterator, bool > insert_or_assign(key_type&& key, M&& obj)");
		}
		{
			// verify rvalue assign occurred
			auto result = mapTO.insert_or_assign(3, mapped_type(9));
			PRINT_COND_ASSERT((!result.second), "Map - Core::Pair< iterator, bool > insert_or_assign(key_type&& key, M&& obj)");
			PRINT_COND_ASSERT((mapTO.size() == 1), "Map - Core::Pair< iterator, bool > insert_or_assign(key_type&& key, M&& obj)");
			PRINT_COND_ASSERT((result.first->first == 3), "Map - Core::Pair< iterator, bool > insert_or_assign(key_type&& key, M&& obj)");
			PRINT_COND_ASSERT((result.first->second == mapped_type(9)), "Map - Core::Pair< iterator, bool > insert_or_assign(key_type&& key, M&& obj)");
		}
		{
			// initial lvalue insert
			mapped_type mt5(5);
			auto result = mapTO.insert_or_assign(5, mt5);
			PRINT_COND_ASSERT((result.second), "Map - Core::Pair< iterator, bool > insert_or_assign(key_type&& key, M&& obj)");
			PRINT_COND_ASSERT((mapTO.size() == 2), "Map - Core::Pair< iterator, bool > insert_or_assign(key_type&& key, M&& obj)");
			PRINT_COND_ASSERT((result.first->first == 5), "Map - Core::Pair< iterator, bool > insert_or_assign(key_type&& key, M&& obj)");
			PRINT_COND_ASSERT((result.first->second == mt5), "Map - Core::Pair< iterator, bool > insert_or_assign(key_type&& key, M&& obj)");
		}
		{
			// verify lvalue assign occurred
			mapped_type mt7(7);
			auto result = mapTO.insert_or_assign(5, mt7);
			PRINT_COND_ASSERT((!result.second), "Map - Core::Pair< iterator, bool > insert_or_assign(key_type&& key, M&& obj)");
			PRINT_COND_ASSERT((mapTO.size() == 2), "Map - Core::Pair< iterator, bool > insert_or_assign(key_type&& key, M&& obj)");
			PRINT_COND_ASSERT((result.first->first == 5), "Map - Core::Pair< iterator, bool > insert_or_assign(key_type&& key, M&& obj)");
			PRINT_COND_ASSERT((result.first->second == mt7), "Map - Core::Pair< iterator, bool > insert_or_assign(key_type&& key, M&& obj)");
		}
		{
			// verify lvalue hints
			mapped_type mt6(6);
			auto hint = mapTO.find(5);
			auto result = mapTO.insert_or_assign(hint, 6, mt6);
			PRINT_COND_ASSERT((result != mapTO.end()), "Map - iterator insert_or_assign(const_iterator hint, key_type&& key, M&& obj)");
			PRINT_COND_ASSERT((mapTO.size() == 3), "Map - iterator insert_or_assign(const_iterator hint, key_type&& key, M&& obj)");
			PRINT_COND_ASSERT((result->first == 6), "Map - iterator insert_or_assign(const_iterator hint, key_type&& key, M&& obj)");
			PRINT_COND_ASSERT((result->second == mt6), "Map - iterator insert_or_assign(const_iterator hint, key_type&& key, M&& obj)");
		}
		{
			// verify rvalue hints
			auto hint = mapTO.find(6);
			auto result = mapTO.insert_or_assign(hint, 7, mapped_type(7));
			PRINT_COND_ASSERT((result != mapTO.end()), "Map - iterator insert_or_assign(const_iterator hint, key_type&& key, M&& obj)");
			PRINT_COND_ASSERT((mapTO.size() == 4), "Map - iterator insert_or_assign(const_iterator hint, key_type&& key, M&& obj)");
			PRINT_COND_ASSERT((result->first == 7), "Map - iterator insert_or_assign(const_iterator hint, key_type&& key, M&& obj)");
			PRINT_COND_ASSERT((result->second == mapped_type(7)), "Map - iterator insert_or_assign(const_iterator hint, key_type&& key, M&& obj)");
		}
	}

	PRINT_COND_ASSERT(TestObject::is_clear(), "Map - Failed");
	TestObject::reset();

	return nErrorCount;
}

template < typename HashContainer >
struct HashContainerReserveTest
{
	FXD::S32 operator()()
	{
		FXD::S32 nErrorCount = 0;
		HashContainer hashContainer;

		const typename HashContainer::size_type reserveSizes[] = { 16, 128, 4096, 32768 };
		for (auto& reserveSize : reserveSizes)
		{
			hashContainer.reserve(reserveSize);

			// verify bucket count and hashtable load_factor requirements
			PRINT_COND_ASSERT((hashContainer.bucket_count() >= reserveSize), "HashContainerReserveTest: Failed");
			PRINT_COND_ASSERT((hashContainer.load_factor() <= ceilf(reserveSize / hashContainer.get_max_load_factor())), "HashContainerReserveTest: Failed");
		}
		return nErrorCount;
	}
};
#endif //TESTS_SYSTEM_TESTS_TESTMAP_H