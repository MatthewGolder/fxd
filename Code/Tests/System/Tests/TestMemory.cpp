// Creator - MatthewGolder
#include "Tests/System/Tests/FXDTest.h"
#include "Tests/System/TestHelperClasses.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/Core/Numeric.h"
#include "FXDEngine/Memory/Memory.h"

static FXD::S32 TestUninitializedStorage(void)
{
	FXD::S32 nErrorCount = 0;

	FXD::Core::RandomGen rng(FXD::App::GetRandSeed());

	// inline ForwardItr uninitialized_copy(InputItr first, InputItr last, ForwardItr result)
	{
		{
			FXD::S32* pEnd = FXD::STD::uninitialized_copy((FXD::S32*)nullptr, (FXD::S32*)nullptr, (FXD::S32*)nullptr);
			PRINT_COND_ASSERT(pEnd == nullptr, "Algorithm - inline ForwardItr uninitialized_copy(InputItr first, InputItr last, ForwardItr result)");
		}
		{
			FXD::S32 arrayInt1[] = { 1, 3, 5, 20, 99 };
			FXD::S32 arrayInt2[5];

			FXD::STD::uninitialized_copy(FXD::STD::begin(arrayInt1), FXD::STD::end(arrayInt1), FXD::STD::begin(arrayInt2));
			FXD::S32 nSum1 = FXD::STD::accumulate(FXD::STD::begin(arrayInt1), FXD::STD::end(arrayInt1), 0);
			FXD::S32 nSum2 = FXD::STD::accumulate(FXD::STD::begin(arrayInt2), FXD::STD::end(arrayInt2), 0);

			PRINT_COND_ASSERT(*FXD::STD::begin(arrayInt2) == 1, "Algorithm - inline ForwardItr uninitialized_copy(InputItr first, InputItr last, ForwardItr result)");
			PRINT_COND_ASSERT(*FXD::STD::rbegin(arrayInt2) == 99, "Algorithm - inline ForwardItr uninitialized_copy(InputItr first, InputItr last, ForwardItr result)");
			PRINT_COND_ASSERT( FXD::STD::is_sorted(FXD::STD::begin(arrayInt2), FXD::STD::end(arrayInt2)), "Algorithm - inline ForwardItr uninitialized_copy(InputItr first, InputItr last, ForwardItr result)");
			PRINT_COND_ASSERT( nSum1 == nSum2, "Algorithm - inline ForwardItr uninitialized_copy(InputItr first, InputItr last, ForwardItr result)");
		}
	}

	// inline ForwardItr uninitialized_copy_n(InputItr first, Count nCount, ForwardItr dst)
	{
		{
			FXD::S32* pEnd = FXD::STD::uninitialized_copy_n((FXD::S32*)nullptr, 0, (FXD::S32*)nullptr);
			PRINT_COND_ASSERT(pEnd == nullptr, "Algorithm inline ForwardItr uninitialized_copy_n(InputItr first, Count nCount, ForwardItr dst)");
		}
		{
			FXD::S32 arrayInt1[] = { 1, 3, 5, 20, 99 };
			FXD::S32 arrayInt2[5];

			FXD::STD::uninitialized_copy_n(FXD::STD::begin(arrayInt1), FXD::STD::size(arrayInt1), FXD::STD::begin(arrayInt2));
			FXD::S32 nSum1 = FXD::STD::accumulate(FXD::STD::begin(arrayInt1), FXD::STD::end(arrayInt1), 0);
			FXD::S32 nSum2 = FXD::STD::accumulate(FXD::STD::begin(arrayInt2), FXD::STD::end(arrayInt2), 0);

			PRINT_COND_ASSERT(*FXD::STD::begin(arrayInt2) == 1, "Algorithm - inline ForwardItr uninitialized_copy_n(InputItr first, Count nCount, ForwardItr dst)");
			PRINT_COND_ASSERT(*FXD::STD::rbegin(arrayInt2) == 99, "Algorithm - inline ForwardItr uninitialized_copy_n(InputItr first, Count nCount, ForwardItr dst)");
			PRINT_COND_ASSERT( FXD::STD::is_sorted(FXD::STD::begin(arrayInt2), FXD::STD::end(arrayInt2)), "Algorithm - inline ForwardItr uninitialized_copy_n(InputItr first, Count nCount, ForwardItr dst)");
			PRINT_COND_ASSERT( nSum1 == nSum2, "Algorithm - inline ForwardItr uninitialized_copy_n(InputItr first, Count nCount, ForwardItr dst)");
		}
	}

	// inline void uninitialized_fill(ForwardItr first, ForwardItr last, const T& val)
	{
		{
			FXD::STD::uninitialized_fill((FXD::S32*)nullptr, (FXD::S32*)nullptr, (FXD::S32)0);
		}
		{
			FXD::Core::String8 str(11, ' ');
			FXD::STD::uninitialized_fill(str.data(), str.data() + 11, 'c');
			PRINT_COND_ASSERT(FXD::STD::all_of(str.begin(), str.end(), [](FXD::Core::String8::value_type ch) { return ch == 'c'; }), "Algorithm - inline void uninitialized_fill(ForwardItr first, ForwardItr last, const T& val)");
		}
	}

	// inline void uninitialized_fill_n(ForwardItr first, Count nCount, const T& val)
	{
		{
			FXD::STD::uninitialized_fill_n((FXD::S32*)nullptr, 0, (FXD::S32)0);
		}
		{
			FXD::Core::String8 str(11, ' ');
			FXD::STD::uninitialized_fill_n(str.data(), 11, 'c');
			PRINT_COND_ASSERT(FXD::STD::all_of(str.begin(), str.end(), [](FXD::Core::String8::value_type ch) { return ch == 'c'; }), "Algorithm - inline void uninitialized_fill_n(ForwardItr first, Count nCount, const T& val)");
		}
	}

	// uninitialized_move TODO
	{
		{
			FXD::S32* pEnd = FXD::STD::uninitialized_move((FXD::S32*)nullptr, (FXD::S32*)nullptr, (FXD::S32*)nullptr);
			PRINT_COND_ASSERT(pEnd == nullptr, "Algorithm - inline ForwardItr uninitialized_copy(InputItr first, InputItr last, ForwardItr result)");
		}
		{
			FXD::S32 arrayInt1[] = { 1, 3, 5, 20, 99 };
			FXD::S32 arrayInt2[5];

			FXD::STD::uninitialized_move(FXD::STD::begin(arrayInt1), FXD::STD::end(arrayInt1), FXD::STD::begin(arrayInt2));
			FXD::S32 nSum1 = FXD::STD::accumulate(FXD::STD::begin(arrayInt1), FXD::STD::end(arrayInt1), 0);
			FXD::S32 nSum2 = FXD::STD::accumulate(FXD::STD::begin(arrayInt2), FXD::STD::end(arrayInt2), 0);

			PRINT_COND_ASSERT(*FXD::STD::begin(arrayInt2) == 1, "Algorithm - inline ForwardItr uninitialized_copy(InputItr first, InputItr last, ForwardItr result)");
			PRINT_COND_ASSERT(*FXD::STD::rbegin(arrayInt2) == 99, "Algorithm - inline ForwardItr uninitialized_copy(InputItr first, InputItr last, ForwardItr result)");
			PRINT_COND_ASSERT(FXD::STD::is_sorted(FXD::STD::begin(arrayInt2), FXD::STD::end(arrayInt2)), "Algorithm - inline ForwardItr uninitialized_copy(InputItr first, InputItr last, ForwardItr result)");
			PRINT_COND_ASSERT(nSum1 == nSum2, "Algorithm - inline ForwardItr uninitialized_copy(InputItr first, InputItr last, ForwardItr result)");
		}
	}

	// uninitialized_move_n TODO
	{
	}

	// uninitialized_default_construct TODO
	{
	}

	// uninitialized_default_construct_m TODO
	{
	}

	// uninitialized_value_construct TODO
	{
	}

	// uninitialized_value_construct_n TODO
	{
	}

	// destroy_at TODO
	{
	}

	// destroy TODO
	{
	}

	// destroy_n TODO
	{
	}

	return nErrorCount;
}

FXD::S32 TestMemory(void)
{
	FXD::S32 nErrorCount = 0;

	{
		nErrorCount += TestUninitializedStorage();
	}

	return nErrorCount;
}