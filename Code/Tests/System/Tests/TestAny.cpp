// Creator - MatthewGolder
#include "Tests/System/Tests/FXDTest.h"
#include "FXDEngine/Container/Any.h"
#include "FXDEngine/Container/Vector.h"
#include "FXDEngine/Core/Numeric.h"
#include "FXDEngine/Core/String.h"

namespace
{
	class SmallTestObject
	{
	public:
		SmallTestObject(void) NOEXCEPT
		{
			m_nCtorCount++;
		}
		SmallTestObject(const SmallTestObject& /*rhs*/) NOEXCEPT
		{
			m_nCtorCount++;
		}
		SmallTestObject(SmallTestObject&& /*rhs*/) NOEXCEPT
		{
			m_nCtorCount++;
		}
		SmallTestObject& operator=(const SmallTestObject& /*rhs*/) NOEXCEPT
		{
			m_nCtorCount++;
			return (*this);
		}
		~SmallTestObject(void) NOEXCEPT
		{
			m_nCtorCount--;
		}

		static void reset(void)
		{
			m_nCtorCount = 0;
		}
		static bool is_clear(void)
		{
			return m_nCtorCount == 0;
		}

	public:
		static FXD::S32 m_nCtorCount;
	};

	FXD::S32 SmallTestObject::m_nCtorCount = 0;

	struct RequiresInitList
	{
		RequiresInitList(std::initializer_list< FXD::S32 > iList)
			: m_nSum(FXD::STD::accumulate(FXD::STD::begin(iList), FXD::STD::end(iList), 0))
		{}

		FXD::S32 m_nSum;
	};
}


FXD::S32 TestAny(void)
{
	FXD::S32 nErrorCount = 0;

	// EXPLICIT FXD::Container::Any(void)
	{ {
			FXD::Container::Any any;
			PRINT_COND_ASSERT(any.has_value() == false, "Any - Any(void)");
		}
		{
			TestObject::reset();
			{
				FXD::Container::Any any(TestObject());
			}
			PRINT_COND_ASSERT(TestObject::is_clear(), "Any - Any(void)");
		}
		{
			SmallTestObject::reset();
			{
				FXD::Container::Any any(SmallTestObject());
			}
			PRINT_COND_ASSERT(TestObject::is_clear(), "Any - Any(void)");
		}
	}

	// template < typename T >
	// FXD::Container::Any(T&& val)

	// template < typename T >
	// FXD::Container::& operator=(T&& val)
	{
		FXD::Container::Any any(42);
		PRINT_COND_ASSERT(any.has_value() == true, "Any - Any(T&& val);");
		PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(any) == 42, "Any - Any(T&& val);");
		PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(any) != 1337, "Any - Any(T&& val);");

		FXD::STD::any_cast< FXD::S32& >(any) = 10;
		PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(any) == 10, "Any - Any(T&& val);");

		any = 1.0f;
		FXD::STD::any_cast< FXD::F32& >(any) = 1337.0f;
		PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::F32 >(any) == 1337.0f, "Any - Any(T&& val);");

		any = 4343;
		PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(any) == 4343, "Any - Any(T&& val);");

		any = FXD::Core::String8("hello world");
		PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::Core::String8 >(any) == "hello world", "Any - Any(T&& val);");
		PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::Core::String8& >(any) == "hello world", "Any - Any(T&& val);");
	}

	// template < typename T >
	// FXD::Container::Any(T&& val)
	{
		struct custom_type
		{
			FXD::S32 nData;
		};

		FXD::Container::Any any = custom_type{};
		FXD::STD::any_cast< custom_type& >(any).nData = 42;
		PRINT_COND_ASSERT(FXD::STD::any_cast< custom_type >(any).nData == 42, "");
	}

	// template < typename T >
	// FXD::Container::Any(T&& val)
	{
		{
			FXD::Container::Vector< FXD::Container::Any > vecAny = {'a', 42, 5555ll, 3333u, 5555ull, 42.0f, 6666.0L};

			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::UTF8 >(vecAny[0]) == 'a', "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(vecAny[1]) == 42, "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S64 >(vecAny[2]) == 5555ll, "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::U32 >(vecAny[3]) == 3333u, "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::U64 >(vecAny[4]) == 5555ull, "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::F32 >(vecAny[5]) == 42.0f, "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::F64 >(vecAny[6]) == 6666.0L, "");
		}

		{
			FXD::Container::Any any(FXD::Core::String8("test string"));
			PRINT_COND_ASSERT(any.has_value(), "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::Core::String8 >(any) == "test string", "");
		}

		{
			FXD::Container::Vector< FXD::Container::Any > vecAny = { 42, 42.0f, 6666.0L, 'a', FXD::Core::String8("rob") };
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(vecAny[0]) == 42, "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::F32 >(vecAny[1]) == 42.0f, "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::F64 >(vecAny[2]) == 6666.0L, "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::UTF8 >(vecAny[3]) == 'a', "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::Core::String8 >(vecAny[4]) == "rob", "");
		}

		{
			FXD::Container::Vector< FXD::Container::Any > vecAny;
			vecAny.push_back(42);
			vecAny.push_back(42.0f);
			vecAny.push_back('a');
			vecAny.push_back(FXD::Core::String8("rob"));

			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(vecAny[0]) == 42, "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::F32 >(vecAny[1]) == 42.0f, "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::UTF8 >(vecAny[2]) == 'a', "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::Core::String8 >(vecAny[3]) == "rob", "");
		}

		{
			TestObject::reset();
			{
				FXD::Container::Vector< FXD::Container::Any > vecAny = {42, 3333u, 5555ll, 5555ull, 42.0f, 6666.0L, 'a'};

				PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(vecAny[0]) == 42, "");
				PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::U32 >(vecAny[1]) == 3333u, "");
				PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S64 >(vecAny[2]) == 5555ll, "");
				PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::U64 >(vecAny[3]) == 5555ull, "");
				PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::F32 >(vecAny[4]) == 42.0f, "");
				PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::F64 >(vecAny[5]) == 6666.0L, "");
				PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::UTF8 >(vecAny[6]) == 'a', "");

				vecAny[1] = TestObject(3333); // replace a small integral with a large heap allocated object.
			
				PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(vecAny[0]) == 42, "");
				PRINT_COND_ASSERT(FXD::STD::any_cast< TestObject >(vecAny[1]).m_nX == 3333, ""); // not 3333u because TestObject ctor takes a signed type.
				PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S64 >(vecAny[2]) == 5555ll, "");
				PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::U64 >(vecAny[3]) == 5555ull, "");
				PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::F32 >(vecAny[4]) == 42.0f, "");
				PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::F64 >(vecAny[5]) == 6666.0L, "");
				PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::UTF8 >(vecAny[6]) == 'a', "");

			}
			PRINT_COND_ASSERT(TestObject::is_clear(), "");
		}
	}

	// void reset(void) NOEXCEPT
	{
		FXD::Container::Any any(FXD::Core::String8("test string"));
		PRINT_COND_ASSERT(any.has_value(), "");
		any.reset();
		PRINT_COND_ASSERT(!any.has_value(), "");
	}

	// FXD::Container::Any(const Any& rhs)
	{
		FXD::Container::Any any1 = 42;
		FXD::Container::Any any2 = any1;

		PRINT_COND_ASSERT(any1.has_value(), "");
		PRINT_COND_ASSERT(any2.has_value(), "");
		PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(any1) == FXD::STD::any_cast< FXD::S32 >(any2), "");
	}

	// template < typename T >
	// FXD::Container::& operator=(T&& val)
	{
		{
			FXD::Container::Any any1;
			PRINT_COND_ASSERT(!any1.has_value(), "");
			{
				FXD::Container::Any any2(FXD::Core::String8("test string"));
				any1 = FXD::STD::any_cast< FXD::Core::String8 >(any2);

				PRINT_COND_ASSERT(any1.has_value(), "");
			}
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::Core::String8 >(any1) == "test string", "");
			PRINT_COND_ASSERT(any1.has_value(), "");
		}

		{
			FXD::Container::Any any1;
			PRINT_COND_ASSERT(!any1.has_value(), "");
			{
				FXD::Container::Any any2(FXD::Core::String8("test string"));
				any1 = any2;
				PRINT_COND_ASSERT(any1.has_value(), "");
			}
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::Core::String8& >(any1) == "test string", "");
			PRINT_COND_ASSERT(any1.has_value(), "");;
		}
	}

	// void swap(Any& rhs) NOEXCEPT
	{
		{
			FXD::Container::Any any1 = 42;
			FXD::Container::Any any2 = 24;
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(any1) == 42, "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(any2) == 24, "");

			any1.swap(any2);
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(any1) == 24, "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(any2) == 42, "");

			FXD::STD::swap(any1, any2);
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(any1) == 42, "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(any2) == 24, "");
		}
		{
			FXD::Container::Any any1 = FXD::Core::String8("hello");
			FXD::Container::Any any2 = FXD::Core::String8("world");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::Core::String8 >(any1) == "hello", "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::Core::String8 >(any2) == "world", "");

			any1.swap(any2);
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::Core::String8 >(any1) == "world", "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::Core::String8 >(any2) == "hello", "");

			FXD::STD::swap(any1, any2);
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::Core::String8 >(any1) == "hello", "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::Core::String8 >(any2) == "world", "");
		}
	}

	// const std::type_info& type(void) const NOEXCEPT
	{
		PRINT_COND_ASSERT(FXD::Core::CharTraits< FXD::UTF8 >::compare(FXD::Container::Any(42).type().name(), "int") == 0, "");
		PRINT_COND_ASSERT(FXD::Core::CharTraits< FXD::UTF8 >::compare(FXD::Container::Any(42.0f).type().name(), "float") == 0, "");
		PRINT_COND_ASSERT(FXD::Core::CharTraits< FXD::UTF8 >::compare(FXD::Container::Any(42u).type().name(), "unsigned int") == 0, "");
		PRINT_COND_ASSERT(FXD::Core::CharTraits< FXD::UTF8 >::compare(FXD::Container::Any(42ul).type().name(), "unsigned long") == 0, "");
		PRINT_COND_ASSERT(FXD::Core::CharTraits< FXD::UTF8 >::compare(FXD::Container::Any(42l).type().name(), "long") == 0, "");
	}

	// template < typename T, typename... Args >
	// void emplace(Args&&... args)
#if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED
	{
		{
			FXD::Container::Any any;

			any.emplace< FXD::S32 >(42);
			PRINT_COND_ASSERT(any.has_value(), "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(any) == 42, "");

			any.emplace< FXD::S16 >((FXD::S16)8); // no way to define a short literal we must cast here.
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S16 >(any) == 8, "");
			PRINT_COND_ASSERT(any.has_value(), "");

			any.reset();
			PRINT_COND_ASSERT(!any.has_value(), "");
		}
		{
			TestObject::reset();
			{
				FXD::Container::Any any;
				any.emplace< TestObject >();
				PRINT_COND_ASSERT(any.has_value(), "");
			}
			PRINT_COND_ASSERT(TestObject::is_clear(), "");
		}
	}

	// template < typename T, typename U, typename... Args >
	// FXD::Container::decay_t< T >& emplace(std::initializer_list< U > il, Args&&... args);
	{
		{
			FXD::Container::Any any;
			any.emplace< RequiresInitList >(std::initializer_list< FXD::S32 >{1, 2, 3, 4, 5, 6});

			PRINT_COND_ASSERT(any.has_value(), "");
			PRINT_COND_ASSERT(FXD::STD::any_cast< RequiresInitList >(any).m_nSum == 21, "");
		}
	}
#endif

	// equivalence tests
	{
		FXD::Container::Any any1;
		FXD::Container::Any any2;
		PRINT_COND_ASSERT(!any1.has_value() == !any2.has_value(), "");

		any1 = 42;
		any2 = 24;
		PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(any1) != FXD::STD::any_cast< FXD::S32 >(any2), "");
		PRINT_COND_ASSERT(any1.has_value() == any2.has_value(), "");

		any1 = 42;
		any2 = 42;
		PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(any1) == FXD::STD::any_cast< FXD::S32 >(any2), "");
		PRINT_COND_ASSERT(any1.has_value() == any2.has_value(), "");
	}

	// move tests
	{
		FXD::Container::Any a = FXD::Core::String8("hello world");
		PRINT_COND_ASSERT(FXD::STD::any_cast<FXD::Core::String8&>(a) == "hello world", "");

		auto s = std::move(FXD::STD::any_cast<FXD::Core::String8&>(a)); // move string out
		PRINT_COND_ASSERT(s == "hello world", "");
		PRINT_COND_ASSERT(FXD::STD::any_cast<FXD::Core::String8&>(a).empty(), "");

		FXD::STD::any_cast<FXD::Core::String8&>(a) = std::move(s); // move string in
		PRINT_COND_ASSERT(FXD::STD::any_cast<FXD::Core::String8&>(a) == "hello world", "");
	}

	// nullptr tests
	{
		FXD::Container::Any* pAny = nullptr;
		PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S16 >(pAny) == nullptr, "");
		PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(pAny) == nullptr, "");
		PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S64 >(pAny) == nullptr, "");
		PRINT_COND_ASSERT(FXD::STD::any_cast<FXD::Core::String8>(pAny) == nullptr, "");

		FXD::Container::Any any;
		PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S16 >(&any) == nullptr, "");
		PRINT_COND_ASSERT(FXD::STD::any_cast< const FXD::S16 >(&any) == nullptr, "");
		PRINT_COND_ASSERT(FXD::STD::any_cast< volatile FXD::S16 >(&any) == nullptr, "");
		PRINT_COND_ASSERT(FXD::STD::any_cast< const volatile FXD::S16 >(&any) == nullptr, "");

		PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S16 *>(&any) == nullptr, "");
		PRINT_COND_ASSERT(FXD::STD::any_cast< const FXD::S16* >(&any) == nullptr, "");
		PRINT_COND_ASSERT(FXD::STD::any_cast< volatile FXD::S16* >(&any) == nullptr, "");
		PRINT_COND_ASSERT(FXD::STD::any_cast< const volatile FXD::S16* >(&any) == nullptr, "");
	}

	// make_any
	{
		{
			auto a = FXD::STD::make_any< FXD::S32 >(42);
			PRINT_COND_ASSERT(FXD::STD::any_cast< FXD::S32 >(a) == 42, "");
		}

		{
			auto a = FXD::STD::make_any< RequiresInitList >(std::initializer_list< FXD::S32 >{1, 2, 3, 4, 5, 6, 7, 8});
			PRINT_COND_ASSERT(FXD::STD::any_cast< RequiresInitList&>(a).m_nSum == 36, "");
		}
	}

	return nErrorCount;
}