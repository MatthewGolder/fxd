// Creator - MatthewGolder
#include "Tests/System/Tests/FXDTest.h"
#include "Tests/System/TestHelperClasses.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/Core/Numeric.h"
#include "FXDEngine/Container/Array.h"
#include <string>

static FXD::S32 TestNumericOperations(void)
{
	FXD::S32 nErrorCount = 0;

	FXD::Core::RandomGen rng(FXD::App::GetRandSeed());

	// inline void iota(ForwardItr first, ForwardItr last, T val)
	{
		FXD::Container::LinkedList< FXD::S32 > listInt(10);
		FXD::STD::iota(listInt.begin(), listInt.end(), -4);

		FXD::Container::Vector< FXD::Container::LinkedList< FXD::S32 >::iterator > vecInt(listInt.size());
		FXD::STD::iota(vecInt.begin(), vecInt.end(), listInt.begin());

		PRINT_COND_ASSERT(FXD::STD::is_sorted(listInt.begin(), listInt.end()), "Algorithm - inline void iota(ForwardItr first, ForwardItr last, T val)");
		PRINT_COND_ASSERT(*(*vecInt.begin()) == -4, "Algorithm - inline void iota(ForwardItr first, ForwardItr last, T val)");
		PRINT_COND_ASSERT(*(*vecInt.rbegin()) == 5, "Algorithm - inline void iota(ForwardItr first, ForwardItr last, T val)");
	}

	// inline T accumulate(InputItr first, InputItr last, T val, Predicate pred)
	// inline T accumulate(InputItr first, InputItr last, T val)
	{
		/*CLANGG
		FXD::Container::Vector< FXD::S32 > vecInt{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		FXD::S32 nSum = FXD::STD::accumulate(vecInt.begin(), vecInt.end(), 0);
		FXD::S32 nProd = FXD::STD::accumulate(vecInt.begin(), vecInt.end(), 1, FXD::STD::multiplies< FXD::S32 >());

		auto dashFold = [](std::string str, FXD::S32 nVal) { return std::move(str) + '-' + std::to_string(nVal); };

		std::string str = FXD::STD::accumulate(FXD::STD::next(vecInt.begin()), vecInt.end(), std::to_string(vecInt[0]), dashFold);
		std::string rStr = FXD::STD::accumulate(FXD::STD::next(vecInt.rbegin()), vecInt.rend(), std::to_string(vecInt.back()), dashFold);

		PRINT_COND_ASSERT(nSum == 55, "Algorithm - inline T accumulate(InputItr first, InputItr last, T val)");
		PRINT_COND_ASSERT(nProd == 3628800, "Algorithm - inline T accumulate(InputItr first, InputItr last, T val, Predicate pred)");
		PRINT_COND_ASSERT(str == "1-2-3-4-5-6-7-8-9-10", "Algorithm - inline T accumulate(InputItr first, InputItr last, T val, Predicate pred)");
		PRINT_COND_ASSERT(rStr == "10-9-8-7-6-5-4-3-2-1", "Algorithm - inline T accumulate(InputItr first, InputItr last, T val, Predicate pred)");
		*/
	}

	// inline T inner_product(InputItr1 first1, InputItr1 last1, InputItr2 first2, T val, Predicate1 pred1, Predicate2 pred2)
	// inline T inner_product(InputItr1 first1, InputItr1 last1, InputItr2 first2, T val)
	{
		FXD::Container::Vector< FXD::S32 > vecInt1{ 0, 1, 2, 3, 4 };
		FXD::Container::Vector< FXD::S32 > vecInt2{ 5, 4, 2, 3, 1 };

		FXD::S32 nVal1 = FXD::STD::inner_product(vecInt1.begin(), vecInt1.end(), vecInt2.begin(), 0);
		FXD::S32 nVal2 = FXD::STD::inner_product(vecInt1.begin(), vecInt1.end(), vecInt2.begin(), 0, FXD::STD::plus<>(), FXD::STD::equal_to<>());

		PRINT_COND_ASSERT(nVal1 == 21, "Algorithm - inline T inner_product(InputItr1 first1, InputItr1 last1, InputItr2 first2, T val)");
		PRINT_COND_ASSERT(nVal2 == 2, "Algorithm - inline T inner_product(InputItr1 first1, InputItr1 last1, InputItr2 first2, T val, Predicate1 pred1, Predicate2 pred2)");
	}

	// inline OutputItr adjacent_difference(InputItr first, InputItr last, OutputItr dst, Predicate pred)
	// inline OutputItr adjacent_difference(InputItSr first, InputItr last, OutputItr dst)
	{
		FXD::Container::Vector< FXD::S32 > vecInt{ 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 };
		FXD::STD::adjacent_difference(vecInt.begin(), vecInt.end(), vecInt.begin());

		FXD::Container::Array< FXD::S32, 10 > arrayInt{ 1 };
		FXD::STD::adjacent_difference(arrayInt.begin(), FXD::STD::prev(arrayInt.end()), FXD::STD::next(arrayInt.begin()), FXD::STD::plus<>());

		PRINT_COND_ASSERT(*vecInt.begin() == 2, "Algorithm - inline OutputItr adjacent_difference(InputItSr first, InputItr last, OutputItr dst)");
		PRINT_COND_ASSERT(FXD::STD::equal(vecInt.begin() + 1, vecInt.end(), vecInt.begin()), "Algorithm - inline OutputItr adjacent_difference(InputItSr first, InputItr last, OutputItr dst)");

		PRINT_COND_ASSERT(*arrayInt.begin() == 1, "Algorithm - inline OutputItr adjacent_difference(InputItr first, InputItr last, OutputItr dst, Predicate pred)");
		PRINT_COND_ASSERT(*arrayInt.rbegin() == 55, "Algorithm - inline OutputItr adjacent_difference(InputItr first, InputItr last, OutputItr dst, Predicate pred)");
		PRINT_COND_ASSERT(FXD::STD::is_sorted(arrayInt.begin(), arrayInt.end()), "Algorithm - inline OutputItr adjacent_difference(InputItr first, InputItr last, OutputItr dst, Predicate pred)");
	}

	// inline OutputItr partial_sum(InputItr first, InputItr last, OutputItr dst, Predicate pred)
	// inline OutputItr partial_sum(InputItr first, InputItr last, OutputItr dst)
	{
		FXD::Container::Vector< FXD::S32 > vecInt1{ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 };
		FXD::Container::Vector< FXD::S32 > vecInt2(10);

		FXD::STD::partial_sum(vecInt1.begin(), vecInt1.end(), vecInt2.begin());
		FXD::STD::partial_sum(vecInt1.begin(), vecInt1.end(), vecInt1.begin(), FXD::STD::multiplies< FXD::S32 >());

		PRINT_COND_ASSERT(*vecInt2.begin() == 2, "Algorithm - inline OutputItr partial_sum(InputItr first, InputItr last, OutputItr dst)");
		PRINT_COND_ASSERT(*vecInt2.rbegin() == 20, "Algorithm - inline OutputItr partial_sum(InputItr first, InputItr last, OutputItr dst)");
		PRINT_COND_ASSERT(FXD::STD::is_sorted(vecInt2.begin(), vecInt2.end()), "Algorithm - inline OutputItr partial_sum(InputItr first, InputItr last, OutputItr dst)");

		PRINT_COND_ASSERT(*vecInt1.begin() == 2, "Algorithm - inline OutputItr partial_sum(InputItr first, InputItr last, OutputItr dst, Predicate pred)");
		PRINT_COND_ASSERT(*vecInt1.rbegin() == 1024, "Algorithm - inline OutputItr partial_sum(InputItr first, InputItr last, OutputItr dst, Predicate pred)");
		PRINT_COND_ASSERT(FXD::STD::is_sorted(vecInt1.begin(), vecInt1.end()), "Algorithm - inline OutputItr partial_sum(InputItr first, InputItr last, OutputItr dst, Predicate pred)");
	}

	// inline T reduce(InputItr first, InputItr last, T val, Predicate pred)
	// inline T reduce(InputItr first, InputItr last, T val)
	{
		/*CLANGG
		FXD::Container::Vector< FXD::S32 > vecInt{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		FXD::S32 nSum = FXD::STD::reduce(vecInt.begin(), vecInt.end(), 0);
		FXD::S32 nProd = FXD::STD::reduce(vecInt.begin(), vecInt.end(), 1, FXD::STD::multiplies< FXD::S32 >());

		auto dashFold = [](std::string str, FXD::S32 nVal)
		{
			return std::move(str) + '-' + std::to_string(nVal);
		};
		std::string str = FXD::STD::reduce(FXD::STD::next(vecInt.begin()), vecInt.end(), std::to_string(vecInt[0]), dashFold);
		std::string rStr = FXD::STD::reduce(FXD::STD::next(vecInt.rbegin()), vecInt.rend(), std::to_string(vecInt.back()), dashFold);

		PRINT_COND_ASSERT(nSum == 55, "Algorithm - inline T reduce(InputItr first, InputItr last, T val)");
		PRINT_COND_ASSERT(nProd == 3628800, "Algorithm - inline T reduce(InputItr first, InputItr last, T val, Predicate pred)");
		PRINT_COND_ASSERT(str == "1-2-3-4-5-6-7-8-9-10", "Algorithm - inline T reduce(InputItr first, InputItr last, T val, Predicate pred)");
		PRINT_COND_ASSERT(rStr == "10-9-8-7-6-5-4-3-2-1", "Algorithm - inline T reduce(InputItr first, InputItr last, T val, Predicate pred)");
		*/
	}

	// inline OutputItr exclusive_scan(InputItr first, InputItr last, OutputItr dst, T val, Predicate pred)
	// inline OutputItr exclusive_scan(InputItr first, InputItr last, OutputItr dst, T val)
	{
		FXD::Container::Vector< FXD::S32 > vecInt1{ 3, 1, 4, 1, 5, 9, 2, 6 };
		FXD::Container::Vector< FXD::S32 > vecInt2(8);

		FXD::STD::exclusive_scan(vecInt1.begin(), vecInt1.end(), vecInt2.begin(), 0);
		PRINT_COND_ASSERT(*vecInt2.begin() == 0, "Algorithm - inline OutputItr exclusive_scan(InputItr first, InputItr last, OutputItr dst, T val)");
		PRINT_COND_ASSERT(*vecInt2.rbegin() == 25, "Algorithm - inline OutputItr exclusive_scan(InputItr first, InputItr last, OutputItr dst, T val)");
		PRINT_COND_ASSERT(FXD::STD::is_sorted(vecInt2.begin(), vecInt2.end()), "Algorithm - inline OutputItr exclusive_scan(InputItr first, InputItr last, OutputItr dst, T val)");

		FXD::STD::exclusive_scan(vecInt1.begin(), vecInt1.end(), vecInt2.begin(), 1, FXD::STD::multiplies<>());
		PRINT_COND_ASSERT(*vecInt2.begin() == 1, "Algorithm - inline OutputItr exclusive_scan(InputItr first, InputItr last, OutputItr dst, T val, Predicate pred)");
		PRINT_COND_ASSERT(*vecInt2.rbegin() == 1080, "Algorithm - inline OutputItr exclusive_scan(InputItr first, InputItr last, OutputItr dst, T val, Predicate pred)");
		PRINT_COND_ASSERT(FXD::STD::is_sorted(vecInt2.begin(), vecInt2.end()), "Algorithm - inline OutputItr exclusive_scan(InputItr first, InputItr last, OutputItr dst, T val, Predicate pred)");
	}

	// inline OutputItr inclusive_scan(InputItr first, InputItr last, OutputItr dst, Predicate pred)
	// inline OutputItr inclusive_scan(InputItr first, InputItr last, OutputItr dst)
	{
		FXD::Container::Vector< FXD::S32 > vecInt1{ 3, 1, 4, 1, 5, 9, 2, 6 };
		FXD::Container::Vector< FXD::S32 > vecInt2(8);

		FXD::STD::inclusive_scan(vecInt1.begin(), vecInt1.end(), vecInt2.begin());
		PRINT_COND_ASSERT(*vecInt2.begin() == 3, "Algorithm - inline OutputItr inclusive_scan(InputItr first, InputItr last, OutputItr dst)");
		PRINT_COND_ASSERT(*vecInt2.rbegin() == 31, "Algorithm - inline OutputItr inclusive_scan(InputItr first, InputItr last, OutputItr dst)");
		PRINT_COND_ASSERT(FXD::STD::is_sorted(vecInt2.begin(), vecInt2.end()), "Algorithm - inline OutputItr inclusive_scan(InputItr first, InputItr last, OutputItr dst)");

		FXD::STD::inclusive_scan(vecInt1.begin(), vecInt1.end(), vecInt2.begin(), FXD::STD::multiplies<>());
		PRINT_COND_ASSERT(*vecInt2.begin() == 3, "Algorithm - inline OutputItr inclusive_scan(InputItr first, InputItr last, OutputItr dst, Predicate pred)");
		PRINT_COND_ASSERT(*vecInt2.rbegin() == 6480, "Algorithm - inline OutputItr inclusive_scan(InputItr first, InputItr last, OutputItr dst, Predicate pred)");
		PRINT_COND_ASSERT(FXD::STD::is_sorted(vecInt2.begin(), vecInt2.end()), "Algorithm - inline OutputItr inclusive_scan(InputItr first, InputItr last, OutputItr dst, Predicate pred)");
	}

	// inline T transform_reduce(InputItr first, InputItr last, T val, Predicate1 pred1, Predicate2 pred2)
	// inline T transform_reduce(InputItr1 first1, InputItr1 last, InputItr2 first2, T val, Predicate1 pred1, Predicate2 pred2)
	// inline T transform_reduce(InputItr1 first1, InputItr1 last1, InputItr2 first2, T val)
	{
		FXD::Container::Vector< FXD::F32 > vecFloat1(10007, 1.0f);
		FXD::Container::Vector< FXD::F32 > vecFloat2(10007, 1.0f);

		FXD::F32 fRes = FXD::STD::transform_reduce(vecFloat1.begin(), vecFloat1.end(), vecFloat2.begin(), 0.0f);
		PRINT_COND_ASSERT(fRes == 10007, "Algorithm - inline T transform_reduce(InputItr1 first1, InputItr1 last1, InputItr2 first2, T val)");

		FXD::Container::Vector< FXD::S32 > vecInt{ 1, 2, 3, 4, 5 };
		FXD::S32 nRes = FXD::STD::transform_reduce(vecInt.begin(), vecInt.end(), 0, [](FXD::S32 a, FXD::S32 b) { return a + b; }, [](FXD::S32 a) { return a * a; });
		PRINT_COND_ASSERT(nRes == 55, "Algorithm - inline T transform_reduce(InputItr first, InputItr last, T val, Predicate1 pred1, Predicate2 pred2)");
	}

	// inline OutputItr transform_exclusive_scan(InputItr first, InputItr last, OutputItr dst, T val, Predicate1 pred1, Predicate2 pred2)
	{
		auto times10 = [](FXD::S32 nVal) { return nVal * 10; };

		FXD::Container::Vector< FXD::S32 > vecInt1{ 3, 1, 4, 1, 5, 9, 2, 6 };
		FXD::Container::Vector< FXD::S32 > vecInt2(8);

		FXD::STD::transform_exclusive_scan(vecInt1.begin(), vecInt1.end(), vecInt2.begin(), 0, FXD::STD::plus< FXD::S32 >(), times10);
		PRINT_COND_ASSERT(*vecInt2.begin() == 0, "Algorithm - inline OutputItr transform_exclusive_scan(InputItr first, InputItr last, OutputItr dst, T val, Predicate1 pred1, Predicate2 pred2)");
		PRINT_COND_ASSERT(*vecInt2.rbegin() == 250, "Algorithm - inline OutputItr transform_exclusive_scan(InputItr first, InputItr last, OutputItr dst, T val, Predicate1 pred1, Predicate2 pred2)");
		PRINT_COND_ASSERT(FXD::STD::is_sorted(vecInt2.begin(), vecInt2.end()), "Algorithm - inline OutputItr inclusive_scan(InputItr first, InputItr last, OutputItr dst, Predicate pred)");
	}

	// inline OutputItr transform_inclusive_scan(InputItr first, InputItr last, OutputItr dst, Predicate1 pred1, Predicate2 pred2, T val)
	// inline OutputItr transform_inclusive_scan(InputItr first, InputItr last, OutputItr dst, Predicate1 pred1, Predicate2 pred2)
	{
		auto times10 = [](FXD::S32 nVal) { return nVal * 10; };

		FXD::Container::Vector< FXD::S32 > vecInt1{ 3, 1, 4, 1, 5, 9, 2, 6 };
		FXD::Container::Vector< FXD::S32 > vecInt2(8);

		FXD::STD::transform_inclusive_scan(vecInt1.begin(), vecInt1.end(), vecInt2.begin(), FXD::STD::plus< FXD::S32 >(), times10);
		PRINT_COND_ASSERT(*vecInt2.begin() == 30, "Algorithm - inline OutputItr transform_exclusive_scan(InputItr first, InputItr last, OutputItr dst, T val, Predicate1 pred1, Predicate2 pred2)");
		PRINT_COND_ASSERT(*vecInt2.rbegin() == 310, "Algorithm - inline OutputItr transform_exclusive_scan(InputItr first, InputItr last, OutputItr dst, T val, Predicate1 pred1, Predicate2 pred2)");
		PRINT_COND_ASSERT(FXD::STD::is_sorted(vecInt2.begin(), vecInt2.end()), "Algorithm - inline OutputItr inclusive_scan(InputItr first, InputItr last, OutputItr dst, Predicate pred)");
	}

	return nErrorCount;
}

FXD::S32 Testnumeric(void)
{
	FXD::S32 nErrorCount = 0;

	{
		nErrorCount += TestNumericOperations();
	}

	return nErrorCount;
}