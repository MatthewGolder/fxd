// Creator - MatthewGolder
#ifndef TESTS_SYSTEM_TESTS_TESTSET_H
#define TESTS_SYSTEM_TESTS_TESTSET_H

#include "Tests/System/Tests/FXDTest.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/Container/Vector.h"
#include "FXDEngine/Core/Algorithm.h"
#include "FXDEngine/Core/Random.h"
#include "FXDEngine/Core/Utility.h"

///////////////////////////////////////////////////////////////////////////////
// TestSetConstruction
//
// This test compares FXD::Container::Set/multiset to std::set/multiset.
//
// Requires a container that can hold at least 1000 items.
///////////////////////////////////////////////////////////////////////////////
template < typename T1, typename T2, bool bMultiset >
FXD::S32 TestSetConstruction(void)
{
	FXD::S32 nErrorCount = 0;
	TestObject::reset();
	{
		std::unique_ptr< T1 > pt1A = std::make_unique< T1 >();
		std::unique_ptr< T2 > pt2A = std::make_unique< T2 >();
		T1& t1A = *pt1A;
		T2& t2A = *pt2A;
		nErrorCount += CompareContainers(t1A, t2A, "Set - Set(void)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());
		PRINT_COND_ASSERT(t1A.validate(), "Set - Set(void)");

		std::unique_ptr< T1 > pt1B = std::make_unique< T1 >();
		std::unique_ptr< T2 > pt2B = std::make_unique< T2 >();
		T1& t1B = *pt1B;
		T2& t2B = *pt2B;
		nErrorCount += CompareContainers(t1B, t2B, "Set - Set(void)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());

		std::unique_ptr< T1 > pt1C = std::make_unique< T1 >();
		std::unique_ptr< T2 > pt2C = std::make_unique< T2 >();
		T1& t1C = *pt1C;
		T2& t2C = *pt2C;
		for (FXD::U32 n1 = 0; n1 < 1000; n1++)
		{
			t1C.insert(typename T1::value_type(typename T1::value_type(n1)));
			t2C.insert(typename T2::value_type(typename T2::value_type(n1)));
			PRINT_COND_ASSERT(t1C.validate(), "Set - insert_return_type insert(value_type&& val)");
			nErrorCount += CompareContainers(t1C, t2C, "Set - insert_return_type insert(value_type&& val)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());
		}

		std::unique_ptr< T1 > pt1D = std::make_unique< T1 >();
		std::unique_ptr< T2 > pt2D = std::make_unique< T2 >();
		T1& t1D = *pt1D;
		T2& t2D = *pt2D;
		nErrorCount += CompareContainers(t1D, t2D, "Set - Set(void)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());

		std::unique_ptr< T1 > pt1E = std::make_unique< T1 >(t1C);
		std::unique_ptr< T2 > pt2E = std::make_unique< T2 >(t2C);
		T1& t1E = *pt1E;
		T2& t2E = *pt2E;
		PRINT_COND_ASSERT(t1E.validate(), "Set - Set(void)");
		nErrorCount += CompareContainers(t1E, t2E, "Set - Set(void)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());

		std::unique_ptr< T1 > pt1F = std::make_unique< T1 >(t1C.begin(), t1C.end());
		std::unique_ptr< T2 > pt2F = std::make_unique< T2 >(t2C.begin(), t2C.end());
		T1& t1F = *pt1F;
		T2& t2F = *pt2F;
		PRINT_COND_ASSERT(t1F.validate(), "Set - Set(InputItr first, InputItr last)");
		nErrorCount += CompareContainers(t1F, t2F, "Set - Set(InputItr first, InputItr last)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());

		// my_type& operator=(const my_type& rhs);
		t1E = t1D;
		t2E = t2D;
		nErrorCount += CompareContainers(t1D, t2D, "Set - my_type& operator=(const my_type& rhs)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());
		nErrorCount += CompareContainers(t1E, t2E, "Set - my_type& operator=(const my_type& rhs)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());

		// insert_return_type insert(const value_type& val);
		// insert_return_type insert(P&& val);
		// insert_return_type insert(value_type&& val);
		{
			std::unique_ptr< T1 > pT1P = std::make_unique< T1 >(t1C.begin(), t1C.end());
			std::unique_ptr< T1 > pT1Q = std::make_unique< T1 >(t2C.begin(), t2C.end());

			T1& t1P = *pT1P;
			T1& t1Q = *pT1Q;

			typename T1::value_type v10(0);
			typename T1::value_type v11(1);
			typename T1::value_type v12(2);
			typename T1::value_type v13(3);
			typename T1::value_type v14(4);
			typename T1::value_type v15(5);

			t1P.insert(v10);
			t1P.insert(v11);
			t1P.insert(v12);

			t1Q.insert(v13);
			t1Q.insert(v14);
			t1Q.insert(v15);

			t1Q = std::move(t1P);
		}

		// void swap(my_type& rhs)
		{
			t1E.swap(t1D);
			t2E.swap(t2D);
			PRINT_COND_ASSERT(t1D.validate(), "Set - void swap(my_type& rhs)");
			PRINT_COND_ASSERT(t1E.validate(), "Set - void swap(my_type& rhs)");
			nErrorCount += CompareContainers(t1D, t2D, "Set - void swap(my_type& rhs)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());
			nErrorCount += CompareContainers(t1E, t2E, "Set - void swap(my_type& rhs)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());

			FXD::STD::swap(t1E, t1D);
			std::swap(t2E, t2D);
			PRINT_COND_ASSERT(t1D.validate(), "Set - void swap(my_type& rhs)");
			PRINT_COND_ASSERT(t1E.validate(), "Set - void swap(my_type& rhs)");
			nErrorCount += CompareContainers(t1D, t2D, "Set - void swap(my_type& rhs)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());
			nErrorCount += CompareContainers(t1E, t2E, "Set - void swap(my_type& rhs)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());
		}

		// void clear(void)
		{
			t1A.clear();
			t2A.clear();
			PRINT_COND_ASSERT(t1A.validate(), "Set - void clear(void)");
			nErrorCount += CompareContainers(t1A, t2A, "Set - void clear(void)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());

			t1B.clear();
			t2B.clear();
			PRINT_COND_ASSERT(t1B.validate(), "Set - void clear(void)");
			nErrorCount += CompareContainers(t1B, t2B, "Set - void clear(void)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());
		}
	}

	PRINT_COND_ASSERT(TestObject::is_clear(), "Set - Failed");
	TestObject::reset();

	return nErrorCount;
}

///////////////////////////////////////////////////////////////////////////////
// TestSetMutation
//
// Requires a container that can hold at least 1000 items.
///////////////////////////////////////////////////////////////////////////////
template < typename T1, typename T2, bool bMultiset >
FXD::S32 TestSetMutation(void)
{
	FXD::S32 nErrorCount = 0;
	TestObject::reset();

	{
		std::unique_ptr< T1 > pt1A = std::make_unique< T1 >();
		std::unique_ptr< T2 > pt2A = std::make_unique< T2 >();
		T1& t1A = *pt1A;
		T2& t2A = *pt2A;


		if (gFXD_TestLevel != FXD_TestLevel::Low)
		{
			// Set up an array of values to randomize / permute.
			FXD::Container::Vector< typename T1::value_type > vecInsert;

			FXD::Core::RandomGen rng(FXD::App::GetRandSeed());

			vecInsert.clear();

			for (FXD::U32 n1 = 0; n1 < 1000; n1++)
			{
				vecInsert.push_back(typename T1::value_type(n1));

				// Occasionally attempt to duplicate an element, both for set and multiset.
				if (((n1 + 1) < 1000) && (rng.rand_limit(4) == 0))
				{
					vecInsert.push_back(typename T1::value_type(n1));
					n1++;
				}
			}

			for (FXD::U32 n1 = 0; n1 < FXD::STD::to_underlying(gFXD_TestLevel) * 20; n1++) // For each permutation...
			{
				FXD::STD::random_shuffle(vecInsert.begin(), vecInsert.end(), rng);

				// insert_return_type insert(value_type&& val);
				for (FXD::U32 n2 = 0, iEnd = (FXD::U32)vecInsert.size(); n2 < iEnd; n2++)
				{
					typename T1::value_type& key = vecInsert[n1];

					t1A.insert(typename T1::value_type(key)); // We expect that both arguments are the same.
					t2A.insert(typename T2::value_type(key));

					PRINT_COND_ASSERT(t1A.validate(), "Set - insert_return_type insert(value_type&& val)");
					nErrorCount += CompareContainers(t1A, t2A, "Set - insert_return_type insert(value_type&& val)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());
				}

				// reverse_iterator rbegin(void) NOEXCEPT
				// reverse_iterator rend(void) NOEXCEPT
				typename T1::reverse_iterator r1 = t1A.rbegin();
				typename T2::reverse_iterator r2 = t2A.rbegin();

				while (r1 != t1A.rend())
				{
					typename T1::value_type key1 = *r1;
					typename T2::value_type key2 = *r2;
					PRINT_COND_ASSERT(key1 == key2, "Set - reverse_iterator rbegin(void) NOEXCEPT");

					r1++;
					r2++;
				}

				// size_type find_erase(const key_type& key)
				// bool contains(const key_type& key) const
				for (FXD::U32 n2 = 0, iEnd = (FXD::U32)vecInsert.size(); n2 < iEnd; n2++)
				{
					typename T1::value_type& key = vecInsert[n2];

					typename T1::size_type nRet1 = t1A.find_erase(key);
					typename T2::size_type nRet2 = t2A.erase(key);

					PRINT_COND_ASSERT((nRet1 == nRet2), "Set - size_type find_erase(const key_type& key)");
					PRINT_COND_ASSERT(t1A.validate(), "Set - size_type find_erase(const key_type& key)");
					nErrorCount += CompareContainers(t1A, t2A, "Set - bool find_erase(const key_type& key)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());
				}

				PRINT_COND_ASSERT((TestObject::sTOCount == 0) || (TestObject::sTOCount == (int64_t)vecInsert.size()), "Set - Failed"); // This test will only have meaning when T1 contains TestObject.
			}
		}

		PRINT_COND_ASSERT(TestObject::is_clear(), "Set - Failed");
		TestObject::reset();

		// Possibly do extended testing.
		if (gFXD_TestLevel == FXD_TestLevel::High)
		{
			// Set up an array of values to randomize / permute.
			FXD::Container::Vector< typename T1::value_type > vecInsert;

			for (FXD::U32 n1 = 0; n1 < 9; n1++) // Much more than this count would take too long to test all permutations.
			{
				vecInsert.push_back(typename T1::value_type(n1));
			}

			// Insert these values into the set in every existing permutation.
			for (FXD::U32 n1 = 0; FXD::STD::next_permutation(vecInsert.begin(), vecInsert.end()); n1++) // For each permutation...
			{
				// insert_return_type insert(value_type&& val);
				for (FXD::U32 n2 = 0, iEnd = (FXD::U32)vecInsert.size(); n2 < iEnd; n2++)
				{
					typename T1::value_type& key = vecInsert[n2];

					t1A.insert(typename T1::value_type(key)); // We expect that both arguments are the same.
					t2A.insert(typename T2::value_type(key));

					PRINT_COND_ASSERT(t1A.validate(), "Set - insert_return_type insert(value_type&& val)");
					nErrorCount += CompareContainers(t1A, t2A, "Set - insert_return_type insert(value_type&& val)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());
				}

				// bool find_erase(const key_type& key)
				// bool contains(const key_type& key) const
				for (FXD::U32 n2 = 0, iEnd = (FXD::U32)vecInsert.size(); n2 < iEnd; n2++)
				{
					typename T1::value_type& key = vecInsert[n2];

					t1A.find_erase(key);
					t2A.erase(key);

					PRINT_COND_ASSERT(t1A.validate(), "Set - bool find_erase(const key_type& key)");
					nErrorCount += CompareContainers(t1A, t2A, "Set - bool find_erase(const key_type& key)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());
				}

				PRINT_COND_ASSERT((TestObject::sTOCount == 0) || (TestObject::sTOCount == (int64_t)vecInsert.size()), "Set - Failed"); // This test will only have meaning when T1 contains TestObject.
			}
		}
	}

	PRINT_COND_ASSERT(TestObject::is_clear(), "Set - Failed");
	TestObject::reset();

	{ // Other insert and erase operations
		std::unique_ptr< T1 > pt1A = std::make_unique< T1 >();
		std::unique_ptr< T2 > pt2A = std::make_unique< T2 >();
		T1& t1A = *pt1A;
		T2& t2A = *pt2A;

		// Set up an array of values to randomize / permute.
		FXD::Container::Vector< typename T1::value_type > vecInsert1;
		std::vector< typename T2::value_type > vecInsert2;

		FXD::Core::RandomGen rng(FXD::App::GetRandSeed());

		for (FXD::U32 n1 = 0; n1 < 100; n1++)
		{
			vecInsert1.push_back(typename T1::value_type(n1));
			vecInsert2.push_back(typename T2::value_type(n1));

			if (rng.rand_limit(3) == 0)
			{
				vecInsert1.push_back(typename T1::value_type(n1));
				vecInsert2.push_back(typename T2::value_type(n1));
			}
		}

		// void insert(InputItr first, InputItr last)
		{
			t1A.insert(vecInsert1.begin(), vecInsert1.end());
			t2A.insert(vecInsert2.begin(), vecInsert2.end());
			PRINT_COND_ASSERT(t1A.validate(), "Set - void insert(InputItr first, InputItr last)");
			nErrorCount += CompareContainers(t1A, t2A, "Set - void insert(InputItr first, InputItr last)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());
		}

		// iterator insert(const_iterator hint, const value_type& val);
		{
			typename T1::iterator it1 = t1A.insert(t1A.find(typename T1::value_type(2)), typename T1::value_type(1));
			typename T2::iterator it2 = t2A.insert(t2A.find(typename T2::value_type(2)), typename T2::value_type(1));
			PRINT_COND_ASSERT(t1A.validate(), "Set - iterator insert(const_iterator hint, const value_type& val)");
			PRINT_COND_ASSERT(*it1 == typename T1::value_type(1), "iterator insert(const_iterator hint, const value_type& val)");
			PRINT_COND_ASSERT(*it2 == typename T2::value_type(1), "iterator insert(const_iterator hint, const value_type& val)");
			nErrorCount += CompareContainers(t1A, t2A, "Set - iterator insert(const_iterator hint, const value_type& val)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());

			it1 = t1A.insert(t1A.end(), typename T1::value_type(5));
			it2 = t2A.insert(t2A.end(), typename T2::value_type(5));
			PRINT_COND_ASSERT(t1A.validate(), "Set - iterator insert(const_iterator hint, const value_type& val)");
			PRINT_COND_ASSERT(*it1 == typename T1::value_type(5), "Set - iterator insert(const_iterator hint, const value_type& val)");
			PRINT_COND_ASSERT(*it2 == typename T2::value_type(5), "Set - iterator insert(const_iterator hint, const value_type& val)");
			nErrorCount += CompareContainers(t1A, t2A, "Set - iterator insert(const_iterator hint, const value_type& val)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());

			// Now we remove these items so that the insertions above can succeed.
			t1A.erase(t1A.find(typename T1::value_type(1)));
			t2A.erase(t2A.find(typename T2::value_type(1)));
			it1 = t1A.insert(t1A.find(typename T1::value_type(2)), typename T1::value_type(1));
			it2 = t2A.insert(t2A.find(typename T2::value_type(2)), typename T2::value_type(1));
			PRINT_COND_ASSERT(t1A.validate(), "Set - iterator insert(const_iterator hint, const value_type& val)");
			PRINT_COND_ASSERT(*it1 == typename T1::value_type(1), "Set - iterator insert(const_iterator hint, const value_type& val)");
			PRINT_COND_ASSERT(*it2 == typename T2::value_type(1), "Set - iterator insert(const_iterator hint, const value_type& val)");
			nErrorCount += CompareContainers(t1A, t2A, "Set - iterator insert(const_iterator hint, const value_type& val)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());

			t1A.erase(t1A.find(typename T1::value_type(5)));
			t2A.erase(t2A.find(typename T2::value_type(5)));
			it1 = t1A.insert(t1A.end(), typename T1::value_type(5));
			it2 = t2A.insert(t2A.end(), typename T2::value_type(5));
			PRINT_COND_ASSERT(t1A.validate(), "Set - iterator insert(const_iterator hint, const value_type& val)");
			PRINT_COND_ASSERT(*it1 == typename T1::value_type(5), "Set - iterator insert(const_iterator hint, const value_type& val)");
			PRINT_COND_ASSERT(*it2 == typename T2::value_type(5), "Set - iterator insert(const_iterator hint, const value_type& val)");
			nErrorCount += CompareContainers(t1A, t2A, "Set - iterator insert(const_iterator hint, const value_type& val)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());
		}

		// iterator erase(const_iterator first, const_iterator last);
		{
			typename T1::iterator it11 = t1A.find(typename T1::value_type(17));
			typename T1::iterator it12 = t1A.find(typename T2::value_type(37));
			t1A.erase(it11, it12);

			typename T2::iterator it21 = t2A.find(typename T1::value_type(17));
			typename T2::iterator it22 = t2A.find(typename T2::value_type(37));
			t2A.erase(it21, it22);

			PRINT_COND_ASSERT(t1A.validate(), "Set - iterator erase(const_iterator first, const_iterator last)");
			nErrorCount += CompareContainers(t1A, t2A, "Set - iterator erase(const_iterator first, const_iterator last)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());
		}

		// iterator erase(const_iterator pos);
		{
			t1A.erase(t1A.find(typename T1::value_type(60)));
			t2A.erase(t2A.find(typename T1::value_type(60)));
			PRINT_COND_ASSERT(t1A.validate(), "Set - iterator erase(const_iterator pos)");
			nErrorCount += CompareContainers(t1A, t2A, "Set - iterator erase(const_iterator pos)", FXD::STD::UseSelf< typename T1::value_type >(), FXD::STD::UseSelf< typename T2::value_type >());
		}
	}

	// Set(std::initializer_list< value_type > iList, const value_compare& compare = value_compare()
	// my_type& operator=(std::initializer_list< T > iList)
	// void insert(std::initializer_list< value_type > iList)
	{
		T1 set = { typename T1::value_type(10), typename T1::value_type(11) };
		PRINT_COND_ASSERT(set.size() == 2, "Set - Set(std::initializer_list< value_type > iList, const value_compare& compare = value_compare()");
		typename T1::iterator itr = set.begin();
		PRINT_COND_ASSERT(*itr == typename T1::value_type(10), "Set - Set(std::initializer_list< value_type > iList, const value_compare& compare = value_compare()");
		itr = set.rbegin().base();
		PRINT_COND_ASSERT(*--itr == typename T1::value_type(11), "Set - Set(std::initializer_list< value_type > iList, const value_compare& compare = value_compare()");

		set = { typename T1::value_type(20), typename T1::value_type(21) };
		PRINT_COND_ASSERT(set.size() == 2, "Set - my_type& operator=(std::initializer_list< T > iList)");
		PRINT_COND_ASSERT(*set.begin() == typename T1::value_type(20), "Set - my_type& operator=(std::initializer_list< T > iList)");
		itr = set.rbegin().base();
		PRINT_COND_ASSERT(*--itr == typename T1::value_type(21), "Set - my_type& operator=(std::initializer_list< T > iList)");

		set.insert({ typename T1::value_type(40), typename T1::value_type(41) });
		PRINT_COND_ASSERT(set.size() == 4, "Set - void insert(std::initializer_list< value_type > iList)");
		itr = set.rbegin().base();
		PRINT_COND_ASSERT(*--itr == typename T1::value_type(41), "Set - void insert(std::initializer_list< value_type > iList)");
	}

	PRINT_COND_ASSERT(TestObject::is_clear(), "Set - Failed");
	TestObject::reset();

	return nErrorCount;
}

template < typename T1 >
FXD::S32 TestSetSpecific(T1& t1A, FXD::STD::false_type) // FXD::STD::false_type means this is a map and not a multimap.
{
	return 0;
}

template < typename T1 >
FXD::S32 TestSetSpecific(T1& t1A, FXD::STD::true_type) // FXD::STD::true_type means this is a multimap and not a map.
{
	FXD::S32 nErrorCount = 0;

	// Core::Pair< iterator, iterator > equal_range_small(const Key& key)
	// Core::Pair< const_iterator, const_iterator > equal_range_small(const Key& key) const
	{
		FXD::Core::Pair< typename T1::iterator, typename T1::iterator > pair = t1A.equal_range_small(typename T1::value_type(499));
		PRINT_COND_ASSERT((*pair.first == typename T1::value_type(499)), "Set - Core::Pair< const_iterator, const_iterator > equal_range_small(const Key& key) const");
		PRINT_COND_ASSERT((*pair.second == typename T1::value_type(501)), "Set - Core::Pair< const_iterator, const_iterator > equal_range_small(const Key& key) const");

		pair = t1A.equal_range_small(typename T1::value_type(-1));
		PRINT_COND_ASSERT((pair.first == pair.second), "Set - Core::Pair< const_iterator, const_iterator > equal_range_small(const Key& key) const");
		PRINT_COND_ASSERT((pair.first == t1A.begin()), "Set - Core::Pair< const_iterator, const_iterator > equal_range_small(const Key& key) const");
	}

	return nErrorCount;
}

///////////////////////////////////////////////////////////////////////////////
// TestSetSearch
//
// This function is designed to work with set, fixed_set (and not hash containers).
// Requires a container that can hold at least 1000 items.
///////////////////////////////////////////////////////////////////////////////
template < typename T1, bool bMultimap >
FXD::S32 TestSetSearch(void)
{
	FXD::S32 nErrorCount = 0;
	TestObject::reset();

	{ // Test find, lower_bound, upper_bound, etc..
		std::unique_ptr< T1 > pt1A = std::make_unique< T1 >();
		T1& t1A = *pt1A;
		typename T1::iterator itr;

		// Set up an array of values to randomize / permute.
		FXD::Container::Vector< typename T1::value_type > vecInsert;

		for (FXD::U32 n1 = 0; n1 < 1000; n1++)
		{
			vecInsert.push_back(typename T1::value_type(n1));
		}

		FXD::Core::RandomGen rng(FXD::App::GetRandSeed());
		FXD::STD::random_shuffle(vecInsert.begin(), vecInsert.end(), rng);

		// insert_return_type insert(value_type&& val)
		for (FXD::U32 n1 = 0, iEnd = (FXD::U32)vecInsert.size(); n1 < iEnd; n1++)
		{
			{
				typename T1::value_type k(n1);
				t1A.insert(typename T1::value_type(k));

				itr = t1A.find(k);
				PRINT_COND_ASSERT((itr != t1A.end()), "Set - insert_return_type insert(value_type&& val)");
			}
		}

		// iterator find(const key_type& key)
		// const_iterator find(const key_type& key) const
		{
			for (FXD::U32 n1 = 0; n1 < 1000; n1++)
			{
				typename T1::value_type k(n1);
				itr = t1A.find(k);

				PRINT_COND_ASSERT((itr != t1A.end()), "Set - const_iterator find(const key_type& key) const");
				PRINT_COND_ASSERT((*itr == k), "Set - const_iterator find(const key_type& key) const");
			}

			itr = t1A.find(typename T1::value_type(-1));
			PRINT_COND_ASSERT((itr == t1A.end()), "Set - const_iterator find(const key_type& key) const");
			itr = t1A.find(typename T1::value_type(1001));
			PRINT_COND_ASSERT((itr == t1A.end()), "Set - const_iterator find(const key_type& key) const");
		}

		// iterator lower_bound(const key_type& key)
		// const_iterator lower_bound(const key_type& key) const
		{
			itr = t1A.lower_bound(typename T1::value_type(0));
			PRINT_COND_ASSERT((itr == t1A.begin()), "Set - const_iterator lower_bound(const key_type& key) const");

			itr = t1A.lower_bound(typename T1::value_type(-1));
			PRINT_COND_ASSERT((itr == t1A.begin()), "Set - const_iterator lower_bound(const key_type& key) const");

			itr = t1A.lower_bound(typename T1::value_type(1001));
			PRINT_COND_ASSERT((itr == t1A.end()), "Set - const_iterator lower_bound(const key_type& key) const");

			t1A.find_erase(typename T1::value_type(500));
			itr = t1A.lower_bound(typename T1::value_type(500));
			PRINT_COND_ASSERT((*itr == typename T1::value_type(501)), "Set - size_type find_erase(const key_type& key) const");
		}

		// iterator upper_bound(const key_type& key)
		// const_iterator upper_bound(const key_type& key) const
		{
			itr = t1A.upper_bound(typename T1::value_type(-1));
			PRINT_COND_ASSERT((itr == t1A.begin()), "Set - const_iterator upper_bound(const key_type& key) const");

			itr = t1A.upper_bound(typename T1::value_type(499));
			PRINT_COND_ASSERT((*itr == typename T1::value_type(501)), "Set - const_iterator upper_bound(const key_type& key) const");

			itr = t1A.upper_bound(typename T1::value_type(-1));
			PRINT_COND_ASSERT((*itr == typename T1::value_type(0)), "Set - const_iterator upper_bound(const key_type& key) const");

			itr = t1A.upper_bound(typename T1::value_type(1000));
			PRINT_COND_ASSERT((itr == t1A.end()), "Set - const_iterator upper_bound(const key_type& key) const");
		}

		// size_type count(const Key& key) const
		{
			typename T1::size_type nCount = t1A.count(typename T1::value_type(-1));
			PRINT_COND_ASSERT((nCount == 0), "Set - size_type count(const Key& key) const");
			nCount = t1A.count(typename T1::value_type(0));
			PRINT_COND_ASSERT((nCount == 1), "Set - size_type count(const Key& key) const");
			nCount = t1A.count(typename T1::value_type(500));
			PRINT_COND_ASSERT((nCount == 0), "Set - size_type count(const Key& key) const");
			nCount = t1A.count(typename T1::value_type(1001));
			PRINT_COND_ASSERT((nCount == 0), "Set - size_type count(const Key& key) const");
		}

		// Core::Pair< iterator, iterator > equal_range(const Key& key)
		// Core::Pair< const_iterator, const_iterator > equal_range(const Key& key) const
		{
			FXD::Core::Pair< typename T1::iterator, typename T1::iterator > pair = t1A.equal_range(typename T1::value_type(200));
			PRINT_COND_ASSERT((*pair.first == typename T1::value_type(200)), "Set - Core::Pair< const_iterator, const_iterator > equal_range(const Key& key) const");
			pair = t1A.equal_range(typename T1::value_type(499));
			PRINT_COND_ASSERT((*pair.first == typename T1::value_type(499)), "Set - Core::Pair< const_iterator, const_iterator > equal_range(const Key& key) const");
			PRINT_COND_ASSERT((*pair.second == typename T1::value_type(501)), "Set - Core::Pair< const_iterator, const_iterator > equal_range(const Key& key) const");

			pair = t1A.equal_range(typename T1::value_type(-1));
			PRINT_COND_ASSERT((pair.first == pair.second), "Set - Core::Pair< const_iterator, const_iterator > equal_range(const Key& key) const");
			PRINT_COND_ASSERT((pair.first == t1A.begin()), "Set - Core::Pair< const_iterator, const_iterator > equal_range(const Key& key) const");
		}

		// Some tests need to be differently between map and multimap.
		nErrorCount += TestSetSpecific(t1A, FXD::STD::integral_constant< bool, bMultimap >());
	}
	PRINT_COND_ASSERT(TestObject::is_clear(), "Set - Failed");
	TestObject::reset();

	return nErrorCount;
}


///////////////////////////////////////////////////////////////////////////////
// TestSetCpp11
//
// This function is designed to work with set, fixed_set, hash_set, fixed_hash_set
///////////////////////////////////////////////////////////////////////////////
template < typename T1 >
FXD::S32 TestSetCpp11(void)
{
	FXD::S32 nErrorCount = 0;
	TestObject::reset();

	// insert_return_type emplace(Args&&... args);
	// iterator emplace_hint(const_iterator pos, Args&&... args);
	// iterator insert(const_iterator pos, value_type&& val);
	// Core::Pair< iterator,bool > insert(value_type&& val);
	{
		using TOSet = T1;
		typename TOSet::insert_return_type toSetInsertResult;
		typename TOSet::iterator itrSet;

		TOSet setTO;
		{
			TestObject to1(0);
			TestObject to2(1);
			toSetInsertResult = setTO.emplace(to1);
			PRINT_COND_ASSERT(toSetInsertResult.second, "Set - insert_return_type emplace(Args&&... args)");

			toSetInsertResult = setTO.emplace(std::move(to2));
			PRINT_COND_ASSERT(toSetInsertResult.second, "Set - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to(4);
			PRINT_COND_ASSERT(setTO.find(to) == setTO.end(), "Set - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(to.m_nX == 4, "Set - insert_return_type emplace(Args&&... args)");

			toSetInsertResult = setTO.emplace(std::move(to));
			PRINT_COND_ASSERT(toSetInsertResult.second == true, "Set - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(to.m_nX == 0, "Set - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to(4);
			toSetInsertResult = setTO.emplace(std::move(to));
			PRINT_COND_ASSERT(!toSetInsertResult.second, "Set - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to(5);
			toSetInsertResult = setTO.emplace(std::move(to));
			PRINT_COND_ASSERT(toSetInsertResult.second, "Set - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to(5);
			itrSet = setTO.emplace_hint(toSetInsertResult.first, std::move(to));
			PRINT_COND_ASSERT(*itrSet == TestObject(5), "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
		}
		{
			TestObject to(6);
			itrSet = setTO.emplace_hint(setTO.begin(), std::move(to));
			PRINT_COND_ASSERT(*itrSet == TestObject(6), "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
		}
		{
			TestObject to(2);
			PRINT_COND_ASSERT(setTO.find(to) == setTO.end(), "Set - insert_return_type emplace(Args&&... args)");

			toSetInsertResult = setTO.emplace(to);
			PRINT_COND_ASSERT(toSetInsertResult.second, "Set - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - insert_return_type emplace(Args&&... args)");

			toSetInsertResult = setTO.emplace(to);
			PRINT_COND_ASSERT(!toSetInsertResult.second, "Set - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to(7);
			toSetInsertResult = setTO.emplace(to);
			PRINT_COND_ASSERT(toSetInsertResult.second == true, "Set - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to(7);
			itrSet = setTO.emplace_hint(toSetInsertResult.first, to);
			PRINT_COND_ASSERT(*itrSet == to, "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
		}
		{
			TestObject to(8);
			itrSet = setTO.emplace_hint(setTO.begin(), to);
			PRINT_COND_ASSERT(*itrSet == to, "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
		}
		{
			TestObject to(3);
			PRINT_COND_ASSERT(setTO.find(to) == setTO.end(), "Set - iterator insert(const_iterator pos, value_type&& val)");

			toSetInsertResult = setTO.insert(TestObject(to));
			PRINT_COND_ASSERT(toSetInsertResult.second == true, "Set - iterator insert(const_iterator pos, value_type&& val)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - iterator insert(const_iterator pos, value_type&& val)");

			toSetInsertResult = setTO.insert(TestObject(to));
			PRINT_COND_ASSERT(toSetInsertResult.second == false, "Set - iterator insert(const_iterator pos, value_type&& val)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - iterator insert(const_iterator pos, value_type&& val)");
		}
		{
			TestObject to(9);
			toSetInsertResult = setTO.emplace(std::move(to));
			PRINT_COND_ASSERT(toSetInsertResult.second, "Set - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to(9);
			itrSet = setTO.emplace_hint(toSetInsertResult.first, std::move(to));
			PRINT_COND_ASSERT(*itrSet == TestObject(9), "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
		}
		{
			TestObject to(10);
			itrSet = setTO.emplace_hint(setTO.begin(), std::move(to));
			PRINT_COND_ASSERT(*itrSet == TestObject(10), "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
		}
	}

	return nErrorCount;
}

///////////////////////////////////////////////////////////////////////////////
// TestMultisetCpp11
//
// This function is designed to work with multiset, fixed_multiset, hash_multiset, fixed_hash_multiset
//
// This is similar to the TestSetCpp11 function, with some differences related 
// to handling of duplicate entries.
///////////////////////////////////////////////////////////////////////////////
template < typename T1 >
FXD::S32 TestMultisetCpp11(void)
{
	FXD::S32 nErrorCount = 0;
	TestObject::reset();

	// insert_return_type emplace(Args&&... args);
	// iterator emplace_hint(const_iterator pos, Args&&... args);
	// iterator insert(const_iterator pos, value_type&& val);
	{
		using TOSet = T1;
		typename TOSet::iterator itrSet;

		TOSet setTO;
		{
			TestObject to1(0);
			TestObject to2(1);

			itrSet = setTO.emplace(to1);
			PRINT_COND_ASSERT(*itrSet == TestObject(0), "Set - insert_return_type emplace(Args&&... args)");

			itrSet = setTO.emplace(std::move(to2));
			PRINT_COND_ASSERT(*itrSet == TestObject(1), "Set - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to(4);
			PRINT_COND_ASSERT(setTO.find(to) == setTO.end(), "Set - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(to.m_nX == 4, "Set - insert_return_type emplace(Args&&... args)");

			itrSet = setTO.emplace(std::move(to));
			PRINT_COND_ASSERT(*itrSet == TestObject(4), "Set - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(to.m_nX == 0, "Set - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to(4);
			itrSet = setTO.emplace(std::move(to));
			PRINT_COND_ASSERT(*itrSet == TestObject(4), "Set - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to(5);
			itrSet = setTO.emplace(std::move(to));
			PRINT_COND_ASSERT(*itrSet == TestObject(5), "Set - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to(5);
			itrSet = setTO.emplace_hint(itrSet, std::move(to));
			PRINT_COND_ASSERT(*itrSet == TestObject(5), "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
		}
		{
			TestObject to(6);
			itrSet = setTO.emplace_hint(setTO.begin(), std::move(to));
			PRINT_COND_ASSERT(*itrSet == TestObject(6), "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
		}
		{
			TestObject to(2);
			PRINT_COND_ASSERT(setTO.find(to) == setTO.end(), "Set - insert_return_type emplace(Args&&... args)");

			itrSet = setTO.emplace(to);
			PRINT_COND_ASSERT(*itrSet == TestObject(2), "Set - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - insert_return_type emplace(Args&&... args)");

			itrSet = setTO.emplace(to);
			PRINT_COND_ASSERT(*itrSet == TestObject(2), "Set - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to(7);
			itrSet = setTO.emplace(to);
			PRINT_COND_ASSERT(*itrSet == TestObject(7), "Set - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to(7);
			itrSet = setTO.emplace_hint(itrSet, to);
			PRINT_COND_ASSERT(*itrSet == to, "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
		}
		{
			TestObject to(8);
			itrSet = setTO.emplace_hint(setTO.begin(), to);
			PRINT_COND_ASSERT(*itrSet == to, "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
		}
		{
			TestObject to(3);
			PRINT_COND_ASSERT(setTO.find(to) == setTO.end(), "Set - insert_return_type emplace(Args&&... args)");

			itrSet = setTO.insert(TestObject(to));
			PRINT_COND_ASSERT(*itrSet == TestObject(3), "Set - iterator insert(const_iterator pos, value_type&& val)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - iterator insert(const_iterator pos, value_type&& val)");

			itrSet = setTO.insert(TestObject(to));
			PRINT_COND_ASSERT(*itrSet == TestObject(3), "Set - iterator insert(const_iterator pos, value_type&& val)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - iterator insert(const_iterator pos, value_type&& val)");
		}
		{
			TestObject to(9);
			itrSet = setTO.emplace(std::move(to));
			PRINT_COND_ASSERT(*itrSet == TestObject(9), "Set - insert_return_type emplace(Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - insert_return_type emplace(Args&&... args)");
		}
		{
			TestObject to(9);
			itrSet = setTO.emplace_hint(itrSet, std::move(to));
			PRINT_COND_ASSERT(*itrSet == TestObject(9), "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
		}
		{
			TestObject to(10);
			itrSet = setTO.emplace_hint(setTO.begin(), std::move(to));
			PRINT_COND_ASSERT(*itrSet == TestObject(10), "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
			PRINT_COND_ASSERT(setTO.find(to) != setTO.end(), "Set - iterator emplace_hint(const_iterator pos, Args&&... args)");
		}
	}

	return nErrorCount;
}
#endif //TESTS_SYSTEM_TESTS_TESTSET_H