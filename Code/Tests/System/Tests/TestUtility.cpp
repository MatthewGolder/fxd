// Creator - MatthewGolder
#include "Tests/System/Tests/FXDTest.h"
#include "FXDEngine/Core/String.h"
#include "FXDEngine/Core/Utility.h"

namespace
{
	class _NoExceptMoveCopy
	{
	public:
		_NoExceptMoveCopy(void)
			: m_bStatus(true)
		{}

		_NoExceptMoveCopy(const _NoExceptMoveCopy&) = default;

		_NoExceptMoveCopy(_NoExceptMoveCopy&& rhs) NOEXCEPT
		{
			rhs.m_bStatus = false;
		}

		NO_ASSIGNMENT(_NoExceptMoveCopy);  // required as VS2015 enabled C4626 by default.

	public:
		bool m_bStatus;
	};

	class _NoExceptMoveNoCopy
	{
	public:
		_NoExceptMoveNoCopy(void)
			: m_bStatus(true)
		{}

		NO_COPY(_NoExceptMoveNoCopy);

		_NoExceptMoveNoCopy(_NoExceptMoveNoCopy&& rhs) NOEXCEPT
		{
			rhs.m_bStatus = false;
		};

		NO_ASSIGNMENT(_NoExceptMoveNoCopy);  // required as VS2015 enabled C4626 by default.

	public:
		bool m_bStatus;
	};

	class _ExceptMoveCopy
	{
	public:
		_ExceptMoveCopy(void)
			: m_bStatus(true)
		{}

		_ExceptMoveCopy(const _ExceptMoveCopy&) = default;

		_ExceptMoveCopy(_ExceptMoveCopy&& rhs) NOEXCEPT_IF(false)
		{
			rhs.m_bStatus = false;
		};

		NO_ASSIGNMENT(_ExceptMoveCopy);  // required as VS2015 enabled C4626 by default.

	public:
		bool m_bStatus;
	};

	class _ExceptMoveNoCopy
	{
	public:
		_ExceptMoveNoCopy(void)
			: m_bStatus(true)
		{}

		_ExceptMoveNoCopy(_ExceptMoveNoCopy&& rhs) NOEXCEPT_IF(false)
		{
			rhs.m_bStatus = false;
		};

		NO_COPY(_ExceptMoveNoCopy);
		NO_ASSIGNMENT(_ExceptMoveNoCopy);  // required as VS2015 enabled C4626 by default.

	public:
		bool m_bStatus;
	};
	//#endif
}

static FXD::S32 TestUtilityMove(void)
{
	FXD::S32 nErrorCount = 0;

	// move_if_noexcept
	{
		_NoExceptMoveCopy nemcA;
		_NoExceptMoveCopy nemcB = FXD::STD::move_if_noexcept(nemcA);  // nemcB should be constructed via NoExceptMoveCopy(NoExceptMoveCopy&&)
		PRINT_COND_ASSERT(!nemcA.m_bStatus, "Utility - move_if_noexcept");

		_NoExceptMoveNoCopy nemncA;
		_NoExceptMoveNoCopy nemncB = FXD::STD::move_if_noexcept(nemncA);  // nemncB should be constructed via NoExceptMoveNoCopy(NoExceptMoveNoCopy&&)
		PRINT_COND_ASSERT(!nemncA.m_bStatus, "Utility - move_if_noexcept");

		_ExceptMoveCopy emcA;
		_ExceptMoveCopy emcB = FXD::STD::move_if_noexcept(emcA);  // emcB should be constructed via ExceptMoveCopy(const ExceptMoveCopy&) if exceptions are enabled.
		PRINT_COND_ASSERT(emcA.m_bStatus, "Utility - move_if_noexcept");

		_ExceptMoveNoCopy emncA;
		_ExceptMoveNoCopy emncB = FXD::STD::move_if_noexcept(emncA);  // emncB should be constructed via ExceptMoveNoCopy(ExceptMoveNoCopy&&)
		PRINT_COND_ASSERT(!emncA.m_bStatus, "Utility - move_if_noexcept");
	}

	return nErrorCount;
}

static FXD::S32 TestUtilityIntegerSequence(void)
{
	FXD::S32 nErrorCount = 0;
#if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED
	// Android clang chokes with an internal compiler error on FXD::STD::make_integer_sequence
#if !IsOSAndroid()
	PRINT_COND_ASSERT((FXD::STD::integer_sequence< int, 0, 1, 2, 3, 4 >::size() == 5), "Utility - integer_sequence");
	PRINT_COND_ASSERT((FXD::STD::make_integer_sequence< int, 5 >::size() == 5), "Utility - make_integer_sequence");
#endif
	PRINT_COND_ASSERT((FXD::STD::index_sequence< 0, 1, 2, 3, 4 >::size() == 5), "Utility - integer_sequence");
	PRINT_COND_ASSERT((FXD::STD::make_index_sequence< 5 >::size() == 5), "Utility - make_integer_sequence");
#endif // FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

	return nErrorCount;
}

static FXD::S32 TestUtilityExchange(void)
{
	FXD::S32 nErrorCount = 0;
	{
		FXD::S32 nVal = 0;

		auto rVal = FXD::STD::exchange(nVal, 1);
		PRINT_COND_ASSERT(rVal == 0, "Utility - exchange");
		PRINT_COND_ASSERT(nVal == 1, "Utility - exchange");
	}
	{
		FXD::S32 nVal = 0;

		auto rVal = FXD::STD::exchange(nVal, 1.78);
		PRINT_COND_ASSERT(rVal == 0, "Utility - exchange");
		PRINT_COND_ASSERT(nVal == 1, "Utility - exchange");
	}
	{
		FXD::S32 nVal = 0;

		auto rVal = FXD::STD::exchange(nVal, 1.78f);
		PRINT_COND_ASSERT(rVal == 0, "Utility - exchange");
		PRINT_COND_ASSERT(nVal == 1, "Utility - exchange");
	}
	{
		FXD::S32 nVal1 = 0;
		FXD::S32 nVal2 = 1;

		auto rVal = FXD::STD::exchange(nVal1, nVal2);
		PRINT_COND_ASSERT(rVal == 0, "Utility - exchange");
		PRINT_COND_ASSERT(nVal1 == 1, "Utility - exchange");
		PRINT_COND_ASSERT(nVal2 == 1, "Utility - exchange");
	}
	{
		bool bVal = true;

		auto rVal = FXD::STD::exchange(bVal, true);
		PRINT_COND_ASSERT(rVal, "Utility - exchange");

		rVal = FXD::STD::exchange(bVal, false);
		PRINT_COND_ASSERT( rVal, "Utility - exchange");
		PRINT_COND_ASSERT(!bVal, "Utility - exchange");

		rVal = FXD::STD::exchange(bVal, true);
		PRINT_COND_ASSERT(!rVal, "Utility - exchange");
		PRINT_COND_ASSERT(bVal, "Utility - exchange");
	}
	{
		TestObject::reset();

		TestObject a(42);
		auto rVal = FXD::STD::exchange(a, TestObject(24));

		PRINT_COND_ASSERT(rVal.m_nX == 42, "Utility - exchange");
		PRINT_COND_ASSERT(a.m_nX == 24, "Utility - exchange");
	}
	{
		const FXD::UTF8* const pFXDEngine = "FXDEngine";
		const FXD::UTF8* const pFunky3D = "Funcky Three Dimensions";

		FXD::Core::String8 strVal(pFXDEngine);
		auto rVal = FXD::STD::exchange(strVal, pFunky3D);

		PRINT_COND_ASSERT(rVal == pFXDEngine, "Utility - exchange");
		PRINT_COND_ASSERT(strVal == pFunky3D, "Utility - exchange");

		rVal = FXD::STD::exchange(strVal, "Utility - FXD Standard Template Library");
		PRINT_COND_ASSERT(strVal == "Utility - FXD Standard Template Library", "Utility - exchange");
	}

	return nErrorCount;
}

FXD::S32 TestUtility(void)
{
	FXD::S32 nErrorCount = 0;

	nErrorCount += TestUtilityMove();
	nErrorCount += TestUtilityIntegerSequence();
	nErrorCount += TestUtilityExchange();

	// as_const
	{
		FXD::S32 n32 = 42;
		CTC_ASSERT((FXD::STD::is_same< decltype(FXD::STD::as_const(n32)), const FXD::S32& >::value), "Utility - as_const< FXD::S32 >");
		PRINT_COND_ASSERT(FXD::STD::as_const(n32) == 42, "Utility - as_const< FXD::S32 >");

		FXD::Core::String8 str = "FXD Engine";
		CTC_ASSERT((FXD::STD::is_same< decltype(FXD::STD::as_const(str)), const FXD::Core::String8& >::value), "Utility - as_const< Core::String8 >");
		PRINT_COND_ASSERT(FXD::STD::as_const(str) == "FXD Engine", "Utility - as_const< Core::String8 >");
	}

	return nErrorCount;
}