// Creator - MatthewGolder
#include "Tests/System/Tests/FXDTest.h"
#include "FXDEngine/Math/Limits.h"

namespace
{
	class _NonNumericType
	{
	public:
		_NonNumericType(void)
			: m_nVal(0)
		{}
		_NonNumericType(FXD::S32 nVal)
			: m_nVal(nVal)
		{}
		bool operator==(FXD::S32 nVal) const
		{
			return (m_nVal == nVal);
		}

	public:
		FXD::S32 m_nVal;
	};
}

///////////////////////////////////////////////////////////////////////////////
// TestNumericLimits
///////////////////////////////////////////////////////////////////////////////
FXD::S32 TestNumericLimits(void)
{
	FXD::S32 nErrorCount = 0;

	// Test a type that is not numeric,.
	{
		PRINT_COND_ASSERT(!FXD::Math::NumericLimits< _NonNumericType >::is_bounded, "NumericLimits - is_bounded< NonNumericType >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< _NonNumericType >::max() == 0, "NumericLimits - max< NonNumericType >");

		PRINT_COND_ASSERT(!FXD::Math::NumericLimits< const _NonNumericType >::is_bounded, "NumericLimits - is_bounded< const NonNumericType >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< const _NonNumericType >::max() == 0, "NumericLimits - max< const NonNumericType >");

		PRINT_COND_ASSERT(!FXD::Math::NumericLimits< volatile _NonNumericType >::is_bounded, "NumericLimits - is_bounded< volatile NonNumericType >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< volatile _NonNumericType >::max() == 0, "NumericLimits - max< volatile NonNumericType >");

		PRINT_COND_ASSERT(!FXD::Math::NumericLimits< const volatile _NonNumericType >::is_bounded, "NumericLimits - is_bounded< const volatile NonNumericType >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< const volatile _NonNumericType >::max() == 0, "NumericLimits - max< const volatile NonNumericType >");
	}

	// Test bool in all const-volatile variants.
	{
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< bool >::is_bounded, "NumericLimits - is_bounded< bool >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< bool >::max() != 0, "NumericLimits - max< bool >");

		PRINT_COND_ASSERT(FXD::Math::NumericLimits< const bool >::is_bounded, "NumericLimits - is_bounded< const bool >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< const bool >::max() != 0, "NumericLimits - max< const bool >");

		PRINT_COND_ASSERT(FXD::Math::NumericLimits< volatile bool >::is_bounded, "NumericLimits - is_bounded< volatile bool >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< volatile bool >::max() != 0, "NumericLimits - max< volatile bool >");

		PRINT_COND_ASSERT(FXD::Math::NumericLimits< const volatile bool >::is_bounded, "NumericLimits - is_bounded< const volatile bool >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< const volatile bool >::max() != 0, "NumericLimits - max< const volatile bool >");
	}

	// Do basic tests of the remaining types.
	{
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::U8 >::is_bounded, "NumericLimits - is_bounded< FXD::U8 >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::U8 >::max() != 0, "NumericLimits - max< FXD::U8 >");
	}
	{
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::S8 >::is_bounded, "NumericLimits - is_bounded< FXD::S8 >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::S8 >::max() != 0, "NumericLimits - max< FXD::S8 >");
	}
	{
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::UTF8 >::is_bounded, "NumericLimits - is_bounded< FXD::UTF8 >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::UTF8 >::max() != 0, "NumericLimits - max< FXD::UTF8 >");
	}
	{
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::UTF16 >::is_bounded, "NumericLimits - is_bounded< FXD::UTF16 >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::UTF16 >::max() != 0, "NumericLimits - max< FXD::UTF16 >");
	}
	{
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::UTF32 >::is_bounded, "NumericLimits - is_bounded< FXD::UTF32 >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::UTF32 >::max() != 0, "NumericLimits - max< FXD::UTF32 >");
	}
	{
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::U16 >::is_bounded, "NumericLimits - is_bounded< FXD::U16 >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::U16 >::max() != 0, "NumericLimits - max< FXD::U16 >");
	}
	{
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::S16 >::is_bounded, "NumericLimits - is_bounded< FXD::S16 >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::S16 >::max() != 0, "NumericLimits - max< FXD::S16 >");
	}
	{
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::U32 >::is_bounded, "NumericLimits - is_bounded< FXD::U32 >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::U32 >::max() != 0, "NumericLimits - max< FXD::U32 >");
	}
	{
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::S32 >::is_bounded, "NumericLimits - is_bounded< FXD::S32 >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::S32 >::max() != 0, "NumericLimits - max< FXD::S32 >");
	}
	{
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::U64 >::is_bounded, "NumericLimits - is_bounded< FXD::U64 >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::U64 >::max() != 0, "NumericLimits - max< FXD::U64 >");
	}
	{
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::S64 >::is_bounded, "NumericLimits - is_bounded< FXD::S64 >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::S64 >::max() != 0, "NumericLimits - max< FXD::S64 >");
	}
	{
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::F32 >::is_bounded, "NumericLimits - is_bounded< FXD::F32 >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::F32 >::max() != 0, "NumericLimits - max< FXD::F32 >");
	}
	{
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::F64 >::is_bounded, "NumericLimits - is_bounded< FXD::F64 >");
		PRINT_COND_ASSERT(FXD::Math::NumericLimits< FXD::F64 >::max() != 0, "NumericLimits - max< FXD::F64 >");
	}

	return nErrorCount;
}