// Creator - MatthewGolder
#ifndef TESTS_SYSTEM_FXDTEST_H
#define TESTS_SYSTEM_FXDTEST_H

#include "FXDEngine/Core/Debugger.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <vector>

FXD::S32 TestAlgorithm(void);
//FXD::S32 TestAllocator(void);
FXD::S32 TestAny(void);
FXD::S32 TestArray(void);
//FXD::S32 TestBitVector(void);
FXD::S32 TestBitset(void);
//FXD::S32 TestCharTraits(void);
//FXD::S32 TestChrono(void);
//FXD::S32 TestCppCXTypeTraits(void);
FXD::S32 TestDeque(void);
//FXD::S32 TestExtra(void);
//FXD::S32 TestFixedFunction(void);
//FXD::S32 TestFixedHash(void);
//FXD::S32 TestFixedList(void);
//FXD::S32 TestFixedMap(void);
//FXD::S32 TestFixedSList(void);
//FXD::S32 TestFixedSet(void);
//FXD::S32 TestFixedString(void);
//FXD::S32 TestFixedTupleVector(void);
//FXD::S32 TestFixedVector(void);
FXD::S32 TestFunctional(void);
//FXD::S32 TestHash(void);
//FXD::S32 TestHeap(void);
//FXD::S32 TestIntrusiveHash(void);
//FXD::S32 TestIntrusiveList(void);
//FXD::S32 TestIntrusiveSDList(void);
//FXD::S32 TestIntrusiveSList(void);
FXD::S32 TestIterator(void);
FXD::S32 TestList(void);
//FXD::S32 TestListMap(void);
FXD::S32 TestMap(void);
FXD::S32 TestMemory(void);
//FXD::S32 TestMeta(void);
FXD::S32 TestNumericLimits(void);
//FXD::S32 TestOptional(void);
FXD::S32 TestPair(void);
//FXD::S32 TestRandom(void);
//FXD::S32 TestRatio(void);
//FXD::S32 TestRingBuffer(void);
//FXD::S32 TestSList(void);
//FXD::S32 TestSegmentedVector(void);
FXD::S32 TestSet(void);
//FXD::S32 TestSmartPtr(void);
//FXD::S32 TestSort(void);
//FXD::S32 TestSpan(void);
//FXD::S32 TestSparseMatrix(void);
FXD::S32 TestString(void);
//FXD::S32 TestStringHashMap(void);
//FXD::S32 TestStringMap(void);
FXD::S32 TestStringView(void);
FXD::S32 TestTuple(void);
FXD::S32 TestTypeTraits(void);
FXD::S32 TestUtility(void);
//FXD::S32 TestVariant(void);
FXD::S32 TestVector(void);


// Now enable warnings as desired.
#if defined(COMPILER_MSVC)
#pragma warning(disable: 4324)      // 'struct_name' : structure was padded due to __declspec(align())
//#pragma warning(disable: 4512)    // 'class' : assignment operator could not be generated
//#pragma warning(disable: 4100)    // 'identifier' : unreferenced formal parameter
//#pragma warning(disable: 4706)    // assignment within conditional expression

#pragma warning(default: 4056)      // Floating-point constant arithmetic generates a result that exceeds the maximum allowable value
#pragma warning(default: 4061)      // The enumerate has no associated handler in a switch statement
#pragma warning(default: 4062)      // The enumerate has no associated handler in a switch statement, and there is no default label
#pragma warning(default: 4191)      // Calling this function through the result pointer may cause your program to crash
#pragma warning(default: 4217)      // Member template functions cannot be used for copy-assignment or copy-construction
//#pragma warning(default: 4242)    // 'variable' : conversion from 'type' to 'type', possible loss of data
#pragma warning(default: 4254)      // 'operator' : conversion from 'type1' to 'type2', possible loss of data
#pragma warning(default: 4255)      // 'function' : no function prototype given: converting '()' to '(void)'
#pragma warning(default: 4263)      // 'function' : member function does not override any base class virtual member function
#pragma warning(default: 4264)      // 'virtual_function' : no override available for virtual member function from base 'class'; function is hidden
#pragma warning(default: 4287)      // 'operator' : unsigned/negative constant mismatch
#pragma warning(default: 4289)      // Nonstandard extension used : 'var' : loop control variable declared in the for-loop is used outside the for-loop scope
#pragma warning(default: 4296)      // 'operator' : expression is always false
#pragma warning(default: 4302)      // 'conversion' : truncation from 'type 1' to 'type 2'
#pragma warning(default: 4339)      // 'type' : use of undefined type detected in CLR meta-data - use of this type may lead to a runtime exception
#pragma warning(default: 4347)      // Behavior change: 'function template' is called instead of 'function'
//#pragma warning(default: 4514)    // unreferenced inline/local function has been removed
#pragma warning(default: 4529)      // 'member_name' : forming a pointer-to-member requires explicit use of the address-of operator ('&') and a qualified name
#pragma warning(default: 4545)      // Expression before comma evaluates to a function which is missing an argument list
#pragma warning(default: 4546)      // Function call before comma missing argument list
#pragma warning(default: 4547)      // 'operator' : operator before comma has no effect; expected operator with side-effect
//#pragma warning(default: 4548)    // expression before comma has no effect; expected expression with side-effect
#pragma warning(default: 4549)      // 'operator' : operator before comma has no effect; did you intend 'operator'?
#pragma warning(default: 4536)      // 'type name' : type-name exceeds meta-data limit of 'limit' characters
#pragma warning(default: 4555)      // Expression has no effect; expected expression with side-effect
#pragma warning(default: 4557)      // '__assume' contains effect 'effect'
//#pragma warning(default: 4619)    // #pragma warning : there is no warning number 'number'
#pragma warning(default: 4623)      // 'derived class' : default constructor could not be generated because a base class default constructor is inaccessible
#pragma warning(default: 4625)      // 'derived class' : copy constructor could not be generated because a base class copy constructor is inaccessible
#pragma warning(default: 4626)      // 'derived class' : assignment operator could not be generated because a base class assignment operator is inaccessible
#pragma warning(default: 4628)      // Digraphs not supported with -Ze. Character sequence 'digraph' not interpreted as alternate token for 'char'
#pragma warning(default: 4640)      // 'instance' : construction of local static object is not thread-safe
#pragma warning(default: 4668)      // 'symbol' is not defined as a preprocessor macro, replacing with '0' for 'directives'
#pragma warning(default: 4682)      // 'parameter' : no directional parameter attribute specified, defaulting to [in]
#pragma warning(default: 4686)      // 'user-defined type' : possible change in behavior, change in UDT return calling convention
//#pragma warning(default: 4710)    // 'function' : function not inlined
//#pragma warning(default: 4786)    // 'identifier' : identifier was truncated to 'number' characters in the debug information
#pragma warning(default: 4793)      // Native code generated for function 'function': 'reason'
//#pragma warning(default: 4820)    // 'bytes' bytes padding added after member 'member'
#pragma warning(default: 4905)      // Wide string literal cast to 'LPSTR'
#pragma warning(default: 4906)      // String literal cast to 'LPWSTR'
#pragma warning(default: 4917)      // 'declarator' : a GUID cannot only be associated with a class, interface or namespace
#pragma warning(default: 4928)      // Illegal copy-initialization; more than one user-defined conversion has been implicitly applied
#pragma warning(default: 4931)      // We are assuming the type library was built for number-bit pointers
#pragma warning(default: 4946)      // reinterpret_cast used between related classes: 'class1' and 'class2'
#endif


///////////////////////////////////////////////////////////////////////////////
// FXD includes
//
// Intentionally keep these includes below the warning settings specified above.
///////////////////////////////////////////////////////////////////////////////
#include "FXDEngine/Core/Algorithm.h"

/// FXD_TestLevel
///
/// Defines how extensive our testing is. A low level is for a desktop or 
/// nightly build in which the test can run quickly but still hit the 
/// majority of functionality. High level is for heavy testing and internal
/// validation which may take numerous hours to run.
///
enum class FXD_TestLevel
{
	Low = 1,      // ~10 seconds for test completion.
	Medium = 5,
	High = 10     // Numerous hours for test completion.
};

extern FXD_TestLevel gFXD_TestLevel;

///////////////////////////////////////////////////////////////////////////////
/// kMagicValue
///
/// Used as a unique integer. We assign this to TestObject in its constructor
/// and verify in the TestObject destructor that the value is unchanged. 
/// This can be used to tell, for example, if an invalid object is being 
/// destroyed.
///////////////////////////////////////////////////////////////////////////////
const FXD::U32 kMagicValue = 0x01f1cbe8;

///////////////////////////////////////////////////////////////////////////////
/// TestObject
///
/// Implements a generic object that is suitable for use in container tests.
/// Note that we choose a very restricted set of functions that are available
/// for this class. Do not add any additional functions, as that would 
/// compromise the intentions of the unit tests.
///////////////////////////////////////////////////////////////////////////////
struct TestObject
{
public:
	FXD::S32	m_nX;				// Value for the TestObject.
	bool		m_bThrowOnCopy;		// Throw an exception of this object is copied, moved, or assigned to another.
	FXD::S64	m_nID;				// Unique id for each object, equal to its creation number. This value is not coped from other TestObjects during any operations, including moves.
	FXD::U32	m_nMagicValue;		// Used to verify that an instance is valid and that it is not corrupted. It should always be kMagicValue.

	static FXD::S64 sTOCount;			// Count of all current existing TestObjects.
	static FXD::S64 sTOCtorCount;		// Count of times any ctor was called.
	static FXD::S64 sTODtorCount;		// Count of times dtor was called.
	static FXD::S64 sTODefaultCtorCount;// Count of times the default ctor was called.
	static FXD::S64 sTOArgCtorCount;	// Count of times the x0,x1,x2 ctor was called.
	static FXD::S64 sTOCopyCtorCount;	// Count of times copy ctor was called.
	static FXD::S64 sTOMoveCtorCount;	// Count of times move ctor was called.
	static FXD::S64 sTOCopyAssignCount;	// Count of times copy assignment was called.
	static FXD::S64 sTOMoveAssignCount;	// Count of times move assignment was called.
	static FXD::S32 sMagicErrorCount;	// Number of magic number mismatch errors.

public:
	EXPLICIT TestObject(FXD::S32 x = 0, bool bThrowOnCopy = false);
	TestObject(FXD::S32 x0, FXD::S32 x1, FXD::S32 x2, bool bThrowOnCopy = false); // This constructor exists for the purpose of testing variadiac template arguments, such as with the emplace container functions.

	~TestObject(void);

	TestObject(const TestObject& rhs);
	// Due to the nature of TestObject, there isn't much special for us to do in our move constructor. A move constructor swaps its contents with the other object, which is often a default-constructed object.
	TestObject(TestObject&& rhs);

	TestObject& operator=(const TestObject& rhs);
	TestObject& operator=(TestObject&& rhs);

	static void reset(void);
	static bool is_clear(void);
};

// Operators
// We specifically define only == and <, in order to verify that 
// our containers and algorithms are not mistakenly expecting other 
// operators for the contained and manipulated classes.
inline bool operator==(const TestObject& lhs, const TestObject& rhs)
{
	return (lhs.m_nX == rhs.m_nX);
}

inline bool operator<(const TestObject& lhs, const TestObject& rhs)
{
	return (lhs.m_nX < rhs.m_nX);
}

///////////////////////////////////////////////////////////////////////////////
/// test_use_self
///
/// Intentionally avoiding a dependency on FXD::STD::UseSelf.
///////////////////////////////////////////////////////////////////////////////
template < typename T >
struct test_use_self
{
public:
	const T& operator()(const T& x) const
	{
		return x;
	}
};


///////////////////////////////////////////////////////////////////////////////
/// CompareContainers
///
/// Does a comparison between the contents of two containers. 
///
/// Specifically tests for the following properties:
///     empty() is the same for both
///     size() is the same for both
///     iteration through both element by element yields equal values.
///////////////////////////////////////////////////////////////////////////////
template < typename T1, typename T2, typename ExtractValue1, typename ExtractValue2 >
FXD::S32 CompareContainers(const T1& t1, const T2& t2, const FXD::UTF8* pName, ExtractValue1 ev1 = test_use_self< T1 >(), ExtractValue2 ev2 = test_use_self< T2 >())
{
	FXD::S32 nErrorCount = 0;

	// Compare emptiness.
	PRINT_COND_ASSERT((t1.empty() == t2.empty()), "TestMap: Failed");

	// Compare sizes.
	const size_t nSize1 = t1.size();
	const size_t nSize2 = t2.size();

	PRINT_COND_ASSERT((nSize1 == nSize2), "TestMap: Failed");
	if (nSize1 != nSize2)
	{
		PRINT_INFO << pName << ": Container size difference: " << nSize1 << ", " << nSize2;
	}

	// Compare values.
	if (nSize1 == nSize2)
	{
		// Test iteration
		typename T1::const_iterator itr1 = t1.begin();
		typename T2::const_iterator itr2 = t2.begin();

		for (FXD::U32 n1 = 0; itr1 != t1.end(); ++itr1, ++itr2, ++n1)
		{
			const typename T1::value_type& v1 = *itr1;
			const typename T2::value_type& v2 = *itr2;

			PRINT_COND_ASSERT((ev1(v1) == ev2(v2)), "TestMap: Failed");
			if (!(ev1(v1) == ev2(v2)))
			{
				PRINT_INFO << pName << ": Container iterator difference at index " << n1;
				break;
			}
		}

		PRINT_COND_ASSERT((itr1 == t1.end()), "TestMap: Failed");
		PRINT_COND_ASSERT((itr2 == t2.end()), "TestMap: Failed");
	}
	return nErrorCount;
}


///////////////////////////////////////////////////////////////////////////////
/// VerifySequence
///
/// Allows the user to specify that a container has a given set of values.
/// 
/// Example usage:
///    vector< FXD::S32 > v;
///    v.push_back(1); v.push_back(3); v.push_back(5);
///    VerifySequence(v.begin(), v.end(), FXD::S32(), "v.push_back", 1, 3, 5, -1);
///
/// Note: The StackValue template argument is a hint to the compiler about what type 
///       the passed vararg sequence is.
///////////////////////////////////////////////////////////////////////////////
template < typename InputItr, typename StackValue >
bool VerifySequence(InputItr first, InputItr last, StackValue /*unused*/, const FXD::UTF8* pName, ...)
{
	using value_type = typename FXD::STD::iterator_traits< InputItr >::value_type;

	FXD::S32 nArgIndex = 0;
	FXD::S32 nSeqIndex = 0;
	bool bReturnValue = true;
	StackValue next;

	va_list args;
	va_start(args, pName);

	for (; first != last; ++first, ++nArgIndex, ++nSeqIndex)
	{
		next = va_arg(args, StackValue);

		if ((next == StackValue(-1)) || !(value_type(next) == *first))
		{
			if (pName != nullptr)
			{
				PRINT_INFO << pName << " Mismatch at index " << nArgIndex;
			}
			else
			{
				PRINT_INFO << " Mismatch at index " << nArgIndex;
			}
			bReturnValue = false;
		}
	}

	for (; first != last; ++first)
	{
		++nSeqIndex;
	}

	if (bReturnValue)
	{
		next = va_arg(args, StackValue);

		if (!(next == StackValue(-1)))
		{
			do
			{
				++nArgIndex;
				next = va_arg(args, StackValue);
			} while (!(next == StackValue(-1)));

			if (pName != nullptr)
			{
				PRINT_INFO << pName << " Too many elements: expected " << nArgIndex << ", found " << nSeqIndex;
			}
			else
			{
				PRINT_INFO << "Too many elements: expected " << nArgIndex << ", found " << nSeqIndex;
			}
			bReturnValue = false;
		}
	}

	va_end(args);

	return bReturnValue;
}

extern FXD::U32 GetRandSeed(void);
extern void SetRandSeed(FXD::U32 nSeed);

#endif //TESTS_SYSTEM_FXDTEST_H