// Creator - MatthewGolder
#include "Tests/System/Tests/TestSet.h"
#include "FXDEngine/Container/Bitset.h"
#include <bitset>

// Template instantations.
// These tell the compiler to compile all the functions for the given class.
template class FXD::Container::Bitset< 1 >;
template class FXD::Container::Bitset< 33 >;
template class FXD::Container::Bitset< 65 >;
template class FXD::Container::Bitset< 129 >;

FXD::S32 TestBitset(void)
{
	FXD::S32 nErrorCount = 0;

	{
		// CONSTEXPR FXD::Container::Bitset< 0 >(void) NOEXCEPT;
		{
			FXD::Container::Bitset< 0 > b0(0x10101010);
			PRINT_COND_ASSERT(b0.count() == 0, "Bitset - CONSTEXPR Bitset(unsigned long long nVal) NOEXCEPT");
			b0.flip();
			PRINT_COND_ASSERT(b0.count() == 0, "Bitset - Bitset& flip(void) NOEXCEPT");
			b0 <<= 1;
			PRINT_COND_ASSERT(b0.count() == 0, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
		}

		// CONSTEXPR FXD::Container::Bitset< 8 >(void) NOEXCEPT;
		{
			FXD::Container::Bitset< 8 > b8(0x10101010);
			PRINT_COND_ASSERT(b8.count() == 1, "Bitset - CONSTEXPR Bitset(unsigned long long nVal) NOEXCEPT; ");
			b8.flip();
			PRINT_COND_ASSERT(b8.count() == 7, "Bitset - Bitset& flip(void) NOEXCEPT");
			b8 <<= 1;
			PRINT_COND_ASSERT(b8.count() == 6, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
			b8.reset();
			b8.flip();
			b8 >>= 33;
			PRINT_COND_ASSERT(b8.count() == 0, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
			b8.reset();
			b8.flip();
			b8 >>= 65;
			PRINT_COND_ASSERT(b8.count() == 0, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
		}

		// CONSTEXPR FXD::Container::Bitset< 16 >(void) NOEXCEPT;
		{
			FXD::Container::Bitset< 16 > b16(0x10101010);
			PRINT_COND_ASSERT(b16.count() == 2, "Bitset - CONSTEXPR Bitset(unsigned long long nVal) NOEXCEPT; ");
			PRINT_COND_ASSERT(b16.to_ulong() == 0x00001010, "Bitset - unsigned long to_ulong(void) const");
			PRINT_COND_ASSERT(b16.to_ullong() == 0x00001010, "Bitset - unsigned long long to_ullong(void) const");
			b16.flip();
			PRINT_COND_ASSERT(b16.count() == 14, "Bitset - Bitset& flip(void) NOEXCEPT");
			PRINT_COND_ASSERT(b16.to_ulong() == 0x0000efef, "Bitset - unsigned long to_ulong(void) const");
			PRINT_COND_ASSERT(b16.to_ullong() == 0x0000efef, "Bitset - unsigned long long to_ullong(void) const");
			b16 <<= 1;
			PRINT_COND_ASSERT(b16.count() == 13, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
			PRINT_COND_ASSERT(b16.to_ulong() == 0x0000dfde, "Bitset - unsigned long to_ulong(void) const");
			PRINT_COND_ASSERT(b16.to_ullong() == 0x0000dfde, "Bitset - unsigned long long to_ullong(void) const");
			b16.reset();
			b16.flip();
			b16 >>= 33;
			PRINT_COND_ASSERT(b16.count() == 0, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
			b16.reset();
			b16.flip();
			b16 >>= 65;
			PRINT_COND_ASSERT(b16.count() == 0, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
		}

		// CONSTEXPR FXD::Container::Bitset< 32 >(void) NOEXCEPT;
		{
			FXD::Container::Bitset< 32 > b32(0x10101010);
			PRINT_COND_ASSERT(b32.count() == 4, "Bitset - CONSTEXPR Bitset(unsigned long long nVal) NOEXCEPT; ");
			PRINT_COND_ASSERT(b32.to_ulong() == 0x10101010, "Bitset - unsigned long to_ulong(void) const");
			PRINT_COND_ASSERT(b32.to_ullong() == 0x10101010, "Bitset - unsigned long long to_ullong(void) const");
			b32.flip();
			PRINT_COND_ASSERT(b32.count() == 28, "Bitset - Bitset& flip(void) NOEXCEPT");
			PRINT_COND_ASSERT(b32.to_ulong() == 0xefefefef, "Bitset - unsigned long to_ulong(void) const");
			PRINT_COND_ASSERT(b32.to_ullong() == 0xefefefef, "Bitset - unsigned long long to_ullong(void) const");
			b32 <<= 1;
			PRINT_COND_ASSERT(b32.count() == 27, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
			PRINT_COND_ASSERT(b32.to_ulong() == 0xdfdfdfde, "Bitset - unsigned long to_ulong(void) const");
			PRINT_COND_ASSERT(b32.to_ullong() == 0xdfdfdfde, "Bitset - unsigned long long to_ullong(void) const");
			b32.reset();
			b32.flip();
			b32 >>= 33;
			PRINT_COND_ASSERT(b32.count() == 0, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
			b32.reset();
			b32.flip();
			b32 >>= 65;
			PRINT_COND_ASSERT(b32.count() == 0, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
		}


		// CONSTEXPR FXD::Container::Bitset< 64 >(void) NOEXCEPT;
		{
			FXD::Container::Bitset< 64 > b64(0x10101010);			// b64 => 00000000 00000000 00000000 00000000 00010000 00010000 00010000 00010000
			PRINT_COND_ASSERT(b64.count() == 4, "");
			PRINT_COND_ASSERT(b64.to_ulong() == 0x10101010, "");
			PRINT_COND_ASSERT(b64.to_ullong() == 0x10101010, "");
			b64.flip();												// b64 => 11111111 11111111 11111111 11111111 11101111 11101111 11101111 11101111
			PRINT_COND_ASSERT(b64.count() == 60, "");
			/*
			if (sizeof(unsigned long) + nErrorCount - nErrorCount == 4) // We have this no-op math here in order to avoid compiler warnings about constant expressions.
			{
#if EASTL_EXCEPTIONS_ENABLED
				try {
					PRINT_COND_ASSERT(b64.to_ulong() == 0xefefefef);
					PRINT_COND_ASSERT(false);
				}
				catch (std::overflow_error&)
				{
					PRINT_COND_ASSERT(true); // This pathway should be taken.
				}
				catch (...)
				{
					PRINT_COND_ASSERT(false);
				}
#else
				PRINT_COND_ASSERT(b64.to_ulong() == 0xefefefef);
#endif
			}
			else
			{
				PRINT_COND_ASSERT(b64.to_ulong() == (unsigned long)UINT64_C(0xffffffffefefefef));
			}

			b64 <<= 1;                                                  // b64 => 11111111 11111111 11111111 11111111 11011111 11011111 11011111 11011110
			PRINT_COND_ASSERT(b64.count() == 59, "");
			if (sizeof(unsigned long) + nErrorCount - nErrorCount == 4)
			{
#if !EASTL_EXCEPTIONS_ENABLED
				PRINT_COND_ASSERT(b64.to_ulong() == 0xdfdfdfde);
#endif
			}
			else
			{
				PRINT_COND_ASSERT(b64.to_ulong() == (unsigned long)UINT64_C(0xffffffffdfdfdfde));
			}

			b64.reset();                                                // b64 => 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000
			PRINT_COND_ASSERT(b64.count() == 0);
			PRINT_COND_ASSERT(b64.to_ulong() == 0);

			b64 <<= 1;                                                  // b64 => 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000
			PRINT_COND_ASSERT(b64.count() == 0);
			PRINT_COND_ASSERT(b64.to_ulong() == 0);

			b64.flip();                                                 // b64 => 11111111 11111111 11111111 11111111 11111111 11111111 11111111 11111111
			PRINT_COND_ASSERT(b64.count() == 64);
			if (sizeof(unsigned long) + nErrorCount - nErrorCount == 4)
			{
#if !EASTL_EXCEPTIONS_ENABLED
				PRINT_COND_ASSERT(b64.to_ulong() == 0xffffffff);
#endif
			}
			else
			{
				PRINT_COND_ASSERT(b64.to_ulong() == (unsigned long)UINT64_C(0xffffffffffffffff));
			}

			b64 <<= 1;                                                  // b64 => 11111111 11111111 11111111 11111111 11111111 11111111 11111111 11111110
			PRINT_COND_ASSERT(b64.count() == 63);
			if (sizeof(unsigned long) + nErrorCount - nErrorCount == 4)
			{
#if !EASTL_EXCEPTIONS_ENABLED
				PRINT_COND_ASSERT(b64.to_ulong() == 0xfffffffe);
#endif
			}
			else
			{
				PRINT_COND_ASSERT(b64.to_ulong() == (unsigned long)UINT64_C(0xfffffffffffffffe));
			}

			b64.reset();
			b64.flip();
			b64 >>= 33;
			PRINT_COND_ASSERT(b64.count() == 31);

			b64.reset();
			b64.flip();
			b64 >>= 65;
			PRINT_COND_ASSERT(b64.count() == 0);

			b64.from_uint32(0x10101010);
			PRINT_COND_ASSERT(b64.to_uint32() == 0x10101010);
			b64.from_uint64(UINT64_C(0x1010101010101010));
			PRINT_COND_ASSERT(b64.to_uint64() == UINT64_C(0x1010101010101010));
			*/
		}
	}

	// CONSTEXPR size_t size(void) const NOEXCEPT;
	// bool any(void) const NOEXCEPT;
	// bool all(void) const NOEXCEPT;
	// bool none(void) const NOEXCEPT;
	// unsigned long to_ulong(void) const;
	// unsigned long long to_ullong(void) const;
	{
		FXD::Container::Bitset< 1 > b1;
		FXD::Container::Bitset< 1 > b1A(1);
		{

			PRINT_COND_ASSERT(b1.size() == 1, "Bitset - CONSTEXPR size_t size(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b1.any() == false, "Bitset - bool any(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b1.all() == false, "Bitset - bool all(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b1.none() == true, "Bitset - bool none(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b1.to_ulong() == 0, "Bitset - unsigned long to_ulong(void) const");
			PRINT_COND_ASSERT(b1.to_ullong() == 0, "Bitset - unsigned long long to_ullong(void) const");

			PRINT_COND_ASSERT(b1A.size() == 1, "Bitset - CONSTEXPR size_t size(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b1A.any() == true, "Bitset - bool any(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b1A.all() == true, "Bitset - bool all(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b1A.none() == false, "Bitset - bool none(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b1A.to_ulong() == 1, "Bitset - unsigned long to_ulong(void) const");
			PRINT_COND_ASSERT(b1A.to_ullong() == 1, "Bitset - unsigned long long to_ullong(void) const");
		}

		FXD::Container::Bitset< 33 > b33;
		FXD::Container::Bitset< 33 > b33A(1);
		{

			PRINT_COND_ASSERT(b33.size() == 33, "Bitset - CONSTEXPR size_t size(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b33.any() == false, "Bitset - bool any(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b33.all() == false, "Bitset - bool all(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b33.none() == true, "Bitset - bool none(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b33.to_ulong() == 0, "Bitset - unsigned long to_ulong(void) const");
			PRINT_COND_ASSERT(b33.to_ullong() == 0, "Bitset - unsigned long long to_ullong(void) const");

			PRINT_COND_ASSERT(b33A.size() == 33, "Bitset - CONSTEXPR size_t size(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b33A.any() == true, "Bitset - bool any(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b33A.all() == false, "Bitset - bool all(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b33A.none() == false, "Bitset - bool none(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b33A.to_ulong() == 1, "Bitset - unsigned long to_ulong(void) const");
			PRINT_COND_ASSERT(b33A.to_ullong() == 1, "Bitset - unsigned long long to_ullong(void) const");
		}

		FXD::Container::Bitset< 65 > b65;
		FXD::Container::Bitset< 65 > b65A(1);
		{

			PRINT_COND_ASSERT(b65.size() == 65, "Bitset - CONSTEXPR size_t size(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b65.any() == false, "Bitset - bool any(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b65.all() == false, "Bitset - bool all(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b65.none() == true, "Bitset - bool none(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b65.to_ulong() == 0, "Bitset - unsigned long to_ulong(void) const");
			PRINT_COND_ASSERT(b65.to_ullong() == 0, "Bitset - unsigned long long to_ullong(void) const");

			PRINT_COND_ASSERT(b65A.size() == 65, "Bitset - CONSTEXPR size_t size(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b65A.any() == true, "Bitset - bool any(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b65A.all() == false, "Bitset - bool all(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b65A.none() == false, "Bitset - bool none(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b65A.to_ulong() == 1, "Bitset - unsigned long to_ulong(void) const");
			PRINT_COND_ASSERT(b65A.to_ullong() == 1, "Bitset - unsigned long long to_ullong(void) const");
		}

		FXD::Container::Bitset< 129 > b129;
		FXD::Container::Bitset< 129 > b129A(1);
		{
			PRINT_COND_ASSERT(b129.size() == 129, "Bitset - CONSTEXPR size_t size(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b129.any() == false, "Bitset - bool any(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b129.all() == false, "Bitset - bool none(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b129.none() == true, "Bitset - bool none(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b129.to_ulong() == 0, "Bitset - unsigned long to_ulong(void) const");
			PRINT_COND_ASSERT(b129.to_ullong() == 0, "Bitset - unsigned long long to_ullong(void) const");

			PRINT_COND_ASSERT(b129A.size() == 129, "Bitset - CONSTEXPR size_t size(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b129A.any() == true, "Bitset - bool any(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b129A.all() == false, "Bitset - bool none(void) const NOEXCEPT");
			PRINT_COND_ASSERT(b129A.none() == false, "Bitset - bool none(void) const NOEXCEPT; ");
			PRINT_COND_ASSERT(b129A.to_ulong() == 1, "Bitset - unsigned long to_ulong(void) const");
			PRINT_COND_ASSERT(b129A.to_ullong() == 1, "Bitset - unsigned long long to_ullong(void) const");
		}

		// reference operator[](size_t nPos);
		// bool test(size_t nPos) const;
		// size_t count(void) const NOEXCEPT;
		{
			{
				b1[0] = true;
				PRINT_COND_ASSERT(b1.test(0) == true, "");
				PRINT_COND_ASSERT(b1.count() == 1, "");
			}
			{
				b33[0] = true;
				b33[32] = true;
				PRINT_COND_ASSERT(b33.test(0) == true, "");
				PRINT_COND_ASSERT(b33.test(15) == false, "");
				PRINT_COND_ASSERT(b33.test(32) == true, "");
				PRINT_COND_ASSERT(b33.count() == 2, "");
			}
			{
				b65[0] = true;
				b65[32] = true;
				b65[64] = true;
				PRINT_COND_ASSERT(b65.test(0) == true, "");
				PRINT_COND_ASSERT(b65.test(15) == false, "");
				PRINT_COND_ASSERT(b65.test(32) == true, "");
				PRINT_COND_ASSERT(b65.test(47) == false, "");
				PRINT_COND_ASSERT(b65.test(64) == true, "");
				PRINT_COND_ASSERT(b65.count() == 3, "");
			}
			{
				b129[0] = true;
				b129[32] = true;
				b129[64] = true;
				b129[128] = true;
				PRINT_COND_ASSERT(b129.test(0) == true, "");
				PRINT_COND_ASSERT(b129.test(15) == false, "");
				PRINT_COND_ASSERT(b129.test(32) == true, "");
				PRINT_COND_ASSERT(b129.test(47) == false, "");
				PRINT_COND_ASSERT(b129.test(64) == true, "");
				PRINT_COND_ASSERT(b129.test(91) == false, "");
				PRINT_COND_ASSERT(b129.test(128) == true, "");
				PRINT_COND_ASSERT(b129.count() == 4, "");
			}
		}

		// Bitset& reset(void) NOEXCEPT;
		// Bitset& reset(size_t nPos);
		// Bitset& set(void) NOEXCEPT;
		// Bitset& set(size_t nPos, bool bVal = true);
		// Bitset& flip(void) NOEXCEPT;
		// Bitset& flip(size_t nPos);
		{
			{
				b1.reset();
				PRINT_COND_ASSERT(b1.count() == 0, "Bitset - Bitset& reset(void) NOEXCEPT");
				PRINT_COND_ASSERT(!b1.all(), "Bitset - Bitset& reset(void) NOEXCEPT");
				PRINT_COND_ASSERT(b1.none(), "Bitset - Bitset& reset(void) NOEXCEPT");

				b1.set();
				PRINT_COND_ASSERT(b1.count() == b1.size(), "Bitset - Bitset& set(void) NOEXCEPT");
				PRINT_COND_ASSERT(b1.all(), "Bitset - Bitset& set(void) NOEXCEPT");
				PRINT_COND_ASSERT(!b1.none(), "Bitset - Bitset& set(void) NOEXCEPT");

				b1.flip();
				PRINT_COND_ASSERT(b1.count() == 0, "Bitset - Bitset& flip(void) NOEXCEPT");
				PRINT_COND_ASSERT(!b1.all(), "Bitset - Bitset& flip(void) NOEXCEPT");
				PRINT_COND_ASSERT(b1.none(), "Bitset - Bitset& flip(void) NOEXCEPT");

				b1.set(0, true);
				PRINT_COND_ASSERT(b1[0] == true, "Bitset - Bitset& set(size_t nPos, bool bVal = true)");

				b1.reset(0);
				PRINT_COND_ASSERT(b1[0] == false, "Bitset - Bitset& reset(size_t nPos)");

				b1.flip(0);
				PRINT_COND_ASSERT(b1[0] == true, "Bitset - Bitset& flip(size_t nPos)");
			}
			{
				b33.reset();
				PRINT_COND_ASSERT(b33.count() == 0, "Bitset - Bitset& reset(void) NOEXCEPT");
				PRINT_COND_ASSERT(!b33.all(), "Bitset - Bitset& reset(void) NOEXCEPT");
				PRINT_COND_ASSERT(b33.none(), "Bitset - Bitset& reset(void) NOEXCEPT");

				b33.set();
				PRINT_COND_ASSERT(b33.count() == b33.size(), "Bitset - Bitset& set(void) NOEXCEPT");
				PRINT_COND_ASSERT(b33.all(), "Bitset - Bitset& set(void) NOEXCEPT");
				PRINT_COND_ASSERT(!b33.none(), "Bitset - Bitset& set(void) NOEXCEPT");

				b33.flip();
				PRINT_COND_ASSERT(b33.count() == 0, "Bitset - Bitset& flip(void) NOEXCEPT");
				PRINT_COND_ASSERT(!b33.all(), "Bitset - Bitset& flip(void) NOEXCEPT");
				PRINT_COND_ASSERT(b33.none(), "Bitset - Bitset& flip(void) NOEXCEPT");

				b33.set(0, true);
				b33.set(32, true);
				PRINT_COND_ASSERT(b33[0] == true, "Bitset - Bitset& set(size_t nPos, bool bVal = true)");
				PRINT_COND_ASSERT(b33[15] == false, "Bitset - Bitset& set(size_t nPos, bool bVal = true)");
				PRINT_COND_ASSERT(b33[32] == true, "Bitset - Bitset& set(size_t nPos, bool bVal = true)");

				b33.reset(0);
				b33.reset(32);
				PRINT_COND_ASSERT(b33[0] == false, "Bitset - Bitset& reset(size_t nPos)");
				PRINT_COND_ASSERT(b33[32] == false, "Bitset - Bitset& reset(size_t nPos)");

				b33.flip(0);
				b33.flip(32);
				PRINT_COND_ASSERT(b33[0] == true, "Bitset - Bitset& flip(size_t nPos)");
				PRINT_COND_ASSERT(b33[32] == true, "Bitset - Bitset& flip(size_t nPos)");
			}
			{
				b65.reset();
				PRINT_COND_ASSERT(b65.count() == 0, "Bitset - Bitset& reset(void) NOEXCEPT");
				PRINT_COND_ASSERT(!b65.all(), "Bitset - Bitset& reset(void) NOEXCEPT");
				PRINT_COND_ASSERT(b65.none(), "Bitset - Bitset& reset(void) NOEXCEPT");

				b65.set();
				PRINT_COND_ASSERT(b65.count() == b65.size(), "Bitset - Bitset& set(void) NOEXCEPT");
				PRINT_COND_ASSERT(b65.all(), "Bitset - Bitset& set(void) NOEXCEPT");
				PRINT_COND_ASSERT(!b65.none(), "Bitset - Bitset& set(void) NOEXCEPT");

				b65.flip();
				PRINT_COND_ASSERT(b65.count() == 0, "Bitset - Bitset& flip(void) NOEXCEPT");
				PRINT_COND_ASSERT(!b65.all(), "Bitset - Bitset& flip(void) NOEXCEPT");
				PRINT_COND_ASSERT(b65.none(), "Bitset - Bitset& flip(void) NOEXCEPT");

				b65.set(0, true);
				b65.set(32, true);
				b65.set(64, true);
				PRINT_COND_ASSERT(b65[0] == true, "Bitset - Bitset& set(size_t nPos, bool bVal = true)");
				PRINT_COND_ASSERT(b65[15] == false, "Bitset - Bitset& set(size_t nPos, bool bVal = true)");
				PRINT_COND_ASSERT(b65[32] == true, "Bitset - Bitset& set(size_t nPos, bool bVal = true)");
				PRINT_COND_ASSERT(b65[50] == false, "Bitset - Bitset& set(size_t nPos, bool bVal = true)");
				PRINT_COND_ASSERT(b65[64] == true, "Bitset - Bitset& set(size_t nPos, bool bVal = true)");

				b65.reset(0);
				b65.reset(32);
				b65.reset(64);
				PRINT_COND_ASSERT(b65[0] == false, "Bitset - Bitset& reset(size_t nPos)");
				PRINT_COND_ASSERT(b65[32] == false, "Bitset - Bitset& reset(size_t nPos)");
				PRINT_COND_ASSERT(b65[64] == false, "Bitset - Bitset& reset(size_t nPos)");

				b65.flip(0);
				b65.flip(32);
				b65.flip(64);
				PRINT_COND_ASSERT(b65[0] == true, "Bitset - Bitset& flip(size_t nPos)");
				PRINT_COND_ASSERT(b65[32] == true, "Bitset - Bitset& flip(size_t nPos)");
				PRINT_COND_ASSERT(b65[64] == true, "Bitset - Bitset& flip(size_t nPos)");
			}
			{
				b129.reset();
				PRINT_COND_ASSERT(b129.count() == 0, "Bitset - Bitset& reset(void) NOEXCEPT");
				PRINT_COND_ASSERT(!b129.all(), "Bitset - Bitset& reset(void) NOEXCEPT");
				PRINT_COND_ASSERT(b129.none(), "Bitset - Bitset& reset(void) NOEXCEPT");

				b129.set();
				PRINT_COND_ASSERT(b129.count() == b129.size(), "Bitset - Bitset& set(void) NOEXCEPT");
				PRINT_COND_ASSERT(b129.all(), "Bitset - Bitset& set(void) NOEXCEPT");
				PRINT_COND_ASSERT(!b129.none(), "Bitset - Bitset& set(void) NOEXCEPT");

				b129.flip();
				PRINT_COND_ASSERT(b129.count() == 0, "Bitset - Bitset& flip(void) NOEXCEPT");
				PRINT_COND_ASSERT(!b129.all(), "Bitset - Bitset& flip(void) NOEXCEPT");
				PRINT_COND_ASSERT(b129.none(), "Bitset - Bitset& flip(void) NOEXCEPT");

				b129.set(0, true);
				b129.set(32, true);
				b129.set(64, true);
				b129.set(128, true);
				PRINT_COND_ASSERT(b129[0] == true, "Bitset - Bitset& set(size_t nPos, bool bVal = true)");
				PRINT_COND_ASSERT(b129[15] == false, "Bitset - Bitset& set(size_t nPos, bool bVal = true)");
				PRINT_COND_ASSERT(b129[32] == true, "Bitset - Bitset& set(size_t nPos, bool bVal = true)");
				PRINT_COND_ASSERT(b129[50] == false, "Bitset - Bitset& set(size_t nPos, bool bVal = true)");
				PRINT_COND_ASSERT(b129[64] == true, "Bitset - Bitset& set(size_t nPos, bool bVal = true)");
				PRINT_COND_ASSERT(b129[90] == false, "Bitset - Bitset& set(size_t nPos, bool bVal = true)");
				PRINT_COND_ASSERT(b129[128] == true, "Bitset - Bitset& set(size_t nPos, bool bVal = true)");

				b129.reset(0);
				b129.reset(32);
				b129.reset(64);
				b129.reset(128);
				PRINT_COND_ASSERT(b129[0] == false, "Bitset - Bitset& reset(size_t nPos)");
				PRINT_COND_ASSERT(b129[32] == false, "Bitset - Bitset& reset(size_t nPos)");
				PRINT_COND_ASSERT(b129[64] == false, "Bitset - Bitset& reset(size_t nPos)");
				PRINT_COND_ASSERT(b129[128] == false, "Bitset - Bitset& reset(size_t nPos)");

				b129.flip(0);
				b129.flip(32);
				b129.flip(64);
				b129.flip(128);
				PRINT_COND_ASSERT(b129[0] == true, "Bitset - Bitset& flip(size_t nPos)");
				PRINT_COND_ASSERT(b129[32] == true, "Bitset - Bitset& flip(size_t nPos)");
				PRINT_COND_ASSERT(b129[64] == true, "Bitset - Bitset& flip(size_t nPos)");
				PRINT_COND_ASSERT(b129[128] == true, "Bitset - Bitset& flip(size_t nPos)");
			}
		}

		// operator~(void) const NOEXCEPT;
		{
			{

				FXD::Container::Bitset< 1 > b1Not = ~b1;
				PRINT_COND_ASSERT(b1[0] == true, "Bitset - bool operator~(void) const NOEXCEPT");
				PRINT_COND_ASSERT(b1Not[0] == false, "Bitset - bool operator~(void) const NOEXCEPT");
			}
			{
				FXD::Container::Bitset< 33 > b33Not(~b33);
				PRINT_COND_ASSERT(b33[0] == true, "Bitset - bool operator~(void) const NOEXCEPT");
				PRINT_COND_ASSERT(b33[32] == true, "Bitset - bool operator~(void) const NOEXCEPT");
				PRINT_COND_ASSERT(b33Not[0] == false, "Bitset - bool operator~(void) const NOEXCEPT");
				PRINT_COND_ASSERT(b33Not[32] == false, "Bitset - bool operator~(void) const NOEXCEPT");
			}
			{
				FXD::Container::Bitset< 65 > b65Not(~b65);
				PRINT_COND_ASSERT(b65[0] == true, "Bitset - bool operator~(void) const NOEXCEPT");
				PRINT_COND_ASSERT(b65[32] == true, "Bitset - bool operator~(void) const NOEXCEPT");
				PRINT_COND_ASSERT(b65[64] == true, "Bitset - bool operator~(void) const NOEXCEPT");
				PRINT_COND_ASSERT(b65Not[0] == false, "Bitset - bool operator~(void) const NOEXCEPT");
				PRINT_COND_ASSERT(b65Not[32] == false, "Bitset - bool operator~(void) const NOEXCEPT");
				PRINT_COND_ASSERT(b65Not[64] == false, "Bitset - bool operator~(void) const NOEXCEPT");
			}
			{
				FXD::Container::Bitset< 129 > b129Not(~b129);
				PRINT_COND_ASSERT(b129[0] == true, "Bitset - bool operator~(void) const NOEXCEPT");
				PRINT_COND_ASSERT(b129[32] == true, "Bitset - bool operator~(void) const NOEXCEPT");
				PRINT_COND_ASSERT(b129[64] == true, "Bitset - bool operator~(void) const NOEXCEPT");
				PRINT_COND_ASSERT(b129[128] == true, "Bitset - bool operator~(void) const NOEXCEPT");
				PRINT_COND_ASSERT(b129Not[0] == false, "Bitset - bool operator~(void) const NOEXCEPT");
				PRINT_COND_ASSERT(b129Not[32] == false, "Bitset - bool operator~(void) const NOEXCEPT");
				PRINT_COND_ASSERT(b129Not[64] == false, "Bitset - bool operator~(void) const NOEXCEPT");
				PRINT_COND_ASSERT(b129Not[128] == false, "Bitset - bool operator~(void) const NOEXCEPT");
			}
		}

		// bool operator==(const Bitset& rhs) const NOEXCEPT;
		// bool operator!=(const Bitset& rhs) const NOEXCEPT;
		{
			{
				FXD::Container::Bitset< 1 > b1Equal(b1);
				FXD::Container::Bitset< 1 > b1Not = ~b1;
				PRINT_COND_ASSERT(b1Equal == b1, "Bitset - bool operator==(const Bitset& rhs) const NOEXCEPT");
				PRINT_COND_ASSERT(b1Equal != b1Not, "Bitset - bool operator!=(const Bitset& rhs) const NOEXCEPT");
			}

			{
				FXD::Container::Bitset< 33 > b33Equal(b33);
				FXD::Container::Bitset< 33 > b33Not = ~b33Equal;
				PRINT_COND_ASSERT(b33Equal == b33, "Bitset - bool operator==(const Bitset& rhs) const NOEXCEPT");
				PRINT_COND_ASSERT(b33Equal != b33Not, "Bitset - bool operator!=(const Bitset& rhs) const NOEXCEPT");
			}

			{
				FXD::Container::Bitset< 65 > b65Equal(b65);
				FXD::Container::Bitset< 65 > b65Not = ~b65Equal;
				PRINT_COND_ASSERT(b65Equal == b65, "Bitset - bool operator==(const Bitset& rhs) const NOEXCEPT");
				PRINT_COND_ASSERT(b65Equal != b65Not, "Bitset - bool operator!=(const Bitset& rhs) const NOEXCEPT");
			}

			{
				FXD::Container::Bitset< 129 > b129Equal(b129);
				FXD::Container::Bitset< 129 > b129Not = ~b129Equal;
				PRINT_COND_ASSERT(b129Equal == b129, "Bitset - bool operator==(const Bitset& rhs) const NOEXCEPT");
				PRINT_COND_ASSERT(b129Equal != b129Not, "Bitset - bool operator!=(const Bitset& rhs) const NOEXCEPT");
			}
		}

		// Bitset operator<<(size_t nPos) const NOEXCEPT;
		// Bitset operator>>(size_t nPos) const NOEXCEPT;
		// Bitset& operator<<=(size_t nPos) NOEXCEPT;
		// Bitset& operator>>=(size_t nPos) NOEXCEPT;
		{
			{
				FXD::Container::Bitset< 1 > b1Equal(b1);
				b1.reset();

				b1[0] = true;
				b1 >>= 0;
				PRINT_COND_ASSERT(b1[0] == true, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				b1 >>= 1;
				PRINT_COND_ASSERT(b1[0] == false, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");

				b1[0] = true;
				b1 <<= 0;
				PRINT_COND_ASSERT(b1[0] == true, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				b1 <<= 1;
				PRINT_COND_ASSERT(b1[0] == false, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");

				b1[0] = true;
				b1Equal = b1 >> 0;
				PRINT_COND_ASSERT(b1Equal == b1, "Bitset - Bitset operator>>(size_t nPos) NOEXCEPT");
				b1Equal = b1 >> 1;
				PRINT_COND_ASSERT(b1Equal[0] == false, "Bitset - Bitset operator>>(size_t nPos) NOEXCEPT");

				b1[0] = true;
				b1Equal = b1 << 0;
				PRINT_COND_ASSERT(b1Equal[0] == true, "Bitset - Bitset operator<<(size_t nPos) NOEXCEPT");
				b1Equal = b1 << 1;
				PRINT_COND_ASSERT(b1Equal[0] == false, "Bitset - Bitset operator<<(size_t nPos) NOEXCEPT");

				b1.reset();
				b1.flip();
				b1 >>= 33;
				PRINT_COND_ASSERT(b1.count() == 0, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(!b1.all(), "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b1.none(), "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");

				b1.reset();
				b1.flip();
				b1 <<= 33;
				PRINT_COND_ASSERT(b1.count() == 0, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(!b1.all(), "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b1.none(), "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");

				b1.reset();
				b1.flip();
				b1 >>= 65;
				PRINT_COND_ASSERT(b1.count() == 0, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(!b1.all(), "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b1.none(), "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");

				b1.reset();
				b1.flip();
				b1 <<= 65;
				PRINT_COND_ASSERT(b1.count() == 0, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(!b1.all(), "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b1.none(), "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
			}
			{
				FXD::Container::Bitset< 33 > b33Equal(b33);
				b33.reset();

				b33[0] = true;
				b33[32] = true;
				b33 >>= 0;
				PRINT_COND_ASSERT(b33[0] == true, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b33[32] == true, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				b33 >>= 10;
				PRINT_COND_ASSERT(b33[22] == true, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");

				b33.reset();
				b33[0] = true;
				b33[32] = true;
				b33 <<= 0;
				PRINT_COND_ASSERT(b33[0] == true, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b33[32] == true, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				b33 <<= 10;
				PRINT_COND_ASSERT(b33[10] == true, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");

				b33.reset();
				b33[0] = true;
				b33[32] = true;
				b33Equal = b33 >> 0;
				PRINT_COND_ASSERT(b33Equal == b33, "Bitset - Bitset operator>>(size_t nPos) NOEXCEPT");
				b33Equal = b33 >> 10;
				PRINT_COND_ASSERT(b33Equal[22] == true, "Bitset - Bitset operator>>(size_t nPos) NOEXCEPT");

				b33.reset();
				b33[0] = true;
				b33[32] = true;
				b33Equal = b33 << 10;
				PRINT_COND_ASSERT(b33Equal[10] == true, "Bitset - Bitset operator<<(size_t nPos) NOEXCEPT");

				b33.reset();
				b33.flip();
				b33 >>= 33;
				PRINT_COND_ASSERT(b33.count() == 0, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(!b33.all(), "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b33.none(), "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");

				b33.reset();
				b33.flip();
				b33 <<= 33;
				PRINT_COND_ASSERT(b33.count() == 0, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(!b33.all(), "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b33.none(), "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");

				b33.reset();
				b33.flip();
				b33 >>= 65;
				PRINT_COND_ASSERT(b33.count() == 0, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(!b33.all(), "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b33.none(), "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");

				b33.reset();
				b33.flip();
				b33 <<= 65;
				PRINT_COND_ASSERT(b33.count() == 0, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(!b33.all(), "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b33.none(), "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
			}
			{
				FXD::Container::Bitset< 65 > b65Equal(b65);
				b65.reset();

				b65[0] = true;
				b65[32] = true;
				b65[64] = true;
				b65 >>= 0;
				PRINT_COND_ASSERT(b65[0] == true, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b65[32] == true, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b65[64] == true, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				b65 >>= 10;
				PRINT_COND_ASSERT(b65[22] == true, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b65[54] == true, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");

				b65.reset();
				b65[0] = true;
				b65[32] = true;
				b65[64] = true;
				b65 <<= 0;
				PRINT_COND_ASSERT(b65[0] == true, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b65[32] == true, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b65[64] == true, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				b65 <<= 10;
				PRINT_COND_ASSERT(b65[10] == true, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b65[42] == true, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");

				b65.reset();
				b65[0] = true;
				b65[32] = true;
				b65[64] = true;
				b65Equal = b65 >> 0;
				PRINT_COND_ASSERT(b65Equal == b65, "Bitset - Bitset operator>>(size_t nPos) NOEXCEPT");
				b65Equal = b65 >> 10;
				PRINT_COND_ASSERT(b65Equal[22] == true, "Bitset - Bitset operator>>(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b65Equal[54] == true, "Bitset - Bitset operator>>(size_t nPos) NOEXCEPT");

				b65.reset();
				b65[0] = true;
				b65[32] = true;
				b65[64] = true;
				b65Equal = b65 << 10;
				PRINT_COND_ASSERT(b65Equal[10] == true, "Bitset - Bitset operator<<(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b65Equal[42] == true, "Bitset - Bitset operator<<(size_t nPos) NOEXCEPT");

				b65.reset();
				b65.flip();
				b65 >>= 33;
				PRINT_COND_ASSERT(b65.count() == 32, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");

				b65.reset();
				b65.flip();
				b65 <<= 33;
				PRINT_COND_ASSERT(b65.count() == 32, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");

				b65.reset();
				b65.flip();
				b65 >>= 65;
				PRINT_COND_ASSERT(b65.count() == 0, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");

				b65.reset();
				b65.flip();
				b65 <<= 65;
				PRINT_COND_ASSERT(b65.count() == 0, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
			}
			{
				FXD::Container::Bitset< 129 > b129Equal(b129);
				b129.reset();

				b129[0] = true;
				b129[32] = true;
				b129[64] = true;
				b129[128] = true;
				b129 >>= 0;
				PRINT_COND_ASSERT(b129[0] == true, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b129[32] == true, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b129[64] == true, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b129[128] == true, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				b129 >>= 10;
				PRINT_COND_ASSERT(b129[22] == true, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b129[54] == true, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b129[118] == true, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");

				b129.reset();
				b129[0] = true;
				b129[32] = true;
				b129[64] = true;
				b129[128] = true;
				b129 <<= 0;
				PRINT_COND_ASSERT(b129[0] == true, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b129[32] == true, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b129[64] == true, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b129[128] == true, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				b129 <<= 10;
				PRINT_COND_ASSERT(b129[10] == true, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b129[42] == true, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b129[74] == true, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");

				b129.reset();
				b129[0] = true;
				b129[32] = true;
				b129[64] = true;
				b129[128] = true;
				b129Equal = b129 >> 0;
				PRINT_COND_ASSERT(b129Equal == b129, "Bitset - Bitset operator>>(size_t nPos) NOEXCEPT");
				b129Equal = b129 >> 10;
				PRINT_COND_ASSERT(b129Equal[22] == true, "Bitset - Bitset operator>>(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b129Equal[54] == true, "Bitset - Bitset operator>>(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b129Equal[118] == true, "Bitset - Bitset operator>>(size_t nPos) NOEXCEPT");

				b129.reset();
				b129[0] = true;
				b129[32] = true;
				b129[64] = true;
				b129[128] = true;
				b129Equal = b129 << 10;
				PRINT_COND_ASSERT(b129Equal[10] == true, "Bitset - Bitset operator<<(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b129Equal[42] == true, "Bitset - Bitset operator<<(size_t nPos) NOEXCEPT");
				PRINT_COND_ASSERT(b129Equal[74] == true, "Bitset - Bitset operator<<(size_t nPos) NOEXCEPT");

				b129.reset();
				b129.flip();
				b129 >>= 33;
				PRINT_COND_ASSERT(b129.count() == 96, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");

				b129.reset();
				b129.flip();
				b129 <<= 33;
				PRINT_COND_ASSERT(b129.count() == 96, "Bitset - Bitset& operator>>=(size_t nPos) NOEXCEPT");

				b129.reset();
				b129.flip();
				b129 >>= 65;
				PRINT_COND_ASSERT(b129.count() == 64, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");

				b129.reset();
				b129.flip();
				b129 <<= 65;
				PRINT_COND_ASSERT(b129.count() == 64, "Bitset - Bitset& operator<<=(size_t nPos) NOEXCEPT");
			}
		}

		// Bitset& operator&=(const Bitset& rhs) NOEXCEPT;
		// Bitset& operator|=(const Bitset& rhs) NOEXCEPT;
		// Bitset& operator^=(const Bitset& rhs) NOEXCEPT;
		{
			{
				b1.set();
				b1[0] = false;
				b1A[0] = true;
				b1 &= b1A;
				PRINT_COND_ASSERT(b1[0] == false, "Bitset - Bitset& operator&=(const Bitset& rhs) NOEXCEPT");
				b1 |= b1A;
				PRINT_COND_ASSERT(b1[0] == true, "Bitset - Bitset& operator|=(const Bitset& rhs) NOEXCEPT");
				b1 ^= b1A;
				PRINT_COND_ASSERT(b1[0] == false, "Bitset - Bitset& operator|=(const Bitset& rhs) NOEXCEPT");
				b1 |= b1A;
				PRINT_COND_ASSERT(b1[0] == true, "Bitset - Bitset& operator|=(const Bitset& rhs) NOEXCEPT");
			}
			{
				b33.set();
				b33[0] = false;
				b33[32] = false;
				b33A[0] = true;
				b33A[32] = true;
				b33 &= b33A;
				PRINT_COND_ASSERT((b33[0] == false) && (b33[32] == false), "Bitset - Bitset& operator&=(const Bitset& rhs) NOEXCEPT");
				b33 |= b33A;
				PRINT_COND_ASSERT((b33[0] == true) && (b33[32] == true), "Bitset - Bitset& operator|=(const Bitset& rhs) NOEXCEPT");
				b33 ^= b33A;
				PRINT_COND_ASSERT((b33[0] == false) && (b33[32] == false), "Bitset - Bitset& operator|=(const Bitset& rhs) NOEXCEPT");
				b33 |= b33A;
				PRINT_COND_ASSERT((b33[0] == true) && (b33[32] == true), "Bitset - Bitset& operator|=(const Bitset& rhs) NOEXCEPT");
			}
			{
				b65.set();
				b65[0] = false;
				b65[32] = false;
				b65[64] = false;
				b65A[0] = true;
				b65A[32] = true;
				b65A[64] = true;
				b65 &= b65A;
				PRINT_COND_ASSERT((b65[0] == false) && (b65[32] == false) && (b65[64] == false), "Bitset - Bitset& operator&=(const Bitset& rhs) NOEXCEPT");
				b65 |= b65A;
				PRINT_COND_ASSERT((b65[0] == true) && (b65[32] == true) && (b65[64] == true), "Bitset - Bitset& operator|=(const Bitset& rhs) NOEXCEPT");
				b65 ^= b65A;
				PRINT_COND_ASSERT((b65[0] == false) && (b65[32] == false) && (b65[64] == false), "Bitset - Bitset& operator|=(const Bitset& rhs) NOEXCEPT");
				b65 |= b65A;
				PRINT_COND_ASSERT((b65[0] == true) && (b65[32] == true) && (b65[64] == true), "Bitset - Bitset& operator|=(const Bitset& rhs) NOEXCEPT");
			}
			{
				b129.set();
				b129[0] = false;
				b129[32] = false;
				b129[64] = false;
				b129[128] = false;
				b129A[0] = true;
				b129A[32] = true;
				b129A[64] = true;
				b129A[128] = true;
				b129 &= b129A;
				PRINT_COND_ASSERT((b129[0] == false) && (b129[32] == false) && (b129[64] == false) && (b129[128] == false), "Bitset - Bitset& operator&=(const Bitset& rhs) NOEXCEPT");
				b129 |= b129A;
				PRINT_COND_ASSERT((b129[0] == true) && (b129[32] == true) && (b129[64] == true) && (b129[128] == true), "Bitset - Bitset& operator|=(const Bitset& rhs) NOEXCEPT");
				b129 ^= b129A;
				PRINT_COND_ASSERT((b129[0] == false) && (b129[32] == false) && (b129[64] == false) && (b129[128] == false), "Bitset - Bitset& operator|=(const Bitset& rhs) NOEXCEPT");
				b129 |= b129A;
				PRINT_COND_ASSERT((b129[0] == true) && (b129[32] == true) && (b129[64] == true) && (b129[128] == true), "Bitset - Bitset& operator|=(const Bitset& rhs) NOEXCEPT");
			}
		}
	}

	// Test Bitset::reference
	{
		{
			FXD::Container::Bitset< 65 > b65;
			FXD::Container::Bitset< 65 >::reference ref = b65[33];

			ref = true;
			PRINT_COND_ASSERT(ref == true, "Bitset - Reference& operator=(bool bVal)");
		}
	}

	return nErrorCount;
}