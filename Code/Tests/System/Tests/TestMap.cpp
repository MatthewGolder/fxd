// Creator - MatthewGolder
#include "Tests/System/Tests/TestMap.h"
#include "FXDEngine/Container/Map.h"
#include "FXDEngine/Container/MultiMap.h"
#include "FXDEngine/Container/Vector.h"
#include <map>
namespace
{
	class _NonCopyable
	{
	public:
		_NonCopyable(void)
			: m_nVal(0)
		{}
		_NonCopyable(FXD::S32 nVal)
			: m_nVal(nVal)
		{}

		NO_COPY(_NonCopyable);

	public:
		FXD::S32 m_nVal;
	};

	inline bool operator<(const _NonCopyable& lhs, const _NonCopyable& rhs)
	{
		return (lhs.m_nVal < rhs.m_nVal);
	}
}

// Template instantations.
// These tell the compiler to compile all the functions for the given class.
template class FXD::Container::Map< FXD::S32, FXD::S32 >;
template class FXD::Container::MultiMap< FXD::S32, FXD::S32 >;
template class FXD::Container::Map< TestObject, TestObject >;
template class FXD::Container::MultiMap< TestObject, TestObject >;

///////////////////////////////////////////////////////////////////////////////
// typedefs
///////////////////////////////////////////////////////////////////////////////
using VM1 = FXD::Container::Map< FXD::S32, FXD::S32 >;
using VM4 = FXD::Container::Map< TestObject, TestObject >;
using VMM1 = FXD::Container::MultiMap< FXD::S32, FXD::S32 >;
using VMM4 = FXD::Container::MultiMap< TestObject, TestObject >;

using VM3 = std::map< FXD::S32, FXD::S32 >;
using VM6 = std::map< TestObject, TestObject >;
using VMM3 = std::multimap< FXD::S32, FXD::S32 >;
using VMM6 = std::multimap< TestObject, TestObject >;
///////////////////////////////////////////////////////////////////////////////

FXD::S32 TestMap(void)
{
	FXD::S32 nErrorCount = 0;

	{ // Test construction
		nErrorCount += TestMapConstruction< VM1, VM3, false >();
		nErrorCount += TestMapConstruction< VM4, VM6, false >();
		nErrorCount += TestMapConstruction< VMM1, VMM3, true >();
		nErrorCount += TestMapConstruction< VMM4, VMM6, true >();
	}

	{ // Test mutating functionality.
		nErrorCount += TestMapMutation< VM1, VM3, false >();
		nErrorCount += TestMapMutation< VM4, VM6, false >();
		nErrorCount += TestMapMutation< VMM1, VMM3, true >();
		nErrorCount += TestMapMutation< VMM4, VMM6, true >();
	}

	{ // Test searching functionality.
		nErrorCount += TestMapSearch< VM1, false >();
		nErrorCount += TestMapSearch< VM4, false >();
		nErrorCount += TestMapSearch< VMM1, true >();
		nErrorCount += TestMapSearch< VMM4, true >();
	}

	{ // C++11 emplace and related functionality
		nErrorCount += TestMapCpp11< FXD::Container::Map< FXD::S32, TestObject > >();
		nErrorCount += TestMultimapCpp11< FXD::Container::MultiMap< FXD::S32, TestObject > >();
		nErrorCount += TestMapCpp11NonCopyable< FXD::Container::Map< FXD::S32, _NonCopyable > >();
	}

	{ // C++17 try_emplace and related functionality
		nErrorCount += TestMapCpp17< FXD::Container::Map< FXD::S32, TestObject >>();
	}

	return nErrorCount;
}