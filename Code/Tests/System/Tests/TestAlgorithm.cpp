// Creator - MatthewGolder
#include "Tests/System/Tests/FXDTest.h"
#include "Tests/System/TestHelperClasses.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/Core/Algorithm.h"
#include "FXDEngine/Core/CharTraits.h"
#include "FXDEngine/Core/Numeric.h"
#include "FXDEngine/Core/Pair.h"
#include "FXDEngine/Core/Random.h"
#include "FXDEngine/Core/String.h"
#include "FXDEngine/Container/Array.h"
#include "FXDEngine/Container/Deque.h"
#include "FXDEngine/Container/LinkedList.h"
#include "FXDEngine/Math/Math.h"
#include <algorithm>

namespace
{
	struct _StructA
	{
		_StructA(FXD::S32 nA)
			: m_nA(nA)
		{}
		FXD::S32 m_nA;
	};
	struct _StructB
	{
		_StructB(FXD::S32 nB)
			: m_nB(nB)
		{}
		FXD::S32 m_nB;
	};

	struct _LessStruct
	{
		bool operator()(const _StructA& lhs, const _StructA& rhs)
		{
			return (lhs.m_nA < rhs.m_nA);
		}
	};

	inline bool _LessFunction(const _StructB& lhs, const _StructB& rhs)
	{
		return (lhs.m_nB < rhs.m_nB);
	}

	template < typename T >
	struct _SetIncrementalIntegers
	{
		_SetIncrementalIntegers(FXD::S32 nX = 0)
			: m_nX(nX)
		{}

		void reset(FXD::S32 nX = 0)
		{
			m_nX = nX;
		}

		void operator()(T& t)
		{
			t = T(m_nX++);
		}

		FXD::S32 m_nX;
	};


	template < typename T >
	struct _GenerateIncrementalIntegers
	{
		_GenerateIncrementalIntegers(FXD::S32 nX = 0)
			: m_nX(nX)
		{}

		void reset(FXD::S32 nX = 0)
		{
			m_nX = nX;
		}

		T operator()()
		{
			return T(m_nX++);
		}

		FXD::S32 m_nX;
	};

	template < typename T >
	struct _Greater : public FXD::STD::binary_function< T, T, bool >
	{
		bool operator()(const T& lhs, const T& rhs)const
		{
			return (rhs < lhs);
		}
	};

	struct _DivisibleBy
	{
		_DivisibleBy(FXD::S32 nInt = 1)
			: m_nInt(nInt)
		{}

		bool operator()(FXD::S32 nInt) const
		{
			return ((nInt % m_nInt) == 0);
		}

		FXD::S32 m_nInt;
	};


	struct _TestObjectNegate : public FXD::STD::unary_function< TestObject, TestObject >
	{
		TestObject operator()(const TestObject& rhs)const
		{
			return TestObject(-rhs.m_nX);
		}
	};
}

static FXD::S32 TestNonModifyingSequences(void)
{
	FXD::S32 nErrorCount = 0;

	// inline bool all_of(InputItr first, InputItr last, Predicate pred);
	// inline bool any_of(InputItr first, InputItr last, Predicate pred);
	// inline bool none_of(InputItr first, InputItr last, Predicate pred);
	{
		{
			FXD::Container::Vector< FXD::S32 > vecInt;
			vecInt.push_back(2);
			vecInt.push_back(4);
			vecInt.push_back(6);
			vecInt.push_back(8);

			PRINT_COND_ASSERT(FXD::STD::all_of(vecInt.begin(), vecInt.end(), _DivisibleBy(2)), "Algorithm - inline bool all_of(InputItr first, InputItr last, Predicate pred)");
			PRINT_COND_ASSERT(FXD::STD::any_of(vecInt.begin(), vecInt.end(), _DivisibleBy(3)), "Algorithm - inline bool any_of(InputItr first, InputItr last, Predicate pred)");
			PRINT_COND_ASSERT(FXD::STD::none_of(vecInt.begin(), vecInt.end(), _DivisibleBy(5)), "Algorithm - inline bool none_of(InputItr first, InputItr last, Predicate pred)");
		}
		{
			FXD::Container::LinkedList< FXD::S32 > listInt;
			listInt.push_back(2);
			listInt.push_back(4);
			listInt.push_back(6);
			listInt.push_back(8);

			PRINT_COND_ASSERT(FXD::STD::all_of(listInt.begin(), listInt.end(), _DivisibleBy(2)), "Algorithm - inline bool all_of(InputItr first, InputItr last, Predicate pred)");
			PRINT_COND_ASSERT(FXD::STD::any_of(listInt.begin(), listInt.end(), _DivisibleBy(3)), "Algorithm - inline bool any_of(InputItr first, InputItr last, Predicate pred)");
			PRINT_COND_ASSERT(FXD::STD::none_of(listInt.begin(), listInt.end(), _DivisibleBy(5)), "Algorithm - inline bool none_of(InputItr first, InputItr last, Predicate pred)");
		}
	}


	// inline Predicate for_each(InputItr first, InputItr last, Predicate pred)
	{
		_SetIncrementalIntegers< FXD::S32 > sii;

		FXD::S32 n1 = 0;
		{
			FXD::Container::Deque< FXD::S32 > dequeInt(1000);

			sii = FXD::STD::for_each(dequeInt.begin(), dequeInt.end(), sii);
			PRINT_COND_ASSERT(sii.m_nX == 1000, "Algorithm - inline Predicate for_each(InputItr first, InputItr last, Predicate pred)");
			for (n1 = 0; n1 < 1000; n1++)
			{
				if (dequeInt[n1] != (FXD::S32)n1)
				{
					break;
				}
			}
			PRINT_COND_ASSERT(n1 == 1000, "Algorithm - inline Predicate for_each(InputItr first, InputItr last, Predicate pred)");
		}

		sii.reset();

		{
			FXD::Container::Array< FXD::S32, 1000 > vecInt;

			sii = FXD::STD::for_each(vecInt.begin(), vecInt.end(), sii);
			PRINT_COND_ASSERT(sii.m_nX == 1000, "Algorithm - inline Predicate for_each(InputItr first, InputItr last, Predicate pred)");
			for (n1 = 0; n1 < 1000; n1++)
			{
				if (vecInt[n1] != (FXD::S32)n1)
				{
					break;
				}
			}
			PRINT_COND_ASSERT(n1 == 1000, "Algorithm - inline Predicate for_each(InputItr first, InputItr last, Predicate pred)");
		}
	}

	// inline InputItr for_each_n(InputItr first, Size nCount, Predicate pred)
#if IsCPP17()
	{
		{
			FXD::Container::Vector< FXD::S32 > vecInt{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
			FXD::STD::for_each_n(vecInt.begin(), 5, [](auto& e) { e += 10; });
			FXD::Container::Vector< FXD::S32 > vecExpected{ 10, 11, 12, 13, 14, 5, 6, 7, 8, 9 };
			PRINT_COND_ASSERT(vecInt == vecExpected, "Algorithm - inline InputItr for_each_n(InputItr first, Size nCount, Predicate pred)");
		}

		// verify lambda can return a result that is ignored.
		{
			FXD::Container::LinkedList< FXD::S32 > listInt{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
			FXD::STD::for_each_n(listInt.begin(), 5, [](auto& e) { e += 10; return 42; });
			FXD::Container::LinkedList< FXD::S32 > listExpected{ 10, 11, 12, 13, 14, 5, 6, 7, 8, 9 };
			PRINT_COND_ASSERT(listInt == listExpected, "Algorithm - inline InputItr for_each_n(InputItr first, Size nCount, Predicate pred)");
		}
	}
#endif

	// inline typename FXD::STD::iterator_traits< InputItr >::difference_type count(InputItr first, InputItr last, const T& val)
	{
		{
			FXD::S32 arrayInt[] = { 1, 2, 1, 5, 4, 1 };
			ptrdiff_t nDif = FXD::STD::count(arrayInt, arrayInt + 6, 1);
			PRINT_COND_ASSERT((nDif == 3), "Algorithm - inline typename FXD::STD::iterator_traits< InputItr >::difference_type count(InputItr first, InputItr last, const T& val)");
		}

		{
			TestObject arrayTO[] = { TestObject(1), TestObject(2), TestObject(1), TestObject(5), TestObject(4), TestObject(1) };
			ptrdiff_t nDif = FXD::STD::count(arrayTO, arrayTO + 6, TestObject(1));
			PRINT_COND_ASSERT((nDif == 3), "Algorithm - inline typename FXD::STD::iterator_traits< InputItr >::difference_type count(InputItr first, InputItr last, const T& val)");
		}
	}

	// inline typename FXD::STD::iterator_traits< InputItr >::difference_type count_if(InputItr first, InputItr last, Predicate pred)
	{
		{
			// Count all items whose value is less than three.
			FXD::S32 arrayInt[] = { 3, 2, 6, 5, 4, 1, 2, 4, 5, 4, 1, 2 };

			ptrdiff_t nDif = FXD::STD::count_if(arrayInt, arrayInt, [&](auto const& val) { return val < 3; });
			PRINT_COND_ASSERT((nDif == 0), "Algorithm - inline typename FXD::STD::iterator_traits< InputItr >::difference_type count_if(InputItr first, InputItr last, Predicate pred)");

			nDif = FXD::STD::count_if(arrayInt, arrayInt + 12, [&](auto const& val) { return val < 3; });
			PRINT_COND_ASSERT((nDif == 5), "Algorithm - inline typename FXD::STD::iterator_traits< InputItr >::difference_type count_if(InputItr first, InputItr last, Predicate pred)");
		}

		{
			// Count all items whose value is less than three.
			TestObject arrayTO[] = { TestObject(1), TestObject(3), TestObject(1), TestObject(4), TestObject(2), TestObject(5) };

			ptrdiff_t nDif = FXD::STD::count_if(arrayTO, arrayTO, [&](auto const& val) { return val < TestObject(3); });
			PRINT_COND_ASSERT((nDif == 0), "Algorithm - inline typename FXD::STD::iterator_traits< InputItr >::difference_type count_if(InputItr first, InputItr last, Predicate pred)");

			nDif = FXD::STD::count_if(arrayTO, arrayTO + 6, [&](auto const& val) { return val < TestObject(3); });
			PRINT_COND_ASSERT((nDif == 3), "Algorithm - inline typename FXD::STD::iterator_traits< InputItr >::difference_type count_if(InputItr first, InputItr last, Predicate pred)");
		}

		{
			// Count all items whose value is less than three.
			FXD::Container::LinkedList< FXD::S32 > listInt{ 5, 2, 4, 1, 3, 1 };

			ptrdiff_t nDif = FXD::STD::count_if(listInt.begin(), listInt.begin(), [&](auto const& elem) { return elem < 3; });
			PRINT_COND_ASSERT((nDif == 0), "Algorithm - inline typename FXD::STD::iterator_traits< InputItr >::difference_type count_if(InputItr first, InputItr last, Predicate pred)");

			nDif = FXD::STD::count_if(listInt.begin(), listInt.end(), [&](auto const& elem) { return elem < 3; });
			PRINT_COND_ASSERT((nDif == 3), "Algorithm - inline typename FXD::STD::iterator_traits< InputItr >::difference_type count_if(InputItr first, InputItr last, Predicate pred)");
		}
	}

	// inline Core::Pair mismatch(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)
	// inline Core::Pair mismatch(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)
	{
		FXD::S32 arrayInt1[] = { -5, 2, 1, 5, 4, 8888 };
		FXD::S32 arrayInt2[] = { -5, 2, 1, 5, 4, 9999 };
		FXD::S32 arrayInt3[] = { -5, 2, 1, 5, 4, 9999 };

		FXD::Core::Pair< FXD::S32*, FXD::S32* > pairInt;

		pairInt = FXD::STD::mismatch(arrayInt1, arrayInt1, arrayInt2);
		PRINT_COND_ASSERT(pairInt.first == arrayInt1 + 0, "Algorithm - inline Core::Pair mismatch(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)");
		PRINT_COND_ASSERT(pairInt.second == arrayInt2 + 0, "Algorithm - inline Core::Pair mismatch(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)");

		pairInt = FXD::STD::mismatch(arrayInt1, arrayInt1 + 6, arrayInt2);
		PRINT_COND_ASSERT(pairInt.first == arrayInt1 + 5, "Algorithm - inline Core::Pair mismatch(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)");
		PRINT_COND_ASSERT(pairInt.second == arrayInt2 + 5, "Algorithm - inline Core::Pair mismatch(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)");

		pairInt = FXD::STD::mismatch(arrayInt2, arrayInt2 + 6, arrayInt3);
		PRINT_COND_ASSERT(pairInt.first == arrayInt2 + 6, "Algorithm - inline Core::Pair mismatch(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)");
		PRINT_COND_ASSERT(pairInt.second == arrayInt3 + 6, "Algorithm - inline Core::Pair mismatch(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)");

		pairInt = FXD::STD::mismatch(arrayInt1, arrayInt1, arrayInt2, FXD::STD::equal_to< FXD::S32 >());
		PRINT_COND_ASSERT(pairInt.first == arrayInt1 + 0, "Algorithm - inline Core::Pair mismatch(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)");
		PRINT_COND_ASSERT(pairInt.second == arrayInt2 + 0, "Algorithm - inline Core::Pair mismatch(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)");

		pairInt = FXD::STD::mismatch(arrayInt1, arrayInt1 + 6, arrayInt2, FXD::STD::equal_to< FXD::S32 >());
		PRINT_COND_ASSERT(pairInt.first == arrayInt1 + 5, "Algorithm - inline Core::Pair mismatch(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)");
		PRINT_COND_ASSERT(pairInt.second == arrayInt2 + 5, "Algorithm - inline Core::Pair mismatch(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)");

		pairInt = FXD::STD::mismatch(arrayInt2, arrayInt2 + 6, arrayInt3, FXD::STD::equal_to< FXD::S32 >());
		PRINT_COND_ASSERT(pairInt.first == arrayInt2 + 6, "Algorithm - inline Core::Pair mismatch(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)");
		PRINT_COND_ASSERT(pairInt.second == arrayInt3 + 6, "Algorithm - inline Core::Pair mismatch(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)");
	}

	// inline InputItr find(InputItr first, InputItr last, const T& val)
	{
		FXD::Container::Vector< FXD::S32 > vecInt{ 0, 1, 2, 3 };
		FXD::Container::Vector< FXD::S32 >::iterator itr = FXD::STD::find(vecInt.begin(), vecInt.end(), 2);

		PRINT_COND_ASSERT( itr == (vecInt.begin() + 2), "Algorithm - inline InputItr find(InputItr first, InputItr last, const T& val)");
		PRINT_COND_ASSERT(*itr == 2, "Algorithm - inline InputItr find(InputItr first, InputItr last, const T& val)");

		itr = FXD::STD::find(vecInt.begin(), vecInt.end(), 7);
		PRINT_COND_ASSERT(itr == vecInt.end(), "Algorithm - inline InputItr find(InputItr first, InputItr last, const T& val)");
	}

	// inline InputItr find_if(InputItr first, InputItr last, Predicate pred)
	// inline InputItr find_if_not(InputItr first, InputItr last, Predicate pred)
	{
		{
			// Find an item which is equal to 1.
			FXD::S32 arrayInt[] = { 3, 2, 6, 5, 4, 1, 2, 4, 5, 4, 1, 2 };

			FXD::S32* pInt = FXD::STD::find_if(arrayInt, arrayInt, [&](auto const& val) { return val == 1; });
			PRINT_COND_ASSERT(pInt == (arrayInt), "Algorithm - inline InputItr find_if(InputItr first, InputItr last, Predicate pred)");

			pInt = FXD::STD::find_if(arrayInt, arrayInt + 12, [&](auto const& val) { return val == 1; });
			PRINT_COND_ASSERT(pInt == (arrayInt + 5), "Algorithm - inline InputItr find_if(InputItr first, InputItr last, Predicate pred)");

			pInt = FXD::STD::find_if(arrayInt, arrayInt + 12, [&](auto const& val) { return val == 99; });
			PRINT_COND_ASSERT(pInt == (arrayInt + 12), "Algorithm - inline InputItr find_if(InputItr first, InputItr last, Predicate pred)");

			pInt = FXD::STD::find_if_not(arrayInt, arrayInt + 12, [&](auto const& val) { return val == 3; });
			PRINT_COND_ASSERT(pInt == (arrayInt + 1), "Algorithm - inline InputItr find_if(InputItr first, InputItr last, Predicate pred)");
		}

		{
			// Find an item which is equal to 1.
			TestObject arrayTO[] = { TestObject(4), TestObject(3), TestObject(2), TestObject(1), TestObject(2), TestObject(5) };

			TestObject* pTO = FXD::STD::find_if(arrayTO, arrayTO, [&](auto const& val) { return val == TestObject(1); });
			PRINT_COND_ASSERT(pTO == (arrayTO), "Algorithm - inline InputItr find_if(InputItr first, InputItr last, Predicate pred)");

			pTO = FXD::STD::find_if(arrayTO, arrayTO + 6, [&](auto const& val) { return val == TestObject(1); });
			PRINT_COND_ASSERT(pTO == (arrayTO + 3), "Algorithm - inline InputItr find_if(InputItr first, InputItr last, Predicate pred)");

			pTO = FXD::STD::find_if(arrayTO, arrayTO + 6, [&](auto const& val) { return val == TestObject(99); });
			PRINT_COND_ASSERT(pTO == (arrayTO + 6), "Algorithm - inline InputItr find_if(InputItr first, InputItr last, Predicate pred)");

			pTO = FXD::STD::find_if_not(arrayTO, arrayTO + 6, [&](auto const& val) { return val == TestObject(4); });
			PRINT_COND_ASSERT(pTO == (arrayTO + 1), "Algorithm - inline InputItr find_if_not(InputItr first, InputItr last, Predicate pred)");
		}

		{
			// Find an item which is equal to 1.
			FXD::Container::LinkedList< FXD::S32 > listInt{ 5, 2, 1, 1, 2, 3, 4 };

			// The list is now: { 5, 2, 1, 2, 3, 4 }
			FXD::Container::LinkedList< FXD::S32 >::iterator itr = FXD::STD::find_if(listInt.begin(), listInt.begin(), [&](auto const& val) { return val == 1; });
			PRINT_COND_ASSERT(itr == listInt.begin(), "Algorithm - inline InputItr find_if(InputItr first, InputItr last, Predicate pred)");

			itr = FXD::STD::find_if(listInt.begin(), listInt.end(), [&](auto const& val) { return val == 1; });
			PRINT_COND_ASSERT(*itr == 1, "Algorithm - inline InputItr find_if(InputItr first, InputItr last, Predicate pred)");

			itr = FXD::STD::find_if(listInt.begin(), listInt.end(), [&](auto const& val) { return val == 99; });
			PRINT_COND_ASSERT(itr == listInt.end(), "Algorithm - inline InputItr find_if(InputItr first, InputItr last, Predicate pred)");

			itr = FXD::STD::find_if_not(listInt.begin(), listInt.end(), [&](auto const& val) { return val == 5; });
			PRINT_COND_ASSERT(*itr == 2, "Algorithm - inline InputItr find_if_not(InputItr first, InputItr last, Predicate pred)");
		}
	}

	// inline InputItr find_end(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)
	// inline InputItr find_end(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)
	{
		FXD::Core::StringBase< FXD::UTF8 > strTest("abcdefg abcdefg abcdefg");
		const FXD::UTF8* pSubstring1 = "abcd";
		const FXD::UTF8* pSubstring2 = "1234";
		{
			// Test via bidirectional/random_access iterator.
			FXD::Core::StringBase< FXD::UTF8 >::iterator itrStr = FXD::STD::find_end(strTest.begin(), strTest.end(), pSubstring1, pSubstring1 + 4);
			PRINT_COND_ASSERT(&*itrStr == &strTest[16], "Algorithm - inline InputItr find_end(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)");

			itrStr = FXD::STD::find_end(strTest.begin(), strTest.end(), pSubstring1, pSubstring1 + 4, FXD::STD::equal_to< FXD::UTF8 >());
			PRINT_COND_ASSERT(&*itrStr == &strTest[16], "Algorithm - inline InputItr find_end(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)");;

			itrStr = FXD::STD::find_end(strTest.begin(), strTest.end(), pSubstring2, pSubstring2 + FXD::Core::CharTraits< FXD::UTF8 >::length(pSubstring2));
			PRINT_COND_ASSERT(itrStr == strTest.end(), "Algorithm - inline InputItr find_end(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)");

			itrStr = FXD::STD::find_end(strTest.begin(), strTest.end(), pSubstring2, pSubstring2 + FXD::Core::CharTraits< FXD::UTF8 >::length(pSubstring2), FXD::STD::equal_to< FXD::UTF8 >());
			PRINT_COND_ASSERT(itrStr == strTest.end(), "Algorithm - inline InputItr find_end(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)");
		}

		{
			// Test via forward iterator.
			FXD::Container::LinkedList< FXD::UTF8 > listUTF8;
			for (FXD::Container::LinkedList< FXD::UTF8 >::size_type n1 = strTest.size(); n1 > 0; --n1)
			{
				listUTF8.push_front(strTest[n1 - 1]);
			}

			FXD::Container::LinkedList< FXD::UTF8 >::iterator itrList1 = FXD::STD::find_end(listUTF8.begin(), listUTF8.end(), pSubstring1, pSubstring1 + FXD::Core::CharTraits< FXD::UTF8 >::length(pSubstring1));
			FXD::Container::LinkedList< FXD::UTF8 >::iterator itrList2 = listUTF8.begin();
			FXD::STD::advance(itrList2, 16);
			PRINT_COND_ASSERT(itrList1 == itrList2, "Algorithm - inline InputItr find_end(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)");

			itrList1 = FXD::STD::find_end(listUTF8.begin(), listUTF8.end(), pSubstring1, pSubstring1 + FXD::Core::CharTraits< FXD::UTF8 >::length(pSubstring1), FXD::STD::equal_to< FXD::UTF8 >());
			itrList2 = listUTF8.begin();
			FXD::STD::advance(itrList2, 16);
			PRINT_COND_ASSERT(itrList1 == itrList2, "inline InputItr find_end(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)");

			itrList1 = FXD::STD::find_end(listUTF8.begin(), listUTF8.end(), pSubstring2, pSubstring2 + FXD::Core::CharTraits< FXD::UTF8 >::length(pSubstring2));
			PRINT_COND_ASSERT(itrList1 == listUTF8.end(), "inline InputItr find_end(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)");

			itrList1 = FXD::STD::find_end(listUTF8.begin(), listUTF8.end(), pSubstring2, pSubstring2 + FXD::Core::CharTraits< FXD::UTF8 >::length(pSubstring2), FXD::STD::equal_to< FXD::UTF8 >());
			PRINT_COND_ASSERT(itrList1 == listUTF8.end(), "inline InputItr find_end(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)");
		}
	}

	// inline InputItr find_first_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)
	// inline InputItr find_first_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)
	{
		FXD::S32 arrayInt1[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::S32 arrayInt2[3] = { 7, 6, 5 };

		FXD::S32* pInt = FXD::STD::find_first_of(arrayInt1, arrayInt1, arrayInt2, arrayInt2 + 3);
		PRINT_COND_ASSERT(pInt == arrayInt1, "Algorithm - inline InputItr find_first_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)");

		pInt = FXD::STD::find_first_of(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2);
		PRINT_COND_ASSERT(pInt == arrayInt1 + 10, "Algorithm - inline InputItr find_first_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)");

		pInt = FXD::STD::find_first_of(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2 + 3);
		PRINT_COND_ASSERT(pInt == arrayInt1 + 5, "Algorithm - inline InputItr find_first_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)");

		pInt = FXD::STD::find_first_of(arrayInt1, arrayInt1, arrayInt2, arrayInt2 + 3, FXD::STD::equal_to< FXD::S32 >());
		PRINT_COND_ASSERT(pInt == arrayInt1, "Algorithm - inline InputItr find_first_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)");

		pInt = FXD::STD::find_first_of(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2, FXD::STD::equal_to< FXD::S32 >());
		PRINT_COND_ASSERT(pInt == arrayInt1 + 10, "Algorithm - inline InputItr find_first_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)");

		pInt = FXD::STD::find_first_of(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2 + 3, FXD::STD::equal_to< FXD::S32 >());
		PRINT_COND_ASSERT(pInt == arrayInt1 + 5, "Algorithm - inline InputItr find_first_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)");
	}

	// inline InputItr find_first_not_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)
	// inline InputItr find_first_not_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)
	{
		FXD::S32 arrayInt1[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::S32 arrayInt2[3] = { 0, 1, 2 };

		FXD::S32* pInt = FXD::STD::find_first_not_of(arrayInt1, arrayInt1, arrayInt2, arrayInt2 + 3);
		PRINT_COND_ASSERT(pInt == arrayInt1, "Algorithm - inline InputItr find_first_not_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)");

		pInt = FXD::STD::find_first_not_of(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2);
		PRINT_COND_ASSERT(pInt == arrayInt1 + 0, "Algorithm - inline InputItr find_first_not_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)");

		pInt = FXD::STD::find_first_not_of(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2 + 3);
		PRINT_COND_ASSERT(pInt == arrayInt1 + 3, "Algorithm - inline InputItr find_first_not_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)");

		pInt = FXD::STD::find_first_not_of(arrayInt1, arrayInt1, arrayInt2, arrayInt2 + 3, FXD::STD::equal_to< FXD::S32 >());
		PRINT_COND_ASSERT(pInt == arrayInt1, "Algorithm - inline InputItr find_first_not_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)");

		pInt = FXD::STD::find_first_not_of(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2, FXD::STD::equal_to< FXD::S32 >());
		PRINT_COND_ASSERT(pInt == arrayInt1 + 0, "Algorithm - inline InputItr find_first_not_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)");

		pInt = FXD::STD::find_first_not_of(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2 + 3, FXD::STD::equal_to< FXD::S32 >());
		PRINT_COND_ASSERT(pInt == arrayInt1 + 3, "Algorithm - inline InputItr find_first_not_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)");
	}

	// inline InputItr find_last_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)
	// inline InputItr find_last_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)
	{
		FXD::S32 arrayInt1[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::S32 arrayInt2[3] = { 3, 4, 5 };

		FXD::S32* pInt = FXD::STD::find_last_of(arrayInt1, arrayInt1, arrayInt2, arrayInt2 + 3);
		PRINT_COND_ASSERT(pInt == arrayInt1, "Algorithm - inline InputItr find_last_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)");

		pInt = FXD::STD::find_last_of(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2);
		PRINT_COND_ASSERT(pInt == arrayInt1 + 10, "Algorithm - inline InputItr find_last_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)");

		pInt = FXD::STD::find_last_of(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2 + 3);
		PRINT_COND_ASSERT(pInt == arrayInt1 + 5, "Algorithm - inline InputItr find_last_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)");

		pInt = FXD::STD::find_last_of(arrayInt1, arrayInt1, arrayInt2, arrayInt2 + 3, FXD::STD::equal_to< FXD::S32 >());
		PRINT_COND_ASSERT(pInt == arrayInt1, "Algorithm - inline InputItr find_last_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)");

		pInt = FXD::STD::find_last_of(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2, FXD::STD::equal_to< FXD::S32 >());
		PRINT_COND_ASSERT(pInt == arrayInt1 + 10, "Algorithm - inline InputItr find_last_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)");

		pInt = FXD::STD::find_last_of(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2 + 3, FXD::STD::equal_to< FXD::S32 >());
		PRINT_COND_ASSERT(pInt == arrayInt1 + 5, "Algorithm - inline InputItr find_last_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)");
	}

	// inline InputItr find_last_not_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)
	// inline InputItr find_last_not_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)
	{
		FXD::S32 arrayInt1[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::S32 arrayInt2[3] = { 7, 8, 9 };

		FXD::S32* pInt = FXD::STD::find_last_not_of(arrayInt1, arrayInt1, arrayInt2, arrayInt2 + 3);
		PRINT_COND_ASSERT(pInt == arrayInt1, "Algorithm - inline InputItr find_last_not_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)");

		pInt = FXD::STD::find_last_not_of(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2);
		PRINT_COND_ASSERT(pInt == arrayInt1 + 10, "Algorithm - inline InputItr find_last_not_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)");

		pInt = FXD::STD::find_last_not_of(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2 + 3);
		PRINT_COND_ASSERT(pInt == arrayInt1 + 6, "Algorithm - inline InputItr find_last_not_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)");

		pInt = FXD::STD::find_last_not_of(arrayInt1, arrayInt1, arrayInt2, arrayInt2 + 3, FXD::STD::equal_to< FXD::S32 >());
		PRINT_COND_ASSERT(pInt == arrayInt1, "Algorithm - inline InputItr find_last_not_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)");

		pInt = FXD::STD::find_last_not_of(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2, FXD::STD::equal_to< FXD::S32 >());
		PRINT_COND_ASSERT(pInt == arrayInt1 + 10, "Algorithm - inline InputItr find_last_not_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)");

		pInt = FXD::STD::find_last_not_of(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2 + 3, FXD::STD::equal_to< FXD::S32 >());
		PRINT_COND_ASSERT(pInt == arrayInt1 + 6, "Algorithm - inline InputItr find_last_not_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)");
	}

	// inline ForwardItr adjacent_find(ForwardItr first, ForwardItr last, Predicate pred)
	// inline ForwardItr adjacent_find(ForwardItr first, ForwardItr last)
	{
		{
			FXD::S32 arrayInt[] = { 3, 2, 5, 5, 4, 1 };

			FXD::S32* pInt = FXD::STD::adjacent_find(arrayInt + 0, arrayInt + 6);
			PRINT_COND_ASSERT(pInt == (arrayInt + 2), "Algorithm - inline ForwardItr adjacent_find(ForwardItr first, ForwardItr last)");

			pInt = FXD::STD::adjacent_find(arrayInt + 3, arrayInt + 6);
			PRINT_COND_ASSERT(pInt == (arrayInt + 6), "Algorithm - inline ForwardItr adjacent_find(ForwardItr first, ForwardItr last)");
		}

		{
			TestObject arrayTO[] = { TestObject(3), TestObject(2), TestObject(5), TestObject(5), TestObject(4), TestObject(1) };

			TestObject* pTO = FXD::STD::adjacent_find(arrayTO + 0, arrayTO + 6);
			PRINT_COND_ASSERT(pTO == (arrayTO + 2), "Algorithm - inline ForwardItr adjacent_find(ForwardItr first, ForwardItr last)");

			pTO = FXD::STD::adjacent_find(arrayTO + 3, arrayTO + 6);
			PRINT_COND_ASSERT(pTO == (arrayTO + 6), "Algorithm - inline ForwardItr adjacent_find(ForwardItr first, ForwardItr last)");
		}
	}

	// ForwardItr1 search(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2, ForwardItr2 last2, Predicate pred)
	// ForwardItr1 search(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2, ForwardItr2 last2)
	{
		// Test via bidirectional/random_access iterator.
		FXD::Core::StringBase< FXD::UTF8 > strTest("abcdefg abcdefg abcdefg");
		const FXD::UTF8* pSubstring1 = " abcd";
		const FXD::UTF8* pSubstring2 = "1234";

		FXD::Core::StringBase< FXD::UTF8 >::iterator itrStr = FXD::STD::search(strTest.begin(), strTest.end(), pSubstring1, pSubstring1 + FXD::Core::CharTraits< FXD::UTF8 >::length(pSubstring1));
		PRINT_COND_ASSERT(&*itrStr == &strTest[7], "Algorithm - inline ForwardItr1 search(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2, ForwardItr2 last2)");

		itrStr = FXD::STD::search(strTest.begin(), strTest.end(), pSubstring1, pSubstring1 + 1); // Search for sequence of 1.
		PRINT_COND_ASSERT(&*itrStr == &strTest[7], "Algorithm - inline ForwardItr1 search(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2, ForwardItr2 last2)");

		// Test via forward iterator.
		FXD::Container::LinkedList< FXD::UTF8 > listUTF8;
		for (FXD::Container::LinkedList< FXD::UTF8 >::size_type n1 = strTest.size(); n1 > 0; --n1)
		{
			listUTF8.push_front(strTest[n1 - 1]);
		}

		FXD::Container::LinkedList< FXD::UTF8 >::iterator itrList1 = FXD::STD::search(listUTF8.begin(), listUTF8.end(), pSubstring1, pSubstring1 + 5);
		FXD::Container::LinkedList< FXD::UTF8 >::iterator itrList2 = listUTF8.begin();
		FXD::STD::advance(itrList2, 7);
		PRINT_COND_ASSERT(itrList1 == itrList2, "Algorithm - inline ForwardItr1 search(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2, ForwardItr2 last2)");

		itrList1 = FXD::STD::search(listUTF8.begin(), listUTF8.end(), pSubstring2, pSubstring2 + FXD::Core::CharTraits< FXD::UTF8 >::length(pSubstring2));
		PRINT_COND_ASSERT(itrList1 == listUTF8.end(), "Algorithm - inline ForwardItr1 search(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2, ForwardItr2 last2)");

		itrList1 = FXD::STD::search(listUTF8.begin(), listUTF8.end(), pSubstring2, pSubstring2); // Search with empty search pattern.
		PRINT_COND_ASSERT(itrList1 == listUTF8.begin(), "Algorithm - inline ForwardItr1 search(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2, ForwardItr2 last2)");
	}

	// inline ForwardItr search_n(ForwardItr first, ForwardItr last, Size nCount, const T& val, Predicate pred)
	// inline ForwardItr search_n(ForwardItr first, ForwardItr last, Size nCount, const T& val)
	{
		const FXD::UTF8* pString = "Hello wwworld";
		const FXD::UTF8* pResult = FXD::STD::search_n(pString, pString + FXD::Core::CharTraits< FXD::UTF8 >::length(pString), 1, 'w');
		PRINT_COND_ASSERT(pResult == pString + 6, "Algorithm - inline ForwardItr search_n(ForwardItr first, ForwardItr last, Size nCount, const T& val)");
	}

	return nErrorCount;
}

static FXD::S32 TestModifyingSequences(void)
{
	FXD::S32 nErrorCount = 0;

	FXD::Core::RandomGen rng(FXD::App::GetRandSeed());

	// inline OutputItr copy(InputItr first, InputItr last, OutputItr dst)
	{
		FXD::S32 arrayInt1[] = { 3, 2, 6, 5, 4, 1 };
		FXD::S32 arrayInt2[] = { 0, 0, 0, 0, 0, 0 };

		FXD::STD::copy(arrayInt1, arrayInt1 + 0, arrayInt2);
		PRINT_COND_ASSERT(VerifySequence(arrayInt2, arrayInt2 + 6, FXD::S32(), "copy", 0, 0, 0, 0, 0, 0, -1), "Algorithm - inline OutputItr copy(InputItr first, InputItr last, OutputItr dst)");

		FXD::STD::copy(arrayInt1, arrayInt1 + 6, arrayInt2);
		PRINT_COND_ASSERT(VerifySequence(arrayInt2, arrayInt2 + 6, FXD::S32(), "copy", 3, 2, 6, 5, 4, 1, -1), "Algorithm - inline OutputItr copy(InputItr first, InputItr last, OutputItr dst)");

		FXD::STD::copy(arrayInt1 + 1, arrayInt1 + 6, arrayInt1 + 0); // Copy over self.
		PRINT_COND_ASSERT(VerifySequence(arrayInt1, arrayInt1 + 6, FXD::S32(), "copy", 2, 6, 5, 4, 1, 1, -1), "Algorithm - inline OutputItr copy(InputItr first, InputItr last, OutputItr dst)");
	}

	// inline OutputItr copy_if(InputItr first, InputItr last, OutputItr dst, Predicate pred)
	{
		FXD::S32 arrayInt1[] = { 9, 1, 9, 9, 9, 9, 1, 1, 9, 9 };
		FXD::S32 arrayInt2[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

		FXD::STD::copy_if(arrayInt1, arrayInt1 + 0, arrayInt2, [&](FXD::S32 val) { return val == 1; });
		PRINT_COND_ASSERT(VerifySequence(arrayInt2, arrayInt2 + 10, FXD::S32(), "copy_if", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1), "Algorithm - inline OutputItr copy_if(InputItr first, InputItr last, OutputItr dst, Predicate pred)");

		FXD::STD::copy_if(arrayInt1, arrayInt1 + 9, arrayInt2, [&](FXD::S32 val) { return val == 1; });
		PRINT_COND_ASSERT(VerifySequence(arrayInt2, arrayInt2 + 10, FXD::S32(), "copy_if", 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, -1), "Algorithm - inline OutputItr copy_if(InputItr first, InputItr last, OutputItr dst, Predicate pred)");

		FXD::STD::copy_if(arrayInt1 + 1, arrayInt1 + 9, arrayInt1 + 0, [&](FXD::S32 val) { return val == 1; });
		PRINT_COND_ASSERT(VerifySequence(arrayInt1, arrayInt1 + 10, FXD::S32(), "copy_if", 1, 1, 1, 9, 9, 9, 1, 1, 9, 9, -1), "Algorithm - inline OutputItr copy_if(InputItr first, InputItr last, OutputItr dst, Predicate pred)");
	}

	// inline OutputItr copy_n(InputItr first, Size count, OutputItr dst)
	{
		FXD::Core::String8 in = "123456";
		FXD::Core::String8 out;

		FXD::STD::copy_n(in.begin(), 4, FXD::STD::back_inserter(out));
		PRINT_COND_ASSERT((out == "1234"), "Algorithm - inline OutputItr copy_n(InputItr first, Size count, OutputItr dst)");
	}

	// inline OutputItr copy_backward(InputItr first, InputItr last, OutputItr dst)
	{
		FXD::S32 arrayInt1[] = { 3, 2, 6, 5, 4, 1 };
		FXD::S32 arrayInt2[] = { 0, 0, 0, 0, 0, 0 };

		FXD::STD::copy_backward(arrayInt1, arrayInt1 + 0, arrayInt2 + 0);
		PRINT_COND_ASSERT(VerifySequence(arrayInt2, arrayInt2 + 6, FXD::S32(), "copy_backward", 0, 0, 0, 0, 0, 0, -1), "Algorithm - inline OutputItr copy_backward(InputItr first, InputItr last, OutputItr dst)");

		FXD::STD::copy_backward(arrayInt1, arrayInt1 + 6, arrayInt2 + 6);
		PRINT_COND_ASSERT(VerifySequence(arrayInt2, arrayInt2 + 6, FXD::S32(), "copy_backward", 3, 2, 6, 5, 4, 1, -1), "Algorithm - inline OutputItr copy_backward(InputItr first, InputItr last, OutputItr dst)");

		FXD::STD::copy_backward(arrayInt1, arrayInt1 + 5, arrayInt1 + 6); // Copy over self.
		PRINT_COND_ASSERT(VerifySequence(arrayInt1, arrayInt1 + 6, FXD::S32(), "copy_backward", 3, 3, 2, 6, 5, 4, -1), "Algorithm - inline OutputItr copy_backward(InputItr first, InputItr last, OutputItr dst)");
	}

	// inline OutputItr move(InputItr first, InputItr last, OutputItr dst)
	{
		{
			FXD::S32 arrayInt1[] = { 3, 2, 6, 5, 4, 1 };
			FXD::S32 arrayInt2[] = { 0, 0, 0, 0, 0, 0 };

			FXD::STD::move(arrayInt1, arrayInt1 + 0, arrayInt2);
			PRINT_COND_ASSERT(VerifySequence(arrayInt2, arrayInt2 + 6, FXD::S32(), "move", 0, 0, 0, 0, 0, 0, -1), "Algorithm - inline OutputItr move(InputItr first, InputItr last, OutputItr dst)");

			FXD::STD::move(arrayInt1, arrayInt1 + 6, arrayInt2);
			PRINT_COND_ASSERT(VerifySequence(arrayInt2, arrayInt2 + 6, FXD::S32(), "move", 3, 2, 6, 5, 4, 1, -1), "Algorithm - inline OutputItr move(InputItr first, InputItr last, OutputItr dst)");

			FXD::STD::move(arrayInt1 + 1, arrayInt1 + 6, arrayInt1 + 0); // Copy over self.
			PRINT_COND_ASSERT(VerifySequence(arrayInt1, arrayInt1 + 6, FXD::S32(), "move", 2, 6, 5, 4, 1, 1, -1), "Algorithm - inline OutputItr move(InputItr first, InputItr last, OutputItr dst)");
		}

		{
			FXD::Container::Vector< FXD::Core::String8 > vecSrc;
			for (FXD::S32 n1 = 0; n1 < 4; n1++)
			{
				vecSrc.push_back(FXD::Core::String8(1, (FXD::UTF8)('0' + n1)));
			}

			FXD::Container::Vector< FXD::Core::String8 > vecDst(vecSrc.size());

			FXD::STD::move(vecSrc.begin(), vecSrc.end(), vecDst.begin());
			PRINT_COND_ASSERT((vecDst[0] == "0") && (vecDst[3] == "3"), "Algorithm - inline OutputItr move(InputItr first, InputItr last, OutputItr dst)");
			PRINT_COND_ASSERT((vecSrc[0].empty() && vecSrc[3].empty()), "Algorithm - inline OutputItr move(InputItr first, InputItr last, OutputItr dst)");
		}
	}

	// inline OutputItr move_backward(InputItr first, InputItr last, OutputItr dst)
	{
		FXD::Container::Vector< FXD::Core::String8 > vecSrc;
		for (FXD::S32 n1 = 0; n1 < 4; n1++)
		{
			vecSrc.push_back(FXD::Core::String8(1, (FXD::UTF8)('0' + n1)));
		}

		FXD::Container::Vector< FXD::Core::String8 > vecDst(vecSrc.size());

		FXD::STD::move_backward(vecSrc.begin(), vecSrc.end(), vecDst.end());
		PRINT_COND_ASSERT((vecDst[0] == "0") && (vecDst[3] == "3"), "Algorithm - inline OutputItr move_backward(InputItr first, InputItr last, OutputItr dst)");
		PRINT_COND_ASSERT((vecSrc[0].empty() && vecSrc[3].empty()), "Algorithm - inline OutputItr move_backward(InputItr first, InputItr last, OutputItr dst)");
	}

	// inline void fill(ForwardItr first, ForwardItr last, const T& val)
	{
		{
			FXD::Container::Vector< FXD::S32 > vecInt(10);

			PRINT_COND_ASSERT(VerifySequence(vecInt.begin(), vecInt.end(), FXD::S32(), "fill", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1), "Algorithm - inline void fill(ForwardItr first, ForwardItr last, const T& val)");

			FXD::STD::fill(vecInt.begin() + 3, vecInt.begin() + 7, 4);
			PRINT_COND_ASSERT(VerifySequence(vecInt.begin(), vecInt.end(), FXD::S32(), "fill", 0, 0, 0, 4, 4, 4, 4, 0, 0, 0, -1), "Algorithm - inline void fill(ForwardItr first, ForwardItr last, const T& val)");

			FXD::Container::LinkedList< FXD::S32 > listInt(10);
			FXD::Container::LinkedList< FXD::S32 >::iterator itrFirst = listInt.begin();
			FXD::Container::LinkedList< FXD::S32 >::iterator itrLast = listInt.begin();

			FXD::STD::advance(itrFirst, 3);
			FXD::STD::advance(itrLast, 7);
			PRINT_COND_ASSERT(VerifySequence(listInt.begin(), listInt.end(), FXD::S32(), "fill", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1), "Algorithm - inline void fill(ForwardItr first, ForwardItr last, const T& val)");

			FXD::STD::fill(itrFirst, itrLast, 4);
			PRINT_COND_ASSERT(VerifySequence(listInt.begin(), listInt.end(), FXD::S32(), "fill", 0, 0, 0, 4, 4, 4, 4, 0, 0, 0, -1), "Algorithm - inline void fill(ForwardItr first, ForwardItr last, const T& val)");
		}

		{
			// Exercise specializations we have for some platform/compiler combinations
			const FXD::S32 kMaxSize = 300;
			FXD::Container::Vector< FXD::U16 > vecU16(kMaxSize, 0);
			FXD::Container::Vector< FXD::S16 > vecS16(kMaxSize, 0);
			FXD::Container::Vector< FXD::U32 > vecU32(kMaxSize, 0);
			FXD::Container::Vector< FXD::S32 > vecS32(kMaxSize, 0);
			FXD::Container::Vector< FXD::U64 > vecU64(kMaxSize, 0);
			FXD::Container::Vector< FXD::S64 > vecS64(kMaxSize, 0);

			for (FXD::U32 n1 = 0; n1 < kMaxSize; ++n1)
			{
				FXD::STD::fill(vecU16.begin(), vecU16.begin() + n1, UINT16_C(0x0123));
				PRINT_COND_ASSERT(FXD::Memory::Memcheck16(&vecU16[0], UINT16_C(0x0123), n1) == nullptr, "Algorithm - inline void fill(ForwardItr first, ForwardItr last, const T& val)");
				FXD::Memory::MemSet(&vecU16[0], 0x00, (n1 * sizeof(FXD::U16)));

				FXD::STD::fill(vecS16.begin(), vecS16.begin() + n1, UINT16_C(0x0123));
				PRINT_COND_ASSERT(FXD::Memory::Memcheck16(&vecS16[0], UINT16_C(0x0123), n1) == nullptr, "Algorithm - inline void fill(ForwardItr first, ForwardItr last, const T& val)");
				FXD::Memory::MemSet(&vecS16[0], 0x00, (n1 * sizeof(FXD::S16)));

				FXD::STD::fill(vecU32.begin(), vecU32.begin() + n1, UINT32_C(0x01234567));
				PRINT_COND_ASSERT(FXD::Memory::Memcheck32(&vecU32[0], UINT32_C(0x01234567), n1) == nullptr, "Algorithm - inline void fill(ForwardItr first, ForwardItr last, const T& val)");
				FXD::Memory::MemSet(&vecU32[0], 0x00, (n1 * sizeof(FXD::U32)));

				FXD::STD::fill(vecS32.begin(), vecS32.begin() + n1, UINT32_C(0x01234567));
				PRINT_COND_ASSERT(FXD::Memory::Memcheck32(&vecS32[0], UINT32_C(0x01234567), n1) == nullptr, "Algorithm - inline void fill(ForwardItr first, ForwardItr last, const T& val)");
				FXD::Memory::MemSet(&vecS32[0], 0x00, (n1 * sizeof(FXD::S32)));

				FXD::STD::fill(vecU64.begin(), vecU64.begin() + n1, UINT64_C(0x0123456789abcdef));
				PRINT_COND_ASSERT(FXD::Memory::Memcheck64(&vecU64[0], UINT64_C(0x0123456789abcdef), n1) == nullptr, "Algorithm - inline void fill(ForwardItr first, ForwardItr last, const T& val)");
				FXD::Memory::MemSet(&vecU64[0], 0x00, (n1 * sizeof(FXD::U64)));

				FXD::STD::fill(vecS64.begin(), vecS64.begin() + n1, UINT64_C(0x0123456789abcdef));
				PRINT_COND_ASSERT(FXD::Memory::Memcheck64(&vecS64[0], UINT64_C(0x0123456789abcdef), n1) == nullptr, "Algorithm - inline void fill(ForwardItr first, ForwardItr last, const T& val)");
				FXD::Memory::MemSet(&vecS64[0], 0x00, (n1 * sizeof(FXD::S64)));
			}

			{
				// Regression for user-reported compile failure.
				enum TestEnum { eTestValue = -1 };
				FXD::Container::Vector< FXD::S32 > vecInt;

				FXD::STD::fill< FXD::Container::Vector< FXD::S32 >::iterator, FXD::S32 >(vecInt.begin(), vecInt.end(), eTestValue);
				PRINT_COND_ASSERT((vecInt.size() == 0), "TestAlgorithm: Failed");
			}
		}
	}

	// inline OutputItr fill_n(OutputItr first, Size nCount, const T& val)
	{
		{
			FXD::Container::Vector< FXD::S32 > vecInt(10);

			PRINT_COND_ASSERT(VerifySequence(vecInt.begin(), vecInt.end(), FXD::S32(), "fill_n", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1), "Algorithm - inline OutputItr fill_n(OutputItr first, Size nCount, const T& val)");
			FXD::STD::fill_n(vecInt.begin() + 3, 4, 4);
			PRINT_COND_ASSERT(VerifySequence(vecInt.begin(), vecInt.end(), FXD::S32(), "fill_n", 0, 0, 0, 4, 4, 4, 4, 0, 0, 0, -1), "Algorithm - inline OutputItr fill_n(OutputItr first, Size nCount, const T& val)");

			FXD::Container::LinkedList< FXD::S32 > listInt(10);
			FXD::Container::LinkedList< FXD::S32 >::iterator first = listInt.begin();

			FXD::STD::advance(first, 3);
			PRINT_COND_ASSERT(VerifySequence(listInt.begin(), listInt.end(), FXD::S32(), "fill_n", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1), "Algorithm - inline OutputItr fill_n(OutputItr first, Size nCount, const T& val)");

			FXD::STD::fill_n(first, 4, 4);
			PRINT_COND_ASSERT(VerifySequence(listInt.begin(), listInt.end(), FXD::S32(), "fill_n", 0, 0, 0, 4, 4, 4, 4, 0, 0, 0, -1), "Algorithm - inline OutputItr fill_n(OutputItr first, Size nCount, const T& val)");
		}

		{
			// Exercise specializations we have for some platform/compiler combinations
			const FXD::U32 kMaxSize = 17;
			FXD::Container::Vector< FXD::U16 > vecU16(kMaxSize, 0);
			FXD::Container::Vector< FXD::S16 > vecS16(kMaxSize, 0);
			FXD::Container::Vector< FXD::U32 > vecU32(kMaxSize, 0);
			FXD::Container::Vector< FXD::S32 > vecS32(kMaxSize, 0);
			FXD::Container::Vector< FXD::U64 > vecU64(kMaxSize, 0);
			FXD::Container::Vector< FXD::S64 > vecS64(kMaxSize, 0);

			FXD::Container::Vector< FXD::U16 >::iterator itrU16 = FXD::STD::fill_n(vecU16.begin(), kMaxSize, UINT16_C(0x0123));
			PRINT_COND_ASSERT(FXD::Memory::Memcheck16(&vecU16[0], UINT16_C(0x0123), kMaxSize) == nullptr, "Algorithm - inline OutputItr fill_n(OutputItr first, Size nCount, const T& val); ");
			PRINT_COND_ASSERT(itrU16 == (vecU16.begin() + kMaxSize), "Algorithm - inline OutputItr fill_n(OutputItr first, Size nCount, const T& val)");
			FXD::Memory::MemSet(&vecU16[0], 0x00, sizeof(FXD::U16));

			FXD::Container::Vector< FXD::S16 >::iterator itrI16 = FXD::STD::fill_n(vecS16.begin(), kMaxSize, UINT16_C(0x0123));
			PRINT_COND_ASSERT(FXD::Memory::Memcheck16(&vecS16[0], UINT16_C(0x0123), kMaxSize) == nullptr, "Algorithm - inline OutputItr fill_n(OutputItr first, Size nCount, const T& val)");
			PRINT_COND_ASSERT(itrI16 == (vecS16.begin() + kMaxSize), "Algorithm - inline OutputItr fill_n(OutputItr first, Size nCount, const T& val)");
			FXD::Memory::MemSet(&vecS16[0], 0x00, sizeof(FXD::S16));

			FXD::Container::Vector< FXD::U32 >::iterator itrU32 = FXD::STD::fill_n(vecU32.begin(), kMaxSize, UINT32_C(0x01234567));
			PRINT_COND_ASSERT(FXD::Memory::Memcheck32(&vecU32[0], UINT32_C(0x01234567), kMaxSize) == nullptr, "Algorithm - inline OutputItr fill_n(OutputItr first, Size nCount, const T& val)");
			PRINT_COND_ASSERT(itrU32 == (vecU32.begin() + kMaxSize), "Algorithm - inline OutputItr fill_n(OutputItr first, Size nCount, const T& val)");
			FXD::Memory::MemSet(&vecS16[0], 0x00, sizeof(FXD::U32));

			FXD::Container::Vector< FXD::S32 >::iterator itrI32 = FXD::STD::fill_n(vecS32.begin(), kMaxSize, UINT32_C(0x01234567));
			PRINT_COND_ASSERT(FXD::Memory::Memcheck32(&vecS32[0], UINT32_C(0x01234567), kMaxSize) == nullptr, "Algorithm - inline OutputItr fill_n(OutputItr first, Size nCount, const T& val)");
			PRINT_COND_ASSERT(itrI32 == (vecS32.begin() + kMaxSize), "Algorithm - inline OutputItr fill_n(OutputItr first, Size nCount, const T& val)");
			FXD::Memory::MemSet(&vecS32[0], 0x00, sizeof(FXD::S32));

			FXD::Container::Vector< FXD::U64 >::iterator itrU64 = FXD::STD::fill_n(vecU64.begin(), kMaxSize, UINT64_C(0x0123456789abcdef));
			PRINT_COND_ASSERT(FXD::Memory::Memcheck64(&vecU64[0], UINT64_C(0x0123456789abcdef), kMaxSize) == nullptr, "Algorithm - inline OutputItr fill_n(OutputItr first, Size nCount, const T& val)");
			PRINT_COND_ASSERT(itrU64 == (vecU64.begin() + kMaxSize), "Algorithm - inline OutputItr fill_n(OutputItr first, Size nCount, const T& val)");
			FXD::Memory::MemSet(&vecU64[0], 0x00, sizeof(FXD::U64));

			FXD::Container::Vector< FXD::S64 >::iterator itrI64 = FXD::STD::fill_n(vecS64.begin(), kMaxSize, UINT64_C(0x0123456789abcdef));
			PRINT_COND_ASSERT(FXD::Memory::Memcheck64(&vecS64[0], UINT64_C(0x0123456789abcdef), kMaxSize) == nullptr, "Algorithm - inline OutputItr fill_n(OutputItr first, Size nCount, const T& val)");
			PRINT_COND_ASSERT(itrI64 == (vecS64.begin() + kMaxSize), "Algorithm - Outpinline OutputItrutItr fill_n(OutputItr first, Size nCount, const T& val)");
			FXD::Memory::MemSet(&vecS64[0], 0x00, sizeof(FXD::S64));
		}
	}

	// inline OutputItr transform(InputItr first, InputItr last, OutputItr dst, Predicate pred)
	{
		{
			FXD::S32 n1 = 0;
			FXD::S32 nEnd = 0;
			FXD::Container::Deque< FXD::S32 > dequeInt((size_t)rng.rand_range(1, 1000));
			for (n1 = 0, nEnd = (FXD::S32)dequeInt.size(); n1 < nEnd; n1++)
			{
				dequeInt[(size_t)n1] = 1;
			}

			FXD::STD::transform(dequeInt.begin(), dequeInt.begin(), dequeInt.begin(), FXD::STD::negate< FXD::S32 >());
			PRINT_COND_ASSERT(dequeInt[0] == 1, "Algorithm - inline OutputItr transform(InputItr first, InputItr last, OutputItr dst, Predicate pred)");

			FXD::STD::transform(dequeInt.begin(), dequeInt.end(), dequeInt.begin(), FXD::STD::negate< FXD::S32 >());
			for (n1 = 0, nEnd = (FXD::S32)dequeInt.size(); n1 < nEnd; n1++)
			{
				if (dequeInt[(size_t)n1] != -1)
				{
					break;
				}
			}
			PRINT_COND_ASSERT((n1 == nEnd), "Algorithm - inline OutputItr transform(InputItr first, InputItr last, OutputItr dst, Predicate pred)");
		}

		{
			FXD::Container::LinkedList< TestObject > listTO;
			for (FXD::S32 n1 = 0, nEnd = rng.rand_range(1, 100); n1 < nEnd; n1++)
			{
				listTO.push_front(TestObject(1));
			}

			FXD::STD::transform(listTO.begin(), listTO.begin(), listTO.begin(), _TestObjectNegate());
			PRINT_COND_ASSERT(listTO.front() == TestObject(1), "Algorithm - inline OutputItr transform(InputItr first, InputItr last, OutputItr dst, Predicate pred)");

			FXD::STD::transform(listTO.begin(), listTO.end(), listTO.begin(), _TestObjectNegate());
			FXD::Container::LinkedList< TestObject >::iterator itr = listTO.begin();
			for (; itr != listTO.end(); itr++)
			{
				if (!(*itr == TestObject(-1)))
				{
					break;
				}
			}
			PRINT_COND_ASSERT((itr == listTO.end()), "Algorithm - inline OutputItr transform(InputItr first, InputItr last, OutputItr dst, Predicate pred)");
		}
	}

	// inline OutputItr transform(InputItr1 first1, InputItr1 last1, InputItr2 first2, OutputItr dst, Predicate pred)
	{
		FXD::S32 arrayInt1[12] = { 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1 };
		FXD::S32 arrayInt2[12] = { 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3 };

		FXD::S32* pInt = FXD::STD::transform(arrayInt1, arrayInt1, arrayInt2, arrayInt2, FXD::STD::plus< FXD::S32 >());
		PRINT_COND_ASSERT(pInt == arrayInt2, "Algorithm - inline OutputItr transform(InputItr1 first1, InputItr1 last1, InputItr2 first2, OutputItr dst, Predicate pred)");
		PRINT_COND_ASSERT(VerifySequence(arrayInt1, arrayInt1 + 12, FXD::S32(), "transform", 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, -1), "Algorithm - inline OutputItr transform(InputItr1 first1, InputItr1 last1, InputItr2 first2, OutputItr dst, Predicate pred)");
		PRINT_COND_ASSERT(VerifySequence(arrayInt2, arrayInt2 + 12, FXD::S32(), "transform", 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, -1), "Algorithm - inline OutputItr transform(InputItr1 first1, InputItr1 last1, InputItr2 first2, OutputItr dst, Predicate pred)");

		pInt = FXD::STD::transform(arrayInt1, arrayInt1 + 12, arrayInt2, arrayInt2, FXD::STD::plus< FXD::S32 >());
		PRINT_COND_ASSERT(pInt == arrayInt2 + 12, "Algorithm - inline OutputItr transform(InputItr1 first1, InputItr1 last1, InputItr2 first2, OutputItr dst, Predicate pred)");
		PRINT_COND_ASSERT(VerifySequence(arrayInt1, arrayInt1 + 12, FXD::S32(), "transform", 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, -1), "Algorithm - inline OutputItr transform(InputItr1 first1, InputItr1 last1, InputItr2 first2, OutputItr dst, Predicate pred)");
		PRINT_COND_ASSERT(VerifySequence(arrayInt2, arrayInt2 + 12, FXD::S32(), "transform", 3, 3, 4, 4, 3, 3, 4, 4, 3, 3, 4, 4, -1), "Algorithm - inline OutputItr transform(InputItr1 first1, InputItr1 last1, InputItr2 first2, OutputItr dst, Predicate pred)");
	}

	// inline void generate(ForwardItr first, ForwardItr last, Generator generator)
	{
		_GenerateIncrementalIntegers< FXD::S32 > gii(0);
		{
			FXD::Container::Deque< FXD::S32 > dequeInt((size_t)rng.rand_range(100, 1000));
			FXD::S32 n1;
			FXD::S32 nEnd;

			FXD::STD::generate(dequeInt.begin(), dequeInt.end(), gii);
			for (n1 = 0, nEnd = (FXD::S32)dequeInt.size(); n1 < nEnd; n1++)
			{
				if (dequeInt[(size_t)n1] != n1)
				{
					break;
				}
			}
			PRINT_COND_ASSERT((n1 == nEnd), "Algorithm - inline void generate(ForwardItr first, ForwardItr last, Generator generator)");
		}

		gii.reset(0);

		{
			FXD::Container::Array< FXD::S32, 1000 > arrayInt;
			FXD::S32 n1;

			FXD::STD::generate(arrayInt.begin(), arrayInt.end(), gii);
			for (n1 = 0; n1 < 1000; n1++)
			{
				if (arrayInt[(size_t)n1] != n1)
				{
					break;
				}
			}
			PRINT_COND_ASSERT((n1 == 1000), "Algorithm - inline void generate(ForwardItr first, ForwardItr last, Generator generator)");
		}
	}

	// inline OutputItr generate_n(OutputItr first, Size nSize, Generator generator)
	{
		_GenerateIncrementalIntegers< FXD::S32 > gii(0);
		
		FXD::S32 arrayInt[9];
		FXD::STD::generate_n(arrayInt, 9, gii);

		PRINT_COND_ASSERT(*FXD::STD::begin(arrayInt) == 0, "Algorithm - inline OutputItr generate_n(OutputItr first, Size nSize, Generator generator)");
		PRINT_COND_ASSERT(*FXD::STD::rbegin(arrayInt) == 8, "Algorithm - inline OutputItr generate_n(OutputItr first, Size nSize, Generator generator)");
		PRINT_COND_ASSERT( FXD::STD::is_sorted(FXD::STD::begin(arrayInt), FXD::STD::end(arrayInt)), "Algorithm - inline OutputItr generate_n(OutputItr first, Size nSize, Generator generator)");
	}

	// inline ForwardItr remove(ForwardItr first, ForwardItr last, const T& val, Predicate pred)
	// inline ForwardItr remove(ForwardItr first, ForwardItr last, const T& val)
	{
		FXD::S32 arrayInt[12] = { 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1 };

		FXD::S32* pInt = FXD::STD::remove(arrayInt, arrayInt, 1);
		PRINT_COND_ASSERT(pInt == arrayInt, "Algorithm - inline ForwardItr remove(ForwardItr first, ForwardItr last, const T& val)");
		PRINT_COND_ASSERT(VerifySequence(arrayInt, arrayInt + 12, FXD::S32(), "remove", 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, -1), "Algorithm - inline ForwardItr remove(ForwardItr first, ForwardItr last, const T& val)");

		pInt = FXD::STD::remove(arrayInt, arrayInt + 12, 1);
		PRINT_COND_ASSERT(pInt == arrayInt + 6, "Algorithm - inline ForwardItr remove(ForwardItr first, ForwardItr last, const T& val)");
		PRINT_COND_ASSERT(VerifySequence(arrayInt, arrayInt + 12, FXD::S32(), "remove", 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, -1), "Algorithm - inline ForwardItr remove(ForwardItr first, ForwardItr last, const T& val)");
	}

	// inline ForwardItr remove_if(ForwardItr first, ForwardItr last, Predicate pred)
	{
		FXD::S32 arrayInt[12] = { 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3 };

		FXD::S32* pInt = FXD::STD::remove_if(arrayInt, arrayInt, [&](auto const& val) { return val == 1; });
		PRINT_COND_ASSERT(pInt == arrayInt, "Algorithm - inline ForwardItr remove_if(ForwardItr first, ForwardItr last, Predicate pred)");
		PRINT_COND_ASSERT(VerifySequence(arrayInt, arrayInt + 12, FXD::S32(), "remove", 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, -1), "Algorithm - inline ForwardItr remove_if(ForwardItr first, ForwardItr last, Predicate pred)");

		pInt = FXD::STD::remove_if(arrayInt, arrayInt + 12, [&](auto const& val) { return val == 1; });
		PRINT_COND_ASSERT(pInt == arrayInt + 12, "Algorithm - inline ForwardItr remove_if(ForwardItr first, ForwardItr last, Predicate pred)");
		PRINT_COND_ASSERT(VerifySequence(arrayInt, arrayInt + 12, FXD::S32(), "remove", 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, -1), "Algorithm - inline ForwardItr remove_if(ForwardItr first, ForwardItr last, Predicate pred)");
	}

	// inline OutputItr remove_copy(InputItr first, InputItr last, OutputItr dst, const T& val, Predicate pred)
	// inline OutputItr remove_copy(InputItr first, InputItr last, OutputItr dst, const T& val)
	// inline OutputItr remove_copy_if(InputItr first, InputItr last, OutputItr dst, Predicate pred)
	{
		FXD::S32 arrayInt1[12] = { 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1 };
		FXD::S32 arrayInt2[12] = { 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3 };

		FXD::S32* pInt = FXD::STD::remove_copy(arrayInt1, arrayInt1, arrayInt2, 1); // No-op
		PRINT_COND_ASSERT(pInt == arrayInt2, "Algorithm - inline OutputItr remove_copy(InputItr first, InputItr last, OutputItr dst, const T& val)");
		PRINT_COND_ASSERT(VerifySequence(arrayInt1, arrayInt1 + 12, FXD::S32(), "inline OutputItr", 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, -1), "Algorithm - OutputItr inline OutputItr(InputItr first, InputItr last, OutputItr dst, const T& val)");
		PRINT_COND_ASSERT(VerifySequence(arrayInt2, arrayInt2 + 12, FXD::S32(), "inline OutputItr", 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, -1), "Algorithm - OutputItr inline OutputItr(InputItr first, InputItr last, OutputItr dst, const T& val)");

		pInt = FXD::STD::remove_copy(arrayInt1, arrayInt1 + 12, arrayInt2, 1);
		PRINT_COND_ASSERT(pInt == arrayInt2 + 6, "Algorithm - OutputItr inline OutputItr(InputItr first, InputItr last, OutputItr dst, const T& val)");
		PRINT_COND_ASSERT(VerifySequence(arrayInt1, arrayInt1 + 12, FXD::S32(), "inline OutputItr", 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, -1), "Algorithm - OutputItr inline OutputItr(InputItr first, InputItr last, OutputItr dst, const T& val)");
		PRINT_COND_ASSERT(VerifySequence(arrayInt2, arrayInt2 + 12, FXD::S32(), "inline OutputItr", 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, -1), "Algorithm - OutputItr inline OutputItr(InputItr first, InputItr last, OutputItr dst, const T& val)");

		pInt = FXD::STD::remove_copy_if(arrayInt1, arrayInt1, arrayInt2, [&](auto const& val) { return val == 0; });
		PRINT_COND_ASSERT(pInt == arrayInt2, "Algorithm - OutputItr remove_copy_if(InputItr first, InputItr last, OutputItr dst, Predicate pred)");
		PRINT_COND_ASSERT(VerifySequence(arrayInt1, arrayInt1 + 12, FXD::S32(), "OutputItr remove_copy_if", 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, -1), "Algorithm - OutputItr remove_copy_if(InputItr first, InputItr last, OutputItr dst, Predicate pred)");
		PRINT_COND_ASSERT(VerifySequence(arrayInt2, arrayInt2 + 12, FXD::S32(), "OutputItr remove_copy_if", 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, -1), "Algorithm - OutputItr remove_copy_if(InputItr first, InputItr last, OutputItr dst, Predicate pred)");

		pInt = FXD::STD::remove_copy_if(arrayInt1, arrayInt1 + 12, arrayInt2, [&](auto const& val) { return val == 0; });
		PRINT_COND_ASSERT(pInt == arrayInt2 + 6, "Algorithm - OutputItr remove_copy_if(InputItr first, InputItr last, OutputItr dst, Predicate pred)");
		PRINT_COND_ASSERT(VerifySequence(arrayInt1, arrayInt1 + 12, FXD::S32(), "OutputItr remove_copy_if", 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, -1), "Algorithm - OutputItr remove_copy_if(InputItr first, InputItr last, OutputItr dst, Predicate pred)");
		PRINT_COND_ASSERT(VerifySequence(arrayInt2, arrayInt2 + 12, FXD::S32(), "OutputItr remove_copy_if", 1, 1, 1, 1, 1, 1, 3, 3, 3, 3, 3, 3, -1), "Algorithm - OutputItr remove_copy_if(InputItr first, InputItr last, OutputItr dst, Predicate pred)");
	}

	// inline void replace(ForwardItr first, ForwardItr last, const T& oldVal, const T& newVal, Predicate pred)
	// inline void replace(ForwardItr first, ForwardItr last, const T& oldVal, const T& newVal)
	// inline void replace_if(ForwardItr first, ForwardItr last, Predicate pred, const T& val)
	{
		{
			FXD::S32 arrayInt[8] = { 0, 3, 2, 7, 5, 4, 5, 3, };

			// Convert 3s to 99s.
			FXD::STD::replace(arrayInt, arrayInt, 3, 99); // No-op
			PRINT_COND_ASSERT((arrayInt[1] == 3) && (arrayInt[7] == 3), "Algorithm - inline void replace(ForwardItr first, ForwardItr last, const T& oldVal, const T& newVal)");

			FXD::STD::replace(arrayInt, arrayInt + 8, 3, 99); // No-op
			PRINT_COND_ASSERT((arrayInt[1] == 99) && (arrayInt[7] == 99), "Algorithm - inline void replace(ForwardItr first, ForwardItr last, const T& oldVal, const T& newVal)");

			// Convert 99s to 88s.
			FXD::STD::replace_if(arrayInt, arrayInt, [&](auto const& val) { return val == 99; }, 88); // No-op
			PRINT_COND_ASSERT((arrayInt[1] == 99) && (arrayInt[7] == 99), "Algorithm - inline void replace_if(ForwardItr first, ForwardItr last, const T& oldVal, const T& newVal)");

			FXD::STD::replace_if(arrayInt, arrayInt + 8, [&](auto const& val) { return val == 99; }, 88);
			PRINT_COND_ASSERT((arrayInt[1] == 88) && (arrayInt[7] == 88), "Algorithm - inline void replace_if(ForwardItr first, ForwardItr last, const T& oldVal, const T& newVal)");
		}

		{
			FXD::Container::LinkedList< TestObject > listTO{ TestObject(0), TestObject(3), TestObject(2), TestObject(7), TestObject(5), TestObject(4), TestObject(5), TestObject(3) };
			FXD::Container::LinkedList< TestObject >::iterator itr;

			// Convert 3s to 99s.
			{
				FXD::STD::replace(listTO.begin(), listTO.begin(), TestObject(3), TestObject(99)); // No-op
				itr = listTO.begin();
				FXD::STD::advance(itr, 1);
				PRINT_COND_ASSERT(*itr == TestObject(3), "Algorithm - inline void replace(ForwardItr first, ForwardItr last, const T& oldVal, const T& newVal)");

				FXD::STD::advance(itr, 6);
				PRINT_COND_ASSERT(*itr == TestObject(3), "Algorithm - inline void replace(ForwardItr first, ForwardItr last, const T& oldVal, const T& newVal)");

				FXD::STD::replace(listTO.begin(), listTO.end(), TestObject(3), TestObject(99));
				itr = listTO.begin();
				FXD::STD::advance(itr, 1);
				PRINT_COND_ASSERT(*itr == TestObject(99), "Algorithm - inline void replace(ForwardItr first, ForwardItr last, const T& oldVal, const T& newVal)");

				FXD::STD::advance(itr, 6);
				PRINT_COND_ASSERT(*itr == TestObject(99), "Algorithm - inline void replace(ForwardItr first, ForwardItr last, const T& oldVal, const T& newVal)");
			}

			// Convert 99s to 88s.
			{
				FXD::STD::replace_if(listTO.begin(), listTO.begin(), [&](auto const& val) { return val == TestObject(99); }, TestObject(88)); // No-op
				itr = listTO.begin();
				FXD::STD::advance(itr, 1);
				PRINT_COND_ASSERT(*itr == TestObject(99), "Algorithm - inline void replace_if(ForwardItr first, ForwardItr last, const T& oldVal, const T& newVal)");

				FXD::STD::advance(itr, 6);
				PRINT_COND_ASSERT(*itr == TestObject(99), "Algorithm - inline void replace_if(ForwardItr first, ForwardItr last, const T& oldVal, const T& newVal)");

				FXD::STD::replace_if(listTO.begin(), listTO.end(), [&](auto const& val) { return val == TestObject(99); }, TestObject(88));
				itr = listTO.begin();
				FXD::STD::advance(itr, 1);
				PRINT_COND_ASSERT(*itr == TestObject(88), "Algorithm - inline void replace_if(ForwardItr first, ForwardItr last, const T& oldVal, const T& newVal)");

				FXD::STD::advance(itr, 6);
				PRINT_COND_ASSERT(*itr == TestObject(88), "Algorithm - inline void replace_if(ForwardItr first, ForwardItr last, const T& oldVal, const T& newVal)");
			}
		}
	}

	// inline OutputItr replace_copy(InputItr first, InputItr last, OutputItr dst, const T& oldVal, const T& newVal)
	// inline OutputItr replace_copy_if(InputItr first, InputItr last, OutputItr dst, Predicate pred, const T& newVal)
	{
		FXD::S32 arrayInt1[12] = { 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1 };
		FXD::S32 arrayInt2[12] = { 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3 };

		FXD::S32* pInt = FXD::STD::replace_copy(arrayInt1, arrayInt1, arrayInt2, 1, 4);
		PRINT_COND_ASSERT(pInt == arrayInt2, "Algorithm - inline OutputItr replace_copy(InputItr first, InputItr last, OutputItr dst, const T& oldVal, const T& newVal)");
		PRINT_COND_ASSERT(VerifySequence(arrayInt1, arrayInt1 + 12, FXD::S32(), "replace_copy", 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, -1), "Algorithm - inline OutputItr replace_copy(InputItr first, InputItr last, OutputItr dst, const T& oldVal, const T& newVal)");
		PRINT_COND_ASSERT(VerifySequence(arrayInt2, arrayInt2 + 12, FXD::S32(), "replace_copy", 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, -1), "Algorithm - inline OutputItr replace_copy(InputItr first, InputItr last, OutputItr dst, const T& oldVal, const T& newVal)");

		pInt = FXD::STD::replace_copy(arrayInt1, arrayInt1 + 12, arrayInt2, 1, 4);
		PRINT_COND_ASSERT(pInt == arrayInt2 + 12, "Algorithm - inline OutputItr replace_copy_if(InputItr first, InputItr last, OutputItr dst, Predicate pred, const T& newVal)");
		PRINT_COND_ASSERT(VerifySequence(arrayInt1, arrayInt1 + 12, FXD::S32(), "replace_copy", 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, -1), "Algorithm - inline OutputItr replace_copy_if(InputItr first, InputItr last, OutputItr dst, Predicate pred, const T& newVal)");
		PRINT_COND_ASSERT(VerifySequence(arrayInt2, arrayInt2 + 12, FXD::S32(), "replace_copy", 0, 0, 4, 4, 0, 0, 4, 4, 0, 0, 4, 4, -1), "Algorithm - inline OutputItr replace_copy_if(InputItr first, InputItr last, OutputItr dst, Predicate pred, const T& newVal)");
	}

	// inline void swap(T& lhs, T& rhs)
	// inline void iter_swap(InputItr lhs, OutputItr rhs)
	{
		{
			FXD::S32 arrayInt[] = { -5, 2, 1, 5, 4, 5 };

			FXD::STD::swap(arrayInt[0], arrayInt[4]);
			PRINT_COND_ASSERT(VerifySequence(arrayInt, arrayInt + 6, FXD::S32(), "swap", 4, 2, 1, 5, -5, 5, -1), "Algorithm - inline void swap(T& lhs, T& rhs)");

			FXD::STD::iter_swap(arrayInt + 2, arrayInt + 3);
			PRINT_COND_ASSERT(VerifySequence(arrayInt, arrayInt + 6, FXD::S32(), "iter_swap", 4, 2, 5, 1, -5, 5, -1), "Algorithm - inline void iter_swap(InputItr lhs, OutputItr rhs)");
		}

		{
			TestObject arrayTO[] = { TestObject(-5), TestObject(2), TestObject(1), TestObject(5), TestObject(4), TestObject(5) };

			FXD::STD::swap(arrayTO[0], arrayTO[4]);
			PRINT_COND_ASSERT(arrayTO[0] == TestObject(4), "Algorithm - inline void swap(T& lhs, T& rhs)");
			PRINT_COND_ASSERT(arrayTO[4] == TestObject(-5), "Algorithm - inline void swap(T& lhs, T& rhs)");

			FXD::STD::iter_swap(arrayTO + 2, arrayTO + 3);
			PRINT_COND_ASSERT(arrayTO[2] == TestObject(5), "Algorithm - inline void iter_swap(InputItr lhs, OutputItr rhs)");
			PRINT_COND_ASSERT(arrayTO[3] == TestObject(1), "Algorithm - inline void iter_swap(InputItr lhs, OutputItr rhs)");
		}
	}

	// inline OutputItr swap_ranges(InputItr first, InputItr last, OutputItr dst)
	{
		{
			FXD::S32 arrayInt1[] = { 3, 2, 6, 5, 4, 1 };
			FXD::S32 arrayInt2[] = { 0, 0, 0, 0, 0, 0 };

			FXD::STD::swap_ranges(arrayInt1, arrayInt1 + 6, arrayInt2);
			PRINT_COND_ASSERT(VerifySequence(arrayInt1, arrayInt1 + 6, FXD::S32(), "swap_ranges", 0, 0, 0, 0, 0, 0, -1), "Algorithm - inline OutputItr swap_ranges(InputItr first, InputItr last, OutputItr dst)");
			PRINT_COND_ASSERT(VerifySequence(arrayInt2, arrayInt2 + 6, FXD::S32(), "swap_ranges", 3, 2, 6, 5, 4, 1, -1), "Algorithm - inline OutputItr swap_ranges(InputItr first, InputItr last, OutputItr dst)");
		}

		{
			TestObject arrayTO1[] = { TestObject(3), TestObject(2), TestObject(6), TestObject(5), TestObject(4), TestObject(1) };
			TestObject arrayTO2[] = { TestObject(0), TestObject(0), TestObject(0), TestObject(0), TestObject(0), TestObject(0) };

			FXD::STD::swap_ranges(arrayTO1, arrayTO1 + 6, arrayTO2);
			PRINT_COND_ASSERT(arrayTO1[0] == TestObject(0), "Algorithm - inline OutputItr swap_ranges(InputItr first, InputItr last, OutputItr dst)");
			PRINT_COND_ASSERT(arrayTO1[5] == TestObject(0), "Algorithm - inline OutputItr swap_ranges(InputItr first, InputItr last, OutputItr dst)");
			PRINT_COND_ASSERT(arrayTO2[0] == TestObject(3), "Algorithm - inline OutputItr swap_ranges(InputItr first, InputItr last, OutputItr dst)");
			PRINT_COND_ASSERT(arrayTO2[5] == TestObject(1), "Algorithm - inline OutputItr swap_ranges(InputItr first, InputItr last, OutputItr dst)");
		}
	}

	// inline void reverse(BidirItr first, BidirItr last)
	{
		{
			FXD::Container::Vector< FXD::S32 > vecInt;
			for (FXD::S32 n1 = 0; n1 < 10; n1++)
			{
				vecInt.push_back(n1);
			}

			FXD::STD::reverse(vecInt.begin(), vecInt.begin());
			PRINT_COND_ASSERT(VerifySequence(vecInt.begin(), vecInt.end(), FXD::S32(), "reverse", 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -1), "Algorithm - inline void reverse(BidirItr first, BidirItr last)");

			FXD::STD::reverse(vecInt.begin(), vecInt.end());
			PRINT_COND_ASSERT(VerifySequence(vecInt.begin(), vecInt.end(), FXD::S32(), "reverse", 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, -1), "Algorithm - inline void reverse(BidirItr first, BidirItr last)");
		}

		{
			FXD::Container::LinkedList< TestObject > listTO;
			for (FXD::S32 n1 = 0; n1 < 10; n1++)
			{
				listTO.push_back(TestObject(n1));
			}

			FXD::STD::reverse(listTO.begin(), listTO.begin()); // No-op
			PRINT_COND_ASSERT(listTO.front() == TestObject(0), "Algorithm - inline void reverse(BidirItr first, BidirItr last)");
			PRINT_COND_ASSERT(listTO.back() == TestObject(9), "Algorithm - inline void reverse(BidirItr first, BidirItr last)");

			FXD::STD::reverse(listTO.begin(), listTO.end());
			PRINT_COND_ASSERT(listTO.front() == TestObject(9), "Algorithm - inline void reverse(BidirItr first, BidirItr last)");
			PRINT_COND_ASSERT(listTO.back() == TestObject(0), "Algorithm - inline void reverse(BidirItr first, BidirItr last)");

			// Verify that reversing an empty range executes without exception.
			FXD::STD::reverse(listTO.begin(), listTO.begin());
		}
	}

	// inline OutputItr reverse_copy(BidirItr first, BidirItr last, OutputItr dst
	{
		{
			FXD::Container::Vector< FXD::S32 > vecInt;
			FXD::S32 arrayInt[10] = { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 };

			for (FXD::S32 n1 = 0; n1 < 10; n1++)
			{
				vecInt.push_back(n1);
			}

			FXD::S32* pInt = FXD::STD::reverse_copy(vecInt.begin(), vecInt.begin(), arrayInt); // No-op
			PRINT_COND_ASSERT(pInt == arrayInt, "Algorithm - inline OutputItr reverse_copy(BidirItr first, BidirItr last, OutputItr dst)");
			PRINT_COND_ASSERT(VerifySequence(arrayInt, arrayInt + 10, FXD::S32(), "reverse_copy", 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, -1), "Algorithm - inline OutputItr reverse_copy(BidirItr first, BidirItr last, OutputItr dst)");

			pInt = FXD::STD::reverse_copy(vecInt.begin(), vecInt.end(), arrayInt);
			PRINT_COND_ASSERT(pInt == arrayInt + vecInt.size(), "Algorithm - inline OutputItr reverse_copy(BidirItr first, BidirItr last, OutputItr dst)");
			PRINT_COND_ASSERT(VerifySequence(arrayInt, arrayInt + 10, FXD::S32(), "reverse_copy", 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, -1), "Algorithm - inline OutputItr reverse_copy(BidirItr first, BidirItr last, OutputItr dst)");
		}

		{
			FXD::Container::LinkedList< TestObject > listTO;
			TestObject arrayTO2[10];

			for (FXD::S32 n1 = 0; n1 < 10; n1++)
			{
				listTO.push_back(TestObject(n1));
				arrayTO2[n1] = TestObject(5);
			}

			TestObject* pTO = FXD::STD::reverse_copy(listTO.begin(), listTO.begin(), arrayTO2); // No-op
			PRINT_COND_ASSERT(pTO == arrayTO2, "Algorithm - inline OutputItr reverse_copy(BidirItr first, BidirItr last, OutputItr dst)");
			PRINT_COND_ASSERT(arrayTO2[0] == TestObject(5), "Algorithm - inline OutputItr reverse_copy(BidirItr first, BidirItr last, OutputItr dst)");
			PRINT_COND_ASSERT(arrayTO2[9] == TestObject(5), "Algorithm - inline OutputItr reverse_copy(BidirItr first, BidirItr last, OutputItr dst)");

			pTO = FXD::STD::reverse_copy(listTO.begin(), listTO.end(), arrayTO2);
			PRINT_COND_ASSERT(pTO == arrayTO2 + 10, "Algorithm - inline OutputItr reverse_copy(BidirItr first, BidirItr last, OutputItr dst)");
		}
	}

	// inline ForwardItr rotate(ForwardItr first, ForwardItr middle, ForwardItr last);
	// OutputItr rotate_copy(ForwardItr first, ForwardItr middle, ForwardItr last, OutputItr dst);
	{
		const FXD::U32 kRotateArraySize = 10;

		// FXD::Container::Array (ContiguousIterator/Pointer)
		{
			using ArrayInt = FXD::Container::Array< FXD::S32, kRotateArraySize >;
			ArrayInt arrayInt;

			for (FXD::U32 n1 = 0; n1 < kRotateArraySize; n1++)
			{
				FXD::STD::generate_n(arrayInt.begin(), kRotateArraySize, _GenerateIncrementalIntegers< FXD::S32 >());
				ArrayInt::iterator itrMid = FXD::STD::next(arrayInt.begin(), n1);
				ArrayInt::iterator itrArray = FXD::STD::rotate(arrayInt.begin(), itrMid, arrayInt.end());

				for (FXD::U32 n2 = 0; n2 < kRotateArraySize; n2++)
				{
					if (itrArray == arrayInt.end())
					{
						itrArray = arrayInt.begin();
					}
					PRINT_COND_ASSERT(*itrArray++ == (FXD::S32)n2, "Algorithm - inline ForwardItr rotate(ForwardItr first, ForwardItr middle, ForwardItr last)");
				}
			}
		}

		// FXD::Container::Vector (ContiguousIterator)
		{
			using IntVector = FXD::Container::Vector< FXD::S32 >;
			for (FXD::U32 n1 = 10; n1 < 500; n1 += (FXD::U32)rng.rand_range(50, 100))
			{
				IntVector intVector(n1, 0);

				for (FXD::U32 n2 = 0; n2 < n1; n2++)
				{
					FXD::STD::generate_n(intVector.begin(), n1, _GenerateIncrementalIntegers< FXD::S32 >());
					IntVector::iterator intVectorItMiddle = FXD::STD::next(intVector.begin(), n2);
					IntVector::iterator intVectorIt = FXD::STD::rotate(intVector.begin(), intVectorItMiddle, intVector.end());

					for (FXD::U32 n3 = 0; n3 < n1; n3++)
					{
						if (intVectorIt == intVector.end())
						{
							intVectorIt = intVector.begin();
						}
						PRINT_COND_ASSERT(*intVectorIt++ == (FXD::S32)n3, "Algorithm - inline ForwardItr rotate(ForwardItr first, ForwardItr middle, ForwardItr last)");
					}
				}
			}
		}

		// FXD::Container::Deque (RandomAccessItr)
		for (FXD::U32 n1 = 10; n1 < 500; n1 += (FXD::U32)rng.rand_range(50, 100))
		{
			using DequeInt = FXD::Container::Deque< FXD::S32 >;
			DequeInt dequeInt(n1, 0);

			for (FXD::U32 n2 = 0; n2 < n1; n2++)
			{
				FXD::STD::generate_n(dequeInt.begin(), n1, _GenerateIncrementalIntegers< FXD::S32 >());
				DequeInt::iterator itrMid = FXD::STD::next(dequeInt.begin(), n2);
				DequeInt::iterator itrDeque = FXD::STD::rotate(dequeInt.begin(), itrMid, dequeInt.end());

				for (FXD::U32 n3 = 0; n3 < n1; n3++)
				{
					if (itrDeque == dequeInt.end())
					{
						itrDeque = dequeInt.begin();
					}
					PRINT_COND_ASSERT(*itrDeque++ == (FXD::S32)n3, "Algorithm - inline ForwardItr rotate(ForwardItr first, ForwardItr middle, ForwardItr last)");
				}
			}
		}

		// FXD::Container::LinkedList (BidirItr)
		for (FXD::U32 n1 = 10; n1 < 500; n1 += (FXD::U32)rng.rand_range(50, 100))
		{
			using IntList = FXD::Container::LinkedList< FXD::S32 >;
			IntList listInt(n1, 0);

			for (FXD::U32 n2 = 0; n2 < n1; n2++)
			{
				FXD::STD::generate_n(listInt.begin(), n1, _GenerateIncrementalIntegers< FXD::S32 >());
				IntList::iterator itrMid = FXD::STD::next(listInt.begin(), n2);
				IntList::iterator itrList = FXD::STD::rotate(listInt.begin(), itrMid, listInt.end());

				for (FXD::U32 n3 = 0; n3 < n1; n3++)
				{
					if (itrList == listInt.end())
					{
						itrList = listInt.begin();
					}
					PRINT_COND_ASSERT(*itrList++ == (FXD::S32)n3, "Algorithm - inline ForwardItr rotate(ForwardItr first, ForwardItr middle, ForwardItr last)");
				}
			}
		}
	}

	// SampleIterator sample(PopulationIterator _First, PopulationIterator _Last, SampleIterator _Dest, Distance _Count, URBG&& _Func) TODO
	{
		//std::string in = "abcdefgh", out;
		//FXD::sample(in.begin(), in.end(), std::back_inserter(out), 5, std::mt19937{ std::random_device{}() });
	}

	// inline ForwardItr unique(ForwardItr first, ForwardItr last, Predicate pred)
	// inline ForwardItr unique(ForwardItr first, ForwardItr last)
	{
		{
			FXD::S32 arrayInt[] = { 1, 2, 3, 3, 4, 4 };

			FXD::S32* pInt = FXD::STD::unique(arrayInt, arrayInt + 0);
			PRINT_COND_ASSERT(pInt == arrayInt, "Algorithm - inline ForwardItr unique(ForwardItr first, ForwardItr last)");
			PRINT_COND_ASSERT(VerifySequence(arrayInt, arrayInt + 6, FXD::S32(), "unique", 1, 2, 3, 3, 4, 4, -1), "Algorithm - inline ForwardItr unique(ForwardItr first, ForwardItr last)");

			pInt = FXD::STD::unique(arrayInt, arrayInt + 6, FXD::STD::equal_to< FXD::S32 >());
			PRINT_COND_ASSERT(pInt == arrayInt + 4, "Algorithm - inline ForwardItr unique(ForwardItr first, ForwardItr last, Predicate pred)");
			PRINT_COND_ASSERT(VerifySequence(arrayInt, arrayInt + 6, FXD::S32(), "unique", 1, 2, 3, 4, 4, 4, -1), "Algorithm - inline ForwardItr unique(ForwardItr first, ForwardItr last, Predicate pred)");
		}

		{
			TestObject arrayTO[] = { TestObject(1), TestObject(2), TestObject(3), TestObject(3), TestObject(4), TestObject(4) };

			TestObject* pTO = FXD::STD::unique(arrayTO, arrayTO + 6);
			PRINT_COND_ASSERT(pTO == arrayTO + 4, "Algorithm - inline ForwardItr unique(ForwardItr first, ForwardItr last)");
			PRINT_COND_ASSERT(arrayTO[3] == TestObject(4), "Algorithm - inline ForwardItr unique(ForwardItr first, ForwardItr last)");
		}
	}

	// inline OutputItr unique_copy(InputItr first, InputItr last, OutputItr dst, Predicate pred)
	// inline OutputItr unique_copy(InputItr first, InputItr last, OutputItr dst)
	{
		FXD::Core::String8 s1 = "The      string    with many       spaces!";
		FXD::Core::String8 s2;
		FXD::STD::unique_copy(s1.begin(), s1.end(), FXD::STD::back_inserter(s2), [](FXD::UTF8 c1, FXD::UTF8 c2) { return (c1 == ' ') && (c2 == ' '); });
		PRINT_COND_ASSERT(s2 == "The string with many spaces!", "Algorithm - inline OutputItr unique_copy(InputIt first, InputIt last, OutputIt result)");
	}

	return nErrorCount;
}

static FXD::S32 TestPartitioningOperations(void)
{
	FXD::S32 nErrorCount = 0;

	FXD::Core::RandomGen rng(FXD::App::GetRandSeed());

	// inline bool is_partitioned(InputItr first, InputItr last, Predicate pred)
	{
		FXD::Container::Array< FXD::S32, 9 > arrayInt{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		auto isEven = [](FXD::S32 nVal) { return nVal % 2 == 0; };

		PRINT_COND_ASSERT(!FXD::STD::is_partitioned(arrayInt.begin(), arrayInt.end(), isEven), "Algorithm - inline bool is_partitioned(InputItr first, InputItr last, Predicate pred)");

		FXD::STD::partition(arrayInt.begin(), arrayInt.end(), isEven);
		PRINT_COND_ASSERT(FXD::STD::is_partitioned(arrayInt.begin(), arrayInt.end(), isEven), "Algorithm - inline bool is_partitioned(InputItr first, InputItr last, Predicate pred)");

		FXD::STD::reverse(arrayInt.begin(), arrayInt.end());
		PRINT_COND_ASSERT(!FXD::STD::is_partitioned(arrayInt.begin(), arrayInt.end(), isEven), "Algorithm - inline bool is_partitioned(InputItr first, InputItr last, Predicate pred)");
	}

	// inline ForwardItr partition(ForwardItr first, ForwardItr last, Predicate pred)
	{
		FXD::Container::Vector< FXD::S32 > vecInt{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		auto isEven = [](FXD::S32 nVal) { return nVal % 2 == 0; };

		auto itr = FXD::STD::partition(vecInt.begin(), vecInt.end(), isEven);
		PRINT_COND_ASSERT( FXD::STD::any_of(vecInt.begin(), itr, isEven), "Algorithm - inline ForwardItr partition(ForwardItr first, ForwardItr last, Predicate pred)");
		PRINT_COND_ASSERT(!FXD::STD::any_of(itr, vecInt.end(), isEven), "Algorithm - inline ForwardItr partition(ForwardItr first, ForwardItr last, Predicate pred)");
	}

	// inline Core::Pair< OutputItr1, OutputItr2 > partition_copy(InputItr first, InputItr last, OutputItr1 dstTrue, OutputItr2 dstFalse, Predicate pred)
	{
		FXD::S32 arrayInt[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		FXD::S32 arrayTrue[5] = { 0 };
		FXD::S32 arrayFalse[5] = { 0 };
		auto isOver = [](FXD::S32 nVal) { return nVal > 5; };

		FXD::STD::partition_copy(FXD::STD::begin(arrayInt), FXD::STD::end(arrayInt), FXD::STD::begin(arrayTrue), FXD::STD::begin(arrayFalse), isOver);

		PRINT_COND_ASSERT( FXD::STD::any_of(FXD::STD::begin(arrayTrue), FXD::STD::end(arrayTrue), isOver), "Algorithm - inline Core::Pair< OutputItr1, OutputItr2 > partition_copy(InputItr first, InputItr last, OutputItr1 dstTrue, OutputItr2 dstFalse, Predicate pred)");
		PRINT_COND_ASSERT(!FXD::STD::any_of(FXD::STD::begin(arrayFalse), FXD::STD::end(arrayFalse), isOver), "Algorithm - inline Core::Pair< OutputItr1, OutputItr2 > partition_copy(InputItr first, InputItr last, OutputItr1 dstTrue, OutputItr2 dstFalse, Predicate pred)");
	}

	// inline ForwardItr stable_partition(ForwardItr first, ForwardItr last, Predicate pred)
	{
		auto test_stable_sort = [&](auto vecTest, size_t nCount)
		{
			auto isEven = [](auto val) { return (val % 2) == 0; };
			auto isOdd = [](auto val) { return (val % 2) != 0; };

			for (size_t n1 = 0; n1 < nCount; n1++)
			{
				vecTest.push_back((FXD::U16)rng.rand());
			}

			FXD::Container::Vector< FXD::U16 > vecEven;
			FXD::Container::Vector< FXD::U16 > vecOdd;

			FXD::STD::copy_if(vecTest.begin(), vecTest.end(), FXD::STD::back_inserter(vecEven), isEven);
			FXD::STD::copy_if(vecTest.begin(), vecTest.end(), FXD::STD::back_inserter(vecOdd), isOdd);

			const auto boundary = FXD::STD::stable_partition(vecTest.begin(), vecTest.end(), isEven);
			
			const auto mEvenCount = FXD::STD::distance(vecTest.begin(), boundary);
			const auto nOddCount = FXD::STD::distance(boundary, vecTest.end());
			const auto nEvenExpectedCount = (ptrdiff_t)vecEven.size();
			const auto nOddExpectedCount = (ptrdiff_t)vecOdd.size();

			PRINT_COND_ASSERT(mEvenCount == nEvenExpectedCount, "Algorithm - inline ForwardItr stable_partition(ForwardItr first, ForwardItr last, Predicate pred)");
			PRINT_COND_ASSERT(nOddCount == nOddExpectedCount, "Algorithm - inline ForwardItr stable_partition(ForwardItr first, ForwardItr last, Predicate pred)");
			PRINT_COND_ASSERT(FXD::STD::equal(vecTest.begin(), boundary, vecEven.begin()), "Algorithm - inline ForwardItr stable_partition(ForwardItr first, ForwardItr last, Predicate pred)");
			PRINT_COND_ASSERT(FXD::STD::equal(boundary, vecTest.end(), vecOdd.begin()), "Algorithm - inline ForwardItr stable_partition(ForwardItr first, ForwardItr last, Predicate pred)");
		};

		test_stable_sort(FXD::Container::Vector< FXD::U16 >(), 1000); // Test stable_partition
		test_stable_sort(FXD::Container::Vector< FXD::U16 >(), 0);	// Test stable_partition on empty container
		test_stable_sort(FXD::Container::Vector< FXD::U16 >(), 1);	// Test stable_partition on container of one element
		test_stable_sort(FXD::Container::Vector< FXD::U16 >(), 2);	// Test stable_partition on container of two element
		test_stable_sort(FXD::Container::LinkedList< FXD::U16 >(), 0);	// Test stable_partition on bidirectional iterator (not random access)
	}

	// inline ForwardItr partition_point(ForwardItr first, ForwardItr last, Predicate pred)
	{
		FXD::Container::Array< FXD::S32, 9 > arrayInt{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };

		auto isEven = [](FXD::S32 nVal) { return nVal % 2 == 0; };
		FXD::STD::partition(arrayInt.begin(), arrayInt.end(), isEven);
		auto p = FXD::STD::partition_point(arrayInt.begin(), arrayInt.end(), isEven);

		FXD::S32 arrayBefore[4] = { 8, 2, 6, 4 };
		FXD::STD::equal(arrayInt.begin(), p, FXD::STD::begin(arrayBefore));

		FXD::S32 arrayAfter[5] = { 5, 3, 7, 1, 9 };
		FXD::STD::equal(p, arrayInt.end(), FXD::STD::begin(arrayAfter));
	}

	return nErrorCount;
}

static FXD::S32 TestSortingOperations(void) 
{
	FXD::S32 nErrorCount = 0;

	FXD::Core::RandomGen rng(FXD::App::GetRandSeed());

	// inline bool is_sorted(ForwardItr first, ForwardItr last)
	{
		FXD::S32 arrayInt[] = { 0, 1, 2, 2, 2, 3, 4, 5, 6, 7, 8, 9, 9, 8, 7, 6, 5, 4, 3, 2, 2, 2, 1, 0 };

		PRINT_COND_ASSERT( FXD::STD::is_sorted(arrayInt + 0, arrayInt + 0), "Algorithm - inline bool is_sorted(ForwardItr first, ForwardItr last)");
		PRINT_COND_ASSERT( FXD::STD::is_sorted(arrayInt + 2, arrayInt + 4), "Algorithm - inline bool is_sorted(ForwardItr first, ForwardItr last)");
		PRINT_COND_ASSERT( FXD::STD::is_sorted(arrayInt + 0, arrayInt + 10), "Algorithm - inline bool is_sorted(ForwardItr first, ForwardItr last)");
		PRINT_COND_ASSERT(!FXD::STD::is_sorted(arrayInt + 0, arrayInt + 14), "Algorithm - inline bool is_sorted(ForwardItr first, ForwardItr last)");
		PRINT_COND_ASSERT( FXD::STD::is_sorted(arrayInt + 11, arrayInt + 23, FXD::STD::greater< FXD::S32 >()), "Algorithm - inline bool is_sorted(ForwardItr first, ForwardItr last, Predicate pred)");
	}

	// inline ForwardItr is_sorted_until(ForwardItr first, ForwardItr last)
	{
		FXD::S32 arrayIntSorted[] = { 0, 1, 2, 3, 4,  5, 6, 7, 8, 9 };
		FXD::S32 arrayIntNotSorted[] = { 0, 1, 2, 3, 4, 42, 6, 7, 8, 9 };

		PRINT_COND_ASSERT(FXD::STD::is_sorted_until(arrayIntSorted + FXD::STD::array_size(arrayIntSorted), arrayIntSorted + FXD::STD::array_size(arrayIntSorted)) == arrayIntSorted + FXD::STD::array_size(arrayIntSorted), "Algorithm - inline ForwardItr is_sorted_until(ForwardItr first, ForwardItr last)");
		PRINT_COND_ASSERT(FXD::STD::is_sorted_until(arrayIntSorted, arrayIntSorted + FXD::STD::array_size(arrayIntSorted)) == arrayIntSorted + FXD::STD::array_size(arrayIntSorted), "Algorithm - inline ForwardItr is_sorted_until(ForwardItr first, ForwardItr last)");
		PRINT_COND_ASSERT(FXD::STD::is_sorted_until(arrayIntSorted + 0, arrayIntSorted + 0) == arrayIntSorted, "Algorithm - inline ForwardItr is_sorted_until(ForwardItr first, ForwardItr last)");
		PRINT_COND_ASSERT(FXD::STD::is_sorted_until(arrayIntSorted + 2, arrayIntSorted + 8) == arrayIntSorted + 8, "Algorithm - inline ForwardItr is_sorted_until(ForwardItr first, ForwardItr last)");
		PRINT_COND_ASSERT(FXD::STD::is_sorted_until(arrayIntNotSorted + 2, arrayIntNotSorted + 8) == arrayIntNotSorted + 6, "Algorithm - inline ForwardItr is_sorted_until(ForwardItr first, ForwardItr last)");

		PRINT_COND_ASSERT(FXD::STD::is_sorted_until(arrayIntSorted + FXD::STD::array_size(arrayIntSorted), arrayIntSorted + FXD::STD::array_size(arrayIntSorted), FXD::STD::less< FXD::S32 >()) == arrayIntSorted + FXD::STD::array_size(arrayIntSorted), "Algorithm - inline ForwardItr is_sorted_until(ForwardItr first, ForwardItr last, Predicate pred)");
		PRINT_COND_ASSERT(FXD::STD::is_sorted_until(arrayIntNotSorted + 2, arrayIntNotSorted + 8, FXD::STD::less< FXD::S32 >()) == arrayIntNotSorted + 6, "Algorithm - inline ForwardItr is_sorted_until(ForwardItr first, ForwardItr last, Predicate pred)");
	}

	// inline void sort(Iterator first, Iterator last)
	// inline void bubble_sort(ForwardItr first, ForwardItr last)
	{
		FXD::Container::Vector< FXD::S64 > vecInt;
		FXD::Container::Vector< FXD::S64 > vecIntSaved;

		for (FXD::S32 n1 = 0; n1 < (150 + (FXD::STD::to_underlying(gFXD_TestLevel) * 20)); n1 += (n1 < 5) ? 1 : 37)
		{
			for (FXD::S32 n2 = 0; n2 < n1; n2++)
			{
				vecIntSaved.push_back(n2);

				if (rng.rand_limit(10) == 0)
				{
					vecIntSaved.push_back(n2);

					if (rng.rand_limit(5) == 0)
					{
						vecIntSaved.push_back(n2);
					}
				}
			}

			const FXD::S64 nExpectedSum = FXD::STD::accumulate(vecIntSaved.begin(), vecIntSaved.end(), FXD::S64(0));

			for (FXD::S32 n2 = 0; n2 < 300 + (FXD::STD::to_underlying(gFXD_TestLevel) * 10); n2++)
			{
				FXD::STD::random_shuffle(vecIntSaved.begin(), vecIntSaved.end(), rng);

				vecInt = vecIntSaved;
				FXD::STD::sort(vecInt.begin(), vecInt.end());
				PRINT_COND_ASSERT(FXD::STD::is_sorted(vecInt.begin(), vecInt.end()), "Algorithm - inline void sort(Iterator first, Iterator last)");
				PRINT_COND_ASSERT(FXD::STD::accumulate(vecIntSaved.begin(), vecIntSaved.end(), FXD::S64(0)) == nExpectedSum, "Algorithm - inline void sort(Iterator first, Iterator last)");

				vecInt = vecIntSaved;
				FXD::STD::bubble_sort(vecInt.begin(), vecInt.end());
				PRINT_COND_ASSERT(FXD::STD::is_sorted(vecInt.begin(), vecInt.end()), "Algorithm - inline void bubble_sort(ForwardItr first, ForwardItr last)");
				PRINT_COND_ASSERT(FXD::STD::accumulate(vecIntSaved.begin(), vecIntSaved.end(), FXD::S64(0)) == nExpectedSum, "Algorithm - inline void bubble_sort(ForwardItr first, ForwardItr last)");
			}
		}
	}

	// Test FXD::STD::sort with move-only type
	// inline void sort(Iterator first, Iterator last)
	{
		{
			FXD::Container::Vector< std::unique_ptr < FXD::S32 >> vec;
			FXD::STD::sort(vec.begin(), vec.end(), [](const std::unique_ptr< FXD::S32 >& lhs, const std::unique_ptr< FXD::S32 >& rhs) { return *lhs < *rhs; });
		}

		{
			FXD::Container::Vector< std::unique_ptr< FXD::S32 > > vec;
			FXD::STD::sort(vec.begin(), vec.end());
		}

		{
			FXD::Container::Vector< Game::MissingMoveConstructor > vec;
			FXD::STD::sort(vec.begin(), vec.end(), [](const Game::MissingMoveConstructor& lhs, const Game::MissingMoveConstructor& rhs) { return lhs < rhs; });
		}

		{
			FXD::Container::Vector< Game::MissingMoveConstructor > vec;
			FXD::STD::sort(vec.begin(), vec.end());
		}
		{
			FXD::Container::Vector< Game::MissingMoveAssignable > vec;
			FXD::STD::sort(vec.begin(), vec.end(), [](const Game::MissingMoveAssignable& lhs, const Game::MissingMoveAssignable& rhs) { return lhs < rhs; });
		}

		{
			FXD::Container::Vector< Game::MissingMoveAssignable > vec;
			FXD::STD::sort(vec.begin(), vec.end());
		}

		{
			FXD::Container::Vector< std::unique_ptr< FXD::S32 > > vecInt;
			vecInt.emplace_back(new FXD::S32(7));
			vecInt.emplace_back(new FXD::S32(-42));
			vecInt.emplace_back(new FXD::S32(5));
			FXD::STD::sort(vecInt.begin(), vecInt.end(), [](const std::unique_ptr< FXD::S32 >& lhs, const std::unique_ptr< FXD::S32 >& rhs) { return (*lhs < *rhs); });
			PRINT_COND_ASSERT(*vecInt[0] == -42, "Algorithm - Failed");
			PRINT_COND_ASSERT(*vecInt[1] == 5, "Algorithm - Failed");
			PRINT_COND_ASSERT(*vecInt[2] == 7, "Algorithm - Failed");
		}

		{
			for (FXD::U32 nTest = 0; nTest < 50; ++nTest)
			{
				FXD::Container::Vector< std::unique_ptr< FXD::S32 > > vecUnique;

				for (FXD::S32 n1 = 0; n1 < 100; ++n1)
				{
					FXD::S32 nRand = rng();
					vecUnique.emplace_back(new FXD::S32(nRand));
				}

				auto vecUniqueCmp = [](const std::unique_ptr< FXD::S32 >& lhs, const std::unique_ptr< FXD::S32 >& rhs) { return *lhs < *rhs; };
				FXD::STD::sort(vecUnique.begin(), vecUnique.end(), vecUniqueCmp);
				PRINT_COND_ASSERT(FXD::STD::is_sorted(vecUnique.begin(), vecUnique.end(), vecUniqueCmp), "Algorithm - Failed");
			}
		}
	}


	// inline void partial_sort(RandomAccessItr first, RandomAccessItr mid, RandomAccessItr last, Predicate pred)
	// inline void partial_sort(RandomAccessItr first, RandomAccessItr mid, RandomAccessItr last)
	{
		FXD::Container::Array< FXD::S32, 10 > vecInt{ 5, 7, 4, 2, 8, 6, 1, 9, 0, 3 };
		FXD::STD::partial_sort(vecInt.begin(), vecInt.begin() + 3, vecInt.end());

		PRINT_COND_ASSERT(*vecInt.begin() == 0, "Algorithm - inline void partial_sort(RandomAccessItr first, RandomAccessItr mid, RandomAccessItr last)");
		PRINT_COND_ASSERT(*vecInt.rbegin() == 3, "Algorithm - inline void partial_sort(RandomAccessItr first, RandomAccessItr mid, RandomAccessItr last)");
		PRINT_COND_ASSERT(*FXD::STD::is_sorted_until(vecInt.begin(), vecInt.end()) == 6, "Algorithm - inline void partial_sort(RandomAccessItr first, RandomAccessItr mid, RandomAccessItr last)");
	}
	// inline RandomAccessItr partial_sort_copy(InputItr first, InputItr last, RandomAccessItr dstFirst, RandomAccessItr dstLast, Predicate pred)
	// inline RandomAccessItr partial_sort_copy(InputItr first, InputItr last, RandomAccessItr dstFirst, RandomAccessItr dstLast)
	{
		FXD::Container::Vector< FXD::S32 > v0{ 4, 2, 5, 1, 3 };
		FXD::Container::Vector< FXD::S32 > v1{ 10, 11, 12 };
		FXD::Container::Vector< FXD::S32 > v2{ 10, 11, 12, 13, 14, 15, 16 };
		FXD::Container::Vector< FXD::S32 >::iterator itr;
		
		itr = FXD::STD::partial_sort_copy(v0.begin(), v0.end(), v1.begin(), v1.end());
		itr = FXD::STD::partial_sort_copy(v0.begin(), v0.end(), v2.begin(), v2.end(), FXD::STD::greater< FXD::S32 >());
	}

	// stable_sort TODO
	{
	}

	// nth_element TODO
	{
	}

	return nErrorCount;
}

static FXD::S32 TestBinarySearchOperations(void)
{
	FXD::S32 nErrorCount = 0;

	FXD::Core::RandomGen rng(FXD::App::GetRandSeed());

	// inline ForwardItr lower_bound(ForwardItr first, ForwardItr last, const T& val, Predicate pred)
	// inline ForwardItr lower_bound(ForwardItr first, ForwardItr last, const T& val)
	{
		{
			PRINT_COND_ASSERT((FXD::STD::lower_bound((FXD::S32*)nullptr, (FXD::S32*)nullptr, 100) == nullptr), "Algorithm - inline ForwardItr lower_bound(ForwardItr first, ForwardItr last, const T& val)");

			for (FXD::S32 n1 = 0; n1 < 20 + (FXD::STD::to_underlying(gFXD_TestLevel) * 20); n1++)
			{
				FXD::Container::Deque< FXD::S32 > dequeInt((FXD::S32)rng.rand_range(2, 500));

				for (FXD::S32 n2 = 0, n2End = (FXD::S32)dequeInt.size(); n2 < n2End; n2++)
				{
					dequeInt[n2] = (FXD::S32)rng.rand_limit(n2End / 2); // This will result in both gaps and duplications.
				}

				for (FXD::S32 n3 = 0, n3End = (FXD::S32)dequeInt.size(); n3 < n3End; n3++)
				{
					FXD::Container::Deque< FXD::S32 >::iterator itr = FXD::STD::lower_bound(dequeInt.begin(), dequeInt.end(), n3);

					if (itr != dequeInt.begin())
					{
						PRINT_COND_ASSERT(*(itr - 1) < n3, "Algorithm - inline ForwardItr lower_bound(ForwardItr first, ForwardItr last, const T& val)");
					}

					if (itr != dequeInt.end())
					{
						PRINT_COND_ASSERT((n3 < *itr) || !(*itr < n3), "Algorithm - inline ForwardItr lower_bound(ForwardItr first, ForwardItr last, const T& val)");
					}
				}
			}
		}

		{
			for (FXD::S32 n1 = 0; n1 < 20 + (FXD::STD::to_underlying(gFXD_TestLevel) * 20); n1++)
			{
				FXD::Container::LinkedList< TestObject > listTO;
				FXD::S32 nSize = (FXD::S32)rng.rand_range(2, 500);

				for (FXD::S32 n2 = 0, n2End = nSize; n2 < n2End; n2++)
				{
					listTO.push_back(TestObject((FXD::S32)rng.rand_limit(n2End / 2)));
				}

				for (FXD::S32 n3 = 0; n3 < nSize; n3++)
				{
					TestObject toN3(n3);
					FXD::Container::LinkedList< TestObject >::iterator itr = FXD::STD::lower_bound(listTO.begin(), listTO.end(), toN3);

					if (itr != listTO.begin())
					{
						--itr;
						PRINT_COND_ASSERT(*itr < toN3, "Algorithm - inline ForwardItr lower_bound(ForwardItr first, ForwardItr last, const T& val)");
						++itr;
					}

					if (itr != listTO.end())
					{
						PRINT_COND_ASSERT((toN3 < *itr) || !(*itr < toN3), "Algorithm - inline ForwardItr lower_bound(ForwardItr first, ForwardItr last, const T& val)");
					}
				}
			}
		}
	}

	// inline ForwardItr upper_bound(ForwardItr first, ForwardItr last, const T& val, Predicate pred)
	// inline ForwardItr upper_bound(ForwardItr first, ForwardItr last, const T& val)
	{
		{
			PRINT_COND_ASSERT((FXD::STD::upper_bound((FXD::S32*)nullptr, (FXD::S32*)nullptr, 100) == nullptr), "Algorithm - inline ForwardItr upper_bound(ForwardItr first, ForwardItr last, const T& val)");

			for (FXD::S32 n1 = 0; n1 < 20 + (FXD::STD::to_underlying(gFXD_TestLevel) * 20); n1++)
			{
				FXD::Container::Deque< FXD::S32 > dequeInt(rng.rand_range(2, 500));

				for (FXD::S32 n2 = 0, n2End = dequeInt.size(); n2 < n2End; n2++)
				{
					dequeInt[n2] = (FXD::S32)rng.rand_limit((FXD::U32)n2End / 2);
				}

				for (FXD::S32 n3 = 0, n3End = (FXD::S32)dequeInt.size(); n3 < n3End; n3++)
				{
					FXD::Container::Deque< FXD::S32 >::iterator itr = FXD::STD::upper_bound(dequeInt.begin(), dequeInt.end(), n3);

					if (itr != dequeInt.begin())
					{
						PRINT_COND_ASSERT((*(itr - 1) < n3) || !(n3 < *(itr - 1)), "Algorithm - inline ForwardItr upper_bound(ForwardItr first, ForwardItr last, const T& val)");
					}
					if (itr != dequeInt.end())
					{
						PRINT_COND_ASSERT((n3 < *itr), "Algorithm - inline ForwardItr upper_bound(ForwardItr first, ForwardItr last, const T& val)");
					}
				}
			}
		}

		{
			for (FXD::S32 n1 = 0; n1 < 20 + (FXD::STD::to_underlying(gFXD_TestLevel) * 20); n1++)
			{
				FXD::Container::LinkedList< TestObject > listTO;
				FXD::S32 nSize = (FXD::S32)rng.rand_range(2, 500);

				for (FXD::U32 n2 = 0, n2End = nSize; n2 < n2End; n2++)
				{
					listTO.push_back(TestObject((FXD::S32)rng.rand_limit(n2End / 2)));
				}

				for (FXD::U32 n3 = 0; n3 < nSize; n3++)
				{
					TestObject toN3(n3);
					FXD::Container::LinkedList< TestObject >::iterator itr = FXD::STD::upper_bound(listTO.begin(), listTO.end(), toN3);

					if (itr != listTO.begin())
					{
						--itr;
						PRINT_COND_ASSERT((*itr < toN3) || !(toN3 < *itr), "Algorithm - inline ForwardItr upper_bound(ForwardItr first, ForwardItr last, const T& val)");
						++itr;
					}

					if (itr != listTO.end())
					{
						PRINT_COND_ASSERT((toN3 < *itr), "Algorithm - inline ForwardItr upper_bound(ForwardItr first, ForwardItr last, const T& val)");
					}
				}
			}
		}
	}

	// inline bool binary_search(ForwardItr first, ForwardItr last, const T& val, Predicate pred)
	// inline bool binary_search(ForwardItr first, ForwardItr last, const T& val)
	// inline ForwardItr binary_search_i(ForwardItr first, ForwardItr last, const T& val, Predicate pred)
	// inline ForwardItr binary_search_i(ForwardItr first, ForwardItr last, const T& val)
	{
		{
			FXD::Container::Vector< FXD::S32 > vecInt;
			for (FXD::S32 n1 = 0; n1 < 1000; n1++)
			{
				vecInt.push_back(n1);
			}

			PRINT_COND_ASSERT(!FXD::STD::binary_search(vecInt.begin(), vecInt.begin(), 0), "Algorithm - inline bool binary_search(ForwardItr first, ForwardItr last, const T& val)");
			PRINT_COND_ASSERT( FXD::STD::binary_search(vecInt.begin(), vecInt.begin() + 1, 0), "Algorithm - inline bool binary_search(ForwardItr first, ForwardItr last, const T& val)");
			PRINT_COND_ASSERT( FXD::STD::binary_search(vecInt.begin(), vecInt.end(), 733, FXD::STD::less< FXD::S32 >()), "Algorithm - inline bool binary_search(ForwardItr first, ForwardItr last, const T& val, Predicate pred)");

			FXD::Container::Vector< FXD::S32 >::iterator itr = FXD::STD::binary_search_i(vecInt.begin(), vecInt.begin(), 0);
			PRINT_COND_ASSERT(itr == vecInt.begin(), "Algorithm - inline ForwardItr binary_search_i(ForwardItr first, ForwardItr last, const T& val)");

			itr = FXD::STD::binary_search_i(vecInt.begin(), vecInt.begin() + 1, 0, FXD::STD::less< FXD::S32 >());
			PRINT_COND_ASSERT(itr == vecInt.begin(), "Algorithm - inline ForwardItr binary_search_i(ForwardItr first, ForwardItr last, const T& val)");

			itr = FXD::STD::binary_search_i(vecInt.begin(), vecInt.end(), 733);
			PRINT_COND_ASSERT(itr == vecInt.begin() + 733, "Algorithm - inline ForwardItr binary_search_i(ForwardItr first, ForwardItr last, const T& val)");
		}

		{
			FXD::Container::LinkedList< TestObject > listTO;
			FXD::Container::LinkedList< TestObject >::iterator toItr;
			for (FXD::S32 n1 = 0; n1 < 1000; n1++)
			{
				listTO.push_back(TestObject(n1));
			}

			PRINT_COND_ASSERT(!FXD::STD::binary_search(listTO.begin(), listTO.begin(), TestObject(0), FXD::STD::less< TestObject >()), "Algorithm - inline bool binary_search(ForwardItr first, ForwardItr last, const T& val, Predicate pred)");

			toItr = listTO.begin();
			toItr++;
			PRINT_COND_ASSERT(FXD::STD::binary_search(listTO.begin(), toItr, TestObject(0)), "Algorithm - inline bool binary_search(ForwardItr first, ForwardItr last, const T& val, Predicate pred)");
			PRINT_COND_ASSERT(FXD::STD::binary_search(listTO.begin(), listTO.end(), TestObject(733)), "Algorithm - inline bool binary_search(ForwardItr first, ForwardItr last, const T& val, Predicate pred)");

			toItr = FXD::STD::binary_search_i(listTO.begin(), listTO.begin(), TestObject(0), FXD::STD::less< TestObject >()); // No-op
			PRINT_COND_ASSERT(toItr == listTO.begin(), "Algorithm - inline ForwardItr binary_search_i(ForwardItr first, ForwardItr last, const T& val)");

			toItr = listTO.begin();
			toItr++;
			toItr = FXD::STD::binary_search_i(listTO.begin(), toItr, TestObject(0));
			PRINT_COND_ASSERT(*toItr == TestObject(0), "Algorithm - inline ForwardItr binary_search_i(ForwardItr first, ForwardItr last, const T& val)");

			toItr = FXD::STD::binary_search_i(listTO.begin(), listTO.end(), TestObject(733));
			PRINT_COND_ASSERT(*toItr == TestObject(733), "Algorithm - inline ForwardItr binary_search_i(ForwardItr first, ForwardItr last, const T& val)");
		}
	}

	// inline Core::Pair< ForwardItr, ForwardItr > equal_range(ForwardItr first, ForwardItr last, const T& val, Predicate pred)
	// inline Core::Pair< ForwardItr, ForwardItr > equal_range(ForwardItr first, ForwardItr last, const T& val)
	{
		{
			FXD::Core::Pair< FXD::S32*, FXD::S32* > pInt = FXD::STD::equal_range((FXD::S32*)nullptr, (FXD::S32*)nullptr, 100);
			PRINT_COND_ASSERT(pInt.first == nullptr, "Algorithm - inline Core::Pair< ForwardItr, ForwardItr > equal_range(ForwardItr first, ForwardItr last, const T& val)");
			PRINT_COND_ASSERT(pInt.second == nullptr, "Algorithm - inline Core::Pair< ForwardItr, ForwardItr > equal_range(ForwardItr first, ForwardItr last, const T& val)");

			for (FXD::S32 n1 = 0; n1 < 20 + (FXD::STD::to_underlying(gFXD_TestLevel) * 20); n1++)
			{
				FXD::Container::Deque< FXD::S32 > dequeInt((FXD::S32)rng.rand_range(2, 500));

				for (FXD::S32 n2 = 0, n2End = (FXD::S32)dequeInt.size(); n2 < n2End; n2++)
				{
					dequeInt[n2] = (FXD::S32)rng.rand_limit(n2End / 2); // This will result in both gaps and duplications.
				}

				for (FXD::S32 n3 = 0, n3End = (FXD::S32)dequeInt.size(); n3 < n3End; n3++)
				{
					FXD::Core::Pair< FXD::Container::Deque< FXD::S32 >::iterator, FXD::Container::Deque< FXD::S32 >::iterator > itr = FXD::STD::equal_range(dequeInt.begin(), dequeInt.end(), n3);

					// Test itr.first as lower_bound.
					if (itr.first != dequeInt.begin())
					{
						PRINT_COND_ASSERT(*(itr.first - 1) < n3, "Algorithm - inline Core::Pair< ForwardItr, ForwardItr > equal_range(ForwardItr first, ForwardItr last, const T& val)");
					}
					if (itr.first != dequeInt.end())
					{
						PRINT_COND_ASSERT((n3 < *itr.first) || !(*itr.first < n3), "Algorithm - inline Core::Pair< ForwardItr, ForwardItr > equal_range(ForwardItr first, ForwardItr last, const T& val)");
					}

					// Test itr.second as upper_bound.
					if (itr.second != dequeInt.begin())
					{
						PRINT_COND_ASSERT((*(itr.second - 1) < n3) || !(n3 < *(itr.second - 1)), "Algorithm - inline Core::Pair< ForwardItr, ForwardItr > equal_range(ForwardItr first, ForwardItr last, const T& val)");
					}
					if (itr.second != dequeInt.end())
					{
						PRINT_COND_ASSERT(n3 < *itr.second, "Algorithm - inline Core::Pair< ForwardItr, ForwardItr > equal_range(ForwardItr first, ForwardItr last, const T& val)");
					}
				}
			}
		}

		{
			for (FXD::S32 n1 = 0; n1 < 20 + (FXD::STD::to_underlying(gFXD_TestLevel) * 20); n1++)
			{
				FXD::Container::LinkedList< TestObject > listTO;
				FXD::S32 nSize = (FXD::S32)rng.rand_range(2, 500);

				for (FXD::S32 n2 = 0, n2End = nSize; n2 < n2End; n2++)
				{
					listTO.push_back(TestObject((FXD::S32)rng.rand_limit(n2End / 2))); // This will result in both gaps and duplications.
				}

				for (FXD::S32 n3 = 0; n3 < nSize; n3++)
				{
					TestObject toN3(n3);
					FXD::Core::Pair< FXD::Container::LinkedList< TestObject >::iterator, FXD::Container::LinkedList< TestObject >::iterator > itr = FXD::STD::equal_range(listTO.begin(), listTO.end(), toN3);

					// Test itr.first as lower_bound
					if (itr.first != listTO.begin())
					{
						--itr.first;
						PRINT_COND_ASSERT(*itr.first < toN3, "Algorithm - FXD::Core::Pair< ForwardItr, ForwardItr > equal_range(ForwardItr first, ForwardItr last, const T& val)");
						++itr.first;
					}

					if (itr.first != listTO.end())
					{
						PRINT_COND_ASSERT((toN3 < *itr.first) || !(*itr.first < toN3), "Algorithm - FXD::Core::Pair< ForwardItr, ForwardItr > equal_range(ForwardItr first, ForwardItr last, const T& val)");
					}

					// Test itr.second as upper_bound
					if (itr.second != listTO.begin())
					{
						--itr.second;
						PRINT_COND_ASSERT((*itr.second < toN3) || !(toN3 < *itr.second), "Algorithm - FXD::Core::Pair< ForwardItr, ForwardItr > equal_range(ForwardItr first, ForwardItr last, const T& val)");
						++itr.second;
					}

					if (itr.second != listTO.end())
					{
						PRINT_COND_ASSERT(toN3 < *itr.second, "Algorithm - FXD::Core::Pair< ForwardItr, ForwardItr > equal_range(ForwardItr first, ForwardItr last, const T& val)");
					}
				}
			}
		}
	}

	// inline OutputItr merge(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst, Predicate pred)
	// inline OutputItr merge(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)
	{
		FXD::S32 arrayInt1[] = { 5, 10, 15, 20, 25 };
		FXD::S32 arrayInt2[] = { 50, 40, 30, 20, 10 };
		FXD::Container::Vector< FXD::S32 > vecInt(10);

		FXD::STD::sort(arrayInt1, arrayInt1 + 5);
		FXD::STD::sort(arrayInt2, arrayInt2 + 5);
		FXD::STD::merge(arrayInt1, arrayInt1 + 5, arrayInt2, arrayInt2 + 5, vecInt.begin());

		PRINT_COND_ASSERT(*vecInt.begin() == 5, "Algorithm - inline OutputItr merge(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)");
		PRINT_COND_ASSERT(*vecInt.rbegin() == 50, "Algorithm - inline OutputItr merge(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)");
		PRINT_COND_ASSERT( FXD::STD::is_sorted(vecInt.begin(), vecInt.end()), "Algorithm - inline OutputItr merge(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)");
	}

	// inplace_merge TODO
	{
	}

	return nErrorCount;
}

static FXD::S32 TestSetOperations(void)
{
	FXD::S32 nErrorCount = 0;

	// inline bool includes(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Pred pred)
	// inline bool includes(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)
	{
		FXD::Container::Vector< FXD::UTF8 > v1{ 'a', 'b', 'c', 'f', 'h', 'x' };
		FXD::Container::Vector< FXD::UTF8 > v2{ 'a', 'b', 'c' };
		FXD::Container::Vector< FXD::UTF8 > v3{ 'a', 'c' };
		FXD::Container::Vector< FXD::UTF8 > v4{ 'g' };
		FXD::Container::Vector< FXD::UTF8 > v5{ 'a', 'c', 'g' };
		FXD::Container::Vector< FXD::UTF8 > v6{ 'A', 'B', 'C' };
		auto cmpNocase = [](FXD::UTF8 lhs, FXD::UTF8 rhs) { return FXD::Core::CharTraits< FXD::UTF8 >::to_lower(lhs) < FXD::Core::CharTraits< FXD::UTF8 >::to_lower(rhs); };

		PRINT_COND_ASSERT( FXD::STD::includes(v1.begin(), v1.end(), v2.begin(), v2.end()), "Algorithm - inline bool includes(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)");
		PRINT_COND_ASSERT( FXD::STD::includes(v1.begin(), v1.end(), v3.begin(), v3.end()), "Algorithm - inline bool includes(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)");
		PRINT_COND_ASSERT(!FXD::STD::includes(v1.begin(), v1.end(), v4.begin(), v4.end()), "Algorithm - inline bool includes(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)");	
		PRINT_COND_ASSERT(!FXD::STD::includes(v1.begin(), v1.end(), v5.begin(), v5.end()), "Algorithm - inline bool includes(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)");
		PRINT_COND_ASSERT( FXD::STD::includes(v1.begin(), v1.end(), v6.begin(), v6.end(), cmpNocase), "Algorithm - inline bool includes(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)");
	}

	// inline OutputItr set_difference(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst, Predicate pred)
	// inline OutputItr set_difference(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)
	{
		FXD::S32 arrayInt1[] = { 0, 0, 2, 5, 8, 8, 12, 24, 26, 43 };
		FXD::S32 arrayInt2[] = { 0, 0, 0, 5, 7, 8, 11, 24, 25, 43 };
		FXD::S32 arrayInt3[] = { 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 };

		FXD::STD::set_difference(arrayInt1, arrayInt1 + 0, arrayInt2, arrayInt2 + 0, arrayInt3);
		PRINT_COND_ASSERT(VerifySequence(arrayInt3, arrayInt3 + 10, FXD::S32(), "set_difference", 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, -1), "Algorithm - inline OutputItr set_difference(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)");

		FXD::STD::set_difference(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2 + 10, arrayInt3);
		PRINT_COND_ASSERT(VerifySequence(arrayInt3, arrayInt3 + 10, FXD::S32(), "set_difference", 2, 8, 12, 26, 9, 9, 9, 9, 9, 9, -1), "Algorithm - inline OutputItr set_difference(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)");

		arrayInt3[0] = arrayInt3[1] = arrayInt3[2] = 9;

		FXD::STD::set_difference(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2 + 10, arrayInt3, FXD::STD::less< FXD::S32 >());
		PRINT_COND_ASSERT(VerifySequence(arrayInt3, arrayInt3 + 10, FXD::S32(), "set_difference", 2, 8, 12, 26, 9, 9, 9, 9, 9, 9, -1), "Algorithm - inline OutputItr set_difference(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)");
	}

	// inline OutputItr set_intersection(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst, Predicate pred)
	// inline OutputItr set_intersection(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)
	{
		FXD::S32 arrayInt1[] = { 0, 0, 2, 5, 8, 8, 12, 24, 26, 43 };
		FXD::S32 arrayInt2[] = { 0, 0, 0, 5, 7, 8, 11, 24, 25, 43 };
		FXD::S32 arrayInt3[] = { 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 };

		FXD::STD::set_intersection(arrayInt1, arrayInt1 + 0, arrayInt2, arrayInt2 + 0, arrayInt3);
		PRINT_COND_ASSERT(VerifySequence(arrayInt3, arrayInt3 + 10, FXD::S32(), "set_intersection", 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, -1), "Algorithm - inline OutputItr set_intersection(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)");

		FXD::STD::set_intersection(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2 + 10, arrayInt3);
		PRINT_COND_ASSERT(VerifySequence(arrayInt3, arrayInt3 + 10, FXD::S32(), "set_intersection", 0, 0, 5, 8, 24, 43, 9, 9, 9, 9, -1), "Algorithm - inline OutputItr set_intersection(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)");

		arrayInt3[0] = arrayInt3[1] = arrayInt3[2] = arrayInt3[4] = arrayInt3[5] = arrayInt3[6] = 9;

		FXD::STD::set_intersection(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2 + 10, arrayInt3, FXD::STD::less< FXD::S32 >());
		PRINT_COND_ASSERT(VerifySequence(arrayInt3, arrayInt3 + 10, FXD::S32(), "set_intersection", 0, 0, 5, 8, 24, 43, 9, 9, 9, 9, -1), "Algorithm - inline OutputItr set_intersection(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)");
	}

	// inline OutputItr set_symmetric_difference(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst, Predicate pred)
	// inline OutputItr set_symmetric_difference(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)
	{
		FXD::S32 arrayInt1[] = { 0, 0, 2, 5, 8, 8, 12, 24, 26, 43 };
		FXD::S32 arrayInt2[] = { 0, 0, 0, 5, 7, 8, 11, 24, 25, 43 };
		FXD::S32 arrayInt3[] = { 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 };

		FXD::STD::set_symmetric_difference(arrayInt1, arrayInt1 + 0, arrayInt2, arrayInt2 + 0, arrayInt3);
		PRINT_COND_ASSERT(VerifySequence(arrayInt3, arrayInt3 + 10, FXD::S32(), "set_symmetric_difference", 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, -1), "Algorithm - inline OutputItr set_symmetric_difference(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)");

		FXD::STD::set_symmetric_difference(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2 + 10, arrayInt3);
		PRINT_COND_ASSERT(VerifySequence(arrayInt3, arrayInt3 + 10, FXD::S32(), "set_symmetric_difference", 0, 2, 7, 8, 11, 12, 25, 26, 9, 9, -1), "Algorithm - inline OutputItr set_symmetric_difference(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)");

		arrayInt3[0] = arrayInt3[1] = arrayInt3[2] = arrayInt3[4] = arrayInt3[5] = arrayInt3[6] = 9;

		FXD::STD::set_symmetric_difference(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2 + 10, arrayInt3, FXD::STD::less< FXD::S32 >());
		PRINT_COND_ASSERT(VerifySequence(arrayInt3, arrayInt3 + 10, FXD::S32(), "set_symmetric_difference", 0, 2, 7, 8, 11, 12, 25, 26, 9, 9, -1), "Algorithm - inline OutputItr set_symmetric_difference(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)");
	}

	// inline OutputItr set_union(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst, Predicate pred)
	// inline OutputItr set_union(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)
	{
		FXD::S32 arrayInt1[] = { 0, 0, 2, 5, 8, 8, 12, 24, 26, 43 };
		FXD::S32 arrayInt2[] = { 0, 0, 0, 5, 7, 8, 11, 24, 25, 43 };
		FXD::S32 arrayInt3[] = { 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 };

		FXD::STD::set_union(arrayInt1, arrayInt1 + 0, arrayInt2, arrayInt2 + 0, arrayInt3);
		PRINT_COND_ASSERT(VerifySequence(arrayInt3, arrayInt3 + 20, FXD::S32(), "set_union", 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, -1), "Algorithm - inline OutputItr set_union(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)");

		FXD::STD::set_union(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2 + 10, arrayInt3);
		PRINT_COND_ASSERT(VerifySequence(arrayInt3, arrayInt3 + 20, FXD::S32(), "set_union", 0, 0, 0, 2, 5, 7, 8, 8, 11, 12, 24, 25, 26, 43, 9, 9, 9, 9, 9, 9, -1), "Algorithm - inline OutputItr set_union(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)");

		arrayInt3[0] = arrayInt3[1] = arrayInt3[2] = arrayInt3[3] = arrayInt3[4] = arrayInt3[5] = arrayInt3[6] = arrayInt3[7] = arrayInt3[8] = arrayInt3[9] = arrayInt3[10] = arrayInt3[11] = 9;

		FXD::STD::set_union(arrayInt1, arrayInt1 + 10, arrayInt2, arrayInt2 + 10, arrayInt3, FXD::STD::less< FXD::S32 >());
		PRINT_COND_ASSERT(VerifySequence(arrayInt3, arrayInt3 + 20, FXD::S32(), "set_union", 0, 0, 0, 2, 5, 7, 8, 8, 11, 12, 24, 25, 26, 43, 9, 9, 9, 9, 9, 9, -1), "Algorithm - inline OutputItr set_union(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)");
	}

	return nErrorCount;
}

static FXD::S32 TestHeapOperations(void)
{
	FXD::S32 nErrorCount = 0;

	{
		FXD::S32 arrayInt[] = { 10, 20, 30, 5, 15 };
		FXD::Container::Vector< FXD::S32 > vecInt(arrayInt, arrayInt + 5);

		// inline void make_heap(RandomAccessItr first, RandomAccessItr last, Predicate pred)
		// inline void make_heap(RandomAccessItr first, RandomAccessItr last)
		{
			FXD::STD::make_heap(vecInt.begin(), vecInt.end());
			PRINT_COND_ASSERT(vecInt.front() == 30, "Algorithm - inline void make_heap(RandomAccessItr first, RandomAccessItr last)");
		}

		// inline void pop_heap(RandomAccessItr first, RandomAccessItr last, Predicate pred)
		// inline void pop_heap(RandomAccessItr first, RandomAccessItr last)
		{
			FXD::STD::pop_heap(vecInt.begin(), vecInt.end());
			vecInt.pop_back();
			PRINT_COND_ASSERT(vecInt.front() == 20, "Algorithm - inline void pop_heap(RandomAccessItr first, RandomAccessItr last)");
		}

		// inline void push_heap(RandomAccessItr first, RandomAccessItr last, Predicate pred)
		// inline void push_heap(RandomAccessItr first, RandomAccessItr last)
		{
			vecInt.push_back(99);
			FXD::STD::push_heap(vecInt.begin(), vecInt.end());
			PRINT_COND_ASSERT(vecInt.front() == 99, "Algorithm - inline void push_heap(RandomAccessItr first, RandomAccessItr last)");
		}

		// inline void sort_heap(RandomAccessItr first, RandomAccessItr last, Predicate pred)
		// inline void sort_heap(RandomAccessItr first, RandomAccessItr last)
		{
			FXD::STD::sort_heap(vecInt.begin(), vecInt.end());
			PRINT_COND_ASSERT(*vecInt.begin() == 5, "Algorithm - inline void sort_heap(RandomAccessItr first, RandomAccessItr last)");
			PRINT_COND_ASSERT(*vecInt.rbegin() == 99, "Algorithm - inline void sort_heap(RandomAccessItr first, RandomAccessItr last)");
			PRINT_COND_ASSERT( FXD::STD::is_sorted(vecInt.begin(), vecInt.end()), "Algorithm - inline void sort_heap(RandomAccessItr first, RandomAccessItr last)");
		}
	}

	// inline RandomAccessItr is_heap_until(RandomAccessItr first, RandomAccessItr last, Predicate pred)
	// inline RandomAccessItr is_heap_until(RandomAccessItr first, RandomAccessItr last)
	{
		FXD::Container::Vector< FXD::S32 > vecInt{ 2, 6, 9, 3, 8, 4, 5, 1, 7 };

		FXD::STD::sort(vecInt.begin(), vecInt.end());
		FXD::STD::reverse(vecInt.begin(), vecInt.end());

		auto last = FXD::STD::is_heap_until(vecInt.begin(), vecInt.end());
		PRINT_COND_ASSERT((last - vecInt.begin()) == 9, "Algorithm - inline RandomAccessItr is_heap_until(RandomAccessItr first, RandomAccessItr last)");
		PRINT_COND_ASSERT(*vecInt.begin() == 9, "Algorithm - inline RandomAccessItr is_heap_until(RandomAccessItr first, RandomAccessItr last)");
		PRINT_COND_ASSERT(*vecInt.rbegin() == 1, "Algorithm - inline RandomAccessItr is_heap_until(RandomAccessItr first, RandomAccessItr last)");
		PRINT_COND_ASSERT( FXD::STD::is_sorted(vecInt.rbegin(), vecInt.rend()), "Algorithm - inline RandomAccessItr is_heap_until(RandomAccessItr first, RandomAccessItr last)");
	}

	return nErrorCount;
}

static FXD::S32 TestMinMax(void)
{
	FXD::S32 nErrorCount = 0;

	// CONSTEXPR14 const T& Min(const T& lhs, const T& rhs, Predicate pred)
	// CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)
	// CONSTEXPR14 T Min(std::initializer_list< T > iList, Predicate pred)
	// CONSTEXPR14 T Min(std::initializer_list< T > iList)
	// CONSTEXPR14 const T& Max(const T& lhs, const T& rhs, Predicate pred)
	// CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)
	// CONSTEXPR14 T Max(std::initializer_list< T > iList, Predicate pred)
	// CONSTEXPR14 T Max(std::initializer_list< T > iList)
	{
		{
			_StructA a1(1);
			_StructA a2(2);
			_StructA a3(3);
			a3 = FXD::STD::Min(a1, a2, _LessStruct());
			PRINT_COND_ASSERT(a3.m_nA == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs, Predicate pred)");
			a3 = FXD::STD::Max(a1, a2, _LessStruct());
			PRINT_COND_ASSERT(a3.m_nA == 2, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs, Predicate pred)");
		}

		{
			_StructB b1(1);
			_StructB b2(2);
			_StructB b3(3);
			b3 = FXD::STD::Min(b2, b1, _LessFunction);
			PRINT_COND_ASSERT(b3.m_nB == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs, Predicate pred)");
			b3 = FXD::STD::Max(b2, b1, _LessFunction);
			PRINT_COND_ASSERT(b3.m_nB == 2, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs, Predicate pred)");
		}

		{
			TestObject t1(1);
			TestObject t2(2);
			TestObject t3(3);
			t3 = FXD::STD::Min(t2, t1);
			PRINT_COND_ASSERT(t3.m_nX == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			t3 = FXD::STD::Max(t2, t1);
			PRINT_COND_ASSERT(t3.m_nX == 2, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
		}

		{
			FXD::S32 i1;
			FXD::S32 i2(-1);
			FXD::S32 i3(1);
			i1 = FXD::STD::Min(i2, i3);
			PRINT_COND_ASSERT(i1 == -1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			i1 = FXD::STD::Min(i3, i2);
			PRINT_COND_ASSERT(i1 == -1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			i1 = FXD::STD::Max(i2, i3);
			PRINT_COND_ASSERT(i1 == 1, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			i1 = FXD::STD::Max(i3, i2);
			PRINT_COND_ASSERT(i1 == 1, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");

			const volatile FXD::S32 i2cv(-1);
			const volatile FXD::S32 i3cv(1);
			i1 = FXD::STD::Min(i2cv, i3cv);
			PRINT_COND_ASSERT(i1 == -1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			i1 = FXD::STD::Min(i3cv, i2cv);
			PRINT_COND_ASSERT(i1 == -1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			i1 = FXD::STD::Max(i2cv, i3cv);
			PRINT_COND_ASSERT(i1 == 1, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			i1 = FXD::STD::Max(i3cv, i2cv);
			PRINT_COND_ASSERT(i1 == 1, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
		}

		{
			FXD::F32 f1;
			FXD::F32 f2(-1.0f);
			FXD::F32 f3(1.0f);
			f1 = FXD::STD::Min(f2, f3);
			PRINT_COND_ASSERT(f1 == -1.0f, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			f1 = FXD::STD::Min(f3, f2);
			PRINT_COND_ASSERT(f1 == -1.0f, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			f1 = FXD::STD::Max(f2, f3);
			PRINT_COND_ASSERT(f1 == 1.0f, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			f1 = FXD::STD::Max(f3, f2);
			PRINT_COND_ASSERT(f1 == 1.0f, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
		}

		{
			FXD::F64 d1;
			FXD::F64 d2(-1.0);
			FXD::F64 d3(1.0);
			d1 = FXD::STD::Min(d2, d3);
			PRINT_COND_ASSERT(d1 == -1.0, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			d1 = FXD::STD::Min(d3, d2);
			PRINT_COND_ASSERT(d1 == -1.0, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			d1 = FXD::STD::Max(d2, d3);
			PRINT_COND_ASSERT(d1 == 1.0, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			d1 = FXD::STD::Max(d3, d2);
			PRINT_COND_ASSERT(d1 == 1.0, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");

			void* p1 = nullptr;
			void* p2 = &d2;
			void* p3 = &d3;
			p1 = FXD::STD::Min(p2, p3);
			PRINT_COND_ASSERT((uintptr_t)p1 == FXD::STD::Min((uintptr_t)p2, (uintptr_t)p3), "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");

			FXD::F64* pd1;
			FXD::F64* pd2 = &d2;
			FXD::F64* pd3 = &d3;
			pd1 = FXD::STD::Min(pd2, pd3);
			PRINT_COND_ASSERT((uintptr_t)pd1 == FXD::STD::Min((uintptr_t)pd2, (uintptr_t)pd3), "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
		}

		// Test scalar specializations
		{
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::S8)1, (FXD::S8)1) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::S8)1, (FXD::S8)2) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::S8)2, (FXD::S8)1) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");

			PRINT_COND_ASSERT(FXD::STD::Min((FXD::U8)1, (FXD::U8)1) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::U8)1, (FXD::U8)2) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::U8)2, (FXD::U8)1) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");

			PRINT_COND_ASSERT(FXD::STD::Min((FXD::S16)1, (FXD::S16)1) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::S16)1, (FXD::S16)2) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::S16)2, (FXD::S16)1) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");

			PRINT_COND_ASSERT(FXD::STD::Min((FXD::U16)1, (FXD::U16)1) == 1, "Algorithm - CONSTEXPR14 CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::U16)1, (FXD::U16)2) == 1, "Algorithm - CONSTEXPR14 CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::U16)2, (FXD::U16)1) == 1, "Algorithm - CONSTEXPR14 CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");

			PRINT_COND_ASSERT(FXD::STD::Min((FXD::S32)1, (FXD::S32)1) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::S32)1, (FXD::S32)2) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::S32)2, (FXD::S32)1) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");

			PRINT_COND_ASSERT(FXD::STD::Min((FXD::U32)1, (FXD::U32)1) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::U32)1, (FXD::U32)2) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::U32)2, (FXD::U32)1) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");

			PRINT_COND_ASSERT(FXD::STD::Min((FXD::S64)1, (FXD::S64)1) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::S64)1, (FXD::S64)2) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::S64)2, (FXD::S64)1) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");

			PRINT_COND_ASSERT(FXD::STD::Min((FXD::U64)1, (FXD::U64)1) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::U64)1, (FXD::U64)2) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::U64)2, (FXD::U64)1) == 1, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");

			PRINT_COND_ASSERT(FXD::STD::Min((FXD::F32)1.0f, (FXD::F32)1.0f) == 1.0f, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::F32)1.0f, (FXD::F32)2.0f) == 1.0f, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::F32)2.0f, (FXD::F32)1.0f) == 1.0f, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");

			PRINT_COND_ASSERT(FXD::STD::Min((FXD::F64)1.0, (FXD::F64)1.0) == 1.0, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::F64)1.0, (FXD::F64)2.0) == 1.0, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Min((FXD::F64)2.0, (FXD::F64)1.0) == 1.0, "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
		}

		// Test max specializations
		{
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::S8)1, (FXD::S8)1) == 1, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::S8)1, (FXD::S8)2) == 2, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::S8)2, (FXD::S8)1) == 2, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");

			PRINT_COND_ASSERT(FXD::STD::Max((FXD::U8)1, (FXD::U8)1) == 1, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::U8)1, (FXD::U8)2) == 2, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::U8)2, (FXD::U8)1) == 2, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");

			PRINT_COND_ASSERT(FXD::STD::Max((FXD::S16)1, (FXD::S16)1) == 1, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::S16)1, (FXD::S16)2) == 2, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::S16)2, (FXD::S16)1) == 2, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");

			PRINT_COND_ASSERT(FXD::STD::Max((FXD::U16)1, (FXD::U16)1) == 1, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::U16)1, (FXD::U16)2) == 2, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::U16)2, (FXD::U16)1) == 2, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");

			PRINT_COND_ASSERT(FXD::STD::Max((FXD::S32)1, (FXD::S32)1) == 1, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::S32)1, (FXD::S32)2) == 2, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::S32)2, (FXD::S32)1) == 2, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");

			PRINT_COND_ASSERT(FXD::STD::Max((FXD::U32)1, (FXD::U32)1) == 1, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::U32)1, (FXD::U32)2) == 2, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::U32)2, (FXD::U32)1) == 2, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");

			PRINT_COND_ASSERT(FXD::STD::Max((FXD::S64)1, (FXD::S64)1) == 1, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::S64)1, (FXD::S64)2) == 2, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::S64)2, (FXD::S64)1) == 2, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");

			PRINT_COND_ASSERT(FXD::STD::Max((FXD::U64)1, (FXD::U64)1) == 1, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::U64)1, (FXD::U64)2) == 2, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::U64)2, (FXD::U64)1) == 2, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");

			PRINT_COND_ASSERT(FXD::STD::Max((FXD::F32)1.0f, (FXD::F32)1.0f) == 1.0f, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::F32)1.0f, (FXD::F32)2.0f) == 2.0f, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::F32)2.0f, (FXD::F32)1.0f) == 2.0f, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");

			PRINT_COND_ASSERT(FXD::STD::Max((FXD::F64)1.0, (FXD::F64)1.0) == 1.0, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::F64)1.0, (FXD::F64)2.0) == 2.0, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
			PRINT_COND_ASSERT(FXD::STD::Max((FXD::F64)2.0, (FXD::F64)1.0) == 2.0, "Algorithm - CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)");
		}
	}

	// Make sure enums work
	{
		CTC_ASSERT(FXD::STD::is_enum< Game::Enum >::value, "Algorithm - is_enum failure");
		PRINT_COND_ASSERT((FXD::STD::Min(Game::Enum::eValue1, Game::Enum::eValue2) == Game::Enum::eValue1), "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
	}

	// Make sure pointers work
	{
		TestObject arrayTO[2];
		PRINT_COND_ASSERT((FXD::STD::Min(&arrayTO[0], &arrayTO[1]) == &arrayTO[0]), "Algorithm - CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)");
	}

	// CONSTEXPR17 ForwardItr min_element(ForwardItr first, ForwardItr last, Predicate pred)
	// CONSTEXPR17 ForwardItr min_element(ForwardItr first, ForwardItr last)
	{
		FXD::S32 arrayInt[] = { -5, 2, 1, 5, 4, 5 };
		FXD::S32* pInt = FXD::STD::min_element(arrayInt, arrayInt + 6);
		PRINT_COND_ASSERT(pInt && (*pInt == -5), "Algorithm - CONSTEXPR17 ForwardItr min_element(ForwardItr first, ForwardItr last)");

		pInt = FXD::STD::min_element(arrayInt, arrayInt + 6, _Greater< FXD::S32 >());
		PRINT_COND_ASSERT(pInt && (*pInt == 5), "Algorithm - CONSTEXPR17 ForwardItr min_element(ForwardItr first, ForwardItr last, Predicate pred)");

		TestObject arrayTO[] = { TestObject(7), TestObject(2), TestObject(8), TestObject(5), TestObject(4), TestObject(-12) };
		TestObject* pTO = FXD::STD::min_element(arrayTO, arrayTO + 6);
		PRINT_COND_ASSERT(pTO && (*pTO == TestObject(-12)), "Algorithm - CONSTEXPR17 ForwardItr min_element(ForwardItr first, ForwardItr last)");

		pTO = FXD::STD::min_element(arrayTO, arrayTO + 6, _Greater< TestObject >());
		PRINT_COND_ASSERT(pTO && (*pTO == TestObject(8)), "Algorithm - CONSTEXPR17 ForwardItr min_element(ForwardItr first, ForwardItr last, Predicate pred)");
	}

	// CONSTEXPR17 ForwardItr max_element(ForwardItr first, ForwardItr last, Predicate pred)
	// CONSTEXPR17 ForwardItr max_element(ForwardItr first, ForwardItr last)
	{

		FXD::S32 arrayInt[] = { -5, 2, 1, 5, 4, 5 };
		FXD::S32* pInt = FXD::STD::max_element(arrayInt, arrayInt + 6);
		PRINT_COND_ASSERT(pInt && (*pInt == 5), "Algorithm - CONSTEXPR17 ForwardItr max_element(ForwardItr first, ForwardItr last)");

		pInt = FXD::STD::max_element(arrayInt, arrayInt + 6, FXD::STD::less< FXD::S32 >());
		PRINT_COND_ASSERT(pInt && (*pInt == 5), "Algorithm - CONSTEXPR17 ForwardItr max_element(ForwardItr first, ForwardItr last, Predicate pred)");

		TestObject arrayTO[] = { TestObject(7), TestObject(2), TestObject(8), TestObject(5), TestObject(4), TestObject(-12) };
		TestObject* pTO = FXD::STD::max_element(arrayTO, arrayTO + 6);
		PRINT_COND_ASSERT(pTO && (*pTO == TestObject(8)), "Algorithm - CONSTEXPR17 ForwardItr max_element(ForwardItr first, ForwardItr last)");

		pTO = FXD::STD::max_element(arrayTO, arrayTO + 6, FXD::STD::less< TestObject >());
		PRINT_COND_ASSERT(pTO && (*pTO == TestObject(8)), "Algorithm - CONSTEXPR17 ForwardItr max_element(ForwardItr first, ForwardItr last, Predicate pred)");
	}

	// CONSTEXPR17 Core::Pair< ForwardItr, ForwardItr > minmax_element(ForwardItr first, ForwardItr last)
	{
		FXD::S32 arrayInt[] = { 5, -2, 1, 5, 6, 5 };

		FXD::Core::Pair< FXD::S32*, FXD::S32* > result = FXD::STD::minmax_element(arrayInt, arrayInt + 6);
		PRINT_COND_ASSERT((*result.first == -2) && (*result.second == 6), "Algorithm - CONSTEXPR17 Core::Pair< ForwardItr, ForwardItr > minmax_element(ForwardItr first, ForwardItr last)");
	}

	// CONSTEXPR14 Core::Pair < const T&, const T& > minmax(const T& lhs, const T& rhs, Predicate pred)
	// CONSTEXPR14 Core::Pair < const T&, const T& > minmax(const T& lhs, const T& rhs)
	{
		FXD::S32 i3(3);
		FXD::S32 i2(2);
		FXD::Core::Pair< const FXD::S32&, const FXD::S32& > pairInt = FXD::STD::minmax(i3, i2);
		PRINT_COND_ASSERT((pairInt.first == 2) && (pairInt.second == 3), "Algorithm - CONSTEXPR14 Core::Pair < const T&, const T& > minmax(const T& lhs, const T& rhs)");

		FXD::U8 c3(3);
		FXD::U8 c2(2);
		FXD::Core::Pair< const FXD::U8&, const FXD::U8& > pairU8 = FXD::STD::minmax(c3, c2);
		PRINT_COND_ASSERT((pairU8.first == 2) && (pairU8.second == 3), "Algorithm - CONSTEXPR14 Core::Pair < const T&, const T& > minmax(const T& lhs, const T& rhs)");

		FXD::F32 f3(3.0f);
		FXD::F32 f2(2.0f);
		FXD::Core::Pair< const FXD::F32&, const FXD::F32& > pairFloat = FXD::STD::minmax(f3, f2);
		PRINT_COND_ASSERT((pairFloat.first == 2.0f) && (pairFloat.second == 3.0f), "Algorithm - CONSTEXPR14 Core::Pair < const T&, const T& > minmax(const T& lhs, const T& rhs)");
	}

	// CONSTEXPR14 Core::Pair< T, T > minmax(std::initializer_list< T > iList, Predicate pred)
	// CONSTEXPR14 Core::Pair< T, T > minmax(std::initializer_list< T > iList)
	{
		FXD::Core::Pair< FXD::S32, FXD::S32 > pairInt = FXD::STD::minmax({ 3, 2 });
		PRINT_COND_ASSERT((pairInt.first == 2) && (pairInt.second == 3), "Algorithm - CONSTEXPR14 Core::Pair< T, T > minmax(std::initializer_list< T > iList)");
	}

	return nErrorCount;
}

static FXD::S32 TestClamp(void)
{
	FXD::S32 nErrorCount = 0;

	// CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi)
	{
		CTC_ASSERT(FXD::STD::clamp(42, 1, 100) == 42, "Algorithm - CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi)");
		CTC_ASSERT(FXD::STD::clamp(-42, 1, 100) == 1, "Algorithm - CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi)");
		CTC_ASSERT(FXD::STD::clamp(420, 1, 100) == 100, "Algorithm - CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi)");
		CTC_ASSERT(FXD::STD::clamp(1, 1, 100) == 1, "Algorithm - CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi)");
		CTC_ASSERT(FXD::STD::clamp(100, 1, 100) == 100, "Algorithm - CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi)");
	}

	// CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi, Predicate pred)
	{
		CTC_ASSERT(FXD::STD::clamp(42.f, 1.0f, 100.f, FXD::STD::less< FXD::F32 >()) == 42.f, "Algorithm - CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi, Predicate pred)");
		CTC_ASSERT(FXD::STD::clamp(-42.f, 1.0f, 100.f, FXD::STD::less< FXD::F32 >()) == 1.0f, "Algorithm - CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi, Predicate pred)");
		CTC_ASSERT(FXD::STD::clamp(420.f, 1.0f, 100.f, FXD::STD::less< FXD::F32 >()) == 100.f, "Algorithm - CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi, Predicate pred)");
		CTC_ASSERT(FXD::STD::clamp(1.0f, 1.0f, 100.f, FXD::STD::less< FXD::F32 >()) == 1.0f, "Algorithm - CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi, Predicate pred)");
		CTC_ASSERT(FXD::STD::clamp(100.f, 1.0f, 100.f, FXD::STD::less< FXD::F32 >()) == 100.f, "Algorithm - CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi, Predicate pred)");
		CTC_ASSERT(FXD::STD::clamp(42.0, 1.0, 100.0, FXD::STD::less< FXD::F64 >()) == 42.0, "Algorithm - CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi, Predicate pred)");
		CTC_ASSERT(FXD::STD::clamp(-42.0, 1.0, 100.0, FXD::STD::less< FXD::F64 >()) == 1.0, "Algorithm - CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi, Predicate pred)");
		CTC_ASSERT(FXD::STD::clamp(420.0, 1.0, 100.0, FXD::STD::less< FXD::F64 >()) == 100.0, "Algorithm - CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi, Predicate pred)");
		CTC_ASSERT(FXD::STD::clamp(1.0, 1.0, 100.0, FXD::STD::less< FXD::F64 >()) == 1.0, "Algorithm - CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi, Predicate pred)");
		CTC_ASSERT(FXD::STD::clamp(100.0, 1.0, 100.0, FXD::STD::less< FXD::F64 >()) == 100.0, "Algorithm - CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi, Predicate pred)");
		
		PRINT_COND_ASSERT(FXD::STD::clamp(_StructA(42), _StructA(1), _StructA(100), _LessStruct()).m_nA == _StructA(42).m_nA, "Algorithm - CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi, Predicate pred)");
		PRINT_COND_ASSERT(FXD::STD::clamp(_StructA(-42), _StructA(1), _StructA(100), _LessStruct()).m_nA == _StructA(1).m_nA, "Algorithm - CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi, Predicate pred)");
		PRINT_COND_ASSERT(FXD::STD::clamp(_StructA(420), _StructA(1), _StructA(100), _LessStruct()).m_nA == _StructA(100).m_nA, "Algorithm - CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi, Predicate pred)");
		PRINT_COND_ASSERT(FXD::STD::clamp(_StructA(1), _StructA(1), _StructA(100), _LessStruct()).m_nA == _StructA(1).m_nA, "Algorithm - CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi, Predicate pred)");
		PRINT_COND_ASSERT(FXD::STD::clamp(_StructA(100), _StructA(1), _StructA(100), _LessStruct()).m_nA == _StructA(100).m_nA, "Algorithm - CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi, Predicate pred)");
	}

	return nErrorCount;
}

static FXD::S32 TestComparisonOperations(void)
{
	FXD::S32 nErrorCount = 0;

	FXD::Core::RandomGen rng(FXD::App::GetRandSeed());

	// inline bool equal(InputItr1 first1, InputItr1 last, InputItr2 first2, Predicate pred)
	// inline bool equal(InputItr1 first1, InputItr1 last, InputItr2 first2)
	{
		FXD::Container::Vector< FXD::U32 > vecInt(100);
		FXD::Container::LinkedList< FXD::U32 > listInt(100);
		FXD::STD::generate(vecInt.begin(), vecInt.end(), rng);
		FXD::STD::copy(vecInt.begin(), vecInt.end(), listInt.begin());

		bool bEqual = FXD::STD::equal(vecInt.begin(), vecInt.begin(), (FXD::U32*)nullptr);
		PRINT_COND_ASSERT(bEqual, "Algorithm - inline bool equal(InputItr1 first1, InputItr1 last, InputItr2 first2)");
		bEqual = FXD::STD::equal(vecInt.begin(), vecInt.end(), listInt.begin());
		PRINT_COND_ASSERT(bEqual, "Algorithm - inline bool equal(InputItr1 first1, InputItr1 last, InputItr2 first2)");
		vecInt[50] += 1;
		bEqual = FXD::STD::equal(vecInt.begin(), vecInt.end(), listInt.begin());
		PRINT_COND_ASSERT(!bEqual, "Algorithm - inline bool equal(InputItr1 first1, InputItr1 last, InputItr2 first2)");

		vecInt[50] -= 1;
		bEqual = FXD::STD::equal(vecInt.begin(), vecInt.begin(), (FXD::U32*)nullptr, FXD::STD::equal_to< FXD::U32 >());
		PRINT_COND_ASSERT(bEqual, "Algorithm - inline bool equal(InputItr1 first1, InputItr1 last, InputItr2 first2, Predicate pred)");
		bEqual = FXD::STD::equal(vecInt.begin(), vecInt.end(), listInt.begin(), FXD::STD::equal_to< FXD::U32 >());
		PRINT_COND_ASSERT(bEqual, "Algorithm - inline bool equal(InputItr1 first1, InputItr1 last, InputItr2 first2, Predicate pred)");
		vecInt[50] += 1;
		bEqual = FXD::STD::equal(vecInt.begin(), vecInt.end(), listInt.begin(), FXD::STD::equal_to< FXD::U32 >());
		PRINT_COND_ASSERT(!bEqual, "Algorithm - inline bool equal(InputItr1 first1, InputItr1 last, InputItr2 first2, Predicate pred)");
	}

	// inline bool identical(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)
	// inline bool identical(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)
	{
		FXD::Container::Vector< FXD::U32 > vecInt(100);
		FXD::Container::LinkedList< FXD::U32 > listInt(100);
		FXD::STD::generate(vecInt.begin(), vecInt.end(), rng);
		FXD::STD::copy(vecInt.begin(), vecInt.end(), listInt.begin());

		bool bIdentical = FXD::STD::identical(vecInt.begin(), vecInt.begin(), (FXD::U32*)nullptr, (FXD::U32*)nullptr);
		PRINT_COND_ASSERT(bIdentical, "Algorithm - inline bool identical(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)");
		bIdentical = FXD::STD::identical(vecInt.begin(), vecInt.end(), listInt.begin(), listInt.end());
		PRINT_COND_ASSERT(bIdentical, "Algorithm - inline bool identical(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)");
		bIdentical = FXD::STD::identical(vecInt.begin(), vecInt.end() - 10, listInt.begin(), listInt.end());
		PRINT_COND_ASSERT(!bIdentical, "Algorithm - inline bool identical(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)");
		bIdentical = FXD::STD::identical(listInt.begin(), listInt.end(), vecInt.begin() + 10, vecInt.end());
		PRINT_COND_ASSERT(!bIdentical, "Algorithm - inline bool identical(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)");
		vecInt[50] += 1;
		bIdentical = FXD::STD::identical(vecInt.begin(), vecInt.end(), listInt.begin(), listInt.end());
		PRINT_COND_ASSERT(!bIdentical, "Algorithm - inline bool identical(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)");

		vecInt[50] -= 1; // resulttore its original value so the containers are equal again.
		bIdentical = FXD::STD::identical(vecInt.begin(), vecInt.begin(), (FXD::U32*)nullptr, (FXD::U32*)nullptr, FXD::STD::equal_to< FXD::U32 >());
		PRINT_COND_ASSERT(bIdentical, "Algorithm - inline bool identical(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)");
		bIdentical = FXD::STD::identical(vecInt.begin(), vecInt.end(), listInt.begin(), listInt.end(), FXD::STD::equal_to< FXD::U32 >());
		PRINT_COND_ASSERT(bIdentical, "Algorithm - inline bool identical(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)");
		bIdentical = FXD::STD::identical(vecInt.begin(), vecInt.end() - 10, listInt.begin(), listInt.end(), FXD::STD::equal_to< FXD::U32 >());
		PRINT_COND_ASSERT(!bIdentical, "Algorithm - inline bool identical(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)");
		bIdentical = FXD::STD::identical(listInt.begin(), listInt.end(), vecInt.begin() + 10, vecInt.end(), FXD::STD::equal_to< FXD::U32 >());
		PRINT_COND_ASSERT(!bIdentical, "Algorithm - inline bool identical(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)");
		vecInt[50] += 1;
		bIdentical = FXD::STD::identical(vecInt.begin(), vecInt.end(), listInt.begin(), listInt.end(), FXD::STD::equal_to< FXD::U32 >());
		PRINT_COND_ASSERT(!bIdentical, "Algorithm - inline bool identical(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)");
	}

	// inline bool lexicographical_compare(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)
	// inline bool lexicographical_compare(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)
	{
		FXD::S32 arrayInt1[6] = { 0, 1, 2, 3, 4, 5 };
		FXD::S32 arrayInt2[6] = { 0, 1, 2, 3, 4, 6 };
		FXD::S32 arrayInt3[5] = { 0, 1, 2, 3, 4 };

		bool bCompare = FXD::STD::lexicographical_compare(arrayInt1, arrayInt1, arrayInt2, arrayInt2); // Test empty range.
		PRINT_COND_ASSERT(!bCompare, "Algorithm - inline bool lexicographical_compare(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)");
		bCompare = FXD::STD::lexicographical_compare(arrayInt1, arrayInt1 + 6, arrayInt2, arrayInt2 + 6);
		PRINT_COND_ASSERT(bCompare, "Algorithm - inline bool lexicographical_compare(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)");
		bCompare = FXD::STD::lexicographical_compare(arrayInt2, arrayInt2 + 6, arrayInt1, arrayInt1 + 6);
		PRINT_COND_ASSERT(!bCompare, "Algorithm - inline bool lexicographical_compare(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)");
		bCompare = FXD::STD::lexicographical_compare(arrayInt1, arrayInt1 + 6, arrayInt3, arrayInt3 + 5);
		PRINT_COND_ASSERT(!bCompare, "Algorithm - inline bool lexicographical_compare(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)");

		bCompare = FXD::STD::lexicographical_compare(arrayInt1, arrayInt1, arrayInt2, arrayInt2, FXD::STD::greater< FXD::S32 >()); // Test empty range.
		PRINT_COND_ASSERT(!bCompare, "Algorithm - inline bool lexicographical_compare(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)");
		bCompare = FXD::STD::lexicographical_compare(arrayInt1, arrayInt1 + 6, arrayInt2, arrayInt2 + 6, FXD::STD::greater< FXD::S32 >());
		PRINT_COND_ASSERT(!bCompare, "Algorithm - inline bool lexicographical_compare(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)");
		bCompare = FXD::STD::lexicographical_compare(arrayInt2, arrayInt2 + 6, arrayInt1, arrayInt1 + 6, FXD::STD::greater< FXD::S32 >());
		PRINT_COND_ASSERT(bCompare, "Algorithm - inline bool lexicographical_compare(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)");
		bCompare = FXD::STD::lexicographical_compare(arrayInt3, arrayInt3 + 5, arrayInt1, arrayInt1 + 6, FXD::STD::less< FXD::S32 >());
		PRINT_COND_ASSERT(bCompare, "Algorithm - inline bool lexicographical_compare(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)");
	}

	return nErrorCount;
}

static FXD::S32 TestPermutationOperations(void)
{
	FXD::S32 nErrorCount = 0;

	// inline bool is_permutation(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2, Predicate pred)
	// inline bool is_permutation(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2)
	{
		FXD::Core::RandomGen range(FXD::App::GetRandSeed());

		{
			FXD::S32 arrayInt1[] = { 0, 1, 2, 3, 4 };
			FXD::S32 arrayInt2[] = { 0, 1, 2, 3, 4 };

			// Test an empty set.
			PRINT_COND_ASSERT(FXD::STD::is_permutation(arrayInt1, arrayInt1 + 0, arrayInt2), "Algorithm - inline bool is_permutation(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2)");

			// Test two identical sets.
			PRINT_COND_ASSERT(FXD::STD::is_permutation(arrayInt1, arrayInt1 + FXD_ARRAY_COUNT(arrayInt1), arrayInt2), "Algorithm - inline bool is_permutation(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2)");
			FXD::STD::random_shuffle(arrayInt1, arrayInt1 + FXD_ARRAY_COUNT(arrayInt1), range);

			// Test order randomization.
			PRINT_COND_ASSERT(FXD::STD::is_permutation(arrayInt1, arrayInt1 + FXD_ARRAY_COUNT(arrayInt1), arrayInt2), "Algorithm - inline bool is_permutation(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2)");
			FXD::STD::random_shuffle(arrayInt2, arrayInt2 + FXD_ARRAY_COUNT(arrayInt2), range);
			PRINT_COND_ASSERT(FXD::STD::is_permutation(arrayInt1, arrayInt1 + FXD_ARRAY_COUNT(arrayInt1), arrayInt2), "Algorithm - inline bool is_permutation(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2)");

			// Test the case where there's a difference.
			arrayInt2[4] = arrayInt2[3]; // This change guarantees is_permutation will return false.
			PRINT_COND_ASSERT(!FXD::STD::is_permutation(arrayInt1, arrayInt1 + FXD_ARRAY_COUNT(arrayInt1), arrayInt2), "Algorithm - inline bool is_permutation(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2)");
		}

		{
			FXD::S32 arrayInt1[] = { 0, 0, 0, 1, 1 };
			FXD::S32 arrayInt2[] = { 0, 0, 0, 1, 1 };

			// Test two identical sets.
			PRINT_COND_ASSERT(FXD::STD::is_permutation(arrayInt1, arrayInt1 + FXD_ARRAY_COUNT(arrayInt1), arrayInt2), "Algorithm - inline bool is_permutation(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2)");
			FXD::STD::random_shuffle(arrayInt1, arrayInt1 + FXD_ARRAY_COUNT(arrayInt1), range);

			// Test order randomization.
			PRINT_COND_ASSERT(FXD::STD::is_permutation(arrayInt1, arrayInt1 + FXD_ARRAY_COUNT(arrayInt1), arrayInt2), "Algorithm - inline bool is_permutation(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2)");
			FXD::STD::random_shuffle(arrayInt2, arrayInt2 + FXD_ARRAY_COUNT(arrayInt2), range);
			PRINT_COND_ASSERT(FXD::STD::is_permutation(arrayInt1, arrayInt1 + FXD_ARRAY_COUNT(arrayInt1), arrayInt2), "Algorithm - inline bool is_permutation(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2)");

			// Test the case where there's a difference.
			arrayInt2[4] = (arrayInt2[4] == 0) ? 1 : 0;
			PRINT_COND_ASSERT(!FXD::STD::is_permutation(arrayInt1, arrayInt1 + FXD_ARRAY_COUNT(arrayInt1), arrayInt2), "Algorithm - inline bool is_permutation(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2)");
		}

		for (FXD::S32 n1 = 0; n1 < 100000; n1++)
		{
			FXD::U32 arrayInt1[6];
			FXD::U32 arrayInt2[6];

			for (FXD::S32 n2 = 0; n2 < FXD_ARRAY_COUNT(arrayInt1); n2++)
			{
				arrayInt1[n2] = range.rand_limit(6);
				arrayInt2[n2] = range.rand_limit(6);
			}

			bool bPermutation = FXD::STD::is_permutation(arrayInt1, arrayInt1 + FXD_ARRAY_COUNT(arrayInt1), arrayInt2);

			// If is_permutation returned true, then sorted versions of the two arrays should be identical.
			FXD::STD::sort(arrayInt1, arrayInt1 + FXD_ARRAY_COUNT(arrayInt1));
			FXD::STD::sort(arrayInt2, arrayInt2 + FXD_ARRAY_COUNT(arrayInt2));

			FXD::Core::Pair< FXD::U32*, FXD::U32* > mismatchResult = FXD::STD::mismatch(arrayInt1, arrayInt1 + FXD_ARRAY_COUNT(arrayInt1), arrayInt2);
			bool bIdentical = (mismatchResult.first == (arrayInt1 + FXD_ARRAY_COUNT(arrayInt1)));
			PRINT_COND_ASSERT(bPermutation == bIdentical, "Algorithm - inline bool is_permutation(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2)");
		}
	}

	// inline bool next_permutation(BidirItr first, BidirItr last, Predicate pred);
	// inline bool next_permutation(BidirItr first, BidirItr last);
	{
		FXD::S32 arrayInt[] = { 1, 2, 3 };
		FXD::S32 arrayIntResults[6][3] =
		{
			{ 1, 2, 3 },
			{ 1, 3, 2 },
			{ 2, 1, 3 },
			{ 2, 3, 1 },
			{ 3, 1, 2 },
			{ 3, 2, 1 }
		};
		FXD::STD::sort(arrayInt, arrayInt + 3);

		FXD::U64 nCount = 0;
		do
		{
			PRINT_COND_ASSERT(arrayInt[0] == arrayIntResults[nCount][0], "Algorithm - inline bool next_permutation(BidirItr first, BidirItr last)");
			PRINT_COND_ASSERT(arrayInt[1] == arrayIntResults[nCount][1], "Algorithm - inline bool next_permutation(BidirItr first, BidirItr last)");
			PRINT_COND_ASSERT(arrayInt[2] == arrayIntResults[nCount][2], "Algorithm - inline bool next_permutation(BidirItr first, BidirItr last)");
			nCount++;
		} while (FXD::STD::next_permutation(arrayInt, arrayInt + 3));
		PRINT_COND_ASSERT(nCount == 6, "Algorithm - inline bool next_permutation(BidirItr first, BidirItr last)");
		PRINT_COND_ASSERT(FXD::STD::is_sorted(FXD::STD::begin(arrayInt), FXD::STD::end(arrayInt)), "Algorithm - inline bool next_permutation(BidirItr first, BidirItr last)");

		nCount = 0;
		do
		{
			PRINT_COND_ASSERT(arrayInt[0] == arrayIntResults[nCount][0], "Algorithm - inline bool next_permutation(BidirItr first, BidirItr last, Predicate pred)");
			PRINT_COND_ASSERT(arrayInt[1] == arrayIntResults[nCount][1], "Algorithm - inline bool next_permutation(BidirItr first, BidirItr last, Predicate pred)");
			PRINT_COND_ASSERT(arrayInt[2] == arrayIntResults[nCount][2], "Algorithm - inline bool next_permutation(BidirItr first, BidirItr last, Predicate pred)");
			nCount++;
		} while (FXD::STD::next_permutation(arrayInt, arrayInt + 3));
		PRINT_COND_ASSERT(nCount == 6, "Algorithm - inline bool next_permutation(BidirItr first, BidirItr last, Predicate pred)");
		PRINT_COND_ASSERT(FXD::STD::is_sorted(FXD::STD::begin(arrayInt), FXD::STD::end(arrayInt)), "Algorithm - inline bool next_permutation(BidirItr first, BidirItr last, Predicate pred)");
	}

	// inline bool prev_permutation(BidirItr first, BidirItr last, Predicate pred);
	// inline bool prev_permutation(BidirItr first, BidirItr last);
	{
		FXD::S32 arrayInt[] = { 1, 2, 3 };
		FXD::S32 arrayIntResults[6][3] =
		{
			{ 3, 2, 1 },
			{ 3, 1, 2 },
			{ 2, 3, 1 },
			{ 2, 1, 3 },
			{ 1, 3, 2 },
			{ 1, 2, 3 }
		};
		FXD::STD::sort(arrayInt, arrayInt + 3);
		FXD::STD::reverse(arrayInt, arrayInt + 3);

		FXD::U64 nCount = 0;
		do
		{
			PRINT_COND_ASSERT(arrayInt[0] == arrayIntResults[nCount][0], "Algorithm - inline bool prev_permutation(BidirItr first, BidirItr last)");
			PRINT_COND_ASSERT(arrayInt[1] == arrayIntResults[nCount][1], "Algorithm - inline bool prev_permutation(BidirItr first, BidirItr last)");
			PRINT_COND_ASSERT(arrayInt[2] == arrayIntResults[nCount][2], "Algorithm - inline bool prev_permutation(BidirItr first, BidirItr last)");
			nCount++;
		} while (FXD::STD::prev_permutation(arrayInt, arrayInt + 3));
		PRINT_COND_ASSERT(nCount == 6, "Algorithm - inline bool prev_permutation(BidirItr first, BidirItr last)");
		PRINT_COND_ASSERT(FXD::STD::is_sorted(FXD::STD::rbegin(arrayInt), FXD::STD::rend(arrayInt)), "Algorithm - inline bool next_permutation(BidirItr first, BidirItr last)");

		nCount = 0;
		do
		{
			PRINT_COND_ASSERT(arrayInt[0] == arrayIntResults[nCount][0], "Algorithm - inline bool prev_permutation(BidirItr first, BidirItr last, Predicate pred)");
			PRINT_COND_ASSERT(arrayInt[1] == arrayIntResults[nCount][1], "Algorithm - inline bool prev_permutation(BidirItr first, BidirItr last, Predicate pred)");
			PRINT_COND_ASSERT(arrayInt[2] == arrayIntResults[nCount][2], "Algorithm - inline bool prev_permutation(BidirItr first, BidirItr last, Predicate pred)");
			nCount++;
		} while (FXD::STD::prev_permutation(arrayInt, arrayInt + 3));
		PRINT_COND_ASSERT(nCount == 6, "Algorithm - inline bool prev_permutation(BidirItr first, BidirItr last, Predicate pred)");
		PRINT_COND_ASSERT(FXD::STD::is_sorted(FXD::STD::rbegin(arrayInt), FXD::STD::rend(arrayInt)), "Algorithm - inline bool next_permutation(BidirItr first, BidirItr last, Predicate pred)");
	}
	return nErrorCount;
}

FXD::S32 TestAlgorithm(void)
{
	FXD::S32 nErrorCount = 0;
	TestObject::reset();

	{
		nErrorCount += TestNonModifyingSequences();
		nErrorCount += TestModifyingSequences();
		nErrorCount += TestPartitioningOperations();
		nErrorCount += TestSortingOperations();
		nErrorCount += TestBinarySearchOperations();
		nErrorCount += TestSetOperations();
		nErrorCount += TestHeapOperations();
		nErrorCount += TestMinMax();
		nErrorCount += TestClamp();
		nErrorCount += TestComparisonOperations();
		nErrorCount += TestPermutationOperations();
	}

	PRINT_COND_ASSERT(TestObject::is_clear(), "Algorithm - Failed");

	return nErrorCount;
}