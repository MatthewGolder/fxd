// Creator - MatthewGolder
#include "Tests/System/Tests/FXDTest.h"
#include "FXDEngine/Core/String.h"
#include "FXDEngine/Core/Utility.h"

static FXD::S32 TestUtilityPair(void)
{
	FXD::S32 nErrorCount = 0;

	{
		FXD::S32 n0 = 0, n2 = 2, n3 = 3;
		FXD::F32 float1 = 1.0f;

		// CONSTEXPR FXD::Core::Pair(void) NOEXCEPT;
		{
			FXD::Core::Pair< FXD::S32, FXD::F32 > pair;
			PRINT_COND_ASSERT((pair.first == 0) && (pair.second == 0.0f), "Utility - CONSTEXPR FXD::Core::Pair(void) NOEXCEPT");
		}

		// FXD::Core::Pair(const T1& x, const T2& y);
		{
			FXD::Core::Pair< FXD::S32, FXD::F32 > pair(n0, float1);
			PRINT_COND_ASSERT((pair.first == 0) && (pair.second == 1.0f), "Utility - FXD::Core::Pair(const T1& x, const T2& y)");
		}

		// template < typename U, typename V >
		// FXD::Core::Pair(U&& u, V&& v);
		{
			FXD::Core::Pair< FXD::S32, FXD::F32 > pair(FXD::S32(0), FXD::F32(1.0f));
			PRINT_COND_ASSERT((pair.first == 0) && (pair.second == 1.0f), "Utility - FXD::Core::Pair(const T1& x, const T2& y)");
		}

		// template < typename U >
		// FXD::Core::Pair(U&& x, const T2& y);
		{
			const FXD::F32 fConst = 1.0f;
			FXD::Core::Pair< FXD::S32, FXD::F32 > pair(FXD::S32(0), fConst);
			PRINT_COND_ASSERT((pair.first == 0) && (pair.second == 1.0f), "Utility - FXD::Core::Pair(U&& x, const T2& y)");
		}

		// template < typename V >
		// FXD::Core::Pair(const T1& x, V&& y);
		{
			const FXD::S32 intConst = 0;
			FXD::Core::Pair< FXD::S32, FXD::F32 > pair(intConst, FXD::F32(1.0f));
			PRINT_COND_ASSERT((pair.first == 0) && (pair.second == 1.0f), "Utility - FXD::Core::Pair(const T1& x, V&& y)");

			FXD::Core::Pair< const FXD::S32, const FXD::S32 > pairConstInt(n2, n3);
			PRINT_COND_ASSERT((pairConstInt.first == 2) && (pairConstInt.second == 3), "Utility - FXD::Core::Pair(const T1& x, V&& y)");
		}

		// FXD::Core::Pair(const Pair& rhs);
		{
			FXD::Core::Pair< FXD::S32, FXD::F32 > pair(n0, float1);
			FXD::Core::Pair< const FXD::S32, const FXD::S32 > pairConstInt(n2, n3);

			FXD::Core::Pair< FXD::S32, FXD::F32 > pairCopy(pair);
			PRINT_COND_ASSERT((pairCopy.first == 0) && (pairCopy.second == 1.0f), "Utility - FXD::Core::Pair(const Pair& rhs)");

			FXD::Core::Pair< const FXD::S32, const FXD::S32 > pairConstCopy(pairConstInt);
			PRINT_COND_ASSERT((pairConstCopy.first == 2) && (pairConstCopy.second == 3), "Utility - FXD::Core::Pair(const Pair& rhs)");
		}

		// template < typename UT1, typename UT12 >
		// FXD::Core::Pair(const FXD::Core::Pair< UT1, UT2 >& rhs);
		{
			FXD::Core::Pair< FXD::S32, FXD::F32 > pair(n0, float1);
			FXD::Core::Pair< FXD::S64, FXD::F64 > pairLong(pair);
			PRINT_COND_ASSERT((pairLong.first == 0) && (pairLong.second == 1.0), "Utility - FXD::Core::Pair(const FXD::Core::Pair< U, V >& rhs)");
		}

		// template < typename U >
		// FXD::Core::Pair(U&& f, const T2& s)
		{
			using PairMap = FXD::Core::Pair< FXD::U16, const FXD::UTF8* >;
			PairMap pair[1] = { PairMap(0x0036, UTF_8("af")) };
			PRINT_COND_ASSERT((pair[0].first == 0x0036), "Utility - FXD::Core::Pair(U&& f, const T2& s)");
		}

		// template < typename U >
		// FXD::Core::Pair(const T1& f, V&& s)
		{
			using PairMap = FXD::Core::Pair< const FXD::UTF8*, FXD::U16 >;
			PairMap pair[1] = { PairMap(UTF_8("af"), 0x0036) };
			PRINT_COND_ASSERT((pair[0].second == 0x0036), "Utility - FXD::Core::Pair(const T1& f, V&& s)");
		}

		// FXD::STD::UseSelf(void);
		// FXD::STD::UseFirst(void);
		// FXD::STD::UseSecond(void);
		{
			FXD::STD::UseSelf< FXD::Core::Pair< FXD::S32, FXD::F32 > > pairSelf;
			FXD::STD::UseFirst< FXD::Core::Pair< FXD::S32, FXD::F32 > > pairFirst;
			FXD::STD::UseSecond< FXD::Core::Pair< FXD::S32, FXD::F32 > > pairSecond;
			FXD::Core::Pair< FXD::S32, FXD::F32 > pair(n0, float1);

			pair = pairSelf(pair);
			PRINT_COND_ASSERT((pair.first == 0) && (pair.second == 1), "Utility - FXD::STD::UseSelf(void)");

			FXD::S32 fFirst = pairFirst(pair);
			PRINT_COND_ASSERT(fFirst == 0, "Utility - FXD::STD::UseFirst(void)");

			FXD::F32 fSecond = pairSecond(pair);
			PRINT_COND_ASSERT(fSecond == 1, "Utility - FXD::STD::UseSecond(void)");
		}

		// FXD::Core::make_pair(void);
		{
			{
				FXD::Core::Pair< FXD::S32, FXD::F32 > pair1 = FXD::Core::make_pair(FXD::S32(0), FXD::F32(1));
				PRINT_COND_ASSERT((pair1.first == 0) && (pair1.second == 1.0f), "Utility - FXD::Core::make_pair(void)");

				FXD::Core::Pair< const FXD::UTF8*, FXD::S32 > pair2 = FXD::Core::make_pair("a", 1);
				PRINT_COND_ASSERT((FXD::Core::CharTraits< FXD::UTF8 >::compare(pair2.first, "a") == 0) && (pair1.second == 1), "Utility - FXD::Core::make_pair(void)");

				FXD::Core::Pair< const FXD::UTF8*, FXD::S32 > pair3 = FXD::Core::make_pair< const FXD::UTF8*, FXD::S32 >("a", 1);
				PRINT_COND_ASSERT((FXD::Core::CharTraits< FXD::UTF8 >::compare(pair3.first, "a") == 0) && (pair3.second == 1), "Utility - FXD::Core::make_pair(void)");

				FXD::Core::Pair< FXD::S32, const FXD::UTF8* > pair4 = FXD::Core::make_pair< FXD::S32, const FXD::UTF8* >(1, "b");
				PRINT_COND_ASSERT((pair4.first == 1) && (FXD::Core::CharTraits< FXD::UTF8 >::compare(pair4.second, "b") == 0), "Utility - FXD::Core::make_pair(void)");
			}
			{
				auto pair = FXD::Core::make_pair("a", "b");
				PRINT_COND_ASSERT((FXD::Core::CharTraits< FXD::UTF8 >::compare(pair.first, "a") == 0) && (FXD::Core::CharTraits< FXD::UTF8 >::compare(pair.second, "b") == 0), "Utility - FXD::Core::make_pair(void)");
			}
			{
				auto pair = FXD::Core::make_pair("ab", "cd");
				PRINT_COND_ASSERT((FXD::Core::CharTraits< FXD::UTF8 >::compare(pair.first, "ab") == 0) && (FXD::Core::CharTraits< FXD::UTF8 >::compare(pair.second, "cd") == 0), "Utility - FXD::Core::make_pair(void)");
			}
			{
				auto pair = FXD::Core::make_pair("abc", "bcdef");
				PRINT_COND_ASSERT((FXD::Core::CharTraits< FXD::UTF8 >::compare(pair.first, "abc") == 0) && (FXD::Core::CharTraits< FXD::UTF8 >::compare(pair.second, "bcdef") == 0), "Utility - FXD::Core::make_pair(void)");
			}
			{
				FXD::UTF8 pStrA[] = "a";
				FXD::UTF8 pStrB[] = "bc";

				auto pairStr1 = FXD::Core::make_pair(pStrA, pStrA);
				PRINT_COND_ASSERT((FXD::Core::CharTraits< FXD::UTF8 >::compare(pairStr1.first, "a") == 0) && (FXD::Core::CharTraits< FXD::UTF8 >::compare(pairStr1.second, "a") == 0), "Utility - FXD::Core::make_pair(void)");

				auto pairStr2 = FXD::Core::make_pair(pStrA, pStrB);
				PRINT_COND_ASSERT((FXD::Core::CharTraits< FXD::UTF8 >::compare(pairStr2.first, "a") == 0) && (FXD::Core::CharTraits< FXD::UTF8 >::compare(pairStr2.second, "bc") == 0), "Utility - FXD::Core::make_pair(void)");
			}
			{
				const FXD::UTF8 pStrA[] = "a";
				const FXD::UTF8 pStrB[] = "bc";

				auto pairStr1 = FXD::Core::make_pair(pStrA, pStrA);
				PRINT_COND_ASSERT((FXD::Core::CharTraits< FXD::UTF8 >::compare(pairStr1.first, "a") == 0) && (FXD::Core::CharTraits< FXD::UTF8 >::compare(pairStr1.second, "a") == 0), "Utility - FXD::Core::make_pair(void)");

				auto pairStr2 = FXD::Core::make_pair(pStrA, pStrB);
				PRINT_COND_ASSERT((FXD::Core::CharTraits< FXD::UTF8 >::compare(pairStr2.first, "a") == 0) && (FXD::Core::CharTraits< FXD::UTF8 >::compare(pairStr2.second, "bc") == 0), "Utility - FXD::Core::make_pair(void)");
			}
		}
		{
			FXD::Core::Pair< FXD::UTF8*, FXD::UTF8* > pairNull(0, 0);
			PRINT_COND_ASSERT((pairNull.first == nullptr) && (pairNull.second == nullptr), "Utility - FXD::Core::make_pair(void)");
		}
	}

	return nErrorCount;
}
FXD::S32 TestPair(void)
{
	FXD::S32 nErrorCount = 0;

	nErrorCount += TestUtilityPair();

	return nErrorCount;
}