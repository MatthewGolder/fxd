// Creator - MatthewGolder
#include "Tests/System/Tests/FXDTest.h"
#include "Tests/System/TestHelperClasses.h"
#include "FXDEngine/Container/Deque.h"
#include "FXDEngine/Container/LinkedList.h"
#include "FXDEngine/Container/Vector.h"
#include "FXDEngine/Core/String.h"

namespace
{
	struct _ItemWithConst
	{
	public:
		_ItemWithConst(FXD::S32 nIdx)
			: m_nIdx(nIdx)
		{}
		_ItemWithConst(const _ItemWithConst& rhs)
			: m_nIdx(rhs.m_nIdx)
		{}

	public:
		const FXD::S32 m_nIdx;

	private:
		NO_ASSIGNMENT(_ItemWithConst);
	};

	struct _TestMovable
	{
	public:
		_TestMovable(void) NOEXCEPT
		{}

		_TestMovable(_TestMovable&&) NOEXCEPT
		{}

		_TestMovable& operator=(_TestMovable&&) NOEXCEPT
		{
			return (*this);
		}

	private:
		NO_COPY(_TestMovable);
		NO_ASSIGNMENT(_TestMovable);
	};

	struct _TestMoveAssignToSelf
	{
	public:
		_TestMoveAssignToSelf(void) NOEXCEPT
			: m_movedToSelf(false)
		{}
		_TestMoveAssignToSelf(const _TestMoveAssignToSelf& rhs)
		{
			m_movedToSelf = rhs.m_movedToSelf;
		}
		_TestMoveAssignToSelf& operator=(_TestMoveAssignToSelf&& rhs)
		{
			m_movedToSelf = true;
			return (*this);
		}

	private:
		NO_ASSIGNMENT(_TestMoveAssignToSelf);

	public:
		bool m_movedToSelf;
	};
}


FXD::S32 TestVector(void)
{
	FXD::S32 nErrorCount = 0;

	TestObject::reset();

	// EXPLICIT FXD::Container::Vector(void);
	{
		FXD::Container::Vector< FXD::S32 > vecInt;
		FXD::Container::Vector< TestObject > vecTO;
		FXD::Container::Vector< FXD::Container::LinkedList< TestObject > > vecListTO1;

		PRINT_COND_ASSERT(vecInt.validate(), "Vector - Vector(void)");
		PRINT_COND_ASSERT(vecInt.empty(), "Vector - Vector(void)");
		PRINT_COND_ASSERT(vecTO.validate(), "Vector - Vector(void)");
		PRINT_COND_ASSERT(vecTO.empty(), "Vector - Vector(void)");
		PRINT_COND_ASSERT(vecListTO1.validate(), "Vector - Vector(void)");
		PRINT_COND_ASSERT(vecListTO1.empty(), "Vector - Vector(void)");
	}

	Game::MallocAllocator::reset_all();

	{
		Game::MallocAllocator ma;

		// EXPLICIT FXD::Container::Vector(const allocator_type& alloc);
		FXD::Container::Vector< FXD::S32, Game::MallocAllocator > vecInt1(ma);
		FXD::Container::Vector< TestObject, Game::MallocAllocator > vecTO1(ma);
		FXD::Container::Vector< FXD::Container::LinkedList< TestObject >, Game::MallocAllocator > vecListTO1(ma);
		{
			vecInt1.resize(1);
			vecTO1.resize(1);
			vecListTO1.resize(1);
			PRINT_COND_ASSERT(Game::MallocAllocator::m_nAllocCountAll == 3, "Vector - Vector(const allocator_type& alloc)");
		}

		// EXPLICIT FXD::Container::Vector(size_type nCount)
		FXD::Container::Vector< FXD::S32 > vecInt2(10);
		FXD::Container::Vector< TestObject > vecTO2(10);
		FXD::Container::Vector< FXD::Container::LinkedList< TestObject> > vecListTO2(10);
		{
			PRINT_COND_ASSERT(vecInt2.validate(), "Vector - Vector(size_type nCount)");
			PRINT_COND_ASSERT(vecInt2.size() == 10, "Vector - Vector(size_type nCount)");
			PRINT_COND_ASSERT(vecTO2.validate(), "Vector - Vector(size_type nCount)");
			PRINT_COND_ASSERT(vecTO2.size() == 10, "Vector - Vector(size_type nCount)");
			PRINT_COND_ASSERT(vecListTO2.validate(), "Vector - Vector(size_type nCount)");
			PRINT_COND_ASSERT(vecListTO2.size() == 10, "Vector - Vector(size_type nCount)");
		}

		// FXD::Container::Vector(size_type nCount, const value_type& value)
		FXD::Container::Vector< FXD::S32 > vecInt3(10, 7);
		FXD::Container::Vector< TestObject > vecTO3(10, TestObject(7));
		FXD::Container::Vector< FXD::Container::LinkedList< TestObject > > vecListTO3(10, FXD::Container::LinkedList< TestObject >(7));
		{
			PRINT_COND_ASSERT(vecInt3.validate(), "Vector - Vector(size_type nCount, const value_type& value)");
			PRINT_COND_ASSERT(vecInt3.size() == 10, "Vector - Vector(size_type nCount, const value_type& value)");
			PRINT_COND_ASSERT(vecInt3[5] == 7, "Vector - Vector(size_type nCount, const value_type& value)");
			PRINT_COND_ASSERT(vecTO3.validate(), "Vector - Vector(size_type nCount, const value_type& value)");
			PRINT_COND_ASSERT(vecTO3[5] == TestObject(7), "Vector - Vector(size_type nCount, const value_type& value)");
			PRINT_COND_ASSERT(vecListTO3.validate(), "Vector - Vector(size_type nCount, const value_type& value)");
			PRINT_COND_ASSERT(vecListTO3[5] == FXD::Container::LinkedList< TestObject >(7), "Vector - Vector(size_type nCount, const value_type& value)");
		}

		// FXD::Container::Vector(const my_type& rhs)
		FXD::Container::Vector< FXD::S32 > vecInt4(vecInt2);
		FXD::Container::Vector< TestObject > vecTO4(vecTO2);
		FXD::Container::Vector< FXD::Container::LinkedList< TestObject > > vecListTO4(vecListTO2);
		{
			PRINT_COND_ASSERT(vecInt4.validate(), "Vector - Vector(const my_type& rhs)");
			PRINT_COND_ASSERT(vecInt4 == vecInt2, "Vector - Vector(const my_type& rhs)");
			PRINT_COND_ASSERT(vecTO4.validate(), "Vector - Vector(const my_type& rhs)");
			PRINT_COND_ASSERT(vecTO4 == vecTO2, "Vector - Vector(const my_type& rhs)");
			PRINT_COND_ASSERT(vecInt4.validate(), "Vector - Vector(const my_type& rhs)");
			PRINT_COND_ASSERT(vecListTO4 == vecListTO2, "Vector - Vector(const my_type& rhs)");
		}

		Game::MallocAllocator::reset_all();

		// FXD::Container::Vector(const my_type& rhs, const allocator_type& alloc)
		{
			FXD::Container::Vector< FXD::S32, Game::MallocAllocator > vecInt5(vecInt1, ma);
			FXD::Container::Vector< TestObject, Game::MallocAllocator > vecTO5(vecTO1, ma);
			FXD::Container::Vector< FXD::Container::LinkedList< TestObject >, Game::MallocAllocator > vecListTO5(vecListTO1, ma);
			PRINT_COND_ASSERT(Game::MallocAllocator::m_nAllocCountAll == 3, "Vector - Vector(const my_type& rhs, const allocator_type& alloc)");
		}

		// FXD::Container::Vector(InputItr first, InputItr last)
		{
			FXD::Container::Deque< FXD::S32 > dequeInt(3);
			FXD::Container::Deque< TestObject > dequeTO(3);
			FXD::Container::Deque< FXD::Container::LinkedList< TestObject > > dequeListTO(3);

			FXD::Container::Vector< FXD::S32 > arrayInt5(dequeInt.begin(), dequeInt.end());
			FXD::Container::Vector< TestObject > toArray5(dequeTO.begin(), dequeTO.end());
			FXD::Container::Vector< FXD::Container::LinkedList< TestObject > > vecListTO(dequeListTO.begin(), dequeListTO.end());
		}

		// FXD::Container::Vector(std::initializer_list< T > iList);
		{
			FXD::Container::Vector< FXD::F32 > vecFloat{ 0, 1, 2, 3 };

			PRINT_COND_ASSERT(vecFloat.size() == 4, "");
			PRINT_COND_ASSERT((vecFloat[0] == 0) && (vecFloat[3] == 3), "Vector - Vector(std::initializer_list< T > iList)");
		}

		// FXD::Container::Vector& operator=(const my_type& rhs);
		{
			vecInt3 = vecInt4;
			vecTO3 = vecTO4;
			vecListTO3 = vecListTO4;

			PRINT_COND_ASSERT(vecInt3.validate(), "Vector - Vector(const my_type& rhs)");
			PRINT_COND_ASSERT(vecInt3 == vecInt4, "Vector - Vector(const my_type& rhs)");
			PRINT_COND_ASSERT(vecTO3.validate(), "Vector - Vector(const my_type& rhs)");
			PRINT_COND_ASSERT(vecTO3 == vecTO4, "Vector - Vector(const my_type& rhs)");
			PRINT_COND_ASSERT(vecInt3.validate(), "Vector - Vector(const my_type& rhs)");
			PRINT_COND_ASSERT(vecListTO3 == vecListTO4, "Vector - Vector(const my_type& rhs)");
		}

		// my_type& operator=(std::initializer_list< T > ilist);
		{
			vecInt3 = { 0, 1, 2, 3 };
			PRINT_COND_ASSERT((vecInt3.size() == 4) && (vecInt3[0] == 0) && (vecInt3[3] == 3), "Vector - my_type& operator=(std::initializer_list< T > ilist)");
		}
	}

	PRINT_COND_ASSERT(TestObject::is_clear(), "");
	TestObject::reset();

	// FXD::Container::Vector(my_type&& rhs)
	// FXD::Container::Vector(my_type&& rhs, const Allocator& allocator)
	// my_type& operator=(my_type&& rhs)
	{
		FXD::Container::Vector< TestObject > vecTO(3, TestObject(33));
		FXD::Container::Vector< TestObject > vecTOMove(std::move(vecTO));
		PRINT_COND_ASSERT((vecTOMove.size() == 3) && (vecTOMove.front().m_nX == 33) && (vecTO.size() == 0), "Vector - Vector(my_type&& rhs)");
	}

	// The following is not as strong a test of this ctor as itr could be. A stronger test would be to use IntanceAllocator with different instances.
	{
		FXD::Container::Vector< TestObject, Game::MallocAllocator > vecTO1(4, TestObject(44), Game::MallocAllocator());
		FXD::Container::Vector< TestObject, Game::MallocAllocator > vecTO2(std::move(vecTO1), Game::MallocAllocator());
		PRINT_COND_ASSERT((vecTO2.size() == 4) && (vecTO2.front().m_nX == 44) && (vecTO1.size() == 0), "Vector -");

		FXD::Container::Vector< TestObject, Game::MallocAllocator > vecTO3(5, TestObject(55), Game::MallocAllocator());
		vecTO2 = std::move(vecTO3);
		PRINT_COND_ASSERT((vecTO2.size() == 5) && (vecTO2.front().m_nX == 55) && (vecTO3.size() == 0), "Vector -");
	}

	// Should be able to emplace_back an item with const members (non-copyable)
	{
		FXD::Container::Vector< _ItemWithConst > vec1;
		_ItemWithConst& ref = vec1.emplace_back(42);
		PRINT_COND_ASSERT(vec1.back().m_nIdx == 42, "Vector - Vector< _ItemWithConst >");
		PRINT_COND_ASSERT(ref.m_nIdx == 42, "Vector - Vector< _ItemWithConst >");
	}

	// FXD::Container::Vector(const my_type& rhs, const allocator_type& alloc);
	{
		Game::MallocAllocator mallocator;
		FXD::Container::Vector< FXD::S32, Game::MallocAllocator > vec1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		FXD::Container::Vector< FXD::S32, Game::MallocAllocator > vec2(vec1, mallocator);
		PRINT_COND_ASSERT(vec1 == vec2, "Vector - Vector(const my_type& rhs, const allocator_type& alloc)");
		PRINT_COND_ASSERT(vec1.validate(), "Vector - Vector(const my_type& rhs, const allocator_type& alloc)");
		PRINT_COND_ASSERT(vec1.size() == vec2.size(), "Vector - Vector(const my_type& rhs, const allocator_type& alloc)");
		PRINT_COND_ASSERT(vec2.validate(), "Vector - Vector(const my_type& rhs, const allocator_type& alloc)");
		PRINT_COND_ASSERT(vec1.allocator() == vec2.allocator(), "Vector - Vector(const my_type& rhs, const allocator_type& alloc)");
	}

	// pointer data(void);
	// const_pointer data(void) const;
	// reference front(void);
	// const_reference front(void) const;
	// reference back(void);
	// const_reference back(void) const;
	{
		FXD::Container::Vector< FXD::S32 > vecInt(10, 7);
		vecInt[0] = 10;
		vecInt[1] = 11;
		vecInt[2] = 12;

		PRINT_COND_ASSERT(vecInt.data() == &vecInt[0], "Vector - data(void)");
		PRINT_COND_ASSERT(*vecInt.data() == 10, "Vector - data(void)");
		PRINT_COND_ASSERT(vecInt.front() == 10, "Vector - front(void)");
		PRINT_COND_ASSERT(vecInt.back() == 7, "Vector - back(void)");

		const FXD::Container::Vector< TestObject > vecTO(10, TestObject(7));

		PRINT_COND_ASSERT(vecTO.data() == &vecTO[0], "Vector - data(void)");
		PRINT_COND_ASSERT(*vecTO.data() == TestObject(7), "Vector - data(void)");
		PRINT_COND_ASSERT(vecTO.front() == TestObject(7), "Vector - front(void)");
		PRINT_COND_ASSERT(vecTO.back() == TestObject(7), "Vector - back(void)");
	}


	// iterator begin(void);
	// const_iterator begin(void) const;
	// iterator end(void);
	// const_iterator end(void) const;
	// reverse_iterator rbegin(void);
	// const_reverse_iterator rbegin(void) const;
	// reverse_iterator rend(void);
	// const_reverse_iterator rend(void) const;
	{
		FXD::Container::Vector< FXD::S32 > vecInt(20);
		for (FXD::U32 n1 = 0; n1 < 20; n1++)
		{
			vecInt[n1] = (FXD::S32)n1;
		}
		FXD::U32 n1 = 0;
		for (FXD::Container::Vector< FXD::S32 >::iterator itr = vecInt.begin(); itr != vecInt.end(); ++itr, ++n1)
		{
			PRINT_COND_ASSERT(*itr == (FXD::S32)n1, "Vector - iterator");
		}
		n1 = vecInt.size() - 1;
		for (FXD::Container::Vector< FXD::S32 >::reverse_iterator itr = vecInt.rbegin(); itr != vecInt.rend(); ++itr, --n1)
		{
			PRINT_COND_ASSERT(*itr == (FXD::S32)n1, "Vector - reverse_iterator");
		}
	}

	PRINT_COND_ASSERT(TestObject::is_clear(), "");
	TestObject::reset();

	// void swap(FXD::Container::Vector& rhs);
	// void assign(size_type nCount, const value_type& value);
	// void assign(InputItr first, InputItr last);
	// void assign(std::initializer_list<T> ilist);
	{
		const FXD::S32 array1[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 };
		const FXD::S32 array2[] = { 99, 99, 99, 99, 99 };
		const size_t N = sizeof(array1) / sizeof(FXD::S32);
		const size_t M = sizeof(array2) / sizeof(FXD::S32);

		// assign from pointer range
		FXD::Container::Vector< FXD::S32 > vec1;
		vec1.assign(array1, array1 + N);
		PRINT_COND_ASSERT(FXD::STD::equal(vec1.begin(), vec1.end(), array1), "Vector - void assign(InputItr first, InputItr last)");
		PRINT_COND_ASSERT(vec1.size() == N, "Vector - void assign(InputItr first, InputItr last)");

		// assign from iterator range
		FXD::Container::Vector< FXD::S32 > vec2;
		vec2.assign(vec1.begin(), vec1.end());
		PRINT_COND_ASSERT(FXD::STD::equal(vec2.begin(), vec2.end(), array1), "Vector - void assign(InputItr first, InputItr last)");
		PRINT_COND_ASSERT(FXD::STD::equal(array1, array1 + N, vec2.begin()), "Vector - void assign(InputItr first, InputItr last)");

		// assign from initializer range with resize
		vec2.assign(M, 99);
		PRINT_COND_ASSERT(FXD::STD::equal(vec2.begin(), vec2.end(), array2), "Vector - void assign(size_type nCount, const value_type& value)");
		PRINT_COND_ASSERT(FXD::STD::equal(array2, array2 + M, vec2.begin()), "Vector - void assign(size_type nCount, const value_type& value)");
		PRINT_COND_ASSERT((vec2.size() == M) && (M != N), "Vector - void assign(size_type nCount, const value_type& value)");

		// void assign(std::initializer_list<T> ilist);
		vec2.assign({ 0, 1, 2, 3 });
		PRINT_COND_ASSERT(vec2.size() == 4, "");
		PRINT_COND_ASSERT((vec2[0] == 0) && (vec2[3] == 3), "Vector - void assign(std::initializer_list<T> iList)");
	}

	PRINT_COND_ASSERT(TestObject::is_clear(), "");
	TestObject::reset();

	// reference operator[](size_type nID);
	// const_reference operator[](size_type nID) const;
	// reference at(size_type nID);
	// const_reference at(size_type nID) const;
	{
		FXD::Container::Vector< FXD::S32 > vecInt(5);
		PRINT_COND_ASSERT(vecInt[3] == 0, "Vector - reference operator[](size_type nID)");
		PRINT_COND_ASSERT(vecInt.at(3) == 0, "Vector - reference at(size_type nID)");

		FXD::Container::Vector< TestObject > vecTO(5);
		PRINT_COND_ASSERT(vecTO[3] == TestObject(0), "Vector - reference operator[](size_type nID)");
		PRINT_COND_ASSERT(vecTO.at(3) == TestObject(0), "Vector - reference at(size_type nID)");
	}

	PRINT_COND_ASSERT(TestObject::is_clear(), "");
	TestObject::reset();

	// void push_back(const value_type& val);
	// void push_back(void);
	// void pop_back(void);
	// void push_back(value_type&& val);
	{
		FXD::Container::Vector< FXD::S32 > vecInt(6);
		for (FXD::U32 n1 = 0; n1 < 6; n1++)
		{
			vecInt[n1] = (FXD::S32)n1;
		}

		PRINT_COND_ASSERT(vecInt.validate(), "");
		PRINT_COND_ASSERT(vecInt.size() == 6, "");
		PRINT_COND_ASSERT(vecInt[5] == 5, "");

		for (FXD::U32 n1 = 0; n1 < 40; n1++)
		{
			FXD::S32& ref = vecInt.push_back();
			PRINT_COND_ASSERT(&ref == &vecInt.back(), "Vector - reference push_back(void)");
			ref = 98;
		}

		PRINT_COND_ASSERT(vecInt.validate(), "Vector - reference push_back(void)");
		PRINT_COND_ASSERT(vecInt.size() == 46, "Vector - reference push_back(void)");
		PRINT_COND_ASSERT(vecInt[45] == 98, "Vector - reference push_back(void)");

		for (FXD::U32 n1 = 0; n1 < 40; n1++)
		{
			vecInt.push_back(99);
		}

		PRINT_COND_ASSERT(vecInt.validate(), "Vector - void push_back(value_type&& value)");
		PRINT_COND_ASSERT(vecInt.size() == 86, "Vector - void push_back&& value)");
		PRINT_COND_ASSERT(vecInt[85] == 99, "Vector - void push_back(value_type&& value)");

		for (FXD::U32 n1 = 0; n1 < 30; n1++)
		{
			vecInt.pop_back();
		}

		PRINT_COND_ASSERT(vecInt.validate(), "Vector - void pop_back(void)");
		PRINT_COND_ASSERT(vecInt.size() == 56, "Vector - void pop_back(void)");
		PRINT_COND_ASSERT(vecInt[5] == 5, "Vector - void pop_back(void)");
	}

	// template < class... Args >
	// reference emplace_back(const_iterator pos, Args&&... args);
	{
		TestObject::reset();

		FXD::Container::Vector< TestObject > vecTO;

		TestObject& ref = vecTO.emplace_back(2, 3, 4);
		PRINT_COND_ASSERT((vecTO.size() == 1) && (vecTO.back().m_nX == (2 + 3 + 4)) && (TestObject::sTOCtorCount == 1), "Vector - reference emplace_back(Args&&... args)");
		PRINT_COND_ASSERT(ref.m_nX == (2 + 3 + 4), "Vector - reference emplace_back(Args&&... args)");

		vecTO.emplace(vecTO.begin(), 3, 4, 5);
		PRINT_COND_ASSERT((vecTO.size() == 2) && (vecTO.front().m_nX == (3 + 4 + 5)) && (TestObject::sTOCtorCount == 3), "Vector - reference emplace_back(Args&&... args)");
	}

	TestObject::reset();

	// void push_back(T&& val);
	// iterator insert(const_iterator pos, T&& value);
	{
		FXD::Container::Vector< TestObject > vecTO;

		vecTO.push_back(TestObject(2, 3, 4));
		PRINT_COND_ASSERT((vecTO.size() == 1) && (vecTO.back().m_nX == (2 + 3 + 4)) && (TestObject::sTOMoveCtorCount == 1), "Vector - void push_back(T&& val)");

		vecTO.insert(vecTO.begin(), TestObject(3, 4, 5));
		PRINT_COND_ASSERT((vecTO.size() == 2) && (vecTO.front().m_nX == (3 + 4 + 5)) && (TestObject::sTOMoveCtorCount == 3), "Vector - iterator insert(const_iterator pos, T&& value)");
	}

	// We don't check for TestObject::is_clear because we messed with state above and don't currently have a matching set of ctors and dtors.
	TestObject::reset();

	// iterator erase(const_iterator pos);
	// iterator erase(const_iterator first, const_iterator last);
	// void clear(void);
	{
		FXD::Container::Vector< FXD::S32 > vecInt(20);
		for (FXD::U32 n1 = 0; n1 < 20; n1++)
		{
			vecInt[n1] = (FXD::S32)n1;
		}

		// 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19

		vecInt.erase(vecInt.begin() + 10); // Becomes: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19
		PRINT_COND_ASSERT(vecInt.validate(), "Vector - iterator erase(const_iterator pos)");
		PRINT_COND_ASSERT(vecInt.size() == 19, "Vector - iterator erase(const_iterator pos)");
		PRINT_COND_ASSERT(vecInt[0] == 0, "Vector - iterator erase(const_iterator pos)");
		PRINT_COND_ASSERT(vecInt[10] == 11, "Vector - iterator erase(const_iterator pos)");
		PRINT_COND_ASSERT(vecInt[18] == 19, "Vector - iterator erase(const_iterator pos)");

		vecInt.erase(vecInt.begin() + 10, vecInt.begin() + 15); // Becomes: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 16, 17, 18, 19
		PRINT_COND_ASSERT(vecInt.validate(), "Vector - iterator erase(const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT(vecInt.size() == 14, "Vector - iterator erase(const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT(vecInt[9] == 9, "Vector - iterator erase(const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT(vecInt[13] == 19, "Vector - iterator erase(const_iterator first, const_iterator last)");

		vecInt.erase(vecInt.begin() + 1, vecInt.begin() + 5); // Becomes: 0, 5, 6, 7, 8, 9, 16, 17, 18, 19
		PRINT_COND_ASSERT(vecInt.validate(), "Vector - iterator erase(const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT(vecInt.size() == 10, "Vector - iterator erase(const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT(vecInt[0] == 0, "Vector - iterator erase(const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT(vecInt[1] == 5, "Vector - iterator erase(const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT(vecInt[9] == 19, "Vector - iterator erase(const_iterator first, const_iterator last)");

		vecInt.erase(vecInt.begin() + 7, vecInt.begin() + 10); // Becomes: 0, 5, 6, 7, 8, 9, 16
		PRINT_COND_ASSERT(vecInt.validate(), "Vector - iterator erase(const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT(vecInt.size() == 7, "Vector - iterator erase(const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT(vecInt[0] == 0, "Vector - iterator erase(const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT(vecInt[1] == 5, "Vector - iterator erase(const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT(vecInt[6] == 16, "Vector - iterator erase(const_iterator first, const_iterator last)");

		vecInt.clear();
		PRINT_COND_ASSERT(vecInt.validate(), "Vector - iterator erase(const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT(vecInt.empty(), "Vector - iterator erase(const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT(vecInt.size() == 0, "Vector - iterator erase(const_iterator first, const_iterator last)");

		FXD::Container::Vector< TestObject > toArray(20);
		for (FXD::U32 n1 = 0; n1 < 20; n1++)
		{
			toArray[n1] = TestObject((FXD::S32)n1);
		}

		toArray.erase(toArray.begin() + 10);
		PRINT_COND_ASSERT(toArray.validate(), "Vector - iterator erase(const_iterator pos)");
		PRINT_COND_ASSERT(toArray.size() == 19, "Vector - iterator erase(const_iterator pos)");
		PRINT_COND_ASSERT(toArray[10] == TestObject(11), "Vector - iterator erase(const_iterator pos)");

		toArray.erase(toArray.begin() + 10, toArray.begin() + 15);
		PRINT_COND_ASSERT(toArray.validate(), "Vector - iterator erase(const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT(toArray.size() == 14, "Vector - iterator erase(const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT(toArray[10] == TestObject(16), "Vector - iterator erase(const_iterator first, const_iterator last)");

		toArray.clear();
		PRINT_COND_ASSERT(toArray.validate(), "Vector - iterator erase(const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT(toArray.empty(), "Vector - iterator erase(const_iterator first, const_iterator last)");
		PRINT_COND_ASSERT(toArray.size() == 0, "Vector - iterator erase(const_iterator first, const_iterator last)");
	}

	PRINT_COND_ASSERT(TestObject::is_clear(), "");
	TestObject::reset();

	// reverse_iterator erase(reverse_iterator pos);
	// reverse_iterator erase(reverse_iterator first, reverse_iterator last);
	// reverse_iterator erase_unsorted(reverse_iterator pos);
	{
		FXD::Container::Vector< FXD::S32 > vecInt;

		for (FXD::U32 n1 = 0; n1 < 20; n1++)
		{
			vecInt.push_back((FXD::S32)n1);
		}
		PRINT_COND_ASSERT((vecInt.size() == 20) && (vecInt[0] == 0) && (vecInt[19] == 19), "Vector - void push_back(value_type&& value)");

		FXD::Container::Vector< FXD::S32 >::reverse_iterator r2A = vecInt.rbegin();
		FXD::Container::Vector< FXD::S32 >::reverse_iterator r2B = r2A + 3;
		vecInt.erase(r2A, r2B);
		PRINT_COND_ASSERT((vecInt.size() == 17), "Vector - reverse_iterator erase(reverse_iterator pos)");
		PRINT_COND_ASSERT((vecInt[0] == 0), "Vector - reverse_iterator erase(reverse_iterator pos)");
		PRINT_COND_ASSERT((vecInt[16] == 16), "Vector - reverse_iterator erase(reverse_iterator pos)");

		r2B = vecInt.rend();
		r2A = r2B - 3;
		vecInt.erase(r2A, r2B);
		PRINT_COND_ASSERT((vecInt.size() == 14), "Vector - reverse_iterator erase(reverse_iterator first, reverse_iterator last)");
		PRINT_COND_ASSERT((vecInt[0] == 3), "Vector - reverse_iterator erase(reverse_iterator first, reverse_iterator last)");
		PRINT_COND_ASSERT((vecInt[13] == 16), "Vector - reverse_iterator erase(reverse_iterator first, reverse_iterator last)");

		r2B = vecInt.rend() - 1;
		vecInt.erase(r2B);
		PRINT_COND_ASSERT((vecInt.size() == 13), "Vector - reverse_iterator erase(reverse_iterator pos)");
		PRINT_COND_ASSERT((vecInt[0] == 4), "Vector - reverse_iterator erase(reverse_iterator pos)");
		PRINT_COND_ASSERT((vecInt[12] == 16), "Vector - - reverse_iterator erase(reverse_iterator pos)");

		r2B = vecInt.rbegin();
		vecInt.erase(r2B);
		PRINT_COND_ASSERT((vecInt.size() == 12), "Vector - - reverse_iterator erase(reverse_iterator pos)");
		PRINT_COND_ASSERT((vecInt[0] == 4), "Vector - - reverse_iterator erase(reverse_iterator pos)");
		PRINT_COND_ASSERT((vecInt[11] == 15), "Vector - - reverse_iterator erase(reverse_iterator pos)");

		r2A = vecInt.rbegin();
		r2B = vecInt.rend();
		vecInt.erase(r2A, r2B);
		PRINT_COND_ASSERT(vecInt.size() == 0, "Vector - - reverse_iterator erase(reverse_iterator pos)");

		vecInt.resize(20);
		for (FXD::U32 n1 = 0; n1 < 20; n1++)
		{
			vecInt[n1] = (FXD::S32)n1;
		}

		vecInt.erase_unsorted(vecInt.rbegin() + 0);
		PRINT_COND_ASSERT(vecInt.validate(), "Vector - reverse_iterator erase_unsorted(reverse_iterator pos)");
		PRINT_COND_ASSERT(vecInt.size() == 19, "Vector - reverse_iterator erase_unsorted(reverse_iterator pos)");
		PRINT_COND_ASSERT(vecInt[0] == 0, "Vector - reverse_iterator erase_unsorted(reverse_iterator pos)");
		PRINT_COND_ASSERT(vecInt[10] == 10, "Vector - reverse_iterator erase_unsorted(reverse_iterator pos)");
		PRINT_COND_ASSERT(vecInt[18] == 18, "Vector - reverse_iterator erase_unsorted(reverse_iterator pos)");

		vecInt.erase_unsorted(vecInt.rbegin() + 10);
		PRINT_COND_ASSERT(vecInt.validate(), "Vector - reverse_iterator erase_unsorted(reverse_iterator pos)");
		PRINT_COND_ASSERT(vecInt.size() == 18, "Vector - reverse_iterator erase_unsorted(reverse_iterator pos)");
		PRINT_COND_ASSERT(vecInt[0] == 0, "Vector - reverse_iterator erase_unsorted(reverse_iterator pos)");
		PRINT_COND_ASSERT(vecInt[8] == 18, "Vector - reverse_iterator erase_unsorted(reverse_iterator pos)");
		PRINT_COND_ASSERT(vecInt[17] == 17, "Vector - reverse_iterator erase_unsorted(reverse_iterator pos)");

		vecInt.erase_unsorted(vecInt.rbegin() + 17);
		PRINT_COND_ASSERT(vecInt.validate(), "Vector - reverse_iterator erase_unsorted(reverse_iterator pos)");
		PRINT_COND_ASSERT(vecInt.size() == 17, "Vector - reverse_iterator erase_unsorted(reverse_iterator pos)");
		PRINT_COND_ASSERT(vecInt[0] == 17, "Vector - reverse_iterator erase_unsorted(reverse_iterator pos)");
		PRINT_COND_ASSERT(vecInt[8] == 18, "Vector - reverse_iterator erase_unsorted(reverse_iterator pos)");
		PRINT_COND_ASSERT(vecInt[16] == 16, "Vector - reverse_iterator erase_unsorted(reverse_iterator pos)");
	}

	PRINT_COND_ASSERT(TestObject::is_clear(), "");
	TestObject::reset();

	{
		const FXD::S32 nToRemove = 44;
		FXD::S32 arrayInt[] = { 42, 43, 44, 45, 46, 47 };

		FXD::Container::Vector< std::unique_ptr< FXD::S32 > > vecUnique;

		for (auto& te : arrayInt)
		{
			vecUnique.push_back(std::make_unique< FXD::S32 >(te));
		}

		// remove 'nToRemove' from the container
		auto iterToRemove = FXD::STD::find_if(vecUnique.begin(), vecUnique.end(), [&](std::unique_ptr< FXD::S32 >& e)
		{
			return (*e == nToRemove);
		});

		vecUnique.erase_unsorted(iterToRemove);
		PRINT_COND_ASSERT(vecUnique.size() == 5, "Vector - reverse_iterator erase_unsorted(reverse_iterator pos)");

		// verify 'nToRemove' is no longer in the container
		PRINT_COND_ASSERT(FXD::STD::find_if(vecUnique.begin(), vecUnique.end(), [&](std::unique_ptr< FXD::S32 >& e) { return *e == nToRemove; }) == vecUnique.end(), "Vector - reverse_iterator erase_unsorted(reverse_iterator pos)");

		// verify all other expected values are in the container
		for (auto& te : arrayInt)
		{
			if (te == nToRemove)
			{
				continue;
			}
			PRINT_COND_ASSERT(FXD::STD::find_if(vecUnique.begin(), vecUnique.end(), [&](std::unique_ptr< FXD::S32 >& e) { return *e == te; }) != vecUnique.end(), "Vector - reverse_iterator erase_unsorted(reverse_iterator pos)");
		}
	}

	PRINT_COND_ASSERT(TestObject::is_clear(), "");
	TestObject::reset();

	// iterator insert(iterator pos, const value_type& value);
	// iterator insert(const_iterator pos, value_type&& value);
	// iterator insert(iterator pos, size_type n, const value_type& values);
	// iterator insert(iterator pos, InputItr first, InputItr last);
	// iterator insert(const_iterator pos, std::initializer_list<T> iList);
	{
		FXD::Container::Vector< FXD::S32 > vecInsert(7, 13);
		PRINT_COND_ASSERT(VerifySequence(vecInsert.begin(), vecInsert.end(), FXD::S32(), "FXD::Container::Vector", 13, 13, 13, 13, 13, 13, 13, -1), "");

		// insert at end of size and capacity.
		vecInsert.insert(vecInsert.end(), 99);
		PRINT_COND_ASSERT(vecInsert.validate(), "");
		PRINT_COND_ASSERT(VerifySequence(vecInsert.begin(), vecInsert.end(), FXD::S32(), "FXD::Container::Vector.insert", 13, 13, 13, 13, 13, 13, 13, 99, -1), "Vector - iterator insert(const_iterator pos, value_type&& value)");

		// insert at end of size.
		vecInsert.reserve(30);
		vecInsert.insert(vecInsert.end(), 999);
		PRINT_COND_ASSERT(vecInsert.validate(), "");
		PRINT_COND_ASSERT(VerifySequence(vecInsert.begin(), vecInsert.end(), FXD::S32(), "FXD::Container::Vector.insert", 13, 13, 13, 13, 13, 13, 13, 99, 999, -1), "Vector - iterator insert(const_iterator pos, value_type&& value)");

		// Insert in middle.
		FXD::Container::Vector< FXD::S32 >::iterator itr = vecInsert.begin() + 7;
		itr = vecInsert.insert(itr, 49);
		PRINT_COND_ASSERT(vecInsert.validate(), "");
		PRINT_COND_ASSERT(VerifySequence(vecInsert.begin(), vecInsert.end(), FXD::S32(), "FXD::Container::Vector.insert", 13, 13, 13, 13, 13, 13, 13, 49, 99, 999, -1), "Vector - iterator insert(const_iterator pos, value_type&& value)");

		// Insert multiple copies
		itr = vecInsert.insert(vecInsert.begin() + 5, 3, 42);
		PRINT_COND_ASSERT(itr == vecInsert.begin() + 5, "");
		PRINT_COND_ASSERT(VerifySequence(vecInsert.begin(), vecInsert.end(), FXD::S32(), "FXD::Container::Vector.insert", 13, 13, 13, 13, 13, 42, 42, 42, 13, 13, 49, 99, 999, -1), "Vector - iterator insert(const_iterator pos, size_type nCount, const value_type& value)");

		// Insert multiple copies with count == 0
		FXD::Container::Vector< FXD::S32 >::iterator at = vecInsert.end();
		itr = vecInsert.insert(at, 0, 666);
		PRINT_COND_ASSERT(itr == at, "");
		PRINT_COND_ASSERT(VerifySequence(vecInsert.begin(), vecInsert.end(), FXD::S32(), "FXD::Container::Vector.insert", 13, 13, 13, 13, 13, 42, 42, 42, 13, 13, 49, 99, 999, -1), "Vector - iterator insert(const_iterator pos, size_type nCount, const value_type& value)");

		// Insert iterator range
		const FXD::S32 data[] = { 2, 3, 4, 5 };
		itr = vecInsert.insert(vecInsert.begin() + 1, data, data + 4);
		PRINT_COND_ASSERT(itr == vecInsert.begin() + 1, "");
		PRINT_COND_ASSERT(VerifySequence(vecInsert.begin(), vecInsert.end(), FXD::S32(), "FXD::Container::Vector.insert", 13, 2, 3, 4, 5, 13, 13, 13, 13, 42, 42, 42, 13, 13, 49, 99, 999, -1), "Vector - iterator insert(const_iterator pos, InputItr first, InputItr last)");

		// Insert empty iterator range
		at = vecInsert.begin() + 1;
		itr = vecInsert.insert(at, data + 4, data + 4);
		PRINT_COND_ASSERT(itr == at, "");
		PRINT_COND_ASSERT(VerifySequence(vecInsert.begin(), vecInsert.end(), FXD::S32(), "FXD::Container::Vector.insert", 13, 2, 3, 4, 5, 13, 13, 13, 13, 42, 42, 42, 13, 13, 49, 99, 999, -1), "Vector - iterator insert(const_iterator pos, InputItr first, InputItr last)");

		// Insert with reallocation
		itr = vecInsert.insert(vecInsert.end() - 3, 6, 17);
		PRINT_COND_ASSERT(itr == vecInsert.end() - (3 + 6), "");
		PRINT_COND_ASSERT(VerifySequence(vecInsert.begin(), vecInsert.end(), FXD::S32(), "FXD::Container::Vector.insert", 13, 2, 3, 4, 5, 13, 13, 13, 13, 42, 42, 42, 13, 13, 17, 17, 17, 17, 17, 17, 49, 99, 999, -1), "Vector - iterator insert(const_iterator pos, size_type nCount, const value_type& value)");

		// Single insert with reallocation
		FXD::Container::Vector< FXD::S32 > vecInt2;
		vecInt2.reserve(100);
		vecInt2.insert(vecInt2.begin(), 100, 17);
		PRINT_COND_ASSERT(vecInt2.size() == 100, "Vector - iterator insert(const_iterator pos, size_type nCount, const value_type& value)");
		PRINT_COND_ASSERT(vecInt2[0] == 17, "Vector - iterator insert(const_iterator pos, size_type nCount, const value_type& value)");

		vecInt2.insert(vecInt2.begin() + 50, 42);
		PRINT_COND_ASSERT(vecInt2.size() == 101, "Vector - iterator insert(const_iterator pos, value_type&& value)");
		PRINT_COND_ASSERT(vecInt2[50] == 42, "Vector - iterator insert(const_iterator pos, value_type&& value)");

		// Test insertion of values that come from within the FXD::Container::Vector.
		vecInsert.insert(vecInsert.end() - 3, vecInsert.end() - 5, vecInsert.end());
		PRINT_COND_ASSERT(VerifySequence(vecInsert.begin(), vecInsert.end(), FXD::S32(), "FXD::Container::Vector.insert", 13, 2, 3, 4, 5, 13, 13, 13, 13, 42, 42, 42, 13, 13, 17, 17, 17, 17, 17, 17, 17, 17, 49, 99, 999, 49, 99, 999, -1), "Vector - iterator insert(const_iterator pos, InputItr first, InputItr last)");

		vecInsert.insert(vecInsert.end() - 3, vecInsert.back());
		PRINT_COND_ASSERT(VerifySequence(vecInsert.begin(), vecInsert.end(), FXD::S32(), "FXD::Container::Vector.insert", 13, 2, 3, 4, 5, 13, 13, 13, 13, 42, 42, 42, 13, 13, 17, 17, 17, 17, 17, 17, 17, 17, 49, 99, 999, 999, 49, 99, 999, -1), "Vector - iterator insert(const_iterator pos, InputItr first, InputItr last)");

		vecInsert.insert(vecInsert.end() - 3, 2, vecInsert[vecInsert.size() - 3]);
		PRINT_COND_ASSERT(VerifySequence(vecInsert.begin(), vecInsert.end(), FXD::S32(), "FXD::Container::Vector.insert", 13, 2, 3, 4, 5, 13, 13, 13, 13, 42, 42, 42, 13, 13, 17, 17, 17, 17, 17, 17, 17, 17, 49, 99, 999, 999, 49, 49, 49, 99, 999, -1), "Vector - iterator insert(const_iterator pos, InputItr first, InputItr last)");

		// iterator insert(const_iterator pos, std::initializer_list<T> iList);
		FXD::Container::Vector< FXD::F32 > vecFloat;
		vecFloat.insert(vecFloat.end(), { 0, 1, 2, 3 });
		PRINT_COND_ASSERT(vecFloat.size() == 4, "Vector - iterator insert(const_iterator pos, std::initializer_list<T> iList)");
		PRINT_COND_ASSERT((vecFloat[0] == 0) && (vecFloat[3] == 3), "Vector - iterator insert(const_iterator pos, std::initializer_list<T> iList)");
	}

	PRINT_COND_ASSERT(TestObject::is_clear(), "");
	TestObject::reset();

	{
		// Test insert move objects
		FXD::Container::Vector< TestObject > vecTO1;
		vecTO1.reserve(20);
		for (FXD::S32 n1 = 0; n1 < 2; ++n1)
		{
			vecTO1.push_back(TestObject(n1));
		}

		FXD::Container::Vector< TestObject > vecTO2;
		for (FXD::S32 n1 = 0; n1 < 3; ++n1)
		{
			vecTO2.push_back(TestObject(10 + n1));
		}

		// Insert more objects than the existing number using insert with iterator
		TestObject::reset();
		FXD::Container::Vector< TestObject >::iterator itr;
		itr = vecTO1.insert(vecTO1.begin(), vecTO2.begin(), vecTO2.end());
		PRINT_COND_ASSERT(itr == vecTO1.begin(), "Vector - iterator insert(const_iterator pos, InputItr first, InputItr last)");
		PRINT_COND_ASSERT(VerifySequence(vecTO1.begin(), vecTO1.end(), FXD::S32(), "FXD::Container::Vector.insert", 10, 11, 12, 0, 1, -1), "Vector - iterator insert(const_iterator pos, InputItr first, InputItr last)");
		PRINT_COND_ASSERT(TestObject::sTOMoveCtorCount + TestObject::sTOMoveAssignCount == 2 && TestObject::sTOCopyCtorCount + TestObject::sTOCopyAssignCount == 3, "Vector - iterator insert(const_iterator pos, InputItr first, InputItr last)");

		FXD::Container::Vector< TestObject > toVector3;
		toVector3.push_back(TestObject(20));

		// Insert less objects than the existing number using insert with iterator
		TestObject::reset();
		itr = vecTO1.insert(vecTO1.begin(), toVector3.begin(), toVector3.end());
		PRINT_COND_ASSERT(VerifySequence(vecTO1.begin(), vecTO1.end(), FXD::S32(), "FXD::Container::Vector.insert", 20, 10, 11, 12, 0, 1, -1), "Vector - iterator insert(const_iterator pos, InputItr first, InputItr last)");
		PRINT_COND_ASSERT(itr == vecTO1.begin(), "Vector - iterator insert(const_iterator pos, InputItr first, InputItr last)");
		PRINT_COND_ASSERT(TestObject::sTOMoveCtorCount + TestObject::sTOMoveAssignCount == 5 && TestObject::sTOCopyCtorCount + TestObject::sTOCopyAssignCount == 1, "Vector - iterator insert(const_iterator pos, InputItr first, InputItr last)");

		// Insert more objects than the existing number using insert without iterator
		TestObject::reset();
		itr = vecTO1.insert(vecTO1.begin(), 1, TestObject(17));
		PRINT_COND_ASSERT(itr == vecTO1.begin(), "Vector - iterator insert(const_iterator pos, size_type nCount, const value_type& value)");
		PRINT_COND_ASSERT(VerifySequence(vecTO1.begin(), vecTO1.end(), FXD::S32(), "FXD::Container::Vector.insert", 17, 20, 10, 11, 12, 0, 1, -1), "Vector - iterator insert(const_iterator pos, size_type nCount, const value_type& value)");
		PRINT_COND_ASSERT(TestObject::sTOMoveCtorCount + TestObject::sTOMoveAssignCount == 6 && TestObject::sTOCopyCtorCount + TestObject::sTOCopyAssignCount == 2, "Vector - iterator insert(const_iterator pos, size_type nCount, const value_type& value)");

		// Insert less objects than the existing number using insert without iterator
		TestObject::reset();
		itr = vecTO1.insert(vecTO1.begin(), 10, TestObject(18));
		PRINT_COND_ASSERT(itr == vecTO1.begin(), "Vector - iterator insert(const_iterator pos, size_type nCount, const value_type& value)");
		PRINT_COND_ASSERT(VerifySequence(vecTO1.begin(), vecTO1.end(), FXD::S32(), "FXD::Container::Vector.insert", 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 17, 20, 10, 11, 12, 0, 1, -1), "Vector - iterator insert(const_iterator pos, size_type nCount, const value_type& value)");
		PRINT_COND_ASSERT(TestObject::sTOMoveCtorCount + TestObject::sTOMoveAssignCount == 7 && TestObject::sTOCopyCtorCount + TestObject::sTOCopyAssignCount == 11, "Vector - iterator insert(const_iterator pos, size_type nCount, const value_type& value)");
	}

	TestObject::reset();

	// void reserve(size_type nCount);
	// void resize(size_type nCount);
	// void resize(size_type nCount, const value_type& value);
	// size_type capacity(void) const NOEXCEPT;
	// void clear(void) NOEXCEPT;
	{
		FXD::Container::Vector< FXD::S32 > vec(10, 17);
		vec.reserve(20);
		PRINT_COND_ASSERT(vec.validate(), "Vector - void reserve(size_type nCount)");
		PRINT_COND_ASSERT(vec.size() == 10, "Vector - void reserve(size_type nCount)");
		PRINT_COND_ASSERT(vec.capacity() == 20, "Vector - void reserve(size_type nCount)");

		// Shrink
		vec.resize(7);
		PRINT_COND_ASSERT(vec.validate(), "Vector - void resize(size_type nCount)");
		PRINT_COND_ASSERT(vec.capacity() == 20, "Vector - void resize(size_type nCount)");

		// Grow without reallocation
		vec.resize(17);
		PRINT_COND_ASSERT(vec.validate(), "Vector - void resize(size_type nCount)");
		PRINT_COND_ASSERT(vec.capacity() == 20, "Vector - void resize(size_type nCount)");

		// Grow with reallocation
		vec.resize(42);
		FXD::Container::Vector< FXD::S32 >::size_type nCap = vec.capacity();
		PRINT_COND_ASSERT(vec.validate(), "Vector - void resize(size_type nCount)");
		PRINT_COND_ASSERT(vec[41] == 0, "Vector - void resize(size_type nCount)");
		PRINT_COND_ASSERT(nCap >= 42, "Vector - void resize(size_type nCount)");

		// Grow with reallocation
		vec.resize(44, 19);
		PRINT_COND_ASSERT(vec.validate(), "Vector - void resize(size_type nCount, const value_type& value)");
		PRINT_COND_ASSERT(vec[43] == 19, "Vector - void resize(size_type nCount, const value_type& value)");

		nCap = vec.capacity();
		PRINT_COND_ASSERT(nCap == 84, "Vector - size_type capacity(void) const NOEXCEPT");

		vec.clear();
		PRINT_COND_ASSERT(vec.validate(), "Vector - void clear(void) NOEXCEPT");
		PRINT_COND_ASSERT(vec.empty(), "Vector - void clear(void) NOEXCEPT");
		PRINT_COND_ASSERT(vec.capacity() == nCap, "Vector - void clear(void) NOEXCEPT");

		// How to shrink a FXD::Container::Vector's capacity to be equal to its size.
		FXD::Container::Vector< FXD::S32 >(vec).swap(vec);
		PRINT_COND_ASSERT(vec.validate(), "");
		PRINT_COND_ASSERT(vec.empty(), "");
		PRINT_COND_ASSERT(vec.capacity() == vec.size(), "");

		// How to completely clear a FXD::Container::Vector(size = 0, capacity = 0, no allocation).
		FXD::Container::Vector< FXD::S32 >().swap(vec);
		PRINT_COND_ASSERT(vec.validate(), "");
		PRINT_COND_ASSERT(vec.empty(), "");
		PRINT_COND_ASSERT(vec.capacity() == 0, "");
	}

	// set_capacity / reset
	{
		const FXD::S32 arrayInt[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 };
		const size_t karrayIntSize = sizeof(arrayInt) / sizeof(FXD::S32);

		FXD::Container::Vector< FXD::S32 > vec(30);
		PRINT_COND_ASSERT(vec.capacity() >= 30, "Vector - Vector(size_type nCount, const allocator_type& alloc)");

		vec.assign(arrayInt, arrayInt + karrayIntSize);
		PRINT_COND_ASSERT(VerifySequence(vec.begin(), vec.end(), FXD::S32(), "FXD::Container::Vector.assign", 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, -1), "Vector - void assign(InputItr first, InputItr last)");
	}

	TestObject::reset();

	// bool validate(void) const;
	{
		FXD::Container::Vector< FXD::S32 > vecInt(20);
		PRINT_COND_ASSERT(vecInt.validate(), "Vector - bool validate(void) const");
	}

	// bool operator==(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)
	// bool operator!=(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)
	// bool operator<=(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)
	// bool operator>=(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)
	// bool operator<(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)
	// bool operator>(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)
	{
		FXD::Container::Vector< FXD::S32 > vecInt1(10);
		FXD::Container::Vector< FXD::S32 > vecInt2(10);

		for (FXD::U32 n1 = 0; n1 < vecInt1.size(); n1++)
		{
			vecInt1[n1] = (FXD::S32)n1;
			vecInt2[n1] = (FXD::S32)n1;
		}

		PRINT_COND_ASSERT((vecInt1 == vecInt2), "Vector - bool operator==(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)");
		PRINT_COND_ASSERT(!(vecInt1 != vecInt2), "Vector - bool operator!=(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)");
		PRINT_COND_ASSERT((vecInt1 <= vecInt2), "Vector - bool operator<=(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)");
		PRINT_COND_ASSERT((vecInt1 >= vecInt2), "Vector - bool operator>=(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)");
		PRINT_COND_ASSERT(!(vecInt1 < vecInt2), "Vector - bool operator<(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)");
		PRINT_COND_ASSERT(!(vecInt1 > vecInt2), "Vector - bool operator>(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)");

		vecInt1.push_back(100);
		vecInt2.push_back(101);

		PRINT_COND_ASSERT(!(vecInt1 == vecInt2), "Vector - bool operator==(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)");
		PRINT_COND_ASSERT((vecInt1 != vecInt2), "Vector - bool operator!=(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)");
		PRINT_COND_ASSERT((vecInt1 <= vecInt2), "Vector - bool operator<=(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)");
		PRINT_COND_ASSERT(!(vecInt1 >= vecInt2), "Vector - bool operator>=(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)");
		PRINT_COND_ASSERT((vecInt1 < vecInt2), "Vector - bool operator<(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)");
		PRINT_COND_ASSERT(!(vecInt1 > vecInt2), "Vector - bool operator>(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)");
	}

	// Test FXD::Container::Vector< Align64 >
	{
		FXD::Container::Vector< Game::Align64 > vec64(10);

		vec64.resize(2);
		PRINT_COND_ASSERT(vec64.size() == 2, "Vector - bool resize(size_type nCount)");

		vec64.push_back(Game::Align64());
		PRINT_COND_ASSERT(vec64.size() == 3, "Vector - bool push_back(value_type&& value)");

		vec64.resize(0);
		PRINT_COND_ASSERT(vec64.size() == 0, "Vector - bool resize(size_type nCount)");

		vec64.insert(vec64.begin(), Game::Align64());
		PRINT_COND_ASSERT(vec64.size() == 1, "Vector - iterator insert(const_iterator pos, value_type&& value)");

		vec64.resize(20);
		PRINT_COND_ASSERT(vec64.size() == 20, "Vector - bool resize(size_type nCount)");
	}

	{
		FXD::Container::Vector< FXD::S32 > vecEmpty1;
		PRINT_COND_ASSERT(vecEmpty1.data() == nullptr, "Vector - Vector(void)");
		PRINT_COND_ASSERT(vecEmpty1.size() == 0, "Vector - Vector(void)");
		PRINT_COND_ASSERT(vecEmpty1.capacity() == 0, "Vector - Vector(void)");

		FXD::Container::Vector< FXD::S32 > vecEmpty2 = vecEmpty1;
		PRINT_COND_ASSERT(vecEmpty2.data() == nullptr, "Vector - my_type& operator=(const my_type& rhs)");
		PRINT_COND_ASSERT(vecEmpty2.size() == 0, "Vector - my_type& operator=(const my_type& rhs)");
		PRINT_COND_ASSERT(vecEmpty2.capacity() == 0, "Vector - my_type& operator=(const my_type& rhs)");
	}

	{
		// Test whose purpose is to see if calling FXD::Container::Vector::size() in a const loop results in the compiler optimizing the size() call to outside the loop.
		FXD::Container::Vector< TestObject > vecTO;

		vecTO.resize(7);

		for (FXD::U32 n1 = 0; n1 < vecTO.size(); n1++)
		{
			TestObject& to = vecTO[n1];

			if (to.m_nX == 99999)
			{
				to.m_nX++;
			}
		}
	}

	{
		// Test assign from iterator type.
		TestObject to;
		FXD::Container::Vector< TestObject > vecTO1;

		// InputItr
		Game::demoted_iterator< TestObject*, FXD::STD::forward_iterator_tag > itrDemote(&to);
		vecTO1.assign(itrDemote, itrDemote);

		// BidirItr
		FXD::Container::LinkedList< TestObject > toList;
		vecTO1.assign(toList.begin(), toList.end());

		// RandomAccessItr
		FXD::Container::Deque< TestObject > dequeTO;
		vecTO1.assign(dequeTO.begin(), dequeTO.end());

		// ContiguousIterator (note: as of this writing, FXD::Container::Vector doesn't actually use contiguous_iterator_tag)
		FXD::Container::Vector< TestObject > vecTO2;
		vecTO1.assign(vecTO2.begin(), vecTO2.end());
	}

	PRINT_COND_ASSERT(TestObject::is_clear(), "");
	TestObject::reset();

	{
		// Test user report that they think they saw code like this leak memory.
		FXD::Container::Vector< FXD::S32 > vecInt;

		vecInt.push_back(1);
		vecInt = FXD::Container::Vector< FXD::S32 >();

		FXD::Container::Vector< TestObject > vecTO;

		vecTO.push_back(TestObject(1));
		vecTO = FXD::Container::Vector< TestObject >();
	}

	PRINT_COND_ASSERT(TestObject::is_clear(), "");
	TestObject::reset();

	{
		// Regression to verify that const FXD::Container::Vector works.
		const FXD::Container::Vector< FXD::S32 > vecConst1;
		PRINT_COND_ASSERT(vecConst1.empty(), "Vector - const FXD::Container::Vector(void)");

		FXD::S32 arrayInt[3] = { 37, 38, 39 };
		const FXD::Container::Vector< FXD::S32 > vecConst2(arrayInt, arrayInt + 3);
		PRINT_COND_ASSERT(vecConst2.size() == 3, "Vector - const FXD::Container::Vector(void)");

		const FXD::Container::Vector< FXD::S32 > vecConst3(4, 37);
		PRINT_COND_ASSERT(vecConst3.size() == 4, "Vector - const FXD::Container::Vector(void)");

		const FXD::Container::Vector< FXD::S32 > vecConst4;
		const FXD::Container::Vector< FXD::S32 > vecConst5 = vecConst4;
	}

	{
		// Regression to verify that a bug fix for a FXD::Container::Vector optimization works.
		{
			FXD::Container::Vector< FXD::S32 > vec;
			vec.reserve(128);
			vec.resize(128, 37);
			vec.push_back(vec.front());
			PRINT_COND_ASSERT(vec.back() == 37, "Vector - ");
		}
		{
			FXD::Container::Vector< FXD::S32 > vec;
			vec.reserve(1024);
			vec.resize(1024, 37);
			vec.resize(2048, vec.front());
			PRINT_COND_ASSERT(vec.back() == 37, "Vector - ");
		}
	}

	// C++11 Range
	{
		FXD::Container::Vector< FXD::F32 > vecFloat;

		vecFloat.push_back(0.0);
		vecFloat.push_back(1.0);

		for (auto& f : vecFloat)
		{
			f += 1.0;
		}
		PRINT_COND_ASSERT(vecFloat.back() == 2.0, "Vector - for (auto)");
	}

	// C++11 cbegin, cend, crbegin, crend
	{
		// FXD::F32 FXD::Container::Vector
		{
			FXD::Container::Vector< FXD::F32 > vecFloat1;

			auto cb = vecFloat1.cbegin();
			auto ce = vecFloat1.cend();
			auto crb = vecFloat1.crbegin();
			auto cre = vecFloat1.crend();

			PRINT_COND_ASSERT(FXD::STD::distance(cb, ce) == 0, "Vector - const_iterator cbegin(void) const NOEXCEPT");
			PRINT_COND_ASSERT(FXD::STD::distance(crb, cre) == 0, "Vector - const_reverse_iterator crbegin(void) const NOEXCEPT");
		}

		// const FXD::F32 FXD::Container::Vector
		{
			const FXD::Container::Vector< FXD::F32 > vecFloat2;

			auto ccb = vecFloat2.cbegin();
			auto cce = vecFloat2.cend();
			auto ccrb = vecFloat2.crbegin();
			auto ccre = vecFloat2.crend();

			PRINT_COND_ASSERT(FXD::STD::distance(ccb, cce) == 0, "Vector - const_iterator cbegin(void) const NOEXCEPT");
			PRINT_COND_ASSERT(FXD::STD::distance(ccrb, ccre) == 0, "Vector - const_reverse_iterator crbegin(void) const NOEXCEPT");
		}
	}

	{
		// Regression for failure in DoRealloc's use of uninitialize_move.
		const FXD::Core::String8 str0 = "TestString0";
		FXD::Container::Vector< FXD::Core::String8 > vec(1, str0);
		FXD::Container::Vector< FXD::Core::String8 > vecCopy;

		// Test operator=
		vecCopy = vec;
		PRINT_COND_ASSERT((vecCopy.size() == 1), "Vector - my_type& operator=(const my_type& rhs)");
		PRINT_COND_ASSERT(FXD::STD::find(vecCopy.begin(), vecCopy.end(), str0) != vecCopy.end(), "Vector - my_type& operator=(const my_type& rhs)");
		PRINT_COND_ASSERT(vec.size() == 1, "Vector - my_type& operator=(const my_type& rhs)");
		PRINT_COND_ASSERT(FXD::STD::find(vec.begin(), vec.end(), str0) != vec.end(), "Vector - my_type& operator=(const my_type& rhs)");

		// Test assign.
		vec.clear();
		vec.push_back(str0);
		vecCopy.assign(vec.begin(), vec.end());
		PRINT_COND_ASSERT(vecCopy.size() == 1, "Vector - assign(InputItr first, InputItr last)");
		PRINT_COND_ASSERT(FXD::STD::find(vecCopy.begin(), vecCopy.end(), str0) != vecCopy.end(), "Vector - assign(InputItr first, InputItr last)");
		PRINT_COND_ASSERT(vec.size() == 1, "Vector - assign(InputItr first, InputItr last)");
		PRINT_COND_ASSERT(FXD::STD::find(vec.begin(), vec.end(), str0) != vec.end(), "Vector - assign(InputItr first, InputItr last)");
	}

	// Test shrink_to_fit
	{
		FXD::Container::Vector< FXD::S32 > vec;
		vec.resize(100);
		PRINT_COND_ASSERT(vec.capacity() == 100, "Vector - assign(InputItr first, InputItr last)");
		vec.clear();
		PRINT_COND_ASSERT(vec.capacity() == 100, "Vector - assign(InputItr first, InputItr last)");
		vec.shrink_to_fit();
		PRINT_COND_ASSERT(vec.capacity() == 0, "Vector - assign(InputItr first, InputItr last)");
	}

	// Regression for issue with FXD::Container::Vector containing non-copyable values reported by user
	{
		FXD::Container::Vector< _TestMovable > v1;
		_TestMovable moveable;
		v1.insert(v1.end(), std::move(moveable));
	}

	// Calling erase of empty range should not call a move assignment to self
	{
		FXD::Container::Vector< _TestMoveAssignToSelf > vec;
		vec.push_back(_TestMoveAssignToSelf());
		PRINT_COND_ASSERT(!vec[0].m_movedToSelf, "");
		vec.erase(vec.begin(), vec.begin());
		PRINT_COND_ASSERT(!vec[0].m_movedToSelf, "");
	}

	return nErrorCount;
}