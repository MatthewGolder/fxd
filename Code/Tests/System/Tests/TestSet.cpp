// Creator - MatthewGolder
#include "Tests/System/Tests/TestSet.h"
#include "FXDEngine/Container/Set.h"
#include "FXDEngine/Container/MultiSet.h"
#include "FXDEngine/Container/Vector.h"
#include <set>

// Template instantations.
// These tell the compiler to compile all the functions for the given class.
template class FXD::Container::Set< FXD::S32 >;
template class FXD::Container::Multiset< FXD::F32 >;
template class FXD::Container::Set< TestObject >;
template class FXD::Container::Multiset< TestObject>;

///////////////////////////////////////////////////////////////////////////////
// typedefs
///////////////////////////////////////////////////////////////////////////////
using VS1 = FXD::Container::Set< FXD::S32 >;
using VS4 = FXD::Container::Set< TestObject >;
using VMS1 = FXD::Container::Multiset< FXD::S32 >;
using VMS4 = FXD::Container::Multiset< TestObject >;

using VS3 = std::set< FXD::S32 >;
using VS6 = std::set< TestObject >;
using VMS3 = std::multiset< FXD::S32 >;
using VMS6 = std::multiset< TestObject >;

///////////////////////////////////////////////////////////////////////////////
FXD::S32 TestSet(void)
{
	FXD::S32 nErrorCount = 0;

	{
		// Test construction
		nErrorCount += TestSetConstruction< VS1, VS3, false >();
		nErrorCount += TestSetConstruction< VS4, VS6, false >();
		nErrorCount += TestSetConstruction< VMS1, VMS3, true >();
		nErrorCount += TestSetConstruction< VMS4, VMS6, true >();
	}
	{
		// Test mutating functionality.
		nErrorCount += TestSetMutation< VS1, VS3, false >();
		nErrorCount += TestSetMutation< VS4, VS6, false >();
		nErrorCount += TestSetMutation< VMS1, VMS3, true >();
		nErrorCount += TestSetMutation< VMS4, VMS6, true >();
	}
	{
		// Test searching functionality.
		nErrorCount += TestSetSearch< VS1, false >();
		nErrorCount += TestSetSearch< VS4, false >();
		nErrorCount += TestSetSearch< VMS1, true >();
		nErrorCount += TestSetSearch< VMS4, true >();
	}
	{
		// C++11 emplace and related functionality
		nErrorCount += TestSetCpp11< FXD::Container::Set< TestObject > >();
		nErrorCount += TestMultisetCpp11< FXD::Container::Multiset< TestObject > >();
	}
	{
		// Misc tests
		// const key_compare& key_comp(void) const;
		// key_compare& key_comp(void);
		VS1 vs;
		const VS1 vsc;

		const VS1::key_compare& kc = vsc.key_comp();
		vs.key_comp() = kc;
	}
	return nErrorCount;
}