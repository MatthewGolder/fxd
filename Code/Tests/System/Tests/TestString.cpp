// Creator - MatthewGolder
#include "Tests/System/Tests/FXDTest.h"
#include "FXDEngine/Core/String.h"

#define TEST_STRING_NAME TestBasicString
#define LITERAL(x) x
#include "TestString.inl"

#define TEST_STRING_NAME TestBasicString16
#define LITERAL(x) (u##x)
#include "TestString.inl"

FXD::S32 TestString(void)
{
	FXD::S32 nErrorCount = 0;

	nErrorCount += TestBasicString< FXD::Core::StringBase< FXD::UTF8 > >();
	nErrorCount += TestBasicString< FXD::Core::String8 >();

	nErrorCount += TestBasicString16< FXD::Core::StringBase< FXD::UTF16 > >();
	nErrorCount += TestBasicString16< FXD::Core::String16 >();

	return nErrorCount;
}