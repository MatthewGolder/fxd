// Creator - MatthewGolder
#ifndef TESTS_SYSTEM_TESTS_H
#define TESTS_SYSTEM_TESTS_H

#include "FXDEngine/App/EngineApp.h"

namespace Game
{
	// ------
	// Enum
	// -
	// ------
	enum Enum
	{
		eValue1 = 0,
		eValue2 = 1
	};

	class Class
	{};

	struct Struct
	{};

	class FinalClass FINAL
	{};

	struct FinalStruct FINAL
	{};

	class ClassNonEmpty
	{
	public:
		FXD::S32 m_nX;
	};

	class Subclass : public Class
	{};

	struct ConvType
	{
		operator FXD::S32(void) const;
	};

	struct Polymorphic
	{
		virtual ~Polymorphic(void)
		{}
		virtual void Function(void)
		{}
	};

	struct DerivedPolymorphic1: public Polymorphic
	{};

	struct DerivedPolymorphic2
	{
		virtual ~DerivedPolymorphic2(void)
		{}
		virtual void Function(void) PURE;
	};

	struct Abstract1
	{
		virtual void Function(void) PURE;
	};

	struct Abstract2
	{
		virtual ~Abstract2(void) NOEXCEPT(false) PURE;
	};

	struct Abstract3
	{
		~Abstract3(void) NOEXCEPT(false);
		virtual void foo(void) NOEXCEPT PURE;
	};

	struct AbstractWithDtor
	{
		virtual void Function(void) PURE;
		virtual ~AbstractWithDtor(void)
		{}
	};

	struct NotTrivialNotStandardType   // neither trivial nor standard-layout
	{
		virtual ~NotTrivialNotStandardType(void);

		FXD::S32 i;
		FXD::S32 j;
	};

	struct TrivialNotStandardType   // trivial but not standard-layout
	{
		FXD::S32 i;

	private:
		FXD::S32 j;
	};

	struct NotTrivialStandardType  // standard-layout but not trivial
	{
		~NotTrivialStandardType(void);

		FXD::S32 i;
		FXD::S32 j;
	};

	struct TrivialStandardType // both trivial and standard-layout
	{
		FXD::S32 i;
		FXD::S32 j;
	};

	union UnionTrivial
	{
		FXD::S32 m_nX;
		FXD::S16 m_nY;
	};

	union UnionNoTrivial
	{
		FXD::S32 m_nX;
		NotTrivialStandardType m_nonT;
	};

	struct ConstTrivial
	{
		FXD::S32 m_nX;
	};

	struct ConstNoTrivial
	{
		ConstNoTrivial(void)
		{}

		FXD::S32 m_nX;
	};

	struct ConstDefault
	{
		ConstDefault(void) = default;

		FXD::S32 m_nX;
	};

	struct ConstDelete
	{
		ConstDelete(void) = delete;

		FXD::S32 m_nX;
	};

	struct ConstExcept
	{
		ConstExcept(void) NOEXCEPT(false);
	};

	struct ConstNoExcept
	{
		ConstNoExcept(void) NOEXCEPT(true);
	};

	struct ConstOnly
	{
		ConstOnly(void) = default;
		ConstOnly(ConstOnly&&) = delete;
		ConstOnly(const ConstOnly&) = delete;
		ConstOnly& operator=(const ConstOnly&) = delete;
		ConstOnly& operator=(ConstOnly&&) = delete;
	};

	struct CopyConstTrivial
	{
		FXD::S32 m_nX;
	};

	struct CopyConstNoTrivial
	{
		CopyConstNoTrivial(CopyConstNoTrivial const& rhs)
		{}

		FXD::S32 m_nX;
	};

	struct CopyConstDefault
	{
		CopyConstDefault(CopyConstDefault const& rhs) = default;

		FXD::S32 m_X;
	};

	struct CopyConstDelete
	{
		CopyConstDelete(CopyConstDelete const& rhs) = delete;

		FXD::S32 m_X;
	};

	struct CopyConstExcept
	{
		CopyConstExcept(const CopyConstExcept& rhs) NOEXCEPT(false);
	};

	struct CopyConstNoExcept
	{
		CopyConstNoExcept(const CopyConstNoExcept& rhs) NOEXCEPT(true);
	};
	
	struct CopyConstOnly
	{
		CopyConstOnly(void) = delete;
		CopyConstOnly(CopyConstOnly&&) = delete;
		CopyConstOnly(const CopyConstOnly&) = default;
		CopyConstOnly& operator=(const CopyConstOnly&) = delete;
		CopyConstOnly& operator=(CopyConstOnly&&) = delete;
	};

	struct MoveConstTrivial
	{
		FXD::S32 m_nX;
	};

	struct MoveConstNoTrivial
	{
		MoveConstNoTrivial(MoveConstNoTrivial&& rhs)
		{}

		FXD::S32 m_nX;
	};

	struct MoveConstDefault
	{
		MoveConstDefault(MoveConstDefault&& rhs) = default;

		FXD::S32 m_X;
	};

	struct MoveConstDelete
	{
		MoveConstDelete(MoveConstDelete&& rhs) = delete;

		FXD::S32 m_X;
	};

	struct MoveConstExcept
	{
		MoveConstExcept(MoveConstExcept&& rhs) NOEXCEPT(false);
	};

	struct MoveConstNoExcept
	{
		MoveConstNoExcept(MoveConstNoExcept&& rhs) NOEXCEPT(true);
	};

	struct MoveConstOnly
	{
		MoveConstOnly(void) = delete;
		MoveConstOnly(const MoveConstOnly& rhs) = delete;
		MoveConstOnly(MoveConstOnly&& rhs) = default;
		MoveConstOnly& operator=(const MoveConstOnly&rhs) = delete;
		MoveConstOnly& operator=(MoveConstOnly&& rhs) = delete;
	};

	struct AssignTrivial
	{
		FXD::S32 m_nX;
	};

	struct AssignNoTrivial
	{
		virtual ~AssignNoTrivial(void)
		{}
	};

	struct AssignDefault
	{
		AssignDefault& operator=(const AssignDefault& rhs) = default;
		AssignDefault& operator=(AssignDefault&& rhs) = default;

		FXD::S32 m_nX;
	};

	struct AssignDelete
	{
		AssignDefault& operator=(const AssignDelete& rhs) = delete;
		AssignDefault& operator=(AssignDefault&& rhs) = delete;

		FXD::S32 m_nX;
	};

	struct AssignExcept
	{
		AssignExcept& operator=(const AssignExcept& rhs) NOEXCEPT(false);
		AssignExcept& operator=(AssignExcept&& rhs) NOEXCEPT(false);
	
		FXD::S32 m_nX;
	};

	struct AssignNoExcept
	{
		AssignNoExcept& operator=(const AssignNoExcept& rhs) NOEXCEPT(true);
		AssignNoExcept& operator=(AssignNoExcept&& rhs) NOEXCEPT(true);

		FXD::S32 m_nX;
	};

	struct AssignOnly
	{
		AssignOnly(void) = delete;
		AssignOnly(const AssignOnly& rhs) = delete;
		AssignOnly(AssignOnly&& rhs) = delete;
		AssignOnly& operator=(const AssignOnly& rhs) = default;
		AssignOnly& operator=(AssignOnly&& rhs) = default;

		FXD::S32 m_nX;
	};

	struct CopyAssignTrivial
	{
		FXD::S32 m_nX;
	};

	struct CopyAssignNoTrivial
	{
		virtual ~CopyAssignNoTrivial(void)
		{}
	};

	struct CopyAssignDefault
	{
		CopyAssignDefault& operator=(const CopyAssignDefault& rhs) = default;

		FXD::S32 m_nX;
	};

	struct CopyAssignDelete
	{
		CopyAssignDelete& operator=(const CopyAssignDelete& rhs) = delete;

		FXD::S32 m_nX;
	};

	struct CopyAssignExcept
	{
		CopyAssignExcept& operator=(const CopyAssignExcept& rhs) NOEXCEPT(false);

		FXD::S32 m_nX;
	};

	struct CopyAssignNoExcept
	{
		CopyAssignNoExcept& operator=(const CopyAssignNoExcept& rhs) NOEXCEPT(true);

		FXD::S32 m_nX;
	};

	struct CopyAssignOnly
	{
		CopyAssignOnly(void) = delete;
		CopyAssignOnly(const CopyAssignOnly& rhs) = delete;
		CopyAssignOnly(CopyAssignOnly&& rhs) = delete;
		CopyAssignOnly& operator=(const CopyAssignOnly& rhs) = default;
		CopyAssignOnly& operator=(CopyAssignOnly&& rhs) = delete;

		FXD::S32 m_nX;
	};

	struct MoveAssignTrivial
	{
		FXD::S32 m_nX;
	};

	struct MoveAssignNoTrivial
	{
		virtual ~MoveAssignNoTrivial(void)
		{}
	};

	struct MoveAssignDefault
	{
		MoveAssignDefault& operator=(MoveAssignDefault&& rhs) = default;

		FXD::S32 m_nX;
	};

	struct MoveAssignDelete
	{
		MoveAssignDelete& operator=(MoveAssignDelete&& rhs) = delete;

		FXD::S32 m_nX;
	};

	struct MoveAssignExcept
	{
		MoveAssignExcept& operator=(MoveAssignExcept&& rhs) NOEXCEPT(false);

		FXD::S32 m_nX;
	};

	struct MoveAssignNoExcept
	{
		MoveAssignNoExcept& operator=(MoveAssignNoExcept&& rhs) NOEXCEPT(true);

		FXD::S32 m_nX;
	};

	struct MoveAssignOnly
	{
		MoveAssignOnly(void) = delete;
		MoveAssignOnly(const MoveAssignOnly& rhs) = delete;
		MoveAssignOnly(MoveAssignOnly&& rhs) = delete;
		MoveAssignOnly& operator=(const MoveAssignOnly& rhs) = delete;
		MoveAssignOnly& operator=(MoveAssignOnly&& rhs) = default;

		FXD::S32 m_nX;
	};

	struct MoveOnlyType
	{
		MoveOnlyType(void) = delete;
		MoveOnlyType(FXD::S32 nVal) : m_nVal(nVal) {}
		MoveOnlyType(const MoveOnlyType&) = delete;
		MoveOnlyType(MoveOnlyType&& rhs) : m_nVal(rhs.m_nVal) { rhs.m_nVal = 0; }
		MoveOnlyType& operator=(const MoveOnlyType&) = delete;
		MoveOnlyType& operator=(MoveOnlyType&& rhs)
		{
			m_nVal = rhs.m_nVal;
			rhs.m_nVal = 0;
			return *this;
		}
		bool operator==(const MoveOnlyType& rhs) const { return m_nVal == rhs.m_nVal; }

		FXD::S32 m_nVal;
	};

	struct DestructibleTrivial
	{
		FXD::S32 m_nX;
	};

	struct DestructibleNoTrivial
	{
		virtual ~DestructibleNoTrivial(void)
		{}
	};

	struct DestructibleDefault
	{
		~DestructibleDefault(void) = default;

		FXD::S32 m_nX;
	};

	struct DestructibleDelete
	{
		~DestructibleDelete(void) = delete;

		FXD::S32 m_nX;
	};

	struct DestructibleExcept
	{
		~DestructibleExcept(void) NOEXCEPT_IF(false);

		FXD::S32 m_nX;
	};

	struct DestructibleNoExcept
	{
		~DestructibleNoExcept(void) NOEXCEPT_IF(true);

		FXD::S32 m_nX;
	};

	struct DestructibleOnly
	{
		DestructibleOnly(void) = delete;
		~DestructibleOnly(void) = default;
		DestructibleOnly(const DestructibleOnly& rhs) = delete;
		DestructibleOnly(DestructibleOnly&& rhs) = delete;
		DestructibleOnly& operator=(const DestructibleOnly& rhs) = delete;
		DestructibleOnly& operator=(DestructibleOnly&& rhs) = delete;

		FXD::S32 m_nX;
	};




	// Destructible - only valid operation on this class is to destroy it.
	class Destructible
	{
	public:
		Destructible(void) = delete;
		~Destructible(void) = default;
	};

	class NotDestructible
	{
	public:
		NotDestructible(void) = delete;
		~NotDestructible(void) = delete;
	};


	class DefaultConstructible
	{
	public:
		static const FXD::S32 kDefaultValue = 42;

		DefaultConstructible(void)
			: m_nVal(kDefaultValue)
		{}
		~DefaultConstructible(void)
		{}

		NO_COPY(DefaultConstructible);
		NO_MOVE(DefaultConstructible);
		NO_ASSIGNMENT(DefaultConstructible);
		NO_MOVE_ASSIGNMENT(DefaultConstructible);

	private:
		const FXD::S32 m_nVal;
	};


	struct SwapA
	{
		SwapA(SwapA const&) = delete;
		SwapA& operator=(SwapA const&) = delete;
	};
	struct SwapB
	{
		SwapB(SwapB const&) = delete;
		SwapB& operator=(SwapB const&) = delete;
	};
	struct SwapC
	{};
	struct SwapMove
	{};

	inline void swap(SwapA&, SwapA&)
	{}
	inline void swap(SwapA&, SwapB&) NOEXCEPT_IF(true)
	{}
	inline void swap(SwapA&, SwapC&) NOEXCEPT_IF(true)
	{}
	inline void swap(SwapB&, SwapA&) NOEXCEPT_IF(true)
	{}
	inline void swap(SwapC&, SwapA&)
	{}
	inline void swap(SwapMove&&, SwapMove&&) NOEXCEPT_IF(true)
	{}




	class MoveAssignable
	{
	public:
		static const FXD::S32 kDefaultValue = 42;

		static MoveAssignable Create(void)
		{
			return MoveAssignable{};
		}

		MoveAssignable(MoveAssignable&& rhs)
			: m_nVal(rhs.m_nVal)
		{}
		MoveAssignable& operator=(MoveAssignable&& rhs)
		{
			m_nVal = rhs.m_nVal;
			return (*this);
		}
		~MoveAssignable(void)
		{}

		NO_COPY(MoveAssignable);
		NO_ASSIGNMENT(MoveAssignable);

	private:
		MoveAssignable(void)
			: m_nVal(kDefaultValue)
		{}

	public:
		FXD::S32 m_nVal;
	};

	class MoveAndDefaultConstructible
	{
	public:
		static const FXD::S32 kDefaultValue = 42;

		MoveAndDefaultConstructible(void)
			: m_nVal(kDefaultValue)
		{}
		MoveAndDefaultConstructible(MoveAndDefaultConstructible&& rhs)
			: m_nVal(rhs.m_nVal)
		{}
		~MoveAndDefaultConstructible(void) = default;

		NO_COPY(MoveAndDefaultConstructible);
		NO_ASSIGNMENT(MoveAndDefaultConstructible);
		NO_MOVE_ASSIGNMENT(MoveAndDefaultConstructible);

	public:
		const FXD::S32 m_nVal;
	};

	struct MissingMoveConstructor
	{
		MissingMoveConstructor(void)
		{}
		MissingMoveConstructor(const MissingMoveConstructor&)
		{}
		MissingMoveConstructor& operator=(MissingMoveConstructor&&)
		{
			return (*this);
		}
		MissingMoveConstructor& operator=(const MissingMoveConstructor&)
		{
			return (*this);
		}
		bool operator<(const MissingMoveConstructor&) const
		{
			return true;
		}
	};

	struct MissingMoveAssignable
	{
		MissingMoveAssignable(void)
		{}
		MissingMoveAssignable(const MissingMoveAssignable&)
		{}
		MissingMoveAssignable(MissingMoveAssignable&&)
		{}
		MissingMoveAssignable& operator=(const MissingMoveAssignable&)
		{
			return (*this);
		}
		bool operator<(const MissingMoveAssignable&) const
		{
			return true;
		}
	};

	struct HasEquality
	{
		HasEquality& operator=(const HasEquality&)
		{}
		HasEquality& operator=(HasEquality&&)
		{}
	};

	struct MissingEquality
	{
		MissingEquality& operator==(const MissingEquality&) = delete;
	};

	struct PodEmpty
	{};

	struct Pod1
	{
		Game::PodEmpty m_podEmpty;
		FXD::S32 m_nX;
	};

	struct Pod2
	{
		Game::PodEmpty m_podEmpty;
		Game::Pod1 m_pod1;
		FXD::S32 m_nX;
	};

	struct NonPod1
	{
		NonPod1(void)
		{}
		virtual ~NonPod1(void)
		{}
	};

	struct NonPod2
	{
		virtual ~NonPod2(void)
		{}
		virtual void Function(void)
		{}
	};

	// ------
	// Align32
	// -
	// ------
	#define kFXDAlign32 32

	struct FXD_ALIGN_AS(kFXDAlign32) Align32
	{
	public:
		EXPLICIT Align32(FXD::S32 x = 0)
			: m_nX(x)
		{}

	public:
		FXD::S32 m_nX;
	};

	inline bool operator==(const Game::Align32& lhs, const Game::Align32& rhs)
	{
		return (lhs.m_nX == rhs.m_nX);
	}

	inline bool operator<(const Game::Align32& lhs, const Game::Align32& rhs)
	{
		return (lhs.m_nX < rhs.m_nX);
	}


	// ------
	// Align64
	// -
	// ------
	#define kFXDAlign64 64

	struct FXD_ALIGN_AS(kFXDAlign64) Align64
	{
	public:
		EXPLICIT Align64(FXD::S32 x = 0)
			: m_nX(x)
		{}

	public:
		FXD::S32 m_nX;
	};

	inline bool operator==(const Game::Align64& lhs, const Game::Align64& rhs)
	{
		return (lhs.m_nX < rhs.m_nX);
	}

	inline bool operator<(const Game::Align64& lhs, const Game::Align64& rhs)
	{
		return (lhs.m_nX < rhs.m_nX);
	}

	///////////////////////////////////////////////////////////////////////////////
	/// demoted_iterator
	///
	/// Converts an iterator into a demoted category. For example, you can convert
	/// an iterator of type bidirectional_iterator_tag to forward_iterator_tag.
	/// The following is a list of iterator types. A demonted iterator can be demoted
	/// only to a lower iterator category (earlier in the following list):
	///     input_iterator_tag
	///     forward_iterator_tag
	///     bidirectional_iterator_tag
	///     random_access_iterator_tag
	///     contiguous_iterator_tag
	///
	/// Converts something which can be iterated into a formal input iterator.
	/// This class is useful for testing functions and algorithms that expect
	/// InputIterators, which are the lowest and 'weakest' form of iterators.
	/// 
	/// Key traits of InputIterators:
	///    Algorithms on input iterators should never attempt to pass
	///    through the same iterator twice. They should be single pass 
	///    algorithms. value_type T is not required to be an lvalue type.
	///
	/// Example usage:
	///     using PointerAsBidirectionalIterator = demoted_iterator< FXD::S32*, FXD::STD::bidirectional_iterator_tag >;
	///     using VectorIteratorAsForwardIterator = demoted_iterator< MyVector::iterator, FXD::STD::forward_iterator_tag >;
	///
	/// Example usage:
	///     IntVector v;
	///     comb_sort(to_forward_iterator(v.begin()), to_forward_iterator(v.end()));
	///////////////////////////////////////////////////////////////////////////////
	template < typename Itr, typename ItrCategory >
	class demoted_iterator
	{
	public:
		using my_type = demoted_iterator< Itr, ItrCategory >;
		using iterator_type = Itr;
		using iterator_category = ItrCategory;
		using value_type = typename FXD::STD::iterator_traits< Itr >::value_type;
		using difference_type = typename FXD::STD::iterator_traits< Itr >::difference_type;
		using reference = typename FXD::STD::iterator_traits< Itr >::reference;
		using pointer = typename FXD::STD::iterator_traits< Itr >::pointer;

		demoted_iterator(void)
			: m_itr()
		{}

		EXPLICIT demoted_iterator(const iterator_type& rhs)
			: m_itr(rhs)
		{}

		demoted_iterator(const my_type& rhs)
			: m_itr(rhs.m_itr)
		{}
		my_type& operator=(const iterator_type& rhs)
		{
			m_itr = rhs;
			return (*this);
		}
		my_type& operator=(const my_type& rhs)
		{
			m_itr = rhs.m_itr;
			return (*this);
		}
		reference operator*() const
		{
			return *m_itr;
		}
		pointer operator->() const
		{
			return m_itr;
		}
		my_type& operator++()
		{
			++m_itr;
			return (*this);
		}
		my_type operator++(FXD::S32)
		{
			return my_type(m_itr++);
		}
		my_type& operator--()
		{
			--m_itr;
			return (*this);
		}
		my_type operator--(FXD::S32)
		{
			return my_type(m_itr--);
		}
		reference operator[](const difference_type& n) const
		{
			return m_itr[n];
		}
		my_type& operator+=(const difference_type& n)
		{
			m_itr += n;
			return (*this);
		}
		my_type operator+(const difference_type& n) const
		{
			return my_type(m_itr + n);
		}
		my_type& operator-=(const difference_type& n)
		{
			m_itr -= n;
			return (*this);
		}
		my_type operator-(const difference_type& n) const
		{
			return my_type(m_itr - n);
		}
		const iterator_type& base(void) const
		{
			return m_itr;
		}

	protected:
		iterator_type m_itr;
	};

	template < typename Itr1, typename ItrCategory1, typename Itr2, typename ItrCategory2 >
	inline bool operator==(const demoted_iterator< Itr1, ItrCategory1 >& lhs, const demoted_iterator< Itr2, ItrCategory2 >& rhs)
	{
		return (lhs.base() == rhs.base());
	}
	template < typename Itr1, typename ItrCategory1, typename Itr2, typename ItrCategory2 >
	inline bool operator!=(const demoted_iterator< Itr1, ItrCategory1 >& lhs, const demoted_iterator< Itr2, ItrCategory2 >& rhs)
	{
		return !(lhs == rhs);
	}
	template < typename Itr1, typename ItrCategory1, typename Itr2, typename ItrCategory2 >
	inline bool operator<(const demoted_iterator< Itr1, ItrCategory1 >& lhs, const demoted_iterator< Itr2, ItrCategory2 >& rhs)
	{
		return (lhs.base() < rhs.base());
	}
	template < typename Itr1, typename ItrCategory1, typename Itr2, typename ItrCategory2 >
	inline bool operator<=(const demoted_iterator< Itr1, ItrCategory1 >& lhs, const demoted_iterator< Itr2, ItrCategory2 >& rhs)
	{
		return !(rhs < lhs);
	}
	template < typename Itr1, typename ItrCategory1, typename Itr2, typename ItrCategory2 >
	inline bool operator>(const demoted_iterator< Itr1, ItrCategory1 >& lhs, const demoted_iterator< Itr2, ItrCategory2 >& rhs)
	{
		return (rhs < lhs);
	}
	template < typename Itr1, typename ItrCategory1, typename Itr2, typename ItrCategory2 >
	inline bool operator>=(const demoted_iterator< Itr1, ItrCategory1 >& lhs, const demoted_iterator< Itr2, ItrCategory2 >& rhs)
	{
		return !(lhs < rhs);
	}
	template < typename Itr1, typename ItrCategory1, typename Itr2, typename ItrCategory2 >
	inline demoted_iterator< Itr1, ItrCategory1 > operator-(const demoted_iterator< Itr1, ItrCategory1 >& lhs, const demoted_iterator< Itr2, ItrCategory2 >& rhs)
	{
		return demoted_iterator< Itr1, ItrCategory1 >(lhs.base() - rhs.base());
	}
	template < typename Itr1, typename ItrCategory1 >
	inline demoted_iterator< Itr1, ItrCategory1 > operator+(typename demoted_iterator< Itr1, ItrCategory1 >::difference_type n, const demoted_iterator< Itr1, ItrCategory1 >& lhs)
	{
		return (lhs + n);
	}

	///////////////////////////////////////////////////////////////////////////////
	/// to_xxx_iterator
	///
	/// Returns a demoted iterator
	///////////////////////////////////////////////////////////////////////////////
	template < typename Iterator >
	inline demoted_iterator< Iterator, FXD::STD::input_iterator_tag > to_input_iterator(const Iterator& itr)
	{
		return demoted_iterator< Iterator, FXD::STD::input_iterator_tag >(itr);
	}
	template < typename Iterator >
	inline demoted_iterator< Iterator, FXD::STD::forward_iterator_tag > to_forward_iterator(const Iterator& itr)
	{
		return demoted_iterator< Iterator, FXD::STD::forward_iterator_tag >(itr);
	}
	template < typename Iterator >
	inline demoted_iterator< Iterator, FXD::STD::bidirectional_iterator_tag > to_bidirectional_iterator(const Iterator& itr)
	{
		return demoted_iterator< Iterator, FXD::STD::bidirectional_iterator_tag >(itr);
	}
	template < typename Iterator >
	inline demoted_iterator< Iterator, FXD::STD::random_access_iterator_tag > to_random_access_iterator(const Iterator& itr)
	{
		return demoted_iterator< Iterator, FXD::STD::random_access_iterator_tag >(itr);
	}


	///////////////////////////////////////////////////////////////////////////////
	// MallocAllocator
	//
	// Implements an allocator that uses malloc/free as opposed to 
	// new/delete or PPMalloc Malloc/Free. This is useful for testing 
	// allocator behaviour of code.
	//
	// Example usage:
	// vector< FXD::S32, MallocAllocator > intVector;
	///////////////////////////////////////////////////////////////////////////////
	class MallocAllocator
	{
	public:
		MallocAllocator(const FXD::UTF8* pStr = "MallocAllocator");
		MallocAllocator(const MallocAllocator& rhs);
		MallocAllocator(const MallocAllocator&, const FXD::UTF8* pStr);
		MallocAllocator& operator=(const MallocAllocator& rhs);

		void* allocate(size_t nSize, FXD::S32 nFlags = 0);
		void deallocate(void* pMem, size_t nSize);

		const FXD::UTF8* get_name(void) const;
		void set_name(const FXD::UTF8* /*pStr*/);

		static void reset_all(void);

	public:
		FXD::S32 m_nAllocCount;
		FXD::S32 m_nFreeCount;
		FXD::S32 m_nAllocVolume;

		static FXD::S32 m_nAllocCountAll;
		static FXD::S32 m_nFreeCountAll;
		static FXD::S32 m_nAllocVolumeAll;
		static void* m_pLastAllocation;
	};

	inline bool operator==(const MallocAllocator&, const MallocAllocator&)
	{
		return true;
	}
	inline bool operator!=(const MallocAllocator&, const MallocAllocator&)
	{
		return false;
	}

} //namespace Game
#endif //TESTS_SYSTEM_TESTS_H