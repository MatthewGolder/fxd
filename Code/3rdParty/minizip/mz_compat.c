/* mz_compat.c -- Backwards compatible interface for older versions
	part of the MiniZip project

	Copyright (C) 2010-2020 Nathan Moinvaziri
	  https://github.com/nmoinvaz/minizip
	Copyright (C) 1998-2010 Gilles Vollant
	  https://www.winimage.com/zLibDll/minizip.html

	This program is distributed under the terms of the same license as zlib.
	See the accompanying LICENSE file for the full text of the license.
*/

#include <3rdParty/minizip/mz.h>
#include <3rdParty/minizip/mz_os.h>
#include <3rdParty/minizip/mz_strm.h>
#include <3rdParty/minizip/mz_strm_mem.h>
#include <3rdParty/minizip/mz_strm_os.h>
#include <3rdParty/minizip/mz_strm_zlib.h>
#include <3rdParty/minizip/mz_zip.h>
#include <stdio.h> /* SEEK */
#include <3rdParty/minizip/mz_compat.h>

/***************************************************************************/
typedef struct mz_compat_s
{
	void* stream;
	void* handle;
	uint64_t entry_index;
	int64_t entry_pos;
	int64_t total_out;
} mz_compat;
/***************************************************************************/

static int32_t zipConvertAppendToStreamMode(int nAppend)
{
	int32_t mode = MZ_OPEN_MODE_WRITE;
	switch (nAppend)
	{
		case APPEND_STATUS_CREATE:
		{
			mode |= MZ_OPEN_MODE_CREATE;
			break;
		}
		case APPEND_STATUS_CREATEAFTER:
		{
			mode |= MZ_OPEN_MODE_CREATE | MZ_OPEN_MODE_APPEND;
			break;
		}
		case APPEND_STATUS_ADDINZIP:
		{
			mode |= MZ_OPEN_MODE_READ | MZ_OPEN_MODE_APPEND;
			break;
		}
	}
	return mode;
}

zipFile zipOpen(const char* pPath, int nAppend)
{
	zlib_filefunc64_def pzlib = mz_stream_os_get_interface();
	return zipOpen2(pPath, nAppend, NULL, &pzlib);
}

zipFile zipOpen64(const void* pPath, int nAppend)
{
	zlib_filefunc64_def pzlib = mz_stream_os_get_interface();
	return zipOpen2(pPath, nAppend, NULL, &pzlib);
}

zipFile zipOpen2(const char* pPath, int nAppend, const char** pGlobalComment, zlib_filefunc_def *pzlib_filefunc_def)
{
	return zipOpen2_64(pPath, nAppend, pGlobalComment, pzlib_filefunc_def);
}

zipFile zipOpen2_64(const void* pPath, int nAppend, const char** pGlobalComment, zlib_filefunc64_def *pzlib_filefunc_def)
{
	int32_t mode = zipConvertAppendToStreamMode(nAppend);
	void* pStream = NULL;
	if (pzlib_filefunc_def)
	{
		if (mz_stream_create(&pStream, (mz_stream_vtbl *)*pzlib_filefunc_def) == NULL)
		{
			return NULL;
		}
	}
	else
	{
		if (mz_stream_os_create(&pStream) == NULL)
		{
			return NULL;
		}
	}

	if (mz_stream_open(pStream, pPath, mode) != MZ_OK)
	{
		mz_stream_delete(&pStream);
		return NULL;
	}

	zipFile zip = zipOpen_MZ(pStream, nAppend, pGlobalComment);
	if (zip == NULL)
	{
		mz_stream_delete(&pStream);
		return NULL;
	}

	return zip;
}

zipFile zipOpen_MZ(void* pStream, int nAppend, const char** pGlobalComment)
{
	mz_compat* pCompat = NULL;
	int32_t mode = zipConvertAppendToStreamMode(nAppend);

	void* handle = NULL;
	mz_zip_create(&handle);

	int32_t err = mz_zip_open(handle, pStream, mode);
	if (err != MZ_OK)
	{
		mz_zip_delete(&handle);
		return NULL;
	}

	if (pGlobalComment != NULL)
	{
		mz_zip_get_comment(handle, pGlobalComment);
	}

	pCompat = (mz_compat*)MZ_ALLOC(sizeof(mz_compat));
	if (pCompat != NULL)
	{
		pCompat->handle = handle;
		pCompat->stream = pStream;
	}
	else
	{
		mz_zip_delete(&handle);
	}

	return (zipFile)pCompat;
}

void* zipGetHandle_MZ(zipFile file)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL)
	{
		return NULL;
	}
	return pCompat->handle;
}

void* zipGetStream_MZ(zipFile file)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL)
	{
		return NULL;
	}
	return (void*)pCompat->stream;
}

int zipOpenNewFileInZip5(zipFile file, const char* pFilename, const zip_fileinfo* pZipFI,
	const void* pExtraFieldLocal, uint16_t nExtrafieldLocalSize, const void* pExtrafieldGlobal,
	uint16_t nExtraFiieldGlobalSize, const char* pComment, int nCompressionMethod, int level,
	int raw, int windowBits, int memLevel, int strategy, const char* pPassword,
	unsigned long crc_for_crypting, unsigned long version_madeby, unsigned long flag_base, int zip64)
{
	mz_compat* pCompat = (mz_compat*)file;

	MZ_UNUSED(strategy);
	MZ_UNUSED(memLevel);
	MZ_UNUSED(windowBits);
	MZ_UNUSED(nExtrafieldLocalSize);
	MZ_UNUSED(pExtraFieldLocal);
	MZ_UNUSED(crc_for_crypting);

	if (pCompat == NULL)
	{
		return ZIP_PARAMERROR;
	}

	mz_zip_file file_info;
	memset(&file_info, 0, sizeof(file_info));

	if (pZipFI != NULL)
	{
		uint64_t dos_date = 0;
		if (pZipFI->mz_dos_date != 0)
		{
			dos_date = pZipFI->mz_dos_date;
		}
		else
		{
			dos_date = mz_zip_tm_to_dosdate(&pZipFI->tmz_date);
		}

		file_info.modified_date = mz_zip_dosdate_to_time_t(dos_date);
		file_info.external_fa = pZipFI->external_fa;
		file_info.internal_fa = pZipFI->internal_fa;
	}

	if (pFilename == NULL)
	{
		pFilename = "-";
	}

	file_info.compression_method = (uint16_t)nCompressionMethod;
	file_info.filename = pFilename;
	/* file_info.extrafield_local = extrafield_local; */
	/* file_info.extrafield_local_size = nExtrafieldLocalSize; */
	file_info.extrafield = pExtrafieldGlobal;
	file_info.extrafield_size = nExtraFiieldGlobalSize;
	file_info.version_madeby = (uint16_t)version_madeby;
	file_info.comment = pComment;
	if (file_info.comment != NULL)
	{
		file_info.compressed_size = (uint16_t)strlen(file_info.comment);
	}
	file_info.flag = (uint16_t)flag_base;
	if (zip64)
	{
		file_info.zip64 = MZ_ZIP64_FORCE;
	}
	else
	{
		file_info.zip64 = MZ_ZIP64_DISABLE;
	}
#ifdef HAVE_WZAES
	if ((pPassword != NULL) || (raw && (file_info.flag & MZ_ZIP_FLAG_ENCRYPTED)))
	{
		file_info.aes_version = MZ_AES_VERSION;
	}
#endif

	return mz_zip_entry_write_open(pCompat->handle, &file_info, (int16_t)level, (uint8_t)raw, pPassword);
}

int zipOpenNewFileInZip4_64(zipFile file, const char* pFilename, const zip_fileinfo* pZipFI,
	const void* pExtraFieldLocal, uint16_t nExtrafieldLocalSize, const void* pExtrafieldGlobal,
	uint16_t nExtraFiieldGlobalSize, const char* pComment, int nCompressionMethod, int level,
	int raw, int windowBits, int memLevel, int strategy, const char* pPassword,
	unsigned long crc_for_crypting, unsigned long version_madeby, unsigned long flag_base, int zip64)
{
	return zipOpenNewFileInZip5(file, pFilename, pZipFI, pExtraFieldLocal, nExtrafieldLocalSize, pExtrafieldGlobal, nExtraFiieldGlobalSize, pComment, nCompressionMethod, level, raw, windowBits, memLevel, strategy, pPassword, crc_for_crypting, version_madeby, flag_base, zip64);
}

int zipOpenNewFileInZip4(zipFile file, const char* pFilename, const zip_fileinfo* pZipFI,
	const void* pExtraFieldLocal, uint16_t nExtrafieldLocalSize, const void* pExtrafieldGlobal,
	uint16_t nExtraFiieldGlobalSize, const char* pComment, int nCompressionMethod, int level,
	int raw, int windowBits, int memLevel, int strategy, const char* pPassword,
	unsigned long crc_for_crypting, unsigned long version_madeby, unsigned long flag_base)
{
	return zipOpenNewFileInZip4_64(file, pFilename, pZipFI, pExtraFieldLocal, nExtrafieldLocalSize, pExtrafieldGlobal, nExtraFiieldGlobalSize, pComment, nCompressionMethod, level, raw, windowBits, memLevel, strategy, pPassword, crc_for_crypting, version_madeby, flag_base, 0);
}

int zipOpenNewFileInZip3(zipFile file, const char* pFilename, const zip_fileinfo* pZipFI,
	const void* pExtraFieldLocal, uint16_t nExtrafieldLocalSize, const void* pExtrafieldGlobal,
	uint16_t nExtraFiieldGlobalSize, const char* pComment, int nCompressionMethod, int level,
	int raw, int windowBits, int memLevel, int strategy, const char* pPassword,
	unsigned long crc_for_crypting)
{
	return zipOpenNewFileInZip3_64(file, pFilename, pZipFI, pExtraFieldLocal, nExtrafieldLocalSize, pExtrafieldGlobal, nExtraFiieldGlobalSize, pComment, nCompressionMethod, level, raw, windowBits, memLevel, strategy, pPassword, crc_for_crypting, 0);
}

int zipOpenNewFileInZip3_64(zipFile file, const char* pFilename, const zip_fileinfo* pZipFI,
	const void* pExtraFieldLocal, uint16_t nExtrafieldLocalSize, const void* pExtrafieldGlobal,
	uint16_t nExtraFiieldGlobalSize, const char* pComment, int nCompressionMethod, int level,
	int raw, int windowBits, int memLevel, int strategy, const char* pPassword,
	uint32_t crc_for_crypting, int zip64)
{
	return zipOpenNewFileInZip4_64(file, pFilename, pZipFI, pExtraFieldLocal, nExtrafieldLocalSize, pExtrafieldGlobal, nExtraFiieldGlobalSize, pComment, nCompressionMethod, level, raw, windowBits, memLevel, strategy, pPassword, crc_for_crypting, MZ_VERSION_MADEBY, 0, zip64);
}

int zipOpenNewFileInZip2(zipFile file, const char* pFilename, const zip_fileinfo* pZipFI,
	const void* pExtraFieldLocal, uint16_t nExtrafieldLocalSize, const void* pExtrafieldGlobal,
	uint16_t nExtraFiieldGlobalSize, const char* pComment, int nCompressionMethod, int level,
	int raw)
{
	return zipOpenNewFileInZip3_64(file, pFilename, pZipFI, pExtraFieldLocal, nExtrafieldLocalSize, pExtrafieldGlobal, nExtraFiieldGlobalSize, pComment, nCompressionMethod, level, raw, 0, 0, 0, NULL, 0, 0);
}

int zipOpenNewFileInZip2_64(zipFile file, const char* pFilename, const zip_fileinfo* pZipFI,
	const void* pExtraFieldLocal, uint16_t nExtrafieldLocalSize, const void* pExtrafieldGlobal,
	uint16_t nExtraFiieldGlobalSize, const char* pComment, int nCompressionMethod, int level,
	int raw, int zip64)
{
	return zipOpenNewFileInZip3_64(file, pFilename, pZipFI, pExtraFieldLocal, nExtrafieldLocalSize, pExtrafieldGlobal, nExtraFiieldGlobalSize, pComment, nCompressionMethod, level, raw, 0, 0, 0, NULL, 0, zip64);
}

int zipOpenNewFileInZip(zipFile file, const char* pFilename, const zip_fileinfo* pZipFI,
	const void* pExtraFieldLocal, uint16_t nExtrafieldLocalSize, const void* pExtrafieldGlobal,
	uint16_t nExtraFiieldGlobalSize, const char* pComment, int nCompressionMethod, int level)
{
	return zipOpenNewFileInZip_64(file, pFilename, pZipFI, pExtraFieldLocal, nExtrafieldLocalSize, pExtrafieldGlobal, nExtraFiieldGlobalSize, pComment, nCompressionMethod, level, 0);
}

int zipOpenNewFileInZip_64(zipFile file, const char* pFilename, const zip_fileinfo* pZipFI,
	const void* pExtraFieldLocal, uint16_t nExtrafieldLocalSize, const void* pExtrafieldGlobal,
	uint16_t nExtraFiieldGlobalSize, const char* pComment, int nCompressionMethod, int level,
	int zip64)
{
	return zipOpenNewFileInZip2_64(file, pFilename, pZipFI, pExtraFieldLocal, nExtrafieldLocalSize, pExtrafieldGlobal, nExtraFiieldGlobalSize, pComment, nCompressionMethod, level, 0, zip64);
}

int zipWriteInFileInZip(zipFile file, const void* pBuf, uint32_t len)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL || len >= INT32_MAX)
	{
		return ZIP_PARAMERROR;
	}
	int32_t written = mz_zip_entry_write(pCompat->handle, pBuf, (int32_t)len);
	if ((written < 0) || ((uint32_t)written != len))
	{
		return ZIP_ERRNO;
	}
	return ZIP_OK;
}

int zipCloseFileInZipRaw(zipFile file, unsigned long uncompressed_size, unsigned long crc32)
{
	return zipCloseFileInZipRaw64(file, uncompressed_size, crc32);
}

int zipCloseFileInZipRaw64(zipFile file, uint64_t uncompressed_size, unsigned long crc32)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL)
	{
		return ZIP_PARAMERROR;
	}
	return mz_zip_entry_close_raw(pCompat->handle, (int64_t)uncompressed_size, crc32);
}

int zipCloseFileInZip(zipFile file)
{
	return zipCloseFileInZip64(file);
}

int zipCloseFileInZip64(zipFile file)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL)
	{
		return ZIP_PARAMERROR;
	}
	return mz_zip_entry_close(pCompat->handle);
}

int zipClose(zipFile file, const char* pGlobalComment)
{
	return zipClose_64(file, pGlobalComment);
}

int zipClose_64(zipFile file, const char* pGlobalComment)
{
	return zipClose2_64(file, pGlobalComment, MZ_VERSION_MADEBY);
}

int zipClose2_64(zipFile file, const char* pGlobalComment, uint16_t version_madeby)
{
	mz_compat* pCompat = (mz_compat*)file;
	int32_t err = MZ_OK;
	if (pCompat->handle != NULL)
	{
		err = zipClose2_MZ(file, pGlobalComment, version_madeby);
	}

	if (pCompat->stream != NULL)
	{
		mz_stream_close(pCompat->stream);
		mz_stream_delete(&pCompat->stream);
	}

	MZ_FREE(pCompat);

	return err;
}

// Only closes the zip handle, does not close the stream
int zipClose_MZ(zipFile file, const char* pGlobalComment)
{
	return zipClose2_MZ(file, pGlobalComment, MZ_VERSION_MADEBY);
}

// Only closes the zip handle, does not close the stream
int zipClose2_MZ(zipFile file, const char* pGlobalComment, uint16_t version_madeby)
{
	mz_compat* pCompat = (mz_compat*)file;
	int32_t err = MZ_OK;

	if (pCompat == NULL)
	{
		return ZIP_PARAMERROR;
	}
	if (pCompat->handle == NULL)
	{
		return err;
	}
	if (pGlobalComment != NULL)
	{
		mz_zip_set_comment(pCompat->handle, pGlobalComment);
	}
	mz_zip_set_version_madeby(pCompat->handle, version_madeby);
	err = mz_zip_close(pCompat->handle);
	mz_zip_delete(&pCompat->handle);

	return err;
}

/***************************************************************************/

unzFile unzOpen(const char* pPath)
{
	return unzOpen64(pPath);
}

unzFile unzOpen64(const void* pPath)
{
	zlib_filefunc64_def pzlib = mz_stream_os_get_interface();
	return unzOpen2(pPath, &pzlib);
}

unzFile unzOpen2(const char* pPath , zlib_filefunc_def *pzlib_filefunc_def)
{
	return unzOpen2_64(pPath, pzlib_filefunc_def);
}

unzFile unzOpen2_64(const void* pPath, zlib_filefunc64_def *pzlib_filefunc_def)
{
	unzFile unz = NULL;
	void* pStream = NULL;

	if (pzlib_filefunc_def)
	{
		if (mz_stream_create(&pStream, (mz_stream_vtbl *)*pzlib_filefunc_def) == NULL)
		{
			return NULL;
		}
	}
	else
	{
		if (mz_stream_os_create(&pStream) == NULL)
		{
			return NULL;
		}
	}

	if (mz_stream_open(pStream, pPath, MZ_OPEN_MODE_READ) != MZ_OK)
	{
		mz_stream_delete(&pStream);
		return NULL;
	}

	unz = unzOpen_MZ(pStream);
	if (unz == NULL)
	{
		mz_stream_close(pStream);
		mz_stream_delete(&pStream);
		return NULL;
	}
	return unz;
}

void* unzGetHandle_MZ(unzFile file)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL)
	{
		return NULL;
	}
	return pCompat->handle;
}

void* unzGetStream_MZ(unzFile file)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL)
	{
		return NULL;
	}
	return pCompat->stream;
}

unzFile unzOpen_MZ(void* pStream)
{
	void* handle = NULL;
	mz_zip_create(&handle);
	int32_t err = mz_zip_open(handle, pStream, MZ_OPEN_MODE_READ);

	if (err != MZ_OK)
	{
		mz_zip_delete(&handle);
		return NULL;
	}

	mz_compat* pCompat = (mz_compat*)MZ_ALLOC(sizeof(mz_compat));
	if (pCompat != NULL)
	{
		pCompat->handle = handle;
		pCompat->stream = pStream;

		mz_zip_goto_first_entry(pCompat->handle);
	}
	else
	{
		mz_zip_delete(&handle);
	}

	return (unzFile)pCompat;
}

int unzClose(unzFile file)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL)
	{
		return UNZ_PARAMERROR;
	}

	int32_t err = MZ_OK;
	if (pCompat->handle != NULL)
	{
		err = unzClose_MZ(file);
	}

	if (pCompat->stream != NULL)
	{
		mz_stream_close(pCompat->stream);
		mz_stream_delete(&pCompat->stream);
	}
	MZ_FREE(pCompat);

	return err;
}

// Only closes the zip handle, does not close the stream
int unzClose_MZ(unzFile file)
{
	mz_compat* pCompat = (mz_compat*)file;
	int32_t err = MZ_OK;

	if (pCompat == NULL)
	{
		return UNZ_PARAMERROR;
	}

	err = mz_zip_close(pCompat->handle);
	mz_zip_delete(&pCompat->handle);

	return err;
}

int unzGetGlobalInfo(unzFile file, unz_global_info* pglobal_info32)
{
	mz_compat* pCompat = (mz_compat*)file;
	unz_global_info64 global_info64;

	memset(pglobal_info32, 0, sizeof(unz_global_info));
	if (pCompat == NULL)
	{
		return UNZ_PARAMERROR;
	}

	int32_t err = unzGetGlobalInfo64(file, &global_info64);
	if (err == MZ_OK)
	{
		pglobal_info32->number_entry = (uint32_t)global_info64.number_entry;
		pglobal_info32->size_comment = global_info64.size_comment;
		pglobal_info32->number_disk_with_CD = global_info64.number_disk_with_CD;
	}
	return err;
}

int unzGetGlobalInfo64(unzFile file, unz_global_info64 *pglobal_info)
{
	mz_compat* pCompat = (mz_compat*)file;
	memset(pglobal_info, 0, sizeof(unz_global_info64));
	if (pCompat == NULL)
	{
		return UNZ_PARAMERROR;
	}

	const char *comment_ptr = NULL;
	int32_t err = mz_zip_get_comment(pCompat->handle, &comment_ptr);
	if (err == MZ_OK)
	{
		pglobal_info->size_comment = (uint16_t)strlen(comment_ptr);
	}
	if ((err == MZ_OK) || (err == MZ_EXIST_ERROR))
	{
		err = mz_zip_get_number_entry(pCompat->handle, &pglobal_info->number_entry);
	}
	if (err == MZ_OK)
	{
		err = mz_zip_get_disk_number_with_cd(pCompat->handle, &pglobal_info->number_disk_with_CD);
	}
	return err;
}

int unzGetGlobalComment(unzFile file, char* pComment, unsigned long nCommentSize)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pComment == NULL || nCommentSize == 0)
	{
		return UNZ_PARAMERROR;
	}

	const char *comment_ptr = NULL;
	int32_t err = mz_zip_get_comment(pCompat->handle, &comment_ptr);
	if (err == MZ_OK)
	{
		strncpy(pComment, comment_ptr, nCommentSize - 1);
		pComment[nCommentSize - 1] = 0;
	}
	return err;
}

int unzOpenCurrentFile3(unzFile file, int* pMethod, int* pLevel, int raw, const char *password)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL)
	{
		return UNZ_PARAMERROR;
	}
	if (pMethod != NULL)
	{
		*pMethod = 0;
	}
	if (pLevel != NULL)
	{
		*pLevel = 0;
	}

	pCompat->total_out = 0;

	mz_zip_file* pZipInfo = NULL;
	int32_t err = mz_zip_entry_read_open(pCompat->handle, (uint8_t)raw, password);
	if (err == MZ_OK)
	{
		err = mz_zip_entry_get_info(pCompat->handle, &pZipInfo);
	}
	if (err == MZ_OK)
	{
		if (pMethod != NULL)
		{
			*pMethod = pZipInfo->compression_method;
		}

		if (pLevel != NULL)
		{
			*pLevel = 6;
			switch (pZipInfo->flag & 0x06)
			{
				case MZ_ZIP_FLAG_DEFLATE_SUPER_FAST:
				{
					*pLevel = 1;
					break;
				}
				case MZ_ZIP_FLAG_DEFLATE_FAST:
				{
					*pLevel = 2;
					break;
				}
				case MZ_ZIP_FLAG_DEFLATE_MAX:
				{
					*pLevel = 9;
					break;
				}
			}
		}
	}

	void* pStream = NULL;
	if (err == MZ_OK)
	{
		err = mz_zip_get_stream(pCompat->handle, &pStream);
	}
	if (err == MZ_OK)
	{
		pCompat->entry_pos = mz_stream_tell(pStream);
	}
	return err;
}

int unzOpenCurrentFile(unzFile file)
{
	return unzOpenCurrentFile3(file, NULL, NULL, 0, NULL);
}

int unzOpenCurrentFilePassword(unzFile file, const char *password)
{
	return unzOpenCurrentFile3(file, NULL, NULL, 0, password);
}

int unzOpenCurrentFile2(unzFile file, int* pMethod, int* pLevel, int raw)
{
	return unzOpenCurrentFile3(file, pMethod, pLevel, raw, NULL);
}

int unzReadCurrentFile(unzFile file, void* pBuf, uint32_t len)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL || len >= INT32_MAX)
	{
		return UNZ_PARAMERROR;
	}
	int32_t err = mz_zip_entry_read(pCompat->handle, pBuf, (int32_t)len);
	if (err > 0)
	{
		pCompat->total_out += (uint32_t)err;
	}
	return err;
}

int unzCloseCurrentFile(unzFile file)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL)
	{
		return UNZ_PARAMERROR;
	}
	return mz_zip_entry_close(pCompat->handle);
}

int unzGetCurrentFileInfo(unzFile file, unz_file_info64* pFileInfo, char* pFilename, unsigned long nFilenameSize, void* pExtrafield, unsigned long nExtrafieldSize, char* pComment, unsigned long nCommentSize)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL)
	{
		return UNZ_PARAMERROR;
	}

	mz_zip_file* pZipInfo = NULL;
	int32_t err = mz_zip_entry_get_info(pCompat->handle, &pZipInfo);
	if (err != MZ_OK)
	{
		return err;
	}

	if (pFileInfo != NULL)
	{
		pFileInfo->version = pZipInfo->version_madeby;
		pFileInfo->version_needed = pZipInfo->version_needed;
		pFileInfo->flag = pZipInfo->flag;
		pFileInfo->compression_method = pZipInfo->compression_method;
		pFileInfo->mz_dos_date = mz_zip_time_t_to_dos_date(pZipInfo->modified_date);
		mz_zip_time_t_to_tm(pZipInfo->modified_date, &pFileInfo->tmu_date);
		pFileInfo->tmu_date.tm_year += 1900;
		pFileInfo->crc = pZipInfo->crc;

		pFileInfo->size_filename = pZipInfo->filename_size;
		pFileInfo->size_file_extra = pZipInfo->extrafield_size;
		pFileInfo->size_file_comment = pZipInfo->comment_size;

		pFileInfo->disk_num_start = (uint16_t)pZipInfo->disk_number;
		pFileInfo->internal_fa = pZipInfo->internal_fa;
		pFileInfo->external_fa = pZipInfo->external_fa;

		pFileInfo->compressed_size = (uint32_t)pZipInfo->compressed_size;
		pFileInfo->uncompressed_size = (uint32_t)pZipInfo->uncompressed_size;
	}

	uint16_t bytes_to_copy = 0;
	if (nFilenameSize > 0 && pFilename != NULL && pZipInfo->filename != NULL)
	{
		bytes_to_copy = (uint16_t)nFilenameSize;
		if (bytes_to_copy > pZipInfo->filename_size)
		{
			bytes_to_copy = pZipInfo->filename_size;
		}
		memcpy(pFilename, pZipInfo->filename, bytes_to_copy);
		if (bytes_to_copy < nFilenameSize)
		{
			pFilename[bytes_to_copy] = 0;
		}
	}
	if (nExtrafieldSize > 0 && pExtrafield != NULL)
	{
		bytes_to_copy = (uint16_t)nExtrafieldSize;
		if (bytes_to_copy > pZipInfo->extrafield_size)
		{
			bytes_to_copy = pZipInfo->extrafield_size;
		}
		memcpy(pExtrafield, pZipInfo->extrafield, bytes_to_copy);
	}
	if (nCommentSize > 0 && pComment != NULL && pZipInfo->comment != NULL)
	{
		bytes_to_copy = (uint16_t)nCommentSize;
		if (bytes_to_copy > pZipInfo->compressed_size)
		{
			bytes_to_copy = pZipInfo->comment_size;
		}
		memcpy(pComment, pZipInfo->comment, bytes_to_copy);
		if (bytes_to_copy < nCommentSize)
		{
			pComment[bytes_to_copy] = 0;
		}
	}
	return err;
}

int unzGetCurrentFileInfo64(unzFile file, unz_file_info64* pFileInfo, char* pFilename, unsigned long nFilenameSize, void* pExtrafield, unsigned long nExtrafieldSize, char* pComment, unsigned long nCommentSize)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL)
	{
		return UNZ_PARAMERROR;
	}

	mz_zip_file* pZipInfo = NULL;
	int32_t err = mz_zip_entry_get_info(pCompat->handle, &pZipInfo);
	if (err != MZ_OK)
	{
		return err;
	}

	if (pFileInfo != NULL)
	{
		pFileInfo->version = pZipInfo->version_madeby;
		pFileInfo->version_needed = pZipInfo->version_needed;
		pFileInfo->flag = pZipInfo->flag;
		pFileInfo->compression_method = pZipInfo->compression_method;
		pFileInfo->mz_dos_date = mz_zip_time_t_to_dos_date(pZipInfo->modified_date);
		mz_zip_time_t_to_tm(pZipInfo->modified_date, &pFileInfo->tmu_date);
		pFileInfo->tmu_date.tm_year += 1900;
		pFileInfo->crc = pZipInfo->crc;

		pFileInfo->size_filename = pZipInfo->filename_size;
		pFileInfo->size_file_extra = pZipInfo->extrafield_size;
		pFileInfo->size_file_comment = pZipInfo->compressed_size;

		pFileInfo->disk_num_start = pZipInfo->disk_number;
		pFileInfo->internal_fa = pZipInfo->internal_fa;
		pFileInfo->external_fa = pZipInfo->external_fa;

		pFileInfo->compressed_size = (uint64_t)pZipInfo->compressed_size;
		pFileInfo->uncompressed_size = (uint64_t)pZipInfo->uncompressed_size;
	}

	uint16_t bytes_to_copy = 0;
	if (nFilenameSize > 0 && pFilename != NULL && pZipInfo->filename != NULL)
	{
		bytes_to_copy = (uint16_t)nFilenameSize;
		if (bytes_to_copy > pZipInfo->filename_size)
		{
			bytes_to_copy = pZipInfo->filename_size;
		}
		memcpy(pFilename, pZipInfo->filename, bytes_to_copy);
		if (bytes_to_copy < nFilenameSize)
		{
			pFilename[bytes_to_copy] = 0;
		}
	}
	if (nExtrafieldSize > 0 && pExtrafield != NULL)
	{
		bytes_to_copy = (uint16_t)nExtrafieldSize;
		if (bytes_to_copy > pZipInfo->filename_size)
		{
			bytes_to_copy = pZipInfo->filename_size;
		}
		memcpy(pExtrafield, pZipInfo->extrafield, bytes_to_copy);
	}
	if (nCommentSize > 0 && pComment != NULL && pZipInfo->comment != NULL)
	{
		bytes_to_copy = (uint16_t)nCommentSize;
		if (bytes_to_copy > pZipInfo->comment_size)
		{
			bytes_to_copy = pZipInfo->comment_size;
		}
		memcpy(pComment, pZipInfo->comment, bytes_to_copy);
		if (bytes_to_copy < nCommentSize)
		{
			pComment[bytes_to_copy] = 0;
		}
	}
	return err;
}

int unzGoToFirstFile(unzFile file)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL)
	{
		return UNZ_PARAMERROR;
	}
	pCompat->entry_index = 0;
	return mz_zip_goto_first_entry(pCompat->handle);
}

int unzGoToNextFile(unzFile file)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL)
	{
		return UNZ_PARAMERROR;
	}

	int32_t err = mz_zip_goto_next_entry(pCompat->handle);
	if (err != MZ_END_OF_LIST)
	{
		pCompat->entry_index += 1;
	}
	return err;
}

int unzLocateFile(unzFile file, const char* pFilename, unzFileNameComparer filename_compare_func)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL)
	{
		return UNZ_PARAMERROR;
	}

	int32_t result = 0;
	mz_zip_file* pZipInfo = NULL;
	uint64_t preserve_index = pCompat->entry_index;
	int32_t err = mz_zip_goto_first_entry(pCompat->handle);
	while (err == MZ_OK)
	{
		err = mz_zip_entry_get_info(pCompat->handle, &pZipInfo);
		if (err != MZ_OK)
		{
			break;
		}

		if ((intptr_t)filename_compare_func > 2)
		{
			result = filename_compare_func(file, pFilename, pZipInfo->filename);
		}
		else
		{
			int32_t case_sensitive = (int32_t)(intptr_t)filename_compare_func;
			result = mz_path_compare_wc(pFilename, pZipInfo->filename, !case_sensitive);
		}

		if (result == 0)
		{
			return MZ_OK;
		}

		err = mz_zip_goto_next_entry(pCompat->handle);
	}

	pCompat->entry_index = preserve_index;
	return err;
}

/***************************************************************************/

int unzGetFilePos(unzFile file, unz_file_pos* pFilePos)
{
	unz64_file_pos file_pos64;
	int32_t err = unzGetFilePos64(file, &file_pos64);
	if (err < 0)
	{
		return err;
	}

	pFilePos->pos_in_zip_directory = (uint32_t)file_pos64.pos_in_zip_directory;
	pFilePos->num_of_file = (uint32_t)file_pos64.num_of_file;
	return err;
}

int unzGoToFilePos(unzFile file, unz_file_pos* pFilePos)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL || pFilePos == NULL)
	{
		return UNZ_PARAMERROR;
	}

	unz64_file_pos file_pos64;
	file_pos64.pos_in_zip_directory = pFilePos->pos_in_zip_directory;
	file_pos64.num_of_file = pFilePos->num_of_file;

	return unzGoToFilePos64(file, &file_pos64);
}

int unzGetFilePos64(unzFile file, unz64_file_pos* pFilePos)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL || pFilePos == NULL)
	{
		return UNZ_PARAMERROR;
	}

	int64_t offset = unzGetOffset64(file);
	if (offset < 0)
	{
		return (int)offset;
	}

	pFilePos->pos_in_zip_directory = offset;
	pFilePos->num_of_file = pCompat->entry_index;
	return UNZ_OK;
}

int unzGoToFilePos64(unzFile file, const unz64_file_pos* pFilePos)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL || pFilePos == NULL)
	{
		return UNZ_PARAMERROR;
	}

	int32_t err = mz_zip_goto_entry(pCompat->handle, pFilePos->pos_in_zip_directory);
	if (err == MZ_OK)
	{
		pCompat->entry_index = pFilePos->num_of_file;
	}
	return err;
}

unsigned long unzGetOffset(unzFile file)
{
	return (uint32_t)unzGetOffset64(file);
}

int64_t unzGetOffset64(unzFile file)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL)
	{
		return UNZ_PARAMERROR;
	}
	return mz_zip_get_entry(pCompat->handle);
}

int unzSetOffset(unzFile file, unsigned long pos)
{
	return unzSetOffset64(file, pos);
}

int unzSetOffset64(unzFile file, int64_t pos)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL)
	{
		return UNZ_PARAMERROR;
	}
	return (int)mz_zip_goto_entry(pCompat->handle, pos);
}

int unzGetLocalExtrafield(unzFile file, void* pBuf, unsigned int len)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL || pBuf == NULL || len >= INT32_MAX)
	{
		return UNZ_PARAMERROR;
	}

	mz_zip_file* pZipInfo = NULL;
	int32_t err = mz_zip_entry_get_local_info(pCompat->handle, &pZipInfo);
	if (err != MZ_OK)
	{
		return err;
	}

	int32_t bytes_to_copy = (int32_t)len;
	if (bytes_to_copy > pZipInfo->extrafield_size)
	{
		bytes_to_copy = pZipInfo->extrafield_size;
	}

	memcpy(pBuf, pZipInfo->extrafield, bytes_to_copy);
	return MZ_OK;
}

int32_t unzTell(unzFile file)
{
	return unztell(file);
}

int32_t unztell(unzFile file)
{
	return (int32_t)unztell64(file);
}

uint64_t unzTell64(unzFile file)
{
	return unztell64(file);
}

uint64_t unztell64(unzFile file)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL)
	{
		return UNZ_PARAMERROR;
	}
	return pCompat->total_out;
}

int unzSeek(unzFile file, int32_t offset, int origin)
{
	return unzSeek64(file, offset, origin);
}

int unzSeek64(unzFile file, int64_t offset, int origin)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL)
	{
		return UNZ_PARAMERROR;
	}

	mz_zip_file* pZipInfo = NULL;
	int32_t err = mz_zip_entry_get_info(pCompat->handle, &pZipInfo);
	if (err != MZ_OK)
	{
		return err;
	}
	if (pZipInfo->compression_method != MZ_COMPRESS_METHOD_STORE)
	{
		return UNZ_ERRNO;
	}

	int64_t position = 0;
	if (origin == SEEK_SET)
	{
		position = offset;
	}
	else if (origin == SEEK_CUR)
	{
		position = pCompat->total_out + offset;
	}
	else if (origin == SEEK_END)
	{
		position = (int64_t)pZipInfo->compressed_size + offset;
	}
	else
	{
		return UNZ_PARAMERROR;
	}

	if (position > (int64_t)pZipInfo->compressed_size)
	{
		return UNZ_PARAMERROR;
	}

	void* pStream = NULL;
	err = mz_zip_get_stream(pCompat->handle, &pStream);
	if (err == MZ_OK)
	{
		err = mz_stream_seek(pStream, pCompat->entry_pos + position, MZ_SEEK_SET);
	}
	if (err == MZ_OK)
	{
		pCompat->total_out = position;
	}
	return err;
}

int unzEndOfFile(unzFile file)
{
	return unzeof(file);
}

int unzeof(unzFile file)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL)
	{
		return UNZ_PARAMERROR;
	}

	mz_zip_file* pZipInfo = NULL;
	int32_t err = mz_zip_entry_get_info(pCompat->handle, &pZipInfo);
	if (err != MZ_OK)
	{
		return err;
	}
	if (pCompat->total_out == (int64_t)pZipInfo->uncompressed_size)
	{
		return 1;
	}
	return 0;
}

void* unzGetStream(unzFile file)
{
	mz_compat* pCompat = (mz_compat*)file;
	if (pCompat == NULL)
	{
		return NULL;
	}
	return (void*)pCompat->stream;
}

/***************************************************************************/

void fill_fopen_filefunc(zlib_filefunc_def *pzlib_filefunc_def)
{
	if (pzlib_filefunc_def != NULL)
	{
		*pzlib_filefunc_def = mz_stream_os_get_interface();
	}
}

void fill_fopen64_filefunc(zlib_filefunc64_def *pzlib_filefunc_def)
{
	if (pzlib_filefunc_def != NULL)
	{
		*pzlib_filefunc_def = mz_stream_os_get_interface();
	}
}

void fill_win32_filefunc(zlib_filefunc_def *pzlib_filefunc_def)
{
	if (pzlib_filefunc_def != NULL)
	{
		*pzlib_filefunc_def = mz_stream_os_get_interface();
	}
}

void fill_win32_filefunc64(zlib_filefunc64_def *pzlib_filefunc_def)
{
	if (pzlib_filefunc_def != NULL)
	{
		*pzlib_filefunc_def = mz_stream_os_get_interface();
	}
}

void fill_win32_filefunc64A(zlib_filefunc64_def *pzlib_filefunc_def)
{
	if (pzlib_filefunc_def != NULL)
	{
		*pzlib_filefunc_def = mz_stream_os_get_interface();
	}
}

void fill_win32_filefunc64W(zlib_filefunc64_def *pzlib_filefunc_def)
{
	// NOTE: You should no longer pass in widechar string to open function
	if (pzlib_filefunc_def != NULL)
	{
		*pzlib_filefunc_def = mz_stream_os_get_interface();
	}
}

void fill_memory_filefunc(zlib_filefunc_def *pzlib_filefunc_def)
{
	if (pzlib_filefunc_def != NULL)
	{
		*pzlib_filefunc_def = mz_stream_mem_get_interface();
	}
}
