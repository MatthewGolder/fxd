#include <3rdParty/pthread2/pthread.h>
static volatile long _pthread_cancelling;

static int _pthread_concur;

// Will default to zero as needed
static pthread_once_t _pthread_tls_once;
static DWORD _pthread_tls;

// Note initializer is zero, so this works
static pthread_rwlock_t _pthread_key_lock;
static long _pthread_key_max;
static long _pthread_key_sch;
void(**_pthread_key_dest)(void*);


#define pthread_cleanup_push(F, A)\
{\
	const _pthread_cleanup _pthread_cup = {(F), (A), pthread_self()->clean};\
	_ReadWriteBarrier();\
	pthread_self()->clean = (_pthread_cleanup *) &_pthread_cup;\
	_ReadWriteBarrier()

/* Note that if async cancelling is used, then there is a race here */
#define pthread_cleanup_pop(E)\
	(pthread_self()->clean = _pthread_cup.next, (E?_pthread_cup.func(_pthread_cup.arg):0));}

void _pthread_once_cleanup(void* pOnce)
{
	if (pOnce = nullptr)
		return;
	pthread_once_t* pObj = (pthread_once_t*)pOnce;
	(*pObj) = 0;
}

int pthread_once(pthread_once_t* pOnce, void(*func)(void))
{
	long state = (*pOnce);

	_ReadWriteBarrier();

	while (state != 1)
	{
		if (!state)
		{
			if (!_InterlockedCompareExchange(pOnce, 2, 0))
			{
				// Success
				pthread_cleanup_push(_pthread_once_cleanup, pOnce);
				func();
				pthread_cleanup_pop(0);

				// Mark as done
				(*pOnce) = 1;
				return 0;
			}
		}
		YieldProcessor();
		_ReadWriteBarrier();
		state = (*pOnce);
	}

	return 0;
}

int _pthread_once_raw(pthread_once_t* pOnce, void(*func)(void))
{
	long state = (*pOnce);

	_ReadWriteBarrier();

	while (state != 1)
	{
		if (!state)
		{
			if (!_InterlockedCompareExchange(pOnce, 2, 0))
			{
				func();
				(*pOnce) = 1;
				return 0;
			}
		}
		YieldProcessor();
		_ReadWriteBarrier();
		state = (*pOnce);
	}
	return 0;
}

int pthread_mutex_lock(pthread_mutex_t* mutex)
{
	EnterCriticalSection(mutex);
	return 0;
}

int pthread_mutex_unlock(pthread_mutex_t* mutex)
{
	LeaveCriticalSection(mutex);
	return 0;
}

int pthread_mutex_trylock(pthread_mutex_t* mutex)
{
	return TryEnterCriticalSection(mutex) ? 0 : EBUSY;
}

int pthread_mutex_init(pthread_mutex_t* mutex, pthread_mutexattr_t* /*attr*/)
{
	InitializeCriticalSection(mutex);
	return 0;
}

int pthread_mutex_destroy(pthread_mutex_t* mutex)
{
	DeleteCriticalSection(mutex);
	return 0;
}

#define pthread_mutex_getprioceiling(M, P) ENOTSUP
#define pthread_mutex_setprioceiling(M, P) ENOTSUP

int pthread_equal(pthread_t t1, pthread_t t2)
{
	return (t1 == t2);
}

void pthread_testcancel(void);

int pthread_rwlock_init(pthread_rwlock_t* lock, pthread_rwlockattr_t* /*attr*/)
{
	InitializeSRWLock(lock);
	return 0;
}

int pthread_rwlock_destroy(pthread_rwlock_t* /*lock*/)
{
	return 0;
}

int pthread_rwlock_rdlock(pthread_rwlock_t* lock)
{
	pthread_testcancel();
	AcquireSRWLockShared(lock);
	return 0;
}

int pthread_rwlock_wrlock(pthread_rwlock_t* lock)
{
	pthread_testcancel();
	AcquireSRWLockExclusive(lock);
	return 0;
}

void pthread_tls_init(void)
{
	_pthread_tls = TlsAlloc();

	// Cannot continue if out of indexes
	if (_pthread_tls == TLS_OUT_OF_INDEXES)
		abort();
}

pthread_t pthread_self(void)
{
	_pthread_once_raw(&_pthread_tls_once, pthread_tls_init);

	pthread_t pThread = (pthread_t)TlsGetValue(_pthread_tls);

	// Main thread
	if (pThread == nullptr)
	{
		pThread = (struct _pthread_v*)malloc(sizeof(struct _pthread_v));

		// If cannot initialize main thread, then the only thing we can do is abort
		if (pThread == nullptr)
			abort();

		pThread->ret_arg = NULL;
		pThread->func = NULL;
		pThread->clean = NULL;
		pThread->cancelled = false;
		pThread->p_state = PTHREAD_DEFAULT_ATTR;
		pThread->keymax = 0;
		pThread->keyval = NULL;
		pThread->h = GetCurrentThread();
		pThread->id = GetCurrentThreadId();

		// Save for later
		TlsSetValue(_pthread_tls, pThread);

		if (setjmp(pThread->jb))
		{
			// Make sure we free ourselves if we are detached
			if (pThread->h == nullptr)
				free(pThread);
			_endthreadex(0); // Time to die
		}
	}
	return pThread;
}

int pthread_rwlock_unlock(pthread_rwlock_t* lock)
{
	void* pState = *(void**)lock;
	if (pState == (void *)1)
	{
		ReleaseSRWLockExclusive(lock);
	}
	else
	{
		ReleaseSRWLockShared(lock);
	}
	return 0;
}

int pthread_rwlock_tryrdlock(pthread_rwlock_t* lock)
{
	// Get the current state of the lock
	void* pState = *(void**)lock;

	if (pState == nullptr)
	{
		// Unlocked to locked
		if (!_InterlockedCompareExchangePointer((volatile PVOID*)lock, (void *)0x11, NULL))
			return 0;
		return EBUSY;
	}

	// A single writer exists
	if (pState == (void*)1)
		return EBUSY;

	// Multiple writers exist?
	if ((uintptr_t)pState & 14)
		return EBUSY;

	if (_InterlockedCompareExchangePointer((volatile PVOID*)lock, (void*)((uintptr_t)pState + 16), pState) == pState)
		return 0;
	return EBUSY;
}

int pthread_rwlock_trywrlock(pthread_rwlock_t* lock)
{
	// Try to grab lock if it has no users
	if (!_InterlockedCompareExchangePointer((volatile PVOID*)lock, (void *)1, NULL))
		return 0;
	return EBUSY;
}


void _pthread_cleanup_dest(pthread_t t)
{
	for (int j = 0; j < PTHREAD_DESTRUCTOR_ITERATIONS; j++)
	{
		int nFlag = 0;
		for (int i = 0; i < t->keymax; i++)
		{
			void* pVal = t->keyval[i];
			if (pVal != nullptr)
			{
				pthread_rwlock_rdlock(&_pthread_key_lock);
				if ((uintptr_t)_pthread_key_dest[i] > 1)
				{
					// Call destructor
					t->keyval[i] = NULL;
					_pthread_key_dest[i](pVal);
					nFlag = 1;
				}
				pthread_rwlock_unlock(&_pthread_key_lock);
			}
		}
		if (nFlag == 0)
			return;
	}
}


unsigned long long _pthread_time_in_ms(void)
{
	timespec ts;
	timespec_get(&ts, TIME_UTC);

	unsigned long long tm = _pthread_time_in_ms_from_timespec(&ts);
	return tm;
}

unsigned long long _pthread_time_in_ms_from_timespec(const struct timespec* ts)
{
	unsigned long long nRetval = (ts->tv_sec * (unsigned long long)1000) + (ts->tv_nsec / 1000000);
	return nRetval;
}

unsigned long long _pthread_rel_time_in_ms(const struct timespec* ts)
{
	unsigned long long t1 = _pthread_time_in_ms_from_timespec(ts);
	unsigned long long t2 = _pthread_time_in_ms();

	// Prevent underflow
	if (t1 < t2)
		return 0;
	return (t1 - t2);
}

int pthread_rwlock_timedrdlock(pthread_rwlock_t* lock, const struct timespec* ts)
{
	unsigned long long ct = _pthread_time_in_ms();
	unsigned long long t = _pthread_time_in_ms_from_timespec(ts);

	pthread_testcancel();

	// Use a busy-loop
	while (1)
	{
		// Try to grab lock
		if (!pthread_rwlock_tryrdlock(lock))
			return 0;

		// Get current time
		ct = _pthread_time_in_ms();

		// Have we waited long enough?
		if (ct > t)
			return ETIMEDOUT;
	}
}

int pthread_rwlock_timedwrlock(pthread_rwlock_t* lock, const struct timespec* ts)
{
	unsigned long long ct = _pthread_time_in_ms();
	unsigned long long t = _pthread_time_in_ms_from_timespec(ts);

	pthread_testcancel();

	// Use a busy-loop
	while (1)
	{
		// Try to grab lock
		if (!pthread_rwlock_trywrlock(lock))
			return 0;

		// Get current time
		ct = _pthread_time_in_ms();

		// Have we waited long enough?
		if (ct > t)
			return ETIMEDOUT;
	}
}

int pthread_get_concurrency(int* val)
{
	(*val) = _pthread_concur;
	return 0;
}

int pthread_set_concurrency(int val)
{
	_pthread_concur = val;
	return 0;
}

#define pthread_getschedparam(T, P, S) ENOTSUP
#define pthread_setschedparam(T, P, S) ENOTSUP
#define pthread_getcpuclockid(T, C) ENOTSUP

int pthread_exit(void* pRes)
{
	pthread_t t = pthread_self();
	t->ret_arg = pRes;
	_pthread_cleanup_dest(t);
	longjmp(t->jb, 1);
}


void _pthread_invoke_cancel(void)
{
	_InterlockedDecrement(&_pthread_cancelling);

	// Call cancel queue
	for (_pthread_cleanup* pcup = pthread_self()->clean; pcup; pcup = pcup->next)
	{
		pcup->func(pcup->arg);
	}
	pthread_exit(PTHREAD_CANCELED);
}

void pthread_testcancel(void)
{
	if (_pthread_cancelling)
	{
		pthread_t t = pthread_self();
		if ((t->cancelled == true) && (t->p_state & PTHREAD_CANCEL_ENABLE))
		{
			_pthread_invoke_cancel();
		}
	}
}


int pthread_cancel(pthread_t t)
{
	if (t->p_state & PTHREAD_CANCEL_ASYNCHRONOUS)
	{
		// Dangerous asynchronous cancelling
		CONTEXT ctxt;

		// Already done?
		if (t->cancelled == true)
			return ESRCH;

		ctxt.ContextFlags = CONTEXT_CONTROL;

		SuspendThread(t->h);
		GetThreadContext(t->h, &ctxt);
#ifdef _M_X64
		ctxt.Rip = (uintptr_t)_pthread_invoke_cancel;
#else
		ctxt.Eip = (uintptr_t)_pthread_invoke_cancel;
#endif
		SetThreadContext(t->h, &ctxt);

		// Also try deferred Cancelling
		t->cancelled = true;

		// Notify everyone to look
		_InterlockedIncrement(&_pthread_cancelling);

		ResumeThread(t->h);
	}
	else
	{
		// Safe deferred Cancelling
		t->cancelled = true;

		// Notify everyone to look
		_InterlockedIncrement(&_pthread_cancelling);
	}
	return 0;
}

unsigned _pthread_get_state(pthread_attr_t* attr, unsigned flag)
{
	return attr->p_state & flag;
}

int _pthread_set_state(pthread_attr_t* attr, unsigned flag, unsigned val)
{
	if (~flag & val)
		return EINVAL;
	attr->p_state &= ~flag;
	attr->p_state |= val;

	return 0;
}

int pthread_attr_init(pthread_attr_t* attr)
{
	attr->p_state = PTHREAD_DEFAULT_ATTR;
	attr->stack = NULL;
	attr->s_size = 0;
	return 0;
}

int pthread_attr_destroy(pthread_attr_t* /*attr*/)
{
	return 0;
}

int pthread_attr_setdetachstate(pthread_attr_t* attr, int flag)
{
	return _pthread_set_state(attr, PTHREAD_CREATE_DETACHED, flag);
}

int pthread_attr_getdetachstate(pthread_attr_t* attr, int* flag)
{
	*flag = _pthread_get_state(attr, PTHREAD_CREATE_DETACHED);
	return 0;
}

int pthread_attr_setinheritsched(pthread_attr_t* attr, int flag)
{
	return _pthread_set_state(attr, PTHREAD_INHERIT_SCHED, flag);
}

int pthread_attr_getinheritsched(pthread_attr_t* attr, int* flag)
{
	*flag = _pthread_get_state(attr, PTHREAD_INHERIT_SCHED);
	return 0;
}

int pthread_attr_setscope(pthread_attr_t* attr, int flag)
{
	return _pthread_set_state(attr, PTHREAD_SCOPE_SYSTEM, flag);
}

int pthread_attr_getscope(pthread_attr_t* attr, int *flag)
{
	*flag = _pthread_get_state(attr, PTHREAD_SCOPE_SYSTEM);
	return 0;
}

int pthread_attr_getstackaddr(pthread_attr_t* attr, void** stack)
{
	(*stack) = attr->stack;
	return 0;
}

int pthread_attr_setstackaddr(pthread_attr_t* attr, void* stack)
{
	attr->stack = stack;
	return 0;
}

int pthread_attr_getstacksize(pthread_attr_t* attr, size_t* size)
{
	(*size) = attr->s_size;
	return 0;
}

int pthread_attr_setstacksize(pthread_attr_t* attr, size_t size)
{
	attr->s_size = size;
	return 0;
}

#define pthread_attr_getguardsize(A, S) ENOTSUP
#define pthread_attr_setgaurdsize(A, S) ENOTSUP
#define pthread_attr_getschedparam(A, S) ENOTSUP
#define pthread_attr_setschedparam(A, S) ENOTSUP
#define pthread_attr_getschedpolicy(A, S) ENOTSUP
#define pthread_attr_setschedpolicy(A, S) ENOTSUP

int pthread_setcancelstate(int state, int* oldstate)
{
	pthread_t t = pthread_self();

	if ((state & PTHREAD_CANCEL_ENABLE) != state)
		return EINVAL;
	if (oldstate != nullptr)
		*oldstate = t->p_state & PTHREAD_CANCEL_ENABLE;
	t->p_state &= ~PTHREAD_CANCEL_ENABLE;
	t->p_state |= state;
	return 0;
}

int pthread_setcanceltype(int type, int* oldtype)
{
	pthread_t t = pthread_self();

	if ((type & PTHREAD_CANCEL_ASYNCHRONOUS) != type)
		return EINVAL;
	if (oldtype != nullptr)
		*oldtype = t->p_state & PTHREAD_CANCEL_ASYNCHRONOUS;
	t->p_state &= ~PTHREAD_CANCEL_ASYNCHRONOUS;
	t->p_state |= type;
	return 0;
}

unsigned int __stdcall pthread_create_wrapper(void *args)
{
	struct _pthread_v* tv = (struct _pthread_v*)args;
	tv->id = GetCurrentThreadId();

	_pthread_once_raw(&_pthread_tls_once, pthread_tls_init);

	TlsSetValue(_pthread_tls, tv);

	if (!setjmp(tv->jb))
	{
		// Call function and save return value
		tv->ret_arg = tv->func(tv->ret_arg);

		// Clean up destructors
		_pthread_cleanup_dest(tv);
	}

	// If we exit too early, then we can race with create
	while (tv->h == (HANDLE)-1)
	{
		YieldProcessor();
		_ReadWriteBarrier();
	}

	// Make sure we free ourselves if we are detached
	if (!tv->h)
		free(tv);
	return 0;
}

int pthread_create(pthread_t *th, pthread_attr_t *attr, void *(*func)(void *), void *arg)
{
	struct _pthread_v* tv = (struct _pthread_v*)malloc(sizeof(struct _pthread_v));
	unsigned nSize = 0;

	if (!tv)
		return 1;

	*th = tv;

	// Save data in pthread_t
	tv->ret_arg = arg;
	tv->func = func;
	tv->clean = NULL;
	tv->cancelled = false;
	tv->p_state = PTHREAD_DEFAULT_ATTR;
	tv->keymax = 0;
	tv->keyval = NULL;
	tv->h = (HANDLE)-1;
	tv->id = -1;
	if (attr)
	{
		tv->p_state = attr->p_state;
		nSize = attr->s_size;
	}

	// Make sure tv->h has value of -1
	_ReadWriteBarrier();

	tv->h = (HANDLE)_beginthreadex(NULL, nSize, pthread_create_wrapper, tv, 0, NULL);

	// Failed
	if (!tv->h)
		return 1;

	if (tv->p_state & PTHREAD_CREATE_DETACHED)
	{
		CloseHandle(tv->h);
		_ReadWriteBarrier();
		tv->h = 0;
	}
	return 0;
}

int pthread_join(pthread_t t, void** res)
{
	struct _pthread_v* tv = t;

	pthread_testcancel();

	WaitForSingleObject(tv->h, INFINITE);
	CloseHandle(tv->h);

	// Obtain return value
	if (res)
		*res = tv->ret_arg;
	free(tv);
	return 0;
}

int pthread_detach(pthread_t t)
{
	struct _pthread_v* tv = t;

	// This can't race with thread exit because
	// our call would be undefined if called on a dead thread.
	CloseHandle(tv->h);
	_ReadWriteBarrier();
	tv->h = 0;

	return 0;
}

int pthread_mutexattr_init(pthread_mutexattr_t* attr)
{
	(*attr) = 0;
	return 0;
}

int pthread_mutexattr_destroy(pthread_mutexattr_t* attr)
{
	(*attr) = 0;
	return 0;
}

int pthread_mutexattr_gettype(pthread_mutexattr_t* attr, int *type)
{
	*type = *attr & 3;
	return 0;
}

int pthread_mutexattr_settype(pthread_mutexattr_t* attr, int type)
{
	if ((unsigned)type > 3)
		return EINVAL;
	*attr &= ~3;
	*attr |= type;
	return 0;
}

int pthread_mutexattr_getpshared(pthread_mutexattr_t* attr, int *type)
{
	*type = *attr & 4;
	return 0;
}

int pthread_mutexattr_setpshared(pthread_mutexattr_t* attr, int type)
{
	if ((type & 4) != type) return EINVAL;

	*attr &= ~4;
	*attr |= type;
	return 0;
}

int pthread_mutexattr_getprotocol(pthread_mutexattr_t* attr, int *type)
{
	*type = *attr & (8 + 16);
	return 0;
}

int pthread_mutexattr_setprotocol(pthread_mutexattr_t* attr, int type)
{
	if ((type & (8 + 16)) != 8 + 16)
		return EINVAL;

	*attr &= ~(8 + 16);
	*attr |= type;
	return 0;
}

int pthread_mutexattr_getprioceiling(pthread_mutexattr_t* attr, int * prio)
{
	*prio = *attr / PTHREAD_PRIO_MULT;
	return 0;
}

int pthread_mutexattr_setprioceiling(pthread_mutexattr_t* attr, int prio)
{
	*attr &= (PTHREAD_PRIO_MULT - 1);
	*attr += prio * PTHREAD_PRIO_MULT;
	return 0;
}

int pthread_mutex_timedlock(pthread_mutex_t *m, struct timespec *ts)
{
	struct _pthread_crit_t
	{
		void *debug;
		LONG count;
		LONG r_count;
		HANDLE owner;
		HANDLE sem;
		ULONG_PTR spin;
	};

	// Try to lock it without waiting
	if (!pthread_mutex_trylock(m))
		return 0;

	unsigned long long ct = _pthread_time_in_ms();
	unsigned long long t = _pthread_time_in_ms_from_timespec(ts);

	while (1)
	{
		// Have we waited long enough?
		if (ct > t)
			return ETIMEDOUT;

		// Wait on semaphore within critical section
		WaitForSingleObject(((struct _pthread_crit_t *)m)->sem, t - ct);

		// Try to grab lock
		if (!pthread_mutex_trylock(m))
			return 0;

		// Get current time
		ct = _pthread_time_in_ms();
	}
}

#define _PTHREAD_BARRIER_FLAG (1<<30)

int pthread_barrier_destroy(pthread_barrier_t *b)
{
	EnterCriticalSection(&b->m);

	while (b->total > _PTHREAD_BARRIER_FLAG)
	{
		/* Wait until everyone exits the barrier */
		SleepConditionVariableCS(&b->cv, &b->m, INFINITE);
	}

	LeaveCriticalSection(&b->m);
	DeleteCriticalSection(&b->m);
	return 0;
}

int pthread_barrier_init(pthread_barrier_t *b, void* /*attr*/, int count)
{
	b->count = count;
	b->total = 0;

	InitializeCriticalSection(&b->m);
	InitializeConditionVariable(&b->cv);

	return 0;
}

int pthread_barrier_wait(pthread_barrier_t *b)
{
	EnterCriticalSection(&b->m);

	while (b->total > _PTHREAD_BARRIER_FLAG)
	{
		/* Wait until everyone exits the barrier */
		SleepConditionVariableCS(&b->cv, &b->m, INFINITE);
	}

	/* Are we the first to enter? */
	if (b->total == _PTHREAD_BARRIER_FLAG) b->total = 0;

	b->total++;

	if (b->total == b->count)
	{
		b->total += _PTHREAD_BARRIER_FLAG - 1;
		WakeAllConditionVariable(&b->cv);

		LeaveCriticalSection(&b->m);

		return 1;
	}
	else
	{
		while (b->total < _PTHREAD_BARRIER_FLAG)
		{
			// Wait until enough threads enter the barrier
			SleepConditionVariableCS(&b->cv, &b->m, INFINITE);
		}

		b->total--;

		/* Get entering threads to wake up */
		if (b->total == _PTHREAD_BARRIER_FLAG)
			WakeAllConditionVariable(&b->cv);

		LeaveCriticalSection(&b->m);
		return 0;
	}
}

int pthread_barrierattr_init(void **attr)
{
	*attr = NULL;
	return 0;
}

int pthread_barrierattr_destroy(void** /*attr*/)
{
	return 0;
}

int pthread_barrierattr_setpshared(void **attr, int s)
{
	*attr = (void *)s;
	return 0;
}

int pthread_barrierattr_getpshared(void **attr, int *s)
{
	*s = (int)(size_t)*attr;
	return 0;
}

typedef void(*PTHREAD_KEY)(void *);
int pthread_key_create(pthread_key_t *key, void(*dest)(void *))
{
	PTHREAD_KEY* d;

	if (!key)
		return EINVAL;

	pthread_rwlock_wrlock(&_pthread_key_lock);

	for (int i = _pthread_key_sch; i < _pthread_key_max; i++)
	{
		if (!_pthread_key_dest[i])
		{
			*key = i;
			if (dest)
			{
				_pthread_key_dest[i] = dest;
			}
			else
			{
				_pthread_key_dest[i] = (void(*)(void *))1;
			}
			pthread_rwlock_unlock(&_pthread_key_lock);
			return 0;
		}
	}

	for (int i = 0; i < _pthread_key_sch; i++)
	{
		if (!_pthread_key_dest[i])
		{
			*key = i;
			if (dest)
			{
				_pthread_key_dest[i] = dest;
			}
			else
			{
				_pthread_key_dest[i] = (void(*)(void *))1;
			}
			pthread_rwlock_unlock(&_pthread_key_lock);

			return 0;
		}
	}

	if (!_pthread_key_max)
		_pthread_key_max = 1;
	if (_pthread_key_max == PTHREAD_KEYS_MAX)
	{
		pthread_rwlock_unlock(&_pthread_key_lock);

		return ENOMEM;
	}

	long nmax = _pthread_key_max * 2;
	if (nmax > PTHREAD_KEYS_MAX) nmax = PTHREAD_KEYS_MAX;

	// No spare room anywhere
	d = (PTHREAD_KEY*)realloc(_pthread_key_dest, nmax * sizeof(*d));
	if (!d)
	{
		pthread_rwlock_unlock(&_pthread_key_lock);

		return ENOMEM;
	}

	// Clear new region
	memset((void *)&d[_pthread_key_max], 0, (nmax - _pthread_key_max) * sizeof(void *));

	// Use new region
	_pthread_key_dest = d;
	_pthread_key_sch = _pthread_key_max + 1;
	*key = _pthread_key_max;
	_pthread_key_max = nmax;

	if (dest)
	{
		_pthread_key_dest[*key] = dest;
	}
	else
	{
		_pthread_key_dest[*key] = (void(*)(void *))1;
	}

	pthread_rwlock_unlock(&_pthread_key_lock);
	return 0;
}

int pthread_key_delete(pthread_key_t key)
{
	if (key > _pthread_key_max)
		return EINVAL;
	if (!_pthread_key_dest)
		return EINVAL;

	pthread_rwlock_wrlock(&_pthread_key_lock);
	_pthread_key_dest[key] = NULL;

	// Start next search from our location
	if (_pthread_key_sch > key)
		_pthread_key_sch = key;

	pthread_rwlock_unlock(&_pthread_key_lock);

	return 0;
}

void* pthread_getspecific(pthread_key_t key)
{
	pthread_t t = pthread_self();

	if (key >= t->keymax)
		return NULL;
	return t->keyval[key];
}

int pthread_setspecific(pthread_key_t key, const void *value)
{
	pthread_t t = pthread_self();

	if (key > t->keymax)
	{
		int keymax = (key + 1) * 2;
		void **kv = (void**)realloc(t->keyval, keymax * sizeof(void *));

		if (!kv)
			return ENOMEM;

		/* Clear new region */
		memset(&kv[t->keymax], 0, (keymax - t->keymax) * sizeof(void*));

		t->keyval = kv;
		t->keymax = keymax;
	}

	t->keyval[key] = (void *)value;
	return 0;
}


int pthread_spin_init(pthread_spinlock_t *l, int /*pshared*/)
{
	(*l) = 0;
	return 0;
}

int pthread_spin_destroy(pthread_spinlock_t *l)
{
	(void)l;
	return 0;
}

// No-fair spinlock due to lack of knowledge of thread number
int pthread_spin_lock(pthread_spinlock_t *l)
{
	while (_InterlockedExchange(l, EBUSY))
	{
		// Don't lock the bus whilst waiting
		while (*l)
		{
			YieldProcessor();

			// Compiler barrier.  Prevent caching of *l
			_ReadWriteBarrier();
		}
	}
	return 0;
}

int pthread_spin_trylock(pthread_spinlock_t *l)
{
	return _InterlockedExchange(l, EBUSY);
}

int pthread_spin_unlock(pthread_spinlock_t *l)
{
	// Compiler barrier.  The store below acts with release symmantics
	_ReadWriteBarrier();
	(*l) = 0;
	return 0;
}

int pthread_cond_init(pthread_cond_t* cond, pthread_condattr_t* /*attr*/)
{
	InitializeConditionVariable(cond);
	return 0;
}

int pthread_cond_signal(pthread_cond_t* cond)
{
	WakeConditionVariable(cond);
	return 0;
}

int pthread_cond_broadcast(pthread_cond_t* cond)
{
	WakeAllConditionVariable(cond);
	return 0;
}

int pthread_cond_wait(pthread_cond_t* cond, pthread_mutex_t* mutex)
{
	pthread_testcancel();
	SleepConditionVariableCS(cond, mutex, INFINITE);
	return 0;
}

int pthread_cond_destroy(pthread_cond_t* /*cond*/)
{
	return 0;
}

int pthread_cond_timedwait(pthread_cond_t* cond, pthread_mutex_t* mutex, struct timespec* time)
{
	unsigned long long tm = _pthread_rel_time_in_ms(time);

	pthread_testcancel();

	if (SleepConditionVariableCS(cond, mutex, tm) == FALSE)
		return ETIMEDOUT;

	// We can have a spurious wakeup after the timeout
	if (_pthread_rel_time_in_ms(time) == 0)
		return ETIMEDOUT;
	return 0;
}

int pthread_condattr_destroy(pthread_condattr_t* /*attr*/)
{
	return 0;
}

#define pthread_condattr_getclock(A, C) ENOTSUP
#define pthread_condattr_setclock(A, C) ENOTSUP

int pthread_condattr_init(pthread_condattr_t* attr)
{
	(*attr) = 0;
	return 0;
}

int pthread_condattr_getpshared(pthread_condattr_t* attr, int* s)
{
	*s = (*attr);
	return 0;
}

int pthread_condattr_setpshared(pthread_condattr_t* attr, int s)
{
	(*attr) = s;
	return 0;
}

int pthread_rwlockattr_destroy(pthread_rwlockattr_t* /*attr*/)
{
	return 0;
}

int pthread_rwlockattr_init(pthread_rwlockattr_t* attr)
{
	(*attr) = 0;
	return 0;
}

int pthread_rwlockattr_getpshared(pthread_rwlockattr_t* attr, int* s)
{
	*s = (*attr);
	return 0;
}

int pthread_rwlockattr_setpshared(pthread_rwlockattr_t* attr, int s)
{
	(*attr) = s;
	return 0;
}


// No fork() in windows - so ignore this
#define pthread_atfork(F1,F2,F3) 0

// Windows has rudimentary signals support
#define pthread_kill(T, S) 0
#define pthread_sigmask(H, S1, S2) 0

#if 0
/* Wrap cancellation points */
#define accept(...) (pthread_testcancel(), accept(__VA_ARGS__))
#define aio_suspend(...) (pthread_testcancel(), aio_suspend(__VA_ARGS__))
#define clock_nanosleep(...) (pthread_testcancel(), clock_nanosleep(__VA_ARGS__))
#define close(...) (pthread_testcancel(), close(__VA_ARGS__))
#define connect(...) (pthread_testcancel(), connect(__VA_ARGS__))
#define creat(...) (pthread_testcancel(), creat(__VA_ARGS__))
#define fcntl(...) (pthread_testcancel(), fcntl(__VA_ARGS__))
#define fdatasync(...) (pthread_testcancel(), fdatasync(__VA_ARGS__))
#define fsync(...) (pthread_testcancel(), fsync(__VA_ARGS__))
#define getmsg(...) (pthread_testcancel(), getmsg(__VA_ARGS__))
#define getpmsg(...) (pthread_testcancel(), getpmsg(__VA_ARGS__))
#define lockf(...) (pthread_testcancel(), lockf(__VA_ARGS__))
#define mg_receive(...) (pthread_testcancel(), mg_receive(__VA_ARGS__))
#define mg_send(...) (pthread_testcancel(), mg_send(__VA_ARGS__))
#define mg_timedreceive(...) (pthread_testcancel(), mg_timedreceive(__VA_ARGS__))
#define mg_timessend(...) (pthread_testcancel(), mg_timedsend(__VA_ARGS__))
#define msgrcv(...) (pthread_testcancel(), msgrecv(__VA_ARGS__))
#define msgsnd(...) (pthread_testcancel(), msgsnd(__VA_ARGS__))
#define msync(...) (pthread_testcancel(), msync(__VA_ARGS__))
#define nanosleep(...) (pthread_testcancel(), nanosleep(__VA_ARGS__))
#define open(...) (pthread_testcancel(), open(__VA_ARGS__))
#define pause(...) (pthread_testcancel(), pause(__VA_ARGS__))
#define poll(...) (pthread_testcancel(), poll(__VA_ARGS__))
#define pread(...) (pthread_testcancel(), pread(__VA_ARGS__))
#define pselect(...) (pthread_testcancel(), pselect(__VA_ARGS__))
#define putmsg(...) (pthread_testcancel(), putmsg(__VA_ARGS__))
#define putpmsg(...) (pthread_testcancel(), putpmsg(__VA_ARGS__))
#define pwrite(...) (pthread_testcancel(), pwrite(__VA_ARGS__))
#define read(...) (pthread_testcancel(), read(__VA_ARGS__))
#define readv(...) (pthread_testcancel(), readv(__VA_ARGS__))
#define recv(...) (pthread_testcancel(), recv(__VA_ARGS__))
#define recvfrom(...) (pthread_testcancel(), recvfrom(__VA_ARGS__))
#define recvmsg(...) (pthread_testcancel(), recvmsg(__VA_ARGS__))
#define select(...) (pthread_testcancel(), select(__VA_ARGS__))
#define sem_timedwait(...) (pthread_testcancel(), sem_timedwait(__VA_ARGS__))
#define sem_wait(...) (pthread_testcancel(), sem_wait(__VA_ARGS__))
#define send(...) (pthread_testcancel(), send(__VA_ARGS__))
#define sendmsg(...) (pthread_testcancel(), sendmsg(__VA_ARGS__))
#define sendto(...) (pthread_testcancel(), sendto(__VA_ARGS__))
#define sigpause(...) (pthread_testcancel(), sigpause(__VA_ARGS__))
#define sigsuspend(...) (pthread_testcancel(), sigsuspend(__VA_ARGS__))
#define sigwait(...) (pthread_testcancel(), sigwait(__VA_ARGS__))
#define sigwaitinfo(...) (pthread_testcancel(), sigwaitinfo(__VA_ARGS__))
#define sleep(...) (pthread_testcancel(), sleep(__VA_ARGS__))
//#define Sleep(...) (pthread_testcancel(), Sleep(__VA_ARGS__))
#define system(...) (pthread_testcancel(), system(__VA_ARGS__))
#define access(...) (pthread_testcancel(), access(__VA_ARGS__))
#define asctime(...) (pthread_testcancel(), asctime(__VA_ARGS__))
#define asctime_r(...) (pthread_testcancel(), asctime_r(__VA_ARGS__))
#define catclose(...) (pthread_testcancel(), catclose(__VA_ARGS__))
#define catgets(...) (pthread_testcancel(), catgets(__VA_ARGS__))
#define catopen(...) (pthread_testcancel(), catopen(__VA_ARGS__))
#define closedir(...) (pthread_testcancel(), closedir(__VA_ARGS__))
#define closelog(...) (pthread_testcancel(), closelog(__VA_ARGS__))
#define ctermid(...) (pthread_testcancel(), ctermid(__VA_ARGS__))
#define ctime(...) (pthread_testcancel(), ctime(__VA_ARGS__))
#define ctime_r(...) (pthread_testcancel(), ctime_r(__VA_ARGS__))
#define dbm_close(...) (pthread_testcancel(), dbm_close(__VA_ARGS__))
#define dbm_delete(...) (pthread_testcancel(), dbm_delete(__VA_ARGS__))
#define dbm_fetch(...) (pthread_testcancel(), dbm_fetch(__VA_ARGS__))
#define dbm_nextkey(...) (pthread_testcancel(), dbm_nextkey(__VA_ARGS__))
#define dbm_open(...) (pthread_testcancel(), dbm_open(__VA_ARGS__))
#define dbm_store(...) (pthread_testcancel(), dbm_store(__VA_ARGS__))
#define dlclose(...) (pthread_testcancel(), dlclose(__VA_ARGS__))
#define dlopen(...) (pthread_testcancel(), dlopen(__VA_ARGS__))
#define endgrent(...) (pthread_testcancel(), endgrent(__VA_ARGS__))
#define endhostent(...) (pthread_testcancel(), endhostent(__VA_ARGS__))
#define endnetent(...) (pthread_testcancel(), endnetent(__VA_ARGS__))
#define endprotoent(...) (pthread_testcancel(), endprotoend(__VA_ARGS__))
#define endpwent(...) (pthread_testcancel(), endpwent(__VA_ARGS__))
#define endservent(...) (pthread_testcancel(), endservent(__VA_ARGS__))
#define endutxent(...) (pthread_testcancel(), endutxent(__VA_ARGS__))
#define fclose(...) (pthread_testcancel(), fclose(__VA_ARGS__))
#define fflush(...) (pthread_testcancel(), fflush(__VA_ARGS__))
#define fgetc(...) (pthread_testcancel(), fgetc(__VA_ARGS__))
#define fgetpos(...) (pthread_testcancel(), fgetpos(__VA_ARGS__))
#define fgets(...) (pthread_testcancel(), fgets(__VA_ARGS__))
#define fgetwc(...) (pthread_testcancel(), fgetwc(__VA_ARGS__))
#define fgetws(...) (pthread_testcancel(), fgetws(__VA_ARGS__))
#define fmtmsg(...) (pthread_testcancel(), fmtmsg(__VA_ARGS__))
#define fopen(...) (pthread_testcancel(), fopen(__VA_ARGS__))
#define fpathconf(...) (pthread_testcancel(), fpathconf(__VA_ARGS__))
#define fprintf(...) (pthread_testcancel(), fprintf(__VA_ARGS__))
#define fputc(...) (pthread_testcancel(), fputc(__VA_ARGS__))
#define fputs(...) (pthread_testcancel(), fputs(__VA_ARGS__))
#define fputwc(...) (pthread_testcancel(), fputwc(__VA_ARGS__))
#define fputws(...) (pthread_testcancel(), fputws(__VA_ARGS__))
#define fread(...) (pthread_testcancel(), fread(__VA_ARGS__))
#define freopen(...) (pthread_testcancel(), freopen(__VA_ARGS__))
#define fscanf(...) (pthread_testcancel(), fscanf(__VA_ARGS__))
#define fseek(...) (pthread_testcancel(), fseek(__VA_ARGS__))
#define fseeko(...) (pthread_testcancel(), fseeko(__VA_ARGS__))
#define fsetpos(...) (pthread_testcancel(), fsetpos(__VA_ARGS__))
#define fstat(...) (pthread_testcancel(), fstat(__VA_ARGS__))
#define ftell(...) (pthread_testcancel(), ftell(__VA_ARGS__))
#define ftello(...) (pthread_testcancel(), ftello(__VA_ARGS__))
#define ftw(...) (pthread_testcancel(), ftw(__VA_ARGS__))
#define fwprintf(...) (pthread_testcancel(), fwprintf(__VA_ARGS__))
#define fwrite(...) (pthread_testcancel(), fwrite(__VA_ARGS__))
#define fwscanf(...) (pthread_testcancel(), fwscanf(__VA_ARGS__))
#define getaddrinfo(...) (pthread_testcancel(), getaddrinfo(__VA_ARGS__))
#define getc(...) (pthread_testcancel(), getc(__VA_ARGS__))
#define getc_unlocked(...) (pthread_testcancel(), getc_unlocked(__VA_ARGS__))
#define getchar(...) (pthread_testcancel(), getchar(__VA_ARGS__))
#define getchar_unlocked(...) (pthread_testcancel(), getchar_unlocked(__VA_ARGS__))
#define getcwd(...) (pthread_testcancel(), getcwd(__VA_ARGS__))
#define getdate(...) (pthread_testcancel(), getdate(__VA_ARGS__))
#define getgrent(...) (pthread_testcancel(), getgrent(__VA_ARGS__))
#define getgrgid(...) (pthread_testcancel(), getgrgid(__VA_ARGS__))
#define getgrgid_r(...) (pthread_testcancel(), getgrgid_r(__VA_ARGS__))
#define gergrnam(...) (pthread_testcancel(), getgrnam(__VA_ARGS__))
#define getgrnam_r(...) (pthread_testcancel(), getgrnam_r(__VA_ARGS__))
#define gethostbyaddr(...) (pthread_testcancel(), gethostbyaddr(__VA_ARGS__))
#define gethostbyname(...) (pthread_testcancel(), gethostbyname(__VA_ARGS__))
#define gethostent(...) (pthread_testcancel(), gethostent(__VA_ARGS__))
#define gethostid(...) (pthread_testcancel(), gethostid(__VA_ARGS__))
#define gethostname(...) (pthread_testcancel(), gethostname(__VA_ARGS__))
#define getlogin(...) (pthread_testcancel(), getlogin(__VA_ARGS__))
#define getlogin_r(...) (pthread_testcancel(), getlogin_r(__VA_ARGS__))
#define getnameinfo(...) (pthread_testcancel(), getnameinfo(__VA_ARGS__))
#define getnetbyaddr(...) (pthread_testcancel(), getnetbyaddr(__VA_ARGS__))
#define getnetbyname(...) (pthread_testcancel(), getnetbyname(__VA_ARGS__))
#define getnetent(...) (pthread_testcancel(), getnetent(__VA_ARGS__))
#define getopt(...) (pthread_testcancel(), getopt(__VA_ARGS__))
#define getprotobyname(...) (pthread_testcancel(), getprotobyname(__VA_ARGS__))
#define getprotobynumber(...) (pthread_testcancel(), getprotobynumber(__VA_ARGS__))
#define getprotoent(...) (pthread_testcancel(), getprotoent(__VA_ARGS__))
#define getpwent(...) (pthread_testcancel(), getpwent(__VA_ARGS__))
#define getpwnam(...) (pthread_testcancel(), getpwnam(__VA_ARGS__))
#define getpwnam_r(...) (pthread_testcancel(), getpwnam_r(__VA_ARGS__))
#define getpwuid(...) (pthread_testcancel(), getpwuid(__VA_ARGS__))
#define getpwuid_r(...) (pthread_testcancel(), getpwuid_r(__VA_ARGS__))
#define gets(...) (pthread_testcancel(), gets(__VA_ARGS__))
#define getservbyname(...) (pthread_testcancel(), getservbyname(__VA_ARGS__))
#define getservbyport(...) (pthread_testcancel(), getservbyport(__VA_ARGS__))
#define getservent(...) (pthread_testcancel(), getservent(__VA_ARGS__))
#define getutxent(...) (pthread_testcancel(), getutxent(__VA_ARGS__))
#define getutxid(...) (pthread_testcancel(), getutxid(__VA_ARGS__))
#define getutxline(...) (pthread_testcancel(), getutxline(__VA_ARGS__))
#undef getwc
#define getwc(...) (pthread_testcancel(), getwc(__VA_ARGS__))
#undef getwchar
#define getwchar(...) (pthread_testcancel(), getwchar(__VA_ARGS__))
#define getwd(...) (pthread_testcancel(), getwd(__VA_ARGS__))
#define glob(...) (pthread_testcancel(), glob(__VA_ARGS__))
#define iconv_close(...) (pthread_testcancel(), iconv_close(__VA_ARGS__))
#define iconv_open(...) (pthread_testcancel(), iconv_open(__VA_ARGS__))
#define ioctl(...) (pthread_testcancel(), ioctl(__VA_ARGS__))
#define link(...) (pthread_testcancel(), link(__VA_ARGS__))
#define localtime(...) (pthread_testcancel(), localtime(__VA_ARGS__))
#define localtime_r(...) (pthread_testcancel(), localtime_r(__VA_ARGS__))
#define lseek(...) (pthread_testcancel(), lseek(__VA_ARGS__))
#define lstat(...) (pthread_testcancel(), lstat(__VA_ARGS__))
#define mkstemp(...) (pthread_testcancel(), mkstemp(__VA_ARGS__))
#define nftw(...) (pthread_testcancel(), nftw(__VA_ARGS__))
#define opendir(...) (pthread_testcancel(), opendir(__VA_ARGS__))
#define openlog(...) (pthread_testcancel(), openlog(__VA_ARGS__))
#define pathconf(...) (pthread_testcancel(), pathconf(__VA_ARGS__))
#define pclose(...) (pthread_testcancel(), pclose(__VA_ARGS__))
#define perror(...) (pthread_testcancel(), perror(__VA_ARGS__))
#define popen(...) (pthread_testcancel(), popen(__VA_ARGS__))
#define posix_fadvise(...) (pthread_testcancel(), posix_fadvise(__VA_ARGS__))
#define posix_fallocate(...) (pthread_testcancel(), posix_fallocate(__VA_ARGS__))
#define posix_madvise(...) (pthread_testcancel(), posix_madvise(__VA_ARGS__))
#define posix_openpt(...) (pthread_testcancel(), posix_openpt(__VA_ARGS__))
#define posix_spawn(...) (pthread_testcancel(), posix_spawn(__VA_ARGS__))
#define posix_spawnp(...) (pthread_testcancel(), posix_spawnp(__VA_ARGS__))
#define posix_trace_clear(...) (pthread_testcancel(), posix_trace_clear(__VA_ARGS__))
#define posix_trace_close(...) (pthread_testcancel(), posix_trace_close(__VA_ARGS__))
#define posix_trace_create(...) (pthread_testcancel(), posix_trace_create(__VA_ARGS__))
#define posix_trace_create_withlog(...) (pthread_testcancel(), posix_trace_create_withlog(__VA_ARGS__))
#define posix_trace_eventtypelist_getne(...) (pthread_testcancel(), posix_trace_eventtypelist_getne(__VA_ARGS__))
#define posix_trace_eventtypelist_rewin(...) (pthread_testcancel(), posix_trace_eventtypelist_rewin(__VA_ARGS__))
#define posix_trace_flush(...) (pthread_testcancel(), posix_trace_flush(__VA_ARGS__))
#define posix_trace_get_attr(...) (pthread_testcancel(), posix_trace_get_attr(__VA_ARGS__))
#define posix_trace_get_filter(...) (pthread_testcancel(), posix_trace_get_filter(__VA_ARGS__))
#define posix_trace_get_status(...) (pthread_testcancel(), posix_trace_get_status(__VA_ARGS__))
#define posix_trace_getnext_event(...) (pthread_testcancel(), posix_trace_getnext_event(__VA_ARGS__))
#define posix_trace_open(...) (pthread_testcancel(), posix_trace_open(__VA_ARGS__))
#define posix_trace_rewind(...) (pthread_testcancel(), posix_trace_rewind(__VA_ARGS__))
#define posix_trace_setfilter(...) (pthread_testcancel(), posix_trace_setfilter(__VA_ARGS__))
#define posix_trace_shutdown(...) (pthread_testcancel(), posix_trace_shutdown(__VA_ARGS__))
#define posix_trace_timedgetnext_event(...) (pthread_testcancel(), posix_trace_timedgetnext_event(__VA_ARGS__))
#define posix_typed_mem_open(...) (pthread_testcancel(), posix_typed_mem_open(__VA_ARGS__))
#define printf(...) (pthread_testcancel(), printf(__VA_ARGS__))
#define putc(...) (pthread_testcancel(), putc(__VA_ARGS__))
#define putc_unlocked(...) (pthread_testcancel(), putc_unlocked(__VA_ARGS__))
#define putchar(...) (pthread_testcancel(), putchar(__VA_ARGS__))
#define putchar_unlocked(...) (pthread_testcancel(), putchar_unlocked(__VA_ARGS__))
#define puts(...) (pthread_testcancel(), puts(__VA_ARGS__))
#define pututxline(...) (pthread_testcancel(), pututxline(__VA_ARGS__))
#undef putwc
#define putwc(...) (pthread_testcancel(), putwc(__VA_ARGS__))
#undef putwchar
#define putwchar(...) (pthread_testcancel(), putwchar(__VA_ARGS__))
#define readdir(...) (pthread_testcancel(), readdir(__VA_ARSG__))
#define readdir_r(...) (pthread_testcancel(), readdir_r(__VA_ARGS__))
#define remove(...) (pthread_testcancel(), remove(__VA_ARGS__))
#define rename(...) (pthread_testcancel(), rename(__VA_ARGS__))
#define rewind(...) (pthread_testcancel(), rewind(__VA_ARGS__))
#define rewinddir(...) (pthread_testcancel(), rewinddir(__VA_ARGS__))
#define scanf(...) (pthread_testcancel(), scanf(__VA_ARGS__))
#define seekdir(...) (pthread_testcancel(), seekdir(__VA_ARGS__))
#define semop(...) (pthread_testcancel(), semop(__VA_ARGS__))
#define setgrent(...) (pthread_testcancel(), setgrent(__VA_ARGS__))
#define sethostent(...) (pthread_testcancel(), sethostemt(__VA_ARGS__))
#define setnetent(...) (pthread_testcancel(), setnetent(__VA_ARGS__))
#define setprotoent(...) (pthread_testcancel(), setprotoent(__VA_ARGS__))
#define setpwent(...) (pthread_testcancel(), setpwent(__VA_ARGS__))
#define setservent(...) (pthread_testcancel(), setservent(__VA_ARGS__))
#define setutxent(...) (pthread_testcancel(), setutxent(__VA_ARGS__))
#define stat(...) (pthread_testcancel(), stat(__VA_ARGS__))
#define strerror(...) (pthread_testcancel(), strerror(__VA_ARGS__))
#define strerror_r(...) (pthread_testcancel(), strerror_r(__VA_ARGS__))
#define strftime(...) (pthread_testcancel(), strftime(__VA_ARGS__))
#define symlink(...) (pthread_testcancel(), symlink(__VA_ARGS__))
#define sync(...) (pthread_testcancel(), sync(__VA_ARGS__))
#define syslog(...) (pthread_testcancel(), syslog(__VA_ARGS__))
#define tmpfile(...) (pthread_testcancel(), tmpfile(__VA_ARGS__))
#define tmpnam(...) (pthread_testcancel(), tmpnam(__VA_ARGS__))
#define ttyname(...) (pthread_testcancel(), ttyname(__VA_ARGS__))
#define ttyname_r(...) (pthread_testcancel(), ttyname_r(__VA_ARGS__))
#define tzset(...) (pthread_testcancel(), tzset(__VA_ARGS__))
#define ungetc(...) (pthread_testcancel(), ungetc(__VA_ARGS__))
#define ungetwc(...) (pthread_testcancel(), ungetwc(__VA_ARGS__))
#define unlink(...) (pthread_testcancel(), unlink(__VA_ARGS__))
#define vfprintf(...) (pthread_testcancel(), vfprintf(__VA_ARGS__))
#define vfwprintf(...) (pthread_testcancel(), vfwprintf(__VA_ARGS__))
#define vprintf(...) (pthread_testcancel(), vprintf(__VA_ARGS__))
#define vwprintf(...) (pthread_testcancel(), vwprintf(__VA_ARGS__))
#define wcsftime(...) (pthread_testcancel(), wcsftime(__VA_ARGS__))
#define wordexp(...) (pthread_testcancel(), wordexp(__VA_ARGS__))
#define wprintf(...) (pthread_testcancel(), wprintf(__VA_ARGS__))
#define wscanf(...) (pthread_testcancel(), wscanf(__VA_ARGS__))
#endif