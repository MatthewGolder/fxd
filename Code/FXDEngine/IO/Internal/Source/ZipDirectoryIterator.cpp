// Creator - MatthewGolder
#include "FXDEngine/IO/ZipDirectoryIterator.h"
#include "FXDEngine/IO/Path.h"
#include "FXDEngine/App/Platform.h"

using namespace FXD;
using namespace IO;

// ------
// IO::Internal::_zip_directory_iterator >
// - 
// ------
IO::Internal::_zip_directory_iterator::_zip_directory_iterator(void)
	: m_unzFile(nullptr)
{
}

IO::Internal::_zip_directory_iterator::~_zip_directory_iterator(void)
{
	_close();
}

IO::Internal::_zip_directory_iterator::_zip_directory_iterator(Internal::_zip_directory_iterator&& rhs) NOEXCEPT
	: m_strRoot(std::move(rhs.m_strRoot))
	, m_strPattern(std::move(rhs.m_strPattern))
	, m_entry(std::move(rhs.m_entry))
	, m_unzFile(std::move(rhs.m_unzFile))
{
	rhs.m_unzFile = nullptr;
}

bool IO::Internal::_zip_directory_iterator::is_good(void)const
{
	return (m_unzFile != nullptr);
}

void IO::Internal::_zip_directory_iterator::_open(const IO::Path& path, const Core::String8& strPattern)
{
	m_strRoot = path;
	m_strPattern = strPattern;
	m_unzFile = unzOpen64(path.c_str());
	if (m_unzFile == nullptr)
	{
		_close();
		return;
	}

	FXD::S32 nRes = unzGoToFirstFile(m_unzFile);
	if (unzGoToFirstFile(m_unzFile) < 0)
	{
		_close();
		return;
	}

	if (_try_assign() == false)
	{
		_advance();
	}
}

bool IO::Internal::_zip_directory_iterator::_advance(void)
{
	while (unzGoToNextFile(m_unzFile) == 0)
	{
		if (_try_assign() == true)
		{
			return true;
		}
	}
	_close();
	return false;
}

void IO::Internal::_zip_directory_iterator::_close(void)
{
	if (m_unzFile != nullptr)
	{
		unzClose(m_unzFile);
	}
	m_entry = IO::DirectoryEntry();
	m_unzFile = nullptr;
}

bool IO::Internal::_zip_directory_iterator::_try_assign(void)
{
	FXD::UTF8 pStr[256];
	unz_file_info64 fileInfo;
	FXD::S32 nRes = unzGetCurrentFileInfo64(m_unzFile, &fileInfo, pStr, 256, nullptr, 0, nullptr, 0);
	if (nRes < 0)
	{
		return false;
	}
	Core::StringView8 str(pStr);
	if (str == "." || str == "..")
	{
		return false;
	}
	m_entry.assign(IO::Path(m_strRoot) / str.c_str());
	return true;
}

// ------
// zip_directory_iterator
// - 
// ------
zip_directory_iterator::zip_directory_iterator(void)
{
}

zip_directory_iterator::zip_directory_iterator(const IO::Path& path, const Core::String8& strPattern)
{
	m_internal = std::make_shared< IO::Internal::_zip_directory_iterator >();
	m_internal->_open(path, strPattern);
	if (m_internal->is_good() == false)
	{
		m_internal.reset();
	}
}

zip_directory_iterator::~zip_directory_iterator(void)
{
}

zip_directory_iterator::zip_directory_iterator(const zip_directory_iterator& rhs)
	: m_internal(rhs.m_internal)
{
}

zip_directory_iterator::zip_directory_iterator(zip_directory_iterator&& rhs)
	: m_internal(std::move(rhs.m_internal))
{
}

zip_directory_iterator& zip_directory_iterator::operator=(const zip_directory_iterator& rhs)
{
	if (this != &rhs)
	{
		m_internal = rhs.m_internal;
	}
	return (*this);
}

zip_directory_iterator& zip_directory_iterator::operator=(zip_directory_iterator&& rhs) NOEXCEPT
{
	if (this != &rhs)
	{
		m_internal = std::move(rhs.m_internal);
	}
	return (*this);
}

zip_directory_iterator& zip_directory_iterator::operator++()
{
	return increment();
}

zip_directory_iterator zip_directory_iterator::operator++(int)
{
	zip_directory_iterator tmp = (*this);
	++(*this);
	return (tmp);
}

const IO::DirectoryEntry& zip_directory_iterator::operator*(void) const
{
	return m_internal->m_entry;
}

const IO::DirectoryEntry* zip_directory_iterator::operator->(void) const
{
	return &m_internal->m_entry;
}

bool zip_directory_iterator::operator==(const zip_directory_iterator& rhs) const
{
	if (m_internal == rhs.m_internal)
	{
		return true;
	}
	if (rhs.m_internal == nullptr)
	{
		return (m_internal->m_entry == IO::DirectoryEntry());
	}
	if (m_internal == nullptr)
	{
		return (rhs.m_internal->m_entry == IO::DirectoryEntry());
	}
	return (m_internal->m_entry == rhs.m_internal->m_entry);
}

bool zip_directory_iterator::operator!=(const zip_directory_iterator& rhs) const
{
	return !(*this == rhs);
}

IO::zip_directory_iterator& zip_directory_iterator::increment(void)
{
	m_internal->_advance();
	return (*this);
}