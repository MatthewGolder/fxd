// Creator - MatthewGolder
#include "FXDEngine/IO/Types.h"

#if IsSystemWindows()
#include "FXDEngine/IO/Internal/DirectoryIterator.h"
#include "FXDEngine/IO/Path.h"
#include "FXDEngine/App/Platform.h"

#include <shlobj.h>
#include <stdio.h>
#include <sys/stat.h>

using namespace FXD;
using namespace IO;

// ------
// _directory_iterator::_directory_iterator >
// - 
// ------
IO::Internal::_directory_iterator::_directory_iterator(void)
	: m_hFind(INVALID_HANDLE_VALUE)
{
	Memory::MemZero_T(m_findFileData);
}

IO::Internal::_directory_iterator::~_directory_iterator(void)
{
	_close();
}

IO::Internal::_directory_iterator::_directory_iterator(_directory_iterator&& rhs) NOEXCEPT
	: m_strRoot(std::move(rhs.m_strRoot))
	, m_strPattern(std::move(rhs.m_strPattern))
	, m_entry(std::move(rhs.m_entry))
	, m_hFind(rhs.m_hFind)
	, m_findFileData(rhs.m_findFileData)
{
	rhs.m_hFind = nullptr;
	Memory::MemZero_T(rhs.m_findFileData);
}

IO::Internal::_directory_iterator& IO::Internal::_directory_iterator::operator=(const IO::Internal::_directory_iterator& rhs)
{
	if (this != &rhs)
	{
		m_strRoot = rhs.m_strRoot;
		m_strPattern = rhs.m_strPattern;
		m_entry = rhs.m_entry;
		m_hFind = rhs.m_hFind;
		m_findFileData = rhs.m_findFileData;
	}
	return (*this);
}

bool IO::Internal::_directory_iterator::is_good(void)const
{
	return (m_hFind != INVALID_HANDLE_VALUE);
}

void IO::Internal::_directory_iterator::_open(const IO::Path& path, const Core::String8& strPattern)
{
	m_strRoot = path;
	m_strPattern = strPattern;
	m_hFind = FindFirstFileA((path / strPattern.c_str()).c_str(), &m_findFileData);
	if (m_hFind == INVALID_HANDLE_VALUE)
	{
		_close();
		return;
	}

	if (_try_assign() == false)
	{
		_advance();
	}
}

bool IO::Internal::_directory_iterator::_advance(void)
{
	while (::FindNextFileA(m_hFind, &m_findFileData) == TRUE)
	{
		if (_try_assign() == true)
		{
			return true;
		}
	}
	_close();
	return false;
}

void IO::Internal::_directory_iterator::_close(void)
{
	if (m_hFind != nullptr)
	{
		::FindClose(m_hFind);
	}
	m_entry = IO::DirectoryEntry();
	m_hFind = INVALID_HANDLE_VALUE;
	Memory::MemZero_T(m_findFileData);
}

bool IO::Internal::_directory_iterator::_try_assign(void)
{
	Core::StringView8 str(m_findFileData.cFileName);
	if (str == "." || str == "..")
	{
		return false;
	}
	m_entry.assign(IO::Path(m_strRoot) / str.c_str());
	return true;
}

#endif //IsSystemWindows()