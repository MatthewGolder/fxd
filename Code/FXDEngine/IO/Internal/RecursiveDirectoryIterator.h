// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_INTERNAL_RECURSIVEDIRECTORYITERATOR_H
#define FXDENGINE_IO_INTERNAL_RECURSIVEDIRECTORYITERATOR_H

#include "FXDEngine/IO/Internal/DirectoryIterator.h"
#include "FXDEngine/Container/Vector.h"

namespace FXD
{
	namespace IO
	{
		namespace Internal
		{
			struct _recursive_directory_iterator
			{
			public:
				friend class IO::directory_iterator;
				friend class IO::recursive_directory_iterator;
				
			private:
				Container::Vector< Internal::_directory_iterator > m_stack;
			};
		} //namespace Internal
	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_INTERNAL_RECURSIVEDIRECTORYITERATOR_H