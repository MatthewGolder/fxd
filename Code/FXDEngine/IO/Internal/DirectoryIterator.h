// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_INTERNAL_DIRECTORYITERATOR_H
#define FXDENGINE_IO_INTERNAL_DIRECTORYITERATOR_H

#include "FXDEngine/Core/String.h"
#include "FXDEngine/IO/DirectoryEntry.h"

#if IsSystemWindows()
#	include "FXDEngine/Core/Win32/Win32.h"
#elif IsSystemPosix()
#	if IsOSPC()
#		include <3rdParty/dirent/dirent.h>
#	elif IsOSAndroid()
#		include <dirent.h>
#	endif
#endif

namespace FXD
{
	namespace IO
	{
		class directory_iterator;
		class recursive_directory_iterator;

		namespace Internal
		{
			class _directory_iterator
			{
			public:
				friend class IO::directory_iterator;
				friend class IO::recursive_directory_iterator;

			public:
				_directory_iterator(void);
				~_directory_iterator(void);

				_directory_iterator(_directory_iterator&& rhs) NOEXCEPT;
				_directory_iterator& operator=(const _directory_iterator& rhs);

				bool is_good(void)const;

			private:
				void _open(const IO::Path& path, const Core::String8& strPattern);
				bool _advance(void);
				void _close(void);
#if IsSystemWindows()
				bool _try_assign(void);
#endif

			private:
				Core::String8 m_strRoot;
				Core::String8 m_strPattern;
				IO::DirectoryEntry m_entry;
#if IsSystemWindows()
				HANDLE m_hFind;
				WIN32_FIND_DATAA m_findFileData;
#elif IsSystemPosix()
				struct DIR* m_pDir;
				struct dirent* m_pDirent;
#endif
			};
		} //namespace Internal
	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_INTERNAL_DIRECTORYITERATOR_H