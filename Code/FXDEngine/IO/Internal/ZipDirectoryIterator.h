// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_INTERNAL_ZIPDIRECTORYITERATOR_H
#define FXDENGINE_IO_INTERNAL_ZIPDIRECTORYITERATOR_H

#include "FXDEngine/Core/String.h"
#include "FXDEngine/IO/DirectoryEntry.h"
#include <3rdParty/minizip/mz_compat.h>

namespace FXD
{
	namespace IO
	{
		namespace Internal
		{
			class _zip_directory_iterator
			{
			public:
				friend class IO::zip_directory_iterator;

			public:
				_zip_directory_iterator(void);
				~_zip_directory_iterator(void);

				_zip_directory_iterator(Internal::_zip_directory_iterator&& rhs) NOEXCEPT;
				_zip_directory_iterator& operator=(const Internal::_zip_directory_iterator& rhs) = delete;

				bool is_good(void)const;

			private:
				void _open(const IO::Path& path, const Core::String8& strPattern);
				bool _advance(void);
				void _close(void);
				bool _try_assign(void);

			private:
				Core::String8 m_strRoot;
				Core::String8 m_strPattern;
				IO::DirectoryEntry m_entry;
				unzFile m_unzFile;
			};
		} //namespace Internal
	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_INTERNAL_ZIPDIRECTORYITERATOR_H