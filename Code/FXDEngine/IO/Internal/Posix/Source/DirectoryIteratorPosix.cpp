// Creator - MatthewGolder
#include "FXDEngine/IO/Types.h"

#if IsSystemPosix()
#include "FXDEngine/IO/Internal/DirectoryIterator.h"
#include "FXDEngine/IO/Path.h"
#include "FXDEngine/App/Platform.h"

#if IsOSPC()
#	include <direct.h>
#	include <process.h>
#	define getcwd _getcwd // stupid MSFT "deprecation" warning
#elif IsOSAndroid()
#	include <unistd.h>
#	include <sys/stat.h>
#	include <stdlib.h>
#	include "FXDEngine/App/Android/GlobalsAndroid.h"
#	include "FXDEngine/App/Platform.h"
#	include <android/asset_manager.h> 
#	include "FXDEngine/IO/IOJPThread.h"
#	include "FXDEngine/IO/IOSystem.h"
#	include "FXDEngine/IO/IODevice.h"
#	include "FXDEngine/App/FXDApi.h"
#endif

using namespace FXD;
using namespace IO;

namespace
{
	struct dirent* readdir(DIR* pDir, const Core::String8& strPattern)
	{
		if (strPattern.empty())
		{
			return nullptr;
		}

		// Return everything
		struct dirent* pDirent = readdir(pDir);
		if (strPattern == UTF_8("*"))
		{
			return pDirent;
		}

		if (strPattern.starts_with(UTF_8("*")))
		{
			Core::String8 strTmpPattern = strPattern;
			strTmpPattern.find_replace(UTF_8("*"), UTF_8(""));
			while (pDirent != nullptr)
			{
				// Return directories
				if (strPattern.ends_with(UTF_8(".")))
				{
					struct stat stat;
					::stat(pDirent->d_name, &stat);
					bool bDir = (stat.st_mode & S_IFDIR);
					if (bDir)
					{
						break;
					}
				}
				Core::String8 strDirent(pDirent->d_name);
				if (strDirent.ends_with_no_case(strTmpPattern))
				{
					break;
				}
				pDirent = ::readdir(pDir);
			}
		}
		return pDirent;
	}
}

// ------
// IO::Internal::_directory_iterator >
// - 
// ------
IO::Internal::_directory_iterator::_directory_iterator(void)
	: m_pDir(nullptr)
	, m_pDirent(nullptr)
{
}

IO::Internal::_directory_iterator::~_directory_iterator(void)
{
	_close();
}

IO::Internal::_directory_iterator::_directory_iterator(Internal::_directory_iterator&& rhs) NOEXCEPT
	: m_strRoot(std::move(rhs.m_strRoot))
	, m_strPattern(std::move(rhs.m_strPattern))
	, m_entry(std::move(rhs.m_entry))
	, m_pDir(rhs.m_pDir)
	, m_pDirent(rhs.m_pDirent)
{
	rhs.m_pDir = nullptr;
	rhs.m_pDirent = nullptr;
}

IO::Internal::_directory_iterator& IO::Internal::_directory_iterator::operator=(const IO::Internal::_directory_iterator& rhs)
{
	if (this != &rhs)
	{
		m_strRoot = rhs.m_strRoot;
		m_strPattern = rhs.m_strPattern;
		m_entry = rhs.m_entry;
		m_pDir = rhs.m_pDir;
		m_pDirent = rhs.m_pDirent;
	}
	return (*this);
}

bool IO::Internal::_directory_iterator::is_good(void)const
{
	return (m_pDir != nullptr);
}

void IO::Internal::_directory_iterator::_open(const IO::Path& path, const Core::String8& strPattern)
{
	m_strRoot = path;
	m_strPattern = strPattern;

	m_pDir = opendir(path.c_str());
	if (m_pDir == nullptr)
	{
		_close();
		return;
	}

	_advance();
}

bool IO::Internal::_directory_iterator::_advance(void)
{
	while (true)
	{
		m_pDirent = readdir(m_pDir, m_strPattern);
		if (m_pDirent == nullptr)
		{
			_close();
			return false;
		}
		else
		{
			Core::StringView8 str(m_pDirent->d_name);
			if (str == "." || str == "..")
			{
				continue;
			}
			else if (str.empty())
			{
				_close();
				return false;
			}
			else
			{
				m_entry.assign(IO::Path(m_strRoot) / str.c_str());
				return true;
			}
		}
	}
}

void IO::Internal::_directory_iterator::_close(void)
{
	if (m_pDir != nullptr)
	{
		closedir(m_pDir);
	}
	m_entry = IO::DirectoryEntry();
	m_pDir = nullptr;
	m_pDirent = nullptr;
}
#endif //IsSystemPosix()