// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_DATAREGISTRY_H
#define FXDENGINE_IO_DATAREGISTRY_H

#include "FXDEngine/Core/String.h"
#include "FXDEngine/Container/Map.h"
#include "FXDEngine/Memory/MemHandle.h"
#include "FXDEngine/Thread/RWLock.h"
#include "FXDEngine/Job/Event.h"
#include "FXDEngine/IO/Types.h"
#include "FXDEngine/IO/Save.h"

namespace FXD
{
	namespace IO
	{
		class DataRegistryEvent : public Job::IEvent
		{
		public:
			DataRegistryEvent(const Core::String8& strName)
				: m_strName(strName)
			{}
			~DataRegistryEvent(void)
			{}

		public:
			const Core::String8 m_strName;
		};

		// ------
		// IDataRegistry
		// -
		// ------
		class IDataRegistry FINAL : public IO::ISaveable, public IO::ILoadable, public Core::NonCopyable
		{
		public:
			// ------
			// RWDataRegistry
			// -
			// ------
			class RWDataRegistry
			{
			public:
				RWDataRegistry(void);
				~RWDataRegistry(void);

			public:
				FXD::U64 m_nChangeCounter;
				Container::Map< Core::String8, bool > m_booleans;
				Container::Map< Core::String8, FXD::S64 > m_ints;
				Container::Map< Core::String8, FXD::F32 > m_floats;
				Container::Map< Core::String8, Core::String8 > m_strings8;
				Container::Map< Core::String8, Core::String16 > m_strings16;
				Container::Map< Core::String8, Memory::MemHandle > m_binaryBlobs;
			};

		public:
			IDataRegistry(void);
			virtual ~IDataRegistry(void);
			
			void set_string8(const Core::String8& strName, const Core::String8& strValue);
			void set_string16(const Core::String8& strName, const Core::String16& strValue);
			void set_bool(const Core::String8& strName, bool bValue);
			void set_int(const Core::String8& strName, FXD::S64 nValue);
			void set_float(const Core::String8& strName, FXD::F32 fValue);
			void set_binary(const Core::String8& strName, const Memory::MemHandle& memValue);

			Core::String8 get_string8(const Core::String8& strName, const Core::String8& strDefault = Core::String8()) const;
			Core::String16 get_string16(const Core::String8& strName, const Core::String16& strDefault = Core::String16()) const;
			bool get_bool(const Core::String8& strName, bool nDefault = false) const;
			FXD::S64 get_int(const Core::String8& strName, FXD::S64 nDefault = 0) const;
			FXD::F32 get_float(const Core::String8& strName, FXD::F32 fDefault = 0.0f) const;
			Memory::MemHandle get_binary(const Core::String8& strName) const;

			void remove_all(const Core::String8& strStartingWith);
			bool exists(const Core::String8& strStartingWith) const;

			const Job::Handler< IO::DataRegistryEvent >* add_handler(FXD::STD::function< void(IO::DataRegistryEvent) >&& func);
			void remove_handler(const Job::Handler< IO::DataRegistryEvent >* pHandler);
			FXD::U64 get_change_counter(void) const;

		private:
			void _save_to_stream(Core::IStreamOut& ostr) const FINAL;
			void _load_from_stream(Core::IStreamIn& istr) FINAL;

			template < typename Type >
			void _set_type(const Core::String8& strName, const Type& value, Container::Map< Core::String8, Type >& map, Thread::RWLockData< RWDataRegistry >::ScopedWriteLock& rwData)
			{
				typename Container::Map< Core::String8, Type >::iterator itr = map.find(strName);
				if ((itr != map.end()) && ((*itr).second == value))
				{
					return;
				}
				map[strName] = value;
				rwData->m_nChangeCounter++;
				_raise_handlers(strName, rwData);
			}

			void _raise_handlers(const Core::String8& strName, Thread::RWLockData< RWDataRegistry >::ScopedWriteLock& rwData);

		private:
			Thread::RWLockData< RWDataRegistry > m_rwData;
		};

	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_DATAREGISTRY_H