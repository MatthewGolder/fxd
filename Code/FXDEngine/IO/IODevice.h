// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_IODEVICE_H
#define FXDENGINE_IO_IODEVICE_H

#include "FXDEngine/IO/Types.h"
#include "FXDEngine/IO/Save.h"
#include "FXDEngine/Job/Future.h"
#include "FXDEngine/Memory/MemHandle.h"

namespace FXD
{
	namespace IO
	{
		// ------
		// IIODevice
		// -
		// ------
		class IIODevice : public Core::HasImpl< IIODevice, 16 >
		{
		public:
			IIODevice(void);
			virtual ~IIODevice(void);

			Job::Future< IO::StreamFile > file_open_stream(const IO::Path& ioPath, IO::FileOpenParams fileOpenParams, Core::CompressParams compressParams = Core::CompressParams::Default) const;
			Job::Future< Memory::StreamMemHandle > file_open_memory_stream(const IO::Path& ioPath, bool bIgnoreMissingFiles, Core::CompressParams compressParams = Core::CompressParams::Default) const;
			Job::Future< Memory::MemHandle > file_open_memhandle(const IO::Path& ioPath, bool bIgnoreMissingFiles, Core::CompressParams compressParams = Core::CompressParams::Default) const;

			Job::Future< bool > load(IO::ILoadable* pLoadable, const IO::Path& ioPath, bool bIgnoreMissingFiles, Core::CompressParams compressParams = Core::CompressParams::Default) const;
			Job::Future< bool > save(IO::ISaveable* pSaveable, const IO::Path& ioPath, bool bIgnoreMissingFiles, Core::CompressParams compressParams = Core::CompressParams::Default) const;

		protected:
			IO::StreamFile _file_open_stream(const IO::Path& ioPath, IO::FileOpenParams fileOpenParams, Core::CompressParams compressParams) const;
			Memory::StreamMemHandle _file_open_memory_stream(const IO::Path& ioPath, bool bIgnoreMissingFiles, Core::CompressParams compressParams) const;
			Memory::MemHandle _file_open_memhandle(const IO::Path& ioPath, bool bIgnoreMissingFiles, Core::CompressParams compressParams) const;

			bool _load(IO::ILoadable* pLoadable, const IO::Path& ioPath, bool bIgnoreMissingFiles, Core::CompressParams) const;
			bool _save(IO::ISaveable* pSaveable, const IO::Path& ioPath, bool bIgnoreMissingFiles, Core::CompressParams compressParams) const;
		};

	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_IODEVICE_H
