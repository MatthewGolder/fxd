// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_XML_XMLSAXPARSER_H
#define FXDENGINE_IO_XML_XMLSAXPARSER_H

#include "FXDEngine/IO/IOStreamParser.h"
#include "FXDEngine/Core/String.h"
#include "FXDEngine/IO/Types.h"

namespace FXD
{
	namespace IO
	{
		// ------
		// XMLSaxParser
		// -
		// ------
		class XMLSaxParser : public Core::NonCopyable
		{
		public:
			// ------
			// ITagCB
			// -
			// ------
			class ITagCB
			{
			public:
				ITagCB(void)
				{}
				virtual ~ITagCB(void)
				{}

				virtual void handle_tag(const Core::String8& /*strText*/, ITagCB*& /*pTagCB*/)
				{}

				virtual void end_tag(const Core::String8& /*strName*/)
				{}

				virtual void destroyTagHandlers(ITagCB* /*ptagCB*/)
				{}

				virtual void handle_text(const Core::String8& /*strText*/, const IO::StreamParser::Position& /*pos*/)
				{}

				virtual void handle_attribute(const Core::String8& /*strText*/, const Core::String8& /*strValue*/)
				{}
			};

			// ------
			// TagParser
			// -
			// ------
			class TagParser
			{
			public:
				bool operator()(IO::StreamParser& pc, XMLSaxParser::ITagCB& pTagCB, Core::String8& strTagName) const;
			};

			// ------
			// XmlRootParser
			// -
			// ------
			class XmlRootParser
			{
			public:
				bool operator()(IO::StreamParser& pc, XMLSaxParser::ITagCB& pTagCB) const;
			};

			// Parse an XML stream.
			static bool parse(IO::StreamParser& pc, XMLSaxParser::ITagCB& pTagCB);
		};
	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_XML_XMLSAXPARSER_H