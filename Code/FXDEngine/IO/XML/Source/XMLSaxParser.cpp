// Creator - MatthewGolder
#include "FXDEngine/IO/XML/XMLSaxParser.h"

using namespace FXD;
using namespace IO;

// ------
// XMLSaxParser
// -
// ------
bool XMLSaxParser::XmlRootParser::operator()(IO::StreamParser& pc, XMLSaxParser::ITagCB& /*pTagCB*/) const
{
	IO::StreamParser::Position pos = pc.get_parse_position();

	// Parse open tag.
	pc.skip_whitespace();
	IO::Parser::FixedString fixedString;
	if (!fixedString(pc, "<"))
	{
		pc.set_parse_position(pos);
		/*
		XmlBinaryReader binaryReader;
		binaryReader(pc, pTagCB);
		*/
		PRINT_ASSERT << "IO: XmlBinaryReader";
		return false;
	}

	pc.set_parse_position(pos); // reset parser
	if (!fixedString(pc, "<?xml "))
	{
		pc.set_parse_position(pos);
		return true;
	}

	// Parse attributes.
	pc.skip_whitespace();
	IO::Parser::Identifier identifier;
	while (!fixedString(pc, "?>"))
	{
		Core::String8 strAttrName;
		if (!identifier(pc, strAttrName))
		{
			pc.set_parse_position(pos);
			return false;
		}
		pc.skip_whitespace();
		if (pc.get_next_char() != '=')
		{
			pc.set_parse_position(pos);
			return false;
		}
		pc.skip_whitespace();
		Core::String8 strAttrValue;
		IO::Parser::QuotedString quotedString;
		if (!quotedString(pc, strAttrValue))
		{
			pc.set_parse_position(pos);
			return false;
		}
		// Could do something with strAttrName and strAttrValue here if required.
		pc.skip_whitespace();
	};
	return true;
}

static XMLSaxParser::ITagCB g_bChildCB;

bool XMLSaxParser::TagParser::operator()(IO::StreamParser& pc, XMLSaxParser::ITagCB& pTagCB, Core::String8& strTagName) const
{
	IO::StreamParser::Position pos = pc.get_parse_position();

	// Parse open tag.
	IO::Parser::FixedString fixedString;
	bool bOpenTag = false;
	while (!bOpenTag)
	{
		pc.skip_whitespace();
		if (pc.get_next_char() != '<')
		{
			pc.set_parse_position(pos);
			return false;
		}
		bOpenTag = true;
	}
	//Core::String8 strTagName;
	IO::Parser::Identifier identifier;
	if (!identifier(pc, strTagName))
	{
		pc.set_parse_position(pos);
		return false;
	}

	// Get sub-parsers.
	ITagCB* pChildCB = &g_bChildCB;
	pTagCB.handle_tag(strTagName, pChildCB);

	// Parse attributes.
	pc.skip_whitespace();
	bool bLeafTag = false;
	while (pc.peek_next_char() != '>')
	{
		if (pc.peek_next_char() == '/')
		{
			bLeafTag = true;
			pc.get_next_char();
			continue;
		}
		Core::String8 strAttrName;
		if (!identifier(pc, strAttrName))
		{
			pc.set_parse_position(pos);
			return false;
		}
		pc.skip_whitespace();
		if (pc.get_next_char() != '=')
		{
			pc.set_parse_position(pos);
			return false;
		}
		pc.skip_whitespace();
		Core::String8 strAttrValue;
		IO::Parser::QuotedString quotedString;
		if (!quotedString(pc, strAttrValue))
		{
			pc.set_parse_position(pos);
			return false;
		}
		pChildCB->handle_attribute(strAttrName, strAttrValue);
		pc.skip_whitespace();
	};
	pc.get_next_char();
	if (bLeafTag)
	{
		return true;
	}
	pc.skip_whitespace();

	TagParser childParser;
	Core::String8 strCloseTagBld;
	strCloseTagBld.append("</");
	strCloseTagBld.append(strTagName);
	strCloseTagBld.append(">");

	Core::String8 strText;
	IO::StreamParser::Position textPosition(pc.get_parse_position());
	bool bCData = false;
	for (;;)
	{
		if (!bCData)
		{
			if (fixedString(pc, "<![CDATA["))
			{
				bCData = true;
				strText.clear();
			}
		}
		else
		{
			if (fixedString(pc, "]]>"))
			{
				bCData = false;
			}
		}
		if (!bCData)
		{
			if (fixedString(pc, strCloseTagBld.c_str()))
			{
				break;
			}
			else if (fixedString(pc, "</"))
			{
				PRINT_ASSERT << "IO: XML Error: Expected '" << strCloseTagBld.c_str() << "' not found";
			}
			if (fixedString(pc, "<!--"))
			{
				while (!fixedString(pc, "-->"))
				{
					if (pc.get_next_char() == IO::StreamParser::kEndOfFile)
					{
						return false;
					}
				}
			}
			FXD::UTF8 peek = pc.peek_next_char();
			if (peek == '<')
			{
				if (strText.not_empty())
				{
					pChildCB->handle_text(strText, textPosition);
					strText.clear();
				}
				Core::String8 strEndTag = "";
				if (!childParser(pc, (*pChildCB), strEndTag))
				{
					return false;
				}
				pChildCB->end_tag(strEndTag);
				pc.skip_whitespace();
			}
			else
			{
				IO::StreamParser::Position parserPosition(pc.get_parse_position());
				FXD::UTF8 charBuff[256];
				FXD::S32 nCount = 0;
				while (pc.peek_next_char() != '<')
				{
					FXD::UTF8 c = pc.get_next_char();
					if (c == IO::StreamParser::kEndOfFile)
					{
						return false;
					}
					charBuff[nCount++] = c;
					if (nCount > 200)
					{
						break;
					}
				}
				if (strText.empty())
				{
					textPosition = parserPosition;
				}
				charBuff[nCount] = 0;
				strText += charBuff;
			}
		}
		else
		{
			IO::StreamParser::Position parserPosition(pc.get_parse_position());
			FXD::UTF8 ch = pc.get_next_char();
			if (ch == IO::StreamParser::kEndOfFile)
			{
				return false;
			}
			if (strText.empty())
			{
				textPosition = parserPosition;
			}
			strText += ch;
		}
	}
	if (strText.not_empty())
	{
		pChildCB->handle_text(strText, textPosition);
	}
	pTagCB.destroyTagHandlers(/*pAttrCB, pTextCB,*/ pChildCB);
	return true;
}

// Parse an xml stream.
bool XMLSaxParser::parse(IO::StreamParser& pc, XMLSaxParser::ITagCB& pTagCB)
{
	IO::StreamParser::Position pos = pc.get_parse_position();
	bool bUTF8Lead = false;
	if ((FXD::UTF8)pc.get_next_char() == 0xef)
	{
		if ((FXD::UTF8)pc.get_next_char() == 0xbb)
		{
			if ((FXD::UTF8)pc.get_next_char() == 0xbf)
			{
				bUTF8Lead = true;
			}
		}
	}
	if (!bUTF8Lead)
	{
		pc.set_parse_position(pos);
	}

	bool bParsed = false;
	XmlRootParser rootParser;
	if (rootParser(pc, pTagCB))
	{
		TagParser tagParser;
		Core::String8 strEndTag = "";
		while (tagParser(pc, pTagCB, strEndTag))
		{
			bParsed = true;
		}
		pTagCB.end_tag(strEndTag);
	}
	else
	{
		bParsed = true; // must be binary format so yeah should_ be ok
	}
	return bParsed;
}