// Creator - MatthewGolder
#include "FXDEngine/IO/XML/XMLWriter.h"

using namespace FXD;
using namespace IO;

// ------
// IXMLWriter< FXD::UTF8 >
// -
// ------
template <>
void XMLWriter8::XMLNode::write_indent(const Core::StreamOut& stream)
{
	FXD::U32 nDepth = 0;
	XMLNode* pParent = m_pParent;
	while (pParent != nullptr)
	{
		nDepth++;
		pParent = pParent->m_pParent;
		(*m_stream) << UTF_8("   ");
	}
}

template <>
XMLWriter8::XMLNode::XMLNode(const Core::StreamOut& stream, const Core::String8& strName, const Core::String8& strText, XMLWriter8::XMLNode* pParent, XMLWriter8::XMLAttribute** pAttrs, FXD::U32 nAttrCount, bool bNewline, bool bHasChildren)
	: m_pParent(pParent)
	, m_bNewline(bNewline)
	, m_bHasChildren(bHasChildren)
	, m_stream(stream)
	, m_strName(strName)
	, m_strText(strText)
{
	write_indent(m_stream);
	(*m_stream) << UTF_8("<") << m_strName;
	for (FXD::U32 n1 = 0; n1 < nAttrCount; n1++)
	{
		(*m_stream) << UTF_8(" ") << pAttrs[n1]->m_strName << UTF_8("=\"") << pAttrs[n1]->m_strValue << UTF_8("\"");
	}
	if (m_bHasChildren)
	{
		(*m_stream) << UTF_8(">");
	}
	else
	{
		(*m_stream) << UTF_8(" />");
	}
	if (m_bNewline)
	{
		(*m_stream) << UTF_8("\r\n");
	}
}

template <>
XMLWriter8::XMLNode::~XMLNode(void)
{
	if (!m_bHasChildren)
	{
		return;
	}
	if (m_bNewline)
	{
		write_indent(m_stream);
	}
	(*m_stream) << m_strText;
	(*m_stream) << UTF_8("</") << m_strName << UTF_8(">\r\n");
}