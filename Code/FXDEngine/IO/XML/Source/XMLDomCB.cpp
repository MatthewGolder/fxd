// Creator - MatthewGolder
#include "FXDEngine/IO/XML/XMLDomCB.h"
#include "FXDEngine/Memory/StreamMemHandle.h"

using namespace FXD;
using namespace IO;

// ------
// XMLDomCB
// -
// ------
XMLDomCB::XMLDomCB(void)
{
}

XMLDomCB::~XMLDomCB(void)
{
}

void XMLDomCB::parse(const Core::StreamIn& stream, IO::XMLDomCB& node)
{
	IO::XMLDomCB workingNode;
	IO::StreamParser pc(stream);
	IO::XMLSaxParser::parse(pc, workingNode);
	if (workingNode.m_childrenNodes.size())
	{
		node = workingNode.m_childrenNodes[0];
	}
}

void XMLDomCB::handle_tag(const Core::String8& strName, IO::XMLDomCB::ITagCB*& pTagCB)
{
	m_childrenNodes.push_back(XMLDomCB());
	m_childrenNodes.back().m_strNodeName = strName;
	pTagCB = &m_childrenNodes.back();
}

void XMLDomCB::handle_text(const Core::String8& strText, const IO::StreamParser::Position& /*pos*/)
{
	m_strText += strText;
}

void XMLDomCB::handle_attribute(const Core::String8& strName, const Core::String8& strValue)
{
	m_nodeAttributes[strName] = strValue;
}

const IO::XMLDomCB* XMLDomCB::find_node(const Core::String8& strName) const
{
	Core::String8 strThisName;
	Core::String8 strChildName;
	FXD::U32 nDot = strName.find('.');
	if (nDot == Core::String8::npos)
	{
		strThisName = strName;
	}
	else
	{
		strThisName = strName.substr(0, nDot);
		strChildName = strName.substr(nDot + 1);
	}
	fxd_for(const auto& node, m_childrenNodes)
	{
		if (node.m_strNodeName == strThisName)
		{
			if (strChildName.not_empty())
			{
				const IO::XMLDomCB* pN = node.find_node(strChildName);
				if (pN != nullptr)
				{
					return pN;
				}
			}
			else
			{
				return &node;
			}
		}
	}
	return nullptr;
}

Core::String8 XMLDomCB::get_node_text(const Core::String8& strName) const
{
	const XMLDomCB* pNode = find_node(strName);
	if (pNode != nullptr)
	{
		return pNode->m_strText;
	}
	else
	{
		return Core::String8();
	}
}

FXD::U32 XMLDomCB::get_child_node_count(void) const
{
	return m_childrenNodes.size();
}

IO::XMLDomCB& XMLDomCB::get_child_node_at(FXD::U32 nID)
{
	return m_childrenNodes[nID];
}

const IO::XMLDomCB& XMLDomCB::get_child_node_at(FXD::U32 nID) const
{
	return m_childrenNodes[nID];
}