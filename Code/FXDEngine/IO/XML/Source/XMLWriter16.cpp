// Creator - MatthewGolder
#include "FXDEngine/IO/XML/XMLWriter.h"

using namespace FXD;
using namespace IO;

// ------
// XMLWriter16
// -
// ------
template <>
void XMLWriter16::XMLNode::write_indent(const Core::StreamOut& stream)
{
	FXD::U32 nDepth = 0;
	XMLNode* pParent = m_pParent;
	while (pParent != nullptr)
	{
		nDepth++;
		pParent = pParent->m_pParent;
		(*m_stream) << UTF_16("   ");
	}
}
template <>
XMLWriter16::XMLNode::XMLNode(const Core::StreamOut& stream, const Core::String16& strName, const Core::String16& strText, XMLWriter16::XMLNode* pParent, XMLWriter16::XMLAttribute** pAttrs, FXD::U32 nAttrCount, bool bNewline, bool bHasChildren)
	: m_pParent(pParent)
	, m_bNewline(bNewline)
	, m_bHasChildren(bHasChildren)
	, m_stream(stream)
	, m_strName(strName)
	, m_strText(strText)
{
	this->write_indent(m_stream);
	(*m_stream) << UTF_16("<") << m_strName;
	for (FXD::U32 n1 = 0; n1 < nAttrCount; n1++)
	{
		(*m_stream) << UTF_16(" ") << pAttrs[n1]->m_strName << UTF_16("=\"") << pAttrs[n1]->m_strValue << UTF_16("\"");
	}
	if (m_bHasChildren)
	{
		(*m_stream) << UTF_16(">");
	}
	else
	{
		(*m_stream) << UTF_16(" />");
	}
	if (m_bNewline)
	{
		(*m_stream) << UTF_16("\r\n");
	}
}

template <>
XMLWriter16::XMLNode::~XMLNode(void)
{
	if (!m_bHasChildren)
	{
		return;
	}
	if (m_bNewline)
	{
		write_indent(m_stream);
	}
	(*m_stream) << m_strText;
	(*m_stream) << UTF_16("</") << m_strName << UTF_16(">\r\n");
}