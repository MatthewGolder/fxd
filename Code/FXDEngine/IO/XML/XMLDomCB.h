// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_XML_XMLDOMCB_H
#define FXDENGINE_IO_XML_XMLDOMCB_H

#include "FXDEngine/Container/Map.h"
#include "FXDEngine/Container/Vector.h"
#include "FXDEngine/Core/NonCopyable.h"
#include "FXDEngine/IO/XML/XMLSaxParser.h"

#include <vector>

namespace FXD
{
	namespace IO
	{
		// ------
		// XMLDomCB
		// -
		// ------
		class XMLDomCB FINAL : public XMLSaxParser::ITagCB
		{
		public:
			static void parse(const Core::StreamIn& stream, IO::XMLDomCB& node);

		public:
			XMLDomCB(void);
			~XMLDomCB(void);

			void handle_tag(const Core::String8& strName, XMLSaxParser::ITagCB*& pTagCB) FINAL;
			void handle_text(const Core::String8& strText, const IO::StreamParser::Position& pos) FINAL;
			void handle_attribute(const Core::String8& strName, const Core::String8& strValue) FINAL;

			const IO::XMLDomCB* find_node(const Core::String8& strName) const;
			Core::String8 get_node_text(const Core::String8& strName) const;

			FXD::U32 get_child_node_count(void) const;
			IO::XMLDomCB& get_child_node_at(FXD::U32 nID);
			const IO::XMLDomCB& get_child_node_at(FXD::U32 nID) const;

		private:
			Core::String8 m_strNodeName;
			Core::String8 m_strText;
			Container::Map< Core::String8, Core::String8 > m_nodeAttributes;
			std::vector< IO::XMLDomCB > m_childrenNodes;
			//Container::Vector< IO::XMLDomCB > m_childrenNodes;
		};

	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_XML_XMLDOMCB_H
