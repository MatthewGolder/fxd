// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_XML_XMLWRITER_H
#define FXDENGINE_IO_XML_XMLWRITER_H

#include "FXDEngine/Core/NonCopyable.h"
#include "FXDEngine/Core/String.h"

namespace FXD
{
	namespace IO
	{
		template < typename CharType >
		class IXMLWriter;

		using XMLWriter8 = IO::IXMLWriter< FXD::UTF8 >;
		using XMLWriter16 = IO::IXMLWriter< FXD::UTF16 >;
		
		// ------
		// IXMLWriter
		// -
		// ------
		template < typename CharType >
		class IXMLWriter
		{
		public:
			// ------
			// XMLAttribute
			// -
			// ------
			class XMLAttribute FINAL : public Core::NonCopyable
			{
			public:
				XMLAttribute(const Core::StringBase< CharType >& strName, const Core::StringBase< CharType >& strValue)
					: m_strName(strName)
					, m_strValue(strValue)
				{}
				~XMLAttribute(void)
				{}
			
			public:
				Core::StringBase< CharType > m_strName;
				Core::StringBase< CharType > m_strValue;
			};

			
			// ------
			// XMLNode
			// -
			// ------
			class XMLNode FINAL : public Core::NonCopyable
			{
			public:
				XMLNode(const Core::StreamOut& stream, const Core::StringBase< CharType >& strName, const Core::StringBase< CharType >& strText, XMLNode* pParent = nullptr, XMLAttribute** pAttrs = nullptr, FXD::U32 nAttrCount = 0, bool bNewline = true, bool bHasChildren = true);
				~XMLNode(void);

				void write_indent(const Core::StreamOut& stream);

			public:
				XMLNode* m_pParent;
				bool m_bNewline;
				bool m_bHasChildren;
				Core::StreamOut m_stream;
				Core::StringBase< CharType > m_strName;
				Core::StringBase< CharType > m_strText;
			};			
		};

	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_XML_XMLWRITER_H
