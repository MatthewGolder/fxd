// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_FILECACHE_H
#define FXDENGINE_IO_FILECACHE_H

#include "FXDEngine/Container/Map.h"
#include "FXDEngine/Core/String.h"
#include "FXDEngine/Core/StringView.h"
#include "FXDEngine/IO/Types.h"
#include "FXDEngine/IO/Path.h"
#include "FXDEngine/Thread/RWLock.h"

namespace FXD
{
	namespace IO
	{
		// ------
		// IAsset
		// -
		// ------
		class IAsset : public Core::NonCopyable
		{
		public:
			IAsset(const Core::String8& strAssetName)
				: m_strAssetName(strAssetName)
			{}
			virtual ~IAsset(void)
			{}

			virtual bool is_platform_supported(void) const PURE;

			virtual const Core::StringView8 get_asset_type(void) const PURE;
			Core::String8 get_asset_name(void) const { return m_strAssetName; }

		protected:
			Core::String8 m_strAssetName;
		};

		// ------
		// FileCache
		// -
		// ------
		template < typename Asset >
		class FileCache FINAL : public Core::NonCopyable
		{
		public:
			using container_type = typename Thread::RWLockData< Container::Map< FXD::HashValue, Asset > >;

		public:
			FileCache(void)
			{}
			~FileCache(void)
			{
				clear();
			}

			template < typename FuncType, typename... ArgTypes >
			Asset load(const IO::Path& ioPath, FuncType&& inFunc, ArgTypes&&... inArgs)
			{
				FXD::STD::function< Asset(const IO::Path&, ArgTypes...) > func = inFunc;
				const FXD::HashValue hash = ioPath.string().to_hash();

				// 1. Check to see if the file is already open
				Asset asset;
				if (m_fileMap.get_quick_read_lock()->try_get_value(hash, asset))
				{
					return asset;
				}

				// 2. Open the file and add it to the map
				asset = func(ioPath, inArgs...);
				m_fileMap.get_quick_write_lock()->emplace(hash, asset);
				return asset;
			}

			void clear(void)
			{
				typename container_type::ScopedWriteLock writeLock(m_fileMap);
				while ((*writeLock).size())
				{
					(*writeLock).erase((*writeLock).begin());
				}
			}

			void remove_unique(void)
			{
				typename container_type::ScopedWriteLock writeLock(m_fileMap);
				for (auto itr = (*writeLock).begin(); itr != (*writeLock).end(); /**/)
				{
					if ((*itr).second.unique())
					{
						itr = (*writeLock).erase(itr);
					}
					else
					{
						++itr;
					}
				}
			}

		public:
			Thread::RWLockData< Container::Map< FXD::HashValue, Asset > > m_fileMap;
		};

	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_FILECACHE_H