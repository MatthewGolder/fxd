// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_IOFILEINFO_H
#define FXDENGINE_IO_IOFILEINFO_H

#include "FXDEngine/Core/Date.h"

namespace FXD
{
	namespace IO
	{
		// ------
		// FileInfo
		// -
		// ------
		class FileInfo
		{
		public:
			FileInfo(const Core::Date& creation = Core::Date(), const Core::Date& lastAccess = Core::Date(), const Core::Date& lastModify = Core::Date(), FXD::U64 nFileSize = 0);
			~FileInfo(void);

			GET_R(FXD::U64, nFileSize, file_size);
			GET_REF_R(Core::Date, creationDate, creation_date);
			GET_REF_R(Core::Date, lastAccessDate, last_access_date);
			GET_REF_R(Core::Date, lastModifyDate, last_modify_date);

		private:
			FXD::U64 m_nFileSize;
			Core::Date m_creationDate;
			Core::Date m_lastAccessDate;
			Core::Date m_lastModifyDate;
		};

	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_IOFILEINFO_H