// Creator - MatthewGolder
#include "FXDEngine/IO/IOSystem.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/IO/IODevice.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace IO;

// ------
// IIOSystem
// -
// ------
IIOSystem::IIOSystem(void)
{
}

IIOSystem::~IIOSystem(void)
{
}

Job::Future< bool > IIOSystem::create_system(void)
{
	auto funcRef = [&](void) { return _create_system(); };

	return Job::Async(FXDIO(), [=]() { return funcRef(); });
}

Job::Future< bool > IIOSystem::release_system(void)
{
	auto funcRef = [&](void) { return _release_system(); };

	return Job::Async(FXDIO(), [=]() { return funcRef(); });
}

bool IIOSystem::_create_system(void)
{
	m_ioDevice = std::make_shared< IO::IIODevice >();
	return true;
}

bool IIOSystem::_release_system(void)
{
	if (m_ioDevice)
	{
		m_ioDevice = IO::IODevice();
	}
	return true;
}

void IIOSystem::_process(FXD::F32 /*dt*/)
{
}
