// Creator - MatthewGolder
#include "FXDEngine/IO/Types.h"

#include "FXDEngine/IO/DirectoryIterator.h"
#include "FXDEngine/IO/Path.h"
#include "FXDEngine/App/Platform.h"

using namespace FXD;
using namespace IO;

// ------
// directory_iterator
// - 
// ------
directory_iterator::directory_iterator(void)
{
}
directory_iterator::directory_iterator(const IO::Path& path, const Core::String8& strPattern)
{
	m_internal = std::make_shared< IO::Internal::_directory_iterator >();
	m_internal->_open(path, strPattern);
	if (m_internal->is_good() == false)
	{
		m_internal.reset();
	}
}

directory_iterator::~directory_iterator(void)
{
}

directory_iterator::directory_iterator(const directory_iterator& rhs)
	: m_internal(rhs.m_internal)
{
}

directory_iterator::directory_iterator(directory_iterator&& rhs)
	: m_internal(std::move(rhs.m_internal))
{
}

directory_iterator& directory_iterator::operator=(const directory_iterator& rhs)
{
	if (this != &rhs)
	{
		m_internal = rhs.m_internal;
	}
	return (*this);
}

directory_iterator& directory_iterator::operator=(directory_iterator&& rhs) NOEXCEPT
{
	if (this != &rhs)
	{
		m_internal = std::move(rhs.m_internal);
	}
	return (*this);
}

directory_iterator& directory_iterator::operator++()
{
	return increment();
}

directory_iterator directory_iterator::operator++(int)
{
	directory_iterator tmp = (*this);
	++(*this);
	return (tmp);
}

const IO::DirectoryEntry& directory_iterator::operator*(void) const
{
	return m_internal->m_entry;
}

const IO::DirectoryEntry* directory_iterator::operator->(void) const
{
	return &m_internal->m_entry;
}

bool directory_iterator::operator==(const directory_iterator& rhs) const
{
	if (m_internal == rhs.m_internal)
	{
		return true;
	}
	if (rhs.m_internal == nullptr)
	{
		return (m_internal->m_entry == IO::DirectoryEntry());
	}
	if (m_internal == nullptr)
	{
		return (rhs.m_internal->m_entry == IO::DirectoryEntry());
	}
	return (m_internal->m_entry == rhs.m_internal->m_entry);
}

bool directory_iterator::operator!=(const directory_iterator& rhs) const
{
	return !(*this == rhs);
}

IO::directory_iterator& directory_iterator::increment(void)
{
	m_internal->_advance();
	return (*this);
}