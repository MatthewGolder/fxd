// Creator - MatthewGolder
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOSystem.h"

using namespace FXD;
using namespace IO;

// 0 - Not Initialised
// 1 - Initialised
// 2 - ShutDown
FXD::S32 g_nIOStatus = 0;

// ------
// IIOJPThread
// -
// ------
IIOJPThread::IIOJPThread(void)
	: IAsyncTaskPoolThread(display_name())
	, m_ioSystem(nullptr)
{
	if (g_nIOStatus == 2)
	{
		PRINT_ASSERT << "IO: Accessed after shutdown";
	}
}

IIOJPThread::~IIOJPThread(void)
{
	stop_and_wait_for_exit();
}

bool IIOJPThread::init_pool(void)
{
	IO::Funcs::RestrictIOThread();
	PRINT_INFO << "IO: IIOJPThread::init_pool()";

	m_ioSystem = std::make_unique< IO::IIOSystem >();
	m_timer.start_counter();
	g_nIOStatus = 1;
	m_shared.get_quick_write_lock()->m_eState = Job::E_AsyncState::Running;
	return true;
}

bool IIOJPThread::shutdown_pool(void)
{
	m_ioSystem = nullptr;
	IO::Funcs::UnrestrictIOThread();
	PRINT_INFO << "IO: IIOJPThread::shutdown_pool()";

	g_nIOStatus = 2;
	m_shared.get_quick_write_lock()->m_eState = Job::E_AsyncState::ShutDown;
	return true;
}

void IIOJPThread::idle_process(void)
{
	if (is_shuttingdown())
	{
		return;
	}

	const FXD::S32 nProcessFrequency = 10;

	// 1. Get initial time stamp.
	FXD::F32 ts1 = (FXD::F32)Core::Funcs::GetTimeSecsF();

	FXD::F32 dt = m_timer.elapsed_time();
	m_fps.update(dt);
	if (m_ioSystem)
	{
		m_ioSystem->_process(dt);
	}

	// 2. Get final time stamp and wait for next update interval.
	FXD::F32 ts2 = (FXD::F32)Core::Funcs::GetTimeSecsF();
	FXD::S32 nToWait = nProcessFrequency - (FXD::S32)((ts2 - ts1) * 1000.0f);
	if (nToWait > 0)
	{
		Thread::Funcs::Sleep(nToWait);
	}
}

bool IIOJPThread::is_thread_restricted(void) const
{
	return IO::Funcs::IsIOThreadRestricted();
}

// ------
// Funcs
// -
// ------
static FXD::S32 g_ContextThread = 0;
bool FXD::IO::Funcs::IsIOThreadRestricted(void)
{
	return (g_ContextThread != 0) && (g_ContextThread != Thread::Funcs::GetCurrentThreadID());
}

void FXD::IO::Funcs::RestrictIOThread(void)
{
	PRINT_COND_ASSERT((IO::Funcs::IsIOThreadRestricted() == false), "IO: Not allowed on current thread" << Thread::Funcs::GetCurrentThreadID());
	g_ContextThread = Thread::Funcs::GetCurrentThreadID();
}

void FXD::IO::Funcs::UnrestrictIOThread(void)
{
	PRINT_COND_ASSERT((IO::Funcs::IsIOThreadRestricted() == false), "IO: Not allowed on current thread" << Thread::Funcs::GetCurrentThreadID());
	g_ContextThread = 0;
}