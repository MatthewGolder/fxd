// Creator - MatthewGolder
#include "FXDEngine/IO/RecursiveDirectoryIterator.h"
#include "FXDEngine/IO/DirectoryEntry.h"
#include "FXDEngine/App/Platform.h"

using namespace FXD;
using namespace IO;

// ------
// recursive_directory_iterator
// - 
// ------
recursive_directory_iterator::recursive_directory_iterator(void) NOEXCEPT
	: m_bRecPending(false)
	, m_internal(nullptr)
{
}

recursive_directory_iterator::recursive_directory_iterator(const IO::Path& path, const Core::String8& strPattern)
	: m_bRecPending(true)
	, m_internal(nullptr)
{	
	Internal::_directory_iterator dir;
	dir._open(path, strPattern);
	if (dir.is_good() == false)
	{
		return;
	}

	m_internal = std::make_shared< IO::Internal::_recursive_directory_iterator >();
	m_internal->m_stack.push_back(std::move(dir));
}

recursive_directory_iterator::~recursive_directory_iterator(void)
{
}

recursive_directory_iterator::recursive_directory_iterator(const recursive_directory_iterator& rhs)
	: m_internal(rhs.m_internal)
	, m_bRecPending(rhs.m_bRecPending)
{
}

recursive_directory_iterator::recursive_directory_iterator(recursive_directory_iterator&& rhs)
	: m_internal(std::move(rhs.m_internal))
	, m_bRecPending(rhs.m_bRecPending)
{
}

recursive_directory_iterator& recursive_directory_iterator::operator=(const recursive_directory_iterator& rhs)
{
	if (this != &rhs)
	{
		m_internal = rhs.m_internal;
		m_bRecPending = rhs.m_bRecPending;
	}
	return (*this);
}

recursive_directory_iterator& recursive_directory_iterator::operator=(recursive_directory_iterator&& rhs) NOEXCEPT
{
	if (this != &rhs)
	{
		m_internal = std::move(rhs.m_internal);
		m_bRecPending = rhs.m_bRecPending;
	}
	return (*this);
}

recursive_directory_iterator& recursive_directory_iterator::operator++()
{
	return increment();
}

recursive_directory_iterator recursive_directory_iterator::operator++(int)
{
	recursive_directory_iterator tmp = (*this);
	++(*this);
	return (tmp);
}

const IO::DirectoryEntry& recursive_directory_iterator::operator*(void) const
{
	return m_internal->m_stack.back().m_entry;
}

const IO::DirectoryEntry* recursive_directory_iterator::operator->(void) const
{
	return (&*(*this));
}

bool recursive_directory_iterator::operator==(const recursive_directory_iterator& rhs) const
{
	return (m_internal == rhs.m_internal);
}

bool recursive_directory_iterator::operator!=(const recursive_directory_iterator& rhs) const
{
	return !(*this == rhs);
}

recursive_directory_iterator& recursive_directory_iterator::increment(void)
{
	if (recursion_pending())
	{
		if (_try_recursion())
		{
			return (*this);
		}
	}
	m_bRecPending = true;
	_advance();
	return (*this);
}

FXD::S32 recursive_directory_iterator::depth(void)const
{
	return (m_internal->m_stack.size() - 1);
}

void recursive_directory_iterator::pop(void)
{
	m_internal->m_stack.pop_back();
	if (m_internal->m_stack.size() == 0)
	{
		m_internal.reset();
	}
	else
	{
		_advance();
	}
}

bool recursive_directory_iterator::recursion_pending(void) const
{
	return m_bRecPending;
}

void recursive_directory_iterator::disable_recursion_pending(void)
{
	m_bRecPending = false;
}

bool recursive_directory_iterator::_try_recursion(void)
{
	IO::Internal::_directory_iterator& curr = m_internal->m_stack.back();

	bool rec_sym = false;// bool(options() & directory_options::follow_directory_symlink);
	bool skip_rec = false;
	if (!rec_sym)
	{
		FileStatus st(curr.m_entry.status());
		if (FS::IsSymlink(st) || FS::IsDirectory(st) == false)
		{
			skip_rec = true;
		}
	}
	else
	{
		FileStatus st(curr.m_entry.status());
		if (FS::IsDirectory(st) == false)
		{
			skip_rec = true;
		}
	}

	if (!skip_rec)
	{
		Internal::_directory_iterator dir;
		dir._open(curr.m_entry.path(), curr.m_strPattern);
		if (dir.is_good())
		{
			m_internal->m_stack.push_back(std::move(dir));
			return true;
		}
	}
	return false;
}

void recursive_directory_iterator::_advance(void)
{
	auto& stack = m_internal->m_stack;

	while (stack.size() > 0)
	{
		if (stack.back()._advance())
		{
			return;
		}
		stack.pop_back();
	}

	m_internal.reset();
}