// Creator - MatthewGolder
#include "FXDEngine/IO/IOStreamFile.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/Core/SerializeInBinary.h"
#include "FXDEngine/Core/SerializeOutBinary.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace IO;

static Core::ISerializeInBinary s_IBinary;
static Core::ISerializeOutBinary s_OBinary;
static Core::ISerializeOutString< FXD::UTF8 > s_OString;

// ------
// IStreamFile
// - 
// ------
bool IStreamFile::open(const IO::Path& ioPath, IO::FileOpenParams fileOpenParams, Core::CompressParams compressParams)
{
	auto funcRef = [&](const IO::Path& ioPath, IO::FileOpenParams fileOpenParams, Core::CompressParams compressParams)
	{
		switch (fileOpenParams.SerilizeMode)
		{
			case E_SerilizeMode::String:
			{
				this->_set_serialize_in(nullptr);
				this->_set_serialize_out(&s_OString);
				break;
			}
			case E_SerilizeMode::Binary:
			{
				this->_set_serialize_in(&s_IBinary);
				this->_set_serialize_out(&s_OBinary);
				break;
			}
			default:
			{
				this->_set_serialize_in(nullptr);
				this->_set_serialize_out(nullptr);
				break;
			}
		}
		this->_set_compressor_in(compressParams);
		this->_set_compressor_out(compressParams);

		return _open(ioPath, fileOpenParams, compressParams);
	};
	
	return FXD::Job::Async(FXD::FXDIO(), [=]()
	{
		return funcRef(ioPath, fileOpenParams, compressParams);
	}).get();
}

bool IStreamFile::close(void)
{
	auto funcRef = [&]()
	{
		return _close();
	};

	return Job::Async(FXDIO(), [=]()
	{
		return funcRef();
	}).get();
}

FXD::U64 IStreamFile::read_size(void* pData, FXD::U64 nSize)
{
	auto funcRef = [&](void* pData, FXD::U64 nSize)
	{
		return _read_size(pData, nSize);
	};

	return Job::Async(FXDIO(), [=]()
	{
		return funcRef(pData, nSize);
	}).get();
}

FXD::U64 IStreamFile::write_size(const void* pData, const FXD::U64 nSize)
{
	auto funcRef = [&](const void* pData, FXD::U64 nSize)
	{
		return _write_size(pData, nSize);
	};

	return Job::Async(FXDIO(), [=]()
	{
		return funcRef(pData, nSize);
	}).get();
}

FXD::U64 IStreamFile::length(void)
{
	auto funcRef = [&](void)
	{
		return _length();
	};

	return Job::Async(FXDIO(), [=]()
	{
		return funcRef();
	}).get();
}

FXD::S32 IStreamFile::tell(void)
{
	auto funcRef = [&](void)
	{
		return _tell();
	};

	return Job::Async(FXDIO(), [=]()
	{
		return funcRef();
	}).get();
}

FXD::U64 IStreamFile::seek_to(FXD::U64 nOffset, Core::E_SeekOffset eFrom)
{
	auto funcRef = [&](FXD::U64 nOffset, Core::E_SeekOffset eFrom)
	{
		return _seek_to(nOffset, eFrom);
	};

	return Job::Async(FXDIO(), [=]()
	{
		return funcRef(nOffset, eFrom);
	}).get();
}

IO::FileInfo IStreamFile::get_file_info(void)
{
	auto funcRef = [&](void)
	{
		return _get_file_info();
	};

	return Job::Async(FXDIO(), [=]()
	{
		return funcRef();
	}).get();
}

// ------
// FileInfo
// -
// ------
FileInfo::FileInfo(const Core::Date& creation, const Core::Date& lastAccess, const Core::Date& lastModify, FXD::U64 nFileSize)
	: m_nFileSize(nFileSize)
	, m_creationDate(creation)
	, m_lastAccessDate(lastAccess)
	, m_lastModifyDate(lastModify)
{
}

FileInfo::~FileInfo(void)
{
}

// ------
// Funcs
// - 
// ------
Thread::AtomicInt g_openFileCount(0);
const FXD::U32 FXD::IO::Funcs::GetOpenFileCount(void)
{
	return (const FXD::U32)g_openFileCount.get_instant_value();
}

void FXD::IO::Funcs::IncrementOpenFileCount(void)
{
	g_openFileCount.increment();
}

void FXD::IO::Funcs::DecrementOpenFileCount(void)
{
	g_openFileCount.decrement();
}