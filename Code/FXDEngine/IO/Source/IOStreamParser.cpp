// Creator - MatthewGolder
#include "FXDEngine/IO/IOStreamParser.h"
#include "FXDEngine/Memory/Memory.h"
#include "FXDEngine/Math/Math.h"

using namespace FXD;
using namespace IO;

// ------
// StreamParser::Position
// -
// ------
StreamParser::Position::Position(void)
	: m_nFileOffset(0)
	, m_nLine(1)
	, m_nColumn(1)
{
}

StreamParser::Position::Position(const Position& rhs)
	: m_nFileOffset(rhs.m_nFileOffset)
	, m_nLine(rhs.m_nLine)
	, m_nColumn(rhs.m_nColumn)
{
}

StreamParser::Position::~Position(void)
{
}

void StreamParser::Position::_advance_char(FXD::UTF8 ch)
{
	m_nFileOffset++;
	if (ch == '\n')
	{
		m_nLine++;
		m_nColumn = 1;
	}
	else if (ch != kEndOfFile)
	{
		m_nColumn++;
	}
}

void StreamParser::Position::_advance_by(FXD::U32 nAmount)
{
	m_nFileOffset += nAmount;
}

// ------
// StreamParser
// -
// ------
StreamParser::StreamParser(const Core::StreamIn& stream)
	: IStreamIn(this, Core::CompressParams::Default)
	, m_streamHandler(stream)
	, m_nBufferOffset(0)
	, m_nFileSize((FXD::U32)-1)
{
	FXD::U32 nRead = m_streamHandler->read_size(m_buffer, kBufferSize);
	if (nRead < kBufferSize)
	{
		m_nFileSize = nRead;
	}
}

StreamParser::~StreamParser(void)
{
}

FXD::U64 StreamParser::read_size(void* pDst, const FXD::U64 nSize)
{
	FXD::U64 nAmountRead = 0;
	FXD::U64 nSize2 = nSize;
	while ((nSize2 != 0) && (peek_next_char() != kEndOfFile))
	{
		FXD::U32 nCanRead = kBufferSize - m_nBufferOffset;
		if (nCanRead > nSize2)
		{
			nCanRead = nSize2;
		}
		if (m_nFileSize != (FXD::U64)-1)
		{
			if ((m_position.m_nFileOffset + nCanRead) > m_nFileSize)
			{
				nCanRead = (m_nFileSize - m_position.m_nFileOffset);
			}
		}
		Memory::MemCopy(pDst, &m_buffer[m_nBufferOffset], nCanRead);
		m_nBufferOffset += nCanRead;
		nSize2 -= nCanRead;
		pDst = (FXD::UTF8*)pDst + nCanRead;
		nAmountRead += nCanRead;
		m_position._advance_by(nCanRead);
		if (m_nBufferOffset == kBufferSize)
		{
			m_nBufferOffset = 0;
			FXD::U32 nRead = m_streamHandler->read_size(m_buffer, kBufferSize);
			if (nRead < kBufferSize)
			{
				m_nFileSize = nRead + m_position.m_nFileOffset;
			}
		}
	}
	return nAmountRead;
}

FXD::U64 StreamParser::length(void)
{
	PRINT_ASSERT << "IO: Not supported";
	return 0;
}

FXD::S32 StreamParser::tell(void)
{
	PRINT_ASSERT << "IO: Not supported";
	return 0;
}

FXD::U64 StreamParser::seek_to(FXD::U64 /*nOffset*/, Core::E_SeekOffset /*eFrom*/)
{
	PRINT_ASSERT << "IO: Not supported";
	return -1;
}

// Peek at the next character without advancing.
FXD::UTF8 StreamParser::peek_next_char(void) const
{
	if ((m_position.m_nFileOffset != (FXD::U64)-1) && (m_position.m_nFileOffset >= m_nFileSize))
	{
		return kEndOfFile;
	}
	return m_buffer[m_nBufferOffset];
}

// Get next character and advance.
FXD::UTF8 StreamParser::get_next_char(void)
{
	FXD::UTF8 ch = peek_next_char();
	_advance();
	return ch;
}

// Skip one or more characters.
void StreamParser::skip(FXD::U32 nCount)
{
	for (FXD::U32 c1 = 0; c1 < nCount; c1++)
	{
		_advance();
	}
}

// Set the position.
void StreamParser::set_parse_position(const StreamParser::Position& pos)
{
	FXD::U32 nCurBufLoc = m_position.m_nFileOffset & ~(kBufferSize - 1);
	FXD::U32 nNewBufLoc = pos.m_nFileOffset & ~(kBufferSize - 1);
	if (nCurBufLoc != nNewBufLoc)
	{
		m_streamHandler->seek_to(nNewBufLoc, Core::E_SeekOffset::Begin);
		m_streamHandler->read_size(m_buffer, kBufferSize);
	}
	m_nBufferOffset = pos.m_nFileOffset - nNewBufLoc;
	m_position = pos;
}

// Determine if the next character is whitespace.
bool StreamParser::is_next_whitespace(void) const
{
	const FXD::UTF8 ch = peek_next_char();
	return Core::CharTraits< FXD::UTF8 >::is_white_space(ch);
}

// Skip all whitespace.
void StreamParser::skip_whitespace(void)
{
	while (is_next_whitespace())
	{
		_advance();
	}
}

// Advance one place.
void StreamParser::_advance(void)
{
	const FXD::UTF8 ch = peek_next_char();
	m_position._advance_char(ch);
	if (++m_nBufferOffset == kBufferSize)
	{
		m_nBufferOffset = 0;
		FXD::U32 nRead = m_streamHandler->read_size(m_buffer, kBufferSize);
		if (nRead < kBufferSize)
		{
			m_nFileSize = nRead + m_position.m_nFileOffset;
		}
	}
}

bool IO::Parser::Identifier::operator()(StreamParser& pc, Core::String8& outStr) const
{
	FXD::UTF8 ch = pc.peek_next_char();
	bool bInvalid = (ch != '_') && (Core::CharTraits< FXD::UTF8 >::is_alpha(ch) == false);
	if (m_bXml)
	{
		bInvalid &= ((ch != ':') && (ch != '-'));
	}
	if (bInvalid)
	{
		return false;
	}

	Core::StringBuilder8 strStream;
	do
	{
		strStream << ch;
		pc.get_next_char();
		ch = pc.peek_next_char();
	} while (Core::CharTraits< FXD::UTF8 >::is_alpha_numeric(ch) || (ch == '_') || (((ch == ':') || (ch == '-')) && (m_bXml)));
	outStr += strStream.str();
	return true;
}

bool IO::Parser::QuotedString::operator()(StreamParser& pc, Core::String8& outStr) const
{
	outStr.clear();
	StreamParser::Position pos = pc.get_parse_position();
	if (pc.peek_next_char() != '"')
	{
		return false;
	}

	pc.get_next_char();
	Core::String8 str;
	bool bEscape = false;
	for (;;)
	{
		const FXD::UTF8 ch = pc.get_next_char();
		if (bEscape)
		{
			if (ch == 'n')
			{
				str += '\n';
			}
			else
			{
				str += ch;
			}
			bEscape = false;
		}
		else
		{
			if (ch == '"')
			{
				outStr += str;
				return true;
			}
			else if (ch == StreamParser::kEndOfFile)
			{
				pc.set_parse_position(pos);
				return false;
			}
			else if (ch == '\\')
			{
				bEscape = true;
			}
			else
			{
				str += ch;
			}
		}
	}
}

bool IO::Parser::FixedString::operator()(StreamParser& pc, const Core::String8& outStr) const
{
	StreamParser::Position pos = pc.get_parse_position();
	const FXD::UTF8* p = outStr.c_str();
	FXD::UTF8 ch;
	while ((ch = *p++) != 0)
	{
		if (ch != pc.get_next_char())
		{
			pc.set_parse_position(pos);
			return false;
		}
	}
	return true;
}


// Template specialisation: parse float.
template <>
bool IO::Parser::TypeParser< Core::String8 >::operator()(StreamParser& pc, Core::String8& strOutVal) const
{
	StreamParser::Position pos = pc.get_parse_position();
	pc.skip_whitespace();
	FXD::UTF8 ch = pc.peek_next_char();

	Core::String8 str;
	while ((ch != 0) && (Core::CharTraits< FXD::UTF8 >::is_space(ch) == false))
	{
		pc.get_next_char();
		str += ch;
		ch = pc.peek_next_char();
	};
	strOutVal = str;
	return true;
}

// Template specialisation: parse integer.
template <>
bool IO::Parser::TypeParser< FXD::S32 >::operator()(StreamParser& pc, FXD::S32& nOutVal) const
{
	StreamParser::Position pos = pc.get_parse_position();

	FXD::S32 nSign = 1;
	FXD::UTF8 ch = pc.get_next_char();
	if (ch == '-')
	{
		nSign = -1;
		ch = pc.get_next_char();
	}

	if (Core::CharTraits< FXD::UTF8 >::is_numeric(ch) == false)
	{
		pc.set_parse_position(pos);
		return false;
	}

	nOutVal = ch - '0';
	ch = pc.peek_next_char();
	while (Core::CharTraits< FXD::UTF8 >::is_numeric(ch))
	{
		pc.get_next_char();
		nOutVal *= 10;
		nOutVal += ch - '0';
		ch = pc.peek_next_char();
	}
	nOutVal *= nSign;

	if ((ch != 0)/*ParserContext::kEndOfFile*/ && (!Core::CharTraits< FXD::UTF8 >::is_space(ch)) && (!m_bAllowAnyNextChar))
	{
		// Floating point value?
		pc.set_parse_position(pos);
		return false;
	}
	return true;
}

// Template specialisation: parse double.
template <>
bool IO::Parser::TypeParser< FXD::F64 >::operator()(StreamParser& pc, FXD::F64& fOutVal) const
{
	StreamParser::Position pos = pc.get_parse_position();

	FXD::S32 nSign = 1;
	FXD::UTF8 ch = pc.get_next_char();
	if (ch == '-')
	{
		nSign = -1;
		// Check for -kInf
		if (ch == 'I')
		{
			if (pc.get_next_char() == 'N' && pc.get_next_char() == 'F')
			{
				// Technically ok, but warn
				*(FXD::S64*)(&fOutVal) = 0xfff0000000000000LL;
				PRINT_WARN << "IO: Invalid float point number -kInf read";
				return true;
			}
			else
			{
				pc.set_parse_position(pos);
				return false;
			}
		}
		ch = pc.get_next_char();
	}

	// Process whole units.
	if (Core::CharTraits< FXD::UTF8 >::is_numeric(ch))
	{
		fOutVal = ch - '0';
		ch = pc.peek_next_char();
		while (Core::CharTraits< FXD::UTF8 >::is_numeric(ch))
		{
			pc.get_next_char();
			fOutVal *= 10;
			fOutVal += ch - '0';
			ch = pc.peek_next_char();
		}
	}
	else if (ch == 'N')
	{
		if ((pc.get_next_char() == 'a') && (pc.get_next_char() == 'N'))
		{
			*(FXD::S64*)(&fOutVal) = 0x7ff0000000000002LL;
			// Technically ok, but warn
			PRINT_WARN << "IO: Invalid float point number NaN read";
			return true;
		}
		else
		{
			pc.set_parse_position(pos);
			return false;
		}
	}

	// Process fraction.
	if (ch == '.')
	{
		ch = pc.get_next_char();
		FXD::S32 divisor = 1;
		FXD::F64 fraction = 0;

		ch = pc.peek_next_char();
		while (Core::CharTraits< FXD::UTF8 >::is_numeric(ch))
		{
			pc.get_next_char();
			divisor *= 10;
			fraction *= 10;
			fraction += ch - '0';
			ch = pc.peek_next_char();
		}
		fraction /= FXD::F64(divisor);
		fOutVal += fraction;
	}

	// Process exponent.
	if (ch == 'e')
	{
		// skip 'e'
		ch = pc.get_next_char();
		FXD::S32 nMantissaSign = 1;

		// query sign
		ch = pc.peek_next_char();
		if ((ch == '-') || (ch == '+'))
		{
			if (ch == '-')
			{
				nMantissaSign = -1;
			}
			ch = pc.get_next_char();
			ch = pc.peek_next_char();
		}

		if (Core::CharTraits< FXD::UTF8 >::is_numeric(ch) == false)
		{
			pc.set_parse_position(pos);
			return false;
		}

		FXD::S32 nMantissa = (ch - '0');
		pc.get_next_char();

		ch = pc.peek_next_char();
		while (Core::CharTraits< FXD::UTF8 >::is_numeric(ch))
		{
			pc.get_next_char();
			nMantissa *= 10;
			nMantissa += ch - '0';
			ch = pc.peek_next_char();
		}
		fOutVal *= FXD::Math::Pow(10.0, (nMantissa * nMantissaSign));
	}

	// Finally, handle negative numbers.
	fOutVal *= nSign;
	if ((ch != 0)/*ParserContext::kEndOfFile*/ && (Core::CharTraits< FXD::UTF8 >::is_space(ch) == false))
	{
		// Invalid suffix on number?
		pc.set_parse_position(pos);
		return false;
	}
	return true;
}

// Template specialisation: parse float.
template <>
bool IO::Parser::TypeParser< FXD::F32 >::operator()(StreamParser& pc, FXD::F32& fOutVal) const
{
	FXD::F64 d;
	TypeParser< FXD::F64 > parser;
	if (parser(pc, d) == false)
		return false;

	fOutVal = static_cast<FXD::F32>(d);
	return true;
}