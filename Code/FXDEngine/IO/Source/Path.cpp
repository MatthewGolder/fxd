// Creator - MatthewGolder
#include "FXDEngine/IO/Path.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/DirectoryIterator.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace IO;

// ------
// Path
// -
// ------
bool Path::_ensure_created(void)const
{
	bool bSuccess = true;

	IO::Path path;
	fxd_for(const auto& itr, (*this))
	{
		if (IS_PATH_SEPERATOR(itr))
		{
			continue;
		}
		if (path.empty() == false)
		{
			path += PATH_SEPERATOR;
		}

		path += itr.c_str();

		if (IO::FS::DirectoryExists(path) == false)
		{
			bool bCreated = IO::FS::DirectoryCreate(path);
			if (bCreated == false)
			{
				bSuccess = false;
				PRINT_ASSERT << "IO::FS: DirectoryCreate failed";
			}
		}
	}
	return bSuccess;
}

bool Path::directory_exists(void) const
{
	auto funcRef = [&]() { return IO::FS::DirectoryExists(*this); };

	return Job::Async(FXDIO(), [=]() { return funcRef(); }).get();
}

bool Path::file_exists(void) const
{
	auto funcRef = [&]() { return IO::FS::FileExists(*this); };

	return Job::Async(FXDIO(), [=]() { return funcRef(); }).get();

}

bool Path::file_delete(void) const
{
	auto funcRef = [&]() { return IO::FS::FileDelete(*this); };

	return Job::Async(FXDIO(), [=]() { return funcRef(); }).get();
}

// ------
// FS
// - 
// ------
bool FXD::IO::FS::IsBlockFile(IO::FileStatus status) NOEXCEPT
{
	return (status.type() == IO::E_FileType::Block);
}

bool FXD::IO::FS::IsBlockFile(const IO::Path& path)
{
	return IO::FS::IsBlockFile(IO::FS::Status(path));
}

bool FXD::IO::FS::IsCharacterFile(IO::FileStatus status) NOEXCEPT
{
	return (status.type() == IO::E_FileType::Character);
}

bool FXD::IO::FS::IsCharacterFile(const IO::Path& path)
{
	return IO::FS::IsCharacterFile(IO::FS::Status(path));
}

bool FXD::IO::FS::IsDirectory(IO::FileStatus status) NOEXCEPT
{
	return (status.type() == IO::E_FileType::Directory);
}

bool FXD::IO::FS::IsDirectory(const IO::Path& path)
{
	return IO::FS::IsDirectory(IO::FS::Status(path));
}

bool FXD::IO::FS::IsFIFO(IO::FileStatus status) NOEXCEPT
{
	return (status.type() == IO::E_FileType::Fifo);
}

bool FXD::IO::FS::IsFIFO(const IO::Path& path)
{
	return IO::FS::IsFIFO(IO::FS::Status(path));
}

bool FXD::IO::FS::IsRegularFile(IO::FileStatus status) NOEXCEPT
{
	return (status.type() == IO::E_FileType::Regular);
}

bool FXD::IO::FS::IsRegularFile(const IO::Path& path)
{
	return IO::FS::IsRegularFile(IO::FS::Status(path));
}

bool FXD::IO::FS::IsSocket(IO::FileStatus status) NOEXCEPT
{
	return (status.type() == IO::E_FileType::Socket);
}

bool FXD::IO::FS::IsSocket(const IO::Path& path)
{
	return IO::FS::IsSocket(IO::FS::Status(path));
}

bool FXD::IO::FS::IsSymlink(IO::FileStatus status) NOEXCEPT
{
	return (status.type() == IO::E_FileType::Symlink);
}

bool FXD::IO::FS::IsSymlink(const IO::Path& path)
{
	return IO::FS::IsSymlink(IO::FS::Status(path));
}

bool FXD::IO::FS::StatusKnown(IO::FileStatus status) NOEXCEPT
{
	return (status.type() != IO::E_FileType::None);
}

bool FXD::IO::FS::StatusKnown(const IO::Path& path)
{
	return IO::FS::StatusKnown(IO::FS::Status(path));
}