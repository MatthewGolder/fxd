// Creator - MatthewGolder
#include "FXDEngine/IO/FileStatus.h"
#include <utility>

using namespace FXD;
using namespace IO;

// ------
// FileStatus
// -
// ------
FileStatus::FileStatus(void) NOEXCEPT
	: FileStatus(IO::E_FileType::None)
{}

FileStatus::FileStatus(IO::E_FileType type) NOEXCEPT
	: m_type(type)
{}

FileStatus::~FileStatus(void)
{}

FileStatus::FileStatus(const IO::FileStatus& rhs) NOEXCEPT
	: m_type(rhs.m_type)
{
}

FileStatus::FileStatus(IO::FileStatus&& rhs) NOEXCEPT
	: m_type(std::move(rhs.m_type))
{
}

IO::FileStatus& FileStatus::operator=(const IO::FileStatus& rhs) NOEXCEPT
{
	if (&rhs != this)
	{
		m_type = rhs.m_type;
	}
	return (*this);
}

IO::FileStatus& FileStatus::operator=(IO::FileStatus&& rhs) NOEXCEPT
{
	if (&rhs != this)
	{
		m_type = std::move(rhs.m_type);
	}
	return (*this);
}

IO::E_FileType FileStatus::type(void) const NOEXCEPT
{
	return m_type;
}

void FileStatus::type(IO::E_FileType type) NOEXCEPT
{
	m_type = type;
}