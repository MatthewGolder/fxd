// Creator - MatthewGolder
#include "FXDEngine/IO/DataRegistry.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Memory/StreamMemory.h"
#include "FXDEngine/Memory/StreamMemHandle.h"
#include "FXDEngine/Job/EventHandler.h"

using namespace FXD;
using namespace IO;

static FXD::F32 RegistryDataVersion = 0.01f;

// ------
// IDataRegistry::RWDataRegistry
// -
// ------
IDataRegistry::RWDataRegistry::RWDataRegistry(void)
	: m_nChangeCounter(0)
{
}

IDataRegistry::RWDataRegistry::~RWDataRegistry(void)
{
}


// ------
// IDataRegistry
// -
// ------
IDataRegistry::IDataRegistry(void)
{
}

IDataRegistry::~IDataRegistry(void)
{
}

void IDataRegistry::set_string8(const Core::String8& strName, const Core::String8& strValue)
{
	Thread::RWLockData< RWDataRegistry >::ScopedWriteLock rwData(m_rwData);
	_set_type(strName, strValue, rwData->m_strings8, rwData);
}

void IDataRegistry::set_string16(const Core::String8& strName, const Core::String16& strValue)
{
	Thread::RWLockData< RWDataRegistry >::ScopedWriteLock rwData(m_rwData);
	_set_type(strName, strValue, rwData->m_strings16, rwData);
}

void IDataRegistry::set_bool(const Core::String8& strName, bool bValue)
{
	Thread::RWLockData< RWDataRegistry >::ScopedWriteLock rwData(m_rwData);
	_set_type(strName, bValue, rwData->m_booleans, rwData);
}

void IDataRegistry::set_int(const Core::String8& strName, FXD::S64 nValue)
{
	Thread::RWLockData< RWDataRegistry >::ScopedWriteLock rwData(m_rwData);
	_set_type(strName, nValue, rwData->m_ints, rwData);
}

void IDataRegistry::set_float(const Core::String8& strName, FXD::F32 fValue)
{
	Thread::RWLockData< RWDataRegistry >::ScopedWriteLock rwData(m_rwData);
	_set_type(strName, fValue, rwData->m_floats, rwData);
}

void IDataRegistry::set_binary(const Core::String8& strName, const Memory::MemHandle& memValue)
{
	Thread::RWLockData< RWDataRegistry >::ScopedWriteLock rwData(m_rwData);
	_set_type(strName, memValue, rwData->m_binaryBlobs, rwData);
}

Core::String8 IDataRegistry::get_string8(const Core::String8& strName, const Core::String8& strDefault) const
{
	PRINT_COND_ASSERT(strName.length(), "IO: Zero-length game data name string specified");

	Thread::RWLockData< RWDataRegistry >::ScopedReadLock rwData(m_rwData);
	Core::String8 retStr;
	if (rwData->m_strings8.try_get_value(strName, retStr))
	{
		return retStr;
	}
	return strDefault;
}

Core::String16 IDataRegistry::get_string16(const Core::String8& strName, const Core::String16& strDefault) const
{
	PRINT_COND_ASSERT(strName.length(), "IO: Zero-length game data name string specified");

	Thread::RWLockData< RWDataRegistry >::ScopedReadLock rwData(m_rwData);
	Core::String16 retStr;
	if (rwData->m_strings16.try_get_value(strName, retStr))
	{
		return retStr;
	}
	return strDefault;
}

bool IDataRegistry::get_bool(const Core::String8& strName, bool bDefault) const
{
	PRINT_COND_ASSERT(strName.length(), "IO: Zero-length game data name string specified");

	Thread::RWLockData< RWDataRegistry >::ScopedReadLock rwData(m_rwData);
	bool retBool;
	if (rwData->m_booleans.try_get_value(strName, retBool))
	{
		return retBool;
	}
	return bDefault;
}

FXD::S64 IDataRegistry::get_int(const Core::String8& strName, FXD::S64 nDefault) const
{
	PRINT_COND_ASSERT(strName.length(), "IO: Zero-length game data name string specified");

	Thread::RWLockData< RWDataRegistry >::ScopedReadLock rwData(m_rwData);
	FXD::S64 retInt;
	if (rwData->m_ints.try_get_value(strName, retInt))
	{
		return retInt;
	}
	return nDefault;
}

FXD::F32 IDataRegistry::get_float(const Core::String8& strName, FXD::F32 fDefault) const
{
	PRINT_COND_ASSERT(strName.length(), "IO: Zero-length game data name string specified");

	Thread::RWLockData< RWDataRegistry >::ScopedReadLock rwData(m_rwData);
	FXD::F32 retFloat;
	if (rwData->m_floats.try_get_value(strName, retFloat))
	{
		return retFloat;
	}
	return fDefault;
}

Memory::MemHandle IDataRegistry::get_binary(const Core::String8& strName) const
{
	PRINT_COND_ASSERT(strName.length(), "IO: Zero-length game data name string specified");

	Thread::RWLockData< RWDataRegistry >::ScopedReadLock rwData(m_rwData);
	Memory::MemHandle retMem;
	if (rwData->m_binaryBlobs.try_get_value(strName, retMem))
	{
		return retMem;
	}
	return retMem;
}

void IDataRegistry::remove_all(const Core::String8& strStartingWith)
{
	Thread::RWLockData< RWDataRegistry >::ScopedWriteLock rwData(m_rwData);

	for (auto itr = rwData->m_booleans.begin(); itr != rwData->m_booleans.end(); /**/)
	{
		if (itr->first.find_no_case(strStartingWith) == 0)
		{
			itr = rwData->m_booleans.erase(itr);
			rwData->m_nChangeCounter++;
		}
		else
		{
			++itr;
		}
	}
	for (auto itr = rwData->m_ints.begin(); itr != rwData->m_ints.end(); /**/)
	{
		if (itr->first.find_no_case(strStartingWith) == 0)
		{
			itr = rwData->m_ints.erase(itr);
			rwData->m_nChangeCounter++;
		}
		else
		{
			++itr;
		}
	}
	for (auto itr = rwData->m_floats.begin(); itr != rwData->m_floats.end(); /**/)
	{
		if (itr->first.find_no_case(strStartingWith) == 0)
		{
			itr = rwData->m_floats.erase(itr);
			rwData->m_nChangeCounter++;
		}
		else
		{
			++itr;
		}
	}
	for (auto itr = rwData->m_strings8.begin(); itr != rwData->m_strings8.end(); /**/)
	{
		if (itr->first.find_no_case(strStartingWith) == 0)
		{
			itr = rwData->m_strings8.erase(itr);
			rwData->m_nChangeCounter++;
		}
		else
		{
			++itr;
		}
	}
	for (auto itr = rwData->m_strings16.begin(); itr != rwData->m_strings16.end(); /**/)
	{
		if (itr->first.find_no_case(strStartingWith) == 0)
		{
			itr = rwData->m_strings16.erase(itr);
			rwData->m_nChangeCounter++;
		}
		else
		{
			++itr;
		}
	}
	for (auto itr = rwData->m_binaryBlobs.begin(); itr != rwData->m_binaryBlobs.end(); /**/)
	{
		if (itr->first.find_no_case(strStartingWith) == 0)
		{
			itr = rwData->m_binaryBlobs.erase(itr);
			rwData->m_nChangeCounter++;
		}
		else
		{
			++itr;
		}
	}
}

bool IDataRegistry::exists(const Core::String8& strStartingWith) const
{
	Thread::RWLockData< RWDataRegistry >::ScopedReadLock rwData(m_rwData);

	fxd_for(const auto& itr, rwData->m_booleans)
	{
		if (itr.first.find_no_case(strStartingWith) == 0)
		{
			return true;
		}
	}

	fxd_for(const auto& itr, rwData->m_ints)
	{
		if (itr.first.find_no_case(strStartingWith) == 0)
		{
			return true;
		}
	}

	fxd_for(const auto& itr, rwData->m_floats)
	{
		if (itr.first.find_no_case(strStartingWith) == 0)
		{
			return true;
		}
	}

	fxd_for(const auto& itr, rwData->m_strings8)
	{
		if (itr.first.find_no_case(strStartingWith) == 0)
		{
			return true;
		}
	}

	fxd_for(const auto& itr, rwData->m_strings16)
	{
		if (itr.first.find_no_case(strStartingWith) == 0)
		{
			return true;
		}
	}

	fxd_for(const auto& itr, rwData->m_binaryBlobs)
	{
		if (itr.first.find_no_case(strStartingWith) == 0)
		{
			return true;
		}
	}
	return false;
}

const Job::Handler< IO::DataRegistryEvent >* IDataRegistry::add_handler(FXD::STD::function< void(IO::DataRegistryEvent) >&& func)
{
	PRINT_COND_ASSERT(func, "IO: No handler specified");

	Thread::RWLockData< RWDataRegistry >::ScopedWriteLock rwData(m_rwData);
	return FXD::FXDEvent()->attach< IO::DataRegistryEvent >(std::move(func));
}

void IDataRegistry::remove_handler(const Job::Handler< IO::DataRegistryEvent >* pHandler)
{
	PRINT_COND_ASSERT((pHandler != nullptr), "IO: No handler specified");

	Thread::RWLockData< RWDataRegistry >::ScopedWriteLock rwData(m_rwData);
	FXD::FXDEvent()->detach< IO::DataRegistryEvent >(pHandler);
}

FXD::U64 IDataRegistry::get_change_counter(void) const
{
	Thread::RWLockData< RWDataRegistry >::ScopedReadLock rwData(m_rwData);
	return rwData->m_nChangeCounter;
}

void IDataRegistry::_save_to_stream(Core::IStreamOut& ostr) const
{
	Thread::RWLockData< RWDataRegistry >::ScopedReadLock rwData(m_rwData);

	ostr << RegistryDataVersion;
	ostr << (FXD::U32)rwData->m_booleans.size();
	fxd_for(const auto& itr, rwData->m_booleans)
	{
		ostr << itr.first;
		ostr << itr.second;
	}

	ostr << (FXD::U32)rwData->m_ints.size();
	fxd_for(const auto& itr, rwData->m_ints)
	{
		ostr << itr.first;
		ostr << itr.second;
	}

	ostr << (FXD::U32)rwData->m_floats.size();
	fxd_for(const auto& itr, rwData->m_floats)
	{
		ostr << itr.first;
		ostr << itr.second;
	}

	ostr << (FXD::U32)rwData->m_strings8.size();
	fxd_for(const auto& itr, rwData->m_strings8)
	{
		ostr << itr.first;
		ostr << itr.second;
	}

	ostr << (FXD::U32)rwData->m_strings16.size();
	fxd_for(const auto& itr, rwData->m_strings16)
	{
		ostr << itr.first;
		ostr << itr.second;
	}

	ostr << (FXD::U32)rwData->m_binaryBlobs.size();
	fxd_for(const auto& itr, rwData->m_binaryBlobs)
	{
		ostr << itr.first;
		ostr << itr.second;
	}
	ostr << RegistryDataVersion;
}

void IDataRegistry::_load_from_stream(Core::IStreamIn& istr)
{
	Thread::RWLockData< RWDataRegistry >::ScopedWriteLock rwData(m_rwData);

	rwData->m_booleans.clear();
	rwData->m_ints.clear();
	rwData->m_floats.clear();
	rwData->m_strings8.clear();
	rwData->m_strings16.clear();
	rwData->m_binaryBlobs.clear();
	rwData->m_nChangeCounter = 0;

	// Validate.
	FXD::F32 fVersion = 0.0f;
	istr >> fVersion;
	if (fVersion != RegistryDataVersion)
	{
		PRINT_ASSERT << "IO: Start of DataRegistry does not match RegistryDataVersion";
		return;
	}

	FXD::U32 nBooleanCount = 0;
	istr >> nBooleanCount;
	for (FXD::U32 n1 = 0; n1 < nBooleanCount; n1++)
	{
		Core::String8 strName;
		bool bValue;
		istr >> strName;
		istr >> bValue;
		rwData->m_booleans[strName] = bValue;
		_raise_handlers(strName, rwData);
	}

	FXD::U32 nIntCount = 0;
	istr >> nIntCount;
	for (FXD::U32 n1 = 0; n1 < nIntCount; n1++)
	{
		Core::String8 strName;
		FXD::S64 nValue;
		istr >> strName;
		istr >> nValue;
		rwData->m_ints[strName] = nValue;
		_raise_handlers(strName, rwData);
	}

	FXD::U32 nFloatCount = 0;
	istr >> nFloatCount;
	for (FXD::U32 n1 = 0; n1 < nFloatCount; n1++)
	{
		Core::String8 strName;
		FXD::F32 fValue;
		istr >> strName;
		istr >> fValue;
		rwData->m_floats[strName] = fValue;
		_raise_handlers(strName, rwData);
	}

	FXD::U32 nStr8Count = 0;
	istr >> nStr8Count;
	for (FXD::U32 n1 = 0; n1 < nStr8Count; n1++)
	{
		Core::String8 strName;
		Core::String8 strValue;
		istr >> strName;
		istr >> strValue;
		rwData->m_strings8[strName] = strValue;
		_raise_handlers(strName, rwData);
	}

	FXD::U32 nStr16Count = 0;
	istr >> nStr16Count;
	for (FXD::U32 n1 = 0; n1 < nStr16Count; n1++)
	{
		Core::String8 strName;
		Core::String16 strValue;
		istr >> strName;
		istr >> strValue;
		rwData->m_strings16[strName] = strValue;
		_raise_handlers(strName, rwData);
	}

	FXD::U32 nMemoryCount = 0;
	istr >> nMemoryCount;
	for (FXD::U32 n1 = 0; n1 < nMemoryCount; n1++)
	{
		Core::String8 strName;
		Memory::MemHandle memValue;
		istr >> strName;
		istr >> memValue;
		rwData->m_binaryBlobs[strName] = memValue;
		_raise_handlers(strName, rwData);
	}
	istr >> fVersion;
	if (fVersion != RegistryDataVersion)
	{
		PRINT_ASSERT << "IO: End of DataRegistry does not match RegistryDataVersion";
		return;
	}
}

void IDataRegistry::_raise_handlers(const Core::String8& strName, Thread::RWLockData< RWDataRegistry >::ScopedWriteLock& /*rwData*/)
{
	FXD::FXDEvent()->raise(IO::DataRegistryEvent(strName));
}