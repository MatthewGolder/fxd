// Creator - MatthewGolder
#include "FXDEngine/IO/IODevice.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOStreamFile.h"
#include "FXDEngine/Job/Async.h"
#include "FXDEngine/Memory/StreamMemHandle.h"

using namespace FXD;
using namespace IO;

// ------
// IODevice
// - 
// ------
Job::Future< IO::StreamFile > IIODevice::file_open_stream(const IO::Path& ioPath, IO::FileOpenParams fileOpenParams, Core::CompressParams compressParams) const
{
	auto funcRef = [&](const IO::Path& ioPath, IO::FileOpenParams fileOpenParams, Core::CompressParams compressParams)
	{
		return _file_open_stream(ioPath, fileOpenParams, compressParams);
	};

	return Job::Async(FXDIO(), [=]()
	{
		return funcRef(ioPath, fileOpenParams, compressParams);
	});
}

Job::Future< Memory::StreamMemHandle > IIODevice::file_open_memory_stream(const IO::Path& ioPath, bool bIgnoreMissingFiles, Core::CompressParams compressParams) const
{
	auto funcRef = [&](const IO::Path& ioPath, bool bIgnoreMissingFiles, Core::CompressParams compressParams)
	{
		return _file_open_memory_stream(ioPath, bIgnoreMissingFiles, compressParams);
	};

	return Job::Async(FXDIO(), [=]()
	{
		return funcRef(ioPath, bIgnoreMissingFiles, compressParams);
	});
}

Job::Future< Memory::MemHandle > IIODevice::file_open_memhandle(const IO::Path& ioPath, bool bIgnoreMissingFiles, Core::CompressParams compressParams) const
{
	auto funcRef = [&](const IO::Path& ioPath, bool bIgnoreMissingFiles, Core::CompressParams compressParams)
	{
		return _file_open_memhandle(ioPath, bIgnoreMissingFiles, compressParams);
	};

	return Job::Async(FXDIO(), [=]()
	{
		return funcRef(ioPath, bIgnoreMissingFiles, compressParams);
	});
}

Job::Future< bool > IIODevice::load(IO::ILoadable* pLoadable, const IO::Path& ioPath, bool bIgnoreMissingFiles, Core::CompressParams compressParams) const
{
	auto funcRef = [&](::ILoadable* pLoadable, const IO::Path& ioPath, bool bIgnoreMissingFiles, Core::CompressParams compressParams)
	{
		return _load(pLoadable, ioPath, bIgnoreMissingFiles, compressParams);
	};

	return Job::Async(FXDIO(), [=]()
	{
		return funcRef(pLoadable, ioPath, bIgnoreMissingFiles, compressParams);
	});
}

Job::Future< bool > IIODevice::save(IO::ISaveable* pSaveable, const IO::Path& ioPath, bool bIgnoreMissingFiles, Core::CompressParams compressParams) const
{
	auto funcRef = [&](::ISaveable* pSaveable, const IO::Path& ioPath, bool bIgnoreMissingFiles, Core::CompressParams compressParams)
	{
		return _save(pSaveable, ioPath, bIgnoreMissingFiles, compressParams);
	};

	return Job::Async(FXDIO(), [=]()
	{
		return funcRef(pSaveable, ioPath, bIgnoreMissingFiles, compressParams);
	});
}

IO::StreamFile IIODevice::_file_open_stream(const IO::Path& ioPath, IO::FileOpenParams fileOpenParams, Core::CompressParams compressParams) const
{
	PRINT_COND_ASSERT(!IO::Funcs::IsIOThreadRestricted(), "IO: is not allowed on current thread");

	auto fileStream = std::make_shared< IO::IStreamFile >();
	if (!fileStream->open(ioPath, fileOpenParams, compressParams))
	{
		return nullptr;
	}
	return fileStream;
}

Memory::StreamMemHandle IIODevice::_file_open_memory_stream(const IO::Path& ioPath, bool bIgnoreMissingFiles, Core::CompressParams compressParams) const
{
	PRINT_COND_ASSERT(!IO::Funcs::IsIOThreadRestricted(), "IO: is not allowed on current thread");

	auto memHandle = _file_open_memhandle(ioPath, bIgnoreMissingFiles, compressParams);
	if (!memHandle.is_vaild())
	{
		return nullptr;
	}
	return std::make_shared< Memory::IStreamMemHandle >(memHandle);
}

Memory::MemHandle IIODevice::_file_open_memhandle(const IO::Path& ioPath, bool bIgnoreMissingFiles, Core::CompressParams compressParams) const
{
	PRINT_COND_ASSERT(!IO::Funcs::IsIOThreadRestricted(), "IO: is not allowed on current thread");

	auto fileStream = _file_open_stream(ioPath, { IO::E_FileMode::Read, IO::E_SerilizeMode::Binary, bIgnoreMissingFiles }, compressParams);
	if (fileStream)
	{
		return Memory::MemHandle::read_to_end((*fileStream), true);
	}
	return Memory::MemHandle();
}

bool IIODevice::_load(IO::ILoadable* pLoadable, const IO::Path& ioPath, bool bIgnoreMissingFiles, Core::CompressParams compressParams) const
{
	PRINT_COND_ASSERT(!IO::Funcs::IsIOThreadRestricted(), "IO: is not allowed on current thread");

	if ((!ioPath.file_exists()) || (pLoadable == nullptr))
	{
		return false;
	}
	
	auto fileStream = file_open_stream(ioPath, { IO::E_FileMode::Read, IO::E_SerilizeMode::Binary, bIgnoreMissingFiles }, compressParams).get();
	if (fileStream)
	{
		(*fileStream) >> (*pLoadable);
		return true;
	}
	return false;
}

bool IIODevice::_save(IO::ISaveable* pSaveable, const IO::Path& ioPath, bool bIgnoreMissingFiles, Core::CompressParams compressParams) const
{
	PRINT_COND_ASSERT(!IO::Funcs::IsIOThreadRestricted(), "IO: is not allowed on current thread");

	if (pSaveable == nullptr)
	{
		return false;
	}

	auto fileStream = file_open_stream(ioPath, { IO::E_FileMode::Write, IO::E_SerilizeMode::Binary, bIgnoreMissingFiles }, compressParams).get();
	if (fileStream)
	{
		(*fileStream) << (*pSaveable);
		return true;
	}
	return false;
}