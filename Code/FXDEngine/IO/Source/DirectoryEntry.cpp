// Creator - MatthewGolder
#include "FXDEngine/IO/DirectoryEntry.h"
#include "FXDEngine/IO/Path.h"

using namespace FXD;
using namespace IO;

// ------
// DirectoryEntry
// -
// ------
DirectoryEntry::DirectoryEntry(void) NOEXCEPT
{
}

DirectoryEntry::DirectoryEntry(const IO::Path& path)
	: m_path(path.c_str())
{
	refresh();
}

DirectoryEntry::~DirectoryEntry(void)
{
}

DirectoryEntry::DirectoryEntry(const DirectoryEntry& rhs)
	: m_path(rhs.m_path)
	, m_status(rhs.m_status)
{
}

DirectoryEntry::DirectoryEntry(DirectoryEntry&& rhs) NOEXCEPT
	: m_path(std::move(rhs.m_path))
	, m_status(std::move(rhs.m_status))
{
}

DirectoryEntry& DirectoryEntry::operator=(const DirectoryEntry& rhs)
{
	if (this != &rhs)
	{
		m_path = rhs.m_path;
		m_status = rhs.m_status;
	}
	return (*this);
}

DirectoryEntry& DirectoryEntry::operator=(DirectoryEntry&& rhs) NOEXCEPT
{
	if (this != &rhs)
	{
		m_path = std::move(rhs.m_path);
		m_status = std::move(rhs.m_status);
	}
	return (*this);
}

void DirectoryEntry::assign(const IO::Path& path)
{
	m_path = path;
	refresh();
}

void DirectoryEntry::replace_filename(const IO::Path& path)
{
	m_path.replace_filename(path);
	refresh();
}

bool DirectoryEntry::is_block_file(void)const
{
	return FS::IsBlockFile(m_status);
}

bool DirectoryEntry::is_character_file(void)const
{
	return FS::IsCharacterFile(m_status);
}

bool DirectoryEntry::is_directory(void)const
{
	return FS::IsDirectory(m_status);
}

bool DirectoryEntry::is_fifo(void)const
{
	return FS::IsFIFO(m_status);
}

bool DirectoryEntry::is_regular_file(void)const
{
	return FS::IsRegularFile(m_status);
}

bool DirectoryEntry::is_socket(void)const
{
	return FS::IsSocket(m_status);
}

bool DirectoryEntry::is_symlink(void)const
{
	return FS::IsSymlink(m_status);
}

void DirectoryEntry::refresh(void)
{
	m_status = IO::FS::Status(path());
}

const IO::Path& DirectoryEntry::path(void)const
{
	return m_path;
}

IO::FileStatus DirectoryEntry::status(void)const
{
	return m_status;
}