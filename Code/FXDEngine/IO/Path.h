// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_PATH_H
#define FXDENGINE_IO_PATH_H

#include "FXDEngine/Container/Iterator.h"
#include "FXDEngine/Core/String.h"
#include "FXDEngine/IO/FileType.h"
#include "FXDEngine/IO/FileStatus.h"

namespace FXD
{
	namespace IO
	{
		class directory_iterator;
		class zip_directory_iterator;

		namespace Internal
		{
			// ------
			// _path_iterator
			// -
			// ------
			template< typename PathType >
			class _path_iterator
			{
			public:
				using my_type = _path_iterator< PathType >;
				using path_type = PathType;
				using string_type = typename path_type::string_type;
				using value_type = path_type;
				using reference = const value_type&;
				using pointer = const value_type*;
				using difference_type = std::ptrdiff_t;
				using iterator_category = FXD::STD::bidirectional_iterator_tag;

			public:
				_path_iterator(void);
				_path_iterator(const path_type& path, size_t nOff);

				_path_iterator(const _path_iterator& rhs);
				_path_iterator(_path_iterator&& rhs) NOEXCEPT;

				my_type& operator=(const _path_iterator& rhs);
				my_type& operator=(_path_iterator&& rhs) NOEXCEPT;

				my_type& operator++();
				my_type operator++(int);

				my_type& operator--();
				my_type operator--(int);

				reference operator*(void)const;
				pointer operator->(void)const;

			private:
				void _get_val(void);

				// Non-member comparison functions
				// Equality
				template < typename PathType2 >
				friend bool operator==(const _path_iterator< PathType2 >& lhs, const _path_iterator< PathType2 >& rhs) NOEXCEPT;
				// Inequality
				template < typename PathType2 >
				friend bool operator!=(const _path_iterator< PathType2 >& lhs, const _path_iterator< PathType2 >& rhs) NOEXCEPT;

			public:
				const path_type* m_pType;	// pointer to full path
				path_type m_elem;				// current element subpath
				size_t m_nOffset;				// current offset in full path
			};

			// ------
			// _path_iterator
			// -
			// ------
			template < typename PathType >
			_path_iterator< PathType >::_path_iterator(void)
				: m_pType(nullptr)
				, m_nOffset(0)
			{}

			template < typename PathType >
			_path_iterator< PathType >::_path_iterator(const path_type& path, size_t nOff)
				: m_pType(&path)
				, m_nOffset(nOff)
			{
				_get_val();
			}

			template < typename PathType >
			_path_iterator< PathType >::_path_iterator(const _path_iterator& rhs)
				: m_pType(rhs.m_pType)
				, m_elem(rhs.m_elem)
				, m_nOffset(rhs.m_nOffset)
			{}

			template < typename PathType >
			_path_iterator< PathType >::_path_iterator(_path_iterator&& rhs) NOEXCEPT
				: m_pType(rhs.m_pType)
				, m_elem(std::move(rhs.m_elem))
				, m_nOffset(rhs.m_nOffset)
			{}

			template < typename PathType >
			typename _path_iterator< PathType >::my_type& _path_iterator< PathType >::operator=(const _path_iterator& rhs)
			{
				m_pType = rhs.m_pType;
				m_elem = rhs.m_elem;
				m_nOffset = rhs.m_nOffset;
				return (*this);
			}

			template < typename PathType >
			typename _path_iterator< PathType >::my_type& _path_iterator< PathType >::operator=(_path_iterator&& rhs) NOEXCEPT
			{
				m_pType = rhs.m_pType;
				m_elem = std::move(rhs.m_elem);
				m_nOffset = rhs.m_nOffset;
				return (*this);
			}

			template < typename PathType >
			typename _path_iterator< PathType >::my_type& _path_iterator< PathType >::operator++()
			{
				const size_t nPrefixEnd = m_pType->_prefix_end();
				const size_t nSize = m_pType->m_strPath.size();

				if (m_nOffset < nPrefixEnd)
				{
					m_nOffset = nPrefixEnd;	// point past "x:"
				}
				else if ((m_nOffset == nPrefixEnd) && (nPrefixEnd < nSize) && (IS_PATH_SEPERATOR(m_pType->m_strPath[nPrefixEnd]) == true))
				{
					// point past root "/" and following slashes
					for (++m_nOffset; m_nOffset < nSize; ++m_nOffset)
					{
						if (IS_PATH_SEPERATOR(m_pType->m_strPath[m_nOffset]) == false)
						{
							break;
						}
					}
				}
				else
				{
					// point past slashes followed by stuff
					for (; m_nOffset < nSize; ++m_nOffset)
					{
						if (IS_PATH_SEPERATOR(m_pType->m_strPath[m_nOffset]) == false)
						{
							break;
						}
					}
					for (; m_nOffset < nSize; ++m_nOffset)
					{
						if (IS_PATH_SEPERATOR(m_pType->m_strPath[m_nOffset]) == true)
						{
							break;
						}
					}
				}
				_get_val();
				return (*this);
			}

			template < typename PathType >
			typename _path_iterator< PathType >::my_type _path_iterator< PathType >::operator++(int)
			{
				const my_type tmp(*this);
				operator++();
				return tmp;
			}

			template < typename PathType >
			typename _path_iterator< PathType >::my_type& _path_iterator< PathType >::operator--()
			{
				const size_t nOffsetSav = m_nOffset;
				size_t nOffBack = 0;
				m_nOffset = 0;
				do
				{	// scan down to previous
					nOffBack = m_nOffset;
					++(*this);
				} while (m_nOffset < nOffsetSav);
				m_nOffset = nOffBack;
				_get_val();

				return (*this);
			}

			template < typename PathType >
			typename _path_iterator< PathType >::my_type _path_iterator< PathType >::operator--(int)
			{
				const my_type tmp(*this);
				operator--();
				return tmp;
			}

			template < typename PathType >
			typename _path_iterator< PathType >::reference _path_iterator< PathType >::operator*(void)const
			{
				return (m_elem);
			}

			template < typename PathType >
			typename _path_iterator< PathType >::pointer _path_iterator< PathType >::operator->(void)const
			{
				return &(operator*());
			}

			template < typename PathType >
			void _path_iterator< PathType >::_get_val(void)
			{
				string_type str;
				const size_t nPrefixEnd = m_pType->_prefix_end();
				const size_t nSize = m_pType->m_strPath.size();

				if (nSize > m_nOffset)
				{
					if (m_nOffset < nPrefixEnd)
					{
						str = m_pType->m_strPath.substr(0, nPrefixEnd);	// get "x:"
					}
					else if ((m_nOffset == nPrefixEnd) && (nPrefixEnd < nSize) && (IS_PATH_SEPERATOR(m_pType->m_strPath[nPrefixEnd]) == true))
					{
						str = FXD::PATH_SEPERATOR;	// get "/"
					}
					else
					{
						// determine next field as slashes followed by stuff
						size_t nSlash = 0;
						size_t nStuff = 0;

						for (; m_nOffset + nSlash < nSize; ++nSlash)
						{
							if (!IS_PATH_SEPERATOR(m_pType->m_strPath[m_nOffset + nSlash]))
							{
								break;
							}
						}
						for (; m_nOffset + nSlash + nStuff < nSize; ++nStuff)
						{
							if (IS_PATH_SEPERATOR(m_pType->m_strPath[m_nOffset + nSlash + nStuff]))
							{
								break;
							}
						}
						if (0 < nStuff)
						{
							str = m_pType->m_strPath.substr(m_nOffset + nSlash, nStuff);	// get "stuff"
						}
						else if (0 < nSlash)
						{
							str = FXD::PATH_PERIOD;	// get "."
						}
					}
				}
				m_elem = str;
			}

			// Non-member comparison functions
			// Equality
			template < typename PathType2 >
			inline bool operator==(const _path_iterator< PathType2 >& lhs, const _path_iterator< PathType2 >& rhs) NOEXCEPT
			{
				return ((lhs.m_pType == rhs.m_pType) && (lhs.m_nOffset == rhs.m_nOffset));
			}
			// Inequality
			template < typename PathType2 >
			inline bool operator!=(const _path_iterator< PathType2 >& lhs, const _path_iterator< PathType2 >& rhs) NOEXCEPT
			{
				return ((lhs.m_pType != rhs.m_pType) || (lhs.m_nOffset != rhs.m_nOffset));
			}
		} //namespace Internal

		// ------
		// Path
		// -
		// ------
		class Path
		{
		public:
			using value_type = FXD::UTF8;
			using string_type = FXD::Core::StringBase< value_type >;
			using string_view_type = FXD::Core::StringViewBase< value_type >;

			using iterator = Internal::_path_iterator< IO::Path >;
			using const_iterator = iterator;

			friend iterator;
			friend const_iterator;

		public:
			Path(void) NOEXCEPT;
			Path(string_type&& source);
			template< class Source >
			Path(const Source& source);
			template< class InputItr >
			Path(InputItr first, InputItr last);

			~Path(void);

			Path(const Path& rhs);
			Path(Path&& rhs) NOEXCEPT;

			Path& operator=(const Path& rhs);
			Path& operator=(Path&& rhs) NOEXCEPT;
			Path& operator=(string_type&& source);
			template< class Source >
			Path& operator=(const Source& source);

			Path& operator/=(const Path& rhs);
			template< class Source >
			Path& operator/=(const Source& source);
			template< class Source >
			Path& append(const Source& source);
			template< class InputItr >
			Path& append(InputItr first, InputItr last);

			template< class Source >
			Path& append_create(const Source& source);
			template< class InputItr >
			Path& append_create(InputItr first, InputItr last);

			Path& operator+=(const Path& path);
			Path& operator+=(const string_type& str);
			Path& operator+=(string_view_type str);
			Path& operator+=(const value_type* pStr);
			Path& operator+=(value_type val);
			template< class Source >
			Path& operator+=(const Source& source);
			template< class Source >
			Path& concat(const Source& source);
			template< class InputIt >
			Path& concat(InputIt first, InputIt last);
			
			Path root_name(void) const;
			Path root_directory(void) const;
			Path root_path(void) const;
			Path relative_path(void) const;
			Path parent_path(void) const;

			void clear(void);

			Path& remove_filename(void);
			Path& replace_filename(const Path& newName);
			Path& replace_extension(const Path& newExt = Path());

			bool directory_exists(void) const;
			bool file_exists(void) const;
			bool file_delete(void) const;
			bool empty(void) const;

			bool has_root_name(void) const;
			bool has_root_directory(void) const;
			bool has_root_path(void) const;
			bool has_relative_path(void) const;
			bool has_parent_path(void) const;
			bool has_filename(void) const;
			bool has_stem(void) const;
			bool has_extension(void) const;
			bool is_absolute(void) const;
			bool is_relative(void) const;

			string_type string(void) const;
			const value_type* c_str(void) const;
			operator string_type(void) const;
			Path filename(void) const;
			Path stem(void) const;
			Path extension(void) const;

			FXD::S32 compare(const IO::Path& path) const NOEXCEPT;
			FXD::S32 compare(const string_type& str) const;
			FXD::S32 compare(string_view_type str) const;
			FXD::S32 compare(const value_type* pStr) const;

			iterator begin(void) const;
			iterator end(void) const;

			friend bool operator==(const Path& lhs, const Path& rhs) NOEXCEPT;
			friend bool operator!=(const Path& lhs, const Path& rhs) NOEXCEPT;
			friend bool operator<(const Path& lhs, const Path& rhs) NOEXCEPT;
			friend bool operator<=(const Path& lhs, const Path& rhs) NOEXCEPT;
			friend bool operator>(const Path& lhs, const Path& rhs) NOEXCEPT;
			friend bool operator>=(const Path& lhs, const Path& rhs) NOEXCEPT;
			friend Path operator/(const Path& lhs, const Path& rhs);

		protected:
			bool _ensure_created(void) const;
			Path& _append(const string_type& str);
			FXD::S32 _compare(string_view_type str) const;
			FXD::U32 _prefix_end(void) const;
			FXD::U32 _root_end(void) const;

		protected:
			string_type m_strPath;
		};

		inline Path::Path(void) NOEXCEPT
		{
		}
		inline Path::Path(string_type&& strPath)
			: m_strPath(std::move(strPath))
		{
		}
		template< class Source >
		inline Path::Path(const Source& source)
			: m_strPath(source)
		{
		}
		template< class InputItr >
		inline Path::Path(InputItr first, InputItr last)
			: m_strPath(first, last)
		{
		}

		inline Path::~Path(void)
		{
		}

		inline Path::Path(const Path& rhs)
			: m_strPath(rhs.m_strPath)
		{
		}
		inline Path::Path(Path&& rhs) NOEXCEPT
			: m_strPath(std::move(rhs.m_strPath))
		{
		}

		inline Path& Path::operator=(const Path& rhs)
		{
			m_strPath = rhs.m_strPath;
			return (*this);
		}
		inline Path& Path::operator=(Path&& rhs) NOEXCEPT
		{
			m_strPath = std::move(rhs.m_strPath);
			return (*this);
		}
		inline Path& Path::operator=(string_type&& source)
		{
			m_strPath = std::move(source);
			return (*this);
		}
		template< class Source >
		inline Path& Path::operator=(const Source& source)
		{
			m_strPath = source;
			return (*this);
		}

		inline Path& Path::operator/=(const Path& rhs)
		{
			return append(rhs.string());
		}
		template< class Source >
		inline Path& Path::operator/=(const Source& source)
		{
			return append(IO::Path(source).string());
		}
		template< class Source >
		inline Path& Path::append(const Source& source)
		{
			return _append(IO::Path(source).string());
		}
		template< class InputItr >
		inline Path& Path::append(InputItr first, InputItr last)
		{
			return append(IO::Path(first, last).string());
		}
		template< class Source >
		inline Path& Path::append_create(const Source& source)
		{
			append(source);
			_ensure_created();
			return (*this);
		}
		template< class InputItr >
		inline Path& Path::append_create(InputItr first, InputItr last)
		{
			append(first, last);
			_ensure_created();
			return (*this);
		}

		inline Path& Path::operator+=(const Path& path)
		{
			m_strPath.append(path.c_str());
			return (*this);
		}
		inline Path& Path::operator+=(const string_type& str)
		{
			m_strPath.append(str);
			return (*this);
		}
		inline Path& Path::operator+=(string_view_type str)
		{
			m_strPath.append(str.c_str());
			return (*this);
		}
		inline Path& Path::operator+=(const value_type* pStr)
		{
			m_strPath.append(pStr);
			return (*this);
		}
		inline Path& Path::operator+=(value_type val)
		{
			m_strPath.append(val);
			return (*this);
		}
		template< class Source >
		inline Path& Path::operator+=(const Source& source)
		{
			return operator+=(Path(source));
		}
		template< class Source >
		inline Path& Path::concat(const Source& source)
		{
			return operator+=(Path(source));
		}
		template< class InputItr >
		inline Path& Path::concat(InputItr first, InputItr last)
		{
			return operator+=(Path(first, last));
		}

		inline Path Path::root_name(void) const
		{
			return m_strPath.substr(0, _prefix_end()).c_str();
		}

		inline Path Path::root_directory(void) const
		{	// get root directory
			FXD::U32 nID = _prefix_end();
			if (nID < m_strPath.size() && IS_PATH_SEPERATOR(m_strPath[nID]))
			{
				return (IO::Path(string_type(1, FXD::PATH_SEPERATOR)));
			}
			else
			{
				return IO::Path();
			}
		}

		inline Path Path::root_path(void) const
		{	// get root path
			return (IO::Path(string_type(m_strPath.c_str(), _root_end())));
		}

		inline Path Path::relative_path(void) const
		{	// get relative path
			FXD::U32 nID = _root_end();
			while (nID < m_strPath.size() && IS_PATH_SEPERATOR(m_strPath[nID]))
			{
				++nID;	// skip extra '/' after root
			}
			return (IO::Path(m_strPath.substr(nID)));
		}

		inline Path Path::parent_path(void) const
		{	// get parent (branch) path
			IO::Path retVal;
			if (!empty())
			{
				// append all but last element
				iterator itrEnd = --end();
				for (iterator itr = begin(); itr != itrEnd; ++itr)
				{
					retVal /= *itr;
				}
			}
			return (retVal);
		}

		inline void Path::clear(void)
		{
			m_strPath.clear();
		}

		inline Path& Path::remove_filename(void)
		{
			return (empty() ? IO::Path() : IO::Path(*--end()));
		}

		inline Path& Path::replace_filename(const Path& newFileName)
		{
			remove_filename();
			(*this) /= newFileName;
			return (*this);
		}

		inline Path& Path::replace_extension(const Path& newExt)
		{
			if ((newExt.empty()) || (newExt.c_str()[0] == FXD::PATH_PERIOD))
			{
				operator=(parent_path() / IO::Path((stem().string() + newExt.c_str())));
			}
			else
			{
				operator=(parent_path() / IO::Path((stem().string() + FXD::PATH_PERIOD + newExt.c_str())));
			}
			return (*this);
		}

		inline bool Path::empty(void) const
		{
			return m_strPath.empty();
		}

		inline bool Path::has_root_name(void) const
		{	// test if root name is nonempty
			return (!root_name().empty());
		}

		inline bool Path::has_root_directory(void) const
		{	// test if root directory is nonempty
			return (!root_directory().empty());
		}

		inline bool Path::has_root_path(void) const
		{	// test if root path is nonempty
			return (!root_path().empty());
		}

		inline bool Path::has_relative_path(void) const
		{	// test if relative path is nonempty
			return (!relative_path().empty());
		}

		inline bool Path::has_parent_path(void) const
		{	// test if parent (branch) path is nonempty
			return (!parent_path().empty());
		}

		inline bool Path::has_filename(void) const
		{	// test if filename (leaf) is nonempty
			return (!filename().empty());
		}

		inline bool Path::has_stem(void) const
		{	// test if stem (basename) is nonempty
			return (!stem().empty());
		}

		inline bool Path::has_extension(void) const
		{	// test if extension is nonempty
			return (!extension().empty());
		}

		inline bool Path::is_absolute(void) const
		{	// test if path is absolute
			return ((has_root_name()) && (has_root_directory()));
		}

		inline bool Path::is_relative(void) const
		{	// test if path is relative
			return (!is_absolute());
		}

		inline Path::string_type Path::string(void) const
		{
			return (m_strPath);
		}

		inline const Path::value_type* Path::c_str(void) const
		{
			return m_strPath.c_str();
		}

		inline Path::operator string_type(void) const
		{ // convert to file string
			return (m_strPath);
		}

		inline Path Path::filename(void) const
		{
			// get filename (leaf)
			return (empty() ? Path() : Path(*--end()));
		}

		inline Path Path::stem(void) const
		{// pick off stem (basename) in filename (leaf) before dot
			string_type str1 = filename();
			string_type str2 = extension();
			str1.resize(str1.size() - str2.size());
			return Path(str1);
		}

		inline Path Path::extension(void) const
		{ // pick off .extension in filename (leaf), including dot
			string_type str = filename();

			const size_t nIdx = str.rfind(FXD::PATH_PERIOD);

			return	(nIdx == string_type::npos) || 	/*No.*/
						(str.size() == 1) ||					/*Only.*/
						(str.size() == 2 && str[0] == FXD::PATH_PERIOD && str[1] == FXD::PATH_PERIOD)	/*only..*/ ? IO::Path() : IO::Path(str.substr(nIdx));
		}

		inline FXD::S32 Path::compare(const IO::Path& path) const NOEXCEPT
		{
			return _compare(path.c_str());
		}

		inline FXD::S32 Path::compare(const string_type& str) const
		{
			return _compare(str);
		}

		inline FXD::S32 Path::compare(string_view_type str) const
		{
			return _compare(str);
		}

		inline FXD::S32 Path::compare(const value_type* pStr) const
		{
			return _compare(pStr);
		}

		inline Path::iterator Path::begin(void)const
		{
			return (IO::Path::iterator(*this, (size_t)0));
		}

		inline Path::iterator Path::end(void) const
		{
			return (IO::Path::iterator(*this, m_strPath.size()));
		}
		
		inline Path& Path::_append(const string_type& str)
		{
			string_type strWork(str.begin(), str.end());
			if (strWork.empty())
			{
				return (*this);
			}
			strWork.find_replace(PATH_NOT_SEPERATOR, PATH_SEPERATOR);
			{
				IO::Path pathTest(strWork);
				if (pathTest.is_absolute())
				{
					return operator=(pathTest);
				}
			}

			if ((m_strPath.not_empty() == true) &&
				 (strWork.not_empty() == true) &&
				 (m_strPath.back() != PATH_COLON) &&
				 (IS_PATH_SEPERATOR(m_strPath.back()) == false) &&
				 (IS_PATH_SEPERATOR(strWork[0])) == false)
			{
				m_strPath.push_back(PATH_SEPERATOR);
			}

			m_strPath.append(strWork);
			return (*this);
		}

		inline FXD::S32 Path::_compare(string_view_type str) const
		{
			return m_strPath.compare(str.c_str());
		}

		inline FXD::U32 Path::_prefix_end(void) const
		{	// get end of prefix
			if ((2 < m_strPath.size()) &&
				 IS_PATH_SEPERATOR(m_strPath[0] == true) &&
				 IS_PATH_SEPERATOR(m_strPath[1] == true) &&
				 IS_PATH_SEPERATOR(m_strPath[2] == false))
			{	// network name, pick off \\name
				FXD::U32 nID = 3;
				for (; nID < m_strPath.size() && !IS_PATH_SEPERATOR(m_strPath[nID]); ++nID)
				{
				}
				return nID;
			}
			else
			{	// no network name, pick off drive:
				FXD::U32 nID = m_strPath.find(':', 0);
				(nID == m_strPath.npos) ? nID = 0 : ++nID;
				return nID;
			}
		}

		inline FXD::U32 Path::_root_end(void) const
		{	// get end of root
			FXD::U32 nID = _prefix_end();
			if (nID < m_strPath.size() && IS_PATH_SEPERATOR(m_strPath[nID]))
			{
				++nID;
			}
			return (nID);
		}

		inline bool operator==(const Path& lhs, const Path& rhs) NOEXCEPT
		{
			return lhs.compare(rhs) == 0;
		}
		inline bool operator!=(const Path& lhs, const Path& rhs) NOEXCEPT
		{
			return lhs.compare(rhs) != 0;
		}
		inline bool operator<(const Path& lhs, const Path& rhs) NOEXCEPT
		{
			return lhs.compare(rhs) < 0;
		}
		inline bool operator<=(const Path& lhs, const Path& rhs) NOEXCEPT
		{
			return lhs.compare(rhs) <= 0;
		}
		inline bool operator>(const Path& lhs, const Path& rhs) NOEXCEPT
		{
			return lhs.compare(rhs) > 0;
		}
		inline bool operator>=(const Path& lhs, const Path& rhs) NOEXCEPT
		{
			return lhs.compare(rhs) >= 0;
		}

		inline Path operator/(const Path& lhs, const Path& rhs)
		{
			IO::Path pathRet = lhs;
			return (pathRet /= rhs);
		}

		// IStreamOut
		inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, Path const& rhs)
		{
			return ostr << rhs.c_str();
		}

		namespace FS
		{
			extern bool DirectoryCreate(const IO::Path& path);
			extern bool DirectoryDelete(const IO::Path& path);
			extern bool DirectoryExists(const IO::Path& path);

			extern bool FileRemame(const IO::Path& pathOld, const IO::Path& pathNew);
			extern bool FileCopy(const IO::Path& pathOld, const IO::Path& pathNew);
			extern bool FileDelete(const IO::Path& path);
			extern bool FileExists(const IO::Path& path);

			extern IO::Path GetCWD(void);															// C:\Media\Code\FXD\Code\SampleProject
			extern IO::Path GetTempDirectory(void);											// C:\Users\Matthew\AppData\Local\Temp
			extern IO::Path GetModuleDirectory(void);											// C:\Media\Code\FXD\Output\x64\Debug\SampleProject\out
			extern IO::Path GetUtilityDirectory(Core::StringView8 strFilePath);		// C:\Users\Matthew\AppData\Local\FXDTest
			extern IO::Path GetPathFromEnviroment(Core::StringView8 strFilePath);	//

			extern bool IsBlockFile(IO::FileStatus status) NOEXCEPT;
			extern bool IsBlockFile(const IO::Path& path);
			extern bool IsCharacterFile(IO::FileStatus status) NOEXCEPT;
			extern bool IsCharacterFile(const IO::Path& path);
			extern bool IsDirectory(IO::FileStatus status) NOEXCEPT;
			extern bool IsDirectory(const IO::Path& path);
			extern bool IsFIFO(IO::FileStatus status) NOEXCEPT;
			extern bool IsFIFO(const IO::Path& path);
			extern bool IsRegularFile(IO::FileStatus status) NOEXCEPT;
			extern bool IsRegularFile(const IO::Path& path);
			extern bool IsSocket(IO::FileStatus status) NOEXCEPT;
			extern bool IsSocket(const IO::Path& path);
			extern bool IsSymlink(IO::FileStatus status) NOEXCEPT;
			extern bool IsSymlink(const IO::Path& path);
			extern bool StatusKnown(IO::FileStatus status) NOEXCEPT;
			extern bool StatusKnown(const IO::Path& path);
			extern IO::FileStatus Status(const IO::Path& path);

		} //namespace FS

	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_IOPATH_H