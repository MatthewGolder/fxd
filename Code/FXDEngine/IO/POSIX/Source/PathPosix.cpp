// Creator - MatthewGolder
#include "FXDEngine/IO/Types.h"

#if IsSystemPosix()
#include "FXDEngine/IO/Path.h"
#include "FXDEngine/App/Platform.h"

#if IsOSPC()
#	include <sys/stat.h>
#	include <direct.h>
#	include <process.h>
#	define getcwd _getcwd // stupid MSFT "deprecation" warning
#elif IsOSAndroid()
#	include <unistd.h>
#	include <sys/stat.h>
#	include <errno.h>
#	include <stdlib.h>
#	include "FXDEngine/App/Android/GlobalsAndroid.h"
#	include "FXDEngine/App/Platform.h"
#	include <android/asset_manager.h> 
#	include "FXDEngine/IO/IOJPThread.h"
#	include "FXDEngine/IO/IOSystem.h"
#	include "FXDEngine/IO/IODevice.h"
#	include "FXDEngine/App/FXDApi.h"
#	include "FXDEngine/IO/IOStreamFile.h"
#endif

using namespace FXD;
using namespace IO;

// ------
// FS
// -
// ------
bool FXD::IO::FS::DirectoryCreate(const IO::Path& path)
{
	if (IO::FS::DirectoryExists(path))
	{
		return true;
	}

	FXD::S32 nRetVal = 0;
#if IsOSPC()
	nRetVal = ::mkdir(path.c_str());
#else
	nRetVal = ::mkdir(path.c_str(), S_IRWXU);
#endif
	return (nRetVal == 0);
}

bool FXD::IO::FS::DirectoryDelete(const IO::Path& path)
{
	if (!IO::FS::DirectoryExists(path))
	{
		return true;
	}
	return false;
}

bool FXD::IO::FS::DirectoryExists(const IO::Path& path)
{
	struct stat sb;
	bool bRetVal = (stat(path.c_str(), &sb) == 0);
	return bRetVal;
}

bool FXD::IO::FS::FileRemame(const IO::Path& pathOld, const IO::Path& pathNew)
{
	FXD::S32 nRetVal = ::rename(pathOld.c_str(), pathNew.c_str());
	return (nRetVal == 0);
}

bool FXD::IO::FS::FileCopy(const IO::Path& pathOld, const IO::Path& pathNew)
{
	FILE* pFileOld = fopen(pathOld.c_str(), "r");
	if (pFileOld == 0)
	{
		fclose(pFileOld);
		return false;
	}

	FILE* pFileNew = fopen(pathNew.c_str(), "w");
	if (pFileNew == 0)
	{
		fclose(pFileOld);
		fclose(pFileNew);
		return false;
	}

	FXD::U8 buffer[BUFSIZ];
	size_t nSize = 0;
	while ((nSize = fread(buffer, sizeof(FXD::U8), sizeof(buffer), pFileOld)) > 0)
	{
		if (fwrite(buffer, sizeof(FXD::U8), nSize, pFileNew) != nSize)
		{
			fclose(pFileOld);
			fclose(pFileNew);
			return false;
		}
	}
	fclose(pFileOld);
	fclose(pFileNew);
	return true;
}

bool FXD::IO::FS::FileDelete(const IO::Path& path)
{
	FXD::S32 nRetVal = ::remove(path.c_str());
	return (nRetVal == 0);
}

bool FXD::IO::FS::FileExists(const IO::Path& path)
{
	struct stat sb;
	bool bRetVal = (stat(path.c_str(), &sb) == 0);
	return bRetVal;
}

IO::Path FXD::IO::FS::GetCWD(void)
{
	//ANativeActivity* pActivity = FXD::App::GetGlobalData().m_pActivity;

	FXD::UTF8 pChar[256];
	bool bRetVal = (::getcwd(pChar, sizeof(pChar)) != nullptr);
	if (bRetVal == false)
	{
		return Path(UTF_8(""));
	}
	return Path(pChar);
}

IO::Path FXD::IO::FS::GetTempDirectory(void)
{
	//ANativeActivity* pActivity = FXD::App::GetGlobalData().m_pActivity;

	{
		const FXD::UTF8* pFolder = getenv("TMPDIR");
		if (pFolder != nullptr)
		{
			return Path(pFolder).append(App::GetGameNameUTF8(), true);
		}
	}
	{
		const FXD::UTF8* pFolder = getenv("TMP");
		if (pFolder != nullptr)
		{
			return Path(pFolder).append(App::GetGameNameUTF8(), true);
		}
	}
	{
		FXD::UTF8* pFolder = getenv("TEMP");
		if (pFolder != nullptr)
		{
			return Path(pFolder).append(App::GetGameNameUTF8(), true);
		}
	}
	{
		FXD::UTF8* pFolder = getenv("TEMPDIR");
		if (pFolder != nullptr)
		{
			return Path(pFolder).append(App::GetGameNameUTF8(), true);
		}
	}
	return IO::Path(UTF_8(""));
}

IO::Path FXD::IO::FS::GetModuleDirectory(void)
{
	//ANativeActivity* pActivity = FXD::App::GetGlobalData().m_pActivity;

#if IsOSPC()
	FXD::UTF8* pTempStrBuf;
	errno_t err = _get_pgmptr(&pTempStrBuf);
	if (err != 0)
	{
		return UTF_8("");
	}

	Core::String8 str(pTempStrBuf);
	FXD::U32 nId = str.rfind_no_case(FXD::PATH_SEPERATOR);

	Core::String8 strPath(str, 0, nId);
	return Path(strPath);
#elif IsOSAndroid()
	/*
	char arg1[20];
	char exepath[PATH_MAX + 1] = { 0 };

	sprintf(arg1, "/proc/%d/exe", getpid());
	readlink(arg1, exepath, 1024);
	return string(exepath);
	*/
	return IO::Path();
#endif
}

IO::Path FXD::IO::FS::GetUtilityDirectory(Core::StringView8 strFilePath)
{
	/*
	ANativeActivity* pActivity = FXD::App::GetGlobalData().m_pActivity;

	{
		Path p = Path("/data/local/tmp");
		p.append("test.txt");

		bool b = FS::FileExists(p);
		{
			Core::StreamOut fileStream = FXD::FXDIO()->io_system()->io_device()->file_open_stream(p, IO::E_FileMode::Write, IO::E_SerilizeMode::String, true).get();
			(*fileStream) << "asasasa";
		}
	}


	{
		AAssetDir* assetDir = AAssetManager_openDir(pActivity->assetManager, "");
		const char* filename;
		while ((filename = AAssetDir_getNextFileName(assetDir)) != NULL)
		{
			PRINT_INFO << filename;
		}
		AAssetDir_close(assetDir);

		Path pp = Path(pActivity->externalDataPath);

		fxd_for_io(itr, pp, UTF_8("*"))
		{
			IO::E_FileType ePathType = itr.get_path().path_type();
			const FXD::UTF8* pChr = itr.get_path().c_str();
			PRINT_INFO << pChr;
		}
	}

	FXD::UTF8 pChar[256];
	FXD::UTF8* pFolder = getenv("LOCALAPPDATA");
	if (pFolder != nullptr)
	{
		IO::Path ioPath = Path(pFolder);
		ioPath.append(App::GetGameNameUTF8(), true);
		ioPath.append(strFilePath, true);
;		return ioPath;
	}
	*/
	return IO::Path(UTF_8(""));
}

IO::Path FXD::IO::FS::GetPathFromEnviroment(Core::StringView8 strFilePath)
{
	FXD::UTF8 pChar[256];
	FXD::UTF8* pFolder = getenv(strFilePath.c_str());
	if (pFolder != nullptr)
	{
		return Path(pFolder);
	}
	return IO::Path(UTF_8(""));
}

IO::E_FileType _Status(IO::Path const& p, const struct stat& pathStat)
{
	/*
	if (pathStat.st_mode & S_IFLNK)
	{
		return IO::E_FileType::symlink;
	}
	*/
	if (pathStat.st_mode & S_IFREG)
	{
		return IO::E_FileType::Regular;
	}
	else if (pathStat.st_mode & S_IFDIR)
	{
		return IO::E_FileType::Directory;
	}
	/*
	else if (pathStat.st_mode & S_IFBLK)
	{
		return IO::E_FileType::block;
	}
	*/
	else if (pathStat.st_mode & S_IFCHR)
	{
		return IO::E_FileType::Character;
	}
	else if (pathStat.st_mode & S_IFIFO)
	{
		return IO::E_FileType::Fifo;
	}
	/*
	else if (pathStat.st_mode & S_IFSOCK)
	{
		return IO::E_FileType::Socket;
	}
	*/
	else
	{
		return IO::E_FileType::Unknown;
	}
}

IO::FileStatus FXD::IO::FS::Status(const IO::Path& path)
{
	struct stat pathStat;
	if (::stat(path.c_str(), &pathStat) == -1)
	{
		switch (errno)
		{
			case ENOENT:
			case ENOTDIR:
			{
				return IO::FileStatus(IO::E_FileType::NotFound);
			}
			default:
			{
				break;
			}
		}
	}
	return IO::FileStatus(_Status(path, pathStat));
}
#endif //IsSystemPosix()