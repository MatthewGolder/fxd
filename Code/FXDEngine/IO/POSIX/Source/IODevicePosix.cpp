// Creator - MatthewGolder
#include "FXDEngine/IO/Types.h"

#if IsSystemPosix()
#include "FXDEngine/IO/IODevice.h"

using namespace FXD;
using namespace IO;

// ------
// Core::Impl::Impl< IIODevice >
// -
// ------
template <>
class Core::Impl::Impl< IIODevice >
{
public:
	Impl(void)
	{}
	~Impl(void)
	{}
};

// ------
// IIODevice
// - 
// ------
IIODevice::IIODevice(void)
{}

IIODevice::~IIODevice(void)
{}
#endif //IsSystemPosix()