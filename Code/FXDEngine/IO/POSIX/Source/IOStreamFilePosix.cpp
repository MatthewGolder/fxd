// Creator - MatthewGolder
#include "FXDEngine/IO/Types.h"

#if IsSystemPosix()
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOStreamFile.h"

#include <sys/stat.h>

using namespace FXD;
using namespace IO;

// ------
// Core::Impl::Impl< IStreamFile >
// -
// ------
template <>
class Core::Impl::Impl< IStreamFile >
{
public:
	friend class IStreamFile;

public:
	Impl(void)
		: m_pFile(nullptr)
	{}
	~Impl(void)
	{}

public:
	FILE* m_pFile;
};


// ------
// IStreamFile
// - 
// ------
IStreamFile::IStreamFile(void)
	: IStreamOut(nullptr, Core::CompressParams::Default)
	, IStreamIn(nullptr, Core::CompressParams::Default)
{
}

IStreamFile::IStreamFile(const IO::Path& ioPath, IO::FileOpenParams fileOpenParams, Core::CompressParams compressParams)
	: IStreamFile()
{
	open(ioPath, fileOpenParams, compressParams);
}

IStreamFile::~IStreamFile(void)
{
	if (m_impl->m_pFile != nullptr)
	{
		close();
	}
}

bool IStreamFile::_open(const IO::Path& ioPath, IO::FileOpenParams fileOpenParams, Core::CompressParams compressParams)
{
	PRINT_COND_ASSERT(!IO::Funcs::IsIOThreadRestricted(), "IO: is not allowed on current thread");

	// 1.1 Attempt to open
	const FXD::UTF8* pName = ioPath.c_str();
	switch (fileOpenParams.FileMode)
	{
		case E_FileMode::Read:
		{
			m_impl->m_pFile = fopen(pName, "rb");
			break;
		}
		case E_FileMode::Write:
		{
			m_impl->m_pFile = fopen(pName, "wb");
			break;
		}
		case E_FileMode::Append:
		{
			m_impl->m_pFile = fopen(pName, "a");
			break;
		}
		default:
		{
			PRINT_ASSERT << "IO: Invalid file open mode";
		}
	}

	// 1.2 Handle if the file has opened
	if ((m_impl->m_pFile == nullptr) && (fileOpenParams.IgnoreMissingFiles))
	{
		return false;
	}
	PRINT_COND_ASSERT((m_impl->m_pFile != nullptr), "IO: The file '" << pName << "' could not be opened");

	// 1.3 Move to file end if we are appending and set local variables
	if (fileOpenParams.FileMode == E_FileMode::Append)
	{
		seek_end();
	}
	m_ioPath = ioPath;
	IO::Funcs::IncrementOpenFileCount();
	return true;
}

bool IStreamFile::_close(void)
{
	PRINT_COND_ASSERT(!IO::Funcs::IsIOThreadRestricted(), "IO: is not allowed on current thread");

	FXD::S32 nRetVal = 0;
	if (m_impl->m_pFile != nullptr)
	{
		nRetVal = fclose(m_impl->m_pFile);
		m_impl->m_pFile = nullptr;
		IO::Funcs::DecrementOpenFileCount();
	}
	return (nRetVal == 0);
}

FXD::U64 IStreamFile::_read_size(void* pData, FXD::U64 nSize)
{
	PRINT_COND_ASSERT(!IO::Funcs::IsIOThreadRestricted(), "IO: is not allowed on current thread");

	FXD::U64 nBytesRead = fread(pData, 1, (size_t)nSize, m_impl->m_pFile);
	return nBytesRead;
}

FXD::U64 IStreamFile::_write_size(const void* pData, FXD::U64 nSize)
{
	PRINT_COND_ASSERT(!IO::Funcs::IsIOThreadRestricted(), "IO: is not allowed on current thread");

	FXD::U64 nBytesWritten = fwrite(pData, 1, nSize, m_impl->m_pFile);
	return nBytesWritten;
}

FXD::U64 IStreamFile::_length(void)
{
	PRINT_COND_ASSERT(!IO::Funcs::IsIOThreadRestricted(), "IO: is not allowed on current thread");

	fflush(m_impl->m_pFile);
	struct stat s;
	fstat(fileno(m_impl->m_pFile), &s);
	return s.st_size;
}

FXD::S32 IStreamFile::_tell(void)
{
	PRINT_COND_ASSERT(!IO::Funcs::IsIOThreadRestricted(), "IO: is not allowed on current thread");

	return ftell(m_impl->m_pFile);
}

FXD::U64 IStreamFile::_seek_to(FXD::U64 nOffset, Core::E_SeekOffset eFrom)
{
	PRINT_COND_ASSERT(!IO::Funcs::IsIOThreadRestricted(), "IO: is not allowed on current thread");

	FXD::U64 nRetVal;
	switch (eFrom)
	{
		case Core::E_SeekOffset::Begin:
		{
			nRetVal = fseek(m_impl->m_pFile, nOffset, SEEK_SET);
			break;
		}
		case Core::E_SeekOffset::Current:
		{
			nRetVal = fseek(m_impl->m_pFile, nOffset, SEEK_CUR);
			break;
		}
		case Core::E_SeekOffset::End:
		{
			nRetVal = fseek(m_impl->m_pFile, nOffset, SEEK_END);
			break;
		}
		default:
		{
			nRetVal = (FXD::U64)-1;
			PRINT_ASSERT << "IO: Invalid seek mode";
		}
	}
	return nRetVal;
}

IO::FileInfo IStreamFile::_get_file_info(void)
{
	if (m_impl->m_pFile == nullptr)
	{
		return IO::FileInfo();
	}

	fflush(m_impl->m_pFile);
	struct stat s;
	FXD::S32 nRetVal = fstat(fileno(m_impl->m_pFile), &s);
	if (nRetVal == 0)
	{
		time_t creationTime = s.st_ctime;
		time_t lastAccessTime = s.st_atime;
		time_t lastWriteTime = s.st_mtime;
		Core::Date creationDate(creationTime);
		Core::Date lastAccessDate(lastAccessTime);
		Core::Date lastWriteDate(lastWriteTime);
		return IO::FileInfo(creationDate, lastAccessDate, lastWriteDate, s.st_size);
	}
	return IO::FileInfo();
}
#endif //IsSystemPosix()