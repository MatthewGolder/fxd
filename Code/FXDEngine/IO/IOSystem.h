// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_IOSYSTEM_H
#define FXDENGINE_IO_IOSYSTEM_H

#include "FXDEngine/IO/Types.h"
#include "FXDEngine/Job/Future.h"

namespace FXD
{
	namespace IO
	{
		// ------
		// IIOSystem
		// - 
		// ------
		class IIOSystem FINAL : public Core::NonCopyable
		{
		public:
			friend class IIOJPThread;

		public:
			IIOSystem(void);
			~IIOSystem(void);

			Job::Future< bool > create_system(void);
			Job::Future< bool > release_system(void);

			GET_REF_W(IO::IODevice, ioDevice, io_device);

		private:
			bool _create_system(void);
			bool _release_system(void);

			void _process(FXD::F32 dt);

		private:
			IO::IODevice m_ioDevice;
		};

	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_IOSYSTEM_H