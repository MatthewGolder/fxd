// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_DIRECTORYENTRY_H
#define FXDENGINE_IO_DIRECTORYENTRY_H

#include "FXDEngine/IO/Path.h"

namespace FXD
{
	namespace IO
	{
		// ------
		// DirectoryEntry
		// -
		// ------
		class DirectoryEntry
		{
		public:
			DirectoryEntry(void) NOEXCEPT;
			EXPLICIT DirectoryEntry(const IO::Path& path);
			~DirectoryEntry(void);

			DirectoryEntry(const DirectoryEntry& rhs);
			DirectoryEntry(DirectoryEntry&& rhs) NOEXCEPT;

			DirectoryEntry& operator=(const DirectoryEntry& rhs);
			DirectoryEntry& operator=(DirectoryEntry&& rhs) NOEXCEPT;

			void assign(const IO::Path& path);
			void replace_filename(const IO::Path& path);

			bool is_block_file(void)const;
			bool is_character_file(void)const;
			bool is_directory(void)const;
			bool is_fifo(void)const;
			bool is_regular_file(void)const;
			bool is_socket(void)const;
			bool is_symlink(void)const;

			void refresh(void);
			const IO::Path& path(void)const;
			IO::FileStatus status(void)const;

			friend inline bool operator==(const DirectoryEntry& lhs, const DirectoryEntry& rhs) NOEXCEPT;
			friend inline bool operator!=(const DirectoryEntry& lhs, const DirectoryEntry& rhs) NOEXCEPT;
			friend inline bool operator<(const DirectoryEntry& lhs, const DirectoryEntry& rhs) NOEXCEPT;
			friend inline bool operator>(const DirectoryEntry& lhs, const DirectoryEntry& rhs) NOEXCEPT;
			friend inline bool operator<=(const DirectoryEntry& lhs, const DirectoryEntry& rhs) NOEXCEPT;
			friend inline bool operator>=(const DirectoryEntry& lhs, const DirectoryEntry& rhs) NOEXCEPT;

		private:
			IO::Path m_path;
			IO::FileStatus m_status;
		};

		inline bool operator==(const DirectoryEntry& lhs, const DirectoryEntry& rhs) NOEXCEPT
		{
			return (lhs.m_path == rhs.m_path);
		}

		inline bool operator!=(const DirectoryEntry& lhs, const DirectoryEntry& rhs) NOEXCEPT
		{
			return (lhs.m_path != rhs.m_path);
		}

		inline bool operator<(const DirectoryEntry& lhs, const DirectoryEntry& rhs) NOEXCEPT
		{
			return (lhs.m_path < rhs.m_path);
		}

		inline bool operator>(const DirectoryEntry& lhs, const DirectoryEntry& rhs) NOEXCEPT
		{
			return (lhs.m_path > rhs.m_path);
		}

		inline bool operator<=(const DirectoryEntry& lhs, const DirectoryEntry& rhs) NOEXCEPT
		{
			return (lhs.m_path <= rhs.m_path);
		}

		inline bool operator>=(const DirectoryEntry& lhs, const DirectoryEntry& rhs) NOEXCEPT
		{
			return (lhs.m_path >= rhs.m_path);
		}

	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_DIRECTORYENTRY_H