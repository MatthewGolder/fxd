// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_SAVE_H
#define FXDENGINE_IO_SAVE_H

#include "FXDEngine/Core/StreamIn.h"
#include "FXDEngine/Core/StreamOut.h"

namespace FXD
{
	namespace IO
	{
		// ------
		// ISaveable
		// -
		// ------
		class ISaveable
		{
		public:
			ISaveable(void)
			{}
			virtual ~ISaveable(void)
			{}

		protected:
			virtual void _save_to_stream(Core::IStreamOut& ostr) const PURE;

			friend Core::IStreamOut& operator<<(Core::IStreamOut& ostr, IO::ISaveable const& rhs);
		};
		// IStreamOut
		inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, IO::ISaveable const& rhs)
		{
			rhs._save_to_stream(ostr);
			return ostr;
		}

		// ------
		// ILoadable
		// -
		// ------
		class ILoadable
		{
		public:
			ILoadable(void)
			{}
			virtual ~ILoadable(void)
			{}

		protected:
			virtual void _load_from_stream(Core::IStreamIn& istr) PURE;

			friend Core::IStreamIn& operator>>(Core::IStreamIn& istr, IO::ILoadable& rhs);
		};
		// IStreamIn
		inline Core::IStreamIn& operator>>(Core::IStreamIn& istr, IO::ILoadable& rhs)
		{
			rhs._load_from_stream(istr);
			return istr;
		}

	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_SAVE_H