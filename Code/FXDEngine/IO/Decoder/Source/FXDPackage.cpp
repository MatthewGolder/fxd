// Creator - MatthewGolder
#include "FXDEngine/IO/Decoder/FXDPackage.h"

using namespace FXD;
using namespace IO;

// ------
// IFXDPackage
// -
// ------
IFXDPackage::IFXDPackage(Core::StreamIn& streamIn, const Core::String8& strAssetName)
	: IAsset(strAssetName)
	, IStreamIn(nullptr, Core::CompressParams::Default)
	, m_streamIn(streamIn)
{
}

IFXDPackage::~IFXDPackage(void)
{
}

bool IFXDPackage::is_platform_supported(void) const
{
	return true;
}
