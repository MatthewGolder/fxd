// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_DECODERS_FXDPACKAGE_H
#define FXDENGINE_IO_DECODERS_FXDPACKAGE_H

#include "FXDEngine/IO/FileCache.h"

namespace FXD
{
	namespace IO
	{
		// ------
		// IFXDPackage
		// -
		// ------
		class IFXDPackage : public IO::IAsset, public Core::IStreamIn
		{
		public:
			IFXDPackage(Core::StreamIn& streamIn, const Core::String8& strAssetName);
			virtual ~IFXDPackage(void);

			virtual bool is_platform_supported(void) const PURE;

			const Core::StringView8 get_asset_type(void) const FINAL { return UTF_8("FXDPackage"); }

		protected:
			Core::StreamIn m_streamIn;
		};

	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_DECODERS_FXDPACKAGE_H