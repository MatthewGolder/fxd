// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_FILESTATUS_H
#define FXDENGINE_IO_FILESTATUS_H

#include "FXDEngine/Core/Types.h"
#include "FXDEngine/IO/FileType.h"

namespace FXD
{
	namespace IO
	{
		// ------
		// FileStatus
		// -
		// ------
		class FileStatus
		{
		public:
			FileStatus(void) NOEXCEPT;
			EXPLICIT FileStatus(IO::E_FileType type) NOEXCEPT;

			~FileStatus(void);

			FileStatus(const IO::FileStatus& rhs) NOEXCEPT;
			FileStatus(IO::FileStatus&& rhs) NOEXCEPT;

			IO::FileStatus& operator=(const IO::FileStatus& rhs) NOEXCEPT;
			IO::FileStatus& operator=(IO::FileStatus&& rhs) NOEXCEPT;

			IO::E_FileType type(void) const NOEXCEPT;
			void type(IO::E_FileType type) NOEXCEPT;

		private:
			IO::E_FileType m_type;
		};

	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_FILESTATUS_H