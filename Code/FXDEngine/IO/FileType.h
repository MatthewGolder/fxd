// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_FILETYPE_H
#define FXDENGINE_IO_FILETYPE_H

namespace FXD
{
	namespace IO
	{
		// ------
		// E_FileType
		// -
		// ------
		enum class E_FileType
		{
			None,
			NotFound,
			Regular,
			Directory,
			Symlink,
			Block,
			Character,
			Fifo,
			Socket,
			Count,
			Unknown = 0xffff
		};

	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_FILETYPE_H