// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_IOSTREAMPARSER_H
#define FXDENGINE_IO_IOSTREAMPARSER_H

#include "FXDEngine/Core/StreamIn.h"
#include "FXDEngine/Core/SerializeInBinary.h"
#include "FXDEngine/Core/String.h"
#include "FXDEngine/IO/Types.h"

namespace FXD
{
	namespace IO
	{
		// ------
		// StreamParser
		// -
		// ------
		class StreamParser FINAL : public Core::IStreamIn, public Core::ISerializeInBinary, Core::NonCopyable
		{
		public:
			static const FXD::U64 kBufferSize = 2048; 	// Parse buffer, Must be a power of 2.
			static const FXD::UTF8 kEndOfFile = '\xFF';	// End-of-file.

			// ------
			// Position
			// -
			// ------
			class Position
			{
			public:
				friend class StreamParser;

			public:
				Position(const Position& rhs);
				~Position(void);

				GET_R(FXD::U64, nFileOffset, fileOffset);
				GET_R(FXD::U64, nLine, line);
				GET_R(FXD::U64, nColumn, column);

			private:
				Position(void); // Only the parser context can create positions.

				void _advance_char(FXD::UTF8 ch);
				void _advance_by(FXD::U32 nAmount);

			private:
				FXD::U64 m_nFileOffset;
				FXD::U64 m_nLine;
				FXD::U64 m_nColumn;
			};

		public:
			StreamParser(const Core::StreamIn& stream);
			~StreamParser(void);

			FXD::U64 read_size(void* pDst, const FXD::U64 nSize) FINAL;

			FXD::U64 length(void) FINAL;
			FXD::S32 tell(void) FINAL;
			FXD::U64 seek_to(FXD::U64 nOffset, Core::E_SeekOffset eFrom) FINAL;

			FXD::UTF8 peek_next_char(void) const;	// Peek at the next character without advancing.
			FXD::UTF8 get_next_char(void);			// Get next character and advance.
			void skip(FXD::U32 nCount);				// Skip one or more characters.

			// Get the current position.
			const Position& get_parse_position(void) const
			{
				return m_position;
			};

			// Set the position.
			void set_parse_position(const Position& pos);

			// Determine if the next character is whitespace.
			bool is_next_whitespace(void) const;

			// Skip all whitespace.
			void skip_whitespace(void);

		private:
			void _advance(void);

		public:
			FXD::UTF8 m_buffer[kBufferSize];
			FXD::U64 m_nBufferOffset;
			FXD::U64 m_nFileSize;
			Position m_position;

		private:
			Core::StreamIn m_streamHandler;
		};

		namespace Parser
		{
			// ------
			// Identifier
			// -
			// ------
			class Identifier
			{
			public:
				Identifier(void)
					: m_bXml(true)
				{}
				~Identifier(void)
				{}

				bool operator()(StreamParser& pc, Core::String8& out) const;

			public:
				bool m_bXml;
			};

			// ------
			// QuotedString
			// -
			// ------
			class QuotedString
			{
			public:
				bool operator()(StreamParser& pc, Core::String8& out) const;
			};

			// ------
			// FixedString
			// -
			// ------
			class FixedString
			{
			public:
				bool operator()(StreamParser& pc, const Core::String8& str) const;
			};

			// ------
			// TypeParser
			// -
			// ------
			template < typename T >
			class TypeParser
			{
			public:
				TypeParser(void)
					: m_bAllowAnyNextChar(false)
				{}
				~TypeParser(void)
				{}

				bool operator()(StreamParser& pc, T& result) const;

			public:
				bool m_bAllowAnyNextChar;
			};
			template <>
			bool TypeParser< Core::String8 >::operator()(StreamParser& pc, Core::String8& strOutVal) const;
			template <>
			bool TypeParser< FXD::S32 >::operator()(StreamParser& pc, FXD::S32& nOutVal) const;
			template <>
			bool TypeParser< FXD::F64 >::operator()(StreamParser& pc, FXD::F64& fOutVal) const;
			template <>
			bool TypeParser< FXD::F32 >::operator()(StreamParser& pc, FXD::F32& fOutVal) const;
		}

	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_IOSTREAMPARSER_H