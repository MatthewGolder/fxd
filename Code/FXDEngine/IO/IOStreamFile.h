// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_IOSTREAMFILE_H
#define FXDENGINE_IO_IOSTREAMFILE_H

#include "FXDEngine/Core/StreamIn.h"
#include "FXDEngine/Core/StreamOut.h"
#include "FXDEngine/IO/IOFileInfo.h"
#include "FXDEngine/IO/Path.h"
#include "FXDEngine/IO/Types.h"

namespace FXD
{
	namespace IO
	{
		// ------
		// IStreamFile
		// -
		// ------
		class IStreamFile : public Core::HasImpl< IO::IStreamFile, 32 >, public Core::IStreamOut, public Core::IStreamIn
		{
		public:
			friend class FileInfo;

		public:
			IStreamFile(void);
			IStreamFile(const IO::Path& ioPath, IO::FileOpenParams fileOpenParams, Core::CompressParams compressParams = Core::CompressParams::Default);
			~IStreamFile(void);

			bool open(const IO::Path& ioPath, IO::FileOpenParams fileOpenParams, Core::CompressParams compressParams = Core::CompressParams::Default);
			bool close(void);

			FXD::U64 read_size(void* pData, const FXD::U64 nSize) FINAL;
			FXD::U64 write_size(const void* pData, const FXD::U64 nSize) FINAL;

			FXD::U64 length(void) FINAL;
			FXD::S32 tell(void) FINAL;
			FXD::U64 seek_to(FXD::U64 nOffset, Core::E_SeekOffset eFrom) FINAL;

			IO::FileInfo get_file_info(void);

		private:
			bool _open(const IO::Path& ioPath, IO::FileOpenParams fileOpenParams, Core::CompressParams compressParams);
			bool _close(void);

			FXD::U64 _read_size(void* pData, FXD::U64 nSize);
			FXD::U64 _write_size(const void* pData, FXD::U64 nSize);

			FXD::U64 _length(void);
			FXD::S32 _tell(void);
			FXD::U64 _seek_to(FXD::U64 nOffset, Core::E_SeekOffset eFrom);

			IO::FileInfo _get_file_info(void);

		private:
			IO::Path m_ioPath;
		};

		namespace Funcs
		{
			extern const FXD::U32 GetOpenFileCount(void);
			extern void IncrementOpenFileCount(void);
			extern void DecrementOpenFileCount(void);
		} //namespace Funcs

	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_IOSTREAMFILE_H