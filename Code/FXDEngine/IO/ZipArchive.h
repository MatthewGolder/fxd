// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_ZIPARCHIVE_H
#define FXDENGINE_IO_ZIPARCHIVE_H

#include <3rdParty/minizip/mz_compat.h>

namespace FXD
{
	namespace IO
	{
		// ------
		// IZipArchiveEntry
		// -
		// ------
		class IZipArchiveEntry
		{
		public:
		private:
		};

		// ------
		// IZipArchive
		// -
		// ------
		class IZipArchive
		{
		public:
			//IZipArchive(const IO::Path& ioPath, IO::E_FileMode eMode, IO::E_StreamMode eStream, bool bIgnoreMissingFiles);
			//~IZipArchive(void);
		
		private:
			zipFile m_zFile;
			unzFile m_unzFile;
		};
	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_ZIPARCHIVE_H