// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_DIRECTORYITERATOR_H
#define FXDENGINE_IO_DIRECTORYITERATOR_H

#include "FXDEngine/IO/Types.h"
#include "FXDEngine/IO/Internal/DirectoryIterator.h"

namespace FXD
{
	namespace IO
	{
		// ------
		// directory_iterator
		// -
		// ------
		class directory_iterator
		{
		public:
			using value_type = IO::DirectoryEntry;
			using difference_type = std::ptrdiff_t;
			using pointer = IO::DirectoryEntry const*;
			using reference = IO::DirectoryEntry const&;
			using iterator_category = FXD::STD::input_iterator_tag;

		public:
			directory_iterator(void);
			directory_iterator(const IO::Path& path, const Core::String8& strPattern = UTF_8("*"));
			~directory_iterator(void);

			directory_iterator(const directory_iterator& rhs);
			directory_iterator(directory_iterator&& rhs);

			directory_iterator& operator=(const directory_iterator& rhs);
			directory_iterator& operator=(directory_iterator&& rhs) NOEXCEPT;

			directory_iterator& operator++();
			directory_iterator operator++(int);
			
			const IO::DirectoryEntry& operator*(void) const;
			const IO::DirectoryEntry* operator->(void) const;

			bool operator==(const directory_iterator& rhs) const;
			bool operator!=(const directory_iterator& rhs) const;

			directory_iterator& increment(void);

		protected:
			std::shared_ptr< Internal::_directory_iterator > m_internal;
		};

		inline const IO::directory_iterator& begin(const IO::directory_iterator& itr) NOEXCEPT
		{
			return (itr);
		}
		inline IO::directory_iterator end(const IO::directory_iterator&) NOEXCEPT
		{
			return (IO::directory_iterator());
		}

	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_DIRECTORYITERATOR_H