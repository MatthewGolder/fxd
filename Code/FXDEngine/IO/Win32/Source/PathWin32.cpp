// Creator - MatthewGolder
#include "FXDEngine/IO/Types.h"

#if IsSystemWindows()
#include "FXDEngine/IO/Path.h"
#include "FXDEngine/App/Platform.h"

#include <shlobj.h>
#include <stdio.h>
#include <sys/stat.h>

using namespace FXD;
using namespace IO;

// ------
// FS
// - 
// ------
bool FXD::IO::FS::DirectoryCreate(const IO::Path& path)
{
	if (IO::FS::DirectoryExists(path))
	{
		return true;
	}
	DWORD nRetVal = CreateDirectoryA(path.c_str(), NULL);
	return (nRetVal != FALSE);
}

bool FXD::IO::FS::DirectoryDelete(const IO::Path& path)
{
	if (IO::FS::DirectoryExists(path) == false)
	{
		return true;
	}
	DWORD nRetVal = RemoveDirectoryA(path.c_str());
	return (nRetVal != FALSE);
}

bool FXD::IO::FS::DirectoryExists(const IO::Path& path)
{
	DWORD nRetVal = GetFileAttributesA(path.c_str());
	bool bRetVal = (nRetVal != INVALID_FILE_ATTRIBUTES && (nRetVal & FILE_ATTRIBUTE_DIRECTORY));
	return bRetVal;
}

bool FXD::IO::FS::FileRemame(const IO::Path& pathOld, const IO::Path& pathNew)
{
	DWORD nRetVal = MoveFileA(pathOld.c_str(), pathNew.c_str());
	return (nRetVal != FALSE);
}

bool FXD::IO::FS::FileCopy(const IO::Path& pathOld, const IO::Path& pathNew)
{
	DWORD nRetVal = CopyFileA(pathOld.c_str(), pathNew.c_str(), TRUE);
	return (nRetVal != FALSE);
}

bool FXD::IO::FS::FileDelete(const IO::Path& path)
{
	DWORD nRetVal = DeleteFileA(path.c_str());
	return (nRetVal != FALSE);
}

bool FXD::IO::FS::FileExists(const IO::Path& path)
{
	DWORD nRetVal = GetFileAttributesA(path.c_str());
	bool bRetVal = (nRetVal != INVALID_FILE_ATTRIBUTES);
	return bRetVal;
}

IO::Path FXD::IO::FS::GetCWD(void)
{
	FXD::UTF8 pChar[256];
	DWORD nRetVal = GetCurrentDirectoryA(256, pChar);
	if (nRetVal == FALSE)
	{
		return Path(UTF_8(""));
	}
	return Path(pChar);
}

IO::Path FXD::IO::FS::GetTempDirectory(void)
{
	FXD::UTF8 tempPath[MAX_PATH];
	DWORD nRetVal = GetTempPathA(MAX_PATH, tempPath);
	if (nRetVal == FALSE)
	{
		return Path(UTF_8(""));
	}

	Core::String8 retStr(tempPath);
	FXD::U32 nSlashIdx = retStr.rfind_no_case(FXD::PATH_SEPERATOR);
	retStr = retStr.substr(0, nSlashIdx);

	return IO::Path(retStr).append_create(App::GetGameNameUTF8());
}

IO::Path FXD::IO::FS::GetModuleDirectory(void)
{
	FXD::UTF8 tempStrBuf[1024];
	DWORD dwrd = GetModuleFileNameA(0, tempStrBuf, 1024);

	Core::String8 str(tempStrBuf);
	FXD::U32 nId = str.rfind_no_case(FXD::PATH_SEPERATOR);

	Core::String8 strPath(str, 0, nId);
	return Path(strPath);
}

IO::Path FXD::IO::FS::GetUtilityDirectory(Core::StringView8 strFilePath)
{
	FXD::UTF8 cPath[MAX_PATH];
	FXD::S32 nFolder = CSIDL_LOCAL_APPDATA;

	/*
	int nFolder = CSIDL_COMMON_DOCUMENTS;
	if(nFlags & Utility_CurrentUser)
	{
		nFolder = CSIDL_LOCAL_APPDATA;
		if(nFlags & Utility_UserVisible)
		{
			nFolder = CSIDL_PERSONAL;
		}
	}
	*/
	SHGetFolderPathA(NULL, nFolder, NULL, SHGFP_TYPE_CURRENT, cPath);

	IO::Path ioPath = Path(cPath);
	ioPath.append_create(App::GetGameNameUTF8());
	ioPath.append_create(strFilePath);
	return ioPath;
}

IO::Path FXD::IO::FS::GetPathFromEnviroment(Core::StringView8 strFilePath)
{
	const DWORD nBuffSize = 65535;
	FXD::UTF8 buffer[nBuffSize];
	GetEnvironmentVariableA(strFilePath.c_str(), buffer, nBuffSize);
	IO::Path ioPath = Path(buffer);
	return ioPath;
}


static IO::E_FileType _Map_mode(FXD::S32 nMode)
{
	CONSTEXPR FXD::S32 kFileAttributes =
		FILE_ATTRIBUTE_ARCHIVE | FILE_ATTRIBUTE_COMPRESSED | FILE_ATTRIBUTE_ENCRYPTED | FILE_ATTRIBUTE_HIDDEN |
		FILE_ATTRIBUTE_NORMAL | FILE_ATTRIBUTE_NOT_CONTENT_INDEXED | FILE_ATTRIBUTE_OFFLINE | FILE_ATTRIBUTE_READONLY |
		FILE_ATTRIBUTE_SPARSE_FILE | FILE_ATTRIBUTE_SYSTEM | FILE_ATTRIBUTE_TEMPORARY;

	if ((nMode & FILE_ATTRIBUTE_DIRECTORY) != 0)
	{
		return IO::E_FileType::Directory;
	}
	else if ((nMode & kFileAttributes) != 0)
	{
		return IO::E_FileType::Regular;
	}
	else
	{
		return IO::E_FileType::Unknown;
	}
}

IO::E_FileType _Status(const FXD::UTF8* pName)
{
	WIN32_FILE_ATTRIBUTE_DATA winData;
	if (GetFileAttributesExA(pName, GetFileExInfoStandard, &winData))
	{
		return _Map_mode(winData.dwFileAttributes);
	}

	FXD::S32 nErrno = GetLastError();
	if (nErrno == ERROR_BAD_NETPATH ||
		 nErrno == ERROR_BAD_PATHNAME ||
		 nErrno == ERROR_FILE_NOT_FOUND ||
		 nErrno == ERROR_INVALID_DRIVE ||
		 nErrno == ERROR_INVALID_NAME ||
		 nErrno == ERROR_INVALID_PARAMETER ||
		 nErrno == ERROR_PATH_NOT_FOUND)
	{
		return IO::E_FileType::NotFound;
	}
	else
	{
		return IO::E_FileType::Unknown;
	}
}

IO::FileStatus FXD::IO::FS::Status(const IO::Path& path)
{
	IO::E_FileType e =_Status(path.c_str());
	return IO::FileStatus(e);
}
#endif //IsSystemWindows()