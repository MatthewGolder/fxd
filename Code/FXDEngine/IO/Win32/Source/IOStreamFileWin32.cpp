// Creator - MatthewGolder
#include "FXDEngine/IO/Types.h"

#if IsSystemWindows()
#include "FXDEngine/Core/Win32/Win32.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOStreamFile.h"

using namespace FXD;
using namespace IO;

// ------
// Core::Impl::Impl< IStreamFile >
// -
// ------
template <>
class Core::Impl::Impl< IO::IStreamFile >
{
public:
	friend class IStreamFile;

public:
	Impl(void)
		: m_handle(INVALID_HANDLE_VALUE)
	{}
	~Impl(void)
	{}

protected:
	HANDLE m_handle;
};

// ------
// IStreamFile
// - 
// ------
IStreamFile::IStreamFile(void)
	: IStreamOut(nullptr, Core::CompressParams::Default)
	, IStreamIn(nullptr, Core::CompressParams::Default)
{
}

IStreamFile::IStreamFile(const IO::Path& ioPath, IO::FileOpenParams fileOpenParams, Core::CompressParams compressParams)
	: IStreamFile()
{
	open(ioPath, fileOpenParams, compressParams);
}

IStreamFile::~IStreamFile(void)
{
	if (m_impl->m_handle != INVALID_HANDLE_VALUE)
	{
		close();
	}
}

bool IStreamFile::_open(const IO::Path& ioPath, IO::FileOpenParams fileOpenParams, Core::CompressParams compressParams)
{
	PRINT_COND_ASSERT(!IO::Funcs::IsIOThreadRestricted(), "IO: is not allowed on current thread");

	// 1.1 Attempt to open
	const FXD::UTF8* pName = ioPath.c_str();
	switch (fileOpenParams.FileMode)
	{
		case E_FileMode::Read:
		{
			m_impl->m_handle = CreateFileA(pName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
			break;
		}
		case E_FileMode::Write:
		{
			m_impl->m_handle = CreateFileA(pName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
			break;
		}
		case E_FileMode::Append:
		{
			m_impl->m_handle = CreateFileA(pName, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
			break;
		}
		default:
		{
			PRINT_ASSERT << "IO: Invalid file open mode";
		}
	}

	// 1.2 Handle if the file has opened
	if (m_impl->m_handle == INVALID_HANDLE_VALUE)
	{
		if (fileOpenParams.IgnoreMissingFiles)
		{
			return false;
		}
		DWORD err = GetLastError();
	}
	PRINT_COND_ASSERT((m_impl->m_handle != INVALID_HANDLE_VALUE), "IO: The file '" << pName << "' could not be opened");

	// 1.3 Move to file end if we are appending and set local variables
	if (fileOpenParams.FileMode == E_FileMode::Append)
	{
		seek_end();
	}
	m_ioPath = ioPath;
	IO::Funcs::IncrementOpenFileCount();
	return true;
}

bool IStreamFile::_close(void)
{
	PRINT_COND_ASSERT(!IO::Funcs::IsIOThreadRestricted(), "IO: is not allowed on current thread");

	BOOL bRetVal = TRUE;
	if (m_impl->m_handle != INVALID_HANDLE_VALUE)
	{
		bRetVal = CloseHandle(m_impl->m_handle);
		m_impl->m_handle = INVALID_HANDLE_VALUE;
		IO::Funcs::DecrementOpenFileCount();
	}
	return (bRetVal);
}

FXD::U64 IStreamFile::_read_size(void* pData, FXD::U64 nSize)
{
	PRINT_COND_ASSERT(!IO::Funcs::IsIOThreadRestricted(), "IO: is not allowed on current thread");

	DWORD nBytesRead;
	if (ReadFile(m_impl->m_handle, pData, (DWORD)nSize, &nBytesRead, NULL) == false)
	{
		nBytesRead = 0;
	}
	return nBytesRead;
}

FXD::U64 IStreamFile::_write_size(const void* pData, FXD::U64 nSize)
{
	PRINT_COND_ASSERT(!IO::Funcs::IsIOThreadRestricted(), "IO: is not allowed on current thread");

	DWORD nBytesWritten;
	if (WriteFile(m_impl->m_handle, pData, (DWORD)nSize, &nBytesWritten, NULL) == false)
	{
		nBytesWritten = 0;
	}
	return nBytesWritten;
}

FXD::U64 IStreamFile::_length(void)
{
	PRINT_COND_ASSERT(!IO::Funcs::IsIOThreadRestricted(), "IO: is not allowed on current thread");

	return GetFileSize(m_impl->m_handle, NULL);
}

FXD::S32 IStreamFile::_tell(void)
{
	PRINT_COND_ASSERT(!IO::Funcs::IsIOThreadRestricted(), "IO: is not allowed on current thread");

	DWORD nPos = SetFilePointer(m_impl->m_handle, 0, NULL, FILE_CURRENT);
	if (nPos == INVALID_SET_FILE_POINTER)
	{
		return -1;
	}
	return FXD::S32(nPos);
}

FXD::U64 IStreamFile::_seek_to(FXD::U64 nOffset, Core::E_SeekOffset eFrom)
{
	PRINT_COND_ASSERT(!IO::Funcs::IsIOThreadRestricted(), "IO: is not allowed on current thread");

	FXD::U64 nRetVal;
	switch (eFrom)
	{
		case Core::E_SeekOffset::Begin:
		{
			nRetVal = SetFilePointer(m_impl->m_handle, (LONG)nOffset, NULL, FILE_BEGIN);
			break;
		}
		case Core::E_SeekOffset::Current:
		{
			nRetVal = SetFilePointer(m_impl->m_handle, (LONG)nOffset, NULL, FILE_CURRENT);
			break;
		}
		case Core::E_SeekOffset::End:
		{
			nRetVal = SetFilePointer(m_impl->m_handle, (LONG)nOffset, NULL, FILE_END);
			break;
		}
		default:
		{
			nRetVal = (FXD::U64)-1;
			PRINT_ASSERT << "IO: Invalid seek mode";
		}
	}
	return nRetVal;
}

time_t filetime_to_timet(const FILETIME& ft)
{
	ULARGE_INTEGER ull;
	ull.LowPart = ft.dwLowDateTime;
	ull.HighPart = ft.dwHighDateTime;

	return ull.QuadPart / 10000000ULL - 11644473600ULL;
}

IO::FileInfo IStreamFile::_get_file_info(void)
{
	if (m_impl->m_handle == INVALID_HANDLE_VALUE)
	{
		return IO::FileInfo();
	}

	BY_HANDLE_FILE_INFORMATION fileinfo = {};
	Memory::MemZero_T(fileinfo);

	BOOL bSuccess = GetFileInformationByHandle(m_impl->m_handle, &fileinfo);
	if (bSuccess)
	{
		Core::Date dateCreate = Core::Date(filetime_to_timet(fileinfo.ftCreationTime));
		Core::Date dateLastAccess = Core::Date(filetime_to_timet(fileinfo.ftLastAccessTime));
		Core::Date dateLastWrite = Core::Date(filetime_to_timet(fileinfo.ftLastWriteTime));
		return IO::FileInfo(dateCreate, dateLastAccess, dateLastWrite, fileinfo.nFileSizeLow);
	}
	return IO::FileInfo();
}
#endif //IsSystemWindows()