// Creator - MatthewGolder
#include "FXDEngine/IO/Types.h"

#if IsSystemWindows()
#include "FXDEngine/IO/IODevice.h"

#include <shlobj.h>
#include <stdio.h>

using namespace FXD;
using namespace IO;

// ------
// Core::Impl::Impl< IIODevice >
// -
// ------
template <>
class Core::Impl::Impl< IIODevice >
{
public:
	friend class IIODevice;

public:
	Impl(void)
	{}
	~Impl(void)
	{}
};

// ------
// IIODevice
// - 
// ------
IIODevice::IIODevice(void)
{
}

IIODevice::~IIODevice(void)
{
}
#endif //IsSystemWindows()