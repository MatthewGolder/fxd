// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_IOJPTHREAD_H
#define FXDENGINE_IO_IOJPTHREAD_H

#include "FXDEngine/Core/FPS.h"
#include "FXDEngine/Core/Timer.h"
#include "FXDEngine/IO/Types.h"
#include "FXDEngine/Job/AsyncJob.h"

namespace FXD
{
	namespace IO
	{
		// ------
		// IIOJPThread
		// -
		// ------
		class IIOJPThread FINAL : public Job::IAsyncTaskPoolThread
		{
		public:
			IIOJPThread(void);
			~IIOJPThread(void);

			const Core::StringView8 display_name(void) const FINAL { return UTF_8("IOThread"); }

			GET_REF_W(IO::IOSystem, ioSystem, io_system);

		protected:
			bool init_pool(void) FINAL;
			bool shutdown_pool(void) FINAL;
			void idle_process(void) FINAL;
			bool is_thread_restricted(void) const FINAL;

		private:
			Core::FPS m_fps;
			Core::Timer m_timer;
			IO::IOSystem m_ioSystem;
		};

		namespace Funcs
		{
			extern bool IsIOThreadRestricted(void);
			extern void RestrictIOThread(void);
			extern void UnrestrictIOThread(void);
		} //namespace Funcs
	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_IOJPTHREAD_H