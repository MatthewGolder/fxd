// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_ARCHIVE_ARCHIVE_H
#define FXDENGINE_IO_ARCHIVE_ARCHIVE_H

#include "FXDEngine/IO/Types.h"
#include "FXDEngine/Container/Map.h"
#include "FXDEngine/Core/String.h"

namespace FXD
{
	namespace IO
	{
		// ------
		// Archive
		// -
		// ------
		class Archive
		{
			class Info
			{
				FXD::U64 m_nOffset;
				FXD::U64 m_nCompressedSize;
			};

			FXD::Container::Map< FXD::Core::String8, FXD::Core::String8 > m_map
		};

	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_ARCHIVE_ARCHIVE_H