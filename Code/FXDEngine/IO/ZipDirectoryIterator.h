// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_ZIPDIRECTORYITERATOR_H
#define FXDENGINE_IO_ZIPDIRECTORYITERATOR_H

#include "FXDEngine/IO/Types.h"
#include "FXDEngine/IO/Internal/ZipDirectoryIterator.h"

namespace FXD
{
	namespace IO
	{
		// ------
		// zip_directory_iterator
		// -
		// ------
		class zip_directory_iterator
		{
		public:
			using value_type = IO::DirectoryEntry;
			using difference_type = std::ptrdiff_t;
			using pointer = IO::DirectoryEntry const*;
			using reference = IO::DirectoryEntry const&;
			using iterator_category = FXD::STD::input_iterator_tag;

		public:
			zip_directory_iterator(void);
			zip_directory_iterator(const IO::Path& path, const Core::String8& strPattern);
			~zip_directory_iterator(void);

			zip_directory_iterator(const zip_directory_iterator& rhs);
			zip_directory_iterator(zip_directory_iterator&& rhs);

			zip_directory_iterator& operator=(const zip_directory_iterator& rhs);
			zip_directory_iterator& operator=(zip_directory_iterator&& rhs) NOEXCEPT;

			zip_directory_iterator& operator++();
			zip_directory_iterator operator++(int);

			const IO::DirectoryEntry& operator*(void) const;
			const IO::DirectoryEntry* operator->(void) const;

			bool operator==(const zip_directory_iterator& rhs) const;
			bool operator!=(const zip_directory_iterator& rhs) const;

			zip_directory_iterator& increment(void);

		private:
			std::shared_ptr< Internal::_zip_directory_iterator > m_internal;
		};

		inline const IO::zip_directory_iterator& begin(const IO::zip_directory_iterator& itr) NOEXCEPT
		{
			return (itr);
		}
		inline IO::zip_directory_iterator end(const IO::zip_directory_iterator&) NOEXCEPT
		{
			return (IO::zip_directory_iterator());
		}

	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_ZIPDIRECTORYITERATOR_H