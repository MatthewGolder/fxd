// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_RECURSIVEDIRECTORYITERATOR_H
#define FXDENGINE_IO_RECURSIVEDIRECTORYITERATOR_H

#include "FXDEngine/Core/String.h"
#include "FXDEngine/IO/Types.h"
#include "FXDEngine/IO/Internal/RecursiveDirectoryIterator.h"

namespace FXD
{
	namespace IO
	{
		// ------
		// recursive_directory_iterator
		// -
		// ------
		class recursive_directory_iterator
		{
		public:
			using value_type = IO::DirectoryEntry;
			using difference_type = std::ptrdiff_t;
			using pointer = IO::DirectoryEntry const*;
			using reference = IO::DirectoryEntry const&;
			using iterator_category = FXD::STD::input_iterator_tag;

		public:
			recursive_directory_iterator(void) NOEXCEPT;
			recursive_directory_iterator(const IO::Path& path, const Core::String8& strPattern = UTF_8("*"));
			~recursive_directory_iterator(void);

			recursive_directory_iterator(const recursive_directory_iterator& rhs);
			recursive_directory_iterator(recursive_directory_iterator&& rhs);

			recursive_directory_iterator& operator=(const recursive_directory_iterator& rhs);
			recursive_directory_iterator& operator=(recursive_directory_iterator&& rhs) NOEXCEPT;

			recursive_directory_iterator& operator++();
			recursive_directory_iterator operator++(int);

			const IO::DirectoryEntry& operator*(void) const;
			const IO::DirectoryEntry* operator->(void) const;

			bool operator==(const recursive_directory_iterator& rhs) const;
			bool operator!=(const recursive_directory_iterator& rhs) const;

			recursive_directory_iterator& increment(void);

			FXD::S32 depth(void)const;
			void pop(void);
			
			bool recursion_pending(void) const;
			void disable_recursion_pending(void);

		private:
			bool _try_recursion(void);
			void _advance(void);

		private:
			bool m_bRecPending;
			std::shared_ptr< Internal::_recursive_directory_iterator > m_internal;
		};

		inline const IO::recursive_directory_iterator& begin(const IO::recursive_directory_iterator& itr) NOEXCEPT
		{
			return (itr);
		}
		inline IO::recursive_directory_iterator end(const IO::recursive_directory_iterator&) NOEXCEPT
		{
			return (IO::recursive_directory_iterator());
		}
	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_DIRECTORYITERATOR_H