// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_IO_TYPES_H
#define FXDENGINE_IO_TYPES_H

#include "FXDEngine/Core/Types.h"
#include <memory>

namespace FXD
{
	namespace IO
	{
		// ------
		// E_IOApi
		// -
		// ------
		enum class E_IOApi
		{
			Windows = 0,
			Posix,
			Count,
			Unknown = 0xffff
		};

		// ------
		// E_FileMode
		// -
		// ------
		enum class E_FileMode
		{
			Read,
			Write,
			Append,
			Count,
			Unknown = 0xffff
		};

		// ------
		// E_SerilizeMode
		// -
		// ------
		enum class E_SerilizeMode
		{
			String,
			Binary,
			Count,
			Unknown = 0xffff
		};

		// ------
		// FileOpenParams
		// -
		// ------
		struct FileOpenParams
		{
			IO::E_FileMode FileMode = IO::E_FileMode::Unknown;
			IO::E_SerilizeMode SerilizeMode = IO::E_SerilizeMode::Unknown;
			bool IgnoreMissingFiles = false;
		};

		// DirectoryEntry
		class DirectoryEntry;

		// Path
		class Path;

		// IIOSystem
		class IIOSystem;
		typedef std::unique_ptr< IO::IIOSystem > IOSystem;

		// IIODevice
		class IIODevice;
		typedef std::shared_ptr< IO::IIODevice > IODevice;

		// IStreamFile
		class IStreamFile;
		typedef std::shared_ptr< IO::IStreamFile > StreamFile;

		// IDataRegistry
		class IDataRegistry;
		typedef std::shared_ptr< IO::IDataRegistry > DataRegistry;

		class IFXDPackage;
		typedef std::shared_ptr< IFXDPackage > FXDPackage;

	} //namespace IO
} //namespace FXD
#endif //FXDENGINE_IO_TYPES_H