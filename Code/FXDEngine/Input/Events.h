// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_EVENTS_H
#define FXDENGINE_INPUT_EVENTS_H

#include "FXDEngine/Core/String.h"
#include "FXDEngine/Input/Types.h"
#include "FXDEngine/Math/Vector2I.h"
#include "FXDEngine/Job/Event.h"

namespace FXD
{
	namespace Input
	{
		// ------
		// OnKeyboardButton
		// -
		// ------
		class OnKeyboardButton FINAL : public Job::IEvent
		{
		public:
			OnKeyboardButton(const bool bPressed, const Core::String8& strName, const FXD::U16 nID)
				: Pressed(bPressed)
				, Name(strName)
				, ID(nID)
			{}
			~OnKeyboardButton(void)
			{}

		public:
			const bool Pressed;
			const Core::String8 Name;
			const FXD::U16 ID;
		};

		// ------
		// OnMouseButton
		// -
		// ------
		class OnMouseButton FINAL : public Job::IEvent
		{
		public:
			OnMouseButton(const bool bPressed, const Math::Vector2I& pos, const Input::E_MouseButtons& eControl)
				: Pressed(bPressed)
				, Pos(pos)
				, Control(eControl)
			{}
			~OnMouseButton(void)
			{}

		public:
			const bool Pressed;
			const Math::Vector2I Pos;
			const Input::E_MouseButtons Control;
		};

		// ------
		// OnMouseWheel
		// -
		// ------
		class OnMouseWheel FINAL : public Job::IEvent
		{
		public:
			OnMouseWheel(const FXD::F32 fValue)
				: Value(fValue)
			{}
			~OnMouseWheel(void)
			{}

		public:
			const FXD::F32 Value;
		};

		// ------
		// OnTouchPad
		// -
		// ------
		class OnTouchPad FINAL : public Job::IEvent
		{
		public:
			OnTouchPad(const Input::IInputDevice* pDevice, const bool bPressed, const FXD::U8 nTouchID, const FXD::U8 nTrackingID, const Math::Vector2I& pos)
				: Device(pDevice)
				, Pressed(bPressed)
				, TouchID(nTouchID)
				, TrackingID(nTrackingID)
				, Pos(pos)
			{}
			~OnTouchPad(void)
			{}

		public:
			const Input::IInputDevice* Device;
			const bool Pressed;
			const FXD::U8 TouchID;
			const FXD::U8 TrackingID;
			const Math::Vector2I Pos;
		};

		// ------
		// OnControllerConnection
		// -
		// ------
		class OnControllerConnection FINAL : public Job::IEvent
		{
		public:
			OnControllerConnection(const Input::IInputDevice* pDevice, const bool bConnected, const FXD::S32 nPort)
				: Device(pDevice)
				, Connected(bConnected)
				, Port(nPort)
			{}
			~OnControllerConnection(void)
			{}

		public:
			const Input::IInputDevice* Device;
			const bool Connected;
			const FXD::S32 Port;
		};

		// ------
		// OnControllerButton
		// -
		// ------
		class OnControllerButton FINAL : public Job::IEvent
		{
		public:
			OnControllerButton(const Input::IInputDevice* pDevice, const bool bPressed, const Input::E_GamePadButtons eControl, const FXD::S32 nPort)
				: Device(pDevice)
				, Pressed(bPressed)
				, Control(eControl)
				, Port(nPort)
			{}
			~OnControllerButton(void)
			{}

		public:
			const Input::IInputDevice* Device;
			const bool Pressed;
			const Input::E_GamePadButtons Control;
			const FXD::S32 Port;
		};

		// ------
		// OnControllerAnalog
		// -
		// ------
		class OnControllerAnalog FINAL : public Job::IEvent
		{
		public:
			OnControllerAnalog(const Input::IInputDevice* pDevice, const FXD::F32 fValue, const Input::E_GamePadButtons eControl, const FXD::S32 nPort)
				: Device(pDevice)
				, Value(fValue)
				, Control(eControl)
				, Port(nPort)
			{}
			~OnControllerAnalog(void)
			{}

		public:
			const Input::IInputDevice* Device;
			const FXD::F32 Value;
			const Input::E_GamePadButtons Control;
			const FXD::S32 Port;
		};

	} //namespace Input
} //namespace FXD
#endif //FXDENGINE_INPUT_EVENTS_H