// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_MOUSE_H
#define FXDENGINE_INPUT_MOUSE_H

#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/Input/InputDevice.h"

namespace FXD
{
	namespace Input
	{
		// ------
		// IMouse
		// -
		// ------
		class IMouse : public Core::HasImpl< Input::IMouse, 256 >, public Input::IInputDevice
		{
		public:
			IMouse(void);
			~IMouse(void);

			bool is_connected(void) const FINAL;
			bool is_mouse_available(void) const FINAL;
	
			Core::String8 get_config_name(void) const FINAL;
			Core::String8 get_device_name(void) const FINAL;
			Input::E_DeviceType get_device_type(void) const FINAL;

			//Math::Vector get_pointer_pos(const GFX::Viewport& display) const;
			//bool is_active_in_display(const GFX::Viewport& display) const;
			
			// Get the position of the scroll wheel.
			FXD::F32 get_scroll_wheel_pos(void) const;

		protected:
			void _update(FXD::F32 dt) FINAL;

			const Widget::WIDGET_DESC* _get_widget_defs(void) const FINAL;
			void _get_widget_state(FXD::U16 nID, void* pState) const FINAL;
		};

	} //namespace Input
} //namespace FXD
#endif //FXDENGINE_INPUT_MOUSE_H