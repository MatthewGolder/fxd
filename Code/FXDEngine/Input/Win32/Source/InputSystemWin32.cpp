// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputWin32()
#include "FXDEngine/Input/InputSystem.h"
#include "FXDEngine/Input/InputDevice.h"
#include "FXDEngine/Input/Win32/MouseWin32.h"
#include "FXDEngine/Input/Win32/KeyboardWin32.h"
#include "FXDEngine/Input/Steam/GamePadSteam.h"
#include "FXDEngine/Input/Steam/InputApiSteam.h"
#include "FXDEngine/Input/XInput/GamePadXInput.h"
#include "FXDEngine/Input/XInput/InputApiXInput.h"
#include "FXDEngine/Input/HIDAPI/HIDAPIDevice.h"
#include "FXDEngine/Input/HIDAPI/PS4/GamePadPS4.h"
#include "FXDEngine/Input/HIDAPI/PS4/InputApiPS4.h"
#include "FXDEngine/Input/HIDAPI/Switch/GamePadSwitch.h"
#include "FXDEngine/Input/HIDAPI/Switch/InputApiSwitch.h"
#include "FXDEngine/Input/HIDAPI/XInput/GamePadXInput.h"
#include "FXDEngine/Input/HIDAPI/XInput/InputApiXInput.h"

using namespace FXD;
using namespace Input;

// ------
// IInputSystem
// -
// ------
bool IInputSystem::_create_system(void)
{
	// 1. Early out if the api already exists
	if (m_inputApis.not_empty())
	{
		return false;
	}

	// 2. Create the desired apis
#if IsInputPS4()
	m_inputApis.push_back(std::make_unique< Input::HidApi::IInputApiPS4 >());
#endif //IsInputPS4()
#if IsInputSwitch()
	m_inputApis.push_back(std::make_unique< Input::HidApi::IInputApiSwitch >());
#endif //IsInputSwitch()
#if IsInputXInputH()
	m_inputApis.push_back(std::make_unique< Input::HidApi::IInputApiXInputH >());
#endif //IsInputXInputH()
#if IsInputSteam()
	m_inputApis.push_back(std::make_unique< Input::IInputApiSteam >());
#endif //IsInputSteam()
#if IsInputXInput()
	m_inputApis.push_back(std::make_unique< Input::IInputApiXInput >());
#endif //IsInputXInput()

	// 3. Create the desired apis
	bool bRetVal = true;
	fxd_for(auto& api, m_inputApis)
	{
		if (!api->create_api())
		{
			bRetVal = false;
		}
	}

	return bRetVal;
}

bool IInputSystem::_release_system(void)
{
	fxd_for(auto& api, m_inputApis)
	{
		FXD_RELEASE(api, release_api());
	}
	m_inputApis.clear();

	return true;
}

bool IInputSystem::_create_devices(void)
{
	_register_device(std::make_shared< Input::IMouse >());
	_register_device(std::make_shared< Input::IKeyboard >());

	for (FXD::U32 n1 = 0; n1 < XUSER_MAX_COUNT; n1++)
	{
		_register_device(std::make_shared< Input::IGamePadXInput >(n1));
	}

#if IsInputHIDAPI()
	struct hid_device_info* pDevs = hid_enumerate(0, 0);

	if (pDevs != nullptr)
	{
		HidApi::HidApiDevice* pDevice = FXD_NEW(HidApi::HidApiDevice)(pDevs);
		if (pDevice->should_ignore())
		{
			FXD_DELETE(pDevice);
		}
		else
		{
			switch (pDevice->controller_type())
			{
#if IsInputPS4()
				case Input::HidApi::E_HidControllerType::PS4:
				{
					_register_device(std::make_shared< Input::HidApi::IGamePadPS4 >(pDevice));
					break;
				}
#endif //IsInputPS4()
#if IsInputSwitch()
				case Input::HidApi::E_HidControllerType::SwitchJoycon:
				case Input::HidApi::E_HidControllerType::SwitchPro:
				{
					_register_device(std::make_shared< Input::HidApi::IGamePadSwitch >(pDevice));
					break;
				}
#endif //IsInputSwitch()
				default:
				{
					FXD_DELETE(pDevice);
					break;
				}
			}
		}
	}

	hid_free_enumeration(pDevs);
#endif //IsInputHIDAPI()

	return true;
}

bool IInputSystem::_release_devices(void)
{
	while (m_unusedDevices.not_empty())
	{
		_unregister_device(m_unusedDevices[0]);
	}

	m_unusedDevices.clear();
	return true;
}
#endif //IsInputWin32()