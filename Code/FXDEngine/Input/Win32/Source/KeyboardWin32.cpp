// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputWin32()
#include "FXDEngine/Input/Win32/KeyboardWin32.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Input/Events.h"
#include "FXDEngine/Job/EventHandler.h"

using namespace FXD;
using namespace Input;

static FXD::U32 modifierBits(WPARAM kc)
{
	switch (kc)
	{
		case VK_LSHIFT:
		{
			return FXD::STD::to_underlying(E_ModifierKey::LeftShift);
		}
		case VK_RSHIFT:
		{
			return FXD::STD::to_underlying(E_ModifierKey::RightShift);
		}
		case VK_LCONTROL:
		{
			return FXD::STD::to_underlying(E_ModifierKey::LeftControl);
		}
		case VK_RCONTROL:
		{
			return FXD::STD::to_underlying(E_ModifierKey::RightControl);
		}
		case VK_LMENU:
		{
			return FXD::STD::to_underlying(E_ModifierKey::LeftAlt);
		}
		case VK_RMENU:
		{
			return FXD::STD::to_underlying(E_ModifierKey::RightAlt);
		}
		default:
		{
			return 0;
		}
	}
}

// ------
// IKeyboard::Impl
// -
// ------
IKeyboard::Impl::Impl::Impl(void)
	: OnKeyboardButton(nullptr)
{
	m_osKeys.reset();
	m_osModifiers.reset();
	m_appKeys.reset();
	m_appModifiers.reset();

	enable_message_process(true);

	OnKeyboardButton = FXDEvent()->attach< Input::OnKeyboardButton >([&](auto evt)
	{
		PRINT_INFO << evt.Pressed;
	});
}

IKeyboard::Impl::~Impl(void)
{
	if (OnKeyboardButton != nullptr)
	{
		FXDEvent()->detach(OnKeyboardButton);
		OnKeyboardButton = nullptr;
	}
}

bool IKeyboard::Impl::process_win_msg(HWND /*hWnd*/, UINT msg, const FXD::UTF8* /*pMsgStr*/, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
		case WM_SYSKEYDOWN:
		case WM_KEYDOWN:
		{
			FXD::S32 nWin32Key = wParam;
			FXD::S32 nActualKey = nWin32Key;
			bool bIsRepeat = ((lParam & 0x40000000) != 0);
			if (!bIsRepeat)
			{
				switch (nWin32Key)
				{
					case VK_MENU:
					{
						nActualKey = ((lParam & 0x1000000) == 0) ? VK_LMENU : VK_RMENU;
						m_osModifiers.set(modifierBits(nActualKey));
						break;
					}
					case VK_CONTROL:
					{
						nActualKey = ((lParam & 0x1000000) == 0) ? VK_LCONTROL : VK_RCONTROL;
						m_osModifiers.set(modifierBits(nActualKey));
						break;
					}
					case VK_SHIFT:
					{
						nActualKey = MapVirtualKey((lParam & 0x00ff0000) >> 16, MAPVK_VSC_TO_VK_EX);
						m_osModifiers.set(modifierBits(nActualKey));
						break;
					}
					default:
					{
						break;
					}
				}
				m_osKeys.set(nActualKey, true);
			}
			break;
		}
		case WM_KEYUP:
		case WM_SYSKEYUP:
		{
			FXD::S32 nWin32Key = (FXD::S32)wParam;
			FXD::S32 nActualKey = nWin32Key;
			switch (nWin32Key)
			{
				case VK_MENU:
				{
					nActualKey = ((lParam & 0x1000000) == 0) ? VK_LMENU : VK_RMENU;
					m_osModifiers.reset(modifierBits(nActualKey));
					break;
				}
				case VK_CONTROL:
				{
					nActualKey = ((lParam & 0x1000000) == 0) ? VK_LCONTROL : VK_RCONTROL;
					m_osModifiers.reset(modifierBits(nActualKey));
					break;
				}
				case VK_SHIFT:
				{
					nActualKey = MapVirtualKey((lParam & 0x00ff0000) >> 16, MAPVK_VSC_TO_VK_EX);
					m_osModifiers.reset(modifierBits(nActualKey));
					break;
				}
				default:
				{
					break;
				}
			}
			m_osKeys.reset(nActualKey);
			break;
		}
		case WM_KILLFOCUS:
		{
			m_osKeys.reset();
			m_osModifiers.reset();
			m_appKeys.reset();
			m_appModifiers.reset();
			break;
		}
		default:
		{
			break;
		}
	}
	if ((msg == WM_SYSKEYUP) || (msg == WM_SYSKEYDOWN))
	{
		return false;
	}
	return true;
}

// ------
// IKeyboard
// -
// ------
IKeyboard::IKeyboard(void)
{
}

IKeyboard::~IKeyboard(void)
{
}

Core::String8 IKeyboard::get_config_name(void) const
{
	return UTF_8("Keyboard");
}

Core::String8 IKeyboard::get_device_name(void) const
{
	return UTF_8("Keyboard");
}

Input::E_DeviceType IKeyboard::get_device_type(void) const
{
	return Input::E_DeviceType::Keyboard;
}

bool IKeyboard::is_modifier_key_pressed(Input::E_ModifierKey eMKey) const
{
	return m_impl->m_appModifiers[FXD::STD::to_underlying(eMKey)];
}

void IKeyboard::_update(FXD::F32 /*dt*/)
{
	const Widget::WIDGET_DESC* pDefs = _get_widget_defs();
	for (FXD::U16 n1 = 0; pDefs[n1].Type != Input::E_Widget_type::Count; n1++)
	{
		const Widget::WIDGET_DESC& currentDef = pDefs[n1];
		if (currentDef.Type == Input::E_Widget_type::Digital)
		{
			bool bNext = m_impl->m_osKeys[currentDef.ID];
			bool bCurr = m_impl->m_appKeys[currentDef.ID];
			if (bCurr != bNext)
			{
				FXDEvent()->raise(Input::OnKeyboardButton(bNext, currentDef.Name.c_str(), currentDef.ID));
				PRINT_INFO << currentDef.Name;
			}
		}
	}
	m_impl->m_appKeys = m_impl->m_osKeys;
	m_impl->m_appModifiers = m_impl->m_osModifiers;
}

#define HANDLE_KEY_CASE_CHAR(k, c, f) Widget::WIDGET_DESC(k, Input::E_Widget_type::Digital, c, f)
#define HANDLE_KEY_CASE_FLAG(k, f) Widget::WIDGET_DESC(k, Input::E_Widget_type::Digital, #k, f)
static CONSTEXPR const Widget::WIDGET_DESC kKeyboardWidgets[] =
{
	HANDLE_KEY_CASE_FLAG(VK_SPACE, (Widget::kWidgetFlagActionButton | Widget::kWidgetFlagStartButton)),
	HANDLE_KEY_CASE_FLAG(VK_RETURN, (Widget::kWidgetFlagActionButton | Widget::kWidgetFlagStartButton)),

	HANDLE_KEY_CASE_CHAR('0', "VK_0", 0),
	HANDLE_KEY_CASE_CHAR('1', "VK_1", 0),
	HANDLE_KEY_CASE_CHAR('2', "VK_2", 0),
	HANDLE_KEY_CASE_CHAR('3', "VK_3", 0),
	HANDLE_KEY_CASE_CHAR('4', "VK_4", 0),
	HANDLE_KEY_CASE_CHAR('5', "VK_5", 0),
	HANDLE_KEY_CASE_CHAR('6', "VK_6", 0),
	HANDLE_KEY_CASE_CHAR('7', "VK_7", 0),
	HANDLE_KEY_CASE_CHAR('8', "VK_8", 0),
	HANDLE_KEY_CASE_CHAR('9', "VK_9", 0),
	HANDLE_KEY_CASE_CHAR('A', "VK_A", 0),
	HANDLE_KEY_CASE_CHAR('B', "VK_B", 0),
	HANDLE_KEY_CASE_CHAR('C', "VK_C", 0),
	HANDLE_KEY_CASE_CHAR('D', "VK_D", 0),
	HANDLE_KEY_CASE_CHAR('E', "VK_E", 0),
	HANDLE_KEY_CASE_CHAR('F', "VK_F", 0),
	HANDLE_KEY_CASE_CHAR('G', "VK_G", 0),
	HANDLE_KEY_CASE_CHAR('H', "VK_H", 0),
	HANDLE_KEY_CASE_CHAR('I', "VK_I", 0),
	HANDLE_KEY_CASE_CHAR('J', "VK_J", 0),
	HANDLE_KEY_CASE_CHAR('K', "VK_K", 0),
	HANDLE_KEY_CASE_CHAR('L', "VK_L", 0),
	HANDLE_KEY_CASE_CHAR('M', "VK_M", 0),
	HANDLE_KEY_CASE_CHAR('N', "VK_N", 0),
	HANDLE_KEY_CASE_CHAR('O', "VK_O", 0),
	HANDLE_KEY_CASE_CHAR('P', "VK_P", 0),
	HANDLE_KEY_CASE_CHAR('Q', "VK_Q", 0),
	HANDLE_KEY_CASE_CHAR('R', "VK_R", 0),
	HANDLE_KEY_CASE_CHAR('S', "VK_S", 0),
	HANDLE_KEY_CASE_CHAR('T', "VK_T", 0),
	HANDLE_KEY_CASE_CHAR('U', "VK_U", 0),
	HANDLE_KEY_CASE_CHAR('V', "VK_V", 0),
	HANDLE_KEY_CASE_CHAR('W', "VK_W", 0),
	HANDLE_KEY_CASE_CHAR('X', "VK_X", 0),
	HANDLE_KEY_CASE_CHAR('Y', "VK_Y", 0),
	HANDLE_KEY_CASE_CHAR('Z', "VK_Z", 0),

	HANDLE_KEY_CASE_FLAG(VK_LBUTTON, 0),
	HANDLE_KEY_CASE_FLAG(VK_RBUTTON, 0),
	HANDLE_KEY_CASE_FLAG(VK_CANCEL, 0),
	HANDLE_KEY_CASE_FLAG(VK_MBUTTON, 0),
#if(_WIN32_WINNT >= 0x0500)
	HANDLE_KEY_CASE_FLAG(VK_XBUTTON1, 0),
	HANDLE_KEY_CASE_FLAG(VK_XBUTTON2, 0),
#endif
	HANDLE_KEY_CASE_FLAG(VK_BACK, 0),
	HANDLE_KEY_CASE_FLAG(VK_TAB, 0),
	HANDLE_KEY_CASE_FLAG(VK_CLEAR, 0),
	HANDLE_KEY_CASE_FLAG(VK_RETURN, 0),
	HANDLE_KEY_CASE_FLAG(VK_SHIFT, 0),
	HANDLE_KEY_CASE_FLAG(VK_CONTROL, 0),
	HANDLE_KEY_CASE_FLAG(VK_MENU, 0),
	HANDLE_KEY_CASE_FLAG(VK_PAUSE, 0),
	HANDLE_KEY_CASE_FLAG(VK_CAPITAL, 0),
	HANDLE_KEY_CASE_FLAG(VK_KANA, 0),
	HANDLE_KEY_CASE_FLAG(VK_JUNJA, 0),
	HANDLE_KEY_CASE_FLAG(VK_FINAL, 0),
	HANDLE_KEY_CASE_FLAG(VK_KANJI, 0),
	HANDLE_KEY_CASE_FLAG(VK_ESCAPE, Widget::kWidgetFlagBackButton),
	HANDLE_KEY_CASE_FLAG(VK_CONVERT, 0),
	HANDLE_KEY_CASE_FLAG(VK_NONCONVERT, 0),
	HANDLE_KEY_CASE_FLAG(VK_ACCEPT, 0),
	HANDLE_KEY_CASE_FLAG(VK_MODECHANGE, 0),
	//HANDLE_KEY_CASE_FLAG(VK_SPACE, 0),
	HANDLE_KEY_CASE_FLAG(VK_PRIOR, 0),
	HANDLE_KEY_CASE_FLAG(VK_NEXT, 0),
	HANDLE_KEY_CASE_FLAG(VK_END, 0),
	HANDLE_KEY_CASE_FLAG(VK_HOME, 0),
	HANDLE_KEY_CASE_FLAG(VK_LEFT, 0),
	HANDLE_KEY_CASE_FLAG(VK_UP, 0),
	HANDLE_KEY_CASE_FLAG(VK_RIGHT, 0),
	HANDLE_KEY_CASE_FLAG(VK_DOWN, 0),
	HANDLE_KEY_CASE_FLAG(VK_SELECT, 0),
	HANDLE_KEY_CASE_FLAG(VK_PRINT, 0),
	HANDLE_KEY_CASE_FLAG(VK_EXECUTE, 0),
	HANDLE_KEY_CASE_FLAG(VK_SNAPSHOT, 0),
	HANDLE_KEY_CASE_FLAG(VK_INSERT, 0),
	HANDLE_KEY_CASE_FLAG(VK_DELETE, 0),
	HANDLE_KEY_CASE_FLAG(VK_HELP, 0),
	HANDLE_KEY_CASE_FLAG(VK_LWIN, 0),
	HANDLE_KEY_CASE_FLAG(VK_RWIN, 0),
	HANDLE_KEY_CASE_FLAG(VK_APPS, 0),
	HANDLE_KEY_CASE_FLAG(VK_SLEEP, 0),
	HANDLE_KEY_CASE_FLAG(VK_NUMPAD0, 0),
	HANDLE_KEY_CASE_FLAG(VK_NUMPAD1, 0),
	HANDLE_KEY_CASE_FLAG(VK_NUMPAD2, 0),
	HANDLE_KEY_CASE_FLAG(VK_NUMPAD3, 0),
	HANDLE_KEY_CASE_FLAG(VK_NUMPAD4, 0),
	HANDLE_KEY_CASE_FLAG(VK_NUMPAD5, 0),
	HANDLE_KEY_CASE_FLAG(VK_NUMPAD6, 0),
	HANDLE_KEY_CASE_FLAG(VK_NUMPAD7, 0),
	HANDLE_KEY_CASE_FLAG(VK_NUMPAD8, 0),
	HANDLE_KEY_CASE_FLAG(VK_NUMPAD9, 0),
	HANDLE_KEY_CASE_FLAG(VK_MULTIPLY, 0),
	HANDLE_KEY_CASE_FLAG(VK_ADD, 0),
	HANDLE_KEY_CASE_FLAG(VK_SEPARATOR, 0),
	HANDLE_KEY_CASE_FLAG(VK_SUBTRACT, 0),
	HANDLE_KEY_CASE_FLAG(VK_DECIMAL, 0),
	HANDLE_KEY_CASE_FLAG(VK_DIVIDE, 0),
	HANDLE_KEY_CASE_FLAG(VK_F1, 0),
	HANDLE_KEY_CASE_FLAG(VK_F2, 0),
	HANDLE_KEY_CASE_FLAG(VK_F3, 0),
	HANDLE_KEY_CASE_FLAG(VK_F4, 0),
	HANDLE_KEY_CASE_FLAG(VK_F5, 0),
	HANDLE_KEY_CASE_FLAG(VK_F6, 0),
	HANDLE_KEY_CASE_FLAG(VK_F7, 0),
	HANDLE_KEY_CASE_FLAG(VK_F8, 0),
	HANDLE_KEY_CASE_FLAG(VK_F9, 0),
	HANDLE_KEY_CASE_FLAG(VK_F10, 0),
	HANDLE_KEY_CASE_FLAG(VK_F11, 0),
	HANDLE_KEY_CASE_FLAG(VK_F12, 0),
	HANDLE_KEY_CASE_FLAG(VK_F13, 0),
	HANDLE_KEY_CASE_FLAG(VK_F14, 0),
	HANDLE_KEY_CASE_FLAG(VK_F15, 0),
	HANDLE_KEY_CASE_FLAG(VK_F16, 0),
	HANDLE_KEY_CASE_FLAG(VK_F17, 0),
	HANDLE_KEY_CASE_FLAG(VK_F18, 0),
	HANDLE_KEY_CASE_FLAG(VK_F19, 0),
	HANDLE_KEY_CASE_FLAG(VK_F20, 0),
	HANDLE_KEY_CASE_FLAG(VK_F21, 0),
	HANDLE_KEY_CASE_FLAG(VK_F22, 0),
	HANDLE_KEY_CASE_FLAG(VK_F23, 0),
	HANDLE_KEY_CASE_FLAG(VK_F24, 0),
	HANDLE_KEY_CASE_FLAG(VK_NUMLOCK, 0),
	HANDLE_KEY_CASE_FLAG(VK_SCROLL, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_FJ_JISHO, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_FJ_MASSHOU, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_FJ_TOUROKU, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_FJ_LOYA, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_FJ_ROYA, 0),
	HANDLE_KEY_CASE_FLAG(VK_LSHIFT, 0),
	HANDLE_KEY_CASE_FLAG(VK_RSHIFT, 0),
	HANDLE_KEY_CASE_FLAG(VK_LCONTROL, 0),
	HANDLE_KEY_CASE_FLAG(VK_RCONTROL, 0),
	HANDLE_KEY_CASE_FLAG(VK_LMENU, 0),
	HANDLE_KEY_CASE_FLAG(VK_RMENU, 0),
#if(_WIN32_WINNT >= 0x0500)
	HANDLE_KEY_CASE_FLAG(VK_BROWSER_BACK, 0),
	HANDLE_KEY_CASE_FLAG(VK_BROWSER_FORWARD, 0),
	HANDLE_KEY_CASE_FLAG(VK_BROWSER_REFRESH, 0),
	HANDLE_KEY_CASE_FLAG(VK_BROWSER_STOP, 0),
	HANDLE_KEY_CASE_FLAG(VK_BROWSER_SEARCH, 0),
	HANDLE_KEY_CASE_FLAG(VK_BROWSER_FAVORITES, 0),
	HANDLE_KEY_CASE_FLAG(VK_BROWSER_HOME, 0),
	HANDLE_KEY_CASE_FLAG(VK_VOLUME_MUTE, 0),
	HANDLE_KEY_CASE_FLAG(VK_VOLUME_DOWN, 0),
	HANDLE_KEY_CASE_FLAG(VK_VOLUME_UP, 0),
	HANDLE_KEY_CASE_FLAG(VK_MEDIA_NEXT_TRACK, 0),
	HANDLE_KEY_CASE_FLAG(VK_MEDIA_PREV_TRACK, 0),
	HANDLE_KEY_CASE_FLAG(VK_MEDIA_STOP, 0),
	HANDLE_KEY_CASE_FLAG(VK_MEDIA_PLAY_PAUSE, 0),
	HANDLE_KEY_CASE_FLAG(VK_LAUNCH_MAIL, 0),
	HANDLE_KEY_CASE_FLAG(VK_LAUNCH_MEDIA_SELECT, 0),
	HANDLE_KEY_CASE_FLAG(VK_LAUNCH_APP1, 0),
	HANDLE_KEY_CASE_FLAG(VK_LAUNCH_APP2, 0),
#endif
	HANDLE_KEY_CASE_FLAG(VK_OEM_1, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_PLUS, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_COMMA, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_MINUS, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_PERIOD, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_2, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_3, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_4, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_5, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_6, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_7, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_8, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_AX, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_102, 0),
	HANDLE_KEY_CASE_FLAG(VK_ICO_HELP, 0),
	HANDLE_KEY_CASE_FLAG(VK_ICO_00, 0),
	HANDLE_KEY_CASE_FLAG(VK_PROCESSKEY, 0),
	HANDLE_KEY_CASE_FLAG(VK_ICO_CLEAR, 0),
#if(_WIN32_WINNT >= 0x0500)
	HANDLE_KEY_CASE_FLAG(VK_PACKET, 0),
#endif
	HANDLE_KEY_CASE_FLAG(VK_OEM_RESET, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_JUMP, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_PA1, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_PA2, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_PA3, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_WSCTRL, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_CUSEL, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_ATTN, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_FINISH, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_COPY, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_AUTO, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_ENLW, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_BACKTAB, 0),
	HANDLE_KEY_CASE_FLAG(VK_ATTN, 0),
	HANDLE_KEY_CASE_FLAG(VK_CRSEL, 0),
	HANDLE_KEY_CASE_FLAG(VK_EXSEL, 0),
	HANDLE_KEY_CASE_FLAG(VK_EREOF, 0),
	HANDLE_KEY_CASE_FLAG(VK_PLAY, 0),
	HANDLE_KEY_CASE_FLAG(VK_ZOOM, 0),
	HANDLE_KEY_CASE_FLAG(VK_NONAME, 0),
	HANDLE_KEY_CASE_FLAG(VK_PA1, 0),
	HANDLE_KEY_CASE_FLAG(VK_OEM_CLEAR, 0),
	Widget::WIDGET_DESC()
};

const Widget::WIDGET_DESC* IKeyboard::_get_widget_defs(void) const
{
	return kKeyboardWidgets;
}

void IKeyboard::_get_widget_state(FXD::U16 nID, void* pState) const
{
	bool* pBool = (bool*)pState;
	*pBool = m_impl->m_appKeys[nID];
}
#endif //IsInputWin32()