// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputWin32()
#include "FXDEngine/Input/Win32/MouseWin32.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Input/Mapping.h"
#include "FXDEngine/Job/EventHandler.h"

using namespace FXD;
using namespace Input;

// ------
// IMouse::Impl
// -
// ------
IMouse::Impl::Impl::Impl(void)
	: m_nOSWheelPos(0)
	, m_nAppWheelPos(0)
	, OnMouseButton(nullptr)
	, OnMouseWheel(nullptr)
{
	m_mousePos = { 0, 0 };
	Memory::MemZero_T(m_osState);
	Memory::MemZero_T(m_appState);
	enable_message_process(true);

	OnMouseButton = FXDEvent()->attach< Input::OnMouseButton >([&](auto evt)
	{
		PRINT_INFO << evt.Pressed;
	});
	OnMouseWheel = FXDEvent()->attach< Input::OnMouseWheel >([&](auto evt)
	{
		PRINT_INFO << evt.Value;
	});
}

IMouse::Impl::~Impl(void)
{
	if (OnMouseButton != nullptr)
	{
		FXDEvent()->detach(OnMouseButton);
		OnMouseButton = nullptr;
	}
	if (OnMouseWheel != nullptr)
	{
		FXDEvent()->detach(OnMouseWheel);
		OnMouseWheel = nullptr;
	}
}

bool IMouse::Impl::process_win_msg(HWND /*hWnd*/, UINT msg, const FXD::UTF8* /*pMsgStr*/, WPARAM wParam, LPARAM /*lParam*/)
{
	switch (msg)
	{
		case WM_LBUTTONDOWN:
		{
			m_osState[FXD::STD::to_underlying(E_MouseButtons::Left)] = true;
			break;
		}
		case WM_LBUTTONUP:
		{
			m_osState[FXD::STD::to_underlying(E_MouseButtons::Left)] = false;
			break;
		}
		case WM_MBUTTONDOWN:
		{
			m_osState[FXD::STD::to_underlying(E_MouseButtons::Middle)] = true;
			break;
		}
		case WM_MBUTTONUP:
		{
			m_osState[FXD::STD::to_underlying(E_MouseButtons::Middle)] = false;
			break;
		}
		case WM_RBUTTONDOWN:
		{
			m_osState[FXD::STD::to_underlying(E_MouseButtons::Right)] = true;
			break;
		}
		case WM_RBUTTONUP:
		{
			m_osState[FXD::STD::to_underlying(E_MouseButtons::Right)] = false;
			break;
		}
		case WM_XBUTTONDOWN:
		{
			E_MouseButtons eMouseButton = (HIWORD(wParam) & XBUTTON1) ? E_MouseButtons::Thumb01 : E_MouseButtons::Thumb02;
			m_osState[FXD::STD::to_underlying(eMouseButton)] = true;
			break;
		}
		case WM_XBUTTONUP:
		{
			E_MouseButtons eMouseButton = (HIWORD(wParam) & XBUTTON1) ? E_MouseButtons::Thumb01 : E_MouseButtons::Thumb02;
			m_osState[FXD::STD::to_underlying(eMouseButton)] = false;
			break;
		}
		case WM_MOUSEWHEEL:
		{
			const FXD::F32 fSpinFactor = (1.0f / 120.0f);
			const SHORT nWheelDelta = GET_WHEEL_DELTA_WPARAM(wParam);
			m_nOSWheelPos += static_cast<FXD::F32>(nWheelDelta) * fSpinFactor;
			break;
		}
		default:
		{
			break;
		}
	}
	return true;
}


// ------
// IMouse
// -
// ------
IMouse::IMouse(void)
{
}

IMouse::~IMouse(void)
{
}

bool IMouse::is_connected(void) const
{
	return true;
}

bool IMouse::is_mouse_available(void) const
{
	return true;
}

Core::String8 IMouse::get_config_name(void) const
{
	return UTF_8("Mouse");
}

Core::String8 IMouse::get_device_name(void) const
{
	return UTF_8("Mouse");
}

Input::E_DeviceType IMouse::get_device_type(void) const
{
	return Input::E_DeviceType::Mouse;
}

FXD::F32 IMouse::get_scroll_wheel_pos(void) const
{
	return (FXD::F32)m_impl->m_nAppWheelPos;
}

void IMouse::_update(FXD::F32 /*dt*/)
{
	::GetCursorPos(&m_impl->m_mousePos);

	const Widget::WIDGET_DESC* pDefs = _get_widget_defs();
	for (FXD::U16 n1 = 0; pDefs[n1].Type != Input::E_Widget_type::Count; n1++)
	{
		const Widget::WIDGET_DESC& currentDef = pDefs[n1];
		if (currentDef.Type == Input::E_Widget_type::Digital)
		{
			bool bNext = m_impl->m_osState[currentDef.ID];
			bool bCurr = m_impl->m_appState[currentDef.ID];
			if (bCurr != bNext)
			{
				FXDEvent()->raise(Input::OnMouseButton(bNext, FXD::Math::Vector2I(m_impl->m_mousePos.x, m_impl->m_mousePos.y), (E_MouseButtons)currentDef.ID));
			}
		}
	}
	if (m_impl->m_nAppWheelPos != m_impl->m_nOSWheelPos)
	{
		FXDEvent()->raise(Input::OnMouseWheel((FXD::F32)m_impl->m_nOSWheelPos));
	}

	Memory::MemCopy(m_impl->m_appState, m_impl->m_osState, sizeof(m_impl->m_osState));
	m_impl->m_nAppWheelPos = m_impl->m_nOSWheelPos;
}

static CONSTEXPR const Widget::WIDGET_DESC kMouseWidgets[] =
{
	Widget::WIDGET_DESC(Input::GetMouseID(E_MouseButtons::Left), Input::E_Widget_type::Digital, Input::GetMouseName(E_MouseButtons::Left), 0),
	Widget::WIDGET_DESC(Input::GetMouseID(E_MouseButtons::Middle), Input::E_Widget_type::Digital, Input::GetMouseName(E_MouseButtons::Middle), 0),
	Widget::WIDGET_DESC(Input::GetMouseID(E_MouseButtons::Right), Input::E_Widget_type::Digital, Input::GetMouseName(E_MouseButtons::Right), 0),
	Widget::WIDGET_DESC(Input::GetMouseID(E_MouseButtons::Thumb01), Input::E_Widget_type::Digital, Input::GetMouseName(E_MouseButtons::Thumb01), 0),
	Widget::WIDGET_DESC(Input::GetMouseID(E_MouseButtons::Thumb02), Input::E_Widget_type::Digital, Input::GetMouseName(E_MouseButtons::Thumb02), 0),
	Widget::WIDGET_DESC()
};

const Widget::WIDGET_DESC* IMouse::_get_widget_defs(void) const
{
	return kMouseWidgets;
}

void IMouse::_get_widget_state(FXD::U16 nID, void* pState) const
{
	bool* pBool = (bool*)pState;
	*pBool = m_impl->m_appState[nID];
}

/*
Math::Vector IMouse::get_pointer_pos(const GFX::Viewport& display) const
{
	Math::Vector pos(0, 0);
	const GFX::IViewport::Impl &d = display->impl();
	if(d.m_clientRectValid)
	{
		pos.x((FXD::F32)(m_impl->m_mousePos.x - d.m_clientRect.left) / (FXD::F32)(d.m_clientRect.right - d.m_clientRect.left));
		pos.y((FXD::F32)(m_impl->m_mousePos.y - d.m_clientRect.top) / (FXD::F32)(d.m_clientRect.bottom - d.m_clientRect.top));
	}
	return pos;
}

bool IMouse::is_active_in_display(const GFX::Viewport& display) const
{
	const GFX::IViewport::Impl &d = display->impl();
	if(!d.m_clientRectValid)
		return false;
	if(IsIconic(d.m_hWnd))
		return false;
	if(GetForegroundWindow() != d.m_hWnd)
		return false;
	return true;
}
*/
#endif //IsInputWin32()