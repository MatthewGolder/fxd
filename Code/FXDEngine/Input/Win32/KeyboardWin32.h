// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_STEAM_KEYBOARDSTEAM_H
#define FXDENGINE_INPUT_STEAM_KEYBOARDSTEAM_H

#include "FXDEngine/Input/Types.h"

#if IsInputWin32()
#include "FXDEngine/App/Win32/MsgProcessWin32.h"
#include "FXDEngine/Container/Bitset.h"
#include "FXDEngine/Input/Events.h"
#include "FXDEngine/Input/Keyboard.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< Input::IKeyboard >
			// -
			// ------
			template <>
			class Impl< Input::IKeyboard > FINAL : public App::MsgProcessPC
			{
			public:
				friend class Input::IKeyboard;

			public:
				Impl(void);
				~Impl(void);

				bool process_win_msg(HWND hWnd, UINT msg, const FXD::UTF8* pMsgStr, WPARAM wParam, LPARAM lParam) FINAL;

			private:
				Container::Bitset< 256 > m_osKeys;
				Container::Bitset< FXD::STD::to_underlying(Input::E_ModifierKey::Count) > m_osModifiers;

				Container::Bitset< 256 > m_appKeys;
				Container::Bitset< FXD::STD::to_underlying(Input::E_ModifierKey::Count) > m_appModifiers;

				const FXD::Job::Handler< Input::OnKeyboardButton >* OnKeyboardButton;
			};
		} //namespace Impl
	} //namespace Core
} //namespace FXD

#endif //IsInputWin32()
#endif //FXDENGINE_INPUT_STEAM_KEYBOARDSTEAM_H