// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_WIN32_MOUSEWIN32_H
#define FXDENGINE_INPUT_WIN32_MOUSEWIN32_H

#include "FXDEngine/Input/Types.h"

#if IsInputWin32()
#include "FXDEngine/App/Win32/MsgProcessWin32.h"
#include "FXDEngine/Input/Events.h"
#include "FXDEngine/Input/Mouse.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< Input::IMouse >
			// -
			// ------
			template <>
			class Impl< Input::IMouse > FINAL : public App::MsgProcessPC
			{
			public:
				friend class Input::IMouse;

			public:
				Impl(void);
				~Impl(void);

				bool process_win_msg(HWND hWnd, UINT msg, const FXD::UTF8* pMsgStr, WPARAM wParam, LPARAM lParam) FINAL;

			private:
				POINT m_mousePos;

				FXD::S32 m_nOSWheelPos;
				bool m_osState[FXD::STD::to_underlying(Input::E_MouseButtons::Count)];
				FXD::S32 m_nAppWheelPos;
				bool m_appState[FXD::STD::to_underlying(Input::E_MouseButtons::Count)];

				const FXD::Job::Handler< Input::OnMouseButton >* OnMouseButton;
				const FXD::Job::Handler< Input::OnMouseWheel >* OnMouseWheel;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD

#endif //IsInputWin32()
#endif //FXDENGINE_INPUT_WIN32_MOUSEWIN32_H