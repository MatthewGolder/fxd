// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_INPUTMAP_H
#define FXDENGINE_INPUT_INPUTMAP_H

#include "FXDEngine/Container/Vector.h"
#include "FXDEngine/Container/Map.h"
#include "FXDEngine/Core/Expression.h"
#include "FXDEngine/Core/String.h"
#include "FXDEngine/Input/Types.h"

namespace FXD
{
	namespace Input
	{
		// ------
		// InputMap
		// -
		// ------
		class InputMap
		{
		public:
			InputMap(void);
			~InputMap(void);

			// Build the action map.
			void build(const InputMapDesc& inputMapDesc, const Core::String8& strConfig, const Container::Vector< InputDevice >& devices);

			// Query control values.
			FXD::F32 get_float(FXD::U32 nID) const;
			bool get_bool(FXD::U32 nID) const;

		public:
			// ------
			// DeviceAction
			// -
			// ------
			class DeviceAction
			{
			public:
				DeviceAction(void)
					: m_pVariableProvider(nullptr)
				{}
				~DeviceAction(void)
				{}

				GET_REF_RW(Core::Expression, expression, expression);

				SET(Core::IVariableProvider*, pVariableProvider, variable_provider);
				GET_R(Core::IVariableProvider*, pVariableProvider, variable_provider);

			private:
				Core::Expression m_expression;
				Core::IVariableProvider* m_pVariableProvider;
			};

			// ------
			// ControlMap
			// -
			// ------
			class ControlMap
			{
			public:
				ControlMap(void)
				{}
				~ControlMap(void)
				{}

				GET_REF_RW(Container::Vector< DeviceAction >, actions, actions);

			private:
				Container::Vector< DeviceAction > m_actions;
			};

		public:
			GET_REF_RW(Container::Vector< Input::InputDevice >, devices, devices);

		private:
			Container::Map< FXD::U32, ControlMap > m_mappings;
			Container::Vector< Input::InputDevice > m_devices;
		};

	} //namespace Input
} //namespace FXD
#endif //FXDENGINE_INPUT_INPUTMAP_H