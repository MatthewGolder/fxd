// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputSteam()
#include "FXDEngine/Input/Steam/InputApiSteam.h"

using namespace FXD;
using namespace Input;

// ------
// IInputApiSteam
// - 
// ------
IInputApiSteam::IInputApiSteam(void)
{
}

IInputApiSteam::~IInputApiSteam(void)
{
}

bool IInputApiSteam::create_api(void)
{
	bool bRetVal = true;

	return bRetVal;
}

bool IInputApiSteam::release_api(void)
{
	bool bRetVal = true;

	return bRetVal;
}

void IInputApiSteam::_process(FXD::F32 dt)
{
}
#endif //IsInputSteam()