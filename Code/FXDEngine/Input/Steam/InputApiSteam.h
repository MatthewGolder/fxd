// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_STEAM_INPUTAPISTEAM_H
#define FXDENGINE_INPUT_STEAM_INPUTAPISTEAM_H

#include "FXDEngine/Input/Types.h"

#if IsInputSteam()
#include "FXDEngine/Input/InputApi.h"
#include "FXDEngine/Input/Steam/Steam.h"

namespace FXD
{
	namespace Input
	{
		// ------
		// IInputApiSteam
		// -
		// ------
		class IInputApiSteam FINAL : public Input::IInputApi
		{
		public:
			IInputApiSteam(void);
			~IInputApiSteam(void);

			bool create_api(void) FINAL;
			bool release_api(void) FINAL;

		private:
			void _process(FXD::F32 dt) FINAL;
		};

	} //namespace Input
} //namespace FXD

#endif //IsInputSteam()
#endif //FXDENGINE_INPUT_STEAM_INPUTAPISTEAM_H