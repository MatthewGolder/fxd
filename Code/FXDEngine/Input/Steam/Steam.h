// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_STEAM_STEAM_H
#define FXDENGINE_INPUT_STEAM_STEAM_H

#include "FXDEngine/Input/Types.h"

#if IsInputSteam()

namespace FXD
{
	namespace Input
	{
		class IGamePadSteam;
		typedef std::shared_ptr< Input::IGamePadSteam > GamePadSteam;

	} //namespace Input
} //namespace FXD
#endif //IsInputSteam()
#endif //FXDENGINE_INPUT_STEAM_STEAM_H