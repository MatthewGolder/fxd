// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_INPUTSYSTEM_H
#define FXDENGINE_INPUT_INPUTSYSTEM_H

#include "FXDEngine/Container/Vector.h"
#include "FXDEngine/Input/InputApi.h"
#include "FXDEngine/Job/Future.h"

namespace FXD
{
	namespace Input
	{
		// ------
		// IInputSystem
		// -
		// ------
		class IInputSystem FINAL : public Core::NonCopyable
		{
		public:
			friend class IInputJPThread;

		public:
			IInputSystem(void);
			~IInputSystem(void);

			Job::Future< bool > create_system(void);
			Job::Future< bool > release_system(void);

			Job::Future< bool > create_devices(void);
			Job::Future< bool > release_devices(void);

			Job::Future< void > pause_haptics(void);
			Job::Future< void > resume_haptics(void);
			Job::Future< void > reset_haptics(void);

			GET_REF_R(Container::Vector< Input::InputDevice >, unusedDevices, unused_devices);
			GET_REF_R(Container::Vector< Input::InputDevice >, selectedDevices, selected_devices);

		private:
			bool _create_system(void);
			bool _release_system(void);

			bool _create_devices(void);
			bool _release_devices(void);

			void _pause_haptics(void);
			void _resume_haptics(void);
			void _reset_haptics(void);

			bool _register_device(Input::InputDevice device);
			bool _unregister_device(Input::InputDevice device);

			void _process(FXD::F32 dt);

		private:
			Container::Vector< Input::InputApi > m_inputApis;
			Container::Vector< Input::InputDevice > m_unusedDevices;
			Container::Vector< Input::InputDevice > m_selectedDevices;
		};
	} //namespace Input
} //namespace FXD
#endif //FXDENGINE_INPUT_INPUTSYSTEM_H