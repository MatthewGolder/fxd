// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_INPUTMAPDESC_H
#define FXDENGINE_INPUT_INPUTMAPDESC_H

#include "FXDEngine/Core/ExpressionTree.h"
#include "FXDEngine/Input/Types.h"
#include "FXDEngine/IO/FileCache.h"

namespace FXD
{
	namespace Input
	{
		// ------
		// IInputMapDesc
		// -
		// ------
		class IInputMapDesc : public IO::IAsset
		{
		public:
			friend class InputMap;

		private:
			// ------
			// Action
			// -
			// ------
			class Action
			{
			public:
				Action(void);
				~Action(void);

				SET_REF(Core::String8, strName, name);
				SET_REF(Core::ExpressionTree, value, value);
				SET(Core::E_ExpressionType, eType, expressionType);

				GET_REF_R(Core::String8, strName, name);
				GET_REF_RW(Core::ExpressionTree, value, value);
				GET_RW(Core::E_ExpressionType, eType, expressionType);

			private:
				Core::String8 m_strName;
				Core::ExpressionTree m_value;
				Core::E_ExpressionType m_eType;
			};

			// ------
			// Device
			// -
			// ------
			class Device
			{
			public:
				Device(void);
				~Device(void);

				SET_REF(Core::String8, strName, name);
				GET_REF_RW(Core::String8, strName, name);
				GET_REF_RW(Container::Vector< IInputMapDesc::Action >, actions, actions);

			private:
				Core::String8 m_strName;
				Container::Vector< IInputMapDesc::Action > m_actions;
			};

			// ------
			// Config
			// -
			// ------
			class Config
			{
			public:
				Config(void);
				~Config(void);

				SET_REF(Core::String8, strName, name);
				GET_REF_R(Core::String8, strName, name);
				GET_REF_RW(Container::Vector< IInputMapDesc::Device >, devices, devices);

			private:
				Core::String8 m_strName;
				Container::Vector< IInputMapDesc::Device > m_devices;
			};

		public:
			static Input::InputMapDesc FromStream(Core::StreamIn& stream, const Core::String8& strFileName);

		public:
			IInputMapDesc(const Core::String8& strFileName);
			~IInputMapDesc(void);

			FXD::U32 getControlID(const Core::String8& strName) const;

			bool is_platform_supported(void) const FINAL;
			const Core::StringView8 get_asset_type(void) const FINAL { return UTF_8("IInputMapDesc"); }

			GET_REF_RW(Container::Vector< IInputMapDesc::Config >, configs, configs);

		private:
			void build_control_ids(void);

		private:
			Container::Vector< IInputMapDesc::Config > m_configs;
			Container::Map< Core::String8, FXD::U32 > m_ids;
		};

	} //namespace Input
} //namespace FXD
#endif //FXDENGINE_INPUT_INPUTMAPDESC_H