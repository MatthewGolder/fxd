// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_HIDAPI_PS4_PS4_H
#define FXDENGINE_INPUT_HIDAPI_PS4_PS4_H

#include "FXDEngine/Input/Types.h"

#if IsInputPS4()

namespace FXD
{
	namespace Input
	{
		namespace HidApi
		{
			class IGamePadPS4;
			typedef std::shared_ptr< Input::HidApi::IGamePadPS4 > GamePadPS4;
		} //namespace HidApi
	} //namespace Input
} //namespace FXD
#endif //IsInputPS4()
#endif //FXDENGINE_INPUT_HIDAPI_PS4_PS4_H