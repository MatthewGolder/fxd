// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_HIDAPI_PS4_INPUTAPIPS4_H
#define FXDENGINE_INPUT_HIDAPI_PS4_INPUTAPIPS4_H

#include "FXDEngine/Input/Types.h"

#if IsInputPS4()
#include "FXDEngine/Input/InputApi.h"
#include "FXDEngine/Input/HIDAPI/PS4/PS4.h"

namespace FXD
{
	namespace Input
	{
		namespace HidApi
		{
			// ------
			// IInputApiPS4
			// -
			// ------
			class IInputApiPS4 FINAL : public Input::IInputApi
			{
			public:
				IInputApiPS4(void);
				~IInputApiPS4(void);

				bool create_api(void) FINAL;
				bool release_api(void) FINAL;

			private:
				void _process(FXD::F32 dt) FINAL;
			};

		} //namespace HidApi
	} //namespace Input
} //namespace FXD

#endif //IsInputPS4()
#endif //FXDENGINE_INPUT_HIDAPI_PS4_INPUTAPIPS4_H