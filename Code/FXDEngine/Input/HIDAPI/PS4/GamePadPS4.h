// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_HIDAPI_PS4_GAMEPADPS4_H
#define FXDENGINE_INPUT_HIDAPI_PS4_GAMEPADPS4_H

#include "FXDEngine/Input/Types.h"

#if IsInputPS4()
#include "FXDEngine/Input/Events.h"
#include "FXDEngine/Input/InputDevice.h"
#include "FXDEngine/Input/HIDAPI/HidApi.h"
#include "FXDEngine/Input/HIDAPI/PS4/PS4.h"

namespace FXD
{
	namespace Input
	{
		namespace HidApi
		{
			// ------
			// IGamePadPS4
			// -
			// ------
			class IGamePadPS4 FINAL : public Input::IInputDevice
			{
			public:
				struct PS4PacketState
				{
					FXD::U8 nThumbLX;
					FXD::U8 nThumbLY;
					FXD::U8 nThumbRX;
					FXD::U8 nThumbRY;
					FXD::U8 nButtons[2];
					FXD::U8 nCounterGuide;
					FXD::U8 nLTrigger;
					FXD::U8 nRTrigger;
					FXD::U8 _rgucPad0[3];
					FXD::S16 sGyroX;
					FXD::S16 sGyroY;
					FXD::S16 sGyroZ;
					FXD::S16 sAccelX;
					FXD::S16 sAccelY;
					FXD::S16 sAccelZ;
					FXD::U8 _rgucPad1[5];
					FXD::U8 nBatteryLevel;
					FXD::U8 _rgucPad2[4];
					FXD::U8 nTrackpad1;
					FXD::U8 pTrackpadData1[3];
					FXD::U8 nTrackpad2;
					FXD::U8 pTrackpadData2[3];

					const FXD::U16 touchpad1_x(void)const;
					const FXD::U16 touchpad1_y(void)const;
					const FXD::U16 touchpad2_x(void)const;
					const FXD::U16 touchpad2_y(void)const;
				};

				struct DS4EffectsState
				{
					FXD::U8 nRumbleRight;
					FXD::U8 nRumbleLeft;
					FXD::U8 nLedRed;
					FXD::U8 nLedGreen;
					FXD::U8 nLedBlue;
					FXD::U8 nLedDelayOn;
					FXD::U8 nLedDelayOff;
					FXD::U8 _rgucPad0[8];
					FXD::U8 nVolumeLeft;
					FXD::U8 nVolumeRight;
					FXD::U8 nVolumeMic;
					FXD::U8 nVolumeSpeaker;
				};

				struct PS4Context
				{
					PS4Context(HidApi::HidApiDevice* pHDevice, const FXD::S32 nPort);

					HidApi::HidApiDevice* m_pHDevice;
					FXD::U32 m_nPortIndex;
					bool m_bConnected;
					bool m_bDongle;
					bool m_bBluetooth;
					bool m_bColLedDirty;
					bool m_bAudioSupported;
					bool m_bRumbleSupported;
					Core::ColourRGB m_colLed;
					PS4PacketState m_state;
				};

			public:
				IGamePadPS4(HidApi::HidApiDevice* pHDevice);
				~IGamePadPS4(void);

				void set_colour(const Core::ColourRGB& col);

				bool is_connected(void) const FINAL;
				bool is_gamepad_available(void) const FINAL;
				bool supports_rumble(void) const FINAL;

				Core::String8 get_config_name(void) const FINAL;
				Core::String8 get_device_name(void) const FINAL;
				Input::E_DeviceType get_device_type(void) const FINAL;

			protected:
				void _update(FXD::F32 dt) FINAL;

				const Widget::WIDGET_DESC* _get_widget_defs(void) const FINAL;
				void _get_widget_state(FXD::U16 nID, void* pState) const FINAL;

			private:
				PS4Context m_context;
				const FXD::Job::Handler< Input::OnControllerButton >* OnControllerButton;
				const FXD::Job::Handler< Input::OnControllerAnalog >* OnControllerAnalog;
				const FXD::Job::Handler< Input::OnTouchPad >* OnTouchPad;
			};

		} //namespace HidApi
	} //namespace Input
} //namespace FXD
#endif //IsInputPS4()
#endif //FXDENGINE_INPUT_HIDAPI_PS4_GAMEPADPS4_H