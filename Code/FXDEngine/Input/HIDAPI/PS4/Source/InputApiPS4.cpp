// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputPS4()
#include "FXDEngine/Input/HIDAPI/PS4/InputApiPS4.h"

using namespace FXD;
using namespace Input;
using namespace HidApi;

// ------
// IInputApiPS4
// - 
// ------
IInputApiPS4::IInputApiPS4(void)
{
}

IInputApiPS4::~IInputApiPS4(void)
{
}

bool IInputApiPS4::create_api(void)
{
	bool bRetVal = true;

	return bRetVal;
}

bool IInputApiPS4::release_api(void)
{
	bool bRetVal = true;

	return bRetVal;
}

void IInputApiPS4::_process(FXD::F32 /*dt*/)
{
}
#endif //IsInputPS4()