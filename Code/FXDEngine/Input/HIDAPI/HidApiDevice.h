// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_HIDAPI_HIDAPIDEVICE_H
#define FXDENGINE_INPUT_HIDAPI_HIDAPIDEVICE_H

#include "FXDEngine/Input/Types.h"

#if IsInputHIDAPI()
#include "FXDEngine/Input/HIDAPI/HidApi.h"
#include "FXDEngine/Input/HIDAPI/HidController.h"

namespace FXD
{
	namespace Input
	{
		namespace HidApi
		{
			struct HidApiDevice
			{
			public:
				HidApiDevice(struct hid_device_info* pDevs);
				~HidApiDevice(void);

				bool should_ignore(void)const;
				Input::HidApi::E_HidControllerType controller_type(void)const;

				hid_device* device(void)const { return m_pDev; }
				GET_REF_R(Core::String8, strPath, path);
				GET_REF_R(Core::String16, strName, name);
				GET_R(Input::HidApi::E_USB_Vendor, eVendorId, vendorId);
				GET_R(FXD::U16, nProductID, productID);
				GET_R(FXD::U16, nVersion, version);
				GET_R(FXD::S32, nInterfaceNumber, interfaceNumber);
				GET_R(FXD::S32, nInterfaceClass, interfaceClass);
				GET_R(FXD::S32, nInterfaceSubclass, interfaceSubclass);
				GET_R(FXD::S32, nInterfaceProtocol, interfaceProtocol);
				GET_REF_R(Input::HidApi::HidGuid, guid, guid);

			private:
				hid_device* m_pDev;
				Core::String8 m_strPath;
				Core::String16 m_strName;
				Input::HidApi::E_USB_Vendor m_eVendorId;
				FXD::U16 m_nProductID;
				FXD::U16 m_nVersion;
				FXD::S32 m_nInterfaceNumber;
				FXD::S32 m_nInterfaceClass;
				FXD::S32 m_nInterfaceSubclass;
				FXD::S32 m_nInterfaceProtocol;
				Input::HidApi::HidGuid m_guid;
			};
		} //namespace HidApi
	} //namespace Input
} //namespace FXD
#endif //IsInputHIDAPI()
#endif //FXDENGINE_INPUT_HIDAPI_HIDAPIDEVICE_H