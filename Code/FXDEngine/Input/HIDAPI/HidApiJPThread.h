// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_HIDAPI_HIDAPIJPTHREAD_H
#define FXDENGINE_INPUT_HIDAPI_HIDAPIJPTHREAD_H

#include "FXDEngine/Input/Types.h"

#if IsInputHIDAPI()
#include "FXDEngine/Core/Timer.h"
#include "FXDEngine/Core/FPS.h"
#include "FXDEngine/Job/AsyncJob.h"
#include "FXDEngine/Input/HIDAPI/HidApi.h"

namespace FXD
{
	namespace Input
	{
		namespace HidApi
		{
			// ------
			// IHidApiJPThread
			// -
			// ------
			class IHidApiJPThread : public Job::IAsyncTaskPoolThread
			{
			public:
				IHidApiJPThread(void);
				virtual ~IHidApiJPThread(void);

				const Core::StringView8 display_name(void) const FINAL { return UTF_8("HidApiPThread"); }

				GET_REF_R(HidApi::HidApiManager, hidApiManager, hidApiManager);

			protected:
				bool init_pool(void) FINAL;
				bool shutdown_pool(void) FINAL;
				void idle_process(void) FINAL;
				bool is_thread_restricted(void) const FINAL;

			private:
				Core::FPS m_fps;
				Core::Timer m_timer;
				HidApi::HidApiManager m_hidApiManager;
			};

			namespace Funcs
			{
				extern bool IsHidApiThreadRestricted(void);
				extern void RestrictHidApiThread(void);
				extern void UnrestrictHidApiThread(void);
			} //namespace Funcs

		} //namespace HidApi
	} //namespace Input
} //namespace FXD
#endif //IsInputHIDAPI()
#endif //FXDENGINE_INPUT_HIDAPI_HIDAPIJPTHREAD_H