// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_HIDAPI_HIDAPI_H
#define FXDENGINE_INPUT_HIDAPI_HIDAPI_H

#include "FXDEngine/Input/Types.h"

#if IsInputHIDAPI()
#include "3rdParty/Hidapi/hidapi.h"

#include <memory>

namespace FXD
{
	namespace Input
	{
		namespace HidApi
		{
#			define HARDWARE_BUS_USB 0x03
#			define HARDWARE_BUS_BLUETOOTH 0x05
#			define USB_PACKET_LENGTH 64

			struct HidApiDevice;
	
			class IHidApiManager;
			typedef std::unique_ptr< HidApi::IHidApiManager > HidApiManager;

		} //namespace HidApi
	} //namespace Input
} //namespace FXD
#endif //IsInputHIDAPI()
#endif //FXDENGINE_INPUT_HIDAPI_HIDAPI_H