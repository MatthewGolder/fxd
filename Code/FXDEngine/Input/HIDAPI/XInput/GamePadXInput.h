// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_HIDAPI_XINPUT_GAMEPADXINPUT_H
#define FXDENGINE_INPUT_HIDAPI_XINPUT_GAMEPADXINPUT_H

#include "FXDEngine/Input/Types.h"

#if IsInputXInputH()
#include "FXDEngine/Input/Events.h"
#include "FXDEngine/Input/InputDevice.h"
#include "FXDEngine/Input/HIDAPI/HidApi.h"
#include "FXDEngine/Input/HIDAPI/XInput/XInput.h"

namespace FXD
{
	namespace Input
	{
		namespace HidApi
		{
			// ------
			// IGamePadXInputH
			// -
			// ------
			class IGamePadXInputH FINAL : public Input::IInputDevice
			{
			public:
				struct XInputPacketState
				{
					FXD::U16 leftX;
					FXD::U16 leftY;
					FXD::U16 rightX;
					FXD::U16 rightY;

					FXD::U8 leftTrigger;
					FXD::U8 righttrigger;

					FXD::U8 buttons[USB_PACKET_LENGTH];
				};

				struct XInputContext
				{
					XInputContext(HidApi::HidApiDevice* pHDevice, const FXD::S32 nPort);

					HidApi::HidApiDevice* m_pHDevice;
					bool m_bConnected;
					bool m_bRumbleDirty;
					//BYTE m_subType;
					FXD::U32 m_nPortIndex;
					XInputPacketState m_state;
				};

			public:
				IGamePadXInputH(HidApi::HidApiDevice* pHDevice);
				~IGamePadXInputH(void);

				bool is_connected(void) const FINAL;
				bool is_gamepad_available(void) const FINAL;
				bool supports_rumble(void) const FINAL;

				Core::String8 get_config_name(void) const FINAL;
				Core::String8 get_device_name(void) const FINAL;
				Input::E_DeviceType get_device_type(void) const FINAL;

			protected:
				void _update(FXD::F32 dt) FINAL;

				const Widget::WIDGET_DESC* _get_widget_defs(void) const FINAL;
				void _get_widget_state(FXD::U16 nID, void* pState) const FINAL;

			private:
				XInputContext m_context;
				const FXD::Job::Handler< Input::OnControllerButton >* OnControllerButton;
				const FXD::Job::Handler< Input::OnControllerAnalog >* OnControllerAnalog;
			};

		} //namespace HidApi
	} //namespace Input
} //namespace FXD
#endif //IsInputXInputH()
#endif //FXDENGINE_INPUT_HIDAPI_XINPUT_GAMEPADXINPUT_H