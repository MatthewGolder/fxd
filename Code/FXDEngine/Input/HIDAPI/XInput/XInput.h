// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_HIDAPI_XINPUT_XINPUT_H
#define FXDENGINE_INPUT_HIDAPI_XINPUT_XINPUT_H

#include "FXDEngine/Input/Types.h"

#if IsInputXInputH()

namespace FXD
{
	namespace Input
	{
		namespace HidApi
		{
			class IGamePadXInputH;
			typedef std::shared_ptr< Input::HidApi::IGamePadXInputH > GamePadXInputH;

		} //namespace HidApi
	} //namespace Input
} //namespace FXD
#endif //IsInputXInputH()
#endif //FXDENGINE_INPUT_HIDAPI_XINPUT_XINPUT_H