// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputXInputH()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Input/Mapping.h"
#include "FXDEngine/Input/HIDAPI/HidApiDevice.h"
#include "FXDEngine/Input/HIDAPI/HidApiJPThread.h"
#include "FXDEngine/Input/HIDAPI/HidApiManager.h"
#include "FXDEngine/Input/HIDAPI/XInput/GamePadXInput.h"
#include "FXDEngine/Job/EventHandler.h"

using namespace FXD;
using namespace Input;
using namespace HidApi;

#define X_GAMEPAD_LEFT_THUMB_DEADZONE 7849
#define X_GAMEPAD_RIGHT_THUMB_DEADZONE 8689
namespace
{
	static FXD::F32 map_trigger_value(FXD::U8 nVal)
	{
		return ((FXD::F32)nVal / 255.0f);
	}

	static FXD::F32 map_stick_value(FXD::U16 nVal, const FXD::S16 nDeadZone)
	{
		FXD::S16 nValS = (FXD::S16)nVal - 32768;

		if (nValS < 0)
		{
			nValS = (nValS + nDeadZone);
			if (nValS > 0)
			{
				nValS = 0;
			}
			nValS = (nValS * 32768) / (32768 - nDeadZone);
		}
		else
		{
			nValS = (nValS - nDeadZone);
			if (nValS < 0)
			{
				nValS = 0;
			}
			nValS = (nValS * 32768) / (32768 - nDeadZone);
		}
		return (FXD::F32)nValS / 32768.0f;
	}
}

// ------
// IGamePadXInputH::XInputContext
// -
// ------
IGamePadXInputH::XInputContext::XInputContext(HidApi::HidApiDevice* pHDevice, const FXD::S32 nPort)
	: m_pHDevice(pHDevice)
	, m_bConnected(false)
	, m_bRumbleDirty(false)
	//, m_subType(0)
	, m_nPortIndex(nPort)
{
	//Memory::MemZero_T(m_state);
}

// ------
// IGamePadXInputH
// -
// ------
IGamePadXInputH::IGamePadXInputH(HidApi::HidApiDevice* pHDevice)
	: IInputDevice()
	, m_context(pHDevice, 0)
	, OnControllerButton(nullptr)
	, OnControllerAnalog(nullptr)
{
	OnControllerButton = FXDEvent()->attach< Input::OnControllerButton >([&](Input::OnControllerButton evt)
	{
		if (evt.Device == this)
		{
			PRINT_INFO << Input::GetGamePadName(evt.Control);
		}
	});
	OnControllerAnalog = FXDEvent()->attach< Input::OnControllerAnalog >([&](Input::OnControllerAnalog evt)
	{
		if (evt.Device == this)
		{
			PRINT_INFO << evt.Value;
		}
	});

	m_context.m_pHDevice->pDev = hid_open_path(m_context.m_pHDevice->path.c_str(), 0);
}

IGamePadXInputH::~IGamePadXInputH(void)
{
	if (OnControllerButton != nullptr)
	{
		FXDEvent()->detach(OnControllerButton);
		OnControllerButton = nullptr;
	}
	if (OnControllerAnalog != nullptr)
	{
		FXDEvent()->detach(OnControllerAnalog);
		OnControllerAnalog = nullptr;
	}
}

bool IGamePadXInputH::is_connected(void) const
{
	return (m_context.m_bConnected);
}

bool IGamePadXInputH::is_gamepad_available(void) const
{
	return (m_context.m_bConnected);
}

bool IGamePadXInputH::supports_rumble(void) const
{
	return false;
}

Core::String8 IGamePadXInputH::get_config_name(void) const
{
	return UTF_8("XInput");
}

Core::String8 IGamePadXInputH::get_device_name(void) const
{
	/*
	switch (m_context.m_subType)
	{
		case XINPUT_DEVSUBTYPE_GAMEPAD:
		{
			return UTF_8("XInput Controller");
		}
		case XINPUT_DEVSUBTYPE_WHEEL:
		{
			return UTF_8("XInput Wheel");
		}
		case XINPUT_DEVSUBTYPE_ARCADE_STICK:
		{
			return UTF_8("XInput ArcadeStick");
		}
		case XINPUT_DEVSUBTYPE_FLIGHT_STICK:
		{
			return UTF_8("XInput FlightStick");
		}
		case XINPUT_DEVSUBTYPE_DANCE_PAD:
		{
			return UTF_8("XInput DancePad");
		}
		case XINPUT_DEVSUBTYPE_GUITAR:
		case XINPUT_DEVSUBTYPE_GUITAR_ALTERNATE:
		case XINPUT_DEVSUBTYPE_GUITAR_BASS:
		{
			return UTF_8("XInput Guitar");
		}
		case XINPUT_DEVSUBTYPE_DRUM_KIT:
		{
			return UTF_8("XInput DrumKit");
		}
		case XINPUT_DEVSUBTYPE_ARCADE_PAD:
		{
			return UTF_8("XInput ArcadePad");
		}
		default:
		{
			break;
		}
	}
	*/
	return UTF_8("Unknown");
}

Input::E_DeviceType IGamePadXInputH::get_device_type(void) const
{
	return Input::E_DeviceType::Unknown;
}

void IGamePadXInputH::_update(FXD::F32 dt)
{
	bool bWasConnected = m_context.m_bConnected;

	FXD::U8 pData[USB_PACKET_LENGTH];
	FXD::S32 nSize = 0;
	XInputPacketState* pPacket = nullptr;
	{
		while ((nSize = FXDHidApi()->hidApiManager()->read_input(m_context.m_pHDevice, pData, sizeof(pData), 0)) > 0)
		{
			pPacket = (XInputPacketState*)(pData);
			break;
		}
	}
	m_context.m_bConnected = !(nSize < 0);

	if (!bWasConnected && m_context.m_bConnected)
	{
		FXDEvent()->raise(Input::OnControllerConnection(this, true, m_context.m_nPortIndex));
	}
	else if (bWasConnected && !m_context.m_bConnected)
	{
		FXDEvent()->raise(Input::OnControllerConnection(this, false, m_context.m_nPortIndex));
	}

	if (pPacket != nullptr)
	{
		/*
		if (pPacket->nBatteryLevel & 0x10)
		{
			m_ePowerLevel = E_PowerLevel::Wired;
		}
		else
		{
			// Battery level ranges from 0 to 10
			FXD::S32 nLevel = (pPacket->nBatteryLevel & 0xF);
			if (nLevel == 0)
			{
				m_ePowerLevel = E_PowerLevel::Empty;
			}
			else if (nLevel <= 2)
			{
				m_ePowerLevel = E_PowerLevel::Low;
			}
			else if (nLevel <= 7)
			{
				m_ePowerLevel = E_PowerLevel::Medium;
			}
			else
			{
				m_ePowerLevel = E_PowerLevel::Full;
			}
		}
		*/

		if (m_context.m_bConnected)
		{
			/*
			bool bStateDirty = false;
			FXD::F32 fLeftRumble = 0.0f;
			FXD::F32 fRightRumble = 0.0f;

			if (m_context.m_bColLedDirty)
			{
				bStateDirty = true;
				fLeftRumble = m_rumble.m_fLeftRumble;
				fLeftRumble = m_rumble.m_fRightRumble;
			}

			if (m_context.m_bRumbleDirty)
			{
				bStateDirty = true;
				fLeftRumble = m_rumble.m_fLeftRumble;
				fLeftRumble = m_rumble.m_fRightRumble;
			}
			else
			{
				if (m_rumble.m_fLeftRumble > 0.0f || m_rumble.m_fRightRumble > 0.0f || m_rumble.m_fDuration > 0.0f)
				{
					m_fRumbleExpiration += dt;
					if (m_fRumbleExpiration > m_rumble.m_fDuration)
					{
						m_rumble.m_fLeftRumble = 0.0f;
						m_rumble.m_fRightRumble = 0.0f;
						m_rumble.m_fDuration = 0.0f;
						m_fRumbleExpiration = 0.0f;
						bStateDirty = true;
					}
				}
			}

			if (bStateDirty)
			{
				PS4SetState(m_context, fLeftRumble, fRightRumble);
				m_context.m_bRumbleDirty = false;
				m_context.m_bColLedDirty = false;
			}
			*/
		}

		if (pPacket != nullptr && (m_context.m_bConnected || bWasConnected))
		{
			{
				FXD::U8 nData = (pPacket->buttons[1] & 0x3C);
				FXD::U8 nLastData = (m_context.m_state.buttons[1] & 0x3C);

				bool bUp = ((nData == 4) || (nData == 8) || (nData == 32));
				bool bLastUp = ((nLastData == 4) || (nLastData == 8) || (nLastData == 32));

				bool bDown = ((nData == 16) || (nData == 20) || (nData == 24));
				bool bLastDown = ((nLastData == 16) || (nLastData == 20) || (nLastData == 24));

				bool bLeft = ((nData == 24) || (nData == 28) || (nData == 32));
				bool bLastLeft = ((nLastData == 24) || (nLastData == 28) || (nLastData == 32));

				bool bRight = ((nData == 8) || (nData == 12) || (nData == 16));
				bool bLastRight = ((nLastData == 8) || (nLastData == 12) || (nLastData == 16));

				if (bUp != bLastUp)
				{
					FXDEvent()->raise(Input::OnControllerButton(this, bLeft, E_GamePadButtons::DPadUp, m_context.m_nPortIndex));
				}
				if (bDown != bLastDown)
				{
					FXDEvent()->raise(Input::OnControllerButton(this, bLeft, E_GamePadButtons::DPadDown, m_context.m_nPortIndex));
				}
				if (bLeft != bLastLeft)
				{
					FXDEvent()->raise(Input::OnControllerButton(this, bLeft, E_GamePadButtons::DPadLeft, m_context.m_nPortIndex));
				}
				if (bRight != bLastRight)
				{
					FXDEvent()->raise(Input::OnControllerButton(this, bLeft, E_GamePadButtons::DPadRight, m_context.m_nPortIndex));
				}
			}
			if ((pPacket->buttons[1] & 0x01) != (m_context.m_state.buttons[1] & 0x01))
			{
				const bool bVal = (pPacket->buttons[1] & 0x01);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::LeftThumb, m_context.m_nPortIndex));
			}
			if ((pPacket->buttons[1] & 0x02) != (m_context.m_state.buttons[1] & 0x02))
			{
				const bool bVal = (pPacket->buttons[1] & 0x02);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::RightThumb, m_context.m_nPortIndex));
			}
			if ((pPacket->buttons[0] & 0x10) != (m_context.m_state.buttons[0] & 0x10))
			{
				const bool bVal = (pPacket->buttons[0] & 0x10);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::LeftBumper, m_context.m_nPortIndex));
			}
			if ((pPacket->buttons[0] & 0x20) != (m_context.m_state.buttons[0] & 0x20))
			{
				const bool bVal = (pPacket->buttons[0] & 0x20);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::RightBumper, m_context.m_nPortIndex));
			}

			if ((pPacket->buttons[0] & 0x80) != (m_context.m_state.buttons[0] & 0x80))
			{
				const bool bVal = (pPacket->buttons[0] & 0x80);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::Start, m_context.m_nPortIndex));
			}
			if ((pPacket->buttons[0] & 0x40) != (m_context.m_state.buttons[0] & 0x40))
			{
				const bool bVal = (pPacket->buttons[0] & 0x40);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::Back, m_context.m_nPortIndex));
			}
			if ((pPacket->buttons[0] & 0x01) != (m_context.m_state.buttons[0] & 0x01))
			{
				const bool bVal = (pPacket->buttons[0] & 0x01);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::AButton, m_context.m_nPortIndex));
			}
			if ((pPacket->buttons[0] & 0x02) != (m_context.m_state.buttons[0] & 0x02))
			{
				const bool bVal = (pPacket->buttons[0] & 0x02);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::BButton, m_context.m_nPortIndex));
			}
			if ((pPacket->buttons[0] & 0x04) != (m_context.m_state.buttons[0] & 0x04))
			{
				const bool bVal = (pPacket->buttons[0] & 0x04);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::XButton, m_context.m_nPortIndex));
			}
			if ((pPacket->buttons[0] & 0x08) != (m_context.m_state.buttons[0] & 0x08))
			{
				const bool bVal = (pPacket->buttons[0] & 0x08);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::YButton, m_context.m_nPortIndex));
			}
			/*
			if ((pPacket->nCounterGuide & 0x03) != (m_context.m_state.nCounterGuide & 0x03))
			{
				const bool bVal = (pPacket->nCounterGuide & 0x03);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::Guide, m_context.m_nPortIndex));
			}
			if ((pPacket->leftTrigger) != (m_context.m_state.leftTrigger))
			{
				const FXD::F32 fVal = map_trigger_value(pPacket->leftTrigger);
				FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::LeftTrigger, m_context.m_nPortIndex));
			}
			if ((pPacket->nRTrigger) != (m_context.m_state.nRTrigger))
			{
				const FXD::F32 fVal = map_trigger_value(pPacket->nRTrigger);
				FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::RightTrigger, m_context.m_nPortIndex));
			}
			*/
			if (map_stick_value(pPacket->leftX, X_GAMEPAD_LEFT_THUMB_DEADZONE) !=
				map_stick_value(m_context.m_state.leftX, X_GAMEPAD_LEFT_THUMB_DEADZONE))
			{
				const FXD::F32 fVal = map_stick_value(pPacket->leftX, X_GAMEPAD_LEFT_THUMB_DEADZONE);
				FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::LeftThumbX, m_context.m_nPortIndex));
			}
			if (map_stick_value(pPacket->leftY, X_GAMEPAD_LEFT_THUMB_DEADZONE) !=
				map_stick_value(m_context.m_state.leftY, X_GAMEPAD_LEFT_THUMB_DEADZONE))
			{
				const FXD::F32 fVal = map_stick_value(pPacket->leftY, X_GAMEPAD_LEFT_THUMB_DEADZONE);
				FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::LeftThumbY, m_context.m_nPortIndex));
			}
			if (map_stick_value(pPacket->rightX, X_GAMEPAD_RIGHT_THUMB_DEADZONE) !=
				map_stick_value(m_context.m_state.rightX, X_GAMEPAD_RIGHT_THUMB_DEADZONE))
			{
				const FXD::F32 fVal = map_stick_value(pPacket->rightX, X_GAMEPAD_RIGHT_THUMB_DEADZONE);
				FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::RightThumbX, m_context.m_nPortIndex));
			}
			if (map_stick_value(pPacket->rightY, X_GAMEPAD_RIGHT_THUMB_DEADZONE) !=
				map_stick_value(m_context.m_state.rightY, X_GAMEPAD_RIGHT_THUMB_DEADZONE))
			{
				const FXD::F32 fVal = map_stick_value(pPacket->rightY, X_GAMEPAD_RIGHT_THUMB_DEADZONE);
				FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::RightThumbY, m_context.m_nPortIndex));
			}
		}
		Memory::MemCopy(&m_context.m_state, pPacket, sizeof(XInputPacketState));
	}
	else
	{
		if ((nSize < 0) && bWasConnected)
		{
			m_ePowerLevel = E_PowerLevel::Unknown;
			Memory::MemZero_T(m_context.m_state);
			//HIDAPI_JoystickDisconnected(device, joystick->instance_id, false);
		}
	}

}

static CONSTEXPR const Widget::WIDGET_DESC kXInputWidgets[] =
{
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::DPadUp), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::DPadUp), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::DPadDown), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::DPadDown), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::DPadLeft), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::DPadLeft), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::DPadRight), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::DPadRight), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftThumbX), Input::E_Widget_type::FullAxis, Input::GetGamePadName(E_GamePadButtons::LeftThumbX), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftThumbY), Input::E_Widget_type::FullAxis, Input::GetGamePadName(E_GamePadButtons::LeftThumbY), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightThumbX), Input::E_Widget_type::FullAxis, Input::GetGamePadName(E_GamePadButtons::RightThumbX), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightThumbY), Input::E_Widget_type::FullAxis, Input::GetGamePadName(E_GamePadButtons::RightThumbY), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftThumb), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::LeftThumb), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightThumb), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::RightThumb), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftBumper), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::LeftBumper), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightBumper), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::RightBumper), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftTrigger), Input::E_Widget_type::HalfAxis, Input::GetGamePadName(E_GamePadButtons::LeftTrigger), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightTrigger), Input::E_Widget_type::HalfAxis, Input::GetGamePadName(E_GamePadButtons::RightTrigger), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::Start), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::Start), Widget::kWidgetFlagStartButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::Back), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::Back), Widget::kWidgetFlagBackButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::AButton), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::AButton), Widget::kWidgetFlagActionButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::BButton), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::BButton), Widget::kWidgetFlagBackButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::XButton), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::XButton), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::YButton), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::YButton), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::Guide), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::Guide), 0),
	Widget::WIDGET_DESC()
};

const Widget::WIDGET_DESC* IGamePadXInputH::_get_widget_defs(void) const
{
	return kXInputWidgets;
}

void IGamePadXInputH::_get_widget_state(FXD::U16 nID, void* pState) const
{
	/*
	bool* pDigitalState = (bool*)pState;
	FXD::F32* pAnalogState = (FXD::F32*)pState;
	switch ((E_GamePadButtons)nID)
	{
		case E_GamePadButtons::DPadUp:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP);
			break;
		}
		case E_GamePadButtons::DPadDown:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN);
			break;
		}
		case E_GamePadButtons::DPadLeft:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT);
			break;
		}
		case E_GamePadButtons::DPadRight:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT);
			break;
		}
		case E_GamePadButtons::LeftThumb:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB);
			break;
		}
		case E_GamePadButtons::RightThumb:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB);
			break;
		}
		case E_GamePadButtons::LeftBumper:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER);
			break;
		}
		case E_GamePadButtons::RightBumper:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER);
			break;
		}
		case E_GamePadButtons::Start:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_START);
			break;
		}
		case E_GamePadButtons::Back:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_BACK);
			break;
		}
		case E_GamePadButtons::AButton:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_A);
			break;
		}
		case E_GamePadButtons::BButton:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_B);
			break;
		}
		case E_GamePadButtons::XButton:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_X);
			break;
		}
		case E_GamePadButtons::YButton:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_Y);
			break;
		}
		case E_GamePadButtons::Guide:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_GUIDE);
			break;
		}
			case E_GamePadButtons::LeftTrigger:
		{
			*pAnalogState = map_trigger_value(m_context.m_state.Gamepad.bLeftTrigger);
			break;
		}
		case E_GamePadButtons::RightTrigger:
		{
			*pAnalogState = map_trigger_value(m_context.m_state.Gamepad.bRightTrigger);
			break;
		}
		case E_GamePadButtons::LeftThumbX:
		{
			*pAnalogState = map_stick_value(m_context.m_state.Gamepad.sThumbLX, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
			break;
		}
		case E_GamePadButtons::LeftThumbY:
		{
			*pAnalogState = map_stick_value(m_context.m_state.Gamepad.sThumbLY, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
			break;
		}
		case E_GamePadButtons::RightThumbX:
		{
			*pAnalogState = map_stick_value(m_context.m_state.Gamepad.sThumbRX, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
			break;
		}
		case E_GamePadButtons::RightThumbY:
		{
			*pAnalogState = map_stick_value(m_context.m_state.Gamepad.sThumbRY, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
			break;
		}
}
	*/
}
#endif //IsInputXInputH()