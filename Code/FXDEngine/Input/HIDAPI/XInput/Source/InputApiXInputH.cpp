// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputXInputH()
#include "FXDEngine/Input/HIDAPI/XInput/InputApiXInput.h"
#include "FXDEngine/Input/HIDAPI/XInput/GamePadXInput.h"
#include "FXDEngine/Core/Debugger.h"

using namespace FXD;
using namespace Input;
using namespace HidApi;

// ------
// IInputApiXInputH
// - 
// ------
IInputApiXInputH::IInputApiXInputH(void)
{
}

IInputApiXInputH::~IInputApiXInputH(void)
{
}

bool IInputApiXInputH::create_api(void)
{
	bool bRetVal = true;

	return bRetVal;
}

bool IInputApiXInputH::release_api(void)
{
	bool bRetVal = true;

	return bRetVal;
}

void IInputApiXInputH::_process(FXD::F32 dt)
{
}
#endif //IsInputXInputH()