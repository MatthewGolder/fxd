// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_HIDAPI_XINPUT_INPUTAPIXINPUT_H
#define FXDENGINE_INPUT_HIDAPI_XINPUT_INPUTAPIXINPUT_H

#include "FXDEngine/Input/Types.h"

#if IsInputXInputH()
#include "FXDEngine/Container/Vector.h"
#include "FXDEngine/Input/InputApi.h"
#include "FXDEngine/Input/HIDAPI/XInput/XInput.h"

namespace FXD
{
	namespace Input
	{
		namespace HidApi
		{
			// ------
			// IInputApiXInput
			// -
			// ------
			class IInputApiXInputH FINAL : public Input::IInputApi
			{
			public:
				IInputApiXInputH(void);
				~IInputApiXInputH(void);

				bool create_api(void) FINAL;
				bool release_api(void) FINAL;

			private:
				void _process(FXD::F32 dt) FINAL;

			private:
				Container::Vector< Input::InputDevice > m_detectedDevices;
			};

		} //namespace HidApi
	} //namespace Input
} //namespace FXD

#endif //IsInputXInputH()
#endif //FXDENGINE_INPUT_HIDAPI_XINPUT_INPUTAPIXINPUT_H