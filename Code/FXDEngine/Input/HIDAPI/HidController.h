// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_HIDAPI_HIDCONTROLLER_H
#define FXDENGINE_INPUT_HIDAPI_HIDCONTROLLER_H

#include "FXDEngine/Input/Types.h"

#if IsInputHIDAPI()
#include "FXDEngine/Core/String.h"
#include "FXDEngine/Input/HIDAPI/HidUSBVendors.h"

namespace FXD
{
	namespace Input
	{
		namespace HidApi
		{
			struct HidGuid
			{
				void set_guid_info(HidApi::E_USB_Vendor eVendorId, FXD::U16 nProduct, FXD::U16 nVersion);
				void get_guid_info(HidApi::E_USB_Vendor* pVendor, FXD::U16* pProduct, FXD::U16* pVersion)const;

				FXD::U8 data[16];
			};

			enum class E_HidControllerType : FXD::U8
			{
				XBox360,
				XBoxOne,
				PS3,
				PS4,
				Steam,
				SwitchPro,
				SwitchJoycon,
				Count,
				Unknown = 0xff
			};

			enum class E_HidControllerHelper : FXD::U8
			{
				// Steam Controllers
				SteamUnknown = 0,
				SteamV1 = 1,
				SteamV2 = 2,
				// Other Controllers
				XBox360 = 31,
				XBoxOne = 32,
				PS3 = 33,
				PS4 = 34,
				//Apple = 35,
				//Android = 36,
				SwitchJoyConLeft = 37,
				SwitchJoyConRight = 38,
				SwitchPro = 39,
				SwitchProInput = 40,
				Count,
				Unknown = 0xff
			};

#			define MAKE_CONTROLLER_ID(nVID, nPID) (FXD::U32)((FXD::U32)nVID << 16 | (FXD::U32)nPID)
			struct ControllerDesc
			{
				const FXD::U32 DeviceID;
				const E_HidControllerHelper ControllerType;
				const Core::StringView16 Name;
			};

			static CONSTEXPR const ControllerDesc kControllerDescs[] =
			{
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Atmel, 0xff01), E_HidControllerHelper::XBox360, Core::StringView16() },											// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::BigBenInteractive, 0x0601), E_HidControllerHelper::XBox360, Core::StringView16() },								// BigBen Interactive XBOX 360 Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::BigBenInteractive, 0x0602), E_HidControllerHelper::XBox360, Core::StringView16() },								// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Elecom, 0x2004), E_HidControllerHelper::XBox360, Core::StringView16() },											// Elecom JC-U3613M
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Elecom, 0x2012), E_HidControllerHelper::XBox360, Core::StringView16() },											// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::HoneyBee, 0x0004), E_HidControllerHelper::XBox360, Core::StringView16() },										// Honey Bee Xbox360 dancepad
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::HoneyBee, 0x0301), E_HidControllerHelper::XBox360, Core::StringView16() },										// PDP AFTERGLOW AX.1
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::HoneyBee, 0x0303), E_HidControllerHelper::XBox360, Core::StringView16() },										// Mortal Kombat Klassic FightStick
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x000a), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 Co. DOA4 FightStick") },							// Hori Co. DOA4 FightStick
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x000c), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 Hori PadEX Turbo") },							// Hori PadEX Turbo
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x000d), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 Hori Fighting Stick EX2") },						// Hori Fighting Stick EX2
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x0016), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 Hori Real Arcade Pro.EX") },						// Hori Real Arcade Pro.EX
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x001b), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 Hori Real Arcade Pro VX") },						// Hori Real Arcade Pro VX
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x008c), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 Hori Real Arcade Pro 4") },						// Hori Real Arcade Pro 4
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x00db), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 HORI Slime Controller") },						// Hori Dragon Quest Slime Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x00dc), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 HORI Battle Pad") },								// HORI Battle Pad. Is a Switch controller but shows up through XInput on Windows.
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x006d), E_HidControllerHelper::XBox360, Core::StringView16() },											// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x00a4), E_HidControllerHelper::XBox360, Core::StringView16() },											// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x00b1), E_HidControllerHelper::XBox360, Core::StringView16() },											// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x00ae), E_HidControllerHelper::XBox360, Core::StringView16() },											// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Joytech, 0xbeef), E_HidControllerHelper::XBox360, Core::StringView16() },										// Joytech Neo-Se Take2
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Logitech, 0xc21d), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 Logitech Gamepad F310") },					// Logitech Gamepad F310
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Logitech, 0xc21e), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 Logitech Gamepad F510") },					// Logitech Gamepad F510
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Logitech, 0xc21f), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 Logitech Gamepad F710") },					// Logitech Gamepad F710
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Logitech, 0xc242), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 Logitech Chillstream") },					// Logitech Chillstream Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Logitech, 0x0401), E_HidControllerHelper::XBox360, Core::StringView16() },										// logitech xinput
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Logitech, 0x0301), E_HidControllerHelper::XBox360, Core::StringView16() },										// logitech xinput
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Logitech, 0xcaa3), E_HidControllerHelper::XBox360, Core::StringView16() },										// logitech xinput
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Logitech, 0xc261), E_HidControllerHelper::XBox360, Core::StringView16() },										// logitech xinput
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Logitech, 0x0291), E_HidControllerHelper::XBox360, Core::StringView16() },										// logitech xinput
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Logitech, 0x1000), E_HidControllerHelper::XBox360, Core::StringView16() },										// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::LogitechEurope, 0xf51a), E_HidControllerHelper::XBox360, Core::StringView16() },									// Saitek P3600
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0x4716), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 MadCatz Wired Controller") },					// Mad Catz Wired Xbox 360 Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0x4726), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 MadCatz Controller") },						// Mad Catz Xbox 360 Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0x4718), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 MadCatz SFIV FightStick SE") },				// Mad Catz SFIV FightStick SE
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0x4728), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 MadCatz SFIV FightPad") },					// Mad Catz SFIV FightPad
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0x4738), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 MadCatz SFIV Wired Controller") },			// Mad Catz SFIV Wired Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0x4736), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 MadCatz MicroCon Gamepad") },					// Mad Catz MicroCon Gamepad
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0x4740), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 MadCatz Beat Pad") },							// Mad Catz Beat Pad
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0xb726), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 MadCatz MW2 Controller") },					// Mad Catz MW2 MW2 Controller 
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0xbeef), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 MadCatz JOYTECH NEO SE Advanced GamePad") },	// Mad Catz JOYTECH NEO SE Advanced GamePad
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0xcb02), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 MadCatz Saitek Cyborg Rumble Pad") },			// Mad Catz Saitek Cyborg Rumble Pad - PC/Xbox 360
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0xcb03), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 MadCatz Catz Saitek P3200 Rumble Pad") },		// Mad Catz Saitek P3200 Rumble Pad - PC/Xbox 360
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0xf738), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 MadCatz Super SFIV FightStick TE S") },		// Mad Catz Super SFIV FightStick TE S
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Microsoft, 0x028e), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 Controller") },								// Microsoft X-Box 360 pad
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Microsoft, 0x028f), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 Controller") },								// Microsoft X-Box 360 pad v2
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Microsoft, 0x0291), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 Wireless Controller") },					// Xbox 360 Wireless Receiver (XBOX)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Microsoft, 0x02a0), E_HidControllerHelper::XBox360, Core::StringView16() },										// Microsoft X-Box 360 Big Button IR
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Microsoft, 0x02a1), E_HidControllerHelper::XBox360, Core::StringView16() },										// Microsoft X-Box 360 pad
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Microsoft, 0x02a9), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 Wireless Controller") },					// Xbox 360 Wireless Receiver (third party knockoff)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Microsoft, 0x0719), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 Wireless Controller") },					// Xbox 360 Wireless Receiver
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Microsoft, 0x02a2), E_HidControllerHelper::XBox360, Core::StringView16() },										// Unknown Controller - Microsoft VID
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Nacon, 0x55f0), E_HidControllerHelper::XBox360, Core::StringView16() },											// Nacon GC-100XF
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Numark, 0x3f00), E_HidControllerHelper::XBox360, Core::StringView16() },											// Power A Mini Pro Elite
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Numark, 0x3f0a), E_HidControllerHelper::XBox360, Core::StringView16() },											// Xbox Airflo wired controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Numark, 0x3f10), E_HidControllerHelper::XBox360, Core::StringView16() },											// Batarang Xbox 360 controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Nvidia, 0x7210), E_HidControllerHelper::XBox360, Core::StringView16() },											// Nvidia Shield local controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Nvidia, 0xb400), E_HidControllerHelper::XBox360, Core::StringView16() },											// NVIDIA Shield streaming controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0105), E_HidControllerHelper::XBox360, Core::StringView16() },											// HSM3 Xbox360 dancepad
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0113), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 PDP Afterglow") },								// PDP Afterglow Gamepad for Xbox 360
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x011f), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 PDP Rock Candy") },								// PDP Rock Candy Gamepad for Xbox 360
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0125), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 PDP INJUSTICE FightStick") },						// PDP INJUSTICE FightStick for Xbox 360
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0127), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 PDP INJUSTICE FightPad") },						// PDP INJUSTICE FightPad for Xbox 360
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0131), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 PDP EA Soccer Controller") },						// PDP EA Soccer Gamepad
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0133), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 PDP Battlefield 4 Controller") },					// PDP Battlefield 4 Gamepad
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0143), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 PDP MK X Fight Stick") },							// PDP MK X Fight Stick for Xbox 360
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0147), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 PDP Xbox 360 Marvel Controller") },				// PDP Marvel Controller for Xbox 360
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0201), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 PDP Xbox 360 Controller") },						// PDP Gamepad for Xbox 360
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0213), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 PDP Xbox 360 Afterglow") },						// PDP Afterglow Gamepad for Xbox 360
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x021f), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 PDP Xbox 360 Rock Candy") },						// PDP Rock Candy Gamepad for Xbox 360
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0301), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 PDP Xbox 360 Controller") },						// PDP Gamepad for Xbox 360
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0313), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 PDP Xbox 360 Afterglow") },						// PDP Afterglow Gamepad for Xbox 360
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0314), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 PDP Xbox 360 Afterglow") },						// PDP Afterglow Gamepad for Xbox 360
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0401), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 PDP Xbox 360 Controller") },						// PDP Gamepad for Xbox 360
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0413), E_HidControllerHelper::XBox360, Core::StringView16() },											// PDP Afterglow AX.1 (unlisted)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0501), E_HidControllerHelper::XBox360, Core::StringView16() },											// PDP Xbox 360 Controller (unlisted)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0xf900), E_HidControllerHelper::XBox360, Core::StringView16() },											// PDP Afterglow AX.1 (unlisted)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x1414), E_HidControllerHelper::XBox360, Core::StringView16() },											// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0159), E_HidControllerHelper::XBox360, Core::StringView16() },											// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RazerOnza, 0xfd00), E_HidControllerHelper::XBox360, Core::StringView16() },									// Razer Onza Tournament Edition
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RazerOnza, 0xfd01), E_HidControllerHelper::XBox360, Core::StringView16() },									// Razer Onza Classic Edition
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RazerOnza, 0xfe00), E_HidControllerHelper::XBox360, Core::StringView16() },									// Razer Sabertooth
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RedOctane, 0x02a0), E_HidControllerHelper::XBox360, Core::StringView16() },									// RedOctane Controller Adapter
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RedOctane, 0x4748), E_HidControllerHelper::XBox360, Core::StringView16() },									// RedOctane Guitar Hero X-plorer
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RedOctane, 0xf801), E_HidControllerHelper::XBox360, Core::StringView16() },									// RedOctane Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ShenzhenLongshengweiTechnology, 0x18d4), E_HidControllerHelper::XBox360, Core::StringView16() },				// GPD Win 2 X-Box Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ShenzhenLongshengweiTechnology, 0x1832), E_HidControllerHelper::XBox360, Core::StringView16() },				// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ShenzhenLongshengweiTechnology, 0x187f), E_HidControllerHelper::XBox360, Core::StringView16() },				// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ShenzhenLongshengweiTechnology, 0x1883), E_HidControllerHelper::XBox360, Core::StringView16() },				// Unknown Controller	
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ShenzhenLongshengweiTechnology, 0x18d3), E_HidControllerHelper::XBox360, Core::StringView16() },				// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ShenzhenLongshengweiTechnology, 0x188e), E_HidControllerHelper::XBox360, Core::StringView16() },				// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ShenzhenLongshengweiTechnology, 0x187c), E_HidControllerHelper::XBox360, Core::StringView16() },				// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ShenzhenLongshengweiTechnology, 0x189c), E_HidControllerHelper::XBox360, Core::StringView16() },				// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ShenzhenLongshengweiTechnology, 0x1874), E_HidControllerHelper::XBox360, Core::StringView16() },				// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::SinoLite, 0x6006), E_HidControllerHelper::XBox360, Core::StringView16() },										// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::SteelSeries, 0x1430), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 SteelSeries Stratus Duo") },			// SteelSeries Stratus Duo
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::SteelSeries, 0x1431), E_HidControllerHelper::XBox360, UTF_16("Xbox 360 SteelSeries Stratus Duo") },			// SteelSeries Stratus Duo
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::SteelSeries, 0xb360), E_HidControllerHelper::XBox360, Core::StringView16() },									// SteelSeries Nimbus/Stratus XL
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMaster, 0xb326), E_HidControllerHelper::XBox360, Core::StringView16() },									// Thrustmaster Gamepad GP XID
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMaster, 0xfaff), E_HidControllerHelper::XBox360, Core::StringView16() },									// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x5000), E_HidControllerHelper::XBox360, Core::StringView16() },								// Razer Atrox Arcade Stick
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x5300), E_HidControllerHelper::XBox360, Core::StringView16() },								// PowerA MINI PROEX Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x5303), E_HidControllerHelper::XBox360, Core::StringView16() },								// Xbox Airflo wired controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x530a), E_HidControllerHelper::XBox360, Core::StringView16() },								// Xbox 360 Pro EX Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x531a), E_HidControllerHelper::XBox360, Core::StringView16() },								// PowerA Pro Ex
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x5397), E_HidControllerHelper::XBox360, Core::StringView16() },								// FUS1ON Tournament Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x5500), E_HidControllerHelper::XBox360, Core::StringView16() },								// Hori XBOX 360 EX 2 with Turbo
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x5501), E_HidControllerHelper::XBox360, Core::StringView16() },								// Hori Real Arcade Pro VX-SA
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x5502), E_HidControllerHelper::XBox360, Core::StringView16() },								// Hori Fighting Stick VX Alt
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x5503), E_HidControllerHelper::XBox360, Core::StringView16() },								// Hori Fighting Edge
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x5506), E_HidControllerHelper::XBox360, Core::StringView16() },								// Hori SOULCALIBUR V Stick
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x550d), E_HidControllerHelper::XBox360, Core::StringView16() },								// Hori GEM Xbox controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x550e), E_HidControllerHelper::XBox360, Core::StringView16() },								// Hori Real Arcade Pro V Kai 360
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x5508), E_HidControllerHelper::XBox360, Core::StringView16() },								// Hori PAD A
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x5510), E_HidControllerHelper::XBox360, Core::StringView16() },								// Hori Fighting Commander ONE
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x5b00), E_HidControllerHelper::XBox360, Core::StringView16() },								// ThrustMaster Ferrari Italia 458 Racing Wheel
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x5b02), E_HidControllerHelper::XBox360, Core::StringView16() },								// Thrustmaster, Inc. GPX Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x5b03), E_HidControllerHelper::XBox360, Core::StringView16() },								// Thrustmaster Ferrari 458 Racing Wheel
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x5d04), E_HidControllerHelper::XBox360, Core::StringView16() },								// Razer Sabertooth
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0xfafa), E_HidControllerHelper::XBox360, Core::StringView16() },								// Aplay Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0xfafb), E_HidControllerHelper::XBox360, Core::StringView16() },								// Aplay Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0xfafc), E_HidControllerHelper::XBox360, Core::StringView16() },								// Afterglow Gamepad 1
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0xfafd), E_HidControllerHelper::XBox360, Core::StringView16() },								// Afterglow Gamepad 3
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0xfafe), E_HidControllerHelper::XBox360, Core::StringView16() },								// Rock Candy Gamepad for Xbox 360
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Zeroplus, 0x0ef8), E_HidControllerHelper::XBox360, Core::StringView16() },										// Homemade fightstick based on brook pcb (with XInput driver??)
				{ MAKE_CONTROLLER_ID(0x1bad, 0x0002), E_HidControllerHelper::XBox360, Core::StringView16() },													// Harmonix Rock Band Guitar
				{ MAKE_CONTROLLER_ID(0x1bad, 0x0003), E_HidControllerHelper::XBox360, Core::StringView16() },													// Harmonix Rock Band Drumkit
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf016), E_HidControllerHelper::XBox360, Core::StringView16() },													// Mad Catz Xbox 360 Controller
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf018), E_HidControllerHelper::XBox360, Core::StringView16() },													// Mad Catz Street Fighter IV SE Fighting Stick
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf019), E_HidControllerHelper::XBox360, Core::StringView16() },													// Mad Catz Brawlstick for Xbox 360
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf021), E_HidControllerHelper::XBox360, Core::StringView16() },													// Mad Cats Ghost Recon FS GamePad
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf023), E_HidControllerHelper::XBox360, Core::StringView16() },													// MLG Pro Circuit Controller (Xbox)
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf025), E_HidControllerHelper::XBox360, Core::StringView16() },													// Mad Catz Call Of Duty
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf027), E_HidControllerHelper::XBox360, Core::StringView16() },													// Mad Catz FPS Pro
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf028), E_HidControllerHelper::XBox360, Core::StringView16() },													// Street Fighter IV FightPad
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf02e), E_HidControllerHelper::XBox360, Core::StringView16() },													// Mad Catz Fightpad
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf036), E_HidControllerHelper::XBox360, Core::StringView16() },													// Mad Catz MicroCon GamePad Pro
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf038), E_HidControllerHelper::XBox360, Core::StringView16() },													// Street Fighter IV FightStick TE
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf039), E_HidControllerHelper::XBox360, Core::StringView16() },													// Mad Catz MvC2 TE
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf03a), E_HidControllerHelper::XBox360, Core::StringView16() },													// Mad Catz SFxT Fightstick Pro
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf03d), E_HidControllerHelper::XBox360, Core::StringView16() },													// Street Fighter IV Arcade Stick TE - Chun Li
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf03e), E_HidControllerHelper::XBox360, Core::StringView16() },													// Mad Catz MLG FightStick TE
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf03f), E_HidControllerHelper::XBox360, Core::StringView16() },													// Mad Catz FightStick SoulCaliber
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf042), E_HidControllerHelper::XBox360, Core::StringView16() },													// Mad Catz FightStick TES+
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf080), E_HidControllerHelper::XBox360, Core::StringView16() },													// Mad Catz FightStick TE2
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf501), E_HidControllerHelper::XBox360, Core::StringView16() },													// HoriPad EX2 Turbo
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf502), E_HidControllerHelper::XBox360, Core::StringView16() },													// Hori Real Arcade Pro.VX SA
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf503), E_HidControllerHelper::XBox360, Core::StringView16() },													// Hori Fighting Stick VX
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf504), E_HidControllerHelper::XBox360, Core::StringView16() },													// Hori Real Arcade Pro. EX
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf505), E_HidControllerHelper::XBox360, Core::StringView16() },													// Hori Fighting Stick EX2B
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf506), E_HidControllerHelper::XBox360, Core::StringView16() },													// Hori Real Arcade Pro.EX Premium VLX
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf900), E_HidControllerHelper::XBox360, Core::StringView16() },													// Harmonix Xbox 360 Controller
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf901), E_HidControllerHelper::XBox360, Core::StringView16() },													// Gamestop Xbox 360 Controller
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf902), E_HidControllerHelper::XBox360, Core::StringView16() },													// Mad Catz Gamepad2
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf903), E_HidControllerHelper::XBox360, Core::StringView16() },													// Tron Xbox 360 controller
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf904), E_HidControllerHelper::XBox360, Core::StringView16() },													// PDP Versus Fighting Pad
				{ MAKE_CONTROLLER_ID(0x1bad, 0xf906), E_HidControllerHelper::XBox360, Core::StringView16() },													// MortalKombat FightStick
				{ MAKE_CONTROLLER_ID(0x1bad, 0xfa01), E_HidControllerHelper::XBox360, Core::StringView16() },													// MadCatz GamePad
				{ MAKE_CONTROLLER_ID(0x1bad, 0xfd00), E_HidControllerHelper::XBox360, Core::StringView16() },													// Razer Onza TE
				{ MAKE_CONTROLLER_ID(0x1bad, 0xfd01), E_HidControllerHelper::XBox360, Core::StringView16() },													// Razer Onza
				{ MAKE_CONTROLLER_ID(0x0000, 0x0000), E_HidControllerHelper::XBox360, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x2c22, 0x2303), E_HidControllerHelper::XBox360, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x0001, 0x0001), E_HidControllerHelper::XBox360, Core::StringView16() },													// Unknown Controller

				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::BigBenInteractive, 0x0611), E_HidControllerHelper::XBoxOne, Core::StringView16() },							// Xbox Controller Mode for NACON Revolution 3
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::BigBenInteractive, 0x604), E_HidControllerHelper::XBoxOne, Core::StringView16() },							// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::BigBenInteractive, 0x605), E_HidControllerHelper::XBoxOne, Core::StringView16() },							// NACON PS4 controller in Xbox mode - might also be other bigben brand xbox controllers
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::BigBenInteractive, 0x606), E_HidControllerHelper::XBoxOne, Core::StringView16() },							// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::BigBenInteractive, 0x609), E_HidControllerHelper::XBoxOne, Core::StringView16() },							// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::HoneyBee, 0x304), E_HidControllerHelper::XBoxOne, Core::StringView16() },										// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x0063), E_HidControllerHelper::XBoxOne, Core::StringView16() },										// Hori Real Arcade Pro Hayabusa (USA) Xbox One
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x0067), E_HidControllerHelper::XBoxOne, Core::StringView16() },										// HORIPAD ONE
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x0078), E_HidControllerHelper::XBoxOne, Core::StringView16() },										// Hori Real Arcade Pro V Kai Xbox One
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x00c5), E_HidControllerHelper::XBoxOne, Core::StringView16() },										// HORI Fighting Commander
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hyperkin, 0x0652), E_HidControllerHelper::XBoxOne, Core::StringView16() },									// Hyperkin Duke
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hyperkin, 0x1618), E_HidControllerHelper::XBoxOne, Core::StringView16() },									// Hyperkin Duke
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hyperkin, 0x1688), E_HidControllerHelper::XBoxOne, Core::StringView16() },									// Hyperkin X91
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Microsoft, 0x02d1), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One Controller") },							// Microsoft X-Box One pad
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Microsoft, 0x02dd), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One Controller") },							// Microsoft X-Box One pad (Firmware 2015)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Microsoft, 0x02e0), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One S Controller") },						// Microsoft X-Box One S pad (Bluetooth)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Microsoft, 0x02e3), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One Elite Controller") },					// Microsoft X-Box One Elite pad
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Microsoft, 0x02ea), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One S Controller") },						// Microsoft X-Box One S pad
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Microsoft, 0x02fd), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One S Controller") },						// Microsoft X-Box One S pad (Bluetooth)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Microsoft, 0x02ff), E_HidControllerHelper::XBoxOne, Core::StringView16() },									// Microsoft X-Box One controller with the RAWINPUT driver on Windows
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Microsoft, 0x0b00), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One Elite 2 Controller") },					// Microsoft X-Box One Elite Series 2 pad
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Microsoft, 0x0b05), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One Elite 2 Controller") },					// Microsoft X-Box One Elite Series 2 pad (Bluetooth)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0x4a01), E_HidControllerHelper::XBoxOne, Core::StringView16() },										// Mad Catz FightStick TE 2
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0139), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Afterglow") },								// PDP Afterglow Wired Controller for Xbox One
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x013B), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Face-Off Controller") },					// PDP Face-Off Gamepad for Xbox One
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x013a), E_HidControllerHelper::XBoxOne, Core::StringView16() },											// PDP Xbox One Controller (unlisted)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0145), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP MK X Fight Pad") },						// PDP MK X Fight Pad for Xbox One
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0146), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Rock Candy") },							// PDP Rock Candy Wired Controller for Xbox One
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x015b), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Fallout 4 Vault Boy Controller") },		// PDP Fallout 4 Vault Boy Wired Controller for Xbox One
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x015c), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP @Play Controller") },						// PDP @Play Wired Controller for Xbox One 
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x015d), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Mirror's Edge Controller") },				// PDP Mirror's Edge Wired Controller for Xbox One
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x015f), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Metallic Controller") },					// PDP Metallic Wired Controller for Xbox One
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0160), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP NFL Face-Off Controller") },				// PDP NFL Official Face-Off Wired Controller for Xbox One
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0161), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Camo") },									// PDP Camo Wired Controller for Xbox One
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0162), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Controller") },							// PDP Wired Controller for Xbox One
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0163), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Deliverer of Truth") },					// PDP Legendary Collection: Deliverer of Truth
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0164), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Battlefield 1 Controller") },				// PDP Battlefield 1 Official Wired Controller for Xbox One
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0165), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Titanfall 2 Controller") },				// PDP Titanfall 2 Official Wired Controller for Xbox One
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0166), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Mass Effect: Andromeda Controller") },		// PDP Mass Effect: Andromeda Official Wired Controller for Xbox One
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0167), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Halo Wars 2 Face-Off Controller") },		// PDP Halo Wars 2 Official Face-Off Wired Controller for Xbox One
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0205), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Victrix Pro Fight Stick") },				// PDP Victrix Pro Fight Stick
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0206), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Mortal Kombat Controller") },				// PDP Mortal Kombat 25 Anniversary Edition Stick (Xbox One)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0246), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Rock Candy") },							// PDP Rock Candy Wired Controller for Xbox One
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0261), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Camo") },									// PDP Camo Wired Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0262), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Controller") },							// PDP Wired Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02a0), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Midnight Blue") },							// PDP Wired Controller for Xbox One - Midnight Blue
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02a1), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Verdant Green") },							// PDP Wired Controller for Xbox One - Verdant Green
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02a2), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Crimson Red") },							// PDP Wired Controller for Xbox One - Crimson Red
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02a3), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Arctic White") },							// PDP Wired Controller for Xbox One - Arctic White
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02a4), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Phantom Black") },							// PDP Wired Controller for Xbox One - Stealth Series | Phantom Black
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02a5), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Ghost White") },							// PDP Wired Controller for Xbox One - Stealth Series | Ghost White
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02a6), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Revenant Blue") },							// PDP Wired Controller for Xbox One - Stealth Series | Revenant Blue
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02a7), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Raven Black") },							// PDP Wired Controller for Xbox One - Raven Black
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02a8), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Arctic White") },							// PDP Wired Controller for Xbox One - Arctic White
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02a9), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Midnight Blue") },							// PDP Wired Controller for Xbox One - Midnight Blue
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02aa), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Verdant Green") },							// PDP Wired Controller for Xbox One - Verdant Green
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02ab), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Crimson Red") },							// PDP Wired Controller for Xbox One - Crimson Red
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02ac), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Ember Orange") },							// PDP Wired Controller for Xbox One - Ember Orange
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02ad), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Phantom Black") },							// PDP Wired Controller for Xbox One - Stealth Series | Phantom Black
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02ae), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Ghost White") },							// PDP Wired Controller for Xbox One - Stealth Series | Ghost White
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02af), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Revenant Blue") },							// PDP Wired Controller for Xbox One - Stealth Series | Revenant Blue
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02b0), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Raven Black") },							// PDP Wired Controller for Xbox One - Raven Black
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02b1), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Arctic White") },							// PDP Wired Controller for Xbox One - Arctic White
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02b3), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Afterglow") },								// PDP Afterglow Prismatic Wired Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02b5), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP GAMEware Controller") },					// PDP GAMEware Wired Controller Xbox One
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02b6), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP 1-Handed Joystick Adaptive Controller") },	// PDP 1-Handed Joystick Adaptive Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02bd), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Royal Purple") },							// PDP Wired Controller for Xbox One - Royal Purple
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02be), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Raven Black") },							// PDP Deluxe Wired Controller for Xbox One - Raven Black
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02bf), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Midnight Blue") },							// PDP Deluxe Wired Controller for Xbox One - Midnight Blue
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02c0), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Phantom Black") },							// PDP Deluxe Wired Controller for Xbox One - Stealth Series | Phantom Black
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02c1), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Ghost White") },							// PDP Deluxe Wired Controller for Xbox One - Stealth Series | Ghost White
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02c2), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Revenant Blue") },							// PDP Deluxe Wired Controller for Xbox One - Stealth Series | Revenant Blue
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02c3), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Verdant Green") },							// PDP Deluxe Wired Controller for Xbox One - Verdant Green
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02c4), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Ember Orange") },							// PDP Deluxe Wired Controller for Xbox One - Ember Orange
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02c5), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Royal Purple") },							// PDP Deluxe Wired Controller for Xbox One - Royal Purple
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02c6), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Crimson Red") },							// PDP Deluxe Wired Controller for Xbox One - Crimson Red
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02c7), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Arctic White") },							// PDP Deluxe Wired Controller for Xbox One - Arctic White
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02c8), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Kingdom Hearts Controller") },				// PDP Kingdom Hearts Wired Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02c9), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Phantasm Red") },							// PDP Deluxe Wired Controller for Xbox One - Stealth Series | Phantasm Red
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02ca), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Specter Violet") },						// PDP Deluxe Wired Controller for Xbox One - Stealth Series | Specter Violet
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02cb), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Specter Violet") },						// PDP Wired Controller for Xbox One - Stealth Series | Specter Violet
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02cd), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Blu-merang") },							// PDP Rock Candy Wired Controller for Xbox One - Blu-merang
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02ce), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Cranblast") },								// PDP Rock Candy Wired Controller for Xbox One - Cranblast
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02cf), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Aqualime") },								// PDP Rock Candy Wired Controller for Xbox One - Aqualime
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x02d5), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP Red Camo") },								// PDP Wired Controller for Xbox One - Red Camo
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0346), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP RC Gamepad") },							// PDP RC Gamepad for Xbox One
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0446), E_HidControllerHelper::XBoxOne, UTF_16("Xbox One PDP RC Gamepad") },							// PDP RC Gamepad for Xbox One
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RazerSabertooth, 0x0a00), E_HidControllerHelper::XBoxOne, Core::StringView16() },								// Razer Atrox Arcade Stick
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RazerSabertooth, 0x0a03), E_HidControllerHelper::XBoxOne, Core::StringView16() },								// Razer Wildcat
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RazerSabertooth, 0x0a14), E_HidControllerHelper::XBoxOne, Core::StringView16() },								// Razer Wolverine Ultimate
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RedOctane, 0x719), E_HidControllerHelper::XBoxOne, Core::StringView16() },									// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RedOctane, 0x291), E_HidControllerHelper::XBoxOne, Core::StringView16() },									// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RedOctane, 0x2a9), E_HidControllerHelper::XBoxOne, Core::StringView16() },									// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RedOctane, 0x70b), E_HidControllerHelper::XBoxOne, Core::StringView16() },									// Unknown Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x541a), E_HidControllerHelper::XBoxOne, Core::StringView16() },								// PowerA Xbox One Mini Wired Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x542a), E_HidControllerHelper::XBoxOne, Core::StringView16() },								// Xbox ONE spectra
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x543a), E_HidControllerHelper::XBoxOne, UTF_16("PowerA XBox One Controller") },				// PowerA Xbox ONE liquid metal controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x551a), E_HidControllerHelper::XBoxOne, Core::StringView16() },								// PowerA FUSION Pro Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x561a), E_HidControllerHelper::XBoxOne, Core::StringView16() },								// PowerA FUSION Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x581a), E_HidControllerHelper::XBoxOne, Core::StringView16() },								// BDA XB1 Classic Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x591a), E_HidControllerHelper::XBoxOne, Core::StringView16() },								// PowerA FUSION Pro Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x592a), E_HidControllerHelper::XBoxOne, Core::StringView16() },								// BDA XB1 Spectra Pro
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc, 0x791a), E_HidControllerHelper::XBoxOne, Core::StringView16() },								// PowerA Fusion Fight Pad
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMasterInc,	0x5509), E_HidControllerHelper::XBoxOne, Core::StringView16() },								// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x2f24, 0x0050), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x2f24, 0x2e), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x9886, 0x24), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x2f24, 0x91), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xf0d, 0xed), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x3eb, 0xff02), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xf0d, 0xc0), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xe6f, 0x152), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xe6f, 0x2a7), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x46d, 0x1007), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xe6f, 0x2b8), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xe6f, 0x2a8), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x2c22, 0x2503), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x79, 0x18a1), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x0, 0x6686), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x11ff, 0x511), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x1bad, 0x28e), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x1bad, 0x2a0), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x1bad, 0x5500), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x20ab, 0x55ef), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x2516, 0x69), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x25b1, 0x360), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x2c22, 0x2203), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x2f24, 0x11), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x2f24, 0x53), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x2f24, 0xb7), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x46d, 0x0), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x46d, 0x1004), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x46d, 0x1008), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x46d, 0xf301), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x738, 0x2a0), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x738, 0x7263), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x738, 0xb738), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x738, 0xcb29), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x738, 0xf401), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x79, 0x18c2), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x79, 0x18c8), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0x79, 0x18cf), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xc12, 0xe17), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xc12, 0xe1c), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xc12, 0xe22), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xc12, 0xe30), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xd2d2, 0xd2d2), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xd62, 0x9a1a), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xd62, 0x9a1b), E_HidControllerHelper::XBoxOne, Core::StringView16() },													// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xe00, 0xe00), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xe6f, 0x12a), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xe6f, 0x2a1), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xe6f, 0x2a2), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xe6f, 0x2a5), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xe6f, 0x2b2), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xe6f, 0x2bd), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xe6f, 0x2bf), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xe6f, 0x2c0), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xe6f, 0x2c6), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xf0d, 0x97), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xf0d, 0xba), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xf0d, 0xd8), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller
				{ MAKE_CONTROLLER_ID(0xfff, 0x2a1), E_HidControllerHelper::XBoxOne, Core::StringView16() },														// Unknown Controller

				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::BigBenInteractive, 0x0603), E_HidControllerHelper::PS3, Core::StringView16() },								// From SDL
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::BigBenInteractive, 0x5500), E_HidControllerHelper::PS3, Core::StringView16() },								// From SDL
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Elecom, 0x200f), E_HidControllerHelper::PS3, Core::StringView16() },											// From SDL
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Elecom, 0x2013), E_HidControllerHelper::PS3, Core::StringView16() },											// JC-U4113SBK
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x0009), E_HidControllerHelper::PS3, Core::StringView16() },											// HORI BDA GP1
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x004d), E_HidControllerHelper::PS3, Core::StringView16() },											// Horipad 3
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x005e), E_HidControllerHelper::PS3, Core::StringView16() },											// HORI Fighting commander ps4
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x005f), E_HidControllerHelper::PS3, Core::StringView16() },											// HORI Fighting commander ps3
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x006a), E_HidControllerHelper::PS3, Core::StringView16() },											// Real Arcade Pro 4
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x006e), E_HidControllerHelper::PS3, Core::StringView16() },											// HORI horipad4 ps3
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x0085), E_HidControllerHelper::PS3, Core::StringView16() },											// HORI Fighting Commander PS3
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x0086), E_HidControllerHelper::PS3, Core::StringView16() },											// HORI Fighting Commander PC (Uses the Xbox 360 protocol, but has PS3 buttons)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x0087), E_HidControllerHelper::PS3, Core::StringView16() },											// HORI fighting mini stick
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::JessTechnology, 0x1100), E_HidControllerHelper::PS3, Core::StringView16() },									// Quanba Q1 fight stick
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::LakeviewResearch, 0x0005), E_HidControllerHelper::PS3, Core::StringView16() },								// Sony PS3 Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::LakeviewResearch, 0x8866), E_HidControllerHelper::PS3, Core::StringView16() },								// PS2 maybe break out later
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::LakeviewResearch, 0x8888), E_HidControllerHelper::PS3, Core::StringView16() },								// Actually ps2 -maybe break out later Lakeview Research WiseGroup Ltd, MP-8866 Dual Joypad
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::LogitechEurope, 0xf622), E_HidControllerHelper::PS3, Core::StringView16() },									// Cyborg V3
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0x3180), E_HidControllerHelper::PS3, Core::StringView16() },											// Mad Catz Alpha PS3 mode
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0x3250), E_HidControllerHelper::PS3, Core::StringView16() },											// Mad Catz fightpad pro ps3
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0x8180), E_HidControllerHelper::PS3, Core::StringView16() },											// Mad Catz Alpha PS4 mode (no touchpad on device)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0x8838), E_HidControllerHelper::PS3, Core::StringView16() },											// Mad Catz Fightstick Pro
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PansignalTechnology, 0x0008), E_HidControllerHelper::PS3, Core::StringView16() },								// Green Asia
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PansignalTechnology, 0x3075), E_HidControllerHelper::PS3, Core::StringView16() },								// SpeedLink Strike FX
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PansignalTechnology, 0x310d), E_HidControllerHelper::PS3, Core::StringView16() },								// From SDL
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0109), E_HidControllerHelper::PS3, Core::StringView16() },												// PDP Versus Fighting Pad
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x011e), E_HidControllerHelper::PS3, Core::StringView16() },												// Rock Candy PS4
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0128), E_HidControllerHelper::PS3, Core::StringView16() },												// Rock Candy PS3
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0203), E_HidControllerHelper::PS3, Core::StringView16() },												// Victrix Pro FS (PS4 peripheral but no trackpad/lightbar)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0214), E_HidControllerHelper::PS3, Core::StringView16() },												// afterglow ps3
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x1314), E_HidControllerHelper::PS3, Core::StringView16() },												// PDP Afterglow Wireless PS3 controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x6302), E_HidControllerHelper::PS3, Core::StringView16() },												// From SDL
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PersonalCommunicationSystems, 0x0001), E_HidControllerHelper::PS3, Core::StringView16() },					// actually ps2 - maybe break out later
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PersonalCommunicationSystems, 0x0003), E_HidControllerHelper::PS3, Core::StringView16() },					// actually ps2 - maybe break out later
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ShenzhenLongshengweiTechnology, 0x181a), E_HidControllerHelper::PS3, Core::StringView16() },					// Venom Arcade Stick
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ShenzhenLongshengweiTechnology, 0x1844), E_HidControllerHelper::PS3, Core::StringView16() },					// From SDL
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Sony, 0x0268), E_HidControllerHelper::PS3, Core::StringView16() },											// Sony PS3 Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::SinoLite, 0x1000), E_HidControllerHelper::PS3, Core::StringView16() },										// PS2 ACME GA-D5
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::SinoLite, 0x6005), E_HidControllerHelper::PS3, Core::StringView16() },										// PS2 maybe break out later
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Sysgration, 0x1004), E_HidControllerHelper::PS3, Core::StringView16() },										// From SDL
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Sysgration, 0x1006), E_HidControllerHelper::PS3, Core::StringView16() },										// JC-U3412SBK
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMaster, 0xb315), E_HidControllerHelper::PS3, Core::StringView16() },									// Firestorm Dual Analog 3
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMaster, 0xd007), E_HidControllerHelper::PS3, Core::StringView16() },									// Thrustmaster wireless 3-1
				{ MAKE_CONTROLLER_ID(0x11ff, 0x3331), E_HidControllerHelper::PS3, Core::StringView16() },																// SRXJ-PH2400
				{ MAKE_CONTROLLER_ID(0x1a34, 0x0836), E_HidControllerHelper::PS3, Core::StringView16() },																// Afterglow PS3
				{ MAKE_CONTROLLER_ID(0x20bc, 0x5500), E_HidControllerHelper::PS3, Core::StringView16() },																// ShanWan PS3
				{ MAKE_CONTROLLER_ID(0x20d6, 0x576d), E_HidControllerHelper::PS3, Core::StringView16() },																// Power A PS3
				{ MAKE_CONTROLLER_ID(0x20d6, 0xca6d), E_HidControllerHelper::PS3, Core::StringView16() },																// From SDL
				{ MAKE_CONTROLLER_ID(0x2563, 0x0523), E_HidControllerHelper::PS3, Core::StringView16() },																// Digiflip GP006
				{ MAKE_CONTROLLER_ID(0x2563, 0x0575), E_HidControllerHelper::PS3, Core::StringView16() },																// From SDL
				{ MAKE_CONTROLLER_ID(0x25f0, 0x83c3), E_HidControllerHelper::PS3, Core::StringView16() },																// gioteck vx2
				{ MAKE_CONTROLLER_ID(0x25f0, 0xc121), E_HidControllerHelper::PS3, Core::StringView16() },																//
				{ MAKE_CONTROLLER_ID(0x2c22, 0x2000), E_HidControllerHelper::PS3, Core::StringView16() },																// Quanba Drone
				{ MAKE_CONTROLLER_ID(0x2c22, 0x2003), E_HidControllerHelper::PS3, Core::StringView16() },																// From SDL
				{ MAKE_CONTROLLER_ID(0x8380, 0x0003), E_HidControllerHelper::PS3, Core::StringView16() },																// BTP 2163
				{ MAKE_CONTROLLER_ID(0x8888, 0x0308), E_HidControllerHelper::PS3, Core::StringView16() },																// Sony PS3 Controller

				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::BigBenInteractive, 0x0d13), E_HidControllerHelper::PS4, Core::StringView16() },								// NACON Revolution 3
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::BigBenInteractive, 0x0d09), E_HidControllerHelper::PS4, Core::StringView16() },								// NACON Daija Fight Stick - touchpad but no gyro/rumble
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::BigBenInteractive, 0x0d10), E_HidControllerHelper::PS4, Core::StringView16() },								// NACON Revolution Unlimited
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::BigBenInteractive, 0x0d08), E_HidControllerHelper::PS4, Core::StringView16() },								// NACON Revolution Unlimited Wireless Dongle 
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::BigBenInteractive, 0x0d06), E_HidControllerHelper::PS4, Core::StringView16() },								// NACON Asymetrical Controller Wireless Dongle -- show up as ps4 until you connect controller to it then it reboots into Xbox controller with different vvid/pid
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::BigBenInteractive, 0x1103), E_HidControllerHelper::PS4, Core::StringView16() },								// NACON Asymetrical Controller -- on windows this doesn't enumerate
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::BigBenInteractive, 0x0d01), E_HidControllerHelper::PS4, Core::StringView16() },								// Nacon Revolution Pro Controller - has gyro
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::BigBenInteractive, 0x0d02), E_HidControllerHelper::PS4, Core::StringView16() },								// Nacon Revolution Pro Controller v2 - has gyro
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::BigBenInteractive, 0x0d10), E_HidControllerHelper::PS4, Core::StringView16() },								// NACON Revolution Infinite - has gyro
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x0055), E_HidControllerHelper::PS4, Core::StringView16() },											// HORIPAD 4 FPS
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x0066), E_HidControllerHelper::PS4, Core::StringView16() },											// HORIPAD 4 FPS Plus 
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x0084), E_HidControllerHelper::PS4, Core::StringView16() },											// HORI Fighting Commander PS4
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x008a), E_HidControllerHelper::PS4, Core::StringView16() },											// HORI Real Arcade Pro 4
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x009c), E_HidControllerHelper::PS4, Core::StringView16() },											// HORI TAC PRO mousething
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x00a0), E_HidControllerHelper::PS4, Core::StringView16() },											// HORI TAC4 mousething
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x00ee), E_HidControllerHelper::PS4, Core::StringView16() },											// Hori mini wired https://www.playstation.com/en-us/explore/accessories/gaming-controllers/mini-wired-gamepad/
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x0123), E_HidControllerHelper::PS4, Core::StringView16() },											// HORI Wireless Controller Light (Japan only) - only over bt- over usb is xbox and pid 0x0124
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0x8250), E_HidControllerHelper::PS4, Core::StringView16() },											// Mad Catz FightPad Pro PS4
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0x8384), E_HidControllerHelper::PS4, Core::StringView16() },											// Mad Catz FightStick TE S+ PS4
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0x8480), E_HidControllerHelper::PS4, Core::StringView16() },											// Mad Catz FightStick TE 2 PS4
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::MadCatz, 0x8481), E_HidControllerHelper::PS4, Core::StringView16() },											// Mad Catz FightStick TE 2+ PS4
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0207), E_HidControllerHelper::PS4, Core::StringView16() },												// Victrix Pro Fightstick w/ Touchpad for PS4
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RazerSabertooth, 0X0401), E_HidControllerHelper::PS4, Core::StringView16() },									// Razer Panthera PS4 Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RazerSabertooth, 0x1000), E_HidControllerHelper::PS4, Core::StringView16() },									// Razer Raiju PS4 Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RazerSabertooth, 0x1004), E_HidControllerHelper::PS4, Core::StringView16() },									// Razer Raiju 2 Ultimate USB
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RazerSabertooth, 0x1007), E_HidControllerHelper::PS4, Core::StringView16() },									// Razer Raiju 2 Tournament edition USB
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RazerSabertooth, 0x1008), E_HidControllerHelper::PS4, Core::StringView16() },									// Razer Panthera Evo Fightstick
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RazerSabertooth, 0x1009), E_HidControllerHelper::PS4, Core::StringView16() },									// Razer Raiju 2 Ultimate BT
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RazerSabertooth, 0x100A), E_HidControllerHelper::PS4, Core::StringView16() },									// Razer Raiju 2 Tournament edition BT
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::RazerSabertooth, 0x1100), E_HidControllerHelper::PS4, Core::StringView16() },									// Razer RAION Fightpad - Trackpad, no gyro, lightbar hardcoded to green
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ShenzhenLongshengweiTechnology, 0x181b), E_HidControllerHelper::PS4, Core::StringView16() },					// Venom Arcade Stick - XXX:this may not work and may need to be called a ps3 controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Sony, 0x05c4), E_HidControllerHelper::PS4, Core::StringView16() },											// Sony PS4 Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Sony, 0x05c5), E_HidControllerHelper::PS4, Core::StringView16() },											// STRIKEPAD PS4 Grip Add-on
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Sony, 0x09cc), E_HidControllerHelper::PS4, Core::StringView16() },											// Sony PS4 Slim Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Sony, 0x0ba0), E_HidControllerHelper::PS4, Core::StringView16() },											// Sony PS4 Controller (Wireless dongle)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::ThrustMaster, 0xd00e), E_HidControllerHelper::PS4, Core::StringView16() },									// Thrustmast Eswap Pro - No gyro and lightbar doesn't change color. Works otherwise
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Zeroplus, 0x1e10), E_HidControllerHelper::PS4, Core::StringView16() },										// P4 Wired Gamepad generic knock off - lightbar but not trackpad or gyro
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Zeroplus, 0x0E10), E_HidControllerHelper::PS4, Core::StringView16() },										// Armor Armor 3 Pad PS4
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Zeroplus, 0x1CF6), E_HidControllerHelper::PS4, Core::StringView16() },										// EMIO PS4 Elite Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Zeroplus, 0x0e15), E_HidControllerHelper::PS4, Core::StringView16() },										// Game:Pad 4
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Zeroplus, 0x0ef6), E_HidControllerHelper::PS4, Core::StringView16() },										// Hitbox Arcade Stick
				{ MAKE_CONTROLLER_ID(0x11c0, 0x4001), E_HidControllerHelper::PS4, Core::StringView16() },																// "PS4 Fun Controller" added from user log
				{ MAKE_CONTROLLER_ID(0x20d6, 0x792a), E_HidControllerHelper::PS4, Core::StringView16() },																// PowerA - Fusion Fight Pad
				{ MAKE_CONTROLLER_ID(0x7545, 0x0104), E_HidControllerHelper::PS4, Core::StringView16() },																// Armor 3 or Level Up Cobra - At least one variant has gyro
				{ MAKE_CONTROLLER_ID(0x9886, 0x0025), E_HidControllerHelper::PS4, Core::StringView16() },																// Astro C40

				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Nintendo, 0x2006), E_HidControllerHelper::SwitchJoyConLeft, Core::StringView16() },											// Nintendo Switch Joy-Con (Left)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Nintendo, 0x2007), E_HidControllerHelper::SwitchJoyConRight, Core::StringView16() },											// Nintendo Switch Joy-Con (Right)

				// This same controller ID is spoofed by many 3rd-party Switch controllers.
				// The ones we currently know of are:
				// * Any 8bitdo controller with Switch support
				// * ORTZ Gaming Wireless Pro Controller
				// * ZhiXu Gamepad Wireless
				// * Sunwaytek Wireless Motion Controller for Nintendo Switch
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Nintendo, 0x2009), E_HidControllerHelper::SwitchPro, Core::StringView16() },									// Nintendo Switch Pro Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x00f6), E_HidControllerHelper::SwitchPro, Core::StringView16() },										// HORI Wireless Switch Pad

				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x00c1), E_HidControllerHelper::SwitchProInput, Core::StringView16() },								// HORIPAD for Nintendo Switch
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x0092), E_HidControllerHelper::SwitchProInput, Core::StringView16() },								// HORI Pokken Tournament DX Pro Pad
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Hori, 0x00aa), E_HidControllerHelper::SwitchProInput, Core::StringView16() },								// HORI Real Arcade Pro V Hayabusa in Switch Mode
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0185), E_HidControllerHelper::SwitchProInput, Core::StringView16() },									// PDP Wired Fight Pad Pro for Nintendo Switch
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0180), E_HidControllerHelper::SwitchProInput, Core::StringView16() },									// PDP Faceoff Wired Pro Controller for Nintendo Switch
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0181), E_HidControllerHelper::SwitchProInput, Core::StringView16() },									// PDP Faceoff Deluxe Wired Pro Controller for Nintendo Switch
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0186), E_HidControllerHelper::SwitchProInput, Core::StringView16() },									// PDP Afterglow Wireless Switch Controller - working gyro. USB doesn't work
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0184), E_HidControllerHelper::SwitchProInput, Core::StringView16() },									// PDP Faceoff Wired Deluxe+ Audio Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0188), E_HidControllerHelper::SwitchProInput, Core::StringView16() },									// PDP Afterglow Wired Deluxe+ Audio Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::PDP, 0x0187), E_HidControllerHelper::SwitchProInput, Core::StringView16() },									// PDP Rockcandy Wirec Controller
				{ MAKE_CONTROLLER_ID(0x20d6, 0xa711), E_HidControllerHelper::SwitchProInput, Core::StringView16() },													// PowerA Wired Controller Plus/PowerA Wired Controller Nintendo GameCube Style
				{ MAKE_CONTROLLER_ID(0x20d6, 0xa712), E_HidControllerHelper::SwitchProInput, Core::StringView16() },													// PowerA - Fusion Fight Pad
				{ MAKE_CONTROLLER_ID(0x20d6, 0xa713), E_HidControllerHelper::SwitchProInput, Core::StringView16() },													// PowerA - Super Mario Controller

				// Valve products - don't add to public list
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Valve, 0x1101), E_HidControllerHelper::SteamV1, Core::StringView16() },											// Valve Legacy Steam Controller (CHELL)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Valve, 0x1102), E_HidControllerHelper::SteamV1, Core::StringView16() },											// Valve wired Steam Controller (D0G)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Valve, 0x1105), E_HidControllerHelper::SteamV1, Core::StringView16() },											// Valve Bluetooth Steam Controller (D0G)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Valve, 0x1106), E_HidControllerHelper::SteamV1, Core::StringView16() },											// Valve Bluetooth Steam Controller (D0G)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Valve, 0x1142), E_HidControllerHelper::SteamV1, Core::StringView16() },											// Valve wireless Steam Controller
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Valve, 0x1201), E_HidControllerHelper::SteamV2, Core::StringView16() },										// Valve wired Steam Controller (HEADCRAB)
				{ MAKE_CONTROLLER_ID(HidApi::E_USB_Vendor::Valve, 0x1202), E_HidControllerHelper::SteamV2, Core::StringView16() },										// Valve Bluetooth Steam Controller (HEADCRAB)
			};

			namespace Funcs
			{
				extern Input::HidApi::E_USB_Vendor GetUSBVendor(FXD::U16 nVendorID);
				extern Input::HidApi::E_HidControllerType GetGameControllerType(const FXD::UTF16* pName, Input::HidApi::E_USB_Vendor eVendor, FXD::U16 nProduct, FXD::S32 nInterfaceNumber, FXD::S32 nInterfaceClass, FXD::S32 nInterfaceSubclass, FXD::S32 nInterfaceProtocol);
				extern Input::HidApi::E_HidControllerHelper GuessControllerType(Input::HidApi::E_USB_Vendor eVendor, FXD::S32 nPID);
				extern bool ShouldIgnoreController(const FXD::UTF16* pName, Input::HidApi::HidGuid guid);
				extern FXD::Core::String16 CreateControllerName(Input::HidApi::E_USB_Vendor eVendor, FXD::U16 nProduct, const FXD::UTF16* pVendorName, const FXD::UTF16* pProductName);
				extern FXD::Core::StringView16 GuessControllerName(Input::HidApi::E_USB_Vendor eVendor, FXD::S32 nPID);
			} //namespace Funcs
		} //namespace HidApi
	} //namespace Input
} //namespace FXD
#endif //IsInputHIDAPI()
#endif //FXDENGINE_INPUT_HIDAPI_HIDCONTROLLER_H