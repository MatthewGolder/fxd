// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_HIDAPI_PS3_PS3_H
#define FXDENGINE_INPUT_HIDAPI_PS3_PS3_H

#include "FXDEngine/Input/Types.h"

#if IsInputPS3()

namespace FXD
{
	namespace Input
	{
		namespace HidApi
		{
			class IGamePadPS3;
			typedef std::shared_ptr< Input::HidApi::IGamePadPS3 > GamePadPS3;
		} //namespace HidApi
	} //namespace Input
} //namespace FXD
#endif //IsInputPS3()
#endif //FXDENGINE_INPUT_HIDAPI_PS3_PS3_H