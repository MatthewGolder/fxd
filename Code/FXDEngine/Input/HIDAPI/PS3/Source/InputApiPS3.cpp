// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputPS3()
#include "FXDEngine/Input/HIDAPI/PS3/InputApiPS3.h"

using namespace FXD;
using namespace Input;
using namespace HidApi;

// ------
// IInputApiPS3
// - 
// ------
IInputApiPS3::IInputApiPS3(void)
{
}

IInputApiPS3::~IInputApiPS3(void)
{
}

bool IInputApiPS3::create_api(void)
{
	bool bRetVal = true;

	return bRetVal;
}

bool IInputApiPS3::release_api(void)
{
	bool bRetVal = true;

	return bRetVal;
}

void IInputApiPS3::_process(FXD::F32 /*dt*/)
{
}
#endif //IsInputPS3()