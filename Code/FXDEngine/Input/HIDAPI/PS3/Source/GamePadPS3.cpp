// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputPS3()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Input/Mapping.h"
#include "FXDEngine/Input/HIDAPI/HidApiDevice.h"
#include "FXDEngine/Input/HIDAPI/HidApiJPThread.h"
#include "FXDEngine/Input/HIDAPI/HidApiManager.h"
#include "FXDEngine/Input/HIDAPI/PS3/GamePadPS3.h"
#include "FXDEngine/Job/EventHandler.h"

using namespace FXD;
using namespace Input;
using namespace HidApi;

enum EPS3ReportId
{
	k_EPS3ReportIdUsbState = 1,
	k_EPS3ReportIdUsbEffects = 5,
	k_EPS3ReportIdBluetoothState = 17,
	k_EPS3ReportIdBluetoothEffects = 17,
	k_EPS3ReportIdDisconnectMessage = 226,
};

#define USB_PRODUCT_NINTENDO_GAMECUBE_ADAPTER			0x0337
#define USB_PRODUCT_NINTENDO_SWITCH_PRO					0x2009
#define USB_PRODUCT_RAZER_PANTHERA							0x0401
#define USB_PRODUCT_RAZER_PANTHERA_EVO						0x1008
#define USB_PRODUCT_RAZER_ATROX								0x0a00
#define USB_PRODUCT_SONY_DS4									0x05c4
#define USB_PRODUCT_SONY_DS4_DONGLE							0x0ba0
#define USB_PRODUCT_SONY_DS4_SLIM							0x09cc
#define USB_PRODUCT_XBOX_ONE_ELITE_SERIES_1				0x02e3
#define USB_PRODUCT_XBOX_ONE_ELITE_SERIES_2				0x0b00
#define USB_PRODUCT_XBOX_ONE_ELITE_SERIES_2_BLUETOOTH	0x0b05
#define USB_PRODUCT_XBOX_ONE_S								0x02ea
#define USB_PRODUCT_XBOX_ONE_S_REV1_BLUETOOTH			0x02e0
#define USB_PRODUCT_XBOX_ONE_S_REV2_BLUETOOTH			0x02fd

#define PS3_GAMEPAD_LEFT_THUMB_DEADZONE	7849
#define PS3_GAMEPAD_RIGHT_THUMB_DEADZONE	8689
#define PS3_GAMEPAD_START						0x0020
#define PS3_GAMEPAD_BACK						0x0010
#define PS3_GAMEPAD_GUIDE						0x0003
#define PS3_GAMEPAD_LEFT_TRIGGER				0x0080
#define PS3_GAMEPAD_RIGHT_TRIGGER			0x0080
#define PS3_GAMEPAD_LEFT_THUMB				0x0040
#define PS3_GAMEPAD_RIGHT_THUMB				0x0080
#define PS3_GAMEPAD_LEFT_BUMPER				0x0001
#define PS3_GAMEPAD_RIGHT_BUMPER				0x0002
#define PS3_GAMEPAD_A							0x0002
#define PS3_GAMEPAD_B							0x0004
#define PS3_GAMEPAD_X							0x0001
#define PS3_GAMEPAD_Y							0x0008

CONSTEXPR FXD::U8 k_ePS3FeatureReportIdSerialNumber = 0x12;

namespace
{
	static FXD::U32 crc32_for_byte(FXD::U32 r)
	{
		for (FXD::U32 n1 = 0; n1 < 8; ++n1)
		{
			r = (r & 1 ? 0 : (FXD::U32)0xEDB88320L) ^ r >> 1;
		}
		return r ^ (FXD::U32)0xFF000000L;
	}

	FXD::U32 crc32(FXD::U32 crc, const void* pData, FXD::U32 nCount)
	{
		for (FXD::U32 n1 = 0; n1 < nCount; ++n1)
		{
			crc = crc32_for_byte((FXD::U8)crc ^ ((const FXD::U8*)pData)[n1]) ^ crc >> 8;
		}
		return crc;
	}

	bool CheckUSBConnectedPS3(const HidApi::HidApiDevice* pDevice)
	{
		// This will fail if we're on Bluetooth
		FXD::U8 pData[16];
		Memory::MemHandle memHandle(pData, sizeof(pData));

		if (FXDHidApi()->hidApiManager()->read_feature_report(pDevice, k_ePS3FeatureReportIdSerialNumber, memHandle))
		{
			for (FXD::U32 n1 = 0; n1 < sizeof(pData); ++n1)
			{
				if (pData[n1] != 0x00)
				{
					return true;
				}
			}
		}
		return false;
	}

	bool CanRumblePS3(HidApi::E_USB_Vendor eVendorId, FXD::U16 nProductId)
	{
		// The Razer Panthera fight stick hangs when trying to rumble
		if ((eVendorId == E_USB_Vendor::RazerSabertooth) &&
			(nProductId == USB_PRODUCT_RAZER_PANTHERA || nProductId == USB_PRODUCT_RAZER_PANTHERA_EVO))
		{
			return false;
		}
		return true;
	}

	FXD::F32 map_trigger_value(FXD::U8 nVal)
	{
		return ((FXD::F32)nVal / 255.0f);
	}

	FXD::F32 map_stick_value(FXD::U8 nVal, const FXD::S16 nDeadZone)
	{
		FXD::S16 nRealVal = ((FXD::S16)((FXD::S32)nVal * 257) - 32768);

		// Deal with dead zone.
		if (nRealVal < 0)
		{
			nRealVal = (nRealVal + nDeadZone);
			if (nRealVal > 0)
			{
				nRealVal = 0;
			}
			nRealVal = (nRealVal * 32768) / (32768 - nDeadZone);
		}
		else
		{
			nRealVal = (nRealVal - nDeadZone);
			if (nRealVal < 0)
			{
				nRealVal = 0;
			}
			nRealVal = (nRealVal * 32768) / (32768 - nDeadZone);
		}
		return (FXD::F32)nRealVal / 32768.0f;
	}

	void PS3SetState(const IGamePadPS3::PS3Context& context, FXD::F32 fLeftRumble, FXD::F32 fRightRumble)
	{
		// In order to send rumble, we have to send a complete effect packet
		FXD::U32 nOffset = 0;
		FXD::U32 nReportSize = 0;
		FXD::U8 pData[78];
		Memory::MemZero_T(pData);

		if (context.m_bBluetooth)
		{
			pData[0] = k_EPS3ReportIdBluetoothEffects;
			pData[1] = (0xC0 | 0x04);	// Magic value HID + CRC, also sets interval to 4ms for samples
			pData[3] = 0x03;			// 0x1 is rumble, 0x2 is lightbar, 0x4 is the blink interval

			nReportSize = 78;
			nOffset = 6;
		}
		else
		{
			pData[0] = k_EPS3ReportIdUsbEffects;
			pData[1] = 0x07;			// Magic value

			nReportSize = 32;
			nOffset = 4;
		}

		IGamePadPS3::DS4EffectsState* pEffects = (IGamePadPS3::DS4EffectsState*)&pData[nOffset];
		if (context.m_bRumbleSupported)
		{
			pEffects->ucRumbleLeft = (FXD::U8)FXD::STD::clamp((FXD::S32)(fLeftRumble * 255), 0, 255);
			pEffects->ucRumbleRight = (FXD::U8)FXD::STD::clamp((FXD::S32)(fRightRumble * 255), 0, 255);
		}
		pEffects->ucLedRed = (FXD::U8)FXD::STD::clamp((FXD::S32)(context.m_colLed.getR() * 255), 0, 255);
		pEffects->ucLedGreen = (FXD::U8)FXD::STD::clamp((FXD::S32)(context.m_colLed.getG() * 255), 0, 255);
		pEffects->ucLedBlue = (FXD::U8)FXD::STD::clamp((FXD::S32)(context.m_colLed.getB() * 255), 0, 255);

		if (context.m_bBluetooth)
		{
			// Bluetooth reports need a CRC at the end of the packet (at least on Linux)
			FXD::U8 ubHdr = 0xA2; // hidp header is part of the CRC calculation
			FXD::U32 unCRC;
			unCRC = crc32(0, &ubHdr, 1);
			unCRC = crc32(unCRC, pData, (FXD::U32)(nReportSize - sizeof(unCRC)));
			Memory::MemCopy(&pData[nReportSize - sizeof(unCRC)], &unCRC, sizeof(unCRC));
		}

		Memory::MemHandle memHandle(pData, nReportSize);
		FXDHidApi()->hidApiManager()->submit_rumble_request(context.m_pHDevice, memHandle);
	}
}

// ------
// IGamePadPS3::PS3Context
// -
// ------
IGamePadPS3::PS3Context::PS3Context(HidApi::HidApiDevice* pHDevice, const FXD::S32 nPort)
	: m_pHDevice(pHDevice)
	, m_bConnected(false)
	, m_bColLedDirty(false)
	, m_bDongle(false)
	, m_bBluetooth(false)
	, m_bAudioSupported(false)
	, m_bRumbleSupported(false)
	, m_nVolume(0)
	, last_volume_check(0)
	, m_nPortIndex(nPort)
{
	Memory::MemZero_T(m_state);
}

// ------
// IGamePadPS3
// -
// ------
IGamePadPS3::IGamePadPS3(HidApi::HidApiDevice* pHDevice)
	: IInputDevice()
	, m_context(pHDevice, 0)
	, OnControllerButton(nullptr)
	, OnControllerAnalog(nullptr)
{
	OnControllerButton = FXDEvent()->attach< Input::OnControllerButton >([&](Input::OnControllerButton evt)
	{
		if (evt.Device == this)
		{
			PRINT_INFO << Input::GetGamePadName(evt.Control);
		}
	});
	OnControllerAnalog = FXDEvent()->attach< Input::OnControllerAnalog >([&](Input::OnControllerAnalog evt)
	{
		if (evt.Device == this)
		{
			PRINT_INFO << evt.Value;
		}
	});

	// Check for type of connection
	m_context.m_bDongle = (m_context.m_pHDevice->vendorId() == E_USB_Vendor::Sony && m_context.m_pHDevice->productID() == USB_PRODUCT_SONY_DS4_DONGLE);
	if (m_context.m_bDongle)
	{
		m_context.m_bBluetooth = false;
	}
	else if (m_context.m_pHDevice->vendorId() == E_USB_Vendor::Sony)
	{
		m_context.m_bBluetooth = !CheckUSBConnectedPS3(m_context.m_pHDevice);
	}
	else
	{
		// Third party controllers appear to all be wired
		m_context.m_bBluetooth = false;
	}
	// Check to see if audio is supported
	if (m_context.m_pHDevice->vendorId() == E_USB_Vendor::Sony && (m_context.m_pHDevice->productID() == USB_PRODUCT_SONY_DS4_SLIM || m_context.m_pHDevice->productID() == USB_PRODUCT_SONY_DS4_DONGLE))
	{
		m_context.m_bAudioSupported = true;
	}

	if (CanRumblePS3(m_context.m_pHDevice->vendorId(), m_context.m_pHDevice->productID()))
	{
		if (m_context.m_bBluetooth)
		{
			m_context.m_bRumbleSupported = true;
		}
		else
		{
			m_context.m_bRumbleSupported = true;
		}
	}
}

IGamePadPS3::~IGamePadPS3(void)
{
	if (OnControllerButton != nullptr)
	{
		FXDEvent()->detach(OnControllerButton);
		OnControllerButton = nullptr;
	}
	if (OnControllerAnalog != nullptr)
	{
		FXDEvent()->detach(OnControllerAnalog);
		OnControllerAnalog = nullptr;
	}
}

void IGamePadPS3::set_colour(const Core::ColourRGB& col)
{
	m_context.m_colLed = col;
	m_context.m_bColLedDirty = true;
}

bool IGamePadPS3::is_connected(void) const
{
	return false;
}

bool IGamePadPS3::is_gamepad_available(void) const
{
	return false;
}

bool IGamePadPS3::supports_rumble(void) const
{
	return false;
}

Core::String8 IGamePadPS3::get_config_name(void) const
{
	return UTF_8("PS3");
}

Core::String8 IGamePadPS3::get_device_name(void) const
{
	return UTF_8("PS3");
}

Input::E_DeviceType IGamePadPS3::get_device_type(void) const
{
	return Input::E_DeviceType::Controller;
}

void IGamePadPS3::_update(FXD::F32 dt)
{
	bool bWasConnected = m_context.m_bConnected;

	FXD::U8 pData[USB_PACKET_LENGTH];
	FXD::S32 nReadSize = 0;
	PS3PacketState* pPacket = nullptr;
	{
		while ((nReadSize = FXDHidApi()->hidApiManager()->read_input(m_context.m_pHDevice, pData, sizeof(pData), 0)) > 0)
		{
			switch (pData[0])
			{
				case k_EPS3ReportIdUsbState:
				{
					pPacket = (PS3PacketState*)(&pData[1]);
					break;
				}
				case k_EPS3ReportIdBluetoothState:
				{
					// Bluetooth state packets have two additional bytes at the beginning
					pPacket = (PS3PacketState*)(&pData[3]);
					break;
				}
				default:
				{
					pPacket = nullptr;
					break;
				}
			}
		}
	}
	m_context.m_bConnected = !(nReadSize < 0);

	if (!bWasConnected && m_context.m_bConnected)
	{
		FXDEvent()->raise(Input::OnControllerConnection(this, true, m_context.m_nPortIndex));
	}
	else if (bWasConnected && !m_context.m_bConnected)
	{
		FXDEvent()->raise(Input::OnControllerConnection(this, false, m_context.m_nPortIndex));
	}

	if (pPacket != nullptr)
	{
		if (pPacket->nBatteryLevel & 0x10)
		{
			m_ePowerLevel = E_PowerLevel::Wired;
		}
		else
		{
			// Battery level ranges from 0 to 10
			FXD::S32 nLevel = (pPacket->nBatteryLevel & 0xF);
			if (nLevel == 0)
			{
				m_ePowerLevel = E_PowerLevel::Empty;
			}
			else if (nLevel <= 2)
			{
				m_ePowerLevel = E_PowerLevel::Low;
			}
			else if (nLevel <= 7)
			{
				m_ePowerLevel = E_PowerLevel::Medium;
			}
			else
			{
				m_ePowerLevel = E_PowerLevel::Full;
			}
		}

		if (m_context.m_bConnected)
		{
			bool bStateDirty = false;
			FXD::F32 fLeftRumble = 0.0f;
			FXD::F32 fRightRumble = 0.0f;

			if (m_context.m_bColLedDirty)
			{
				fLeftRumble = (m_bRumblePaused) ? 0.0f : m_rumbleData.m_fLeftRumble;
				fRightRumble = (m_bRumblePaused) ? 0.0f : m_rumbleData.m_fRightRumble;
				bStateDirty = true;
			}

			if (m_bRumbleDirty)
			{
				fLeftRumble = (m_bRumblePaused) ? 0.0f : m_rumbleData.m_fLeftRumble;
				fRightRumble = (m_bRumblePaused) ? 0.0f : m_rumbleData.m_fRightRumble;
				bStateDirty = true;
			}
			else
			{
				if (!m_bRumblePaused && (m_rumbleData.m_fLeftRumble > 0.0f || m_rumbleData.m_fRightRumble > 0.0f || m_rumbleData.m_fDuration > 0.0f))
				{
					m_fRumbleExpiration += dt;
					if (m_fRumbleExpiration > m_rumbleData.m_fDuration)
					{
						m_fRumbleExpiration = 0.0f;
						m_rumbleData.m_fLeftRumble = 0.0f;
						m_rumbleData.m_fRightRumble = 0.0f;
						m_rumbleData.m_fDuration = 0.0f;
						bStateDirty = true;
					}
				}
			}

			if (bStateDirty)
			{
				PS3SetState(m_context, fLeftRumble, fRightRumble);
				m_bRumbleDirty = false;
				m_context.m_bColLedDirty = false;
			}
		}

		if (m_context.m_bConnected || bWasConnected)
		{
			{
				FXD::U8 nData = (pPacket->nButtons[0] & 0x0F);
				FXD::U8 nLastData = (m_context.m_state.nButtons[0] & 0x0F);

				bool bUp = ((nData == 0) || (nData == 1) || (nData == 7));
				bool bLastUp = ((nLastData == 0) || (nLastData == 1) || (nLastData == 7));

				bool bDown = ((nData == 3) || (nData == 4) || (nData == 5));
				bool bLastDown = ((nLastData == 3) || (nLastData == 4) || (nLastData == 5));

				bool bLeft = ((nData == 5) || (nData == 6) || (nData == 7));
				bool bLastLeft = ((nLastData == 5) || (nLastData == 6) || (nLastData == 7));

				bool bRight = ((nData == 1) || (nData == 2) || (nData == 3));
				bool bLastRight = ((nLastData == 1) || (nLastData == 2) || (nLastData == 3));

				if (bUp != bLastUp)
				{
					FXDEvent()->raise(Input::OnControllerButton(this, bLeft, E_GamePadButtons::DPadUp, m_context.m_nPortIndex));
				}
				if (bDown != bLastDown)
				{
					FXDEvent()->raise(Input::OnControllerButton(this, bLeft, E_GamePadButtons::DPadDown, m_context.m_nPortIndex));
				}
				if (bLeft != bLastLeft)
				{
					FXDEvent()->raise(Input::OnControllerButton(this, bLeft, E_GamePadButtons::DPadLeft, m_context.m_nPortIndex));
				}
				if (bRight != bLastRight)
				{
					FXDEvent()->raise(Input::OnControllerButton(this, bLeft, E_GamePadButtons::DPadRight, m_context.m_nPortIndex));
				}
			}
			if ((pPacket->nButtons[1] & PS3_GAMEPAD_LEFT_THUMB) != (m_context.m_state.nButtons[1] & PS3_GAMEPAD_LEFT_THUMB))
			{
				const bool bVal = (pPacket->nButtons[1] & PS3_GAMEPAD_LEFT_THUMB);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::LeftThumb, m_context.m_nPortIndex));
			}
			if ((pPacket->nButtons[1] & PS3_GAMEPAD_RIGHT_THUMB) != (m_context.m_state.nButtons[1] & PS3_GAMEPAD_RIGHT_THUMB))
			{
				const bool bVal = (pPacket->nButtons[1] & PS3_GAMEPAD_RIGHT_THUMB);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::RightThumb, m_context.m_nPortIndex));
			}
			if ((pPacket->nButtons[1] & PS3_GAMEPAD_LEFT_BUMPER) != (m_context.m_state.nButtons[1] & PS3_GAMEPAD_LEFT_BUMPER))
			{
				const bool bVal = (pPacket->nButtons[1] & PS3_GAMEPAD_LEFT_BUMPER);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::LeftBumper, m_context.m_nPortIndex));
			}
			if ((pPacket->nButtons[1] & PS3_GAMEPAD_RIGHT_BUMPER) != (m_context.m_state.nButtons[1] & PS3_GAMEPAD_RIGHT_BUMPER))
			{
				const bool bVal = (pPacket->nButtons[1] & PS3_GAMEPAD_RIGHT_BUMPER);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::RightBumper, m_context.m_nPortIndex));
			}
			if ((pPacket->nButtons[1] & PS3_GAMEPAD_START) != (m_context.m_state.nButtons[1] & PS3_GAMEPAD_START))
			{
				const bool bVal = (pPacket->nButtons[1] & PS3_GAMEPAD_START);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::Start, m_context.m_nPortIndex));
			}
			if ((pPacket->nButtons[1] & PS3_GAMEPAD_BACK) != (m_context.m_state.nButtons[1] & PS3_GAMEPAD_BACK))
			{
				const bool bVal = (pPacket->nButtons[1] & PS3_GAMEPAD_BACK);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::Back, m_context.m_nPortIndex));
			}
			if ((pPacket->nButtons[0] >> 4 & PS3_GAMEPAD_A) != (m_context.m_state.nButtons[0] >> 4 & PS3_GAMEPAD_A))
			{
				const bool bVal = (pPacket->nButtons[0] >> 4 & PS3_GAMEPAD_A);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::AButton, m_context.m_nPortIndex));
			}
			if ((pPacket->nButtons[0] >> 4 & PS3_GAMEPAD_B) != (m_context.m_state.nButtons[0] >> 4 & PS3_GAMEPAD_B))
			{
				const bool bVal = (pPacket->nButtons[0] >> 4 & PS3_GAMEPAD_B);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::BButton, m_context.m_nPortIndex));
			}
			if ((pPacket->nButtons[0] >> 4 & PS3_GAMEPAD_X) != (m_context.m_state.nButtons[0] >> 4 & PS3_GAMEPAD_X))
			{
				const bool bVal = (pPacket->nButtons[0] >> 4 & PS3_GAMEPAD_X);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::XButton, m_context.m_nPortIndex));
			}
			if ((pPacket->nButtons[0] >> 4 & PS3_GAMEPAD_Y) != (m_context.m_state.nButtons[0] >> 4 & PS3_GAMEPAD_Y))
			{
				const bool bVal = (pPacket->nButtons[0] >> 4 & PS3_GAMEPAD_Y);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::YButton, m_context.m_nPortIndex));
			}
			if ((pPacket->nCounterGuide & PS3_GAMEPAD_GUIDE) != (m_context.m_state.nCounterGuide & PS3_GAMEPAD_GUIDE))
			{
				const bool bVal = (pPacket->nCounterGuide & PS3_GAMEPAD_GUIDE);
				FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::Guide, m_context.m_nPortIndex));
			}
			if ((pPacket->nLTrigger) != (m_context.m_state.nLTrigger))
			{
				const FXD::F32 fVal = map_trigger_value(pPacket->nLTrigger);
				FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::LeftTrigger, m_context.m_nPortIndex));
			}
			if ((pPacket->nRTrigger) != (m_context.m_state.nRTrigger))
			{
				const FXD::F32 fVal = map_trigger_value(pPacket->nRTrigger);
				FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::RightTrigger, m_context.m_nPortIndex));
			}
			if (map_stick_value(pPacket->sThumbLX, PS3_GAMEPAD_LEFT_THUMB_DEADZONE) !=
				map_stick_value(m_context.m_state.sThumbLX, PS3_GAMEPAD_LEFT_THUMB_DEADZONE))
			{
				const FXD::F32 fVal = map_stick_value(pPacket->sThumbLX, PS3_GAMEPAD_LEFT_THUMB_DEADZONE);
				FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::LeftThumbX, m_context.m_nPortIndex));
			}
			if (map_stick_value(pPacket->sThumbLY, PS3_GAMEPAD_LEFT_THUMB_DEADZONE) !=
				map_stick_value(m_context.m_state.sThumbLY, PS3_GAMEPAD_LEFT_THUMB_DEADZONE))
			{
				const FXD::F32 fVal = map_stick_value(pPacket->sThumbLY, PS3_GAMEPAD_LEFT_THUMB_DEADZONE);
				FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::LeftThumbY, m_context.m_nPortIndex));
			}
			if (map_stick_value(pPacket->sThumbRX, PS3_GAMEPAD_RIGHT_THUMB_DEADZONE) !=
				map_stick_value(m_context.m_state.sThumbRX, PS3_GAMEPAD_RIGHT_THUMB_DEADZONE))
			{
				const FXD::F32 fVal = map_stick_value(pPacket->sThumbRX, PS3_GAMEPAD_RIGHT_THUMB_DEADZONE);
				FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::RightThumbX, m_context.m_nPortIndex));
			}
			if (map_stick_value(pPacket->sThumbRY, PS3_GAMEPAD_RIGHT_THUMB_DEADZONE) !=
				map_stick_value(m_context.m_state.sThumbRY, PS3_GAMEPAD_RIGHT_THUMB_DEADZONE))
			{
				const FXD::F32 fVal = map_stick_value(pPacket->sThumbRY, PS3_GAMEPAD_RIGHT_THUMB_DEADZONE);
				FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::RightThumbY, m_context.m_nPortIndex));
			}
		}
		Memory::MemCopy(&m_context.m_state, pPacket, sizeof(PS3PacketState));
	}
	else
	{
		if ((nReadSize < 0) && bWasConnected)
		{
			m_ePowerLevel = E_PowerLevel::Unknown;
			Memory::MemZero_T(m_context.m_state);
			//HIDAPI_JoystickDisconnected(device, joystick->instance_id, false);
		}
	}
}

static CONSTEXPR const Widget::WIDGET_DESC kPS3Widgets[] =
{
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::DPadUp), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::DPadUp), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::DPadDown), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::DPadDown), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::DPadLeft), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::DPadLeft), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::DPadRight), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::DPadRight), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftThumbX), Input::E_Widget_type::FullAxis, Input::GetGamePadName(E_GamePadButtons::LeftThumbX), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftThumbY), Input::E_Widget_type::FullAxis, Input::GetGamePadName(E_GamePadButtons::LeftThumbY), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightThumbX), Input::E_Widget_type::FullAxis, Input::GetGamePadName(E_GamePadButtons::RightThumbX), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightThumbY), Input::E_Widget_type::FullAxis, Input::GetGamePadName(E_GamePadButtons::RightThumbY), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftThumb), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::LeftThumb), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightThumb), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::RightThumb), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftBumper), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::LeftBumper), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightBumper), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::RightBumper), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftTrigger), Input::E_Widget_type::HalfAxis, Input::GetGamePadName(E_GamePadButtons::LeftTrigger), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightTrigger), Input::E_Widget_type::HalfAxis, Input::GetGamePadName(E_GamePadButtons::RightTrigger), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::Start), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::Start), Widget::kWidgetFlagStartButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::Back), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::Back), Widget::kWidgetFlagBackButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::AButton), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::AButton), Widget::kWidgetFlagActionButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::BButton), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::BButton), Widget::kWidgetFlagBackButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::XButton), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::XButton), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::YButton), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::YButton), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::Guide), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::Guide), Widget::kWidgetFlagGuideButton),
	Widget::WIDGET_DESC()
};

const Widget::WIDGET_DESC* IGamePadPS3::_get_widget_defs(void) const
{
	return kPS3Widgets;
}

void IGamePadPS3::_get_widget_state(FXD::U16 nID, void* pState) const
{
	bool* pDigitalState = (bool*)pState;
	FXD::F32* pAnalogState = (FXD::F32*)pState;

	switch ((E_GamePadButtons)nID)
	{
		case E_GamePadButtons::DPadUp:
		{
			FXD::U8 nData = (m_context.m_state.nButtons[0] & 0x0F);
			bool bVal = ((nData == 0) || (nData == 1) || (nData == 7));
			*pDigitalState = bVal;
			break;
		}
		case E_GamePadButtons::DPadDown:
		{
			FXD::U8 nData = (m_context.m_state.nButtons[0] & 0x0F);
			bool bVal = ((nData == 3) || (nData == 4) || (nData == 5));
			*pDigitalState = bVal;
			break;
		}
		case E_GamePadButtons::DPadLeft:
		{
			FXD::U8 nData = (m_context.m_state.nButtons[0] & 0x0F);
			bool bVal = ((nData == 5) || (nData == 6) || (nData == 7));
			*pDigitalState = bVal;
			break;
		}
		case E_GamePadButtons::DPadRight:
		{
			FXD::U8 nData = (m_context.m_state.nButtons[0] & 0x0F);
			bool bVal = ((nData == 1) || (nData == 2) || (nData == 3));
			*pDigitalState = bVal;
			break;
		}
		case E_GamePadButtons::LeftThumb:
		{
			const FXD::U8 nData = m_context.m_state.nButtons[1];
			*pDigitalState = (nData & 0x0040);
			break;
		}
		case E_GamePadButtons::RightThumb:
		{
			const FXD::U8 nData = m_context.m_state.nButtons[1];
			*pDigitalState = (nData & 0x0080);
			break;
		}
		case E_GamePadButtons::LeftBumper:
		{
			const FXD::U8 nData = m_context.m_state.nButtons[1];
			*pDigitalState = (nData & 0x0001);
			break;
		}
		case E_GamePadButtons::RightBumper:
		{
			const FXD::U8 nData = m_context.m_state.nButtons[1];
			*pDigitalState = (nData & 0x0002);
			break;
		}
		case E_GamePadButtons::Start:
		{
			const FXD::U8 nData = m_context.m_state.nButtons[1];
			*pDigitalState = (nData & 0x0020);
			break;
		}
		case E_GamePadButtons::Back:
		{
			const FXD::U8 nData = m_context.m_state.nButtons[1];
			*pDigitalState = (nData & 0x0010);
			break;
		}
		case E_GamePadButtons::AButton:
		{
			const FXD::U8 nData = (m_context.m_state.nButtons[0] >> 4);
			*pDigitalState = (nData & 0x0002);
			break;
		}
		case E_GamePadButtons::BButton:
		{
			const FXD::U8 nData = (m_context.m_state.nButtons[0] >> 4);
			*pDigitalState = (nData & 0x0004);
			break;
		}
		case E_GamePadButtons::XButton:
		{
			const FXD::U8 nData = (m_context.m_state.nButtons[0] >> 4);
			*pDigitalState = (nData & 0x0001);
			break;
		}
		case E_GamePadButtons::YButton:
		{
			const FXD::U8 nData = (m_context.m_state.nButtons[0] >> 4);
			*pDigitalState = (nData & 0x0008);
			break;
		}
		case E_GamePadButtons::Guide:
		{
			const FXD::U8 nData = (m_context.m_state.nCounterGuide & 0x03);
			*pDigitalState = (nData & 0x0001);
			break;
		}
		case E_GamePadButtons::LeftTrigger:
		{
			*pAnalogState = map_trigger_value(m_context.m_state.nLTrigger);
			break;
		}
		case E_GamePadButtons::RightTrigger:
		{
			*pAnalogState = map_trigger_value(m_context.m_state.nRTrigger);
			break;
		}
		case E_GamePadButtons::LeftThumbX:
		{
			*pAnalogState = map_stick_value(m_context.m_state.sThumbLX, PS3_GAMEPAD_LEFT_THUMB_DEADZONE);
			break;
		}
		case E_GamePadButtons::LeftThumbY:
		{
			*pAnalogState = map_stick_value(m_context.m_state.sThumbLY, PS3_GAMEPAD_LEFT_THUMB_DEADZONE);
			break;
		}
		case E_GamePadButtons::RightThumbX:
		{
			*pAnalogState = map_stick_value(m_context.m_state.sThumbRX, PS3_GAMEPAD_RIGHT_THUMB_DEADZONE);
			break;
		}
		case E_GamePadButtons::RightThumbY:
		{
			*pAnalogState = map_stick_value(m_context.m_state.sThumbRY, PS3_GAMEPAD_RIGHT_THUMB_DEADZONE);
			break;
		}
}
}
#endif //IsInputPS3()