// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_HIDAPI_PS3_INPUTAPIPS3_H
#define FXDENGINE_INPUT_HIDAPI_PS3_INPUTAPIPS3_H

#include "FXDEngine/Input/Types.h"

#if IsInputPS3()
#include "FXDEngine/Input/InputApi.h"
#include "FXDEngine/Input/HIDAPI/PS3/PS3.h"

namespace FXD
{
	namespace Input
	{
		namespace HidApi
		{
			// ------
			// IInputApiPS3
			// -
			// ------
			class IInputApiPS3 FINAL : public Input::IInputApi
			{
			public:
				IInputApiPS3(void);
				~IInputApiPS3(void);

				bool create_api(void) FINAL;
				bool release_api(void) FINAL;

			private:
				void _process(FXD::F32 dt) FINAL;
			};

		} //namespace HidApi
	} //namespace Input
} //namespace FXD

#endif //IsInputPS3()
#endif //FXDENGINE_INPUT_HIDAPI_PS3_INPUTAPIPS3_H