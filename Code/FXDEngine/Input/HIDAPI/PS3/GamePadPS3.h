// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_HIDAPI_PS3_GAMEPADPS3_H
#define FXDENGINE_INPUT_HIDAPI_PS3_GAMEPADPS3_H

#include "FXDEngine/Input/Types.h"

#if IsInputPS3()
#include "FXDEngine/Input/Events.h"
#include "FXDEngine/Input/InputDevice.h"
#include "FXDEngine/Input/HIDAPI/HidApi.h"
#include "FXDEngine/Input/HIDAPI/PS3/PS3.h"

namespace FXD
{
	namespace Input
	{
		namespace HidApi
		{
			// ------
			// IGamePadPS3
			// -
			// ------
			class IGamePadPS3 FINAL : public Input::IInputDevice
			{
			public:
				struct PS3PacketState
				{
					FXD::U8 sThumbLX;
					FXD::U8 sThumbLY;
					FXD::U8 sThumbRX;
					FXD::U8 sThumbRY;
					FXD::U8 nButtons[2];
					FXD::U8 nCounterGuide;
					FXD::U8 nLTrigger;
					FXD::U8 nRTrigger;
					FXD::U8 _rgucPad0[3];
					FXD::S16 sGyroX;
					FXD::S16 sGyroY;
					FXD::S16 sGyroZ;
					FXD::S16 sAccelX;
					FXD::S16 sAccelY;
					FXD::S16 sAccelZ;
					FXD::U8 _rgucPad1[5];
					FXD::U8 nBatteryLevel;
					FXD::U8 _rgucPad2[4];
					FXD::U8 ucTrackpadCounter1;
					FXD::U8 rgucTrackpadData1[3];
					FXD::U8 ucTrackpadCounter2;
					FXD::U8 rgucTrackpadData2[3];
				};

				struct DS4EffectsState
				{
					FXD::U8 ucRumbleRight;
					FXD::U8 ucRumbleLeft;
					FXD::U8 ucLedRed;
					FXD::U8 ucLedGreen;
					FXD::U8 ucLedBlue;
					FXD::U8 ucLedDelayOn;
					FXD::U8 ucLedDelayOff;
					FXD::U8 _rgucPad0[8];
					FXD::U8 ucVolumeLeft;
					FXD::U8 ucVolumeRight;
					FXD::U8 ucVolumeMic;
					FXD::U8 ucVolumeSpeaker;
				};

				struct PS3Context
				{
					PS3Context(HidApi::HidApiDevice* pHDevice, const FXD::S32 nPort);

					HidApi::HidApiDevice* m_pHDevice;
					bool m_bConnected;
					bool m_bColLedDirty;
					bool m_bDongle;
					bool m_bBluetooth;
					bool m_bAudioSupported;
					bool m_bRumbleSupported;
					FXD::U8 m_nVolume;
					FXD::U32 last_volume_check;
					FXD::U32 m_nPortIndex;
					Core::ColourRGB m_colLed;
					PS3PacketState m_state;
				};

			public:
				IGamePadPS3(HidApi::HidApiDevice* pHDevice);
				~IGamePadPS3(void);

				void set_colour(const Core::ColourRGB& col);

				bool is_connected(void) const FINAL;
				bool is_gamepad_available(void) const FINAL;
				bool supports_rumble(void) const FINAL;

				Core::String8 get_config_name(void) const FINAL;
				Core::String8 get_device_name(void) const FINAL;
				Input::E_DeviceType get_device_type(void) const FINAL;

			protected:
				void _update(FXD::F32 dt) FINAL;

				const Widget::WIDGET_DESC* _get_widget_defs(void) const FINAL;
				void _get_widget_state(FXD::U16 nID, void* pState) const FINAL;

			private:
				PS3Context m_context;
				const FXD::Job::Handler< Input::OnControllerButton >* OnControllerButton;
				const FXD::Job::Handler< Input::OnControllerAnalog >* OnControllerAnalog;
			};

		} //namespace HidApi
	} //namespace Input
} //namespace FXD
#endif //IsInputPS3()
#endif //FXDENGINE_INPUT_HIDAPI_PS3_GAMEPADPS3_H