// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_HIDAPI_HIDUSBVENDORS_H
#define FXDENGINE_INPUT_HIDAPI_HIDUSBVENDORS_H

#include "FXDEngine/Input/Types.h"

#if IsInputHIDAPI()
namespace FXD
{
	namespace Input
	{
		namespace HidApi
		{
			static CONSTEXPR const FXD::U16 kNintendoSwitchHoriWirelessPad = 0x00f6;
			static CONSTEXPR const FXD::U16 kNintendoSwitchJoyconL = 0x2006;
			static CONSTEXPR const FXD::U16 kNintendoSwitchJoyconR = 0x2007;
			static CONSTEXPR const FXD::U16 kSonyRazerPanthera = 0x0401;
			static CONSTEXPR const FXD::U16 kSonyRazerPantheraEvo = 0x1008;
			static CONSTEXPR const FXD::U16 kSonyDS4 = 0x05c4;
			static CONSTEXPR const FXD::U16 kSonyDS4Dongle = 0x0ba0;
			static CONSTEXPR const FXD::U16 kSonyDS4Slim = 0x09cc;

			enum class E_USB_Vendor : FXD::U16
			{
				Atmel = 0x03eb,
				BigBenInteractive = 0x146b,
				Elecom = 0x056e,
				Harmonix = 0x1bad,
				HoneyBee = 0x12ab,
				Hori = 0x0f0d,
				Hyperkin = 0x2e24,
				JessTechnology = 0x0f30,
				Joytech = 0x162e,
				LakeviewResearch = 0x0925,
				Logitech = 0x046D,
				LogitechEurope = 0x06a3,
				MadCatz = 0x0738,
				Microsoft = 0x045e,
				Nacon = 0x11c9,
				Nintendo = 0x057e,
				Numark = 0x15e4,
				Nvidia = 0x0955,
				PansignalTechnology = 0x0e8f,
				PDP = 0x0e6f,
				PersonalCommunicationSystems = 0x0810,
				RazerOnza = 0x1689,
				RazerSabertooth = 0x1532,
				RedOctane = 0x1430,
				//Saitek = 0x06a3,
				ShenzhenLongshengweiTechnology = 0x0079,
				SinoLite = 0x1345,
				Sony = 0x054c,
				SteelSeries = 0x1038,
				Sysgration = 0x05b8,
				ThrustMaster = 0x044f,
				ThrustMasterInc = 0x24c6,
				Valve = 0x28de,
				Zeroplus = 0x0C12,

				Unknown = 0xffff
			};

			static CONSTEXPR const HidApi::E_USB_Vendor kUSBVendors[] =
			{
				E_USB_Vendor::Atmel,							// 0x03eb
				E_USB_Vendor::BigBenInteractive,				// BigBenInteractive
				E_USB_Vendor::Elecom,							// Elecom
				E_USB_Vendor::Harmonix,							// Harmonix
				E_USB_Vendor::HoneyBee,							// HoneyBee
				E_USB_Vendor::Hori,								// Hori
				E_USB_Vendor::Hyperkin,							// Hyperkin
				E_USB_Vendor::JessTechnology,					// JessTechnology
				E_USB_Vendor::Joytech,							// Joytech
				E_USB_Vendor::LakeviewResearch,					// LakeviewResearch
				E_USB_Vendor::Logitech,							// Logitech
				E_USB_Vendor::LogitechEurope,					// LogitechEurope
				E_USB_Vendor::MadCatz,							// MadCatz
				E_USB_Vendor::Microsoft,						// Microsoft
				E_USB_Vendor::Nacon,							// Nacon
				E_USB_Vendor::Nintendo,							// Nintendo
				E_USB_Vendor::Numark,							// Numark
				E_USB_Vendor::Nvidia,							// Nvidia
				E_USB_Vendor::PansignalTechnology,				// PansignalTechnology
				E_USB_Vendor::PDP,								// PDP
				E_USB_Vendor::PersonalCommunicationSystems,		// PersonalCommunicationSystems
				E_USB_Vendor::RazerOnza,						// RazerOnza
				E_USB_Vendor::RazerSabertooth,					// RazerSabertooth
				E_USB_Vendor::RedOctane,						// RedOctane
				//E_USB_Vendor::Saitek,							// Saitek
				E_USB_Vendor::ShenzhenLongshengweiTechnology,	// ShenzhenLongshengweiTechnology
				E_USB_Vendor::SinoLite,							// SinoLite
				E_USB_Vendor::Sony,								// Sony
				E_USB_Vendor::SteelSeries,						// SteelSeries
				E_USB_Vendor::Sysgration,						// Sysgration
				E_USB_Vendor::ThrustMaster,						// ThrustMaster
				E_USB_Vendor::ThrustMasterInc,					// ThrustMasterInc
				E_USB_Vendor::Valve,							// Valve
				E_USB_Vendor::Zeroplus,							// Zeroplus
			};
		} //namespace HidApi
	} //namespace Input
} //namespace FXD
#endif //IsInputHIDAPI()
#endif //FXDENGINE_INPUT_HIDAPI_HIDUSBVENDORS_H