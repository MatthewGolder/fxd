// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_HIDAPI_HIDAPIMANAGER_H
#define FXDENGINE_INPUT_HIDAPI_HIDAPIMANAGER_H

#include "FXDEngine/Input/Types.h"

#if IsInputHIDAPI()
#include "FXDEngine/Container/Map.h"
#include "FXDEngine/Input/HIDAPI/HidApi.h"
#include "FXDEngine/Job/Future.h"

namespace FXD
{
	namespace Input
	{
		namespace HidApi
		{
			struct HidApiRumbleRequest
			{
			public:
				#define SIZEHERE (2 * USB_PACKET_LENGTH)

				HidApiRumbleRequest(const HidApi::HidApiDevice* pDevice, const FXD::U8* pData, FXD::U32 nRequestSize)
					: m_pDevice(pDevice)
					, m_nRequestSize(nRequestSize)
				{
					Memory::MemCopy(m_pData, pData, m_nRequestSize);
				}

			public:
				const HidApi::HidApiDevice* m_pDevice;
				FXD::U8 m_pData[SIZEHERE]; // need enough space for the biggest report: dualshock4 is 78 bytes
				FXD::U32 m_nRequestSize;
			};


			// ------
			// IHidApiManager
			// -
			// ------
			class IHidApiManager FINAL : public Core::NonCopyable
			{
			public:
				friend class IHidApiJPThread;

			public:
				IHidApiManager(void);
				~IHidApiManager(void);

				Job::Future< bool > create_system(void);
				Job::Future< bool > release_system(void);

				FXD::S32 read_input(const HidApi::HidApiDevice* pDevice, FXD::U8* pBuf, FXD::U8 nBufSize, FXD::S32 nTimeout);
				bool submit_rumble_request(const HidApi::HidApiDevice* pDevice, const Memory::MemHandle& memHandle);
				bool read_feature_report(const HidApi::HidApiDevice* pDevice, FXD::U8 nReportID, Memory::MemHandle& memHandle);

			private:
				bool _create_system(void);
				bool _release_system(void);

				void _process(FXD::F32 dt);
				void _process_requests(FXD::F32 dt);

			public:
				FXD::S32 WritePacket(const HidApi::HidApiDevice* pDevice, FXD::U8* pBuf, FXD::U32 nBufSize);
				bool _process_write(const HidApi::HidApiDevice* pDevice, FXD::U8* pBuf, FXD::U32 nBufSize, FXD::S32 nCommandId, FXD::S32 nTimeout, FXD::S32 nRetries = 5, bool bWaitForReply = false);
				
				/*
				FXD::S32 write_output(const HidApi::HidApiDevice* pHDevice, const Memory::MemHandle& memHandle);
				*/

			private:
				Container::Map< const HidApi::HidApiDevice*, HidApi::HidApiRumbleRequest > m_rumbleRequests;
			};
		} //namespace HidApi
	} //namespace Input
} //namespace FXD
#endif //IsInputHIDAPI()
#endif //FXDENGINE_INPUT_HIDAPI_HIDAPIMANAGER_H