// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_HIDAPI_SWITCH_GAMEPADSWITCH_H
#define FXDENGINE_INPUT_HIDAPI_SWITCH_GAMEPADSWITCH_H

#include "FXDEngine/Input/Types.h"

#if IsInputSwitch()
#include "FXDEngine/Input/Events.h"
#include "FXDEngine/Input/InputDevice.h"
#include "FXDEngine/Input/HIDAPI/HidApi.h"

namespace FXD
{
	namespace Input
	{
		namespace HidApi
		{
			// ------
			// IGamePadSwitch
			// -
			// ------
			class IGamePadSwitch FINAL : public Input::IInputDevice
			{
			public:
				struct SwitchStickCalibration
				{
					struct
					{
						FXD::S16 sCenter;
						FXD::S16 sMin;
						FXD::S16 sMax;
					} axis[2];
				};

				struct SwitchStickExtents
				{
					struct
					{
						FXD::S16 sMin;
						FXD::S16 sMax;
					} axis[2];
				};

				struct SwitchRumbleData
				{
					SwitchRumbleData(void);

					void set_neutral(void);

					FXD::U8 data2[4];
					/*
					FXD::U8 usHighFreq;
					FXD::U8 ucHighFreqAmp;
					FXD::U8 ucLowFreq;
					FXD::U8 usLowFreqAmp;
					*/
				};

				struct SwitchPacketState
				{
					FXD::U16 nButtonsL;
					FXD::U16 nButtonsR;
					FXD::S16 nThumbLX;
					FXD::S16 nThumbLY;
					FXD::S16 nThumbRX;
					FXD::S16 nThumbRY;
					FXD::S32 nBatteryLevel;
				};

				struct SwitchContext
				{
					SwitchContext(HidApi::HidApiDevice* pHDevice, const FXD::S32 nPort);

					HidApiDevice* m_pHDevice;
					FXD::U8 m_nTypeHandle;
					FXD::U32 m_nPortIndex;
					bool m_bConnected;
					bool m_bBluetooth;
					bool m_bHasHomeLED;
					mutable FXD::U8 m_nCommandNumber;
					Core::ColourRGBA m_colBody;
					Core::ColourRGBA m_colButtons;
					Core::ColourRGBA m_colGripL;
					Core::ColourRGBA m_colGripR;
					SwitchRumbleData m_jcRumbleData[2];
					SwitchStickCalibration m_jcStickCalData[2];
					SwitchStickExtents m_jcStickExtents[2];
					SwitchPacketState m_state;
				};

			public:
				IGamePadSwitch(HidApi::HidApiDevice* pHDevice);
				~IGamePadSwitch(void);

				bool is_connected(void) const FINAL;
				bool is_gamepad_available(void) const FINAL;
				bool supports_rumble(void) const FINAL;

				Core::String8 get_config_name(void) const FINAL;
				Core::String8 get_device_name(void) const FINAL;
				Input::E_DeviceType get_device_type(void) const FINAL;

			protected:
				void _update(FXD::F32 dt) FINAL;

				const Widget::WIDGET_DESC* _get_widget_defs(void) const FINAL;
				void _get_widget_state(FXD::U16 nID, void* pState) const FINAL;

			private:
				SwitchContext m_context;
				const FXD::Job::Handler< Input::OnControllerButton >* OnControllerButton;
				const FXD::Job::Handler< Input::OnControllerAnalog >* OnControllerAnalog;
			};

		} //namespace HidApi
	} //namespace Input
} //namespace FXD

#endif //IsInputSwitch()
#endif //FXDENGINE_INPUT_HIDAPI_SWITCH_GAMEPADSWITCH_H