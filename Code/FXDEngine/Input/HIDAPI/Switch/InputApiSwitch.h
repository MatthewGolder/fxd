// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_HIDAPI_SWITCH_INPUTAPISWITCH_H
#define FXDENGINE_INPUT_HIDAPI_SWITCH_INPUTAPISWITCH_H

#include "FXDEngine/Input/Types.h"

#if IsInputSwitch()
#include "FXDEngine/Container/Vector.h"
#include "FXDEngine/Input/InputApi.h"
#include "FXDEngine/Input/HIDAPI/Switch/Switch.h"

namespace FXD
{
	namespace Input
	{
		namespace HidApi
		{
			// ------
			// IInputApiSwitch
			// -
			// ------
			class IInputApiSwitch FINAL : public Input::IInputApi
			{
			public:
				IInputApiSwitch(void);
				~IInputApiSwitch(void);

				bool create_api(void) FINAL;
				bool release_api(void) FINAL;

			private:
				void _process(FXD::F32 dt) FINAL;

			private:
				Container::Vector< Input::InputDevice > m_detectedDevices;
			};

		} //namespace HidApi
	} //namespace Input
} //namespace FXD

#endif //IsInputSwitch()
#endif //FXDENGINE_INPUT_HIDAPI_SWITCH_INPUTAPISWITCH_H