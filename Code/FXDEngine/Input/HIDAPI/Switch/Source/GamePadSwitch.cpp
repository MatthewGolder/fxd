// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputSwitch()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Input/Mapping.h"
#include "FXDEngine/Input/HIDAPI/HidApiDevice.h"
#include "FXDEngine/Input/HIDAPI/HidApiJPThread.h"
#include "FXDEngine/Input/HIDAPI/HidApiManager.h"
#include "FXDEngine/Input/HIDAPI/Switch/GamePadSwitch.h"
#include "FXDEngine/Job/EventHandler.h"
#include "FXDEngine/Math/Math.h"

using namespace FXD;
using namespace Input;
using namespace HidApi;

namespace
{
	#define SWITCH_GAMEPAD_LEFT_THUMB_DEADZONE	7849
	#define SWITCH_GAMEPAD_RIGHT_THUMB_DEADZONE	8689
	#define SWITCH_GAMEPAD_DPAD_UP					0x0002
	#define SWITCH_GAMEPAD_DPAD_DOWN					0x0001
	#define SWITCH_GAMEPAD_DPAD_LEFT					0x0008
	#define SWITCH_GAMEPAD_DPAD_RIGHT				0x0004
	#define SWITCH_GAMEPAD_START						0x0200
	#define SWITCH_GAMEPAD_BACK						0x0100
	#define SWITCH_GAMEPAD_CAPTURE					0x2000
	#define SWITCH_GAMEPAD_GUIDE						0x1000
	#define SWITCH_GAMEPAD_LEFT_TRIGGER				0x0080
	#define SWITCH_GAMEPAD_RIGHT_TRIGGER			0x0080
	#define SWITCH_GAMEPAD_LEFT_THUMB				0x0800
	#define SWITCH_GAMEPAD_RIGHT_THUMB				0x0400
	#define SWITCH_GAMEPAD_LEFT_BUMPER				0x0040
	#define SWITCH_GAMEPAD_RIGHT_BUMPER				0x0040
	#define SWITCH_GAMEPAD_A							0x0008
	#define SWITCH_GAMEPAD_B							0x0004
	#define SWITCH_GAMEPAD_X							0x0002
	#define SWITCH_GAMEPAD_Y							0x0001
	#define SWITCH_GAMEPAD_SL							0x0020
	#define SWITCH_GAMEPAD_SR							0x0010

	CONSTEXPR FXD::U8 kSwitchOutputPacketDataLength = 49;
	CONSTEXPR FXD::U8 kSwitchMaxOutputPacketLength = 64;
	CONSTEXPR FXD::U8 kSwitchBluetoothPacketLength = kSwitchOutputPacketDataLength;
	CONSTEXPR FXD::U8 kSwitchUSBPacketLength = kSwitchMaxOutputPacketLength;

	CONSTEXPR FXD::U32 kSPIFactoryDeviceTypeOffset = 0x6012;
	CONSTEXPR FXD::U32 kSPIFactoryStickCalibrationOffset = 0x603D;
	CONSTEXPR FXD::U32 kSPIColourOffset = 0x6050;
	CONSTEXPR FXD::U32 kSPIUserStickCalibrationStartOffset = 0x8010;

	enum ESwitchDevice
	{
		k_ESwitchDevice_JCL = 1,
		k_ESwitchDevice_JCR = 2,
		k_ESwitchDevice_PRO = 3,
		k_Count,
		k_Unknown = 0xff
	};

	enum class ESwitchInputReportID : FXD::U8
	{
		SubcommandReply = 0x21,
		FullControllerState = 0x30,
		SimpleControllerState = 0x3F,
		CommandAck = 0x81,
	};

	enum class ESwitchOutputReportID : FXD::U8
	{
		RumbleAndSubcommand = 0x01,
		Rumble = 0x10,
		Proprietary = 0x80,
	};

	enum ESwitchSubcommandIDs
	{
		k_eSwitchSubcommandID_BluetoothManualPair = 0x01,
		k_eSwitchSubcommandID_RequestDeviceInfo = 0x02,
		k_eSwitchSubcommandID_InputReportModeWrite = 0x03,
		k_eSwitchSubcommandID_HCIStateWrite = 0x06,
		k_eSwitchSubcommandID_SPIFlashRead = 0x10,
		k_eSwitchSubcommandID_SPIFlashWrite = 0x11,
		k_eSwitchSubcommandID_PlayerLightsWrite = 0x30,
		k_eSwitchSubcommandID_PlayerLightsRead = 0x31,
		k_eSwitchSubcommandID_HomeLightWrite = 0x38,
		k_eSwitchSubcommandID_EnableIMU = 0x40,
		k_eSwitchSubcommandID_IMUSensitivityWrite = 0x41,
		k_eSwitchSubcommandID_EnableVibration = 0x48,
	};

	enum class ESwitchProprietaryCommandID : FXD::U8
	{
		Handshake = 0x02,
		HighSpeed = 0x03,
		ForceUSB = 0x04,
		ClearUSB = 0x05,
		ResetMCU = 0x06,
	};

#pragma pack(1)
	struct SwitchCommonOutputPacket_t
	{
		FXD::U8 ucPacketType;
		FXD::U8 ucPacketNumber;
		IGamePadSwitch::SwitchRumbleData rumbleData[2];
	};

	struct SwitchSubcommandOutputPacket_t
	{
		SwitchCommonOutputPacket_t commonData;

		FXD::U8 ucSubcommandID;
		FXD::U8 rgucSubcommandData[kSwitchOutputPacketDataLength - sizeof(SwitchCommonOutputPacket_t) - 1];
	};

	struct SwitchSPIOpData_t
	{
		FXD::U32 unAddress;
		FXD::U8 ucLength;
	};

	struct SwitchControllerStatePacket_t
	{
		FXD::U8 nCounter;
		FXD::U8 nBatteryConnection;
		FXD::U8 pButtons[3];
		FXD::U8 pJoystickLeft[3];
		FXD::U8 pJoystickRight[3];
		FXD::U8 nVibrationCode;
	};

	struct SwitchSubcommandInputPacket_t
	{
		SwitchControllerStatePacket_t m_controllerState;

		FXD::U8 ucSubcommandAck;
		FXD::U8 ucSubcommandID;

		#define k_unSubcommandDataBytes 35
		union
		{
			FXD::U8 rgucSubcommandData[k_unSubcommandDataBytes];
			struct
			{
				SwitchSPIOpData_t opData;
				FXD::U8 rgucReadData[k_unSubcommandDataBytes - sizeof(SwitchSPIOpData_t)];
			} spiReadData;
		};
	};

	struct SwitchProprietaryOutputPacket_t
	{
		FXD::U8 ucPacketType;
		FXD::U8 ucProprietaryID;
		FXD::U8 rgucProprietaryData[kSwitchOutputPacketDataLength - 1 - 1];
	};
#pragma pack()

	FXD::S32 WritePacket(const IGamePadSwitch::SwitchContext& context, FXD::U8* pData, FXD::U8 nLen)
	{
		if (nLen > kSwitchOutputPacketDataLength)
		{
			return -1;
		}

		FXD::U8 pInBuf[kSwitchMaxOutputPacketLength];
		const FXD::U8 nWriteSize = context.m_bBluetooth ? kSwitchBluetoothPacketLength : kSwitchUSBPacketLength;

		if (nLen < nWriteSize)
		{
			Memory::MemCopy(pInBuf, pData, nLen);
			Memory::MemSet(pInBuf + nLen, 0, nWriteSize - nLen);
			pData = pInBuf;
			nLen = nWriteSize;
		}

		FXD::S32 nRet = FXD::FXDHidApi()->hidApiManager()->WritePacket(context.m_pHDevice, pData, nLen);
		return nRet;
	}

	FXD::S32 ReadSubcommand(const IGamePadSwitch::SwitchContext& context, ESwitchSubcommandIDs eCommandID, SwitchSubcommandInputPacket_t* pOut)
	{
		FXD::U8 pBuffer[kSwitchMaxOutputPacketLength];
		Memory::MemZero_T(pBuffer);

		FXD::S32 nRead = 0;
		FXD::S32 nTicks = 0;
		FXD::F32 fElapsed = 0.0f;
		Core::Timer time(true);
		while ((nRead = FXD::FXDHidApi()->hidApiManager()->read_input(context.m_pHDevice, pBuffer, sizeof(pBuffer), 0)) != -1)
		{
			nTicks++;
			fElapsed = time.elapsed_time();
			if (nRead > 0)
			{
				if (pBuffer[0] == FXD::STD::to_underlying(ESwitchInputReportID::SubcommandReply))
				{
					SwitchSubcommandInputPacket_t* pReply = (SwitchSubcommandInputPacket_t*)&pBuffer[1];
					if (pReply->ucSubcommandID == eCommandID && (pReply->ucSubcommandAck & FXD::STD::to_underlying(ESwitchOutputReportID::Proprietary)))
					{
						Memory::MemCopy(pOut, pReply, sizeof(SwitchSubcommandInputPacket_t));
						return nRead;
					}
				}
			}
			else
			{
				Thread::Funcs::Sleep(1);
			}

			if (time.elapsed_time() > 0.3f)
			{
				break;
			}
		}
		return nRead;
	}

	SwitchSubcommandOutputPacket_t ConstructSubcommand(const IGamePadSwitch::SwitchContext& context, ESwitchSubcommandIDs eCommandID, FXD::U8* pData, FXD::U16 nLen)
	{
		SwitchSubcommandOutputPacket_t packet;
		Memory::MemZero_T(packet);

		packet.commonData.ucPacketType = FXD::STD::to_underlying(ESwitchOutputReportID::RumbleAndSubcommand);
		packet.commonData.ucPacketNumber = context.m_nCommandNumber;
		Memory::MemCopy(&packet.commonData.rumbleData, context.m_jcRumbleData, sizeof(context.m_jcRumbleData));

		packet.ucSubcommandID = eCommandID;

		Memory::MemCopy(packet.rgucSubcommandData, pData, nLen);

		context.m_nCommandNumber = (context.m_nCommandNumber + 1) & 0xF;

		return packet;
	}

	FXD::S32 WriteSubcommand(const IGamePadSwitch::SwitchContext& context, ESwitchSubcommandIDs eCommandID, FXD::U8* pData, FXD::U16 nLen)
	{
		SwitchSubcommandOutputPacket_t commandPacket = ConstructSubcommand(context, eCommandID, pData, nLen);

		FXD::S32 nRet = 0;
		if ((nRet = WritePacket(context, (FXD::U8*)&commandPacket, sizeof(commandPacket))) == -1)
		{
			return nRet;
		}
		return nRet;
	}

	bool ReadProprietaryReply(const IGamePadSwitch::SwitchContext& context, ESwitchProprietaryCommandID eExpectedID, FXD::U8* pData, FXD::U16 nLen)
	{
		FXD::U8 pBuffer[kSwitchMaxOutputPacketLength];
		Memory::MemZero_T(pBuffer);

		FXD::S32 nRead = 0;
		FXD::S32 nTicks = 0;
		FXD::F32 fElapsed = 0.0f;
		Core::Timer time(true);
		while ((nRead = FXD::FXDHidApi()->hidApiManager()->read_input(context.m_pHDevice, pBuffer, sizeof(pBuffer), 0)) != -1)
		{
			nTicks++;
			fElapsed = time.elapsed_time();
			if (nRead > 0)
			{
				if ((pBuffer[0] == FXD::STD::to_underlying(ESwitchInputReportID::CommandAck)) &&
					 (pBuffer[0] == FXD::STD::to_underlying(eExpectedID)))
				{
					TODO_WARN("");
					//Memory::MemCopy(pOut, pReply, sizeof(SwitchSubcommandInputPacket_t));
					return nRead;
				}
			}
			else
			{
				Thread::Funcs::Sleep(1);
			}

			if (time.elapsed_time() > 0.3f)
			{
				break;
			}
		}
		return nRead;
	}

	bool WriteProprietary(const IGamePadSwitch::SwitchContext& context, ESwitchProprietaryCommandID eExpectedID, FXD::U8* pData, FXD::U16 nLen, bool bWait)
	{
		FXD::S32 nRetries = 5;
		while (nRetries--)
		{
			SwitchProprietaryOutputPacket_t packet;
			packet.ucPacketType = FXD::STD::to_underlying(ESwitchOutputReportID::Proprietary);
			packet.ucProprietaryID = FXD::STD::to_underlying(eExpectedID);
			if ((pData == nullptr && nLen > 0) || (nLen > sizeof(packet.rgucProprietaryData)))
			{
				return false;
			}
			if (pData != nullptr)
			{
				Memory::MemCopy(packet.rgucProprietaryData, pData, nLen);
			}

			FXD::S32 nRet = 0;
			if ((nRet = WritePacket(context, (FXD::U8*)&packet, sizeof(packet))) == -1)
			{
				continue;
			}

			if (bWait == false || ReadProprietaryReply(context, eExpectedID, pData, nLen))
			{
				return true;
			}
		}
		return false;
	}

	FXD::S32 switch_get_spi_data(const IGamePadSwitch::SwitchContext& context, FXD::U32 nSpiOffset, FXD::U16 nLen, SwitchSubcommandInputPacket_t* pOutData)
	{
		SwitchSPIOpData_t readParams;
		readParams.unAddress = nSpiOffset;
		readParams.ucLength = nLen;

		FXD::S32 nRetries = 5;
		while (nRetries--)
		{
			FXD::S32 nRet = 0;
			if ((nRet = WriteSubcommand(context, k_eSwitchSubcommandID_SPIFlashRead, (FXD::U8*)&readParams, sizeof(readParams))) == -1)
			{
				continue;
			}
			if ((nRet = ReadSubcommand(context, k_eSwitchSubcommandID_SPIFlashRead, (SwitchSubcommandInputPacket_t*)pOutData)) == -1)
			{
				continue;
			}
			return nRet;
		}
		return -1;
	}

	FXD::S32 switch_get_dev_info(IGamePadSwitch::SwitchContext& context)
	{
		SwitchSubcommandInputPacket_t reply;
		FXD::S32 nRet = 0;
		if ((nRet = switch_get_spi_data(context, kSPIFactoryDeviceTypeOffset, 1, &reply)) == -1)
		{
			return nRet;
		}

		context.m_nTypeHandle = reply.spiReadData.rgucReadData[0];

		return nRet;
	}

	FXD::S32 switch_get_colour(IGamePadSwitch::SwitchContext& context)
	{
		SwitchSubcommandInputPacket_t reply;
		FXD::S32 nRet = 0;
		if ((nRet = switch_get_spi_data(context, kSPIColourOffset, 12, &reply)) == -1)
		{
			return nRet;
		}

		context.m_colBody = Core::ColourRGBA::from_hex(
			reply.spiReadData.rgucReadData[0],
			reply.spiReadData.rgucReadData[1],
			reply.spiReadData.rgucReadData[2],
			(Math::NumericLimits< FXD::U8 > ::max()));

		context.m_colButtons = Core::ColourRGBA::from_hex(
			reply.spiReadData.rgucReadData[3],
			reply.spiReadData.rgucReadData[4],
			reply.spiReadData.rgucReadData[5],
			(Math::NumericLimits< FXD::U8 > ::max()));

		context.m_colGripL = Core::ColourRGBA::from_hex(
			reply.spiReadData.rgucReadData[6],
			reply.spiReadData.rgucReadData[7],
			reply.spiReadData.rgucReadData[8],
			(Math::NumericLimits< FXD::U8 > ::max()));

		context.m_colGripR = Core::ColourRGBA::from_hex(
			reply.spiReadData.rgucReadData[9],
			reply.spiReadData.rgucReadData[10],
			reply.spiReadData.rgucReadData[11],
			(Math::NumericLimits< FXD::U8 > ::max()));
		return nRet;
	}

	FXD::S32 switch_get_stick(IGamePadSwitch::SwitchContext& context, ESwitchInputReportID eSwitchInputReportID)
	{
		SwitchSubcommandInputPacket_t reply;
		FXD::S32 nRet = 0;
		if ((nRet = switch_get_spi_data(context, kSPIFactoryStickCalibrationOffset, 18, &reply)) == -1)
		{
			return nRet;
		}

		const FXD::U8* pStickCal = reply.spiReadData.rgucReadData;

		// Left stick
		context.m_jcStickCalData[0].axis[0].sMax = ((pStickCal[1] << 8) & 0xF00) | pStickCal[0];		// X Axis max above center
		context.m_jcStickCalData[0].axis[1].sMax = (pStickCal[2] << 4) | (pStickCal[1] >> 4);			// Y Axis max above center
		context.m_jcStickCalData[0].axis[0].sCenter = ((pStickCal[4] << 8) & 0xF00) | pStickCal[3];	// X Axis center
		context.m_jcStickCalData[0].axis[1].sCenter = (pStickCal[5] << 4) | (pStickCal[4] >> 4);		// Y Axis center
		context.m_jcStickCalData[0].axis[0].sMin = ((pStickCal[7] << 8) & 0xF00) | pStickCal[6];		// X Axis min below center
		context.m_jcStickCalData[0].axis[1].sMin = (pStickCal[8] << 4) | (pStickCal[7] >> 4);			// Y Axis min below center

		// Right stick
		context.m_jcStickCalData[1].axis[0].sCenter = ((pStickCal[10] << 8) & 0xF00) | pStickCal[9];	// X Axis center
		context.m_jcStickCalData[1].axis[1].sCenter = (pStickCal[11] << 4) | (pStickCal[10] >> 4);	// Y Axis center
		context.m_jcStickCalData[1].axis[0].sMin = ((pStickCal[13] << 8) & 0xF00) | pStickCal[12];	// X Axis min below center
		context.m_jcStickCalData[1].axis[1].sMin = (pStickCal[14] << 4) | (pStickCal[13] >> 4);		// Y Axis min below center
		context.m_jcStickCalData[1].axis[0].sMax = ((pStickCal[16] << 8) & 0xF00) | pStickCal[15];	// X Axis max above center
		context.m_jcStickCalData[1].axis[1].sMax = (pStickCal[17] << 4) | (pStickCal[16] >> 4);		// Y Axis max above center

		for (FXD::U32 nStick = 0; nStick < 2; ++nStick)
		{
			for (FXD::U32 nAxis = 0; nAxis < 2; ++nAxis)
			{
				if (context.m_jcStickCalData[nStick].axis[nAxis].sCenter == 0xFFF)
				{
					context.m_jcStickCalData[nStick].axis[nAxis].sCenter = 0;
				}
				if (context.m_jcStickCalData[nStick].axis[nAxis].sMax == 0xFFF)
				{
					context.m_jcStickCalData[nStick].axis[nAxis].sMax = 0;
				}
				if (context.m_jcStickCalData[nStick].axis[nAxis].sMin == 0xFFF)
				{
					context.m_jcStickCalData[nStick].axis[nAxis].sMin = 0;
				}
			}
		}

		if (eSwitchInputReportID == ESwitchInputReportID::SimpleControllerState)
		{
			for (FXD::U32 nStick = 0; nStick < 2; ++nStick)
			{
				for (FXD::U32 nAxis = 0; nAxis < 2; ++nAxis)
				{
					context.m_jcStickExtents[nStick].axis[nAxis].sMin = (FXD::S16)(Math::NumericLimits< FXD::S16 >::min() * 0.5f);
					context.m_jcStickExtents[nStick].axis[nAxis].sMax = (FXD::S16)(Math::NumericLimits< FXD::S16 >::max() * 0.5f);
				}
			}
		}
		else
		{
			for (FXD::U32 nStick = 0; nStick < 2; ++nStick)
			{
				for (FXD::U32 nAxis = 0; nAxis < 2; ++nAxis)
				{
					context.m_jcStickExtents[nStick].axis[nAxis].sMin = -(FXD::S16)(context.m_jcStickCalData[nStick].axis[nAxis].sMin * 0.7f);
					context.m_jcStickExtents[nStick].axis[nAxis].sMax = (FXD::S16)(context.m_jcStickCalData[nStick].axis[nAxis].sMax * 0.7f);
				}
			}
		}
		return nRet;
	}

	FXD::S32 switch_set_vibration_enabled(const IGamePadSwitch::SwitchContext& context, FXD::U8 nEnabled)
	{
		FXD::S32 nRet = 0;
		if ((nRet = WriteSubcommand(context, k_eSwitchSubcommandID_EnableVibration, &nEnabled, sizeof(nEnabled))) == -1)
		{
			return nRet;
		}
		return nRet;
	}

	FXD::S32 switch_set_input_mode(const IGamePadSwitch::SwitchContext& context, ESwitchInputReportID eSwitchInputReportID)
	{
		FXD::U8 nReport = FXD::STD::to_underlying(eSwitchInputReportID);
		FXD::S32 nRet = 0;
		if ((nRet = WriteSubcommand(context, k_eSwitchSubcommandID_InputReportModeWrite, &nReport, sizeof(nReport))) == -1)
		{
			return nRet;
		}
		return nRet;
	}

	FXD::S32 switch_set_home_led(const IGamePadSwitch::SwitchContext& context, FXD::U8 nBrightness)
	{
		FXD::U8 nLedIntensity = 0;
		FXD::U8 rgucBuffer[4];

		if (nBrightness > 0)
		{
			if (nBrightness < 65)
			{
				nLedIntensity = (nBrightness + 5) / 10;
			}
			else
			{
				nLedIntensity = (FXD::U8)Math::Ceil(0xF * Math::PowF((FXD::F32)nBrightness / 100.f, 2.13f));
			}
		}

		rgucBuffer[0] = (0x0 << 4) | 0x2;							// 0 mini cycles (besides first), cycle duration 8ms
		rgucBuffer[1] = ((nLedIntensity & 0xF) << 4) | 0x0;	// LED start intensity (0x0-0xF), 0 cycles (LED stays on at start intensity after first cycle)
		rgucBuffer[2] = ((nLedIntensity & 0xF) << 4) | 0x0;	// First cycle LED intensity, 0x0 intensity for second cycle
		rgucBuffer[3] = (0x0 << 4) | 0x0;							// 8ms fade transition to first cycle, 8ms first cycle LED duration

		FXD::S32 nRet = 0;
		if ((nRet = WriteSubcommand(context, k_eSwitchSubcommandID_HomeLightWrite, (FXD::U8*)&rgucBuffer, sizeof(rgucBuffer))) == -1)
		{
			return nRet;
		}
		return nRet;
	}

	FXD::S32 switch_set_slot_led(const IGamePadSwitch::SwitchContext& context, FXD::U8 nSlot)
	{
		FXD::U8 nLedData = (1 << nSlot);
		FXD::S32 nRet = 0;
		if ((nRet = WriteSubcommand(context, k_eSwitchSubcommandID_PlayerLightsWrite, &nLedData, sizeof(nLedData))) == -1)
		{
			return nRet;
		}
		return nRet;
	}

	void switch_has_home_led(IGamePadSwitch::SwitchContext& context)
	{
		// The Power A Nintendo Switch Pro controllers don't have a Home LED
		if ((context.m_pHDevice->vendorId() == HidApi::E_USB_Vendor::Unknown) && (context.m_pHDevice->productID() == 0))
		{
			context.m_bHasHomeLED = false;
			return;
		}
		// HORI Wireless Switch Pad
		if ((context.m_pHDevice->vendorId() == HidApi::E_USB_Vendor::Hori) && (context.m_pHDevice->productID() == kNintendoSwitchHoriWirelessPad))
		{
			context.m_bHasHomeLED = false;
			return;
		}
		if ((context.m_pHDevice->vendorId() == HidApi::E_USB_Vendor::Nintendo) && (context.m_pHDevice->productID() == kNintendoSwitchJoyconL))
		{
			context.m_bHasHomeLED = false;
			return;
		}
		context.m_bHasHomeLED = true;
	}

	void switch_has_setup_bluetooth(IGamePadSwitch::SwitchContext& context)
	{
		// We have to send a connection handshake to the controller when communicating over USB before we're able to send it other commands.
		// This command is not supported Sover Bluetooth, so we can use the controller's lack of response as a way to determine if the connection 
		// is over USB or Bluetooth
		if (WriteProprietary(context, ESwitchProprietaryCommandID::Handshake, nullptr, 0, true) == false)
		{
			context.m_bBluetooth = true;
			return;
		}
		if (WriteProprietary(context, ESwitchProprietaryCommandID::HighSpeed, nullptr, 0, true) == false)
		{
			// The 8BitDo M30 and SF30 Pro don't respond to this command, but otherwise work correctly
			// return false;
		}
		if (WriteProprietary(context, ESwitchProprietaryCommandID::Handshake, nullptr, 0, true) == false)
		{
			context.m_bBluetooth = true;
			return;
		}
		context.m_bBluetooth = false;
	}

	FXD::F32 switch_remap(FXD::F32 fVal, FXD::F32 A, FXD::F32 B, FXD::F32 C, FXD::F32 D)
	{
		if (A == B)
		{
			return ((fVal - B) >= 0) ? D : C;
		}
		return C + (D - C) * (fVal - A) / (B - A);
	}

	FXD::S16 switch_calibrate_stick_centered(IGamePadSwitch::SwitchContext& context, FXD::S32 nStick, FXD::S32 nAxis, FXD::S16 nRawVal, FXD::S16 nCenter)
	{
		nRawVal -= nCenter;

		if (nRawVal > context.m_jcStickExtents[nStick].axis[nAxis].sMax)
		{
			context.m_jcStickExtents[nStick].axis[nAxis].sMax = nRawVal;
		}
		if (nRawVal < context.m_jcStickExtents[nStick].axis[nAxis].sMin)
		{
			context.m_jcStickExtents[nStick].axis[nAxis].sMin = nRawVal;
		}

		if (nRawVal > 0)
		{
			return (FXD::S16)(switch_remap(nRawVal, 0, context.m_jcStickExtents[nStick].axis[nAxis].sMax, 0, Math::NumericLimits< FXD::S16 >::max()));
		}
		else
		{
			return (FXD::S16)(switch_remap(nRawVal, context.m_jcStickExtents[nStick].axis[nAxis].sMin, 0, Math::NumericLimits< FXD::S16 >::min(), 0));
		}
	}

	FXD::S16 switch_calibrate_stick(IGamePadSwitch::SwitchContext& context, FXD::S32 nStick, FXD::S32 nAxis, FXD::S16 nRawVal)
	{
		return switch_calibrate_stick_centered(context, nStick, nAxis, nRawVal, context.m_jcStickCalData[nStick].axis[nAxis].sCenter);
	}

	void switch_encode_rumble(IGamePadSwitch::SwitchRumbleData& rumble, FXD::U16 usHighFreq, FXD::U8 ucHighFreqAmp, FXD::U8 ucLowFreq, FXD::U16 usLowFreqAmp)
	{
		if (ucHighFreqAmp > 0 || usLowFreqAmp > 0)
		{
			// High-band frequency and low-band amplitude are actually nine-bits each so they take a bit from the high-band amplitude and low-band frequency bytes respectively
			rumble.data2[0] = usHighFreq & 0xFF;
			rumble.data2[1] = ucHighFreqAmp | ((usHighFreq >> 8) & 0x01);
			rumble.data2[2] = ucLowFreq | ((usLowFreqAmp >> 8) & 0x80);
			rumble.data2[3] = usLowFreqAmp & 0xFF;
		}
		else
		{
			rumble.set_neutral();
		}
	}

	void switch_write_rumble(IGamePadSwitch::SwitchContext& context, FXD::F32 fLeftRumble, FXD::F32 fRightRumble)
	{
		const FXD::U16 k_usHighFreq = 0x0074;
		const FXD::U8 k_ucHighFreqAmp = 0xBE;
		const FXD::U8 k_ucLowFreq = 0x3D;
		const FXD::U16 k_usLowFreqAmp = 0x806F;

		if (fLeftRumble > 0.0f)
		{
			switch_encode_rumble(context.m_jcRumbleData[0], k_usHighFreq, k_ucHighFreqAmp, k_ucLowFreq, k_usLowFreqAmp);
		}
		else
		{
			context.m_jcRumbleData[0].set_neutral();
		}

		if (fRightRumble > 0.0f)
		{
			switch_encode_rumble(context.m_jcRumbleData[1], k_usHighFreq, k_ucHighFreqAmp, k_ucLowFreq, k_usLowFreqAmp);
		}
		else
		{
			context.m_jcRumbleData[1].set_neutral();
		}

		SwitchCommonOutputPacket_t commonData;
		commonData.ucPacketType = FXD::STD::to_underlying(ESwitchOutputReportID::Rumble);
		commonData.ucPacketNumber = context.m_nCommandNumber;
		Memory::MemCopy(&commonData.rumbleData, &context.m_jcRumbleData, sizeof(context.m_jcRumbleData));
		
		context.m_nCommandNumber = (context.m_nCommandNumber + 1) & 0xF;

		FXD::S32 nRet = 0;
		if ((nRet = WritePacket(context, (FXD::U8*)&commonData, sizeof(commonData))) == -1)
		{
			return;
		}
		return;
	}

	FXD::F32 switch_map_stick_value(FXD::S16 nVal, const FXD::S16 nDeadZone)
	{
		if (nVal < 0)
		{
			nVal = (nVal + nDeadZone);
			if (nVal > 0)
			{
				nVal = 0;
			}
			nVal = (nVal * 32768) / (32768 - nDeadZone);
		}
		else
		{
			nVal = (nVal - nDeadZone);
			if (nVal < 0)
			{
				nVal = 0;
			}
			nVal = (nVal * 32768) / (32768 - nDeadZone);
		}
		return (FXD::F32)nVal / 32768.0f;
	}
}

// ------
// IGamePadSwitchPro::SwitchRumbleData
// -
// ------
IGamePadSwitch::SwitchRumbleData::SwitchRumbleData(void)
{
	set_neutral();
}

void IGamePadSwitch::SwitchRumbleData::set_neutral(void)
{
	data2[0] = 0x00;
	data2[1] = 0x01;
	data2[2] = 0x40;
	data2[3] = 0x40;
}

// ------
// IGamePadSwitch::SwitchContext
// -
// ------
IGamePadSwitch::SwitchContext::SwitchContext(HidApi::HidApiDevice* pHDevice, const FXD::S32 nPort)
	: m_pHDevice(pHDevice)
	, m_nTypeHandle(k_Unknown)
	, m_nPortIndex(nPort)
	, m_bConnected(false)
	, m_bBluetooth(true)
	, m_bHasHomeLED(false)
	, m_nCommandNumber(0)
{
	Memory::MemZero_T(m_state);
	Memory::MemZero_T(m_jcStickCalData);
	Memory::MemZero_T(m_jcStickExtents);
}

// ------
// IGamePadSwitch
// -
// ------
IGamePadSwitch::IGamePadSwitch(HidApi::HidApiDevice* pHDevice)
	: IInputDevice()
	, m_context(pHDevice, 0)
	, OnControllerButton(nullptr)
	, OnControllerAnalog(nullptr)
{
	OnControllerButton = FXDEvent()->attach< Input::OnControllerButton >([&](Input::OnControllerButton evt)
	{
		if (evt.Device == this)
		{
			PRINT_INFO << Input::GetGamePadName(evt.Control);
		}
	});
	OnControllerAnalog = FXDEvent()->attach< Input::OnControllerAnalog >([&](Input::OnControllerAnalog evt)
	{
		if (evt.Device == this)
		{
			PRINT_INFO << evt.Value;
		}
	});

	hid_set_nonblocking(m_context.m_pHDevice->device(), 0);

	switch_has_home_led(m_context);
	switch_has_setup_bluetooth(m_context);
	switch_get_dev_info(m_context);
	switch_get_colour(m_context);
	switch_get_stick(m_context, ESwitchInputReportID::FullControllerState);
	switch_set_vibration_enabled(m_context, 1);
	switch_set_input_mode(m_context, ESwitchInputReportID::FullControllerState);
	if (m_context.m_bHasHomeLED)
	{
		switch_set_home_led(m_context, 20);
	}
	switch_set_slot_led(m_context, 0);
}

IGamePadSwitch::~IGamePadSwitch(void)
{
	if (OnControllerButton != nullptr)
	{
		FXDEvent()->detach(OnControllerButton);
		OnControllerButton = nullptr;
	}
	if (OnControllerAnalog != nullptr)
	{
		FXDEvent()->detach(OnControllerAnalog);
		OnControllerAnalog = nullptr;
	}
}

bool IGamePadSwitch::is_connected(void) const
{
	return (m_context.m_bConnected);
}

bool IGamePadSwitch::is_gamepad_available(void) const
{
	return (m_context.m_bConnected);
}

bool IGamePadSwitch::supports_rumble(void) const
{
	return false;
}

Core::String8 IGamePadSwitch::get_config_name(void) const
{
	return UTF_8("Switch");
}

Core::String8 IGamePadSwitch::get_device_name(void) const
{
	return UTF_8("Unknown");
}

Input::E_DeviceType IGamePadSwitch::get_device_type(void) const
{
	return Input::E_DeviceType::Controller;
}

void IGamePadSwitch::_update(FXD::F32 dt)
{
	bool bWasConnected = m_context.m_bConnected;

	FXD::U8* pPacket = nullptr;
	FXD::U8 pData[0x170];
	FXD::S32 nReadSize = 0;
	{
		while ((nReadSize = FXDHidApi()->hidApiManager()->read_input(m_context.m_pHDevice, pData, sizeof(pData), 0)) > 0)
		{
			pPacket = (FXD::U8*)(pData);
			break;
		}
		m_context.m_bConnected = !(nReadSize < 0);

		if (!bWasConnected && m_context.m_bConnected)
		{
			FXDEvent()->raise(Input::OnControllerConnection(this, true, m_context.m_nPortIndex));
		}
		else if (bWasConnected && !m_context.m_bConnected)
		{
			FXDEvent()->raise(Input::OnControllerConnection(this, false, m_context.m_nPortIndex));
		}
	}

	if (pPacket != nullptr)
	{
		if (pPacket[0] == FXD::STD::to_underlying(ESwitchInputReportID::FullControllerState))
		{
			SwitchPacketState state;
			Memory::MemZero_T(state);

			SwitchControllerStatePacket_t* pSPacket = (SwitchControllerStatePacket_t*)&pPacket[1];
			FXD::U8 nOffset = m_context.m_bBluetooth ? 0 : 10;
			
			// Battery
			{
				if (pSPacket->nBatteryConnection & 0x1)
				{
					m_ePowerLevel = E_PowerLevel::Wired;
				}
				else
				{
					FXD::S32 nLevel = (pSPacket->nBatteryConnection & 0xE0) >> 4;
					switch (nLevel)
					{
						case 0:
						case 1:
						{
							m_ePowerLevel = E_PowerLevel::Empty;
							break;
						}
						case 2:
						case 3:
						{
							m_ePowerLevel = E_PowerLevel::Low;
							break;
						}
						case 4:
						case 5:
						{
							m_ePowerLevel = E_PowerLevel::Medium;
							break;
						}
						case 6:
						case 7:
						{
							m_ePowerLevel = E_PowerLevel::Good;
							break;
						}
						case 8:
						{
							m_ePowerLevel = E_PowerLevel::Full;
							break;
						}
					}
				}
			}
			
			// Rumble
			{
				if (m_context.m_bConnected)
				{
					bool bStateDirty = false;
					FXD::F32 fLeftRumble = 0.0f;
					FXD::F32 fRightRumble = 0.0f;

					if (m_bRumbleDirty)
					{
						fLeftRumble = (m_bRumblePaused) ? 0.0 : m_rumbleData.m_fLeftRumble;
						fRightRumble = (m_bRumblePaused) ? 0.0 : m_rumbleData.m_fRightRumble;
						bStateDirty = true;
					}
					else
					{
						if (!m_bRumblePaused && (m_rumbleData.m_fLeftRumble > 0.0f || m_rumbleData.m_fRightRumble > 0.0f || m_rumbleData.m_fDuration > 0.0f))
						{
							m_fRumbleExpiration += dt;
							if (m_fRumbleExpiration > m_rumbleData.m_fDuration)
							{
								m_fRumbleExpiration = 0.0f;
								m_rumbleData.m_fLeftRumble = 0.0f;
								m_rumbleData.m_fRightRumble = 0.0f;
								m_rumbleData.m_fDuration = 0.0f;
								bStateDirty = true;
							}
						}
					}

					if (bStateDirty)
					{
						switch_write_rumble(m_context, fLeftRumble, fRightRumble);
						m_bRumbleDirty = false;
					}
				}
			}

			if (m_context.m_bConnected || bWasConnected)
			{
				state.nButtonsL = (m_context.m_nTypeHandle == k_ESwitchDevice_JCL || m_context.m_nTypeHandle == k_ESwitchDevice_PRO) ? (pSPacket->pButtons[1] << 8) | (pSPacket->pButtons[2] & 0xFF) : 0;
				state.nButtonsR = (m_context.m_nTypeHandle == k_ESwitchDevice_JCR || m_context.m_nTypeHandle == k_ESwitchDevice_PRO) ? (pSPacket->pButtons[1] << 8) | (pSPacket->pButtons[0] & 0xFF) : 0;

				if (m_context.m_nTypeHandle == k_ESwitchDevice_JCL || m_context.m_nTypeHandle == k_ESwitchDevice_PRO)
				{
					const FXD::S16 nXAxis = pSPacket->pJoystickLeft[0] | ((pSPacket->pJoystickLeft[1] & 0xF) << 8);
					const FXD::S16 nYAxis = ((pSPacket->pJoystickLeft[1] & 0xF0) >> 4) | (pSPacket->pJoystickLeft[2] << 4);
					state.nThumbLX = switch_calibrate_stick(m_context, 0, 0, nXAxis);
					state.nThumbLY = switch_calibrate_stick(m_context, 0, 1, nYAxis);
				}
				if (m_context.m_nTypeHandle == k_ESwitchDevice_JCR || m_context.m_nTypeHandle == k_ESwitchDevice_PRO)
				{
					const FXD::S16 nXAxis = pSPacket->pJoystickRight[0] | ((pSPacket->pJoystickRight[1] & 0xF) << 8);
					const FXD::S16 nYAxis = ((pSPacket->pJoystickRight[0] & 0xF0) >> 4) | (pSPacket->pJoystickRight[2] << 4);
					state.nThumbRX = switch_calibrate_stick(m_context, 1, 0, nXAxis);
					state.nThumbRY = switch_calibrate_stick(m_context, 1, 1, nYAxis);
				}

				if (m_context.m_nTypeHandle == k_ESwitchDevice_JCL || m_context.m_nTypeHandle == k_ESwitchDevice_PRO)
				{
					if ((state.nButtonsL & SWITCH_GAMEPAD_DPAD_UP) != (m_context.m_state.nButtonsL & SWITCH_GAMEPAD_DPAD_UP))
					{
						const bool bVal = (state.nButtonsL & SWITCH_GAMEPAD_DPAD_UP);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::DPadUp, m_context.m_nPortIndex));
					}
					if ((state.nButtonsL & SWITCH_GAMEPAD_DPAD_DOWN) != (m_context.m_state.nButtonsL & SWITCH_GAMEPAD_DPAD_DOWN))
					{
						const bool bVal = (state.nButtonsL & SWITCH_GAMEPAD_DPAD_DOWN);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::DPadDown, m_context.m_nPortIndex));
					}
					if ((state.nButtonsL & SWITCH_GAMEPAD_DPAD_LEFT) != (m_context.m_state.nButtonsL & SWITCH_GAMEPAD_DPAD_LEFT))
					{
						const bool bVal = (state.nButtonsL & SWITCH_GAMEPAD_DPAD_LEFT);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::DPadLeft, m_context.m_nPortIndex));
					}
					if ((state.nButtonsL & SWITCH_GAMEPAD_DPAD_RIGHT) != (m_context.m_state.nButtonsL & SWITCH_GAMEPAD_DPAD_RIGHT))
					{
						const bool bVal = (state.nButtonsL & SWITCH_GAMEPAD_DPAD_RIGHT);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::DPadRight, m_context.m_nPortIndex));
					}
					if ((state.nButtonsL & SWITCH_GAMEPAD_LEFT_THUMB) != (m_context.m_state.nButtonsL & SWITCH_GAMEPAD_LEFT_THUMB))
					{
						const bool bVal = (state.nButtonsL & SWITCH_GAMEPAD_LEFT_THUMB);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::LeftThumb, m_context.m_nPortIndex));
					}
					if ((state.nButtonsL & SWITCH_GAMEPAD_LEFT_BUMPER) != (m_context.m_state.nButtonsL & SWITCH_GAMEPAD_LEFT_BUMPER))
					{
						const bool bVal = (state.nButtonsL & SWITCH_GAMEPAD_LEFT_BUMPER);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::LeftBumper, m_context.m_nPortIndex));
					}
					if ((state.nButtonsL & SWITCH_GAMEPAD_BACK) != (m_context.m_state.nButtonsL & SWITCH_GAMEPAD_BACK))
					{
						const bool bVal = (state.nButtonsL & SWITCH_GAMEPAD_BACK);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::Back, m_context.m_nPortIndex));
					}
					if ((state.nButtonsL & SWITCH_GAMEPAD_CAPTURE) != (m_context.m_state.nButtonsL & SWITCH_GAMEPAD_CAPTURE))
					{
						const bool bVal = (state.nButtonsL & SWITCH_GAMEPAD_CAPTURE);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::Capture, m_context.m_nPortIndex));
					}
					if ((state.nButtonsL & SWITCH_GAMEPAD_SL) != (m_context.m_state.nButtonsL & SWITCH_GAMEPAD_SL))
					{
						const bool bVal = (state.nButtonsL & SWITCH_GAMEPAD_SL);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::SL, m_context.m_nPortIndex));
					}
					if ((state.nButtonsL & SWITCH_GAMEPAD_SR) != (m_context.m_state.nButtonsL & SWITCH_GAMEPAD_SR))
					{
						const bool bVal = (state.nButtonsL & SWITCH_GAMEPAD_SR);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::SR, m_context.m_nPortIndex));
					}
					if ((state.nButtonsL & SWITCH_GAMEPAD_LEFT_TRIGGER) != (m_context.m_state.nButtonsL & SWITCH_GAMEPAD_LEFT_TRIGGER))
					{
						const bool bVal = (state.nButtonsL & SWITCH_GAMEPAD_LEFT_TRIGGER);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::LeftTrigger, m_context.m_nPortIndex));
					}
					if (switch_map_stick_value(state.nThumbLX, SWITCH_GAMEPAD_LEFT_THUMB_DEADZONE) !=
						switch_map_stick_value(m_context.m_state.nThumbLX, SWITCH_GAMEPAD_LEFT_THUMB_DEADZONE))
					{
						const FXD::F32 fVal = switch_map_stick_value(state.nThumbLX, SWITCH_GAMEPAD_LEFT_THUMB_DEADZONE);
						FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::LeftThumbX, m_context.m_nPortIndex));
					}
					if (switch_map_stick_value(state.nThumbLY, SWITCH_GAMEPAD_LEFT_THUMB_DEADZONE) !=
						switch_map_stick_value(m_context.m_state.nThumbLY, SWITCH_GAMEPAD_LEFT_THUMB_DEADZONE))
					{
						const FXD::F32 fVal = switch_map_stick_value(state.nThumbLY, SWITCH_GAMEPAD_LEFT_THUMB_DEADZONE);
						FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::LeftThumbY, m_context.m_nPortIndex));
					}
				}
				if (m_context.m_nTypeHandle == k_ESwitchDevice_JCR || m_context.m_nTypeHandle == k_ESwitchDevice_PRO)
				{
					if ((state.nButtonsR & SWITCH_GAMEPAD_A) != (m_context.m_state.nButtonsR & SWITCH_GAMEPAD_A))
					{
						const bool bVal = (state.nButtonsR & 0x08);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::AButton, m_context.m_nPortIndex));
					}
					if ((state.nButtonsR & SWITCH_GAMEPAD_B) != (m_context.m_state.nButtonsR & SWITCH_GAMEPAD_B))
					{
						const bool bVal = (state.nButtonsR & SWITCH_GAMEPAD_B);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::BButton, m_context.m_nPortIndex));
					}
					if ((state.nButtonsR & SWITCH_GAMEPAD_X) != (m_context.m_state.nButtonsR & SWITCH_GAMEPAD_X))
					{
						const bool bVal = (state.nButtonsR & SWITCH_GAMEPAD_X);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::XButton, m_context.m_nPortIndex));
					}
					if ((state.nButtonsR & SWITCH_GAMEPAD_Y) != (m_context.m_state.nButtonsR & SWITCH_GAMEPAD_Y))
					{
						const bool bVal = (state.nButtonsR & SWITCH_GAMEPAD_Y);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::YButton, m_context.m_nPortIndex));
					}
					if ((state.nButtonsR & SWITCH_GAMEPAD_RIGHT_THUMB) != (m_context.m_state.nButtonsR & SWITCH_GAMEPAD_RIGHT_THUMB))
					{
						const bool bVal = (state.nButtonsR & SWITCH_GAMEPAD_RIGHT_THUMB);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::RightThumb, m_context.m_nPortIndex));
					}
					if ((state.nButtonsR & SWITCH_GAMEPAD_RIGHT_BUMPER) != (m_context.m_state.nButtonsR & SWITCH_GAMEPAD_RIGHT_BUMPER))
					{
						const bool bVal = (state.nButtonsR & SWITCH_GAMEPAD_RIGHT_BUMPER);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::RightBumper, m_context.m_nPortIndex));
					}
					if ((state.nButtonsR & SWITCH_GAMEPAD_START) != (m_context.m_state.nButtonsR & SWITCH_GAMEPAD_START))
					{
						const bool bVal = (state.nButtonsR & SWITCH_GAMEPAD_START);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::Start, m_context.m_nPortIndex));
					}
					if ((state.nButtonsR & SWITCH_GAMEPAD_GUIDE) != (m_context.m_state.nButtonsR & SWITCH_GAMEPAD_GUIDE))
					{
						const bool bVal = (state.nButtonsR & SWITCH_GAMEPAD_GUIDE);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::Guide, m_context.m_nPortIndex));
					}
					if ((state.nButtonsR & SWITCH_GAMEPAD_SL) != (m_context.m_state.nButtonsR & SWITCH_GAMEPAD_SL))
					{
						const bool bVal = (state.nButtonsR & SWITCH_GAMEPAD_SL);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::SL, m_context.m_nPortIndex));
					}
					if ((state.nButtonsR & SWITCH_GAMEPAD_SR) != (m_context.m_state.nButtonsR & SWITCH_GAMEPAD_SR))
					{
						const bool bVal = (state.nButtonsR & SWITCH_GAMEPAD_SR);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::SR, m_context.m_nPortIndex));
					}
					if ((state.nButtonsR & SWITCH_GAMEPAD_RIGHT_TRIGGER) != (m_context.m_state.nButtonsR & SWITCH_GAMEPAD_RIGHT_TRIGGER))
					{
						const bool bVal = (state.nButtonsR & SWITCH_GAMEPAD_RIGHT_TRIGGER);
						FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::RightTrigger, m_context.m_nPortIndex));
					}
					if (switch_map_stick_value(state.nThumbRX, SWITCH_GAMEPAD_RIGHT_THUMB_DEADZONE) !=
						switch_map_stick_value(m_context.m_state.nThumbRX, SWITCH_GAMEPAD_RIGHT_THUMB_DEADZONE))
					{
						const FXD::F32 fVal = switch_map_stick_value(state.nThumbRX, SWITCH_GAMEPAD_RIGHT_THUMB_DEADZONE);
						FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::RightThumbX, m_context.m_nPortIndex));
					}
					if (switch_map_stick_value(state.nThumbRY, SWITCH_GAMEPAD_RIGHT_THUMB_DEADZONE) !=
						switch_map_stick_value(m_context.m_state.nThumbRY, SWITCH_GAMEPAD_RIGHT_THUMB_DEADZONE))
					{
						const FXD::F32 fVal = switch_map_stick_value(state.nThumbRY, SWITCH_GAMEPAD_RIGHT_THUMB_DEADZONE);
						FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::RightThumbY, m_context.m_nPortIndex));
					}
				}
			}
			Memory::MemCopy(&m_context.m_state, &state, sizeof(SwitchPacketState));
		}
	}
	else
	{
		if ((nReadSize < 0) && bWasConnected)
		{
			m_ePowerLevel = E_PowerLevel::Unknown;
			Memory::MemZero_T(m_context.m_state);
			//HIDAPI_JoystickDisconnected(device, joystick->instance_id, false);
		}
	}
}

static CONSTEXPR const Widget::WIDGET_DESC kSwitchWidgets[] =
{
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::DPadUp), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::DPadUp), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::DPadDown), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::DPadDown), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::DPadLeft), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::DPadLeft), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::DPadRight), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::DPadRight), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftThumbX), Input::E_Widget_type::FullAxis, Input::GetGamePadName(E_GamePadButtons::LeftThumbX), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftThumbY), Input::E_Widget_type::FullAxis, Input::GetGamePadName(E_GamePadButtons::LeftThumbY), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightThumbX), Input::E_Widget_type::FullAxis, Input::GetGamePadName(E_GamePadButtons::RightThumbX), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightThumbY), Input::E_Widget_type::FullAxis, Input::GetGamePadName(E_GamePadButtons::RightThumbY), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftThumb), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::LeftThumb), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightThumb), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::RightThumb), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftBumper), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::LeftBumper), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightBumper), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::RightBumper), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftTrigger), Input::E_Widget_type::HalfAxis, Input::GetGamePadName(E_GamePadButtons::LeftTrigger), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightTrigger), Input::E_Widget_type::HalfAxis, Input::GetGamePadName(E_GamePadButtons::RightTrigger), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::Start), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::Start), Widget::kWidgetFlagStartButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::Back), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::Back), Widget::kWidgetFlagBackButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::AButton), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::AButton), Widget::kWidgetFlagActionButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::BButton), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::BButton), Widget::kWidgetFlagBackButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::XButton), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::XButton), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::YButton), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::YButton), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::Guide), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::Guide), Widget::kWidgetFlagGuideButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::Capture), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::Capture), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::SL), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::SL), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::SR), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::SR), 0),
	Widget::WIDGET_DESC()
};

const Widget::WIDGET_DESC* IGamePadSwitch::_get_widget_defs(void) const
{
	return kSwitchWidgets;
}

void IGamePadSwitch::_get_widget_state(FXD::U16 nID, void* pState) const
{
	bool* pDigitalState = (bool*)pState;
	FXD::F32* pAnalogState = (FXD::F32*)pState;
	switch ((E_GamePadButtons)nID)
	{
		case E_GamePadButtons::DPadUp:
		{
			*pDigitalState = !!(m_context.m_state.nButtonsL & SWITCH_GAMEPAD_DPAD_UP);
			break;
		}
		case E_GamePadButtons::DPadDown:
		{
			*pDigitalState = !!(m_context.m_state.nButtonsL & SWITCH_GAMEPAD_DPAD_DOWN);
			break;
		}
		case E_GamePadButtons::DPadLeft:
		{
			*pDigitalState = !!(m_context.m_state.nButtonsL & SWITCH_GAMEPAD_DPAD_LEFT);
			break;
		}
		case E_GamePadButtons::DPadRight:
		{
			*pDigitalState = !!(m_context.m_state.nButtonsL & SWITCH_GAMEPAD_DPAD_RIGHT);
			break;
		}
		case E_GamePadButtons::LeftThumb:
		{
			*pDigitalState = !!(m_context.m_state.nButtonsL & SWITCH_GAMEPAD_LEFT_THUMB);
			break;
		}
		case E_GamePadButtons::RightThumb:
		{
			*pDigitalState = !!(m_context.m_state.nButtonsR & SWITCH_GAMEPAD_RIGHT_THUMB);
			break;
		}
		case E_GamePadButtons::LeftBumper:
		{
			*pDigitalState = !!(m_context.m_state.nButtonsL & SWITCH_GAMEPAD_LEFT_BUMPER);
			break;
		}
		case E_GamePadButtons::RightBumper:
		{
			*pDigitalState = !!(m_context.m_state.nButtonsR & SWITCH_GAMEPAD_RIGHT_BUMPER);
			break;
		}
		case E_GamePadButtons::Start:
		{
			*pDigitalState = !!(m_context.m_state.nButtonsR & SWITCH_GAMEPAD_START);
			break;
		}
		case E_GamePadButtons::Back:
		{
			*pDigitalState = !!(m_context.m_state.nButtonsL & SWITCH_GAMEPAD_BACK);
			break;
		}
		case E_GamePadButtons::AButton:
		{
			*pDigitalState = !!(m_context.m_state.nButtonsR & SWITCH_GAMEPAD_A);
			break;
		}
		case E_GamePadButtons::BButton:
		{
			*pDigitalState = !!(m_context.m_state.nButtonsR & SWITCH_GAMEPAD_B);
			break;
		}
		case E_GamePadButtons::XButton:
		{
			*pDigitalState = !!(m_context.m_state.nButtonsR & SWITCH_GAMEPAD_X);
			break;
		}
		case E_GamePadButtons::YButton:
		{
			*pDigitalState = !!(m_context.m_state.nButtonsR & SWITCH_GAMEPAD_Y);
			break;
		}
		case E_GamePadButtons::Guide:
		{
			*pDigitalState = !!(m_context.m_state.nButtonsR & SWITCH_GAMEPAD_GUIDE);
			break;
		}
		case E_GamePadButtons::Capture:
		{
			*pDigitalState = !!(m_context.m_state.nButtonsL & SWITCH_GAMEPAD_CAPTURE);
			break;
		}
		case E_GamePadButtons::SL:
		{
			if (m_context.m_nTypeHandle == k_ESwitchDevice_JCL)
			{
				*pDigitalState = !!(m_context.m_state.nButtonsL & SWITCH_GAMEPAD_SL);
			}
			if (m_context.m_nTypeHandle == k_ESwitchDevice_JCR)
			{
				*pDigitalState = !!(m_context.m_state.nButtonsR & SWITCH_GAMEPAD_SL);
			}
			break;
		}
		case E_GamePadButtons::SR:
		{
			if (m_context.m_nTypeHandle == k_ESwitchDevice_JCL)
			{
				*pDigitalState = !!(m_context.m_state.nButtonsL & SWITCH_GAMEPAD_SL);
			}
			if (m_context.m_nTypeHandle == k_ESwitchDevice_JCR)
			{
				*pDigitalState = !!(m_context.m_state.nButtonsR & SWITCH_GAMEPAD_SL);
			}
			break;
		}
		case E_GamePadButtons::LeftTrigger:
		{
			*pAnalogState = (m_context.m_state.nButtonsL & SWITCH_GAMEPAD_LEFT_BUMPER) ? 1.0f : 0.0f;
			break;
		}
		case E_GamePadButtons::RightTrigger:
		{
			*pAnalogState = (m_context.m_state.nButtonsR & SWITCH_GAMEPAD_RIGHT_BUMPER) ? 1.0f : 0.0f;
			break;
		}
		case E_GamePadButtons::LeftThumbX:
		{
			*pAnalogState = switch_map_stick_value(m_context.m_state.nThumbLX, SWITCH_GAMEPAD_LEFT_THUMB_DEADZONE);
			break;
		}
		case E_GamePadButtons::LeftThumbY:
		{
			*pAnalogState = switch_map_stick_value(m_context.m_state.nThumbLY, SWITCH_GAMEPAD_LEFT_THUMB_DEADZONE);
			break;
		}
		case E_GamePadButtons::RightThumbX:
		{
			*pAnalogState = switch_map_stick_value(m_context.m_state.nThumbRX, SWITCH_GAMEPAD_RIGHT_THUMB_DEADZONE);
			break;
		}
		case E_GamePadButtons::RightThumbY:
		{
			*pAnalogState = switch_map_stick_value(m_context.m_state.nThumbRY, SWITCH_GAMEPAD_RIGHT_THUMB_DEADZONE);
			break;
		}
	}
}
#endif //IsInputSwitch()