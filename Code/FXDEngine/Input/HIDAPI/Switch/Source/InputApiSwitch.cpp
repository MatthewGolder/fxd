// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputSwitch()
#include "FXDEngine/Input/HIDAPI/Switch/InputApiSwitch.h"
#include "FXDEngine/Input/HIDAPI/Switch/GamePadSwitch.h"
#include "FXDEngine/Core/Debugger.h"

using namespace FXD;
using namespace Input;
using namespace HidApi;

// ------
// IInputApiSwitch
// - 
// ------
IInputApiSwitch::IInputApiSwitch(void)
{
}

IInputApiSwitch::~IInputApiSwitch(void)
{
}

bool IInputApiSwitch::create_api(void)
{
	bool bRetVal = true;

	return bRetVal;
}

bool IInputApiSwitch::release_api(void)
{
	bool bRetVal = true;

	return bRetVal;
}

void IInputApiSwitch::_process(FXD::F32 /*dt*/)
{
}
#endif //IsInputSwitch()