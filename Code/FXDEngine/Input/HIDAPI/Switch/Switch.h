// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_HIDAPI_SWITCH_SWITCH_H
#define FXDENGINE_INPUT_HIDAPI_SWITCH_SWITCH_H

#include "FXDEngine/Input/Types.h"

#if IsInputSwitch()

namespace FXD
{
	namespace Input
	{
		namespace HidApi
		{
			class IGamePadSwitch;
			typedef std::shared_ptr< Input::HidApi::IGamePadSwitch > GamePadSwitch;

		} //namespace HidApi
	} //namespace Input
} //namespace FXD

#endif //IsInputSwitch()
#endif //FXDENGINE_INPUT_HIDAPI_SWITCH_SWITCH_H