// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputHIDAPI()
#include "FXDEngine/Input/HIDAPI/HidController.h"
#include "FXDEngine/Input/HIDAPI/HidApi.h"
#include "FXDEngine/Core/Endian.h"

using namespace FXD;
using namespace Input;
using namespace HidApi;

void HidGuid::set_guid_info(HidApi::E_USB_Vendor eVendorId, FXD::U16 nProduct, FXD::U16 nVersion)
{
	FXD::U16* pGuid16 = (FXD::U16*)data;

	*pGuid16++ = FXD_SwapLE16(HARDWARE_BUS_USB);
	*pGuid16++ = 0;
	*pGuid16++ = FXD_SwapLE16(FXD::STD::to_underlying(eVendorId));
	*pGuid16++ = 0;
	*pGuid16++ = FXD_SwapLE16(nProduct);
	*pGuid16++ = 0;
	*pGuid16++ = FXD_SwapLE16(nVersion);
	*pGuid16++ = 0;

	// Note that this is a HIDAPI device for special handling elsewhere
	data[14] = 'h';
	data[15] = 0;
}

void HidGuid::get_guid_info(HidApi::E_USB_Vendor* pVendor, FXD::U16* pProduct, FXD::U16* pVersion)const
{
	// If the GUID fits the form of BUS 0000 VENDOR 0000 PRODUCT 0000, return the data
	// data[0] is device bus type
	// data[2] is vendor ID
	// data[4] is product ID
	// data[6] is product version

	const FXD::U16* pGuid16 = (const FXD::U16*)data;
	if ((pGuid16[1] == 0x0000) && (pGuid16[3] == 0x0000) && (pGuid16[5] == 0x0000))
	{
		if (pVendor != nullptr)
		{
			*pVendor = (HidApi::E_USB_Vendor)pGuid16[2];
		}
		if (pProduct != nullptr)
		{
			*pProduct = pGuid16[4];
		}
		if (pVersion != nullptr)
		{
			*pVersion = pGuid16[6];
		}
	}
	else
	{
		if (pVendor != nullptr)
		{
			*pVendor = HidApi::E_USB_Vendor::Unknown;
		}
		if (pProduct != nullptr)
		{
			*pProduct = 0;
		}
		if (pVersion != nullptr)
		{
			*pVersion = 0;
		}
	}
}

// ------
// Funcs
// -
// ------

#define MakeVendorProductID(VID, PID) (((FXD::U32)(VID))<<16|(PID))

static CONSTEXPR const Input::HidApi::E_USB_Vendor kSupportedVendors[] =
{
	HidApi::E_USB_Vendor::BigBenInteractive,				// 0x146b
	HidApi::E_USB_Vendor::Elecom,							// 0x056e
	HidApi::E_USB_Vendor::Harmonix,							// 0x1bad
	HidApi::E_USB_Vendor::Hori,								// 0x0f0d
	HidApi::E_USB_Vendor::Joytech,							// 0x162e
	HidApi::E_USB_Vendor::Logitech,							// 0x046d
	HidApi::E_USB_Vendor::MadCatz,							// 0x0738
	HidApi::E_USB_Vendor::Microsoft,						// 0x045e
	HidApi::E_USB_Vendor::Nacon,							// 0x11c9
	HidApi::E_USB_Vendor::Numark,							// 0x15e4
	HidApi::E_USB_Vendor::PDP,								// 0x0e6f
	HidApi::E_USB_Vendor::RazerOnza,						// 0x1689
	HidApi::E_USB_Vendor::RazerSabertooth,					// 0x1532
	HidApi::E_USB_Vendor::RedOctane,						// 0x1430
	HidApi::E_USB_Vendor::ShenzhenLongshengweiTechnology,	// 0x0079
	HidApi::E_USB_Vendor::SteelSeries,						// 0x1038
	HidApi::E_USB_Vendor::ThrustMaster,						// 0x044f
	HidApi::E_USB_Vendor::ThrustMasterInc,					// 0x24c6
	HidApi::E_USB_Vendor::Unknown,							// 0x12ab
};

static CONSTEXPR const Input::HidApi::E_USB_Vendor kSupportedVendors2[] =
{
	HidApi::E_USB_Vendor::Hori,								// 0x0f0d
	HidApi::E_USB_Vendor::Hyperkin,							// 0x2e24
	HidApi::E_USB_Vendor::MadCatz,							// 0x0738
	HidApi::E_USB_Vendor::Microsoft,						// 0x045e
	HidApi::E_USB_Vendor::PDP,								// 0x0e6f
	HidApi::E_USB_Vendor::RazerSabertooth,					// 0x1532
	HidApi::E_USB_Vendor::ThrustMasterInc,					// 0x24c6
};

static CONSTEXPR const FXD::S32 kLibUsbClassVendorSpec = 0xFF;
static CONSTEXPR const FXD::S32 kXB360IFaceSubclass = 93;
static CONSTEXPR const FXD::S32 kXB360IFaceProtocol = 1;	// Wired
static CONSTEXPR const FXD::S32 kXB360WIFaceProtocol = 129;	// Wireless
static CONSTEXPR const FXD::S32 kXBoneIFaceSubclass = 71;
static CONSTEXPR const FXD::S32 kXBoneIFaceProtocol = 208;

static FXD::U32 kBlacklist[] =
{
	// Microsoft Microsoft Wireless Optical Desktop 2.10
	// Microsoft Wireless Desktop - Comfort Edition
	MakeVendorProductID(0x045e, 0x009d),

	// Microsoft Microsoft Digital Media Pro Keyboard
	// Microsoft Corp. Digital Media Pro Keyboard
	MakeVendorProductID(0x045e, 0x00b0),

	// Microsoft Microsoft Digital Media Keyboard
	// Microsoft Corp. Digital Media Keyboard 1.0A
	MakeVendorProductID(0x045e, 0x00b4),

	// Microsoft Microsoft Digital Media Keyboard 3000
	MakeVendorProductID(0x045e, 0x0730),

	// Microsoft Microsoft 2.4GHz Transceiver v6.0
	// Microsoft Microsoft 2.4GHz Transceiver v8.0
	// Microsoft Corp. Nano Transceiver v1.0 for Bluetooth
	// Microsoft Wireless Mobile Mouse 1000
	// Microsoft Wireless Desktop 3000
	MakeVendorProductID(0x045e, 0x0745),

	// Microsoft SideWinder(TM) 2.4GHz Transceiver
	MakeVendorProductID(0x045e, 0x0748),

	// Microsoft Corp. Wired Keyboard 600
	MakeVendorProductID(0x045e, 0x0750),

	// Microsoft Corp. Sidewinder X4 keyboard
	MakeVendorProductID(0x045e, 0x0768),

	// Microsoft Corp. Arc Touch Mouse Transceiver
	MakeVendorProductID(0x045e, 0x0773),

	// Microsoft 2.4GHz Transceiver v9.0
	// Microsoft Nano Transceiver v2.1
	// Microsoft Sculpt Ergonomic Keyboard (5KV-00001)
	MakeVendorProductID(0x045e, 0x07a5),

	// Microsoft Nano Transceiver v1.0
	// Microsoft Wireless Keyboard 800
	MakeVendorProductID(0x045e, 0x07b2),

	// Microsoft Nano Transceiver v2.0
	MakeVendorProductID(0x045e, 0x0800),

	MakeVendorProductID(0x046d, 0xc30a), // Logitech, Inc. iTouch Composite keboard

	MakeVendorProductID(0x04d9, 0xa0df), // Tek Syndicate Mouse (E-Signal USB Gaming Mouse)

	// List of Wacom devices at: http://linuxwacom.sourceforge.net/wiki/index.php/Device_IDs
	MakeVendorProductID(0x056a, 0x0010), // Wacom ET-0405 Graphire
	MakeVendorProductID(0x056a, 0x0011), // Wacom ET-0405A Graphire2 (4x5)
	MakeVendorProductID(0x056a, 0x0012), // Wacom ET-0507A Graphire2 (5x7)
	MakeVendorProductID(0x056a, 0x0013), // Wacom CTE-430 Graphire3 (4x5)
	MakeVendorProductID(0x056a, 0x0014), // Wacom CTE-630 Graphire3 (6x8)
	MakeVendorProductID(0x056a, 0x0015), // Wacom CTE-440 Graphire4 (4x5)
	MakeVendorProductID(0x056a, 0x0016), // Wacom CTE-640 Graphire4 (6x8)
	MakeVendorProductID(0x056a, 0x0017), // Wacom CTE-450 Bamboo Fun (4x5)
	MakeVendorProductID(0x056a, 0x0018), // Wacom CTE-650 Bamboo Fun 6x8
	MakeVendorProductID(0x056a, 0x0019), // Wacom CTE-631 Bamboo One
	MakeVendorProductID(0x056a, 0x00d1), // Wacom Bamboo Pen and Touch CTH-460
	MakeVendorProductID(0x056a, 0x030e), // Wacom Intuos Pen (S) CTL-480

	MakeVendorProductID(0x09da, 0x054f), // A4 Tech Co., G7 750 mouse
	MakeVendorProductID(0x09da, 0x1410), // A4 Tech Co., Ltd Bloody AL9 mouse
	MakeVendorProductID(0x09da, 0x3043), // A4 Tech Co., Ltd Bloody R8A Gaming Mouse
	MakeVendorProductID(0x09da, 0x31b5), // A4 Tech Co., Ltd Bloody TL80 Terminator Laser Gaming Mouse
	MakeVendorProductID(0x09da, 0x3997), // A4 Tech Co., Ltd Bloody RT7 Terminator Wireless
	MakeVendorProductID(0x09da, 0x3f8b), // A4 Tech Co., Ltd Bloody V8 mouse
	MakeVendorProductID(0x09da, 0x51f4), // Modecom MC-5006 Keyboard
	MakeVendorProductID(0x09da, 0x5589), // A4 Tech Co., Ltd Terminator TL9 Laser Gaming Mouse
	MakeVendorProductID(0x09da, 0x7b22), // A4 Tech Co., Ltd Bloody V5
	MakeVendorProductID(0x09da, 0x7f2d), // A4 Tech Co., Ltd Bloody R3 mouse
	MakeVendorProductID(0x09da, 0x8090), // A4 Tech Co., Ltd X-718BK Oscar Optical Gaming Mouse
	MakeVendorProductID(0x09da, 0x9033), // A4 Tech Co., X7 X-705K
	MakeVendorProductID(0x09da, 0x9066), // A4 Tech Co., Sharkoon Fireglider Optical
	MakeVendorProductID(0x09da, 0x9090), // A4 Tech Co., Ltd XL-730K / XL-750BK / XL-755BK Laser Mouse
	MakeVendorProductID(0x09da, 0x90c0), // A4 Tech Co., Ltd X7 G800V keyboard
	MakeVendorProductID(0x09da, 0xf012), // A4 Tech Co., Ltd Bloody V7 mouse
	MakeVendorProductID(0x09da, 0xf32a), // A4 Tech Co., Ltd Bloody B540 keyboard
	MakeVendorProductID(0x09da, 0xf613), // A4 Tech Co., Ltd Bloody V2 mouse
	MakeVendorProductID(0x09da, 0xf624), // A4 Tech Co., Ltd Bloody B120 Keyboard

	MakeVendorProductID(0x1b1c, 0x1b3c), // Corsair Harpoon RGB gaming mouse

	MakeVendorProductID(0x1d57, 0xad03), // [T3] 2.4GHz and IR Air Mouse Remote Control

	MakeVendorProductID(0x1e7d, 0x2e4a), // Roccat Tyon Mouse

	MakeVendorProductID(0x20a0, 0x422d), // Winkeyless.kr Keyboards

	MakeVendorProductID(0x2516, 0x001f), // Cooler Master Storm Mizar Mouse
	MakeVendorProductID(0x2516, 0x0028), // Cooler Master Storm Alcor Mouse
};

extern Input::HidApi::E_USB_Vendor Input::HidApi::Funcs::GetUSBVendor(FXD::U16 nVendorID)
{
	for (FXD::U32 n1 = 0; n1 < sizeof(HidApi::kUSBVendors) / sizeof(HidApi::kUSBVendors[0]); ++n1)
	{
		if (nVendorID == FXD::STD::to_underlying(HidApi::kUSBVendors[n1]))
		{
			return HidApi::kUSBVendors[n1];
		}
	}

	return HidApi::E_USB_Vendor::Unknown;

}

extern Input::HidApi::E_HidControllerType Input::HidApi::Funcs::GetGameControllerType(const FXD::UTF16* pName, HidApi::E_USB_Vendor eVendor, FXD::U16 nProduct, FXD::S32 nInterfaceNumber, FXD::S32 nInterfaceClass, FXD::S32 nInterfaceSubclass, FXD::S32 nInterfaceProtocol)
{
	Input::HidApi::E_HidControllerType eType = Input::HidApi::E_HidControllerType::Unknown;

	// This code should match the checks in libusb/hid.c and HIDDeviceManager.java
	if ((nInterfaceClass == kLibUsbClassVendorSpec && nInterfaceSubclass == kXB360IFaceSubclass) &&
		(nInterfaceProtocol == kXB360IFaceProtocol || nInterfaceProtocol == kXB360WIFaceProtocol))
	{
		for (FXD::U32 n1 = 0; n1 < FXD::STD::array_size(kSupportedVendors); ++n1)
		{
			if (eVendor == kSupportedVendors[n1])
			{
				eType = Input::HidApi::E_HidControllerType::XBox360;
				break;
			}
		}
	}

	if (nInterfaceNumber == 0 && nInterfaceClass == kLibUsbClassVendorSpec && nInterfaceSubclass == kXBoneIFaceSubclass && nInterfaceProtocol == kXBoneIFaceProtocol)
	{
		for (FXD::U32 n1 = 0; n1 < FXD::STD::array_size(kSupportedVendors2); ++n1)
		{
			if (eVendor == kSupportedVendors2[n1])
			{
				eType = Input::HidApi::E_HidControllerType::XBoxOne;
				break;
			}
		}
	}

	if (eType == Input::HidApi::E_HidControllerType::Unknown)
	{
		switch (GuessControllerType(eVendor, nProduct))
		{
			case Input::HidApi::E_HidControllerHelper::SteamUnknown:
			case Input::HidApi::E_HidControllerHelper::SteamV1:
			case Input::HidApi::E_HidControllerHelper::SteamV2:
			{
				eType = Input::HidApi::E_HidControllerType::Steam;
				break;
			}
			case Input::HidApi::E_HidControllerHelper::XBox360:
			{
				eType = Input::HidApi::E_HidControllerType::XBox360;
				break;
			}
			case Input::HidApi::E_HidControllerHelper::XBoxOne:
			{
				eType = Input::HidApi::E_HidControllerType::XBoxOne;
				break;
			}
			case Input::HidApi::E_HidControllerHelper::PS3:
			{
				eType = Input::HidApi::E_HidControllerType::PS3;
				break;
			}
			case Input::HidApi::E_HidControllerHelper::PS4:
			{
				eType = Input::HidApi::E_HidControllerType::PS4;
				break;
			}
			case Input::HidApi::E_HidControllerHelper::SwitchJoyConLeft:
			case Input::HidApi::E_HidControllerHelper::SwitchJoyConRight:
			{
				eType = Input::HidApi::E_HidControllerType::SwitchJoycon;
				break;
			}
			case Input::HidApi::E_HidControllerHelper::SwitchPro:
			case Input::HidApi::E_HidControllerHelper::SwitchProInput:
			{
				eType = Input::HidApi::E_HidControllerType::SwitchPro;
				break;
			}
			default:
			{
				eType = Input::HidApi::E_HidControllerType::Unknown;
				break;
			}
		}
	}
	return eType;
}

Input::HidApi::E_HidControllerHelper Input::HidApi::Funcs::GuessControllerType(HidApi::E_USB_Vendor eVendor, FXD::S32 nPID)
{
	FXD::U32 nDeviceID = MAKE_CONTROLLER_ID(eVendor, nPID);

	for (FXD::U32 n1 = 0; n1 < sizeof(Input::HidApi::kControllerDescs) / sizeof(Input::HidApi::kControllerDescs[0]); ++n1)
	{
		if (nDeviceID == Input::HidApi::kControllerDescs[n1].DeviceID)
		{
			return Input::HidApi::kControllerDescs[n1].ControllerType;
		}
	}

	return Input::HidApi::E_HidControllerHelper::Unknown;
}

bool Input::HidApi::Funcs::ShouldIgnoreController(const FXD::UTF16* pName, Input::HidApi::HidGuid guid)
{
	HidApi::E_USB_Vendor eVendor = HidApi::E_USB_Vendor::Unknown;
	FXD::U16 nProduct = 0;
	guid.get_guid_info(&eVendor, &nProduct, nullptr);

	// Check the joystick blacklist
	FXD::U32 nId = MakeVendorProductID(eVendor, nProduct);
	for (FXD::U32 n1 = 0; n1 < FXD::STD::array_size(kBlacklist); ++n1)
	{
		if (nId == kBlacklist[n1])
		{
			return true;
		}
	}

	for (FXD::U32 n1 = 0; n1 < FXD::STD::array_size(kBlacklist); ++n1)
	{
		if (nId == kBlacklist[n1])
		{
			return true;
		}
	}

	return false;
}

Core::String16 Input::HidApi::Funcs::CreateControllerName(HidApi::E_USB_Vendor eVendor, FXD::U16 nProduct, const FXD::UTF16* pVendorName, const FXD::UTF16* pProductName)
{
	FXD::Core::String16 strName16;
	FXD::Core::StringView16 strCustomName = Funcs::GuessControllerName(eVendor, nProduct);
	if (strCustomName.not_empty())
	{
		return strCustomName.c_str();
	}

	if (pVendorName == nullptr)
	{
		pVendorName = UTF_16("");
	}
	if (pProductName == nullptr)
	{
		pProductName = UTF_16("");
	}

	while (*pVendorName == UTF16(' '))
	{
		++pVendorName;
	}
	while (*pProductName == UTF16(' '))
	{
		++pProductName;
	}

	if (*pVendorName && *pProductName)
	{
		strName16 = pVendorName;
		strName16 += UTF16(' ');
		strName16 += pProductName;
	}
	else if (*pProductName)
	{
		strName16 = pProductName;
	}
	else if (eVendor != HidApi::E_USB_Vendor::Unknown || nProduct)
	{
		Core::StringBuilder16 strBuilder;
		strBuilder << FXD::STD::to_underlying(eVendor) << nProduct;
		strName16 = strBuilder.str();
	}
	else
	{
		strName16 = UTF_16("Controller");
	}

	// Trim trailing whitespace
	strName16.rtrim();

	return strName16;
}

Core::StringView16 Input::HidApi::Funcs::GuessControllerName(HidApi::E_USB_Vendor eVendor, FXD::S32 nPID)
{
	FXD::U32 nDeviceID = MAKE_CONTROLLER_ID(eVendor, nPID);

	for (FXD::U32 n1 = 0; n1 < sizeof(Input::HidApi::kControllerDescs) / sizeof(Input::HidApi::kControllerDescs[0]); ++n1)
	{
		if (nDeviceID == Input::HidApi::kControllerDescs[n1].DeviceID)
		{
			return Input::HidApi::kControllerDescs[n1].Name;
		}
	}

	return UTF_16("");
}
#endif //IsInputHIDAPI()