// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputHIDAPI()
#include "FXDEngine/Input/HIDAPI/HidApiManager.h"
#include "FXDEngine/Input/HIDAPI/HIDAPIDevice.h"
#include "FXDEngine/Input/HIDAPI/HidApiJPThread.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Memory/MemHandle.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace Input;
using namespace HidApi;

// ------
// IHidApiManager
// -
// ------
IHidApiManager::IHidApiManager(void)
{
}

IHidApiManager::~IHidApiManager(void)
{
	//PRINT_COND_ASSERT((m_inputDevices.size() == 0), "Input: m_inputDevices are not shutdown");
}

Job::Future< bool > IHidApiManager::create_system(void)
{
	auto funcRef = [&](void) { return _create_system(); };

	return Job::Async(FXDHidApi(), [=]()
	{
		return funcRef();
	});
}

Job::Future< bool > IHidApiManager::release_system(void)
{
	auto funcRef = [&](void) { return _release_system(); };

	return Job::Async(FXDHidApi(), [=]()
	{
		return funcRef();
	});
}

FXD::S32 IHidApiManager::read_input(const HidApi::HidApiDevice* pDevice, FXD::U8* pBuf, FXD::U8 nBufSize, FXD::S32 nTimeout)
{
	//PRINT_COND_ASSERT((!HidApi::Funcs::IsHidApiThreadRestricted()), "Input: Not allowed on current thread " << Thread::Funcs::GetCurrentThreadID());

	// Make sure we don't try to read at the same time a write is happening

	/*
	if (SDL_AtomicGet(&ctx->device->rumble_pending) > 0)
	{
		return 0;
	}
	*/
	return hid_read_timeout(pDevice->device(), pBuf, nBufSize, nTimeout);
}


bool IHidApiManager::submit_rumble_request(const HidApi::HidApiDevice* pDevice, const Memory::MemHandle& memHandle)
{
	//PRINT_COND_ASSERT((!HidApi::Funcs::IsHidApiThreadRestricted()), "Input: Not allowed on current thread " << Thread::Funcs::GetCurrentThreadID());
	PRINT_COND_ASSERT((SIZEHERE > memHandle.get_size()), "Input: Couldn't send rumble package, the package size is too big.");

	Container::Map< const HidApi::HidApiDevice*, HidApi::HidApiRumbleRequest >::iterator itr = m_rumbleRequests.find(pDevice);
	if (itr != m_rumbleRequests.end())
	{
		itr->second.m_nRequestSize = memHandle.get_size();
		Memory::MemCopy(itr->second.m_pData, memHandle.get_data(), memHandle.get_size());
		return true;
	}

	m_rumbleRequests.emplace(pDevice, HidApi::HidApiRumbleRequest(pDevice, (const FXD::U8*)memHandle.get_data(), memHandle.get_size()));
	return memHandle.get_size();
}

void IHidApiManager::_process_requests(FXD::F32 /*dt*/)
{
	PRINT_COND_ASSERT((!HidApi::Funcs::IsHidApiThreadRestricted()), "Input: Not allowed on current thread " << Thread::Funcs::GetCurrentThreadID());

	fxd_for(auto request, m_rumbleRequests)
	{
		hid_write(request.second.m_pDevice->device(), request.second.m_pData, request.second.m_nRequestSize);
	}
	m_rumbleRequests.clear();
}


bool IHidApiManager::read_feature_report(const HidApi::HidApiDevice* pDevice, FXD::U8 nReportID, Memory::MemHandle& memHandle)
{
	//PRINT_COND_ASSERT((!HidApi::Funcs::IsHidApiThreadRestricted()), "Input: Not allowed on current thread " << Thread::Funcs::GetCurrentThreadID());
	FXD::U8 pReport[USB_PACKET_LENGTH + 1];
	Memory::MemZero_T(pReport);
	pReport[0] = nReportID;

	FXD::S32 nRetVal = 0;
	if ((nRetVal = hid_get_feature_report(pDevice->device(), pReport, sizeof(pReport))) < 0)
	{
		return false;
	}

	Memory::MemHandle::LockGuard lock(memHandle);
	Memory::MemCopy(lock.get_mem(), pReport, FXD::STD::Min(memHandle.get_size(), (FXD::U32)sizeof(pReport)));
	return true;
}

bool IHidApiManager::_create_system(void)
{
	FXD::S32 nRetVal = hid_init();
	return (nRetVal >= 0);
}

bool IHidApiManager::_release_system(void)
{
	FXD::S32 nRetVal = hid_exit();
	return (nRetVal >= 0);
}

void IHidApiManager::_process(FXD::F32 dt)
{
	PRINT_COND_ASSERT((!HidApi::Funcs::IsHidApiThreadRestricted()), "Input: Not allowed on current thread " << Thread::Funcs::GetCurrentThreadID());

#if defined(FXD_ENABLE_WATCHDOG)
	static Core::WatchDogThread sWDThread("HidApiManager - WatchDog", 500);
	sWDThread.increment();
#endif

	_process_requests(dt);
}

/*
FXD::S32 IHidApiManager::write_output(const HidApi::HidApiDevice* pHDevice, const Memory::MemHandle& memHandle)
{
	Job::Future< FXD::S32 > fut = Job::Async(FXDAsync(), [&]()
	{
		FXD::S32 nRetVal = add_rumble_request(pHDevice, memHandle);
		return nRetVal;
	});
	return fut.get();
}
*/

FXD::S32 IHidApiManager::WritePacket(const HidApi::HidApiDevice* pDevice, FXD::U8* pBuf, FXD::U32 nBufSize)
{
	FXD::S32 nRetVal = hid_write(pDevice->device(), pBuf, nBufSize);
	if (nRetVal > 0)
	{
		return nRetVal;
	}
	return nRetVal;
}

namespace
{
	bool ReadPacket(const HidApi::HidApiDevice* pDevice, FXD::U8* pBuf, FXD::U32 nBufSize, FXD::S32 nCommandId, FXD::S32 nTimeout)
	{
		FXD::S32 nRetVal = 0;
		while ((nRetVal = hid_read_timeout(pDevice->device(), pBuf, nBufSize, nTimeout)) != -1)
		{
			if (nRetVal > 0)
			{
				if (pBuf[0] == nCommandId)
				{
					return true;
				}
			}
			else
			{
				Thread::Funcs::Sleep(100);
			}
		}
		return false;
	}
}


bool IHidApiManager::_process_write(const HidApi::HidApiDevice* pDevice, FXD::U8* pBuf, FXD::U32 nBufSize, FXD::S32 nCommandId, FXD::S32 nTimeout, FXD::S32 nRetries, bool bWaitForReply)
{
	while (nRetries--)
	{
		if (WritePacket(pDevice, pBuf, nBufSize) == false)
		{
			continue;
		}
		if (bWaitForReply == false)
		{
			return true;
		}
		if (ReadPacket(pDevice, pBuf, nBufSize, nCommandId, nTimeout) == false)
		{
			return true;
		}
	}
	return false;
}

#endif //IsInputHIDAPI()