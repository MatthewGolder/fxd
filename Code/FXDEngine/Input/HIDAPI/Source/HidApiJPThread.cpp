// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputHIDAPI()
#include "FXDEngine/Input/HIDAPI/HidApiJPThread.h"
#include "FXDEngine/Input/HIDAPI/HidApiManager.h"

using namespace FXD;
using namespace Input;
using namespace HidApi;

// 0 - Not Initialised
// 1 - Initialised
// 2 - ShutDown
FXD::S32 g_nHidApiStatus = 0;

// ------
// IHidApiJPThread
// -
// ------
IHidApiJPThread::IHidApiJPThread(void)
	: IAsyncTaskPoolThread(display_name())
	, m_hidApiManager(nullptr)
{
	if (g_nHidApiStatus == 2)
	{
		PRINT_ASSERT << "Input::HidApi::IHidApiJPThread accessed after shutdown";
	}
}

IHidApiJPThread::~IHidApiJPThread(void)
{
	stop_and_wait_for_exit();
}

bool IHidApiJPThread::init_pool(void)
{
	HidApi::Funcs::RestrictHidApiThread();
	PRINT_INFO << "Input: IHidApiJPThread::init_pool()";

	m_hidApiManager = std::make_unique< HidApi::IHidApiManager >();
	m_timer.start_counter();
	g_nHidApiStatus = 1;
	m_shared.get_quick_write_lock()->m_eState = Job::E_AsyncState::Running;
	return true;
}

bool IHidApiJPThread::shutdown_pool(void)
{
	m_hidApiManager = nullptr;
	HidApi::Funcs::UnrestrictHidApiThread();
	PRINT_INFO << "Input: IInputJPThread::shutdown_pool()";

	g_nHidApiStatus = 2;
	m_shared.get_quick_write_lock()->m_eState = Job::E_AsyncState::ShutDown;
	return true;
}

void IHidApiJPThread::idle_process(void)
{
	if (is_shuttingdown())
	{
		return;
	}

	const FXD::S32 nProcessFrequency = 5;

	// 1. Get initial time stamp.
	FXD::F32 ts1 = (FXD::F32)Core::Funcs::GetTimeSecsF();

	FXD::F32 dt = m_timer.elapsed_time();
	m_fps.update(dt);
	if (m_hidApiManager)
	{
		m_hidApiManager->_process(dt);
	}

	// 2. Get final time stamp and wait for next update interval.
	FXD::F32 ts2 = (FXD::F32)Core::Funcs::GetTimeSecsF();
	FXD::S32 nToWait = nProcessFrequency - (FXD::S32)((ts2 - ts1) * 1000.0f);
	if (nToWait > 0)
	{
		Thread::Funcs::Sleep(nToWait);
	}
}

bool IHidApiJPThread::is_thread_restricted(void) const
{
	return HidApi::Funcs::IsHidApiThreadRestricted();
}

// ------
// Funcs
// -
// ------
static FXD::S32 g_ContextThread = 0;
bool FXD::Input::HidApi::Funcs::IsHidApiThreadRestricted(void)
{
	return (g_ContextThread != 0) && (g_ContextThread != Thread::Funcs::GetCurrentThreadID());
}

void FXD::Input::HidApi::Funcs::RestrictHidApiThread(void)
{
	PRINT_COND_ASSERT((!Funcs::IsHidApiThreadRestricted()), "Input: Not allowed on current thread " << Thread::Funcs::GetCurrentThreadID());
	g_ContextThread = Thread::Funcs::GetCurrentThreadID();
}

void FXD::Input::HidApi::Funcs::UnrestrictHidApiThread(void)
{
	PRINT_COND_ASSERT((!Funcs::IsHidApiThreadRestricted()), "Input: Not allowed on current thread " << Thread::Funcs::GetCurrentThreadID());
	g_ContextThread = 0;
}
#endif //IsInputHIDAPI()