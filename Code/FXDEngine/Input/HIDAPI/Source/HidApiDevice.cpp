// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputHIDAPI()
#include "FXDEngine/Input/HIDAPI/HidApiDevice.h"
#include "FXDEngine/Input/InputApi.h"

using namespace FXD;
using namespace Input;
using namespace HidApi;

// ------
// HidApiDevice
// -
// ------
HidApiDevice::HidApiDevice(struct hid_device_info* pDevs)
	: m_pDev(nullptr)
	, m_strPath(pDevs->path)
	, m_eVendorId(Funcs::GetUSBVendor(pDevs->vendor_id))
	, m_nProductID(pDevs->product_id)
	, m_nVersion(pDevs->release_number)
	, m_nInterfaceNumber(pDevs->interface_number)
	, m_nInterfaceClass(pDevs->interface_class)
	, m_nInterfaceSubclass(pDevs->interface_subclass)
	, m_nInterfaceProtocol(pDevs->interface_protocol)
{
	m_strName = Funcs::CreateControllerName(m_eVendorId, m_nProductID, pDevs->manufacturer_string, pDevs->product_string);
	m_guid.set_guid_info(m_eVendorId, m_nProductID, m_nVersion);

	if (!should_ignore())
	{
		m_pDev = hid_open_path(m_strPath.c_str(), 0);
	}
}

HidApiDevice::~HidApiDevice(void)
{
	if (m_pDev != nullptr)
	{
		hid_close(m_pDev);
		m_pDev = nullptr;
	}
	PRINT_COND_ASSERT((m_pDev == nullptr), "Input: m_pDev is not shutdown");
}

bool HidApiDevice::should_ignore(void)const
{
	return Funcs::ShouldIgnoreController(m_strName.c_str(), m_guid);
}

Input::HidApi::E_HidControllerType HidApiDevice::controller_type(void)const
{
	return Funcs::GetGameControllerType(m_strName.c_str(), m_eVendorId, m_nProductID, m_nInterfaceNumber, m_nInterfaceClass, m_nInterfaceSubclass, m_nInterfaceProtocol);
}
#endif //IsInputHIDAPI()