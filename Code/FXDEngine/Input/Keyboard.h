// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_KEYBOARD_H
#define FXDENGINE_INPUT_KEYBOARD_H

#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/Input/InputDevice.h"

namespace FXD
{
	namespace Input
	{
		enum class E_SpecialKey
		{
			None = 0,
			Up,
			Down,
			Left,
			Right,
			Home,
			End,
			Backspace,
			Delete,
			Escape,
			Count,
			Unknown = 0xffff
		};

		enum class E_ModifierKey
		{
			LeftShift = 0,
			RightShift,
			LeftControl,
			RightControl,
			LeftAlt,
			RightAlt,
			Count,
			Unknown = 0xffff
		};

		// ------
		// IKeyboard
		// -
		// ------
		class IKeyboard FINAL : public Core::HasImpl< Input::IKeyboard, 1024 >, public Input::IInputDevice
		{
		public:
			IKeyboard(void);
			virtual ~IKeyboard(void);
			
			Core::String8 get_config_name(void) const FINAL;
			Core::String8 get_device_name(void) const FINAL;
			Input::E_DeviceType get_device_type(void) const FINAL;

			bool is_connected(void) const FINAL						{ return true; }
			bool is_keyboard_available(void) const FINAL			{ return true; }
			bool is_modifier_key_pressed(Input::E_ModifierKey eMKey) const;

		protected:
			void _update(FXD::F32 dt) FINAL;

			const Widget::WIDGET_DESC* _get_widget_defs(void) const FINAL;
			void _get_widget_state(FXD::U16 nID, void* pState) const FINAL;
		};

	} //namespace Input
} //namespace FXD
#endif //FXDENGINE_INPUT_KEYBOARD_H