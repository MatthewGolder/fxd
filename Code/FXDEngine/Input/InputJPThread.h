// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_INPUTJPTHREAD_H
#define FXDENGINE_INPUT_INPUTJPTHREAD_H

#include "FXDEngine/Core/Timer.h"
#include "FXDEngine/Core/FPS.h"
#include "FXDEngine/Input/Types.h"
#include "FXDEngine/Job/AsyncJob.h"

namespace FXD
{
	namespace Input
	{
		// ------
		// IInputJPThread
		// -
		// ------
		class IInputJPThread : public Job::IAsyncTaskPoolThread
		{
		public:
			IInputJPThread(void);
			virtual ~IInputJPThread(void);

			const Core::StringView8 display_name(void) const FINAL { return UTF_8("InputThread"); }

			GET_REF_R(Input::InputSystem, inputSystem, input_system);

		protected:
			bool init_pool(void) FINAL;
			bool shutdown_pool(void) FINAL;
			void idle_process(void) FINAL;
			bool is_thread_restricted(void) const FINAL;

		private:
			Core::FPS m_fps;
			Core::Timer m_timer;
			Input::InputSystem m_inputSystem;
		};

		namespace Funcs
		{
			extern bool IsInputThreadRestricted(void);
			extern void RestrictInputThread(void);
			extern void UnrestrictInputThread(void);
		} //namespace Funcs

	} //namespace Input
} //namespace FXD
#endif //FXDENGINE_INPUT_INPUTJPTHREAD_H
