// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_ANDROID_GAMEPADANDROID_H
#define FXDENGINE_INPUT_ANDROID_GAMEPADANDROID_H

#include "FXDEngine/Input/Types.h"

#if IsInputGooglePlay()
#include "FXDEngine/Input/Events.h"
#include "FXDEngine/Input/InputDevice.h"
#include "FXDEngine/Input/Android/Android.h"

namespace FXD
{
	namespace Input
	{
		// ------
		// IGamePadAndroid
		// -
		// ------
		class IGamePadAndroid FINAL : public Input::IInputDevice
		{			
		public:
			// ------
			// AndroidInputContext
			// -
			// ------
			struct AndroidInputContext
			{
				AndroidInputContext(const FXD::S32 nPort);

				bool m_bConnected;
				FXD::U32 m_nPortIndex;
			};

		public:
			IGamePadAndroid(const FXD::S32 nPort);
			~IGamePadAndroid(void);

			bool is_connected(void) const FINAL;
			bool is_gamepad_available(void) const FINAL;
			bool supports_rumble(void) const FINAL;

			Core::String8 get_config_name(void) const FINAL;
			Core::String8 get_device_name(void) const FINAL;
			Input::E_DeviceType get_device_type(void) const FINAL;

		protected:
			void _update(FXD::F32 dt) FINAL;

			const Widget::WIDGET_DESC* _get_widget_defs(void) const FINAL;
			void _get_widget_state(FXD::U16 nID, void* pState) const FINAL;

		private:
			AndroidInputContext m_context;
			const FXD::Job::Handler< Input::OnControllerButton >* OnControllerButton;
			const FXD::Job::Handler< Input::OnControllerAnalog >* OnControllerAnalog;
		};

	} //namespace Input
} //namespace FXD

#endif //IsInputGooglePlay()
#endif //FXDENGINE_INPUT_ANDROID_GAMEPADANDROID_H