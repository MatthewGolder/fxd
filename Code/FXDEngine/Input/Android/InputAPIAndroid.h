// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_ANDROID_INPUTAPIANDROID_H
#define FXDENGINE_INPUT_ANDROID_INPUTAPIANDROID_H

#include "FXDEngine/Input/Types.h"

#if IsInputGooglePlay()
#include "FXDEngine/Input/InputApi.h"

namespace FXD
{
	namespace Input
	{
		// ------
		// IInputApiAndroid
		// -
		// ------
		class IInputApiAndroid FINAL : public Input::IInputApi
		{
		public:
			IInputApiAndroid(void);
			~IInputApiAndroid(void);

			bool create_api(void) FINAL;
			bool release_api(void) FINAL;

		private:
			void _process(FXD::F32 dt) FINAL;

		private:
			Container::Vector< Input::InputDevice > m_detectedDevices;
		};

	} //namespace Input
} //namespace FXD

#endif //IsInputGooglePlay()
#endif //FXDENGINE_INPUT_ANDROID_INPUTAPIANDROID_H