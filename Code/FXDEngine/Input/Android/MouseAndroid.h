// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_ANDROID_MOUSEANDROID_H
#define FXDENGINE_INPUT_ANDROID_MOUSEANDROID_H

#include "FXDEngine/Input/Types.h"

#if IsInputGooglePlay()
#include "FXDEngine/Input/Events.h"
#include "FXDEngine/Input/Mouse.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< Input::IMouse >
			// -
			// ------
			template <>
			class Impl< Input::IMouse > FINAL
			{
			public:
				friend class Input::IMouse;

			public:
				Impl(void);
				~Impl(void);

			private:
				const FXD::Job::Handler< Input::OnMouseButton >* OnMouseButton;
				const FXD::Job::Handler< Input::OnMouseWheel >* OnMouseWheel;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD

#endif //IsInputGooglePlay()
#endif //FXDENGINE_INPUT_ANDROID_MOUSEANDROID_H