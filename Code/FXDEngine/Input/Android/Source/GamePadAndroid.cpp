// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputGooglePlay()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Input/Mapping.h"
#include "FXDEngine/Input/Android/GamePadAndroid.h"
#include "FXDEngine/Job/EventHandler.h"

using namespace FXD;
using namespace Input;

// ------
// IGamePadAndroid::AndroidInputContext
// -
// ------
IGamePadAndroid::AndroidInputContext::AndroidInputContext(const FXD::S32 nPort)
	: m_bConnected(false)
	, m_nPortIndex(0)
{
}


// ------
// IGamePadAndroid
// -
// ------
IGamePadAndroid::IGamePadAndroid(const FXD::S32 nPort)
	: IInputDevice()
	, m_context(nPort)
	, OnControllerButton(nullptr)
	, OnControllerAnalog(nullptr)
{
	//Memory::MemZero_T(m_state);
	OnControllerButton = FXDEvent()->attach< Input::OnControllerButton >([&](Input::OnControllerButton evt)
	{
		if (evt.Device == this)
		{
			PRINT_INFO << evt.Pressed;
		}
	});
	OnControllerAnalog = FXDEvent()->attach< Input::OnControllerAnalog >([&](Input::OnControllerAnalog evt)
	{
		if (evt.Device == this)
		{
			PRINT_INFO << evt.Value;
		}
	});
}

IGamePadAndroid::~IGamePadAndroid(void)
{
	if (OnControllerButton != nullptr)
	{
		FXDEvent()->detach(OnControllerButton);
		OnControllerButton = nullptr;
	}
	if (OnControllerAnalog != nullptr)
	{
		FXDEvent()->detach(OnControllerAnalog);
		OnControllerAnalog = nullptr;
	}
}

bool IGamePadAndroid::is_connected(void) const
{
	return (m_context.m_bConnected);
}

bool IGamePadAndroid::is_gamepad_available(void) const
{
	return (m_context.m_bConnected);
}

bool IGamePadAndroid::supports_rumble(void) const
{
	return false;
}

Core::String8 IGamePadAndroid::get_config_name(void) const
{
	return UTF_8("GamePadAndroid");
}

Core::String8 IGamePadAndroid::get_device_name(void) const
{
	return UTF_8("GamePadAndroid");
}

Input::E_DeviceType IGamePadAndroid::get_device_type(void) const
{
	return Input::E_DeviceType::Controller;
}

void IGamePadAndroid::_update(FXD::F32 /*dt*/)
{
	FXD_ERROR("IGamePadAndroid::_update");
}

static CONSTEXPR const Widget::WIDGET_DESC kGamePadWidgets[] =
{
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::DPadUp), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::DPadUp), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::DPadDown), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::DPadDown), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::DPadLeft), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::DPadLeft), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::DPadRight), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::DPadRight), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftThumbX), Input::E_Widget_type::FullAxis, Input::GetGamePadName(E_GamePadButtons::LeftThumbX), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftThumbY), Input::E_Widget_type::FullAxis, Input::GetGamePadName(E_GamePadButtons::LeftThumbY), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightThumbX), Input::E_Widget_type::FullAxis, Input::GetGamePadName(E_GamePadButtons::RightThumbX), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightThumbY), Input::E_Widget_type::FullAxis, Input::GetGamePadName(E_GamePadButtons::RightThumbY), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftThumb), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::LeftThumb), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightThumb), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::RightThumb), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftBumper), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::LeftBumper), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightBumper), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::RightBumper), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftTrigger), Input::E_Widget_type::HalfAxis, Input::GetGamePadName(E_GamePadButtons::LeftTrigger), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightTrigger), Input::E_Widget_type::HalfAxis, Input::GetGamePadName(E_GamePadButtons::RightTrigger), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::Start), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::Start), Widget::kWidgetFlagStartButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::Back), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::Back), Widget::kWidgetFlagBackButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::AButton), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::AButton), Widget::kWidgetFlagActionButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::BButton), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::BButton), Widget::kWidgetFlagBackButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::XButton), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::XButton), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::YButton), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::YButton), 0),
	Widget::WIDGET_DESC()
};

const Widget::WIDGET_DESC* IGamePadAndroid::_get_widget_defs(void) const
{
	return kGamePadWidgets;
}

void IGamePadAndroid::_get_widget_state(FXD::U16 nID, void* pState) const
{
	FXD_ERROR("IGamePadAndroid::_get_widget_state");
}
#endif //IsInputGooglePlay()