// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputGooglePlay()
#include "FXDEngine/Input/Android/InputApiAndroid.h"

using namespace FXD;
using namespace Input;

// ------
// IInputApiAndroid
// - 
// ------
IInputApiAndroid::IInputApiAndroid(void)
{
}

IInputApiAndroid::~IInputApiAndroid(void)
{
}

bool IInputApiAndroid::create_api(void)
{
	return true;
}

bool IInputApiAndroid::release_api(void)
{
	return true;
}

void IInputApiAndroid::_process(FXD::F32 dt)
{
}
#endif //IsInputGooglePlay()