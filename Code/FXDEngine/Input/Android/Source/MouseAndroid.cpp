// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputGooglePlay()
#include "FXDEngine/Input/Android/MouseAndroid.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Input/Mapping.h"
#include "FXDEngine/Job/EventHandler.h"

using namespace FXD;
using namespace Input;

// ------
// IMouse::Impl
// -
// ------
IMouse::Impl::Impl::Impl(void)
	: OnMouseButton(nullptr)
	, OnMouseWheel(nullptr)
{
	OnMouseButton = FXDEvent()->attach< Input::OnMouseButton >([&](auto evt)
	{
		PRINT_INFO << evt.Pressed;
	});
	OnMouseWheel = FXDEvent()->attach< Input::OnMouseWheel >([&](auto evt)
	{
		PRINT_INFO << evt.Value;
	});
}

IMouse::Impl::~Impl(void)
{
	if (OnMouseButton != nullptr)
	{
		FXDEvent()->detach(OnMouseButton);
		OnMouseButton = nullptr;
	}
	if (OnMouseWheel != nullptr)
	{
		FXDEvent()->detach(OnMouseWheel);
		OnMouseWheel = nullptr;
	}
}

// ------
// IMouse
// -
// ------
IMouse::IMouse(void)
{
}

IMouse::~IMouse(void)
{
}

bool IMouse::is_connected(void) const
{
	return true;
}

bool IMouse::is_mouse_available(void) const
{
	return true;
}

Core::String8 IMouse::get_config_name(void) const
{
	return UTF_8("Mouse");
}

Core::String8 IMouse::get_device_name(void) const
{
	return UTF_8("Mouse");
}

Input::E_DeviceType IMouse::get_device_type(void) const
{
	return Input::E_DeviceType::Mouse;
}

FXD::F32 IMouse::get_scroll_wheel_pos(void) const
{
	FXD_ERROR("IMouse::get_scroll_wheel_pos");
	return 0.0f;
}

void IMouse::_update(FXD::F32 /*dt*/)
{
	FXD_ERROR("IMouse::_update");
}

static CONSTEXPR const Widget::WIDGET_DESC kMouseWidgets[] =
{
	Widget::WIDGET_DESC(Input::GetMouseID(E_MouseButtons::Left), Input::E_Widget_type::Digital, Input::GetMouseName(E_MouseButtons::Left), 0),
	Widget::WIDGET_DESC(Input::GetMouseID(E_MouseButtons::Middle), Input::E_Widget_type::Digital, Input::GetMouseName(E_MouseButtons::Middle), 0),
	Widget::WIDGET_DESC(Input::GetMouseID(E_MouseButtons::Right), Input::E_Widget_type::Digital, Input::GetMouseName(E_MouseButtons::Right), 0),
	Widget::WIDGET_DESC(Input::GetMouseID(E_MouseButtons::Thumb01), Input::E_Widget_type::Digital, Input::GetMouseName(E_MouseButtons::Thumb01), 0),
	Widget::WIDGET_DESC(Input::GetMouseID(E_MouseButtons::Thumb02), Input::E_Widget_type::Digital, Input::GetMouseName(E_MouseButtons::Thumb02), 0),
	Widget::WIDGET_DESC()
};

const Widget::WIDGET_DESC* IMouse::_get_widget_defs(void) const
{
	return kMouseWidgets;
}

void IMouse::_get_widget_state(FXD::U16 nID, void* pState) const
{
}

/*
Math::Vector IMouse::get_pointer_pos(const GFX::Viewport& display) const
{
	Math::Vector pos(0, 0);
	const GFX::IViewport::Impl &d = display->impl();
	if(d.m_clientRectValid)
	{
		pos.x((FXD::F32)(m_impl->m_mousePos.x - d.m_clientRect.left) / (FXD::F32)(d.m_clientRect.right - d.m_clientRect.left));
		pos.y((FXD::F32)(m_impl->m_mousePos.y - d.m_clientRect.top) / (FXD::F32)(d.m_clientRect.bottom - d.m_clientRect.top));
	}
	return pos;
}

bool IMouse::is_active_in_display(const GFX::Viewport& display) const
{
	const GFX::IViewport::Impl &d = display->impl();
	if(!d.m_clientRectValid)
		return false;
	if(IsIconic(d.m_hWnd))
		return false;
	if(GetForegroundWindow() != d.m_hWnd)
		return false;
	return true;
}
*/
#endif //IsInputGooglePlay()