// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputGooglePlay()
#include "FXDEngine/Input/InputSystem.h"
#include "FXDEngine/Input/InputDevice.h"
#include "FXDEngine/Input/Android/MouseAndroid.h"
#include "FXDEngine/Input/Android/GamePadAndroid.h"
#include "FXDEngine/Input/Android/InputApiAndroid.h"
#include "FXDEngine/Input/Android/KeyboardAndroid.h"

using namespace FXD;
using namespace Input;

// ------
// IInputSystem
// -
// ------
bool IInputSystem::_create_system(void)
{
	// 1. Early out if the api already exists
	if (m_inputApis.not_empty())
	{
		return false;
	}

	// 2. Create the desired apis
#if IsInputGooglePlay()
	m_inputApis.push_back(std::make_unique< Input::IInputApiAndroid >());
#endif //IsInputPS4()

	// 3. Create the desired apis
	bool bRetVal = true;
	fxd_for(auto& api, m_inputApis)
	{
		if (!api->create_api())
		{
			bRetVal = false;
		}
	}

	return bRetVal;
}

bool IInputSystem::_release_system(void)
{
	fxd_for(auto& api, m_inputApis)
	{
		FXD_RELEASE(api, release_api());
	}
	m_inputApis.clear();

	return true;
}

bool IInputSystem::_create_devices(void)
{
	_register_device(std::make_shared< Input::IMouse >());
	_register_device(std::make_shared< Input::IKeyboard >());

	for (FXD::U32 n1 = 0; n1 < 4; n1++)
	{
		_register_device(std::make_shared< Input::IGamePadAndroid >(n1));
	}

	return true;
}

bool IInputSystem::_release_devices(void)
{
	while (m_unusedDevices.not_empty())
	{
		_unregister_device(m_unusedDevices[0]);
	}

	m_unusedDevices.clear();
	return true;
}
#endif //IsInputGooglePlay()