// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_ANDROID_ANDROID_H
#define FXDENGINE_INPUT_ANDROID_ANDROID_H

#include "FXDEngine/Input/Types.h"

#if IsInputGooglePlay()

namespace FXD
{
	namespace Input
	{
		class IGamePadAndroid;
		typedef std::shared_ptr< Input::IGamePadAndroid > GamePadAndroid;
	} //namespace Input
} //namespace FXD
#endif //IsInputGooglePlay()
#endif //FXDENGINE_INPUT_ANDROID_ANDROID_H