// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_ANDROID_KEYBOARDANDROID_H
#define FXDENGINE_INPUT_ANDROID_KEYBOARDANDROID_H

#include "FXDEngine/Input/Types.h"

#if IsInputGooglePlay()
#include "FXDEngine/Input/Keyboard.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< Input::IKeyboard >
			// -
			// ------
			template <>
			class Impl< Input::IKeyboard > FINAL
			{
			public:
				friend class Input::IKeyboard;

			public:
				Impl(void);
				~Impl(void);

			private:
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD

#endif //IsInputGooglePlay()
#endif //FXDENGINE_INPUT_ANDROID_KEYBOARDANDROID_H