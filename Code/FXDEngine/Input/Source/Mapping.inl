// Creator - MatthewGolder
#include "FXDEngine/Core/StringView.h"
#include "FXDEngine/Core/TypeTraits.h"
#include "FXDEngine/Input/Types.h"

using namespace FXD;
using namespace Input;

namespace
{
	// ------
	// MouseButtonsDesc
	// -
	// ------
	struct MouseButtonsDesc
	{
		const Core::StringView8 strName;
		const Input::E_MouseButtons eInput;
		const FXD::U16 nID;
	};

	CONSTEXPR const MouseButtonsDesc kMouseDescs[] =
	{
		//-----------------------------------------------------------------------
		{
			"Left",
			Input::E_MouseButtons::Left,
			FXD::STD::to_underlying(Input::E_MouseButtons::Left),
		},
		//-----------------------------------------------------------------------
		{
			"Right",
			Input::E_MouseButtons::Right,
			FXD::STD::to_underlying(Input::E_MouseButtons::Right),
		},
		//-----------------------------------------------------------------------
		{
			"Middle",
			Input::E_MouseButtons::Middle,
			FXD::STD::to_underlying(Input::E_MouseButtons::Middle),
		},
		//-----------------------------------------------------------------------
		{
			"Thumb01",
			Input::E_MouseButtons::Thumb01,
			FXD::STD::to_underlying(Input::E_MouseButtons::Thumb01),
		},
		//-----------------------------------------------------------------------
		{
			"Thumb02",
			Input::E_MouseButtons::Thumb02,
			FXD::STD::to_underlying(Input::E_MouseButtons::Thumb02),
		},
		//-----------------------------------------------------------------------
		{
			"Unknown",
			Input::E_MouseButtons::Unknown,
			FXD::STD::to_underlying(Input::E_MouseButtons::Unknown),
		}
	};

	CONSTEXPR const MouseButtonsDesc& getMouseDesc(const Input::E_MouseButtons eMouse)
	{
		return kMouseDescs[FXD::STD::to_underlying(eMouse)];
	}

	// ------
	// GamePadButtonsDesc
	// -
	// ------
	struct GamePadButtonsDesc
	{
		const Core::StringView8 strName;
		const Input::E_GamePadButtons eInput;
		const FXD::U16 nID;
	};

	CONSTEXPR const GamePadButtonsDesc kGamePadDescs[] =
	{
		//-----------------------------------------------------------------------
		{
			"DPadUp",
			Input::E_GamePadButtons::DPadUp,
			FXD::STD::to_underlying(Input::E_GamePadButtons::DPadUp),
		},
		//-----------------------------------------------------------------------
		{
			"DPadDown",
			Input::E_GamePadButtons::DPadDown,
			FXD::STD::to_underlying(Input::E_GamePadButtons::DPadDown),
		},
		//-----------------------------------------------------------------------
		{
			"DPadLeft",
			Input::E_GamePadButtons::DPadLeft,
			FXD::STD::to_underlying(Input::E_GamePadButtons::DPadLeft),
		},
		//-----------------------------------------------------------------------
		{
			"DPadRight",
			Input::E_GamePadButtons::DPadRight,
			FXD::STD::to_underlying(Input::E_GamePadButtons::DPadRight),
		},
		//-----------------------------------------------------------------------
		{
			"LeftThumbX",
			Input::E_GamePadButtons::LeftThumbX,
			FXD::STD::to_underlying(Input::E_GamePadButtons::LeftThumbX),
		},
		//-----------------------------------------------------------------------
		{
			"LeftThumbY",
			Input::E_GamePadButtons::LeftThumbY,
			FXD::STD::to_underlying(Input::E_GamePadButtons::LeftThumbY),
		},
		//-----------------------------------------------------------------------
		{
			"RightThumbX",
			Input::E_GamePadButtons::RightThumbX,
			FXD::STD::to_underlying(Input::E_GamePadButtons::RightThumbX),
		},
		//-----------------------------------------------------------------------
		{
			"RightThumbY",
			Input::E_GamePadButtons::RightThumbY,
			FXD::STD::to_underlying(Input::E_GamePadButtons::RightThumbY),
		},
		//-----------------------------------------------------------------------
		{
			"LeftThumb",
			Input::E_GamePadButtons::LeftThumb,
			FXD::STD::to_underlying(Input::E_GamePadButtons::LeftThumb),
		},
		//-----------------------------------------------------------------------
		{
			"RightThumb",
			Input::E_GamePadButtons::RightThumb,
			FXD::STD::to_underlying(Input::E_GamePadButtons::RightThumb),
		},
		//-----------------------------------------------------------------------
		{
			"LeftBumper",
			Input::E_GamePadButtons::LeftBumper,
			FXD::STD::to_underlying(Input::E_GamePadButtons::LeftBumper),
		},
		//-----------------------------------------------------------------------
		{
			"RightBumper",
			Input::E_GamePadButtons::RightBumper,
			FXD::STD::to_underlying(Input::E_GamePadButtons::RightBumper),
		},
		//-----------------------------------------------------------------------
		{
			"LeftTrigger",
			Input::E_GamePadButtons::LeftTrigger,
			FXD::STD::to_underlying(Input::E_GamePadButtons::LeftTrigger),
		},
		//-----------------------------------------------------------------------
		{
			"RightTrigger",
			Input::E_GamePadButtons::RightTrigger,
			FXD::STD::to_underlying(Input::E_GamePadButtons::RightTrigger),
		},
		//-----------------------------------------------------------------------
		{
			"Start",
			Input::E_GamePadButtons::Start,
			FXD::STD::to_underlying(Input::E_GamePadButtons::Start),
		},
		//-----------------------------------------------------------------------
		{
			"Back",
			Input::E_GamePadButtons::Back,
			FXD::STD::to_underlying(Input::E_GamePadButtons::Back),
		},
		//-----------------------------------------------------------------------
		{
			"AButton",
			Input::E_GamePadButtons::AButton,
			FXD::STD::to_underlying(Input::E_GamePadButtons::AButton),
		},
		//-----------------------------------------------------------------------
		{
			"BButton",
			Input::E_GamePadButtons::BButton,
			FXD::STD::to_underlying(Input::E_GamePadButtons::BButton),
		},
		//-----------------------------------------------------------------------
		{
			"XButton",
			Input::E_GamePadButtons::XButton,
			FXD::STD::to_underlying(Input::E_GamePadButtons::XButton),
		},
		//-----------------------------------------------------------------------
		{
			"YButton",
			Input::E_GamePadButtons::YButton,
			FXD::STD::to_underlying(Input::E_GamePadButtons::YButton),
		},
		//-----------------------------------------------------------------------
		{
			"Guide",
			Input::E_GamePadButtons::Guide,
			FXD::STD::to_underlying(Input::E_GamePadButtons::Guide),
		},
		//-----------------------------------------------------------------------
		{
			"Touchpad",
			Input::E_GamePadButtons::Touchpad,
			FXD::STD::to_underlying(Input::E_GamePadButtons::Touchpad),
		},
		//-----------------------------------------------------------------------
		{
			"Capture",
			Input::E_GamePadButtons::Capture,
			FXD::STD::to_underlying(Input::E_GamePadButtons::Capture),
		},
		//-----------------------------------------------------------------------
		{
			"SL",
			Input::E_GamePadButtons::SL,
			FXD::STD::to_underlying(Input::E_GamePadButtons::SL),
		},
		//-----------------------------------------------------------------------
		{
			"SR",
			Input::E_GamePadButtons::SR,
			FXD::STD::to_underlying(Input::E_GamePadButtons::SR),
		},
		//-----------------------------------------------------------------------
		{
			"Unknown",
			Input::E_GamePadButtons::Unknown,
			FXD::STD::to_underlying(Input::E_GamePadButtons::Unknown),
		}
	};

	CONSTEXPR const GamePadButtonsDesc& getGamepadDesc(const Input::E_GamePadButtons eGamePad)
	{
		return kGamePadDescs[FXD::STD::to_underlying(eGamePad)];
	}
}