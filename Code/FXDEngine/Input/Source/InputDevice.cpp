// Creator - MatthewGolder
#include "FXDEngine/Input/InputDevice.h"

using namespace FXD;
using namespace Input;

// ------
// IInputDevice
// -
// ------
IInputDevice::IInputDevice(void)
	: m_ePowerLevel(Input::E_PowerLevel::Unknown)
{
}

IInputDevice::~IInputDevice(void)
{
}

bool IInputDevice::is_any_button_pressed(void) const
{
	const Widget::WIDGET_DESC* pDefs = _get_widget_defs();
	for (FXD::U16 n1 = 0; pDefs[n1].Type != Input::E_Widget_type::Count; n1++)
	{
		if (pDefs[n1].Type == Input::E_Widget_type::Digital)
		{
			bool bPressed = false;
			_get_widget_state(pDefs[n1].ID, &bPressed);
			if (bPressed)
			{
				return true;
			}
		}
	}
	return false;
}

bool IInputDevice::is_action_button_pressed(void) const
{
	const Widget::WIDGET_DESC* pDefs = _get_widget_defs();
	for (FXD::U16 n1 = 0; pDefs[n1].Type != Input::E_Widget_type::Count; n1++)
	{
		if ((pDefs[n1].Type == Input::E_Widget_type::Digital) && (pDefs[n1].Flags & Widget::kWidgetFlagActionButton))
		{
			bool bPressed = false;
			_get_widget_state(pDefs[n1].ID, &bPressed);
			if (bPressed)
			{
				return true;
			}
		}
	}
	return false;
}

bool IInputDevice::is_start_button_pressed(void) const
{
	const Widget::WIDGET_DESC* pDefs = _get_widget_defs();
	for (FXD::U16 n1 = 0; pDefs[n1].Type != Input::E_Widget_type::Count; n1++)
	{
		if ((pDefs[n1].Type == Input::E_Widget_type::Digital) && (pDefs[n1].Flags & Widget::kWidgetFlagStartButton))
		{
			bool bPressed = false;
			_get_widget_state(pDefs[n1].ID, &bPressed);
			if (bPressed)
			{
				return true;
			}
		}
	}
	return false;
}

bool IInputDevice::is_back_button_pressed(void) const
{
	const Widget::WIDGET_DESC* pDefs = _get_widget_defs();
	for (FXD::U16 n1 = 0; pDefs[n1].Type != Input::E_Widget_type::Count; n1++)
	{
		if ((pDefs[n1].Type == Input::E_Widget_type::Digital) && (pDefs[n1].Flags & Widget::kWidgetFlagBackButton))
		{
			bool bPressed = false;
			_get_widget_state(pDefs[n1].ID, &bPressed);
			if (bPressed)
			{
				return true;
			}
		}
	}
	return false;
}

FXD::S32 IInputDevice::get_variableID_and_type(const Core::String8& strName, Core::E_ExpressionType& type) const
{
	const Widget::WIDGET_DESC* pDefs = _get_widget_defs();
	for (FXD::U16 n1 = 0; pDefs[n1].Type != Input::E_Widget_type::Count; n1++)
	{
		if (pDefs[n1].Name == strName.c_str())
		{
			switch (pDefs[n1].Type)
			{
				case Input::E_Widget_type::Digital:
				{
					type = Core::E_ExpressionType::Bool;
					break;
				}
				case Input::E_Widget_type::HalfAxis:
				case Input::E_Widget_type::FullAxis:
				{
					type = Core::E_ExpressionType::Float;
					break;
				}
				default:
				{
					PRINT_ASSERT << "Input: Input type not yet supported by expressions";
					break;
				}
			}
			return pDefs[n1].ID;
		}
	}
	PRINT_ASSERT << "Input: Variable '" << strName << "' not found on '" << get_config_name() << "'";
	return 0;
}

void IInputDevice::get_variable_data(FXD::U16 nID, void* pData) const
{
	_get_widget_state(nID, pData);
}