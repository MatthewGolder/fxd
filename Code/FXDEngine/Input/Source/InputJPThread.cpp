// Creator - MatthewGolder
#include "FXDEngine/Input/InputJPThread.h"
#include "FXDEngine/Input/InputSystem.h"

using namespace FXD;
using namespace Input;

// 0 - Not Initialised
// 1 - Initialised
// 2 - ShutDown
FXD::S32 g_nInputStatus = 0;

// ------
// IInputJPThread
// -
// ------
IInputJPThread::IInputJPThread(void)
	: IAsyncTaskPoolThread(display_name())
	, m_inputSystem(nullptr)
{
	if (g_nInputStatus == 2)
	{
		PRINT_ASSERT << "Input: IInputJPThread accessed after shutdown";
	}
}

IInputJPThread::~IInputJPThread(void)
{
	stop_and_wait_for_exit();
}

bool IInputJPThread::init_pool(void)
{
	Input::Funcs::RestrictInputThread();
	PRINT_INFO << "Input: IInputJPThread::init_pool()";

	m_inputSystem = std::make_unique< Input::IInputSystem >();
	m_timer.start_counter();
	g_nInputStatus = 1;
	m_shared.get_quick_write_lock()->m_eState = Job::E_AsyncState::Running;
	return true;
}

bool IInputJPThread::shutdown_pool(void)
{
	m_inputSystem = nullptr;
	Input::Funcs::UnrestrictInputThread();
	PRINT_INFO << "Input: IInputJPThread::shutdown_pool()";

	g_nInputStatus = 2;
	m_shared.get_quick_write_lock()->m_eState = Job::E_AsyncState::ShutDown;
	return true;
}

void IInputJPThread::idle_process(void)
{
	if (is_shuttingdown())
	{
		return;
	}

	const FXD::S32 nProcessFrequency = 5;

	// 1. Get initial time stamp.
	FXD::F32 ts1 = (FXD::F32)Core::Funcs::GetTimeSecsF();

	FXD::F32 dt = m_timer.elapsed_time();
	m_fps.update(dt);
	if (m_inputSystem)
	{
		m_inputSystem->_process(dt);
	}

	// 2. Get final time stamp and wait for next update interval.
	FXD::F32 ts2 = (FXD::F32)Core::Funcs::GetTimeSecsF();
	FXD::S32 nToWait = nProcessFrequency - (FXD::S32)((ts2 - ts1) * 1000.0f);
	if (nToWait > 0)
	{
		Thread::Funcs::Sleep(nToWait);
	}
}

bool IInputJPThread::is_thread_restricted(void) const
{
	return Input::Funcs::IsInputThreadRestricted();
}

// ------
// Funcs
// -
// ------
static FXD::S32 g_ContextThread = 0;
bool FXD::Input::Funcs::IsInputThreadRestricted(void)
{
	return (g_ContextThread != 0) && (g_ContextThread != Thread::Funcs::GetCurrentThreadID());
}

void FXD::Input::Funcs::RestrictInputThread(void)
{
	PRINT_COND_ASSERT((!Funcs::IsInputThreadRestricted()), "Input: Not allowed on current thread " << Thread::Funcs::GetCurrentThreadID());
	g_ContextThread = Thread::Funcs::GetCurrentThreadID();
}

void FXD::Input::Funcs::UnrestrictInputThread(void)
{
	PRINT_COND_ASSERT((!Funcs::IsInputThreadRestricted()), "Input: Not allowed on current thread " << Thread::Funcs::GetCurrentThreadID());
	g_ContextThread = 0;
}