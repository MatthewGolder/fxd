// Creator - MatthewGolder
#include "FXDEngine/Input/InputSystem.h"
#include "FXDEngine/Input/InputDevice.h"
#include "FXDEngine/Input/InputJPThread.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Core/WatchDogThread.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace Input;

// ------
// IInputSystem
// -
// ------
IInputSystem::IInputSystem(void)
{
}

IInputSystem::~IInputSystem(void)
{
	PRINT_COND_ASSERT((m_inputApis.size() == 0), "Input: m_inputApis are not shutdown");
	PRINT_COND_ASSERT((m_unusedDevices.size() == 0), "Input: m_unusedDevices are not shutdown");
	PRINT_COND_ASSERT((m_selectedDevices.size() == 0), "Input: m_selectedDevices are not shutdown");
}

Job::Future< bool > IInputSystem::create_system(void)
{
	auto funcRef = [&](void) { return _create_system(); };

	return Job::Async(FXDInput(), [=]() { return funcRef(); });
}

Job::Future< bool > IInputSystem::release_system(void)
{
	auto funcRef = [&](void) { return _release_system(); };

	return Job::Async(FXDInput(), [=]() { return funcRef(); });
}

Job::Future< bool > IInputSystem::create_devices(void)
{
	auto funcRef = [&](void) { return _create_devices(); };

	return Job::Async(FXDInput(), [=]() { return funcRef(); });
}

Job::Future< bool > IInputSystem::release_devices(void)
{
	auto funcRef = [&](void) { return _release_devices(); };

	return Job::Async(FXDInput(), [=]() { return funcRef(); });
}

Job::Future< void > IInputSystem::pause_haptics(void)
{
	auto funcRef = [&](void) { return _pause_haptics(); };

	return Job::Async(FXDInput(), [=]() { return funcRef(); });
}

Job::Future< void > IInputSystem::resume_haptics(void)
{
	auto funcRef = [&](void) { return _resume_haptics(); };

	return Job::Async(FXDInput(), [=]() { return funcRef(); });
}

Job::Future< void > IInputSystem::reset_haptics(void)
{
	auto funcRef = [&](void) { return _reset_haptics(); };

	return Job::Async(FXDInput(), [=]() { return funcRef(); });
}

void IInputSystem::_pause_haptics(void)
{
	fxd_for(auto& device, m_unusedDevices)
	{
		device->pause_haptics();
	}
	fxd_for(auto& device, m_selectedDevices)
	{
		device->pause_haptics();
	}
}

void IInputSystem::_resume_haptics(void)
{
	fxd_for(auto& device, m_unusedDevices)
	{
		device->resume_haptics();
	}
	fxd_for(auto& device, m_selectedDevices)
	{
		device->resume_haptics();
	}
}

void IInputSystem::_reset_haptics(void)
{
	fxd_for(auto& device, m_unusedDevices)
	{
		device->reset_haptics();
	}
	fxd_for(auto& device, m_selectedDevices)
	{
		device->reset_haptics();
	}
	}

void IInputSystem::_process(FXD::F32 dt)
{
	PRINT_COND_ASSERT((!Input::Funcs::IsInputThreadRestricted()), "Input: Not allowed on current thread " << Thread::Funcs::GetCurrentThreadID());

#if defined(FXD_ENABLE_WATCHDOG)
	static Core::WatchDogThread sWDThread("InputSystem - WatchDog", 500);
	sWDThread.increment();
#endif

	fxd_for(auto& inputApi, m_inputApis)
	{
		inputApi->_process(dt);
	}
	fxd_for(auto& device, m_unusedDevices)
	{
		device->_update(dt);
	}
	fxd_for(auto& device, m_selectedDevices)
	{
		device->_update(dt);
	}
}

bool IInputSystem::_register_device(Input::InputDevice device)
{
	PRINT_COND_ASSERT((!Input::Funcs::IsInputThreadRestricted()), "Input: Not allowed on current thread " << Thread::Funcs::GetCurrentThreadID());

	if (!m_unusedDevices.contains(device))
	{
		m_unusedDevices.emplace_back(device);
		device->set_motor_speeds(Input::DualMotorRumbleData(0.7f, 0.7f, 10.0f));

		return true;
	}
	
	return false;
}

bool IInputSystem::_unregister_device(Input::InputDevice device)
{
	PRINT_COND_ASSERT((!Input::Funcs::IsInputThreadRestricted()), "Input: Not allowed on current thread " << Thread::Funcs::GetCurrentThreadID());

	if (m_unusedDevices.contains(device))
	{
		m_unusedDevices.find_erase(device);
		return true;
	}

	return false;
}