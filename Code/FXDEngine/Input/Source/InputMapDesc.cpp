// Creator - MatthewGolder
#include "FXDEngine/Input/InputMapDesc.h"
#include "FXDEngine/IO/XML/XMLDOMCB.h"

using namespace FXD;
using namespace Input;

// ------
// InputMapTagCB
// -
// ------
class InputMapTagCB FINAL : public IO::XMLSaxParser::ITagCB
{
public:
	enum class E_Tag
	{
		InputMap,
		Config,
		Device,
		Action,
		Count,
		Unknown = 0xffff
	};

public:
	InputMapTagCB(Input::InputMapDesc& inputMapDef)
		: m_eTag(E_Tag::Unknown)
		, m_inputMapDef(inputMapDef)
	{
	}
	~InputMapTagCB(void)
	{
	}

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL
	{
		pTagCB = this;
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("inputmap"))
				{
					m_eTag = E_Tag::InputMap;
				}
				break;
			}
			case E_Tag::InputMap:
			{
				if (strName == UTF_8("config"))
				{
					m_inputMapDef->configs().emplace_back();
					m_eTag = E_Tag::Config;
				}
				break;
			}
			case E_Tag::Config:
			{
				if (strName == UTF_8("device"))
				{
					m_inputMapDef->configs().back().devices().emplace_back();
					m_eTag = E_Tag::Device;
				}
				break;
			}
			case E_Tag::Device:
			{
				if (strName == UTF_8("bool"))
				{
					m_inputMapDef->configs().back().devices().back().actions().emplace_back();
					m_inputMapDef->configs().back().devices().back().actions().back().expressionType(Core::E_ExpressionType::Bool);
					m_eTag = E_Tag::Action;
				}
				else if (strName == UTF_8("float"))
				{
					m_inputMapDef->configs().back().devices().back().actions().emplace_back();
					m_inputMapDef->configs().back().devices().back().actions().back().expressionType(Core::E_ExpressionType::Float);
					m_eTag = E_Tag::Action;
				}
				break;
			}
			default:
			{
				PRINT_ASSERT << "Input: Unknown tag found '" << strName << "'";
				break;
			}
		}
	}
	void end_tag(const Core::String8& /*strName*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::InputMap:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			case E_Tag::Config:
			{
				m_eTag = E_Tag::InputMap;
				break;
			}
			case E_Tag::Device:
			{
				m_eTag = E_Tag::Config;
				break;
			}
			case E_Tag::Action:
			{
				m_eTag = E_Tag::Device;
				break;
			}
			default:
			{
				break;
			}
		}
	}

	void handle_text(const Core::String8& /*strText*/, const IO::StreamParser::Position& /*pos*/) FINAL
	{
	}

	void handle_attribute(const Core::String8& strName, const Core::String8& strValue) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Config:
			{
				if (strName == UTF_8("name"))
				{
					fxd_for(const auto& config, m_inputMapDef->configs())
					{
						PRINT_COND_ASSERT((!config.name().contains_no_case(strValue)), "Input: Duplicate input map config found in file");
					}

					m_inputMapDef->configs().back().name(strValue);
					break;
				}
				else
				{
					PRINT_ASSERT << "Input: Unknown tag found '" << strName << "' '" << strValue << "'";
					break;
				}
			}
			case E_Tag::Device:
			{
				if (strName == UTF_8("name"))
				{
					fxd_for(const auto& device, m_inputMapDef->configs().back().devices())
					{
						PRINT_COND_ASSERT((!device.name().contains_no_case(strValue)), "Input: Duplicate device name found in file");
					}

					m_inputMapDef->configs().back().devices().back().name(strValue);
					break;
				}
				else
				{
					PRINT_ASSERT << "Input: Unknown tag found '" << strName << "' '" << strValue << "'";
					break;
				}
			}
			case E_Tag::Action:
			{
				if (strName == UTF_8("name"))
				{
					fxd_for(const auto& action, m_inputMapDef->configs().back().devices().back().actions())
					{
						PRINT_COND_ASSERT((!action.name().contains_no_case(strValue)), "Input: Duplicate action name found in file");
					}

					m_inputMapDef->configs().back().devices().back().actions().back().name(strValue);
					break;
				}
				else if (strName == UTF_8("value"))
				{
					Core::ExpressionTokeniser et(strValue);
					m_inputMapDef->configs().back().devices().back().actions().back().value().build_tree(et);
					break;
				}
				else
				{
					PRINT_ASSERT << "Input: Unknown tag found '" << strName << "' '" << strValue << "'";
					break;
				}
			}
			default:
			{
				PRINT_ASSERT << "Input: Unknown tag found '" << strName << "' '" << strValue << "'";
				break;
			}
		}
	}

public:
	E_Tag m_eTag;
	InputMapDesc& m_inputMapDef;
};

// ------
// IInputMapDesc::Action
// -
// ------
IInputMapDesc::Action::Action(void)
{
}

IInputMapDesc::Action::~Action(void)
{
}

// ------
// IInputMapDesc::Device
// -
// ------
IInputMapDesc::Device::Device(void)
{
}

IInputMapDesc::Device::~Device(void)
{
}

// ------
// IInputMapDesc::Config
// -
// ------
IInputMapDesc::Config::Config(void)
{
}

IInputMapDesc::Config::~Config(void)
{
}

// ------
// IActionMapDef
// -
// ------
Input::InputMapDesc Input::IInputMapDesc::FromStream(Core::StreamIn& stream, const Core::String8& strFileName)
{
	if (stream)
	{
		FXD::Input::InputMapDesc desc = std::make_shared< FXD::Input::IInputMapDesc >(strFileName);

		InputMapTagCB callback(desc);
		IO::StreamParser streamParser(stream);
		IO::XMLSaxParser::parse(streamParser, callback);

		desc->build_control_ids();
		return desc;
	}
	return nullptr;
}

IInputMapDesc::IInputMapDesc(const Core::String8& strFileName)
	: IAsset(strFileName)
{
}

IInputMapDesc::~IInputMapDesc(void)
{
}

FXD::U32 IInputMapDesc::getControlID(const Core::String8& strName) const
{
	FXD::U32 nRetID = 0;
	PRINT_COND_ASSERT(m_ids.try_get_value(strName, nRetID), "Input: Action ID for '" << strName << "' not found");
	return nRetID;
}

bool IInputMapDesc::is_platform_supported(void) const
{
	return true;
}

void IInputMapDesc::build_control_ids(void)
{
	FXD::U32 nGid = 1;
	fxd_for(const auto& config, m_configs)
	{
		fxd_for(const auto& device, config.devices())
		{
			fxd_for(const auto& action, device.actions())
			{
				if (m_ids.contains(action.name()))
				{
					PRINT_WARN << "Input: Action ID for '" << action.name() << "' already found";
				}
				//PRINT_COND_WARN((m_ids.contains(action.name()) == false), "Input: Action ID for '" << act.name() << "' already found");
				m_ids[action.name()] = nGid++;
			}
		}
	}
}