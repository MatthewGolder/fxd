// Creator - MatthewGolder
#include "FXDEngine/Input/InputMap.h"
#include "FXDEngine/Input/InputDevice.h"
#include "FXDEngine/Input/InputMapDesc.h"

using namespace FXD;
using namespace Input;

// ------
// InputMap
// -
// ------
InputMap::InputMap(void)
{
}

InputMap::~InputMap(void)
{
}

void InputMap::build(const Input::InputMapDesc& inputMapDesc, const Core::String8& strConfig, const Container::Vector< InputDevice >& devices)
{
	// Store the devices to ensure they don't go away, and clear previous mappings.
	m_devices = devices;
	m_mappings.clear();

	// Apply the definition.
	fxd_for(const auto& config, inputMapDesc->m_configs)
	{
		// Ensure we are applying the right config.
		if (config.name() != strConfig)
		{
			continue;
		}

		// Iterate the devices.
		for (const auto& configDev : config.devices())
		{
			// Find the input device.
			Input::InputDevice inDev;
			fxd_for(auto& itrDev, m_devices)
			{
				if (itrDev->get_config_name() == configDev.name())
				{
					inDev = itrDev;
					break;
				}
			}

			if (inDev == nullptr)
			{
				continue;
			}

			// Iterate the actions.
			fxd_for(const auto& action, configDev.actions())
			{
				FXD::U32 nActionId = inputMapDesc->getControlID(action.name());

				DeviceAction dAction;
				dAction.variable_provider(inDev.get());
				action.value().build_compiled_expression(dAction.expression(), (*dAction.variable_provider()));
				PRINT_COND_ASSERT((dAction.expression().get_type() == action.expressionType()), "Input: Control " << config.name() << "" << configDev.name() << "" << action.name() << " has the wrong type for it's value expression");

				if (m_mappings.contains(nActionId))
				{
					PRINT_WARN << "Input: Action ID for '" << nActionId << "' already found";
				}
				m_mappings[nActionId].actions().push_back(dAction);
			}
		}
	}
}

FXD::F32 InputMap::get_float(FXD::U32 nID) const
{
	ControlMap ctrl;
	if (!m_mappings.try_get_value(nID, ctrl))
	{
		return 0.0f;
	}

	FXD::F32 fTot = 0.0f;
	FXD::F32 fVal = 0.0f;
	fxd_for(const auto& action, ctrl.actions())
	{
		PRINT_COND_ASSERT((action.expression().get_type() == Core::E_ExpressionType::Float), "Input: Control is not a float value");

		action.expression().evaluate(*action.variable_provider(), &fVal);
		fTot += fVal;
	}
	return fTot;
}

bool InputMap::get_bool(FXD::U32 nID) const
{
	ControlMap ctrl;
	if (!m_mappings.try_get_value(nID, ctrl))
	{
		return false;
	}

	bool bVal = false;
	fxd_for(const auto& action, ctrl.actions())
	{
		PRINT_COND_ASSERT((action.expression().get_type() == Core::E_ExpressionType::Bool), "Input: Control is not a bool value");

		action.expression().evaluate(*action.variable_provider(), &bVal);
		if (bVal)
		{
			return true;
		}
	}
	return false;
}