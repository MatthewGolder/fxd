// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_INPUTDEVICE_H
#define FXDENGINE_INPUT_INPUTDEVICE_H

#include "FXDEngine/Core/Expression.h"
#include "FXDEngine/Core/StringView.h"
#include "FXDEngine/Input/Types.h"
#include "FXDEngine/Core/Colour.h"

namespace FXD
{
	namespace Input
	{
		// ------
		// E_Widget_type
		// -
		// ------
		enum class E_Widget_type : FXD::U16
		{
			Digital,
			HalfAxis,
			FullAxis,
			Count,
			Unknown = 0xffff
		};

		// ------
		// Widget
		// -
		// ------
		class Widget
		{
		public:
			// Widget flags.
			static CONSTEXPR const FXD::S32 kWidgetFlagActionButton = (1 << 0);
			static CONSTEXPR const FXD::S32 kWidgetFlagStartButton = (1 << 1);
			static CONSTEXPR const FXD::S32 kWidgetFlagBackButton = (1 << 2);
			static CONSTEXPR const FXD::S32 kWidgetFlagGuideButton = (1 << 3);

			// ------
			// WIDGET_DESC
			// -
			// ------
			struct WIDGET_DESC
			{
				CONSTEXPR WIDGET_DESC(void)
					: ID(0)
					, Flags(0)
					, Type(Input::E_Widget_type::Count)
				{}
				CONSTEXPR WIDGET_DESC(const FXD::U16 nID, const Input::E_Widget_type eType, const Core::StringView8 strName, const FXD::S32 nFlags)
					: ID(nID)
					, Flags(nFlags)
					, Name(strName)
					, Type(eType)
				{}

				const FXD::U16 ID = 0;
				const FXD::S32 Flags = 0;
				const Core::StringView8 Name;
				const Input::E_Widget_type Type = Input::E_Widget_type::Count;
			};
		};

		// ------
		// IInputDevice
		// -
		// ------
		class IInputDevice : public Input::IDualMotorRumble, public Core::IVariableProvider
		{
		public:
			friend class IInputSystem;

		public:
			IInputDevice(void);
			virtual ~IInputDevice(void);

			// Determine if a button is being pressed.
			bool is_any_button_pressed(void) const;
			bool is_action_button_pressed(void) const;
			bool is_start_button_pressed(void) const;
			bool is_back_button_pressed(void) const;

			virtual void set_colour(const Core::ColourRGB& /*col*/)
			{}

			virtual bool is_connected(void) const			{ return false; }
			virtual bool is_gamepad_available(void) const	{ return false; }
			virtual bool is_mouse_available(void) const		{ return false; }
			virtual bool is_keyboard_available(void) const	{ return false; }
			virtual bool supports_rumble(void) const		{ return false; }

			// Get the name of this device.
			virtual Core::String8 get_config_name(void) const PURE;
			virtual Core::String8 get_device_name(void) const PURE;
			virtual Input::E_DeviceType get_device_type(void) const PURE;

		protected:
			// Update the input device.
			virtual void _update(FXD::F32 /*dt*/)
			{}
			virtual const Widget::WIDGET_DESC* _get_widget_defs(void) const PURE;	// Get the list of widgets, terminated by a null widget def.
			virtual void _get_widget_state(FXD::U16 nID, void* pState) const PURE;	// Get the state of a widget.

			// Get an index and type for a variable name.
			FXD::S32 get_variableID_and_type(const Core::String8& strName, Core::E_ExpressionType& type) const FINAL;

			// Get the value of a variable by index.
			void get_variable_data(FXD::U16 nID, void* pData) const FINAL;

		protected:
			Input::E_PowerLevel m_ePowerLevel;
		};

	} //namespace Input
} //namespace FXD
#endif //FXDENGINE_INPUT_INPUTDEVICE_H