// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_TYPES_H
#define FXDENGINE_INPUT_TYPES_H

#include "FXDEngine/Core/Types.h"

#include <memory>

#if IsOSPC()
#	define IsInputGooglePlay() 0
#	define IsInputWin32() 1
#	define IsInputSteam() 0
#	define IsInputXInput() 1
#	define IsInputHIDAPI() 1
#	if IsInputHIDAPI()
#		define IsInputPS3() 1
#		define IsInputPS4() 1
#		define IsInputSwitch() 1
#		define IsInputXInputH() 0
#	else
#		define IsInputPS3() 0
#		define IsInputPS4() 0
#		define IsInputSwitch() 0
#		define IsInputXInputH() 0
#	endif
#elif IsOSAndroid()
#	define IsInputGooglePlay() 1
#	define IsInputWin32() 0
#	define IsInputSteam() 0
#	define IsInputXInput() 0
#	define IsInputHIDAPI() 1
#	if IsInputHIDAPI()
#		define IsInputPS3() 0
#		define IsInputPS4() 0
#		define IsInputSwitch() 0
#		define IsInputXInputH() 0
#	else
#		define IsInputPS3() 0
#		define IsInputPS4() 0
#		define IsInputSwitch() 0
#		define IsInputXInputH() 0
#	endif
#endif

namespace FXD
{
	namespace Input
	{
		class IInputApi;
		typedef std::unique_ptr< Input::IInputApi > InputApi;

		class IInputSystem;
		typedef std::unique_ptr< Input::IInputSystem > InputSystem;

		class IInputDevice;
		typedef std::shared_ptr< Input::IInputDevice > InputDevice;

		class IMouse;
		typedef std::shared_ptr< Input::IMouse > Mouse;

		class IKeyboard;
		typedef std::shared_ptr< Input::IKeyboard > Keyboard;

		class IInputMapDesc;
		typedef std::shared_ptr< Input::IInputMapDesc > InputMapDesc;

		// ------
		// FXDControl
		// -
		// ------
		class FXDControl
		{
		public:
			enum class E_ControlType
			{
				Digital,	// Widget state has a value of 0 or 1.
				HalfAxis,	// Widget state has any value between 0 and 1.
				FullAxis,	// Widget state has any value between -1 and 1.
				Count,
				Unknown = 0xffff
			};

			// Widget digital events.
			static const FXD::S32 kPressed = 1;
			static const FXD::S32 kReleased = 2;
			static const FXD::S32 kNotDigital = 4;
		};

		// ------
		// DualMotorRumbleData
		// -
		// ------
		class DualMotorRumbleData
		{
		public:
			DualMotorRumbleData(void)
				: m_fLeftRumble(0.0f)
				, m_fRightRumble(0.0f)
				, m_fDuration(0.0f)
			{}
			DualMotorRumbleData(FXD::F32 fLeftRumble, FXD::F32 fRightRumble, FXD::F32 fDuration)
				: m_fLeftRumble(fLeftRumble)
				, m_fRightRumble(fRightRumble)
				, m_fDuration(fDuration)
			{}
			~DualMotorRumbleData(void)
			{}

		public:
			FXD::F32 m_fLeftRumble;
			FXD::F32 m_fRightRumble;
			FXD::F32 m_fDuration;
		};

		// ------
		// IDualMotorRumble
		// -
		// ------
		class IDualMotorRumble
		{
		public:
			IDualMotorRumble(void)
				: m_bRumbleDirty(false)
				, m_bRumblePaused(false)
				, m_fRumbleExpiration(0.0f)
				, m_rumbleData()
			{}
			virtual ~IDualMotorRumble(void)
			{}

			void set_motor_speeds(const Input::DualMotorRumbleData& rumbleData)
			{
				m_bRumbleDirty = true;
				m_fRumbleExpiration = 0.0f;
				m_rumbleData = rumbleData;
			}
			void pause_haptics(void)
			{
				m_bRumblePaused = true;
				m_bRumbleDirty = true;
			}
			void resume_haptics(void)
			{
				m_bRumblePaused = false;
				m_bRumbleDirty = true;
			}
			void reset_haptics(void)
			{
				set_motor_speeds(Input::DualMotorRumbleData(0.0f, 0.0f, 0.0f));
			}

		protected:
			bool m_bRumbleDirty;
			bool m_bRumblePaused;
			FXD::F32 m_fRumbleExpiration;
			DualMotorRumbleData m_rumbleData;
		};

		// ------
		// E_MouseButtons
		// -
		// ------
		enum class E_MouseButtons : FXD::U8
		{
			Left,
			Middle,
			Right,
			Thumb01,
			Thumb02,
			Count,
			Unknown = 0xff
		};

		// ------
		// E_KeyboardButtons
		// -
		// ------
		enum class E_KeyboardButtons : FXD::U8
		{
			Count,
			Unknown = 0xff
		};

		// ------
		// E_GamePadButtons
		// - We use xbox notations in the engine for simplicity
		// ------
		enum class E_GamePadButtons : FXD::U8
		{
			DPadUp,
			DPadDown,
			DPadLeft,
			DPadRight,
			LeftThumbX,
			LeftThumbY,
			RightThumbX,
			RightThumbY,
			LeftThumb,
			RightThumb,
			LeftBumper,
			RightBumper,
			LeftTrigger,
			RightTrigger,
			Start,
			Back,
			AButton,
			BButton,
			XButton,
			YButton,
			Guide,
			Touchpad,
			Capture,
			SL,
			SR,
			Count,
			Unknown = 0xff
		};

		// ------
		// E_DeviceType
		// -
		// ------
		enum class E_DeviceType : FXD::U8
		{
			Mouse,
			Keyboard,
			Controller,
			Wheel,
			ArcadeStick,
			FlightStick,
			DancePad,
			Guitar,
			DrumKit,
			ArcadePad,
			Throttle,
			Count,
			Unknown = 0xff
		};

		// ------
		// E_PowerLevel
		// -
		// ------
		enum class E_PowerLevel : FXD::U8
		{
			Empty,	// <= 5%
			Low,	// <= 25%
			Medium,	// <= 50%
			Good,	// <= 75%
			Full,	// <= 100%
			Wired,
			Count,
			Unknown = 0xff
		};

	} //namespace Input
} //namespace FXD
#endif //FXDENGINE_INPUT_TYPES_H