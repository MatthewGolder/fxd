// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_INPUTAPI_H
#define FXDENGINE_INPUT_INPUTAPI_H

#include "FXDEngine/Input/Types.h"
#include "FXDEngine/Core/String.h"

namespace FXD
{
	namespace Input
	{
		// ------
		// IInputApi
		// -
		// ------
		class IInputApi
		{
		public:
			friend class IInputSystem;

		public:
			IInputApi(void);
			virtual ~IInputApi(void);

			virtual bool create_api(void) PURE;
			virtual bool release_api(void) PURE;

		private:
			virtual void _process(FXD::F32 dt) PURE;
		};

	} //namespace Input
} //namespace FXD
#endif //FXDENGINE_INPUT_INPUTAPI_H