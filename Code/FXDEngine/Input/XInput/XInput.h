// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_XINPUT_XINPUT_H
#define FXDENGINE_INPUT_XINPUT_XINPUT_H

#include "FXDEngine/Input/Types.h"

#if IsInputXInput()
#	if IsOSPC()
#		include "FXDEngine/Core/Win32/Win32.h"
#	endif

#	include <xinput.h>
#	pragma comment(lib,"XInput.lib")

#	ifndef XINPUT_GAMEPAD_GUIDE
#		define XINPUT_GAMEPAD_GUIDE 0x0400
#	endif

namespace FXD
{
	namespace Input
	{
		class IGamePadXInput;
		typedef std::shared_ptr< Input::IGamePadXInput > GamePadXInput;

	} //namespace Input
} //namespace FXD
#endif //IsInputXInput()
#endif //FXDENGINE_INPUT_XINPUT_XINPUT_H