// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_XINPUT_INPUTAPIXINPUT_H
#define FXDENGINE_INPUT_XINPUT_INPUTAPIXINPUT_H

#include "FXDEngine/Input/Types.h"

#if IsInputXInput()
#include "FXDEngine/Container/Vector.h"
#include "FXDEngine/Input/InputApi.h"
#include "FXDEngine/Input/XInput/XInput.h"

namespace FXD
{
	namespace Input
	{
		// ------
		// IInputApiXInput
		// -
		// ------
		class IInputApiXInput FINAL : public Input::IInputApi
		{
		public:
			IInputApiXInput(void);
			~IInputApiXInput(void);

			bool create_api(void) FINAL;
			bool release_api(void) FINAL;

		private:
			void _process(FXD::F32 dt) FINAL;

		private:
			HMODULE m_XInputDLL;
			Container::Vector< Input::InputDevice > m_detectedDevices;
		};

	} //namespace Input
} //namespace FXD

#endif //IsInputXInput()
#endif //FXDENGINE_INPUT_XINPUT_INPUTAPIXINPUT_H