// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputXInput()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Input/Mapping.h"
#include "FXDEngine/Input/XInput/GamePadXInput.h"
#include "FXDEngine/Job/EventHandler.h"
#include "FXDEngine/Core/Algorithm.h"

using namespace FXD;
using namespace Input;

namespace
{
	static FXD::F32 xinput_map_trigger_value(FXD::U8 nVal)
	{
		return ((FXD::F32)nVal / 255.0f);
	}

	static FXD::F32 xinput_map_stick_value(SHORT nVal, const SHORT nDeadZone)
	{
		if (nVal < 0)
		{
			nVal = (nVal + nDeadZone);
			if (nVal > 0)
			{
				nVal = 0;
			}
			nVal = (nVal * 32768) / (32768 - nDeadZone);
		}
		else
		{
			nVal = (nVal - nDeadZone);
			if (nVal < 0)
			{
				nVal = 0;
			}
			nVal = (nVal * 32768) / (32768 - nDeadZone);
		}
		return (FXD::F32)nVal / 32768.0f;
	}
}

// ------
// IGamePadXInput::XInputContext
// -
// ------
IGamePadXInput::XInputContext::XInputContext(const FXD::S32 nPort)
	: m_bConnected(false)
	, m_subType(0)
	, m_nPortIndex(XUSER_INDEX_ANY)
{
	Memory::MemZero_T(m_state);
}

// ------
// IGamePadXInput
// -
// ------
IGamePadXInput::IGamePadXInput(const FXD::S32 nPort)
	: IInputDevice()
	, m_context(nPort)
	, OnControllerButton(nullptr)
	, OnControllerAnalog(nullptr)
{
	OnControllerButton = FXDEvent()->attach< Input::OnControllerButton >([&](Input::OnControllerButton evt)
	{
		if (evt.Device == this)
		{
			PRINT_INFO << Input::GetGamePadName(evt.Control);
		}
	});
	OnControllerAnalog = FXDEvent()->attach< Input::OnControllerAnalog >([&](Input::OnControllerAnalog evt)
	{
		if (evt.Device == this)
		{
			PRINT_INFO << evt.Value;
		}
	});
}

IGamePadXInput::~IGamePadXInput(void)
{
	if (OnControllerButton != nullptr)
	{
		FXDEvent()->detach(OnControllerButton);
		OnControllerButton = nullptr;
	}
	if (OnControllerAnalog != nullptr)
	{
		FXDEvent()->detach(OnControllerAnalog);
		OnControllerAnalog = nullptr;
	}
}

bool IGamePadXInput::is_connected(void) const
{
	return (m_context.m_bConnected);
}

bool IGamePadXInput::is_gamepad_available(void) const
{
	return (m_context.m_bConnected);
}

bool IGamePadXInput::supports_rumble(void) const
{
	return false;
}

Core::String8 IGamePadXInput::get_config_name(void) const
{
	return UTF_8("XInput");
}

Core::String8 IGamePadXInput::get_device_name(void) const
{
	switch (m_context.m_subType)
	{
		case XINPUT_DEVSUBTYPE_GAMEPAD:
		{
			return UTF_8("XInput Controller");
		}
		case XINPUT_DEVSUBTYPE_WHEEL:
		{
			return UTF_8("XInput Wheel");
		}
		case XINPUT_DEVSUBTYPE_ARCADE_STICK:
		{
			return UTF_8("XInput ArcadeStick");
		}
		case XINPUT_DEVSUBTYPE_FLIGHT_STICK:
		{
			return UTF_8("XInput FlightStick");
		}
		case XINPUT_DEVSUBTYPE_DANCE_PAD:
		{
			return UTF_8("XInput DancePad");
		}
		case XINPUT_DEVSUBTYPE_GUITAR:
		case XINPUT_DEVSUBTYPE_GUITAR_ALTERNATE:
		case XINPUT_DEVSUBTYPE_GUITAR_BASS:
		{
			return UTF_8("XInput Guitar");
		}
		case XINPUT_DEVSUBTYPE_DRUM_KIT:
		{
			return UTF_8("XInput DrumKit");
		}
		case XINPUT_DEVSUBTYPE_ARCADE_PAD:
		{
			return UTF_8("XInput ArcadePad");
		}
		default:
		{
			break;
		}
	}
	return UTF_8("Unknown");
}

Input::E_DeviceType IGamePadXInput::get_device_type(void) const
{
	switch (m_context.m_subType)
	{
		case XINPUT_DEVSUBTYPE_GAMEPAD:
		{
			return Input::E_DeviceType::Controller;
		}
		case XINPUT_DEVSUBTYPE_WHEEL:
		{
			return Input::E_DeviceType::Wheel;
		}
		case XINPUT_DEVSUBTYPE_ARCADE_STICK:
		{
			return Input::E_DeviceType::ArcadeStick;
		}
		case XINPUT_DEVSUBTYPE_FLIGHT_STICK:
		{
			return Input::E_DeviceType::FlightStick;
		}
		case XINPUT_DEVSUBTYPE_DANCE_PAD:
		{
			return Input::E_DeviceType::DancePad;
		}
		case XINPUT_DEVSUBTYPE_GUITAR:
		case XINPUT_DEVSUBTYPE_GUITAR_ALTERNATE:
		case XINPUT_DEVSUBTYPE_GUITAR_BASS:
		{
			return Input::E_DeviceType::Guitar;
		}
		case XINPUT_DEVSUBTYPE_DRUM_KIT:
		{
			return Input::E_DeviceType::DrumKit;
		}
		case XINPUT_DEVSUBTYPE_ARCADE_PAD:
		{
			return Input::E_DeviceType::ArcadePad;
		}
		default:
		{
			break;
		}
	}
	return Input::E_DeviceType::Unknown;
}

void IGamePadXInput::_update(FXD::F32 dt)
{
	bool bWasConnected = m_context.m_bConnected;
	
	XINPUT_CAPABILITIES xCapabilities;
	Memory::MemZero_T(xCapabilities);

	const DWORD result = XInputGetCapabilities(m_context.m_nPortIndex, XINPUT_FLAG_GAMEPAD, &xCapabilities);
	m_context.m_bConnected = (result == ERROR_SUCCESS);
	m_context.m_subType = xCapabilities.SubType;

	if (!bWasConnected && m_context.m_bConnected)
	{
		FXDEvent()->raise(Input::OnControllerConnection(this, true, m_context.m_nPortIndex));
	}
	else if (bWasConnected && !m_context.m_bConnected)
	{
		FXDEvent()->raise(Input::OnControllerConnection(this, false, m_context.m_nPortIndex));
	}

	// Battery
	{
		XINPUT_BATTERY_INFORMATION xBatteryInfo = {};
		Memory::MemZero_T(xBatteryInfo);
		XInputGetBatteryInformation(m_context.m_nPortIndex, XINPUT_FLAG_GAMEPAD, &xBatteryInfo);

		if (xBatteryInfo.BatteryType == BATTERY_TYPE_UNKNOWN)
		{
			m_ePowerLevel = E_PowerLevel::Unknown;
		}
		else if (xBatteryInfo.BatteryType == BATTERY_TYPE_WIRED)
		{
			m_ePowerLevel = E_PowerLevel::Wired;
		}
		else
		{
			switch (xBatteryInfo.BatteryLevel)
			{
				case BATTERY_LEVEL_EMPTY:
				{
					m_ePowerLevel = E_PowerLevel::Empty;
					break;
				}
				case BATTERY_LEVEL_LOW:
				{
					m_ePowerLevel = E_PowerLevel::Low;
					break;
				}
				case BATTERY_LEVEL_MEDIUM:
				{
					m_ePowerLevel = E_PowerLevel::Medium;
					break;
				}
				case BATTERY_LEVEL_FULL:
				default:
				{
					m_ePowerLevel = E_PowerLevel::Full;
					break;
				}
			}
		}
	}

	// Rumble
	{
		if (m_context.m_bConnected)
		{
			bool bStateDirty = false;
			FXD::F32 fLeftRumble = 0.0f;
			FXD::F32 fRightRumble = 0.0f;

			if (m_bRumbleDirty)
			{
				fLeftRumble = (m_bRumblePaused) ? 0.0 : m_rumbleData.m_fLeftRumble;
				fRightRumble = (m_bRumblePaused) ? 0.0 : m_rumbleData.m_fRightRumble;
				bStateDirty = true;
			}
			else
			{
				if (!m_bRumblePaused && (m_rumbleData.m_fLeftRumble > 0.0f || m_rumbleData.m_fRightRumble > 0.0f || m_rumbleData.m_fDuration > 0.0f))
				{
					m_fRumbleExpiration += dt;
					if (m_fRumbleExpiration > m_rumbleData.m_fDuration)
					{
						m_fRumbleExpiration = 0.0f;
						m_rumbleData.m_fLeftRumble = 0.0f;
						m_rumbleData.m_fRightRumble = 0.0f;
						m_rumbleData.m_fDuration = 0.0f;
						bStateDirty = true;
					}
				}
			}

			if (bStateDirty)
			{
				XINPUT_VIBRATION xVib = {};
				xVib.wLeftMotorSpeed = (WORD)FXD::STD::clamp(fLeftRumble * 65535.0f, 0.0f, 65535.0f);
				xVib.wRightMotorSpeed = (WORD)FXD::STD::clamp(fRightRumble * 65535.0f, 0.0f, 65535.0f);
				XInputSetState(m_context.m_nPortIndex, &xVib);
				m_bRumbleDirty = false;
			}
		}
	}

	XINPUT_STATE nextState = {};
	Memory::MemZero_T(nextState);
	XInputGetState(m_context.m_nPortIndex, &nextState);

	if (m_context.m_bConnected || bWasConnected)
	{
		if ((m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP) != (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP))
		{
			const bool bVal = (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP);
			FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::DPadUp, m_context.m_nPortIndex));
		}
		if ((m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN) != (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN))
		{
			const bool bVal = (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN);
			FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::DPadDown, m_context.m_nPortIndex));
		}
		if ((m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT) != (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT))
		{
			const bool bVal = (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT);
			FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::DPadLeft, m_context.m_nPortIndex));
		}
		if ((m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT) != (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT))
		{
			const bool bVal = (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT);
			FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::DPadRight, m_context.m_nPortIndex));
		}
		if ((m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB) != (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB))
		{
			const bool bVal = (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB);
			FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::LeftThumb, m_context.m_nPortIndex));
		}
		if ((m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB) != (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB))
		{
			const bool bVal = (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB);
			FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::RightThumb, m_context.m_nPortIndex));
		}
		if ((m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER) != (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER))
		{
			const bool bVal = (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER);
			FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::LeftBumper, m_context.m_nPortIndex));
		}
		if ((m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER) != (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER))
		{
			const bool bVal = (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER);
			FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::RightBumper, m_context.m_nPortIndex));
		}
		if ((m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_START) != (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_START))
		{
			const bool bVal = (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_START);
			FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::Start, m_context.m_nPortIndex));
		}
		if ((m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_BACK) != (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_BACK))
		{
			const bool bVal = (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_BACK);
			FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::Back, m_context.m_nPortIndex));
		}
		if ((m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_A) != (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_A))
		{
			const bool bVal = (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_A);
			FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::AButton, m_context.m_nPortIndex));
		}
		if ((m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_B) != (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_B))
		{
			const bool bVal = (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_B);
			FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::BButton, m_context.m_nPortIndex));
		}
		if ((m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_X) != (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_X))
		{
			const bool bVal = (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_X);
			FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::XButton, m_context.m_nPortIndex));
		}
		if ((m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_Y) != (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_Y))
		{
			const bool bVal = (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_Y);
			FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::YButton, m_context.m_nPortIndex));
		}
		if ((m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_GUIDE) != (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_GUIDE))
		{
			const bool bVal = (nextState.Gamepad.wButtons & XINPUT_GAMEPAD_GUIDE);
			FXDEvent()->raise(Input::OnControllerButton(this, bVal, E_GamePadButtons::Guide, m_context.m_nPortIndex));
		}
		if (m_context.m_state.Gamepad.bLeftTrigger != nextState.Gamepad.bLeftTrigger)
		{
			const FXD::F32 fVal = xinput_map_trigger_value(nextState.Gamepad.bLeftTrigger);
			FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::LeftTrigger, m_context.m_nPortIndex));
		}
		if (m_context.m_state.Gamepad.bRightTrigger != nextState.Gamepad.bRightTrigger)
		{
			const FXD::F32 fVal = xinput_map_trigger_value(nextState.Gamepad.bRightTrigger);
			FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::RightTrigger, m_context.m_nPortIndex));
		}
		if (xinput_map_stick_value(m_context.m_state.Gamepad.sThumbLX, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) !=
			xinput_map_stick_value(nextState.Gamepad.sThumbLX, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE))
		{
			const FXD::F32 fVal = xinput_map_stick_value(nextState.Gamepad.sThumbLX, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
			FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::LeftThumbX, m_context.m_nPortIndex));
		}
		if (xinput_map_stick_value(m_context.m_state.Gamepad.sThumbLY, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) !=
			xinput_map_stick_value(nextState.Gamepad.sThumbLY, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE))
		{
			const FXD::F32 fVal = xinput_map_stick_value(nextState.Gamepad.sThumbLY, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
			FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::LeftThumbY, m_context.m_nPortIndex));
		}
		if (xinput_map_stick_value(m_context.m_state.Gamepad.sThumbRX, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) !=
			xinput_map_stick_value(nextState.Gamepad.sThumbRX, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE))
		{
			const FXD::F32 fVal = xinput_map_stick_value(nextState.Gamepad.sThumbRX, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
			FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::RightThumbX, m_context.m_nPortIndex));
		}
		if (xinput_map_stick_value(m_context.m_state.Gamepad.sThumbRY, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) !=
			xinput_map_stick_value(nextState.Gamepad.sThumbRY, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE))
		{
			const FXD::F32 fVal = xinput_map_stick_value(nextState.Gamepad.sThumbRY, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
			FXDEvent()->raise(Input::OnControllerAnalog(this, fVal, E_GamePadButtons::RightThumbY, m_context.m_nPortIndex));
		}

		Memory::MemCopy(&m_context.m_state, &nextState, sizeof(XINPUT_STATE));
	}
	else
	{
		Memory::MemZero_T(m_context.m_state);
	}
}

static CONSTEXPR const Widget::WIDGET_DESC kXInputWidgets[] =
{
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::DPadUp), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::DPadUp), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::DPadDown), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::DPadDown), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::DPadLeft), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::DPadLeft), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::DPadRight), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::DPadRight), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftThumbX), Input::E_Widget_type::FullAxis, Input::GetGamePadName(E_GamePadButtons::LeftThumbX), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftThumbY), Input::E_Widget_type::FullAxis, Input::GetGamePadName(E_GamePadButtons::LeftThumbY), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightThumbX), Input::E_Widget_type::FullAxis, Input::GetGamePadName(E_GamePadButtons::RightThumbX), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightThumbY), Input::E_Widget_type::FullAxis, Input::GetGamePadName(E_GamePadButtons::RightThumbY), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftThumb), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::LeftThumb), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightThumb), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::RightThumb), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftBumper), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::LeftBumper), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightBumper), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::RightBumper), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::LeftTrigger), Input::E_Widget_type::HalfAxis, Input::GetGamePadName(E_GamePadButtons::LeftTrigger), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::RightTrigger), Input::E_Widget_type::HalfAxis, Input::GetGamePadName(E_GamePadButtons::RightTrigger), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::Start), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::Start), Widget::kWidgetFlagStartButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::Back), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::Back), Widget::kWidgetFlagBackButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::AButton), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::AButton), Widget::kWidgetFlagActionButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::BButton), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::BButton), Widget::kWidgetFlagBackButton),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::XButton), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::XButton), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::YButton), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::YButton), 0),
	Widget::WIDGET_DESC(Input::GetGamePadID(E_GamePadButtons::Guide), Input::E_Widget_type::Digital, Input::GetGamePadName(E_GamePadButtons::Guide), Widget::kWidgetFlagGuideButton),
	Widget::WIDGET_DESC()
};

const Widget::WIDGET_DESC* IGamePadXInput::_get_widget_defs(void) const
{
	return kXInputWidgets;
}

void IGamePadXInput::_get_widget_state(FXD::U16 nID, void* pState) const
{
	bool* pDigitalState = (bool*)pState;
	FXD::F32* pAnalogState = (FXD::F32*)pState;
	switch ((E_GamePadButtons)nID)
	{
		case E_GamePadButtons::DPadUp:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP);
			break;
		}
		case E_GamePadButtons::DPadDown:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN);
			break;
		}
		case E_GamePadButtons::DPadLeft:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT);
			break;
		}
		case E_GamePadButtons::DPadRight:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT);
			break;
		}
		case E_GamePadButtons::LeftThumb:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB);
			break;
		}
		case E_GamePadButtons::RightThumb:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB);
			break;
		}
		case E_GamePadButtons::LeftBumper:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER);
			break;
		}
		case E_GamePadButtons::RightBumper:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER);
			break;
		}
		case E_GamePadButtons::Start:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_START);
			break;
		}
		case E_GamePadButtons::Back:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_BACK);
			break;
		}
		case E_GamePadButtons::AButton:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_A);
			break;
		}
		case E_GamePadButtons::BButton:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_B);
			break;
		}
		case E_GamePadButtons::XButton:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_X);
			break;
		}
		case E_GamePadButtons::YButton:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_Y);
			break;
		}
		case E_GamePadButtons::Guide:
		{
			*pDigitalState = !!(m_context.m_state.Gamepad.wButtons & XINPUT_GAMEPAD_GUIDE);
			break;
		}
		case E_GamePadButtons::LeftTrigger:
		{
			*pAnalogState = xinput_map_trigger_value(m_context.m_state.Gamepad.bLeftTrigger);
			break;
		}
		case E_GamePadButtons::RightTrigger:
		{
			*pAnalogState = xinput_map_trigger_value(m_context.m_state.Gamepad.bRightTrigger);
			break;
		}
		case E_GamePadButtons::LeftThumbX:
		{
			*pAnalogState = xinput_map_stick_value(m_context.m_state.Gamepad.sThumbLX, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
			break;
		}
		case E_GamePadButtons::LeftThumbY:
		{
			*pAnalogState = xinput_map_stick_value(m_context.m_state.Gamepad.sThumbLY, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
			break;
		}
		case E_GamePadButtons::RightThumbX:
		{
			*pAnalogState = xinput_map_stick_value(m_context.m_state.Gamepad.sThumbRX, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
			break;
		}
		case E_GamePadButtons::RightThumbY:
		{
			*pAnalogState = xinput_map_stick_value(m_context.m_state.Gamepad.sThumbRY, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
			break;
		}
	}
}
#endif //IsInputXInput()