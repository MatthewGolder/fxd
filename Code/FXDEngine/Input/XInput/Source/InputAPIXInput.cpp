// Creator - MatthewGolder
#include "FXDEngine/Input/Types.h"

#if IsInputXInput()
#include "FXDEngine/Input/XInput/InputApiXInput.h"
#include "FXDEngine/Input/XInput/GamePadXInput.h"
#include "FXDEngine/Core/Debugger.h"

using namespace FXD;
using namespace Input;

// ------
// IInputApiXInput
// - 
// ------
IInputApiXInput::IInputApiXInput(void)
	: m_XInputDLL(nullptr)
{
}

IInputApiXInput::~IInputApiXInput(void)
{
}

bool IInputApiXInput::create_api(void)
{
	bool bRetVal = true;

	DWORD version = 0;

	// 1.4 Ships with Windows 8.
	m_XInputDLL = LoadLibraryExW(L"XInput1_4.dll", nullptr, 0x00000800 /*LOAD_LIBRARY_SEARCH_SYSTEM32*/);
	if (m_XInputDLL == nullptr)
	{
		//version = (1 << 16) | 3;
		// 1.3 can be installed as a redistributable component.
		m_XInputDLL = LoadLibraryExW(L"XInput1_3.dll", nullptr, 0x00000800 /*LOAD_LIBRARY_SEARCH_SYSTEM32*/);
	}
	if (m_XInputDLL == nullptr)
	{
		m_XInputDLL = LoadLibraryExW(L"bin\\XInput1_3.dll", nullptr, 0x00000800 /*LOAD_LIBRARY_SEARCH_SYSTEM32*/);
	}
	if (m_XInputDLL == nullptr)
	{
		// "9.1.0" Ships with Vista and Win7, and is more limited than 1.3+ (e.g. XInputGetStateEx is not available.) 
		m_XInputDLL = LoadLibraryExW(L"XInput9_1_0.dll", nullptr, 0x00000800 /*LOAD_LIBRARY_SEARCH_SYSTEM32*/);
	}

	if (m_XInputDLL == nullptr)
	{
		PRINT_WARN << "Input: m_XInputDLL failed to load";
		bRetVal = false;
	}

	return bRetVal;
}

bool IInputApiXInput::release_api(void)
{
	if (m_XInputDLL != nullptr)
	{
		FreeLibrary(m_XInputDLL);
		m_XInputDLL = nullptr;
	}
	return true;
}

void IInputApiXInput::_process(FXD::F32 /*dt*/)
{
}
#endif //IsInputXInput()