// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_INPUT_MAPPING_H
#define FXDENGINE_INPUT_MAPPING_H

#include "FXDEngine/Input/Source/Mapping.inl"

namespace FXD
{
	namespace Input
	{
		CONSTEXPR const Core::StringView8 GetMouseName(const Input::E_MouseButtons eMouse)
		{
			return getMouseDesc(eMouse).strName;
		}
		CONSTEXPR FXD::U16 GetMouseID(const Input::E_MouseButtons eMouse)
		{
			return getMouseDesc(eMouse).nID;
		}

		CONSTEXPR const Core::StringView8 GetGamePadName(const Input::E_GamePadButtons eGamePad)
		{
			return getGamepadDesc(eGamePad).strName;
		}
		CONSTEXPR const FXD::U16 GetGamePadID(const Input::E_GamePadButtons eGamePad)
		{
			return getGamepadDesc(eGamePad).nID;
		}

	} //namespace Input
} //namespace FXD
#endif //FXDENGINE_INPUT_MAPPING_H