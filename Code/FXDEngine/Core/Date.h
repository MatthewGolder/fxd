// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_DATE_H
#define FXDENGINE_CORE_DATE_H

#include "FXDEngine/Core/String.h"
#include <time.h>

namespace FXD
{
	namespace Core
	{
		// ------
		// Date
		// -
		// ------
		class Date
		{
		public:
			Date(void);
			Date(time_t utc);
			~Date(void);

			bool operator<(const Core::Date& rhs) const
			{
				return (m_utc < rhs.m_utc);
			}
			bool operator>(const Core::Date& rhs) const
			{
				return (m_utc > rhs.m_utc);
			}

			static Core::Date now(void);

			Core::String8 get_string(void) const;
			FXD::S32 get_day(void) const;
			FXD::S32 get_month(void) const;
			FXD::S32 get_year(void) const;
			FXD::S32 get_hours(void) const;
			FXD::S32 get_minutes(void) const;
			FXD::S32 get_seconds(void) const;

		private:
			time_t m_utc;
		};

		// IStreamOut
		inline IStreamOut& operator<<(IStreamOut& ostr, Core::Date const& rhs)
		{
			return ostr << rhs.get_string();
		}

	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_DATE_H