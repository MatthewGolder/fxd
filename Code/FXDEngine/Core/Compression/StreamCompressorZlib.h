// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_COMPRESSION_STREACOMPRESSORZLIB_H
#define FXDENGINE_CORE_COMPRESSION_STREACOMPRESSORZLIB_H

#include "FXDEngine/Core/Compression/StreamCompressor.h"
#include <3rdParty/zlib/zlib.h>

namespace FXD
{
	namespace Core
	{
		// ------
		// IStreamCompressorZlib
		// -
		// ------
		class IStreamCompressorZlib FINAL : public Core::IStreamCompressor
		{
		public:
			IStreamCompressorZlib(Core::IStreamOut* pStream, Core::E_CompressionQuality eQuality);
			~IStreamCompressorZlib(void);

			FXD::U64 compress_size(const void* pSrc, const FXD::U64 nSize) FINAL;

		private:
			z_stream m_zInfoC;
		};

		// ------
		// IStreamCompressorStreamZlib
		// -
		// ------
		class IStreamCompressorStreamZlib FINAL : public Core::IStreamCompressor
		{
		public:
			IStreamCompressorStreamZlib(Core::IStreamOut* pStream, Core::E_CompressionQuality eQuality);
			~IStreamCompressorStreamZlib(void);

			FXD::U64 compress_size(const void* pSrc, const FXD::U64 nSize) FINAL;

		private:
			z_stream m_zInfoC;
			FXD::U8 m_buffer[BLOCK_BYTES];
		};

	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_COMPRESSION_STREACOMPRESSORZLIB_H