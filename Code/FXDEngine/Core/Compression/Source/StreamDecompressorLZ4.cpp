// Creator - MatthewGolder
#include "FXDEngine/Core/Compression/StreamDecompressorLZ4.h"
#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Core/StreamIn.h"
#include "FXDEngine/Memory/Allocator/Aligned.h"
#include <3rdParty/lz4/lz4frame.h>

using namespace FXD;
using namespace Core;

// ------
// IStreamDecompressorLZ4
// -
// ------
IStreamDecompressorLZ4::IStreamDecompressorLZ4(Core::IStreamIn* pStream, Core::E_CompressionQuality eQuality)
	: IStreamDecompressor(pStream, eQuality)
{
}

IStreamDecompressorLZ4::~IStreamDecompressorLZ4(void)
{
}

FXD::U64 IStreamDecompressorLZ4::decompress_size(void* pDst, const FXD::U64 nSize)
{
	FXD::U64 nDecompressedBytes = 0;
	FXD::U64 nCmpSize = 0;
	FXD::U64 nBytesRead = m_pStream->read_size(&nCmpSize, sizeof(nCmpSize));
	if (nBytesRead == 0)
	{
		return 0;
	}

	FXD::U8* pCmpBuffer = (FXD::U8*)FXD::Memory::Alloc::Default.allocate(nCmpSize);
	nBytesRead = m_pStream->read_size(pCmpBuffer, nCmpSize);
	if (nBytesRead != nCmpSize)
	{
		PRINT_ASSERT << "Core: LZ4 " << "";
	}

	FXD::S32 nErr = LZ4_decompress_safe((const char*)pCmpBuffer, (char*)pDst, nCmpSize, nSize);
	if (nErr <= 0)
	{
		PRINT_ASSERT << "Core: LZ4 " << LZ4F_getErrorName(nErr);
	}
	else
	{
		const FXD::U64 nTotalCmpBytes = nErr;

		nDecompressedBytes = nErr;
	}
	FXD::Memory::Alloc::Default.deallocate(pCmpBuffer);

	return nDecompressedBytes;
}

// ------
// IStreamDecompressorStreamLZ4
// -
// ------
IStreamDecompressorStreamLZ4::IStreamDecompressorStreamLZ4(Core::IStreamIn* pStream, Core::E_CompressionQuality eQuality)
	: IStreamDecompressor(pStream, eQuality)
{
	Memory::MemZero_T(m_lDecodeStream);
	Memory::MemZero(m_dstBuffer[0], BLOCK_BYTES);
	Memory::MemZero(m_dstBuffer[1], BLOCK_BYTES);
	Memory::MemZero(m_cmpBuffer, LZ4_COMPRESSBOUND(BLOCK_BYTES));
	LZ4_setStreamDecode(&m_lDecodeStream, NULL, 0);
}

IStreamDecompressorStreamLZ4::~IStreamDecompressorStreamLZ4(void)
{
}

FXD::U64 IStreamDecompressorStreamLZ4::decompress_size(void* pDst, const FXD::U64 nSize)
{
	FXD::U64 nTotalBytesRead = 0;
	FXD::U64 nBytesOffset = 0;
	FXD::U64 nDecompressedBytes = 0;
	FXD::S8 nBufferIdx = 0;

	for (;;)
	{
		FXD::U64 nCmpSize = 0;
		FXD::U64 nBytesRead = m_pStream->read_size(&nCmpSize, sizeof(nCmpSize));
		if (nBytesRead == 0)
		{
			break;
		}

		FXD::U8* pDstData = &((FXD::U8*)pDst)[nBytesOffset];
		FXD::U8* pDstBuffer = m_dstBuffer[nBufferIdx];
		FXD::U8* pCmpBuffer = m_cmpBuffer;

		nBytesRead = m_pStream->read_size(pCmpBuffer, nCmpSize);
		if (nBytesRead != nCmpSize)
		{
			break;
		}

		FXD::S32 nErr = LZ4_decompress_safe_continue(&m_lDecodeStream, (const char*)pCmpBuffer, (char*)pDstBuffer, nCmpSize, BLOCK_BYTES);
		if (nErr <= 0)
		{
			PRINT_ASSERT << "Core: LZ4 " << LZ4F_getErrorName(nErr);
		}
		else
		{
			const FXD::U64 nTotalCmpBytes = nErr;

			Memory::MemCopy(pDstData, pDstBuffer, nTotalCmpBytes);
			nBytesOffset += nTotalCmpBytes;
			nDecompressedBytes += nTotalCmpBytes;
		}

		nTotalBytesRead += nBytesRead;
		nBufferIdx = (nBufferIdx + 1) % 2;
	}

	return nDecompressedBytes;
}