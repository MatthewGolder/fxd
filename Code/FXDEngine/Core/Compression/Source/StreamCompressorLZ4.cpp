// Creator - MatthewGolder
#include "FXDEngine/Core/Compression/StreamCompressorLZ4.h"
#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Core/StreamOut.h"
#include "FXDEngine/Memory/Allocator/Aligned.h"
#include <3rdParty/lz4/lz4frame.h>

using namespace FXD;
using namespace Core;

// ------
// IStreamCompressorLZ4
// -
// ------
IStreamCompressorLZ4::IStreamCompressorLZ4(Core::IStreamOut* pStream, Core::E_CompressionQuality eQuality)
	: IStreamCompressor(pStream, eQuality)
{
}

IStreamCompressorLZ4::~IStreamCompressorLZ4(void)
{
}

FXD::U64 IStreamCompressorLZ4::compress_size(const void* pSrc, const FXD::U64 nSize)
{
	FXD::U64 nBytesProcessed = 0;

	const FXD::U64 nCmpSize = LZ4_compressBound(nSize);
	FXD::U8* pCmpPtr = (FXD::U8*)FXD::Memory::Alloc::Default.allocate(nCmpSize);
	const FXD::S32 nErr = LZ4_compress_fast((char*)pSrc, (char*)pCmpPtr, nSize, nCmpSize, Core::Compression::GetQualityVal(Core::E_CompressionType::LZ4, m_eQuality));
	if (nErr <= 0)
	{
		PRINT_ASSERT << "Core: LZ4 " << LZ4F_getErrorName(nErr);
	}
	else
	{
		const FXD::U64 nTotalCmpBytes = nErr;

		m_nTotalUnCompressed += nSize;
		m_nTotalCompressed += sizeof(nTotalCmpBytes);
		m_nTotalCompressed += nTotalCmpBytes;
		
		nBytesProcessed += m_pStream->write_size(&nTotalCmpBytes, sizeof(nTotalCmpBytes));
		nBytesProcessed += m_pStream->write_size(pCmpPtr, nTotalCmpBytes);
	}

	FXD::Memory::Alloc::Default.deallocate(pCmpPtr);

	return nBytesProcessed;
}

// ------
// IStreamCompressorStreamLZ4
// -
// ------
IStreamCompressorStreamLZ4::IStreamCompressorStreamLZ4(Core::IStreamOut* pStream, Core::E_CompressionQuality eQuality)
	: IStreamCompressor(pStream, eQuality)
{
	Memory::MemZero_T(m_lStream);
	Memory::MemZero(m_dstBuffer[0], BLOCK_BYTES);
	Memory::MemZero(m_dstBuffer[1], BLOCK_BYTES);
	Memory::MemZero(m_cmpBuffer, LZ4_COMPRESSBOUND(BLOCK_BYTES));
	LZ4_initStream(&m_lStream, sizeof(m_lStream));
}

IStreamCompressorStreamLZ4::~IStreamCompressorStreamLZ4(void)
{
}

FXD::U64 IStreamCompressorStreamLZ4::compress_size(const void* pSrc, const FXD::U64 nSize)
{
	FXD::U64 nRemainingBytes = nSize;
	FXD::U64 nBytesOffset = 0;
	FXD::U64 nBytesProcessed = 0;
	FXD::S8 nBufferIdx = 0;

	while(nRemainingBytes != 0)
	{
		FXD::U64 nBytesToWrite = 0;
		if (nRemainingBytes > BLOCK_BYTES)
		{
			nBytesToWrite = BLOCK_BYTES;
			nRemainingBytes -= BLOCK_BYTES;
		}
		else
		{
			nBytesToWrite = nRemainingBytes;
			nRemainingBytes -= nRemainingBytes;
		}

		FXD::U8* pSrcData = &((FXD::U8*)pSrc)[nBytesOffset];
		FXD::U8* pDstPtr = m_dstBuffer[nBufferIdx];
		Memory::MemCopy(pDstPtr, pSrcData, nBytesToWrite);

		FXD::U8* pCmpPtr = m_cmpBuffer;
		const FXD::S32 nErr = LZ4_compress_fast_continue(&m_lStream, (const char*)pDstPtr, (char*)pCmpPtr, nBytesToWrite, sizeof(m_cmpBuffer), Core::Compression::GetQualityVal(Core::E_CompressionType::LZ4Streamed, m_eQuality));
		if (nErr <= 0)
		{
			PRINT_ASSERT << "Core: LZ4 " << LZ4F_getErrorName(nErr);
		}
		else
		{
			const FXD::U64 nTotalCmpBytes = nErr;

			m_nTotalUnCompressed += nBytesToWrite;
			m_nTotalCompressed += sizeof(nTotalCmpBytes);
			m_nTotalCompressed += nTotalCmpBytes;

			nBytesProcessed += m_pStream->write_size(&nTotalCmpBytes, sizeof(nTotalCmpBytes));
			nBytesProcessed += m_pStream->write_size(pCmpPtr, nTotalCmpBytes);
		}

		nBytesOffset += nBytesToWrite;
		nBufferIdx = (nBufferIdx + 1) % 2;
	}

	return nBytesProcessed;
}