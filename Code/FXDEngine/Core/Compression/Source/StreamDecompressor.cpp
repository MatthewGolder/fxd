// Creator - MatthewGolder
#include "FXDEngine/Core/Compression/StreamDecompressor.h"

using namespace FXD;
using namespace Core;

// ------
// IStreamDecompressor
// -
// ------
IStreamDecompressor::IStreamDecompressor(Core::IStreamIn* pStream, Core::E_CompressionQuality eQuality)
	: m_pStream(pStream)
	, m_eQuality(eQuality)
{
}

IStreamDecompressor::~IStreamDecompressor(void)
{
}