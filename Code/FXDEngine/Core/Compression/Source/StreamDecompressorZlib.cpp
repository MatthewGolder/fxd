// Creator - MatthewGolder
#include "FXDEngine/Core/Compression/StreamDecompressorZlib.h"
#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Core/StreamIn.h"
#include "FXDEngine/Memory/Allocator/Aligned.h"

using namespace FXD;
using namespace Core;

namespace
{
	FXD::U32 GetMaxCompressedLen(int nLenSrc)
	{
		FXD::U32 n16kBlocks = ((nLenSrc + 16383) / 16384); // round up any fraction of a block
		return (nLenSrc + 6 + (n16kBlocks * 5));
	}
}

// ------
// IStreamDecompressorZlib
// -
// ------
IStreamDecompressorZlib::IStreamDecompressorZlib(Core::IStreamIn* pStream, Core::E_CompressionQuality eQuality)
	: IStreamDecompressor(pStream, eQuality)
{
	Memory::MemZero_T(m_zInfoD);
	FXD::S32 nErr = inflateInit(&m_zInfoD);
}

IStreamDecompressorZlib::~IStreamDecompressorZlib(void)
{
	FXD::S32 nErr = inflateEnd(&m_zInfoD);
}

FXD::U64 IStreamDecompressorZlib::decompress_size(void* pDst, const FXD::U64 nSize)
{
	FXD::U32 nCompSize;
	FXD::U32 nBytesRead = m_pStream->read_size(&nCompSize, sizeof(nCompSize));
	if (nBytesRead == 0)
	{
		return 0;
	}

	FXD::U8* pCompData = (FXD::U8*)FXD::Memory::Alloc::Default.allocate(nCompSize);
	m_pStream->read_size(pCompData, nCompSize);

	//PRINT_COND_ASSERT((nSize >= nCompSize), "Core: ");

	m_zInfoD.next_in = (FXD::U8*)pCompData;
	m_zInfoD.avail_in = nCompSize;
	m_zInfoD.next_out = (FXD::U8*)pDst;
	m_zInfoD.avail_out = nSize;

	FXD::U32 nReadSize = 0;
	FXD::S32 nErr = inflate(&m_zInfoD, Z_FINISH);
	if (nErr == Z_STREAM_END)
	{
		nReadSize = m_zInfoD.total_out;
	}
	FXD::Memory::Alloc::Default.deallocate(pCompData);

	return nReadSize;
}