// Creator - MatthewGolder
#include "FXDEngine/Core/Compression/StreamCompressorZlib.h"
#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Core/StreamOut.h"
#include "FXDEngine/Memory/Allocator/Aligned.h"

using namespace FXD;
using namespace Core;

namespace
{
	FXD::U64 GetMaxCompressedLen(int nLenSrc)
	{
		FXD::U64 n16kBlocks = ((nLenSrc + 16383) / 16384); // round up any fraction of a block
		return (nLenSrc + 6 + (n16kBlocks * 5));
	}

	/*
	void* pppAlloc(void* opaque, uInt items, uInt size)
	{
		float f = 0;
		return 0;
	}

	void pppFree(void* opaque, void* address)
	{
		float f = 0;
	}
	*/
}

// ------
// IStreamCompressorZlib
// -
// ------
IStreamCompressorZlib::IStreamCompressorZlib(Core::IStreamOut* pStream, Core::E_CompressionQuality eQuality)
	: IStreamCompressor(pStream, eQuality)
{
	Memory::MemZero_T(m_zInfoC);
}

IStreamCompressorZlib::~IStreamCompressorZlib(void)
{
}

FXD::U64 IStreamCompressorZlib::compress_size(const void* pSrc, const FXD::U64 nSize)
{
	FXD::U64 nBytesProcessed = 0;

	const FXD::U32 nCmpSize = GetMaxCompressedLen(nSize);
	FXD::U8* pCmpPtr = (FXD::U8*)FXD::Memory::Alloc::Default.allocate(nCmpSize);

	{
		FXD::S32 nErr = deflateInit(&m_zInfoC, Core::Compression::GetQualityVal(Core::E_CompressionType::ZLib, m_eQuality));
		m_zInfoC.zalloc = (alloc_func)0;
		m_zInfoC.zfree = (free_func)0;
		m_zInfoC.opaque = (voidpf)0;
		m_zInfoC.next_in = (FXD::U8*)pSrc;
		m_zInfoC.avail_in = nSize;
		m_zInfoC.next_out = pCmpPtr;
		m_zInfoC.avail_out = nCmpSize;

		nErr = 0;
		while (m_zInfoC.total_in != nSize && m_zInfoC.total_out < nCmpSize)
		{
			m_zInfoC.avail_in = m_zInfoC.avail_out = 1; /* force small buffers */
			nErr = deflate(&m_zInfoC, Z_NO_FLUSH);
			PRINT_COND_ASSERT(nErr == Z_OK, "deflate");
		}
		for (;;)
		{
			m_zInfoC.avail_out = 1;
			nErr = deflate(&m_zInfoC, Z_FINISH);
			if (nErr == Z_STREAM_END)
			{
				break;
			}
			PRINT_COND_ASSERT(nErr == Z_OK, "deflate");
		}

		nErr = deflateEnd(&m_zInfoC);
		float f = 0;
	}

	FXD::S32 nErr = deflate(&m_zInfoC, Z_FINISH);
	if (nErr == Z_STREAM_END)
	{
		const FXD::U64 nTotalCmpBytes = nErr;

		m_nTotalUnCompressed += nSize;
		m_nTotalCompressed += sizeof(nTotalCmpBytes);
		m_nTotalCompressed += nTotalCmpBytes;

		m_pStream->write_size(&nTotalCmpBytes, sizeof(nTotalCmpBytes));
		nBytesProcessed += m_pStream->write_size(pCmpPtr, nTotalCmpBytes);
	}
	FXD::Memory::Alloc::Default.deallocate(pCmpPtr);

	return nBytesProcessed;
}

// ------
// IStreamCompressorStreamZlib
// -
// ------
IStreamCompressorStreamZlib::IStreamCompressorStreamZlib(Core::IStreamOut* pStream, Core::E_CompressionQuality eQuality)
	: IStreamCompressor(pStream, eQuality)
{
	Memory::MemZero_T(m_zInfoC);
	Memory::MemZero(m_buffer, BLOCK_BYTES);
	FXD::S32 nErr = deflateInit(&m_zInfoC, Core::Compression::GetQualityVal(Core::E_CompressionType::ZLib, m_eQuality));
}

IStreamCompressorStreamZlib::~IStreamCompressorStreamZlib(void)
{
	FXD::S32 nErr = deflateEnd(&m_zInfoC);
}

FXD::U64 IStreamCompressorStreamZlib::compress_size(const void* pSrc, const FXD::U64 nSize)
{
	m_nTotalUnCompressed += nSize;

	FXD::U64 nRemainingBytes = nSize;
	FXD::U64 nBytesOffset = 0;
	FXD::U64 nWrittenBytes = 0;
	FXD::S8 nBufferIdx = 0;

	while (nRemainingBytes != 0)
	{
		FXD::U32 nBytesToWrite = 0;
		if (nRemainingBytes > BLOCK_BYTES)
		{
			nBytesToWrite = BLOCK_BYTES;
			nRemainingBytes -= BLOCK_BYTES;
		}
		else
		{
			nBytesToWrite = nRemainingBytes;
			nRemainingBytes -= nRemainingBytes;
		}

		m_zInfoC.next_in = &((FXD::U8*)pSrc)[nBytesOffset];
		m_zInfoC.avail_in = nBytesToWrite;
		m_zInfoC.next_out = m_buffer;
		m_zInfoC.avail_out = BLOCK_BYTES;

		for (;;)
		{
			FXD::S32 nErr = deflate(&m_zInfoC, Z_NO_FLUSH);
			if (nErr == Z_OK)
			{
				float f = 0;
			}
			if (nErr == Z_STREAM_END)
			{
				FXD::U32 nOutSize = m_zInfoC.total_out;

				m_pStream->write_size(&nOutSize, sizeof(nOutSize));
				nWrittenBytes += m_pStream->write_size(m_buffer, nOutSize);
			}
		}

		nBytesOffset += nBytesToWrite;
		nBufferIdx = (nBufferIdx + 1) % 2;
	}

	return nWrittenBytes;
}