// Creator - MatthewGolder
#include "FXDEngine/Core/Compression/StreamCompressor.h"

using namespace FXD;
using namespace Core;

// ------
// IStreamCompressor
// -
// ------
IStreamCompressor::IStreamCompressor(Core::IStreamOut* pStream, Core::E_CompressionQuality eQuality)
	: m_pStream(pStream)
	, m_eQuality(eQuality)
	, m_nTotalCompressed(0)
	, m_nTotalUnCompressed(0)
{
}

IStreamCompressor::~IStreamCompressor(void)
{
}