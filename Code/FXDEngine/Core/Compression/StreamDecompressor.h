// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_COMPRESSION_STREAMDECOMPRESSOR_H
#define FXDENGINE_CORE_COMPRESSION_STREAMDECOMPRESSOR_H

#include "FXDEngine/Core/Compression.h"

namespace FXD
{
	namespace Core
	{
		enum
		{
			BLOCK_BYTES = 1024 * 8,
		};

		class IStreamIn;
		// ------
		// IStreamDecompressor
		// -
		// ------
		class IStreamDecompressor
		{
		public:
			IStreamDecompressor(Core::IStreamIn* pStream, Core::E_CompressionQuality eQuality);
			virtual ~IStreamDecompressor(void);

			virtual FXD::U64 decompress_size(void* pDst, const FXD::U64 nSize) PURE;

		protected:
			Core::IStreamIn* m_pStream;
			Core::E_CompressionQuality m_eQuality;
		};

	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_COMPRESSION_STREAMDECOMPRESSOR_H