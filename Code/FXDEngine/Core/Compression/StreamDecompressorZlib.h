// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_COMPRESSION_STREAMDECOMPRESSORZLIB_H
#define FXDENGINE_CORE_COMPRESSION_STREAMDECOMPRESSORZLIB_H

#include "FXDEngine/Core/Compression/StreamDecompressor.h"
#include <3rdParty/zlib/zlib.h>

namespace FXD
{
	namespace Core
	{
		// ------
		// IStreamDecompressorZlib
		// -
		// ------
		class IStreamDecompressorZlib FINAL : public Core::IStreamDecompressor
		{
		public:
			IStreamDecompressorZlib(Core::IStreamIn* pStream, Core::E_CompressionQuality eQuality);
			~IStreamDecompressorZlib(void);

			FXD::U64 decompress_size(void* pDst, const FXD::U64 nSize) FINAL;

		private:
			z_stream m_zInfoD;
		};

	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_COMPRESSION_STREAMDECOMPRESSORZLIB_H