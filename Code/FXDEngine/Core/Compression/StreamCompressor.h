// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_COMPRESSION_STREACOMPRESSOR_H
#define FXDENGINE_CORE_COMPRESSION_STREACOMPRESSOR_H

#include "FXDEngine/Core/Compression.h"

namespace FXD
{
	namespace Core
	{
		enum
		{
			BLOCK_BYTES = 1024 * 8,
		};

		class IStreamOut;
		// ------
		// IStreamCompressor
		// -
		// ------
		class IStreamCompressor
		{
		public:
			IStreamCompressor(Core::IStreamOut* pStream, Core::E_CompressionQuality eQuality);
			virtual ~IStreamCompressor(void);

			virtual FXD::U64 compress_size(const void* pData, const FXD::U64 nSize) PURE;

		protected:
			Core::IStreamOut* m_pStream;
			Core::E_CompressionQuality m_eQuality;
			FXD::U64 m_nTotalCompressed;
			FXD::U64 m_nTotalUnCompressed;
		};
	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_COMPRESSION_STREACOMPRESSOR_H