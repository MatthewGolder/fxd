// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_COMPRESSION_STREAMDECOMPRESSORLZ4_H
#define FXDENGINE_CORE_COMPRESSION_STREAMDECOMPRESSORLZ4_H

#include "FXDEngine/Core/Compression/StreamDecompressor.h"
#include <3rdParty/lz4/lz4.h>

namespace FXD
{
	namespace Core
	{
		// ------
		// IStreamDecompressorLZ4
		// -
		// ------
		class IStreamDecompressorLZ4 FINAL : public Core::IStreamDecompressor
		{
		public:
			IStreamDecompressorLZ4(Core::IStreamIn* pStream, Core::E_CompressionQuality eQuality);
			~IStreamDecompressorLZ4(void);

			FXD::U64 decompress_size(void* pDst, const FXD::U64 nSize) FINAL;

		private:
		};

		// ------
		// IStreamDecompressorStreamLZ4
		// -
		// ------
		class IStreamDecompressorStreamLZ4 FINAL : public Core::IStreamDecompressor
		{
		public:
			IStreamDecompressorStreamLZ4(Core::IStreamIn* pStream, Core::E_CompressionQuality eQuality);
			~IStreamDecompressorStreamLZ4(void);

			FXD::U64 decompress_size(void* pDst, const FXD::U64 nSize) FINAL;

		private:
			LZ4_streamDecode_t m_lDecodeStream;
			FXD::U8 m_dstBuffer[2][BLOCK_BYTES];
			FXD::U8 m_cmpBuffer[LZ4_COMPRESSBOUND(BLOCK_BYTES)];
		};

	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_COMPRESSION_STREAMDECOMPRESSORLZ4_H