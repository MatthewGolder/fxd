// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_COMPRESSION_STREACOMPRESSORLZ4_H
#define FXDENGINE_CORE_COMPRESSION_STREACOMPRESSORLZ4_H

#include "FXDEngine/Core/Compression/StreamCompressor.h"
#include <3rdParty/lz4/lz4.h>

namespace FXD
{
	namespace Core
	{
		// ------
		// IStreamCompressorLZ4
		// -
		// ------
		class IStreamCompressorLZ4 FINAL : public Core::IStreamCompressor
		{
		public:
			IStreamCompressorLZ4(Core::IStreamOut* pStream, Core::E_CompressionQuality eQuality);
			~IStreamCompressorLZ4(void);

			FXD::U64 compress_size(const void* pSrc, const FXD::U64 nSize) FINAL;
		};

		// ------
		// IStreamCompressorStreamLZ4
		// -
		// ------
		class IStreamCompressorStreamLZ4 FINAL : public Core::IStreamCompressor
		{
		public:
			IStreamCompressorStreamLZ4(Core::IStreamOut* pStream, Core::E_CompressionQuality eQuality);
			~IStreamCompressorStreamLZ4(void);

			FXD::U64 compress_size(const void* pSrc, const FXD::U64 nSize) FINAL;

		private:
			LZ4_stream_t m_lStream;
			FXD::U8 m_dstBuffer[2][BLOCK_BYTES];
			FXD::U8 m_cmpBuffer[LZ4_COMPRESSBOUND(BLOCK_BYTES)];
		};

	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_COMPRESSION_STREACOMPRESSORLZ4_H