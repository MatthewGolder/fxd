// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_EXPRESSION_H
#define FXDENGINE_CORE_EXPRESSION_H

#include "FXDEngine/Core/String.h"
#include "FXDEngine/Memory/MemHandle.h"

namespace FXD
{
	namespace Core
	{
		// ------
		// E_ExpressionCode
		// -
		// ------
		enum class E_ExpressionCode
		{
			Invalid,
			FloatLiteral,
			BoolLiteral,
			FloatVariable,
			BoolVariable,
			FloatAdd,
			FloatSubtract,
			FloatMultiply,
			FloatDivide,
			FloatGreater,
			FloatGreaterEqual,
			FloatLess,
			FloatLessEqual,
			FloatNegate,
			BoolNot,
			BoolAdd,
			BoolOr,
			ConvertBoolFloat,
			Count,
			Unknown = 0xffff
		};

		// ------
		// E_ExpressionType
		// -
		// ------
		enum class E_ExpressionType
		{
			Float,
			Bool,
			Count,
			Unknown = 0xffff
		};

		// ------
		// IVariableProvider
		// -
		// ------
		class IVariableProvider
		{
		public:
			IVariableProvider(void)
			{}
			virtual ~IVariableProvider(void)
			{}

			// Get an index and type for a variable name.
			virtual FXD::S32 get_variableID_and_type(const Core::String8& strName, Core::E_ExpressionType& eType) const PURE;

			// Get the value of a variable by index.
			virtual void get_variable_data(FXD::U16 nID, void* pData) const PURE;
		};

		// ------
		// Expression
		// -
		// ------
		class Expression
		{
		public:
			Expression(void)
				: m_pData(nullptr)
			{}
			~Expression(void)
			{}

			// Set from a compiled data blob.
			void set_from_compiled(const Memory::MemHandle& memHandle, Core::E_ExpressionType eType);

			// Parse an expression.
			void parse(const Core::String8& strExpr, const Core::IVariableProvider& vars);

			// Get the type of the result of the expression.
			inline Core::E_ExpressionType get_type(void) const { return m_eType; }

			// Evaluate the expression.
			void evaluate(const Core::IVariableProvider& vars, void* pData) const;

			// Convert an expression type to a string.
			static const Core::String8 type_to_string(const Core::E_ExpressionType eType);

		private:
			void* m_pData;
			Memory::MemHandle m_data;
			Core::E_ExpressionType m_eType;
		};

	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_EXPRESSION_H