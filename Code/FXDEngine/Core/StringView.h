// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_STRINGVIEW_H
#define FXDENGINE_CORE_STRINGVIEW_H

#include "FXDEngine/Container/Iterator.h"
#include "FXDEngine/Core/Algorithm.h"
#include "FXDEngine/Core/CharTraits.h"
#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Math/Limits.h"

namespace FXD
{
	namespace Core
	{
		template < typename charT, typename traits = Core::CharTraits< charT > >
		class StringViewBase;

		using StringView8 = StringViewBase< FXD::UTF8 >;
		using StringView16 = StringViewBase< FXD::UTF16 >;

		// ------
		// StringViewBase
		// -
		// ------
		template < typename charT, typename traits >
		class StringViewBase
		{
		public:
			using my_type = StringViewBase< charT, traits >;
			using value_type = charT;
			using traits_type = traits;
			using pointer = charT* ;
			using const_pointer = const charT*;
			using reference = charT & ;
			using const_reference = const charT&;
			using size_type = FXD::U32;
			using difference_type = std::ptrdiff_t;
			using iterator = pointer;
			using const_iterator = const_pointer;
			using reverse_iterator = FXD::STD::reverse_iterator< iterator >;
			using const_reverse_iterator = FXD::STD::reverse_iterator< const_iterator >;

			friend iterator;
			friend const_iterator;
			friend reverse_iterator;
			friend const_reverse_iterator;

			static CONSTEXPR const size_type npos = size_type(-1); // bad/missing length/position

			CONSTEXPR StringViewBase(void) NOEXCEPT;
			CONSTEXPR StringViewBase(const my_type& rhs) NOEXCEPT;
			CONSTEXPR StringViewBase(const_pointer pStr, size_type nLen);
			CONSTEXPR StringViewBase(const_pointer pStr);

			my_type& operator=(const my_type& rhs) NOEXCEPT;

			// Iterator Support
			CONSTEXPR const_iterator begin(void) const NOEXCEPT;
			CONSTEXPR const_iterator cbegin(void) const NOEXCEPT;
			CONSTEXPR const_iterator end(void) const NOEXCEPT;
			CONSTEXPR const_iterator cend(void) const NOEXCEPT;

			CONSTEXPR const_reverse_iterator rbegin(void) const NOEXCEPT;
			CONSTEXPR const_reverse_iterator crbegin(void) const NOEXCEPT;
			CONSTEXPR const_reverse_iterator rend(void) const NOEXCEPT;
			CONSTEXPR const_reverse_iterator crend(void) const NOEXCEPT;

			// Access
			CONSTEXPR const_reference operator[](size_type nOffSet) const NOEXCEPT;
			CONSTEXPR const_reference at(size_type nOffSet) const;

			CONSTEXPR const_reference front(void) const;
			CONSTEXPR const_reference back(void) const;

			CONSTEXPR const_pointer c_str(void) const NOEXCEPT;
			CONSTEXPR const_pointer data(void) const NOEXCEPT;

			CONSTEXPR14 my_type substr(size_type nOffSet = 0, size_type nCount = npos) const;

			// Comparisons
			CONSTEXPR14 FXD::S32 compare(my_type rhs) const NOEXCEPT;
			CONSTEXPR FXD::S32 compare(size_type nOffSet, size_type nLength, my_type str) const NOEXCEPT;
			CONSTEXPR FXD::S32 compare(size_type nOffSet, size_type nLength, my_type str, size_type nSubpos, size_type nSublen) const;
			CONSTEXPR FXD::S32 compare(const_pointer pStr) const;
			CONSTEXPR FXD::S32 compare(size_type nOffSet, size_type nLength, const_pointer pStr) const;
			CONSTEXPR FXD::S32 compare(size_type nOffSet, size_type nLength, const_pointer pStr, size_type nCount) const;

			CONSTEXPR14 FXD::S32 compare_no_case(my_type rhs) const NOEXCEPT;
			CONSTEXPR FXD::S32 compare_no_case(size_type nOffSet, size_type nLength, my_type str) const NOEXCEPT;
			CONSTEXPR FXD::S32 compare_no_case(size_type nOffSet, size_type nLength, my_type str, size_type nSubpos, size_type nSublen) const;
			CONSTEXPR FXD::S32 compare_no_case(const_pointer pStr) const;
			CONSTEXPR FXD::S32 compare_no_case(size_type nOffSet, size_type nLength, const_pointer pStr) const;
			CONSTEXPR FXD::S32 compare_no_case(size_type nOffSet, size_type nLength, const_pointer pPtr, size_type nCount) const;

			// Find operations (case sensitive)
			CONSTEXPR14 size_type find(my_type str, size_type nOffSet = 0) const NOEXCEPT;
			CONSTEXPR14 size_type find(const_pointer pStr, size_type nOffSet = 0) const NOEXCEPT;
			CONSTEXPR14 size_type find(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT;
			CONSTEXPR14 size_type find(charT ch, size_type nOffSet = 0) const NOEXCEPT;

			// Reverse find operations (case sensitive)
			CONSTEXPR14 size_type rfind(my_type str, size_type nOffSet = npos) const NOEXCEPT;
			CONSTEXPR14 size_type rfind(const_pointer pStr, size_type nOffSet = npos) const NOEXCEPT;
			CONSTEXPR14 size_type rfind(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT;
			CONSTEXPR14 size_type rfind(charT ch, size_type nOffSet = npos) const NOEXCEPT;

			// Find first of operations
			CONSTEXPR14 size_type find_first_of(my_type str, size_type nOffSet = 0) const NOEXCEPT;
			CONSTEXPR14 size_type find_first_of(const_pointer pStr, size_type nOffSet = 0) const NOEXCEPT;
			CONSTEXPR14 size_type find_first_of(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT;
			CONSTEXPR14 size_type find_first_of(charT ch, size_type nOffSet = 0) const NOEXCEPT;

			// Find last of operations
			CONSTEXPR14 size_type find_last_of(my_type str, size_type nOffSet = npos) const NOEXCEPT;
			CONSTEXPR14 size_type find_last_of(const_pointer pStr, size_type nOffSet = npos) const NOEXCEPT;
			CONSTEXPR14 size_type find_last_of(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT;
			CONSTEXPR14 size_type find_last_of(charT ch, size_type nOffSet = npos) const NOEXCEPT;

			// Find first not of operations
			CONSTEXPR14 size_type find_first_not_of(my_type str, size_type nOffSet = 0) const NOEXCEPT;
			CONSTEXPR14 size_type find_first_not_of(const_pointer pStr, size_type nOffSet = 0) const NOEXCEPT;
			CONSTEXPR14 size_type find_first_not_of(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT;
			CONSTEXPR14 size_type find_first_not_of(charT ch, size_type nOffSet = 0) const NOEXCEPT;

			// Find last not of operations
			CONSTEXPR14 size_type find_last_not_of(my_type str, size_type nOffSet = npos) const NOEXCEPT;
			CONSTEXPR14 size_type find_last_not_of(const_pointer pStr, size_type nOffSet = npos) const NOEXCEPT;
			CONSTEXPR14 size_type find_last_not_of(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT;
			CONSTEXPR14 size_type find_last_not_of(charT ch, size_type nOffSet = npos) const NOEXCEPT;

			// Searches
			CONSTEXPR bool starts_with(my_type str) const NOEXCEPT;
			CONSTEXPR bool starts_with(charT ch) const NOEXCEPT;
			CONSTEXPR bool starts_with_no_case(my_type str) const NOEXCEPT;

			CONSTEXPR bool ends_with(my_type str) const NOEXCEPT;
			CONSTEXPR bool ends_with(charT ch) const NOEXCEPT;
			CONSTEXPR bool ends_with_no_case(my_type str) const NOEXCEPT;

			// Capacity
			CONSTEXPR bool empty(void) const NOEXCEPT;
			CONSTEXPR bool not_empty(void) const NOEXCEPT;
			CONSTEXPR size_type length(void) const NOEXCEPT;
			CONSTEXPR size_type size(void) const NOEXCEPT;
			CONSTEXPR size_type max_size(void) const NOEXCEPT;

			CONSTEXPR14 void swap(my_type& rhs) NOEXCEPT;

			CONSTEXPR14 void remove_prefix(size_type nCount);
			CONSTEXPR14 void remove_suffix(size_type nCount);

			size_type copy(pointer pStr, size_type nCount, size_type nOffSet = 0) const;

		private:
			const_pointer _first(void) const NOEXCEPT;
			const_pointer _last(void) const NOEXCEPT;

			template < typename r_iter >
			size_type _reverse_distance(r_iter first, r_iter last) const NOEXCEPT;

			template < typename Iterator >
			Iterator _find_not_of(Iterator first, Iterator last, my_type str) const NOEXCEPT;

		private:
			size_type m_nSize;
			const_pointer m_pData;
		};


		template < typename charT, typename traits >
		CONSTEXPR inline StringViewBase< charT, traits >::StringViewBase(void) NOEXCEPT
			: m_nSize(0)
			, m_pData(nullptr)
		{}
		template < typename charT, typename traits >
		CONSTEXPR inline StringViewBase< charT, traits >::StringViewBase(const my_type& rhs) NOEXCEPT
			: m_nSize(rhs.m_nSize)
			, m_pData(rhs.m_pData)
		{}
		template < typename charT, typename traits >
		CONSTEXPR inline StringViewBase< charT, traits >::StringViewBase(const_pointer pStr, size_type nLen)
			: m_nSize(nLen)
			, m_pData(pStr)
		{}
		template < typename charT, typename traits >
		CONSTEXPR inline StringViewBase< charT, traits >::StringViewBase(const_pointer pStr)
			: m_nSize(traits::length(pStr))
			, m_pData(pStr)
		{}


		template < typename charT, typename traits >
		inline typename StringViewBase< charT, traits >::my_type& StringViewBase< charT, traits >::operator=(const my_type& rhs) NOEXCEPT
		{
			m_nSize = rhs.m_nSize;
			m_pData = rhs.m_pData;
			return (*this);
		}

		// Iterator Support
		template < typename charT, typename traits >
		CONSTEXPR inline typename StringViewBase< charT, traits >::const_iterator StringViewBase< charT, traits >::begin(void) const NOEXCEPT
		{
			return _first();
		}
		template < typename charT, typename traits >
		CONSTEXPR inline typename StringViewBase< charT, traits >::const_iterator StringViewBase< charT, traits >::cbegin(void) const NOEXCEPT
		{
			return _first();
		}
		template < typename charT, typename traits >
		CONSTEXPR inline typename StringViewBase< charT, traits >::const_iterator StringViewBase< charT, traits >::end(void) const NOEXCEPT
		{
			return _last();
		}
		template < typename charT, typename traits >
		CONSTEXPR inline typename StringViewBase< charT, traits >::const_iterator StringViewBase< charT, traits >::cend(void) const NOEXCEPT
		{
			return _last();
		}

		template < typename charT, typename traits >
		CONSTEXPR inline typename StringViewBase< charT, traits >::const_reverse_iterator StringViewBase< charT, traits >::rbegin(void) const NOEXCEPT
		{
			return const_reverse_iterator(const_iterator(end()));
		}
		template < typename charT, typename traits >
		CONSTEXPR inline typename StringViewBase< charT, traits >::const_reverse_iterator StringViewBase< charT, traits >::crbegin(void) const NOEXCEPT
		{
			return const_reverse_iterator(const_iterator(end()));
		}
		template < typename charT, typename traits >
		CONSTEXPR inline typename StringViewBase< charT, traits >::const_reverse_iterator StringViewBase< charT, traits >::rend(void) const NOEXCEPT
		{
			return const_reverse_iterator(const_iterator(begin()));
		}
		template < typename charT, typename traits >
		CONSTEXPR inline typename StringViewBase< charT, traits >::const_reverse_iterator StringViewBase< charT, traits >::crend(void) const NOEXCEPT
		{
			return const_reverse_iterator(const_iterator(begin()));
		}

		// Access
		template < typename charT, typename traits >
		CONSTEXPR inline typename StringViewBase< charT, traits >::const_reference StringViewBase< charT, traits >::operator[](size_type nOffSet) const NOEXCEPT
		{
			return m_pData[nOffSet];
		}
		template < typename charT, typename traits >
		CONSTEXPR inline typename StringViewBase< charT, traits >::const_reference StringViewBase< charT, traits >::at(size_type nOffSet) const
		{
			PRINT_COND_ASSERT((m_nSize > nOffSet), "Core: Invalid string position");
			return (c_str()[nOffSet]);
		}

		template < typename charT, typename traits >
		CONSTEXPR inline typename StringViewBase< charT, traits >::const_reference StringViewBase< charT, traits >::front(void) const
		{
			return m_pData[0];
		}
		template < typename charT, typename traits >
		CONSTEXPR inline typename StringViewBase< charT, traits >::const_reference StringViewBase< charT, traits >::back(void) const
		{
			return m_pData[m_nSize - 1];
		}
		template < typename charT, typename traits >
		CONSTEXPR inline typename StringViewBase< charT, traits >::const_pointer StringViewBase< charT, traits >::c_str(void) const NOEXCEPT
		{
			return m_pData;
		}
		template < typename charT, typename traits >
		CONSTEXPR inline typename StringViewBase< charT, traits >::const_pointer StringViewBase< charT, traits >::data(void) const NOEXCEPT
		{
			return m_pData;
		}

		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::my_type StringViewBase< charT, traits >::substr(size_type nOffSet, size_type nCount) const
		{
			if (nOffSet > size())
			{
				PRINT_ASSERT << "Core: string_view::substr";
			}

			nCount = FXD::STD::Min(nCount, size() - nOffSet);
			return my_type(m_pData + nOffSet, nCount);
		}

		// Comparisons
		template < typename charT, typename traits >
		CONSTEXPR14 inline FXD::S32 StringViewBase< charT, traits >::compare(my_type rhs) const NOEXCEPT
		{
			FXD::S32 nRet = traits::compare(m_pData, rhs.c_str(), FXD::STD::Min(size(), rhs.size()));
			return (nRet != 0) ? nRet : (size() == rhs.size()) ? 0 : (size() < rhs.size()) ? -1 : 1;
		}
		template < typename charT, typename traits >
		CONSTEXPR inline FXD::S32 StringViewBase< charT, traits >::compare(size_type nOffSet, size_type nLength, my_type str) const NOEXCEPT
		{
			return substr(nOffSet, nLength).compare(str);
		}
		template < typename charT, typename traits >
		CONSTEXPR inline FXD::S32 StringViewBase< charT, traits >::compare(size_type nOffSet, size_type nLength, my_type str, size_type nSubpos, size_type nSublen) const
		{
			return substr(nOffSet, nLength).compare(str.substr(nSubpos, nSublen));
		}
		template < typename charT, typename traits >
		CONSTEXPR inline FXD::S32 StringViewBase< charT, traits >::compare(const_pointer pStr) const
		{
			return compare(StringViewBase(pStr));
		}
		template < typename charT, typename traits >
		CONSTEXPR inline FXD::S32 StringViewBase< charT, traits >::compare(size_type nOffSet, size_type nLength, const_pointer pStr) const
		{
			return substr(nOffSet, nLength).compare(StringViewBase(pStr));
		}
		template < typename charT, typename traits >
		CONSTEXPR inline FXD::S32 StringViewBase< charT, traits >::compare(size_type nOffSet, size_type nLength, const_pointer pPtr, size_type nCount) const
		{
			return substr(nOffSet, nLength).compare(StringViewBase(pPtr, nCount));
		}

		template < typename charT, typename traits >
		CONSTEXPR14 inline FXD::S32 StringViewBase< charT, traits >::compare_no_case(my_type rhs) const NOEXCEPT
		{
			FXD::S32 nRet = traits::compare_no_case(m_pData, rhs.c_str(), FXD::STD::Min(size(), rhs.size()));
			return (nRet != 0) ? nRet : (size() == rhs.size()) ? 0 : (size() < rhs.size()) ? -1 : 1;
		}
		template < typename charT, typename traits >
		CONSTEXPR inline FXD::S32 StringViewBase< charT, traits >::compare_no_case(size_type nOffSet, size_type nLength, my_type str) const NOEXCEPT
		{
			return substr(nOffSet, nLength).compare_no_case(str);
		}
		template < typename charT, typename traits >
		CONSTEXPR inline FXD::S32 StringViewBase< charT, traits >::compare_no_case(size_type nOffSet, size_type nLength, my_type str, size_type nSubpos, size_type nSublen) const
		{
			return substr(nOffSet, nLength).compare_no_case(str.substr(nSubpos, nSublen));
		}
		template < typename charT, typename traits >
		CONSTEXPR inline FXD::S32 StringViewBase< charT, traits >::compare_no_case(const_pointer pStr) const
		{
			return compare_no_case(StringViewBase(pStr));
		}
		template < typename charT, typename traits >
		CONSTEXPR inline FXD::S32 StringViewBase< charT, traits >::compare_no_case(size_type nOffSet, size_type nLength, const_pointer pStr) const
		{
			return substr(nOffSet, nLength).compare_no_case(StringViewBase(pStr));
		}
		template < typename charT, typename traits >
		CONSTEXPR inline FXD::S32 StringViewBase< charT, traits >::compare_no_case(size_type nOffSet, size_type nLength, const_pointer pPtr, size_type nCount) const
		{
			return substr(nOffSet, nLength).compare_no_case(StringViewBase(pPtr, nCount));
		}

		// Find operations (case sensitive)
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::find(my_type str, size_type nOffSet) const NOEXCEPT
		{
			auto* pEnd = (m_pData + m_nSize);
			if (((npos - str.size()) >= nOffSet) && (nOffSet + str.size()) <= m_nSize)
			{
				const value_type* const pTemp = FXD::STD::search(m_pData + nOffSet, pEnd, str.data(), str.data() + str.size());
				if ((pTemp != pEnd) || (str.size() == 0))
				{
					return (size_type)(pTemp - m_pData);
				}
			}
			return npos;
		}
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::find(const_pointer pStr, size_type nOffSet) const NOEXCEPT
		{
			return find(StringViewBase(pStr), nOffSet);
		}
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::find(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT
		{
			return find(StringViewBase(pStr, nCount), nOffSet);
		}
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::find(charT ch, size_type nOffSet) const NOEXCEPT
		{
			return find(StringViewBase(&ch, 1), nOffSet);
		}

		// Reverse find operations (case sensitive)
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::rfind(my_type str, size_type nOffSet) const NOEXCEPT
		{
			if (size() < str.size())
			{
				return npos;
			}
			if (nOffSet > size() - str.size())
			{
				nOffSet = (size() - str.size());
			}
			if (str.size() == 0u) // an empty string is always found
			{
				return nOffSet;
			}
			for (const_pointer pCur = m_pData + nOffSet; ; --pCur)
			{
				if (traits::compare(pCur, str.m_pData, str.size()) == 0)
				{
					return (pCur - m_pData);
				}
				if (pCur == m_pData)
				{
					return npos;
				}
			}
		}
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::rfind(const_pointer pStr, size_type nOffSet) const NOEXCEPT
		{
			return rfind(StringViewBase(pStr), nOffSet);
		}
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::rfind(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT
		{
			return rfind(StringViewBase(pStr, nCount), nOffSet);
		}
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::rfind(charT ch, size_type nOffSet) const NOEXCEPT
		{
			return rfind(StringViewBase(&ch, 1), nOffSet);
		}

		// Find first of operations
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::find_first_of(my_type str, size_type nOffSet) const NOEXCEPT
		{
			if (nOffSet >= size() || str.size() == 0)
			{
				return npos;
			}
			const_iterator itr = FXD::STD::find_first_of(cbegin() + nOffSet, cend(), str.cbegin(), str.cend(), traits::eq);
			return (itr == cend()) ? npos : FXD::STD::distance(cbegin(), itr);
		}
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::find_first_of(const_pointer pStr, size_type nOffSet) const NOEXCEPT
		{
			return find_first_of(StringViewBase(pStr), nOffSet);
		}
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::find_first_of(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT
		{
			return find_first_of(StringViewBase(pStr, nCount), nOffSet);
		}
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::find_first_of(charT ch, size_type nOffSet) const NOEXCEPT
		{
			return find_first_of(StringViewBase(&ch, 1), nOffSet);
		}

		// Find last of operations
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::find_last_of(my_type str, size_type nOffSet) const NOEXCEPT
		{
			if (str.size() == 0u)
			{
				return npos;
			}
			if (nOffSet >= size())
			{
				nOffSet = 0;
			}
			else
			{
				nOffSet = size() - (nOffSet + 1);
			}
			const_reverse_iterator itr = FXD::STD::find_first_of(crbegin() + nOffSet, crend(), str.cbegin(), str.cend(), traits::eq);
			return (itr == crend()) ? npos : _reverse_distance(crbegin(), itr);
		}
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::find_last_of(const_pointer pStr, size_type nOffSet) const NOEXCEPT
		{
			return find_last_of(StringViewBase(pStr), nOffSet);
		}
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::find_last_of(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT
		{
			return find_last_of(StringViewBase(pStr, nCount), nOffSet);
		}
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::find_last_of(charT ch, size_type nOffSet) const NOEXCEPT
		{
			return find_last_of(StringViewBase(&ch, 1), nOffSet);
		}

		// Find first not of operations
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::find_first_not_of(my_type str, size_type nOffSet) const NOEXCEPT
		{
			if (nOffSet >= size())
			{
				return npos;
			}
			if (str.size() == 0)
			{
				return nOffSet;
			}
			const_iterator itr = _find_not_of(cbegin() + nOffSet, cend(), str);
			return (itr == cend()) ? npos : FXD::STD::distance(cbegin(), itr);
		}
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::find_first_not_of(const_pointer pStr, size_type nOffSet) const NOEXCEPT
		{
			return find_first_not_of(StringViewBase(pStr), nOffSet);
		}
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::find_first_not_of(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT
		{
			return find_first_not_of(StringViewBase(pStr, nCount), nOffSet);
		}
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::find_first_not_of(charT ch, size_type nOffSet) const NOEXCEPT
		{
			return find_first_not_of(StringViewBase(&ch, 1), nOffSet);
		}

		// Find last not of operations
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::find_last_not_of(my_type str, size_type nOffSet) const NOEXCEPT
		{
			if (nOffSet >= size())
			{
				nOffSet = (size() - 1);
			}
			if (str.size() == 0u)
			{
				return nOffSet;
			}
			nOffSet = (size() - (nOffSet + 1));
			const_reverse_iterator itr = _find_not_of(crbegin() + nOffSet, crend(), str);
			return (itr == crend()) ? npos : _reverse_distance(crbegin(), itr);
		}
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::find_last_not_of(const_pointer pStr, size_type nOffSet) const NOEXCEPT
		{
			return find_last_not_of(StringViewBase(pStr), nOffSet);
		}
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::find_last_not_of(const_pointer pStr, size_type nOffSet, size_type nCount) const NOEXCEPT
		{
			return find_last_not_of(StringViewBase(pStr, nCount), nOffSet);
		}
		template < typename charT, typename traits >
		CONSTEXPR14 inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::find_last_not_of(charT ch, size_type nOffSet) const NOEXCEPT
		{
			return find_last_not_of(StringViewBase(&ch, 1), nOffSet);
		}

		// Searches
		template < typename charT, typename traits >
		CONSTEXPR inline bool StringViewBase< charT, traits >::starts_with(my_type str) const NOEXCEPT
		{
			return (size() >= str.size()) && (traits::compare(m_pData, str.m_pData, str.size()) == 0);
		}
		template < typename charT, typename traits >
		CONSTEXPR inline bool StringViewBase< charT, traits >::starts_with(charT ch) const NOEXCEPT
		{
			return (not_empty()) && traits::equals(ch, front());
		}
		template < typename charT, typename traits >
		CONSTEXPR inline bool StringViewBase< charT, traits >::starts_with_no_case(my_type str) const NOEXCEPT
		{
			return (size() >= str.size()) && (traits::compare_no_case(m_pData, str.m_pData, str.size()) == 0);
		}

		template < typename charT, typename traits >
		CONSTEXPR inline bool StringViewBase< charT, traits >::ends_with(my_type str) const NOEXCEPT
		{
			return (size() >= str.size()) && (traits::compare(m_pData + size() - str.size(), str.m_pData, str.size()) == 0);
		}
		template < typename charT, typename traits >
		CONSTEXPR inline bool StringViewBase< charT, traits >::ends_with(charT ch) const NOEXCEPT
		{
			return (not_empty()) && traits::equals(ch, back());
		}
		template < typename charT, typename traits >
		CONSTEXPR inline bool StringViewBase< charT, traits >::ends_with_no_case(my_type str) const NOEXCEPT
		{
			return (size() >= str.size()) && (traits::compare_no_case(m_pData + size() - str.size(), str.m_pData, str.size()) == 0);
		}


		template < typename charT, typename traits >
		CONSTEXPR inline bool StringViewBase< charT, traits >::empty(void) const NOEXCEPT
		{
			return (m_nSize == 0);
		}
		template < typename charT, typename traits >
		CONSTEXPR inline bool StringViewBase< charT, traits >::not_empty(void) const NOEXCEPT
		{
			return (m_nSize != 0);
		}
		template < typename charT, typename traits >
		CONSTEXPR inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::length(void) const NOEXCEPT
		{
			return m_nSize;
		}
		template < typename charT, typename traits >
		CONSTEXPR inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::size(void) const NOEXCEPT
		{
			return m_nSize;
		}
		template < typename charT, typename traits >
		CONSTEXPR inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::max_size(void) const NOEXCEPT
		{
			return FXD::Math::NumericLimits< size_type >::max();
		}

		template < typename charT, typename traits >
		CONSTEXPR14 inline void StringViewBase< charT, traits >::swap(my_type& rhs) NOEXCEPT
		{
			FXD::STD::swap(m_pData, rhs.m_pData);
			FXD::STD::swap(m_nSize, rhs.m_nSize);
		}

		template < typename charT, typename traits >
		CONSTEXPR14 inline void StringViewBase< charT, traits >::remove_prefix(size_type nCount)
		{
			PRINT_COND_ASSERT((m_nSize >= nCount), "Core: Invalid string position");
			m_pData += nCount;
			m_nSize -= nCount;
		}
		template < typename charT, typename traits >
		CONSTEXPR14 inline void StringViewBase< charT, traits >::remove_suffix(size_type nCount)
		{
			m_nSize -= nCount;
		}

		template < typename charT, typename traits >
		inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::copy(pointer pStr, size_type nCount, size_type nOffSet) const
		{
			if (nOffSet > size())
			{
				PRINT_ASSERT << "Core: string_view::copy";
			}

			size_type nRLen = FXD::STD::Min(nCount, (size() - nOffSet));
			traits_type::copy(pStr, data() + nOffSet, nRLen);
			return nRLen;
		}

		template < typename charT, typename traits >
		inline typename StringViewBase< charT, traits >::const_pointer StringViewBase< charT, traits >::_first(void) const NOEXCEPT
		{
			return &data()[0];
		}
		template < typename charT, typename traits >
		inline typename StringViewBase< charT, traits >::const_pointer StringViewBase< charT, traits >::_last(void) const NOEXCEPT
		{
			return &data()[size()];
		}

		template < typename charT, typename traits >
		template < typename r_iter >
		inline typename StringViewBase< charT, traits >::size_type StringViewBase< charT, traits >::_reverse_distance(r_iter first, r_iter last) const NOEXCEPT
		{
			return (size() - 1 - FXD::STD::distance(first, last));
		}

		template < typename charT, typename traits >
		template < typename Iterator >
		inline Iterator StringViewBase< charT, traits >::_find_not_of(Iterator first, Iterator last, my_type str) const NOEXCEPT
		{
			for (; first != last; ++first)
			{
				if (traits::find(str.m_pData, str.size(), (*first)) == 0)
				{
					return first;
				}
			}
			return last;
		}


		// Non-member comparison functions
		// Equality
		template < typename charT, typename traits >
		inline bool operator==(StringViewBase< charT, traits > lhs, StringViewBase< charT, traits > rhs) NOEXCEPT
		{
			if (lhs.size() != rhs.size())
			{
				return false;
			}
			return (lhs.compare(rhs) == 0);
		}
		template < typename charT, typename traits >
		inline bool operator==(StringViewBase< charT, traits > lhs, const charT* pRhs) NOEXCEPT
		{
			return (lhs == StringViewBase< charT, traits >(pRhs));
		}
		template < typename charT, typename traits >
		inline bool operator==(const charT* pLhs, StringViewBase< charT, traits > rhs) NOEXCEPT
		{
			return (StringViewBase< charT, traits >(pLhs) == rhs);
		}
		// Inequality
		template < typename charT, typename traits >
		inline bool operator!=(StringViewBase< charT, traits > lhs, StringViewBase< charT, traits > rhs) NOEXCEPT
		{
			if (lhs.size() != rhs.size())
			{
				return true;
			}
			return (lhs.compare(rhs) != 0);
		}
		template < typename charT, typename traits >
		inline bool operator!=(StringViewBase< charT, traits > lhs, const charT* pRhs) NOEXCEPT
		{
			return (lhs != StringViewBase< charT, traits >(pRhs));
		}
		template < typename charT, typename traits >
		inline bool operator!=(const charT* pLhs, StringViewBase< charT, traits > rhs) NOEXCEPT
		{
			return(StringViewBase< charT, traits >(pLhs) != rhs);
		}
		// Less than
		template < typename charT, typename traits >
		inline bool operator<(StringViewBase< charT, traits > lhs, StringViewBase< charT, traits > rhs) NOEXCEPT
		{
			return (lhs.compare(rhs) < 0);
		}
		template < typename charT, typename traits >
		inline bool operator<(StringViewBase< charT, traits > lhs, const charT* pRhs) NOEXCEPT
		{
			return (lhs < StringViewBase< charT, traits >(pRhs));
		}
		template < typename charT, typename traits >
		inline bool operator<(const charT* pLhs, StringViewBase< charT, traits > pRhs) NOEXCEPT
		{
			return (StringViewBase< charT, traits >(pLhs) < pRhs);
		}
		// Greater than
		template < typename charT, typename traits >
		inline bool operator>(StringViewBase< charT, traits > lhs, StringViewBase< charT, traits > rhs) NOEXCEPT
		{
			return (lhs.compare(rhs) > 0);
		}
		template < typename charT, typename traits >
		inline bool operator>(StringViewBase< charT, traits > lhs, const charT* pRhs) NOEXCEPT
		{
			return (lhs > StringViewBase< charT, traits >(pRhs));
		}
		template < typename charT, typename traits >
		inline bool operator>(const charT* pLhs, StringViewBase< charT, traits > rhs) NOEXCEPT
		{
			return (StringViewBase< charT, traits >(pLhs) > rhs);
		}
		// Less than or equal to
		template < typename charT, typename traits >
		inline bool operator<=(StringViewBase< charT, traits > lhs, StringViewBase< charT, traits > rhs) NOEXCEPT
		{
			return (lhs.compare(rhs) <= 0);
		}
		template < typename charT, typename traits >
		inline bool operator<=(StringViewBase< charT, traits > lhs, const charT* pRhs) NOEXCEPT
		{
			return (lhs <= StringViewBase< charT, traits >(pRhs));
		}
		template < typename charT, typename traits >
		inline bool operator<=(const charT* pLhs, StringViewBase< charT, traits > rhs) NOEXCEPT
		{
			return (StringViewBase< charT, traits >(pLhs) <= rhs);
		}
		// Greater than or equal to
		template < typename charT, typename traits >
		inline bool operator>=(StringViewBase< charT, traits > lhs, StringViewBase< charT, traits > rhs) NOEXCEPT
		{
			return (lhs.compare(rhs) >= 0);
		}
		template < typename charT, typename traits >
		inline bool operator>=(StringViewBase< charT, traits > lhs, const charT* pRhs) NOEXCEPT
		{
			return (lhs >= StringViewBase< charT, traits >(pRhs));
		}
		template < typename charT, typename traits >
		inline bool operator>=(const charT* pLhs, StringViewBase< charT, traits > rhs) NOEXCEPT
		{
			return (StringViewBase< charT, traits >(pLhs) >= rhs);
		}

		// Inserters and extractors
		template < typename charT, typename traits = Core::CharTraits< charT > >
		inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, Core::StringViewBase< charT, traits > const& rhs)
		{
			return ostr << rhs.c_str();
		}

		namespace Literals
		{
			CONSTEXPR inline StringView8 operator""_SV8(const FXD::UTF8* pStr, size_t nLen) NOEXCEPT
			{
				return StringView8(pStr, (FXD::U32)nLen);
			}
			CONSTEXPR inline StringView16 operator""_SV16(const FXD::UTF16* pStr, size_t nLen) NOEXCEPT
			{
				return StringView16(pStr, (FXD::U32)nLen);
			}
		} //namespace Literals

	} //namespace Core
} //namespace FXD

using FXD::Core::Literals::operator"" _SV8;
using FXD::Core::Literals::operator"" _SV16;

#endif //FXDENGINE_CORE_STRINGVIEW_H