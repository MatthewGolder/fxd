// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_NONCOPYABLE_H
#define FXDENGINE_CORE_NONCOPYABLE_H

#include "FXDEngine/Core/Types.h"

namespace FXD
{
	namespace Core
	{
		// ------
		// NonCopyable
		// -
		// ------
		class NonCopyable
		{
		protected:
			NonCopyable(void)
			{}
			virtual ~NonCopyable(void)
			{}

		private:
			NO_COPY(NonCopyable);
			NO_ASSIGNMENT(NonCopyable);
		};
	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_NONCOPYABLE_H