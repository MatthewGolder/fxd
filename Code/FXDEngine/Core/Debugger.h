// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_DEBUGGER_H
#define FXDENGINE_CORE_DEBUGGER_H

#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/Core/NonCopyable.h"
#include "FXDEngine/Core/SerializeOutString.h"

#if defined(FXD_PRINT_DEBUG)
#	define ENABLE_DEBUGGER
#endif
#if defined(ENABLE_DEBUGGER)
#endif

namespace FXD
{
	namespace Core
	{
#if defined(ENABLE_DEBUGGER)
		enum class E_DBGLevel : FXD::U8
		{
			None = 0,
			Info,
			Warn,
			Error,
			Assert
		};

		// ------
		// DBGStreamOut
		// -
		// ------
		class DBGStreamOut FINAL : public Core::IStreamOut, public Core::ISerializeOutString< FXD::UTF8 >, public Core::NonCopyable
		{
		public:
			struct Internal;

		public:
			DBGStreamOut(void);
			~DBGStreamOut(void);

			Core::DBGStreamOut& operator*=(const Core::IStreamOut& rhs);
			Core::DBGStreamOut& operator%=(const Core::IStreamOut& rhs);

			Core::DBGStreamOut& acquire_lock(void);
			Core::DBGStreamOut& release_lock(void);

			void apply(Core::E_DBGLevel eLevel);

		private:
			FXD::U64 write_size(const void* pData, const FXD::U64 nSize);
			void _do_print(const FXD::UTF8* pStr) const;
			void _break_application(void) const;

		private:
			Core::E_DBGLevel m_eLevel;
			Core::PimplBuffer< Internal, 128 > m_impl;
		};

		// IStreamOut;
		inline Core::IStreamOut& operator<<(Core::DBGStreamOut& streamOut, Core::E_DBGLevel const& rhs)
		{
			streamOut.apply(rhs);
			return streamOut;
		}
		inline Core::IStreamOut& operator<<(Core::IStreamOut& streamOut, Core::E_DBGLevel const& /*rhs*/)
		{
			return streamOut;
		}

#else
		// ------
		// DebuggerDummy
		// -
		// ------
		class DebuggerDummy
		{
		public:
			DebuggerDummy(void)
			{}
			~DebuggerDummy(void)
			{}

			template < typename T >
			inline const DebuggerDummy& operator<<(const T& /*type*/) const
			{
				return (*this);
			}
		};
#endif
		namespace Internal
		{
#if defined(ENABLE_DEBUGGER)
			extern Core::DBGStreamOut& get_instance(void);
#else
			extern Core::DebuggerDummy& get_instance(void);
#endif
		} //namespace Internal

#if defined(ENABLE_DEBUGGER)
#		define PRINT_LINE FXD::Core::Internal::get_instance() *= FXD::Core::Internal::get_instance().acquire_lock() << FXD::Core::E_DBGLevel::None
#		define PRINT_INFO FXD::Core::Internal::get_instance() *= FXD::Core::Internal::get_instance().acquire_lock() << FXD::Core::E_DBGLevel::Info
#		define PRINT_WARN FXD::Core::Internal::get_instance() *= FXD::Core::Internal::get_instance().acquire_lock() << FXD::Core::E_DBGLevel::Warn
#		define PRINT_ERROR FXD::Core::Internal::get_instance() *= FXD::Core::Internal::get_instance().acquire_lock() << FXD::Core::E_DBGLevel::Error
#		define PRINT_ASSERT FXD::Core::Internal::get_instance() %= FXD::Core::Internal::get_instance().acquire_lock() << FXD::Core::E_DBGLevel::Assert

#		define PRINT_COND_INFO(c, msg) if(!(c)) PRINT_INFO << "COND: " << #c << ": " << msg;
#		define PRINT_COND_WARN(c, msg) if(!(c)) PRINT_WARN << "COND: " << #c << ": " msg;
#		define PRINT_COND_ERROR(c, msg) if(!(c)) PRINT_ERROR << "COND: " << #c << ": " << msg;
#		define PRINT_COND_ASSERT(c, msg) if(!(c)) PRINT_ASSERT << "COND: " << #c << ": " << msg;
#else
#		define PRINT_LINE FXD::Core::Internal::get_instance()
#		define PRINT_INFO FXD::Core::Internal::get_instance()
#		define PRINT_WARN FXD::Core::Internal::get_instance()
#		define PRINT_ERROR FXD::Core::Internal::get_instance()
#		define PRINT_ASSERT FXD::Core::Internal::get_instance()

#		define PRINT_COND_INFO(c, msg){}
#		define PRINT_COND_WARN(c, msg){}
#		define PRINT_COND_ERROR(c, msg){}
#		define PRINT_COND_ASSERT(c, msg){}
#endif

#if defined(ENABLE_DEBUGGER)
#		define FXD_ERROR(s){static bool b = false; if(!b){b = true; PRINT_ASSERT << s;}}
#		define TODO_WARN(s){static bool b = false; if(!b){b = true; PRINT_WARN << s;}}
#else
#		define FXD_ERROR(s){}
#		define TODO_WARN(s){}
#endif
	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_DEBUGGER_H