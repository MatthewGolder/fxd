// Creator - MatthewGolder
#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Core/Internal/Debugger.h"

using namespace FXD;
using namespace Core;

#if defined(ENABLE_DEBUGGER)
// ------
// DBGStreamOut
// -
// ------
DBGStreamOut::DBGStreamOut(void)
	: IStreamOut(this, {})
	, m_eLevel(E_DBGLevel::None)
{
}

DBGStreamOut::~DBGStreamOut(void)
{
}

Core::DBGStreamOut& DBGStreamOut::operator*=(const Core::IStreamOut& /*rhs*/)
{
	_do_print(((*m_impl).m_str.append("\n")).c_str());
	m_impl->m_str.clear();

	(*this) << E_DBGLevel::None;
	return release_lock();
}

Core::DBGStreamOut& DBGStreamOut::operator%=(const Core::IStreamOut& /*rhs*/)
{
	_do_print(((*m_impl).m_str.append("\n")).c_str());
	m_impl->m_str.clear();

	(*this) << E_DBGLevel::None;
	_break_application();
	return release_lock();
}

Core::DBGStreamOut& DBGStreamOut::acquire_lock(void)
{
	m_impl->m_cs.acquire_lock();
	return (*this);
}

Core::DBGStreamOut& DBGStreamOut::release_lock(void)
{
	m_impl->m_cs.release_lock();
	return (*this);
}

DBGStreamOut& Core::Internal::get_instance(void)
{
	static DBGStreamOut dbg;
	return dbg;
}
#else
DebuggerDummy& Core::Internal::get_instance(void)
{
	static DebuggerDummy dbg;
	return dbg;
}
#endif