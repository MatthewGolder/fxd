// Creator - MatthewGolder
#include "FXDEngine/Core/String.h"
#include "FXDEngine/Core/Unicode.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

using namespace FXD;
using namespace Core;

// ------
// String8
// -
// ------
template <>
const FXD::HashValue String8::to_hash(void) const
{
	return Core::CharTraits< FXD::UTF8 >::to_hash(_myPtr(), m_nSize);
}

template <>
bool String8::to_bool(void) const
{
	bool bRetVal = false;
	if (Core::CharTraits< FXD::UTF8 >::compare_no_case(c_str(), UTF_8("true")) == 0)
	{
		bRetVal = true;
	}
	else if (Core::CharTraits< FXD::UTF8 >::compare_no_case(c_str(), UTF_8("yes")) == 0)
	{
		bRetVal = true;
	}
	else if (Core::CharTraits< FXD::UTF8 >::compare_no_case(c_str(), UTF_8("on")) == 0)
	{
		bRetVal = true;
	}
	else if (Core::CharTraits< FXD::UTF8 >::compare_no_case(c_str(), UTF_8("1")) == 0)
	{
		bRetVal = true;
	}
	return bRetVal;
}

template <>
FXD::F32 String8::to_F32(void) const
{
	return (FXD::F32)::atof(c_str());
}

template <>
FXD::S16 String8::to_S16(void) const
{
	return (FXD::S16)::atoi(c_str());
}

template <>
FXD::S32 String8::to_S32(void) const
{
	return (FXD::S32)::atoi(c_str());
}

template <>
void String8::to_string(String16& strOut) const
{
	const FXD::UTF8* pStr = _myPtr();
	FXD::UTF16* pUTF16 = Core::UnicodeFuncs::convertUTF8toUTF16(pStr);
	strOut = pUTF16;
	FXD_DELETE_ARRAY(pUTF16);
}

template <>
String8& String8::find_replace(const FXD::UTF8 oldChar, const String8& str)
{
	Core::StringBuilder8 strStream;
	FXD::U32 nID1 = 0;
	while (nID1 != String8::npos)
	{
		FXD::U32 nID2 = find(oldChar, nID1);
		if (nID2 == String8::npos)
		{
			strStream << substr(nID1);
		}
		else
		{
			if (nID2 > nID1)
			{
				strStream << substr(nID1, nID2 - nID1) << str;
			}
			else
			{
				strStream << str;
			}
			nID2++;
		}
		nID1 = nID2;
	}
	assign(strStream.str());
	return (*this);
}

template <>
void String8::ltrim(void)
{
	const value_type kWhiteSpaceArray[] = { ' ', '\t', 0 };
	erase(0, find_first_not_of(kWhiteSpaceArray));
}

template <>
void String8::rtrim(void)
{
	const value_type kWhiteSpaceArray[] = { ' ', '\t', 0 };
	erase(find_last_not_of(kWhiteSpaceArray) + 1);
}