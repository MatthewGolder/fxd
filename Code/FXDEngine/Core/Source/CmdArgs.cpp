// Creator - MatthewGolder
#include "FXDEngine/Core/CmdArgs.h"

using namespace FXD;
using namespace Core;

// ------
// CmdArgs
// -
// ------
CmdArgs::CmdArgs(void)
{
}

CmdArgs::~CmdArgs(void)
{
}

void CmdArgs::set_args(FXD::S32 nArgc, FXD::UTF8* nArgv[])
{
	m_argMap.clear();
	for (FXD::S32 c1 = 0; c1 < nArgc; c1++)
	{
		Core::String8 str = (nArgv[c1]);
		FXD::U32 nPos = str.find('=');
		if (nPos != Core::String8::npos)
		{
			Core::String8 strKey = Core::String8(str.c_str(), nPos);
			Core::String8 strVal = str.substr(nPos + 1);
			m_argMap[strKey] = strVal;
		}
		else
		{
			m_argMap[str] = UTF_8("");
		}
	}
}

bool CmdArgs::has_arg(const Core::StringView8 strArg) const
{
	fxd_for(const auto& arg, m_argMap)
	{
		if (arg.first.compare_no_case(strArg.c_str()) == 0)
		{
			return true;
		}
	}
	return false;
}

Core::String8 CmdArgs::get_value(const Core::StringView8 strArg) const
{
	fxd_for(const auto& arg, m_argMap)
	{
		if (arg.first.compare_no_case(strArg.c_str()) == 0)
		{
			return arg.second;
		}
	}
	return Core::String8();
}