// Creator - MatthewGolder
#include "FXDEngine/Core/SerializeOutString.h"
#include "FXDEngine/Core/StreamOut.h"
#include "FXDEngine/Core/Unicode.h"
#include "FXDEngine/Memory/Memory.h"

using namespace FXD;
using namespace Core;

// ------
// ISerializeOutString< FXD::UTF8 >
// -
// ------
template <>
void ISerializeOutString< FXD::UTF8 >::_pass(const FXD::UTF8* pStr, IStreamOut* pStream)
{
	_write_size(pStr, pStream);
}

template <>
void ISerializeOutString< FXD::UTF8 >::_pass(const FXD::UTF16* pStr, IStreamOut* pStream)
{
	FXD::UTF8* pUTF8 = Core::UnicodeFuncs::convertUTF16toUTF8(pStr);
	_write_size(pUTF8, pStream);
	FXD_DELETE_ARRAY(pUTF8);
}

template <>
void ISerializeOutString< FXD::UTF8 >::_pass(bool b, IStreamOut* pStream)
{
	_write_size(b ? UTF_8("true") : UTF_8("false"), pStream);
}

// ------
// ISerializeOutString< FXD::UTF16 >
// -
// ------
template <>
void ISerializeOutString< FXD::UTF16 >::_pass(const FXD::UTF8* pStr, IStreamOut* pStream)
{
	FXD::UTF16* pUTF16 = Core::UnicodeFuncs::convertUTF8toUTF16(pStr);
	_write_size(pUTF16, pStream);
	FXD_DELETE_ARRAY(pUTF16);
}

template <>
void ISerializeOutString< FXD::UTF16 >::_pass(const FXD::UTF16* pStr, IStreamOut* pStream)
{
	_write_size(pStr, pStream);
}

template <>
void ISerializeOutString< FXD::UTF16 >::_pass(bool b, IStreamOut* pStream)
{
	_write_size(b ? UTF_16("true") : UTF_16("false"), pStream);
}