// Creator - MatthewGolder
#include "FXDEngine/Core/String.h"
#include "FXDEngine/Core/Unicode.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

using namespace FXD;
using namespace Core;

// ------
// String16
// -
// ------
template <>
const FXD::HashValue String16::to_hash(void) const
{
	return Core::CharTraits< FXD::UTF16 >::to_hash(_myPtr(), m_nSize);
}

template <>
FXD::F32 String16::to_F32(void) const
{
	String8 str = to_string< FXD::UTF8 >();
	return str.to_F32();
}

template <>
FXD::S16 String16::to_S16(void) const
{
	return to_string< FXD::UTF8 >().to_S16();
}

template <>
FXD::S32 String16::to_S32(void) const
{
	return to_string< FXD::UTF8 >().to_S32();
}

template <>
bool String16::to_bool(void) const
{
	bool bRetVal = false;
	if (Core::CharTraits< FXD::UTF16 >::compare_no_case(c_str(), UTF_16("true")) == 0)
	{
		bRetVal = true;
	}
	else if (Core::CharTraits< FXD::UTF16 >::compare_no_case(c_str(), UTF_16("yes")) == 0)
	{
		bRetVal = true;
	}
	else if (Core::CharTraits< FXD::UTF16 >::compare_no_case(c_str(), UTF_16("on")) == 0)
	{
		bRetVal = true;
	}
	else if (Core::CharTraits< FXD::UTF16 >::compare_no_case(c_str(), UTF_16("1")) == 0)
	{
		bRetVal = true;
	}
	return bRetVal;
}

template <>
void String16::to_string(String8& strOut) const
{
	const FXD::UTF16* pStr = _myPtr();
	FXD::UTF8* pUTF8 = Core::UnicodeFuncs::convertUTF16toUTF8(pStr);
	strOut = pUTF8;
	FXD_DELETE_ARRAY(pUTF8);
}

/*
template <>
bool String16::readTextLine(const Core::StreamBinary& stream)
{
	Core::StringBuffer16 strStream;
	FXD::UTF16 c;
	bool bEndOfFile = true;
	while(stream->read_size(&c, sizeof(FXD::UTF16)))
	{
		bEndOfFile = false;
		if(c == L'\r')
		{
			if(strStream.length() == 0)
		{
				continue;
			}
			break;
		}
		if((c != 0xfeff) && (c != 0xfffe))
		{
			if((strStream.length() != 0)|| (c != L'\n'))
			{
				strStream << c;
			}
		}
	}
	assign(strStream.get_string());
	return (!bEndOfFile);
}
*/

template <>
String16& String16::find_replace(const FXD::UTF16 oldChar, const String16& str)
{
	Core::StringBuilder16 strStream;
	FXD::U32 nID1 = 0;
	while (nID1 != String16::npos)
	{
		FXD::U32 nID2 = find(oldChar, nID1);
		if (nID2 == String16::npos)
		{
			strStream << substr(nID1);
		}
		else
		{
			if (nID2 > nID1)
			{
				strStream << substr(nID1, nID2 - nID1) << str;
			}
			else
			{
				strStream << str;
			}
			nID2++;
		}
		nID1 = nID2;
	}
	assign(strStream.str());
	return (*this);
}

template <>
void String16::ltrim(void)
{
	const value_type kWhiteSpaceArray[] = { ' ', '\t', 0 };
	erase(0, find_first_not_of(kWhiteSpaceArray));
}

template <>
void String16::rtrim(void)
{
	const value_type kWhiteSpaceArray[] = { ' ', '\t', 0 };
	erase(find_last_not_of(kWhiteSpaceArray) + 1);
}