// Creator - MatthewGolder
#include "FXDEngine/Core/Date.h"
#include "FXDEngine/Memory/Memory.h"

using namespace FXD;
using namespace Core;

// ------
// Date
// -
// ------
Date::Date(void)
{
	Memory::MemZero_T(m_utc);
}

Date::Date(time_t utc)
	: m_utc(utc)
{
}

Date::~Date(void)
{
}

Core::Date Date::now(void)
{
	Date date;
	date.m_utc = time(0);
	return date;
}

Core::String8 Date::get_string(void) const
{
	Core::StringBuilder8 strStream;
	strStream
		<< get_day()
		<< '/' << get_month()
		<< '/' << get_year()
		<< ' ' << get_hours()
		<< ':' << get_minutes()
		<< ':' << get_seconds();
	return strStream.str();
}

FXD::S32 Date::get_day(void) const
{
	time_t time = m_utc;
	tm* pNow = localtime(&time);
	return (pNow->tm_mday);
}

FXD::S32 Date::get_month(void) const
{
	time_t time = m_utc;
	tm* pNow = localtime(&time);
	return (pNow->tm_mon + 1);
}

FXD::S32 Date::get_year(void) const
{
	time_t time = m_utc;
	tm* pNow = localtime(&time);
	return (pNow->tm_year + 1900);
}

FXD::S32 Date::get_hours(void) const
{
	time_t time = m_utc;
	tm* pNow = localtime(&time);
	return (pNow->tm_hour);
}

FXD::S32 Date::get_minutes(void) const
{

	time_t time = m_utc;
	tm* pNow = localtime(&time);
	return (pNow->tm_min);
}

FXD::S32 Date::get_seconds(void) const
{
	time_t time = m_utc;
	tm* pNow = localtime(&time);
	return (pNow->tm_sec);
}