// Creator - MatthewGolder
#include "FXDEngine/Core/String.h"
#include "FXDEngine/Memory/MemHandle.h"

using namespace FXD;
using namespace Core;

static const FXD::UTF8 kEncodingTable[] =
{
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
	'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
	'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
	'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
	'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
	'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
	'w', 'x', 'y', 'z', '0', '1', '2', '3',
	'4', '5', '6', '7', '8', '9', '+', '/'
};
static FXD::UTF8 g_decodingTable[256];
static bool g_bDecodingTableCreated = false;
static FXD::S32 g_modTable[] = { 0, 2, 1 };


Core::String8 FXD::Core::encode_string(const Memory::MemHandle& mem)
{
	if (mem.get_size() == 0)
	{
		return Core::String8();
	}

	FXD::U8* pData = (FXD::U8*)Memory::MemHandle::LockGuard(mem).get_mem();

	FXD::U32 nOutputLength = 4 * ((mem.get_size() + 2) / 3);
	Memory::MemHandle ret = Memory::MemHandle::allocate(nOutputLength + 1);
	FXD::UTF8* pEncoded_data = (FXD::UTF8*)Memory::MemHandle::LockGuard(ret).get_mem();
	for (FXD::U32 n1 = 0, n2 = 0; n1 < mem.get_size(); /**/)
	{
		FXD::U32 nOctet_A = (n1 < mem.get_size()) ? pData[n1++] : 0;
		FXD::U32 nOctet_B = (n1 < mem.get_size()) ? pData[n1++] : 0;
		FXD::U32 nOctet_C = (n1 < mem.get_size()) ? pData[n1++] : 0;
		FXD::U32 nTriple = (nOctet_A << 0x10) + (nOctet_B << 0x08) + nOctet_C;
		pEncoded_data[n2++] = kEncodingTable[(nTriple >> (3 * 6)) & 0x3F];
		pEncoded_data[n2++] = kEncodingTable[(nTriple >> (2 * 6)) & 0x3F];
		pEncoded_data[n2++] = kEncodingTable[(nTriple >> (1 * 6)) & 0x3F];
		pEncoded_data[n2++] = kEncodingTable[(nTriple >> (0 * 6)) & 0x3F];
	}

	for (FXD::U32 n1 = 0; n1 < g_modTable[mem.get_size() % 3]; n1++)
	{
		pEncoded_data[nOutputLength - 1 - n1] = '=';
	}

	pEncoded_data[nOutputLength] = 0;
	return Core::String8(pEncoded_data);
}

Memory::MemHandle FXD::Core::decode_string(const Core::String8& str)
{
	if (str.empty())
	{
		return Memory::MemHandle();
	}

	// Create DecodingTable
	if (!g_bDecodingTableCreated)
	{
		for (FXD::UTF8 n1 = 0; n1 < 64; n1++)
		{
			g_decodingTable[(FXD::U8)kEncodingTable[n1]] = n1;
		}
		g_bDecodingTableCreated = true;
	}

	if (str.length() % 4 != 0)
	{
		return Memory::MemHandle();
	}
	FXD::U32 nOutputLength = (str.length() / 4 * 3);
	if (str[str.length() - 1] == '=')
	{
		nOutputLength--;
	}
	if (str[str.length() - 2] == '=')
	{
		nOutputLength--;
	}

	Memory::MemHandle mh = Memory::MemHandle::allocate(nOutputLength);
	FXD::U8* pDecodedData = (FXD::U8*)Memory::MemHandle::LockGuard(mh).get_mem();
	for (FXD::U32 n1 = 0, n2 = 0; n1 < str.length(); /**/)
	{
		FXD::U32 nSextet_a = (str[n1] == '=') ? 0 & n1++ : g_decodingTable[str[n1++]];
		FXD::U32 nSextet_b = (str[n1] == '=') ? 0 & n1++ : g_decodingTable[str[n1++]];
		FXD::U32 nSextet_C = (str[n1] == '=') ? 0 & n1++ : g_decodingTable[str[n1++]];
		FXD::U32 nSextet_D = (str[n1] == '=') ? 0 & n1++ : g_decodingTable[str[n1++]];

		FXD::U32 nTriple =
			(nSextet_a << (3 * 6))
			+ (nSextet_b << (2 * 6))
			+ (nSextet_C << (1 * 6))
			+ (nSextet_D << (0 * 6));

		if (n2 < nOutputLength)
			pDecodedData[n2++] = (nTriple >> (2 * 8)) & 0xFF;
		if (n2 < nOutputLength)
			pDecodedData[n2++] = (nTriple >> (1 * 8)) & 0xFF;
		if (n2 < nOutputLength)
			pDecodedData[n2++] = (nTriple >> (0 * 8)) & 0xFF;
	}
	return mh;
}