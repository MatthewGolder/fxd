// Creator - MatthewGolder
#include "FXDEngine/Core/SerializeOutBinary.h"
#include "FXDEngine/Core/StreamOut.h"
#include "FXDEngine/Core/CharTraits.h"

using namespace FXD;
using namespace Core;

// ------
// ISerializeOutBinary
// -
// ------
ISerializeOutBinary::ISerializeOutBinary(void)
{
}
ISerializeOutBinary::~ISerializeOutBinary(void)
{
}

void ISerializeOutBinary::_pass(const FXD::UTF8* pVal, IStreamOut* pStream)
{
	FXD::U32 nLen = Core::CharTraits< FXD::UTF8 >::length(pVal);
	pStream->write_size(&nLen, sizeof(FXD::U32));
	pStream->write_size(pVal, (nLen * sizeof(FXD::UTF8)));
}
void ISerializeOutBinary::_pass(const FXD::UTF16* pVal, IStreamOut* pStream)
{
	FXD::U32 nLen = Core::CharTraits< FXD::UTF16 >::length(pVal);
	pStream->write_size(&nLen, sizeof(FXD::U32));
	pStream->write_size(pVal, (nLen * sizeof(FXD::UTF16)));
}
void ISerializeOutBinary::_pass(const bool val, IStreamOut* pStream)
{
	pStream->write_size(&val, sizeof(bool));
}
void ISerializeOutBinary::_pass(const FXD::UTF8 val, IStreamOut* pStream)
{
	pStream->write_size(&val, sizeof(FXD::UTF8));
}
void ISerializeOutBinary::_pass(const FXD::S8 nVal, IStreamOut* pStream)
{
	pStream->write_size(&nVal, sizeof(FXD::S8));
}
void ISerializeOutBinary::_pass(const FXD::U8 nVal, IStreamOut* pStream)
{
	pStream->write_size(&nVal, sizeof(FXD::U8));
}
void ISerializeOutBinary::_pass(const FXD::S16 nVal, IStreamOut* pStream)
{
	pStream->write_size(&nVal, sizeof(FXD::S16));
}
void ISerializeOutBinary::_pass(const FXD::U16 nVal, IStreamOut* pStream)
{
	pStream->write_size(&nVal, sizeof(FXD::U16));
}
void ISerializeOutBinary::_pass(const FXD::S32 nVal, IStreamOut* pStream)
{
	pStream->write_size(&nVal, sizeof(FXD::S32));
}
void ISerializeOutBinary::_pass(const FXD::U32 nVal, IStreamOut* pStream)
{
	pStream->write_size(&nVal, sizeof(FXD::U32));
}
void ISerializeOutBinary::_pass(const FXD::S64 nVal, IStreamOut* pStream)
{
	pStream->write_size(&nVal, sizeof(FXD::S64));
}
void ISerializeOutBinary::_pass(const FXD::U64 nVal, IStreamOut* pStream)
{
	pStream->write_size(&nVal, sizeof(FXD::U64));
}
void ISerializeOutBinary::_pass(const FXD::F32 fVal, IStreamOut* pStream)
{
	pStream->write_size(&fVal, sizeof(FXD::F32));
}
void ISerializeOutBinary::_pass(const FXD::F64 fVal, IStreamOut* pStream)
{
	pStream->write_size(&fVal, sizeof(FXD::F64));
}