// Creator - MatthewGolder
#include "FXDEngine/Core/StreamIn.h"
#include "FXDEngine/Core/SerializeIn.h"
#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Core/Compression/StreamDecompressorLZ4.h"
#include "FXDEngine/Core/Compression/StreamDecompressorZlib.h"

using namespace FXD;
using namespace Core;

// ------
// IStreamIn
// -
// ------
IStreamIn::IStreamIn(Core::ISerializeIn* pSerializeIn, Core::CompressParams compressParams)
	: m_pSerializeIn(nullptr)
	, m_pDecompressor(nullptr)
{
	_set_serialize_in(pSerializeIn);
	_set_compressor_in(compressParams);
}

IStreamIn::~IStreamIn(void)
{
}

IStreamIn& IStreamIn::operator>>(FXD::UTF16* v)
{
	m_pSerializeIn->_pass(v, this);
	return (*this);
}
IStreamIn& IStreamIn::operator>>(FXD::UTF8* v)
{
	m_pSerializeIn->_pass(v, this);
	return (*this);
}
IStreamIn& IStreamIn::operator>>(bool& v)
{
	m_pSerializeIn->_pass(v, this);
	return (*this);
}
IStreamIn& IStreamIn::operator>>(FXD::UTF8& v)
{
	m_pSerializeIn->_pass(v, this);
	return (*this);
}
IStreamIn& IStreamIn::operator>>(FXD::S8& v)
{
	m_pSerializeIn->_pass(v, this);
	return (*this);
}
IStreamIn& IStreamIn::operator>>(FXD::U8& v)
{
	m_pSerializeIn->_pass(v, this);
	return (*this);
}
IStreamIn& IStreamIn::operator>>(FXD::S16& v)
{
	m_pSerializeIn->_pass(v, this);
	return (*this);
}
IStreamIn& IStreamIn::operator>>(FXD::U16& v)
{
	m_pSerializeIn->_pass(v, this);
	return (*this);
}
IStreamIn& IStreamIn::operator>>(FXD::S32& v)
{
	m_pSerializeIn->_pass(v, this);
	return (*this);
}
IStreamIn& IStreamIn::operator>>(FXD::U32& v)
{
	m_pSerializeIn->_pass(v, this);
	return (*this);
}
IStreamIn& IStreamIn::operator>>(FXD::S64& v)
{
	m_pSerializeIn->_pass(v, this);
	return (*this);
}
IStreamIn& IStreamIn::operator>>(FXD::U64& v)
{
	m_pSerializeIn->_pass(v, this);
	return (*this);
}
IStreamIn& IStreamIn::operator>>(FXD::F32& v)
{
	m_pSerializeIn->_pass(v, this);
	return (*this);
}
IStreamIn& IStreamIn::operator>>(FXD::F64& v)
{
	m_pSerializeIn->_pass(v, this);
	return (*this);
}

FXD::U64 IStreamIn::decompress_size(void* pData, const FXD::U64 nSize)
{
	if (m_pDecompressor != nullptr)
	{
		return m_pDecompressor->decompress_size(pData, nSize);
	}
	else
	{
		return read_size(pData, nSize);
	}
}

void IStreamIn::_set_serialize_in(Core::ISerializeIn* pSerializeIn)
{
	PRINT_COND_ASSERT((m_pSerializeIn == nullptr), "Core: m_pSerializeIn has already been constructed.");
	m_pSerializeIn = pSerializeIn;
}

void IStreamIn::_set_compressor_in(Core::CompressParams compressParams)
{
	PRINT_COND_ASSERT((m_pDecompressor == nullptr), "Core: m_pDecompressor has already been constructed.");
	
	switch (compressParams.Type)
	{
		case Core::E_CompressionType::ZLib:
		{
			m_pDecompressor = FXD_NEW(Core::IStreamDecompressorZlib)(this, compressParams.Quality);
			break;
		}
		case Core::E_CompressionType::ZLibStreamed:
		{
			m_pDecompressor = FXD_NEW(Core::IStreamDecompressorZlib)(this, compressParams.Quality);
			break;
		}
		case Core::E_CompressionType::LZ4:
		{
			m_pDecompressor = FXD_NEW(Core::IStreamDecompressorLZ4)(this, compressParams.Quality);
			break;
		}
		case Core::E_CompressionType::LZ4Streamed:
		{
			m_pDecompressor = FXD_NEW(Core::IStreamDecompressorStreamLZ4)(this, compressParams.Quality);
			break;
		}
		default:
		{
			break;
		}
	}
}