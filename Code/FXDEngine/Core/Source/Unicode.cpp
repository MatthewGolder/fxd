// Creator - MatthewGolder
#include "FXDEngine/Core/Unicode.h"
#include "FXDEngine/Core/Algorithm.h"
#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Memory/New.h"

using namespace FXD;
using namespace Core;

// ------
// UnicodeFuncs
// -
// ------
// replacement character. Standard correct value is 0xFFFD.
#define kReplacementChar 0xFFFD

// Look up table. Shift a byte >> 1, then look up how many bytes to expect after it.
// Contains -1's for illegal values.
static const FXD::U8 kFirstByteLUT[128] =
{
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 0x0F // single byte ascii
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 0x1F // single byte ascii
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 0x2F // single byte ascii
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 0x3F // single byte ascii

	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0x4F // trailing utf8
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0x5F // trailing utf8
	2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, // 0x6F // first of 2
	3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 6, 0, // 0x7F // first of 3,4,5,illegal in utf-8
};

// Look up table. Shift a 16-bit word >> 10, then look up whether it is a surrogate,
// and which part. 0 means non-surrogate, 1 means 1st in pair, 2 means 2nd in pair.
static const FXD::U8 kSurrogateLUT[64] =
{
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0x0F 
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0x1F 
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0x2F 
	0, 0, 0, 0, 0, 0, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, // 0x3F 
};

// Look up table. Feed value from firstByteLUT in, gives you the mask for the data bits of that UTF-8 code unit.
// last 0=6, 1=7, 2=5, 4, 3, 2, 1 bits
static const FXD::U8 kByteMask8LUT[] =
{
	0x3f,
	0x7f,
	0x1f,
	0x0f,
	0x07,
	0x03,
	0x01
};

// Mask for the data bits of a UTF-16 surrogate.
static const FXD::U16 kByteMaskLow10 = 0x03ff;

//-----------------------------------------------------------------------------
inline bool isSurrogateRange(FXD::U32 nCodepoint)
{
	return ((0xd800 < nCodepoint) && (nCodepoint < 0xdfff));
}

inline bool isAboveBMP(FXD::U32 nCodepoint)
{
	return (nCodepoint > 0xFFFF);
}

//-----------------------------------------------------------------------------
FXD::UTF16* UnicodeFuncs::convertUTF8toUTF16(const FXD::UTF8* pString8)
{
	FXD::U32 nLen = Core::CharTraits< FXD::UTF8 >::length(pString8) + 1;
	FXD::UTF16* pRet = FXD_NEW_ARRAY(FXD::UTF16, nLen);
	return UnicodeFuncs::convertUTF8toUTF16(pString8, pRet);
}

FXD::UTF16* UnicodeFuncs::convertUTF8toUTF16(const FXD::UTF8* pString, FXD::UTF16* pStringOut)
{
	FXD::U32 nLen = Core::CharTraits< FXD::UTF8 >::length(pString) + 1;
	FXD::U32 nCodepoints = UnicodeFuncs::convertUTF8toUTF16(pString, pStringOut, nLen);
	nCodepoints++;
	return pStringOut;
}

FXD::U32 UnicodeFuncs::convertUTF8toUTF16(const FXD::UTF8* pUnistring, FXD::UTF16* pOutbuffer, FXD::U32 nLen)
{
	PRINT_COND_ASSERT((nLen >= 1), "Core: Buffer for unicode conversion must be large enough to hold at least the null terminator");

	FXD::U32 nCodepoints = 0;
	while ((*pUnistring != '\0') && (nCodepoints < nLen))
	{
		FXD::U32 nWalked = 1;
		FXD::UTF32 nMiddleman = UnicodeFuncs::oneUTF8toUTF32(pUnistring, &nWalked);
		pOutbuffer[nCodepoints] = UnicodeFuncs::oneUTF32toUTF16(nMiddleman);
		pUnistring += nWalked;
		nCodepoints++;
	}

	nCodepoints = FXD::STD::Min(nCodepoints, nLen - 1);
	pOutbuffer[nCodepoints] = '\0';

	return nCodepoints;
}

//-----------------------------------------------------------------------------
FXD::UTF8* UnicodeFuncs::convertUTF16toUTF8(const FXD::UTF16* pString)
{
	FXD::U32 nLen = Core::CharTraits< FXD::UTF16 >::length(pString) + 1;
	FXD::UTF8* pRet = FXD_NEW_ARRAY(FXD::UTF8, nLen);
	FXD::U32 nCodeunits = UnicodeFuncs::convertUTF16toUTF8(pString, pRet, nLen);
	nCodeunits++;
	return pRet;
}

FXD::UTF8* UnicodeFuncs::convertUTF16toUTF8(const FXD::UTF16* pString, FXD::UTF8* pStringOut)
{
	FXD::U32 nLen = Core::CharTraits< FXD::UTF16 >::length(pString) + 1;
	FXD::U32 nCodepoints = UnicodeFuncs::convertUTF16toUTF8(pString, pStringOut, nLen);
	nCodepoints++;
	return pStringOut;
}

FXD::U32 UnicodeFuncs::convertUTF16toUTF8(const FXD::UTF16* pUnistring, FXD::UTF8* pOutbuffer, FXD::U32 nLen)
{
	PRINT_COND_ASSERT((nLen >= 1), "Core: Buffer for unicode conversion must be large enough to hold at least the null terminator");

	FXD::U32 nCodeunits = 0;
	while ((*pUnistring != '\0') && (nCodeunits < nLen))
	{
		FXD::U32 nWalked = 1;
		FXD::UTF32 nMiddleman = oneUTF16toUTF32(pUnistring, &nWalked);
		FXD::U32 nCodeunitLen = oneUTF32toUTF8(nMiddleman, &pOutbuffer[nCodeunits]);
		pUnistring += nWalked;
		nCodeunits += nCodeunitLen;
	}

	nCodeunits = FXD::STD::Min(nCodeunits, nLen - 1);
	pOutbuffer[nCodeunits] = '\0';
	return nCodeunits;
}

//-----------------------------------------------------------------------------
// Functions that converts one unicode codepoint at a time
//-----------------------------------------------------------------------------
FXD::UTF32 UnicodeFuncs::oneUTF8toUTF32(const FXD::UTF8* pCodepoint, FXD::U32* pUnitsWalked)
{
	// codepoints 6 codeunits long are read, but do not convert correctly, and are filtered out anyway.
	// early out for ascii
	if (!(*pCodepoint & 0x0080))
	{
		if (pUnitsWalked != nullptr)
		{
			*pUnitsWalked = 1;
		}
		return (FXD::UTF32)*pCodepoint;
	}

	FXD::U32 nExpectedByteCount;
	FXD::UTF32 nRetVal = 0;
	FXD::U8 nCodeunit;

	// check the first byte (a.k.a. nCodeunit) .
	FXD::U8 c = pCodepoint[0];
	c = c >> 1;
	nExpectedByteCount = kFirstByteLUT[c];
	if (nExpectedByteCount > 0) // 0 or negative is illegal to start with
	{
		// process 1st nCodeunit
		nRetVal |= kByteMask8LUT[nExpectedByteCount] & pCodepoint[0]; // bug?

																		// process trailing codeunits
		for (FXD::U32 i = 1; i < nExpectedByteCount; i++)
		{
			nCodeunit = pCodepoint[i];
			if (kFirstByteLUT[nCodeunit >> 1] == 0)
			{
				nRetVal <<= 6; // shift up 6
				nRetVal |= (nCodeunit & 0x3f); // mask in the low 6 bits of this nCodeunit byte.
			}
			else
			{
				// found a bad codepoint - did not get a medial where we wanted one.
				// Dump the replacement, and claim to have parsed only 1 char,
				// so that we'll dump a slew of replacements, instead of eating the next char.
				nRetVal = kReplacementChar;
				nExpectedByteCount = 1;
				break;
			}
		}
	}
	else
	{
		// found a bad codepoint - got a medial or an illegal nCodeunit. 
		// Dump the replacement, and claim to have parsed only 1 char,
		// so that we'll dump a slew of replacements, instead of eating the next char.
		nRetVal = kReplacementChar;
		nExpectedByteCount = 1;
	}

	if (pUnitsWalked != nullptr)
	{
		*pUnitsWalked = nExpectedByteCount;
	}

	// codepoints in the surrogate range are illegal, and should be replaced.
	if (isSurrogateRange(nRetVal))
	{
		nRetVal = kReplacementChar;
	}

	// codepoints outside the Basic Multilingual Plane add complexity to our FXD::UTF16 string classes,
	// we've read them correctly so they won't foul the byte stream,
	// but we kill them here to make sure they wont foul anything else
	if (isAboveBMP(nRetVal))
	{
		nRetVal = kReplacementChar;
	}
	return nRetVal;
}

//-----------------------------------------------------------------------------
FXD::UTF32 UnicodeFuncs::oneUTF16toUTF32(const FXD::UTF16* pCodepoint, FXD::U32* pUnitsWalked)
{
	//PROFILE_START(oneUTF16toUTF32);
	FXD::U8 expectedType;
	FXD::U32 nUnitCount;
	FXD::UTF32 nRetVal = 0;
	FXD::UTF16 codeunit1;
	FXD::UTF16 codeunit2;

	codeunit1 = pCodepoint[0];
	expectedType = kSurrogateLUT[codeunit1 >> 10];
	switch (expectedType)
	{
		case 0: // simple
		{
			nRetVal = codeunit1;
			nUnitCount = 1;
			break;
		}
		case 1: // 2 surrogates
		{
			codeunit2 = pCodepoint[1];
			if (kSurrogateLUT[codeunit2 >> 10] == 2)
			{
				nRetVal = ((codeunit1 & kByteMaskLow10) << 10) | (codeunit2 & kByteMaskLow10);
				nUnitCount = 2;
				break;
			}
			// else, did not find a trailing surrogate where we expected one, so fall through to the error
		}
		case 2: // error
		{
			// found a trailing surrogate where we expected a codepoint or leading surrogate.
			// Dump the replacement.
			nRetVal = kReplacementChar;
			nUnitCount = 1;
			break;
		}
		default:
		{
			// unexpected return
			PRINT_ASSERT << "Core: oneUTF16toUTF323: unexpected type";
			nRetVal = kReplacementChar;
			nUnitCount = 1;
			break;
		}
	}

	if (pUnitsWalked != nullptr)
	{
		*pUnitsWalked = nUnitCount;
	}

	// codepoints in the surrogate range are illegal, and should be replaced.
	if (isSurrogateRange(nRetVal))
	{
		nRetVal = kReplacementChar;
	}

	// codepoints outside the Basic Multilingual Plane add complexity to our FXD::UTF16 string classes,
	// we've read them correctly so they wont foul the byte stream,
	// but we kill them here to make sure they wont foul anything else
	// NOTE: these are perfectly legal codepoints, we just dont want to deal with them.
	if (isAboveBMP(nRetVal))
	{
		nRetVal = kReplacementChar;
	}
	return nRetVal;
}

//-----------------------------------------------------------------------------
FXD::UTF16 UnicodeFuncs::oneUTF32toUTF16(const FXD::UTF32 codepoint)
{
	// Found a codepoint outside the encodable UTF-16 range! or, found an illegal codeoint!
	if ((codepoint >= 0x10FFFF) || (isSurrogateRange(codepoint)))
	{
		return kReplacementChar;
	}

	// These are legal, we just don't want to deal with them.
	if (isAboveBMP(codepoint))
	{
		return kReplacementChar;
	}
	return (FXD::UTF16)codepoint;
}

//-----------------------------------------------------------------------------
FXD::U32 UnicodeFuncs::oneUTF32toUTF8(const FXD::UTF32 nCodepoint, FXD::UTF8* pThreeByteCodeunitBuf)
{
	FXD::U32 nBytecount = 0;
	FXD::U32 nWorking = nCodepoint;
	FXD::UTF8* pBuf = pThreeByteCodeunitBuf;

	//-----------------
	if (isSurrogateRange(nWorking))	// found an illegal codepoint!
	{
		nWorking = kReplacementChar;
	}
	
	// These are legal, we just dont want to deal with them.
	if (isAboveBMP(nWorking))
	{
		nWorking = kReplacementChar;
	}

	//-----------------
	if (nWorking < (1 << 7))	// codeable in 7 bits
	{
		nBytecount = 1;
	}
	else if (nWorking < (1 << 11))	// codeable in 11 bits
	{
		nBytecount = 2;
	}
	else if (nWorking < (1 << 16))	// codeable in 16 bits
	{
		nBytecount = 3;
	}

	//AssertISV(bytecount > 0, "Error converting to UTF-8 in oneUTF32toUTF8(). isAboveBMP() should have caught this!");

	//-----------------
	FXD::U8 mask = kByteMask8LUT[0];	// 0011 1111
	FXD::U8 marker = (~mask << 1);		// 1000 0000

													// Process the low order bytes, shifting the codepoint down 6 each pass.
	for (FXD::S32 n1 = nBytecount - 1; n1 > 0; n1--)
	{
		pThreeByteCodeunitBuf[n1] = marker | (nWorking & mask);
		nWorking >>= 6;
	}

	// Process the 1st byte. filter based on the # of expected bytes.
	mask = kByteMask8LUT[nBytecount];
	marker = (~mask << 1);
	pThreeByteCodeunitBuf[0] = marker | nWorking & mask;

	//PROFILE_END();
	return nBytecount;
}

//------------------------------------------------------------------------------
// Byte Order Mark functions

bool UnicodeFuncs::chompUTF8BOM(const FXD::UTF8* pInString, FXD::UTF8** pOutStringPtr)
{
	*pOutStringPtr = const_cast< FXD::UTF8* >(pInString);

	FXD::U8 bom[4];
	Memory::MemCopy(bom, pInString, 4);

	bool bValid = isValidUTF8BOM(bom);

	// This is hackey, but I am not sure the best way to do it at the present.
	// The only valid BOM is a FXD::UTF8 BOM, which is 3 bytes, even though we read
	// 4 bytes because it could possibly be a FXD::UTF32 BOM, and we want to provide
	// an accurate error message. Perhaps this could be re-worked when more UTF
	// formats are supported to have isValidBOM return the size of the BOM, in
	// bytes.
	if (bValid)
	{
		(*pOutStringPtr) += 3; // SEE ABOVE!! -pw
	}
	return bValid;
}

bool UnicodeFuncs::isValidUTF8BOM(FXD::U8 bom[4])
{
	// Is it a BOM?
	if (bom[0] == 0)
	{
		// Could be UTF32BE
		if ((bom[1] == 0) && (bom[2] == 0xFE) && (bom[3] == 0xFF))
		{
			PRINT_WARN << "Core: Encountered a FXD::UTF32 BE BOM in this file; Torque does NOT support this file encoding. Use FXD::UTF8!";
			return false;
		}

		return false;
	}
	else if (bom[0] == 0xFF)
	{
		// It's little endian, either FXD::UTF16 or FXD::UTF32
		if (bom[1] == 0xFE)
		{
			if ((bom[2] == 0) && (bom[3] == 0))
			{
				PRINT_WARN << "Core: Encountered a FXD::UTF32 LE BOM in this file; Torque does NOT support this file encoding. Use FXD::UTF8!";
			}
			else
			{
				PRINT_WARN << "Core: Encountered a FXD::UTF16 LE BOM in this file; Torque does NOT support this file encoding. Use FXD::UTF8!";
			}
		}

		return false;
	}
	else if ((bom[0] == 0xFE) && (bom[1] == 0xFF))
	{
		PRINT_WARN << "Core: Encountered a FXD::UTF16 BE BOM in this file; Torque does NOT support this file encoding. Use FXD::UTF8!";
		return false;
	}
	else if ((bom[0] == 0xEF) && (bom[1] == 0xBB) && (bom[2] == 0xBF))
	{
		// Can enable this if you want -pw
		// Con::printf("Encountered a FXD::UTF8 BOM. Torque supports this");
		return true;
	}
	// Don't print out an error message here, because it will try this with
	// every script. -pw
	return false;
}