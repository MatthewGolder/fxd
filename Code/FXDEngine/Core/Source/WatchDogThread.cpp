// Creator - MatthewGolder
#include "FXDEngine/Core/WatchDogThread.h"

using namespace FXD;
using namespace Core;

// ------
// WatchDogThread
// -
// ------
WatchDogThread::WatchDogThread(const FXD::UTF8* pName, const FXD::U32 nTimeoutMS)
	: IThread(pName)
	, m_bShuttingDown(false)
	, m_nTimeoutMS(nTimeoutMS)
	, m_nCounter(0)
{
	start_thread();
}

WatchDogThread::~WatchDogThread(void)
{
	m_bShuttingDown = true;
	if (!join())
	{
		PRINT_ASSERT << "Core: WatchDogThread took too long to shutdown";
	}
}

bool WatchDogThread::thread_init_func(void)
{
	return true;
}

bool WatchDogThread::thread_process_func(void)
{
	if (m_bShuttingDown)
	{
		return false;
	}

	FXD::U64 nVal1 = m_nCounter;
	Thread::Funcs::Sleep(m_nTimeoutMS);
	FXD::U64 nVal2 = m_nCounter;
	if (nVal1 == nVal2)
	{
		PRINT_ASSERT << "Core: Timeout watchdog has triggered";
	}
	return true;
}

bool WatchDogThread::thread_shutdown_func(void)
{
	return true;
}

void WatchDogThread::increment(void)
{
	m_nCounter += 1;
}