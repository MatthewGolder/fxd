// Creator - MatthewGolder
#include "FXDEngine/Core/ExpressionTree.h"
#include "FXDEngine/Core/TypeTraits.h"
#include "FXDEngine/Memory/StreamMemory.h"

using namespace FXD;
using namespace Core;

// ------
// ExpressionTree
// -
// ------
ExpressionTree::ExpressionTree(void)
{
}

ExpressionTree::ExpressionTree(const Core::ExpressionTokeniser& tokens)
{
	build_tree(tokens);
}

ExpressionTree::~ExpressionTree(void)
{
}

void ExpressionTree::build_tree(const Core::ExpressionTokeniser& tokens)
{
	FXD::U16 nTokenID = 0;
	m_rootNode = parse_expression(tokens, nTokenID);
	PRINT_COND_ASSERT((m_rootNode != nullptr), "Core: Error parsing expression tree - could not create expression");
	PRINT_COND_ASSERT((nTokenID == tokens.get_tokens().size()), "Core: Error parsing expression tree - did not fully consume expression");
}

// Compile a cast from one type to another.
void writeConversionOp(Memory::StreamMemory& stream, Core::E_ExpressionType srcExpressionType, Core::E_ExpressionType dstExpressionType)
{
	if ((srcExpressionType == Core::E_ExpressionType::Bool) && (dstExpressionType == Core::E_ExpressionType::Float))
	{
		stream << FXD::STD::to_underlying(E_ExpressionCode::ConvertBoolFloat);
	}
	else
	{
		PRINT_ASSERT << "Core: Cannot convert between these types";
	}
}

// ------
// ExpressionTree::IExpressionNode_BinaryOp
// -
// ------
class ExpressionTree::IExpressionNode_BinaryOp FINAL : public ExpressionTree::IExpressionNode
{
public:
	ExpressionTree::expression_node m_leftNode;
	ExpressionTree::expression_node m_rightNode;
	Core::E_TokenType m_eOp;

public:
	IExpressionNode_BinaryOp(const ExpressionTree::expression_node& leftNode, const ExpressionTree::expression_node& rightNode, Core::E_TokenType eOp)
		: m_leftNode(leftNode)
		, m_rightNode(rightNode)
		, m_eOp(eOp)
	{
	}
	~IExpressionNode_BinaryOp(void)
	{
	}

	void precompile(Core::E_ExpressionCode& eExprCode, Core::E_ExpressionType& eLeftType, Core::E_ExpressionType& eRightType, Core::E_ExpressionType& eResultType, const Core::IVariableProvider& vars) const
	{
		// First find the actual types of the two sides.
		eLeftType = m_leftNode->get_type(vars);
		eRightType = m_rightNode->get_type(vars);

		// Now determine the operation.
		switch (m_eOp)
		{
		case Core::E_TokenType::Add:
		{
			eExprCode = Core::E_ExpressionCode::FloatAdd;
			eLeftType = Core::E_ExpressionType::Float;
			eRightType = Core::E_ExpressionType::Float;
			eResultType = Core::E_ExpressionType::Float;
			break;
		}
		case Core::E_TokenType::Minus:
		{
			eExprCode = Core::E_ExpressionCode::FloatSubtract;
			eLeftType = Core::E_ExpressionType::Float;
			eRightType = Core::E_ExpressionType::Float;
			eResultType = Core::E_ExpressionType::Float;
			break;
		}
		case Core::E_TokenType::Multiply:
		{
			eExprCode = Core::E_ExpressionCode::FloatMultiply;
			eLeftType = Core::E_ExpressionType::Float;
			eRightType = Core::E_ExpressionType::Float;
			eResultType = Core::E_ExpressionType::Float;
			break;
		}
		case Core::E_TokenType::Divide:
		{
			eExprCode = Core::E_ExpressionCode::FloatDivide;
			eLeftType = Core::E_ExpressionType::Float;
			eRightType = Core::E_ExpressionType::Float;
			eResultType = Core::E_ExpressionType::Float;
			break;
		}
		case Core::E_TokenType::Greater:
		{
			eExprCode = Core::E_ExpressionCode::FloatGreater;
			eLeftType = Core::E_ExpressionType::Float;
			eRightType = Core::E_ExpressionType::Float;
			eResultType = Core::E_ExpressionType::Bool;
			break;
		}
		case Core::E_TokenType::GreaterEqual:
		{
			eExprCode = Core::E_ExpressionCode::FloatGreaterEqual;
			eLeftType = Core::E_ExpressionType::Float;
			eRightType = Core::E_ExpressionType::Float;
			eResultType = Core::E_ExpressionType::Bool;
			break;
		}
		case Core::E_TokenType::Less:
		{
			eExprCode = Core::E_ExpressionCode::FloatLess;
			eLeftType = Core::E_ExpressionType::Float;
			eRightType = Core::E_ExpressionType::Float;
			eResultType = Core::E_ExpressionType::Bool;
			break;
		}
		case Core::E_TokenType::LessEqual:
		{
			eExprCode = Core::E_ExpressionCode::FloatLessEqual;
			eLeftType = Core::E_ExpressionType::Float;
			eRightType = Core::E_ExpressionType::Float;
			eResultType = Core::E_ExpressionType::Bool;
			break;
		}
		case Core::E_TokenType::And:
		{
			eExprCode = Core::E_ExpressionCode::BoolAdd;
			eLeftType = Core::E_ExpressionType::Bool;
			eRightType = Core::E_ExpressionType::Bool;
			eResultType = Core::E_ExpressionType::Bool;
			break;
		}
		case Core::E_TokenType::Or:
		{
			eExprCode = Core::E_ExpressionCode::BoolOr;
			eLeftType = Core::E_ExpressionType::Bool;
			eRightType = Core::E_ExpressionType::Bool;
			eResultType = Core::E_ExpressionType::Bool;
			break;
		}
		default:
		{
			PRINT_ASSERT << "Core: Cannot determine expression op for " << Expression::type_to_string(eLeftType) << " " << ExpressionTokeniser::token_type_to_string(m_eOp) << " " << Expression::type_to_string(eRightType);
			break;
		}
		}
	}
	void compile(Memory::StreamMemory& stream, const Core::IVariableProvider& vars) const FINAL
	{
		// Find the types of the two sides.
		Core::E_ExpressionType eLeftTypeReal = m_leftNode->get_type(vars);
		Core::E_ExpressionType eRightTypeReal = m_rightNode->get_type(vars);

		// Do the precompile step.
		Core::E_ExpressionCode ec;
		Core::E_ExpressionType eLeftType, eRightType, eResultType;
		precompile(ec, eLeftType, eRightType, eResultType, vars);

		// Evaluate the left and convert to correct type.
		m_leftNode->compile(stream, vars);
		if (eLeftType != eLeftTypeReal)
		{
			writeConversionOp(stream, eLeftTypeReal, eLeftType);
		}

		// Evaluate the right and convert to correct type.
		m_rightNode->compile(stream, vars);
		if (eRightType != eRightTypeReal)
		{
			writeConversionOp(stream, eRightTypeReal, eRightType);
		}

		// Now write the op.
		stream << FXD::STD::to_underlying(ec);
	}
	Core::E_ExpressionType get_type(const Core::IVariableProvider& vars) const FINAL
	{
		Core::E_ExpressionCode eExpCode;
		Core::E_ExpressionType eLeftType, eRightType, eResultType;
		precompile(eExpCode, eLeftType, eRightType, eResultType, vars);
		return eResultType;
	}
};

// ------
// ExpressionTree::IExpressionNode_UnaryOp
// -
// ------
class ExpressionTree::IExpressionNode_UnaryOp FINAL : public ExpressionTree::IExpressionNode
{
public:
	ExpressionTree::expression_node m_expressionNode;
	Core::E_TokenType m_eOP;

public:
	IExpressionNode_UnaryOp(const ExpressionTree::expression_node& expressionNode, Core::E_TokenType eOP)
		: m_expressionNode(expressionNode)
		, m_eOP(eOP)
	{
	}
	~IExpressionNode_UnaryOp(void)
	{
	}

	void compile(Memory::StreamMemory& stream, const Core::IVariableProvider& vars) const FINAL
	{

		Core::E_ExpressionType type = m_expressionNode->get_type(vars);
		switch (m_eOP)
		{
		case Core::E_TokenType::Minus:
		{
			PRINT_COND_ASSERT(type == Core::E_ExpressionType::Float, "Core: Unary -(negate) only works on FXD::F32 types");
			m_expressionNode->compile(stream, vars);

			stream << FXD::STD::to_underlying(E_ExpressionCode::FloatNegate);
			break;
		}
		case Core::E_TokenType::Not:
		{
			PRINT_COND_ASSERT(type == Core::E_ExpressionType::Bool, "Core: Unary !(NOT) only works on bool types");
			m_expressionNode->compile(stream, vars);

			stream << FXD::STD::to_underlying(E_ExpressionCode::BoolNot);
			break;
		}
		default:
		{
			PRINT_ASSERT << "Core: Cannot determine expression op for " << ExpressionTokeniser::token_type_to_string(m_eOP) << " " << Expression::type_to_string(type);
			break;
		}
		}
	}
	Core::E_ExpressionType get_type(const Core::IVariableProvider& vars) const FINAL
	{
		return m_expressionNode->get_type(vars);
	}
};

// ------
// ExpressionTree::IExpressionNode_Literal
// -
// ------
class ExpressionTree::IExpressionNode_Literal FINAL : public ExpressionTree::IExpressionNode
{
public:
	IExpressionNode_Literal(FXD::F32 fValue)
		: m_fValue(fValue)
	{
	}
	~IExpressionNode_Literal(void)
	{
	}

	void compile(Memory::StreamMemory& stream, const Core::IVariableProvider& /*vars*/) const FINAL
	{
		stream << FXD::STD::to_underlying(E_ExpressionCode::FloatLiteral);
		stream << (FXD::U32)(m_fValue);
	}
	Core::E_ExpressionType get_type(const Core::IVariableProvider& /*vars*/) const FINAL
	{
		return Core::E_ExpressionType::Float;
	}

public:
	FXD::F32 m_fValue;
};

// ------
// ExpressionTree::IExpressionNode_Variable
// -
// ------
class ExpressionTree::IExpressionNode_Variable FINAL : public ExpressionTree::IExpressionNode
{
public:
	IExpressionNode_Variable(const Core::String8& strName)
		: m_strName(strName)
	{
	}
	~IExpressionNode_Variable(void)
	{
	}

	void compile(Memory::StreamMemory& stream, const Core::IVariableProvider& vars) const FINAL
	{
		Core::E_ExpressionType eType;
		FXD::S32 nID = vars.get_variableID_and_type(m_strName, eType);
		if (eType == Core::E_ExpressionType::Float)
		{
			stream << FXD::STD::to_underlying(E_ExpressionCode::FloatVariable);
		}
		else if (eType == Core::E_ExpressionType::Bool)
		{
			stream << FXD::STD::to_underlying(E_ExpressionCode::BoolVariable);
		}
		else
		{
			PRINT_ASSERT << "Core: Expressions do not support variables of type " << Expression::type_to_string(eType);
		}	stream << (FXD::U32)(nID);
	}

	Core::E_ExpressionType get_type(const Core::IVariableProvider& vars) const FINAL
	{
		Core::E_ExpressionType eType;
		vars.get_variableID_and_type(m_strName, eType);
		return eType;
	}

public:
	Core::String8 m_strName;
};

ExpressionTree::expression_node ExpressionTree::create_binary_expression_node(const ExpressionTree::expression_node& eLeftNode, const ExpressionTree::expression_node& eRightNode, Core::E_TokenType eOp)
{
	ExpressionTree::expression_node ptr = std::make_shared< ExpressionTree::IExpressionNode_BinaryOp >(eLeftNode, eRightNode, eOp);
	return ptr;
}

ExpressionTree::expression_node ExpressionTree::create_literal_expression_node(FXD::F32 fValue)
{
	ExpressionTree::expression_node ptr = std::make_shared< ExpressionTree::IExpressionNode_Literal >(fValue);
	return ptr;
}

ExpressionTree::expression_node ExpressionTree::create_variable_expression_node(const Core::String8& strName)
{
	ExpressionTree::expression_node ptr = std::make_shared< ExpressionTree::IExpressionNode_Variable >(strName);
	return ptr;
}

ExpressionTree::expression_node ExpressionTree::create_unary_expression_node(const ExpressionTree::expression_node& expressionNode, Core::E_TokenType eOp)
{
	ExpressionTree::expression_node ptr = std::make_shared< ExpressionTree::IExpressionNode_UnaryOp >(expressionNode, eOp);
	return ptr;
}

// Expression parsing.
ExpressionTree::expression_node ExpressionTree::parse_expression(const Core::ExpressionTokeniser& tokens, FXD::U16& nTokenID)
{
	return parse_logical_expression(tokens, nTokenID);
}

ExpressionTree::expression_node ExpressionTree::parse_logical_expression(const Core::ExpressionTokeniser& tokens, FXD::U16& nTokenID)
{
	FXD::U16 nInitToken = nTokenID;
	ExpressionTree::expression_node expr = parse_equality_expression(tokens, nTokenID);
	if (!expr)
	{
		return expr;
	}

	while (nTokenID < tokens.get_tokens().size())
	{
		Core::E_TokenType eNextTokenType = tokens.get_tokens()[nTokenID].m_eType;
		switch (eNextTokenType)
		{
		case Core::E_TokenType::And:
		case Core::E_TokenType::Or:
		{
			nTokenID++;
			ExpressionTree::expression_node expr2 = parse_equality_expression(tokens, nTokenID);
			if (expr2)
			{
				expr = create_binary_expression_node(expr, expr2, eNextTokenType);
			}
			else
			{
				nTokenID = nInitToken;
				return ExpressionTree::expression_node();
			}
			break;
		}
		default:
		{
			return expr;
		}
		}
	}
	return expr;
}

ExpressionTree::expression_node ExpressionTree::parse_equality_expression(const Core::ExpressionTokeniser& tokens, FXD::U16& nTokenID)
{
	FXD::U16 nInitToken = nTokenID;
	ExpressionTree::expression_node expr = parse_comparison_expression(tokens, nTokenID);
	if (!expr)
	{
		return expr;
	}

	while (nTokenID < tokens.get_tokens().size())
	{
		Core::E_TokenType eNextTokenType = tokens.get_tokens()[nTokenID].m_eType;
		switch (eNextTokenType)
		{
		case Core::E_TokenType::IsEqual:
		case Core::E_TokenType::NotEqual:
		{
			nTokenID++;
			ExpressionTree::expression_node expr2 = parse_comparison_expression(tokens, nTokenID);
			if (expr2)
			{
				expr = create_binary_expression_node(expr, expr2, eNextTokenType);
			}
			else
			{
				nTokenID = nInitToken;
				return ExpressionTree::expression_node();
			}
			break;
		}
		default:
		{
			return expr;
		}
		}
	}
	return expr;
}

ExpressionTree::expression_node ExpressionTree::parse_comparison_expression(const Core::ExpressionTokeniser& tokens, FXD::U16& nTokenID)
{
	FXD::U16 nInitToken = nTokenID;
	ExpressionTree::expression_node expr = parse_additive_expression(tokens, nTokenID);
	if (!expr)
	{
		return expr;
	}

	while (nTokenID < tokens.get_tokens().size())
	{
		Core::E_TokenType eNextTokenType = tokens.get_tokens()[nTokenID].m_eType;
		switch (eNextTokenType)
		{
		case Core::E_TokenType::Less:
		case Core::E_TokenType::LessEqual:
		case Core::E_TokenType::Greater:
		case Core::E_TokenType::GreaterEqual:
		{
			nTokenID++;
			ExpressionTree::expression_node expr2 = parse_additive_expression(tokens, nTokenID);
			if (expr2)
			{
				expr = create_binary_expression_node(expr, expr2, eNextTokenType);
			}
			else
			{
				nTokenID = nInitToken;
				return ExpressionTree::expression_node();
			}
			break;
		}
		default:
		{
			return expr;
		}
		}
	}
	return expr;
}

ExpressionTree::expression_node ExpressionTree::parse_additive_expression(const Core::ExpressionTokeniser& tokens, FXD::U16& nTokenID)
{
	FXD::U16 nInitToken = nTokenID;
	ExpressionTree::expression_node expr = parse_multiplicative_expression(tokens, nTokenID);
	if (!expr)
	{
		return expr;
	}

	while (nTokenID < tokens.get_tokens().size())
	{
		Core::E_TokenType eNextTokenType = tokens.get_tokens()[nTokenID].m_eType;
		switch (eNextTokenType)
		{
		case Core::E_TokenType::Add:
		case Core::E_TokenType::Minus:
		{
			nTokenID++;
			ExpressionTree::expression_node expr2 = parse_multiplicative_expression(tokens, nTokenID);
			if (expr2)
			{
				expr = create_binary_expression_node(expr, expr2, eNextTokenType);
			}
			else
			{
				nTokenID = nInitToken;
				return ExpressionTree::expression_node();
			}
			break;
		}
		default:
		{
			return expr;
		}
		}
	}
	return expr;
}

ExpressionTree::expression_node ExpressionTree::parse_multiplicative_expression(const Core::ExpressionTokeniser& tokens, FXD::U16& nTokenID)
{
	FXD::U16 nInitToken = nTokenID;
	ExpressionTree::expression_node expr = parse_primary_expression(tokens, nTokenID);
	if (!expr)
	{
		return expr;
	}

	while (nTokenID < tokens.get_tokens().size())
	{
		Core::E_TokenType eNextTokenType = tokens.get_tokens()[nTokenID].m_eType;
		switch (eNextTokenType)
		{
		case Core::E_TokenType::Multiply:
		case Core::E_TokenType::Divide:
		{
			nTokenID++;
			ExpressionTree::expression_node expr2 = parse_primary_expression(tokens, nTokenID);
			if (expr2)
			{
				expr = create_binary_expression_node(expr, expr2, eNextTokenType);
			}
			else
			{
				nTokenID = nInitToken;
				return expression_node();
			}
			break;
		}
		default:
		{
			return expr;
		}
		}
	}
	return expr;
}

ExpressionTree::expression_node ExpressionTree::parse_primary_expression(const Core::ExpressionTokeniser& tokens, FXD::U16& nTokenID)
{
	FXD::U16 nInitToken = nTokenID;

	// Try parenthesis.
	if (tokens.get_tokens()[nTokenID].m_eType == Core::E_TokenType::LeftParenthesis)
	{
		nTokenID++;
		ExpressionTree::expression_node expr = parse_expression(tokens, nTokenID);
		if (expr)
		{
			if (tokens.get_tokens()[nTokenID].m_eType == Core::E_TokenType::RightParenthesis)
			{
				nTokenID++;
				return expr;
			}
		}
		nTokenID = nInitToken;
	}

	// Try number.
	if (tokens.get_tokens()[nTokenID].m_eType == Core::E_TokenType::Number)
	{
		return create_literal_expression_node(tokens.get_tokens()[nTokenID++].m_strText.to_F32());
	}

	// Try variable.
	if (tokens.get_tokens()[nTokenID].m_eType == Core::E_TokenType::Variable)
	{
		return create_variable_expression_node(tokens.get_tokens()[nTokenID++].m_strText);
	}

	// Try unary negate.
	if (tokens.get_tokens()[nTokenID].m_eType == Core::E_TokenType::Minus)
	{
		nTokenID++;
		ExpressionTree::expression_node expr = parse_primary_expression(tokens, nTokenID);
		if (expr)
		{
			return create_unary_expression_node(expr, Core::E_TokenType::Minus);
		}
		nTokenID = nInitToken;
	}
	// No match found.
	return ExpressionTree::expression_node();
}

// Build a compiled expression.
void ExpressionTree::build_compiled_expression(Core::Expression& expression, const Core::IVariableProvider& vars) const
{
	Memory::StreamMemory stream;
	m_rootNode->compile(stream, vars);
	expression.set_from_compiled(Memory::MemHandle::read_to_end(stream, true), m_rootNode->get_type(vars));
}