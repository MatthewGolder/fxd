// Creator - MatthewGolder
#include "FXDEngine/Core/CharTraits.h"
#include "FXDEngine/Memory/Memory.h"
#include <3rdParty/xxHash/xxhash.h>

#include <stdio.h>

using namespace FXD;
using namespace Core;

const FXD::U64 kStrSeed = 100;

// ------
// CharTraits< FXD::UTF16 >
// -
// ------
template <>
FXD::UTF16 CharTraits< FXD::UTF16 >::to_upper(const FXD::UTF16 ch)
{
	if (ch < 128)
	{
		return (FXD::UTF16)Core::CharTraits< FXD::UTF8 >::to_upper((FXD::UTF8)ch);
	}
	return ch;
}
template <>
FXD::UTF16* CharTraits< FXD::UTF16 >::to_upper(FXD::UTF16* const pStr, const FXD::U32 nCount)
{
	for (FXD::U32 nID = 0; nID < nCount; nID++)
	{
		pStr[nID] = to_upper(pStr[nID]);
	}
	return pStr;
}
template <>
FXD::UTF16* CharTraits< FXD::UTF16 >::to_upper(FXD::UTF16* const pStr)
{
	return to_upper(pStr, length(pStr));
}
template <>
FXD::UTF16 CharTraits< FXD::UTF16 >::to_lower(const FXD::UTF16 ch)
{
	if (ch < 128)
	{
		return (FXD::UTF16)Core::CharTraits< FXD::UTF8 >::to_lower((FXD::UTF8)ch);
	}
	return ch;
}
template <>
FXD::UTF16* CharTraits< FXD::UTF16 >::to_lower(FXD::UTF16* const pStr, const FXD::U32 nCount)
{
	for (FXD::U32 nID = 0; nID < nCount; nID++)
	{
		pStr[nID] = to_lower(pStr[nID]);
	}
	return pStr;
}
template <>
FXD::UTF16* CharTraits< FXD::UTF16 >::to_lower(FXD::UTF16* const pStr)
{
	return to_lower(pStr, length(pStr));
}
template <>
const FXD::HashValue CharTraits< FXD::UTF16 >::to_hash(const FXD::UTF16* const pStr, const FXD::U32 nCount)
{
	FXD::HashValue retHash = XXH64(pStr, (nCount * sizeof(FXD::UTF16)), kStrSeed);
	return retHash;
}
template <>
const FXD::HashValue CharTraits< FXD::UTF16 >::to_hash(const FXD::UTF16* const pStr)
{
	return to_hash(pStr, length(pStr));
}

template <>
FXD::UTF16* CharTraits< FXD::UTF16 >::assign(FXD::UTF16* const pDst, const FXD::UTF16 src, const FXD::U32 nCount)
{
	FXD::U32 nCount2 = nCount;
	FXD::UTF16* pDstCpy = pDst;
	for (; 0 < nCount2; ++pDstCpy, --nCount2)
	{
		(*pDstCpy) = src;
	}
	return pDst;
}
template <>
FXD::UTF16* CharTraits< FXD::UTF16 >::copy(FXD::UTF16* const pDst, const FXD::UTF16* pSrc, const FXD::U32 nCount)
{
	FXD::U32 nCount2 = nCount;
	FXD::UTF16* pNext = pDst;
	for (; 0 < nCount2; --nCount2, ++pNext, ++pSrc)
	{
		assign(*pNext, *pSrc);
	}
	return pDst;
}
template <>
FXD::UTF16* CharTraits< FXD::UTF16 >::move(FXD::UTF16* const pDst, const FXD::UTF16* pSrc, const FXD::U32 nCount)
{
	FXD::U32 nCount2 = nCount;
	FXD::UTF16* pNext = pDst;
	if (pSrc < pNext && pNext < pSrc + nCount2)
	{
		for (pNext += nCount2, pSrc += nCount2; 0 < nCount2; --nCount2)
		{
			assign(*--pNext, *--pSrc);
		}
	}
	else
	{
		for (; 0 < nCount2; --nCount2, ++pNext, ++pSrc)
		{
			assign(*pNext, *pSrc);
		}
	}
	return pDst;
}
template <>
FXD::S32 CharTraits< FXD::UTF16 >::compare(const FXD::UTF16* const pLhs, const FXD::UTF16* const pRhs, const FXD::U32 nCount)
{
	FXD::U32 nCount2 = nCount;
	for (FXD::U32 nID = 0; 0 < nCount2; --nCount2, nID++)
	{
		if (!equals((pLhs[nID]), (pRhs[nID])))
		{
			return (lt((pLhs[nID]), (pRhs[nID])) ? -1 : +1);
		}
	}
	return 0;
}
template <>
FXD::S32 CharTraits< FXD::UTF16 >::compare(const FXD::UTF16* const pLhs, const FXD::UTF16* const pRhs)
{
	return compare(pLhs, pRhs, length(pRhs));
}
template <>
FXD::S32 CharTraits< FXD::UTF16 >::compare_no_case(const FXD::UTF16* const pLhs, const FXD::UTF16* const pRhs, const FXD::U32 nCount)
{
	FXD::U32 nCount2 = nCount;
	for (FXD::U32 nID = 0; 0 < nCount2; --nCount2, nID++)
	{
		if (!equals(to_lower(pLhs[nID]), to_lower(pRhs[nID])))
		{
			return (lt(to_lower(pLhs[nID]), to_lower(pRhs[nID])) ? -1 : +1);
		}
	}
	return 0;
}
template <>
FXD::S32 CharTraits< FXD::UTF16 >::compare_no_case(const FXD::UTF16* const pLhs, const FXD::UTF16* const pRhs)
{
	return compare_no_case(pLhs, pRhs, length(pRhs));
}
template <>
const FXD::UTF16* CharTraits< FXD::UTF16 >::find(const FXD::UTF16* const pStr, const FXD::U32 nCount, const FXD::UTF16& ch)
{
	FXD::U32 nCount2 = nCount;
	for (FXD::U32 nID = 0; 0 < nCount2; nID++, --nCount2)
	{
		if (pStr[nID] == ch)
		{
			return (FXD::UTF16*)&pStr[nID];
		}
	}
	return nullptr;
}
template <>
const FXD::UTF16* CharTraits< FXD::UTF16 >::find(const FXD::UTF16* const pStr, const FXD::UTF16& ch)
{
	if (pStr == nullptr)
	{
		return nullptr;
	}

	FXD::U32 nLen = length(pStr);
	for (FXD::U32 nID = 0; 0 < nLen; nID++, --nLen)
	{
		if (pStr[nID] == ch)
		{
			return (FXD::UTF16*)&pStr[nID];
		}
	}
	return nullptr;
}
template <>
const FXD::UTF16* CharTraits< FXD::UTF16 >::find_no_case(const FXD::UTF16* const pStr, const FXD::U32 nCount, const FXD::UTF16& ch)
{
	FXD::U32 nCount2 = nCount;
	FXD::UTF16 l = to_lower(ch);
	for (FXD::U32 nID = 0; 0 < nCount2; nID++, --nCount2)
	{
		FXD::UTF16 f = to_lower(pStr[nID]);
		if (f == l)
		{
			return (FXD::UTF16*)&pStr[nID];
		}
	}
	return nullptr;
}
template <>
const FXD::UTF16* CharTraits< FXD::UTF16 >::find_no_case(const FXD::UTF16* const pStr, const FXD::UTF16& ch)
{
	if (pStr == nullptr)
	{
		return nullptr;
	}

	FXD::U32 nLen = length(pStr);
	FXD::UTF16 l = to_lower(ch);
	for (FXD::U32 nID = 0; 0 < nLen; nID++, --nLen)
	{
		FXD::UTF16 f = to_lower(pStr[nID]);
		if (f == l)
		{
			return (FXD::UTF16*)&pStr[nID];
		}
	}
	return nullptr;
}
template <>
const bool CharTraits< FXD::UTF16 >::is_quote(const FXD::UTF16 ch)
{
	return (ch == u'\"');
}
template <>
const bool CharTraits< FXD::UTF16 >::is_dot(const FXD::UTF16 ch)
{
	return (ch == u'.');
}
template <>
const bool CharTraits< FXD::UTF16 >::is_underscore(const FXD::UTF16 ch)
{
	return (ch == u'_');
}
template <>
const bool CharTraits< FXD::UTF16 >::is_null_terminator(const FXD::UTF16 ch)
{
	return (ch == u'\0');
}
template <>
const bool CharTraits< FXD::UTF16 >::is_white_space(const FXD::UTF16 ch)
{
	return ((ch == u' ') || (ch == u'\t') || (ch == u'\n') || (ch == u'\r'));
}
template <>
const bool CharTraits< FXD::UTF16 >::is_space(const FXD::UTF16 ch)
{
	return ((ch == ' ') || ((ch >= 0x09) && (ch <= 0x0d)));
}
template <>
const bool CharTraits< FXD::UTF16 >::is_numeric(const FXD::UTF16 ch)
{
	return ((u'0' <= ch) && (u'9' >= ch));
}
template <>
const bool CharTraits< FXD::UTF16 >::is_alpha(const FXD::UTF16 ch)
{
	return (((u'A' <= ch) && (u'Z' >= ch)) || ((u'a' <= ch) && (u'z' >= ch)));
}
template <>
const bool CharTraits< FXD::UTF16 >::is_alpha_numeric(const FXD::UTF16 ch)
{
	return (is_numeric(ch) || is_alpha(ch));
}
template <>
const bool CharTraits< FXD::UTF16 >::is_japanese_punctuation(const FXD::UTF16 ch)
{
	return ((ch >= 0x3000) && (ch <= 0x303f));
}
template <>
const bool CharTraits< FXD::UTF16 >::is_hiragana(const FXD::UTF16 ch)
{
	return ((ch >= 0x3040) && (ch <= 0x309f));
}
template <>
const bool CharTraits< FXD::UTF16 >::is_katakana(const FXD::UTF16 ch)
{
	return ((ch >= 0x30a0) && (ch <= 0x30ff));
}
template <>
const bool CharTraits< FXD::UTF16 >::is_katakana_phonetic_extensions(const FXD::UTF16 ch)
{
	return ((ch >= 0x31F0) && (ch <= 0x31FF));
}
template <>
const bool CharTraits< FXD::UTF16 >::is_kanji(const FXD::UTF16 ch)
{
	return ((ch >= 0x4E00) && (ch <= 0x9FCC));
}
template <>
const bool CharTraits< FXD::UTF16 >::is_japanese(const FXD::UTF16 ch)
{
	return ((is_japanese_punctuation(ch)) || (is_hiragana(ch)) || (is_katakana(ch)) || (is_katakana_phonetic_extensions(ch)) || (is_kanji(ch)));
}

template <>
void CharTraits< FXD::UTF16 >::to_string(FXD::UTF16* const pBuf, const FXD::U32 nSize, const FXD::S8 nVal)
{
	swprintf((wchar_t*)pBuf, nSize, L"%d", nVal);
	//swprintf_s((wchar_t*)pBuf, nVal, L"%d", (wchar_t*)pBuf);
}
template <>
void CharTraits< FXD::UTF16 >::to_string(FXD::UTF16* const pBuf, const FXD::U32 nSize, const FXD::U8 nVal)
{
	swprintf((wchar_t*)pBuf, nSize, L"%d", nVal);
	//swprintf_s((wchar_t*)pBuf, nVal, L"%d", (wchar_t*)pBuf);
}
template <>
void CharTraits< FXD::UTF16 >::to_string(FXD::UTF16* const pBuf, const FXD::U32 nSize, const FXD::S16 nVal)
{
	swprintf((wchar_t*)pBuf, nSize, L"%d", nVal);
	//swprintf_s((wchar_t*)pBuf, nVal, L"%d", (wchar_t*)pBuf);
}
template <>
void CharTraits< FXD::UTF16 >::to_string(FXD::UTF16* const pBuf, const FXD::U32 nSize, const FXD::U16 nVal)
{
	swprintf((wchar_t*)pBuf, nSize, L"%d", nVal);
	//swprintf_s((wchar_t*)pBuf, nVal, L"%d", (wchar_t*)pBuf);
}
template <>
void CharTraits< FXD::UTF16 >::to_string(FXD::UTF16* const pBuf, const FXD::U32 nSize, const FXD::S32 nVal)
{
	swprintf((wchar_t*)pBuf, nSize, L"%d", nVal);
	//swprintf_s((wchar_t*)pBuf, nVal, L"%d", (wchar_t*)pBuf);
}
template <>
void CharTraits< FXD::UTF16 >::to_string(FXD::UTF16* const pBuf, const FXD::U32 nSize, const FXD::U32 nVal)
{
	swprintf((wchar_t*)pBuf, nSize, L"%d", nVal);
	//swprintf_s((wchar_t*)pBuf, nVal, L"%d", (wchar_t*)pBuf);
}
template <>
void CharTraits< FXD::UTF16 >::to_string(FXD::UTF16* const pBuf, const FXD::U32 nSize, const FXD::S64 nVal)
{
	swprintf((wchar_t*)pBuf, nSize, L"%lld", nVal);
	//swprintf_s((wchar_t*)pBuf, nSize, L"%lld", nVal);
}
template <>
void CharTraits< FXD::UTF16 >::to_string(FXD::UTF16* const pBuf, const FXD::U32 nSize, const FXD::U64 nVal)
{
	swprintf((wchar_t*)pBuf, nSize, L"%llu", nVal);
	//swprintf_s((wchar_t*)pBuf, nSize, L"%llu", nVal);
}
template <>
void CharTraits< FXD::UTF16 >::to_string(FXD::UTF16* const pBuf, const FXD::U32 nSize, const FXD::F32 fVal)
{
	swprintf((wchar_t*)pBuf, nSize, L"%f", fVal);
	//swprintf_s((wchar_t*)pBuf, nSize, L"%f", fVal);
}
template <>
void CharTraits< FXD::UTF16 >::to_string(FXD::UTF16* const pBuf, const FXD::U32 nSize, const FXD::F64 fVal)
{
	swprintf((wchar_t*)pBuf, nSize, L"%f", fVal);
	//swprintf_s((wchar_t*)pBuf, nSize, L"%f", fVal);
}