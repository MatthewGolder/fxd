// Creator - MatthewGolder
#include "FXDEngine/Core/Seek.h"

using namespace FXD;
using namespace Core;

// ------
// IStreamSeekable
// -
// ------
IStreamSeekable::IStreamSeekable(void)
{
}
IStreamSeekable::~IStreamSeekable(void)
{
}

FXD::U64 IStreamSeekable::seek_begin(void)
{
	return seek_to(0, Core::E_SeekOffset::Begin);
}

FXD::U64 IStreamSeekable::seek_end(void)
{
	return seek_to(0, Core::E_SeekOffset::End);
}

FXD::U64 IStreamSeekable::remaining(void)
{
	FXD::U64 nRet = (length() - tell());
	return nRet;
}