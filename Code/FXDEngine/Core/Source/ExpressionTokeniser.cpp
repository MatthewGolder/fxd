// Creator - MatthewGolder
#include "FXDEngine/Core/ExpressionTokeniser.h"

using namespace FXD;
using namespace Core;

static bool consume_whitespace(const Core::String8& strExpression, FXD::U16& nCharIndex)
{
	if (!Core::CharTraits< FXD::UTF8 >::is_white_space(strExpression[nCharIndex]))
	{
		return false;
	}
	while (Core::CharTraits< FXD::UTF8 >::is_white_space(strExpression[nCharIndex]))
	{
		nCharIndex++;
	}
	return true;
}

static bool consume_number(const Core::String8& strExpression, Core::StringBuilder8& strB, FXD::U16& nCharIndex)
{
	if (!Core::CharTraits< FXD::UTF8 >::is_numeric(strExpression[nCharIndex]))
	{
		return false;
	}

	strB << strExpression[nCharIndex++];
	while (true)
	{
		if (strExpression.length() <= nCharIndex)
		{
			break;
		}
		if ((Core::CharTraits< FXD::UTF8 >::is_numeric(strExpression[nCharIndex])) || (Core::CharTraits< FXD::UTF8 >::is_dot(strExpression[nCharIndex])))
		{
			strB << strExpression[nCharIndex++];
		}
		else
		{
			break;
		}
	}
	return true;
}

static bool consume_variable(const Core::String8& strExpression, Core::StringBuilder8& strB, FXD::U16& nCharIndex)
{
	if (!Core::CharTraits< FXD::UTF8 >::is_alpha(strExpression[nCharIndex]))
	{
		return false;
	}
	strB << strExpression[nCharIndex++];

	while (true)
	{
		if (strExpression.length() <= nCharIndex)
		{
			break;
		}
		if ((Core::CharTraits< FXD::UTF8 >::is_alpha_numeric(strExpression[nCharIndex])) || (Core::CharTraits< FXD::UTF8 >::is_underscore(strExpression[nCharIndex])))
		{
			strB << strExpression[nCharIndex++];
		}
		else
		{
			break;
		}
	}
	return true;
}

static bool consumeOperator(const Core::String8& strExpression, FXD::U16& nCharIndex, Core::E_TokenType& eToken)
{
	FXD::UTF8 c1 = strExpression[nCharIndex];
	switch (c1)
	{
		case '(':
		{
			eToken = Core::E_TokenType::LeftParenthesis;
			nCharIndex++;
			return true;
		}
		case ')':
		{
			eToken = Core::E_TokenType::RightParenthesis;
			nCharIndex++;
			return true;
		}
		case '+':
		{
			eToken = Core::E_TokenType::Add;
			nCharIndex++;
			return true;
		}
		case '-':
		{
			eToken = Core::E_TokenType::Minus;
			nCharIndex++;
			return true;
		}
		case '*':
		{
			eToken = Core::E_TokenType::Multiply;
			nCharIndex++;
			return true;
		}
		case '/':
		{
			eToken = Core::E_TokenType::Divide;
			nCharIndex++;
			return true;
		}
		case '&':
		{
			eToken = Core::E_TokenType::And;
			nCharIndex++;
			return true;
		}
		case '|':
		{
			eToken = Core::E_TokenType::Or;
			nCharIndex++;
			return true;
		}
		case '?':
		{
			eToken = Core::E_TokenType::Question;
			nCharIndex++;
			return true;
		}
		case ':':
		{
			eToken = Core::E_TokenType::Colon;
			nCharIndex++;
			return true;
		}
		case '>':
		{
			FXD::UTF8 c2 = strExpression[nCharIndex + 1];
			nCharIndex++;
			if (c2 == '=')
			{
				nCharIndex++;
				eToken = Core::E_TokenType::GreaterEqual;
			}
			else
			{
				eToken = Core::E_TokenType::Greater;
			}
			return true;
		}
		case '<':
		{
			FXD::UTF8 c2 = strExpression[nCharIndex + 1];
			nCharIndex++;
			if (c2 == '=')
			{
				nCharIndex++;
				eToken = Core::E_TokenType::LessEqual;
			}
			else
			{
				eToken = Core::E_TokenType::Less;
			}
			return true;
		}
		case '=':
		{
			FXD::UTF8 c2 = strExpression[nCharIndex + 1];
			if (c2 == '=')
			{
				nCharIndex += 2;
				eToken = Core::E_TokenType::IsEqual;
				return true;
			}
			break;
		}
		case '!':
		{
			FXD::UTF8 c2 = strExpression[nCharIndex + 1];
			nCharIndex++;
			if (c2 == '=')
			{
				nCharIndex++;
				eToken = Core::E_TokenType::NotEqual;
			}
			else
			{
				eToken = Core::E_TokenType::Not;
			}
			return true;
		}
	}
	return false;
}

// ------
// ExpressionTokeniser
// -
// ------
ExpressionTokeniser::ExpressionTokeniser(const Core::String8& strExpression)
{
	// Loop through string.
	FXD::U16 nCharIndex = 0;
	while (nCharIndex < strExpression.length())
	{
		Token token;
		StringBuilder8 strStream;
		if (consume_number(strExpression, strStream, nCharIndex))
		{
			token.m_eType = Core::E_TokenType::Number;
			token.m_strText = strStream.str();
			m_tokens.push_back(token);
		}
		else if (consume_variable(strExpression, strStream, nCharIndex))
		{
			token.m_eType = Core::E_TokenType::Variable;
			token.m_strText = strStream.str();
			m_tokens.push_back(token);
		}
		else if (consumeOperator(strExpression, nCharIndex, token.m_eType))
		{
			m_tokens.push_back(token);
		}
		else if (consume_whitespace(strExpression, nCharIndex))
		{
		}
		else
		{
			PRINT_ASSERT << "Core: Syntax error in expression '" << strExpression << "' at position " << (FXD::U16)nCharIndex;
		}
	}
}

ExpressionTokeniser::~ExpressionTokeniser(void)
{
}

const Container::Vector< ExpressionTokeniser::Token >& ExpressionTokeniser::get_tokens(void) const
{
	return m_tokens;
}

Core::String8 ExpressionTokeniser::token_type_to_string(Core::E_TokenType eToken)
{
	switch (eToken)
	{
		case Core::E_TokenType::Number:
		{
			return UTF_8("#");
		}
		case Core::E_TokenType::Variable:
		{
			return UTF_8("@");
		}
		case Core::E_TokenType::LeftParenthesis:
		{
			return UTF_8("(");
		}
		case Core::E_TokenType::RightParenthesis:
		{
			return UTF_8(")");
		}
		case Core::E_TokenType::Add:
		{
			return UTF_8("+");
		}
		case Core::E_TokenType::Minus:
		{
			return UTF_8("-");
		}
		case Core::E_TokenType::Multiply:
		{
			return UTF_8("*");
		}
		case Core::E_TokenType::Divide:
		{
			return UTF_8("/");
		}
		case Core::E_TokenType::And:
		{
			return UTF_8("&");
		}
		case Core::E_TokenType::Or:
		{
			return UTF_8("|");
		}
		case Core::E_TokenType::Greater:
		{
			return UTF_8(">");
		}
		case Core::E_TokenType::Less:
		{
			return UTF_8("<");
		}
		case Core::E_TokenType::GreaterEqual:
		{
			return UTF_8(">=");
		}
		case Core::E_TokenType::LessEqual:
		{
			return UTF_8("<=");
		}
		case Core::E_TokenType::IsEqual:
		{
			return UTF_8("==");
		}
		case Core::E_TokenType::NotEqual:
		{
			return UTF_8("!=");
		}
		case Core::E_TokenType::Not:
		{
			return UTF_8("!");
		}
		case Core::E_TokenType::Question:
		{
			return UTF_8("?");
		}
		case Core::E_TokenType::Colon:
		{
			return UTF_8(":");
		}
	}
	PRINT_ASSERT << "Core: Unknown token type";
	return UTF_8("");
}