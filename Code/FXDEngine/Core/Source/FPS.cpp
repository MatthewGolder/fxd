// Creator - MatthewGolder
#include "FXDEngine/Core/FPS.h"
#include "FXDEngine/Core/Debugger.h"

using namespace FXD;
using namespace Core;

// ------
// FPS
// -
// ------
FPS::FPS(void)
	: m_nFrameCount(0)
	, m_fTime(0.0f)
{
}

FPS::~FPS(void)
{
}

void FPS::update(const FXD::F32 fFrameTime, const FXD::UTF8* pName)
{
	m_fTime += fFrameTime;
	m_nFrameCount++;
	if (m_fTime > 1.0f)
	{
		if (pName != nullptr)
		{
			PRINT_INFO << "Core: [FPS - " << pName << "] " << m_nFrameCount;
		}
		m_fTime = 0.0f;
		m_nFrameCount = 0;
	}
}