// Creator - MatthewGolder
#include "FXDEngine/Core/Expression.h"
#include "FXDEngine/Core/ExpressionTree.h"

using namespace FXD;
using namespace Core;

// ------
// Stack
// - Expression evaluation stack.
// ------
class Stack
{
public:
	Stack(void)
		: m_nOffset(0)
	{
	}
	~Stack(void)
	{
	}

	template < typename T >
	void push(const T& t)
	{
		PRINT_COND_ASSERT(((m_nOffset + sizeof(T)) < kStackSize), "Core: Stack overflow");
		*((T*)&m_data[m_nOffset]) = t;
		m_nOffset += sizeof(T);
	}
	template < typename T >
	T pop(void)
	{
		PRINT_COND_ASSERT((m_nOffset >= sizeof(T)), "Core: Stack underflow");
		m_nOffset -= sizeof(T);
		return *((T*)&m_data[m_nOffset]);
	}

	static const FXD::S32 kStackSize = 1024;
	FXD::S8 m_data[1024];
	FXD::S32 m_nOffset;
};

// ------
// CodeList
// - Parser for encoded expression data.
// ------
class CodeList
{
public:
	CodeList(void* pData, FXD::S32 nSize)
		: m_pData((FXD::S8*)pData)
		, m_nSize(nSize)
	{
	}
	~CodeList(void)
	{
	}

	inline bool eof(void) const
	{
		return (m_nSize == 0);
	}
	template < typename T >
	T pull(void)
	{
		PRINT_COND_ASSERT((m_nSize >= sizeof(T)), "Core: Reading off end of code list");
		T t = *(T*)m_pData;
		m_pData += sizeof(T);
		m_nSize -= sizeof(T);
		return t;
	}

	FXD::S8* m_pData;
	FXD::S32 m_nSize;
};

// ------
// Expression
// -
// ------
const Core::String8 Expression::type_to_string(const Core::E_ExpressionType eType)
{
	switch (eType)
	{
		case Core::E_ExpressionType::Float:
		{
			return UTF_8("Float");
		}
		case Core::E_ExpressionType::Bool:
		{
			return UTF_8("Bool");
		}
	}
	PRINT_ASSERT << "Core: Unknown expression type - " << FXD::STD::to_underlying(eType);
	return UTF_8("");
}

// Parse an expression.
void Expression::parse(const Core::String8& strExpr, const Core::IVariableProvider& vars)
{
	ExpressionTokeniser tokens(strExpr);
	ExpressionTree tree(tokens);
	tree.build_compiled_expression(*this, vars);
}

// Set from a compiled data blob.
void Expression::set_from_compiled(const Memory::MemHandle& memHandle, Core::E_ExpressionType eType)
{
	m_data = memHandle;
	m_eType = eType;
	Memory::MemHandle::LockGuard memLock(m_data);
	m_pData = memLock.get_mem();
}

// Evaluate the expression.
void Expression::evaluate(const Core::IVariableProvider& vars, void* pData) const
{
	// Stack on which to evaluate.
	Stack stack;

	// Code list containing the coded expression.
	CodeList codeList(m_pData, m_data.get_size());

	// Loop reading from code list.
	while (!codeList.eof())
	{
		Core::E_ExpressionCode ec = codeList.pull< Core::E_ExpressionCode >();
		switch (ec)
		{
			case Core::E_ExpressionCode::FloatLiteral:
			{
				stack.push(codeList.pull< FXD::F32 >());
				break;
			}
			case Core::E_ExpressionCode::BoolLiteral:
			{
				stack.push(codeList.pull< bool >());
				break;
			}
			case Core::E_ExpressionCode::FloatVariable:
			{
				FXD::F32 fVal = 0.0f;
				vars.get_variable_data(codeList.pull< FXD::S32 >(), &fVal);
				stack.push(fVal);
				break;
			}
			case Core::E_ExpressionCode::BoolVariable:
			{
				bool bVal = false;
				vars.get_variable_data(codeList.pull< FXD::S32 >(), &bVal);
				stack.push(bVal);
				break;
			}
			case Core::E_ExpressionCode::FloatAdd:
			{
				stack.push(stack.pop< FXD::F32 >() + stack.pop< FXD::F32 >());
				break;
			}
			case Core::E_ExpressionCode::FloatSubtract:
			{
				FXD::F32 f2 = stack.pop< FXD::F32 >();
				FXD::F32 f1 = stack.pop< FXD::F32 >();
				stack.push(f1 - f2);
				break;
			}
			case Core::E_ExpressionCode::FloatMultiply:
			{
				stack.push(stack.pop< FXD::F32 >() * stack.pop< FXD::F32 >());
				break;
			}
			case Core::E_ExpressionCode::FloatDivide:
			{
				FXD::F32 f2 = stack.pop< FXD::F32 >();
				FXD::F32 f1 = stack.pop< FXD::F32 >();
				stack.push(f1 / f2);
				break;
			}
			case Core::E_ExpressionCode::FloatGreater:
			{
				FXD::F32 f2 = stack.pop< FXD::F32 >();
				FXD::F32 f1 = stack.pop< FXD::F32 >();
				stack.push(f1 > f2);
				break;
			}
			case Core::E_ExpressionCode::FloatGreaterEqual:
			{
				FXD::F32 f2 = stack.pop< FXD::F32 >();
				FXD::F32 f1 = stack.pop< FXD::F32 >();
				stack.push(f1 >= f2);
				break;
			}
			case Core::E_ExpressionCode::FloatLess:
			{
				FXD::F32 f2 = stack.pop< FXD::F32 >();
				FXD::F32 f1 = stack.pop< FXD::F32 >();
				stack.push(f1 < f2);
				break;
			}
			case Core::E_ExpressionCode::FloatLessEqual:
			{
				FXD::F32 f2 = stack.pop< FXD::F32 >();
				FXD::F32 f1 = stack.pop< FXD::F32 >();
				stack.push(f1 <= f2);
				break;
			}
			case Core::E_ExpressionCode::FloatNegate:
			{
				stack.push(-stack.pop< FXD::F32 >());
				break;
			}
			case Core::E_ExpressionCode::BoolNot:
			{
				stack.push(!stack.pop< bool >());
				break;
			}
			case Core::E_ExpressionCode::BoolAdd:
			{
				bool b2 = stack.pop< bool >();
				bool b1 = stack.pop< bool >();
				stack.push(b1 && b2);
				break;
			}
			case Core::E_ExpressionCode::BoolOr:
			{
				bool b2 = stack.pop< bool >();
				bool b1 = stack.pop< bool >();
				stack.push(b1 || b2);
				break;
			}
			case Core::E_ExpressionCode::ConvertBoolFloat:
			{
				stack.push(stack.pop< bool >() ? 1.0f : 0.0f);
				break;
			}
			default:
			{
				PRINT_ASSERT << "Core: Unhandled operation!";
				break;
			}
		}
	}

	// Now return the result.
	switch (m_eType)
	{
		case Core::E_ExpressionType::Float:
		{
			*(FXD::F32*)pData = stack.pop< FXD::F32 >();
			break;
		}
		case Core::E_ExpressionType::Bool:
		{
			*(bool*)pData = stack.pop< bool >();
			break;
		}
		default:
		{
			PRINT_ASSERT << "Core: Unsupported expression return type - " << Expression::type_to_string(m_eType);
			break;
		}
	}
	// Ensure the stack is now empty.
	PRINT_COND_ASSERT((stack.m_nOffset == 0), "Core: Stack not empty after expression evaluation");
}