// Creator - MatthewGolder
#include "FXDEngine/Core/SerializeInBinary.h"
#include "FXDEngine/Core/StreamIn.h"

using namespace FXD;
using namespace Core;

// ------
// ISerializeInBinary
// -
// ------
ISerializeInBinary::ISerializeInBinary(void)
{
}
ISerializeInBinary::~ISerializeInBinary(void)
{
}
void ISerializeInBinary::_pass(FXD::UTF8* pVal, IStreamIn* pStream)
{
	FXD::U32 nLen = 0;
	pStream->read_size(&nLen, sizeof(FXD::U32));
	pStream->read_size(pVal, (nLen * sizeof(FXD::UTF8)));
}
void ISerializeInBinary::_pass(FXD::UTF16* pVal, IStreamIn* pStream)
{
	FXD::U32 nLen = 0;
	pStream->read_size(&nLen, sizeof(FXD::U32));
	pStream->read_size(pVal, (nLen * sizeof(FXD::UTF16)));
}
void ISerializeInBinary::_pass(bool& val, IStreamIn* pStream)
{
	pStream->read_size(&val, sizeof(bool));
}
void ISerializeInBinary::_pass(FXD::UTF8& val, IStreamIn* pStream)
{
	pStream->read_size(&val, sizeof(FXD::UTF8));
}
void ISerializeInBinary::_pass(FXD::S8& nVal, IStreamIn* pStream)
{
	pStream->read_size(&nVal, sizeof(FXD::S8));
}
void ISerializeInBinary::_pass(FXD::U8& nVal, IStreamIn* pStream)
{
	pStream->read_size(&nVal, sizeof(FXD::U8));
}
void ISerializeInBinary::_pass(FXD::S16& nVal, IStreamIn* pStream)
{
	pStream->read_size(&nVal, sizeof(FXD::S16));
}
void ISerializeInBinary::_pass(FXD::U16& nVal, IStreamIn* pStream)
{
	pStream->read_size(&nVal, sizeof(FXD::U16));
}
void ISerializeInBinary::_pass(FXD::S32& nVal, IStreamIn* pStream)
{
	pStream->read_size(&nVal, sizeof(FXD::S32));
}
void ISerializeInBinary::_pass(FXD::U32& nVal, IStreamIn* pStream)
{
	pStream->read_size(&nVal, sizeof(FXD::U32));
}
void ISerializeInBinary::_pass(FXD::S64& nVal, IStreamIn* pStream)
{
	pStream->read_size(&nVal, sizeof(FXD::S64));
}
void ISerializeInBinary::_pass(FXD::U64& nVal, IStreamIn* pStream)
{
	pStream->read_size(&nVal, sizeof(FXD::U64));
}
void ISerializeInBinary::_pass(FXD::F32& fVal, IStreamIn* pStream)
{
	pStream->read_size(&fVal, sizeof(FXD::F32));
}
void ISerializeInBinary::_pass(FXD::F64& fVal, IStreamIn* pStream)
{
	pStream->read_size(&fVal, sizeof(FXD::F64));
}