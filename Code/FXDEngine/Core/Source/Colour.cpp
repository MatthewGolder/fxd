// Creator - MatthewGolder
#include "FXDEngine/Core/Colour.h"
#include "FXDEngine/Core/StreamOut.h"
#include "FXDEngine/Math/Math.h"

using namespace FXD;
using namespace Core;

// ------
// ColourRGB
// -
// ------
const ColourRGB ColourRGB::AliceBlue = ColourRGB(0.941176534f, 0.972549081f, 1.000000000f);
const ColourRGB ColourRGB::AntiqueWhite = ColourRGB(0.980392218f, 0.921568692f, 0.843137324f);
const ColourRGB ColourRGB::Aqua = ColourRGB(0.000000000f, 1.000000000f, 1.000000000f);
const ColourRGB ColourRGB::Aquamarine = ColourRGB(0.498039246f, 1.000000000f, 0.831372619f);
const ColourRGB ColourRGB::Azure = ColourRGB(0.941176534f, 1.000000000f, 1.000000000f);
const ColourRGB ColourRGB::Beige = ColourRGB(0.960784376f, 0.960784376f, 0.862745166f);
const ColourRGB ColourRGB::Bisque = ColourRGB(1.000000000f, 0.894117713f, 0.768627524f);
const ColourRGB ColourRGB::Black = ColourRGB(0.000000000f, 0.000000000f, 0.000000000f);
const ColourRGB ColourRGB::BlanchedAlmond = ColourRGB(1.000000000f, 0.921568692f, 0.803921640f);
const ColourRGB ColourRGB::Blue = ColourRGB(0.000000000f, 0.000000000f, 1.000000000f);
const ColourRGB ColourRGB::BlueViolet = ColourRGB(0.541176498f, 0.168627456f, 0.886274576f);
const ColourRGB ColourRGB::Brown = ColourRGB(0.647058845f, 0.164705887f, 0.164705887f);
const ColourRGB ColourRGB::BurlyWood = ColourRGB(0.870588303f, 0.721568644f, 0.529411793f);
const ColourRGB ColourRGB::CadetBlue = ColourRGB(0.372549027f, 0.619607866f, 0.627451003f);
const ColourRGB ColourRGB::Chartreuse = ColourRGB(0.498039246f, 1.000000000f, 0.000000000f);
const ColourRGB ColourRGB::Chocolate = ColourRGB(0.823529482f, 0.411764741f, 0.117647067f);
const ColourRGB ColourRGB::Coral = ColourRGB(1.000000000f, 0.498039246f, 0.313725501f);
const ColourRGB ColourRGB::CornflowerBlue = ColourRGB(0.392156899f, 0.584313750f, 0.929411829f);
const ColourRGB ColourRGB::Cornsilk = ColourRGB(1.000000000f, 0.972549081f, 0.862745166f);
const ColourRGB ColourRGB::Crimson = ColourRGB(0.862745166f, 0.078431375f, 0.235294133f);
const ColourRGB ColourRGB::Cyan = ColourRGB(0.000000000f, 1.000000000f, 1.000000000f);
const ColourRGB ColourRGB::DarkBlue = ColourRGB(0.000000000f, 0.000000000f, 0.545098066f);
const ColourRGB ColourRGB::DarkCyan = ColourRGB(0.000000000f, 0.545098066f, 0.545098066f);
const ColourRGB ColourRGB::DarkGoldenrod = ColourRGB(0.721568644f, 0.525490224f, 0.043137256f);
const ColourRGB ColourRGB::DarkGray = ColourRGB(0.662745118f, 0.662745118f, 0.662745118f);
const ColourRGB ColourRGB::DarkGreen = ColourRGB(0.000000000f, 0.392156899f, 0.000000000f);
const ColourRGB ColourRGB::DarkKhaki = ColourRGB(0.741176486f, 0.717647076f, 0.419607878f);
const ColourRGB ColourRGB::DarkMagenta = ColourRGB(0.545098066f, 0.000000000f, 0.545098066f);
const ColourRGB ColourRGB::DarkOliveGreen = ColourRGB(0.333333343f, 0.419607878f, 0.184313729f);
const ColourRGB ColourRGB::DarkOrange = ColourRGB(1.000000000f, 0.549019635f, 0.000000000f);
const ColourRGB ColourRGB::DarkOrchid = ColourRGB(0.600000024f, 0.196078449f, 0.800000072f);
const ColourRGB ColourRGB::DarkRed = ColourRGB(0.545098066f, 0.000000000f, 0.000000000f);
const ColourRGB ColourRGB::DarkSalmon = ColourRGB(0.913725555f, 0.588235319f, 0.478431404f);
const ColourRGB ColourRGB::DarkSeaGreen = ColourRGB(0.560784340f, 0.737254918f, 0.545098066f);
const ColourRGB ColourRGB::DarkSlateBlue = ColourRGB(0.282352954f, 0.239215702f, 0.545098066f);
const ColourRGB ColourRGB::DarkSlateGray = ColourRGB(0.184313729f, 0.309803933f, 0.309803933f);
const ColourRGB ColourRGB::DarkTurquoise = ColourRGB(0.000000000f, 0.807843208f, 0.819607913f);
const ColourRGB ColourRGB::DarkViolet = ColourRGB(0.580392182f, 0.000000000f, 0.827451050f);
const ColourRGB ColourRGB::DeepPink = ColourRGB(1.000000000f, 0.078431375f, 0.576470613f);
const ColourRGB ColourRGB::DeepSkyBlue = ColourRGB(0.000000000f, 0.749019623f, 1.000000000f);
const ColourRGB ColourRGB::DimGray = ColourRGB(0.411764741f, 0.411764741f, 0.411764741f);
const ColourRGB ColourRGB::DodgerBlue = ColourRGB(0.117647067f, 0.564705908f, 1.000000000f);
const ColourRGB ColourRGB::Firebrick = ColourRGB(0.698039234f, 0.133333340f, 0.133333340f);
const ColourRGB ColourRGB::FloralWhite = ColourRGB(1.000000000f, 0.980392218f, 0.941176534f);
const ColourRGB ColourRGB::ForestGreen = ColourRGB(0.133333340f, 0.545098066f, 0.133333340f);
const ColourRGB ColourRGB::Fuchsia = ColourRGB(1.000000000f, 0.000000000f, 1.000000000f);
const ColourRGB ColourRGB::Gainsboro = ColourRGB(0.862745166f, 0.862745166f, 0.862745166f);
const ColourRGB ColourRGB::GhostWhite = ColourRGB(0.972549081f, 0.972549081f, 1.000000000f);
const ColourRGB ColourRGB::Gold = ColourRGB(1.000000000f, 0.843137324f, 0.000000000f);
const ColourRGB ColourRGB::Goldenrod = ColourRGB(0.854902029f, 0.647058845f, 0.125490203f);
const ColourRGB ColourRGB::Gray = ColourRGB(0.501960814f, 0.501960814f, 0.501960814f);
const ColourRGB ColourRGB::Green = ColourRGB(0.000000000f, 0.501960814f, 0.000000000f);
const ColourRGB ColourRGB::GreenYellow = ColourRGB(0.678431392f, 1.000000000f, 0.184313729f);
const ColourRGB ColourRGB::Honeydew = ColourRGB(0.941176534f, 1.000000000f, 0.941176534f);
const ColourRGB ColourRGB::HotPink = ColourRGB(1.000000000f, 0.411764741f, 0.705882370f);
const ColourRGB ColourRGB::IndianRed = ColourRGB(0.803921640f, 0.360784322f, 0.360784322f);
const ColourRGB ColourRGB::Indigo = ColourRGB(0.294117659f, 0.000000000f, 0.509803951f);
const ColourRGB ColourRGB::Ivory = ColourRGB(1.000000000f, 1.000000000f, 0.941176534f);
const ColourRGB ColourRGB::Khaki = ColourRGB(0.941176534f, 0.901960850f, 0.549019635f);
const ColourRGB ColourRGB::Lavender = ColourRGB(0.901960850f, 0.901960850f, 0.980392218f);
const ColourRGB ColourRGB::LavenderBlush = ColourRGB(1.000000000f, 0.941176534f, 0.960784376f);
const ColourRGB ColourRGB::LawnGreen = ColourRGB(0.486274540f, 0.988235354f, 0.000000000f);
const ColourRGB ColourRGB::LemonChiffon = ColourRGB(1.000000000f, 0.980392218f, 0.803921640f);
const ColourRGB ColourRGB::LightBlue = ColourRGB(0.678431392f, 0.847058892f, 0.901960850f);
const ColourRGB ColourRGB::LightCoral = ColourRGB(0.941176534f, 0.501960814f, 0.501960814f);
const ColourRGB ColourRGB::LightCyan = ColourRGB(0.878431439f, 1.000000000f, 1.000000000f);
const ColourRGB ColourRGB::LightGoldenrodYellow = ColourRGB(0.980392218f, 0.980392218f, 0.823529482f);
const ColourRGB ColourRGB::LightGreen = ColourRGB(0.564705908f, 0.933333397f, 0.564705908f);
const ColourRGB ColourRGB::LightGray = ColourRGB(0.827451050f, 0.827451050f, 0.827451050f);
const ColourRGB ColourRGB::LightPink = ColourRGB(1.000000000f, 0.713725507f, 0.756862819f);
const ColourRGB ColourRGB::LightSalmon = ColourRGB(1.000000000f, 0.627451003f, 0.478431404f);
const ColourRGB ColourRGB::LightSeaGreen = ColourRGB(0.125490203f, 0.698039234f, 0.666666687f);
const ColourRGB ColourRGB::LightSkyBlue = ColourRGB(0.529411793f, 0.807843208f, 0.980392218f);
const ColourRGB ColourRGB::LightSlateGray = ColourRGB(0.466666698f, 0.533333361f, 0.600000024f);
const ColourRGB ColourRGB::LightSteelBlue = ColourRGB(0.690196097f, 0.768627524f, 0.870588303f);
const ColourRGB ColourRGB::LightYellow = ColourRGB(1.000000000f, 1.000000000f, 0.878431439f);
const ColourRGB ColourRGB::Lime = ColourRGB(0.000000000f, 1.000000000f, 0.000000000f);
const ColourRGB ColourRGB::LimeGreen = ColourRGB(0.196078449f, 0.803921640f, 0.196078449f);
const ColourRGB ColourRGB::Linen = ColourRGB(0.980392218f, 0.941176534f, 0.901960850f);
const ColourRGB ColourRGB::Magenta = ColourRGB(1.000000000f, 0.000000000f, 1.000000000f);
const ColourRGB ColourRGB::Maroon = ColourRGB(0.501960814f, 0.000000000f, 0.000000000f);
const ColourRGB ColourRGB::MediumAquamarine = ColourRGB(0.400000036f, 0.803921640f, 0.666666687f);
const ColourRGB ColourRGB::MediumBlue = ColourRGB(0.000000000f, 0.000000000f, 0.803921640f);
const ColourRGB ColourRGB::MediumOrchid = ColourRGB(0.729411781f, 0.333333343f, 0.827451050f);
const ColourRGB ColourRGB::MediumPurple = ColourRGB(0.576470613f, 0.439215720f, 0.858823597f);
const ColourRGB ColourRGB::MediumSeaGreen = ColourRGB(0.235294133f, 0.701960802f, 0.443137288f);
const ColourRGB ColourRGB::MediumSlateBlue = ColourRGB(0.482352972f, 0.407843173f, 0.933333397f);
const ColourRGB ColourRGB::MediumSpringGreen = ColourRGB(0.000000000f, 0.980392218f, 0.603921592f);
const ColourRGB ColourRGB::MediumTurquoise = ColourRGB(0.282352954f, 0.819607913f, 0.800000072f);
const ColourRGB ColourRGB::MediumVioletRed = ColourRGB(0.780392230f, 0.082352944f, 0.521568656f);
const ColourRGB ColourRGB::MidnightBlue = ColourRGB(0.098039225f, 0.098039225f, 0.439215720f);
const ColourRGB ColourRGB::MintCream = ColourRGB(0.960784376f, 1.000000000f, 0.980392218f);
const ColourRGB ColourRGB::MistyRose = ColourRGB(1.000000000f, 0.894117713f, 0.882353008f);
const ColourRGB ColourRGB::Moccasin = ColourRGB(1.000000000f, 0.894117713f, 0.709803939f);
const ColourRGB ColourRGB::NavajoWhite = ColourRGB(1.000000000f, 0.870588303f, 0.678431392f);
const ColourRGB ColourRGB::Navy = ColourRGB(0.000000000f, 0.000000000f, 0.501960814f);
const ColourRGB ColourRGB::OldLace = ColourRGB(0.992156923f, 0.960784376f, 0.901960850f);
const ColourRGB ColourRGB::Olive = ColourRGB(0.501960814f, 0.501960814f, 0.000000000f);
const ColourRGB ColourRGB::OliveDrab = ColourRGB(0.419607878f, 0.556862772f, 0.137254909f);
const ColourRGB ColourRGB::Orange = ColourRGB(1.000000000f, 0.647058845f, 0.000000000f);
const ColourRGB ColourRGB::OrangeRed = ColourRGB(1.000000000f, 0.270588249f, 0.000000000f);
const ColourRGB ColourRGB::Orchid = ColourRGB(0.854902029f, 0.439215720f, 0.839215755f);
const ColourRGB ColourRGB::PaleGoldenrod = ColourRGB(0.933333397f, 0.909803987f, 0.666666687f);
const ColourRGB ColourRGB::PaleGreen = ColourRGB(0.596078455f, 0.984313786f, 0.596078455f);
const ColourRGB ColourRGB::PaleTurquoise = ColourRGB(0.686274529f, 0.933333397f, 0.933333397f);
const ColourRGB ColourRGB::PaleVioletRed = ColourRGB(0.858823597f, 0.439215720f, 0.576470613f);
const ColourRGB ColourRGB::PapayaWhip = ColourRGB(1.000000000f, 0.937254965f, 0.835294187f);
const ColourRGB ColourRGB::PeachPuff = ColourRGB(1.000000000f, 0.854902029f, 0.725490212f);
const ColourRGB ColourRGB::Peru = ColourRGB(0.803921640f, 0.521568656f, 0.247058839f);
const ColourRGB ColourRGB::Pink = ColourRGB(1.000000000f, 0.752941251f, 0.796078503f);
const ColourRGB ColourRGB::Plum = ColourRGB(0.866666734f, 0.627451003f, 0.866666734f);
const ColourRGB ColourRGB::PowderBlue = ColourRGB(0.690196097f, 0.878431439f, 0.901960850f);
const ColourRGB ColourRGB::Purple = ColourRGB(0.501960814f, 0.000000000f, 0.501960814f);
const ColourRGB ColourRGB::Red = ColourRGB(1.000000000f, 0.000000000f, 0.000000000f);
const ColourRGB ColourRGB::RosyBrown = ColourRGB(0.737254918f, 0.560784340f, 0.560784340f);
const ColourRGB ColourRGB::RoyalBlue = ColourRGB(0.254901975f, 0.411764741f, 0.882353008f);
const ColourRGB ColourRGB::SaddleBrown = ColourRGB(0.545098066f, 0.270588249f, 0.074509807f);
const ColourRGB ColourRGB::Salmon = ColourRGB(0.980392218f, 0.501960814f, 0.447058856f);
const ColourRGB ColourRGB::SandyBrown = ColourRGB(0.956862807f, 0.643137276f, 0.376470625f);
const ColourRGB ColourRGB::SeaGreen = ColourRGB(0.180392161f, 0.545098066f, 0.341176480f);
const ColourRGB ColourRGB::SeaShell = ColourRGB(1.000000000f, 0.960784376f, 0.933333397f);
const ColourRGB ColourRGB::Sienna = ColourRGB(0.627451003f, 0.321568638f, 0.176470593f);
const ColourRGB ColourRGB::Silver = ColourRGB(0.752941251f, 0.752941251f, 0.752941251f);
const ColourRGB ColourRGB::SkyBlue = ColourRGB(0.529411793f, 0.807843208f, 0.921568692f);
const ColourRGB ColourRGB::SlateBlue = ColourRGB(0.415686309f, 0.352941185f, 0.803921640f);
const ColourRGB ColourRGB::SlateGray = ColourRGB(0.439215720f, 0.501960814f, 0.564705908f);
const ColourRGB ColourRGB::Snow = ColourRGB(1.000000000f, 0.980392218f, 0.980392218f);
const ColourRGB ColourRGB::SpringGreen = ColourRGB(0.000000000f, 1.000000000f, 0.498039246f);
const ColourRGB ColourRGB::SteelBlue = ColourRGB(0.274509817f, 0.509803951f, 0.705882370f);
const ColourRGB ColourRGB::Tan = ColourRGB(0.823529482f, 0.705882370f, 0.549019635f);
const ColourRGB ColourRGB::Teal = ColourRGB(0.000000000f, 0.501960814f, 0.501960814f);
const ColourRGB ColourRGB::Thistle = ColourRGB(0.847058892f, 0.749019623f, 0.847058892f);
const ColourRGB ColourRGB::Tomato = ColourRGB(1.000000000f, 0.388235331f, 0.278431386f);
const ColourRGB ColourRGB::Turquoise = ColourRGB(0.250980407f, 0.878431439f, 0.815686345f);
const ColourRGB ColourRGB::Violet = ColourRGB(0.933333397f, 0.509803951f, 0.933333397f);
const ColourRGB ColourRGB::Wheat = ColourRGB(0.960784376f, 0.870588303f, 0.701960802f);
const ColourRGB ColourRGB::White = ColourRGB(1.000000000f, 1.000000000f, 1.000000000f);
const ColourRGB ColourRGB::WhiteSmoke = ColourRGB(0.960784376f, 0.960784376f, 0.960784376f);
const ColourRGB ColourRGB::Yellow = ColourRGB(1.000000000f, 1.000000000f, 0.000000000f);
const ColourRGB ColourRGB::YellowGreen = ColourRGB(0.603921592f, 0.803921640f, 0.196078449f);

ColourRGB ColourRGB::from_hex(FXD::U8 r, FXD::U8 g, FXD::U8 b)
{
	ColourRGB retVal(	((FXD::F32)r / (FXD::F32)Math::NumericLimits< FXD::U8 > ::max()),
						((FXD::F32)g / (FXD::F32)Math::NumericLimits< FXD::U8 > ::max()),
						((FXD::F32)b / (FXD::F32)Math::NumericLimits< FXD::U8 > ::max()));
	return retVal;
}

ColourRGB ColourRGB::from_string(const Core::String8& str, FXD::UTF8 delim)
{
	Container::Vector< FXD::F32 > vec = str.to_F32_array(delim);
	ColourRGB retVal(vec[0], vec[1], vec[2]);
	return retVal;
}

ColourRGB ColourRGB::random(void)
{
	return ColourRGB(Math::RandomRange(0.0f, 1.0f), Math::RandomRange(0.0f, 1.0f), Math::RandomRange(0.0f, 1.0f));
}

inline Core::IStreamOut& FXD::Core::operator<<(Core::IStreamOut& ostr, ColourRGB const& rhs)
{
	return ostr << rhs.getR() << UTF_8(" ") << rhs.getG() << UTF_8(" ") << rhs.getB();
}

// ------
// ColourRGBA
// -
// ------
const ColourRGBA ColourRGBA::AliceBlue = ColourRGBA(0.941176534f, 0.972549081f, 1.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::AntiqueWhite = ColourRGBA(0.980392218f, 0.921568692f, 0.843137324f, 1.000000000f);
const ColourRGBA ColourRGBA::Aqua = ColourRGBA(0.000000000f, 1.000000000f, 1.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::Aquamarine = ColourRGBA(0.498039246f, 1.000000000f, 0.831372619f, 1.000000000f);
const ColourRGBA ColourRGBA::Azure = ColourRGBA(0.941176534f, 1.000000000f, 1.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::Beige = ColourRGBA(0.960784376f, 0.960784376f, 0.862745166f, 1.000000000f);
const ColourRGBA ColourRGBA::Bisque = ColourRGBA(1.000000000f, 0.894117713f, 0.768627524f, 1.000000000f);
const ColourRGBA ColourRGBA::Black = ColourRGBA(0.000000000f, 0.000000000f, 0.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::BlanchedAlmond = ColourRGBA(1.000000000f, 0.921568692f, 0.803921640f, 1.000000000f);
const ColourRGBA ColourRGBA::Blue = ColourRGBA(0.000000000f, 0.000000000f, 1.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::BlueViolet = ColourRGBA(0.541176498f, 0.168627456f, 0.886274576f, 1.000000000f);
const ColourRGBA ColourRGBA::Brown = ColourRGBA(0.647058845f, 0.164705887f, 0.164705887f, 1.000000000f);
const ColourRGBA ColourRGBA::BurlyWood = ColourRGBA(0.870588303f, 0.721568644f, 0.529411793f, 1.000000000f);
const ColourRGBA ColourRGBA::CadetBlue = ColourRGBA(0.372549027f, 0.619607866f, 0.627451003f, 1.000000000f);
const ColourRGBA ColourRGBA::Chartreuse = ColourRGBA(0.498039246f, 1.000000000f, 0.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::Chocolate = ColourRGBA(0.823529482f, 0.411764741f, 0.117647067f, 1.000000000f);
const ColourRGBA ColourRGBA::Coral = ColourRGBA(1.000000000f, 0.498039246f, 0.313725501f, 1.000000000f);
const ColourRGBA ColourRGBA::CornflowerBlue = ColourRGBA(0.392156899f, 0.584313750f, 0.929411829f, 1.000000000f);
const ColourRGBA ColourRGBA::Cornsilk = ColourRGBA(1.000000000f, 0.972549081f, 0.862745166f, 1.000000000f);
const ColourRGBA ColourRGBA::Crimson = ColourRGBA(0.862745166f, 0.078431375f, 0.235294133f, 1.000000000f);
const ColourRGBA ColourRGBA::Cyan = ColourRGBA(0.000000000f, 1.000000000f, 1.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::DarkBlue = ColourRGBA(0.000000000f, 0.000000000f, 0.545098066f, 1.000000000f);
const ColourRGBA ColourRGBA::DarkCyan = ColourRGBA(0.000000000f, 0.545098066f, 0.545098066f, 1.000000000f);
const ColourRGBA ColourRGBA::DarkGoldenrod = ColourRGBA(0.721568644f, 0.525490224f, 0.043137256f, 1.000000000f);
const ColourRGBA ColourRGBA::DarkGray = ColourRGBA(0.662745118f, 0.662745118f, 0.662745118f, 1.000000000f);
const ColourRGBA ColourRGBA::DarkGreen = ColourRGBA(0.000000000f, 0.392156899f, 0.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::DarkKhaki = ColourRGBA(0.741176486f, 0.717647076f, 0.419607878f, 1.000000000f);
const ColourRGBA ColourRGBA::DarkMagenta = ColourRGBA(0.545098066f, 0.000000000f, 0.545098066f, 1.000000000f);
const ColourRGBA ColourRGBA::DarkOliveGreen = ColourRGBA(0.333333343f, 0.419607878f, 0.184313729f, 1.000000000f);
const ColourRGBA ColourRGBA::DarkOrange = ColourRGBA(1.000000000f, 0.549019635f, 0.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::DarkOrchid = ColourRGBA(0.600000024f, 0.196078449f, 0.800000072f, 1.000000000f);
const ColourRGBA ColourRGBA::DarkRed = ColourRGBA(0.545098066f, 0.000000000f, 0.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::DarkSalmon = ColourRGBA(0.913725555f, 0.588235319f, 0.478431404f, 1.000000000f);
const ColourRGBA ColourRGBA::DarkSeaGreen = ColourRGBA(0.560784340f, 0.737254918f, 0.545098066f, 1.000000000f);
const ColourRGBA ColourRGBA::DarkSlateBlue = ColourRGBA(0.282352954f, 0.239215702f, 0.545098066f, 1.000000000f);
const ColourRGBA ColourRGBA::DarkSlateGray = ColourRGBA(0.184313729f, 0.309803933f, 0.309803933f, 1.000000000f);
const ColourRGBA ColourRGBA::DarkTurquoise = ColourRGBA(0.000000000f, 0.807843208f, 0.819607913f, 1.000000000f);
const ColourRGBA ColourRGBA::DarkViolet = ColourRGBA(0.580392182f, 0.000000000f, 0.827451050f, 1.000000000f);
const ColourRGBA ColourRGBA::DeepPink = ColourRGBA(1.000000000f, 0.078431375f, 0.576470613f, 1.000000000f);
const ColourRGBA ColourRGBA::DeepSkyBlue = ColourRGBA(0.000000000f, 0.749019623f, 1.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::DimGray = ColourRGBA(0.411764741f, 0.411764741f, 0.411764741f, 1.000000000f);
const ColourRGBA ColourRGBA::DodgerBlue = ColourRGBA(0.117647067f, 0.564705908f, 1.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::Firebrick = ColourRGBA(0.698039234f, 0.133333340f, 0.133333340f, 1.000000000f);
const ColourRGBA ColourRGBA::FloralWhite = ColourRGBA(1.000000000f, 0.980392218f, 0.941176534f, 1.000000000f);
const ColourRGBA ColourRGBA::ForestGreen = ColourRGBA(0.133333340f, 0.545098066f, 0.133333340f, 1.000000000f);
const ColourRGBA ColourRGBA::Fuchsia = ColourRGBA(1.000000000f, 0.000000000f, 1.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::Gainsboro = ColourRGBA(0.862745166f, 0.862745166f, 0.862745166f, 1.000000000f);
const ColourRGBA ColourRGBA::GhostWhite = ColourRGBA(0.972549081f, 0.972549081f, 1.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::Gold = ColourRGBA(1.000000000f, 0.843137324f, 0.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::Goldenrod = ColourRGBA(0.854902029f, 0.647058845f, 0.125490203f, 1.000000000f);
const ColourRGBA ColourRGBA::Gray = ColourRGBA(0.501960814f, 0.501960814f, 0.501960814f, 1.000000000f);
const ColourRGBA ColourRGBA::Green = ColourRGBA(0.000000000f, 0.501960814f, 0.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::GreenYellow = ColourRGBA(0.678431392f, 1.000000000f, 0.184313729f, 1.000000000f);
const ColourRGBA ColourRGBA::Honeydew = ColourRGBA(0.941176534f, 1.000000000f, 0.941176534f, 1.000000000f);
const ColourRGBA ColourRGBA::HotPink = ColourRGBA(1.000000000f, 0.411764741f, 0.705882370f, 1.000000000f);
const ColourRGBA ColourRGBA::IndianRed = ColourRGBA(0.803921640f, 0.360784322f, 0.360784322f, 1.000000000f);
const ColourRGBA ColourRGBA::Indigo = ColourRGBA(0.294117659f, 0.000000000f, 0.509803951f, 1.000000000f);
const ColourRGBA ColourRGBA::Ivory = ColourRGBA(1.000000000f, 1.000000000f, 0.941176534f, 1.000000000f);
const ColourRGBA ColourRGBA::Khaki = ColourRGBA(0.941176534f, 0.901960850f, 0.549019635f, 1.000000000f);
const ColourRGBA ColourRGBA::Lavender = ColourRGBA(0.901960850f, 0.901960850f, 0.980392218f, 1.000000000f);
const ColourRGBA ColourRGBA::LavenderBlush = ColourRGBA(1.000000000f, 0.941176534f, 0.960784376f, 1.000000000f);
const ColourRGBA ColourRGBA::LawnGreen = ColourRGBA(0.486274540f, 0.988235354f, 0.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::LemonChiffon = ColourRGBA(1.000000000f, 0.980392218f, 0.803921640f, 1.000000000f);
const ColourRGBA ColourRGBA::LightBlue = ColourRGBA(0.678431392f, 0.847058892f, 0.901960850f, 1.000000000f);
const ColourRGBA ColourRGBA::LightCoral = ColourRGBA(0.941176534f, 0.501960814f, 0.501960814f, 1.000000000f);
const ColourRGBA ColourRGBA::LightCyan = ColourRGBA(0.878431439f, 1.000000000f, 1.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::LightGoldenrodYellow = ColourRGBA(0.980392218f, 0.980392218f, 0.823529482f, 1.000000000f);
const ColourRGBA ColourRGBA::LightGreen = ColourRGBA(0.564705908f, 0.933333397f, 0.564705908f, 1.000000000f);
const ColourRGBA ColourRGBA::LightGray = ColourRGBA(0.827451050f, 0.827451050f, 0.827451050f, 1.000000000f);
const ColourRGBA ColourRGBA::LightPink = ColourRGBA(1.000000000f, 0.713725507f, 0.756862819f, 1.000000000f);
const ColourRGBA ColourRGBA::LightSalmon = ColourRGBA(1.000000000f, 0.627451003f, 0.478431404f, 1.000000000f);
const ColourRGBA ColourRGBA::LightSeaGreen = ColourRGBA(0.125490203f, 0.698039234f, 0.666666687f, 1.000000000f);
const ColourRGBA ColourRGBA::LightSkyBlue = ColourRGBA(0.529411793f, 0.807843208f, 0.980392218f, 1.000000000f);
const ColourRGBA ColourRGBA::LightSlateGray = ColourRGBA(0.466666698f, 0.533333361f, 0.600000024f, 1.000000000f);
const ColourRGBA ColourRGBA::LightSteelBlue = ColourRGBA(0.690196097f, 0.768627524f, 0.870588303f, 1.000000000f);
const ColourRGBA ColourRGBA::LightYellow = ColourRGBA(1.000000000f, 1.000000000f, 0.878431439f, 1.000000000f);
const ColourRGBA ColourRGBA::Lime = ColourRGBA(0.000000000f, 1.000000000f, 0.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::LimeGreen = ColourRGBA(0.196078449f, 0.803921640f, 0.196078449f, 1.000000000f);
const ColourRGBA ColourRGBA::Linen = ColourRGBA(0.980392218f, 0.941176534f, 0.901960850f, 1.000000000f);
const ColourRGBA ColourRGBA::Magenta = ColourRGBA(1.000000000f, 0.000000000f, 1.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::Maroon = ColourRGBA(0.501960814f, 0.000000000f, 0.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::MediumAquamarine = ColourRGBA(0.400000036f, 0.803921640f, 0.666666687f, 1.000000000f);
const ColourRGBA ColourRGBA::MediumBlue = ColourRGBA(0.000000000f, 0.000000000f, 0.803921640f, 1.000000000f);
const ColourRGBA ColourRGBA::MediumOrchid = ColourRGBA(0.729411781f, 0.333333343f, 0.827451050f, 1.000000000f);
const ColourRGBA ColourRGBA::MediumPurple = ColourRGBA(0.576470613f, 0.439215720f, 0.858823597f, 1.000000000f);
const ColourRGBA ColourRGBA::MediumSeaGreen = ColourRGBA(0.235294133f, 0.701960802f, 0.443137288f, 1.000000000f);
const ColourRGBA ColourRGBA::MediumSlateBlue = ColourRGBA(0.482352972f, 0.407843173f, 0.933333397f, 1.000000000f);
const ColourRGBA ColourRGBA::MediumSpringGreen = ColourRGBA(0.000000000f, 0.980392218f, 0.603921592f, 1.000000000f);
const ColourRGBA ColourRGBA::MediumTurquoise = ColourRGBA(0.282352954f, 0.819607913f, 0.800000072f, 1.000000000f);
const ColourRGBA ColourRGBA::MediumVioletRed = ColourRGBA(0.780392230f, 0.082352944f, 0.521568656f, 1.000000000f);
const ColourRGBA ColourRGBA::MidnightBlue = ColourRGBA(0.098039225f, 0.098039225f, 0.439215720f, 1.000000000f);
const ColourRGBA ColourRGBA::MintCream = ColourRGBA(0.960784376f, 1.000000000f, 0.980392218f, 1.000000000f);
const ColourRGBA ColourRGBA::MistyRose = ColourRGBA(1.000000000f, 0.894117713f, 0.882353008f, 1.000000000f);
const ColourRGBA ColourRGBA::Moccasin = ColourRGBA(1.000000000f, 0.894117713f, 0.709803939f, 1.000000000f);
const ColourRGBA ColourRGBA::NavajoWhite = ColourRGBA(1.000000000f, 0.870588303f, 0.678431392f, 1.000000000f);
const ColourRGBA ColourRGBA::Navy = ColourRGBA(0.000000000f, 0.000000000f, 0.501960814f, 1.000000000f);
const ColourRGBA ColourRGBA::OldLace = ColourRGBA(0.992156923f, 0.960784376f, 0.901960850f, 1.000000000f);
const ColourRGBA ColourRGBA::Olive = ColourRGBA(0.501960814f, 0.501960814f, 0.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::OliveDrab = ColourRGBA(0.419607878f, 0.556862772f, 0.137254909f, 1.000000000f);
const ColourRGBA ColourRGBA::Orange = ColourRGBA(1.000000000f, 0.647058845f, 0.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::OrangeRed = ColourRGBA(1.000000000f, 0.270588249f, 0.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::Orchid = ColourRGBA(0.854902029f, 0.439215720f, 0.839215755f, 1.000000000f);
const ColourRGBA ColourRGBA::PaleGoldenrod = ColourRGBA(0.933333397f, 0.909803987f, 0.666666687f, 1.000000000f);
const ColourRGBA ColourRGBA::PaleGreen = ColourRGBA(0.596078455f, 0.984313786f, 0.596078455f, 1.000000000f);
const ColourRGBA ColourRGBA::PaleTurquoise = ColourRGBA(0.686274529f, 0.933333397f, 0.933333397f, 1.000000000f);
const ColourRGBA ColourRGBA::PaleVioletRed = ColourRGBA(0.858823597f, 0.439215720f, 0.576470613f, 1.000000000f);
const ColourRGBA ColourRGBA::PapayaWhip = ColourRGBA(1.000000000f, 0.937254965f, 0.835294187f, 1.000000000f);
const ColourRGBA ColourRGBA::PeachPuff = ColourRGBA(1.000000000f, 0.854902029f, 0.725490212f, 1.000000000f);
const ColourRGBA ColourRGBA::Peru = ColourRGBA(0.803921640f, 0.521568656f, 0.247058839f, 1.000000000f);
const ColourRGBA ColourRGBA::Pink = ColourRGBA(1.000000000f, 0.752941251f, 0.796078503f, 1.000000000f);
const ColourRGBA ColourRGBA::Plum = ColourRGBA(0.866666734f, 0.627451003f, 0.866666734f, 1.000000000f);
const ColourRGBA ColourRGBA::PowderBlue = ColourRGBA(0.690196097f, 0.878431439f, 0.901960850f, 1.000000000f);
const ColourRGBA ColourRGBA::Purple = ColourRGBA(0.501960814f, 0.000000000f, 0.501960814f, 1.000000000f);
const ColourRGBA ColourRGBA::Red = ColourRGBA(1.000000000f, 0.000000000f, 0.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::RosyBrown = ColourRGBA(0.737254918f, 0.560784340f, 0.560784340f, 1.000000000f);
const ColourRGBA ColourRGBA::RoyalBlue = ColourRGBA(0.254901975f, 0.411764741f, 0.882353008f, 1.000000000f);
const ColourRGBA ColourRGBA::SaddleBrown = ColourRGBA(0.545098066f, 0.270588249f, 0.074509807f, 1.000000000f);
const ColourRGBA ColourRGBA::Salmon = ColourRGBA(0.980392218f, 0.501960814f, 0.447058856f, 1.000000000f);
const ColourRGBA ColourRGBA::SandyBrown = ColourRGBA(0.956862807f, 0.643137276f, 0.376470625f, 1.000000000f);
const ColourRGBA ColourRGBA::SeaGreen = ColourRGBA(0.180392161f, 0.545098066f, 0.341176480f, 1.000000000f);
const ColourRGBA ColourRGBA::SeaShell = ColourRGBA(1.000000000f, 0.960784376f, 0.933333397f, 1.000000000f);
const ColourRGBA ColourRGBA::Sienna = ColourRGBA(0.627451003f, 0.321568638f, 0.176470593f, 1.000000000f);
const ColourRGBA ColourRGBA::Silver = ColourRGBA(0.752941251f, 0.752941251f, 0.752941251f, 1.000000000f);
const ColourRGBA ColourRGBA::SkyBlue = ColourRGBA(0.529411793f, 0.807843208f, 0.921568692f, 1.000000000f);
const ColourRGBA ColourRGBA::SlateBlue = ColourRGBA(0.415686309f, 0.352941185f, 0.803921640f, 1.000000000f);
const ColourRGBA ColourRGBA::SlateGray = ColourRGBA(0.439215720f, 0.501960814f, 0.564705908f, 1.000000000f);
const ColourRGBA ColourRGBA::Snow = ColourRGBA(1.000000000f, 0.980392218f, 0.980392218f, 1.000000000f);
const ColourRGBA ColourRGBA::SpringGreen = ColourRGBA(0.000000000f, 1.000000000f, 0.498039246f, 1.000000000f);
const ColourRGBA ColourRGBA::SteelBlue = ColourRGBA(0.274509817f, 0.509803951f, 0.705882370f, 1.000000000f);
const ColourRGBA ColourRGBA::Tan = ColourRGBA(0.823529482f, 0.705882370f, 0.549019635f, 1.000000000f);
const ColourRGBA ColourRGBA::Teal = ColourRGBA(0.000000000f, 0.501960814f, 0.501960814f, 1.000000000f);
const ColourRGBA ColourRGBA::Thistle = ColourRGBA(0.847058892f, 0.749019623f, 0.847058892f, 1.000000000f);
const ColourRGBA ColourRGBA::Tomato = ColourRGBA(1.000000000f, 0.388235331f, 0.278431386f, 1.000000000f);
const ColourRGBA ColourRGBA::Transparent = ColourRGBA(0.000000000f, 0.000000000f, 0.000000000f, 0.000000000f);
const ColourRGBA ColourRGBA::Turquoise = ColourRGBA(0.250980407f, 0.878431439f, 0.815686345f, 1.000000000f);
const ColourRGBA ColourRGBA::Violet = ColourRGBA(0.933333397f, 0.509803951f, 0.933333397f, 1.000000000f);
const ColourRGBA ColourRGBA::Wheat = ColourRGBA(0.960784376f, 0.870588303f, 0.701960802f, 1.000000000f);
const ColourRGBA ColourRGBA::White = ColourRGBA(1.000000000f, 1.000000000f, 1.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::WhiteSmoke = ColourRGBA(0.960784376f, 0.960784376f, 0.960784376f, 1.000000000f);
const ColourRGBA ColourRGBA::Yellow = ColourRGBA(1.000000000f, 1.000000000f, 0.000000000f, 1.000000000f);
const ColourRGBA ColourRGBA::YellowGreen = ColourRGBA(0.603921592f, 0.803921640f, 0.196078449f, 1.000000000f);
const ColourRGBA ColourRGBA::Alpha = ColourRGBA(0.100000000f, 0.100000000f, 0.100000000f, 0.000000000f);
const ColourRGBA ColourRGBA::Null = ColourRGBA(0.000000000f, 0.000000000f, 0.000000000f, 0.000000000f);

ColourRGBA ColourRGBA::from_hex(FXD::U8 r, FXD::U8 g, FXD::U8 b, FXD::U8 a)
{
	ColourRGBA retVal(	((FXD::F32)r / (FXD::F32)Math::NumericLimits< FXD::U8 > ::max()),
						((FXD::F32)g / (FXD::F32)Math::NumericLimits< FXD::U8 > ::max()),
						((FXD::F32)b / (FXD::F32)Math::NumericLimits< FXD::U8 > ::max()),
						((FXD::F32)a / (FXD::F32)Math::NumericLimits< FXD::U8 > ::max()));
	return retVal;
}

ColourRGBA ColourRGBA::from_string(const Core::String8& str, FXD::UTF8 delim)
{
	Container::Vector< FXD::F32 > vec = str.to_F32_array(delim);
	ColourRGBA retVal(vec[0], vec[1], vec[2], vec[3]);
	return retVal;
}

ColourRGBA ColourRGBA::random(void)
{
	return ColourRGBA(Math::RandomRange(0.0f, 1.0f), Math::RandomRange(0.0f, 1.0f), Math::RandomRange(0.0f, 1.0f), Math::RandomRange(0.0f, 1.0f));
}


Core::IStreamOut& FXD::Core::operator<<(Core::IStreamOut& ostr, ColourRGBA const& rhs)
{
	return ostr << rhs.getR() << UTF_8(" ") << rhs.getG() << UTF_8(" ") << rhs.getB() << UTF_8(" ") << rhs.getA();
}