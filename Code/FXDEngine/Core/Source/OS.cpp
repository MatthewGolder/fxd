// Creator - MatthewGolder
#include "FXDEngine/Core/OS.h"
#include "FXDEngine/Core/TypeTraits.h"

#if IsOSPC()
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>
#include <winnt.h>
#include <securitybaseapi.h>
#endif

using namespace FXD;
using namespace Core;

// ------
// OS
// -
// ------
bool FXD::Core::OS::IsUserAdmin(void)
{
#if IsOSPC()
	SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;
	PSID administratorsGroup = {};
	BOOL bRes = AllocateAndInitializeSid(&NtAuthority, 2, SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS, 0, 0, 0, 0, 0, 0, &administratorsGroup);
	if (bRes)
	{
		if (!CheckTokenMembership(NULL, administratorsGroup, &bRes))
		{
			bRes = FALSE;
		}
		FreeSid(administratorsGroup);
	}

	// If the above determined that we aren't in the administrator's group...
	if (!bRes)
	{
		HANDLE hToken = {};
		if (OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &hToken))
		{
			TOKEN_ELEVATION tknElevation = {};
			DWORD cbSize = sizeof(TOKEN_ELEVATION);
			if (GetTokenInformation(hToken, TokenElevation, &tknElevation, sizeof(tknElevation), &cbSize))
			{
				bRes = tknElevation.TokenIsElevated;
			}
			CloseHandle(hToken);
		}
	}

	return (bRes != FALSE); // Need to check for not FALSE rather than TRUE.
#elif IsOSAndroid()
	return false;
#else
	return false;
#endif
}