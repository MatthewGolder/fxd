// Creator - MatthewGolder
#include "FXDEngine/Core/StreamOut.h"
#include "FXDEngine/Core/SerializeOut.h"
#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Core/Compression/StreamCompressorLZ4.h"
#include "FXDEngine/Core/Compression/StreamCompressorZlib.h"

using namespace FXD;
using namespace Core;

// ------
// IStreamOut
// -
// ------
IStreamOut::IStreamOut(Core::ISerializeOut* pSerializeOut, CompressParams compressParams)
	: m_pSerializeOut(nullptr)
	, m_pCompressor(nullptr)
{
	_set_serialize_out(pSerializeOut);
	_set_compressor_out(compressParams);
}

IStreamOut::~IStreamOut(void)
{
	FXD_SAFE_DELETE(m_pCompressor);
}

IStreamOut& IStreamOut::operator<<(const FXD::UTF8* pVal)
{
	m_pSerializeOut->_pass(pVal, this);
	return (*this);
}
IStreamOut& IStreamOut::operator<<(const FXD::UTF16* pVal)
{
	m_pSerializeOut->_pass(pVal, this);
	return (*this);
}
IStreamOut& IStreamOut::operator<<(const bool val)
{
	m_pSerializeOut->_pass(val, this);
	return (*this);
}
IStreamOut& IStreamOut::operator<<(const FXD::UTF8 val)
{
	m_pSerializeOut->_pass(val, this);
	return (*this);
}
IStreamOut& IStreamOut::operator<<(const FXD::S8 nVal)
{
	m_pSerializeOut->_pass(nVal, this);
	return (*this);
}
IStreamOut& IStreamOut::operator<<(const FXD::U8 nVal)
{
	m_pSerializeOut->_pass(nVal, this);
	return (*this);
}
IStreamOut& IStreamOut::operator<<(const FXD::S16 nVal)
{
	m_pSerializeOut->_pass(nVal, this);
	return (*this);
}
IStreamOut& IStreamOut::operator<<(const FXD::U16 nVal)
{
	m_pSerializeOut->_pass(nVal, this);
	return (*this);
}
IStreamOut& IStreamOut::operator<<(const FXD::S32 nVal)
{
	m_pSerializeOut->_pass(nVal, this);
	return (*this);
}
IStreamOut& IStreamOut::operator<<(const FXD::U32 nVal)
{
	m_pSerializeOut->_pass(nVal, this);
	return (*this);
}
IStreamOut& IStreamOut::operator<<(const FXD::S64 nVal)
{
	m_pSerializeOut->_pass(nVal, this);
	return (*this);
}
IStreamOut& IStreamOut::operator<<(const FXD::U64 nVal)
{
	m_pSerializeOut->_pass(nVal, this);
	return (*this);
}
IStreamOut& IStreamOut::operator<<(const FXD::F32 nVal)
{
	m_pSerializeOut->_pass(nVal, this);
	return (*this);
}
IStreamOut& IStreamOut::operator<<(const FXD::F64 nVal)
{
	m_pSerializeOut->_pass(nVal, this);
	return (*this);
}

FXD::U64 IStreamOut::compress_size(const void* pData, const FXD::U64 nSize)
{
	if (m_pCompressor != nullptr)
	{
		return m_pCompressor->compress_size(pData, nSize);
	}
	else
	{
		return write_size(pData, nSize);
	}
}

void IStreamOut::_set_serialize_out(Core::ISerializeOut* pSerializeOut)
{
	m_pSerializeOut = pSerializeOut;
}

void IStreamOut::_set_compressor_out(CompressParams compressParams)
{
	PRINT_COND_ASSERT((m_pCompressor == nullptr), "Core: m_pCompressor has already been constructed.");
	
	switch (compressParams.Type)
	{
		case Core::E_CompressionType::ZLib:
		{
			m_pCompressor = FXD_NEW(Core::IStreamCompressorZlib)(this, compressParams.Quality);
			break;
		}
		case Core::E_CompressionType::ZLibStreamed:
		{
			m_pCompressor = FXD_NEW(Core::IStreamCompressorStreamZlib)(this, compressParams.Quality);
			break;
		}
		case Core::E_CompressionType::LZ4:
		{
			m_pCompressor = FXD_NEW(Core::IStreamCompressorLZ4)(this, compressParams.Quality);
			break;
		}
		case Core::E_CompressionType::LZ4Streamed:
		{
			m_pCompressor = FXD_NEW(Core::IStreamCompressorStreamLZ4)(this, compressParams.Quality);
			break;
		}
		default:
		{
			break;
		}
	}
}