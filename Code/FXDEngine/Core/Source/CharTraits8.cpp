// Creator - MatthewGolder
#include "FXDEngine/Core/CharTraits.h"
#include "FXDEngine/Memory/Memory.h"
#include <3rdParty/xxHash/xxhash.h>

#include <stdio.h>

#if IsOSPC()
#define snprintf(d, l, s, v)	sprintf_s(d, l, s, v)
#endif

using namespace FXD;
using namespace Core;

const FXD::U64 kStrSeed = 100;

// ------
// CharTraits< FXD::UTF8 >
// -
// ------
template <>
FXD::UTF8 CharTraits< FXD::UTF8 >::to_upper(const FXD::UTF8 ch)
{
	if (('a' <= ch) && ('z' >= ch))
	{
		return (ch & 0xdf);
	}
	return ch;
}
template <>
FXD::UTF8* CharTraits< FXD::UTF8 >::to_upper(FXD::UTF8* const pStr, const FXD::U32 nCount)
{
	for (FXD::U32 nID = 0; nID < nCount; nID++)
	{
		pStr[nID] = to_upper(pStr[nID]);
	}
	return pStr;
}
template <>
FXD::UTF8* CharTraits< FXD::UTF8 >::to_upper(FXD::UTF8* const pStr)
{
	return to_upper(pStr, length(pStr));
}
template <>
FXD::UTF8 CharTraits< FXD::UTF8 >::to_lower(const FXD::UTF8 ch)
{
	if (('A' <= ch) && ('Z' >= ch))
	{
		return (ch | 0x20);
	}
	return ch;
}
template <>
FXD::UTF8* CharTraits< FXD::UTF8 >::to_lower(FXD::UTF8* const pStr, const FXD::U32 nCount)
{
	for (FXD::U32 nID = 0; nID < nCount; nID++)
	{
		pStr[nID] = to_lower(pStr[nID]);
	}
	return pStr;
}
template <>
FXD::UTF8* CharTraits< FXD::UTF8 >::to_lower(FXD::UTF8* const pStr)
{
	return to_lower(pStr, length(pStr));
}
template <>
const FXD::HashValue CharTraits< FXD::UTF8 >::to_hash(const FXD::UTF8* const pStr, const FXD::U32 nCount)
{
	FXD::HashValue retHash = XXH64(pStr, (nCount * sizeof(FXD::UTF8)), kStrSeed);
	return retHash;
}
template <>
const FXD::HashValue CharTraits< FXD::UTF8 >::to_hash(const FXD::UTF8* const pStr)
{
	return to_hash(pStr, length(pStr));
}
template <>
FXD::UTF8* CharTraits< FXD::UTF8 >::assign(FXD::UTF8* const pDst, const FXD::UTF8 src, const FXD::U32 nCount)
{
	FXD::U32 nCount2 = nCount;
	FXD::UTF8* pDstCpy = pDst;
	for (; 0 < nCount2; ++pDstCpy, --nCount2)
	{
		(*pDstCpy) = src;
	}
	return pDst;
}
template <>
FXD::UTF8* CharTraits< FXD::UTF8 >::copy(FXD::UTF8* const pDst, const FXD::UTF8* pSrc, const FXD::U32 nCount)
{
	return (nCount == 0) ? pDst : (FXD::UTF8*)Memory::MemCopy(pDst, pSrc, nCount);
}
template <>
FXD::UTF8* CharTraits< FXD::UTF8 >::move(FXD::UTF8* const pDst, const FXD::UTF8* pSrc, const FXD::U32 nCount)
{
	return (nCount == 0) ? pDst : (FXD::UTF8*)Memory::MemMove(pDst, pSrc, nCount);
}
template <>
FXD::S32 CharTraits< FXD::UTF8 >::compare(const FXD::UTF8* const pLhs, const FXD::UTF8* const pRhs, const FXD::U32 nCount)
{
	return ((nCount == 0) ? 0 : Memory::MemCompare(pLhs, pRhs, nCount));
}
template <>
FXD::S32 CharTraits< FXD::UTF8 >::compare(const FXD::UTF8* const pLhs, const FXD::UTF8* const pRhs)
{
	return compare(pLhs, pRhs, length(pRhs));
}
template <>
FXD::S32 CharTraits< FXD::UTF8 >::compare_no_case(const FXD::UTF8* const pLhs, const FXD::UTF8* const pRhs, const FXD::U32 nCount)
{
	FXD::UTF8 f;
	FXD::UTF8 l;
	FXD::S32 nRetVal = 0;
	FXD::U32 nID = 0;
	FXD::U32 nCounter = nCount;

	if (nCounter != 0)
	{
		do
		{
			f = to_lower(pLhs[nID]);
			l = to_lower(pRhs[nID]);
			nID++;
		} while ((--nCounter) && f && (f == l));

		nRetVal = (FXD::S32)(f - l);
	}
	return nRetVal;
}
template <>
FXD::S32 CharTraits< FXD::UTF8 >::compare_no_case(const FXD::UTF8* const pLhs, const FXD::UTF8* const pRhs)
{
	return compare_no_case(pLhs, pRhs, length(pRhs));
}
template <>
const FXD::UTF8* CharTraits< FXD::UTF8 >::find(const FXD::UTF8* const pStr, const FXD::U32 nCount, const FXD::UTF8& ch)
{
	FXD::U32 nCount2 = nCount;
	for (FXD::U32 nID = 0; 0 < nCount2; nID++, --nCount2)
	{
		if (pStr[nID] == ch)
		{
			return (FXD::UTF8*)&pStr[nID];
		}
	}
	return nullptr;
}
template <>
const FXD::UTF8* CharTraits< FXD::UTF8 >::find(const FXD::UTF8* const pStr, const FXD::UTF8& ch)
{
	if (pStr == nullptr)
	{
		return nullptr;
	}

	FXD::U32 nLen = length(pStr);
	for (FXD::U32 nID = 0; 0 < nLen; nID++, --nLen)
	{
		if (pStr[nID] == ch)
		{
			return (FXD::UTF8*)&pStr[nID];
		}
	}
	return nullptr;
}
template <>
const FXD::UTF8* CharTraits< FXD::UTF8 >::find_no_case(const FXD::UTF8* const pStr, const FXD::U32 nCount, const FXD::UTF8& ch)
{
	FXD::U32 nCount2 = nCount;
	FXD::UTF8 l = to_lower(ch);
	for (FXD::U32 nID = 0; 0 < nCount2; nID++, --nCount2)
	{
		FXD::UTF8 f = to_lower(pStr[nID]);
		if (f == l)
		{
			return (FXD::UTF8*)&pStr[nID];
		}
	}
	return nullptr;
}
template <>
const FXD::UTF8* CharTraits< FXD::UTF8 >::find_no_case(const FXD::UTF8* const pStr, const FXD::UTF8& ch)
{
	if (pStr == nullptr)
	{
		return nullptr;
	}

	FXD::U32 nLen = length(pStr);
	FXD::UTF8 l = to_lower(ch);
	for (FXD::U32 nID = 0; 0 < nLen; nID++, --nLen)
	{
		FXD::UTF8 f = to_lower(pStr[nID]);
		if (f == l)
		{
			return (FXD::UTF8*)&pStr[nID];
		}
	}
	return nullptr;
}
template <>
const bool CharTraits< FXD::UTF8 >::is_quote(const FXD::UTF8 ch)
{
	return (ch == '\"');
}
template <>
const bool CharTraits< FXD::UTF8 >::is_dot(const FXD::UTF8 ch)
{
	return (ch == '.');
}
template <>
const bool CharTraits< FXD::UTF8 >::is_underscore(const FXD::UTF8 ch)
{
	return (ch == '_');
}
template <>
const bool CharTraits< FXD::UTF8 >::is_null_terminator(const FXD::UTF8 ch)
{
	return (ch == '\0');
}
template <>
const bool CharTraits< FXD::UTF8 >::is_white_space(const FXD::UTF8 ch)
{
	return ((ch == ' ') || (ch == '\t') || (ch == '\n') || (ch == '\r'));
}
template <>
const bool CharTraits< FXD::UTF8 >::is_space(const FXD::UTF8 ch)
{
	return ((ch == ' ') || ((ch >= 0x09) && (ch <= 0x0d)));
}
template <>
const bool CharTraits< FXD::UTF8 >::is_numeric(const FXD::UTF8 ch)
{
	return (('0' <= ch) && ('9' >= ch));
}
template <>
const bool CharTraits< FXD::UTF8 >::is_alpha(const FXD::UTF8 ch)
{
	return ((('A' <= ch) && ('Z' >= ch)) || (('a' <= ch) && ('z' >= ch)));
}
template <>
const bool CharTraits< FXD::UTF8 >::is_alpha_numeric(const FXD::UTF8 ch)
{
	return (is_numeric(ch) || is_alpha(ch));
}
template <>
const bool CharTraits< FXD::UTF8 >::is_japanese_punctuation(const FXD::UTF8 /*ch*/)
{
	return false;
}
template <>
const bool CharTraits< FXD::UTF8 >::is_hiragana(const FXD::UTF8 /*ch*/)
{
	return false;
}
template <>
const bool CharTraits< FXD::UTF8 >::is_katakana(const FXD::UTF8 /*ch*/)
{
	return false;
}
template <>
const bool CharTraits< FXD::UTF8 >::is_katakana_phonetic_extensions(const FXD::UTF8 /*ch*/)
{
	return false;
}
template <>
const bool CharTraits< FXD::UTF8 >::is_kanji(const FXD::UTF8 /*ch*/)
{
	return false;
}
template <>
const bool CharTraits< FXD::UTF8 >::is_japanese(const FXD::UTF8 ch)
{
	return ((is_japanese_punctuation(ch)) || (is_hiragana(ch)) || (is_katakana(ch)) || (is_katakana_phonetic_extensions(ch)) || (is_kanji(ch)));
}

template <>
void CharTraits< FXD::UTF8 >::to_string(FXD::UTF8* const pBuf, const FXD::U32 nSize, const FXD::S8 nVal)
{
	snprintf(pBuf, nSize, "%hhi", nVal);
}
template <>
void CharTraits< FXD::UTF8 >::to_string(FXD::UTF8* const pBuf, const FXD::U32 nSize, const FXD::U8 nVal)
{
	snprintf(pBuf, nSize, "%hhu", nVal);
}
template <>
void CharTraits< FXD::UTF8 >::to_string(FXD::UTF8* const pBuf, const FXD::U32 nSize, const FXD::S16 nVal)
{
	snprintf(pBuf, nSize, "%hi", nVal);
}
template <>
void CharTraits< FXD::UTF8 >::to_string(FXD::UTF8* const pBuf, const FXD::U32 nSize, const FXD::U16 nVal)
{
	snprintf(pBuf, nSize, "%hu", nVal);
}
template <>
void CharTraits< FXD::UTF8 >::to_string(FXD::UTF8* const pBuf, const FXD::U32 nSize, const FXD::S32 nVal)
{
	snprintf(pBuf, nSize, "%d", nVal);
}
template <>
void CharTraits< FXD::UTF8 >::to_string(FXD::UTF8* const pBuf, const FXD::U32 nSize, const FXD::U32 nVal)
{
	snprintf(pBuf, nSize, "%u", nVal);
}
template <>
void CharTraits< FXD::UTF8 >::to_string(FXD::UTF8* const pBuf, const FXD::U32 nSize, const FXD::S64 nVal)
{
	snprintf(pBuf, nSize, "%lld", nVal);
}
template <>
void CharTraits< FXD::UTF8 >::to_string(FXD::UTF8* const pBuf, const FXD::U32 nSize, const FXD::U64 nVal)
{
	snprintf(pBuf, nSize, "%lld", nVal);
}
template <>
void CharTraits< FXD::UTF8 >::to_string(FXD::UTF8* const pBuf, const FXD::U32 nSize, const FXD::F32 fVal)
{
	snprintf(pBuf, nSize, "%f", fVal);
}
template <>
void CharTraits< FXD::UTF8 >::to_string(FXD::UTF8* const pBuf, const FXD::U32 nSize, const FXD::F64 fVal)
{
	snprintf(pBuf, nSize, "%lf", fVal);
}