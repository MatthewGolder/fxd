// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_SEEK_H
#define FXDENGINE_CORE_SEEK_H

#include "FXDEngine/Core/Types.h"

namespace FXD
{
	namespace Core
	{
		// ------
		// E_SeekOffset
		// -
		// ------
		enum class E_SeekOffset
		{
			Begin = 0,
			Current = 1,
			End = 2,
			Count,
			Unknown = 0xffff
		};

		// ------
		// IStreamSeekable
		// -
		// ------
		class IStreamSeekable
		{
		public:
			IStreamSeekable(void);
			virtual ~IStreamSeekable(void);

			FXD::U64 seek_begin(void);																	// Seek to a beginning of the stream.
			FXD::U64 seek_end(void);																	// Seek to a end of the stream.

			virtual FXD::U64 length(void) PURE;														// Get the total length of the stream.
			virtual FXD::S32 tell(void) PURE;														// Return current stream position (or -1 on error).				
			virtual FXD::U64 remaining(void);														// Get the total length of the stream from the current position.
			virtual FXD::U64 seek_to(FXD::U64 nOffset, Core::E_SeekOffset eFrom) PURE;	// Seek to a position in the stream.
		};
	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_SEEK_H