// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_UNICODE_H
#define FXDENGINE_CORE_UNICODE_H

#include "FXDEngine/Core/Types.h"

namespace FXD
{
	namespace Core
	{
		// ------
		// UnicodeFuncs
		// -
		// ------
		struct UnicodeFuncs
		{
			// Functions that convert buffers of unicode code points, allocating a buffer.
			// - These functions allocate their own return buffers. You are responsible for
			//	calling delete[] on these buffers.
			// - Because they allocate memory, do not use these functions in a tight loop.
			// - These are useful when you need a new long term copy of a string.
			static FXD::UTF16* convertUTF8toUTF16(const FXD::UTF8* pString);
			static FXD::UTF16* convertUTF8toUTF16(const FXD::UTF8* pString, FXD::UTF16* pStringOut);
			static FXD::U32 convertUTF8toUTF16(const FXD::UTF8* pString, FXD::UTF16* pStringOut, FXD::U32 nLen);

			static FXD::UTF8* convertUTF16toUTF8(const FXD::UTF16* pString);
			static FXD::UTF8* convertUTF16toUTF8(const FXD::UTF16* pString, FXD::UTF8* pStringOut);
			static FXD::U32 convertUTF16toUTF8(const FXD::UTF16* pString, FXD::UTF8* pStringOut, FXD::U32 nLen);


			// Functions that converts one unicode codepoint at a time
			// - oneUTF8toUTF32() and oneUTF16toUTF32() return the converted Unicode code point
			//	in *codepoint, and set *pUnitsWalked to the \# of code units *codepoint took up.
			//	The next Unicode code point should start at *(codepoint + *pUnitsWalked).
			// - oneUTF32toUTF8() requires a 3 byte buffer, and returns the \# of bytes used.
			static FXD::UTF32 oneUTF8toUTF32(const FXD::UTF8* pCodepoint, FXD::U32* pUnitsWalked = nullptr);
			static FXD::UTF32 oneUTF16toUTF32(const FXD::UTF16* pCodepoint, FXD::U32* pUnitsWalked = nullptr);
			static FXD::UTF16 oneUTF32toUTF16(const FXD::UTF32 nCodepoint);
			static FXD::U32	oneUTF32toUTF8(const FXD::UTF32 nCodepoint, FXD::UTF8* pThreeByteCodeunitBuf);

			// Functions to read and validate UTF BOMs (Byte Order Marker): http://en.wikipedia.org/wiki/Byte_Order_Mark
			static bool chompUTF8BOM(const FXD::UTF8* pInString, FXD::UTF8** pOutStringPtr);
			static bool isValidUTF8BOM(FXD::U8 bom[4]);
		};

	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_UNICODE_H
