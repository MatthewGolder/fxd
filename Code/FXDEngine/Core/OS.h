// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_OS_H
#define FXDENGINE_CORE_OS_H

#include "FXDEngine/Core/OS.inl"

namespace FXD
{
	namespace Core
	{
		namespace OS
		{
			CONSTEXPR const Core::StringView8 ToString(const Core::E_OS eType)
			{
				return getToString(eType);
			}
			CONSTEXPR Core::E_OS ToType(const Core::StringView8 strType)
			{
				return getToType(strType);
			}
			CONSTEXPR Core::E_OS OSType(void)
			{
				return getOSType();
			}
			extern bool IsUserAdmin(void);
		}

	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_OS_H