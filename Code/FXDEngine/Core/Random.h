// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_RANDOM_H
#define FXDENGINE_CORE_RANDOM_H

#include "FXDEngine/Core/Types.h"
#include "FXDEngine/Math/Limits.h"
#include "FXDEngine/Core/Internal/IsIntegral.h"
#include "FXDEngine/Core/Internal/IsUnsigned.h"
#include <stdint.h>

namespace FXD
{
	namespace Core
	{
		// ------
		// RandomGen
		// -
		// ------
		class RandomGen
		{
		public:
			RandomGen(FXD::U64 nSeed)
				: m_nSeed(nSeed)
			{}

			FXD::U64 operator()()
			{
				return rand();
			}

			FXD::U64 operator()(FXD::U64 nLimit)
			{
				return rand_limit(nLimit);
			}

			FXD::U64 rand(void)
			{
				// This is not designed to be a high quality random number generator.
				if (m_nSeed == 0)
				{
					m_nSeed = UINT64_C(0xfefefefefefefefe); // Can't have a seed of zero.
				}
				const FXD::U64 nResult64A = ((m_nSeed * UINT64_C(6364136223846793005)) + UINT64_C(1442695040888963407));
				const FXD::U64 nResult64B = ((nResult64A * UINT64_C(6364136223846793005)) + UINT64_C(1442695040888963407));

				m_nSeed = (nResult64A >> 32) ^ nResult64B;

				return (FXD::U64)m_nSeed;
			}

			FXD::U64 rand_limit(FXD::U64 nLimit)
			{
				return (rand() % nLimit);
			}

			FXD::U64 rand_range(FXD::U64 nBegin, FXD::U64 nEnd)
			{
				return nBegin + (FXD::U64)rand_limit((FXD::U64)(nEnd - nBegin));
			}

		protected:
			FXD::U64 m_nSeed;
		};

	} //namespace Core

	namespace STD
	{
		// ------
		// uniform_int_distribution
		// -
		// ------
		template < typename IntType = FXD::S32 >
		class uniform_int_distribution
		{
		private:
			CTC_ASSERT((FXD::STD::is_integral< IntType >::value), "Core: uniform_int_distribution: IntType must be integral.");

		public:
			using result_type = IntType;

			// For uniform_int_distribution, param_type defines simply the min and max values of
			// the range returned by operator(). It may mean something else for other distribution types.
			struct param_type
			{
				EXPLICIT param_type(IntType a = 0, IntType b = FXD::Math::NumericLimits< IntType >::max());

				result_type a(void) const;
				result_type b(void) const;

				bool operator==(const param_type& rhs)
				{
					return (rhs.m_a == m_a) && (rhs.m_b == m_b);
				}
				bool operator!=(const param_type& rhs)
				{
					return (rhs.m_a != m_a) || (rhs.m_b != m_b);
				}

			protected:
				IntType m_a;
				IntType m_b;
			};

			uniform_int_distribution(IntType a = 0, IntType b = FXD::Math::NumericLimits< IntType >::max());
			uniform_int_distribution(const param_type& params);

			void reset(void);

			template < typename Generator >
			result_type operator()(Generator& g);

			template < typename Generator >
			result_type operator()(Generator& g, const param_type& params);

			result_type a(void) const;
			result_type b(void) const;

			param_type param(void) const;
			void param(const param_type& params);

			result_type min(void) const;
			result_type max(void) const;

		protected:
			param_type m_param;
		};

		template < typename IntType >
		inline FXD::STD::uniform_int_distribution< IntType >::param_type::param_type(IntType aVal, IntType bVal)
			: m_a(aVal)
			, m_b(bVal)
		{
			PRINT_COND_ASSERT((aVal <= bVal), "");
		}

		template < typename IntType >
		inline typename FXD::STD::uniform_int_distribution< IntType >::result_type FXD::STD::uniform_int_distribution< IntType >::param_type::a(void) const
		{
			return m_a;
		}

		template < typename IntType >
		inline typename FXD::STD::uniform_int_distribution< IntType >::result_type FXD::STD::uniform_int_distribution< IntType >::param_type::b(void) const
		{
			return m_b;
		}

		template < typename IntType >
		inline FXD::STD::uniform_int_distribution< IntType >::uniform_int_distribution(IntType aVal, IntType bVal)
			: m_param(aVal, bVal)
		{}

		template < typename IntType >
		inline FXD::STD::uniform_int_distribution< IntType >::uniform_int_distribution(const param_type& params)
			: m_param(params)
		{}

		template < typename IntType >
		void FXD::STD::uniform_int_distribution< IntType >::reset(void)
		{
		}

		template < typename IntType >
		template < typename Generator >
		inline typename FXD::STD::uniform_int_distribution< IntType >::result_type FXD::STD::uniform_int_distribution< IntType >::operator()(Generator& g)
		{
			return operator()(g, m_param);
		}

		template < typename IntType >
		template < typename Generator >
		inline typename FXD::STD::uniform_int_distribution< IntType >::result_type FXD::STD::uniform_int_distribution< IntType >::operator()(Generator& g, const param_type& params)
		{
			CTC_ASSERT(FXD::STD::is_unsigned< result_type >::value && (sizeof(result_type) <= 4), "Core: uniform_int_distribution currently supports only FXD::U8, FXD::U16, FXD::U32.");

			result_type v = g(); // Generates a value in the range of (numeric_limits<result_type>::min(), numeric_limits<result_type>::max()).
			result_type r = (result_type)((v * (FXD::U64)((params.b() - params.a()) + 1)) >> (sizeof(result_type) * 8)); // +1 because ranges are inclusive.
			return (params.a() + r);
		}

		template < typename IntType >
		inline typename FXD::STD::uniform_int_distribution< IntType >::result_type FXD::STD::uniform_int_distribution< IntType >::a(void) const
		{
			return m_param.m_a;
		}

		template < typename IntType >
		inline typename FXD::STD::uniform_int_distribution< IntType >::result_type FXD::STD::uniform_int_distribution< IntType >::b(void) const
		{
			return m_param.m_b;
		}

		template < typename IntType >
		inline typename FXD::STD::uniform_int_distribution< IntType >::param_type FXD::STD::uniform_int_distribution< IntType >::param(void) const
		{
			return m_param;
		}

		template < typename IntType >
		inline void FXD::STD::uniform_int_distribution< IntType >::param(const param_type& params)
		{
			m_param = params;
		}

		template < typename IntType >
		inline typename FXD::STD::uniform_int_distribution< IntType >::result_type FXD::STD::uniform_int_distribution< IntType >::min(void) const
		{
			return m_param.m_a;
		}

		template < typename IntType >
		inline typename FXD::STD::uniform_int_distribution< IntType >::result_type FXD::STD::uniform_int_distribution< IntType >::max(void) const
		{
			return m_param.m_b;
		}

		template < typename ResultType >
		inline bool operator==(const FXD::STD::uniform_int_distribution< ResultType >& lhs, const FXD::STD::uniform_int_distribution< ResultType >& rhs)
		{
			return (lhs.param() == rhs.param());
		}

		template < typename ResultType >
		inline bool operator!=(const FXD::STD::uniform_int_distribution< ResultType >& lhs, const FXD::STD::uniform_int_distribution< ResultType >& rhs)
		{
			return (lhs.param() != rhs.param());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_RANDOM_H