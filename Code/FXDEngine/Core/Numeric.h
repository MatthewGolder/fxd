// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_NUMERIC_H
#define FXDENGINE_CORE_NUMERIC_H

#include "FXDEngine/Core/Internal/Accumulate.h"
#include "FXDEngine/Core/Internal/AccumulateIf.h"
#include "FXDEngine/Core/Internal/AdjacentDifference.h"
#include "FXDEngine/Core/Internal/AdjacentDifferenceIf.h"
#include "FXDEngine/Core/Internal/ExclusiveScan.h"
#include "FXDEngine/Core/Internal/ExclusiveScanIf.h"
#include "FXDEngine/Core/Internal/InclusiveScan.h"
#include "FXDEngine/Core/Internal/InclusiveScanIf.h"
#include "FXDEngine/Core/Internal/InnerProduct.h"
#include "FXDEngine/Core/Internal/InnerProductIf.h"
#include "FXDEngine/Core/Internal/Itoa.h"
#include "FXDEngine/Core/Internal/PartialSum.h"
#include "FXDEngine/Core/Internal/PartialSumIf.h"
#include "FXDEngine/Core/Internal/Reduce.h"
#include "FXDEngine/Core/Internal/ReduceIf.h"
#include "FXDEngine/Core/Internal/TransformExclusiveScan.h"
#include "FXDEngine/Core/Internal/TransformInclusiveScan.h"
#include "FXDEngine/Core/Internal/TransformReduce.h"
#include "FXDEngine/Core/Internal/TransformReduceIf.h"

#endif //FXDENGINE_CORE_NUMERIC_Hs