// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_STRING_H
#define FXDENGINE_CORE_STRING_H

#include "FXDEngine/Core/StringView.h"
#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Core/StreamIn.h"
#include "FXDEngine/Container/Vector.h"

#include <initializer_list>

namespace FXD
{
	namespace Core
	{
		template < typename Container, typename T, typename Pointer, typename Reference >
		class _string_iterator;

		template < typename charT, typename traits = Core::CharTraits< charT > >
		class StringBase;

		using String8 = StringBase< FXD::UTF8 >;
		using String16 = StringBase< FXD::UTF16 >;

		template < typename charT, typename traits = Core::CharTraits< charT > >
		class StringBuilderBase;

		using StringBuilder8 = StringBuilderBase< FXD::UTF8 >;
		using StringBuilder16 = StringBuilderBase< FXD::UTF16 >;

		// ------
		// _string_iterator
		// -
		// ------
		template < typename Container, typename T, typename Pointer, typename Reference >
		class _string_iterator
		{
		public:
			using my_type = _string_iterator< Container, T, Pointer, Reference >;
			using container = Container;
			using value_type = T;
			using reference = Reference;
			using pointer = Pointer;
			using difference_type = std::ptrdiff_t;
			using iterator_category = FXD::STD::random_access_iterator_tag;
			using iterator = _string_iterator< Container, T, T*, T& >;
			using const_iterator = _string_iterator< Container, T, const T*, const T& >;

		public:
			_string_iterator(void);
			_string_iterator(container* pCont, pointer pType);
			_string_iterator(const iterator& rhs);

			my_type& operator++();
			my_type operator++(int);

			my_type& operator--();
			my_type operator--(int);

			my_type& operator+=(difference_type nOff) NOEXCEPT;
			my_type& operator-=(difference_type nOff) NOEXCEPT;

			my_type operator+(difference_type nOff) const;
			my_type operator-(difference_type nOff) const;

			reference operator*(void) const;
			pointer operator->(void) const;

		private:
			bool _is_valid(void) const NOEXCEPT;

		public:
			Container* m_pCont;
			Pointer m_pType;
		};

		// ------
		// _string_iterator
		// -
		// ------
		template < typename Container, typename T, typename Pointer, typename Reference >
		_string_iterator< Container, T, Pointer, Reference >::_string_iterator(void)
			: m_pCont(nullptr)
			, m_pType(nullptr)
		{}
		template < typename Container, typename T, typename Pointer, typename Reference >
		_string_iterator< Container, T, Pointer, Reference >::_string_iterator(container* pCont, pointer pType)
			: m_pCont(pCont)
			, m_pType(pType)
		{}
		template < typename Container, typename T, typename Pointer, typename Reference >
		_string_iterator< Container, T, Pointer, Reference >::_string_iterator(const iterator& rhs)
			: m_pCont(rhs.m_pCont)
			, m_pType(rhs.m_pType)
		{}

		template < typename Container, typename T, typename Pointer, typename Reference >
		typename _string_iterator< Container, T, Pointer, Reference >::my_type& _string_iterator< Container, T, Pointer, Reference >::operator++()
		{
			PRINT_COND_ASSERT((((m_pCont == nullptr) || (m_pType == nullptr) || (m_pCont->_last() <= m_pType)) == false), "Core: Iterator is invalid");
			++m_pType;
			return (*this);
		}
		template < typename Container, typename T, typename Pointer, typename Reference >
		typename _string_iterator< Container, T, Pointer, Reference >::my_type _string_iterator< Container, T, Pointer, Reference >::operator++(int)
		{
			const my_type tmp(*this);
			operator++();
			return (tmp);
		}

		template < typename Container, typename T, typename Pointer, typename Reference >
		typename _string_iterator< Container, T, Pointer, Reference >::my_type& _string_iterator< Container, T, Pointer, Reference >::operator--()
		{
			PRINT_COND_ASSERT((((m_pCont == nullptr) || (m_pType == nullptr) || (m_pType <= m_pCont->_first())) == false), "Core: Iterator is invalid");
			--m_pType;
			return (*this);
		}
		template < typename Container, typename T, typename Pointer, typename Reference >
		typename _string_iterator< Container, T, Pointer, Reference >::my_type _string_iterator< Container, T, Pointer, Reference >::operator--(int)
		{
			const my_type tmp(*this);
			operator--();
			return (tmp);
		}

		template < typename Container, typename T, typename Pointer, typename Reference >
		typename _string_iterator< Container, T, Pointer, Reference >::my_type& _string_iterator< Container, T, Pointer, Reference >::operator+=(difference_type nOff) NOEXCEPT
		{
			m_pType += nOff;
			return (*this);
		}
		template < typename Container, typename T, typename Pointer, typename Reference >
		typename _string_iterator< Container, T, Pointer, Reference >::my_type& _string_iterator< Container, T, Pointer, Reference >::operator-=(difference_type nOff) NOEXCEPT
		{
			m_pType -= nOff;
			return (*this);
		}

		template < typename Container, typename T, typename Pointer, typename Reference >
		typename _string_iterator< Container, T, Pointer, Reference >::my_type _string_iterator< Container, T, Pointer, Reference >::operator+(difference_type nOff) const
		{
			return FXD::STD::next((*this), nOff);
		}
		template < typename Container, typename T, typename Pointer, typename Reference >
		typename _string_iterator< Container, T, Pointer, Reference >::my_type _string_iterator< Container, T, Pointer, Reference >::operator-(difference_type nOff) const
		{
			return FXD::STD::prev((*this), nOff);
		}

		template < typename Container, typename T, typename Pointer, typename Reference >
		typename _string_iterator< Container, T, Pointer, Reference >::reference _string_iterator< Container, T, Pointer, Reference >::operator*(void) const
		{
			PRINT_COND_ASSERT((_is_valid()), "Core: Iterator is invalid");
			return (*m_pType);
		}
		template < typename Container, typename T, typename Pointer, typename Reference >
		typename _string_iterator< Container, T, Pointer, Reference >::pointer _string_iterator< Container, T, Pointer, Reference >::operator->(void) const
		{
			return &(operator*());
		}

		template < typename Container, typename T, typename Pointer, typename Reference >
		bool _string_iterator< Container, T, Pointer, Reference >::_is_valid(void) const NOEXCEPT
		{
			if ((m_pCont == nullptr) || (m_pType == nullptr) || (m_pType < m_pCont->_first()) || (m_pCont->_last() <= m_pType))
			{
				return false;
			}
			return true;
		}

		// Non-member comparison functions
		// Equality
		template < typename Container, typename T, typename PtrA, typename RefA, typename PtrB, typename RefB >
		inline bool operator==(const _string_iterator< Container, T, PtrA, RefA >& lhs, const _string_iterator< Container, T, PtrB, RefB >& rhs) NOEXCEPT
		{
			return (lhs.m_pCont == rhs.m_pCont) && (lhs.m_pType == rhs.m_pType);
		}
		template < typename Container, typename T, typename PtrA, typename RefA >
		inline bool operator==(const _string_iterator< Container, T, PtrA, RefA >& lhs, const _string_iterator< Container, T, PtrA, RefA >& rhs) NOEXCEPT
		{
			return (lhs.m_pCont == rhs.m_pCont) && (lhs.m_pType == rhs.m_pType);
		}
		// Inequality
		template < typename Container, typename T, typename PtrA, typename RefA, typename PtrB, typename RefB >
		inline bool operator!=(const _string_iterator< Container, T, PtrA, RefA >& lhs, const _string_iterator< Container, T, PtrB, RefB >& rhs) NOEXCEPT
		{
			return (lhs.m_pCont != rhs.m_pCont) || (lhs.m_pType != rhs.m_pType);
		}
		template < typename Container, typename T, typename PtrA, typename RefA >
		inline bool operator!=(const _string_iterator< Container, T, PtrA, RefA >& lhs, const _string_iterator< Container, T, PtrA, RefA >& rhs) NOEXCEPT
		{
			return (lhs.m_pCont != rhs.m_pCont) || (lhs.m_pType != rhs.m_pType);
		}
		// Less than
		template < typename Container, typename T, typename PtrA, typename RefA, typename PtrB, typename RefB >
		inline bool operator<(const _string_iterator< Container, T, PtrA, RefA >& lhs, const _string_iterator< Container, T, PtrB, RefB >& rhs) NOEXCEPT
		{
			return (lhs.m_pType < rhs.m_pType);
		}
		// Greater than
		template < typename Container, typename T, typename PtrA, typename RefA, typename PtrB, typename RefB >
		inline bool operator>(const _string_iterator< Container, T, PtrA, RefA >& lhs, const _string_iterator< Container, T, PtrB, RefB >& rhs) NOEXCEPT
		{
			return (lhs.m_pType > rhs.m_pType);
		}
		// Less than or equal to
		template < typename Container, typename T, typename PtrA, typename RefA, typename PtrB, typename RefB >
		inline bool operator<=(const _string_iterator< Container, T, PtrA, RefA >& lhs, const _string_iterator< Container, T, PtrB, RefB >& rhs) NOEXCEPT
		{
			return (lhs.m_pType <= rhs.m_pType);
		}
		// Greater than or equal to
		template < typename Container, typename T, typename PtrA, typename RefA, typename PtrB, typename RefB >
		inline bool operator>=(const _string_iterator< Container, T, PtrA, RefA >& lhs, const _string_iterator< Container, T, PtrB, RefB >& rhs) NOEXCEPT
		{
			return (lhs.m_pType >= rhs.m_pType);
		}

		template < typename Container, typename T, typename PtrA, typename RefA, typename PtrB, typename RefB >
		inline typename _string_iterator< Container, T, PtrA, RefA >::difference_type operator+(const _string_iterator< Container, T, PtrA, RefA >& lhs, const _string_iterator< Container, T, PtrB, RefB >& rhs) NOEXCEPT
		{
			return (lhs.m_pType + rhs.m_pType);
		}
		template < typename Container, typename T, typename PtrA, typename RefA, typename PtrB, typename RefB >
		inline typename _string_iterator< Container, T, PtrA, RefA >::difference_type operator-(const _string_iterator< Container, T, PtrA, RefA >& lhs, const _string_iterator< Container, T, PtrB, RefB >& rhs) NOEXCEPT
		{
			return (lhs.m_pType - rhs.m_pType);
		}

		// ------
		// StringBase
		// -
		// ------
		template < typename charT, typename traits >
		class StringBase
		{
		public:
			using my_type = StringBase< charT, traits >;
			using view_type = StringViewBase< charT, traits >;
			using value_type = charT;
			using traits_type = traits;
			using pointer = charT*;
			using const_pointer = const charT*;
			using reference = charT & ;
			using const_reference = const charT&;
			using size_type = FXD::U32;
			using difference_type = std::ptrdiff_t;

			using iterator = _string_iterator< my_type, charT, charT*, charT& >;
			using const_iterator = _string_iterator< my_type, charT, const charT*, const charT& >;
			using reverse_iterator = FXD::STD::reverse_iterator< iterator >;
			using const_reverse_iterator = FXD::STD::reverse_iterator< const_iterator >;

			friend iterator;
			friend const_iterator;
			friend reverse_iterator;
			friend const_reverse_iterator;

			static const size_type npos = size_type(-1); // bad/missing length/position

		public:
			StringBase(void);
			StringBase(const my_type& str, size_type nSubpos, size_type nSublen = npos);
			StringBase(const_pointer pStr);
			StringBase(const_pointer pStr, size_type nCount);
			StringBase(const value_type ch);
			StringBase(size_type nCount, const value_type ch);
			template < typename InputItr >
			StringBase(InputItr first, InputItr last);
			StringBase(std::initializer_list< value_type > iList);

			~StringBase(void);

			StringBase(const my_type& rhs);
			StringBase(my_type&& rhs);

			EXPLICIT StringBase(const view_type& strView);
			StringBase(const view_type& strView, size_type nPos, size_type nSize);

			my_type& operator=(const my_type& rhs);
			my_type& operator=(my_type&& rhs);
			my_type& operator=(const_pointer pStr);
			my_type& operator=(const value_type ch);
			my_type& operator=(std::initializer_list< value_type > iList);
			my_type& operator=(view_type strView);

			my_type& operator+=(const my_type& str);
			my_type& operator+=(const value_type ch);
			my_type& operator+=(const_pointer pStr);
			my_type& operator+=(std::initializer_list< value_type > iList);

			operator StringViewBase< charT >(void) const NOEXCEPT;

			// Iterator Support
			iterator begin(void) NOEXCEPT;
			const_iterator begin(void) const NOEXCEPT;
			const_iterator cbegin(void) const NOEXCEPT;
			iterator end(void) NOEXCEPT;
			const_iterator end(void) const NOEXCEPT;
			const_iterator cend(void) const NOEXCEPT;

			reverse_iterator rbegin(void) NOEXCEPT;
			const_reverse_iterator rbegin(void) const NOEXCEPT;
			const_reverse_iterator crbegin(void) const NOEXCEPT;
			reverse_iterator rend(void) NOEXCEPT;
			const_reverse_iterator rend(void) const NOEXCEPT;
			const_reverse_iterator crend(void) const NOEXCEPT;

			// Access
			reference operator[](size_type nOffSet);
			const_reference operator[](size_type nOffSet) const;
			reference at(size_type nOffSet);
			const_reference at(size_type nOffSet) const;

			reference front(void);
			const_reference front(void) const;
			reference back(void);
			const_reference back(void) const;

			const_pointer c_str(void) const NOEXCEPT;
			pointer data(void) NOEXCEPT;
			const_pointer data(void) const NOEXCEPT;

			// Modifiers
			my_type& append(size_type nCount, const value_type ch);
			my_type& append(const my_type& str);
			my_type& append(const my_type& str, size_type nPos, size_type nCount = my_type::npos);
			my_type& append(const_pointer pStr, size_type nCount);
			my_type& append(const_pointer pStr);
			template < typename InputItr >
			my_type& append(InputItr first, InputItr last);
			my_type& append(std::initializer_list< value_type > iList);

			my_type& assign(size_type nCount, const value_type ch);
			my_type& assign(const my_type& rhs);
			my_type& assign(const my_type& rhs, size_type nPos, size_type nCount = my_type::npos);
			my_type& assign(my_type&& rhs);
			my_type& assign(const_pointer pStr, size_type nCount);
			my_type& assign(const_pointer pStr);
			template < typename InputItr >
			my_type& assign(InputItr first, InputItr last);
			my_type& assign(std::initializer_list< value_type > iList);

			my_type& insert(size_type nPos, size_type nCount, const value_type ch);
			my_type& insert(size_type nPos, const_pointer pStr);
			my_type& insert(size_type nPos, const_pointer pStr, size_type nCount);
			my_type& insert(size_type nPos, const my_type& str);
			my_type& insert(size_type nPos, const my_type& str, size_type nPos2, size_type nCount = my_type::npos);
			iterator insert(const_iterator pos, const value_type ch);
			iterator insert(const_iterator pos, size_type nCount, const value_type ch);
			template < typename InputItr >
			iterator insert(const_iterator pos, InputItr first, InputItr last);
			iterator insert(const_iterator pos, std::initializer_list< value_type > iList);
			void push_back(value_type ch);

			// Replace
			my_type& replace(size_type nPos, size_type nCount, const my_type& str);
			my_type& replace(const_iterator first, const_iterator last, const my_type& str);
			my_type& replace(size_type nPos, size_type nCount, const my_type& str, size_type nPos2, size_type nCount2 = my_type::npos);
			template < typename InputItr >
			my_type& replace(const_iterator first, const_iterator last, InputItr first2, InputItr last2);
			my_type& replace(size_type nPos, size_type nCount, const_pointer pStr, size_type nCount2);
			my_type& replace(const_iterator first, const_iterator last, const_pointer pStr, size_type nCount);
			my_type& replace(size_type nPos, size_type nCount, const_pointer pStr);
			my_type& replace(const_iterator first, const_iterator last, const_pointer pStr);
			my_type& replace(size_type nPos, size_type nCount, size_type nCount2, const value_type ch);
			my_type& replace(const_iterator first, const_iterator last, size_type nCount2, const value_type ch);
			my_type& replace(const_iterator first, const_iterator last, std::initializer_list< value_type > iList);

			my_type& find_replace(const value_type oldChar, const value_type newChar);
			my_type& find_replace(const value_type oldChar, const my_type& str);
			my_type& find_replace(const my_type& strOld, const my_type& strNew);

			my_type substr(size_type nPos = 0, size_type nCount = my_type::npos) const;

			// Comparisons
			bool contains_case(const_pointer pStr) const;
			bool contains_case(const my_type& str) const;
			bool contains_no_case(const_pointer pStr) const;
			bool contains_no_case(const my_type& str) const;

			FXD::S32 compare(const my_type& str) const;
			FXD::S32 compare(size_type nPos, size_type nCount, const my_type& str) const;
			FXD::S32 compare(size_type nPos, size_type nCount, const my_type& str, size_type nPos2, size_type nCount2) const;
			FXD::S32 compare(const_pointer pStr) const;
			FXD::S32 compare(size_type nPos, size_type nCount, const_pointer pStr) const;
			FXD::S32 compare(size_type nPos, size_type nCount, const_pointer pStr, size_type nCount2) const;

			FXD::S32 compare_no_case(const my_type& str) const;
			FXD::S32 compare_no_case(size_type nPos, size_type nCount, const my_type& str) const;
			FXD::S32 compare_no_case(size_type nPos, size_type nCount, const my_type& str, size_type nPos2, size_type nCount2) const;
			FXD::S32 compare_no_case(const_pointer pStr) const;
			FXD::S32 compare_no_case(size_type nPos, size_type nCount, const_pointer pStr) const;
			FXD::S32 compare_no_case(size_type nPos, size_type nCount, const_pointer pStr, size_type nCount2) const;

			// Find operations (case sensitive)
			size_type find(const my_type& str, size_type nPos = 0) const NOEXCEPT;
			size_type find(const_pointer pStr, size_type nPos = 0) const;
			size_type find(const_pointer pStr, size_type nPos, size_type nCount) const;
			size_type find(const value_type ch, size_type nPos = 0) const NOEXCEPT;

			// Find operations (non case sensitive)
			size_type find_no_case(const my_type& str, size_type nPos = 0) const NOEXCEPT;
			size_type find_no_case(const_pointer pStr, size_type nPos = 0) const;
			size_type find_no_case(const_pointer pStr, size_type nPos, size_type nCount) const;
			size_type find_no_case(const value_type ch, size_type nPos = 0) const NOEXCEPT;

			// Reverse find operations (case sensitive)
			size_type rfind(const my_type& str, size_type nPos = my_type::npos) const NOEXCEPT;
			size_type rfind(const_pointer pStr, size_type nPos = my_type::npos) const;
			size_type rfind(const_pointer pStr, size_type nPos, size_type nCount) const;
			size_type rfind(const value_type ch, size_type nPos = my_type::npos) const NOEXCEPT;

			// Reverse find operations (non case sensitive)
			size_type rfind_no_case(const my_type& str, size_type nPos = my_type::npos) const NOEXCEPT;
			size_type rfind_no_case(const_pointer pStr, size_type nPos = my_type::npos) const;
			size_type rfind_no_case(const_pointer pStr, size_type nPos, size_type nCount) const;
			size_type rfind_no_case(const value_type ch, size_type nPos = my_type::npos) const NOEXCEPT;

			// Find first of operations
			size_type find_first_of(const my_type& str, size_type nPos = 0) const NOEXCEPT;
			size_type find_first_of(const_pointer pStr, size_type nPos = 0) const;
			size_type find_first_of(const_pointer pStr, size_type nPos, size_type nCount) const;
			size_type find_first_of(const value_type ch, size_type nPos = 0) const NOEXCEPT;

			// Find last of operations
			size_type find_last_of(const my_type& str, size_type nPos = my_type::npos) const NOEXCEPT;
			size_type find_last_of(const_pointer pStr, size_type nPos = my_type::npos) const;
			size_type find_last_of(const_pointer pStr, size_type nPos, size_type nCount) const;
			size_type find_last_of(const value_type ch, size_type nPos = my_type::npos) const NOEXCEPT;

			// Find first not of operations
			size_type find_first_not_of(const my_type& str, size_type nPos = 0) const NOEXCEPT;
			size_type find_first_not_of(const_pointer pStr, size_type nPos = 0) const;
			size_type find_first_not_of(const_pointer pStr, size_type nPos, size_type nCount) const;
			size_type find_first_not_of(const value_type ch, size_type nPos = 0) const NOEXCEPT;

			// Find last not of operations
			size_type find_last_not_of(const my_type& str, size_type nPos = my_type::npos) const NOEXCEPT;
			size_type find_last_not_of(const_pointer pStr, size_type nPos = my_type::npos) const;
			size_type find_last_not_of(const_pointer pStr, size_type nPos, size_type nCount) const;
			size_type find_last_not_of(const value_type ch, size_type nPos = my_type::npos) const NOEXCEPT;

			// Searches
			bool starts_with(const_pointer pStr) const;
			bool starts_with(const my_type& str) const;
			bool starts_with_no_case(const_pointer pStr) const;
			bool starts_with_no_case(const my_type& str) const;

			bool ends_with(const_pointer pStr) const;
			bool ends_with(const my_type& str) const;
			bool ends_with_no_case(const_pointer pStr) const;
			bool ends_with_no_case(const my_type& str) const;

			// Deletion
			my_type& erase(size_type nPos = 0, size_type nCount = my_type::npos);
			iterator erase(const_iterator pos);
			iterator erase(const_iterator first, const_iterator last);
			reverse_iterator erase(reverse_iterator pos);
			reverse_iterator erase(reverse_iterator first, reverse_iterator last);
			void pop_back(void);
			void clear(void);

			void resize(size_type nNewsize);
			void resize(size_type nNewsize, const value_type ch);
			void reserve(size_type nNewcap = 0);

			// Capacity
			bool empty(void) const NOEXCEPT;
			bool not_empty(void) const NOEXCEPT;
			bool validate(void) const NOEXCEPT;
			size_type length(void) const NOEXCEPT;
			size_type size(void) const NOEXCEPT;
			size_type capacity(void) const NOEXCEPT;
			size_type max_size(void) const NOEXCEPT;

			size_type copy(charT* pStr, size_type nCount, size_type nOffSet = 0) const;

			void swap(my_type& str);

			//void readBinary(Core::IStreamInBinary& is);

			bool to_bool(void) const;
			FXD::F32 to_F32(void) const;
			FXD::S16 to_S16(void) const;
			FXD::S32 to_S32(void) const;
			const FXD::HashValue to_hash(void) const;

			Container::Vector< bool > to_bool_array(const charT delim) const;
			Container::Vector< FXD::F32 > to_F32_array(const charT delim) const;
			Container::Vector< FXD::S16 > to_S16_array(const charT delim) const;
			Container::Vector< FXD::S32 > to_S32_array(const charT delim) const;
			Container::Vector< my_type > to_str_array(const charT delim) const;

			template < typename InputItr >
			size_type to_bool_array(const charT delim, InputItr start, const size_type nMaxCount) const;
			template < typename InputItr >
			size_type to_F32_array(const charT delim, InputItr start, const size_type nMaxCount) const;
			template < typename InputItr >
			size_type to_S32_array(const charT delim, InputItr start, const size_type nMaxCount) const;
			template < typename InputItr >
			size_type to_S16_array(const charT delim, InputItr start, const size_type nMaxCount) const;
			template < typename InputItr >
			size_type to_str_array(const charT delim, InputItr start, const size_type nMaxCount) const;

			void to_string(StringBase< FXD::UTF8 >& strOut) const;
			void to_string(StringBase< FXD::UTF16 >& strOut) const;
			template < typename NewT >
			StringBase< NewT > to_string(void) const;

			void to_lower(void);
			void to_upper(void);

			void ltrim(void);
			void rtrim(void);
			void trim(void);
			void ltrim(const value_type* pStr);
			void rtrim(const value_type* pStr);
			void trim(const value_type* pStr);

		private:
			pointer _first(void) NOEXCEPT;
			const_pointer _first(void) const NOEXCEPT;
			pointer _last(void) NOEXCEPT;
			const_pointer _last(void) const NOEXCEPT;
			pointer _myPtr(void) NOEXCEPT;
			const_pointer _myPtr(void) const NOEXCEPT;

			template < typename InputItr >
			void _construct(InputItr first, InputItr last, FXD::STD::input_iterator_tag);
			template < typename InputItr >
			void _construct(InputItr first, InputItr last, FXD::STD::forward_iterator_tag);
			void _construct(const_pointer pFirst, const_pointer pLast, FXD::STD::random_access_iterator_tag);

			void _assign_rv(my_type&& rhs);
			void _copy(size_type nNewSize, size_type nOldLen);
			void _eos(size_type nNewSize);
			bool _grow(size_type nNewSize, bool bTrim = false);
			bool _is_inside(const_pointer pStr);
			void _clean(bool bBuilt = false, size_type nNewSize = 0);

		private:
			enum
			{ // length of Internal buffer, [1, 16]
				_BUF_SIZE = ((16 / sizeof(charT)) < 1) ? 1 : (16 / sizeof(charT))
			};
			enum
			{ // roundup mask for allocated buffers, [0, 15]
				_ALLOC_MASK = sizeof(charT) <= 1 ? 15
				: sizeof(charT) <= 2 ? 7
				: sizeof(charT) <= 4 ? 3
				: sizeof(charT) <= 8 ? 1 : 0
			};

			union _Bxty
			{ // storage for small buffer or pointer to larger one
				charT m_buffer[_BUF_SIZE];
				charT* m_pPtr;
			} _buffer;

			size_type m_nSize;										// Current length of string
			size_type m_nCapacity;									// Current storage reserved for string
			Memory::Alloc::Allocator< Memory::Alloc::Heap > m_alloc;// Allocator object for strings
		};

		template < typename charT, typename traits >
		inline StringBase< charT, traits >::StringBase(void)
		{
			_clean();
		}
		template < typename charT, typename traits >
		inline StringBase< charT, traits >::StringBase(const my_type& str, size_type nSubpos, size_type nSublen)
		{
			_clean();
			assign(str, nSubpos, nSublen);
		}
		template < typename charT, typename traits >
		inline StringBase< charT, traits >::StringBase(const_pointer pStr)
		{
			_clean();
			assign(pStr);
		}
		template < typename charT, typename traits >
		inline StringBase< charT, traits >::StringBase(const_pointer pStr, size_type nCount)
		{
			_clean();
			assign(pStr, nCount);
		}
		template < typename charT, typename traits >
		inline StringBase< charT, traits >::StringBase(const value_type ch)
		{
			_clean();
			assign(1, ch);
		}
		template < typename charT, typename traits >
		inline StringBase< charT, traits >::StringBase(size_type nCount, const value_type ch)
		{
			_clean();
			assign(nCount, ch);
		}
		template < typename charT, typename traits >
		template < typename InputItr >
		inline StringBase< charT, traits >::StringBase(InputItr first, InputItr last)
		{
			using IC = typename FXD::STD::iterator_traits< InputItr >::iterator_category;
			_clean();
			_construct(first, last, IC());
		}
		template < typename charT, typename traits >
		inline StringBase< charT, traits >::StringBase(std::initializer_list< value_type > iList)
		{
			_clean();
			assign(iList.begin(), iList.end());
		}


		template < typename charT, typename traits >
		inline StringBase< charT, traits >::~StringBase(void)
		{
			_clean(true);
		}

		template < typename charT, typename traits >
		inline StringBase< charT, traits >::StringBase(const my_type& str)
		{
			_clean();
			assign(str, 0, my_type::npos);
		}
		template < typename charT, typename traits >
		inline StringBase< charT, traits >::StringBase(my_type&& rhs)
		{
			_assign_rv(std::forward< my_type >(rhs));
		}

		template < typename charT, typename traits >
		inline StringBase< charT, traits >::StringBase(const view_type& strView)
			: StringBase(strView.data(), strView.size())
		{
		}
		template < typename charT, typename traits >
		inline StringBase< charT, traits >::StringBase(const view_type& strView, size_type nPos, size_type nSize)
			: StringBase(strView.substr(nPos, nSize))
		{
		}

		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::operator=(const my_type& rhs)
		{
			if (&rhs != this)
			{
				assign(rhs);
			}
			return (*this);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::operator=(my_type&& rhs)
		{
			if (this != &rhs)
			{
				_clean(true);
				_assign_rv(std::forward< my_type >(rhs));
			}
			return (*this);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::operator=(const_pointer pStr)
		{
			return assign(pStr);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::operator=(const value_type ch)
		{
			return assign((size_type)1, ch);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::operator=(std::initializer_list< value_type > iList)
		{
			return (assign(iList.begin(), iList.end()));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::operator=(view_type strView)
		{
			return assign(strView.data(), static_cast< my_type::size_type >(strView.size()));
		}

		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::operator+=(const my_type& rhs)
		{
			return append(rhs);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::operator+=(const value_type ch)
		{
			return append((size_type)1, ch);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::operator+=(const_pointer pStr)
		{
			return append(pStr);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::operator+=(std::initializer_list< value_type > iList)
		{
			return (append(iList.begin(), iList.end()));
		}

		template < typename charT, typename traits >
		inline StringBase< charT, traits >::operator FXD::Core::StringViewBase< charT >(void) const NOEXCEPT
		{
			return FXD::Core::StringViewBase< charT >(data(), size());
		}

		// Iterator Support
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::iterator StringBase< charT, traits >::begin(void) NOEXCEPT
		{
			return iterator(this, _first());
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::const_iterator StringBase< charT, traits >::begin(void) const NOEXCEPT
		{
			return const_iterator(const_cast<my_type*>(this), _first());
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::const_iterator StringBase< charT, traits >::cbegin(void) const NOEXCEPT
		{
			return const_iterator(const_cast<my_type*>(this), _first());
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::iterator StringBase< charT, traits >::end(void) NOEXCEPT
		{
			return iterator(this, _last());
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::const_iterator StringBase< charT, traits >::end(void) const NOEXCEPT
		{
			return const_iterator(const_cast<my_type*>(this), _last());
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::const_iterator StringBase< charT, traits >::cend(void) const NOEXCEPT
		{
			return const_iterator(const_cast<my_type*>(this), _last());
		}

		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::reverse_iterator StringBase< charT, traits >::rbegin(void) NOEXCEPT
		{
			return reverse_iterator(iterator(end()));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::const_reverse_iterator StringBase< charT, traits >::rbegin(void) const NOEXCEPT
		{
			return const_reverse_iterator(const_iterator(end()));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::const_reverse_iterator StringBase< charT, traits >::crbegin(void) const NOEXCEPT
		{
			return const_reverse_iterator(const_iterator(end()));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::reverse_iterator StringBase< charT, traits >::rend(void) NOEXCEPT
		{
			return reverse_iterator(iterator(begin()));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::const_reverse_iterator StringBase< charT, traits >::rend(void) const NOEXCEPT
		{
			return const_reverse_iterator(const_iterator(begin()));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::const_reverse_iterator StringBase< charT, traits >::crend(void) const NOEXCEPT
		{
			return const_reverse_iterator(const_iterator(begin()));
		}

		// Access
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::reference StringBase< charT, traits >::operator[](size_type nOffSet)
		{
			return at(nOffSet);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::const_reference StringBase< charT, traits >::operator[](size_type nOffSet) const
		{
			return at(nOffSet);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::reference StringBase< charT, traits >::at(size_type nOffSet)
		{
			PRINT_COND_ASSERT((size() > nOffSet), "Core: Invalid string position");
			return (_myPtr()[nOffSet]);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::const_reference StringBase< charT, traits >::at(size_type nOffSet) const
		{
			PRINT_COND_ASSERT((size() > nOffSet), "Core: Invalid string position");
			return (_myPtr()[nOffSet]);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::reference StringBase< charT, traits >::front(void)
		{
			return (*_first());
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::const_reference StringBase< charT, traits >::front(void) const
		{
			return (*_first());
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::reference StringBase< charT, traits >::back(void)
		{
			return (*(_last() - 1));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::const_reference StringBase< charT, traits >::back(void) const
		{
			return (*(_last() - 1));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::const_pointer StringBase< charT, traits >::c_str(void) const NOEXCEPT
		{
			return _myPtr();
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::pointer StringBase< charT, traits >::data(void) NOEXCEPT
		{
			return _myPtr();
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::const_pointer StringBase< charT, traits >::data(void) const NOEXCEPT
		{
			return _myPtr();
		}

		// Modifiers
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::append(size_type nCount, const value_type ch)
		{
			PRINT_COND_ASSERT(((my_type::npos - size() <= nCount) == false), "Core: String too long");
			const size_type nNum = (size() + nCount);
			if ((0 < nCount) && (_grow(nNum)))
			{
				traits::assign((_myPtr() + size()), ch, nCount);
				_eos(nNum);
			}
			return (*this);
		}
		template < typename charT, typename traits >
		inline StringBase< charT, traits >& StringBase< charT, traits >::append(const my_type& str)
		{
			return (append(str, 0, my_type::npos));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::append(const my_type& str, size_type nPos, size_type nCount)
		{
			size_type nLength = str.size();
			PRINT_COND_ASSERT(((nLength < nPos) == false), "Core: Invalid string position");

			size_type nNum = (nLength - nPos);
			if (nNum < nCount)
			{
				nCount = nNum; // trim nCount to size
			}

			PRINT_COND_ASSERT(((my_type::npos - size() <= nCount) == false), "Core: String too long");
			if ((0 < nCount) && (_grow(nNum = size() + nCount)))
			{ // make room and append new stuff
				traits::copy((_myPtr() + size()), (str._myPtr() + nPos), nCount);
				_eos(nNum);
			}
			return (*this);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::append(const_pointer pStr, size_type nCount)
		{
			if (nCount != 0)
			{
				PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			}
			if (_is_inside(pStr))
			{
				return (append((*this), (size_type)(pStr - _myPtr()), nCount)); // substring
			}

			PRINT_COND_ASSERT(((my_type::npos - size() <= nCount) == false), "Core: String too long");
			const size_type nNum = (size() + nCount);
			if ((0 < nCount) && (_grow(nNum)))
			{ // make room and append new stuff
				traits::copy((_myPtr() + size()), pStr, nCount);
				_eos(nNum);
			}
			return (*this);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::append(const_pointer pStr)
		{
			PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			return (append(pStr, traits::length(pStr)));
		}
		template < typename charT, typename traits >
		template < typename InputItr >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::append(InputItr first, InputItr last)
		{
			return (replace(end(), end(), first, last));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::append(std::initializer_list< value_type > iList)
		{
			return (append(iList.begin(), iList.end()));
		}


		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::assign(size_type nCount, const value_type ch)
		{
			PRINT_COND_ASSERT(((nCount == my_type::npos) == false), "Core: String too long");
			if (_grow(nCount))
			{ // make room and assign new stuff
				traits::assign(_myPtr(), ch, nCount);
				_eos(nCount);
			}
			return (*this);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::assign(const my_type& rhs)
		{
			return assign(rhs, 0, my_type::npos);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::assign(const my_type& rhs, size_type nPos, size_type nCount)
		{
			PRINT_COND_ASSERT(((rhs.size() < nPos) == false), "Core: Invalid string position");
			size_type nNum = (rhs.size() - nPos);
			if (nCount < nNum)
			{
				nNum = nCount; // trim nNum to size
			}
			if (this == &rhs)
			{
				erase((size_type)(nPos + nNum)), erase(0, nPos); // substring
			}
			else if (_grow(nNum))
			{
				traits::copy(_myPtr(), (rhs._myPtr() + nPos), nNum);
				_eos(nNum);
			}
			return (*this);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::assign(my_type&& rhs)
		{
			if (this != &rhs)
			{
				if (_BUF_SIZE <= rhs.capacity())
				{
					(*this) = rhs;
				}
				else
				{
					_clean(true);
					_assign_rv(std::forward< my_type >(rhs));
				}
			}
			return (*this);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::assign(const_pointer pStr, size_type nCount)
		{
			if (nCount != 0)
			{
				PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			}
			if (_is_inside(pStr))
			{
				return (assign((*this), (size_type)(pStr - _myPtr()), nCount)); // substring
			}
			if (_grow(nCount))
			{ // make room and assign new stuff
				traits::copy(_myPtr(), pStr, nCount);
				_eos(nCount);
			}
			return (*this);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::assign(const_pointer pStr)
		{
			PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			return (assign(pStr, traits::length(pStr)));
		}
		template < typename charT, typename traits >
		template < typename InputItr >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::assign(InputItr first, InputItr last)
		{
			return (replace(begin(), end(), first, last));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::assign(std::initializer_list< value_type > iList)
		{
			return (assign(iList.begin(), iList.end()));
		}


		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::insert(size_type nPos, size_type nCount, const value_type ch)
		{
			PRINT_COND_ASSERT(((size() < nPos) == false), "Core: Invalid string position");
			PRINT_COND_ASSERT((((my_type::npos - size()) <= nCount) == false), "Core: String too long");

			size_type nNum = (size() + nCount);
			if (0 < nCount && _grow(nNum))
			{
				traits::move((_myPtr() + nPos + nCount), (_myPtr() + nPos), (size() - nPos)); // empty out hole
				traits::assign(_myPtr() + nPos, ch, nCount);
				_eos(nNum);
			}
			return (*this);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::insert(size_type nPos, const_pointer pStr)
		{
			PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			return (insert(nPos, pStr, traits::length(pStr)));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::insert(size_type nPos, const_pointer pStr, size_type nCount)
		{
			if (nCount != 0)
			{
				PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			}
			if (_is_inside(pStr))
			{
				return (insert(nPos, (*this), (pStr - _myPtr()), nCount)); // substring
			}

			PRINT_COND_ASSERT(((size() < nPos) == false), "Core: String too long");
			PRINT_COND_ASSERT((((my_type::npos - size()) <= nCount) == false), "Core: String too long");
			size_type nNum = (size() + nCount);
			if ((0 < nCount) && (_grow(nNum)))
			{ // make room and insert new stuff
				traits::move((_myPtr() + nPos + nCount), (_myPtr() + nPos), (size() - nPos)); // empty out hole
				traits::copy((_myPtr() + nPos), pStr, nCount); // fill hole
				_eos(nNum);
			}
			return (*this);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::insert(size_type nPos, const my_type& str)
		{
			return (insert(nPos, str, 0, my_type::npos));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::insert(size_type nPos, const my_type& str, size_type nPos2, size_type nCount)
		{
			PRINT_COND_ASSERT(((size() < nPos) || (str.size() < nPos2) == false), "Core: Invalid string position");
			size_type nNum = (str.size() - nPos2);
			if (nNum < nCount)
			{
				nCount = nNum; // trim nCount to size
			}

			PRINT_COND_ASSERT((((my_type::npos - size()) <= nCount) == false), "Core: String too long");
			if ((0 < nCount) && (_grow(nNum = (size() + nCount))))
			{ // make room and insert new stuff
				traits::move((_myPtr() + nPos + nCount), (_myPtr() + nPos), (size() - nPos)); // empty out hole
				if (this == &str)
				{
					traits::move((_myPtr() + nPos), _myPtr() + ((nPos < nPos2) ? (nPos2 + nCount) : nPos2), nCount); // substring
				}
				else
				{
					traits::copy((_myPtr() + nPos), str._myPtr() + nPos2, nCount);
				}
				_eos(nNum);
			}
			return (*this);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::iterator StringBase< charT, traits >::insert(const_iterator pos, const value_type ch)
		{
			size_type nOff = (pos - begin());
			insert(nOff, 1, ch);
			return (begin() + nOff);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::iterator StringBase< charT, traits >::insert(const_iterator pos, size_type nCount, const value_type ch)
		{
			size_type nOff = (pos - begin());
			insert(nOff, nCount, ch);
			return (begin() + nOff);
		}
		template < typename charT, typename traits >
		template < typename InputItr >
		inline typename StringBase< charT, traits >::iterator StringBase< charT, traits >::insert(const_iterator pos, InputItr first, InputItr last)
		{
			size_type nOff = (pos - begin());
			replace(pos, pos, first, last);
			return (begin() + nOff);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::iterator StringBase< charT, traits >::insert(const_iterator pos, std::initializer_list< value_type > iList)
		{
			return (insert(pos, iList.begin(), iList.end()));
		}
		template < typename charT, typename traits >
		void StringBase< charT, traits >::push_back(value_type ch)
		{
			append(1, ch);
		}

		// Replace
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::replace(size_type nPos, size_type nCount, const my_type& str)
		{
			return (replace(nPos, nCount, str, 0, my_type::npos));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::replace(const_iterator first, const_iterator last, const my_type& str)
		{
			return (replace((first - begin()), (last - first), str));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::replace(size_type nPos, size_type nCount, const my_type& str, size_type nPos2, size_type nCount2)
		{
			PRINT_COND_ASSERT(((size() < nPos) || (str.size() < nPos2) == false), "Core: Invalid string position");
			if ((size() - nPos) < nCount)
			{
				nCount = (size() - nPos); // trim nCount to size
			}
			size_type nNum = (str.size() - nPos2);
			if (nNum < nCount2)
			{
				nCount2 = nNum; // trim nCount2 to size
			}
			PRINT_COND_ASSERT(((my_type::npos - nCount2) <= (size() - nCount) == false), "Core: String too long");
			size_type nNumMove = (size() - nCount - nPos); // length of kept tail
			size_type nNumGrow = (size() + nCount2 - nCount);
			if (size() < nNumGrow)
			{
				_grow(nNumGrow);
			}
			if (this != &str)
			{ // no overlap, just move down and copy in new stuff
				traits::move((_myPtr() + nPos + nCount2), (_myPtr() + nPos + nCount), nNumMove); // empty hole
				traits::copy(_myPtr() + nPos, str._myPtr() + nPos2, nCount2); // fill hole
			}
			else if (nCount2 <= nCount)
			{ // hole doesn't get larger, just copy in substring
				traits::move((_myPtr() + nPos), (_myPtr() + nPos2), nCount2); // fill hole
				traits::move((_myPtr() + nPos + nCount2), (_myPtr() + nPos + nCount), nNumMove); // move tail down
			}
			else if (nPos2 <= nPos)
			{ // hole gets larger, substring begins before hole
				traits::move((_myPtr() + nPos + nCount2), (_myPtr() + nPos + nCount), nNumMove); // move tail down
				traits::move((_myPtr() + nPos), (_myPtr() + nPos2), nCount2); // fill hole
			}
			else if ((nPos + nCount) <= nPos2)
			{ // hole gets larger, substring begins after hole
				traits::move((_myPtr() + nPos + nCount2), (_myPtr() + nPos + nCount), nNumMove); // move tail down
				traits::move((_myPtr() + nPos), (_myPtr() + (nPos2 + nCount2 - nCount)), nCount2); // fill hole
			}
			else
			{ // hole gets larger, substring begins in hole
				traits::move((_myPtr() + nPos), (_myPtr() + nPos2), nCount); // fill old hole
				traits::move((_myPtr() + nPos + nCount2), (_myPtr() + nPos + nCount), nNumMove); // move tail down
				traits::move((_myPtr() + nPos + nCount), (_myPtr() + nPos2 + nCount2), (nCount2 - nCount)); // fill rest of new hole
			}
			_eos(nNumGrow);
			return (*this);
		}
		template < typename charT, typename traits >
		template < typename InputItr >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::replace(const_iterator first, const_iterator last, InputItr first2, InputItr last2)
		{
			my_type str(first2, last2);
			replace(first, last, str);
			return (*this);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::replace(size_type nPos, size_type nCount, const_pointer pStr, size_type nCount2)
		{
			if (nCount2 != 0)
			{
				PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			}
			if (_is_inside(pStr))
			{
				return replace(nPos, nCount, *this, (pStr - _myPtr()), nCount2); // substring, replace carefully
			}

			PRINT_COND_ASSERT(((size() < nPos) == false), "Core: Invalid string position");
			if (size() - (nPos < nCount))
			{
				nCount = (size() - nPos); // trim nCount to size
			}

			PRINT_COND_ASSERT(((my_type::npos - nCount2) <= (size() - nCount) == false), "Core: String too long");
			size_type nNumMove = (size() - nCount - nPos);
			if (nCount2 < nCount)
			{
				traits::move((_myPtr() + nPos + nCount), (_myPtr() + nPos + nCount), nNumMove); // smaller hole, move tail up
			}
			size_type nNumGrow = (size() + nCount2 - nCount);
			if (((0 < nCount2) || (0 < nCount)) && (_grow(nNumGrow)))
			{ // make room and rearrange
				if (nCount < nCount2)
				{
					traits::move((_myPtr() + nPos + nCount2), (_myPtr() + nPos + nCount), nNumMove); // move tail down
				}
				traits::copy(_myPtr() + nPos, pStr, nCount2); // fill hole
				_eos(nNumGrow);
			}
			return (*this);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::replace(const_iterator first, const_iterator last, const_pointer pStr, size_type nCount)
		{
			return (replace((first - begin()), (last - first), pStr, nCount));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::replace(size_type nPos, size_type nCount, const_pointer pStr)
		{
			return (replace(nPos, nCount, pStr, traits::length(pStr)));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::replace(const_iterator first, const_iterator last, const_pointer pStr)
		{
			return (replace((first - begin()), (last - first), pStr));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::replace(size_type nPos, size_type nCount, size_type nCount2, const value_type ch)
		{
			PRINT_COND_ASSERT(((size() < nPos) == false), "Core: Invalid string position");
			if ((size() - nPos) < nCount)
			{
				nCount = (size() - nPos); // trim nCount to size
			}

			PRINT_COND_ASSERT(((my_type::npos - nCount2) <= (size() - nCount) == false), "Core: String too long");
			size_type nNumMove = (size() - nCount - nPos);
			if (nCount2 < nCount)
			{
				traits::move((_myPtr() + nPos + nCount2), (_myPtr() + nPos + nCount), nNumMove); // smaller hole, move tail up
			}
			size_type nNumGrow = (size() + nCount2 - nCount);
			if (((0 < nCount2) || (0 < nCount)) && (_grow(nNumGrow)))
			{
				if (nCount < nCount2)
				{
					traits::move((_myPtr() + nPos + nCount2), (_myPtr() + nPos + nCount), nNumMove); // move tail down
				}
				traits::assign(_myPtr() + nPos, ch, nCount2); // fill hole
				_eos(nNumGrow);
			}
			return (*this);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::replace(const_iterator first, const_iterator last, size_type nCount2, const value_type ch)
		{
			return (replace((first - begin()), (last - first), nCount2, ch));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::replace(const_iterator first, const_iterator last, std::initializer_list< value_type > iList)
		{
			return (replace(first, last, iList.begin(), iList.end()));
		}

		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::find_replace(const value_type oldChar, const value_type newChar)
		{
			charT* pData = (charT*)_myPtr();
			for (size_type n1 = 0; n1 < length(); n1++)
			{
				if (traits::equals(pData[n1], oldChar))
				{
					pData[n1] = newChar;
				}
			}
			return (*this);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::find_replace(const my_type& strOld, const my_type& strNew)
		{
			size_type nPos = find(strOld);
			if (nPos != my_type::npos)
			{
				return replace(nPos, strOld.length(), strNew);
			}
			return (*this);
		}
		template < typename charT, typename traits >
		inline StringBase< charT, traits > StringBase< charT, traits >::substr(size_type nPos, size_type nCount) const
		{
			return my_type((*this), nPos, nCount);
		}

		// Comparisons
		template < typename charT, typename traits >
		inline bool StringBase< charT, traits >::contains_case(const_pointer pStr) const
		{
			size_type nRetVal = find(pStr);
			return (nRetVal != my_type::npos);
		}
		template < typename charT, typename traits >
		inline bool StringBase< charT, traits >::contains_case(const my_type& str) const
		{
			size_type nRetVal = find(str);
			return (nRetVal != my_type::npos);
		}
		template < typename charT, typename traits >
		inline bool StringBase< charT, traits >::contains_no_case(const_pointer pStr) const
		{
			size_type nRetVal = find_no_case(pStr);
			return (nRetVal != my_type::npos);
		}
		template < typename charT, typename traits >
		inline bool StringBase< charT, traits >::contains_no_case(const my_type& str) const
		{
			size_type nRetVal = find_no_case(str);
			return (nRetVal != my_type::npos);
		}

		template < typename charT, typename traits >
		inline FXD::S32 StringBase< charT, traits >::compare(const my_type& str) const
		{
			return (compare(0, size(), str._myPtr(), str.size()));
		}
		template < typename charT, typename traits >
		inline FXD::S32 StringBase< charT, traits >::compare(size_type nPos, size_type nCount, const my_type& str) const
		{
			return (compare(nPos, nCount, str, 0, my_type::npos));
		}
		template < typename charT, typename traits >
		inline FXD::S32 StringBase< charT, traits >::compare(size_type nPos, size_type nCount, const my_type& str, size_type nPos2, size_type nCount2) const
		{
			PRINT_COND_ASSERT(((str.size() < nPos2) == false), "Core: Invalid string position");
			if ((str.size() - nPos2) < nCount2)
			{
				nCount2 = str.size() - nPos2; // trim nCount2 to size
			}
			return (compare(nPos, nCount, (str._myPtr() + nPos2), nCount2));
		}
		template < typename charT, typename traits >
		inline FXD::S32 StringBase< charT, traits >::compare(const_pointer pStr) const
		{
			PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			return (compare(0, size(), pStr, traits::length(pStr)));
		}
		template < typename charT, typename traits >
		inline FXD::S32 StringBase< charT, traits >::compare(size_type nPos, size_type nCount, const_pointer pStr) const
		{
			PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			return (compare(nPos, nCount, pStr, traits::length(pStr)));
		}
		template < typename charT, typename traits >
		inline FXD::S32 StringBase< charT, traits >::compare(size_type nPos, size_type nCount, const_pointer pStr, size_type nCount2) const
		{
			if (nCount2 != 0)
			{
				PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			}

			PRINT_COND_ASSERT(((size() < nPos) == false), "Core: Invalid string position");
			if ((size() - nPos) < nCount)
			{
				nCount = (size() - nPos); // trim nCount to size
			}
			size_type nAns = traits::compare((_myPtr() + nPos), pStr, (nCount < nCount2) ? nCount : nCount2);
			FXD::S32 nRetVal = (nAns != 0 ? (FXD::S32)nAns : (nCount < nCount2) ? -1 : (nCount == nCount2) ? 0 : +1);
			return nRetVal;
		}

		template < typename charT, typename traits >
		inline FXD::S32 StringBase< charT, traits >::compare_no_case(const my_type& str) const
		{
			return compare_no_case(0, size(), str._myPtr(), str.size());
		}
		template < typename charT, typename traits >
		inline FXD::S32 StringBase< charT, traits >::compare_no_case(size_type nPos, size_type nCount, const my_type& str) const
		{
			return compare_no_case(nPos, nCount, str, 0, my_type::npos);
		}
		template < typename charT, typename traits >
		inline FXD::S32 StringBase< charT, traits >::compare_no_case(size_type nPos, size_type nCount, const my_type& str, size_type nPos2, size_type nCount2) const
		{
			PRINT_COND_ASSERT(((str.size() < nPos2) == false), "Core: Invalid string position");
			if ((str.size() - nPos2) < nCount2)
			{
				nCount2 = (str.size() - nPos2); // trim nCount2 to size
			}
			return compare_no_case(nPos, nCount, (str._myPtr() + nPos2), nCount2);
		}
		template < typename charT, typename traits >
		inline FXD::S32 StringBase< charT, traits >::compare_no_case(const_pointer pStr) const
		{
			PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			return compare_no_case(0, size(), pStr, traits::length(pStr));
		}
		template < typename charT, typename traits >
		inline FXD::S32 StringBase< charT, traits >::compare_no_case(size_type nPos, size_type nCount, const_pointer pStr) const
		{
			PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			return compare_no_case(nPos, nCount, pStr, traits::length(pStr));
		}
		template < typename charT, typename traits >
		inline FXD::S32 StringBase< charT, traits >::compare_no_case(size_type nPos, size_type nCount, const_pointer pStr, size_type nCount2) const
		{
			if (nCount2 != 0)
			{
				PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			}

			PRINT_COND_ASSERT(((size() < nPos) == false), "Core: Invalid string position");
			if ((size() - nPos) < nCount)
			{
				nCount = (size() - nPos); // trim nCount to size
			}
			size_type nAns = traits::compare_no_case((_myPtr() + nPos), pStr, (nCount < nCount2) ? nCount : nCount2);
			FXD::S32 nRetVal = (nAns != 0 ? (FXD::S32)nAns : (nCount < nCount2) ? -1 : (nCount == nCount2) ? 0 : +1);
			return nRetVal;
		}

		// Find operations (case sensitive)
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find(const my_type& str, size_type nPos) const NOEXCEPT
		{
			return (find(str._myPtr(), nPos, str.size()));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find(const_pointer pStr, size_type nPos) const
		{
			PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			return (find(pStr, nPos, traits::length(pStr)));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find(const_pointer pStr, size_type nPos, size_type nCount) const
		{
			if (nCount != 0)
			{
				PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			}
			if ((nCount == 0) && (nPos <= size()))
			{
				return nPos; // null string always matches (if inside string)
			}

			size_type nNum = (size() - nPos);
			if ((nPos < size()) && (nCount <= nNum))
			{ // room for match, look for it
				const_pointer pUptr;
				const_pointer pVptr;
				for (nNum -= (nCount - 1), pVptr = (_myPtr() + nPos); (pUptr = traits::find(pVptr, nNum, *pStr)) != nullptr; nNum -= (pUptr - pVptr + 1), pVptr = (pUptr + 1))
				{
					if (traits::compare(pUptr, pStr, nCount) == 0)
					{
						return (size_type)(pUptr - _myPtr()); // found a match
					}
				}
			}
			return my_type::npos; // no match
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find(const value_type ch, size_type nPos) const NOEXCEPT
		{
			return (find((const_pointer)&ch, nPos, 1));
		}

		// Find operations (non case sensitive)
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find_no_case(const my_type& str, size_type nPos) const NOEXCEPT
		{
			return (find_no_case(str._myPtr(), nPos, str.size()));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find_no_case(const_pointer pStr, size_type nPos) const
		{
			PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			return (find_no_case(pStr, nPos, traits::length(pStr)));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find_no_case(const_pointer pStr, size_type nPos, size_type nCount) const
		{
			if (nCount != 0)
			{
				PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			}
			if ((nCount == 0) && (nPos <= size()))
			{
				return nPos; // null string always matches (if inside string)
			}
			size_type nNum = (size() - nPos);
			if ((nPos < size()) && (nCount <= nNum))
			{
				const_pointer pUptr;
				const_pointer pVptr;
				for (nNum -= (nCount - 1), pVptr = (_myPtr() + nPos); (pUptr = traits::find_no_case(pVptr, nNum, *pStr)) != nullptr; nNum -= (pUptr - pVptr + 1), pVptr = (pUptr + 1))
				{
					if (traits::compare_no_case(pUptr, pStr, nCount) == 0)
					{
						return (pUptr - _myPtr());
					}
				}
			}
			return my_type::npos;
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find_no_case(const value_type ch, size_type nPos) const NOEXCEPT
		{
			return (find_no_case((const_pointer)&ch, nPos, 1));
		}


		// Reverse find operations (case sensitive)
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::rfind(const my_type& str, size_type nPos) const NOEXCEPT
		{
			return (rfind(str._myPtr(), nPos, str.size()));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::rfind(const_pointer pStr, size_type nPos) const
		{
			PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			return (rfind(pStr, nPos, traits::length(pStr)));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::rfind(const_pointer pStr, size_type nPos, size_type nCount) const
		{
			if (nCount != 0)
			{
				PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			}
			if (nCount == 0)
			{
				return (nPos < size()) ? nPos : size(); // null always matches
			}
			if (nCount <= size())
			{ // room for match, look for it
				const_pointer pUptr = _myPtr() + (nPos < (size() - nCount) ? nPos : (size() - nCount));
				for (;; --pUptr)
				{
					if ((traits::equals(*pUptr, *pStr)) && (traits::compare(pUptr, pStr, nCount) == 0))
					{
						return (pUptr - _myPtr()); // found a match
					}
					else if (pUptr == _myPtr())
					{
						break; // at beginning, no more chance for match
					}
				}
			}
			return my_type::npos; // no match
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::rfind(const value_type ch, size_type nPos) const NOEXCEPT
		{
			return (rfind((const_pointer)&ch, nPos, 1));
		}


		// Reverse find operations (non case sensitive)
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::rfind_no_case(const my_type& str, size_type nPos) const NOEXCEPT
		{
			return (rfind_no_case(str._myPtr(), nPos, str.size()));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::rfind_no_case(const_pointer pStr, size_type nPos) const
		{
			PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			return (rfind_no_case(pStr, nPos, traits::length(pStr)));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::rfind_no_case(const_pointer pStr, size_type nPos, size_type nCount) const
		{
			if (nCount != 0)
			{
				PRINT_COND_ASSERT((pStr != nullptr), "Core: Invalid null pointer");
			}
			if (nCount == 0)
			{
				return ((nPos < size()) ? nPos : size()); // null always matches
			}
			if (nCount <= size())
			{ // room for match, look for it
				const_pointer pUptr = _myPtr() + ((nPos < (size() - nCount)) ? nPos : (size() - nCount));
				for (;; --pUptr)
				{
					const value_type c1 = traits::to_lower(*pUptr);
					const value_type c2 = traits::to_lower(*pStr);
					if ((traits::equals(c1, c2)) && (traits::compare_no_case(pUptr, pStr, nCount) == 0))
					{
						return (pUptr - _myPtr()); // found a match
					}
					else if (pUptr == _myPtr())
					{
						break; // at beginning, no more chance for match
					}
				}
			}
			return my_type::npos; // no match
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::rfind_no_case(const value_type ch, size_type nPos) const NOEXCEPT
		{
			return (rfind_no_case((const_pointer)&ch, nPos, 1));
		}

		// Find first of operations
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find_first_of(const my_type& str, size_type nPos) const NOEXCEPT
		{
			return find_first_of(str._myPtr(), nPos, str.size());
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find_first_of(const_pointer pStr, size_type nPos) const
		{
			return find_first_of(pStr, nPos, (size_type)traits::length(pStr));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find_first_of(const_pointer pStr, size_type nPos, size_type nCount) const
		{
			if ((0 < nCount) && (nPos < size()))
			{
				const_pointer const pVptr = (_myPtr() + size());
				for (const_pointer pUptr = (_myPtr() + nPos); pUptr < pVptr; ++pUptr)
				{
					if (traits::find(pStr, nCount, *pUptr) != nullptr)
					{
						return (pUptr - _myPtr());
					}
				}
			}
			return my_type::npos;
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find_first_of(const value_type ch, size_type nPos) const NOEXCEPT
		{
			return find(ch, nPos);
		}

		// Find last of operations
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find_last_of(const my_type& str, size_type nPos) const NOEXCEPT
		{
			return find_last_of(str._myPtr(), nPos, str.size());
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find_last_of(const_pointer pStr, size_type nPos) const
		{
			return find_last_of(pStr, nPos, (size_type)traits::length(pStr));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find_last_of(const_pointer pStr, size_type nPos, size_type nCount) const
		{
			if ((0 < nCount) && (0 < size()))
			{
				const_pointer pUptr = _myPtr() + (nPos < size() ? nPos : size() - 1);
				for (; ; --pUptr)
				{
					if (traits::find(pStr, nCount, *pUptr) != nullptr)
					{
						return (pUptr - _myPtr());
					}
					else if (pUptr == _myPtr())
					{
						break;
					}
				}
			}
			return my_type::npos;
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find_last_of(const value_type ch, size_type nPos) const NOEXCEPT
		{
			return rfind(ch, nPos);
		}

		// Find first not of operations
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find_first_not_of(const my_type& str, size_type nPos) const NOEXCEPT
		{
			return find_first_not_of(str._myPtr(), nPos, str.size());
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find_first_not_of(const_pointer pStr, size_type nPos) const
		{
			return find_first_not_of(pStr, nPos, (size_type)traits::length(pStr));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find_first_not_of(const_pointer pStr, size_type nPos, size_type nCount) const
		{
			if (nPos <= size())
			{
				const_pointer const pVPtr = _myPtr() + size();
				for (const_pointer pUPtr = (_myPtr() + nPos); pUPtr < pVPtr; ++pUPtr)
				{
					if (traits::find(pStr, nCount, *pUPtr) == nullptr)
					{
						return (pUPtr - _myPtr());
					}
				}
			}
			return my_type::npos;
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find_first_not_of(const value_type ch, size_type nPos) const NOEXCEPT
		{
			return (find_first_not_of((const_pointer)&ch, nPos, 1));
		}

		// Find last not of operations
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find_last_not_of(const my_type& str, size_type nPos) const NOEXCEPT
		{
			return find_last_not_of(str._myPtr(), nPos, str.size());
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find_last_not_of(const_pointer pStr, size_type nPos) const
		{
			return find_last_not_of(pStr, nPos, (size_type)traits::length(pStr));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find_last_not_of(const_pointer pStr, size_type nPos, size_type nCount) const
		{
			if (0 < size())
			{
				const_pointer pUptr = _myPtr() + (nPos < size() ? nPos : size() - 1);
				for (; ; --pUptr)
				{
					if (traits::find(pStr, nCount, *pUptr) == 0)
					{
						return (pUptr - _myPtr());
					}
					else if (pUptr == _myPtr())
					{
						break;
					}
				}
			}
			return my_type::npos;
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::find_last_not_of(value_type ch, size_type nPos) const NOEXCEPT
		{
			return (find_last_not_of((const_pointer)&ch, nPos, 1));
		}

		// Searches
		template < typename charT, typename traits >
		inline bool StringBase< charT, traits >::starts_with(const_pointer pStr) const
		{
			size_type nLength = traits::length(pStr);
			if (nLength > length())
			{
				return false;
			}
			const FXD::S32 nRetVal = traits::compare(_myPtr(), pStr, nLength);
			return (nRetVal == 0);
		}
		template < typename charT, typename traits >
		inline bool StringBase< charT, traits >::starts_with(const my_type& str) const
		{
			return starts_with(str._myPtr());
		}
		template < typename charT, typename traits >
		inline bool StringBase< charT, traits >::starts_with_no_case(const_pointer pStr) const
		{
			size_type nLength = traits::length(pStr);
			if (nLength > length())
			{
				return false;
			}
			const FXD::S32 nRetVal = traits::compare_no_case(_myPtr(), pStr, nLength);
			return (nRetVal == 0);
		}
		template < typename charT, typename traits >
		inline bool StringBase< charT, traits >::starts_with_no_case(const my_type& str) const
		{
			return starts_with_no_case(str._myPtr());
		}

		template < typename charT, typename traits >
		inline bool StringBase< charT, traits >::ends_with(const_pointer pStr) const
		{
			size_type nLLen = length();
			size_type nRLen = traits::length(pStr);
			if (nRLen > nLLen)
			{
				return false;
			}
			for (size_type n1 = 1; n1 <= nRLen; n1++)
			{
				if (_myPtr()[nLLen - n1] != pStr[nRLen - n1])
				{
					return false;
				}
			}
			return true;
		}
		template < typename charT, typename traits >
		inline bool StringBase< charT, traits >::ends_with(const my_type& str) const
		{
			return ends_with(str._myPtr());
		}
		template < typename charT, typename traits >
		inline bool StringBase< charT, traits >::ends_with_no_case(const_pointer pStr) const
		{
			size_type strL = traits::length(pStr);
			size_type nLen = length();
			if (strL > nLen)
			{
				return false;
			}
			for (size_type n1 = 1; n1 <= strL; n1++)
			{
				if (traits::to_lower(pStr[strL - n1]) != traits::to_lower(_myPtr()[nLen - n1]))
				{
					return false;
				}
			}
			return true;
		}
		template < typename charT, typename traits >
		inline bool StringBase< charT, traits >::ends_with_no_case(const my_type& str) const
		{
			return ends_with_no_case(str._myPtr());
		}

		// Deletion
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::my_type& StringBase< charT, traits >::erase(size_type nPos, size_type nCount)
		{
			PRINT_COND_ASSERT(((size() < nPos) == false), "Core: Invalid string position");
			if ((size() - nPos) < nCount)
			{
				nCount = (size() - nPos); // trim nCount
			}
			if (0 < nCount)
			{ // move elements down
				traits::move((_myPtr() + nPos), (_myPtr() + nPos + nCount), (size() - nPos - nCount));
				size_type nNewsize = (size() - nCount);
				_eos(nNewsize);
			}
			return (*this);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::iterator StringBase< charT, traits >::erase(const_iterator pos)
		{
			size_type nCount = (pos - begin());
			erase(nCount, 1);
			return (begin() + nCount);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::iterator StringBase< charT, traits >::erase(const_iterator first, const_iterator last)
		{
			size_type nCount = (first - begin());
			erase(nCount, (last - first));
			return (begin() + nCount);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::reverse_iterator StringBase< charT, traits >::erase(reverse_iterator pos)
		{
			return StringBase< charT, traits >::reverse_iterator(erase((++pos).base()));
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::reverse_iterator StringBase< charT, traits >::erase(reverse_iterator first, reverse_iterator last)
		{
			return StringBase< charT, traits >::reverse_iterator(erase((++last).base(), (++first).base()));
		}
		template < typename charT, typename traits >
		inline void StringBase< charT, traits >::pop_back(void)
		{
			erase(this->size() - 1);
		}
		template < typename charT, typename traits >
		inline void StringBase< charT, traits >::clear(void)
		{
			_eos(0);
		}

		template < typename charT, typename traits >
		inline void StringBase< charT, traits >::resize(size_type nNewsize)
		{
			resize(nNewsize, charT());
		}
		template < typename charT, typename traits >
		inline void StringBase< charT, traits >::resize(size_type nNewsize, const value_type ch)
		{
			if (nNewsize <= size())
			{
				erase(nNewsize);
			}
			else
			{
				append((nNewsize - size()), ch);
			}
		}
		template < typename charT, typename traits >
		inline void StringBase< charT, traits >::reserve(size_type nNewcap)
		{
			if ((size() <= nNewcap) && (capacity() != nNewcap))
			{
				size_type nSize = size();
				if (_grow(nNewcap, true))
				{
					_eos(nSize);
				}
			}
		}

		// Capacity
		template < typename charT, typename traits >
		inline bool StringBase< charT, traits >::empty(void) const NOEXCEPT
		{
			return (size() == 0);
		}
		template < typename charT, typename traits >
		inline bool StringBase< charT, traits >::not_empty(void) const NOEXCEPT
		{
			return !empty();
		}
		template < typename charT, typename traits >
		inline bool StringBase< charT, traits >::validate(void) const NOEXCEPT
		{
			if ((_first() == nullptr) || (_last() == nullptr))
			{
				return false;
			}
			if (_last() < _first())
			{
				return false;
			}
			if (*_last() != 0)
			{
				return false;
			}
			return true;
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::length(void) const NOEXCEPT
		{
			return (m_nSize);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::size(void) const NOEXCEPT
		{
			return (m_nSize);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::capacity(void) const NOEXCEPT
		{
			return (m_nCapacity);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::max_size(void) const NOEXCEPT
		{
			size_type nCount = (size_type)(-1) / sizeof(charT);
			size_type nNum = ((0 < nCount) ? nCount : 1);
			return ((nNum <= 1) ? 1 : (nNum - 1));
		}

		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::size_type StringBase< charT, traits >::copy(charT* pStr, size_type nCount, size_type nOffSet) const
		{
			PRINT_COND_ASSERT(((size() < nOffSet) == false), "Core: Invalid string position");
			if ((size() - nOffSet) < nCount)
			{
				nCount = (size() - nOffSet);
			}
			traits::copy(pStr, _myPtr() + nOffSet, nCount);
			return nCount;
		}

		template < typename charT, typename traits >
		inline void StringBase< charT, traits >::swap(my_type& rhs)
		{
			FXD::STD::swap((*this), rhs);
		}

		/*
		template < typename charT, typename traits >
		inline void StringBase< charT, traits >::fromMemHandle(Memory::MemHandle& memHandle)
		{
			size_type nLength = memHandle.get_size() / sizeof(charT);
			charT* pStr = (charT*)Memory::MemHandle::LockGuard(memHandle).get_mem();
			resize(nLength);
			assign(pStr, nLength);
		}

		template < typename charT, typename traits >
		inline void StringBase< charT, traits >::readBinary(Core::IStreamInBinary& is)
		{
			size_type nLength = 0;
			is >> nLength;
			resize(nLength);
			is.m_pHandler->read_size(_myPtr(), nLength * sizeof(charT));
		}
		*/

		template < typename charT, typename traits >
		Container::Vector< bool > StringBase< charT, traits >::to_bool_array(const value_type delim) const
		{
			const_pointer pCurr = _myPtr();
			const_pointer pPrev = pCurr;
			Container::Vector< bool > retVec;

			while (*pCurr)
			{
				if (traits::equals(delim, (*pCurr)))
				{
					retVec.emplace_back(my_type(pPrev, pCurr).to_bool());
					pPrev = pCurr;
					pPrev++;
				}
				pCurr++;
			}
			retVec.emplace_back(my_type(pPrev).to_bool());
			return retVec;
		}
		template < typename charT, typename traits >
		Container::Vector< FXD::F32 > StringBase< charT, traits >::to_F32_array(const value_type delim) const
		{
			const_pointer pCurr = _myPtr();
			const_pointer pPrev = pCurr;
			Container::Vector< FXD::F32 > retVec;

			while (*pCurr)
			{
				if (traits::equals(delim, (*pCurr)))
				{
					retVec.emplace_back(my_type(pPrev, pCurr).to_F32());
					pPrev = pCurr;
					pPrev++;
				}
				pCurr++;
			}
			my_type str(pPrev);
			if (str.not_empty())
			{
				retVec.emplace_back(str.to_F32());
			}
			return retVec;
		}
		template < typename charT, typename traits >
		Container::Vector< FXD::S16 > StringBase< charT, traits >::to_S16_array(const value_type delim) const
		{
			const_pointer pCurr = _myPtr();
			const_pointer pPrev = pCurr;
			Container::Vector< FXD::S16 > retVec;

			while (*pCurr)
			{
				if (traits::equals(delim, (*pCurr)))
				{
					retVec.emplace_back(my_type(pPrev, pCurr).to_S16());
					pPrev = pCurr;
					pPrev++;
				}
				pCurr++;
			}
			my_type str(pPrev);
			if (str.not_empty())
			{
				retVec.emplace_back(str.to_S16());
			}
			return retVec;
		}
		template < typename charT, typename traits >
		Container::Vector< FXD::S32 > StringBase< charT, traits >::to_S32_array(const value_type delim) const
		{
			const_pointer pCurr = _myPtr();
			const_pointer pPrev = pCurr;
			Container::Vector< FXD::S32 > retVec;

			while (*pCurr)
			{
				if (traits::equals(delim, (*pCurr)))
				{
					retVec.emplace_back(my_type(pPrev, pCurr).to_S32());
					pPrev = pCurr;
					pPrev++;
				}
				pCurr++;
			}
			my_type str(pPrev);
			if (str.not_empty())
			{
				retVec.emplace_back(str.to_S32());
			}
			return retVec;
		}
		template < typename charT, typename traits >
		Container::Vector< StringBase< charT, traits > > StringBase< charT, traits >::to_str_array(const value_type delim) const
		{
			const_pointer pCurr = _myPtr();
			const_pointer pPrev = pCurr;
			Container::Vector< StringBase< charT, traits > > retVec;

			while (*pCurr)
			{
				value_type c = (*pCurr);
				if (traits::equals(delim, c))
				{
					retVec.emplace_back(pPrev, pCurr);
					pPrev = pCurr;
					pPrev++;
				}
				pCurr++;
			}
			my_type str(pPrev);
			if (str.not_empty())
			{
				retVec.emplace_back(str);
			}
			return retVec;
		}


		template < typename charT, typename traits >
		template < typename InputItr >
		typename StringBase< charT, traits >::size_type StringBase< charT, traits >::to_bool_array(const value_type delim, InputItr start, const size_type nMaxCount) const
		{
			const_pointer pCurr = _myPtr();
			const_pointer pPrev = pCurr;
			size_type nCount = 0;

			while ((nCount < nMaxCount) && (*pCurr))
			{
				if (traits::equals(delim, (*pCurr)))
				{
					(*start) = my_type(pPrev, pCurr).to_bool();
					pPrev = pCurr;
					pPrev++;
					start++;
					nCount++;
				}
				pCurr++;
			}
			if (nCount < nMaxCount)
			{
				(*start) = my_type(pPrev).to_bool();
				start++;
				nCount++;
			}
			return nCount;
		}
		template < typename charT, typename traits >
		template < typename InputItr >
		typename StringBase< charT, traits >::size_type StringBase< charT, traits >::to_F32_array(const value_type delim, InputItr start, const size_type nMaxCount) const
		{
			size_type nCount = 0;
			const_pointer pCurr = _myPtr();
			const_pointer pPrev = pCurr;

			while ((nCount < nMaxCount) && (*pCurr))
			{
				if (traits::equals(delim, (*pCurr)))
				{
					(*start) = my_type(pPrev, pCurr).to_F32();
					pPrev = pCurr;
					pPrev++;
					start++;
					nCount++;
				}
				pCurr++;
			}
			if (nCount < nMaxCount)
			{
				(*start) = my_type(pPrev).to_F32();
				start++;
				nCount++;
			}
			return nCount;
		}
		template < typename charT, typename traits >
		template < typename InputItr >
		typename StringBase< charT, traits >::size_type StringBase< charT, traits >::to_S16_array(const value_type delim, InputItr start, const size_type nMaxCount) const
		{
			size_type nCount = 0;
			const_pointer pCurr = _myPtr();
			const_pointer pPrev = pCurr;

			while ((nCount < nMaxCount) && (*pCurr))
			{
				if (traits::equals(delim, (*pCurr)))
				{
					(*start) = my_type(pPrev, pCurr).to_S16();
					pPrev = pCurr;
					pPrev++;
					start++;
					nCount++;
				}
				pCurr++;
			}
			if (nCount < nMaxCount)
			{
				(*start) = my_type(pPrev).to_S16();
				start++;
				nCount++;
			}
			return nCount;
		}
		template < typename charT, typename traits >
		template < typename InputItr >
		typename StringBase< charT, traits >::size_type StringBase< charT, traits >::to_S32_array(const value_type delim, InputItr start, const size_type nMaxCount) const
		{
			size_type nCount = 0;
			const_pointer pCurr = _myPtr();
			const_pointer pPrev = pCurr;

			while ((nCount < nMaxCount) && (*pCurr))
			{
				if (traits::equals::equals(delim, (*pCurr)))
				{
					(*start) = my_type(pPrev, pCurr).to_S32();
					pPrev = pCurr;
					pPrev++;
					start++;
					nCount++;
				}
				pCurr++;
			}
			if (nCount < nMaxCount)
			{
				(*start) = my_type(pPrev).to_S32();
				start++;
				nCount++;
			}
			return nCount;
		}
		template < typename charT, typename traits >
		template < typename InputItr >
		typename StringBase< charT, traits >::size_type StringBase< charT, traits >::to_str_array(const value_type delim, InputItr start, const size_type nMaxCount) const
		{
			size_type nCount = 0;
			const_pointer pCurr = _myPtr();
			const_pointer pPrev = pCurr;

			while ((nCount < nMaxCount) && (*pCurr))
			{
				if (traits::equals(delim, (*pCurr)))
				{
					(*start).assign(pPrev, pCurr);
					pPrev = pCurr;
					pPrev++;
					start++;
					nCount++;
				}
				pCurr++;
			}
			if (nCount < nMaxCount)
			{
				(*start).assign(pPrev);
				start++;
				nCount++;
			}
			return nCount;
		}

		template < typename charT, typename traits >
		template < typename NewT >
		StringBase< NewT > StringBase< charT, traits >::to_string(void) const
		{
			StringBase< NewT > strOut;
			to_string(strOut);
			return strOut;
		}

		template < typename charT, typename traits >
		inline void StringBase< charT, traits >::to_lower(void)
		{
			for (pointer p = _first(); p < _last(); ++p)
			{
				(*p) = traits::to_lower(*p);
			}
		}

		template < typename charT, typename traits >
		inline void StringBase< charT, traits >::to_upper(void)
		{
			for (pointer p = _first(); p < _last(); ++p)
			{
				(*p) = traits::to_upper(*p);
			}
		}

		template < typename charT, typename traits >
		inline void StringBase< charT, traits >::trim(void)
		{
			ltrim();
			rtrim();
		}

		template < typename charT, typename traits >
		inline void StringBase< charT, traits >::ltrim(const value_type* pStr)
		{
			erase(0, find_first_not_of(pStr));
		}

		template < typename charT, typename traits >
		inline void StringBase< charT, traits >::rtrim(const value_type* pStr)
		{
			erase(find_last_not_of(pStr) + 1);
		}

		template < typename charT, typename traits >
		inline void StringBase< charT, traits >::trim(const value_type* pStr)
		{
			ltrim(pStr);
			rtrim(pStr);
		}

		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::pointer StringBase< charT, traits >::_first(void) NOEXCEPT
		{
			return &_myPtr()[0];
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::const_pointer StringBase< charT, traits >::_first(void) const NOEXCEPT
		{
			return &_myPtr()[0];
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::pointer StringBase< charT, traits >::_last(void) NOEXCEPT
		{
			return &_myPtr()[size()];
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::const_pointer StringBase< charT, traits >::_last(void) const NOEXCEPT
		{
			return &_myPtr()[size()];
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::pointer StringBase< charT, traits >::_myPtr(void) NOEXCEPT
		{
			return ((_BUF_SIZE <= capacity()) ? _buffer.m_pPtr : _buffer.m_buffer);
		}
		template < typename charT, typename traits >
		inline typename StringBase< charT, traits >::const_pointer StringBase< charT, traits >::_myPtr(void) const NOEXCEPT
		{
			return ((_BUF_SIZE <= capacity()) ? _buffer.m_pPtr : _buffer.m_buffer);
		}

		template < typename charT, typename traits >
		template < typename InputItr >
		void StringBase< charT, traits >::_construct(InputItr first, InputItr last, FXD::STD::input_iterator_tag)
		{
			for (; first != last; ++first)
			{
				append((size_type)1, (value_type)*first);
			}
		}
		template < typename charT, typename traits >
		template < typename InputItr >
		void StringBase< charT, traits >::_construct(InputItr first, InputItr last, FXD::STD::forward_iterator_tag)
		{
			size_type nCount = FXD::STD::distance(first, last);
			reserve(nCount);
			_construct(first, last, FXD::STD::input_iterator_tag());
		}
		template < typename charT, typename traits >
		void StringBase< charT, traits >::_construct(const_pointer pFirst, const_pointer pLast, FXD::STD::random_access_iterator_tag)
		{
			if (pFirst != pLast)
			{
				assign(pFirst, (pLast - pFirst));
			}
		}

		template < typename charT, typename traits >
		void StringBase< charT, traits >::_assign_rv(my_type&& rhs)
		{
			if (rhs.capacity() < _BUF_SIZE)
			{
				traits::move(_buffer.m_buffer, rhs._buffer.m_buffer, (rhs.size() + 1));
			}
			else
			{
				_buffer.m_pPtr = rhs._buffer.m_pPtr;
				rhs._buffer.m_pPtr = pointer();
			}
			m_nSize = rhs.size();
			m_nCapacity = rhs.capacity();
			rhs._clean();
		}

		template < typename charT, typename traits >
		void StringBase< charT, traits >::_copy(size_type nNewSize, size_type nOldLen)
		{
			size_type nNewres = (nNewSize | _ALLOC_MASK);
			if (max_size() < nNewres)
			{
				nNewres = nNewSize;
			}
			else if ((capacity() / 2) <= (nNewres / 3))
			{
			}
			else if (capacity() <= max_size() - capacity() / 2)
			{
				nNewres = (capacity() + capacity() / 2);
			}
			else
			{
				nNewres = max_size();
			}

			charT* pStr = (charT*)m_alloc.allocate((nNewres + 1) * sizeof(charT));
			if (0 < nOldLen)
			{
				traits::copy(pStr, _myPtr(), nOldLen);
			}
			_clean(true);
			_buffer.m_pPtr = pStr;
			m_nCapacity = nNewres;
			_eos(nOldLen);
		}

		template < typename charT, typename traits >
		void StringBase< charT, traits >::_eos(size_type nNewSize)
		{
			m_nSize = nNewSize;
			traits::assign((_myPtr() + size()), charT(), 1);
		}

		template < typename charT, typename traits >
		bool StringBase< charT, traits >::_grow(size_type nNewSize, bool bTrim)
		{
			PRINT_COND_ASSERT(((max_size() < nNewSize) == false), "Core: String too long");
			if (capacity() < nNewSize)
			{
				_copy(nNewSize, size()); // reallocate to grow
			}
			else if ((bTrim) && (nNewSize < _BUF_SIZE))
			{
				_clean(true, (nNewSize < size()) ? nNewSize : size());// copy and deallocate if trimming to small string
			}
			else if (nNewSize == 0)
			{
				_eos(0); // new size is zero, just null terminate
			}
			return (0 < nNewSize); // return true only if more work to do
		}

		template < typename charT, typename traits >
		bool StringBase< charT, traits >::_is_inside(const_pointer pStr)
		{
			return !((pStr == nullptr) || (pStr < _myPtr()) || ((_myPtr() + size()) <= pStr));
		}

		template < typename charT, typename traits >
		void StringBase< charT, traits >::_clean(bool bBuilt, size_type nNewSize)
		{
			if ((bBuilt) && (_BUF_SIZE <= capacity()))
			{ // copy any leftovers to small buffer and deallocate
				charT* pStr = _buffer.m_pPtr;
				if (0 < nNewSize)
				{
					traits::copy(_buffer.m_buffer, pStr, nNewSize);
				}
				m_alloc.deallocate(pStr, (m_nCapacity + 1));
			}
			m_nCapacity = (_BUF_SIZE - 1);
			_eos(nNewSize);
		}

		// Non-member comparison functions
		// Equality
		template < typename charT, typename traits >
		inline bool operator==(const StringBase< charT, traits >& lhs, const StringBase< charT, traits >& rhs)
		{
			return (lhs.compare(rhs) == 0);
		}
		template < typename charT, typename traits >
		inline bool operator==(const charT* pLhs, const StringBase< charT, traits >& rhs)
		{
			return (rhs.compare(pLhs) == 0);
		}
		template < typename charT, typename traits >
		inline bool operator==(const StringBase< charT, traits >& lhs, const charT* pRhs)
		{
			return (lhs.compare(pRhs) == 0);
		}

		// Inequality
		template < typename charT, typename traits >
		inline bool operator!=(const StringBase< charT, traits >& lhs, const StringBase< charT, traits >& rhs)
		{
			return (!(lhs == rhs));
		}
		template < typename charT, typename traits >
		inline bool operator!=(const charT* pLhs, const StringBase< charT, traits >& rhs)
		{
			return (!(pLhs == rhs));
		}
		template < typename charT, typename traits >
		inline bool operator!=(const StringBase< charT, traits >& lhs, const charT* pRhs)
		{
			return (!(lhs == pRhs));
		}

		// Less than
		template < typename charT, typename traits >
		inline bool operator<(const StringBase< charT, traits >& lhs, const StringBase< charT, traits >& rhs)
		{
			return (lhs.compare(rhs) < 0);
		}
		template < typename charT, typename traits >
		inline bool operator<(const charT* pLhs, const StringBase< charT, traits >& rhs)
		{
			return (rhs.compare(pLhs) > 0);
		}
		template < typename charT, typename traits >
		inline bool operator<(const StringBase< charT, traits >& lhs, const charT* pRhs)
		{
			return (lhs.compare(pRhs) < 0);
		}

		// Greater than
		template < typename charT, typename traits >
		inline bool operator>(const StringBase< charT, traits >& lhs, const StringBase< charT, traits >& rhs)
		{
			return (rhs < lhs);
		}
		template < typename charT, typename traits >
		inline bool operator>(const charT* pLhs, const StringBase< charT, traits >& rhs)
		{
			return (rhs < pLhs);
		}
		template < typename charT, typename traits >
		inline bool operator>(const StringBase< charT, traits >& lhs, const charT* pRhs)
		{
			return (pRhs < lhs);
		}

		// Less than or equal to
		template < typename charT, typename traits >
		inline bool operator<=(const StringBase< charT, traits >& lhs, const StringBase< charT, traits >& rhs)
		{
			return (!(rhs < lhs));
		}
		template < typename charT, typename traits >
		inline bool operator<=(const charT* pLhs, const StringBase< charT, traits >& rhs)
		{
			return (!(rhs < pLhs));
		}
		template < typename charT, typename traits >
		inline bool operator<=(const StringBase< charT, traits >& lhs, const charT* pRhs)
		{
			return (!(*pRhs < lhs));
		}

		// Greater than or equal to
		template < typename charT, typename traits >
		inline bool operator>=(const StringBase< charT, traits >& lhs, const StringBase< charT, traits >& rhs)
		{
			return (!(lhs < rhs));
		}
		template < typename charT, typename traits >
		inline bool operator>=(const charT* pLhs, const StringBase< charT, traits >& rhs)
		{
			return (!(pLhs < rhs));
		}
		template < typename charT, typename traits >
		inline bool operator>=(const StringBase< charT, traits >& lhs, const charT* pRhs)
		{
			return (!(lhs < pRhs));
		}

		// 
		template < typename charT, typename traits >
		StringBase< charT, traits > operator+(const StringBase< charT, traits >& lhs, const StringBase< charT, traits >& rhs)
		{
			return StringBase< charT, traits >(lhs).append(rhs);
		}
		template < typename charT, typename traits >
		StringBase< charT, traits > operator+(const charT* pLhs, const StringBase< charT, traits >& rhs)
		{
			return StringBase< charT, traits >(pLhs) + rhs;;
		}
		template < typename charT, typename traits >
		StringBase< charT, traits > operator+(charT lhs, const StringBase< charT, traits >& rhs)
		{
			return StringBase< charT, traits >(lhs, 1) + rhs;
		}
		template < typename charT, typename traits >
		StringBase< charT, traits > operator+(const StringBase< charT, traits >& lhs, charT* pRhs)
		{
			return lhs + StringBase< charT, traits >(pRhs);
		}
		template < typename charT, typename traits >
		StringBase< charT, traits > operator+(const StringBase< charT, traits >& lhs, charT rhs)
		{
			return lhs + StringBase< charT, traits >(lhs, 1);
		}
		template < typename charT, typename traits >
		StringBase< charT, traits > operator+(StringBase< charT, traits >&& lhs, const StringBase< charT, traits >& rhs)
		{
			return std::move(lhs.append(rhs));
		}
		template < typename charT, typename traits >
		StringBase< charT, traits > operator+(const StringBase< charT, traits >& lhs, StringBase< charT, traits >&& rhs)
		{
			return std::move(rhs.insert(0, lhs));
		}
		template < typename charT, typename traits >
		StringBase< charT, traits > operator+(StringBase< charT, traits >&& lhs, StringBase< charT, traits >&& rhs)
		{
			return std::move(lhs.append(rhs));
		}
		template < typename charT, typename traits >
		StringBase< charT, traits > operator+(const charT* pLhs, StringBase< charT>&& rhs)
		{
			return std::move(rhs.insert(0, pLhs));
		}
		template < typename charT, typename traits >
		StringBase< charT, traits > operator+(charT lhs, StringBase< charT, traits >&& rhs)
		{
			return std::move(rhs.insert(0, 1, lhs));
		}
		template < typename charT, typename traits >
		StringBase< charT, traits > operator+(StringBase< charT, traits >&& lhs, const charT* pRhs)
		{
			return std::move(lhs.append(pRhs));
		}
		template < typename charT, typename traits >
		StringBase< charT, traits > operator+(StringBase< charT, traits >&& lhs, charT rhs)
		{
			return std::move(lhs.append(1, rhs));
		}


		template < FXD::U32 Size, typename N >
		inline auto operator+(const FXD::UTF8(&str)[Size], const N& rhs)
		{
			return String8(str) + rhs;
		}
		template < FXD::U32 Size, typename N >
		inline auto operator+(const FXD::UTF16(&str)[Size], const N& rhs)
		{
			return String16(str) + rhs;
		}
#		
		// Inserters and extractors
		template < typename charT, typename traits >
		inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, Core::StringBase< charT, traits > const& rhs)
		{
			return ostr << rhs.c_str();
		}
		template < typename charT, typename traits >
		inline Core::IStreamIn& operator>>(Core::IStreamIn& istr, Core::StringBase< charT, traits >& rhs)
		{
			FXD::U32 nLength;
			istr >> nLength;
			rhs.resize(nLength);
			istr.read_size((void*)rhs.c_str(), (nLength * sizeof(charT)));
			return istr;
		}

		extern Core::String8 encode_string(const Memory::MemHandle& mem);
		extern Memory::MemHandle decode_string(const Core::String8& str);


		namespace Literals
		{
			inline String8 operator""_S8(const FXD::UTF8* pStr, size_t nLen) NOEXCEPT
			{
				return String8(pStr, (FXD::U32)nLen);
			}
			inline String16 operator""_S16(const FXD::UTF16* pStr, size_t nLen) NOEXCEPT
			{
				return String16(pStr, (FXD::U32)nLen);
			}
		} //namespace Literals


		// ------
		// StringBuilderBase
		// -
		// ------
		template < typename charT, typename traits >
		class StringBuilderBase FINAL : public Core::IStreamOut, public Core::ISerializeOutString< charT >
		{
		public:
			using my_type = StringBuilderBase< charT, traits >;
			using string_type = StringBase< charT, traits >;
			using size_type = FXD::U32;

		public:
			StringBuilderBase(void);
			~StringBuilderBase(void);

			StringBuilderBase(const my_type& rhs);
			StringBuilderBase(my_type&& rhs);

			my_type& operator=(const my_type& rhs);
			my_type& operator=(my_type&& rhs);

			template < typename Type >
			static string_type to_string(const Type& t)
			{
				Core::StringBuilderBase< charT > sb;
				sb << t;
				return sb.str();
			}

			static string_type format_time(FXD::F32 fTime)
			{
				Core::StringBuilderBase< charT > strTime;
				FXD::F32 fSecs = fTime;
				FXD::S32 nDeci = (FXD::S32)(fSecs * 100.0f) % 100;
				FXD::S32 nSecs = (FXD::S32)fSecs;
				FXD::S32 nMins = (nSecs / 60);
				nSecs %= 60;
				if (nMins < 10)
				{
					strTime << "0";
				}
				strTime << (FXD::S32)nMins << ":";
				if (nSecs < 10)
				{
					strTime << "0";
				}
				strTime << nSecs << ":";
				if (nDeci < 10)
				{
					strTime << "0";
				}
				strTime << (FXD::S32)nDeci;
				return strTime.str();
			}

			const string_type& str(void) const NOEXCEPT
			{
				return m_string;
			}

			size_type length(void) const NOEXCEPT
			{
				return m_string.length();
			}

		private:
			FXD::U64 write_size(const void* pData, const FXD::U64 nSize)
			{
				m_string.append((const charT*)pData, nSize);
				return length();
			}

		private:
			string_type m_string;
		};

		// ------
		// StringBuilderBase
		// -
		// ------
		template < typename charT, typename traits >
		StringBuilderBase< charT, traits >::StringBuilderBase(void)
			: IStreamOut(this, Core::CompressParams::Default)
		{}
		template < typename charT, typename traits >
		StringBuilderBase< charT, traits >::~StringBuilderBase(void)
		{}

		template < typename charT, typename traits >
		StringBuilderBase< charT, traits >::StringBuilderBase(const my_type& rhs)
			: IStreamOut(this, Core::CompressParams::Default)
			, m_string(rhs.m_string)
		{}
		template < typename charT, typename traits >
		StringBuilderBase< charT, traits >::StringBuilderBase(my_type&& rhs)
			: IStreamOut(this, Core::CompressParams::Default)
			, m_string(std::move(rhs.m_string))
		{}

		template < typename charT, typename traits >
		typename StringBuilderBase< charT, traits >::my_type& StringBuilderBase< charT, traits >::operator=(const my_type& rhs)
		{
			if (this != &rhs)
			{
				m_string = rhs.m_string;
			}
			return (*this);
		}
		template < typename charT, typename traits >
		typename StringBuilderBase< charT, traits >::my_type& StringBuilderBase< charT, traits >::operator=(my_type&& rhs)
		{
			if (this != &rhs)
			{
				m_string(std::forward< string_type >(rhs.m_string));
			}
			return (*this);
		}

		// to_string8
		template < typename Type >
		inline Core::String8 _to_string8(const Type& type)
		{
			Core::StringBuilder8 str;
			str << type;
			return str.str();
		}
		inline Core::String8 to_string8(bool b)
		{
			return _to_string8< bool >(b);
		}
		inline Core::String8 to_string8(const FXD::UTF8* pStr)
		{
			return _to_string8< const FXD::UTF8* >(pStr);
		}
		inline Core::String8 to_string8(const FXD::UTF16* pStr)
		{
			return _to_string8< const FXD::UTF16* >(pStr);
		}
		inline Core::String8 to_string8(FXD::UTF8 v)
		{
			return _to_string8< FXD::UTF8 >(v);
		}
		inline Core::String8 to_string8(FXD::S16 v)
		{
			return _to_string8< FXD::S16 >(v);
		}
		inline Core::String8 to_string8(FXD::U16 v)
		{
			return _to_string8< FXD::U16 >(v);
		}
		inline Core::String8 to_string8(FXD::S32 v)
		{
			return _to_string8< FXD::S32 >(v);
		}
		inline Core::String8 to_string8(FXD::U32 v)
		{
			return _to_string8< FXD::U32 >(v);
		}
		inline Core::String8 to_string8(FXD::S64 v)
		{
			return _to_string8< FXD::S64 >(v);
		}
		inline Core::String8 to_string8(FXD::U64 v)
		{
			return _to_string8< FXD::U64 >(v);
		}
		inline Core::String8 to_string8(FXD::F32 f)
		{
			return _to_string8< FXD::F32 >(f);
		}
		inline Core::String8 to_string8(FXD::F64 f)
		{
			return _to_string8< FXD::F64 >(f);
		}

		// to_string16
		template < typename Type >
		inline Core::String16 _to_string16(const Type& type)
		{
			Core::StringBuilder16 str;
			str << type;
			return str.str();
		}
		inline Core::String16 to_string16(bool b)
		{
			return _to_string16< bool >(b);
		}
		inline Core::String16 to_string16(const FXD::UTF8* pStr)
		{
			return _to_string16< const FXD::UTF8* >(pStr);
		}
		inline Core::String16 to_string16(const FXD::UTF16* pStr)
		{
			return _to_string16< const FXD::UTF16* >(pStr);
		}
		inline Core::String16 to_string16(FXD::UTF8 v)
		{
			return _to_string16< FXD::UTF8 >(v);
		}
		inline Core::String16 to_string16(FXD::S16 v)
		{
			return _to_string16< FXD::S16 >(v);
		}
		inline Core::String16 to_string16(FXD::U16 v)
		{
			return _to_string16< FXD::U16 >(v);
		}
		inline Core::String16 to_string16(FXD::S32 v)
		{
			return _to_string16< FXD::S32 >(v);
		}
		inline Core::String16 to_string16(FXD::U32 v)
		{
			return _to_string16< FXD::U32 >(v);
		}
		inline Core::String16 to_string16(FXD::S64 v)
		{
			return _to_string16< FXD::S64 >(v);
		}
		inline Core::String16 to_string16(FXD::U64 v)
		{
			return _to_string16< FXD::U64 >(v);
		}
		inline Core::String16 to_string16(FXD::F32 f)
		{
			return _to_string16< FXD::F32 >(f);
		}
		inline Core::String16 to_string16(FXD::F64 f)
		{
			return _to_string16< FXD::F64 >(f);
		}

	} //namespace Core

	namespace STD
	{
		template < typename charT, typename traits, typename U >
		void erase(Core::StringBase< charT, traits >& str, const U& val)
		{
			str.erase(FXD::STD::remove(str.begin(), str.end(), val), str.end());
		}

		template < typename charT, typename traits, typename Pred >
		void erase_if(Core::StringBase< charT, traits >& str, Pred pred)
		{
			str.erase(FXD::STD::remove_if(str.begin(), str.end(), pred), str.end());
		}

	} //namespace STD
} //namespace FXD

using FXD::Core::Literals::operator"" _S8;
using FXD::Core::Literals::operator"" _S16;

#endif //FXDENGINE_CORE_STRING_H