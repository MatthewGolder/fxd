// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_ANDROID_ANDROID_H
#define FXDENGINE_CORE_ANDROID_ANDROID_H

#include "FXDEngine/Core/Types.h"

#if IsOSAndroid()

	#include <unistd.h>
	#include <machine/_types.h>
	#include <jni.h>
	#include <android/native_window.h> // requires ndk r5 or newer
	#include <android/native_window_jni.h> // requires ndk r5 or newer

#endif //IsOSAndroid()
#endif //FXDENGINE_CORE_ANDROID_ANDROID_H