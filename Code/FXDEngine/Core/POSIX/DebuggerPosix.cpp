// Creator - MatthewGolder
#include "FXDEngine/Core/Types.h"

#if IsSystemPosix()
#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Core/Internal/Debugger.h"
#include <cstdio>

#if IsOSAndroid()
#include <android/log.h>
#endif

using namespace FXD;
using namespace Core;

#if defined(ENABLE_DEBUGGER)
// ------
// DBGStreamOut
// -
// ------
FXD::U64 DBGStreamOut::write_size(const void* pData, const FXD::U64 /*nSize*/)
{
	m_impl->m_str.append((const FXD::UTF8*)pData);
	return 0;
}

void DBGStreamOut::apply(Core::E_DBGLevel eLevel)
{
	m_eLevel = eLevel;
#if !IsOSAndroid()
	switch (m_eLevel)
	{
		case FXD::Core::E_DBGLevel::None:
		{
			break;
		}
		case FXD::Core::E_DBGLevel::Info:
		{
			_do_print("[Info] ");
			break;
		}
		case FXD::Core::E_DBGLevel::Warn:
		{
			_do_print("[Warn] ");
			break;
		}
		case FXD::Core::E_DBGLevel::Error:
		{
			_do_print("[Error] ");
			break;
		}
		case FXD::Core::E_DBGLevel::Assert:
		{
			_do_print("[Assert] ");
			break;
		}
		default:
		{
			break;
		}
	}
#endif
}

void DBGStreamOut::_do_print(const FXD::UTF8* pStr) const
{
#if IsOSAndroid()
	FXD::U32 nLevel = 0;
	switch (m_eLevel)
	{
		case FXD::Core::E_DBGLevel::None:
		{
			nLevel = ANDROID_LOG_VERBOSE;
			break;
		}
		case FXD::Core::E_DBGLevel::Info:
		{
			nLevel = ANDROID_LOG_INFO;
			break;
		}
		case FXD::Core::E_DBGLevel::Warn:
		{
			nLevel = ANDROID_LOG_WARN;
			break;
		}
		case FXD::Core::E_DBGLevel::Error:
		{
			nLevel = ANDROID_LOG_ERROR;
			break;
		}
		case FXD::Core::E_DBGLevel::Assert:
		{
			nLevel = ANDROID_LOG_FATAL;
			break;
		}
		default:
		{
			break;
		}
	}
	__android_log_print(nLevel, "FXD", "%s", pStr);
#else
	std::printf("%s", pStr);
#endif
}

void DBGStreamOut::_break_application(void) const
{
	abort();
}
#endif //defined(ENABLE_DEBUGGER)
#endif //IsSystemPosix()