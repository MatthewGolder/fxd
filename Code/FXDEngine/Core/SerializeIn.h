// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_SERILIZEIN_H
#define FXDENGINE_CORE_SERILIZEIN_H

#include "FXDEngine/Core/Types.h"

namespace FXD
{
	namespace Core
	{
		class IStreamIn;

		// ------
		// ISerializeIn
		// -
		// ------
		class ISerializeIn
		{
		public:
			friend class IStreamIn;

		public:
			ISerializeIn(void);
			virtual ~ISerializeIn(void);

		protected:
			virtual void _pass(FXD::UTF8* pVal, Core::IStreamIn* pStream) PURE;
			virtual void _pass(FXD::UTF16* pVal, Core::IStreamIn* pStream) PURE;

			virtual void _pass(bool& val, Core::IStreamIn* pStream) PURE;
			virtual void _pass(FXD::UTF8& val, Core::IStreamIn* pStream) PURE;

			virtual void _pass(FXD::S8& nVal, Core::IStreamIn* pStream) PURE;
			virtual void _pass(FXD::U8& nVal, Core::IStreamIn* pStream) PURE;

			virtual void _pass(FXD::S16& nVal, Core::IStreamIn* pStream) PURE;
			virtual void _pass(FXD::U16& nVal, Core::IStreamIn* pStream) PURE;

			virtual void _pass(FXD::S32& nVal, Core::IStreamIn* pStream) PURE;
			virtual void _pass(FXD::U32& nVal, Core::IStreamIn* pStream) PURE;

			virtual void _pass(FXD::S64& nVal, Core::IStreamIn* pStream) PURE;
			virtual void _pass(FXD::U64& nVal, Core::IStreamIn* pStream) PURE;

			virtual void _pass(FXD::F32& fVal, Core::IStreamIn* pStream) PURE;
			virtual void _pass(FXD::F64& fVal, Core::IStreamIn* pStream) PURE;
		};
	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_SERILIZEIN_H