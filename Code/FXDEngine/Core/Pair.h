// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_PAIR_H
#define FXDENGINE_CORE_PAIR_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Decay.h"
#include "FXDEngine/Core/Internal/EnableIf.h"
#include "FXDEngine/Core/Internal/IntegerSequence.h"
#include "FXDEngine/Core/Internal/IsArray.h"
#include "FXDEngine/Core/Internal/IsConstructible.h"
#include "FXDEngine/Core/Internal/IsConvertible.h"
#include "FXDEngine/Core/Internal/IsCopyConstructible.h"
#include "FXDEngine/Core/Internal/IsDefaultConstructible.h"
#include "FXDEngine/Core/Internal/IsNoThrowCopyAssignable.h"
#include "FXDEngine/Core/Internal/IsNoThrowMoveAssignable.h"
#include "FXDEngine/Core/Internal/IsNoThrowSwappable.h"
#include "FXDEngine/Core/Internal/PiecewiseConstruct.h"
#include "FXDEngine/Core/Internal/RemoveReferenceWrapper.h"
#include "FXDEngine/Core/Internal/Swap.h"
#include "FXDEngine/Container/Tuple.h"
#include <utility>

namespace FXD
{
	namespace Core
	{
		// ------
		// Pair
		// -
		// ------
		template < typename T1, typename T2 >
		class Pair
		{
		public:
			using first_type = T1;
			using second_type = T2;
			using my_type = Pair< T1, T2 >;

		public:
			template <
				typename UT1 = T1,
				typename UT2 = T2,
				typename = typename FXD::STD::enable_if< FXD::STD::is_default_constructible< UT1 >::value && FXD::STD::is_default_constructible< UT2 >::value >::type >
			CONSTEXPR Pair(void)
				: first()
				, second()
			{}

			template <
				typename UT2 = T2,
				typename = typename FXD::STD::enable_if< FXD::STD::is_default_constructible< UT2 >::value >::type >
			CONSTEXPR14 Pair(const T1& f)
				: first(f)
				, second()
			{}

			/*
			template <
				typename UT2 = T2,
				typename = typename FXD::STD::enable_if< FXD::STD::is_default_constructible< UT2 >::value >::type >
			CONSTEXPR14 Pair(T1&& f)
				: first(std::move(f))
				, second()
			{}
			*/

			template <
				typename UT1 = T1,
				typename UT2 = T2,
				typename =
				typename FXD::STD::enable_if< FXD::STD::is_copy_constructible< UT1 >::value && FXD::STD::is_copy_constructible< UT2 >::value >::type,
				typename FXD::STD::enable_if< FXD::STD::is_convertible< const UT1&, UT1 >::value && FXD::STD::is_convertible< const UT2&, UT2 >::value, FXD::S32 >::type = 0 >
			CONSTEXPR14 Pair(const T1& f, const T2& s)
				: first(f)
				, second(s)
			{}

			template <
				typename UT1 = T1,
				typename UT2 = T2,
				typename =
				typename FXD::STD::enable_if< FXD::STD::is_copy_constructible< UT1 >::value && FXD::STD::is_copy_constructible< UT2 >::value >::type,
				typename FXD::STD::enable_if< !FXD::STD::is_convertible< const UT1&, UT1 >::value || !FXD::STD::is_convertible< const UT2&, UT2 >::value, FXD::S32 >::type = 0 >
			CONSTEXPR14 EXPLICIT Pair(const T1& f, const T2& s)
				: first(f)
				, second(s)
			{}

			template <
				typename UT1,
				typename UT2,
				typename =
				typename FXD::STD::enable_if< FXD::STD::is_constructible< T1, const UT1& >::value && FXD::STD::is_constructible< T2, const UT2& >::value >::type,
				typename FXD::STD::enable_if< FXD::STD::is_convertible< const UT1&, T1 >::value && FXD::STD::is_convertible< const UT2&, T2 >::value, FXD::S32 >::type = 0 >
			CONSTEXPR14 Pair(const Pair< UT1, UT2 >& rhs)
				: first(rhs.first)
				, second(rhs.second)
			{}

			template <
				typename UT1,
				typename UT2,
				typename =
				typename FXD::STD::enable_if< FXD::STD::is_constructible< T1, const UT1& >::value && FXD::STD::is_constructible< T2, const UT2& >::value >::type,
				typename FXD::STD::enable_if< !FXD::STD::is_convertible< const UT1&, T1 >::value || !FXD::STD::is_convertible< const UT2&, T2 >::value, FXD::S32 >::type = 0 >
			CONSTEXPR14 EXPLICIT Pair(const Pair< UT1, UT2 >& rhs)
				: first(rhs.first)
				, second(rhs.second)
			{}

			template <
				typename UT1,
				typename UT2,
				typename =
				typename FXD::STD::enable_if< FXD::STD::is_constructible< T1, UT1 >::value && FXD::STD::is_constructible< T2, UT2 >::value >::type,
				typename FXD::STD::enable_if< FXD::STD::is_convertible< UT1, T1 >::value && FXD::STD::is_convertible< UT2, T2 >::value, FXD::S32 >::type = 0 >
			CONSTEXPR14 Pair(UT1&& f, UT2&& s) NOEXCEPT_IF((FXD::STD::is_nothrow_constructible< T1, UT1 >::value && FXD::STD::is_nothrow_constructible< T2, UT2 >::value))
				: first(std::forward< UT1 >(f))
				, second(std::forward< UT2 >(s))
			{}

			template <
				typename UT1,
				typename UT2,
				typename =
				typename FXD::STD::enable_if< FXD::STD::is_constructible< T1, UT1 >::value && FXD::STD::is_constructible< T2, UT2 >::value >::type,
				typename FXD::STD::enable_if< !FXD::STD::is_convertible< UT1, T1 >::value || !FXD::STD::is_convertible< UT2, T2 >::value, FXD::S32 >::type = 0 >
				CONSTEXPR14 EXPLICIT Pair(UT1&& f, UT2&& s) NOEXCEPT_IF((FXD::STD::is_nothrow_constructible< T1, UT1 >::value && FXD::STD::is_nothrow_constructible< T2, UT2 >::value))
				: first(std::forward< UT1 >(f))
				, second(std::forward< UT2 >(s))
			{}
			
			template <
				typename UT1,
				typename UT2,
				typename =
				typename FXD::STD::enable_if< FXD::STD::is_constructible< T1, UT1 >::value && FXD::STD::is_constructible< T2, UT2 >::value >::type,
				typename FXD::STD::enable_if< FXD::STD::is_convertible< UT1, T1 >::value && FXD::STD::is_convertible< UT2, T2 >::value, FXD::S32 >::type = 0 >
			CONSTEXPR14 Pair(Pair< UT1, UT2 >&& rhs) NOEXCEPT_IF((FXD::STD::is_nothrow_constructible< T1, UT1 >::value && FXD::STD::is_nothrow_constructible< T2, UT2 >::value))
				: first(std::forward< UT1 >(rhs.first))
				, second(std::forward< UT2 >(rhs.second))
			{}

			template <
				typename UT1,
				typename UT2,
				typename =
				typename FXD::STD::enable_if< FXD::STD::is_constructible< T1, UT1 >::value && FXD::STD::is_constructible< T2, UT2 >::value >::type,
				typename FXD::STD::enable_if< !FXD::STD::is_convertible< UT1, T1 >::value || !FXD::STD::is_convertible< UT2, T2 >::value, FXD::S32 >::type = 0 >
			CONSTEXPR14 EXPLICIT Pair(Pair< UT1, UT2 >&& rhs) NOEXCEPT_IF((FXD::STD::is_nothrow_constructible< T1, UT1 >::value && FXD::STD::is_nothrow_constructible< T2, UT2 >::value))
				: first(std::forward< UT1 >(rhs.first))
				, second(std::forward< UT2 >(rhs.second))
			{}


			CONSTEXPR14 Pair(const Pair& rhs) NOEXCEPT_IF(FXD::STD::is_nothrow_copy_assignable< T1 >::value && FXD::STD::is_nothrow_copy_assignable< T2 >::value)
				: first(rhs.first)
				, second(rhs.second)
			{}

			CONSTEXPR14 Pair(Pair&& rhs) NOEXCEPT_IF(FXD::STD::is_nothrow_move_assignable< T1 >::value && FXD::STD::is_nothrow_move_assignable< T2 >::value)
				: first(std::forward< T1 >(rhs.first))
				, second(std::forward< T2 >(rhs.second))
			{}

			template <
				typename U,
				typename = typename FXD::STD::enable_if< FXD::STD::is_convertible< U, T1 >::value >::type >
			CONSTEXPR14 Pair(U&& f, const T2& s)
				: first(std::forward< U >(f))
				, second(s)
			{}

			template <
				typename V,
				typename = typename FXD::STD::enable_if< FXD::STD::is_convertible< V, T2 >::value >::type >
			CONSTEXPR14 Pair(const T1& f, V&& s)
				: first(f)
				, second(std::forward< V >(s))
			{}

			template <
				typename... Args1,
				typename... Args2,
				typename = typename FXD::STD::enable_if< FXD::STD::is_constructible< first_type, Args1&&... >::value && FXD::STD::is_constructible< second_type, Args2&&... >::value >::type >
			Pair(FXD::STD::piecewise_construct_t pwc, FXD::Container::Tuple< Args1... > first, FXD::Container::Tuple< Args2 ...> second)
				: Pair(	pwc,
						std::move(first),
						std::move(second),
						FXD::STD::make_index_sequence< sizeof...(Args1) >(),
						FXD::STD::make_index_sequence< sizeof...(Args2) >()
				)
			{}

			Pair& operator=(const Pair& rhs) NOEXCEPT_IF(FXD::STD::is_nothrow_copy_assignable< T1 >::value && FXD::STD::is_nothrow_copy_assignable< T2 >::value)
			{
				first = rhs.first;
				second = rhs.second;
				return (*this);
			}

			template <
				typename UT1,
				typename UT2,
				typename = typename FXD::STD::enable_if< FXD::STD::is_convertible< UT1, T1 >::value && FXD::STD::is_convertible< UT2, T2 >::value >::type >
			Pair& operator=(const Pair< UT1, UT2 >& rhs)
			{
				first = rhs.first;
				second = rhs.second;
				return (*this);
			}

			Pair& operator=(Pair&& rhs) NOEXCEPT_IF(FXD::STD::is_nothrow_move_assignable< T1 >::value && FXD::STD::is_nothrow_move_assignable< T2 >::value)
			{
				first = std::forward< T1 >(rhs.first);
				second = std::forward< T2 >(rhs.second);
				return (*this);
			}

			template <
				typename UT1,
				typename UT2,
				typename = typename FXD::STD::enable_if< FXD::STD::is_convertible< UT1, T1 >::value && FXD::STD::is_convertible< UT2, T2 >::value >::type >
				Pair& operator=(Pair< UT1, UT2 >&& rhs)
			{
				first = std::forward< UT1 >(rhs.first);
				second = std::forward< UT2 >(rhs.second);
				return (*this);
			}

			void swap(Pair& rhs) NOEXCEPT_IF(FXD::STD::is_nothrow_swappable< T1 >::value && FXD::STD::is_nothrow_swappable< T2 >::value)
			{
				FXD::STD::iter_swap(&first, &rhs.first);
				FXD::STD::iter_swap(&second, &rhs.second);
			}

		public:
			T1 first;
			T2 second;

		private:
			template < typename... Args1, typename... Args2, size_t... I1, size_t... I2 >
			Pair(FXD::STD::piecewise_construct_t, FXD::Container::Tuple< Args1... > first, FXD::Container::Tuple< Args2... > second, FXD::STD::index_sequence< I1... >, FXD::STD::index_sequence< I2... >)
				: first(std::forward< Args1 >(FXD::get< I1 >(first))...)
				, second(std::forward< Args2 >(FXD::get< I2 >(second))...)
			{}
		};


		template < typename T1, typename T2 >
		CONSTEXPR14 inline bool operator==(const Pair< T1, T2 >& lhs, const Pair< T1, T2 >& rhs)
		{
			return ((lhs.first == rhs.first) && (lhs.second == rhs.second));
		}

		template < typename T1, typename T2 >
		CONSTEXPR14 inline bool operator<(const Pair< T1, T2 >& lhs, const Pair< T1, T2 >& rhs)
		{
			return ((lhs.first < rhs.first) || (!(rhs.first < lhs.first) && (lhs.second < rhs.second)));
		}

		template < typename T1, typename T2 >
		CONSTEXPR14 inline bool operator!=(const Pair< T1, T2 >& lhs, const Pair< T1, T2 >& rhs)
		{
			return !(lhs == rhs);
		}

		template < typename T1, typename T2 >
		CONSTEXPR14 inline bool operator>(const Pair< T1, T2 >& lhs, const Pair< T1, T2 >& rhs)
		{
			return (rhs < lhs);
		}

		template < typename T1, typename T2 >
		CONSTEXPR14 inline bool operator>=(const Pair< T1, T2 >& lhs, const Pair< T1, T2 >& rhs)
		{
			return !(lhs < rhs);
		}

		template < typename T1, typename T2 >
		CONSTEXPR14 inline bool operator<=(const Pair< T1, T2 >& lhs, const Pair< T1, T2 >& rhs)
		{
			return !(rhs < lhs);
		}

		template < typename T1, typename T2 >
		CONSTEXPR14 inline Core::Pair< typename FXD::STD::remove_reference_wrapper< typename FXD::STD::decay< T1 >::type >::type, typename FXD::STD::remove_reference_wrapper< typename FXD::STD::decay< T2 >::type >::type > make_pair(T1&& lhs, T2&& rhs)
		{
			using T1Type = typename FXD::STD::remove_reference_wrapper< typename FXD::STD::decay< T1 >::type >::type;
			using T2Type = typename FXD::STD::remove_reference_wrapper< typename FXD::STD::decay< T2 >::type >::type;

			return Core::Pair< T1Type, T2Type >(std::forward< T1 >(lhs), std::forward< T2 >(rhs));
		}

		template < typename T1, typename T2 >
		CONSTEXPR14 inline Core::Pair< T1, T2 > make_pair(const T1& lhs, const T2& rhs, typename FXD::STD::enable_if< !FXD::STD::is_array< T1 >::value && !FXD::STD::is_array< T2 >::value >::type* = 0)
		{
			return Core::Pair< T1, T2 >(lhs, rhs);
		}

	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_PAIR_H