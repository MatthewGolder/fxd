// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_EXPRESSIONTOKENISER_H
#define FXDENGINE_CORE_EXPRESSIONTOKENISER_H

#include "FXDEngine/Core/String.h"
#include "FXDEngine/Container/Vector.h"

namespace FXD
{
	namespace Core
	{
		// ------
		// E_TokenType
		// -
		// ------
		enum class E_TokenType
		{
			Number,
			Variable,
			LeftParenthesis,
			RightParenthesis,
			Add,
			Minus,
			Multiply,
			Divide,
			And,
			Or,
			Greater,
			Less,
			GreaterEqual,
			LessEqual,
			IsEqual,
			NotEqual,
			Not,
			Question,
			Colon,
			Count,
			Unknown = 0xffff
		};

		// ------
		// ExpressionTokeniser
		// -
		// ------
		class ExpressionTokeniser
		{			
		public:
			
			// ------
			// Token
			// -
			// ------
			class Token
			{
			public:
				Token(void)
				{}
				~Token(void)
				{}

			public:
				Core::E_TokenType m_eType;
				Core::String8 m_strText;
			};

		public:
			ExpressionTokeniser(const Core::String8& strExpression);
			~ExpressionTokeniser(void);
			
			const Container::Vector< Token >& get_tokens(void) const;
			
			static Core::String8 token_type_to_string(Core::E_TokenType eToken);

		private:
			Container::Vector< Token > m_tokens;
		};
	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_EXPRESSIONTOKENISER_H
