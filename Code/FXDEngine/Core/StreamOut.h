// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_STREAMOUT_H
#define FXDENGINE_CORE_STREAMOUT_H

#include "FXDEngine/Core/Compression.h"
#include <memory>

namespace FXD
{
	namespace Core
	{
		class ISerializeOut;
		class IStreamCompressor;
		// ------
		// IStreamOut
		// -
		// ------
		class IStreamOut
		{
		public:
			friend class Core::ISerializeOut;

		public:
			IStreamOut(Core::ISerializeOut* pSerializeOut, Core::CompressParams compressParams = Core::CompressParams::Default);
			virtual ~IStreamOut(void);

			// Stream methods.
			Core::IStreamOut& operator<<(const FXD::UTF8* pVal);
			Core::IStreamOut& operator<<(const FXD::UTF16* pVal);

			Core::IStreamOut& operator<<(const bool val);
			Core::IStreamOut& operator<<(const FXD::UTF8 val);

			Core::IStreamOut& operator<<(const FXD::S8 nVal);
			Core::IStreamOut& operator<<(const FXD::U8 nVal);

			Core::IStreamOut& operator<<(const FXD::S16 nVal);
			Core::IStreamOut& operator<<(const FXD::U16 nVal);

			Core::IStreamOut& operator<<(const FXD::S32 nVal);
			Core::IStreamOut& operator<<(const FXD::U32 nVal);

			Core::IStreamOut& operator<<(const FXD::S64 nVal);
			Core::IStreamOut& operator<<(const FXD::U64 nVal);

			Core::IStreamOut& operator<<(const FXD::F32 fVal);
			Core::IStreamOut& operator<<(const FXD::F64 fVal);

			virtual FXD::U64 write_size(const void* pData, const FXD::U64 nSize) PURE;
			FXD::U64 compress_size(const void* pData, const FXD::U64 nSize);

		protected:
			void _set_serialize_out(Core::ISerializeOut* pSerializeOut);
			void _set_compressor_out(CompressParams compressParams);

		protected:
			Core::ISerializeOut* m_pSerializeOut;
			Core::IStreamCompressor* m_pCompressor;
		};
		using StreamOut = std::shared_ptr< Core::IStreamOut >;

	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_STREAMOUT_H