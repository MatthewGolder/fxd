// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_COLOUR_H
#define FXDENGINE_CORE_COLOUR_H

#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Core/String.h"

namespace FXD
{
	namespace Core
	{
		class IStreamOut;
			
		// ------
		// ColourRGB
		// -
		// ------
		class ColourRGB
		{
		public:
			const static ColourRGB AliceBlue;
			const static ColourRGB AntiqueWhite;
			const static ColourRGB Aqua;
			const static ColourRGB Aquamarine;
			const static ColourRGB Azure;
			const static ColourRGB Beige;
			const static ColourRGB Bisque;
			const static ColourRGB Black;
			const static ColourRGB BlanchedAlmond;
			const static ColourRGB Blue;
			const static ColourRGB BlueViolet;
			const static ColourRGB Brown;
			const static ColourRGB BurlyWood;
			const static ColourRGB CadetBlue;
			const static ColourRGB Chartreuse;
			const static ColourRGB Chocolate;
			const static ColourRGB Coral;
			const static ColourRGB CornflowerBlue;
			const static ColourRGB Cornsilk;
			const static ColourRGB Crimson;
			const static ColourRGB Cyan;
			const static ColourRGB DarkBlue;
			const static ColourRGB DarkCyan;
			const static ColourRGB DarkGoldenrod;
			const static ColourRGB DarkGray;
			const static ColourRGB DarkGreen;
			const static ColourRGB DarkKhaki;
			const static ColourRGB DarkMagenta;
			const static ColourRGB DarkOliveGreen;
			const static ColourRGB DarkOrange;
			const static ColourRGB DarkOrchid;
			const static ColourRGB DarkRed;
			const static ColourRGB DarkSalmon;
			const static ColourRGB DarkSeaGreen;
			const static ColourRGB DarkSlateBlue;
			const static ColourRGB DarkSlateGray;
			const static ColourRGB DarkTurquoise;
			const static ColourRGB DarkViolet;
			const static ColourRGB DeepPink;
			const static ColourRGB DeepSkyBlue;
			const static ColourRGB DimGray;
			const static ColourRGB DodgerBlue;
			const static ColourRGB Firebrick;
			const static ColourRGB FloralWhite;
			const static ColourRGB ForestGreen;
			const static ColourRGB Fuchsia;
			const static ColourRGB Gainsboro;
			const static ColourRGB GhostWhite;
			const static ColourRGB Gold;
			const static ColourRGB Goldenrod;
			const static ColourRGB Gray;
			const static ColourRGB Green;
			const static ColourRGB GreenYellow;
			const static ColourRGB Honeydew;
			const static ColourRGB HotPink;
			const static ColourRGB IndianRed;
			const static ColourRGB Indigo;
			const static ColourRGB Ivory;
			const static ColourRGB Khaki;
			const static ColourRGB Lavender;
			const static ColourRGB LavenderBlush;
			const static ColourRGB LawnGreen;
			const static ColourRGB LemonChiffon;
			const static ColourRGB LightBlue;
			const static ColourRGB LightCoral;
			const static ColourRGB LightCyan;
			const static ColourRGB LightGoldenrodYellow;
			const static ColourRGB LightGreen;
			const static ColourRGB LightGray;
			const static ColourRGB LightPink;
			const static ColourRGB LightSalmon;
			const static ColourRGB LightSeaGreen;
			const static ColourRGB LightSkyBlue;
			const static ColourRGB LightSlateGray;
			const static ColourRGB LightSteelBlue;
			const static ColourRGB LightYellow;
			const static ColourRGB Lime;
			const static ColourRGB LimeGreen;
			const static ColourRGB Linen;
			const static ColourRGB Magenta;
			const static ColourRGB Maroon;
			const static ColourRGB MediumAquamarine;
			const static ColourRGB MediumBlue;
			const static ColourRGB MediumOrchid;
			const static ColourRGB MediumPurple;
			const static ColourRGB MediumSeaGreen;
			const static ColourRGB MediumSlateBlue;
			const static ColourRGB MediumSpringGreen;
			const static ColourRGB MediumTurquoise;
			const static ColourRGB MediumVioletRed;
			const static ColourRGB MidnightBlue;
			const static ColourRGB MintCream;
			const static ColourRGB MistyRose;
			const static ColourRGB Moccasin;
			const static ColourRGB NavajoWhite;
			const static ColourRGB Navy;
			const static ColourRGB OldLace;
			const static ColourRGB Olive;
			const static ColourRGB OliveDrab;
			const static ColourRGB Orange;
			const static ColourRGB OrangeRed;
			const static ColourRGB Orchid;
			const static ColourRGB PaleGoldenrod;
			const static ColourRGB PaleGreen;
			const static ColourRGB PaleTurquoise;
			const static ColourRGB PaleVioletRed;
			const static ColourRGB PapayaWhip;
			const static ColourRGB PeachPuff;
			const static ColourRGB Peru;
			const static ColourRGB Pink;
			const static ColourRGB Plum;
			const static ColourRGB PowderBlue;
			const static ColourRGB Purple;
			const static ColourRGB Red;
			const static ColourRGB RosyBrown;
			const static ColourRGB RoyalBlue;
			const static ColourRGB SaddleBrown;
			const static ColourRGB Salmon;
			const static ColourRGB SandyBrown;
			const static ColourRGB SeaGreen;
			const static ColourRGB SeaShell;
			const static ColourRGB Sienna;
			const static ColourRGB Silver;
			const static ColourRGB SkyBlue;
			const static ColourRGB SlateBlue;
			const static ColourRGB SlateGray;
			const static ColourRGB Snow;
			const static ColourRGB SpringGreen;
			const static ColourRGB SteelBlue;
			const static ColourRGB Tan;
			const static ColourRGB Teal;
			const static ColourRGB Thistle;
			const static ColourRGB Tomato;
			const static ColourRGB Turquoise;
			const static ColourRGB Violet;
			const static ColourRGB Wheat;
			const static ColourRGB White;
			const static ColourRGB WhiteSmoke;
			const static ColourRGB Yellow;
			const static ColourRGB YellowGreen;

		public:
			ColourRGB(void);
			ColourRGB(const ColourRGB& rhs);
			ColourRGB(FXD::F32 r, FXD::F32 g, FXD::F32 b);
			~ColourRGB(void);

			ColourRGB& operator=(const ColourRGB& rhs);

			FXD::F32 operator[](FXD::S32 nID) const;
			FXD::F32& operator[](const FXD::S32 nID);

			bool operator==(const ColourRGB& rhs) const;
			bool operator!=(const ColourRGB& rhs) const;

			inline FXD::F32 getR(void) const;
			inline FXD::F32 getG(void) const;
			inline FXD::F32 getB(void) const;

			inline void set(const ColourRGB& rhs);
			inline void set(const FXD::F32 r, const FXD::F32 g, const FXD::F32 b);
			inline void setR(const FXD::F32 fVal);
			inline void setG(const FXD::F32 fVal);
			inline void setB(const FXD::F32 fVal);

			static ColourRGB from_hex(FXD::U8 r, FXD::U8 g, FXD::U8 b);
			static ColourRGB from_string(const Core::String8& str, FXD::UTF8 delim);
			static ColourRGB random(void);

		private:
			FXD::F32 m_r;
			FXD::F32 m_g;
			FXD::F32 m_b;
		};

		// ------
		// ColourRGB
		// -
		// ------
		inline ColourRGB::ColourRGB(void)
			: m_r(0.0f)
			, m_g(0.0f)
			, m_b(0.0f)
		{}
		inline ColourRGB::ColourRGB(const ColourRGB& rhs)
			: m_r(rhs.m_r)
			, m_g(rhs.m_g)
			, m_b(rhs.m_b)
		{}
		inline ColourRGB::ColourRGB(FXD::F32 r, FXD::F32 g, FXD::F32 b)
			: m_r(r)
			, m_g(g)
			, m_b(b)
		{}
		inline ColourRGB::~ColourRGB(void)
		{}

		inline ColourRGB& ColourRGB::operator=(const ColourRGB& rhs)
		{
			if (this == &rhs)
			{
				return (*this);
			}
			m_r = rhs.m_r;
			m_g = rhs.m_g;
			m_b = rhs.m_b;
			return (*this);
		}

		inline FXD::F32 ColourRGB::operator[](FXD::S32 nID) const
		{
			PRINT_COND_ASSERT((nID >= 0) && (nID < 3), "Core: ");
			return ((FXD::F32*)this)[nID];
		}
		inline FXD::F32& ColourRGB::operator[](const FXD::S32 nID)
		{
			PRINT_COND_ASSERT((nID >= 0) && (nID < 3), "Core: ");
			return ((FXD::F32*)this)[nID];
		}

		inline bool ColourRGB::operator==(const ColourRGB& rhs) const
		{
			return (
				(m_r == rhs.m_r) &&
				(m_g == rhs.m_g) &&
				(m_b == rhs.m_b));
		}
		inline bool ColourRGB::operator!=(const ColourRGB& rhs) const
		{
			return (
				(m_r != rhs.m_r) ||
				(m_g != rhs.m_g) ||
				(m_b != rhs.m_b));
		}

		inline FXD::F32 ColourRGB::getR(void) const
		{
			return m_r;
		}
		inline FXD::F32 ColourRGB::getG(void) const
		{
			return m_g;
		}
		inline FXD::F32 ColourRGB::getB(void) const
		{
			return m_b;
		}

		inline void ColourRGB::set(const ColourRGB& rhs)
		{
			setR(rhs.m_r);
			setG(rhs.m_g);
			setB(rhs.m_b);
		}
		inline void ColourRGB::set(const FXD::F32 r, const FXD::F32 g, const FXD::F32 b)
		{
			setR(r);
			setG(g);
			setB(b);
		}
		inline void ColourRGB::setR(const FXD::F32 fVal)
		{
			m_r = fVal;
		}
		inline void ColourRGB::setG(const FXD::F32 fVal)
		{
			m_g = fVal;
		}
		inline void ColourRGB::setB(const FXD::F32 fVal)
		{
			m_b = fVal;
		}

		// IStreamOut
		inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, ColourRGB const& rhs);

		// ------
		// ColourRGBA
		// -
		// ------
		class ColourRGBA
		{
		public:
			const static ColourRGBA AliceBlue;
			const static ColourRGBA AntiqueWhite;
			const static ColourRGBA Aqua;
			const static ColourRGBA Aquamarine;
			const static ColourRGBA Azure;
			const static ColourRGBA Beige;
			const static ColourRGBA Bisque;
			const static ColourRGBA Black;
			const static ColourRGBA BlanchedAlmond;
			const static ColourRGBA Blue;
			const static ColourRGBA BlueViolet;
			const static ColourRGBA Brown;
			const static ColourRGBA BurlyWood;
			const static ColourRGBA CadetBlue;
			const static ColourRGBA Chartreuse;
			const static ColourRGBA Chocolate;
			const static ColourRGBA Coral;
			const static ColourRGBA CornflowerBlue;
			const static ColourRGBA Cornsilk;
			const static ColourRGBA Crimson;
			const static ColourRGBA Cyan;
			const static ColourRGBA DarkBlue;
			const static ColourRGBA DarkCyan;
			const static ColourRGBA DarkGoldenrod;
			const static ColourRGBA DarkGray;
			const static ColourRGBA DarkGreen;
			const static ColourRGBA DarkKhaki;
			const static ColourRGBA DarkMagenta;
			const static ColourRGBA DarkOliveGreen;
			const static ColourRGBA DarkOrange;
			const static ColourRGBA DarkOrchid;
			const static ColourRGBA DarkRed;
			const static ColourRGBA DarkSalmon;
			const static ColourRGBA DarkSeaGreen;
			const static ColourRGBA DarkSlateBlue;
			const static ColourRGBA DarkSlateGray;
			const static ColourRGBA DarkTurquoise;
			const static ColourRGBA DarkViolet;
			const static ColourRGBA DeepPink;
			const static ColourRGBA DeepSkyBlue;
			const static ColourRGBA DimGray;
			const static ColourRGBA DodgerBlue;
			const static ColourRGBA Firebrick;
			const static ColourRGBA FloralWhite;
			const static ColourRGBA ForestGreen;
			const static ColourRGBA Fuchsia;
			const static ColourRGBA Gainsboro;
			const static ColourRGBA GhostWhite;
			const static ColourRGBA Gold;
			const static ColourRGBA Goldenrod;
			const static ColourRGBA Gray;
			const static ColourRGBA Green;
			const static ColourRGBA GreenYellow;
			const static ColourRGBA Honeydew;
			const static ColourRGBA HotPink;
			const static ColourRGBA IndianRed;
			const static ColourRGBA Indigo;
			const static ColourRGBA Ivory;
			const static ColourRGBA Khaki;
			const static ColourRGBA Lavender;
			const static ColourRGBA LavenderBlush;
			const static ColourRGBA LawnGreen;
			const static ColourRGBA LemonChiffon;
			const static ColourRGBA LightBlue;
			const static ColourRGBA LightCoral;
			const static ColourRGBA LightCyan;
			const static ColourRGBA LightGoldenrodYellow;
			const static ColourRGBA LightGreen;
			const static ColourRGBA LightGray;
			const static ColourRGBA LightPink;
			const static ColourRGBA LightSalmon;
			const static ColourRGBA LightSeaGreen;
			const static ColourRGBA LightSkyBlue;
			const static ColourRGBA LightSlateGray;
			const static ColourRGBA LightSteelBlue;
			const static ColourRGBA LightYellow;
			const static ColourRGBA Lime;
			const static ColourRGBA LimeGreen;
			const static ColourRGBA Linen;
			const static ColourRGBA Magenta;
			const static ColourRGBA Maroon;
			const static ColourRGBA MediumAquamarine;
			const static ColourRGBA MediumBlue;
			const static ColourRGBA MediumOrchid;
			const static ColourRGBA MediumPurple;
			const static ColourRGBA MediumSeaGreen;
			const static ColourRGBA MediumSlateBlue;
			const static ColourRGBA MediumSpringGreen;
			const static ColourRGBA MediumTurquoise;
			const static ColourRGBA MediumVioletRed;
			const static ColourRGBA MidnightBlue;
			const static ColourRGBA MintCream;
			const static ColourRGBA MistyRose;
			const static ColourRGBA Moccasin;
			const static ColourRGBA NavajoWhite;
			const static ColourRGBA Navy;
			const static ColourRGBA OldLace;
			const static ColourRGBA Olive;
			const static ColourRGBA OliveDrab;
			const static ColourRGBA Orange;
			const static ColourRGBA OrangeRed;
			const static ColourRGBA Orchid;
			const static ColourRGBA PaleGoldenrod;
			const static ColourRGBA PaleGreen;
			const static ColourRGBA PaleTurquoise;
			const static ColourRGBA PaleVioletRed;
			const static ColourRGBA PapayaWhip;
			const static ColourRGBA PeachPuff;
			const static ColourRGBA Peru;
			const static ColourRGBA Pink;
			const static ColourRGBA Plum;
			const static ColourRGBA PowderBlue;
			const static ColourRGBA Purple;
			const static ColourRGBA Red;
			const static ColourRGBA RosyBrown;
			const static ColourRGBA RoyalBlue;
			const static ColourRGBA SaddleBrown;
			const static ColourRGBA Salmon;
			const static ColourRGBA SandyBrown;
			const static ColourRGBA SeaGreen;
			const static ColourRGBA SeaShell;
			const static ColourRGBA Sienna;
			const static ColourRGBA Silver;
			const static ColourRGBA SkyBlue;
			const static ColourRGBA SlateBlue;
			const static ColourRGBA SlateGray;
			const static ColourRGBA Snow;
			const static ColourRGBA SpringGreen;
			const static ColourRGBA SteelBlue;
			const static ColourRGBA Tan;
			const static ColourRGBA Teal;
			const static ColourRGBA Thistle;
			const static ColourRGBA Tomato;
			const static ColourRGBA Transparent;
			const static ColourRGBA Turquoise;
			const static ColourRGBA Violet;
			const static ColourRGBA Wheat;
			const static ColourRGBA White;
			const static ColourRGBA WhiteSmoke;
			const static ColourRGBA Yellow;
			const static ColourRGBA YellowGreen;
			const static ColourRGBA Alpha;
			const static ColourRGBA Null;

		public:
			ColourRGBA(void);
			ColourRGBA(const ColourRGBA& rhs);
			ColourRGBA(const ColourRGB& rhs, FXD::F32 a = 1.0f);
			ColourRGBA(FXD::F32 r, FXD::F32 g, FXD::F32 b, FXD::F32 a = 1.0f);
			~ColourRGBA(void);

			ColourRGBA& operator=(const ColourRGBA& rhs);

			FXD::F32 operator[](FXD::S32 nID) const;
			FXD::F32& operator[](const FXD::S32 nID);

			bool operator==(const ColourRGBA& rhs) const;
			bool operator!=(const ColourRGBA& rhs) const;

			inline FXD::F32 getR(void) const;
			inline FXD::F32 getG(void) const;
			inline FXD::F32 getB(void) const;
			inline FXD::F32 getA(void) const;

			inline void set(const ColourRGBA& rhs);
			inline void set(const FXD::F32 r, const FXD::F32 g, const FXD::F32 b, const FXD::F32 a);
			inline void setR(const FXD::F32 fVal);
			inline void setG(const FXD::F32 fVal);
			inline void setB(const FXD::F32 fVal);
			inline void setA(const FXD::F32 fVal);

			static ColourRGBA from_hex(FXD::U8 r, FXD::U8 g, FXD::U8 b, FXD::U8 a);
			static ColourRGBA from_string(const Core::String8& str, FXD::UTF8 delim);
			static ColourRGBA random(void);

		private:
			FXD::F32 m_r;
			FXD::F32 m_g;
			FXD::F32 m_b;
			FXD::F32 m_a;
		};

		// ------
		// Colour
		// -
		// ------
		inline ColourRGBA::ColourRGBA(void)
			: m_r(0.0f)
			, m_g(0.0f)
			, m_b(0.0f)
			, m_a(0.0f)
		{}
		inline ColourRGBA::ColourRGBA(const ColourRGBA& rhs)
			: m_r(rhs.m_r)
			, m_g(rhs.m_g)
			, m_b(rhs.m_b)
			, m_a(rhs.m_a)
		{}
		inline ColourRGBA::ColourRGBA(const ColourRGB& rhs, FXD::F32 a)
			: m_r(rhs.getR())
			, m_g(rhs.getG())
			, m_b(rhs.getB())
			, m_a(a)
		{}
		inline ColourRGBA::ColourRGBA(FXD::F32 r, FXD::F32 g, FXD::F32 b, FXD::F32 a)
			: m_r(r)
			, m_g(g)
			, m_b(b)
			, m_a(a)
		{}
		inline ColourRGBA::~ColourRGBA(void)
		{}

		inline ColourRGBA& ColourRGBA::operator=(const ColourRGBA& rhs)
		{
			if (this != &rhs)
			{
				m_r = rhs.m_r;
				m_g = rhs.m_g;
				m_b = rhs.m_b;
				m_a = rhs.m_a;
			}
			return (*this);
		}

		inline FXD::F32 ColourRGBA::operator[](FXD::S32 nID) const
		{
			PRINT_COND_ASSERT((nID >= 0) && (nID < 4), "Core: ");
			return ((FXD::F32*)this)[nID];
		}
		inline FXD::F32& ColourRGBA::operator[](const FXD::S32 nID)
		{
			PRINT_COND_ASSERT((nID >= 0) && (nID < 4), "Core: ");
			return ((FXD::F32*)this)[nID];
		}

		inline bool ColourRGBA::operator==(const ColourRGBA& rhs) const
		{
			return (
				(m_r == rhs.m_r) &&
				(m_g == rhs.m_g) &&
				(m_b == rhs.m_b) &&
				(m_a == rhs.m_a));
		}
		inline bool ColourRGBA::operator!=(const ColourRGBA& rhs) const
		{
			return (
				(m_r != rhs.m_r) ||
				(m_g != rhs.m_g) ||
				(m_b != rhs.m_b) ||
				(m_a != rhs.m_a));
		}

		inline FXD::F32 ColourRGBA::getR(void) const
		{
			return m_r;
		}
		inline FXD::F32 ColourRGBA::getG(void) const
		{
			return m_g;
		}
		inline FXD::F32 ColourRGBA::getB(void) const
		{
			return m_b;
		}
		inline FXD::F32 ColourRGBA::getA(void) const
		{
			return m_a;
		}

		inline void ColourRGBA::set(const ColourRGBA& rhs)
		{
			setR(rhs.m_r);
			setG(rhs.m_g);
			setB(rhs.m_b);
			setA(rhs.m_a);
		}
		inline void ColourRGBA::set(const FXD::F32 r, const FXD::F32 g, const FXD::F32 b, const FXD::F32 a)
		{
			setR(r);
			setG(g);
			setB(b);
			setA(a);
		}
		inline void ColourRGBA::setR(const FXD::F32 fVal)
		{
			m_r = fVal;
		}
		inline void ColourRGBA::setG(const FXD::F32 fVal)
		{
			m_g = fVal;
		}
		inline void ColourRGBA::setB(const FXD::F32 fVal)
		{
			m_b = fVal;
		}
		inline void ColourRGBA::setA(const FXD::F32 fVal)
		{
			m_a = fVal;
		}

		inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, ColourRGBA const& rhs);

	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_COLOUR_H