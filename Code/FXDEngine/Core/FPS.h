// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_FPS_H
#define FXDENGINE_CORE_FPS_H

#include "FXDEngine/Core/Types.h"

namespace FXD
{
	namespace Core
	{
		// ------
		// FPS
		// -
		// ------
		class FPS
		{
		public:
			FPS(void);
			~FPS(void);

			void update(const FXD::F32 fFrameTime, const FXD::UTF8* pName = nullptr);

		private:
			FXD::U32 m_nFrameCount;
			FXD::F32 m_fTime;
		};
	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_FPS_H