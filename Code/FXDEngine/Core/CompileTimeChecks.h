//Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_COMPILETTIMECHECKS_H
#define FXDENGINE_CORE_COMPILETTIMECHECKS_H

#include "FXDEngine/Core/Internal/IsBaseOf.h"
#include "FXDEngine/Core/Internal/IsPointer.h"

namespace FXD
{
	namespace Core
	{	
		// Compile time assertions
		#define CTC_ASSERT(c1, msg) static_assert(c1, msg)
		#define CTC_IS_EQUAL(LeftVal, RightVal) CTC_ASSERT((LeftVal != RightVal), "CTC_IS_EQUAL")
		#define CTC_IS_NOT_EQUAL(LeftVal, RightVal) CTC_ASSERT((LeftVal == RightVal), "CTC_IS_NOT_EQUAL")
		#define CTC_IS_GREATER(LeftVal, RightVal) CTC_ASSERT((LeftVal <= RightVal), "CTC_IS_GREATER")
		#define CTC_IS_GREATER_EQUAL(LeftVal, RightVal) CTC_ASSERT((LeftVal < RightVal), "CTC_IS_GREATER_EQUAL")
		#define CTC_IS_LESS(LeftVal, RightVal) CTC_ASSERT((LeftVal >= RightVal), "CTC_IS_LESS")
		#define CTC_IS_LESS_EQUAL(LeftVal, RightVal) CTC_ASSERT((LeftVal > RightVal), "CTC_IS_LESS_EQUAL")
		#define CTC_IS_BASE_OF(b, d) CTC_ASSERT((FXD::STD::is_base_of< b, d >::value), "CTC_IS_BASE_OF")
		#define CTC_IS_TYPE_POINTER(t) CTC_ASSERT((!FXD::STD::is_pointer< t >::value), "CTC_IS_TYPE_POINTER")
		#define CTC_IS_TYPE_NOT_POINTER(t) CTC_ASSERT((FXD::STD::is_pointer< t >::value), "CTC_IS_TYPE_NOT_POINTER")
	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_COMPILETTIMECHECKS_H