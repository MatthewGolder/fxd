// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_TYPES_H
#define FXDENGINE_CORE_TYPES_H

#include "FXDEngine/Core/Internal/Config.h"
#include <cstddef>

namespace FXD
{
#if IsOSPC()
//#	define FXD_PLATFORM_STEAM

	typedef unsigned __int8 U8;
	typedef signed __int8 S8;

	typedef unsigned __int16 U16;
	typedef signed __int16 S16;

	typedef unsigned __int32 U32;
	typedef signed __int32 S32;

	typedef unsigned __int64 U64;
	typedef signed __int64 S64;

	typedef ptrdiff_t PtrDiff;

#elif IsOSAndroid()
	typedef unsigned char U8;
	typedef signed char S8;

	typedef unsigned short U16;
	typedef signed short S16;

	typedef unsigned int U32;
	typedef signed int S32;

	typedef unsigned long long U64;
	typedef signed long long S64;

	typedef ptrdiff_t PtrDiff;

#endif

	typedef float F32;
	typedef long double F64;

	typedef char UTF8;					//< Compiler independent 8 bit Unicode encoded character
	typedef char16_t UTF16;				//< Compiler independent 16 bit Unicode encoded character
	typedef char32_t UTF32;				//< Compiler independent 32 bit Unicode encoded character
	//typedef unsigned short UTF16;		//< Compiler independent 16 bit Unicode encoded character
	//typedef unsigned int UTF32;		//< Compiler independent 32 bit Unicode encoded character

	typedef FXD::U64 HashValue;

#ifndef _UINTPTR_T_DEFINED
	template < int Size > struct PtrSizedIntT
	{};
	template <> struct PtrSizedIntT< sizeof(unsigned int) >
	{
		using Type = unsigned int;
	};
	template <> struct PtrSizedIntT< sizeof(unsigned long long) >
	{
		using Type = unsigned long long;
	};
	using PtrSizedInt = PtrSizedIntT< sizeof(void*) >::Type;
#else
	using PtrSizedInt = uintptr_t;
#endif

#ifndef BIT
#	define BIT(x) (1 << (x))
#endif

	const FXD::UTF8 PATH_COLON = ':';
	const FXD::UTF8 PATH_PERIOD = '.';
#if IsOSPC()
	const FXD::UTF8 PATH_SEPERATOR = '\\';
	const FXD::UTF8 PATH_NOT_SEPERATOR = '/';
#else
	const FXD::UTF8 PATH_SEPERATOR = '/';
	const FXD::UTF8 PATH_NOT_SEPERATOR = '\\';
#endif
#	define IS_PATH_SEPERATOR(x)	((x) == PATH_NOT_SEPERATOR || (x) == PATH_SEPERATOR)

#	define UTF_8(s) (FXD::UTF8*)(s)
//#	define UTF_16(s) (FXD::UTF16*)(L##s)
#	define UTF_16(s) (FXD::UTF16*)(u##s)
//#	define UTF_32(s) (FXD::UTF32*)(U##s)

} //namespace FXD
#endif //FXDENGINE_CORE_TYPES_H