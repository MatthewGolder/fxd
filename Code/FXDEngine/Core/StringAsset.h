// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_STRINGASSET_H
#define FXDENGINE_CORE_STRINGASSET_H

#include "FXDEngine/Core/String.h"

namespace FXD
{
	namespace Core
	{
		// ------
		// StringAsset
		// -
		// ------
		class StringAsset
		{
		public:
			inline EXPLICIT StringAsset(const Core::String8& strID)
				: m_strID(strID)
			{}
			~StringAsset(void)
			{}

			Core::String16 operator()() const;
			Core::String16 operator()(const Core::String16* pArgs, FXD::U32 nArgCount) const;

			inline Core::String16 operator()(const Core::String16& arg1) const { return (*this)(&arg1, 1); }
			inline Core::String16 operator()(const Core::String16& arg1, const Core::String16& arg2) const { Core::String16 args[] = { arg1, arg2 }; return (*this)(args, 2); }
			inline Core::String16 operator()(const Core::String16& arg1, const Core::String16& arg2, const Core::String16& arg3) const { Core::String16 args[] = { arg1, arg2, arg3 }; return (*this)(args, 3); }
			inline Core::String16 operator()(const Core::String16& arg1, const Core::String16& arg2, const Core::String16& arg3, const Core::String16& arg4) const { Core::String16 args[] = { arg1, arg2, arg3, arg4 }; return (*this)(args, 4); }
			inline Core::String16 operator()(const Core::String16& arg1, const Core::String16& arg2, const Core::String16& arg3, const Core::String16& arg4, const Core::String16& arg5) const { Core::String16 args[] = { arg1, arg2, arg3, arg4, arg5 }; return (*this)(args, 5); }

		public:
			Core::String8 m_strID;
		};

	} //namespace Core
} //namespace FXD

#define UTF_SA(s) FXD::Core::StringAsset(UTF_8(s))

#endif //FXDENGINE_CORE_STRINGASSET_H