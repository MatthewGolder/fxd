// Creator - MatthewGolder
#include "FXDEngine/Core/StringView.h"

using namespace FXD;
using namespace Core;

namespace FXD
{
	namespace Core
	{
		// ------
		// E_OS
		// -
		// ------
		enum class E_OS : FXD::U8
		{
			Windows,
			Android,
			Count,
			Unknown = 0xff
		};
	} //namespace Core
} //namespace FXD

namespace
{
	CONSTEXPR const Core::StringView8 kOS[] =
	{
		"Windows",	// Core::E_OS::DX11
		"Android",	// Core::E_OS::Android
		"Count"		// Core::E_OS::Count
	};

	CONSTEXPR const Core::StringView8 getToString(const Core::E_OS eType)
	{
		return kOS[FXD::STD::to_underlying(eType)];
	}

	CONSTEXPR Core::E_OS getToType(const Core::StringView8 strType)
	{
		for (FXD::U8 n1 = 0; n1 < FXD::STD::to_underlying(Core::E_OS::Count); ++n1)
		{
			if (strType.compare_no_case(kOS[n1]) == 0)
			{
				return (Core::E_OS)n1;
			}
		}
		return Core::E_OS::Unknown;
	}

	CONSTEXPR Core::E_OS getOSType(void)
	{
#	if IsOSPC()
		return Core::E_OS::Windows;
#	elif IsOSAndroid()
		return Core::E_OS::Android;
#	endif
	}


}