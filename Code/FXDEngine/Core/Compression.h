// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_COMPRESSION_H
#define FXDENGINE_CORE_COMPRESSION_H

#include "FXDEngine/Core/Compression.inl"

namespace FXD
{
	namespace Core
	{
		namespace Compression
		{
			CONSTEXPR FXD::S32 GetQualityVal(Core::E_CompressionType eType, Core::E_CompressionQuality eQuality)
			{
				return Compression::getCompressionQualityDesc(getCompressionTypeDesc(eType), eQuality).nLevel;
			}
		} //namespace Compression

		// ------
		// CompressParams
		// -
		// ------
		struct CompressParams
		{
			Core::E_CompressionType Type = Core::E_CompressionType::None;
			Core::E_CompressionQuality Quality = Core::E_CompressionQuality::Default;

			static const CompressParams Default;
		};


	} //namespace Core
} //namespace FXD

#endif //FXDENGINE_CORE_COMPRESSION_H