// Creator - MatthewGolder
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/TypeTraits.h"

namespace FXD
{
	namespace Core
	{
		// ------
		// E_CompressionType
		// -
		// ------
		enum class E_CompressionType
		{
			None,
			ZLib,
			ZLibStreamed,
			LZ4,
			LZ4Streamed,
			Count,
			Unknown = 0xffff
		};
		// ------
		// E_CompressionQuality
		// -
		// ------
		enum class E_CompressionQuality
		{
			Default,
			BestSpeed,
			BestCompression,
			Count,
			Unknown = 0xffff
		};

		namespace Compression
		{
			struct _CompressionQualityDesc
			{
				const Core::E_CompressionQuality eCompressionQuality;
				const FXD::S32 nLevel;
			};
			struct _CompressionTypeDesc
			{
				const Core::E_CompressionType eCompressionType;
				const bool bStreamed;
				const Compression::_CompressionQualityDesc kQuality[STD::to_underlying(Core::E_CompressionQuality::Count)];
			};

			CONSTEXPR const Compression::_CompressionTypeDesc kCompression[STD::to_underlying(Core::E_CompressionType::Count)] =
			{
				//-----------------------------------------------------------------------
				{
					E_CompressionType::None,
					false,
					{
						{ E_CompressionQuality::Default, 0 },
						{ E_CompressionQuality::BestSpeed, 0 },
						{ E_CompressionQuality::BestCompression, 0 }
					}
				},
				//-----------------------------------------------------------------------
				{
					E_CompressionType::ZLib,
					false,
					{
						{ E_CompressionQuality::Default, 1 },				// Z_BEST_SPEED
						{ E_CompressionQuality::BestSpeed, 1 },			// Z_BEST_SPEED
						{ E_CompressionQuality::BestCompression, 9 }		// Z_BEST_COMPRESSION
					}
				},
				//-----------------------------------------------------------------------
				{
					E_CompressionType::ZLibStreamed,
					true,
					{
						{ E_CompressionQuality::Default, 1 },				// Z_BEST_SPEED
						{ E_CompressionQuality::BestSpeed, 1 },			// Z_BEST_SPEED
						{ E_CompressionQuality::BestCompression, 9 }		// Z_BEST_COMPRESSION
					}
				},
				//-----------------------------------------------------------------------
				{
					E_CompressionType::LZ4,
					false,
					{
						{ E_CompressionQuality::Default, 1 },				// LZ4_ACCELERATION_DEFAULT
						{ E_CompressionQuality::BestSpeed, 9 },			// Hand picked value
						{ E_CompressionQuality::BestCompression, 1 }		// LZ4_ACCELERATION_DEFAULT
					}
				},
				//-----------------------------------------------------------------------
				{
					E_CompressionType::LZ4Streamed,
					true,
					{
						{ E_CompressionQuality::Default, 1 },				// LZ4_ACCELERATION_DEFAULT
						{ E_CompressionQuality::BestSpeed, 9 },			// Hand picked value
						{ E_CompressionQuality::BestCompression, 1 }		// LZ4_ACCELERATION_DEFAULT
					}
				}
			};
			CONSTEXPR const _CompressionQualityDesc& getCompressionQualityDesc(const _CompressionTypeDesc& type, Core::E_CompressionQuality eQuality)
			{
				return type.kQuality[STD::to_underlying(eQuality)];
			}
			CONSTEXPR const _CompressionTypeDesc& getCompressionTypeDesc(Core::E_CompressionType eType)
			{
				return kCompression[STD::to_underlying(eType)];
			}
		} //namespace Compression
	} //namespace Core
} //namespace FXD