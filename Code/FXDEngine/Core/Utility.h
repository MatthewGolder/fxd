// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_UTILITY_H
#define FXDENGINE_CORE_UTILITY_H

#include "FXDEngine/Core/Internal/AsConst.h"
#include "FXDEngine/Core/Pair.h"
#include "FXDEngine/Core/Internal/Declval.h"
#include "FXDEngine/Core/Internal/Exchange.h"
#include "FXDEngine/Core/Internal/InPlaceT.h"
#include "FXDEngine/Core/Internal/IntegerSequence.h"
#include "FXDEngine/Core/Internal/MoveIfNoExcept.h"
#include "FXDEngine/Core/Internal/Swap.h"
#include "FXDEngine/Core/Internal/TupleElement.h"
#include "FXDEngine/Core/Internal/TupleSize.h"
#include "FXDEngine/Core/Internal/UseFirst.h"
#include "FXDEngine/Core/Internal/UseSecond.h"
#include "FXDEngine/Core/Internal/UseSelf.h"

#endif //FXDENGINE_CORE_UTILITY_H