// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_SERILIZEOUT_H
#define FXDENGINE_CORE_SERILIZEOUT_H

#include "FXDEngine/Core/Types.h"

namespace FXD
{
	namespace Core
	{
		class IStreamOut;

		// ------
		// ISerializeOut
		// -
		// ------
		class ISerializeOut
		{
		public:
			friend class Core::IStreamOut;

		public:
			ISerializeOut(void);
			virtual ~ISerializeOut(void);

		protected:
			virtual void _pass(const FXD::UTF8* pVal, Core::IStreamOut* pStream) PURE;
			virtual void _pass(const FXD::UTF16* pVal, Core::IStreamOut* pStream) PURE;

			virtual void _pass(const bool val, Core::IStreamOut* pStream) PURE;
			virtual void _pass(const FXD::UTF8 val, Core::IStreamOut* pStream) PURE;

			virtual void _pass(const FXD::S8 nVal, Core::IStreamOut* pStream) PURE;
			virtual void _pass(const FXD::U8 nVal, Core::IStreamOut* pStream) PURE;

			virtual void _pass(const FXD::S16 nVal, Core::IStreamOut* pStream) PURE;
			virtual void _pass(const FXD::U16 nVal, Core::IStreamOut* pStream) PURE;

			virtual void _pass(const FXD::S32 nVal, Core::IStreamOut* pStream) PURE;
			virtual void _pass(const FXD::U32 nVal, Core::IStreamOut* pStream) PURE;

			virtual void _pass(const FXD::S64 nVal, Core::IStreamOut* pStream) PURE;
			virtual void _pass(const FXD::U64 nVal, Core::IStreamOut* pStream) PURE;

			virtual void _pass(const FXD::F32 fVal, Core::IStreamOut* pStream) PURE;
			virtual void _pass(const FXD::F64 fVal, Core::IStreamOut* pStream) PURE;
		};
	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_SERILIZEOUT_H