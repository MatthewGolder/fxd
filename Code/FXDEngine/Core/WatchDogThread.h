// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_WATCHDOGTHREAD_H
#define FXDENGINE_CORE_WATCHDOGTHREAD_H

#include "FXDEngine/Thread/Thread.h"

#include <atomic>

namespace FXD
{
	namespace Core
	{
		// ------
		// WatchDogThread
		// -
		// ------
		class WatchDogThread : public Thread::IThread
		{
		public:
			WatchDogThread(const FXD::UTF8* pName, const FXD::U32 nTimeoutMS = 1000);
			~WatchDogThread(void);

			bool thread_init_func(void) FINAL;
			bool thread_process_func(void) FINAL;
			bool thread_shutdown_func(void) FINAL;
			
			void increment(void);

		protected:
			bool m_bShuttingDown;
			const FXD::U32 m_nTimeoutMS;
			std::atomic< FXD::U64 > m_nCounter;
		};

	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_WATCHDOGTHREAD_H