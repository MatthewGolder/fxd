// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_TRIGGERHANDLER_H
#define FXDENGINE_CORE_TRIGGERHANDLER_H

#include "FXDEngine/Container/Vector.h"
#include "FXDEngine/Thread/RWLock.h"

#include <functional>

namespace FXD
{
	namespace Core
	{
		// Forward declare the trigger.
		template < typename T >
		class Trigger;

		// ------
		// TriggerHandler
		// -
		// ------
		template < typename T >
		class TriggerHandler
		{
		public:
			using FuncType = FXD::STD::function< void(const T&) >;
			friend class Core::Trigger< T >;

		public:
			TriggerHandler(void)
				: m_pTrigger(nullptr)
			{}
			TriggerHandler(const TriggerHandler& /*rhs*/)
				: m_pTrigger(nullptr)
			{}
			~TriggerHandler(void)
			{
				detachTrigger();
			}

			void attachTrigger(Core::Trigger< T >& trigger, const FuncType& func)
			{
				detachTrigger();
				m_pTrigger = &trigger;
				m_handler = func;
				m_pTrigger->m_shared.get_quick_write_lock()->m_handlers.push_back(this);
			}

			void detachTrigger(void)
			{
				if (m_pTrigger == nullptr)
				{
					return;
				}

				m_pTrigger->m_shared.get_quick_write_lock()->m_handlers.find_erase(this);
				m_pTrigger = nullptr;
				m_handler = FuncType();
			}

		private:
			FuncType m_handler;
			Core::Trigger< T >* m_pTrigger;
		};


		// ------
		// Trigger
		// -
		// ------
		template < typename T >
		class Trigger
		{
		public:
			friend class TriggerHandler< T >;

		public:
			Trigger(void)
			{}
			Trigger(const Trigger& /*rhs*/)
			{}
			~Trigger(void)
			{
				typename Thread::RWLockData< SharedTS >::ScopedWriteLock s(m_shared);
				fxd_for(auto& handler, s->m_handlers)
				{
					handler->m_pTrigger = nullptr;
					handler->m_handler = typename Core::TriggerHandler< T >::FuncType();
				}
			}

			void fire(const T& t) const
			{
				typename Thread::RWLockData< SharedTS >::ScopedReadLock s(m_shared);
				fxd_for(auto& handler, s->m_handlers)
				{
					handler->m_handler(t);
				}
			}

		private:
			// ------
			// SharedTS
			// -
			// ------
			class SharedTS
			{
			public:
				SharedTS(void)
				{}
				~SharedTS(void)
				{}

			public:
				Container::Vector< Core::TriggerHandler< T >* > m_handlers;
			};
			Thread::RWLockData< SharedTS > m_shared;
		};

	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_TRIGGERHANDLER_H
