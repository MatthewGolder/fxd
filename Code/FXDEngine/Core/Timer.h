// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_TIMER_H
#define FXDENGINE_CORE_TIMER_H

#include "FXDEngine/Core/Types.h"

namespace FXD
{
	namespace Core
	{
		// ------
		// Timer
		// -
		// ------
		class Timer
		{
		public:
			Timer(bool bStart = false);
			~Timer(void);

			void start_counter(void);
			FXD::F32 elapsed_time(void) const;

		private:
			mutable FXD::S64 m_nStartTime;
		};

		namespace Funcs
		{
			extern FXD::S64 GetRawTime(void);
			extern FXD::S64 GetRawTimeFreq(void);
			extern FXD::F32 GetTimeSecsF(void);
			extern FXD::F64 GetTimeSecsD(void);
		} //namespace Funcs

	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_TIMER_H