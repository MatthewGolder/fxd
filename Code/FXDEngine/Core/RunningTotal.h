// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_RUNNINGTOTAL_H
#define FXDENGINE_CORE_RUNNINGTOTAL_H

#include "FXDEngine/Core/Types.h"

namespace FXD
{
	namespace Core
	{
		// ------
		// RunningTotal
		// -
		// ------
		template < typename Type, FXD::U32 Count >
		class RunningTotal
		{
		public:
			RunningTotal(void)
			{
				reset();
			}
			~RunningTotal(void)
			{
			}
			void add(const Type& t)
			{
				m_total -= m_values[m_next];
				m_values[m_next] = t;
				m_total += t;
				if (++m_next == Count)
				{
					m_nNext = 0;
				}
			}
			void reset(void)
			{
				for (FXD::U32 n1 = 0; n1 < Count; n1++)
				{
					m_values[n1] = 0;
				}
				m_total = 0;
				m_nNext = 0;
			}
			const Type& getTotal(void) const			{ return m_total; }
			inline FXD::U32 getCount(void) const		{ return Count; }
			inline Type getAverage(void) const		{ return (getTotal() / (Type)Count); }

		private:
			Type m_values[Count];
			Type m_total;
			FXD::U32 m_nNext;
		};

	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_RUNNINGTOTAL_H
