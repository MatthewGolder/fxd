// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_CMDARGS_H
#define FXDENGINE_CORE_CMDARGS_H

#include "FXDEngine/Core/NonCopyable.h"
#include "FXDEngine/Core/String.h"
#include "FXDEngine/Core/StringView.h"
#include "FXDEngine/Container/Map.h"

namespace FXD
{
	namespace Core
	{
		// ------
		// CmdArgs
		// -
		// ------
		class CmdArgs FINAL : public Core::NonCopyable
		{
		public:
			CmdArgs(void);
			~CmdArgs(void);

			void set_args(FXD::S32 nArgc, FXD::UTF8* nArgv[]);			
			bool has_arg(const Core::StringView8 strArg) const;
			Core::String8 get_value(const Core::StringView8 strArg) const;
			
		private:
			Container::Map< Core::String8, Core::String8 > m_argMap;
		};
	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_CMDARGS_H
