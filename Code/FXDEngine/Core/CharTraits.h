// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_CHARTRAITS_H
#define FXDENGINE_CORE_CHARTRAITS_H

#include "FXDEngine/Core/Types.h"

namespace FXD
{
	namespace Core
	{
		// ------
		// CharTraits
		// -
		// ------
		template < typename CharType >
		struct CharTraits
		{
			using char_type = CharType;
	
			static char_type to_upper(const char_type ch);
			static char_type* to_upper(char_type* const pStr, const FXD::U32 nCount);
			static char_type* to_upper(char_type* const pStr);

			static char_type to_lower(const char_type ch);
			static char_type* to_lower(char_type* const pStr, const FXD::U32 nCount);
			static char_type* to_lower(char_type* const pStr);

			static const FXD::HashValue to_hash(const char_type* const pStr, const FXD::U32 nCount);
			static const FXD::HashValue to_hash(const char_type* const pStr);

			static CONSTEXPR14 FXD::U32 length(const char_type* const pStr);

			static CONSTEXPR14 void assign(char_type& dst, const char_type& src) NOEXCEPT;
			static char_type* assign(char_type* const pDst, const char_type src, const FXD::U32 nCount);

			static char_type* copy(char_type* const pDst, const char_type* pSrc, const FXD::U32 nCount);
			static char_type* move(char_type* const pDst, const char_type* pSrc, const FXD::U32 nCount);

			static CONSTEXPR bool equals(const char_type lhs, const char_type rhs) NOEXCEPT;
			static CONSTEXPR bool lt(const char_type lhs, const char_type rhs) NOEXCEPT;

			static FXD::S32 compare(const char_type* const pLhs, const char_type* const pRhs, const FXD::U32 nCount);
			static FXD::S32 compare(const char_type* const pLhs, const char_type* const pRhs);
			static FXD::S32 compare_no_case(const char_type* const pLhs, const char_type* const pRhs, const FXD::U32 nCount);
			static FXD::S32 compare_no_case(const char_type* const pLhs, const char_type* const pRhs);

			static const char_type* find(const char_type* const pStr, const FXD::U32 nCount, const char_type& ch);
			static const char_type* find(const char_type* const pStr, const char_type& ch);
			static const char_type* find_no_case(const char_type* const pStr, const FXD::U32 nCount, const char_type& ch);
			static const char_type* find_no_case(const char_type* const pStr, const char_type& ch);

			static CONSTEXPR bool eq(const char_type& lhs, const char_type& rhs) NOEXCEPT { return (lhs == rhs); }

			static const bool is_quote(const char_type ch);
			static const bool is_dot(const char_type ch);
			static const bool is_underscore(const char_type ch);
			static const bool is_null_terminator(const char_type ch);
			static const bool is_white_space(const char_type ch);
			static const bool is_space(const char_type ch);
			static const bool is_numeric(const char_type ch);
			static const bool is_alpha(const char_type ch);
			static const bool is_alpha_numeric(const char_type ch);
			static const bool is_japanese_punctuation(const char_type ch);
			static const bool is_hiragana(const char_type ch);
			static const bool is_katakana(const char_type ch);
			static const bool is_katakana_phonetic_extensions(const char_type ch);
			static const bool is_kanji(const char_type ch);
			static const bool is_japanese(const char_type ch);

			static void to_string(char_type* const pBuf, const FXD::U32 nSize, const FXD::S8 nVal);
			static void to_string(char_type* const pBuf, const FXD::U32 nSize, const FXD::U8 nVal);
			static void to_string(char_type* const pBuf, const FXD::U32 nSize, const FXD::S16 nVal);
			static void to_string(char_type* const pBuf, const FXD::U32 nSize, const FXD::U16 nVal);
			static void to_string(char_type* const pBuf, const FXD::U32 nSize, const FXD::S32 nVal);
			static void to_string(char_type* const pBuf, const FXD::U32 nSize, const FXD::U32 nVal);
			static void to_string(char_type* const pBuf, const FXD::U32 nSize, const FXD::S64 nVal);
			static void to_string(char_type* const pBuf, const FXD::U32 nSize, const FXD::U64 nVal);
			static void to_string(char_type* const pBuf, const FXD::U32 nSize, const FXD::F32 fVal);
			static void to_string(char_type* const pBuf, const FXD::U32 nSize, const FXD::F64 fVal);
		};

		template < typename CharType >
		CONSTEXPR14 FXD::U32 CharTraits< CharType >::length(const CharType* pStr)
		{
			FXD::U32 nRet = 0;
			while (!CharTraits< CharType >::equals(pStr[nRet], CharType()))
			{
				++nRet;
			}
			return nRet;
		}
		template < typename CharType >
		CONSTEXPR14 void CharTraits< CharType >::assign(CharType& dst, const CharType& src) NOEXCEPT
		{
			dst = src;
		}
		template < typename CharType >
		CONSTEXPR bool CharTraits< CharType >::equals(const CharType lhs, const CharType rhs) NOEXCEPT
		{
			return (lhs == rhs);
		}
		template < typename CharType >
		CONSTEXPR bool CharTraits< CharType >::lt(const CharType lhs, const CharType rhs) NOEXCEPT
		{
			return (lhs < rhs);
		}

	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_CHARTRAITS_H