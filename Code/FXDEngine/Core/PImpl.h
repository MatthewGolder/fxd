// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_PIMPL_H
#define FXDENGINE_CORE_PIMPL_H

#include "FXDEngine/Memory/Memory.h"

namespace FXD
{
	namespace Core
	{
		// ------
		// PimplAlloc
		// -
		// ------
		template < typename Type >
		class PimplAlloc
		{
		public:
			inline PimplAlloc(void)
				: m_pData(FXD_NEW(Type))
			{}
			template < typename Arg1 >
			inline PimplAlloc(const Arg1& arg1)
			{
				m_pData = FXD_NEW(Type)(arg1);
			}
			inline ~PimplAlloc(void)
			{
				FXD_SAFE_DELETE(m_pData);
			}

			inline Type& operator*(void) const
			{
				return _get_type();
			}
			inline Type* operator->(void) const
			{
				return &_get_type();
			}

		private:
			inline Type& _get_type(void) const
			{
				return *(Type*)m_pData;
			}

		private:
			Type* m_pData;
		};

		struct _Pimpl_Pack
		{
			EXPLICIT _Pimpl_Pack() = default;
		};

		// ------
		// PimplBuffer
		// -
		// ------
		template < typename Type, FXD::U32 BufSize/*, FXD::U32 Align = alignof(Type)*/ >
		class PimplBuffer
		{
		private:
			// ------
			// Buffer
			// -
			// ------
			class Buffer
			{
			public:
				Buffer(void)
					: m_pImpl(nullptr)
				{
					CTC_IS_GREATER(sizeof(Type), BufSize);
					m_pImpl = (Type*)&m_buff[0];
				}

			public:
				Type* m_pImpl;
				FXD::U8 m_buff[BufSize];
			};

		public:
			PimplBuffer(void)
			{
				FXD_PLACEMENT_NEW(m_buffer.m_pImpl, Type)();
			}

			template < typename... Types >
			PimplBuffer(_Pimpl_Pack pack, Types&&... inArgs)
			{
				FXD_PLACEMENT_NEW(m_buffer.m_pImpl, Type)(inArgs...);
			}

			PimplBuffer(const PimplBuffer& rhs)
			{
				FXD_PLACEMENT_NEW(m_buffer.m_pImpl, Type)(rhs._get_type());
			}

			inline ~PimplBuffer(void)
			{
				_tidy();
			}

			PimplBuffer& operator=(const PimplBuffer& rhs)
			{
				_get_type() = rhs._get_type();
				return (*this);
			}

			bool operator==(const std::nullptr_t rhs) const
			{
				return (m_buffer.m_pImpl == nullptr);
			}

			bool operator!=(const std::nullptr_t rhs) const
			{
				return (m_buffer.m_pImpl != nullptr);
			}

			inline Type& operator*(void) const
			{
				return _get_type();
			}
			inline Type* operator->(void) const
			{
				return &_get_type();
			}

		private:
			inline Type& _get_type(void) const
			{
				return *(Type*)m_buffer.m_pImpl;
			}
			inline void _tidy(void) 
			{
				CTC_IS_GREATER(sizeof(Type), BufSize);
				if (m_buffer.m_pImpl != nullptr)
				{
					FXD::STD::destruct(&_get_type());
				}
			}

		private:
			Buffer m_buffer;
		};

		namespace Impl
		{
			template < typename ContainerType >
			class Impl;
		}

		template < typename ContainerType, FXD::U32 Size >
		class HasImpl
		{
		public:
			using Impl = typename FXD::Core::Impl::Impl< ContainerType >;

			HasImpl(void)
				: m_impl()
			{}
			template < typename... Types >
			HasImpl(_Pimpl_Pack pack, Types&&... inArgs)
				: m_impl(pack, inArgs...)
			{}
			HasImpl(const Impl& i)
			{}

			Impl& impl(void)
			{
				return *m_impl;
			}
			const Impl& impl(void) const
			{
				return *m_impl;
			}

		protected:
			Core::PimplBuffer< Impl, Size > m_impl;
		};
	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_PIMPL_H