// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_SERILIZEOUTSTRING_H
#define FXDENGINE_CORE_SERILIZEOUTSTRING_H

#include "FXDEngine/Core/CharTraits.h"
#include "FXDEngine/Core/StreamOut.h"
#include "FXDEngine/Core/SerializeOut.h"
#include <stdlib.h>

#ifndef _CVTBUFSIZE
#	define _CVTBUFSIZE (309+40)
#endif

namespace FXD
{
	namespace Core
	{
		// ------
		// ISerializeOutString
		// -
		// ------
		template < typename CharType >
		class ISerializeOutString : public Core::ISerializeOut
		{
		public:
			friend class Core::IStreamOut;

		public:
			ISerializeOutString(void)
			{}
			~ISerializeOutString(void)
			{}

		protected:
			void _pass(const FXD::UTF8* pVal, Core::IStreamOut* pStream) FINAL;
			void _pass(const FXD::UTF16* pVal, Core::IStreamOut* pStream) FINAL;

			void _pass(const bool val, Core::IStreamOut* pStream) FINAL;
			void _pass(const FXD::UTF8 val, Core::IStreamOut* pStream) FINAL;

			void _pass(const FXD::S8 nVal, Core::IStreamOut* pStream) FINAL;
			void _pass(const FXD::U8 nVal, Core::IStreamOut* pStream) FINAL;

			void _pass(const FXD::S16 nVal, Core::IStreamOut* pStream) FINAL;
			void _pass(const FXD::U16 nVal, Core::IStreamOut* pStream) FINAL;

			void _pass(const FXD::S32 nVal, Core::IStreamOut* pStream) FINAL;
			void _pass(const FXD::U32 nVal, Core::IStreamOut* pStream) FINAL;

			void _pass(const FXD::S64 nVal, Core::IStreamOut* pStream) FINAL;
			void _pass(const FXD::U64 nVal, Core::IStreamOut* pStream) FINAL;

			void _pass(const FXD::F32 fVal, Core::IStreamOut* pStream) FINAL;
			void _pass(const FXD::F64 fVal, Core::IStreamOut* pStream) FINAL;

			void _write_size(const void* pData, Core::IStreamOut* pStream);
		};

		// ------
		// ISerializeOutString
		// -
		// ------
		template < typename CharType >
		void ISerializeOutString< CharType >::_pass(const FXD::UTF8 nVal, Core::IStreamOut* pStream)
		{
			FXD::UTF8 ptr[2];
			ptr[0] = nVal;
			ptr[1] = '\0';
			_write_size(&ptr[0], pStream);
		}
		template < typename CharType >
		void ISerializeOutString< CharType >::_pass(const FXD::S8 nVal, Core::IStreamOut* pStream)
		{
			CharType buf[64];
			Core::CharTraits< CharType >::to_string(buf, 64, nVal);
			_write_size(&buf[0], pStream);
		}
		template < typename CharType >
		void ISerializeOutString< CharType >::_pass(const FXD::U8 nVal, Core::IStreamOut* pStream)
		{
			CharType buf[64];
			Core::CharTraits< CharType >::to_string(buf, 64, nVal);
			_write_size(&buf[0], pStream);
		}
		template < typename CharType >
		void ISerializeOutString< CharType >::_pass(const FXD::S16 nVal, Core::IStreamOut* pStream)
		{
			CharType buf[64];
			Core::CharTraits< CharType >::to_string(buf, 64, nVal);
			_write_size(&buf[0], pStream);
		}
		template < typename CharType >
		void ISerializeOutString< CharType >::_pass(const FXD::U16 nVal, Core::IStreamOut* pStream)
		{
			CharType buf[64];
			Core::CharTraits< CharType >::to_string(buf, 64, nVal);
			_write_size(&buf[0], pStream);
		}
		template < typename CharType >
		void ISerializeOutString< CharType >::_pass(const FXD::S32 nVal, Core::IStreamOut* pStream)
		{
			CharType buf[64];
			Core::CharTraits< CharType >::to_string(buf, 64, nVal);
			_write_size(&buf[0], pStream);
		}
		template < typename CharType >
		void ISerializeOutString< CharType >::_pass(const FXD::U32 nVal, Core::IStreamOut* pStream)
		{
			CharType buf[64];
			Core::CharTraits< CharType >::to_string(buf, 64, nVal);
			_write_size(&buf[0], pStream);
		}
		template < typename CharType >
		void ISerializeOutString< CharType >::_pass(const FXD::S64 nVal, Core::IStreamOut* pStream)
		{
			CharType buf[64];
			Core::CharTraits< CharType >::to_string(buf, 64, nVal);
			_write_size(&buf[0], pStream);
		}
		template < typename CharType >
		void ISerializeOutString< CharType >::_pass(const FXD::U64 nVal, Core::IStreamOut* pStream)
		{
			CharType buf[64];
			Core::CharTraits< CharType >::to_string(buf, 64, nVal);
			_write_size(&buf[0], pStream);
		}
		template < typename CharType >
		void ISerializeOutString< CharType >::_pass(const FXD::F32 fVal, Core::IStreamOut* pStream)
		{
			CharType buf[(_CVTBUFSIZE * 2)];
			Core::CharTraits< CharType >::to_string(buf, (_CVTBUFSIZE * 2), fVal);
			_write_size(&buf[0], pStream);
		}
		template < typename CharType >
		void ISerializeOutString< CharType >::_pass(const FXD::F64 fVal, Core::IStreamOut* pStream)
		{
			CharType buf[(_CVTBUFSIZE * 2)];
			Core::CharTraits< CharType >::to_string(buf, (_CVTBUFSIZE * 2), fVal);
			_write_size(&buf[0], pStream);
		}

		template < typename CharType >
		void ISerializeOutString< CharType >::_write_size(const void* pData, Core::IStreamOut* pStream)
		{
			const CharType* pChar = (const CharType*)pData;
			FXD::U32 nLen = Core::CharTraits< CharType >::length(pChar);
			pStream->write_size(pData, nLen);
		}

	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_SERILIZEOUTSTRING_H