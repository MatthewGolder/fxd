// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_INCLUSIVESCANIF_H
#define FXDENGINE_CORE_INTERNAL_INCLUSIVESCANIF_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"

namespace FXD
{
	namespace STD
	{
		// inclusive_scan_if
#		define FXD_SUPPORTS_INCLUSIVE_SCAN_IF 1

		template < typename InputItr, typename OutputItr, typename T, typename Predicate >
		inline OutputItr inclusive_scan_if(InputItr first, InputItr last, OutputItr dst, Predicate pred, T val)
		{
			for (; first != last; ++first, (void) ++dst)
			{
				val = pred(val, *first);
				*dst = val;
			}
			return dst;
		}

		template < typename InputItr, typename OutputItr, typename Predicate >
		inline OutputItr inclusive_scan_if(InputItr first, InputItr last, OutputItr dst, Predicate pred)
		{
			if (first != last)
			{
				typename FXD::STD::iterator_traits< InputItr >::value_type val = *first;
				*dst++ = val;
				if (++first != last)
				{
					return FXD::STD::inclusive_scan_if(first, last, dst, pred, val);
				}
			}

			return dst;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_INCLUSIVESCANIF_H