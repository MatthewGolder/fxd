#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_COUNTIF_H
#define FXDENGINE_CORE_INTERNAL_COUNTIF_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"

namespace FXD
{
	namespace STD
	{
		// count_if
		template < typename InputItr, typename Predicate >
		inline typename FXD::STD::iterator_traits< InputItr >::difference_type count_if(InputItr first, InputItr last, Predicate pred)
		{
			typename FXD::STD::iterator_traits< InputItr >::difference_type nResult = 0;

			for (; first != last; ++first)
			{
				if (pred(*first))
				{
					++nResult;
				}
			}
			return nResult;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_COUNTIF_H