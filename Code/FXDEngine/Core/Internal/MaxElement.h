#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MAXELEMENT_H
#define FXDENGINE_CORE_INTERNAL_MAXELEMENT_H

#include "FXDEngine/Core/Internal/MaxElementIf.h"

namespace FXD
{
	namespace STD
	{
		// max_element
#		define FXD_SUPPORTS_MAX_ELEMENT 1

		template < typename ForwardItr, typename Predicate >
		CONSTEXPR17 ForwardItr max_element(ForwardItr first, ForwardItr last, Predicate pred)
		{
			return FXD::STD::max_element_if(first, last, pred);
		}

		template < typename ForwardItr >
		CONSTEXPR17 ForwardItr max_element(ForwardItr first, ForwardItr last)
		{
			return FXD::STD::max_element(first, last, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MAXELEMENT_H