#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISNOTHROWINVOCABLER_H
#define FXDENGINE_CORE_INTERNAL_ISNOTHROWINVOCABLER_H

#include "FXDEngine/Core/Internal/IsNoThrowConstructible.h"

namespace FXD
{
	namespace STD
	{
		// is_nothrow_invocable_r
#		define FXD_SUPPORTS_IS_NO_THROW_INVOCABLE_R 1

		template < typename R, typename C, typename... Args >
		struct is_nothrow_invocable_r : FXD::STD::Internal::_invoke_traits< void, C, Args... >::template is_nothrow_invocable_r< R >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename R, typename C, typename... Args >
		CONSTEXPR bool is_nothrow_invocable_r_v = FXD::STD::is_nothrow_invocable_r< R, C, Args... >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISNOTHROWINVOCABLER_H