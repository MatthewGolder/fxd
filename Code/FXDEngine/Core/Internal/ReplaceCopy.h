#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REPLACECOPY_H
#define FXDENGINE_CORE_INTERNAL_REPLACECOPY_H

#include "FXDEngine/Core/Internal/EqualTo.h"
#include "FXDEngine/Core/Internal/ReplaceCopyIf.h"

namespace FXD
{
	namespace STD
	{
		// replace_copy
#		define FXD_SUPPORTS_REPLACE_COPY 1

		template < typename InputItr, typename OutputItr, typename T, typename Predicate >
		inline OutputItr replace_copy(InputItr first, InputItr last, OutputItr dst, const T& oldVal, const T& newVal, Predicate pred)
		{
			return FXD::STD::replace_copy_if(first, last, dst, [&](const T& x) { return pred(x, oldVal); }, newVal);
		}

		template < typename InputItr, typename OutputItr, typename T >
		inline OutputItr replace_copy(InputItr first, InputItr last, OutputItr dst, const T& oldVal, const T& newVal)
		{
			return FXD::STD::replace_copy(first, last, dst, oldVal, newVal, FXD::STD::equal_to< T >());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REPLACECOPY_H