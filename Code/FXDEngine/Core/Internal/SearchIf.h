#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_SEARCHIF_H
#define FXDENGINE_CORE_INTERNAL_SEARCHIF_H

#include "FXDEngine/Core/Internal/Equal.h"
#include "FXDEngine/Container/Internal/Advance.h"
#include "FXDEngine/Container/Internal/Distance.h"

namespace FXD
{
	namespace STD
	{
		// search_if
#		define FXD_SUPPORTS_SEARCH_IF 1

		template < typename ForwardItr1, typename ForwardItr2, typename Predicate >
		inline ForwardItr1 search_if(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2, ForwardItr2 last2, Predicate pred)
		{
			using difference_type_1 = typename FXD::STD::iterator_traits< ForwardItr1 >::difference_type;
			using difference_type_2 = typename FXD::STD::iterator_traits< ForwardItr1 >::difference_type;
			difference_type_2 d2 = FXD::STD::distance(first2, last2);

			if (d2 != 0)
			{
				ForwardItr1 itr(first1);
				FXD::STD::advance(itr, d2);

				for (difference_type_1 d1 = FXD::STD::distance(first1, last1); d1 >= d2; --d1)
				{
					if (FXD::STD::equal< ForwardItr1, ForwardItr2, Predicate >(first1, itr, first2, pred))
					{
						return first1;
					}
					if (d1 > d2) // To do: Find a way to make the algorithm more elegant.
					{
						++first1;
						++itr;
					}
				}
				return last1;
			}
			return first1;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_SEARCHIF_H