// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_TRANSFORMINCLUSIVESCAN_H
#define FXDENGINE_CORE_INTERNAL_TRANSFORMINCLUSIVESCAN_H

namespace FXD
{
	namespace STD
	{
		// transform_inclusive_scan
#		define FXD_SUPPORTS_TRANSFORM_INCLUSIVE_SCAN 1

		template < typename InputItr, typename OutputItr, typename T, typename Predicate1, typename Predicate2 >
		OutputItr transform_inclusive_scan(InputItr first, InputItr last, OutputItr dst, Predicate1 pred1, Predicate2 pred2, T val)
		{
			for (; first != last; ++first, (void) ++dst)
			{
				val = pred1(val, pred2(*first));
				*dst = val;
			}

			return dst;
		}

		template < typename InputItr, typename OutputItr, typename Predicate1, typename Predicate2 >
		OutputItr transform_inclusive_scan(InputItr first, InputItr last, OutputItr dst, Predicate1 pred1, Predicate2 pred2)
		{
			if (first != last)
			{
				typename FXD::STD::iterator_traits< InputItr >::value_type val = pred2(*first);
				*dst++ = val;
				if (++first != last)
				{
					return FXD::STD::transform_inclusive_scan(first, last, dst, pred1, pred2, val);
				}
			}

			return dst;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_TRANSFORMINCLUSIVESCAN_H