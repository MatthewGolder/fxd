#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISAGGREGATE_H
#define FXDENGINE_CORE_INTERNAL_ISAGGREGATE_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Declval.h"

namespace FXD
{
	namespace STD
	{
		// is_aggregate
#		if IsCPP17()
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC))
#		define FXD_SUPPORTS_IS_AGGREGATE 1

		template < typename T >
		struct is_aggregate : public FXD::STD::integral_constant< bool, __is_aggregate(T) >
		{};

#		else
#		define FXD_SUPPORTS_IS_AGGREGATE 0

		template < typename T >
		struct is_aggregate : public FXD::STD::false_type
		{};

#endif

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_aggregate_v = FXD::STD::is_aggregate< T >::value;

#		endif
#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISAGGREGATE_H