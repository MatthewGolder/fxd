#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ADJACENTFIND_H
#define FXDENGINE_CORE_INTERNAL_ADJACENTFIND_H

#include "FXDEngine/Core/Internal/AdjacentFindIf.h"
#include "FXDEngine/Core/Internal/EqualTo.h"

namespace FXD
{
	namespace STD
	{
		// adjacent_find
#		define FXD_SUPPORTS_ADJACENT_FIND 1

		template < typename ForwardItr, typename Predicate >
		inline ForwardItr adjacent_find(ForwardItr first, ForwardItr last, Predicate pred)
		{
			return FXD::STD::adjacent_find_if(first, last, pred);
		}

		template < typename ForwardItr >
		inline ForwardItr adjacent_find(ForwardItr first, ForwardItr last)
		{
			return FXD::STD::adjacent_find(first, last, FXD::STD::equal_to<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ADJACENTFIND_H