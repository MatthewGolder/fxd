#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISINVOCABLER_H
#define FXDENGINE_CORE_INTERNAL_ISINVOCABLER_H

#include "FXDEngine/Core/Internal/InvokeResult.h"

namespace FXD
{
	namespace STD
	{
		namespace Internal
		{
			template < class R, class C, class... _Args >
			using _is_invocable_r_impl = typename FXD::STD::Internal::_invoke_traits< void, C, _Args... >::template is_invocable_r< R >;

			template < class R, class C, class... _Args >
			struct _is_invocable_r : FXD::STD::Internal::_is_invocable_r_impl< R, C, _Args... >
			{};
		} //namespace Internal


		// is_invocable_r
#		define FXD_SUPPORTS_IS_INVOCABLE_R 1

		template < typename R, typename C, typename... Args >
		struct is_invocable_r : FXD::STD::Internal::_is_invocable_r< R, C, Args... >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename R, typename C, typename... Args >
		CONSTEXPR bool is_invocable_r_v = FXD::STD::is_invocable_r< R, C, Args... >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISINVOCABLER_H