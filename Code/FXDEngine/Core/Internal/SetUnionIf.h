// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_SETUNIONIF_H
#define FXDENGINE_CORE_INTERNAL_SETUNIONIF_H

#include "FXDEngine/Core/Internal/Copy.h"

namespace FXD
{
	namespace STD
	{
		// set_union_if
#		define FXD_SUPPORTS_SET_UNION_IF 1

		template < typename InputItr1, typename InputItr2, typename OutputItr, typename Predicate >
		inline OutputItr set_union_if(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst, Predicate pred)
		{
			while ((first1 != last1) && (first2 != last2))
			{
				if (pred(*first1, *first2))
				{
					PRINT_COND_ASSERT(!pred(*first2, *first1), ""); // Validate that the pred function is sane.
					*dst = *first1;
					++first1;
				}
				else if (pred(*first2, *first1))
				{
					PRINT_COND_ASSERT(!pred(*first1, *first2), ""); // Validate that the pred function is sane.
					*dst = *first2;
					++first2;
				}
				else
				{
					*dst = *first1;
					++first1;
					++first2;
				}
				++dst;
			}

			return FXD::STD::copy(first2, last2, FXD::STD::copy(first1, last1, dst));
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_SETUNIONIF_H