// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISPERMUTATIONIF_H
#define FXDENGINE_CORE_INTERNAL_ISPERMUTATIONIF_H

#include "FXDEngine/Core/Internal/Count.h"
#include "FXDEngine/Core/Internal/Find.h"
#include "FXDEngine/Container/Internal/Advance.h"
#include "FXDEngine/Container/Internal/Distance.h"
#include "FXDEngine/Container/Internal/IteratorTraits.h"

namespace FXD
{
	namespace STD
	{
		// is_permutation_if
#		define FXD_SUPPORTS_IS_PERMUTATION_IF 1

		template < typename ForwardItr1, typename ForwardItr2, typename Predicate >
		inline bool is_permutation_if(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2, Predicate pred)
		{
			using difference_type = typename FXD::STD::iterator_traits< ForwardItr1 >::difference_type;

			// Skip past any equivalent initial elements.
			while ((first1 != last1) && pred(*first1, *first2))
			{
				++first1;
				++first2;
			}

			if (first1 != last1)
			{
				const difference_type first1Size = FXD::STD::distance(first1, last1);
				ForwardItr2 last2 = first2;
				FXD::STD::advance(last2, first1Size);

				for (ForwardItr1 itr = first1; itr != last1; ++itr)
				{
					if (itr == FXD::STD::find(first1, itr, *itr, pred))
					{
						const difference_type c = FXD::STD::count(first2, last2, *itr, pred);

						if ((c == 0) || (c != FXD::STD::count(itr, last1, *itr, pred)))
						{
							return false;
						}
					}
				}
			}
			return true;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISPERMUTATIONIF_H