//Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISPOINTER_H
#define FXDENGINE_CORE_INTERNAL_ISPOINTER_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/RemoveCV.h"

namespace FXD
{
	namespace STD
	{
		// is_pointer
#		define FXD_SUPPORTS_IS_POINTER 1

		namespace Internal
		{
			template < typename T >
			struct _is_pointer : FXD::STD::false_type
			{};

			template < typename T >
			struct _is_pointer< T* > : FXD::STD::true_type
			{};
		} //namespace Internal

		template < typename T >
		struct is_pointer : public FXD::STD::integral_constant< bool, (FXD::STD::Internal::_is_pointer< typename FXD::STD::remove_cv< T >::type >::value) >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_pointer_v = FXD::STD::is_pointer< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISPOINTER_H