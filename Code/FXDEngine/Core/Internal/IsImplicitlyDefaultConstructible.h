#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISIMPLICITYDEFAULTCONSTRUCTIBLE_H
#define FXDENGINE_CORE_INTERNAL_ISIMPLICITYDEFAULTCONSTRUCTIBLE_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Void_t.h"

namespace FXD
{
	namespace STD
	{
		// is_implicitly_default_constructible
#		define FXD_SUPPORTS_IS_IMPLICITY_DEFAULT_CONSTRUCTIBLE 1

		namespace Internal
		{
			template < typename T >
			void _is_implicitly_default_constructible(const T&);
		} //namespace Internal

		template < typename T, typename = void >
		struct is_implicitly_default_constructible : FXD::STD::false_type
		{};

		template < typename T >
		struct is_implicitly_default_constructible < T, FXD::STD::void_t< decltype(FXD::STD::Internal::_is_implicitly_default_constructible< T >({})) >> : FXD::STD::true_type
		{};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISIMPLICITYDEFAULTCONSTRUCTIBLE_H