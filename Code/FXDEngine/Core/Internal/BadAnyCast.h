#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_BADANYCAST_H
#define FXDENGINE_CORE_INTERNAL_BADANYCAST_H

#include "FXDEngine/Core/Types.h"
#include <exception>

namespace FXD
{
	namespace STD
	{
		// bad_any_cast
#		define FXD_SUPPORTS_BAD_ANY_CAST 1

		class bad_any_cast : public std::exception
		{
		public:
			bad_any_cast(void) NOEXCEPT
			{}

			virtual const FXD::UTF8* what(void) const NOEXCEPT OVERRIDE
			{
				return ("bad any cast");
			}
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_BADANYCAST_H