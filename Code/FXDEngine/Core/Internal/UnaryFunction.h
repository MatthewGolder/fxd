#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_UNARYFUNCTION_H
#define FXDENGINE_CORE_INTERNAL_UNARYFUNCTION_H

namespace FXD
{
	namespace STD
	{
		// unary_function
#		define FXD_SUPPORTS_UNARY_FUNCTION 1

		template < typename Arg, typename Result >
		struct unary_function
		{
			using argument_type = Arg;
			using result_type = Result;
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_UNARYFUNCTION_H