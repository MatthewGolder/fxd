// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_INCLUSIVESCAN_H
#define FXDENGINE_CORE_INTERNAL_INCLUSIVESCAN_H

#include "FXDEngine/Core/Internal/InclusiveScanIf.h"
#include "FXDEngine/Core/Internal/Plus.h"

namespace FXD
{
	namespace STD
	{
		// inclusive_scan
#		define FXD_SUPPORTS_INCLUSIVE_SCAN 1

		template < typename InputItr, typename OutputItr, typename T, typename Predicate >
		inline OutputItr inclusive_scan_(InputItr first, InputItr last, OutputItr dst, Predicate pred, T val)
		{
			return FXD::STD::inclusive_scan_if(first, last, dst, pred, val);
		}

		template < typename InputItr, typename OutputItr, typename Predicate >
		inline OutputItr inclusive_scan(InputItr first, InputItr last, OutputItr dst, Predicate pred)
		{
			return FXD::STD::inclusive_scan_if(first, last, dst, pred);
		}

		template < typename InputItr, typename OutputItr >
		inline OutputItr inclusive_scan(InputItr first, InputItr last, OutputItr dst)
		{
			return FXD::STD::inclusive_scan(first, last, dst, FXD::STD::plus<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_INCLUSIVESCAN_H