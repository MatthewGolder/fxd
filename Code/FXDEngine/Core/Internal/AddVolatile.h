#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ADDVOLATILE_H
#define FXDENGINE_CORE_INTERNAL_ADDVOLATILE_H

#include "FXDEngine/Core/Internal/IsFunction.h"
#include "FXDEngine/Core/Internal/IsReference.h"
#include "FXDEngine/Core/Internal/IsVolatile.h"

namespace FXD
{
	namespace STD
	{
		// add_volatile
#		define FXD_SUPPORTS_ADD_VOLATILE 1

		namespace Internal
		{
			template < typename T, bool = FXD::STD::is_volatile < T >::value || FXD::STD::is_reference< T >::value || FXD::STD::is_function< T >::value >
			struct _add_volatile
			{
				using type = T;
			};

			template < typename T >
			struct _add_volatile< T, false >
			{
				using type = volatile T;
			};
		} //namespace Internal

		template < typename T >
		struct add_volatile
		{
			using type = typename FXD::STD::Internal::_add_volatile< T >::type;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using add_volatile_t = typename FXD::STD::add_volatile< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ADDVOLATILE_H