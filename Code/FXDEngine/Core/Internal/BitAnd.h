#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_BITAND_H
#define FXDENGINE_CORE_INTERNAL_BITAND_H

#include "FXDEngine/Core/Internal/Config.h"
#include <utility>

namespace FXD
{
	namespace STD
	{
		// bit_and
#		define FXD_SUPPORTS_BIT_AND 1

		template < typename T = void >
		struct bit_and
		{
			using first_argument_type = T;
			using second_argument_type = T;
			using result_type = T;

			CONSTEXPR14 T operator()(const T& lhs, const T& rhs) const
			{
				return lhs & rhs;
			}
		};

		template <>
		struct bit_and< void >
		{
			using is_transparent = FXD::S32;

			template < typename LHS, typename RHS >
			CONSTEXPR14 auto operator()(LHS&& lhs, RHS&& rhs)const->decltype(std::forward< LHS >(lhs) & std::forward< RHS >(rhs))
			{
				return std::forward< LHS >(lhs) & std::forward< RHS >(rhs);
			}
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_BITAND_H