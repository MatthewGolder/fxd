#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_HASVIRTUALDESTRUCTOR_H
#define FXDENGINE_CORE_INTERNAL_HASVIRTUALDESTRUCTOR_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Constants.h"

namespace FXD
{
	namespace STD
	{
		// has_virtual_destructor
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC) || defined(COMPILER_CLANG))
#		define FXD_SUPPORTS_HAS_VIRTUAL_DESTRUCTOR 1

		template < typename T >
		struct has_virtual_destructor : public FXD::STD::integral_constant< bool, __has_virtual_destructor(T) >
		{};

#		else
#		define FXD_SUPPORTS_HAS_VIRTUAL_DESTRUCTOR 0

#		endif

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool has_virtual_destructor_v = FXD::STD::has_virtual_destructor< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_HASVIRTUALDESTRUCTOR_H