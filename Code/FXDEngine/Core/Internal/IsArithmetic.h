#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISARITHMETIC_H
#define FXDENGINE_CORE_INTERNAL_ISARITHMETIC_H

#include "FXDEngine/Core/Internal/IsArithmetic.h"
#include "FXDEngine/Core/Internal/IsFloatingPoint.h"
#include "FXDEngine/Core/Internal/IsIntegral.h"

namespace FXD
{
	namespace STD
	{
		// is_arithmetic
#		define FXD_SUPPORTS_IS_ARITMETIC 1

		template < typename T >
		struct is_arithmetic : public FXD::STD::integral_constant< bool, FXD::STD::is_integral< T >::value || FXD::STD::is_floating_point< T >::value >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_arithmetic_v = FXD::STD::is_arithmetic< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISARITHMETIC_H