// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ADJACENTDIFFERENCE_H
#define FXDENGINE_CORE_INTERNAL_ADJACENTDIFFERENCE_H

#include "FXDEngine/Core/Internal/AdjacentDifferenceIf.h"
#include "FXDEngine/Core/Internal/Minus.h"

namespace FXD
{
	namespace STD
	{
		// adjacent_difference
#		define FXD_SUPPORTS_ADJACENT_DIFFERENCE 1

		template < typename InputItr, typename OutputItr, typename Predicate >
		inline OutputItr adjacent_difference(InputItr first, InputItr last, OutputItr dst, Predicate pred)
		{
			return FXD::STD::adjacent_difference_if(first, last, dst, pred);
		}

		template < typename InputItr, typename OutputItr >
		inline OutputItr adjacent_difference(InputItr first, InputItr last, OutputItr dst)
		{
			return FXD::STD::adjacent_difference(first, last, dst, FXD::STD::minus<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ADJACENTDIFFERENCE_H