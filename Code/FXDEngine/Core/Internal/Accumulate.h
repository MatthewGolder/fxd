// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ACCUMULATE_H
#define FXDENGINE_CORE_INTERNAL_ACCUMULATE_H

#include "FXDEngine/Core/Internal/AccumulateIf.h"
#include "FXDEngine/Core/Internal/Plus.h"

namespace FXD
{
	namespace STD
	{
		// accumulate
#		define FXD_SUPPORTS_ACCUMULATE 1

		template < typename InputItr, typename T, typename Predicate >
		inline T accumulate(InputItr first, InputItr last, T val, Predicate pred)
		{
			return FXD::STD::accumulate_if(first, last, val, pred);
		}

		template < typename InputItr, typename T >
		inline T accumulate(InputItr first, InputItr last, T val)
		{
			return FXD::STD::accumulate(first, last, val, FXD::STD::plus<>());
		}

		template < typename InputItr >
		inline typename FXD::STD::iterator_traits< InputItr >::value_type accumulate(InputItr first, InputItr last)
		{
			return FXD::STD::accumulate(first, last, typename FXD::STD::iterator_traits< InputItr >::value_type{});
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ACCUMULATE_H