#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_PIECEWISECONSTRUCT_H
#define FXDENGINE_CORE_INTERNAL_PIECEWISECONSTRUCT_H

#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// piecewise_construct_t
#		define FXD_SUPPORTS_PIECEWISE_CONSTRUCT_T 1

		struct piecewise_construct_t
		{
			EXPLICIT piecewise_construct_t() = default;
		};

		CONSTEXPR piecewise_construct_t piecewise_construct = FXD::STD::piecewise_construct_t();

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_PIECEWISECONSTRUCT_H