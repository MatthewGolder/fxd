#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_POPHEAPIF_H
#define FXDENGINE_CORE_INTERNAL_POPHEAPIF_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/ShiftHeap.h"

namespace FXD
{
	namespace STD
	{
		// pop_heap_if
#		define FXD_SUPPORTS_POP_HEAP_IF 1

		template < typename RandomAccessItr, typename Predicate >
		inline void pop_heap_if(RandomAccessItr first, RandomAccessItr last, Predicate pred)
		{
			using difference_type = typename FXD::STD::iterator_traits< RandomAccessItr >::difference_type;

			difference_type nLen = (last - first);

			if (nLen > 1)
			{
				swap(*first, *--last);
				FXD::STD::Internal::_shift_heap_down(first, last, pred, (nLen - 1), first);
			}
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_POPHEAPIF_H