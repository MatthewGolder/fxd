// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REMOVECOPY_H
#define FXDENGINE_CORE_INTERNAL_REMOVECOPY_H

#include "FXDEngine/Core/Internal/EqualTo.h"
#include "FXDEngine/Core/Internal/RemoveCopyIf.h"

namespace FXD
{
	namespace STD
	{
		// remove_copy
#		define FXD_SUPPORTS_REMOVE_COPY 1

		template < typename InputItr, typename OutputItr, typename T, typename Predicate >
		inline OutputItr remove_copy(InputItr first, InputItr last, OutputItr dst, const T& val, Predicate pred)
		{
			return FXD::STD::remove_copy_if(first, last, dst, [&](const T& x) { return pred(x, val); });
		}

		template < typename InputItr, typename OutputItr, typename T >
		inline OutputItr remove_copy(InputItr first, InputItr last, OutputItr dst, const T& val)
		{
			return FXD::STD::remove_copy(first, last, dst, val, FXD::STD::equal_to<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REMOVECOPY_H