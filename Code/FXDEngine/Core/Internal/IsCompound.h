#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISCOMPOUND_H
#define FXDENGINE_CORE_INTERNAL_ISCOMPOUND_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsFundamental.h"

namespace FXD
{
	namespace STD
	{
		// is_compound
#		define FXD_SUPPORTS_IS_COMPOUND 1

		template < typename T >
		struct is_compound : public FXD::STD::integral_constant< bool, !FXD::STD::is_fundamental< T >::value >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_compound_v = FXD::STD::is_compound< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISCOMPOUND_H