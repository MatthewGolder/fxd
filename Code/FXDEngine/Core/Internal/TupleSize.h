// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_TUPLESIZE_H
#define FXDENGINE_CORE_INTERNAL_TUPLESIZE_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Void_t.h"

namespace FXD
{
	namespace Core
	{
		template < typename T1, typename T2 >
		class Pair;
	} //namespace Core

	namespace Container
	{
		template < typename T, FXD::U32 SIZE >
		class Array;

		template < typename... >
		class Tuple;
	} //namespace Container
	
	template < typename Tup >
	struct tuple_size;

	namespace Internal
	{
		template < typename Tup, typename = void >
		struct _tuple_size_sfinae
		{};

		template < typename Tup >
		struct _tuple_size_sfinae< Tup, FXD::STD::void_t< decltype(FXD::tuple_size< Tup >::value) > > : FXD::STD::integral_constant< size_t, FXD::tuple_size< Tup >::value >
		{};
	} //namespace Internal

	template < typename Tup >
	struct tuple_size< const Tup > : FXD::Internal::_tuple_size_sfinae< Tup >
	{};

	template < typename Tup >
	struct tuple_size< volatile Tup > : FXD::Internal::_tuple_size_sfinae< Tup >
	{};

	template< typename Tup >
	struct tuple_size< const volatile Tup > : FXD::Internal::_tuple_size_sfinae< Tup >
	{};

	template < typename _Ty, size_t SIZE >
	struct tuple_size< FXD::Container::Array< _Ty, SIZE > > : FXD::STD::integral_constant< size_t, SIZE >
	{};

	template < typename... Args >
	struct tuple_size< FXD::Container::Tuple < Args... > > : FXD::STD::integral_constant< size_t, sizeof...(Args) >
	{};

	template < class T1, class T2 >
	struct tuple_size< FXD::Core::Pair< T1, T2 > > : FXD::STD::integral_constant< size_t, 2 >
	{};

#if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

	template < class T >
	CONSTEXPR size_t tuple_size_v = FXD::tuple_size< T >::value;

#endif

} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_TUPLESIZE_H