#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISREFERENCE_H
#define FXDENGINE_CORE_INTERNAL_ISREFERENCE_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsLValueReference.h"
#include "FXDEngine/Core/Internal/IsRValueReference.h"

namespace FXD
{
	namespace STD
	{
		// is_reference
#		define FXD_SUPPORTS_IS_REFERENCE 1

		template < typename T >
		struct is_reference : public FXD::STD::integral_constant< bool, FXD::STD::is_lvalue_reference< T >::value || ::FXD::STD::is_rvalue_reference< T >::value >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_reference_v = FXD::STD::is_reference< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISREFERENCE_H