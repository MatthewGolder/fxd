#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISNOTHROWSWAPPABLEWITH_H
#define FXDENGINE_CORE_INTERNAL_ISNOTHROWSWAPPABLEWITH_H

#include "FXDEngine/Core/Internal/SwapHelper.h"

namespace FXD
{
	namespace STD
	{
		// is_nothrow_swappable_with
#		define FXD_SUPPORTS_IS_NOTHROW_SWAPPABLE_WITH 1

		template < typename T1, typename T2 >
		struct is_nothrow_swappable_with : public FXD::STD::Internal::_is_nothrow_swappable_with< T1, T2 >::type
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T1, typename T2 >
		CONSTEXPR bool is_nothrow_swappable_with_v = FXD::STD::is_nothrow_swappable_with< T1, T2 >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISNOTHROWSWAPPABLEWITH_H