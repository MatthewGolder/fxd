#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REMOVEREFERENCE_H
#define FXDENGINE_CORE_INTERNAL_REMOVEREFERENCE_H

namespace FXD
{
	namespace STD
	{
		// remove_reference
#		define FXD_SUPPORTS_REMOVE_REFERENCE 1

		template < typename T >
		struct remove_reference
		{
			using type = T;
		};

		template < typename T >
		struct remove_reference< T& >
		{
			using type = T;
		};

		template < typename T >
		struct remove_reference< T&& >
		{
			using type = T;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using remove_reference_t = typename remove_reference< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REMOVEREFERENCE_H