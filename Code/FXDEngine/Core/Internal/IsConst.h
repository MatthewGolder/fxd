#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISCONST_H
#define FXDENGINE_CORE_INTERNAL_ISCONST_H

#include "FXDEngine/Core/Internal/Constants.h"

namespace FXD
{
	namespace STD
	{
		// is_const
#		define FXD_SUPPORTS_IS_CONST 1

		template < typename T >
		struct is_const : FXD::STD::false_type
		{};

		template < typename T >
		struct is_const< const T > : FXD::STD::true_type
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_const_v = FXD::STD::is_const< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISCONST_H