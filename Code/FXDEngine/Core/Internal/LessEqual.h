#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_LESSEQUAL_H
#define FXDENGINE_CORE_INTERNAL_LESSEQUAL_H

#include "FXDEngine/Core/Internal/Config.h"
#include <utility>

namespace FXD
{
	namespace STD
	{
		// less_equal
#		define FXD_SUPPORTS_IS_LESS_EQUAL 1

		template < typename T = void >
		struct less_equal
		{
			using first_argument_type = T;
			using second_argument_type = T;
			using result_type = T;

			CONSTEXPR14 bool operator()(const T& lhs, const T& rhs) const
			{
				return (lhs <= rhs);
			}
		};

		template <>
		struct less_equal< void >
		{
			template < typename LHS, typename RHS >
			CONSTEXPR14 auto operator()(LHS&& lhs, RHS&& rhs) const->decltype(std::forward< LHS >(lhs) <= std::forward< RHS >(rhs))
			{
				return std::forward< LHS >(lhs) <= std::forward< RHS >(rhs);
			}
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_LESSEQUAL_H