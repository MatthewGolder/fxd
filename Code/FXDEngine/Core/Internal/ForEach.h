#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_FOREACH_H
#define FXDENGINE_CORE_INTERNAL_FOREACH_H

#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// for_each
#		define FXD_SUPPORTS_FOR_EACH 1

		template < typename InputItr, typename Predicate >
		inline Predicate for_each(InputItr first, InputItr last, Predicate pred)
		{
			for (; first != last; ++first)
			{
				pred(*first);
			}
			return pred;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_FOREACH_H