#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISINVOCABLE_H
#define FXDENGINE_CORE_INTERNAL_ISINVOCABLE_H

#include "FXDEngine/Core/Internal/InvokeHelper.h"
#include "FXDEngine/Core/Internal/InvokeResult.h"

namespace FXD
{
	namespace STD
	{
		// is_invocable
#		define FXD_SUPPORTS_IS_INVOCABLE

		template < typename C, typename... Args >
		struct is_invocable : FXD::STD::Internal::_invoke_traits< void, C, Args... >::is_invocable
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename C, typename... Args >
		CONSTEXPR bool is_invocable_v = FXD::STD::is_invocable< C, Args... >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISINVOCABLE_H