#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ADDCONST_H
#define FXDENGINE_CORE_INTERNAL_ADDCONST_H

#include "FXDEngine/Core/Internal/IsConst.h"
#include "FXDEngine/Core/Internal/IsReference.h"
#include "FXDEngine/Core/Internal/IsFunction.h"

namespace FXD
{
	namespace STD
	{
		// add_const
#		define FXD_SUPPORTS_ADD_CONST 1

		namespace Internal
		{
			template < typename T, bool = FXD::STD::is_const< T >::value || FXD::STD::is_reference< T >::value || FXD::STD::is_function< T >::value >
			struct _add_const
			{
				using type = T;
			};

			template < typename T >
			struct _add_const< T, false >
			{
				using type = const T;
			};
		} //namespace Internal

		template < typename T >
		struct add_const
		{
			using type = typename FXD::STD::Internal::_add_const< T >::type;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using add_const_t = typename FXD::STD::add_const< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ADDCONST_H