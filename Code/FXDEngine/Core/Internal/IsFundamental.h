#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISFUNDAMENTAL_H
#define FXDENGINE_CORE_INTERNAL_ISFUNDAMENTAL_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsArithmetic.h"
#include "FXDEngine/Core/Internal/IsNullPointer.h"
#include "FXDEngine/Core/Internal/IsVoid.h"

namespace FXD
{
	namespace STD
	{
		// is_fundamental
#		define FXD_SUPPORTS_IS_FUNDAMENTAL 1

		template < typename T >
		struct is_fundamental : public FXD::STD::integral_constant< bool, FXD::STD::is_arithmetic< T >::value || FXD::STD::is_null_pointer< T >::value || FXD::STD::is_void< T >::value >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_fundamental_v = FXD::STD::is_fundamental< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISFUNDAMENTAL_H