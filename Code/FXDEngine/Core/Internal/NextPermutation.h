// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_NEXTPERMUTATION_H
#define FXDENGINE_CORE_INTERNAL_NEXTPERMUTATION_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/NextPermutationIf.h"
#include "FXDEngine/Core/Internal/Less.h"

namespace FXD
{
	namespace STD
	{
		// next_permutation
#		define FXD_SUPPORTS_NEXT_PERMUTATION 1

		template < typename BidirItr, typename Predicate >
		inline bool next_permutation(BidirItr first, BidirItr last, Predicate pred)
		{
			return FXD::STD::next_permutation_if(first, last, pred);
		}

		template < typename BidirItr >
		inline bool next_permutation(BidirItr first, BidirItr last)
		{
			using value_type = typename FXD::STD::iterator_traits< BidirItr >::value_type;

			return FXD::STD::next_permutation(first, last, FXD::STD::less< value_type >());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_NEXTPERMUTATION_H