// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_PREVPERMUTATION_H
#define FXDENGINE_CORE_INTERNAL_PREVPERMUTATION_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/PrevPermutationIf.h"
#include "FXDEngine/Core/Internal/Less.h"

namespace FXD
{
	namespace STD
	{
		// prev_permutation
#		define FXD_SUPPORTS_PREV_PERMUTATION 1

		template < typename BidirItr, typename Predicate >
		inline bool prev_permutation(BidirItr first, BidirItr last, Predicate pred)
		{
			return FXD::STD::prev_permutation_if(first, last, pred);
		}

		template < typename BidirItr >
		inline bool prev_permutation(BidirItr first, BidirItr last)
		{
			using value_type = typename FXD::STD::iterator_traits< BidirItr >::value_type;

			return FXD::STD::prev_permutation(first, last, FXD::STD::less< value_type >());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_PREVPERMUTATION_H