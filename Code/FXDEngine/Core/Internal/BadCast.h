#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_BADCAST_H
#define FXDENGINE_CORE_INTERNAL_BADCAST_H

#include "FXDEngine/Core/Types.h"
#include <exception>

namespace FXD
{
	namespace STD
	{
		// bad_cast
#		define FXD_SUPPORTS_BAD_CAST 1

		class bad_cast : public std::exception
		{
		public:
			bad_cast(void) NOEXCEPT
			{}

			virtual const FXD::UTF8* what(void) const NOEXCEPT OVERRIDE
			{
				return ("bad cast");
			}
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_BADCAST_H