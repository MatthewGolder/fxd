// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_TUPLEELEMENT_H
#define FXDENGINE_CORE_INTERNAL_TUPLEELEMENT_H

#include "FXDEngine/Core/Internal/AddConst.h"
#include "FXDEngine/Core/Internal/AddCV.h"
#include "FXDEngine/Core/Internal/AddVolatile.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Conditional.h"

namespace FXD
{
	namespace Core
	{
		template < typename T1, typename T2 >
		class Pair;
	} //namespace Core

	namespace Container
	{
		template < typename T, FXD::U32 SIZE >
		class Array;

		template < typename... >
		class Tuple;
	} //namespace Container


	template < size_t IDX, typename Tup >
	struct tuple_element;

	template < size_t IDX, typename Tup >
	struct tuple_element< IDX, const Tup > : public FXD::tuple_element< IDX, Tup >
	{
		using base_type = FXD::tuple_element< IDX, Tup >;
		using type = FXD::STD::add_const_t< typename base_type::type>;
	};

	template < size_t IDX, typename Tup >
	struct tuple_element< IDX, volatile Tup > : public FXD::tuple_element< IDX, Tup >
	{
		using base_type = FXD::tuple_element< IDX, Tup >;
		using type = FXD::STD::add_volatile_t< typename base_type::type >;
	};

	template < size_t IDX, typename Tup >
	struct tuple_element< IDX, const volatile Tup > : public FXD::tuple_element< IDX, Tup >
	{
		using base_type = FXD::tuple_element< IDX, Tup >;
		using type = FXD::STD::add_cv_t< typename base_type::type >;
	};

	template < size_t IDX, typename T, size_t SIZE >
	struct tuple_element< IDX, Container::Array< T, SIZE > >
	{
		CTC_ASSERT((IDX < SIZE), "Core: Array index out of bounds");
		using type = T;
	};

	template < size_t IDX >
	struct tuple_element< IDX, Container::Tuple<> >
	{
		CTC_ASSERT((FXD::STD::Internal::_Always_false< FXD::STD::integral_constant< size_t, IDX > >), "Core: Tuple index out of bounds");
	};

	template < typename ThisType, typename... RestArg >
	struct tuple_element< 0, Container::Tuple< ThisType, RestArg... > >
	{
		using type = ThisType;
		using tuple_type = Container::Tuple< ThisType, RestArg... >;
	};

	template < size_t IDX, typename ThisType, typename... RestArg >
	struct tuple_element< IDX, Container::Tuple< ThisType, RestArg... > > : public FXD::tuple_element< IDX - 1, Container::Tuple< RestArg... > >
	{};

	template < size_t IDX, typename T1, typename T2 >
	struct tuple_element< IDX, Core::Pair< T1, T2 > >
	{
		CTC_ASSERT((IDX < 2), "pair index out of bounds");
		using type = FXD::STD::conditional_t< IDX == 0, T1, T2 >;
	};

	template < size_t IDX, typename Tup >
	using tuple_element_t = typename FXD::tuple_element< IDX, Tup >::type;


	namespace Internal
	{
		template < typename R, typename _Pair >
		CONSTEXPR14 R _get_pair(_Pair& pair, FXD::STD::integral_constant< size_t, 0 >) NOEXCEPT
		{
			return (pair.first);
		}

		template < typename R, typename _Pair >
		CONSTEXPR14 R _get_pair(_Pair& pair, FXD::STD::integral_constant< size_t, 1 >) NOEXCEPT
		{
			return (pair.second);
		}
	} //namespace Internal

	template < size_t IDX, typename T1, typename T2 >
	CONSTEXPR14 FXD::tuple_element_t< IDX, Core::Pair< T1, T2 > >& get(Core::Pair< T1, T2 >& pair) NOEXCEPT
	{
		using return_type = FXD::tuple_element_t< IDX, Core::Pair< T1, T2 > >&;
		return (FXD::Internal::_get_pair< return_type >(pair, FXD::STD::integral_constant< size_t, IDX >()));
	}

	template < typename T1, typename T2 >
	CONSTEXPR14 T1& get(Core::Pair< T1, T2 >& pair) NOEXCEPT
	{
		return (FXD::get< 0 >(pair));
	}

	template < typename T2, typename T1 >
	CONSTEXPR14 T2& get(Core::Pair< T1, T2 >& pair) NOEXCEPT
	{
		return (FXD::get< 1 >(pair));
	}

	template < size_t IDX, typename T1, typename T2 >
	CONSTEXPR14 const FXD::tuple_element_t< IDX, Core::Pair< T1, T2 > >& get(const Core::Pair< T1, T2 >& pair) NOEXCEPT
	{
		using const_type = const FXD::tuple_element_t< IDX, Core::Pair< T1, T2 > >&;
		return (FXD::Internal::_get_pair< const_type >(pair, FXD::STD::integral_constant< size_t, IDX >()));
	}

	template < typename T1, typename T2 >
	CONSTEXPR14 const T1& get(const Core::Pair< T1, T2 >& pair) NOEXCEPT
	{
		return (FXD::get< 0 >(pair));
	}

	template < typename T2, typename T1 >
	CONSTEXPR14 const T2& get(const Core::Pair< T1, T2 >& pair) NOEXCEPT
	{
		return (FXD::get< 1 >(pair));
	}

	template < size_t IDX, typename T1, typename T2 >
	CONSTEXPR14 FXD::tuple_element_t< IDX, Core::Pair< T1, T2 > >&& get(Core::Pair< T1, T2 >&& pair) NOEXCEPT
	{
		using return_type = FXD::tuple_element_t< IDX, Core::Pair< T1, T2 > >&&;
		return (std::forward< return_type >(FXD::get< IDX >(pair)));
	}

	template < typename T2, typename T1 >
	CONSTEXPR14 T1&& get(Core::Pair< T1, T2 >&& pair) NOEXCEPT
	{
		return (FXD::get< 0 >(std::move(pair)));
	}

	template < typename T2, typename T1 >
	CONSTEXPR14 T2&& get(Core::Pair< T1, T2 >&& pair) NOEXCEPT
	{
		return (FXD::get< 1 >(std::move(pair)));
	}

	template < size_t IDX, typename T1, typename T2 >
	CONSTEXPR14 const FXD::tuple_element_t< IDX, Core::Pair< T1, T2 > >&& get(const Core::Pair< T1, T2 >&& pair) NOEXCEPT
	{
		using return_type = const FXD::tuple_element_t< IDX, Core::Pair< T1, T2 > >&&;
		return (std::forward< return_type >(FXD::get< IDX >(pair)));
	}

	template < typename T2, typename T1 >
	CONSTEXPR14 const T1&& get(const Core::Pair< T1, T2 >&& pair) NOEXCEPT
	{
		return (FXD::get< 0 >(std::move(pair)));
	}

	template < typename T2, typename T1 >
	CONSTEXPR14 const T2&& get(const Core::Pair< T1, T2 >&& pair) NOEXCEPT
	{
		return (FXD::get< 1 >(std::move(pair)));
	}

} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_TUPLEELEMENT_H