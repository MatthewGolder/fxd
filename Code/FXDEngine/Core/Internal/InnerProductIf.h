// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_INNERPRODUCTIF_H
#define FXDENGINE_CORE_INTERNAL_INNERPRODUCTIF_H

namespace FXD
{
	namespace STD
	{
		// inner_product_if
#		define FXD_SUPPORTS_INNER_PRODUCT_IF 1

		template < typename InputItr1, typename InputItr2, typename T, typename Predicate1, typename Predicate2 >
		inline T inner_product_if(InputItr1 first1, InputItr1 last1, InputItr2 first2, T val, Predicate1 pred1, Predicate2 pred2)
		{
			while (first1 != last1)
			{
				val = pred1(std::move(val), pred2(*first1, *first2));
				++first1;
				++first2;
			}
			return val;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_INNERPRODUCTIF_H