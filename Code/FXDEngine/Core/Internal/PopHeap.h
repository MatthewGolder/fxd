#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_POPHEAP_H
#define FXDENGINE_CORE_INTERNAL_POPHEAP_H

#include "FXDEngine/Core/Internal/Less.h"
#include "FXDEngine/Core/Internal/PopHeapIf.h"

namespace FXD
{
	namespace STD
	{
		// pop_heap
#		define FXD_SUPPORTS_POP_HEAP 1

		template < typename RandomAccessItr, typename Predicate >
		inline void pop_heap(RandomAccessItr first, RandomAccessItr last, Predicate pred)
		{
			FXD::STD::pop_heap_if(first, last, pred);
		}

		template < typename RandomAccessItr >
		inline void pop_heap(RandomAccessItr first, RandomAccessItr last)
		{
			FXD::STD::pop_heap(first, last, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_POPHEAP_H