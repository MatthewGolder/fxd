// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_NONEOF_H
#define FXDENGINE_CORE_INTERNAL_NONEOF_H

#include "FXDEngine/Core/Internal/NoneOfIf.h"
#include "FXDEngine/Core/Internal/EqualTo.h"

namespace FXD
{
	namespace STD
	{
		// none_of
#		define FXD_SUPPORTS_NONE_OF 1

		template < typename InputItr, typename Predicate >
		inline bool none_of(InputItr first, InputItr last, Predicate pred)
		{
			return FXD::STD::none_of_if(first, last, pred);
		}

		template < typename InputItr >
		inline bool none_of(InputItr first, InputItr last)
		{
			return FXD::STD::none_of(first, last, FXD::STD::equal_to<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_NONEOF_H