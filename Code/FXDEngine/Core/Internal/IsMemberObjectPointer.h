#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISMEMBEROBJECTPOINTER_H
#define FXDENGINE_CORE_INTERNAL_ISMEMBEROBJECTPOINTER_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsMemberFunctionPointer.h"

namespace FXD
{
	namespace STD
	{
		// is_member_object_pointer
#	define FXD_SUPPORTS_IS_MEMBER_OBJECT_POINTER 1

		namespace Internal
		{
			template < typename >
			struct _is_member_object_pointer : public FXD::STD::false_type
			{};

			template < typename T, typename CP >
			struct _is_member_object_pointer< T CP::* > : public FXD::STD::integral_constant< bool, !FXD::STD::is_function< T >::value >
			{
				using class_type = CP;
			};
		} //namespace Internal

			/// is_member_object_pointer
			template < typename T >
			struct is_member_object_pointer : public FXD::STD::Internal::_is_member_object_pointer< typename FXD::STD::remove_cv< T >::type >::type
			{};

#	if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_member_object_pointer_v = FXD::STD::is_member_object_pointer< T >::value;

#	endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISMEMBEROBJECTPOINTER_H