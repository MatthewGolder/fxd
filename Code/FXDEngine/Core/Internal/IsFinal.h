#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISFINAL_H
#define FXDENGINE_CORE_INTERNAL_ISFINAL_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/RemoveCV.h"

namespace FXD
{
	namespace STD
	{
		// is_final
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC) || defined(COMPILER_CLANG))
#		define FXD_SUPPORTS_IS_FINAL 1

		template < typename T >
		struct is_final : public FXD::STD::integral_constant< bool, __is_final(T) >
		{};

#		else
#		define FXD_SUPPORTS_IS_FINAL 0
#		endif

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_final_v = FXD::STD::is_final< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISFINAL_H