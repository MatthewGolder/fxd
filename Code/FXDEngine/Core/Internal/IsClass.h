//Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISCLASS_H
#define FXDENGINE_CORE_INTERNAL_ISCLASS_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsUnion.h"

namespace FXD
{
	namespace STD
	{
		// is_class
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC) || defined(COMPILER_CLANG))
#		define FXD_SUPPORTS_IS_CLASS 1

		template < typename T >
		struct is_class : public FXD::STD::integral_constant< bool, __is_class(T) >
		{};

#		else
#		define FXD_SUPPORTS_IS_CLASS 1

		namespace Internal
		{
			template < typename U >
			static FXD::STD::Internal::yes_type _is_class(void (U::*)());

			template < typename U >
			static FXD::STD::Internal::no_type _is_class(...);
		} //namespace Internal

		template < typename T >
		struct is_class : public FXD::STD::integral_constant< bool, sizeof(FXD::STD::Internal::_is_class< T >(0)) == sizeof(FXD::STD::Internal::yes_type) && !FXD::STD::is_union< T >::value >
		{};

#		endif

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_class_v = FXD::STD::is_class< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISCLASS_H