#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISNOTHROWASSIGNABLE_H
#define FXDENGINE_CORE_INTERNAL_ISNOTHROWASSIGNABLE_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Declval.h"
#include "FXDEngine/Core/Internal/IsAssignable.h"

namespace FXD
{
	namespace STD
	{
		// is_nothrow_assignable
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC) || defined(COMPILER_CLANG))
#		define FXD_SUPPORTS_IS_NOTHROW_ASSIGNABLE 1

		template < typename T, typename U >
		struct is_nothrow_assignable : FXD::STD::integral_constant< bool, __is_nothrow_assignable(T, U) >
		{};

#		else
#		define FXD_SUPPORTS_IS_NOTHROW_ASSIGNABLE 1

		namespace Internal
		{
			template < bool, typename T, typename U >
			struct _is_nothrow_assignable;

			template < typename T, typename U >
			struct _is_nothrow_assignable< false, T, U > : public FXD::STD::false_type
			{};

			template < typename T, typename U >
			struct _is_nothrow_assignable< true, T, U > : public FXD::STD::integral_constant< bool, NOEXCEPT(FXD::STD::declval< T >() = FXD::STD::declval< U >()) >
			{};
		} //namespace Internal

		template < typename T, typename U >
		struct is_nothrow_assignable : public FXD::STD::Internal::_is_nothrow_assignable< FXD::STD::is_assignable< T, U >::value, T, U >
		{};

#		endif

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T, typename U >
		CONSTEXPR bool is_nothrow_assignable_v = FXD::STD::is_nothrow_assignable< T, U >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISNOTHROWASSIGNABLE_H