#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISTRIVIALLYCONSTRUCTIBLE_H
#define FXDENGINE_CORE_INTERNAL_ISTRIVIALLYCONSTRUCTIBLE_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsConstructible.h"

namespace FXD
{
	namespace STD
	{
		// is_trivially_constructible
#		define FXD_SUPPORTS_IS_TRIVIALLY_CONSTRUCTIBLE 1

		template < typename T, typename... Args >
		struct is_trivially_constructible : public FXD::STD::integral_constant< bool, FXD::STD::is_constructible< T, Args... >::value && __is_trivially_constructible(T, Args...) >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T, typename... Args >
		CONSTEXPR bool is_trivially_constructible_v = FXD::STD::is_trivially_constructible< T, Args... >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISTRIVIALLYCONSTRUCTIBLE_H