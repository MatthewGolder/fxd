#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_SEARCHNIF_H
#define FXDENGINE_CORE_INTERNAL_SEARCHNIF_H

#include "FXDEngine/Core/Internal/EqualTo.h"
#include "FXDEngine/Container/Internal/IteratorTraits.h"

namespace FXD
{
	namespace STD
	{
		// search_n_if
#		define FXD_SUPPORTS_SEARCH_N_IF 1

		namespace Internal
		{
			template < typename ForwardItr, typename Size, typename T, typename Predicate >
			inline ForwardItr _search_n_if(ForwardItr first, ForwardItr last, Size nCount, const T& val, Predicate& pred, FXD::STD::forward_iterator_tag)
			{
				if (nCount <= 0)
				{
					return (first);
				}

				for (; first != last; ++first)
				{
					if (pred(*first, val))
					{
						ForwardItr midItr = first;
						for (Size nCount1 = nCount; ; )
						{
							if (--nCount1 == 0)
							{
								return first;	// found rest of match, report it
							}
							else if (++midItr == last)
							{
								return last;	// short match at end
							}
							else if (pred(*midItr, val) == false)
							{
								break;
							}
						}

						first = midItr;	// pick up just beyond failed match
					}
				}
				return last;
			}

			template < typename ForwardItr, typename Size, typename T, typename Predicate >
			inline ForwardItr _search_n_if(ForwardItr first, ForwardItr last, Size nCount, const T& val, Predicate& pred, FXD::STD::random_access_iterator_tag)
			{
				if (nCount <= 0)
				{
					return first;
				}

				ForwardItr oldfirst = first;
				for (Size nInc = 0; nCount <= last - oldfirst; )
				{
					first = oldfirst + nInc;
					if (pred(*first, val))
					{
						Size nCount1 = nCount;
						ForwardItr midItr = first;
						for (; oldfirst != first && pred(first[-1], val); --first)
						{
							--nCount1;	// back up over any skipped prefix
						}
						if (nCount1 <= last - midItr)
						{
							for (; ; )
							{	// enough left, test suffix
								if (--nCount1 == 0)
								{
									return first;	// found rest of match, report it
								}
								else if (pred(*++midItr, val) == false)
								{
									break;
								}
							}
						}
						oldfirst = ++midItr;	// failed match, take small jump
						nInc = 0;
					}
					else
					{	// no match, take big jump and back up as needed
						oldfirst = first + 1;
						nInc = nCount - 1;
					}
				}
				return last;
			}
		} //namespace Internal

		template < typename ForwardItr, typename Size, typename T, typename Predicate >
		inline ForwardItr search_n_if(ForwardItr first, ForwardItr last, Size nCount, const T& val, Predicate pred)
		{
			using IC = typename FXD::STD::iterator_traits< ForwardItr >::iterator_category;

			return FXD::STD::Internal::_search_n_if(first, last, nCount, val, pred, IC());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_SEARCHNIF_H