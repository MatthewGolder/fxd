#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_FINDLASTNOTOF_H
#define FXDENGINE_CORE_INTERNAL_FINDLASTNOTOF_H

#include "FXDEngine/Core/Internal/FindLastNotOfIf.h"
#include "FXDEngine/Core/Internal/EqualTo.h"

namespace FXD
{
	namespace STD
	{
		// find_last_not_of
#		define FXD_SUPPORTS_FIND_LAST_NOT_OF 1

		template < typename InputItr, typename ForwardItr, typename Predicate >
		inline InputItr find_last_not_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)
		{
			return FXD::STD::find_last_not_of_if(first1, last1, first2, last2, pred);
		}

		template < typename InputItr, typename ForwardItr >
		inline InputItr find_last_not_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)
		{
			return FXD::STD::find_last_not_of(first1, last1, first2, last2, FXD::STD::equal_to<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_FINDLASTNOTOF_H