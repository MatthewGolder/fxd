// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REF_H
#define FXDENGINE_CORE_INTERNAL_REF_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/ReferenceWrapper.h"

namespace FXD
{
	namespace STD
	{
		// ref
#		define FXD_SUPPORTS_REF 1

		template < typename T >
		inline FXD::STD::reference_wrapper< T > ref(T& val) NOEXCEPT
		{
			return FXD::STD::reference_wrapper< T >(val);
		}

		template < typename T >
		inline FXD::STD::reference_wrapper< T > ref(FXD::STD::reference_wrapper< T > val) NOEXCEPT
		{
			return FXD::STD::ref(val.get());
		}

		template < typename T >
		void ref(const T&&) = delete;

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REF_H