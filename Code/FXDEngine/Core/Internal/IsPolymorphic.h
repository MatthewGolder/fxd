#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISPOLYMORPHIC_H
#define FXDENGINE_CORE_INTERNAL_ISPOLYMORPHIC_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/RemoveCV.h"

namespace FXD
{
	namespace STD
	{
		// is_polymorphic
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC) || defined(COMPILER_CLANG))
#		define FXD_SUPPORTS_IS_POLYMORPHIC 1

		template < typename T >
		struct is_polymorphic : public FXD::STD::integral_constant< bool, __is_polymorphic(T) >
		{};

#		else
#		define FXD_SUPPORTS_IS_POLYMORPHIC 1

		namespace Internal
		{
			template < typename T >
			struct _is_polymorphic_impl_1
			{
				using t = typename FXD::STD::remove_cv< T >::type;

				struct helper_1 : public t
				{
					helper_1(void);
					~helper_1(void) throw();

					FXD::U8 pad[64];
				};

				struct helper_2 : public t
				{
					helper_2(void);
					virtual ~helper_2(void) throw();

#		if defined(COMPILER_MSVC)
					virtual void foo(void);
#		endif
					FXD::U8 pad[64];
				};

				static const bool value = (sizeof(helper_1) == sizeof(helper_2));
			};

			template < typename T >
			struct _is_polymorphic_impl_2
			{
				static const bool value = false;
			};

			template < bool is_class >
			struct _is_polymorphic_selector
			{
				template < typename T >
				struct impl
				{
					typedef FXD::STD::Internal::_is_polymorphic_impl_2< T > type;
				};
			};

			template <>
			struct _is_polymorphic_selector< true >
			{
				template < typename T >
				struct impl
				{
					using type = FXD::STD::Internal::_is_polymorphic_impl_1< T >;
				};
			};

			template < typename T >
			struct _is_polymorphic
			{
				using selector = FXD::STD::Internal::_is_polymorphic_selector< FXD::STD::is_class< T >::value >;
				using binder = typename selector::template impl< T >;
				using imp_type = typename binder::type;

				static const bool value = imp_type::value;
			};
		} //namespace Internal

		template < typename T >
		struct is_polymorphic : public FXD::STD::integral_constant< bool, FXD::STD::Internal::_is_polymorphic< T >::value >
		{};

#		endif

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_polymorphic_v = FXD::STD::is_polymorphic< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISEMPTY_H