// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_EXCLUSIVESCANIF_H
#define FXDENGINE_CORE_INTERNAL_EXCLUSIVESCANIF_H

namespace FXD
{
	namespace STD
	{
		// exclusive_scan_if
#		define FXD_SUPPORTS_EXCLUSIVE_SCAN_IF 1

		template < typename InputItr, typename OutputItr, typename T, typename Predicate >
		inline OutputItr exclusive_scan_if(InputItr first, InputItr last, OutputItr dst, T val, Predicate pred)
		{
			if (first != last)
			{
				T temp = val;
				do
				{
					val = pred(val, *first);
					*dst = temp;
					temp = val;
					++dst;
				} while (++first != last);
			}
			return dst;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_EXCLUSIVESCANIF_H