#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_UNIQUEIF_H
#define FXDENGINE_CORE_INTERNAL_UNIQUEIF_H

#include "FXDEngine/Core/Internal/AdjacentFind.h"

namespace FXD
{
	namespace STD
	{
		// unique_if
#		define FXD_SUPPORTS_UNIQUE_IF 1

		template < typename ForwardItr, typename Predicate >
		inline ForwardItr unique_if(ForwardItr first, ForwardItr last, Predicate pred)
		{
			first = FXD::STD::adjacent_find< ForwardItr, Predicate >(first, last, pred);

			if (first != last)
			{
				ForwardItr dest(first);

				for (++first; first != last; ++first)
				{
					if (!pred(*dest, *first))
					{
						*++dest = *first;
					}
				}
				return ++dest;
			}
			return last;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_UNIQUEIF_H