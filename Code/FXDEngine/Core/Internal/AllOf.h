// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ALLOF_H
#define FXDENGINE_CORE_INTERNAL_ALLOF_H

#include "FXDEngine/Core/Internal/AllOfIf.h"
#include "FXDEngine/Core/Internal/EqualTo.h"

namespace FXD
{
	namespace STD
	{
		// all_of
#		define FXD_SUPPORTS_ALL_OF 1

		template < typename InputItr, typename Predicate >
		inline bool all_of(InputItr first, InputItr last, Predicate pred)
		{
			return FXD::STD::all_of_if(first, last, pred);
		}

		template < typename InputItr >
		inline bool all_of(InputItr first, InputItr last)
		{
			return FXD::STD::all_of(first, last, FXD::STD::equal_to<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ALLOF_H