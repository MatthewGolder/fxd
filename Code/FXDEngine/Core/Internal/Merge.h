#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MERGE_H
#define FXDENGINE_CORE_INTERNAL_MERGE_H

#include "FXDEngine/Container/Iterator.h"
#include "FXDEngine/Core/Internal/MergeIf.h"
#include "FXDEngine/Core/Internal/Less.h"

namespace FXD
{
	namespace STD
	{
		// merge
#		define FXD_SUPPORTS_MERGE 1

		template < typename InputItr1, typename InputItr2, typename OutputItr, typename Predicate >
		inline OutputItr merge(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst, Predicate pred)
		{
			return FXD::STD::merge_if(first1, last1, first2, last2, dst, pred);
		}

		template < typename InputItr1, typename InputItr2, typename OutputItr >
		inline OutputItr merge(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)
		{
			using Less = FXD::STD::less< typename FXD::STD::iterator_traits< InputItr1 >::value_type >;

			return FXD::STD::merge< InputItr1, InputItr2, OutputItr, Less >(first1, last1, first2, last2, dst, Less());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MERGE_H