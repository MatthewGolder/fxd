#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_LEXICOGRAPHICALCOMPARE_H
#define FXDENGINE_CORE_INTERNAL_LEXICOGRAPHICALCOMPARE_H

#include "FXDEngine/Core/Internal/LexicographicalCompareIf.h"
#include "FXDEngine/Core/Internal/Less.h"

namespace FXD
{
	namespace STD
	{
		// lexicographical_compare
#		define FXD_SUPPORTS_LEXICOGRAPHICAL_COMPARE 1

		template < typename InputItr1, typename InputItr2, typename Predicate >
		inline bool lexicographical_compare(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)
		{
			return FXD::STD::lexicographical_compare_if(first1, last1, first2, last2, pred);
		}

		template < typename InputItr1, typename InputItr2 >
		inline bool lexicographical_compare(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)
		{
			return FXD::STD::lexicographical_compare(first1, last1, first2, last2, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_LEXICOGRAPHICALCOMPARE_H