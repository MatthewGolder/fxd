#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISFUNCTION_H
#define FXDENGINE_CORE_INTERNAL_ISFUNCTION_H

#include "FXDEngine/Core/Internal/ArgTypes.h"
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Constants.h"
//#include <xstddef>

namespace FXD
{
	namespace STD
	{
		// is_function
#		define FXD_SUPPORTS_IS_FUNCTION 1

		template < typename >
		struct is_function : public FXD::STD::false_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args...) > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args...) & > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args...) && > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args..., ...) > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args..., ...) & > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args..., ...) && > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args...) const > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args...) const& > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args...) const&& > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args..., ...) const > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args..., ...) const& > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args..., ...) const&& > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args...) volatile > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args...) volatile& > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args...) volatile&& > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args..., ...) volatile > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args..., ...) volatile& > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args..., ...) volatile&& > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args...) const volatile > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args...) const volatile& > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args...) const volatile&& > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args..., ...) const volatile > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args..., ...) const volatile& > : public FXD::STD::true_type
		{};

		template < typename R, typename... Args >
		struct is_function< R(Args..., ...) const volatile&& > : public FXD::STD::true_type
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_function_v = FXD::STD::is_function< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISFUNCTION_H