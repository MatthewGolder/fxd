// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_PARTIALSUMIF_H
#define FXDENGINE_CORE_INTERNAL_PARTIALSUMIF_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"

namespace FXD
{
	namespace STD
	{
		// partial_sum_if
#		define FXD_SUPPORTS_PARTITION_SUM_IF 1

		template < typename InputItr, typename OutputItr, typename Predicate >
		inline OutputItr partial_sum_if(InputItr first, InputItr last, OutputItr dst, Predicate pred)
		{
			if (first == last)
			{
				return dst;
			}

			typename FXD::STD::iterator_traits< InputItr >::value_type sum = *first;

			*dst = sum;

			while (++first != last)
			{
				sum = pred(std::move(sum), *first);
				*++dst = sum;
			}
			return ++dst;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_PARTIALSUMIF_H