// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ITOA_H
#define FXDENGINE_CORE_INTERNAL_ITOA_H

namespace FXD
{
	namespace STD
	{
		// iota
#		define FXD_SUPPORTS_ITOA 1

		template < typename ForwardItr, typename T >
		inline void iota(ForwardItr first, ForwardItr last, T val)
		{
			while (first != last)
			{
				*first++ = val;
				++val;
			}
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ITOA_H