#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_SEARCH_H
#define FXDENGINE_CORE_INTERNAL_SEARCH_H

#include "FXDEngine/Core/Internal/SearchIf.h"
#include "FXDEngine/Core/Internal/EqualTo.h"

namespace FXD
{
	namespace STD
	{
		// search
#		define FXD_SUPPORTS_SEARCH 1

		template < typename ForwardItr1, typename ForwardItr2, typename Predicate >
		inline ForwardItr1 search(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2, ForwardItr2 last2, Predicate pred)
		{
			return FXD::STD::search_if(first1, last1, first2, last2, pred);
		}

		template < typename ForwardItr1, typename ForwardItr2 >
		inline ForwardItr1 search(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2, ForwardItr2 last2)
		{
			return FXD::STD::search(first1, last1, first2, last2, FXD::STD::equal_to<>());
		}

		template < typename ForwardItr, typename Searcher >
		inline ForwardItr search(ForwardItr first, ForwardItr last, const Searcher& searcher)
		{
			return searcher(first, last).first;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_SEARCH_H