#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MAXELEMENTIF_H
#define FXDENGINE_CORE_INTERNAL_MAXELEMENTIF_H

#include "FXDEngine/Core/Internal/Less.h"

namespace FXD
{
	namespace STD
	{
		// max_element_if
#		define FXD_SUPPORTS_MAX_ELEMENT_IF 1

		template < typename ForwardItr, typename Predicate >
		CONSTEXPR17 ForwardItr max_element_if(ForwardItr first, ForwardItr last, Predicate pred)
		{
			if (first != last)
			{
				ForwardItr curMin = first;
				while (++first != last)
				{
					if (pred(*curMin, *first))
					{
						curMin = first;
					}
				}
				return curMin;
			}
			return first;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MAXELEMENTIF_H