// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REMOVE_H
#define FXDENGINE_CORE_INTERNAL_REMOVE_H

#include "FXDEngine/Core/Internal/RemoveIf.h"

namespace FXD
{
	namespace STD
	{
		// remove
#		define FXD_SUPPORTS_REMOVE 1

		template < typename ForwardItr, typename T, typename Predicate >
		inline ForwardItr remove(ForwardItr first, ForwardItr last, const T& val, Predicate pred)
		{
			return FXD::STD::remove_if(first, last, [&](const T& x) { return pred(x, val); });
		}

		template < typename ForwardItr, typename T >
		inline ForwardItr remove(ForwardItr first, ForwardItr last, const T& val)
		{
			return FXD::STD::remove(first, last, val, FXD::STD::equal_to<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REMOVE_H