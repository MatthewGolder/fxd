#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_SHUFFLE_H
#define FXDENGINE_CORE_INTERNAL_SHUFFLE_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/MakeUnsigned.h"
#include "FXDEngine/Core/Internal/Swap.h"
#include "FXDEngine/Core/Random.h"

namespace FXD
{
	namespace STD
	{
		// shuffle
#		define FXD_SUPPORTS_SHUFFLE 1

		template < typename RandomAccessItr, typename UniformRandomNumberGenerator >
		void shuffle(RandomAccessItr first, RandomAccessItr last, UniformRandomNumberGenerator&& urng)
		{
			if (first != last)
			{
				using difference_type = typename FXD::STD::iterator_traits< RandomAccessItr >::difference_type;
				using unsigned_difference_type = typename FXD::STD::make_unsigned< difference_type >::type;
				using uniform_int_distribution_type = typename FXD::STD::uniform_int_distribution< unsigned_difference_type >;
				using uniform_int_distribution_param_type = typename uniform_int_distribution_type::param_type;

				uniform_int_distribution_type uid;

				for (RandomAccessItr itr = first + 1; itr != last; ++itr)
				{
					FXD::STD::iter_swap(itr, first + uid(urng, uniform_int_distribution_param_type(0, itr - first)));
				}
			}
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_SHUFFLE_H