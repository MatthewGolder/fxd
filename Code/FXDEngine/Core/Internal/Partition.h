#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_PARTITION_H
#define FXDENGINE_CORE_INTERNAL_PARTITION_H

#include "FXDEngine/Core/Internal/Swap.h"

namespace FXD
{
	namespace STD
	{
		// partition
#		define FXD_SUPPORTS_PARTITION 1

		template < typename ForwardItr, typename Predicate >
		inline ForwardItr partition(ForwardItr first, ForwardItr last, Predicate pred)
		{
			if (first != last)
			{
				while (pred(*first))
				{
					if (++first == last)
					{
						return first;
					}
				}

				ForwardItr mid = first;

				while (++mid != last)
				{
					if (pred(*mid))
					{
						FXD::STD::swap(*first, *mid);
						++first;
					}
				}
			}
			return first;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_PARTITION_H