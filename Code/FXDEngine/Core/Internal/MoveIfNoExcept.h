#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MOVEIFNOEXCPET_H
#define FXDENGINE_CORE_INTERNAL_MOVEIFNOEXCPET_H

#include "FXDEngine/Core/Internal/Conditional.h"
#include "FXDEngine/Core/Internal/IsCopyConstructible.h"
#include "FXDEngine/Core/Internal/IsNoThrowMoveConstructible.h"

namespace FXD
{
	namespace STD
	{
		// move_if_noexcept
#		define FXD_SUPPORTS_MOVE_IF_NOEXCEPT 1

		template < typename T >
		typename FXD::STD::conditional< !FXD::STD::is_nothrow_move_constructible< T >::value && FXD::STD::is_copy_constructible< T >::value, const T&, T&& >::type move_if_noexcept(T& x) NOEXCEPT
		{
			return (std::move(x));
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MOVEIFNOEXCPET_H