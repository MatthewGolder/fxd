#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_COUNT_H
#define FXDENGINE_CORE_INTERNAL_COUNT_H

#include "FXDEngine/Core/Internal/CountIf.h"
#include "FXDEngine/Core/Internal/EqualTo.h"

namespace FXD
{
	namespace STD
	{
		// count
#		define FXD_SUPPORTS_COUNT 1

		template < typename InputItr, typename T, typename Predicate >
		inline typename FXD::STD::iterator_traits< InputItr >::difference_type count(InputItr first, InputItr last, const T& val, Predicate pred)
		{
			return FXD::STD::count_if(first, last, [&](const T& x) { return pred(x, val); });
		}

		template < typename InputItr, typename T >
		inline typename FXD::STD::iterator_traits< InputItr >::difference_type count(InputItr first, InputItr last, const T& val)
		{
			return FXD::STD::count(first, last, val, FXD::STD::equal_to<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_COUNT_H