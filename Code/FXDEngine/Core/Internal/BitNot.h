#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_BITNOT_H
#define FXDENGINE_CORE_INTERNAL_BITNOT_H

#include "FXDEngine/Core/Internal/Config.h"
#include <utility>

namespace FXD
{
	namespace STD
	{
		// bit_not
#		define FXD_SUPPORTS_BIT_NOT 1

		template < typename T = void >
		struct bit_not
		{
			using first_argument_type = T;
			using result_type = T;

			CONSTEXPR14 T operator()(const T& val) const
			{
				return ~val;
			}
		};

		template <>
		struct bit_not< void >
		{
			using is_transparent = FXD::S32;

			template < typename T >
			CONSTEXPR14 auto operator()(T&& val)const->decltype(~std::forward< T >(val))
			{
				return ~std::forward< T >(val);
			}
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_BITNOT_H