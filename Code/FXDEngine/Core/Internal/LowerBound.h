#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_LOWERBOUND_H
#define FXDENGINE_CORE_INTERNAL_LOWERBOUND_H

#include "FXDEngine/Core/Internal/Less.h"
#include "FXDEngine/Core/Internal/LowerBoundIf.h"

namespace FXD
{
	namespace STD
	{
		// lower_bound
#		define FXD_SUPPORTS_LOWER_BOUND 1

		template < typename ForwardItr, typename T, typename Predicate >
		inline ForwardItr lower_bound(ForwardItr first, ForwardItr last, const T& val, Predicate pred)
		{
			return FXD::STD::lower_bound_if(first, last, val, pred);
		}

		template < typename ForwardItr, typename T >
		inline ForwardItr lower_bound(ForwardItr first, ForwardItr last, const T& val)
		{
			return FXD::STD::lower_bound(first, last, val, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_LOWERBOUND_H