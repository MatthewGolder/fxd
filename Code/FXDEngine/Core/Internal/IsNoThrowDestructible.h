#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISNOTHROWDESTRUCTIBLE_H
#define FXDENGINE_CORE_INTERNAL_ISNOTHROWDESTRUCTIBLE_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Declval.h"
#include "FXDEngine/Core/Internal/Extent.h"
#include "FXDEngine/Core/Internal/Identity.h"
#include "FXDEngine/Core/Internal/IsArray.h"
#include "FXDEngine/Core/Internal/IsFunction.h"
#include "FXDEngine/Core/Internal/IsReference.h"
#include "FXDEngine/Core/Internal/IsScalar.h"
#include "FXDEngine/Core/Internal/IsVoid.h"
#include "FXDEngine/Core/Internal/RemoveAllExtents.h"

namespace FXD
{
	namespace STD
	{
		// is_nothrow_destructible
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && defined(COMPILER_MSVC)
#		define FXD_SUPPORTS_IS_NOTHROW_MOVE_DESTRUCTIBLE 1

		template < typename T >
		struct is_nothrow_destructible : FXD::STD::integral_constant< bool, __is_nothrow_destructible(T) >
		{};

#		else
#		define FXD_SUPPORTS_IS_NOTHROW_MOVE_DESTRUCTIBLE 1

		namespace Internal
		{
			struct _is_nothrow_destructible_impl
			{
			public:
				template < typename T >
				static FXD::STD::integral_constant< bool, NOEXCEPT(FXD::STD::declval< T& >().~T()) > test(FXD::S32);

				template < typename >
				static FXD::STD::false_type test(...);
			};

			template < typename T >
			struct _is_nothrow_destructible_zero : public FXD::STD::identity< decltype(FXD::STD::Internal::_is_nothrow_destructible_impl::test< T >(0)) >::type
			{};

			template < typename T, bool = FXD::STD::is_void< T >::value || (FXD::STD::is_array< T >::value && !FXD::STD::extent< T >::value) || FXD::STD::is_function< T >::value, bool = FXD::STD::is_reference< T >::value || FXD::STD::is_scalar< T >::value >
			struct _is_nothrow_destructible;

			template < typename T >
			struct _is_nothrow_destructible< T, false, false > : public FXD::STD::Internal::_is_nothrow_destructible_zero< typename FXD::STD::remove_all_extents< T >::type >
			{};

			template < typename T >
			struct _is_nothrow_destructible< T, true, false > : public FXD::STD::false_type
			{};

			template < typename T >
			struct _is_nothrow_destructible< T, false, true > : public FXD::STD::true_type
			{};
		} //namespace Internal

		template < typename T >
		struct is_nothrow_destructible : public FXD::STD::Internal::_is_nothrow_destructible< T >
		{};

#		endif

		template <>
		struct is_nothrow_destructible< void > : public FXD::STD::false_type
		{};

		template <>
		struct is_nothrow_destructible< void const > : public FXD::STD::false_type
		{};

		template <>
		struct is_nothrow_destructible< void volatile > : public FXD::STD::false_type
		{};

		template <>
		struct is_nothrow_destructible< void const volatile > : public FXD::STD::false_type
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_nothrow_destructible_v = FXD::STD::is_nothrow_destructible< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISNOTHROWDESTRUCTIBLE_H