#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_DECAY_H
#define FXDENGINE_CORE_INTERNAL_DECAY_H

#include "FXDEngine/Core/Internal/AddPointer.h"
#include "FXDEngine/Core/Internal/Conditional.h"
#include "FXDEngine/Core/Internal/IsArray.h"
#include "FXDEngine/Core/Internal/IsFunction.h"
#include "FXDEngine/Core/Internal/RemoveExtent.h"
#include "FXDEngine/Core/Internal/RemoveCV.h"
#include "FXDEngine/Core/Internal/RemoveReference.h"

namespace FXD
{
	namespace STD
	{
		// decay
#		define FXD_SUPPORTS_DECAY 1

		template < typename T >
		struct decay
		{
			using U = typename FXD::STD::remove_reference< T >::type;
			using type = typename FXD::STD::conditional< FXD::STD::is_array< U >::value, typename FXD::STD::remove_extent< U >::type*, typename FXD::STD::conditional< FXD::STD::is_function< U >::value, typename FXD::STD::add_pointer< U >::type, typename FXD::STD::remove_cv< U >::type >::type >::type;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using decay_t = typename FXD::STD::decay< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_DECAY_H