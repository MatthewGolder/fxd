#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_COPY_H
#define FXDENGINE_CORE_INTERNAL_COPY_H

#include "FXDEngine/Core/Internal/CopyMoveHelper.h"
#include "FXDEngine/Container/Internal/UnwrapIterator.h"

namespace FXD
{
	namespace STD
	{
		// copy
#		define FXD_SUPPORTS_COPY 1

		template < typename InputItr, typename OutputItr >
		inline OutputItr copy(InputItr first, InputItr last, OutputItr dst)
		{
			const bool isMove = FXD::STD::is_move_iterator< InputItr >::value;

			return FXD::STD::Internal::_move_copy_unwrapper< isMove >(FXD::STD::unwrap_iterator(first), FXD::STD::unwrap_iterator(last), dst);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_COPY_H