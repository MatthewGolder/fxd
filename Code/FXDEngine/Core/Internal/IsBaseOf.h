#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISBASEOF_H
#define FXDENGINE_CORE_INTERNAL_ISBASEOF_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsClass.h"

namespace FXD
{
	namespace STD
	{
		// is_base_of
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC) || defined(COMPILER_CLANG))
#		define FXD_SUPPORTS_IS_BASE_OF 1

		template < typename B, typename D >
		struct is_base_of : FXD::STD::integral_constant< bool, __is_base_of(B, D) >
		{};

#		else	
#		define FXD_SUPPORTS_IS_BASE_OF 1

		namespace Internal
		{
			template < typename B >
			FXD::STD::true_type _is_base_ofconvertible(const volatile B*);
			template < typename >
			FXD::STD::false_type _is_base_ofconvertible(const volatile void*);

			template < typename, typename >
			auto _is_base_of(...)->FXD::STD::true_type;
			template < typename B, typename D >
			auto _is_base_of(FXD::S32) -> decltype(FXD::STD::Internal::_is_base_ofconvertible< B >(static_cast<D*>(nullptr)));
		} //namespace Internal

		template < typename B, typename D >
		struct is_base_of : FXD::STD::integral_constant< bool, FXD::STD::is_class< B >::value && FXD::STD::is_class< D >::value && decltype(FXD::STD::Internal::_is_base_of< B, D >(0))::value >
		{};

#		endif

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename B, typename D >
		CONSTEXPR bool is_base_of_v = FXD::STD::is_base_of< B, D >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISBASEOF_H