// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_BIND_H
#define FXDENGINE_CORE_INTERNAL_BIND_H

#include "FXDEngine/Core/Internal/Conditional.h"
#include "FXDEngine/Core/Internal/Decay.h"
#include "FXDEngine/Core/Internal/InvokeHelper.h"
#include "FXDEngine/Core/Internal/IntegerSequence.h"
#include "FXDEngine/Core/Internal/IsBindExpression.h"
#include "FXDEngine/Core/Internal/IsSame.h"
#include "FXDEngine/Core/Internal/IsPlaceholder.h"
#include "FXDEngine/Core/Internal/RemoveCV.h"
#include "FXDEngine/Core/Internal/RemoveCV.h"
#include "FXDEngine/Core/Internal/WeakTypes.h"
#include "FXDEngine/Container/Tuple.h"

namespace FXD
{
	// bind
#	define FXD_SUPPORTS_BIND 1

	namespace Internal
	{
		template <
			typename CvType,
			bool = STD::Internal::_is_specialization_v< FXD::STD::remove_cv_t< CvType >, FXD::STD::reference_wrapper >,
			bool = FXD::STD::is_bind_expression< CvType >::value,
			FXD::S32 = FXD::STD::is_placeholder< CvType >::value >
		struct _Select_fixer;

		template < typename CvType >
		struct _Select_fixer< CvType, true, false, 0 >
		{
			template < typename Untuple >
			static auto _fix(CvType& type, Untuple&&)->typename CvType::type&
			{
				return (type.get());
			}
		};

		template < typename CvType >
		struct _Select_fixer< CvType, false, true, 0 >
		{
			template < typename Untuple, size_t... IDX >
			static auto _apply(CvType& type, Untuple&& ut, FXD::STD::index_sequence< IDX... >)-> decltype(type(FXD::get< IDX >(std::move(ut))...))
			{
				return (type(FXD::get< IDX >(std::move(ut))...));
			}

			template < typename Untuple >
			static auto _fix(CvType& type, Untuple&& ut)-> decltype(_apply(type, std::move(ut), FXD::STD::make_index_sequence< FXD::tuple_size< Untuple >::value >()))
			{
				return (_apply(type, std::move(ut), FXD::STD::make_index_sequence< FXD::tuple_size< Untuple >::value >()));
			}
		};

		template < typename CvType >
		struct _Select_fixer< CvType, false, false, 0 >
		{
			template < typename Untuple >
			static CvType& _fix(CvType& type, Untuple&&)
			{
				return (type);
			}
		};

		template < typename CvType, size_t IDX >
		struct _Select_fixer< CvType, false, false, IDX >
		{
			CTC_ASSERT((IDX > 0), "Core: Invalid is_placeholder value");

			template < typename Untuple >
			static auto _fix(CvType&, Untuple&& ut)->decltype(FXD::get< IDX - 1 >(std::move(ut)))
			{
				return (FXD::get< IDX - 1>(std::move(ut)));
			}
		};

		template < typename CvType, typename Untuple >
		inline auto _fix_arg(CvType& type, Untuple&& ut)->decltype(Internal::_Select_fixer< CvType >::_fix(type, std::move(ut)))
		{
			return (Internal::_Select_fixer< CvType >::_fix(type, std::move(ut)));
		}
	} //namespace Internal


	template < typename R, size_t... Idx, typename CvFD, typename CvType, class Untuple >
	inline auto _Call_binder(Internal::_invoker_ret< R >, FXD::STD::index_sequence< Idx... >, CvFD& _Obj, CvType& type, Untuple&& ut)->decltype(Internal::_invoker_ret< R >::_call(_Obj, Internal::_fix_arg(FXD::get< Idx >(type), std::move(ut))...))
	{
		return (Internal::_invoker_ret< R >::_call(_Obj, Internal::_fix_arg(FXD::get< Idx >(type), std::move(ut))...));
	}

	// CLASS TEMPLATE _Binder
	template < typename R >
	struct _Forced_result_type
	{};

	template < typename R, typename Fx >
	struct _Binder_result_type
	{
		using decayed_type = FXD::STD::decay_t< Fx >;
		using weak_types = typename FXD::STD::Internal::_weak_types< decayed_type >::type;
		using type = FXD::STD::conditional_t< FXD::STD::is_same< R, FXD::Internal::_BindUnforced >::value, FXD::STD::Internal::_weak_result_type< weak_types >, _Forced_result_type< R > >;
	};

	template < typename R, typename Fx, typename... Types>
	class _Binder : public _Binder_result_type< R, Fx >::type
	{
	private:
		using sequence_type = FXD::STD::index_sequence_for< Types... >;
		using first_type = FXD::STD::decay_t< Fx >;
		using second_type = Container::Tuple< FXD::STD::decay_t< Types >...>;

		Core::Pair< first_type, second_type > m_pair;

	public:
		EXPLICIT _Binder(Fx&& _Func, Types&&... _Args) 
			: m_pair(FXD::STD::Internal::_One_then_variadic_args_t(), std::forward< Fx >(_Func), std::forward< Types >(_Args)...)
		{}

		/*
#define _BINDER_OPERATOR(CONST_OPT)																																														\
	template <class... _Unbound>																																															\
	auto operator()(_Unbound&&... _Unbargs) CONST_OPT -> decltype(_Call_binder(_Invoker_ret< R >(), sequence_type(), _Mypair._Get_first(), _Mypair._Get_second(), _STD forward_as_tuple(_STD forward<_Unbound>(_Unbargs)...)))	\
	{																																																					\
		return (_Call_binder(_Invoker_ret< R >(), sequence_type(),_Mypair._Get_first(), _Mypair._Get_second(),_STD forward_as_tuple(_STD forward<_Unbound>(_Unbargs)...)));														\
	}

		_CLASS_DEFINE_CONST(_BINDER_OPERATOR)
#undef _BINDER_OPERATOR
		*/
	};

	/*
	template < typename _Fx, typename... Args >
	inline _Binder<_Unforced, _Fx, Args...> bind(_Fx&& _Func, Args&&... _Args)
	{
		return (_Binder<_Unforced, _Fx, Args...>(std::forward<_Fx>(_Func), std::forward< Args >(_Args)...));
	}

	// FUNCTION TEMPLATE bind (explicit return type)
	template < typename _Ret, typename _Fx, typename... Args >
	_Binder<_Ret, _Fx, Args...> bind(_Fx&& _Func, Args&&... _Args)
	{
		return (_Binder<_Ret, _Fx, Args...>(std::forward<_Fx>(_Func), std::forward< Args >(_Args)...));
	}
	*/
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_BIND_H