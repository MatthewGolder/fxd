#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MAKEHEAPIF_H
#define FXDENGINE_CORE_INTERNAL_MAKEHEAPIF_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/ShiftHeap.h"

namespace FXD
{
	namespace STD
	{
		// make_heap_if
#		define FXD_SUPPORTS_MAKE_HEAP_IF 1

		template < typename RandomAccessItr, typename Predicate >
		inline void make_heap_if(RandomAccessItr first, RandomAccessItr last, Predicate pred)
		{
			using difference_type = typename FXD::STD::iterator_traits< RandomAccessItr >::difference_type;

			difference_type nLen = (last - first);

			if (nLen > 1)
			{
				for (difference_type nStart = (nLen - 2) / 2; nStart >= 0; --nStart)
				{
					FXD::STD::Internal::_shift_heap_down(first, last, pred, nLen, first + nStart);
				}
			}
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MAKEHEAPIF_H