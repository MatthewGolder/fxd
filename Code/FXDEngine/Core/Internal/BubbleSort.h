// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_BUBBLESORT_H
#define FXDENGINE_CORE_INTERNAL_BUBBLESORT_H

#include "FXDEngine/Container/Iterator.h"
#include "FXDEngine/Core/Internal/Less.h"

namespace FXD
{
	namespace STD
	{
		// bubble_sort
#		define FXD_SUPPORTS_BUBBLE_SORT 1

		namespace Internal
		{
			template < typename ForwardItr, typename Predicate >
			inline void _bubble_sort(ForwardItr first, ForwardItr last, Predicate pred, FXD::STD::forward_iterator_tag)
			{
				ForwardItr iCurrent, iNext;
				while (first != last)
				{
					iNext = iCurrent = first;
					for (++iNext; iNext != last; iCurrent = iNext, ++iNext)
					{
						if (pred((*iNext), (*iCurrent)))
						{
							FXD::STD::iter_swap(iCurrent, iNext);
						}
					}
					last = iCurrent;
				}
			}

			template < typename BidirItr, typename Predicate >
			inline void _bubble_sort(BidirItr first, BidirItr last, Predicate pred, FXD::STD::bidirectional_iterator_tag)
			{
				if (first != last)
				{
					BidirItr iCurrent, iNext, iLastModified;
					last--;
					while (first != last)
					{
						iLastModified = iNext = iCurrent = first;

						for (++iNext; iCurrent != last; iCurrent = iNext, ++iNext)
						{
							if (pred((*iNext), (*iCurrent)))
							{
								iLastModified = iCurrent;
								FXD::STD::iter_swap(iCurrent, iNext);
							}
						}
						last = iLastModified;
					}
				}
			}
		} // namespace Internal

		template < typename ForwardItr, typename Predicate >
		inline void bubble_sort(ForwardItr first, ForwardItr last, Predicate pred)
		{
			using IC = typename FXD::STD::iterator_traits< ForwardItr >::iterator_category;

			FXD::STD::Internal::_bubble_sort(first, last, pred, IC());
		}

		template < typename ForwardItr >
		inline void bubble_sort(ForwardItr first, ForwardItr last)
		{
			using IC = typename FXD::STD::iterator_traits< ForwardItr >::iterator_category;

			FXD::STD::Internal::_bubble_sort(first, last, FXD::STD::less<>(), IC());
		}

	} //namespace STDA
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_BUBBLESORT_H