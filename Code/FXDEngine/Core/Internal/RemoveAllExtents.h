#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REMOVEALLEXTENTS_H
#define FXDENGINE_CORE_INTERNAL_REMOVEALLEXTENTS_H

#include <cstddef>

namespace FXD
{
	namespace STD
	{
		// remove_all_extents
#		define FXD_SUPPORTS_REMOVE_ALL_EXTENTS 1

		template < typename T >
		struct remove_all_extents
		{
			using type = T;
		};

		template < typename T, std::size_t SIZE >
		struct remove_all_extents< T[SIZE] >
		{
			using type = typename FXD::STD::remove_all_extents< T >::type;
		};

		template < typename T >
		struct remove_all_extents< T[] >
		{
			using type = typename FXD::STD::remove_all_extents< T >::type;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using remove_all_extents_t = typename FXD::STD::remove_all_extents< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REMOVEALLEXTENTS_H