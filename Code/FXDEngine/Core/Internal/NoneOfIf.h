// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_NONEOFIF_H
#define FXDENGINE_CORE_INTERNAL_NONEOFIF_H

#include "FXDEngine/Core/Internal/Compiler.h"

namespace FXD
{
	namespace STD
	{
		// none_of_if
#		define FXD_SUPPORTS_NONE_OF_IF 1

		template < typename InputItr, typename Predicate >
		inline bool none_of_if(InputItr first, InputItr last, Predicate pred)
		{
			for (; first != last; ++first)
			{
				if (pred(*first))
				{
					return false;
				}
			}
			return true;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_NONEOFIF_H