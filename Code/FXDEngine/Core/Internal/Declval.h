#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_DECLVAL_H
#define FXDENGINE_CORE_INTERNAL_DECLVAL_H

#include "FXDEngine/Core/Internal/AddRValueReference.h"
#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// declval
#		define FXD_SUPPORTS_DECLVAL 1

		template < typename T >
		typename FXD::STD::add_rvalue_reference< T >::type declval(void) NOEXCEPT;

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_DECLVAL_H