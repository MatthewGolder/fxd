#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_PARTIALSORT_H
#define FXDENGINE_CORE_INTERNAL_PARTIALSORT_H

#include "FXDEngine/Core/Internal/Less.h"
#include "FXDEngine/Core/Internal/PartialSortIf.h"

namespace FXD
{
	namespace STD
	{
		// partial_sort
#		define FXD_SUPPORTS_PARTITION_SORT 1

		template < typename RandomAccessItr, typename Predicate >
		inline void partial_sort(RandomAccessItr first, RandomAccessItr mid, RandomAccessItr last, Predicate pred)
		{
			FXD::STD::partial_sort_if(first, mid, last, pred);
		}

		template < typename RandomAccessItr >
		inline void partial_sort(RandomAccessItr first, RandomAccessItr mid, RandomAccessItr last)
		{
			FXD::STD::partial_sort(first, mid, last, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_PARTIALSORT_H