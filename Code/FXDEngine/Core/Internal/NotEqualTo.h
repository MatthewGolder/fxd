#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_NOTEQUALTO_H
#define FXDENGINE_CORE_INTERNAL_NOTEQUALTO_H

#include "FXDEngine/Core/Internal/Config.h"
#include <utility>

namespace FXD
{
	namespace STD
	{
		// not_equal_to
#		define FXD_SUPPORTS_NOT_EQUAL_TO 1

		template < typename T = void >
		struct not_equal_to
		{
			using first_argument_type = T;
			using second_argument_type = T;
			using result_type = T;

			CONSTEXPR14 bool operator()(const T& lhs, const T& rhs) const
			{
				return (lhs != rhs);
			}
		};

		template <>
		struct not_equal_to< void >
		{
			template < typename LHS, typename RHS >
			CONSTEXPR14 auto operator()(LHS&& lhs, RHS&& rhs) const->decltype(std::forward< LHS >(lhs) != std::forward< RHS >(rhs))
			{
				return std::forward< LHS >(lhs) != std::forward< RHS >(rhs);
			}
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_NOTEQUALTO_H