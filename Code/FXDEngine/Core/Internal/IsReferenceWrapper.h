// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISREFERENCEWRAPPER_H
#define FXDENGINE_CORE_INTERNAL_ISREFERENCEWRAPPER_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/EnableIf.h"
#include "FXDEngine/Core/Internal/ReferenceWrapper.h"
#include "FXDEngine/Core/Internal/RemoveCV.h"
#include "FXDEngine/Core/Internal/RemoveReference.h"

namespace FXD
{
	namespace STD
	{
		// is_reference_wrapper
#		define FXD_SUPPORTS_IS_REFERENCE_WRAPPER 1

		namespace Internal
		{
			template < typename T >
			struct _is_reference_wrapper : public FXD::STD::false_type
			{};

			template < typename T >
			struct _is_reference_wrapper< FXD::STD::reference_wrapper< T > > : public FXD::STD::true_type
			{};
		} //namespace Internal

		template < typename T >
		struct is_reference_wrapper : public FXD::STD::Internal::_is_reference_wrapper< typename FXD::STD::remove_cv< T >::type >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		using is_reference_wrapper_v = typename FXD::STD::is_reference_wrapper< T >::value;

#		endif
	} //namespace STD

	namespace Internal
	{
		template < typename R, typename C, typename T, typename... Args >
		auto _invoke(R(C::*func)(Args...), T&& obj, Args&&... args) -> typename FXD::STD::enable_if< FXD::STD::is_reference_wrapper< typename FXD::STD::remove_reference< T >::type >::value, decltype((obj.get().*func)(std::forward< Args >(args)...)) >::type
		{
			return (obj.get().*func)(std::forward< Args >(args)...);
		}

		template < typename M, typename C, typename T >
		auto _invoke(M(C::*member), T&& obj) -> typename FXD::STD::enable_if< FXD::STD::is_reference_wrapper< typename FXD::STD::remove_reference< T >::type >::value, decltype(obj.get().*member) >::type
		{
			return obj.get().*member;
		}
	} //namespace Internal

} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISREFERENCEWRAPPER_H