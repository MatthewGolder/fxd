#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISNOTHROWCONSTRUCTIBLE_H
#define FXDENGINE_CORE_INTERNAL_ISNOTHROWCONSTRUCTIBLE_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Declval.h"
#include "FXDEngine/Core/Internal/IsConstructible.h"
#include <utility>
#include <cstddef>

namespace FXD
{
	namespace STD
	{
		// is_nothrow_constructible
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC) || defined(COMPILER_CLANG))
#		define FXD_SUPPORTS_IS_NOTHROW_CONSTRUCTIBLE 1

		template < typename T, typename... _Args >
		struct is_nothrow_constructible : FXD::STD::integral_constant< bool, __is_nothrow_constructible(T, _Args...) >
		{};

#		else
#		define FXD_SUPPORTS_IS_NOTHROW_CONSTRUCTIBLE 1

		namespace Internal
		{
			template < typename T, typename... Args >
			struct _is_nothrow_constructible_noexcept
			{
				static const bool value = NOEXCEPT(T(FXD::STD::declval< Args >()...));
			};

			template < bool, typename T, typename... Args >
			struct _is_nothrow_constructible;

			template < typename T, typename... Args >
			struct _is_nothrow_constructible< true, T, Args... > : public FXD::STD::integral_constant< bool, FXD::STD::Internal::_is_nothrow_constructible_noexcept< T, Args... >::value >
			{};

			template < typename T, typename Arg >
			struct _is_nothrow_constructible< true, T, Arg > : public FXD::STD::integral_constant< bool, NOEXCEPT(T(FXD::STD::declval< Arg >())) >
			{};

			template < typename T>
			struct _is_nothrow_constructible< true, T> : public FXD::STD::integral_constant< bool, NOEXCEPT(T()) >
			{};

			template < typename T, typename... Args >
			struct _is_nothrow_constructible< false, T, Args... > : public FXD::STD::false_type
			{};
		} //namespace Internal

		template < typename T, typename... Args >
		struct is_nothrow_constructible : public FXD::STD::Internal::_is_nothrow_constructible< FXD::STD::is_constructible< T, Args... >::value, T, Args... >
		{};

		template < typename T, std::size_t SIZE >
		struct is_nothrow_constructible< T[SIZE] > : public FXD::STD::Internal::_is_nothrow_constructible< FXD::STD::is_constructible< T >::value, T >
		{};

#		endif

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T, typename... Args >
		CONSTEXPR bool is_nothrow_constructible_v = FXD::STD::is_nothrow_constructible< T, Args... >::value;

#		endif

	} //namespace FXDSTD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISNOTHROWCONSTRUCTIBLE_H