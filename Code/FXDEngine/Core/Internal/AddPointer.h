#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ADDPOINTER_H
#define FXDENGINE_CORE_INTERNAL_ADDPOINTER_H

#include "FXDEngine/Core/Internal/RemoveReference.h"

namespace FXD
{
	namespace STD
	{
		// add_pointer
#		define FXD_SUPPORTS_ADD_POINTER 1

		template < typename T >
		struct add_pointer
		{
			using type = typename FXD::STD::remove_reference< T >::type*;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using add_pointer_t = typename FXD::STD::add_pointer< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ADDPOINTER_H