// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_PREVPERMUTATIONIF_H
#define FXDENGINE_CORE_INTERNAL_PREVPERMUTATIONIF_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/Compiler.h"
#include "FXDEngine/Core/Internal/Swap.h"
#include "FXDEngine/Core/Internal/Reverse.h"

namespace FXD
{
	namespace STD
	{
		// prev_permutation_if
#		define FXD_SUPPORTS_PREV_PERMUTATION_IF 1

		template < typename BidirItr, typename Predicate >
		inline bool prev_permutation_if(BidirItr first, BidirItr last, Predicate pred)
		{
			BidirItr itr1 = last;
			if (first == last || first == --itr1)
			{
				return false;
			}
			while (true)
			{
				BidirItr itr1Copy = itr1;
				if (pred(*itr1Copy, *--itr1))
				{
					BidirItr itr2 = last;
					while (!pred(*--itr2, *itr1))
					{
					}

					FXD::STD::iter_swap(itr1, itr2);
					FXD::STD::reverse(itr1Copy, last);
					return true;
				}
				if (itr1 == first)
				{
					FXD::STD::reverse(first, last);
					return false;
				}
			}
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_PREVPERMUTATIONIF_H