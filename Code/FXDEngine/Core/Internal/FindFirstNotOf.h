#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_FINDFIRSTNOTOF_H
#define FXDENGINE_CORE_INTERNAL_FINDFIRSTNOTOF_H

#include "FXDEngine/Core/Internal/FindFirstNotOfIf.h"
#include "FXDEngine/Core/Internal/EqualTo.h"

namespace FXD
{
	namespace STD
	{
		// find_first_of
#		define FXD_SUPPORTS_FIND_FIRST_NOT_OF 1

		template < typename InputItr, typename ForwardItr, typename Predicate >
		inline InputItr find_first_not_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)
		{
			return FXD::STD::find_first_not_of_if(first1, last1, first2, last2, pred);
		}

		template < typename InputItr, typename ForwardItr >
		inline InputItr find_first_not_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)
		{
			return FXD::STD::find_first_not_of(first1, last1, first2, last2, FXD::STD::equal_to<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_FINDFIRSTNOTOF_H