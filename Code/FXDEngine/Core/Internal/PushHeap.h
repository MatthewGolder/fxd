#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_PUSHHEAP_H
#define FXDENGINE_CORE_INTERNAL_PUSHHEAP_H

#include "FXDEngine/Core/Internal/Less.h"
#include "FXDEngine/Core/Internal/PushHeapIf.h"

namespace FXD
{
	namespace STD
	{
		// push_heap
#		define FXD_SUPPORTS_PUSH_HEAP 1

		template < typename RandomAccessItr, typename Predicate >
		inline void push_heap(RandomAccessItr first, RandomAccessItr last, Predicate pred)
		{
			FXD::STD::push_heap_if(first, last, pred);
		}

		template < typename RandomAccessItr >
		inline void push_heap(RandomAccessItr first, RandomAccessItr last)
		{
			FXD::STD::push_heap(first, last, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_PUSHHEAP_H