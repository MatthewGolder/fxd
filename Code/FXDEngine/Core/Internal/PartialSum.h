// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_PARTIALSUM_H
#define FXDENGINE_CORE_INTERNAL_PARTIALSUM_H

#include "FXDEngine/Core/Internal/PartialSumIf.h"
#include "FXDEngine/Core/Internal/Plus.h"

namespace FXD
{
	namespace STD
	{
		// partial_sum
#		define FXD_SUPPORTS_PARTITION_SUM 1

		template < typename InputItr, typename OutputItr, typename Predicate >
		inline OutputItr partial_sum(InputItr first, InputItr last, OutputItr dst, Predicate pred)
		{
			return FXD::STD::partial_sum_if(first, last, dst, pred);
		}

		template < typename InputItr, typename OutputItr >
		inline OutputItr partial_sum(InputItr first, InputItr last, OutputItr dst)
		{
			return FXD::STD::partial_sum(first, last, dst, FXD::STD::plus<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_PARTIALSUM_H