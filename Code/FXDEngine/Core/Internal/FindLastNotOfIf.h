#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_FINDLASTNOTOFIF_H
#define FXDENGINE_CORE_INTERNAL_FINDLASTNOTOFIF_H

#include "FXDEngine/Core/Internal/FindIf.h"
#include "FXDEngine/Container/Internal/IteratorTraits.h"

namespace FXD
{
	namespace STD
	{
		// find_last_not_of_if
#		define FXD_SUPPORTS_FIND_LAST_NOT_OF_IF 1

		template < typename InputItr, typename ForwardItr, typename Predicate >
		inline InputItr find_last_not_of_if(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)
		{
			using value_type = typename FXD::STD::iterator_traits< InputItr >::value_type;

			if ((first1 != last1) && (first2 != last2))
			{
				InputItr itr(last1);

				while ((--itr != first1) && (FXD::STD::find_if(first2, last2, [&](const value_type& val) { return pred(val, *itr); }) != last2))
				{
					; // Do nothing
				}
				if ((itr != first1) || (FXD::STD::find_if(first2, last2, [&](const value_type& val) { return pred(val, *itr); })) != last2)
				{
					return itr;
				}
			}
			return last1;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_FINDLASTNOTOFIF_H