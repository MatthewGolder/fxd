#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_UPPERBOUNDIF_H
#define FXDENGINE_CORE_INTERNAL_UPPERBOUNDIF_H

#include "FXDEngine/Container/Internal/Advance.h"
#include "FXDEngine/Container/Internal/Distance.h"
#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// upper_bound_if
#		define FXD_SUPPORTS_UPPER_BOUND_IF 1

		template < typename ForwardItr, typename T, typename Predicate >
		inline ForwardItr upper_bound_if(ForwardItr first, ForwardItr last, const T& val, Predicate pred)
		{
			using DifferenceType = typename FXD::STD::iterator_traits< ForwardItr >::difference_type;

			DifferenceType dist = FXD::STD::distance(first, last);
			while (dist > 0)
			{
				ForwardItr itr = first;
				DifferenceType dist2 = dist >> 1;

				FXD::STD::advance(itr, dist2);

				if (pred(val, *itr))
				{
					dist = dist2;
				}
				else
				{
					first = ++itr;
					dist -= dist2 + 1;
				}
			}
			return first;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_UPPERBOUNDIF_H