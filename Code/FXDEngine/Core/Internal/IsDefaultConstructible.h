#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISDEFAULTCONSTRUCTIBLE_H
#define FXDENGINE_CORE_INTERNAL_ISDEFAULTCONSTRUCTIBLE_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/IsConstructible.h"

namespace FXD
{
	namespace STD
	{
		// is_default_constructible
#		define FXD_SUPPORTS_IS_DEFAULT_CONSTRUCTIBLE 1

		template < typename T >
		struct is_default_constructible : public FXD::STD::is_constructible< T >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_default_constructible_v = FXD::STD::is_default_constructible< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISDEFAULTCONSTRUCTIBLE_H