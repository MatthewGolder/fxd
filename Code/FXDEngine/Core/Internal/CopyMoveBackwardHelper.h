#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_COPYMOVEBACKWARDHELPER_H
#define FXDENGINE_CORE_INTERNAL_COPYMOVEBACKWARDHELPER_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Container/Internal/MoveIterator.h"
#include "FXDEngine/Memory/Memory.h"

namespace FXD
{
	namespace STD
	{
		namespace Internal
		{
			// Implementation moving copying both trivial and non-trivial data via a lesser iterator than random-access.
			template < typename /*InputItrCategory*/, bool /*isMove*/, bool /*isMemmove*/ >
			struct _move_copy_backward
			{
				template < typename InputItr, typename OutputItr >
				static OutputItr _impl(InputItr first, InputItr last, OutputItr dst)
				{
					while (first != last)
					{
						*--dst = *--last;
					}
					return dst; // dst now points to the beginning of the destination sequence instead of the end.
				}
			};

			// Specialization for moving non-trivial data via a lesser iterator than random-access.
			template < typename InputItrCategory >
			struct _move_copy_backward< InputItrCategory, true, false >
			{
				template < typename InputItr, typename OutputItr >
				static OutputItr _impl(InputItr first, InputItr last, OutputItr dst)
				{
					while (first != last)
					{
						*--dst = std::move(*--last);
					}
					return dst; // dst now points to the beginning of the destination sequence instead of the end.
				}
			};

			// Specialization for moving non-trivial data via a random-access iterator. It's theoretically faster because the compiler can see the count when its a compile-time const.
			template <>
			struct _move_copy_backward< FXD::STD::random_access_iterator_tag, true, false >
			{
				template < typename InputItr, typename OutputItr >
				static OutputItr _impl(InputItr first, InputItr last, OutputItr dst)
				{
					using difference_type = typename FXD::STD::iterator_traits< InputItr >::difference_type;
					for (difference_type n1 = (last - first); n1 > 0; --n1)
					{
						*--dst = std::move(*--last);
					}
					return dst; // dst now points to the beginning of the destination sequence instead of the end.
				}
			};

			// Specialization for copying non-trivial data via a random-access iterator. It's theoretically faster because the compiler can see the count when its a compile-time const.
			// This specialization converts the random access InputItr last-first to an integral type. There's simple way for us to take advantage of a random access output iterator,
			// as the range is specified by the input instead of the output, and distance(first, last) for a non-random-access iterator is potentially slow.
			template <>
			struct _move_copy_backward< FXD::STD::random_access_iterator_tag, false, false >
			{
				template < typename InputItr, typename OutputItr >
				static OutputItr _impl(InputItr first, InputItr last, OutputItr dst)
				{
					using difference_type = typename FXD::STD::iterator_traits< InputItr >::difference_type;
					for (difference_type n1 = (last - first); n1 > 0; --n1)
					{
						*--dst = *--last;
					}
					return dst; // dst now points to the beginning of the destination sequence instead of the end.
				}
			};

			// Specialization for when we can use memmove/memcpy. See the notes above for what conditions allow this.
			template < bool IsMove >
			struct _move_copy_backward< FXD::STD::random_access_iterator_tag, IsMove, true >
			{
				template < typename T >
				static T* _impl(const T* first, const T* last, T* resultEnd)
				{
					return (T*)Memory::MemMove(resultEnd - (last - first), first, (size_t)((FXD::PtrSizedInt)last - (FXD::PtrSizedInt)first));
				}
			};

			template < bool IsMove, typename InputItr, typename OutputItr >
			inline OutputItr _movecopy_backward_chooser(InputItr first, InputItr last, OutputItr dst)
			{
				using input_iterator_category = typename FXD::STD::iterator_traits< InputItr >::iterator_category;
				using value_type_input = typename FXD::STD::iterator_traits< InputItr >::value_type;
				using value_type_output = typename FXD::STD::iterator_traits< OutputItr >::value_type;

				const bool bCanBeMemmoved =
					FXD::STD::is_trivially_copyable<value_type_output>::value &&
					FXD::STD::is_same<value_type_input, value_type_output>::value &&
					FXD::STD::is_pointer< InputItr >::value &&
					FXD::STD::is_pointer< OutputItr >::value;

				return FXD::STD::Internal::_move_copy_backward< input_iterator_category, IsMove, bCanBeMemmoved >::_impl(first, last, dst); // Need to chose based on the input iterator tag and not the output iterator tag, because containers accept input ranges of iterator types different than self.
			}

			// A second layer of FXD::STD::unwrap_iterator calls because the original iterator might be something like FXD::STD::move_iterator< FXD::STD::generic_iterator < FXD::S32* > > (i.e. doubly-wrapped).
			template < bool IsMove, typename InputItr, typename OutputItr >
			inline OutputItr _move_copy_backward_unwrapper(InputItr first, InputItr last, OutputItr dst)
			{
				return OutputItr(FXD::STD::Internal::_movecopy_backward_chooser< IsMove >(FXD::STD::unwrap_iterator(first), FXD::STD::unwrap_iterator(last), FXD::STD::unwrap_iterator(dst)));
			}

		} //namespace Internal
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_COPYMOVEBACKWARDHELPER_H