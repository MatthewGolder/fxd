// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ARGTYPES_H
#define FXDENGINE_CORE_INTERNAL_ARGTYPES_H

namespace FXD
{
	namespace STD
	{
		namespace Internal
		{
			template < typename... Args >
			struct _Arg_types
			{};

			template < typename T >
			struct _Arg_types< T >
			{};

			template < typename T1, typename T2 >
			struct _Arg_types< T1, T2 >
			{};
		} //namespace Internal
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ARGTYPES_H