#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_SORTHEAP_H
#define FXDENGINE_CORE_INTERNAL_SORTHEAP_H

#include "FXDEngine/Core/Internal/SortHeapIf.h"
#include "FXDEngine/Core/Internal/Less.h"

namespace FXD
{
	namespace STD
	{
		// sort_heap
#		define FXD_SUPPORTS_SORT_HEAP 1

		template < typename RandomAccessItr, typename Predicate >
		inline void sort_heap(RandomAccessItr first, RandomAccessItr last, Predicate pred)
		{
			FXD::STD::sort_heap_if(first, last, pred);
		}

		template < typename RandomAccessItr >
		inline void sort_heap(RandomAccessItr first, RandomAccessItr last)
		{
			FXD::STD::sort_heap(first, last, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_SORTHEAP_H