// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_UNARYNEGATE_H
#define FXDENGINE_CORE_INTERNAL_UNARYNEGATE_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/UnaryFunction.h"

namespace FXD
{
	namespace STD
	{
		// unary_negate
#		define FXD_SUPPORTS_UNARY_NEGATE 1

		template < typename Predicate >
		class unary_negate
		{
		public:
			using argument_type = typename Predicate::argument_type;
			using result_type = bool;

			EXPLICIT unary_negate(const Predicate& pred)
				: m_pred(pred)
			{}

			bool operator()(const argument_type& val) const
			{
				return (!m_pred(val));
			}

		private:
			Predicate m_pred;
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_UNARYNEGATE_H