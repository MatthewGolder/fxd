#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_PARTIALSORTCOPYIF_H
#define FXDENGINE_CORE_INTERNAL_PARTIALSORTCOPYIF_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/MakeHeap.h"
#include "FXDEngine/Core/Internal/SortHeap.h"

namespace FXD
{
	namespace STD
	{
		// partial_sort_copy_if
#		define FXD_SUPPORTS_PARTITION_SORT_COPY_IF 1

		template < typename InputItr, typename RandomAccessItr, typename Predicate >
		inline RandomAccessItr partial_sort_copy_if(InputItr first, InputItr last, RandomAccessItr dstFirst, RandomAccessItr dstLast, Predicate pred)
		{
			using difference_type = typename FXD::STD::iterator_traits< RandomAccessItr >::difference_type;

			RandomAccessItr result = dstFirst;
			if (result != dstLast)
			{
				for (; first != last && result != dstLast; (void) ++first, ++result)
				{
					*result = *first;
				}

				FXD::STD::make_heap(dstFirst, result, pred);

				difference_type nLen = (result - dstFirst);

				for (; first != last; ++first)
				{
					if (pred(*first, *dstFirst))
					{
						*dstFirst = *first;
						FXD::STD::Internal::_shift_heap_down(dstFirst, result, pred, nLen, dstFirst);
					}
				}
				FXD::STD::sort_heap(dstFirst, result, pred);
			}

			return result;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_PARTIALSORTIF_H