#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISINTERGRAL_H
#define FXDENGINE_CORE_INTERNAL_ISINTERGRAL_H

#include "FXDEngine/Core/Types.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/RemoveCV.h"

namespace FXD
{
	namespace STD
	{
		// is_integral
#		define FXD_SUPPORTS_INTERGRAL 1

		namespace Internal
		{
			template < typename T >
			struct _is_integral : public FXD::STD::false_type
			{};
			template <>
			struct _is_integral< FXD::U8 > : public FXD::STD::true_type
			{};
			template <>
			struct _is_integral< FXD::U16 > : public FXD::STD::true_type
			{};
			template <>
			struct _is_integral< FXD::U32 > : public FXD::STD::true_type
			{};
			template <>
			struct _is_integral< FXD::U64 > : public FXD::STD::true_type
			{};
			template <>
			struct _is_integral< FXD::S8 > : public FXD::STD::true_type
			{};
			template <>
			struct _is_integral< FXD::S16 > : public FXD::STD::true_type
			{};
			template <>
			struct _is_integral< FXD::S32 > : public FXD::STD::true_type
			{};
			template <>
			struct _is_integral< FXD::S64 > : public FXD::STD::true_type
			{};
			template <>
			struct _is_integral< bool > : public FXD::STD::true_type
			{};
			template <>
			struct _is_integral< FXD::UTF8 > : public FXD::STD::true_type
			{};
			template <>
			struct _is_integral< FXD::UTF16 > : public FXD::STD::true_type
			{};
			template <>
			struct _is_integral< FXD::UTF32 > : public FXD::STD::true_type
			{};
		} //namespace Internal

		template < typename T >
		struct is_integral : public FXD::STD::Internal::_is_integral< typename FXD::STD::remove_cv< T >::type >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_integral_v = FXD::STD::is_integral< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISINTERGRAL_H