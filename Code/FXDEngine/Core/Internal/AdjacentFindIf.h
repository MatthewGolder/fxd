#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ADJACENTFINDIF_H
#define FXDENGINE_CORE_INTERNAL_ADJACENTFINDIF_H

#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// adjacent_find_if
#		define FXD_SUPPORTS_ADJACENT_FIND_IF 1

		template < typename ForwardItr, typename Predicate >
		inline ForwardItr adjacent_find_if(ForwardItr first, ForwardItr last, Predicate pred)
		{
			if (first != last)
			{
				ForwardItr itr = first;
				for (++itr; itr != last; ++itr)
				{
					if (pred(*first, *itr))
					{
						return first;
					}
					first = itr;
				}
			}
			return last;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ADJACENTFINDIF_H