#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISDESTRUCTIBLE_H
#define FXDENGINE_CORE_INTERNAL_ISDESTRUCTIBLE_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Declval.h"
#include "FXDEngine/Core/Internal/Identity.h"
#include "FXDEngine/Core/Internal/IsArrayOfUnknownBounds.h"
#include "FXDEngine/Core/Internal/IsFunction.h"
#include "FXDEngine/Core/Internal/IsVoid.h"

namespace FXD
{
	namespace STD
	{
		// is_destructible
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC))
#		define FXD_SUPPORTS_IS_DESTRUCTIBLE 1

		template < typename T >
		struct is_destructible : FXD::STD::integral_constant< bool, __is_destructible(T) >
		{};

#		else
#		define FXD_SUPPORTS_IS_DESTRUCTIBLE 1

		namespace Internal
		{
			template < typename U >
			struct _destructible_test_helper
			{};

			template < typename >
			FXD::STD::false_type _destructible_test_function(...);

			template < typename T, typename U = decltype(FXD::STD::declval< FXD::STD::Internal::_destructible_test_helper< T > >().~_destructible_test_helper< T >()) >
			FXD::STD::true_type _destructible_test_function(FXD::S32);

			template < typename T, bool = FXD::STD::is_array_of_unknown_bounds< T >::value || FXD::STD::is_void< T >::value || FXD::STD::is_function< T >::value >
			struct _is_destructible : public FXD::STD::identity< decltype(FXD::STD::Internal::_destructible_test_function< T >(0)) >::type
			{};

			template < typename T >
			struct _is_destructible< T, true > : public FXD::STD::false_type
			{};
		} //namespace Internal


		template < typename T >
		struct is_destructible : public FXD::STD::Internal::_is_destructible< T >
		{};

#		endif

		template <>
		struct is_destructible< void > : public FXD::STD::false_type
		{};

		template <>
		struct is_destructible< void const > : public FXD::STD::false_type
		{};

		template <>
		struct is_destructible< void volatile > : public FXD::STD::false_type
		{};

		template <>
		struct is_destructible< void const volatile > : public FXD::STD::false_type
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_destructible_v = FXD::STD::is_destructible< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISDESTRUCTIBLE_H