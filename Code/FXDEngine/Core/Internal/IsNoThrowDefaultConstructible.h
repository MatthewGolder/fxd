#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISNOTHROWDEFAULTCONSTRUCTIBLE_H
#define FXDENGINE_CORE_INTERNAL_ISNOTHROWDEFAULTCONSTRUCTIBLE_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/IsNoThrowConstructible.h"

namespace FXD
{
	namespace STD
	{
		// is_nothrow_default_constructible
#		define FXD_SUPPORTS_IS_NOTHROW_DEFUALT_CONSTRUCTIBLE 1

		template < typename T >
		struct is_nothrow_default_constructible : public FXD::STD::is_nothrow_constructible< T >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_nothrow_default_constructible_v = FXD::STD::is_nothrow_default_constructible< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISNOTHROWDEFAULTCONSTRUCTIBLE_H