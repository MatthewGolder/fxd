#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_SWAPHELPER_H
#define FXDENGINE_CORE_INTERNAL_SWAPHELPER_H

#include "FXDEngine/Core/Internal/AddLValueReference.h"
#include "FXDEngine/Core/Internal/AddRValueReference.h"
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Conjunction.h"
#include "FXDEngine/Core/Internal/Declval.h"
#include "FXDEngine/Core/Internal/EnableIf.h"
#include "FXDEngine/Core/Internal/IsMoveAssignable.h"
#include "FXDEngine/Core/Internal/IsMoveConstructible.h"
#include "FXDEngine/Core/Internal/IsNoThrowMoveAssignable.h"
#include "FXDEngine/Core/Internal/IsNoThrowMoveConstructible.h"
#include "FXDEngine/Core/Internal/Void_t.h"

namespace FXD
{
	namespace STD
	{
		namespace Internal
		{
			template < typename T >
			struct _is_swappable;

			template < typename T >
			struct _is_nothrow_swappable;
		} //namespace Internal

		template < typename T, typename FXD::STD::enable_if< FXD::STD::is_move_constructible< T >::value && FXD::STD::is_move_assignable< T >::value, FXD::S32 >::type = 0 >
		inline void swap(T& lhs, T& rhs) NOEXCEPT(FXD::STD::is_nothrow_move_constructible< T >::value && FXD::STD::is_nothrow_move_assignable< T >::value);

		template < typename T2, size_t SIZE, typename FXD::STD::enable_if< FXD::STD::Internal::_is_swappable< T2 >::value, FXD::S32 >::type = 0 >
		inline void swap(T2(&lhs)[SIZE], T2(&rhs)[SIZE]) NOEXCEPT(FXD::STD::Internal::_is_nothrow_swappable< T2 >::value);

		namespace Internal
		{
			template < typename T1, typename T2, typename = void >
			struct _swappable_with_helper : FXD::STD::false_type
			{};

			template < typename T1, typename T2 >
			struct _swappable_with_helper< T1, T2, FXD::STD::void_t< decltype(swap(FXD::STD::declval< T1 >(), FXD::STD::declval< T2 >())) > > : FXD::STD::true_type
			{};

			template < typename T1, typename T2 >
			struct _is_swappable_with : FXD::STD::integral_constant< bool, FXD::STD::conjunction< FXD::STD::Internal::_swappable_with_helper< T1, T2 >, FXD::STD::Internal::_swappable_with_helper< T2, T1 > >::value >
			{};

			template < typename T1, typename T2 >
			struct _swap_cannot_throw : FXD::STD::integral_constant< bool, NOEXCEPT(swap(FXD::STD::declval< T1 >(), FXD::STD::declval< T2 >())) && NOEXCEPT(swap(FXD::STD::declval< T2 >(), FXD::STD::declval< T1 >())) >
			{};

			template < typename T1, typename T2 >
			struct _is_nothrow_swappable_with : FXD::STD::integral_constant< bool, FXD::STD::conjunction< FXD::STD::Internal::_is_swappable_with< T1, T2 >, FXD::STD::Internal::_swap_cannot_throw< T1, T2 > >::value >
			{};

			template < typename T >
			struct _is_swappable : FXD::STD::Internal::_is_swappable_with< typename FXD::STD::add_lvalue_reference< T >::type, typename FXD::STD::add_lvalue_reference< T >::type >::type
			{};

			template < typename T >
			struct _is_nothrow_swappable : FXD::STD::Internal::_is_nothrow_swappable_with< typename FXD::STD::add_lvalue_reference< T >::type, typename FXD::STD::add_lvalue_reference< T >::type >::type
			{};

		} //namespace Internal

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_SWAPHELPER_H