#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_INVOKEHELPER_H
#define FXDENGINE_CORE_INTERNAL_INVOKEHELPER_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Conjunction.h"
#include "FXDEngine/Core/Internal/Decay.h"
#include "FXDEngine/Core/Internal/Declval.h"
#include "FXDEngine/Core/Internal/Disjunction.h"
#include "FXDEngine/Core/Internal/EnableIf.h"
#include "FXDEngine/Core/Internal/IsBaseOf.h"
#include "FXDEngine/Core/Internal/IsConvertible.h"
#include "FXDEngine/Core/Internal/IsMemberFunctionPointer.h"
#include "FXDEngine/Core/Internal/IsMemberObjectPointer.h"
#include "FXDEngine/Core/Internal/IsMemberPointer.h"
#include "FXDEngine/Core/Internal/IsSame.h"
#include "FXDEngine/Core/Internal/IsVoid.h"
#include "FXDEngine/Core/Internal/RemoveCVRef.h"
#include "FXDEngine/Core/Internal/Void_t.h"

namespace FXD
{
	namespace STD
	{
		template < typename T >
		class reference_wrapper;

		namespace Internal
		{
			CONSTEXPR FXD::S32 _Small_object_num_ptrs = 6 + 16 / sizeof(void*);

			template < typename T, template < typename... > class Template >
			CONSTEXPR bool _is_specialization_v = false; // true if and only if _Type is a specialization of Template

			template < template < typename... > class Template, class... Args >
			CONSTEXPR bool _is_specialization_v< Template< Args... >, Template > = true;

			template < typename T, template < typename... > class Template >
			struct _is_specialization : FXD::STD::integral_constant< bool, FXD::STD::Internal::_is_specialization_v< T, Template > >
			{};
		} //namespace Internal
	} //namespace STD


	namespace Internal
	{
#pragma warning(push) // TRANSITION, DevCom-936696
#pragma warning(disable : 28278) // Function '%s' appears with no prototype in scope
	
		enum class _Invoker_strategy
		{
			_Functor,
			_Pmf_object,
			_Pmf_refwrap,
			_Pmf_pointer,
			_Pmd_object,
			_Pmd_refwrap,
			_Pmd_pointer
		};

		struct _invoke_functor
		{
			template < typename C, typename... Args >
			static CONSTEXPR auto _call(C&& call, Args&&... args) NOEXCEPT(NOEXCEPT(static_cast< C&& >(call)(static_cast< Args&& >(args)...)))->decltype(static_cast< C&& >(call)(static_cast< Args&& >(args)...))
			{
				return static_cast< C&& >(call)(static_cast< Args&& >(args)...);
			}
		};

		struct _Invoker_pmf_object
		{
			template < typename Decayed, typename T, typename... Args >
			static CONSTEXPR auto _call(Decayed pDecay, T&& arg1, Args&&... args) NOEXCEPT(NOEXCEPT((static_cast< T&& >(arg1).*pDecay)(static_cast<Args&&>(args)...)))->decltype((static_cast< T&& >(arg1).*pDecay)(static_cast< Args&& >(args)...))
			{
				return (static_cast< T&& >(arg1).*pDecay)(static_cast< Args&& >(args)...);
			}
		};

		struct _Invoker_pmf_refwrap
		{
			template < typename Decayed, typename Refwrap, typename... Args >
			static CONSTEXPR auto _call(Decayed pDecay, Refwrap rw, Args&&... args) NOEXCEPT(NOEXCEPT((rw.get().*pDecay)(static_cast< Args&& >(args)...)))->decltype((rw.get().*pDecay)(static_cast< Args&& >(args)...))
			{
				return (rw.get().*pDecay)(static_cast< Args&& >(args)...);
			}
		};

		struct _Invoker_pmf_pointer
		{
			template < typename Decayed, typename T, typename... Args >
			static CONSTEXPR auto _call(Decayed pDecay, T&& arg1, Args&&... args) NOEXCEPT(NOEXCEPT(((*static_cast< T&& >(arg1)).*pDecay)(static_cast< Args&& >(args)...)))->decltype(((*static_cast< T&& >(arg1)).*pDecay)(static_cast< Args&& >(args)...))
			{
				return ((*static_cast< T&& >(arg1)).*pDecay)(static_cast< Args&& >(args)...);
			}
		};

		struct _Invoker_pmd_object
		{
			template < typename Decayed, typename T >
			static CONSTEXPR auto _call(Decayed pDecay, T&& arg) NOEXCEPT->decltype(static_cast< T&& >(arg).*pDecay)
			{
				return static_cast< T&& >(arg).*pDecay;
			}
		};

		struct _Invoker_pmd_refwrap
		{
			template < typename Decayed, typename Refwrap >
			static CONSTEXPR auto _call(Decayed pDecay, Refwrap rw) NOEXCEPT->decltype(rw.get().*pDecay)
			{
				return rw.get().*pDecay;
			}
		};

		struct _Invoker_pmd_pointer
		{
			template < typename Decayed, typename T >
			static CONSTEXPR auto _call(Decayed pDecay, T&& arg) NOEXCEPT(NOEXCEPT((*static_cast< T&& >(arg)).*pDecay))->decltype((*static_cast< T&& >(arg)).*pDecay)
			{
				return (*static_cast< T&& >(arg)).*pDecay;
			}
		};

		template < typename C, typename T, typename RemovedCVRef = typename FXD::STD::remove_cvref< C >::type, bool _Is_pmf = FXD::STD::is_member_function_pointer< RemovedCVRef >::value, bool _Is_pmd = FXD::STD::is_member_object_pointer< RemovedCVRef >::value >
		struct _invoke_impl;

		template < typename C, typename T, typename RemovedCVRef >
		struct _invoke_impl< C, T, RemovedCVRef, true, false > : FXD::STD::conditional< FXD::STD::is_base_of< typename FXD::STD::Internal::_is_member_function_pointer< RemovedCVRef >::class_type, typename FXD::STD::remove_reference< T >::type >::value, FXD::Internal::_Invoker_pmf_object, typename FXD::STD::conditional< FXD::STD::Internal::_is_specialization_v< typename FXD::STD::remove_cvref< T >::type, FXD::STD::reference_wrapper >, FXD::Internal::_Invoker_pmf_refwrap, FXD::Internal::_Invoker_pmf_pointer >::type >::type
		{}; // pointer to member function

		template < typename C, typename T, typename RemovedCVRef >
		struct _invoke_impl< C, T, RemovedCVRef, false, true > : FXD::STD::conditional< FXD::STD::is_base_of< typename FXD::STD::Internal::_is_member_object_pointer< RemovedCVRef >::class_type, typename FXD::STD::remove_reference< T >::type >::value, FXD::Internal::_Invoker_pmd_object, typename FXD::STD::conditional< FXD::STD::Internal::_is_specialization_v< typename FXD::STD::remove_cvref< T >::type, FXD::STD::reference_wrapper >, FXD::Internal::_Invoker_pmd_refwrap, FXD::Internal::_Invoker_pmd_pointer >::type >::type
		{}; // pointer to member data

		template < typename C, typename T, typename RemovedCVRef >
		struct _invoke_impl< C, T, RemovedCVRef, false, false > : FXD::Internal::_invoke_functor
		{};
	} //namespace Internal

	template < typename C >
	CONSTEXPR17 auto invoke(C&& call) NOEXCEPT(NOEXCEPT(static_cast< C&& >(call)())) -> decltype(static_cast< C&& >(call)())
	{
		return static_cast< C&& >(call)();
	}

	template < typename C, typename T, typename... Args >
	CONSTEXPR17 auto invoke(C&& call, T&& arg1, Args&&... args) NOEXCEPT(NOEXCEPT(FXD::Internal::_invoke_impl< C, T >::_call(static_cast< C&& >(call), static_cast< T&& >(arg1), static_cast< Args&& >(args)...)))-> decltype(FXD::Internal::_invoke_impl< C, T >::_call(static_cast< C&& >(call), static_cast< T&& >(arg1), static_cast< Args&& >(args)...))
	{
		return FXD::Internal::_invoke_impl< C, T >::_call(static_cast< C&& >(call), static_cast< T&& >(arg1), static_cast< Args&& >(args)...);
	}

#pragma warning(pop) 

	namespace Internal
	{
		template < typename T, bool = FXD::STD::is_void< T >::value >
		struct _invoker_ret
		{}; // helper to give INVOKE an explicit return type; avoids undesirable Expression SFINAE

		struct _BindUnforced
		{
			EXPLICIT _BindUnforced(void) = default;
		}; // tag to distinguish bind() from bind<R>()

		template < typename CvVoid >
		struct _invoker_ret< CvVoid, true >
		{
			// selected for T being cv void
			template < typename... Args >
			static void _call(Args&&... args)
			{
				FXD::invoke(static_cast< Args&& >(args)...);
			}
		};

		template < typename T >
		struct _invoker_ret< T, false >
		{
			// selected for all T other than cv void and _BindUnforced
			template < typename... Args >
			static T _call(Args&&... args)
			{
				return FXD::invoke(static_cast< Args&& >(args)...);
			}
		};

		template <>
		struct _invoker_ret< FXD::Internal::_BindUnforced, false >
		{
			// selected for T being _BindUnforced
			template < typename... Args >
			static auto _call(Args&&... args)-> decltype(FXD::invoke(static_cast< Args&& >(args)...))
			{
				return FXD::invoke(static_cast< Args&& >(args)...);
			}
		};


		// TYPE TRAITS FOR invoke()
#pragma warning(push)
	#pragma warning(disable : 4242) // 'identifier': conversion from '_From' to '_To', possible loss of data (/Wall)
	#pragma warning(disable : 4244) // 'argument': conversion from '_From' to '_To', possible loss of data
	#pragma warning(disable : 4365) // 'argument': conversion from '_From' to '_To', signed/unsigned mismatch (/Wall)
	#pragma warning(disable : 5215) // '%s' a function parameter with a volatile qualified type is deprecated in C++20

#ifdef __clang__
	#pragma clang diagnostic push
	#pragma clang diagnostic ignored "-Wdeprecated-volatile"
#endif // __clang__

		template < typename T >
		void _Implicitly_convert_to(T) NOEXCEPT; // not defined

		template < typename F, typename T, bool = FXD::STD::is_convertible< F, T >::value, bool = FXD::STD::is_void< T >::value >
		CONSTEXPR bool _Is_nothrow_convertible_v = NOEXCEPT(FXD::Internal::_Implicitly_convert_to< T >(FXD::STD::declval< F >()));

#ifdef __clang__
	#pragma clang diagnostic pop
#endif // __clang__

#pragma warning(pop)

		template < typename F, typename T, bool _IsVoid >
		CONSTEXPR bool _Is_nothrow_convertible_v< F, T, false, _IsVoid > = false;

		template < typename F, typename T >
		CONSTEXPR bool _Is_nothrow_convertible_v< F, T, true, true > = true;

		template < typename F, typename T >
		struct _Is_nothrow_convertible : FXD::STD::integral_constant< bool, FXD::Internal::_Is_nothrow_convertible_v< F, T > >
		{};
	} //namespace Internal

#if _HAS_CXX20
	template < typename F, typename T >
	CONSTEXPR bool is_nothrow_convertible_v = FXD::Internal::_Is_nothrow_convertible_v< F, T >;

	template < typename F, typename T >
	using is_nothrow_convertible = FXD::Internal::_Is_nothrow_convertible< F, T >;
#endif // _HAS_CXX20

	namespace STD
	{
		namespace Internal
		{
			template < typename _Void, typename... Args >
			struct _invoke_traits
			{
				using is_invocable = FXD::STD::false_type;
				using is_nothrow_invocable = FXD::STD::false_type;
				template < typename T >
				using is_invocable_r = FXD::STD::false_type;
				template < typename T >
				using is_nothrow_invocable_r = FXD::STD::false_type;
			};

			template < typename... Args >
			struct _invoke_traits< FXD::STD::void_t< decltype(FXD::invoke(FXD::STD::declval< Args >()...)) >, Args... >
			{
				using type = decltype(FXD::invoke(FXD::STD::declval< Args >()...));
				using is_invocable = FXD::STD::true_type;
				using is_nothrow_invocable = FXD::STD::integral_constant< bool, NOEXCEPT_OPER(FXD::invoke(FXD::STD::declval< Args >()...))>;
				template < typename T >
				using is_invocable_r = FXD::STD::integral_constant< bool, FXD::STD::disjunction< FXD::STD::is_void< T >, FXD::STD::is_convertible< type, T > >::value >;
				template < typename T >
				using is_nothrow_invocable_r = FXD::STD::integral_constant< bool, FXD::STD::conjunction< is_nothrow_invocable, FXD::STD::disjunction< FXD::STD::is_void< T >, FXD::Internal::_Is_nothrow_convertible< type, T > > >::value >;
			};

		} //namespace Internal
	} //namespace STD

} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_INVOKEHELPER_H