// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REMOVECOPYIF_H
#define FXDENGINE_CORE_INTERNAL_REMOVECOPYIF_H

#include <utility>

namespace FXD
{
	namespace STD
	{
		// remove_copy_if
#		define FXD_SUPPORTS_REMOVE_COPY_IF 1

		template < typename InputItr, typename OutputItr, typename Predicate >
		inline OutputItr remove_copy_if(InputItr first, InputItr last, OutputItr dst, Predicate pred)
		{
			for (; first != last; ++first)
			{
				if (!pred(*first))
				{
					*dst = std::move(*first);
					++dst;
				}
			}
			return dst;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REMOVECOPYIF_H