// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MISMATCH_H
#define FXDENGINE_CORE_INTERNAL_MISMATCH_H

#include "FXDEngine/Core/Pair.h"
#include "FXDEngine/Core/Internal/MismatchIf.h"

namespace FXD
{
	namespace STD
	{
		// mismatch
#		define FXD_SUPPORTS_MISMATCH 1

		template < typename InputItr1, typename InputItr2, typename Predicate >
		inline Core::Pair< InputItr1, InputItr2 > mismatch(InputItr1 first1, InputItr1 last, InputItr2 first2, Predicate pred)
		{
			return FXD::STD::mismatch_if(first1, last, first2, pred);
		}

		template < typename InputItr1, typename InputItr2 >
		inline Core::Pair< InputItr1, InputItr2 > mismatch(InputItr1 first1, InputItr1 last, InputItr2 first2)
		{
			return FXD::STD::mismatch(first1, last, first2, FXD::STD::equal_to<>());
		}

		template < typename InputItr1, typename InputItr2, typename Predicate >
		inline Core::Pair< InputItr1, InputItr2 > mismatch(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)
		{
			return FXD::STD::mismatch_if(first1, last1, first2, last2, pred);
		}

		template < typename InputItr1, typename InputItr2 >
		inline Core::Pair< InputItr1, InputItr2 > mismatch(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)
		{
			return FXD::STD::mismatch(first1, last1, first2, last2, FXD::STD::equal_to<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MISMATCH_H