#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_DISJUNCTION_H
#define FXDENGINE_CORE_INTERNAL_DISJUNCTION_H

#include "FXDEngine/Core/Internal/Conditional.h"
#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// disjunction
#		define FXD_SUPPORTS_DISJUNCTION 1

		template < typename... >
		struct disjunction : FXD::STD::false_type
		{};

		template < typename T >
		struct disjunction< T > : T
		{};

		template < typename T, typename... Tn >
		struct disjunction< T, Tn... > : FXD::STD::conditional< bool(T::value), T, FXD::STD::disjunction< Tn...> >::type
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename...Tn >
		CONSTEXPR bool disjunction_v = FXD::STD::disjunction< Tn... >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_DISJUNCTION_H