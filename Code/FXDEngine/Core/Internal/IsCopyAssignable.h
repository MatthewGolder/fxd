#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISCOPYASSIGNABLE_H
#define FXDENGINE_CORE_INTERNAL_ISCOPYASSIGNABLE_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsAssignable.h"
#include "FXDEngine/Core/Internal/AddConst.h"
#include "FXDEngine/Core/Internal/AddLValueReference.h"

namespace FXD
{
	namespace STD
	{
		// is_copy_assignable
#		define FXD_SUPPORTS_IS_COPY_ASSIGNABLE 1

		template < typename T >
		struct is_copy_assignable : public FXD::STD::is_assignable< typename FXD::STD::add_lvalue_reference< T >::type, typename FXD::STD::add_lvalue_reference< typename FXD::STD::add_const< T >::type >::type >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_copy_assignable_v = FXD::STD::is_copy_assignable< T>::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISCOPYASSIGNABLE_H