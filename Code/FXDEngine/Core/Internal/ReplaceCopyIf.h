#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REPLACECOPYIF_H
#define FXDENGINE_CORE_INTERNAL_REPLACECOPYIF_H

namespace FXD
{
	namespace STD
	{
		// replace_copy_if
#		define FXD_SUPPORTS_REPLACE_COPY_IF 1

		template < typename InputItr, typename OutputItr, typename Predicate, typename T >
		inline OutputItr replace_copy_if(InputItr first, InputItr last, OutputItr dst, Predicate pred, const T& newVal)
		{
			for (; first != last; ++first, ++dst)
			{
				*dst = pred(*first) ? newVal : *first;
			}
			return dst;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REPLACECOPYIF_H