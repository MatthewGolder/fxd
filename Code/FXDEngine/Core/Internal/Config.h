// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_CONFIG_H
#define FXDENGINE_CORE_INTERNAL_CONFIG_H

#include "FXDEngine/Core/Internal/Compiler.h"

// Platform defines
#if defined(_WIN32) || defined(_WIN64)
// If you want to force the Platform to use posix instead of the default system functions
//#	define FXD_FORCE_POSIX
#	define IsOSPC() 1
#	define IsOSAndroid() 0
#	if defined(FXD_FORCE_POSIX)
#		define IsSystemPosix() 1
#		define IsSystemWindows() 0
#	else
#		define IsSystemPosix() 0
#		define IsSystemWindows() 1
#	endif
#elif defined(__ANDROID__)
#	define IsOSPC() 0
#	define IsOSAndroid() 1
#	define IsSystemPosix() 1
#	define IsSystemWindows() 0
#else
#	error "Undefined System"
#endif


// Defines
#if !defined(NO_COPY)
#	define NO_COPY(Class) Class(const Class& /*rhs*/) = delete
#endif

#if !defined(NO_MOVE)
#	define NO_MOVE(Class) Class(Class&& /*rhs*/) = delete
#endif

#if !defined(NO_ASSIGNMENT)
#	define NO_ASSIGNMENT(Class) Class& operator=(const Class& /*rhs*/) = delete
#endif

#if !defined(NO_MOVE_ASSIGNMENT)
#	define NO_MOVE_ASSIGNMENT(Class) Class& operator=(Class&& /*rhs*/) = delete
#endif

#if !defined(FXD_ALIGN_AS)
#	define FXD_ALIGN_AS(a) alignas(a)
#endif

#if !defined(FXD_ALIGN_OF)
#	define FXD_ALIGN_OF(a) alignof(a)
#endif

#if !defined(FXD_ARRAY_COUNT)
#	define FXD_ARRAY_COUNT(x) (sizeof(x) / sizeof(x[0]))
#endif

#if !defined(PURE)
#	define PURE = 0
#endif

#if !defined(OVERRIDE)
#	define OVERRIDE override
#endif

#if !defined(FINAL)
#	define FINAL final
#endif

#if !defined(NOEXCEPT)
#	define NOEXCEPT noexcept
#endif

#if !defined(NOEXCEPT_COND)
#	define NOEXCEPT_COND(...)	noexcept(__VA_ARGS__)
#endif

#if !defined(NOEXCEPT_OPER)
#	define NOEXCEPT_OPER(...)	noexcept(__VA_ARGS__)
#endif

#if !defined(NOEXCEPT_IF)
#	define NOEXCEPT_IF(x) noexcept(x)
#endif

#if !defined(EXPLICIT)
#	define EXPLICIT explicit
#endif

#if !defined(CONSTEXPR)
#	define CONSTEXPR constexpr
#	define CONSTEXPR_DECL constexpr
#else
#	define CONSTEXPR inline
#	define CONSTEXPR_DECL
#endif

#if !defined(CONSTEXPR14)
#	if defined(COMPILER_MSVC14)
#		define CONSTEXPR14
#		define NO_CONSTEXPR14
#	elif IsCPP14()
#		define CONSTEXPR14 constexpr
#	else
#		define CONSTEXPR14 inline
#		define NO_CONSTEXPR14 
#	endif
#endif

#if !defined(CONSTEXPR17)
#	if IsCPP17()
#		define CONSTEXPR17 constexpr
#	else
#		define CONSTEXPR17 inline
#		define NO_CONSTEXPR17 
#	endif
#endif

#if !defined(CONSTEXPR_HAS_IF)
#	if IsCPP17()
#		define CONSTEXPR_HAS_IF 0
#	else
#		define CONSTEXPR_HAS_IF 0
#	endif
#endif

#if !defined(CONSTEXPR_IF)
#	if CONSTEXPR_HAS_IF
#		define CONSTEXPR_IF constexpr
#	endif
#endif

#if !defined(STRUCT_OFFSET)
#	define STRUCT_OFFSET(struc, member) offsetof(struc, member)
#endif

// Decleration
#define SET(type, var, func) void func(type v) { m_##var = v; }
#define SET_REF(type, var, func) void func(const type& v) { m_##var = v; }

#define GET_R(type, var, func) const type func(void) const { return m_##var; }
#define GET_W(type, var, func) type func(void) { return m_##var; }
#define GET_RW(type, var, func) GET_R(type, var, func); GET_W(type, var, func)

#define GET_REF_R(type, var, func) const type& func(void) const { return m_##var; }
#define GET_REF_W(type, var, func) type& func(void) { return m_##var; }
#define GET_REF_RW(type, var, func) GET_REF_R(type, var, func); GET_REF_W(type, var, func)

#endif //FXDENGINE_CORE_INTERNAL_CONFIG_H