#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISALINGED_H
#define FXDENGINE_CORE_INTERNAL_ISALINGED_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// is_aligned
#		define FXD_SUPPORTS_IS_ALIGNED 1

		namespace Internal
		{
			template < typename T >
			struct _is_aligned
			{
				static const bool value = (FXD_ALIGN_OF(T) > 8);
			};
		} //namespace Internal

		template < typename T >
		struct is_aligned : public FXD::STD::integral_constant< bool, FXD::STD::Internal::_is_aligned< T >::value >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR size_t is_aligned_v = is_aligned< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISALINGED_H