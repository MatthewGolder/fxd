#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISMEMBERPOINTER_H
#define FXDENGINE_CORE_INTERNAL_ISMEMBERPOINTER_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsMemberFunctionPointer.h"
#include "FXDEngine/Core/Internal/IsMemberObjectPointer.h"

namespace FXD
{
	namespace STD
	{
		// is_member_pointer
#		define FXD_SUPPORTS_IS_MEMBER_POINTER 1

		template < typename T >
		struct is_member_pointer : FXD::STD::integral_constant< bool, FXD::STD::is_member_object_pointer< T >::value || FXD::STD::is_member_function_pointer< T >::value >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_member_pointer_v = FXD::STD::is_member_pointer< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISMEMBERPOINTER_H