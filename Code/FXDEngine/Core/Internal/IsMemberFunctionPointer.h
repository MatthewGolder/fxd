#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISMEMBERFUNCTIONPOINTER_H
#define FXDENGINE_CORE_INTERNAL_ISMEMBERFUNCTIONPOINTER_H

#include "FXDEngine/Core/Internal/ArgTypes.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/EnableIf.h"
#include "FXDEngine/Core/Internal/IsFunction.h"
#include "FXDEngine/Core/Internal/IsSame.h"
#include "FXDEngine/Core/Internal/RemoveCV.h"
//#include <xstddef>

namespace FXD
{
	namespace STD
	{
		// is_member_function_pointer
#		define FXD_SUPPORTS_IS_MEMBER_FUNCTION_POINTER 1

		namespace Internal
		{
			template < typename >
			struct _is_member_function_pointer : public FXD::STD::false_type
			{};

			template < typename T, typename CP >
			struct _is_member_function_pointer< T CP::* > : public FXD::STD::integral_constant< bool, FXD::STD::is_function< T >::value >
			{
				using class_type = CP;
			};
		} //namespace Internal

		template < typename T >
		struct is_member_function_pointer : public FXD::STD::Internal::_is_member_function_pointer< typename FXD::STD::remove_cv< T >::type >::type
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_member_function_pointer_v = FXD::STD::is_member_function_pointer< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISMEMBERFUNCTIONPOINTER_H