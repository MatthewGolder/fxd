#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MINELEMENT_H
#define FXDENGINE_CORE_INTERNAL_MINELEMENT_H

#include "FXDEngine/Core/Internal/MinElementIf.h"

namespace FXD
{
	namespace STD
	{
		// min_element
#		define FXD_SUPPORTS_MIN_ELEMENT 1

		template < typename ForwardItr, typename Predicate >
		CONSTEXPR17 ForwardItr min_element(ForwardItr first, ForwardItr last, Predicate pred)
		{
			return FXD::STD::min_element_if(first, last, pred);
		}

		template < typename ForwardItr >
		CONSTEXPR17 ForwardItr min_element(ForwardItr first, ForwardItr last)
		{
			return FXD::STD::min_element(first, last, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MINELEMENT_H