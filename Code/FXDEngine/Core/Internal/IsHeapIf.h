#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISHEAPIF_H
#define FXDENGINE_CORE_INTERNAL_ISHEAPIF_H

#include "FXDEngine/Core/Internal/IsHeapUntil.h"

namespace FXD
{
	namespace STD
	{
		// is_heap_if
#		define FXD_SUPPORTS_IS_HEAP_IF 1

		template < typename RandomAccessItr, typename Predicate >
		inline bool is_heap_if(RandomAccessItr first, RandomAccessItr last, Predicate pred)
		{
			return (FXD::STD::is_heap_until(first, last, pred) == last);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISHEAPIF_H