#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_UNMAKESIGNED_H
#define FXDENGINE_CORE_INTERNAL_UNMAKESIGNED_H

#include "FXDEngine/Core/Internal/ChangeSigned.h"

namespace FXD
{
	namespace STD
	{
		// make_unsigned
#		define FXD_SUPPORTS_MAKE_UNSIGNED 1

		template < typename T >
		struct make_unsigned
		{
			using type = typename FXD::STD::Internal::_change_sign< T >::_Unsigned;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using make_unsigned_t = typename FXD::STD::make_unsigned< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_UNMAKESIGNED_H