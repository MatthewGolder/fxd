// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_EXCLUSIVESCAN_H
#define FXDENGINE_CORE_INTERNAL_EXCLUSIVESCAN_H

#include "FXDEngine/Core/Internal/ExclusiveScanIf.h"
#include "FXDEngine/Core/Internal/Plus.h"

namespace FXD
{
	namespace STD
	{
		// exclusive_scan
#		define FXD_SUPPORTS_EXCLUSIVE_SCAN 1

		template < typename InputItr, typename OutputItr, typename T, typename Predicate >
		inline OutputItr exclusive_scan(InputItr first, InputItr last, OutputItr dst, T val, Predicate pred)
		{
			return FXD::STD::exclusive_scan_if(first, last, dst, val, pred);
		}

		template < typename InputItr, typename OutputItr, typename T >
		inline OutputItr exclusive_scan(InputItr first, InputItr last, OutputItr dst, T val)
		{
			return FXD::STD::exclusive_scan(first, last, dst, val, FXD::STD::plus<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_EXCLUSIVESCAN_H