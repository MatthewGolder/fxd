#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_LOGICALNOT_H
#define FXDENGINE_CORE_INTERNAL_LOGICALNOT_H

#include "FXDEngine/Core/Internal/Config.h"
#include <utility>

namespace FXD
{
	namespace STD
	{
		// logical_not
#		define FXD_SUPPORTS_LOGICAL_NOT 1

		template < typename T = void >
		struct logical_not
		{
			using first_argument_type = T;
			using result_type = T;

			CONSTEXPR14 bool operator()(const T& val) const
			{
				return (!val);
			}
		};

		template <>
		struct logical_not< void >
		{
			using is_transparent = FXD::S32;

			template < typename T >
			CONSTEXPR14 auto operator()(T&& val) const -> decltype(!std::forward< T >(val))
			{
				return (!std::forward< T >(val));
			}
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_LOGICALNOT_H