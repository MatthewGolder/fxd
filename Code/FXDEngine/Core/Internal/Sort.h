// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_SORT_H
#define FXDENGINE_CORE_INTERNAL_SORT_H

#include "FXDEngine/Core/Internal/Less.h"

namespace FXD
{
	namespace STD
	{
		// sort
#		define FXD_SUPPORTS_SORT 1

		template < typename Iterator, typename Cmp >
		void sort(Iterator first, Iterator /*last*/, Cmp cmp, size_t nSize)
		{
			if (nSize < 2)
			{
				return;
			}

			// Basic bubble sort that works on all containers
			Iterator itr1 = first;
			for (size_t n1 = 0; n1 < nSize; n1++, itr1++)
			{
				Iterator itr2 = itr1;
				itr2++;
				for (size_t n2 = n1 + 1; n2 < nSize; n2++, itr2++)
				{
					if (cmp((*itr2), (*itr1)))
					{
						FXD::STD::iter_swap(itr1, itr2);
					}
				}
			}
		}

		template < typename Iterator, typename Predicate >
		void sort(Iterator first, Iterator last, Predicate pred)
		{
			FXD::STD::sort(first, last, pred, (last - first));
		}

		template < typename Iterator >
		void sort(Iterator first, Iterator last)
		{
			FXD::STD::sort(first, last, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_SORT_H