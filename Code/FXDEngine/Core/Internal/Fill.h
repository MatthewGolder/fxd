#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_FILL_H
#define FXDENGINE_CORE_INTERNAL_FILL_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/TypeTraits.h"

namespace FXD
{
	namespace STD
	{
		// fill
#		define FXD_SUPPORTS_FILL 1

		namespace Internal
		{
			template < bool bIsScalar >
			struct _fill
			{
				template < typename ForwardItr, typename T >
				static void _impl(ForwardItr first, ForwardItr last, const T& val)
				{
					for (; first != last; ++first)
					{
						(*first) = val;
					}
				}
			};
			template <>
			struct _fill< true >
			{
				template < typename ForwardItr, typename T >
				static void _impl(ForwardItr first, ForwardItr last, const T& val)
				{
					using value_type = typename FXD::STD::iterator_traits< ForwardItr >::value_type;

					for (const T temp = val; first != last; ++first)
					{
						(*first) = static_cast<value_type>(temp);
					}
				}
			};
		} //namespace Internal

		template < typename ForwardItr, typename T >
		inline void fill(ForwardItr first, ForwardItr last, const T& val)
		{
			FXD::STD::Internal::_fill< FXD::STD::is_scalar< T >::value >::_impl(first, last, val);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_FILL_H