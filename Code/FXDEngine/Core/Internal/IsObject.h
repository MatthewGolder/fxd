#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISOBJECT_H
#define FXDENGINE_CORE_INTERNAL_ISOBJECT_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsFunction.h"
#include "FXDEngine/Core/Internal/IsReference.h"
#include "FXDEngine/Core/Internal/IsVoid.h"

namespace FXD
{
	namespace STD
	{
		// is_object
#		define FXD_SUPPORTS_IS_OBJECT 1

		template < typename T >
		struct is_object : public FXD::STD::integral_constant< bool, !FXD::STD::is_reference< T >::value && !FXD::STD::is_void< T >::value && !FXD::STD::is_function< T >::value >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_object_v = FXD::STD::is_object< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISOBJECT_H