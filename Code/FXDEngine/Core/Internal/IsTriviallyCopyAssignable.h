#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISTRIVIALLYCOPYASSIGNABLE_H
#define FXDENGINE_CORE_INTERNAL_ISTRIVIALLYCOPYASSIGNABLE_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/IsTriviallyAssignable.h"
#include "FXDEngine/Core/Internal/AddConst.h"
#include "FXDEngine/Core/Internal/AddLValueReference.h"

namespace FXD
{
	namespace STD
	{
		// is_trivially_copy_assignable
#		define FXD_SUPPORTS_IS_TRIVIALLY_COPY_ASSIGNABLE 1

		template < typename T >
		struct is_trivially_copy_assignable : public FXD::STD::is_trivially_assignable< typename FXD::STD::add_lvalue_reference< T >::type, typename FXD::STD::add_lvalue_reference< typename FXD::STD::add_const< T >::type >::type >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_trivially_copy_assignable_v = FXD::STD::is_trivially_copy_assignable< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISTRIVIALLYCOPYASSIGNABLE_H