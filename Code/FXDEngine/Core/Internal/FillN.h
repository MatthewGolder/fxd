#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_FILLN_H
#define FXDENGINE_CORE_INTERNAL_FILLN_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"

namespace FXD
{
	namespace STD
	{
		// fill_n
#		define FXD_SUPPORTS_FILL_N 1

		namespace Internal
		{
			template < bool bIsScalar >
			struct _fill_n
			{
				template < typename OutputItr, typename Size, typename T >
				static OutputItr _impl(OutputItr dst, Size nCount, const T& val)
				{
					for (; nCount-- > 0; ++dst)
					{
						(*dst) = val;
					}
					return dst;
				}
			};
			template <>
			struct _fill_n< true >
			{
				template < typename OutputItr, typename Size, typename T >
				static OutputItr _impl(OutputItr dst, Size nCount, const T& val)
				{
					using value_type = typename FXD::STD::iterator_traits< OutputItr >::value_type;

					for (const T temp = val; nCount-- > 0; ++dst)
					{
						(*dst) = static_cast<value_type>(temp);
					}
					return dst;
				}
			};
		} //namespace Internal

		template < typename OutputItr, typename Size, typename T >
		inline OutputItr fill_n(OutputItr dst, Size nCount, const T& val)
		{
			if (0 < nCount)
			{
				return FXD::STD::Internal::_fill_n< FXD::STD::is_scalar< T >::value >::_impl(dst, nCount, val);
			}
			return dst;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_FILLN_H