#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISSORTEDUNTILIF_H
#define FXDENGINE_CORE_INTERNAL_ISSORTEDUNTILIF_H

#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// is_sorted_until_if
#		define FXD_SUPPORTS_IS_SORTED_UNTIL_IF 1

		template < typename ForwardItr, typename Predicate >
		inline ForwardItr is_sorted_until_if(ForwardItr first, ForwardItr last, Predicate pred)
		{
			if (first != last)
			{
				ForwardItr itr = first;
				while (++itr != last)
				{
					if (pred((*itr), (*first)))
					{
						return itr;
					}
					first = itr;
				}
			}
			return last;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISSORTEDUNTILIF_H