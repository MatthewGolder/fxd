#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_PARTIALSORTCOPY_H
#define FXDENGINE_CORE_INTERNAL_PARTIALSORTCOPY_H

#include "FXDEngine/Core/Internal/Less.h"
#include "FXDEngine/Core/Internal/PartialSortCopyIf.h"

namespace FXD
{
	namespace STD
	{
		// partial_sort_copy
#		define FXD_SUPPORTS_PARTITION_SORT_COPY 1

		template < typename InputItr, typename RandomAccessItr, typename Predicate >
		inline RandomAccessItr partial_sort_copy(InputItr first, InputItr last, RandomAccessItr dstFirst, RandomAccessItr dstLast, Predicate pred)
		{
			return FXD::STD::partial_sort_copy_if(first, last, dstFirst, dstLast, pred);
		}

		template < typename InputItr, typename RandomAccessItr >
		inline RandomAccessItr partial_sort_copy(InputItr first, InputItr last, RandomAccessItr dstFirst, RandomAccessItr dstLast)
		{
			return FXD::STD::partial_sort_copy(first, last, dstFirst, dstLast, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_PARTIALSORTCOPY_H