#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_PLUS_H
#define FXDENGINE_CORE_INTERNAL_PLUS_H

#include "FXDEngine/Core/Types.h"
#include <utility>

namespace FXD
{
	namespace STD
	{
		// plus
#		define FXD_SUPPORTS_PLUS 1

		template < typename T = void >
		struct plus
		{
			using first_argument_type = T;
			using second_argument_type = T;
			using result_type = T;

			CONSTEXPR14 T operator()(const T& lhs, const T& rhs)const
			{
				return lhs + rhs;
			}
		};

		template <>
		struct plus< void >
		{
			using is_transparent = FXD::S32;

			template < typename LHS, typename RHS >
			CONSTEXPR14 auto operator()(LHS&& lhs, RHS&& rhs)const->decltype(std::forward< LHS >(lhs) + std::forward< RHS >(rhs))
			{
				return std::forward< LHS >(lhs) + std::forward< RHS >(rhs);
			}
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_PLUS_H