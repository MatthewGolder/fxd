#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISSCALAR_H
#define FXDENGINE_CORE_INTERNAL_ISSCALAR_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsArithmetic.h"
#include "FXDEngine/Core/Internal/IsEnum.h"
#include "FXDEngine/Core/Internal/IsFunction.h"
#include "FXDEngine/Core/Internal/IsMemberPointer.h"
#include "FXDEngine/Core/Internal/IsNullPointer.h"
#include "FXDEngine/Core/Internal/IsPointer.h"
#include "FXDEngine/Core/Internal/RemoveCV.h"

namespace FXD
{
	namespace STD
	{
		// is_scalar
#		define FXD_SUPPORTS_IS_SCALAR 1

		template < typename T >
		struct is_scalar : public FXD::STD::integral_constant< bool, FXD::STD::is_arithmetic< T >::value || FXD::STD::is_enum< T >::value || FXD::STD::is_pointer< T >::value || FXD::STD::is_member_pointer< T >::value || FXD::STD::is_null_pointer< T >::value >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_scalar_v = FXD::STD::is_scalar< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISSCALAR_H