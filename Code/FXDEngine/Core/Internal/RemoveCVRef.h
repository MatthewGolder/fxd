#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REMOVECVREF_H
#define FXDENGINE_CORE_INTERNAL_REMOVECVREF_H

#include "FXDEngine/Core/Internal/RemoveConst.h"
#include "FXDEngine/Core/Internal/RemoveReference.h"
#include "FXDEngine/Core/Internal/RemoveVolatile.h"

namespace FXD
{
	namespace STD
	{
		// remove_cvref
#		define FXD_SUPPORTS_REMOVE_CVREF 1

		template < typename T >
		struct remove_cvref
		{
			using type = typename FXD::STD::remove_volatile< typename FXD::STD::remove_const< typename FXD::STD::remove_reference< T >::type >::type >::type;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using remove_cvref_t = typename FXD::STD::remove_cvref< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REMOVECVREF_H