#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_TOUNDERLYING_H
#define FXDENGINE_CORE_INTERNAL_TOUNDERLYING_H

#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// to_underlying
#		define FXD_SUPPORTS_TO_UNDERLYING 1

		template < typename T >
		struct underlying_type
		{
			typedef __underlying_type(T) type;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using underlying_type_t = typename FXD::STD::underlying_type< T >::type;

#		endif

		template < typename Type >
		CONSTEXPR auto to_underlying(const Type e) NOEXCEPT
		{
			return static_cast< typename FXD::STD::underlying_type< Type >::type >(e);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_TOUNDERLYING_H