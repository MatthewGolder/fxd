// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REFERENCEWRAPPER_H
#define FXDENGINE_CORE_INTERNAL_REFERENCEWRAPPER_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Invoke.h"
#include "FXDEngine/Core/Internal/RemoveCV.h"
#include "FXDEngine/Core/Internal/ResultOf.h"
#include "FXDEngine/Memory/Internal/AddressOf.h"
#include <cstddef>

namespace FXD
{
	namespace STD
	{
		// reference_wrapper
#		define FXD_SUPPORTS_REFERENCE_WRAPPER 1

		template < typename T >
		class reference_wrapper
		{
		public:
			using type = T;

		public:
			reference_wrapper(T& val) NOEXCEPT
				: m_pVal(FXD::STD::addressof(val))
			{}

			reference_wrapper(T&&) = delete;
			reference_wrapper(const reference_wrapper< T >& rhs) NOEXCEPT
				: m_pVal(rhs.m_pVal)
			{}

			reference_wrapper& operator=(const reference_wrapper< T >& rhs) NOEXCEPT
			{
				m_pVal = rhs.m_pVal;
				return (*this);
			}

			operator T& (void) const NOEXCEPT
			{
				return *m_pVal;
			}

			T& get(void) const NOEXCEPT
			{
				return *m_pVal;
			}

			template < typename... ArgTypes >
			typename FXD::STD::result_of< T&(ArgTypes&&...) >::type operator() (ArgTypes&&... args) const
			{
				return FXD::invoke(*m_pVal, std::forward< ArgTypes >(args)...);
			}

		private:
			T* m_pVal;
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REFERENCEWRAPPER_H