#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MAKESIGNED_H
#define FXDENGINE_CORE_INTERNAL_MAKESIGNED_H

#include "FXDEngine/Core/Internal/ChangeSigned.h"

namespace FXD
{
	namespace STD
	{
		// make_signed
#		define FXD_SUPPORTS_MAKE_SIGNED 1

			template < typename T >
		struct make_signed
		{
			using type = typename FXD::STD::Internal::_change_sign< T >::_Signed;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using make_signed_t = typename FXD::STD::make_signed< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MAKESIGNED_H