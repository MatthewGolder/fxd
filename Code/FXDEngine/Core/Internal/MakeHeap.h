#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MAKEHEAP_H
#define FXDENGINE_CORE_INTERNAL_MAKEHEAP_H

#include "FXDEngine/Core/Internal/MakeHeapIf.h"

namespace FXD
{
	namespace STD
	{
		// make_heap
#		define FXD_SUPPORTS_MAKE_HEAP 1

		template < typename RandomAccessItr, typename Predicate >
		inline void make_heap(RandomAccessItr first, RandomAccessItr last, Predicate pred)
		{
			FXD::STD::make_heap_if(first, last, pred);
		}

		template < typename RandomAccessItr >
		inline void make_heap(RandomAccessItr first, RandomAccessItr last)
		{
			FXD::STD::make_heap_if(first, last, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MAKEHEAP_H