#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ADDCV_H
#define FXDENGINE_CORE_INTERNAL_ADDCV_H

#include "FXDEngine/Core/Internal/AddConst.h"
#include "FXDEngine/Core/Internal/AddVolatile.h"

namespace FXD
{
	namespace STD
	{
		// add_cv
#		define FXD_SUPPORTS_ADD_CV 1

		template < typename T >
		struct add_cv
		{
			using type = typename FXD::STD::add_const< typename FXD::STD::add_volatile< T >::type >::type;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using add_cv_t = typename FXD::STD::add_cv< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ADDCV_H