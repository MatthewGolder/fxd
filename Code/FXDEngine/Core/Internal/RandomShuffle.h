#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_RANDOMSHUFFLE_H
#define FXDENGINE_CORE_INTERNAL_RANDOMSHUFFLE_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"

namespace FXD
{
	namespace STD
	{
		// random_shuffle
#		define FXD_SUPPORTS_RANDOM_SHUFFLE 1

		template < typename RandomAccessItr, typename RandomNumberGenerator >
		inline void random_shuffle(RandomAccessItr first, RandomAccessItr last, RandomNumberGenerator&& rng)
		{
			using difference_type = typename FXD::STD::iterator_traits< RandomAccessItr >::difference_type;

			for (RandomAccessItr itr = first + 1; itr < last; ++itr)
			{
				FXD::STD::iter_swap(itr, first + (difference_type)rng((std::size_t)((itr - first) + 1)));
			}
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_RANDOMSHUFFLE_H