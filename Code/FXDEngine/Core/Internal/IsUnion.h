//Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISUNION_H
#define FXDENGINE_CORE_INTERNAL_ISUNION_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// is_union
#		define FXD_SUPPORTS_IS_UNION 1

		template < typename T >
		struct is_union : public FXD::STD::integral_constant< bool, __is_union(T) >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_union_v = FXD::STD::is_union< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISUNION_H