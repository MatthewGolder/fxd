// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ROTATECOPY_H
#define FXDENGINE_CORE_INTERNAL_ROTATECOPY_H

#include "FXDEngine/Core/Internal/Copy.h"

namespace FXD
{
	namespace STD
	{
		// rotate_copy
#		define FXD_SUPPORTS_ROTATE_COPY 1

		template < typename ForwardItr, typename OutputItr >
		OutputItr rotate_copy(ForwardItr first, ForwardItr middle, ForwardItr last, OutputItr dst)
		{
			return FXD::STD::copy(first, middle, FXD::STD::copy(middle, last, dst));
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ROTATECOPY_H