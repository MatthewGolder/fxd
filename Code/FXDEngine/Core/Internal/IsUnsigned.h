#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISUNSIGNED_H
#define FXDENGINE_CORE_INTERNAL_ISUNSIGNED_H

#include "FXDEngine/Core/Types.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/RemoveCV.h"

namespace FXD
{
	namespace STD
	{
		// is_unsigned
#		define FXD_SUPPORTS_IS_UNSIGNED 1

		namespace Internal
		{
			template < typename T >
			struct _is_unsigned : public FXD::STD::false_type
			{};
			template <>
			struct _is_unsigned< FXD::U8 > : public FXD::STD::true_type
			{};
			template <>
			struct _is_unsigned< FXD::U16 > : public FXD::STD::true_type
			{};
			template <>
			struct _is_unsigned< FXD::U32 > : public FXD::STD::true_type
			{};
			template <>
			struct _is_unsigned< FXD::U64 > : public FXD::STD::true_type
			{};
		} //namespace Internal

		template < typename T >
		struct is_unsigned : public FXD::STD::Internal::_is_unsigned< typename FXD::STD::remove_cv< T >::type >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_unsigned_v = FXD::STD::is_unsigned< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISUNSIGNED_H