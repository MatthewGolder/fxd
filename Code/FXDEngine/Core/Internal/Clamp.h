// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_CLAMP_H
#define FXDENGINE_CORE_INTERNAL_CLAMP_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Less.h"

namespace FXD
{
	namespace STD
	{
		// clamp
#		define FXD_SUPPORTS_CLAMP 1

		template < typename T, typename Predicate >
		CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi, Predicate pred)
		{
			return pred(val, lo) ? lo : pred(hi, val) ? hi : val;
		}

		template < typename T >
		CONSTEXPR const T& clamp(const T& val, const T& lo, const T& hi)
		{
			return FXD::STD::clamp(val, lo, hi, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_CLAMP_H