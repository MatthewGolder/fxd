#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_INTEGERSEQUENCE_H
#define FXDENGINE_CORE_INTERNAL_INTEGERSEQUENCE_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/IsIntegral.h"
#include "FXDEngine/Core/CompileTimeChecks.h"

namespace FXD
{
	namespace STD
	{
		// integer_sequence
#		define FXD_SUPPORTS_INTEGER_SEQUENCE 1

		template < typename T, T... Ints >
		class integer_sequence
		{
		public:
			using value_type = T;

			CTC_ASSERT((FXD::STD::is_integral< T >::value), "Core: integer_sequence is_integral failure.");

			static CONSTEXPR std::size_t size(void) NOEXCEPT
			{
				return sizeof...(Ints);
			}
		};

		namespace Internal
		{
			template < size_t SIZE, typename IndexSeq >
			struct _make_index_sequence;

			template < size_t SIZE, size_t... IndexSeq >
			struct _make_index_sequence< SIZE, FXD::STD::integer_sequence< size_t, IndexSeq... > >
			{
				using type = typename FXD::STD::Internal::_make_index_sequence< SIZE - 1, FXD::STD::integer_sequence< size_t, SIZE - 1, IndexSeq... > >::type;
			};

			template < size_t... IndexSeq >
			struct _make_index_sequence< 0, FXD::STD::integer_sequence< size_t, IndexSeq... > >
			{
				using type = FXD::STD::integer_sequence< size_t, IndexSeq... >;
			};
		} //namespace Internal

		template < size_t... IndexSeq >
		using index_sequence = FXD::STD::integer_sequence< size_t, IndexSeq... >;

		template < size_t SIZE >
		using make_index_sequence = typename FXD::STD::Internal::_make_index_sequence< SIZE, FXD::STD::integer_sequence< size_t > >::type;

		namespace Internal
		{
			template < typename Target, typename IndexSeq >
			struct _integer_sequence_convert;

			template < typename Target, size_t... IndexSeq >
			struct _integer_sequence_convert< Target, FXD::STD::integer_sequence< size_t, IndexSeq... > >
			{
				using type = FXD::STD::integer_sequence< Target, IndexSeq... >;
			};

			template < typename T, size_t SIZE >
			struct _make_integer_sequence
			{
				using type = typename FXD::STD::Internal::_integer_sequence_convert< T, FXD::STD::make_index_sequence< SIZE > >::type;
			};
		} //namespace Internal

		template < typename T, size_t SIZE >
		using make_integer_sequence = typename FXD::STD::Internal::_make_integer_sequence< T, SIZE >::type;

		template < typename... T >
		using index_sequence_for = FXD::STD::make_index_sequence< sizeof...(T) >;

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_INTEGERSEQUENCE_H