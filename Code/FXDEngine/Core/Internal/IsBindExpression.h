// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISBINDEXPRESSION_H
#define FXDENGINE_CORE_INTERNAL_ISBINDEXPRESSION_H

#include "FXDEngine/Core/Internal/Constants.h"

namespace FXD
{
	namespace STD
	{
		// is_bind_expression

#		define FXD_SUPPORTS_IS_BIND_EXPRESSION 1

		namespace Internal
		{
			template < typename R, typename T, typename... Args >
			class _is_bind_expression;
		} //namespace Internal


		// STRUCT TEMPLATE is_bind_expression
		template < typename T >
		struct is_bind_expression : FXD::STD::false_type
		{};

		template < typename R, class T, class... Args >
		struct is_bind_expression< STD::Internal::_is_bind_expression< R, T, Args... > > : FXD::STD::true_type
		{};

		template < typename T >
		struct is_bind_expression< const T > : FXD::STD::is_bind_expression< T >::type
		{};

		template < typename T >
		struct is_bind_expression< volatile T > : FXD::STD::is_bind_expression< T >::type
		{};

		template < typename T >
		struct is_bind_expression< const volatile T > : FXD::STD::is_bind_expression< T >::type
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_bind_expression_v = FXD::STD::is_bind_expression< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISBINDEXPRESSION_H