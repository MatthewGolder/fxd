#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MINELEMENTIF_H
#define FXDENGINE_CORE_INTERNAL_MINELEMENTIF_H

#include "FXDEngine/Core/Internal/Less.h"

namespace FXD
{
	namespace STD
	{
		// min_element_if
#		define FXD_SUPPORTS_MIN_ELEMENT_IF 1

		template < typename ForwardItr, typename Predicate >
		CONSTEXPR17 ForwardItr min_element_if(ForwardItr first, ForwardItr last, Predicate pred)
		{
			if (first != last)
			{
				ForwardItr curMin = first;
				while (++first != last)
				{
					if (pred(*first, *curMin))
					{
						curMin = first;
					}
				}
				return curMin;
			}
			return first;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MINELEMENTIF_H