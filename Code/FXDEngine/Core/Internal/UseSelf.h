#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_USESELF_H
#define FXDENGINE_CORE_INTERNAL_USESELF_H

namespace FXD
{
	namespace STD
	{
		// UseSelf
		template < typename T >
		class UseSelf
		{
		public:
			using result_type = T;

		public:
			const T& operator()(const T& val) const
			{
				return val;
			}
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_USESELF_H