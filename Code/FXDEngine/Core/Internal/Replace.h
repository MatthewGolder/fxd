#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REPLACE_H
#define FXDENGINE_CORE_INTERNAL_REPLACE_H

#include "FXDEngine/Core/Internal/ReplaceIf.h"

namespace FXD
{
	namespace STD
	{
		// replace
#		define FXD_SUPPORTS_REPLACE 1

		template < typename ForwardItr, typename T, typename Predicate >
		inline void replace(ForwardItr first, ForwardItr last, const T& oldVal, const T& newVal, Predicate pred)
		{
			FXD::STD::replace_if(first, last, [&](const T& x) { return pred(x, oldVal); }, newVal);
		}

		template < typename ForwardItr, typename T >
		inline void replace(ForwardItr first, ForwardItr last, const T& oldVal, const T& newVal)
		{
			FXD::STD::replace(first, last, oldVal, newVal, FXD::STD::equal_to< T >());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REPLACE_H