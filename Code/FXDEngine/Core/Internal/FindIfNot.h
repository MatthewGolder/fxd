#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_FINDIFNOT_H
#define FXDENGINE_CORE_INTERNAL_FINDIFNOT_H

#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// find_if_not
#		define FXD_SUPPORTS_FIND_IF_NOT 1

		template < typename InputItr, typename Predicate >
		inline InputItr find_if_not(InputItr first, InputItr last, Predicate pred)
		{
			for (; first != last; ++first)
			{
				if (!pred(*first))
				{
					return first;
				}
			}
			return last;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_FINDIFNOT_H