#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REMOVEEXTENT_H
#define FXDENGINE_CORE_INTERNAL_REMOVEEXTENT_H

#include <cstddef>

namespace FXD
{
	namespace STD
	{
		// remove_extent
#		define FXD_SUPPORTS_REMOVE_EXTENT 1

		template < typename T >
		struct remove_extent
		{
			using type = T;
		};

		template < typename T >
		struct remove_extent<T[]>
		{
			using type = T;
		};

		template < typename T, std::size_t SIZE >
		struct remove_extent< T[SIZE] >
		{
			using type = T;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using remove_extent_t = typename FXD::STD::remove_extent< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REMOVEEXTENT_H