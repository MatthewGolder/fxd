// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_DEBUGGER_H
#define FXDENGINE_CORE_INTERNAL_DEBUGGER_H

#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Core/String.h"
#include "FXDEngine/Thread/CriticalSection.h"

namespace FXD
{
	namespace Core
	{
#if defined(ENABLE_DEBUGGER)
		struct DBGStreamOut::Internal
		{
			Thread::CSLock m_cs;
			Core::String8 m_str;
		};
#endif
	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_DEBUGGER_H