#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISVOID_H
#define FXDENGINE_CORE_INTERNAL_ISVOID_H

#include "FXDEngine/Core/Internal/Constants.h"

namespace FXD
{
	namespace STD
	{
		// is_void
#		define FXD_SUPPORTS_IS_VOID 1

		template < typename T >
		struct is_void : public FXD::STD::false_type
		{};

		template <>
		struct is_void< void > : public FXD::STD::true_type
		{};

		template <>
		struct is_void< void const > : public FXD::STD::true_type
		{};

		template <>
		struct is_void< void volatile > : public FXD::STD::true_type
		{};

		template <>
		struct is_void< void const volatile > : public FXD::STD::true_type
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_void_v = FXD::STD::is_void< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISVOID_H