#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_EQUALIF_H
#define FXDENGINE_CORE_INTERNAL_EQUALIF_H

#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// equal_if
#		define FXD_SUPPORTS_EQUAL_IF 1

		template < typename InputItr1, typename InputItr2, typename Predicate >
		CONSTEXPR14 inline bool equal_if(InputItr1 first1, InputItr2 last1, InputItr2 first2, InputItr2 last2, Predicate pred)
		{
			for (; first1 != last1; ++first1, ++first2)
			{
				if (!pred((*first1), (*first2)))
				{
					return false;
				}
			}
			return true;
		}

		template < typename InputItr1, typename InputItr2, typename Predicate >
		CONSTEXPR14 inline bool equal_if(InputItr1 first1, InputItr1 last, InputItr2 first2, Predicate pred)
		{
			for (; first1 != last; ++first1, ++first2)
			{
				if (!pred(*first1, *first2))
				{
					return false;
				}
			}
			return true;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_EQUALIF_H