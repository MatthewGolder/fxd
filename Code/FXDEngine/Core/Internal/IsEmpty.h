#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISEMPTY_H
#define FXDENGINE_CORE_INTERNAL_ISEMPTY_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// is_empty
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC) || defined(COMPILER_CLANG))
#		define FXD_SUPPORTS_IS_EMPTY 1

		template < typename T >
		struct is_empty : public FXD::STD::integral_constant< bool, __is_empty(T) >
		{};

#		else
#		define FXD_SUPPORTS_IS_EMPTY 1

		namespace Internal
		{
			template < typename T >
			struct _is_empty_t1 : public T
			{
				FXD::U8 m[64];
			};
			struct _is_empty_t2
			{
				FXD::U8 m[64];
			};

			// The inheritance in empty_helper_t1 will not work with non-class types
			template < typename T, bool is_a_class = false >
			struct _is_empty : public FXD::STD::false_type
			{};

			template < typename T >
			struct _is_empty< T, true > : public FXD::STD::integral_constant< bool, sizeof(FXD::STD::Internal::_is_empty_t1< T >) == sizeof(FXD::STD::Internal::_is_empty_t2) >
			{};

			template < typename T >
			struct _is_empty_2
			{
				using _T = typename FXD::STD::remove_cv< T >::type;
				using type = FXD::STD::Internal::_is_empty< _T, FXD::STD::is_class< _T >::value >;
			};
		} //namespace Internal

		template < typename T >
		struct is_empty : public FXD::STD::Internal::_is_empty_2< T >::type
		{};

#		endif

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_empty_v = FXD::STD::is_empty< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISEMPTY_H