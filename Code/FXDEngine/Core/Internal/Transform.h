#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_TRANSFORM_H
#define FXDENGINE_CORE_INTERNAL_TRANSFORM_H

namespace FXD
{
	namespace STD
	{
		// transform
#		define FXD_SUPPORTS_TRANSFORM 1

		template < typename InputItr, typename OutputItr, typename Predicate >
		inline OutputItr transform(InputItr first, InputItr last, OutputItr dst, Predicate pred)
		{
			for (; first != last; ++first, ++dst)
			{
				*dst = pred(*first);
			}
			return dst;
		}

		template < typename InputItr1, typename InputItr2, typename OutputItr, typename Predicate >
		inline OutputItr transform(InputItr1 first1, InputItr1 last1, InputItr2 first2, OutputItr dst, Predicate pred)
		{
			for (; first1 != last1; ++first1, ++first2, ++dst)
			{
				*dst = pred(*first1, *first2);
			}
			return dst;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_TRANSFORM_H