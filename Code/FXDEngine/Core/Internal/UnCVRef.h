#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_UNCVREF_H
#define FXDENGINE_CORE_INTERNAL_UNCVREF_H

#include "FXDEngine/Core/Internal/RemoveCV.h"
#include "FXDEngine/Core/Internal/RemoveReference.h"

namespace FXD
{
	namespace STD
	{
		// un_cvref
#		define FXD_SUPPORTS_UN_CVREF 1

		template < typename T >
		struct un_cvref
		{
			using type = typename FXD::STD::remove_cv< typename FXD::STD::remove_reference< T >::type >::type;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using un_cvref_t = typename FXD::STD::un_cvref< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_UNCVREF_H