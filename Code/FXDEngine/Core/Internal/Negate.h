#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_NEGATE_H
#define FXDENGINE_CORE_INTERNAL_NEGATE_H

#include "FXDEngine/Core/Types.h"
#include <utility>

namespace FXD
{
	namespace STD
	{
		// negate
#		define FXD_SUPPORTS_NEGATE 1

		template < typename T = void >
		struct negate
		{
			using first_argument_type = T;
			using result_type = T;

			CONSTEXPR14 T operator()(const T& a)const
			{
				return -a;
			}
		};

		template <>
		struct negate< void >
		{
			using is_transparent = FXD::S32;

			template < typename T >
			CONSTEXPR14 auto operator()(T&& t) const->decltype(-std::forward< T >(t))
			{
				return -std::forward< T >(t);
			}
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_NEGATE_H