#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_UNIQUECOPY_H
#define FXDENGINE_CORE_INTERNAL_UNIQUECOPY_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/AddLValueReference.h"
#include "FXDEngine/Core/Internal/EqualTo.h"

namespace FXD
{
	namespace STD
	{
		// unique_copy
#		define FXD_SUPPORTS_UNIQUE_COPY 1

		namespace Internal
		{
			template < typename Predicate, typename InputItr, typename OutputItr >
			OutputItr _unique_copy(InputItr first, InputItr last, OutputItr dst, Predicate pred, FXD::STD::input_iterator_tag, FXD::STD::output_iterator_tag)
			{
				if (first != last)
				{
					typename FXD::STD::iterator_traits< InputItr >::value_type val(*first);
					*dst = val;
					++dst;
					while (++first != last)
					{
						if (!pred(val, *first))
						{
							val = *first;
							*dst = val;
							++dst;
						}
					}
				}
				return dst;
			}

			template < typename Predicate, typename ForwardItr, typename OutputItr >
			OutputItr _unique_copy(ForwardItr first, ForwardItr last, OutputItr dst, Predicate pred, FXD::STD::forward_iterator_tag, FXD::STD::output_iterator_tag)
			{
				if (first != last)
				{
					ForwardItr itr = first;
					*dst = *itr;
					++dst;
					while (++first != last)
					{
						if (!pred(*itr, *first))
						{
							*dst = *first;
							++dst;
							itr = first;
						}
					}
				}
				return dst;
			}

			template < typename Predicate, typename InputItr, typename ForwardItr >
			ForwardItr _unique_copy(InputItr first, InputItr last, ForwardItr dst, Predicate pred, FXD::STD::input_iterator_tag, FXD::STD::forward_iterator_tag)
			{
				if (first != last)
				{
					*dst = *first;
					while (++first != last)
					{
						if (!pred(*dst, *first))
						{
							*++dst = *first;
						}
					}
					++dst;
				}
				return dst;
			}
		} //namespace Internal

		template < typename InputItr, typename OutputItr, typename Predicate >
		inline OutputItr unique_copy(InputItr first, InputItr last, OutputItr dst, Predicate pred)
		{
			return FXD::STD::Internal::_unique_copy< typename FXD::STD::add_lvalue_reference< Predicate >::type >(first, last, dst, pred, typename FXD::STD::iterator_traits< InputItr >::iterator_category(), typename FXD::STD::iterator_traits< OutputItr >::iterator_category());
		}

		template < typename InputItr, typename OutputItr >
		inline OutputItr unique_copy(InputItr first, InputItr last, OutputItr dst)
		{
			return FXD::STD::unique_copy(first, last, dst, FXD::STD::equal_to< void >());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_UNIQUECOPY_H