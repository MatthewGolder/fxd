// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_RESULTOF_H
#define FXDENGINE_CORE_INTERNAL_RESULTOF_H

#include "FXDEngine/Core/Internal/Declval.h"

namespace FXD
{
	namespace STD
	{
		// result_of
#		define FXD_SUPPORTS_RESULT_OF 1

		template < typename >
		struct result_of;

		template < typename F, typename... ArgTypes >
		struct result_of< F(ArgTypes...) >
		{
			using type = decltype(FXD::STD::declval< F >()(FXD::STD::declval< ArgTypes >()...));
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using result_of_t = typename FXD::STD::result_of< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_RESULTOF_H