#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_RANK_H
#define FXDENGINE_CORE_INTERNAL_RANK_H

#include "FXDEngine/Core/Internal/Constants.h"

namespace FXD
{
	namespace STD
	{
		// rank
#		define FXD_SUPPORTS_RANK 1

		template < typename T >
		struct rank : FXD::STD::integral_constant< size_t, 0 >
		{};

		template < typename T, size_t SIZE >
		struct rank< T[SIZE] > : FXD::STD::integral_constant< size_t, FXD::STD::rank< T >::value + 1 >
		{};

		template < typename T >
		struct rank< T[] > : FXD::STD::integral_constant< size_t, FXD::STD::rank< T >::value + 1 >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR auto rank_v = FXD::STD::rank< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_RANK_H