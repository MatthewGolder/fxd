#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_HASTRIVIALASSIGN_H
#define FXDENGINE_CORE_INTERNAL_HASTRIVIALASSIGN_H

#include "FXDEngine/Core/Types.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsConst.h"
#include "FXDEngine/Core/Internal/IsPod.h"
#include "FXDEngine/Core/Internal/IsVolatile.h"

namespace FXD
{
	namespace STD
	{
		// has_trivial_assign
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC) || defined(COMPILER_CLANG))
#		define FXD_SUPPORTS_HAS_TRIVIAL_ASSIGN 1

		template < typename T >
		struct has_trivial_assign : public FXD::STD::integral_constant< bool, (__has_trivial_assign(T) || FXD::STD::is_pod< T >::value) && !FXD::STD::is_const< T >::value && !FXD::STD::is_volatile< T >::value >
		{};

#		else
#		define FXD_SUPPORTS_HAS_TRIVIAL_ASSIGN 1

		template < typename T >
		struct has_trivial_assign : public FXD::STD::integral_constant< bool, FXD::STD::is_pod< T >::value && !FXD::STD::is_const< T >::value && !FXD::STD::is_volatile< T >::value >
		{};

#		endif

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool has_trivial_assign_v = FXD::STD::has_trivial_assign< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_HASTRIVIALASSIGN_H