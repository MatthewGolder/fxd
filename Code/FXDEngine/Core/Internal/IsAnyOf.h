#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISANYOF_H
#define FXDENGINE_CORE_INTERNAL_ISANYOF_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Disjunction.h"
#include "FXDEngine/Core/Internal/IsSame.h"

namespace FXD
{
	namespace STD
	{
		// is_any_of
#		define FXD_SUPPORTS_IS_ANY_OF 1
		template < typename T, typename... Types >
		class is_any_of : public FXD::STD::disjunction< FXD::STD::is_same< T, Types >... >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T, typename... Types >
		CONSTEXPR auto is_any_of_v = FXD::STD::is_any_of< T, Types... >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISANYOF_H