#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_BADFUNCTIONCALL_H
#define FXDENGINE_CORE_INTERNAL_BADFUNCTIONCALL_H

#include "FXDEngine/Core/Types.h"
#include <exception>

namespace FXD
{
	namespace STD
	{
		// bad_function_call
#		define FXD_SUPPORTS_BAD_FUNCTION_CALL 1

		class bad_function_call : public std::exception
		{
		public:
			bad_function_call(void) NOEXCEPT
			{}

			virtual const FXD::UTF8* what(void) const NOEXCEPT OVERRIDE
			{
				return ("bad function call");
			}
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_FUNCTION_H