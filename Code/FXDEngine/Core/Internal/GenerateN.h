#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_GENERATEN_H
#define FXDENGINE_CORE_INTERNAL_GENERATEN_H

namespace FXD
{
	namespace STD
	{
		// generate_n
#		define FXD_SUPPORTS_GENERATE_N 1

		template < typename OutputItr, typename Size, typename Generator >
		inline OutputItr generate_n(OutputItr first, Size nSize, Generator generator)
		{
			for (; nSize > 0; --nSize, ++first)
			{
				*first = generator();
			}
			return first;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_GENERATEN_H