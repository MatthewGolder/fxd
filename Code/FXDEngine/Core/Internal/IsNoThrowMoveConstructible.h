#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISNOTHROWMOVECONSTRUCTIBLE_H
#define FXDENGINE_CORE_INTERNAL_ISNOTHROWMOVECONSTRUCTIBLE_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/AddRValueReference.h"
#include "FXDEngine/Core/Internal/IsNoThrowConstructible.h"

namespace FXD
{
	namespace STD
	{
		// is_nothrow_move_constructible
#		define FXD_SUPPORTS_IS_NOTHROW_MOVE_CONSTRUCTIBLE 1

		template < typename T >
		struct is_nothrow_move_constructible : public FXD::STD::is_nothrow_constructible< T, typename FXD::STD::add_rvalue_reference< T >::type >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_nothrow_move_constructible_v = FXD::STD::is_nothrow_move_constructible< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISNOTHROWMOVECONSTRUCTIBLE_H