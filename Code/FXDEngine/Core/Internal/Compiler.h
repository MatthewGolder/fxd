// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_COMPILER_H
#define FXDENGINE_CORE_INTERNAL_COMPILER_H

#if !defined(COMPILER_CPP11_ENABLED) && defined(__cplusplus)
#	if defined(_MSC_VER) && (_MSC_VER >= 1600)
#		define COMPILER_CPP11_ENABLED 1
#		define IsCPP11() 1
#	elif defined(__clang__) && (__clang_major__ > 3 || (__clang_major__ == 3 && __clang_minor__ >= 3))
#		define COMPILER_CPP11_ENABLED 1
#		define IsCPP11() 1
#	else
#		define COMPILER_CPP11_ENABLED 0
#		error "CPP11 not supported"
#	endif
#endif

#if !defined(COMPILER_CPP14_ENABLED) && defined(__cplusplus)
#	if defined(_MSVC_LANG) && (_MSVC_LANG >= 201402L)
#		define COMPILER_CPP14_ENABLED 1
#		define IsCPP14() 1
#	elif defined(__clang__) && (__clang_major__ > 3 || (__clang_major__ == 3 && __clang_minor__ >= 4))
#		define COMPILER_CPP14_ENABLED 1
#		define IsCPP14() 1
#	else
#		define COMPILER_CPP14_ENABLED 0
#		define IsCPP14() 0
#	endif
#endif

#if !defined(COMPILER_CPP17_ENABLED) && defined(__cplusplus)
#	if defined(_MSVC_LANG) && (_MSVC_LANG >= 201703L)
#		define COMPILER_CPP17_ENABLED 1
#		define IsCPP17() 1
#	elif defined(__clang__) && (__clang_major__ > 3 || (__clang_major__ == 5 && __clang_minor__ >= 0))
#		define COMPILER_CPP17_ENABLED 1
#		define IsCPP17() 1
#	else
#		define COMPILER_CPP17_ENABLED 0
#		define IsCPP17() 0
#	endif
#endif

// Microsoft Visual Studio
#if defined(_MSC_VER)
#	define COMPILER_MSVC 1
#	if (_MSC_VER <= 1900)
#		define COMPILER_MSVC14 1
		// 1900 Visual Studio 2015 version 14.0
#	elif (_MSC_VER <= 1916)
#		define COMPILER_MSVC15 1
		// 1911 Visual Studio 2017 version 15.3
		// 1912 Visual Studio 2017 version 15.5
		// 1913 Visual Studio 2017 version 15.6 
		// 1914 Visual Studio 2017 version 15.7
		// 1915 Visual Studio 2017 version 15.8 
		// 1916 Visual Studio 2017 version 15.9
#	endif
#elif defined(__clang__)
#	define COMPILER_CLANG 1
#else
#	error "Unsupported"
#endif

#if !defined(FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE)
#	if defined(_MSC_VER) && (_MSC_VER >= 1500) // VS2008 or later
#		if ((defined(_HAS_TR1) && _HAS_TR1) || _MSC_VER >= 1700) // VS2012 (1700) and later has built-in type traits support.
#			define FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE 1
#		else
#			define FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE 0
#		endif
#	elif defined(__clang__)
#		define FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE 1
#	endif
#endif

#if !defined(FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED)
#	if IsCPP11()
#		define FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED 1
#else
#		define FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED 0
#	endif
#endif

#if !defined(FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED)
#	if IsCPP14()
#		define FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED 1
#	else
#		define FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED 0
#	endif
#endif

#endif //FXDENGINE_CORE_INTERNAL_COMPILER_H