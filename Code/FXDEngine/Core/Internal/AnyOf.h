// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ANYOF_H
#define FXDENGINE_CORE_INTERNAL_ANYOF_H

#include "FXDEngine/Core/Internal/AnyOfIf.h"
#include "FXDEngine/Core/Internal/EqualTo.h"

namespace FXD
{
	namespace STD
	{
		// any_of
#		define FXD_SUPPORTS_ANY_OF 1

		template < typename InputItr, typename Predicate >
		inline bool any_of(InputItr first, InputItr last, Predicate pred)
		{
			return FXD::STD::any_of_if(first, last, pred);
		}

		template < typename InputItr >
		inline bool any_of(InputItr first, InputItr last)
		{
			return FXD::STD::any_of(first, last, FXD::STD::equal_to<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ANYOF_H