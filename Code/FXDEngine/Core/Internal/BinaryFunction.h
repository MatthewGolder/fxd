#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_BINARYFUNCTION_H
#define FXDENGINE_CORE_INTERNAL_BINARYFUNCTION_H

namespace FXD
{
	namespace STD
	{
		// binary_function
#		define FXD_SUPPORTS_BINARY_FUNCTION 1

		template < typename Arg1, typename Arg2, typename Result >
		struct binary_function
		{
			using first_argument_type = Arg1;
			using second_argument_type = Arg2;
			using result_type = Result;
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_BINARYFUNCTION_H