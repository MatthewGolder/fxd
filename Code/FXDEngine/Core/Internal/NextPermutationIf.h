// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_NEXTPERMUTATIONIF_H
#define FXDENGINE_CORE_INTERNAL_NEXTPERMUTATIONIF_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/Compiler.h"
#include "FXDEngine/Core/Internal/Swap.h"
#include "FXDEngine/Core/Internal/Reverse.h"

namespace FXD
{
	namespace STD
	{
		// next_permutation_if
#		define FXD_SUPPORTS_NEXT_PERMUTATION_IF 1

		template < typename BidirItr, typename Predicate >
		inline bool next_permutation_if(BidirItr first, BidirItr last, Predicate pred)
		{
			if (first != last)
			{
				BidirItr itr1 = last;
				if (first != --itr1) // If the range has more than one item...
				{
					for (;;)
					{
						BidirItr itr1Copy = itr1;

						if (pred(*--itr1, *itr1Copy))
						{
							BidirItr itr2 = last;
							while (!pred(*itr1, *--itr2))
							{}

							FXD::STD::iter_swap(itr1, itr2);
							FXD::STD::reverse(itr1Copy, last);
							return true;
						}

						if (itr1 == first)
						{
							FXD::STD::reverse(first, last);
							break; // We are done.
						}
					}
				}
			}
			return false;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_NEXTPERMUTATIONIF_H