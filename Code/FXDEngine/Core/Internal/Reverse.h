#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REVERSE_H
#define FXDENGINE_CORE_INTERNAL_REVERSE_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/Swap.h"

namespace FXD
{
	namespace STD
	{
		// reverse
#		define FXD_SUPPORTS_REVERSE 1

		namespace Internal
		{
			template < typename BidirItr >
			inline void _reverse(BidirItr first, BidirItr last, FXD::STD::bidirectional_iterator_tag)
			{
				for (; (first != last) && (first != --last); ++first)
				{
					FXD::STD::iter_swap(first, last);
				}
			}

			template < typename RandomAccessItr >
			inline void _reverse(RandomAccessItr first, RandomAccessItr last, FXD::STD::random_access_iterator_tag)
			{
				if (first != last)
				{
					for (; first < --last; ++first)
					{
						FXD::STD::iter_swap(first, last);
					}
				}
			}
		} //namespace Internal

		template < typename BidirItr >
		inline void reverse(BidirItr first, BidirItr last)
		{
			using IC = typename FXD::STD::iterator_traits< BidirItr >::iterator_category;

			FXD::STD::Internal::_reverse(first, last, IC());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REVERSE_H