#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_COMMONTYPE_H
#define FXDENGINE_CORE_INTERNAL_COMMONTYPE_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Decay.h"
#include "FXDEngine/Core/Internal/Declval.h"

namespace FXD
{
	namespace STD
	{
		// common_type
#		define FXD_SUPPORTS_COMMON_TYPE 1

		template < typename... T >
		struct common_type;

		template < typename T >
		struct common_type< T >
		{
			using type = typename FXD::STD::decay< T >::type;
		};

		template < typename T, typename U >
		struct common_type< T, U >
		{
			using type = typename FXD::STD::decay< decltype(true ? FXD::STD::declval< T >() : FXD::STD::declval< U >()) >::type;
		};

		template < typename T, typename U, typename... V >
		struct common_type< T, U, V... >
		{
			using type = typename FXD::STD::common_type< typename FXD::STD::common_type< T, U >::type, V... >::type;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename... T>
		using common_type_t = typename FXD::STD::common_type< T... >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_COMMONTYPE_H