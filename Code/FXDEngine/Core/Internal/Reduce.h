// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REDUCE_H
#define FXDENGINE_CORE_INTERNAL_REDUCE_H

#include "FXDEngine/Core/Internal/ReduceIf.h"
#include "FXDEngine/Core/Internal/Plus.h"

namespace FXD
{
	namespace STD
	{
		// reduce
#		define FXD_SUPPORTS_REDUCE 1

		template < typename InputItr, typename T, typename Predicate >
		inline T reduce(InputItr first, InputItr last, T val, Predicate pred)
		{
			return FXD::STD::reduce_if(first, last, val, pred);
		}

		template < typename InputItr, typename T >
		inline T reduce(InputItr first, InputItr last, T val)
		{
			return FXD::STD::reduce(first, last, val, FXD::STD::plus<>());
		}

		template < typename InputItr >
		inline typename FXD::STD::iterator_traits< InputItr >::value_type reduce(InputItr first, InputItr last)
		{
			return FXD::STD::reduce(first, last, typename FXD::STD::iterator_traits< InputItr >::value_type{});
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REDUCE_H