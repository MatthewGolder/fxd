// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REMOVEIF_H
#define FXDENGINE_CORE_INTERNAL_REMOVEIF_H

#include "FXDEngine/Core/Internal/FindIf.h"
#include "FXDEngine/Core/Internal/RemoveCopyIf.h"

namespace FXD
{
	namespace STD
	{
		// remove_if
#		define FXD_SUPPORTS_REMOVE_IF 1

		template < typename ForwardItr, typename Predicate >
		inline ForwardItr remove_if(ForwardItr first, ForwardItr last, Predicate pred)
		{
			first = FXD::STD::find_if(first, last, pred);
			if (first != last)
			{
				ForwardItr itr(first);
				return FXD::STD::remove_copy_if< ForwardItr, ForwardItr, Predicate >(++itr, last, first, pred);
			}
			return first;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REMOVEIF_H