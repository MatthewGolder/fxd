#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_FINDFIRSTNOTOFIF_H
#define FXDENGINE_CORE_INTERNAL_FINDFIRSTNOTOFIF_H

#include "FXDEngine/Core/Internal/FindIf.h"

namespace FXD
{
	namespace STD
	{
		// find_first_not_of_if	
#		define FXD_SUPPORTS_FIND_FIRST_NOT_OF_IF 1

		template < typename InputItr, typename ForwardItr, typename Predicate >
		inline InputItr find_first_not_of_if(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)
		{
			using value_type = typename FXD::STD::iterator_traits< InputItr >::value_type;

			for (; first1 != last1; ++first1)
			{
				if (FXD::STD::find_if(first2, last2, [&](const value_type& val) { return pred(val, *first1); }) == last2)
				{
					break;
				}
			}
			return first1;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_FINDFIRSTNOTOFIF_H