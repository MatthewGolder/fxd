// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_INNERPRODUCT_H
#define FXDENGINE_CORE_INTERNAL_INNERPRODUCT_H

#include "FXDEngine/Core/Internal/InnerProductIf.h"
#include "FXDEngine/Core/Internal/Multiplies.h"
#include "FXDEngine/Core/Internal/Plus.h"

namespace FXD
{
	namespace STD
	{
		// inner_product
#		define FXD_SUPPORTS_INNER_PRODUCT 1

		template < typename InputItr1, typename InputItr2, typename T, typename Predicate1, typename Predicate2 >
		inline T inner_product(InputItr1 first1, InputItr1 last1, InputItr2 first2, T val, Predicate1 pred1, Predicate2 pred2)
		{
			return FXD::STD::inner_product_if(first1, last1, first2, val, pred1, pred2);
		}

		template < typename InputItr1, typename InputItr2, typename T >
		inline T inner_product(InputItr1 first1, InputItr1 last1, InputItr2 first2, T val)
		{
			return FXD::STD::inner_product(first1, last1, first2, val, FXD::STD::plus<>(), FXD::STD::multiplies<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_INNERPRODUCT_H