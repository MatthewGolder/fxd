// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ANYOFIF_H
#define FXDENGINE_CORE_INTERNAL_ANYOFIF_H

#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// any_of_if
		template < typename InputItr, typename Predicate >
		inline bool any_of_if(InputItr first, InputItr last, Predicate pred)
		{
			for (; first != last; ++first)
			{
				if (pred(*first))
				{
					return true;
				}
			}
			return false;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ANYOFIF_H