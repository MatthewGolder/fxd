#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_HASTRIVIALCONSTRUCTOR_H
#define FXDENGINE_CORE_INTERNAL_HASTRIVIALCONSTRUCTOR_H

#include "FXDEngine/Core/Types.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsPod.h"

namespace FXD
{
	namespace STD
	{
		// has_trivial_constructor
#	if !defined(FXD_SUPPORTS_HAS_TRIVIAL_CONSTRUCTOR)
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC) || defined(COMPILER_CLANG))
#		define FXD_SUPPORTS_HAS_TRIVIAL_CONSTRUCTOR 1

		template < typename T >
		struct has_trivial_constructor : public FXD::STD::integral_constant< bool, __has_trivial_constructor(T) || FXD::STD::is_pod< T >::value >
		{};

#		else
#		define FXD_SUPPORTS_HAS_TRIVIAL_CONSTRUCTOR 1

		template < typename T >
		struct has_trivial_constructor : public FXD::STD::is_pod< T >
		{};

#		endif

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool has_trivial_constructor_v = FXD::STD::has_trivial_constructor< T >::value;

#		endif
#	endif //FXD_SUPPORTS_HAS_TRIVIAL_CONSTRUCTOR

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_HASTRIVIALCONSTRUCTOR_H