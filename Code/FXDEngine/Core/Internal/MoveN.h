#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MOVEN_H
#define FXDENGINE_CORE_INTERNAL_MOVEN_H

#include "FXDEngine/Core/Internal/Move.h"
#include "FXDEngine/Container/Internal/IteratorTraits.h"

namespace FXD
{
	namespace STD
	{
		// move_n
#		define FXD_SUPPORTS_MOVE_N 1

		namespace Internal
		{
			template < typename InputItr, typename Size, typename OutputItr >
			inline OutputItr _move_n(InputItr first, Size nCount, OutputItr dst, FXD::STD::input_iterator_tag)
			{
				for (; nCount > 0; --nCount)
				{
					*dst++ = std::move(*first++);
				}
				return dst;
			}

			template < typename RandomAccessItr, typename Size, typename OutputItr >
			inline OutputItr _move_n(RandomAccessItr first, Size nCount, OutputItr dst, FXD::STD::random_access_iterator_tag)
			{
				return std::move(first, first + nCount, dst); // Take advantage of the optimizations present in the move algorithm.
			}
		} //namespace Internal

		template < typename InputItr, typename Size, typename OutputItr >
		inline OutputItr move_n(InputItr first, Size nCount, OutputItr dst)
		{
			using IC = typename FXD::STD::iterator_traits< InputItr >::iterator_category;
			return FXD::STD::Internal::_move_n(first, nCount, dst, IC());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MOVEN_H