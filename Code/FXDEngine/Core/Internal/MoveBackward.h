#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MOVEBACKWARD_H
#define FXDENGINE_CORE_INTERNAL_MOVEBACKWARD_H

#include "FXDEngine/Core/Internal/CopyMoveBackwardHelper.h"
#include "FXDEngine/Container/Internal/UnwrapIterator.h"

namespace FXD
{
	namespace STD
	{
		// move_backward
#		define FXD_SUPPORTS_MOVE_BACKWARD 1

		template < typename InputItr, typename OutputItr >
		inline OutputItr move_backward(InputItr first, InputItr last, OutputItr dst)
		{
			return FXD::STD::Internal::_move_copy_backward_unwrapper< true >(FXD::STD::unwrap_iterator(first), FXD::STD::unwrap_iterator(last), dst);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MOVEBACKWARD_H