#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_CONDITIONAL_H
#define FXDENGINE_CORE_INTERNAL_CONDITIONAL_H

namespace FXD
{
	namespace STD
	{
		// conditional
#		define FXD_SUPPORTS_CONDITIONAL 1

		template < bool B, typename T, typename F >
		struct conditional
		{
			using type = T;
		};

		template < typename T, typename F >
		struct conditional< false, T, F >
		{
			using type = F;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < bool B, typename T, typename F >
		using conditional_t = typename FXD::STD::conditional< B, T, F >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_CONDITIONAL_H