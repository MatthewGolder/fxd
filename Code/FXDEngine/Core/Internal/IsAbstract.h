#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISABSTRACT_H
#define FXDENGINE_CORE_INTERNAL_ISABSTRACT_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsObject.h"

namespace FXD
{
	namespace STD
	{
		// is_abstract
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC) || defined(COMPILER_CLANG))
#		define FXD_SUPPORTS_IS_ABSTRACT 1

		template < typename T >
		struct is_abstract : public FXD::STD::integral_constant< bool, __is_abstract(T) >
		{};

#		else
#		define FXD_SUPPORTS_IS_ABSTRACT 1

		namespace Internal
		{
			template < typename T, bool = !FXD::STD::is_object< T >::value >
			class _is_abstract
			{
				template < typename >
				static FXD::STD::Internal::yes_type test(...);

				template < typename T1 >
				static FXD::STD::Internal::no_type test(T1(*)[1]);

			public:
				static const bool value = (sizeof(test< T >(nullptr)) == sizeof(FXD::STD::Internal::yes_type));
			};

			template < typename T >
			struct _is_abstract< T, true >
			{
				static const bool value = false;
			};
		} //namespace Internal

		template < typename T >
		struct is_abstract : public FXD::STD::integral_constant< bool, FXD::STD::Internal::_is_abstract< T >::value >
		{};

#endif

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_abstract_v = FXD::STD::is_abstract< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISABSTRACT_H