#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ALIGNMENTOF_H
#define FXDENGINE_CORE_INTERNAL_ALIGNMENTOF_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// alignment_of
#		define FXD_SUPPORTS_ALIGNMENT_OF 1

		namespace Internal
		{
			template < typename T >
			struct _alignment_of
			{
				static const size_t value = FXD_ALIGN_OF(T);
			};
		} //namespace Internal

		template < typename T >
		struct alignment_of : public FXD::STD::integral_constant< size_t, FXD::STD::Internal::_alignment_of< T >::value >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR size_t alignment_of_v = FXD::STD::alignment_of< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ALIGNMENTOF_H