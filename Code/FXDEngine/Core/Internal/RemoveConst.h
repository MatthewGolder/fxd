// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REMOVECONST_H
#define FXDENGINE_CORE_INTERNAL_REMOVECONST_H

#include <cstddef>

namespace FXD
{
	namespace STD
	{
		// remove_const
#		define FXD_SUPPORTS_REMOVE_CONST 1

		template < typename T >
		struct remove_const
		{
			typedef T type;
		};

		template < typename T >
		struct remove_const< const T >
		{
			typedef T type;
		};

		template < typename T >
		struct remove_const< const T[] >
		{
			typedef T type[];
		};

		template < typename T, std::size_t SIZE >
		struct remove_const< const T[SIZE] >
		{
			typedef T type[SIZE];
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using remove_const_t = typename FXD::STD::remove_const< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REMOVECONST_H