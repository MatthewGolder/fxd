#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MIN_H
#define FXDENGINE_CORE_INTERNAL_MIN_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Less.h"
#include "FXDEngine/Core/Internal/MinElement.h"

namespace FXD
{
	namespace STD
	{
		// Min
#		define FXD_SUPPORTS_MIN 1

		template < typename T, typename Predicate >
		CONSTEXPR14 const T& Min(const T& lhs, const T& rhs, Predicate pred)
		{
			return pred(rhs, lhs) ? rhs : lhs;
		}

		template < typename T >
		CONSTEXPR14 const T& Min(const T& lhs, const T& rhs)
		{
			return (rhs < lhs) ? rhs : lhs;
		}

		template < typename T, typename Predicate >
		CONSTEXPR14 T Min(std::initializer_list< T > iList, Predicate pred)
		{
			return *FXD::STD::min_element(iList.begin(), iList.end(), pred);
		}

		template < typename T >
		CONSTEXPR14 T Min(std::initializer_list< T > iList)
		{
			return *FXD::STD::min_element(iList.begin(), iList.end(), FXD::STD::less<>());
		}

		// Min_Tuple
		template < typename T >
		CONSTEXPR14 const T& Min_Tuple(const T& lhs, const T& rhs)
		{
			return (rhs > lhs) ? lhs : rhs;
		}

		template < typename T, typename... Args >
		CONSTEXPR14 const T& Min_Tuple(const T& lhs, const T& rhs, const Args&... args)
		{
			return FXD::STD::Min_Tuple(FXD::STD::Min_Tuple(lhs, rhs), args...);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MIN_H