#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_EQUALRANGE_H
#define FXDENGINE_CORE_INTERNAL_EQUALRANGE_H

#include "FXDEngine/Core/Internal/EqualRangeIf.h"

namespace FXD
{
	namespace STD
	{
		// equal_range
#		define FXD_SUPPORTS_EQUAL_RANGE 1

		template < typename ForwardItr, typename T, typename Predicate >
		inline FXD::Core::Pair< ForwardItr, ForwardItr > equal_range(ForwardItr first, ForwardItr last, const T& val, Predicate pred)
		{
			return FXD::STD::equal_range_if(first, last, val, pred);
		}

		template < typename ForwardItr, typename T >
		inline FXD::Core::Pair< ForwardItr, ForwardItr > equal_range(ForwardItr first, ForwardItr last, const T& val)
		{
			return FXD::STD::equal_range(first, last, val, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_EQUALRANGE_H