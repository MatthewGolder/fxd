#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_FINDLASTOFIF_H
#define FXDENGINE_CORE_INTERNAL_FINDLASTOFIF_H

#include "FXDEngine/Core/Internal/FindIf.h"
#include "FXDEngine/Container/Internal/IteratorTraits.h"

namespace FXD
{
	namespace STD
	{
		// find_last_of_if
#		define FXD_SUPPORTS_FIND_LAST_OF_IF 1

		template < typename BidirectionalItr, typename ForwardItr, typename Predicate >
		inline BidirectionalItr find_last_of_if(BidirectionalItr first1, BidirectionalItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)
		{
			using value_type = typename FXD::STD::iterator_traits< BidirectionalItr >::value_type;

			if ((first1 != last1) && (first2 != last2))
			{
				BidirectionalItr itr(last1);

				while ((--itr != first1) && (FXD::STD::find_if(first2, last2, [&](const value_type& val) { return pred(val, *itr); }) == last2))
				{
					; // Do nothing
				}
				if ((itr != first1) || (FXD::STD::find_if(first2, last2, [&](const value_type& val) { return pred(val, *itr); }) != last2))
				{
					return itr;
				}
			}
			return last1;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_FINDLASTOFIF_H