#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_SHIFTHEAP_H
#define FXDENGINE_CORE_INTERNAL_SHIFTHEAP_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"

namespace FXD
{
	namespace STD
	{
		namespace Internal
		{
			// _shift_heap_up
			template < typename RandomAccessItr, typename Predicate >
			inline void _shift_heap_up(RandomAccessItr first, RandomAccessItr last, Predicate pred, typename FXD::STD::iterator_traits< RandomAccessItr >::difference_type nLen)
			{
				using value_type = typename FXD::STD::iterator_traits< RandomAccessItr >::value_type;

				if (nLen > 1)
				{
					nLen = (nLen - 2) / 2;
					RandomAccessItr temp = first + nLen;
					if (pred(*temp, *--last))
					{
						value_type top(std::move(*last));
						do
						{
							*last = std::move(*temp);
							last = temp;
							if (nLen == 0)
							{
								break;
							}
							nLen = (nLen - 1) / 2;
							temp = first + nLen;
						} while (pred(*temp, top));
						*last = std::move(top);
					}
				}
			}

			// _shift_heap_down
			template < typename RandomAccessItr, typename Predicate >
			inline void _shift_heap_down(RandomAccessItr first, RandomAccessItr /*last*/, Predicate pred, typename FXD::STD::iterator_traits< RandomAccessItr >::difference_type nLen, RandomAccessItr start)
			{
				using difference_type = typename FXD::STD::iterator_traits< RandomAccessItr >::difference_type;
				using value_type = typename FXD::STD::iterator_traits< RandomAccessItr >::value_type;

				// left-child of start is at 2 * start + 1
				// right-child of start is at 2 * start + 2
				difference_type nChild = (start - first);

				if (nLen < 2 || (nLen - 2) / 2 < nChild)
				{
					return;
				}

				nChild = 2 * nChild + 1;
				RandomAccessItr itrChild = (first + nChild);

				if ((nChild + 1) < nLen && pred(*itrChild, *(itrChild + 1)))
				{
					// right-child exists and is greater than left-child
					++itrChild;
					++nChild;
				}

				// check if we are in heap-order
				if (pred(*itrChild, *start))
				{
					// we are, start is larger than it's largest child
					return;
				}

				value_type top(std::move(*start));
				do
				{
					// we are not in heap-order, swap the parent with it's largest child
					*start = std::move(*itrChild);
					start = itrChild;

					if ((nLen - 2) / 2 < nChild)
					{
						break;
					}

					// recompute the child based off of the updated parent
					nChild = 2 * nChild + 1;
					itrChild = (first + nChild);

					if ((nChild + 1) < nLen && pred(*itrChild, *(itrChild + 1)))
					{
						// right-child exists and is greater than left-child
						++itrChild;
						++nChild;
					}

					// check if we are in heap-order
				} while (!pred(*itrChild, top));
				*start = std::move(top);
			}
		} //namespace Internal
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_SHIFTHEAP_H