#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_HASTRIVIALCOPY_H
#define FXDENGINE_CORE_INTERNAL_HASTRIVIALCOPY_H

#include "FXDEngine/Core/Types.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsPod.h"
#include "FXDEngine/Core/Internal/IsReference.h"
#include "FXDEngine/Core/Internal/IsVolatile.h"

namespace FXD
{
	namespace STD
	{
		// has_trivial_copy
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC) || defined(COMPILER_CLANG))
#		define FXD_SUPPORTS_HAS_TRIVIAL_COPY 1

		template < typename T >
		struct has_trivial_copy : public FXD::STD::integral_constant< bool, (__has_trivial_copy(T) || FXD::STD::is_pod< T >::value) && (!FXD::STD::is_volatile< T >::value && !FXD::STD::is_reference< T >::value) >
		{};

#		else
#		define FXD_SUPPORTS_HAS_TRIVIAL_COPY 1

		template < typename T >
		struct has_trivial_copy : public FXD::STD::integral_constant< bool, FXD::STD::is_pod< T >::value && !FXD::STD::is_volatile< T >::value >
		{};

#		endif

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool has_trivial_copy_v = FXD::STD::has_trivial_copy< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_HASTRIVIALCOPY_H