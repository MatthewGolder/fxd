#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_GENERATE_H
#define FXDENGINE_CORE_INTERNAL_GENERATE_H

namespace FXD
{
	namespace STD
	{
		// generate
#		define FXD_SUPPORTS_GENERATE 1

		template < typename ForwardItr, typename Generator >
		inline void generate(ForwardItr first, ForwardItr last, Generator generator)
		{
			for (; first != last; ++first)
			{
				*first = generator();
			}
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_GENERATE_H