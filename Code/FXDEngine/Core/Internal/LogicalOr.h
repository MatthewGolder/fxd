#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_LOGICALOR_H
#define FXDENGINE_CORE_INTERNAL_LOGICALOR_H

#include "FXDEngine/Core/Types.h"
#include <utility>

namespace FXD
{
	namespace STD
	{
		// logical_or
#		define FXD_SUPPORTS_LOGICAL_OR 1

		template < typename T = void >
		struct logical_or
		{
			using first_argument_type = T;
			using second_argument_type = T;
			using result_type = T;

			CONSTEXPR14 bool operator()(const T& lhs, const T& rhs) const
			{
				return (lhs || rhs);
			}
		};

		template <>
		struct logical_or< void >
		{
			using is_transparent = FXD::S32;

			template < typename LHS, typename RHS >
			constexpr auto operator()(LHS&& lhs, RHS&& rhs) const -> decltype(std::forward< LHS >(lhs) || std::forward< RHS >(rhs))
			{
				return (std::forward< LHS >(lhs) || std::forward< RHS >(rhs));
			}
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_LOGICALOR_H