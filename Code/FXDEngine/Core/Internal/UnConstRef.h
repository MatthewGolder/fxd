#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_UNCONSTREF_H
#define FXDENGINE_CORE_INTERNAL_UNCONSTREF_H

#include "FXDEngine/Core/Internal/RemoveConst.h"
#include "FXDEngine/Core/Internal/RemoveReference.h"

namespace FXD
{
	namespace STD
	{
		// add_cv
#		define FXD_SUPPORTS_UN_CVREF 1

		template < typename T >
		struct un_constref
		{
			using type = typename FXD::STD::remove_const< typename FXD::STD::remove_reference< T >::type >::type;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using un_constref_t = typename FXD::STD::un_constref< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_UNCONSTREF_H