#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISTRIVIALLYASSIGNABLE_H
#define FXDENGINE_CORE_INTERNAL_ISTRIVIALLYASSIGNABLE_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// is_trivially_assignable
#		define FXD_SUPPORTS_IS_TRIVIALLY_ASSIGNABLE 1

		template < typename T, typename U >
		struct is_trivially_assignable : FXD::STD::integral_constant< bool, __is_trivially_assignable(T, U) >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T, typename U >
		CONSTEXPR bool is_trivially_assignable_v = FXD::STD::is_trivially_assignable< T, U >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISTRIVIALLYASSIGNABLE_H