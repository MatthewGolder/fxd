#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_IDENTICAL_H
#define FXDENGINE_CORE_INTERNAL_IDENTICAL_H

#include "FXDEngine/Core/Internal/IdenticalIf.h"
#include "FXDEngine/Core/Internal/EqualTo.h"

namespace FXD
{
	namespace STD
	{
		// identical
#		define FXD_SUPPORTS_IDENTICAL 1

		template < typename InputItr1, typename InputItr2, typename Predicate >
		inline bool identical(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)
		{
			return FXD::STD::identical_if(first1, last1, first2, last2, pred);
		}

		template < typename InputItr1, typename InputItr2 >
		inline bool identical(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)
		{
			return FXD::STD::identical(first1, last1, first2, last2, FXD::STD::equal_to<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_IDENTICAL_H