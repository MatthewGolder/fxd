#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_FINDENDIF_H
#define FXDENGINE_CORE_INTERNAL_FINDENDIF_H

#include "FXDEngine/Core/Internal/Search.h"

namespace FXD
{
	namespace STD
	{
		// find_end_if
#		define FXD_SUPPORTS_FIND_END_IF 1

		template < typename InputItr, typename ForwardItr, typename Predicate >
		inline InputItr find_end_if(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)
		{
			if (first2 == last2)
			{
				return last1;
			}

			InputItr result = last1;
			while (true)
			{
				InputItr newResult = FXD::STD::search(first1, last1, first2, last2, pred);
				if (newResult == last1)
				{
					return result;
				}
				else
				{
					result = newResult;
					first1 = result;
					++first1;
				}
			}
			return result;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_FINDENDIF_H