// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_SETUNION_H
#define FXDENGINE_CORE_INTERNAL_SETUNION_H

#include "FXDEngine/Core/Internal/SetUnionIf.h"
#include "FXDEngine/Core/Internal/Less.h"

namespace FXD
{
	namespace STD
	{
		// set_union
#		define FXD_SUPPORTS_SET_UNION 1

		template < typename InputItr1, typename InputItr2, typename OutputItr, typename Predicate >
		inline OutputItr set_union(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst, Predicate pred)
		{
			return FXD::STD::set_union_if(first1, last1, first2, last2, dst, pred);
		}

		template < typename InputItr1, typename InputItr2, typename OutputItr >
		inline OutputItr set_union(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)
		{
			return FXD::STD::set_union(first1, last1, first2, last2, dst, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_SETUNION_H