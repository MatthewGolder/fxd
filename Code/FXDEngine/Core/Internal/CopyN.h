#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_COPYN_H
#define FXDENGINE_CORE_INTERNAL_COPYN_H

#include "FXDEngine/Core/Internal/Copy.h"
#include "FXDEngine/Container/Internal/IteratorTraits.h"

namespace FXD
{
	namespace STD
	{
		// copy_n
#		define FXD_SUPPORTS_COPY_N 1

		namespace Internal
		{
			template < typename InputItr, typename Size, typename OutputItr, typename IteratorTag >
			struct _copy_n
			{
				static OutputItr _impl(InputItr first, Size nCount, OutputItr dst)
				{
					for (; nCount > 0; --nCount)
					{
						*dst++ = *first++;
					}
					return dst;
				}
			};

			template < typename RandomAccessItr, typename Size, typename OutputItr >
			struct _copy_n< RandomAccessItr, Size, OutputItr, FXD::STD::random_access_iterator_tag >
			{
				static inline OutputItr _impl(RandomAccessItr first, Size nCount, OutputItr dst)
				{
					return FXD::STD::copy(first, (first + nCount), dst);
				}
			};
		} //namespace Internal

		template < typename InputItr, typename Size, typename OutputItr >
		inline OutputItr copy_n(InputItr first, Size nCount, OutputItr dst)
		{
			using IC = typename FXD::STD::iterator_traits< InputItr >::iterator_category;

			return FXD::STD::Internal::_copy_n< InputItr, Size, OutputItr, IC >::_impl(first, nCount, dst);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_COPYN_H