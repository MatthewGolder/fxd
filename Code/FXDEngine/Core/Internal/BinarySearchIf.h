// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_BINARYSEARCHIF_H
#define FXDENGINE_CORE_INTERNAL_BINARYSEARCHIF_H

#include "FXDEngine/Core/Internal/LowerBound.h"

namespace FXD
{
	namespace STD
	{
		// binary_search_if
#		define FXD_SUPPORTS_BINARY_SEARCH_IF 1

		template < typename ForwardItr, typename T, typename Predicate >
		inline bool binary_search_if(ForwardItr first, ForwardItr last, const T& val, Predicate pred)
		{
			ForwardItr itr(FXD::STD::lower_bound< ForwardItr, T >(first, last, val, pred));
			return ((itr != last) && !pred(val, *itr));
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_BINARYSEARCHIF_H