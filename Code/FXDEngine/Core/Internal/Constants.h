#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_CONSTANTS_H
#define FXDENGINE_CORE_INTERNAL_CONSTANTS_H

#include "FXDEngine/Core/Types.h"

namespace FXD
{
	namespace STD
	{
		// integral_constant
		template < typename T, T Val >
		class integral_constant
		{
		public:
			static CONSTEXPR T value = Val;
			using value_type = T;
			using type = integral_constant< T, Val >;

		public:
			CONSTEXPR operator value_type() const NOEXCEPT
			{
				return value;
			}
			CONSTEXPR value_type operator()() const NOEXCEPT
			{
				return value;
			}
		};

		// bool_constant
#	if IsCPP17()
		template < bool BVal >
		using bool_constant = FXD::STD::integral_constant< bool, BVal >;
#	endif

		// true_type
		using true_type = FXD::STD::integral_constant< bool, true >;

		// false_type
		using false_type = FXD::STD::integral_constant< bool, false >;


		namespace Internal
		{
			struct _One_then_variadic_args_t
			{};

			// false value attached to a dependent name (for static_assert)
			template< typename >
			CONSTEXPR bool _Always_false = false;

			struct Unused
			{};

			using yes_type = FXD::U8; // (sizeof(yes_type) == 1)
			struct no_type { FXD::U8 padding[8]; }; // (sizeof(no_type) != 1)


			template < bool b1, bool b2, bool b3 = false, bool b4 = false, bool b5 = false >
			struct type_or;

			template < bool b1, bool b2, bool b3, bool b4, bool b5 >
			struct type_or
			{
				static const bool value = true;
			};

			template <>
			struct type_or< false, false, false, false, false >
			{
				static const bool value = false;
			};

			template < typename T, typename = FXD::STD::Internal::Unused, typename = FXD::STD::Internal::Unused >
			struct first_type_select
			{
				typedef T type;
			};

			struct argument_sink
			{
				template < typename... Args >
				argument_sink(Args&&...)
				{}
			};
		} //namespace Internal

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_CONSTANTS_H