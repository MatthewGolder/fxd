#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_HASTRIVIALREALLOCATE_H
#define FXDENGINE_CORE_INTERNAL_HASTRIVIALREALLOCATE_H

#include "FXDEngine/Core/Types.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsPod.h"
#include "FXDEngine/Core/Internal/IsVolatile.h"

namespace FXD
{
	namespace STD
	{
		// has_trivial_relocate
#		define FXD_SUPPORTS_HAS_TRIVIAL_RELOCATE 1

		template < typename T >
		struct has_trivial_relocate : public FXD::STD::integral_constant< bool, FXD::STD::is_pod< T >::value && !FXD::STD::is_volatile< T >::value >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool has_trivial_relocate_v = FXD::STD::has_trivial_relocate< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_HASTRIVIALREALLOCATE_H