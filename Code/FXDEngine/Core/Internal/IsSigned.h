#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISSIGNED_H
#define FXDENGINE_CORE_INTERNAL_ISSIGNED_H

#include "FXDEngine/Core/Types.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/RemoveCV.h"

namespace FXD
{
	namespace STD
	{
		// is_signed
#		define FXD_SUPPORTS_IS_SIGNED 1

		namespace Internal
		{
			template < typename T >
			struct _is_signed : public FXD::STD::false_type
			{};
			template <>
			struct _is_signed< FXD::S8 > : public FXD::STD::true_type
			{};
			template <>
			struct _is_signed< FXD::S16 > : public FXD::STD::true_type
			{};
			template <>
			struct _is_signed< FXD::S32 > : public FXD::STD::true_type
			{};
			template <>
			struct _is_signed< FXD::S64 > : public FXD::STD::true_type
			{};
			template <>
			struct _is_signed< FXD::F32 > : public FXD::STD::true_type
			{};
			template <>
			struct _is_signed< FXD::F64 > : public FXD::STD::true_type
			{};
		} //namespace Internal

		template < typename T >
		struct is_signed : public FXD::STD::Internal::_is_signed< typename FXD::STD::remove_cv< T >::type >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_signed_v = FXD::STD::is_signed< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISSIGNED_H