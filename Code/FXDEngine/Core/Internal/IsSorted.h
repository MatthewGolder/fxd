#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISSORTED_H
#define FXDENGINE_CORE_INTERNAL_ISSORTED_H

#include "FXDEngine/Core/Internal/IsSortedIf.h"

namespace FXD
{
	namespace STD
	{
		// is_sorted
#		define FXD_SUPPORTS_IS_SORTED 1

		template < typename ForwardItr, typename Predicate >
		bool is_sorted(ForwardItr first, ForwardItr last, Predicate pred)
		{
			return FXD::STD::is_sorted_if(first, last, pred);
		}

		template < typename ForwardItr >
		inline bool is_sorted(ForwardItr first, ForwardItr last)
		{
			return FXD::STD::is_sorted(first, last, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISSORTED_H