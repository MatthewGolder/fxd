#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_DISABLEIF_H
#define FXDENGINE_CORE_INTERNAL_DISABLEIF_H

namespace FXD
{
	namespace STD
	{
		// disable_if
#		define FXD_SUPPORTS_DISABLE_IF 1

		template < bool Test, typename T = void >
		struct disable_if
		{};

		template < typename T >
		struct disable_if< false, T >
		{
			using type = T;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < bool B, typename T = void >
		using disable_if_t = typename FXD::STD::disable_if< B, T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_DISABLEIF_H