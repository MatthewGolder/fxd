// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REMOVECV_H
#define FXDENGINE_CORE_INTERNAL_REMOVECV_H

#include "FXDEngine/Core/Internal/RemoveConst.h"
#include "FXDEngine/Core/Internal/RemoveVolatile.h"

namespace FXD
{
	namespace STD
	{
		// remove_cv
#		define FXD_SUPPORTS_REMOVE_CV 1

		template < typename T >
		struct remove_cv
		{
			using type = typename FXD::STD::remove_volatile< typename FXD::STD::remove_const< T >::type >::type;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using remove_cv_t = typename FXD::STD::remove_cv< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REMOVECV_H