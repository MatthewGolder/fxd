#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_SAMPLE_H
#define FXDENGINE_CORE_INTERNAL_SAMPLE_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Container/Internal/Distance.h"
#include "FXDEngine/Core/Random.h"

namespace FXD
{
	namespace STD
	{
		// sample
		/*
		namespace Internal
		{
			template <class _PopulationIterator, class _SampleIterator, class _Distance, class _UniformRandomNumberGenerator>
			_SampleIterator __sample(_PopulationIterator __first, _PopulationIterator __last, _SampleIterator __output_iter, _Distance __n, _UniformRandomNumberGenerator & __g, FXD::STD::input_iterator_tag)
			{

				_Distance __k = 0;
				for (; __first != __last && __k < __n; ++__first, (void)++__k)
				{
					__output_iter[__k] = *__first;
				}
				_Distance __sz = __k;
				for (; __first != __last; ++__first, (void)++__k)
				{
					_Distance __r = FXD::STD::uniform_int_distribution<_Distance>(0, __k)(__g);
					if (__r < __sz)
					{
						__output_iter[__r] = *__first;
					}
				}
				return __output_iter + FXD::STD::Min(__n, __k);
			}

			template <class _PopulationIterator, class _SampleIterator, class _Distance, class _UniformRandomNumberGenerator>
			_SampleIterator __sample(_PopulationIterator __first, _PopulationIterator __last, _SampleIterator __output_iter, _Distance __n, _UniformRandomNumberGenerator& __g, FXD::STD::forward_iterator_tag)
			{
				_Distance __unsampled_sz = FXD::STD::distance(__first, __last);
				for (__n = FXD::STD::Min(__n, __unsampled_sz); __n != 0; ++__first)
				{
					_Distance __r = FXD::STD::uniform_int_distribution<_Distance>(0, --__unsampled_sz)(__g);
					if (__r < __n)
					{
						*__output_iter++ = *__first;
						--__n;
					}
				}
				return __output_iter;
			}

			template <class _PopulationIterator, class _SampleIterator, class _Distance, class _UniformRandomNumberGenerator>
			_SampleIterator __sample(_PopulationIterator __first, _PopulationIterator __last, _SampleIterator __output_iter, _Distance __n, _UniformRandomNumberGenerator& __g)
			{
				typedef typename FXD::STD::iterator_traits<_PopulationIterator>::iterator_category _PopCategory;
				typedef typename FXD::STD::iterator_traits<_PopulationIterator>::difference_type _Difference;
				static_assert(__is_forward_iterator<_PopulationIterator>::value || __is_random_access_iterator<_SampleIterator>::value, "SampleIterator must meet the requirements of RandomAccessIterator");
				typedef typename FXD::STD::common_type<_Distance, _Difference>::type _CommonType;

				_LIBCPP_ASSERT(__n >= 0, "N must be a positive number.");

				return FXD::Internal::sample(__first, __last, __output_iter, _CommonType(__n), __g, _PopCategory());
			}
		} // namespace Internal

		template < typename _PopulationIterator, typename _SampleIterator, typename _Distance, typename _UniformRandomNumberGenerator >
		inline _SampleIterator sample(_PopulationIterator __first, _PopulationIterator __last, _SampleIterator __output_iter, _Distance __n, _UniformRandomNumberGenerator&& __g)
		{
			return FXD::Internal::sample(__first, __last, __output_iter, __n, __g);
		}
		*/

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_SAMPLE_H