#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_IDENTITY_H
#define FXDENGINE_CORE_INTERNAL_IDENTITY_H

namespace FXD
{
	namespace STD
	{
		// identity
#		define FXD_SUPPORTS_IDENTITY 1

		template < typename T >
		struct identity
		{
			using type = T;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using identity_t = typename FXD::STD::identity< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_IDENTITY_H