#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_PARTITIONCOPY_H
#define FXDENGINE_CORE_INTERNAL_PARTITIONCOPY_H

#include "FXDEngine/Core/Pair.h"

namespace FXD
{
	namespace STD
	{
		// partition_copy
#		define FXD_SUPPORTS_PARTITION_COPY 1

		template < typename InputItr, typename OutputItr1, typename OutputItr2, typename Predicate >
		inline Core::Pair< OutputItr1, OutputItr2 > partition_copy(InputItr first, InputItr last, OutputItr1 dstTrue, OutputItr2 dstFalse, Predicate pred)
		{
			while (first != last)
			{
				if (pred(*first))
				{
					*dstTrue = *first;
					++dstTrue;
				}
				else
				{
					*dstFalse = *first;
					++dstFalse;
				}
				++first;
			}
			return Core::Pair< OutputItr1, OutputItr2 >(dstTrue, dstFalse);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_PARTITIONCOPY_H