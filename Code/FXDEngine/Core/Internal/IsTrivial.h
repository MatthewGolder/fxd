#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISTRIVIAL_H
#define FXDENGINE_CORE_INTERNAL_ISTRIVIAL_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsTriviallyCopyable.h"
#include "FXDEngine/Core/Internal/IsTriviallyDefaultConstructible.h"

namespace FXD
{
	namespace STD
	{
		// is_trivial
#		define FXD_SUPPORTS_IS_TRIVIAL 1

		template < typename T >
		struct is_trivial : public FXD::STD::integral_constant< bool, FXD::STD::is_trivially_copyable< T >::value && FXD::STD::is_trivially_default_constructible< T >::value >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_trivial_v = FXD::STD::is_trivial< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISTRIVIAL_H