#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISTRIVIALLYCOPYCONSTRUCTIBLE_H
#define FXDENGINE_CORE_INTERNAL_ISTRIVIALLYCOPYCONSTRUCTIBLE_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/AddConst.h"
#include "FXDEngine/Core/Internal/AddLValueReference.h"
#include "FXDEngine/Core/Internal/IsTriviallyConstructible.h"

namespace FXD
{
	namespace STD
	{
		// is_trivially_copy_constructible
#		define FXD_SUPPORTS_IS_TRIVIALLY_COPY_CONSTRUCTIBLE 1

		template < typename T >
		struct is_trivially_copy_constructible : public FXD::STD::is_trivially_constructible< T, typename FXD::STD::add_lvalue_reference< typename FXD::STD::add_const< T >::type >::type >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_trivially_copy_constructible_v = FXD::STD::is_trivially_copy_constructible< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISTRIVIALLYCOPYCONSTRUCTIBLE_H