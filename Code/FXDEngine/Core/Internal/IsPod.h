#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISPOD_H
#define FXDENGINE_CORE_INTERNAL_ISPOD_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsScalar.h"
#include "FXDEngine/Core/Internal/IsVoid.h"
#include "FXDEngine/Core/Internal/RemoveAllExtents.h"
#include <cstddef>

namespace FXD
{
	namespace STD
	{
		// is_pod
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC) || defined(COMPILER_CLANG))
#		define FXD_SUPPORTS_IS_POD 1

		template < typename T >
		struct is_pod : public FXD::STD::integral_constant< bool, __is_pod(T) >
		{};

#		else
#		define FXD_SUPPORTS_IS_POD 0

		template < typename T >
		struct is_pod : public FXD::STD::integral_constant< bool, FXD::STD::is_void< T >::value || FXD::STD::is_scalar< typename FXD::STD::remove_all_extents< T >::type >::value >
		{};

#		endif

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_pod_v = FXD::STD::is_pod< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISPOD_H