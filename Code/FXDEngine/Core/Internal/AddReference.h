#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ADDREFERENCE_H
#define FXDENGINE_CORE_INTERNAL_ADDREFERENCE_H

namespace FXD
{
	namespace STD
	{
		// add_reference
#		define FXD_SUPPORTS_ADD_REFERENCE 1

		namespace Internal
		{
			template < typename T >
			struct _add_reference
			{
				using type = T & ;
			};

			template < typename T >
			struct _add_reference< T& >
			{
				using type = T & ;
			};

			template <>
			struct _add_reference< void >
			{
				using type = void;
			};
		} //namespace Internal

		template < typename T >
		struct add_reference
		{
			using type = typename FXD::STD::Internal::_add_reference< T >::type;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using add_reference_t = typename FXD::STD::add_reference< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ADDREFERENCE_H