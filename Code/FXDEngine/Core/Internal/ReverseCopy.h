#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REVERSECOPY_H
#define FXDENGINE_CORE_INTERNAL_REVERSECOPY_H

namespace FXD
{
	namespace STD
	{
		// reverse_copy
#		define FXD_SUPPORTS_REVERSE_COPY 1

		template < typename BidirItr, typename OutputItr >
		inline OutputItr reverse_copy(BidirItr first, BidirItr last, OutputItr dst)
		{
			for (; first != last; ++dst)
			{
				(*dst) = (*--last);
			}
			return dst;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REVERSECOPY_H