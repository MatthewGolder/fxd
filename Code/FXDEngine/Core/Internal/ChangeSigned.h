#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_CHANGESIGNED_H
#define FXDENGINE_CORE_INTERNAL_CHANGESIGNED_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsAnyOf.h"
#include "FXDEngine/Core/Internal/IsEnum.h"
#include "FXDEngine/Core/Internal/IsIntegral.h"
#include "FXDEngine/Core/Internal/IsSame.h"
#include "FXDEngine/Core/Internal/RemoveCV.h"

namespace FXD
{
	namespace STD
	{
		// ChangeSign
		namespace Internal
		{
			template < typename T >
			using _is_nonbool_integral = FXD::STD::integral_constant< bool, FXD::STD::is_integral< T >::value && !FXD::STD::is_same< typename FXD::STD::remove_cv< T >::type, bool >::value >;

			template < typename T >
			struct _change_sign
			{
				CTC_ASSERT((FXD::STD::Internal::_is_nonbool_integral< T >::value || FXD::STD::is_enum< T >::value), "Core: make_signed<>/make_unsigned<> require that T shall be a (possibly cv-qualified) integral type or enumeration but not a bool type.");

				using _Signed =
					typename FXD::STD::conditional< FXD::STD::is_any_of< T, long, unsigned long >::value, long,
					typename FXD::STD::conditional< sizeof(T) == 1, FXD::S8,
					typename FXD::STD::conditional< sizeof(T) == 2, FXD::S16,
					typename FXD::STD::conditional< sizeof(T) == 4, FXD::S32,
					long long
					>::type >::type >::type >::type;

				using _Unsigned =
					typename FXD::STD::conditional< FXD::STD::is_any_of< T, long, unsigned long >::value, unsigned long,
					typename FXD::STD::conditional< sizeof(T) == 1, FXD::U8,
					typename FXD::STD::conditional< sizeof(T) == 2, FXD::U16,
					typename FXD::STD::conditional< sizeof(T) == 4, FXD::U32,
					unsigned long long
					>::type >::type >::type >::type;
			};

			template < typename T >
			struct _change_sign< const T >
			{
				using _Signed = const typename FXD::STD::Internal::_change_sign< T >::_Signed;
				using _Unsigned = const typename FXD::STD::Internal::_change_sign< T >::_Unsigned;
			};

			template < typename T >
			struct _change_sign< volatile T >
			{
				using _Signed = volatile typename FXD::STD::Internal::_change_sign< T >::_Signed;
				using _Unsigned = volatile typename FXD::STD::Internal::_change_sign< T >::_Unsigned;
			};

			template < typename T >
			struct _change_sign< const volatile T >
			{
				using _Signed = const volatile typename FXD::STD::Internal::_change_sign< T >::_Signed;
				using _Unsigned = const volatile typename FXD::STD::Internal::_change_sign< T >::_Unsigned;
			};
		} //namespace Internal

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_CHANGESIGNED_H