#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REMOVEPOINTER_H
#define FXDENGINE_CORE_INTERNAL_REMOVEPOINTER_H

namespace FXD
{
	namespace STD
	{
		// remove_pointer
#		define FXD_SUPPORTS_REMOVE_POINTER 1

		template < typename T >
		struct remove_pointer
		{
			using type = T;
		};

		template < typename T >
		struct remove_pointer< T* >
		{
			using type = T;
		};

		template < typename T >
		struct remove_pointer< T* const >
		{
			using type = T;
		};

		template < typename T >
		struct remove_pointer< T* volatile >
		{
			using type = T;
		};

		template < typename T >
		struct remove_pointer< T* const volatile >
		{
			using type = T;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using remove_pointer_t = typename FXD::STD::remove_pointer< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REMOVEPOINTER_H