#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISLVALUEREFERENCE_H
#define FXDENGINE_CORE_INTERNAL_ISLVALUEREFERENCE_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Constants.h"

namespace FXD
{
	namespace STD
	{
		// is_lvalue_reference
#		define FXD_SUPPORTS_IS_LVALUE_REFERENCE 1

		template < typename T >
		struct is_lvalue_reference : public FXD::STD::false_type
		{};

		template < typename T >
		struct is_lvalue_reference< T& > : public FXD::STD::true_type
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_lvalue_reference_v = FXD::STD::is_lvalue_reference< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISLVALUEREFERENCE_H