// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_BINARYSEARCHI_H
#define FXDENGINE_CORE_INTERNAL_BINARYSEARCHI_H

#include "FXDEngine/Core/Internal/BinarySearchIIf.h"
#include "FXDEngine/Core/Internal/Less.h"

namespace FXD
{
	namespace STD
	{
		// binary_search_i
#		define FXD_SUPPORTS_BINARY_SEARCH_I 1

		template < typename ForwardItr, typename T, typename Predicate >
		inline ForwardItr binary_search_i(ForwardItr first, ForwardItr last, const T& val, Predicate pred)
		{
			return FXD::STD::binary_search_i_if(first, last, val, pred);
		}

		template < typename ForwardItr, typename T >
		inline ForwardItr binary_search_i(ForwardItr first, ForwardItr last, const T& val)
		{
			return FXD::STD::binary_search_i(first, last, val, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_BINARYSEARCHI_H