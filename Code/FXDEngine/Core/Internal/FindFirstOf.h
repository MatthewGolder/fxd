#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_FINDFIRSTOF_H
#define FXDENGINE_CORE_INTERNAL_FINDFIRSTOF_H

#include "FXDEngine/Core/Internal/FindFirstOfIf.h"
#include "FXDEngine/Core/Internal/EqualTo.h"

namespace FXD
{
	namespace STD
	{
		// find_first_of
#		define FXD_SUPPORTS_FIND_FIRST_OF 1

		template < typename InputItr, typename ForwardItr, typename Predicate >
		InputItr find_first_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)
		{
			return FXD::STD::find_first_of_if(first1, last1, first2, last2, pred);
		}

		template < typename InputItr, typename ForwardItr >
		InputItr find_first_of(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)
		{
			return FXD::STD::find_first_of(first1, last1, first2, last2, FXD::STD::equal_to<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_FINDFIRSTOF_H