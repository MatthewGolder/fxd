#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISSWAPPABLE_H
#define FXDENGINE_CORE_INTERNAL_ISSWAPPABLE_H

#include "FXDEngine/Core/Internal/SwapHelper.h"

namespace FXD
{
	namespace STD
	{
		// is_swappable
#		define FXD_SUPPORTS_IS_SWAPPABLE 1

		template < typename T >
		struct is_swappable : FXD::STD::Internal::_is_swappable< T >::type
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_swappable_v = FXD::STD::is_swappable< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISSWAPPABLE_H