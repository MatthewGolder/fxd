#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISTRIVIALLYDESTRUCTIBLE_H
#define FXDENGINE_CORE_INTERNAL_ISTRIVIALLYDESTRUCTIBLE_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/IsDestructible.h"
#include "FXDEngine/Core/Internal/IsScalar.h"
#include "FXDEngine/Core/Internal/RemoveAllExtents.h"

namespace FXD
{
	namespace STD
	{
		// is_trivially_destructible
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC) || defined(COMPILER_CLANG))

#		define FXD_SUPPORTS_IS_TRIVIALLY_DESTRUCTIBLE 1

		template < typename T >
		struct  is_trivially_destructible : public FXD::STD::integral_constant< bool, FXD::STD::is_destructible< T >::value && __has_trivial_destructor(T) >
		{};

		//template < typename T >
		//struct is_trivially_destructible : FXD::STD::integral_constant< bool, __is_trivially_destructible(T) >
		//{};

#		else
#		define FXD_SUPPORTS_IS_TRIVIALLY_DESTRUCTIBLE 0

		namespace Internal
		{
			template < typename T >
			struct _is_trivially_destructible : public FXD::STD::integral_constant< bool, FXD::STD::is_scalar< T >::value || FXD::STD::is_reference< T >::value >
			{};
		} //namespace Internal

		template < typename T >
		struct is_trivially_destructible : public FXD::STD::Internal::_is_trivially_destructible< typename FXD::STD::remove_all_extents< T >::type >
		{};

		template < typename T >
		struct is_trivially_destructible< T[] > : public FXD::STD::false_type
		{};

#		endif

		template <>
		struct is_trivially_destructible< void > : public FXD::STD::false_type
		{};

		template <>
		struct is_trivially_destructible< void const > : public FXD::STD::false_type
		{};

		template <>
		struct is_trivially_destructible< void volatile > : public FXD::STD::false_type
		{};

		template <>
		struct is_trivially_destructible< void const volatile > : public FXD::STD::false_type
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_trivially_destructible_v = FXD::STD::is_trivially_destructible< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISTRIVIALLYDESTRUCTIBLE_H