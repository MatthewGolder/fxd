#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_EXCHANGE_H
#define FXDENGINE_CORE_INTERNAL_EXCHANGE_H

namespace FXD
{
	namespace STD
	{
		// exchange
#		define FXD_SUPPORTS_EXCHANGE 1

		template < typename T, typename U = T >
		inline T exchange(T& val, U&& newVal)
		{
			T oldVal = std::move(val);
			val = std::forward< U >(newVal);
			return oldVal;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_EXCHANGE_H