// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_TRANSFORMREDUCE_H
#define FXDENGINE_CORE_INTERNAL_TRANSFORMREDUCE_H

#include "FXDEngine/Core/Internal/TransformReduceIf.h"
#include "FXDEngine/Core/Internal/Multiplies.h"
#include "FXDEngine/Core/Internal/Plus.h"

namespace FXD
{
	namespace STD
	{
		// transform_reduce
#		define FXD_SUPPORTS_TRANSFORM_REDUCE 1

		template < typename InputItr, typename T, typename Predicate1, typename Predicate2 >
		inline T transform_reduce(InputItr first, InputItr last, T val, Predicate1 pred1, Predicate2 pred2)
		{
			return FXD::STD::transform_reduce_if(first, last, val, pred1, pred2);
		}

		template < typename InputItr1, typename InputItr2, typename T, typename Predicate1, typename Predicate2 >
		inline T transform_reduce(InputItr1 first1, InputItr1 last, InputItr2 first2, T val, Predicate1 pred1, Predicate2 pred2)
		{
			return FXD::STD::transform_reduce_if(first1, last, first2, val, pred1, pred2);
		}

		template < typename InputItr1, typename InputItr2, typename T >
		inline T transform_reduce(InputItr1 first1, InputItr1 last1, InputItr2 first2, T val)
		{
			return FXD::STD::transform_reduce(first1, last1, first2, std::move(val), FXD::STD::plus<>(), FXD::STD::multiplies<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_TRANSFORMREDUCE_H