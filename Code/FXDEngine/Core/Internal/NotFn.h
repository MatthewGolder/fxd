#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_NOTFN_H
#define FXDENGINE_CORE_INTERNAL_NOTFN_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Decay.h"
#include "FXDEngine/Core/Internal/Invoke.h"
#include <utility>

namespace FXD
{
	namespace STD
	{
		// not_fn
#		define FXD_SUPPORTS_NOT_FN 1

		namespace Internal
		{
			template < typename F >
			struct _not_fn_t
			{
				F m_f;

				template < typename... Args >
				inline auto operator()(Args&&... args) & NOEXCEPT(NOEXCEPT(!FXD::invoke(m_f, std::forward< Args >(args)...))) -> decltype(!FXD::invoke(m_f, std::forward< Args >(args)...))
				{
					return !FXD::invoke(m_f, std::forward< Args >(args)...);
				}

				template < typename... Args >
				inline auto operator()(Args&&... args) const& NOEXCEPT(NOEXCEPT(!FXD::invoke(m_f, std::forward< Args >(args)...))) -> decltype(!FXD::invoke(m_f, std::forward< Args >(args)...))
				{
					return !FXD::invoke(m_f, std::forward< Args >(args)...);
				}

				template < typename... Args >
				inline auto operator()(Args&&... args) && NOEXCEPT(NOEXCEPT(!FXD::invoke(std::move(m_f), std::forward< Args >(args)...))) -> decltype(!FXD::invoke(std::move(m_f), std::forward< Args >(args)...))
				{
					return !FXD::invoke(std::move(m_f), std::forward< Args >(args)...);
				}

				template < typename... Args >
				inline auto operator()(Args&&... args) const&& NOEXCEPT(NOEXCEPT(!FXD::invoke(std::move(m_f), std::forward< Args >(args)...))) -> decltype(!FXD::invoke(std::move(m_f), std::forward< Args >(args)...))
				{
					return !FXD::invoke(std::move(m_f), std::forward< Args >(args)...);
				}
			};
		}

		template < typename F >
		inline FXD::STD::Internal::_not_fn_t< typename FXD::STD::decay< F >::type > not_fn(F&& f)
		{
			return { std::forward< F >(f) };
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_NOTFN_H