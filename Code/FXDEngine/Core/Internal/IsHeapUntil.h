#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISHEAPUNTIL_H
#define FXDENGINE_CORE_INTERNAL_ISHEAPUNTIL_H

#include "FXDEngine/Core/Internal/IsHeapUntilIf.h"
#include "FXDEngine/Core/Internal/Less.h"

namespace FXD
{
	namespace STD
	{
		// is_heap_until
#		define FXD_SUPPORTS_IS_HEAP_UNTIL 1

		template < typename RandomAccessItr, typename Predicate >
		inline RandomAccessItr is_heap_until(RandomAccessItr first, RandomAccessItr last, Predicate pred)
		{
			return FXD::STD::is_heap_until_if(first, last, pred);
		}

		template < typename RandomAccessItr >
		inline RandomAccessItr is_heap_until(RandomAccessItr first, RandomAccessItr last)
		{
			return FXD::STD::is_heap_until(first, last, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISHEAPUNTIL_H