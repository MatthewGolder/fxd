// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ACCUMULATEIF_H
#define FXDENGINE_CORE_INTERNAL_ACCUMULATEIF_H

namespace FXD
{
	namespace STD
	{
		// accumulate_if
		template < typename InputItr, typename T, typename Predicate >
		inline T accumulate_if(InputItr first, InputItr last, T val, Predicate pred)
		{
			for (; first != last; ++first)
			{
				val = pred(val, *first);
			}
			return val;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ACCUMULATEIF_H