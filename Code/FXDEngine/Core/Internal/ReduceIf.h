// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REDUCEIF_H
#define FXDENGINE_CORE_INTERNAL_REDUCEIF_H

namespace FXD
{
	namespace STD
	{
		// reduce_if
#		define FXD_SUPPORTS_REDUCE_IF 1

		template < typename InputItr, typename T, typename Predicate >
		inline T reduce_if(InputItr first, InputItr last, T val, Predicate pred)
		{
			for (; first != last; ++first)
			{
				val = pred(val, *first);
			}
			return val;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REDUCEIF_H