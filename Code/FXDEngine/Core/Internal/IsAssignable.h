#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISASSIGNABLE_H
#define FXDENGINE_CORE_INTERNAL_ISASSIGNABLE_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Declval.h"

namespace FXD
{
	namespace STD
	{
		// is_assignable
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC) || defined(COMPILER_CLANG))
#		define FXD_SUPPORTS_IS_ASSIGNABLE 1

		template < typename F, typename T >
		struct is_assignable : public FXD::STD::integral_constant< bool, __is_assignable(F, T) >
		{};

#		else
#		define FXD_SUPPORTS_IS_ASSIGNABLE 1

		namespace Internal
		{
			template < typename T, typename U >
			struct _is_assignable
			{
				template < typename, typename >
				static FXD::STD::Internal::no_type _is(...);

				template < typename T1, typename U1 >
				static decltype(FXD::STD::declval< T1 >() = FXD::STD::declval< U1 >(), FXD::STD::Internal::yes_type()) _is(FXD::S32);

				static const bool value = (sizeof(_is< T, U >(0)) == sizeof(FXD::STD::Internal::yes_type));
			};
		} //namespace Internal

		template < typename F, typename T >
		struct is_assignable : public FXD::STD::integral_constant< bool, FXD::STD::Internal::_is_assignable< F, T >::value >
		{};

#		endif

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename F, typename T >
		CONSTEXPR bool is_assignable_v = FXD::STD::is_assignable< F, T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISASSIGNABLE_H