// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_TRANSFORMEXCLUSIVESCAN_H
#define FXDENGINE_CORE_INTERNAL_TRANSFORMEXCLUSIVESCAN_H

namespace FXD
{
	namespace STD
	{
		// transform_exclusive_scan
#		define FXD_SUPPORTS_TRANSFORM_EXCLUSIVE_SCAN 1

		template < typename InputItr, typename OutputItr, typename T, typename Predicate1, typename Predicate2 >
		inline OutputItr transform_exclusive_scan(InputItr first, InputItr last, OutputItr dst, T val, Predicate1 pred1, Predicate2 pred2)
		{
			if (first != last)
			{
				T temp = val;
				do
				{
					val = pred1(val, pred2(*first));
					*dst = temp;
					temp = val;
					++dst;
				} while (++first != last);
			}
			return dst;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_TRANSFORMEXCLUSIVESCAN_H