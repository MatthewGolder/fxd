#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISNOTHROWINVOCABLE_H
#define FXDENGINE_CORE_INTERNAL_ISNOTHROWINVOCABLE_H

#include "FXDEngine/Core/Internal/InvokeResult.h"

namespace FXD
{
	namespace STD
	{
		// is_nothrow_invocable
#		define FXD_SUPPORTS_IS_NO_THROW_INVOCABLE 1

		template < typename C, typename... Args >
		struct is_nothrow_invocable : FXD::STD::Internal::_invoke_traits< void, C, Args... >::is_nothrow_invocable
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename C, typename... Args >
		CONSTEXPR bool is_nothrow_invocable_v = FXD::STD::is_nothrow_invocable< C, Args... >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISNOTHROWINVOCABLE_H