// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_DEFAULTSEARCHER_H
#define FXDENGINE_CORE_INTERNAL_DEFAULTSEARCHER_H

#include "FXDEngine/Core/Internal/EqualTo.h"
#include "FXDEngine/Core/Internal/Search.h"

namespace FXD
{
	namespace STD
	{
		// default_searcher
#		define FXD_SUPPORTS_DEEFAULT_SEARCHER 1

		template < typename ForwardItr, typename Predicate = FXD::STD::equal_to<> >
		class default_searcher
		{
		public:
			default_searcher(ForwardItr first, ForwardItr last, Predicate pred = Predicate())
				: m_first(first)
				, m_last(last)
				, m_pred(pred)
			{}

			template < typename ForwardItr2 >
			ForwardItr2 operator() (ForwardItr2 first, ForwardItr2 last) const
			{
				return FXD::STD::search(first, last, m_first, m_last, m_pred);
			}

		private:
			ForwardItr m_first;
			ForwardItr m_last;
			Predicate m_pred;
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_DEFAULTSEARCHER_H