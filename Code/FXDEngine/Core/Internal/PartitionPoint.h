// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNALPARTITIONPOINT_H
#define FXDENGINE_CORE_INTERNALPARTITIONPOINT_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Container/Internal/Advance.h"

namespace FXD
{
	namespace STD
	{
		// partition_point
#		define FXD_SUPPORTS_PARTITION_POINT 1

		template < typename ForwardItr, typename Predicate >
		inline ForwardItr partition_point(ForwardItr first, ForwardItr last, Predicate pred)
		{
			using difference_type = typename FXD::STD::iterator_traits< ForwardItr >::difference_type;

			difference_type nLen = FXD::STD::distance(first, last);
			while (nLen > 0)
			{
				difference_type nHalf = nLen >> 1;
				ForwardItr itr = first;
				FXD::STD::advance(itr, nHalf);
				if (pred(*itr))
				{
					first = itr;
					++first;
					nLen = nLen - nHalf - 1;
				}
				else
				{
					nLen = nHalf;
				}
			}
			return first;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNALPARTITIONPOINT_H