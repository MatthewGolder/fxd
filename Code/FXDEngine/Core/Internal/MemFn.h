// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MEMFN_H
#define FXDENGINE_CORE_INTERNAL_MEMFN_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/InvokeHelper.h"
#include "FXDEngine/Core/Internal/WeakTypes.h"

namespace FXD
{
	namespace STD
	{
		// mem_fn
#		define FXD_SUPPORTS_MEM_FN 1

		namespace Internal
		{
			template < typename Memptr >
			class _mem_fn_t : public FXD::STD::Internal::_weak_types< Memptr >::type
			{
			private:
				Memptr m_memPtr;

			public:
				EXPLICIT _mem_fn_t(Memptr val) NOEXCEPT
					: m_memPtr(val)
				{}

				template < typename... Args >
				auto operator()(Args&&... args) const NOEXCEPT_COND(NOEXCEPT_OPER(FXD::invoke(m_memPtr, std::forward< Args >(args)...)))-> decltype(FXD::invoke(m_memPtr, std::forward< Args >(args)...))
				{
					return (FXD::invoke(m_memPtr, std::forward< Args >(args)...));
				}

			};
		} //namespace Internal

		template < typename T, typename C >
		inline FXD::STD::Internal::_mem_fn_t< T C::*> mem_fn(T C::* val) NOEXCEPT
		{
			return (FXD::STD::Internal::_mem_fn_t< T C ::*>(val));
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MEMFN_H