#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MODULUS_H
#define FXDENGINE_CORE_INTERNAL_MODULUS_H

#include "FXDEngine/Core/Types.h"
#include <utility>

namespace FXD
{
	namespace STD
	{
		// modulus
#		define FXD_SUPPORTS_MODULUS 1

		template < typename T = void >
		struct modulus
		{
			using first_argument_type = T;
			using second_argument_type = T;
			using result_type = T;

			CONSTEXPR14 T operator()(const T& lhs, const T& rhs)const
			{
				return lhs % rhs;
			}
		};

		template <>
		struct modulus< void >
		{
			using is_transparent = FXD::S32;

			template < typename LHS, typename RHS >
			CONSTEXPR14 auto operator()(LHS&& lhs, RHS&& rhs)const->decltype(std::forward< LHS >(lhs) % std::forward< RHS >(rhs))
			{
				return std::forward< LHS >(lhs) % std::forward< RHS >(rhs);
			}
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MODULUS_H