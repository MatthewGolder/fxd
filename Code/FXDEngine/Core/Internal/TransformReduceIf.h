// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_TRANSFORMREDUCEIF_H
#define FXDENGINE_CORE_INTERNAL_TRANSFORMREDUCEIF_H

namespace FXD
{
	namespace STD
	{
		// transform_reduce_if
#		define FXD_SUPPORTS_TRANSFORM_REDUCE_IF 1

		template < typename InputItr, typename T, typename Predicate1, typename Predicate2 >
		inline T transform_reduce_if(InputItr first, InputItr last, T val, Predicate1 pred1, Predicate2 pred2)
		{
			for (; first != last; ++first)
			{
				val = pred1(val, pred2(*first));
			}
			return val;
		}

		template < typename InputItr1, typename InputItr2, typename T, typename Predicate1, typename Predicate2 >
		inline T transform_reduce_if(InputItr1 first1, InputItr1 last, InputItr2 first2, T val, Predicate1 pred1, Predicate2 pred2)
		{
			for (; first1 != last; ++first1, (void) ++first2)
			{
				val = pred1(val, pred2(*first1, *first2));
			}
			return val;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_TRANSFORMREDUCEIF_H