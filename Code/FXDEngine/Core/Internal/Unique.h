#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_UNIQUE_H
#define FXDENGINE_CORE_INTERNAL_UNIQUE_H

#include "FXDEngine/Core/Internal/UniqueIf.h"
#include "FXDEngine/Core/Internal/EqualTo.h"

namespace FXD
{
	namespace STD
	{
		// unique
#		define FXD_SUPPORTS_UNIQUE 1

		template < typename ForwardItr, typename Predicate >
		inline ForwardItr unique(ForwardItr first, ForwardItr last, Predicate pred)
		{
			return FXD::STD::unique_if(first, last, pred);
		}

		template < typename ForwardItr >
		inline ForwardItr unique(ForwardItr first, ForwardItr last)
		{
			return FXD::STD::unique(first, last, FXD::STD::equal_to<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_UNIQUE_H