#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISARRAYOFUNKNOWNBOUNDS_H
#define FXDENGINE_CORE_INTERNAL_ISARRAYOFUNKNOWNBOUNDS_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsArray.h"
#include "FXDEngine/Core/Internal/Extent.h"

namespace FXD
{
	namespace STD
	{
		// is_array_of_unknown_bounds
#		define FXD_SUPPORTS_IS_ARRAY_OF_UNKNOWN_BOUNDS 1

		template < typename T >
		struct is_array_of_unknown_bounds : public FXD::STD::integral_constant< bool, FXD::STD::is_array< T >::value && (FXD::STD::extent< T >::value == 0) >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR size_t is_array_of_unknown_bounds_v = is_array_of_unknown_bounds< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISARRAYOFUNKNOWNBOUNDS_H