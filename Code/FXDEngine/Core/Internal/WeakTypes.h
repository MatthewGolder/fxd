// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_WEAKTYPES_H
#define FXDENGINE_CORE_INTERNAL_WEAKTYPES_H

#include "FXDEngine/Core/Internal/Compiler.h"
#include "FXDEngine/Core/Internal/Conditional.h"
#include "FXDEngine/Core/Internal/IsFunction.h"
#include "FXDEngine/Core/Internal/IsMemberFunctionPointer.h"
#include "FXDEngine/Core/Internal/RemoveCV.h"
#include "FXDEngine/Core/Internal/RemovePointer.h"
#include "FXDEngine/Core/Internal/Void_t.h"

namespace FXD
{
	namespace STD
	{
		namespace Internal
		{
			template < typename T, typename = void >
			struct _weak_result_type
			{};

			template < typename T >
			struct _weak_result_type< T, FXD::STD::void_t< typename T::result_type > >
			{};

			template < typename T, typename = void >
			struct _weak_argument_type : FXD::STD::Internal::_weak_result_type< T >
			{};

			template < typename T >
			struct _weak_argument_type< T, FXD::STD::void_t< typename T::argument_type > > : FXD::STD::Internal::_weak_result_type< T >
			{};

			template < typename T, typename = void >
			struct _weak_binary_args : FXD::STD::Internal::_weak_argument_type< T >
			{};

			template < typename T >
			struct _weak_binary_args< T, FXD::STD::void_t< typename T::first_argument_type, typename T::second_argument_type > > : FXD::STD::Internal::_weak_argument_type< T >
			{};

			template < typename T >
			struct _weak_types
			{
				using is_f_or_pf = FXD::STD::is_function< typename FXD::STD::remove_pointer< T >::type >;
				using is_pmf = FXD::STD::is_member_function_pointer< typename FXD::STD::remove_cv< T >::type >;
				using type = FXD::STD::conditional_t< is_f_or_pf::value, is_f_or_pf, FXD::STD::conditional_t< is_pmf::value, is_pmf, FXD::STD::Internal::_weak_binary_args< T > > >;
			};
		} //namespace Internal
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_WEAKTYPES_H