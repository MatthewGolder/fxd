#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_IDENTICALIF_H
#define FXDENGINE_CORE_INTERNAL_IDENTICALIF_H

namespace FXD
{
	namespace STD
	{
		// identical_if
#		define FXD_SUPPORTS_IDENTICAL_IF 1

		template < typename InputItr1, typename InputItr2, typename Predicate >
		inline bool identical_if(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)
		{
			while ((first1 != last1) && (first2 != last2) && pred((*first1), (*first2)))
			{
				++first1;
				++first2;
			}
			return (first1 == last1) && (first2 == last2);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_IDENTICALIF_H