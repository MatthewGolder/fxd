#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISTRIVIALLYMOVECONSTRUCTIBLE_H
#define FXDENGINE_CORE_INTERNAL_ISTRIVIALLYMOVECONSTRUCTIBLE_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/AddRValueReference.h"
#include "FXDEngine/Core/Internal/IsTriviallyConstructible.h"

namespace FXD
{
	namespace STD
	{
		// is_trivially_move_constructible
#		define FXD_SUPPORTS_IS_TRIVIALLY_MOVE_CONSTRUCTIBLE 1

		template < typename T >
		struct is_trivially_move_constructible : public FXD::STD::is_trivially_constructible< T, typename FXD::STD::add_rvalue_reference< T >::type >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_trivially_move_constructible_v = FXD::STD::is_trivially_move_constructible< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISTRIVIALLYMOVECONSTRUCTIBLE_H