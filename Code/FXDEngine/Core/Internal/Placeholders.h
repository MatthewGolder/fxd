// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_PLACEHOLDERS_H
#define FXDENGINE_CORE_INTERNAL_PLACEHOLDERS_H

#include "FXDEngine/Core/Types.h"

namespace FXD
{
	namespace STD
	{
		// placeholders
#		define FXD_SUPPORTS_PLACEHOLDERS 1

		template < FXD::S32 N >
		struct _Ph
		{};

		namespace placeholders
		{
			CONSTEXPR14 FXD::STD::_Ph< 1 > _1{};
			CONSTEXPR14 FXD::STD::_Ph< 2 > _2{};
			CONSTEXPR14 FXD::STD::_Ph< 3 > _3{};
			CONSTEXPR14 FXD::STD::_Ph< 4 > _4{};
			CONSTEXPR14 FXD::STD::_Ph< 5 > _5{};
			CONSTEXPR14 FXD::STD::_Ph< 6 > _6{};
			CONSTEXPR14 FXD::STD::_Ph< 7 > _7{};
			CONSTEXPR14 FXD::STD::_Ph< 8 > _8{};
			CONSTEXPR14 FXD::STD::_Ph< 9 > _9{};
			CONSTEXPR14 FXD::STD::_Ph< 10 > _10{};
			CONSTEXPR14 FXD::STD::_Ph< 11 > _11{};
			CONSTEXPR14 FXD::STD::_Ph< 12 > _12{};
			CONSTEXPR14 FXD::STD::_Ph< 13 > _13{};
			CONSTEXPR14 FXD::STD::_Ph< 14 > _14{};
			CONSTEXPR14 FXD::STD::_Ph< 15 > _15{};
			CONSTEXPR14 FXD::STD::_Ph< 16 > _16{};
			CONSTEXPR14 FXD::STD::_Ph< 17 > _17{};
			CONSTEXPR14 FXD::STD::_Ph< 18 > _18{};
			CONSTEXPR14 FXD::STD::_Ph< 19 > _19{};
			CONSTEXPR14 FXD::STD::_Ph< 20 > _20{};
		} //namespace placeholders

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_PLACEHOLDERS_H