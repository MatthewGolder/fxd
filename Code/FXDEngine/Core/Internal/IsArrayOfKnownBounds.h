#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISARRAYOFKNOWNBOUNDS_H
#define FXDENGINE_CORE_INTERNAL_ISARRAYOFKNOWNBOUNDS_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Extent.h"

namespace FXD
{
	namespace STD
	{
		// is_array_of_known_bounds
#		define FXD_SUPPORTS_IS_ARRAY_OF_KNOWN_BOUNDS 1

		template < typename T >
		struct is_array_of_known_bounds : public FXD::STD::integral_constant< bool, FXD::STD::extent< T >::value != 0 >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR size_t is_array_of_known_bounds_v = FXD::STD::is_array_of_known_bounds< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISARRAYOFKNOWNBOUNDS_Hs