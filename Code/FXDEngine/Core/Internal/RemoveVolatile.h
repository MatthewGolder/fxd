// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REMOVEVOLATILE_H
#define FXDENGINE_CORE_INTERNAL_REMOVEVOLATILE_H

#include <cstddef>

namespace FXD
{
	namespace STD
	{
		// remove_volatile
#		define FXD_SUPPORTS_REMOVE_VOLATILE 1

		template < typename T >
		struct remove_volatile
		{
			using type = T;
		};

		template < typename T >
		struct remove_volatile< volatile T >
		{
			using type = T;
		};

		template < typename T >
		struct remove_volatile< volatile T[] >
		{
			typedef T type[];
		};

		template < typename T, std::size_t SIZE >
		struct remove_volatile< volatile T[SIZE] >
		{
			typedef T type[SIZE];
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using remove_volatile_t = typename FXD::STD::remove_volatile< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REMOVEVOLATILE_H