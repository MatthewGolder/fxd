// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ALLOFIF_H
#define FXDENGINE_CORE_INTERNAL_ALLOFIF_H

#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// all_of_if
		template < typename InputItr, typename Predicate >
		inline bool all_of_if(InputItr first, InputItr last, Predicate pred)
		{
			for (; first != last; ++first)
			{
				if (!pred(*first))
				{
					return false;
				}
			}
			return true;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ALLOFIF_H