#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISCONVERTIBLE_H
#define FXDENGINE_CORE_INTERNAL_ISCONVERTIBLE_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Declval.h"
#include "FXDEngine/Core/Internal/IsArray.h"
#include "FXDEngine/Core/Internal/IsFunction.h"
#include "FXDEngine/Core/Internal/IsVoid.h"

namespace FXD
{
	namespace STD
	{
		// is_convertible
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC) || defined(COMPILER_CLANG))
#		define FXD_SUPPORTS_IS_CONVERTIBLE 1

		template < typename From, typename To >
		struct is_convertible : public FXD::STD::integral_constant< bool, __is_convertible_to(From, To) >
		{};

#		else
#		define FXD_SUPPORTS_IS_CONVERTIBLE 1

		namespace Internal
		{
			template < typename From, typename To, bool = FXD::STD::is_void< From >::value || FXD::STD::is_function< To >::value || FXD::STD::is_array< To >::value >
			struct _is_convertible
			{
				static const bool value = FXD::STD::is_void< To >::value;
			};

			template < typename From, typename To >
			class _is_convertible< From, To, false >
			{
				template < typename To1 >
				static void ToFunction(To1); // We try to call this function with an instance of From. It is valid if From can be converted to To.

				template < typename /*From1*/, typename /*To1*/ >
				static FXD::STD::Internal::no_type _is(...);

				template < typename From1, typename To1 >
				static decltype(ToFunction< To1 >(FXD::STD::declval< From1 >()), FXD::STD::Internal::yes_type()) _is(FXD::S32);

			public:
				static const bool value = sizeof(_is< From, To >(0)) == 1;
			};
		} //namespace Internal

		template < typename From, typename To >
		struct is_convertible : public FXD::STD::integral_constant< bool, FXD::STD::Internal::_is_convertible< From, To >::value >
		{};

#		endif

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename From, typename To >
		CONSTEXPR bool is_convertible_v = FXD::STD::is_convertible< From, To >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISCONVERTIBLE_H