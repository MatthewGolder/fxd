#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_HASUNIQUEOBJECTREPRESENTATIONS_H
#define FXDENGINE_CORE_INTERNAL_HASUNIQUEOBJECTREPRESENTATIONS_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/RemoveAllExtents.h"
#include "FXDEngine/Core/Internal/RemoveCV.h"

namespace FXD
{
	namespace STD
	{
		// has_unique_object_representations
#		if IsCPP17()
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC))
#		define FXD_SUPPORTS_HAS_UNIQUE_OBJECT_REPRESENTATIONS 1

		template < typename T >
		struct has_unique_object_representations : FXD::STD::integral_constant< bool, __has_unique_object_representations(T) >
		{};

#		else
#		define FXD_SUPPORTS_HAS_UNIQUE_OBJECT_REPRESENTATIONS 0

		template < typename T >
		struct has_unique_object_representations : FXD::STD::integral_constant< bool, FXD::STD::is_integral< typename FXD::STD::remove_cv< typename FXD::STD::remove_all_extents< T >::type >::type >::value >
		{};
		// Only integral types (floating point types excluded).

#		endif

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool has_unique_object_representations_v = FXD::STD::has_unique_object_representations< T >::value;

#		endif
#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_HASUNIQUEOBJECTREPRESENTATIONS_H