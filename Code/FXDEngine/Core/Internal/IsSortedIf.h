#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISSORTEDIF_H
#define FXDENGINE_CORE_INTERNAL_ISSORTEDIF_H

#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// is_sorted_if
#		define FXD_SUPPORTS_IS_SORTED_IF 1

		template < typename ForwardItr, typename Predicate >
		bool is_sorted_if(ForwardItr first, ForwardItr last, Predicate pred)
		{
			if (first != last)
			{
				ForwardItr itr = first;
				for (++itr; itr != last; first = itr, ++itr)
				{
					if (pred(*itr, *first))
					{
						return false;
					}
				}
			}
			return true;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISSORTEDIF_H