#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_PARTIALSORTIF_H
#define FXDENGINE_CORE_INTERNAL_PARTIALSORTIF_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/SortHeap.h"

namespace FXD
{
	namespace STD
	{
		// partial_sort_if
#		define FXD_SUPPORTS_PARTITION_SORT_IF 1

		template < typename RandomAccessItr, typename Predicate >
		inline void partial_sort_if(RandomAccessItr first, RandomAccessItr mid, RandomAccessItr last, Predicate pred)
		{
			using difference_type = typename FXD::STD::iterator_traits< RandomAccessItr >::difference_type;

			FXD::STD::make_heap< RandomAccessItr, Predicate >(first, mid, pred);

			difference_type nLen = (mid - first);

			for (RandomAccessItr itr = mid; itr < last; ++itr)
			{
				if (pred(*itr, *first))
				{
					swap(*itr, *first);
					FXD::STD::Internal::_shift_heap_down(first, mid, pred, nLen, first);
				}
			}

			FXD::STD::sort_heap< RandomAccessItr, Predicate >(first, mid, pred);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_PARTIALSORTIF_H