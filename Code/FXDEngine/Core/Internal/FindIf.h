#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_FINDIF_H
#define FXDENGINE_CORE_INTERNAL_FINDIF_H

#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// find_if
#		define FXD_SUPPORTS_FIND_IF 1

		template < typename InputItr, typename Predicate >
		inline InputItr find_if(InputItr first, InputItr last, Predicate pred)
		{
			while ((first != last) && (!pred(*first)))
			{
				++first;
			}
			return first;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_FINDIF_H