#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_HASVOIDARG_H
#define FXDENGINE_CORE_INTERNAL_HASVOIDARG_H

#include "FXDEngine/Core/Internal/IsVoid.h"

namespace FXD
{
	namespace STD
	{
		// has_void_arg
#		define FXD_SUPPORTS_HAS_VOID_ARG 1

		template < typename ...Args >
		struct has_void_arg;

		template <>
		struct has_void_arg<> : public FXD::STD::false_type
		{};

		template < typename A0, typename ...Args >
		struct has_void_arg< A0, Args... >
		{
			static const bool value = (FXD::STD::is_void< A0 >::value || FXD::STD::has_void_arg< Args... >::value);
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_HASVOIDARG_H