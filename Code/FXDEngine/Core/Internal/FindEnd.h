#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_FINDEND_H
#define FXDENGINE_CORE_INTERNAL_FINDEND_H

#include "FXDEngine/Core/Internal/FindEndIf.h"
#include "FXDEngine/Core/Internal/EqualTo.h"

namespace FXD
{
	namespace STD
	{
		// find_end
#		define FXD_SUPPORTS_FIND_END 1

		template < typename InputItr, typename ForwardItr, typename Predicate >
		inline InputItr find_end(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)
		{
			return FXD::STD::find_end_if(first1, last1, first2, last2, pred);
		}

		template < typename InputItr, typename ForwardItr >
		inline InputItr find_end(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2)
		{
			return FXD::STD::find_end(first1, last1, first2, last2, FXD::STD::equal_to<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_FINDEND_H