#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_UPPERBOUND_H
#define FXDENGINE_CORE_INTERNAL_UPPERBOUND_H

#include "FXDEngine/Core/Internal/Less.h"
#include "FXDEngine/Core/Internal/UpperBoundIf.h"

namespace FXD
{
	namespace STD
	{
		// upper_bound
#		define FXD_SUPPORTS_UPPER_BOUND 1

		template < typename ForwardItr, typename T, typename Predicate >
		inline ForwardItr upper_bound(ForwardItr first, ForwardItr last, const T& val, Predicate pred)
		{
			return FXD::STD::upper_bound_if(first, last, val, pred);
		}

		template < typename ForwardItr, typename T >
		inline ForwardItr upper_bound(ForwardItr first, ForwardItr last, const T& val)
		{
			return FXD::STD::upper_bound(first, last, val, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_UPPERBOUND_H