#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_VOID_T_H
#define FXDENGINE_CORE_INTERNAL_VOID_T_H

namespace FXD
{
	namespace STD
	{
		// void_t
#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

#		define FXD_SUPPORTS_VOID_T 1

		template < typename... _Types >
		using void_t = void;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_VOID_T_H