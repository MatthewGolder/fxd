// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MISMATCHIF_H
#define FXDENGINE_CORE_INTERNAL_MISMATCHIF_H

#include "FXDEngine/Core/Pair.h"
#include "FXDEngine/Core/Internal/EqualTo.h"

namespace FXD
{
	namespace STD
	{
		// mismatch_if
#		define FXD_SUPPORTS_MISMATCH_IF 1

		template < typename InputItr1, typename InputItr2, typename Predicate >
		Core::Pair< InputItr1, InputItr2 > mismatch_if(InputItr1 first1, InputItr1 last, InputItr2 first2, Predicate pred)
		{
			while (first1 != last && pred(*first1, *first2))
			{
				++first1, ++first2;
			}
			return Core::make_pair(first1, first2);
		}

		template < typename InputItr1, typename InputItr2, typename Predicate >
		Core::Pair< InputItr1, InputItr2 > mismatch_if(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)
		{
			while (first1 != last1 && first2 != last2 && pred(*first1, *first2))
			{
				++first1, ++first2;
			}
			return Core::make_pair(first1, first2);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MISMATCHIF_H