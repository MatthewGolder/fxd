#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_STABLEPARTITION_H
#define FXDENGINE_CORE_INTERNAL_STABLEPARTITION_H

#include "FXDEngine/Container/Internal/Distance.h"
#include "FXDEngine/Core/Internal/FindIfNot.h"
#include "FXDEngine/Memory/Internal/Destroy.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Fill.h"
#include "FXDEngine/Memory/Allocator/Aligned.h"

namespace FXD
{
	namespace STD
	{
		// stable_partition
#		define FXD_SUPPORTS_STABLE_PARTITION 1

		template < typename ForwardItr, typename Predicate >
		inline ForwardItr stable_partition(ForwardItr first, ForwardItr last, Predicate pred)
		{
			using value_type = typename FXD::STD::iterator_traits< ForwardItr >::value_type;

			first = FXD::STD::find_if_not(first, last, pred);
			if (first == last)
			{
				return first;
			}

			const auto nReqSize = FXD::STD::distance(first, last);
			value_type* const pBuffer = (value_type*)Memory::Alloc::Default.allocate(nReqSize * sizeof(value_type));
			FXD::STD::uninitialized_fill(pBuffer, pBuffer + nReqSize, value_type());

			ForwardItr result1 = first;
			value_type* pResult2 = pBuffer;

			*pResult2 = std::move(*first);
			++pResult2;
			++first;
			for (; first != last; ++first)
			{
				if (pred(*first))
				{
					*result1 = std::move(*first);
					++result1;
				}
				else
				{
					*pResult2 = std::move(*first);
					++pResult2;
				}
			}

			FXD::STD::copy(pBuffer, pResult2, result1);
			FXD::STD::destruct(pBuffer, pBuffer + nReqSize);
			Memory::Alloc::Default.deallocate(pBuffer, nReqSize * sizeof(value_type));

			return result1;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_STABLEPARTITION_H