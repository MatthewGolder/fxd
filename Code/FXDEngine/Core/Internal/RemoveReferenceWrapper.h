// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REMOVEREFERENCEWRAPPER_H
#define FXDENGINE_CORE_INTERNAL_REMOVEREFERENCEWRAPPER_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/ReferenceWrapper.h"

namespace FXD
{
	namespace STD
	{
		// remove_reference_wrapper
#		define FXD_SUPPORTS_REMOVE_REFERENCE_WRAPPER 1

		template < typename T >
		struct remove_reference_wrapper
		{
			using type = T;
		};

		template < typename T >
		struct remove_reference_wrapper< FXD::STD::reference_wrapper< T > >
		{
			using type = T & ;
		};

		template < typename T >
		struct remove_reference_wrapper< const FXD::STD::reference_wrapper< T > >
		{
			using type = T & ;
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REMOVEREFERENCEWRAPPER_H