#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_CONDITIONALEXTENSIONS_H
#define FXDENGINE_CORE_INTERNAL_CONDITIONALEXTENSIONS_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Conditional.h"

namespace FXD
{
	namespace STD
	{
		namespace Internal
		{
			// _or
			template < typename... >
			struct _or;

			template <>
			struct _or<> : public FXD::STD::false_type
			{};

			template < typename B1 >
			struct _or< B1 > : public B1
			{};

			template < typename B1, typename B2 >
			struct _or< B1, B2 > : public FXD::STD::conditional< B1::value, B1, B2 >::type
			{};

			template < typename B1, typename B2, typename B3, typename... Bn >
			struct _or< B1, B2, B3, Bn... > : public FXD::STD::conditional< B1::value, B1, FXD::STD::Internal::_or< B2, B3, Bn... > >::type
			{};

			// _and
			template < typename... >
			struct _and;

			template <>
			struct _and<> : public FXD::STD::true_type
			{};

			template < typename B1 >
			struct _and< B1 > : public B1
			{};

			template < typename B1, typename B2 >
			struct _and< B1, B2 > : public FXD::STD::conditional< B1::value, B2, B1 >::type
			{};

			template < typename B1, typename B2, typename B3, typename... Bn >
			struct _and< B1, B2, B3, Bn... > : public FXD::STD::conditional< B1::value, FXD::STD::Internal::_and< B2, B3, Bn... >, B1 >::type
			{};

			// _not
			template < typename B1 >
			struct _not : public FXD::STD::integral_constant< bool, !bool(B1::value) >
			{};

		} //namespace Internal
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_CONDITIONALEXTENSIONS_H