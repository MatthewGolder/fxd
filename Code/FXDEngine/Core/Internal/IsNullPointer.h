#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISNULLPOINTER_H
#define FXDENGINE_CORE_INTERNAL_ISNULLPOINTER_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/IsSame.h"
#include "FXDEngine/Core/Internal/RemoveCV.h"

namespace FXD
{
	namespace STD
	{
		// is_null_pointer
#		define FXD_SUPPORTS_IS_NULL_POINTER 1

		template < typename T >
		struct is_null_pointer : public FXD::STD::is_same< typename FXD::STD::remove_cv< T >::type, decltype(nullptr) >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_null_pointer_v = FXD::STD::is_null_pointer< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISNULLPOINTER_H