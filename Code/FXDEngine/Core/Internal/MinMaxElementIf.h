#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MINMAXELEMENTIF_H
#define FXDENGINE_CORE_INTERNAL_MINMAXELEMENTIF_H

#include "FXDEngine/Core/Pair.h"

namespace FXD
{
	namespace STD
	{
		// minmax_element_if
#		define FXD_SUPPORTS_MINMAX_ELEMENT_IF 1

		template < typename ForwardItr, typename Predicate >
		CONSTEXPR17 Core::Pair< ForwardItr, ForwardItr > minmax_element_if(ForwardItr first, ForwardItr last, Predicate pred)
		{
			Core::Pair< ForwardItr, ForwardItr > result(first, first);

			if (!(first == last) && !(++first == last))
			{
				if (pred(*first, *result.first))
				{
					result.second = result.first;
					result.first = first;
				}
				else
				{
					result.second = first;
				}

				while (++first != last)
				{
					ForwardItr itr = first;

					if (++first == last)
					{
						if (pred(*itr, *result.first))
						{
							result.first = itr;
						}
						else if (!pred(*itr, *result.second))
						{
							result.second = itr;
						}
						break;
					}
					else
					{
						if (pred(*first, *itr))
						{
							if (pred(*first, *result.first))
							{
								result.first = first;
							}
							if (!pred(*itr, *result.second))
							{
								result.second = itr;
							}
						}
						else
						{
							if (pred(*itr, *result.first))
							{
								result.first = itr;
							}
							if (!pred(*first, *result.second))
							{
								result.second = first;
							}
						}
					}
				}
			}
			return result;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MINMAXELEMENTIF_H