#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MINMAXELEMENT_H
#define FXDENGINE_CORE_INTERNAL_MINMAXELEMENT_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/MinMaxElementIf.h"

namespace FXD
{
	namespace STD
	{
		// minmax_element
#		define FXD_SUPPORTS_MINMAX_ELEMENT 1

		template < typename ForwardItr, typename Predicate >
		CONSTEXPR17 Core::Pair< ForwardItr, ForwardItr > minmax_element(ForwardItr first, ForwardItr last, Predicate pred)
		{
			return FXD::STD::minmax_element_if(first, last, pred);
		}


		template < typename ForwardItr >
		CONSTEXPR17 Core::Pair< ForwardItr, ForwardItr > minmax_element(ForwardItr first, ForwardItr last)
		{
			using value_type = typename FXD::STD::iterator_traits< ForwardItr >::value_type;

			return FXD::STD::minmax_element(first, last, FXD::STD::less< value_type >());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MINELEMENT_H