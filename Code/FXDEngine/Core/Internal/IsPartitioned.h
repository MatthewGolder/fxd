#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISPARTITIONED_H
#define FXDENGINE_CORE_INTERNAL_ISPARTITIONED_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsFundamental.h"

namespace FXD
{
	namespace STD
	{
		// is_partitioned
#		define FXD_SUPPORTS_IS_PARTITIONED 1

		template < typename InputItr, typename Predicate >
		inline bool is_partitioned(InputItr first, InputItr last, Predicate pred)
		{
			for (; first != last; ++first)
			{
				if (!pred(*first))
				{
					break;
				}
			}

			for (; first != last; ++first)
			{
				if (pred(*first))
				{
					return false;
				}
			}
			return true;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISPARTITIONED_H