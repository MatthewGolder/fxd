// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_BINARYSEARCH_H
#define FXDENGINE_CORE_INTERNAL_BINARYSEARCH_H

#include "FXDEngine/Core/Internal/BinarySearchIf.h"
#include "FXDEngine/Core/Internal/Less.h"

namespace FXD
{
	namespace STD
	{
		// binary_search
#		define FXD_SUPPORTS_BINARY_SEARCH 1

		template < typename ForwardItr, typename T, typename Predicate >
		inline bool binary_search(ForwardItr first, ForwardItr last, const T& val, Predicate pred)
		{
			return FXD::STD::binary_search_if(first, last, val, pred);
		}

		template < typename ForwardItr, typename T >
		inline bool binary_search(ForwardItr first, ForwardItr last, const T& val)
		{
			return FXD::STD::binary_search(first, last, val, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_BINARYSEARCH_H