#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_CONJUNCTION_H
#define FXDENGINE_CORE_INTERNAL_CONJUNCTION_H

#include "FXDEngine/Core/Internal/Conditional.h"
#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// conjunction
#		define FXD_SUPPORTS_CONJUNCTION 1

		template < typename... >
		struct conjunction : FXD::STD::true_type
		{};

		template < typename T >
		struct conjunction< T > : T
		{};

		template < typename T, typename...Tn >
		struct conjunction< T, Tn... > : FXD::STD::conditional< bool(T::value), FXD::STD::conjunction< Tn... >, T >::type
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename...Tn >
		CONSTEXPR bool conjunction_v = FXD::STD::conjunction< Tn... >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_CONJUNCTION_H