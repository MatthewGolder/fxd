#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_LEXICOGRAPHICALCOMPAREIF_H
#define FXDENGINE_CORE_INTERNAL_LEXICOGRAPHICALCOMPAREIF_H

namespace FXD
{
	namespace STD
	{
		// lexicographical_compare_if
#		define FXD_SUPPORTS_LEXICOGRAPHICAL_COMPARE_IF 1

		template < typename InputItr1, typename InputItr2, typename Predicate >
		inline bool lexicographical_compare_if(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)
		{
			for (; (first1 != last1) && (first2 != last2); ++first1, ++first2)
			{
				if (pred((*first1), (*first2)))
				{
					return true;
				}
				if (pred((*first2), (*first1)))
				{
					return false;
				}
			}
			return (first1 == last1) && (first2 != last2);
		}

	} //namespace FXD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_LEXICOGRAPHICALCOMPAREIF_H