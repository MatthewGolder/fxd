// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ADJACENTDIFFERENCEIF_H
#define FXDENGINE_CORE_INTERNAL_ADJACENTDIFFERENCEIF_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"

namespace FXD
{
	namespace STD
	{
		// adjacent_difference_if
#		define FXD_SUPPORTS_ADJACENT_DIFFERENCE_IF 1

		template < typename InputItr, typename OutputItr, typename Predicate >
		inline OutputItr adjacent_difference_if(InputItr first, InputItr last, OutputItr dst, Predicate pred)
		{
			using value_type = typename FXD::STD::iterator_traits< InputItr >::value_type;

			if (first != last)
			{
				value_type val(*first);

				for (*dst = val; ++first != last; )
				{
					const value_type temp(*first);

					*++dst = pred(temp, val);
					val = temp;
				}
				++dst;
			}
			return dst;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ADJACENTDIFFERENCEIF_H