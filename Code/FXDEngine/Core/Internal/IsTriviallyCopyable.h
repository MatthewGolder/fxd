#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISTRIVIALLYCOPYABLE_H
#define FXDENGINE_CORE_INTERNAL_ISTRIVIALLYCOPYABLE_H

#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// is_trivially_copyable
#		define FXD_SUPPORTS_IS_TRIVIALLY_COPYABLE 1

		template < typename T >
		struct is_trivially_copyable : public FXD::STD::integral_constant< bool, __is_trivially_copyable(T) >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_trivially_copyable_v = FXD::STD::is_trivially_copyable< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISTRIVIALLYCOPYABLE_H