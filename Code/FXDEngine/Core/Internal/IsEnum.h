#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISENUM_H
#define FXDENGINE_CORE_INTERNAL_ISENUM_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/AddReference.h"
#include "FXDEngine/Core/Internal/IsArithmetic.h"
#include "FXDEngine/Core/Internal/IsClass.h"
#include "FXDEngine/Core/Internal/IsConvertible.h"
#include "FXDEngine/Core/Internal/IsReference.h"

namespace FXD
{
	namespace STD
	{
		// is_enum
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC) || defined(COMPILER_CLANG))
#		define FXD_SUPPORTS_IS_ENUM 1

		template < typename T >
		struct is_enum : public FXD::STD::integral_constant< bool, __is_enum(T) >
		{};

#		else	
#		define FXD_SUPPORTS_IS_ENUM 1

		namespace Internal
		{
			struct _int_convertible
			{
				_int_convertible(FXD::S32);
			};

			template < bool is_arithmetic_or_reference >
			struct _is_enum
			{
				template < typename T >
				struct _nested : public FXD::STD::is_convertible< T, FXD::STD::Internal::_int_convertible >
				{};
			};

			template <>
			struct _is_enum< true >
			{
				template < typename T >
				struct _nested : public FXD::STD::false_type
				{};
			};

			template < typename T >
			struct _is_enum_2
			{
				using selector = FXD::STD::Internal::type_or< FXD::STD::is_arithmetic< T >::value, FXD::STD::is_reference< T >::value, FXD::STD::is_class< T >::value >;
				using helper_t = FXD::STD::Internal::_is_enum< selector::value >;
				using ref_t = typename FXD::STD::add_reference< T >::type;
				using result = typename helper_t::template _nested< ref_t >;
			};
		} //namespace Internal

		template < typename T >
		struct is_enum : public FXD::STD::integral_constant< bool, FXD::STD::Internal::_is_enum_2< T >::result::value >
		{};

#		endif

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_enum_v = FXD::STD::is_enum< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISENUM_H