#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MINMAX_H
#define FXDENGINE_CORE_INTERNAL_MINMAX_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/MinMaxIf.h"
#include <initializer_list>

namespace FXD
{
	namespace STD
	{
		// minmax
#		define FXD_SUPPORTS_MINMAX 1

		template < typename T, typename Pred >
		CONSTEXPR14 Core::Pair< const T&, const T& > minmax(const T& lhs, const T& rhs, Pred pred)
		{
			return FXD::STD::minmax_if(lhs, rhs, pred);
		}

		template < typename T >
		CONSTEXPR14 Core::Pair< const T&, const T& > minmax(const T& lhs, const T& rhs)
		{
			return FXD::STD::minmax(lhs, rhs, FXD::STD::less<>());
		}

		template < typename T, typename Pred >
		CONSTEXPR14 Core::Pair< T, T > minmax(std::initializer_list< T > iList, Pred pred)
		{
			return FXD::STD::minmax_if(iList, pred);
		}

		template < typename T >
		CONSTEXPR14 Core::Pair< T, T > minmax(std::initializer_list< T > iList)
		{
			return FXD::STD::minmax(iList, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MINMAX_H