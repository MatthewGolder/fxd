// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ROTATE_H
#define FXDENGINE_CORE_INTERNAL_ROTATE_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Container/Internal/Next.h"
#include "FXDEngine/Container/Internal/Prev.h"
#include "FXDEngine/Core/Internal/IsPod.h"
#include "FXDEngine/Core/Internal/IsScalar.h"
#include "FXDEngine/Core/Internal/IsTriviallyMoveAssignable.h"
#include "FXDEngine/Core/Internal/Move.h"
#include "FXDEngine/Core/Internal/MoveBackward.h"
#include "FXDEngine/Core/Internal/Swap.h"

namespace FXD
{
	namespace STD
	{
		// rotate
#		define FXD_SUPPORTS_ROTATE 1

		namespace Internal
		{
			template < typename ForwardItr >
			inline ForwardItr _rotate_general(ForwardItr first, ForwardItr middle, ForwardItr last)
			{
				ForwardItr current = middle;

				do
				{
					FXD::STD::swap(*first++, *current++);

					if (first == middle)
					{
						middle = current;
					}
				} while (current != last);

				ForwardItr result = first;
				current = middle;

				while (current != last)
				{
					FXD::STD::swap(*first++, *current++);

					if (first == middle)
					{
						middle = current;
					}
					else if (current == last)
					{
						current = middle;
					}
				}
				return result;
			}


			template < typename ForwardItr >
			inline ForwardItr _move_rotate_left_by_one(ForwardItr first, ForwardItr last)
			{
				using value_type = typename FXD::STD::iterator_traits< ForwardItr >::value_type;

				value_type temp(std::move(*first));
				ForwardItr result = FXD::STD::move(FXD::STD::next(first), last, first);
				*result = std::move(temp);

				return result;
			}


			template < typename BidirItr >
			inline BidirItr _move_rotate_right_by_one(BidirItr first, BidirItr last)
			{
				using value_type = typename FXD::STD::iterator_traits< BidirItr >::value_type;

				BidirItr beforeLast = FXD::STD::prev(last);
				value_type temp(std::move(*beforeLast));
				BidirItr result = FXD::STD::move_backward(first, beforeLast, last);
				*first = std::move(temp);

				return result;
			}

			template < typename Integer >
			inline Integer _greatest_common_divisor(Integer lhs, Integer rhs)
			{
				do
				{
					Integer t = (lhs % rhs);
					lhs = rhs;
					rhs = t;
				} while (rhs);

				return lhs;
			}

			template < typename /*ItrCategory*/, bool /*FXD::STD::is_trivially_move_assignable*/ >
			struct _rotate
			{
				template < typename ForwardItr >
				static ForwardItr _impl(ForwardItr first, ForwardItr middle, ForwardItr last)
				{
					return FXD::STD::Internal::_rotate_general(first, middle, last);
				}
			};

			template <>
			struct _rotate< FXD::STD::forward_iterator_tag, true >
			{
				template < typename ForwardItr >
				static ForwardItr _impl(ForwardItr first, ForwardItr middle, ForwardItr last)
				{
					if (FXD::STD::next(first) == middle)
					{
						return FXD::STD::Internal::_move_rotate_left_by_one(first, last);
					}
					return FXD::STD::Internal::_rotate_general(first, middle, last);
				}
			};

			template <>
			struct _rotate< FXD::STD::bidirectional_iterator_tag, false >
			{
				template < typename BidirItr >
				static BidirItr _impl(BidirItr first, BidirItr middle, BidirItr last)
				{
					return FXD::STD::Internal::_rotate_general(first, middle, last);
				}
			};

			template <>
			struct _rotate< FXD::STD::bidirectional_iterator_tag, true >
			{
				template < typename BidirItr >
				static BidirItr _impl(BidirItr first, BidirItr middle, BidirItr last)
				{
					if (FXD::STD::next(first) == middle)
					{
						return FXD::STD::Internal::_move_rotate_left_by_one(first, last);
					}
					if (FXD::STD::next(middle) == last)
					{
						return FXD::STD::Internal::_move_rotate_right_by_one(first, last);
					}
					return FXD::STD::Internal::_rotate_general(first, middle, last);
				}
			};

			template <>
			struct _rotate< FXD::STD::random_access_iterator_tag, false >
			{
				template < typename RandomAccessItr >
				static RandomAccessItr _impl(RandomAccessItr first, RandomAccessItr middle, RandomAccessItr last)
				{
					using difference_type = typename FXD::STD::iterator_traits< RandomAccessItr >::difference_type;
					using value_type = typename FXD::STD::iterator_traits< RandomAccessItr >::value_type;

					const difference_type m1 = (middle - first);
					const difference_type m2 = (last - middle);
					const difference_type g = FXD::STD::Internal::_greatest_common_divisor(m1, m2);
					value_type temp;

					for (RandomAccessItr p = first + g; p != first;)
					{
						temp = std::move(*--p);
						RandomAccessItr p1 = p;
						RandomAccessItr p2 = p + m1;
						do
						{
							*p1 = std::move(*p2);
							p1 = p2;
							const difference_type d = (last - p2);

							if (m1 < d)
							{
								p2 += m1;
							}
							else
							{
								p2 = first + (m1 - d);
							}
						} while (p2 != p);

						*p1 = std::move(temp);
					}

					return first + m2;
				}
			};

			template <>
			struct _rotate< FXD::STD::random_access_iterator_tag, true >
			{
				template < typename RandomAccessItr >
				static RandomAccessItr _impl(RandomAccessItr first, RandomAccessItr middle, RandomAccessItr last)
				{
					if (FXD::STD::next(first) == middle)
					{
						return FXD::STD::Internal::_move_rotate_left_by_one(first, last);
					}
					if (FXD::STD::next(middle) == last)
					{
						return FXD::STD::Internal::_move_rotate_right_by_one(first, last);
					}
					if ((last - first) < 32)
					{
						return FXD::STD::Internal::_rotate_general(first, middle, last);
					}
					return FXD::STD::Internal::_rotate< FXD::STD::random_access_iterator_tag, false >::_impl(first, middle, last);
				}
			};

		} // namespace Internal


		template < typename ForwardItr >
		inline ForwardItr rotate(ForwardItr first, ForwardItr middle, ForwardItr last)
		{
			using IC = typename FXD::STD::iterator_traits< ForwardItr >::iterator_category;
			using value_type = typename FXD::STD::iterator_traits< ForwardItr >::value_type;

			if (middle != first)
			{
				return last;
			}
			else if (middle != last)
			{
				return first;
			}

			return FXD::STD::Internal::_rotate< IC, FXD::STD::is_trivially_move_assignable< value_type >::value || FXD::STD::is_pod< value_type >::value || FXD::STD::is_scalar< value_type >::value >::_impl(first, middle, last);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ROTATE_H