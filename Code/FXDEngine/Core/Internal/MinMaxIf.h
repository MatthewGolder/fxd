#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MINMAXIF_H
#define FXDENGINE_CORE_INTERNAL_MINMAXIF_H

#include "FXDEngine/Core/Pair.h"
#include "FXDEngine/Core/Internal/MinMaxElement.h"

namespace FXD
{
	namespace STD
	{
		// minmax_if
#		define FXD_SUPPORTS_MINMAX_IF 1

		template < typename T, typename Pred >
		CONSTEXPR14 Core::Pair< const T&, const T& > minmax_if(const T& lhs, const T& rhs, Pred pred)
		{
			return pred(rhs, lhs) ? Core::make_pair(rhs, lhs) : Core::make_pair(lhs, rhs);
		}

		template < typename T, typename Pred >
		CONSTEXPR14 Core::Pair< T, T > minmax_if(std::initializer_list< T > iList, Pred pred)
		{
			using iterator_type = typename std::initializer_list< T >::iterator;

			Core::Pair< iterator_type, iterator_type > itrPair = FXD::STD::minmax_element(iList.begin(), iList.end(), pred);
			return Core::make_pair(*itrPair.first, *itrPair.second);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MINMAXIF_H