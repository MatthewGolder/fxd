#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISFLOATINGPOINT_H
#define FXDENGINE_CORE_INTERNAL_ISFLOATINGPOINT_H

#include "FXDEngine/Core/Types.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/RemoveCV.h"

namespace FXD
{
	namespace STD
	{
		// is_floating_point
#		define FXD_SUPPORTS_IS_FLOATING_POINT 1

		namespace Internal
		{
			template < typename T >
			struct _is_floating_point : public FXD::STD::false_type
			{};

			template <>
			struct _is_floating_point< FXD::F32 > : public FXD::STD::true_type
			{};

			template <>
			struct _is_floating_point< FXD::F64 > : public FXD::STD::true_type
			{};
		} //namespace Internal

		template < typename T >
		struct is_floating_point : public FXD::STD::Internal::_is_floating_point< typename FXD::STD::remove_cv< T >::type >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_floating_point_v = FXD::STD::is_floating_point< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISFLOATINGPOINT_H