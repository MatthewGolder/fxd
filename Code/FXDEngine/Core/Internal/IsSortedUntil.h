#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISSORTEDUNTIL_H
#define FXDENGINE_CORE_INTERNAL_ISSORTEDUNTIL_H

#include "FXDEngine/Core/Internal/IsSortedUntilIf.h"

namespace FXD
{
	namespace STD
	{
		// is_sorted_until
#		define FXD_SUPPORTS_IS_SORTED_UNTIL 1

		template < typename ForwardItr, typename Predicate >
		inline ForwardItr is_sorted_until(ForwardItr first, ForwardItr last, Predicate pred)
		{
			return FXD::STD::is_sorted_until_if(first, last, pred);
		}

		template < typename ForwardItr >
		inline ForwardItr is_sorted_until(ForwardItr first, ForwardItr last)
		{
			return FXD::STD::is_sorted_until(first, last, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISSORTEDUNTIL_H