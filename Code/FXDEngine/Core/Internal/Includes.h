// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_INCLUDES_H
#define FXDENGINE_CORE_INTERNAL_INCLUDES_H

#include "FXDEngine/Core/Internal/IncludesIf.h"
#include "FXDEngine/Core/Internal/Less.h"

namespace FXD
{
	namespace STD
	{
		// includes
#		define FXD_SUPPORTS_INCLUDES 1

		template < typename InputItr1, typename InputItr2, typename Predicate >
		inline bool includes(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)
		{
			return FXD::STD::includes_if(first1, last1, first2, last2, pred);
		}

		template < typename InputItr1, typename InputItr2 >
		inline bool includes(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2)
		{
			return FXD::STD::includes(first1, last1, first2, last2, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_INCLUDES_H