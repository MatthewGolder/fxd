#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ADDRVALUEREFERENCE_H
#define FXDENGINE_CORE_INTERNAL_ADDRVALUEREFERENCE_H

#include "FXDEngine/Core/Internal/TypeIdentity.h"

namespace FXD
{
	namespace STD
	{
		// add_rvalue_reference
#		define FXD_SUPPORTS_ADD_RVALUE_REFERENCE 1

		namespace Internal
		{
			template < typename T >
			auto _add_rvalue_reference(FXD::S32)->FXD::STD::type_identity< T&& >;

			template < typename T >
			auto _add_rvalue_reference(...)->FXD::STD::type_identity< T >;
		} // namespace Internal

		template < typename T >
		struct add_rvalue_reference : decltype(FXD::STD::Internal::_add_rvalue_reference< T >(0))
		{};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using add_rvalue_reference_t = typename FXD::STD::add_rvalue_reference< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ADDRVALUEREFERENCE_H