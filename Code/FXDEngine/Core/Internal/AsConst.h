#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ASCONST_H
#define FXDENGINE_CORE_INTERNAL_ASCONST_H

#include "FXDEngine/Core/Internal/AddConst.h"

namespace FXD
{
	namespace STD
	{
		// add_const
#		define FXD_SUPPORTS_ADD_CONST 1

		template < typename T >
		CONSTEXPR typename FXD::STD::add_const< T >::type& as_const(T& t) NOEXCEPT
		{
			return t;
		}

		template < typename T >
		void as_const(const T&&) = delete;

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ASCONST_H