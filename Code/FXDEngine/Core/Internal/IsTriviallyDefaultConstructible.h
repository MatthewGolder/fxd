#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISTRIVALLYDEFAULTCONSTRUCTIBLE_H
#define FXDENGINE_CORE_INTERNAL_ISTRIVALLYDEFAULTCONSTRUCTIBLE_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/IsTriviallyConstructible.h"

namespace FXD
{
	namespace STD
	{
		// is_trivially_default_constructible
#		define FXD_SUPPORTS_IS_TRIVIALLY_DEFAULT_CONSTRUCTIBLE 1

		template < typename T >
		struct is_trivially_default_constructible : public FXD::STD::is_trivially_constructible< T >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_trivially_default_constructible_v = FXD::STD::is_trivially_default_constructible< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISTRIVALLYDEFAULTCONSTRUCTIBLE_H