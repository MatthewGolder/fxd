#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_EXTENT_H
#define FXDENGINE_CORE_INTERNAL_EXTENT_H

#include "FXDEngine/Core/Internal/Config.h"
#include <cstddef>

namespace FXD
{
	namespace STD
	{
		// extent
#		define FXD_SUPPORTS_EXTENT 1

		template < typename T, FXD::U32 SIZE = 0 >
		struct extent : FXD::STD::integral_constant< size_t, 0 >
		{};

		template < typename T >
		struct extent< T[], 0 > : FXD::STD::integral_constant< size_t, 0 >
		{};

		template < typename T, FXD::U32 SIZE >
		struct extent< T[], SIZE > : FXD::STD::extent< T, SIZE - 1 >
		{};

		template < typename T, size_t I >
		struct extent< T[I], 0 > : FXD::STD::integral_constant< size_t, I >
		{};

		template < typename T, size_t I, FXD::U32 SIZE >
		struct extent< T[I], SIZE > : FXD::STD::extent< T, SIZE - 1 >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T, FXD::U32 SIZE = 0 >
		CONSTEXPR auto extent_v = FXD::STD::extent< T, SIZE >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_EXTENT_H