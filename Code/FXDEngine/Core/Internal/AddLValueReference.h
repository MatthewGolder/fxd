#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ADDLVALUEREFERENCE_H
#define FXDENGINE_CORE_INTERNAL_ADDLVALUEREFERENCE_H

#include "FXDEngine/Core/Internal/TypeIdentity.h"

namespace FXD
{
	namespace STD
	{
		// add_lvalue_reference
#		define FXD_SUPPORTS_ADD_LVALUE_REFERENCE 1

		namespace Internal
		{
			template < typename T >
			auto _add_lvalue_reference(FXD::S32)->FXD::STD::type_identity< T& >;

			template < typename T >
			auto _add_lvalue_reference(...)->FXD::STD::type_identity< T >;
		} // namespace Internal

		template < typename T >
		struct add_lvalue_reference : decltype(FXD::STD::Internal::_add_lvalue_reference< T >(0))
		{};


#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using add_lvalue_reference_t = typename FXD::STD::add_lvalue_reference< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ADDLVALUEREFERENCE_H