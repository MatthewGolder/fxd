#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_TYPEIDENTITY_H
#define FXDENGINE_CORE_INTERNAL_TYPEIDENTITY_H

namespace FXD
{
	namespace STD
	{
		// type_identity
#		define FXD_SUPPORTS_TYPE_IDENTITY 1

		template < typename T >
		struct type_identity
		{
			using type = T;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename T >
		using type_identity_t = typename FXD::STD::type_identity< T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_TYPEIDENTITY_H