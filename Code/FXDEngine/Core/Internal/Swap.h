#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_SWAP_H
#define FXDENGINE_CORE_INTERNAL_SWAP_H

#include "FXDEngine/Core/Internal/IsNoThrowMoveConstructible.h"
#include "FXDEngine/Core/Internal/IsNoThrowMoveAssignable.h"
#include "FXDEngine/Core/Internal/IsNoThrowSwappable.h"
#include "FXDEngine/Core/Internal/SwapHelper.h"

namespace FXD
{
	namespace STD
	{
		// swap
#		define FXD_SUPPORTS_SWAP 1

		template < typename T, typename FXD::STD::enable_if< FXD::STD::is_move_constructible< T >::value && FXD::STD::is_move_assignable< T >::value, FXD::S32 >::type _Enabled >
		inline void swap(T& lhs, T& rhs) NOEXCEPT(FXD::STD::is_nothrow_move_constructible< T >::value && FXD::STD::is_nothrow_move_assignable< T >::value)
		{
			T tmp = std::move(lhs);
			lhs = std::move(rhs);
			rhs = std::move(tmp);
		}

		// iter_swap
		template < typename InputItr, typename OutputItr >
		inline void iter_swap(InputItr lhs, OutputItr rhs)
		{
			FXD::STD::swap(*lhs, *rhs);
		}

		// swap_ranges
		template < typename InputItr, typename OutputItr >
		inline OutputItr swap_ranges(InputItr first, InputItr last, OutputItr dst)
		{
			for (; first != last; ++first, ++dst)
			{
				FXD::STD::iter_swap(first, dst);
			}
			return dst;
		}

		// swap
		template < typename T2, size_t SIZE, typename FXD::STD::enable_if< FXD::STD::Internal::_is_swappable< T2 >::value, FXD::S32 >::type _Enabled >
		inline void swap(T2(&lhs)[SIZE], T2(&rhs)[SIZE]) NOEXCEPT(FXD::STD::Internal::_is_nothrow_swappable< T2 >::value)
		{
			if (&lhs != &rhs)
			{
				T2* pFirst1 = lhs;
				T2* pLast1 = pFirst1 + SIZE;
				T2* pFirst2 = rhs;
				for (; pFirst1 != pLast1; ++pFirst1, ++pFirst2)
				{
					FXD::STD::iter_swap(pFirst1, pFirst2);
				}
			}
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_SWAP_H