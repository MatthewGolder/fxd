#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISNOTHROWMOVEASSIGNABLE_H
#define FXDENGINE_CORE_INTERNAL_ISNOTHROWMOVEASSIGNABLE_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsNoThrowAssignable.h"
#include "FXDEngine/Core/Internal/AddLValueReference.h"
#include "FXDEngine/Core/Internal/AddRValueReference.h"

namespace FXD
{
	namespace STD
	{
		// is_nothrow_move_assignable
#		define FXD_SUPPORTS_IS_NOTHROW_MOVE_ASSIGNABLE 1

		template < typename T >
		struct is_nothrow_move_assignable : public FXD::STD::is_nothrow_assignable< typename FXD::STD::add_lvalue_reference< T >::type, typename FXD::STD::add_rvalue_reference< T >::type >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_nothrow_move_assignable_v = is_nothrow_move_assignable< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISNOTHROWMOVEASSIGNABLE_H