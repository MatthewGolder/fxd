// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_SETDIFFERENCE_H
#define FXDENGINE_CORE_INTERNAL_SETDIFFERENCE_H

#include "FXDEngine/Core/Internal/SetDifferenceIf.h"
#include "FXDEngine/Core/Internal/Less.h"

namespace FXD
{
	namespace STD
	{
		// set_difference
#		define FXD_SUPPORTS_SET_DIFFERENCE 1

		template < typename InputItr1, typename InputItr2, typename OutputItr, typename Predicate >
		inline OutputItr set_difference(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst, Predicate pred)
		{
			return FXD::STD::set_difference_if(first1, last1, first2, last2, dst, pred);
		}

		template < typename InputItr1, typename InputItr2, typename OutputItr >
		inline OutputItr set_difference(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst)
		{
			return FXD::STD::set_difference(first1, last1, first2, last2, dst, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_SETDIFFERENCE_H