#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_PUSHHEAPIF_H
#define FXDENGINE_CORE_INTERNAL_PUSHHEAPIF_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/ShiftHeap.h"

namespace FXD
{
	namespace STD
	{
		// push_heap_if
#		define FXD_SUPPORTS_PUSH_HEAP_IF 1

		template < typename RandomAccessItr, typename Predicate >
		inline void push_heap_if(RandomAccessItr first, RandomAccessItr last, Predicate pred)
		{
			FXD::STD::Internal::_shift_heap_up(first, last, pred, (last - first));
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_PUSHHEAPIF_H