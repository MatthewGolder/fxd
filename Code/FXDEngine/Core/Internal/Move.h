#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MOVE_H
#define FXDENGINE_CORE_INTERNAL_MOVE_H

#include "FXDEngine/Core/Internal/CopyMoveHelper.h"
#include "FXDEngine/Container/Internal/UnwrapIterator.h"

namespace FXD
{
	namespace STD
	{
		// move
#		define FXD_SUPPORTS_MOVE 1

		template < typename InputItr, typename OutputItr >
		inline OutputItr move(InputItr first, InputItr last, OutputItr dst)
		{
			return FXD::STD::Internal::_move_copy_unwrapper< true >(FXD::STD::unwrap_iterator(first), FXD::STD::unwrap_iterator(last), dst);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MOVE_H