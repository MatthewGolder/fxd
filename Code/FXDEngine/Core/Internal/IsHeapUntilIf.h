#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISHEAPUNTILIF_H
#define FXDENGINE_CORE_INTERNAL_ISHEAPUNTILIF_H

namespace FXD
{
	namespace STD
	{
		// is_heap_until_if
#		define FXD_SUPPORTS_IS_HEAP_UNTIL_IF 1

		template < typename RandomAccessItr, typename Predicate >
		inline RandomAccessItr is_heap_until_if(RandomAccessItr first, RandomAccessItr last, Predicate pred)
		{
			FXD::S32 nCount = 0;

			for (RandomAccessItr itr = first + 1; itr < last; ++itr, nCount ^= 1)
			{
				if (pred(*first, *itr))
				{
					return itr;
				}
				first += nCount; // counter switches between 0 and 1 every time through.
			}

			return last;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISHEAPUNTILIF_H