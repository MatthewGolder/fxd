#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_FUNCTION_H
#define FXDENGINE_CORE_INTERNAL_FUNCTION_H

#include "FXDEngine/Core/Internal/BadFunctionCall.h"
#include "FXDEngine/Core/Internal/Conjunction.h"
#include "FXDEngine/Core/Internal/Decay.h"
#include "FXDEngine/Core/Internal/Disjunction.h"
#include "FXDEngine/Core/Internal/EnableIf.h"
#include "FXDEngine/Core/Internal/InvokeHelper.h"
#include "FXDEngine/Core/Internal/IsFunction.h"
#include "FXDEngine/Core/Internal/IsInvocableR.h"
#include "FXDEngine/Core/Internal/IsPointer.h"
#include "FXDEngine/Core/Internal/IsMemberPointer.h"
#include "FXDEngine/Core/Internal/IsNoThrowMoveConstructible.h"
#include "FXDEngine/Core/Internal/IsSame.h"
#include "FXDEngine/Core/Internal/Negation.h"
#include "FXDEngine/Core/Internal/RemovePointer.h"
#include "FXDEngine/Memory/New.h"

/*
#define _NON_MEMBER_CALL2(FUNC, CV_OPT, REF_OPT, NOEXCEPT_OPT) \
	_EMIT_CDECL(FUNC, CV_OPT, REF_OPT, NOEXCEPT_OPT) \
	_EMIT_CLRCALL(FUNC, CV_OPT, REF_OPT, NOEXCEPT_OPT) \
	_EMIT_FASTCALL(FUNC, CV_OPT, REF_OPT, NOEXCEPT_OPT) \
	_EMIT_STDCALL(FUNC, CV_OPT, REF_OPT, NOEXCEPT_OPT) \
	_EMIT_VECTORCALL(FUNC, CV_OPT, REF_OPT, NOEXCEPT_OPT)
*/

namespace FXD
{
	namespace STD
	{
		template < typename T >
		class function;

		namespace Internal
		{
			CONSTEXPR size_t _Space_size = (FXD::STD::Internal::_Small_object_num_ptrs - 1) * sizeof(void*);

			template < typename Impl >
			struct _is_large : FXD::STD::integral_constant< bool, FXD::STD::Internal::_Space_size < sizeof(Impl) || !Impl::_nothrow_move::value >
			{};

			template < typename T >
			inline bool _Test_callable(const T& arg, FXD::STD::true_type) NOEXCEPT
			{
				return (!!arg);
			}

			template < typename T >
			inline bool _Test_callable(const T&, FXD::STD::false_type) NOEXCEPT
			{	// FXD::STD::function must store arbitrary callable objects
				return (true);
			}

			template < typename T >
			inline bool _Test_callable(const T& arg) NOEXCEPT
			{	// determine whether FXD::STD::function must store arg
				FXD::STD::integral_constant< bool, FXD::STD::is_member_pointer< T >::value || (FXD::STD::is_pointer< T >::value && FXD::STD::is_function< typename FXD::STD::remove_pointer< T >::type >::value) > _Testable;
				return (FXD::STD::Internal::_Test_callable(arg, _Testable));
			}

			template < typename T >
			inline bool _Test_callable(const FXD::STD::function< T >& arg) NOEXCEPT
			{	// determine whether FXD::STD::function must store arg
				return (!!arg);
			}

#		pragma warning(push)
#		pragma warning(disable: 4265)	// class has virtual functions, but destructor is not virtual
			// ------
			// _IFunctionBase
			// -
			// ------
			template < typename R, typename... Args >
			class _IFunctionBase
			{
			public:
				_IFunctionBase(void) = default;
				_IFunctionBase(const _IFunctionBase& /*rhs*/) = delete;
				_IFunctionBase& operator=(const _IFunctionBase&  /*rhs*/) = delete;
				// dtor non-virtual due to _delete_this()

				virtual _IFunctionBase* _copy(void*) const PURE;
				virtual _IFunctionBase* _move(void*) PURE;
				virtual R _do_call(Args&&...) PURE;
				virtual const std::type_info& _target_type(void) const NOEXCEPT PURE;
				virtual void _delete_this(bool) NOEXCEPT PURE;

#			if FXD_HAS_STATIC_RTTI
				const void* _target(const std::type_info& info) const NOEXCEPT
				{
					return (_target_type() == info ? _get() : nullptr);
				}
#			endif // FXD_HAS_STATIC_RTTI

			private:
				virtual const void* _get(void) const NOEXCEPT PURE;
			};
#		pragma warning(pop)

#		pragma warning(push)
#		pragma warning(disable: 4265)	// class has virtual functions, but destructor is not virtual

#			if FXD_HAS_FUNCTION_ALLOCATOR_SUPPORT
			// ------
			// _FunctionImpl
			// -
			// ------
			template < typename Callable, typename Alloc, typename R, typename... Args >
			class _FunctionImpl FINAL : public FXD::STD::Internal::_IFunctionBase< R, Args... >
			{
			public:
				using base_type = FXD::STD::Internal::_IFunctionBase< R, Args... >;
				using _Myalty = _Rebind_alloc_t< Alloc, FXD::STD::Internal::_FunctionImpl >;
				using _Myalty_traits = allocator_traits< _Myalty >;
				using _nothrow_move = FXD::STD::is_nothrow_move_constructible< Callable >;

			public:
				template < typename T, typename _Other2 >
				_FunctionImpl(T&& val, _Other2&& alloc)
					: _Mypair(FXD::STD::Internal::_One_then_variadic_args_t(), std::forward< _Other2 >(alloc), std::forward< T >(val))
				{}

				// dtor non-virtual due to _delete_this()

			private:
				virtual base_type* _copy(void* pWhere) const OVERRIDE
				{
					return (_copy_impl(pWhere, FXD::STD::Internal::_is_large< FXD::STD::Internal::_FunctionImpl >()));
				}

				base_type* _copy_impl(void*, FXD::STD::true_type) const // (dynamically allocated)
				{
					_Myalty alloc(_my_alloc());
					const auto pPtr = _Myalty_traits::allocate(alloc, 1);

					_TRY_BEGIN
						_Myalty_traits::construct(alloc, _Unfancy(pPtr), _callee(), _my_alloc());
					_CATCH_ALL
						_Myalty_traits::deallocate(alloc, pPtr, 1);
					_RERAISE;
					_CATCH_END

					return (_Unfancy(pPtr));
				}

				base_type* _copy_impl(void* pWhere, FXD::STD::false_type) const // (locally stored)
				{
					_Myalty alloc(_my_alloc());
					FXD::STD::Internal::_FunctionImpl* pPtr = static_cast<FXD::STD::Internal::_FunctionImpl* >(pWhere);
					_Myalty_traits::construct(alloc, pPtr, _callee(), _my_alloc());
					return (pPtr);
				}

				virtual base_type* _move(void* pWhere) OVERRIDE
				{
					_Myalty alloc(_my_alloc());
					FXD::STD::Internal::_FunctionImpl* pPtr = static_cast<FXD::STD::Internal::_FunctionImpl* >(pWhere);
					_Myalty_traits::construct(alloc, pPtr, std::move(_callee()), std::move(_my_alloc()));
					return (pPtr);
				}

				virtual R _do_call(Args&&... args) OVERRIDE
				{
					auto& callee = _callee();
					return (FXD::Internal::_invoker_ret< R >::_call(callee, std::forward< Args >(args)...));
				}

				virtual const std::type_info& _target_type(void) const NOEXCEPT OVERRIDE
				{
#			if FXD_HAS_STATIC_RTTI
					return (typeid(Callable));
#			else // FXD_HAS_STATIC_RTTI
					return (typeid(void));
#			endif // FXD_HAS_STATIC_RTTI
				}

				virtual const void* _get(void) const NOEXCEPT OVERRIDE
				{
					return (FXD::STD::addressof(_callee()));
				}

				virtual void _delete_this(bool bDeallocate) NOEXCEPT OVERRIDE
				{
					_Myalty alloc(_my_alloc());
					_Myalty_traits::destroy(alloc, this);
					if (bDeallocate)
					{
						_Deallocate_plain(alloc, this);
					}
				}

				Alloc& _my_alloc(void) NOEXCEPT
				{
					return (_Mypair._Get_first());
				}

				const Alloc& _my_alloc(void) const NOEXCEPT
				{
					return (_Mypair._Get_first());
				}

				Callable& _callee(void) NOEXCEPT
				{
					return (_Mypair._Get_second());
				}

				const Callable& _callee(void) const NOEXCEPT
				{
					return (_Mypair._Get_second());
				}

			private:
				_Compressed_pair< Alloc, Callable > _Mypair;
			};
#		endif // FXD_HAS_FUNCTION_ALLOCATOR_SUPPORT

			// ------
			// _FunctionNoAllocImpl
			// -
			// ------
			template < typename Callable, typename R, typename... Args >
			class _FunctionNoAllocImpl FINAL : public FXD::STD::Internal::_IFunctionBase< R, Args... >
			{
			public:
				using base_type = FXD::STD::Internal::_IFunctionBase< R, Args... >;
				using _nothrow_move = FXD::STD::is_nothrow_move_constructible< Callable >;

			public:
				template < typename Rhs, typename = typename FXD::STD::enable_if< !FXD::STD::is_same< _FunctionNoAllocImpl, typename FXD::STD::decay< Rhs >::type >::value >::type >
				EXPLICIT _FunctionNoAllocImpl(Rhs&& val)
					: m_callee(std::forward< Rhs >(val))
				{}

				// dtor non-virtual due to _delete_this()

			private:
				virtual base_type* _copy(void* pWhere) const OVERRIDE
				{
					return (_copy_impl(pWhere, FXD::STD::Internal::_is_large< _FunctionNoAllocImpl >()));
				}

				base_type* _copy_impl(void* /*pWhere*/, FXD::STD::true_type) const // (dynamically allocated)
				{
					//TODO_WARN("Use Align");
					return FXD_NEW(_FunctionNoAllocImpl)(std::move(m_callee));
				}

				base_type* _copy_impl(void* pWhere, FXD::STD::false_type) const // (locally stored)
				{
					return FXD_PLACEMENT_NEW(pWhere, _FunctionNoAllocImpl(m_callee));
				}

				virtual base_type* _move(void* pWhere) OVERRIDE
				{
					return FXD_PLACEMENT_NEW(pWhere, _FunctionNoAllocImpl(std::move(m_callee)));
				}

				virtual R _do_call(Args&&... args) OVERRIDE
				{
					return (FXD::Internal::_invoker_ret< R >::_call(m_callee, std::forward< Args >(args)...));
				}

				virtual const std::type_info& _target_type(void) const NOEXCEPT OVERRIDE
				{
#			if FXD_HAS_STATIC_RTTI
					return (typeid(Callable));
#			else // FXD_HAS_STATIC_RTTI
					return (typeid(void));
#			endif // FXD_HAS_STATIC_RTTI
				}

				virtual const void* _get(void) const NOEXCEPT OVERRIDE
				{
					return (FXD::STD::addressof(m_callee));
				}

				virtual void _delete_this(bool bDealloc) NOEXCEPT OVERRIDE
				{
					this->~_FunctionNoAllocImpl();
					if (bDealloc)
					{
						//TODO_WARN("Use Align");
						FXD_DELETE(this);
					}
				}

			private:
				Callable m_callee;
			};
#		pragma warning(pop)

			// ------
			// _FunctionClass
			// -
			// ------
			template < typename R, typename... Args >
			class _FunctionClass : public FXD::STD::Internal::_Arg_types< Args... >
			{
			public:
				using result_type = R;
				using ptr_type = FXD::STD::Internal::_IFunctionBase< R, Args... >;

			public:
				_FunctionClass(void) NOEXCEPT
				{
					_set(nullptr);
				}

				R operator()(Args... args) const
				{
					if (_is_empty())
					{
						FXD::STD::bad_function_call();
					}
					return (_get_impl()->_do_call(std::forward< Args >(args)...));
				}

				~_FunctionClass(void) NOEXCEPT
				{
					_tidy();
				}

			protected:
				template < typename T, typename _Function >
				using _Enable_if_callable_t = typename FXD::STD::enable_if< FXD::STD::conjunction< FXD::STD::negation< FXD::STD::is_same< typename FXD::STD::decay< T >::type, _Function > >, FXD::STD::Internal::_is_invocable_r< R, T, Args... > >::value >::type;

				bool _is_empty(void) const NOEXCEPT
				{
					return (_get_impl() == nullptr);
				}

				void _reset_copy(const _FunctionClass& rhs)
				{
					if (!rhs._is_empty())
					{
						_set(rhs._get_impl()->_copy(_get_space()));
					}
				}

				void _reset_move(_FunctionClass&& rhs)
				{
					if (!rhs._is_empty())
					{
						if (rhs._is_local())
						{	// move and tidy
							_set(rhs._get_impl()->_move(_get_space()));
							rhs._tidy();
						}
						else
						{	// steal from rhs
							_set(rhs._get_impl());
							rhs._set(nullptr);
						}
					}
				}

				template < typename T >
				void _reset(T&& val)
				{
					if (!FXD::STD::Internal::_Test_callable(val))
					{
						return;	// empty
					}

					using Impl = FXD::STD::Internal::_FunctionNoAllocImpl< typename FXD::STD::decay< T >::type, R, Args... >;
					_reset_impl< Impl >(std::forward< T >(val), FXD::STD::Internal::_is_large< Impl >());
				}

				template < typename MyImpl, typename T >
				void _reset_impl(T&& val, FXD::STD::true_type) // (dynamically allocated)
				{
					//TODO_WARN("Use Align");
					_set(FXD_NEW(MyImpl)(val));
				}

				template < typename MyImpl, typename T >
				void _reset_impl(T&& val, FXD::STD::false_type)
				{
					_set(FXD_PLACEMENT_NEW(_get_space(), MyImpl(std::forward< T >(val))));
				}

#			if FXD_HAS_FUNCTION_ALLOCATOR_SUPPORT
				template < typename T, typename Alloc >
				void _reset_alloc(T&& val, const Alloc& alloc)
				{
					if (!FXD::STD::Internal::_Test_callable(val))
					{
						return;	// is empty
					}

					using MyImpl = FXD::STD::Internal::_FunctionImpl< typename FXD::STD::decay< T >::type, Alloc, R, Args... >;
					MyImpl* pPtr = nullptr;

					_Rebind_alloc_t< Alloc, MyImpl > alloc(alloc);
					_reset_alloc_impl(std::forward< T >(val), alloc, pPtr, alloc, FXD::STD::Internal::_is_large< MyImpl >());
				}

				template < typename T, typename Alloc, typename MyImpl, typename _Alimpl >
				void _reset_alloc_impl(T&& val, const Alloc& alloc, MyImpl*, _Alimpl& alloc, FXD::STD::true_type)
				{
					using _Alimpl_traits = allocator_traits< _Alimpl >;
					const auto pPtr = _Alimpl_traits::allocate(alloc, 1);

					_TRY_BEGIN
						_Alimpl_traits::construct(alloc, _Unfancy(pPtr), std::forward< T >(val), alloc);
					_CATCH_ALL
						_Alimpl_traits::deallocate(alloc, pPtr, 1);
					_RERAISE;
					_CATCH_END

					_set(_Unfancy(pPtr));
				}

				template < typename T, typename Alloc, typename MyImpl, typename _Alimpl >
				void _reset_alloc_impl(T&& val, const Alloc& alloc, MyImpl*, _Alimpl& alloc, FXD::STD::false_type)
				{
					MyImpl* pPtr = static_cast< MyImpl* >(_get_space());
					allocator_traits< _Alimpl >::construct(alloc, pPtr, std::forward< T >(val), alloc);
					_set(pPtr);
				}
#			endif // FXD_HAS_FUNCTION_ALLOCATOR_SUPPORT

				void _tidy(void) NOEXCEPT
				{
					if (!_is_empty())
					{	// destroy callable object and maybe delete it
						_get_impl()->_delete_this(!_is_local());
						_set(nullptr);
					}
				}

				void _swap(_FunctionClass& rhs) NOEXCEPT
				{
					if (!_is_local() && !rhs._is_local())
					{	// just swap pointers
						ptr_type* pTemp = _get_impl();
						_set(rhs._get_impl());
						rhs._set(pTemp);
					}
					else
					{	// do three-way move
						_FunctionClass tmp;
						tmp._reset_move(std::move(*this));
						_reset_move(std::move(rhs));
						rhs._reset_move(std::move(tmp));
					}
				}

#			if FXD_HAS_STATIC_RTTI
				const std::type_info& _target_type(void) const NOEXCEPT
				{
					return (_get_impl() ? _get_impl()->_target_type() : typeid(void));
				}

				const void* _target(const std::type_info& info) const NOEXCEPT
				{
					return (_get_impl() ? _get_impl()->_target(info) : nullptr);
				}
#			endif // FXD_HAS_STATIC_RTTI

			private:
				bool _is_local(void) const NOEXCEPT
				{
					return (_get_impl() == _get_space());
				}

				ptr_type* _get_impl(void) const NOEXCEPT
				{
					return (m_storage.m_pPtrs[FXD::STD::Internal::_Small_object_num_ptrs - 1]);
				}

				void _set(ptr_type* pPtr) NOEXCEPT
				{
					m_storage.m_pPtrs[FXD::STD::Internal::_Small_object_num_ptrs - 1] = pPtr;
				}

				void* _get_space(void) NOEXCEPT
				{
					return (&m_storage);
				}

				const void* _get_space(void) const NOEXCEPT
				{
					return (&m_storage);
				}

			private:
				union _Storage
				{	// storage for small objects (basic_string is small)
					std::max_align_t m_dummy1;													// for maximum alignment
					FXD::U8 m_dummy2[FXD::STD::Internal::_Space_size];					// to permit aliasing
					ptr_type* m_pPtrs[FXD::STD::Internal::_Small_object_num_ptrs];	// m_pPtrs[FXD::Internal::_Small_object_num_ptrs - 1] is reserved
				};

				_Storage m_storage;
				enum // Helper for expression evaluator
				{
					_FEEN_IMPL = FXD::STD::Internal::_Small_object_num_ptrs - 1
				};
			};

			// ------
			// _get_function_impl
			// -
			// ------
			template < class R >
			struct _get_function_impl;
			/*
#	define _GET_FUNCTION_IMPL(CALL_OPT, X1, X2, X3)								\
			template < typename R, typename... A >									\
			struct _get_function_impl< R CALL_OPT (A...) >						\
			{																					\
				using type = FXD::STD::Internal::_FunctionClass< R, A... >;	\
			};

			_NON_MEMBER_CALL2(_GET_FUNCTION_IMPL, X1, X2, X3)
#	undef _GET_FUNCTION_IMPL
				*/


			template < typename R, typename... Args >
			struct _get_function_impl< R(Args...) >
			{
				using type = FXD::STD::Internal::_FunctionClass< R, Args... >;
			};

		} //namespace Internal

		// ------
		// function
		// -
		// ------
		template < typename FT >
		class function : public FXD::STD::Internal::_get_function_impl< FT >::type
		{
		private:
			using base_type = typename FXD::STD::Internal::_get_function_impl< FT >::type;

		public:
			function(void) NOEXCEPT
			{}

			function(std::nullptr_t) NOEXCEPT
			{}

			function(const function& rhs)
			{
				this->_reset_copy(rhs);
			}

			template < typename T, typename = typename base_type::template _Enable_if_callable_t< T&, function > >
			function(T func)
			{
				this->_reset(std::move(func));
			}

#	if FXD_HAS_FUNCTION_ALLOCATOR_SUPPORT
			template < typename Alloc >
			function(allocator_arg_t, const Alloc&) NOEXCEPT
			{}

			template < typename Alloc >
			function(allocator_arg_t, const Alloc&, std::nullptr_t) NOEXCEPT
			{}

			template < typename Alloc >
			function(allocator_arg_t, const Alloc& alloc, const FXD::STD::function& rhs)
			{
				_reset_alloc(rhs, alloc);
			}

			template < typename T, typename Alloc, typename = typename base_type::template _Enable_if_callable_t< T&, FXD::STD::function > >
			function(allocator_arg_t, const Alloc& alloc, T func)
			{
				_reset_alloc(std::move(func), alloc);
			}
#	endif // FXD_HAS_FUNCTION_ALLOCATOR_SUPPORT

			function& operator=(const function& rhs)
			{
				function(rhs).swap(*this);
				return (*this);
			}

			function(function&& rhs)
			{
				this->_reset_move(std::move(rhs));
			}

#	if FXD_HAS_FUNCTION_ALLOCATOR_SUPPORT
			template < typename Alloc >
			function(allocator_arg_t, const Alloc& alloc, FXD::STD::function&& rhs)
			{
				_reset_alloc(std::move(rhs), alloc);
			}
#	endif // FXD_HAS_FUNCTION_ALLOCATOR_SUPPORT

			function& operator=(function&& rhs)
			{
				if (this != FXD::STD::addressof(rhs))
				{
					this->_tidy();
					this->_reset_move(std::move(rhs));
				}
				return (*this);
			}

			template < typename T, typename = typename base_type::template _Enable_if_callable_t< typename FXD::STD::decay< T >::type&, FXD::STD::function< T > > >
			function& operator=(T&& func)
			{
				function(std::forward< T >(func)).swap(*this);
				return (*this);
			}

#	if FXD_HAS_FUNCTION_ALLOCATOR_SUPPORT
			template < typename T, typename Alloc >
			void assign(T&& func, const Alloc& alloc)
			{
				function(allocator_arg, alloc, std::forward< T >(func)).swap(*this);
			}
#	endif // FXD_HAS_FUNCTION_ALLOCATOR_SUPPORT

			function& operator=(std::nullptr_t) NOEXCEPT
			{
				this->_tidy();
				return (*this);
			}

			template < typename T >
			function& operator=(FXD::STD::reference_wrapper< T > func) NOEXCEPT
			{
				this->_tidy();
				this->_reset(func);
				return (*this);
			}

			void swap(function& rhs) NOEXCEPT
			{
				this->_swap(rhs);
			}

			EXPLICIT operator bool(void) const NOEXCEPT
			{
				return (!this->_is_empty());
			}

#	if FXD_HAS_STATIC_RTTI
			const std::type_info& target_type(void) const NOEXCEPT
			{
				return (_target_type());
			}

			template < typename T >
			T* target(void) NOEXCEPT
			{
				return (reinterpret_cast< T* >(const_cast< void* >(_target(typeid(T)))));
			}

			template < typename T >
			const T* target(void) const NOEXCEPT
			{
				return (reinterpret_cast< const T* >(_target(typeid(T))));
			}

#	else // FXD_HAS_STATIC_RTTI
			const std::type_info& target_type(void) const NOEXCEPT = delete;	// requires static RTTI

			template < typename T >
			T* target(void) NOEXCEPT = delete;	// requires static RTTI

			template < typename T >
			const T* target(void) const NOEXCEPT = delete;	// requires static RTTI
#	endif // FXD_HAS_STATIC_RTTI
		};

#	if FXD_HAS_CXX17
#	define _FUNCTION_POINTER_DEDUCTION_GUIDE(CALL_OPT, X1, X2, X3)		\
		template < typename R, typename... Args >								\
		function(R (CALL_OPT*)(Args...))-> function< R (Args...) >;	// intentionally discards CALL_OPT

		_NON_MEMBER_CALL(_FUNCTION_POINTER_DEDUCTION_GUIDE, X1, X2, X3)
#	undef _FUNCTION_POINTER_DEDUCTION_GUIDE

		// STRUCT TEMPLATE _Deduce_signature
		template < typename T, typename = void >
		struct _Deduce_signature
		{};

		template < typename T >
		struct _Deduce_signature< T, FXD::STD::void_t< decltype(&T::operator())>> : FXD::STD::Internal::_is_memfunptr<decltype(&T::operator()) >::_Guide_type
		{	// N4687 23.14.13.2.1 [func.wrap.func.con]/12
		};

		template < typename T >
		function(T)->function< typename _Deduce_signature< T >::type >;
#	endif // FXD_HAS_CXX17

		template < typename T >
		inline bool operator==(const ::FXD::STD::function< T >& lhs, std::nullptr_t) NOEXCEPT
		{
			return (!lhs);
		}

		template < typename T >
		inline bool operator==(std::nullptr_t, const FXD::STD::function< T >& rhs) NOEXCEPT
		{
			return (!rhs);
		}

		template < typename T >
		inline bool operator!=(const FXD::STD::function< T >& lhs, std::nullptr_t) NOEXCEPT
		{
			return (static_cast< bool >(lhs));
		}

		template < typename T >
		inline bool operator!=(std::nullptr_t, const FXD::STD::function< T >& rhs) NOEXCEPT
		{
			return (static_cast< bool >(rhs));
		}

		template < typename T >
		inline void swap(FXD::STD::function< T >& lhs, FXD::STD::function< T >& rhs) NOEXCEPT
		{
			lhs.swap(rhs);
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_FUNCTION_H