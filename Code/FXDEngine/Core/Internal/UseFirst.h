#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_USEFIRST_H
#define FXDENGINE_CORE_INTERNAL_USEFIRST_H

namespace FXD
{
	namespace STD
	{
		// UseFirst
		template < typename T >
		class UseFirst
		{
		public:
			using result_type = typename T::first_type;

		public:
			const result_type& operator()(const T& val) const
			{
				return val.first;
			}
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_USEFIRST_H