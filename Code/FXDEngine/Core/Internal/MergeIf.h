#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MERGEIF_H
#define FXDENGINE_CORE_INTERNAL_MERGEIF_H

#include "FXDEngine/Core/Internal/Copy.h"

namespace FXD
{
	namespace STD
	{
		// merge_if
#		define FXD_SUPPORTS_MERGE_IF 1

		template < typename InputItr1, typename InputItr2, typename OutputItr, typename Predicate >
		inline OutputItr merge_if(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, OutputItr dst, Predicate pred)
		{
			while ((first1 != last1) && (first2 != last2))
			{
				if (pred(*first2, *first1))
				{
					*dst = *first2;
					++first2;
				}
				else
				{
					*dst = *first1;
					++first1;
				}
				++dst;
			}

			if (first1 == last1)
			{
				return FXD::STD::copy(first2, last2, dst);
			}
			else
			{
				return FXD::STD::copy(first1, last1, dst);
			}
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MERGEIF_H