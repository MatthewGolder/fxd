#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_DIVIDES_H
#define FXDENGINE_CORE_INTERNAL_DIVIDES_H

#include "FXDEngine/Core/Types.h"
#include <type_traits>

namespace FXD
{
	namespace STD
	{
		// divides
#		define FXD_SUPPORTS_DIVIDES 1

		template < typename T = void >
		struct divides
		{
			using first_argument_type = T;
			using second_argument_type = T;
			using result_type = T;

			CONSTEXPR14 T operator()(const T& lhs, const T& rhs)const
			{
				return (lhs / rhs);
			}
		};

		template <>
		struct divides< void >
		{
			using is_transparent = FXD::S32;

			template < typename LHS, typename RHS >
			CONSTEXPR14 auto operator()(LHS&& lhs, RHS&& rhs)const->decltype(std::forward< LHS >(lhs) / std::forward< RHS >(rhs))
			{
				return std::forward< LHS >(lhs) / std::forward< RHS >(rhs);
			}
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_DIVIDES_H