#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_FINDFIRSTOFIF_H
#define FXDENGINE_CORE_INTERNAL_FINDFIRSTOFIF_H

namespace FXD
{
	namespace STD
	{
		// find_first_of_if
#		define FXD_SUPPORTS_FIND_FIRST_OF_IF 1

		template < typename InputItr, typename ForwardItr, typename Predicate >
		InputItr find_first_of_if(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, Predicate pred)
		{
			for (; first1 != last1; ++first1)
			{
				for (ForwardItr itr = first2; itr != last2; ++itr)
				{
					if (pred(*first1, *itr))
					{
						return first1;
					}
				}
			}
			return last1;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_FINDFIRSTOFIF_H