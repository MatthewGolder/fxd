#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_USESECOND_H
#define FXDENGINE_CORE_INTERNAL_USESECOND_H

namespace FXD
{
	namespace STD
	{
		// UseSecond
		template < typename T >
		class UseSecond
		{
		public:
			using result_type = typename T::second_type;

		public:
			const result_type& operator()(const T& val) const
			{
				return val.second;
			}
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_USESECOND_H