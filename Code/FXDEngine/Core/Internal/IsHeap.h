#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISHEAP_H
#define FXDENGINE_CORE_INTERNAL_ISHEAP_H

#include "FXDEngine/Core/Internal/IsHeapIf.h"

namespace FXD
{
	namespace STD
	{
		// is_heap
#		define FXD_SUPPORTS_IS_HEAP_IF 1

		template < typename RandomAccessItr, typename Predicate >
		inline bool is_heap(RandomAccessItr first, RandomAccessItr last, Predicate pred)
		{
			return FXD::STD::is_heap_if(first, last, pred);
		}

		template < typename RandomAccessItr >
		inline bool is_heap(RandomAccessItr first, RandomAccessItr last)
		{
			return FXD::STD::is_heap(first, last, FXD::STD::less<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISHEAP_H