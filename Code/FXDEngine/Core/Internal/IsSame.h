#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISSAME_H
#define FXDENGINE_CORE_INTERNAL_ISSAME_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// is_same
#		define FXD_SUPPORTS_IS_SAME 1

		template < typename T1, typename T2 >
		struct is_same : public FXD::STD::false_type
		{};

		template < typename T1 >
		struct is_same< T1, T1 > : public FXD::STD::true_type
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T, typename U >
		CONSTEXPR bool is_same_v = FXD::STD::is_same< T, U >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISSAME_H