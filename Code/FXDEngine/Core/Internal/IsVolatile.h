#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISVOLATILE_H
#define FXDENGINE_CORE_INTERNAL_ISVOLATILE_H

#include "FXDEngine/Core/Internal/Constants.h"

namespace FXD
{
	namespace STD
	{
		// is_volatile
#		define FXD_SUPPORTS_IS_VOLATILE 1

		template < typename T >
		struct is_volatile : FXD::STD::false_type
		{};

		template < typename T >
		struct is_volatile< volatile T > : FXD::STD::true_type
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_volatile_v = FXD::STD::is_volatile< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISVOLATILE_H