#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_MAX_H
#define FXDENGINE_CORE_INTERNAL_MAX_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Less.h"
#include "FXDEngine/Core/Internal/MaxElement.h"

namespace FXD
{
	namespace STD
	{
		// Max
#		define FXD_SUPPORTS_MAX 1

		template < typename T, typename Predicate >
		CONSTEXPR14 const T& Max(const T& lhs, const T& rhs, Predicate pred)
		{
			return pred(lhs, rhs) ? rhs : lhs;
		}

		template < typename T >
		CONSTEXPR14 const T& Max(const T& lhs, const T& rhs)
		{
			return (lhs < rhs) ? rhs : lhs;
		}

		template < typename T, typename Predicate >
		CONSTEXPR14 T Max(std::initializer_list< T > iList, Predicate pred)
		{
			return *FXD::STD::max_element(iList.begin(), iList.end(), pred);
		}

		template < typename T >
		CONSTEXPR14 T Max(std::initializer_list< T > iList)
		{
			return *FXD::STD::max_element(iList.begin(), iList.end(), FXD::STD::less<>());
		}


		// Max_Tuple
		template < typename T >
		CONSTEXPR14 const T& Max_Tuple(const T& lhs, const T& rhs)
		{
			return (lhs > rhs) ? lhs : rhs;
		}

		template < typename T, typename... Args >
		CONSTEXPR14 const T& Max_Tuple(const T& lhs, const T& rhs, const Args&... args)
		{
			return FXD::STD::Max_Tuple(FXD::STD::Max_Tuple(lhs, rhs), args...);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_MAX_H