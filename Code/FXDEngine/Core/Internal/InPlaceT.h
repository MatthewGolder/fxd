#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_INPLACET_H
#define FXDENGINE_CORE_INTERNAL_INPLACET_H

#include "FXDEngine/Core/Internal/Constants.h"

namespace FXD
{
	namespace STD
	{
		// in_place_t
#		define FXD_SUPPORTS_IN_PLACE 1

		// ------
		// in_place_t
		// - tag used to select a constructor which initializes a contained object in place
		// ------
		struct in_place_t
		{
			EXPLICIT in_place_t(void) = default;
		};
		CONSTEXPR FXD::STD::in_place_t in_place {};

		// ------
		// in_place_type_t
		// - tag that selects a type to construct in place
		// ------
		template < typename >
		struct in_place_type_t
		{
			EXPLICIT in_place_type_t(void) = default;
		};
		template < typename T>
		CONSTEXPR FXD::STD::in_place_type_t< T > in_place_type {};

		// ------
		// in_place_index_t
		// - tag that selects the index of a type to construct in place
		// ------
		template < size_t >
		struct in_place_index_t
		{
			EXPLICIT in_place_index_t(void) = default;
		};
		template < size_t Idx >
		CONSTEXPR FXD::STD::in_place_index_t< Idx > in_place_index {};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_INPLACET_H