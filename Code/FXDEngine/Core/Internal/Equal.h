#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_EQUAL_H
#define FXDENGINE_CORE_INTERNAL_EQUAL_H

#include "FXDEngine/Core/Internal/EqualIf.h"
#include "FXDEngine/Core/Internal/EqualTo.h"

namespace FXD
{
	namespace STD
	{
		// equal
#		define FXD_SUPPORTS_EQUAL 1

		template < typename InputItr1, typename InputItr2, typename Predicate >
		CONSTEXPR14 inline bool equal(InputItr1 first1, InputItr2 last1, InputItr2 first2, InputItr2 last2, Predicate pred)
		{
			return FXD::STD::equal_if(first1, last1, first2, last2, pred);
		}

		template < typename InputItr1, typename InputItr2 >
		CONSTEXPR14 inline bool equal(InputItr2 first1, InputItr2 last1, InputItr2 first2, InputItr2 last2)
		{
			return FXD::STD::equal(first1, last1, first2, last2, FXD::STD::equal_to<>());
		}

		template < typename InputItr1, typename InputItr2, typename Predicate >
		CONSTEXPR14 inline bool equal(InputItr1 first1, InputItr1 last, InputItr2 first2, Predicate pred)
		{
			return FXD::STD::equal_if(first1, last, first2, pred);
		}

		template < typename InputItr1, typename InputItr2 >
		CONSTEXPR14 inline bool equal(InputItr1 first1, InputItr1 last, InputItr2 first2)
		{
			return FXD::STD::equal(first1, last, first2, FXD::STD::equal_to<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_EQUAL_H