// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_CREF_H
#define FXDENGINE_CORE_INTERNAL_CREF_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/ReferenceWrapper.h"

namespace FXD
{
	namespace STD
	{
		// cref
#		define FXD_SUPPORTS_CREF 1

		template < typename T >
		inline FXD::STD::reference_wrapper< const T > cref(const T& val) NOEXCEPT
		{
			return FXD::STD::reference_wrapper< const T >(val);
		}

		template < typename T >
		inline FXD::STD::reference_wrapper< const T > cref(FXD::STD::reference_wrapper< T > val) NOEXCEPT
		{
			return FXD::STD::cref(val.get());
		}

		template < typename T >
		void cref(const T&&) = delete;

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_CREF_H