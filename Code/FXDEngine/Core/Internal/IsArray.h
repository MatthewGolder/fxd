#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISARRAY_H
#define FXDENGINE_CORE_INTERNAL_ISARRAY_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Constants.h"

namespace FXD
{
	namespace STD
	{
		// is_array
#		define FXD_SUPPORTS_IS_ARRAY 1

		template < typename T >
		struct is_array : public FXD::STD::false_type
		{};

		template < typename T >
		struct is_array< T[] > : FXD::STD::true_type
		{};

		template < typename T, std::size_t SIZE >
		struct is_array< T[SIZE] > : public FXD::STD::true_type
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_array_v = FXD::STD::is_array< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISARRAY_H