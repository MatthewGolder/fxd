#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_FOREACHN_H
#define FXDENGINE_CORE_INTERNAL_FOREACHN_H

#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// for_each_n
#		if IsCPP17()
#		define FXD_SUPPORTS_FOR_EACH_N 1

		template < typename InputItr, typename Size, typename Predicate >
		inline InputItr for_each_n(InputItr first, Size nCount, Predicate pred)
		{
			for (Size n1 = 0; n1 < nCount; ++first, (void) ++n1)
			{
				pred(*first);
			}
			return first;
		}
#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_FOREACHN_H