#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISSTANDARDLAYOUT_H
#define FXDENGINE_CORE_INTERNAL_ISSTANDARDLAYOUT_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/IsScalar.h"
#include "FXDEngine/Core/Internal/IsVoid.h"

namespace FXD
{
	namespace STD
	{
		// is_standard_layout
#		define FXD_SUPPORTS_IS_STANDARD_LAYOUT 1

		template < typename T >
		struct is_standard_layout : public FXD::STD::integral_constant< bool, __is_standard_layout(T) || FXD::STD::is_void< T >::value || FXD::STD::is_scalar< T >::value >
		{};

#if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_standard_layout_v = FXD::STD::is_standard_layout< T >::value;

#endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISSTANDARDLAYOUT_H