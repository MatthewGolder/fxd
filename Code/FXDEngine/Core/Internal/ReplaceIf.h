#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_REPLACEIF_H
#define FXDENGINE_CORE_INTERNAL_REPLACEIF_H

#include "FXDEngine/Core/Internal/EqualTo.h"

namespace FXD
{
	namespace STD
	{
		// replace_if
#		define FXD_SUPPORTS_REPLACE_IF 1

		template < typename ForwardItr, typename Predicate, typename T >
		inline void replace_if(ForwardItr first, ForwardItr last, Predicate pred, const T& val)
		{
			for (; first != last; ++first)
			{
				if (pred(*first))
				{
					*first = val;
				}
			}
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_REPLACEIF_H