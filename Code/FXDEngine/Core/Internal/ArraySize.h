#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ARRAYSIZE_H
#define FXDENGINE_CORE_INTERNAL_ARRAYSIZE_H

#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// array_size
#		define FXD_SUPPORTS_ARRAY_SIZE 1

		template < typename T, size_t SIZE >
		CONSTEXPR inline size_t array_size(T(&arr)[SIZE])
		{
			return SIZE;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ARRAYSIZE_H