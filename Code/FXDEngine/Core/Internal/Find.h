#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_FIND_H
#define FXDENGINE_CORE_INTERNAL_FIND_H

#include "FXDEngine/Core/Internal/FindIf.h"
#include "FXDEngine/Core/Internal/EqualTo.h"

namespace FXD
{
	namespace STD
	{
		// find
#		define FXD_SUPPORTS_FIND 1

		template < typename InputItr, typename T, typename Predicate >
		inline InputItr find(InputItr first, InputItr last, const T& val, Predicate pred)
		{
			return FXD::STD::find_if(first, last, [&](const T& x) { return pred(x, val); });
		}

		template < typename InputItr, typename T >
		inline InputItr find(InputItr first, InputItr last, const T& val)
		{
			return FXD::STD::find(first, last, val, FXD::STD::equal_to<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_FIND_H