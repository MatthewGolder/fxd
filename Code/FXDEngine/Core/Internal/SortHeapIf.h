#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_SORTHEAPIF_H
#define FXDENGINE_CORE_INTERNAL_SORTHEAPIF_H

#include "FXDEngine/Core/Internal/PopHeap.h"

namespace FXD
{
	namespace STD
	{
		// sort_heap_if
#		define FXD_SUPPORTS_SORT_HEAP_IF 1

		template < typename RandomAccessItr, typename Predicate >
		inline void sort_heap_if(RandomAccessItr first, RandomAccessItr last, Predicate pred)
		{
			for (; (last - first) > 1; --last) // We simply use the heap to sort itself.
			{
				FXD::STD::pop_heap< RandomAccessItr, Predicate >(first, last, pred);
			}
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_SORTHEAPIF_H