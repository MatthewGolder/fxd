#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ENABLEIF_H
#define FXDENGINE_CORE_INTERNAL_ENABLEIF_H

namespace FXD
{
	namespace STD
	{
		// enable_if
#		define FXD_SUPPORTS_ENABLE_IF 1

		template < bool Test, typename T = void >
		struct enable_if
		{};

		template < typename T >
		struct enable_if< true, T >
		{
			using type = T;
		};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < bool B, typename T = void >
		using enable_if_t = typename FXD::STD::enable_if< B, T >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ENABLEIF_H