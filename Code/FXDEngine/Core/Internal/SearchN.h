#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_SEARCHN_H
#define FXDENGINE_CORE_INTERNAL_SEARCHN_H

#include "FXDEngine/Core/Internal/SearchNIf.h"

namespace FXD
{
	namespace STD
	{
		// search_n
#		define FXD_SUPPORTS_SEARCH_N 1

		template < typename ForwardItr, typename Size, typename T, typename Predicate >
		inline ForwardItr search_n(ForwardItr first, ForwardItr last, Size nCount, const T& val, Predicate pred)
		{
			return FXD::STD::search_n_if(first, last, nCount, val, pred);
		}

		template < typename ForwardItr, typename Size, typename T >
		inline ForwardItr search_n(ForwardItr first, ForwardItr last, Size nCount, const T& val)
		{
			return FXD::STD::search_n(first, last, nCount, val, FXD::STD::equal_to<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_SEARCHN_H