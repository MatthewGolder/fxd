// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISPLACEHOLDER_H
#define FXDENGINE_CORE_INTERNAL_ISPLACEHOLDER_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Placeholders.h"

namespace FXD
{
	namespace STD
	{
		// is_placeholder
#		define FXD_SUPPORTS_IS_PLACEHOLDER 1

		template < typename T >
		struct is_placeholder : FXD::STD::integral_constant< FXD::S32, 0 >
		{};

		template < FXD::S32 N >
		struct is_placeholder< FXD::STD::_Ph< N > > : FXD::STD::integral_constant< FXD::S32, N >
		{};

		template < typename T >
		struct is_placeholder< const T > : FXD::STD::is_placeholder< T >::type
		{};

		template < typename T >
		struct is_placeholder< volatile T > : FXD::STD::is_placeholder< T >::type
		{};

		template < typename T >
		struct is_placeholder< const volatile T > : FXD::STD::is_placeholder< T >::type
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR FXD::S32 is_placeholder_v = is_placeholder< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISPLACEHOLDER_H