#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_INVOKERESULT_H
#define FXDENGINE_CORE_INTERNAL_INVOKERESULT_H

#include "FXDEngine/Core/Internal/InvokeHelper.h"

namespace FXD
{
	namespace STD
	{
		template < typename C, typename... Args >
		struct invoke_result : FXD::STD::Internal::_invoke_traits< void, C, Args... >
		{};

#		if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED

		template < typename C, typename... Args >
		using invoke_result_t = typename FXD::STD::invoke_result< C, Args... >::type;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_INVOKERESULT_H