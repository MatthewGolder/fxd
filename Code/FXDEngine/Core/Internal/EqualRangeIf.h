#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_EQUALRANGEIF_H
#define FXDENGINE_CORE_INTERNAL_EQUALRANGEIF_H

#include "FXDEngine/Container/Internal/Distance.h"
#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Pair.h"
#include "FXDEngine/Core/Internal/LowerBound.h"
#include "FXDEngine/Core/Internal/UpperBound.h"

namespace FXD
{
	namespace STD
	{
		// equal_range_if
#		define FXD_SUPPORTS_EQUAL_RANGE_IF 1

		template < typename ForwardItr, typename T, typename Predicate >
		inline FXD::Core::Pair< ForwardItr, ForwardItr > equal_range_if(ForwardItr first, ForwardItr last, const T& val, Predicate pred)
		{
			using ResultType = FXD::Core::Pair< ForwardItr, ForwardItr >;
			using DifferenceType = typename FXD::STD::iterator_traits< ForwardItr >::difference_type;

			DifferenceType dist = FXD::STD::distance(first, last);

			while (dist > 0)
			{
				ForwardItr itr(first);
				DifferenceType dist2 = dist >> 1;

				FXD::STD::advance(itr, dist2);
				if (pred(*itr, val))
				{
					PRINT_COND_ASSERT(pred(val, *itr) == false, "");
					first = ++itr;
					dist -= dist2 + 1;
				}
				else if (pred(val, *itr))
				{
					PRINT_COND_ASSERT(pred(*itr, val) == false, "");
					dist = dist2;
					last = itr;
				}
				else
				{
					ForwardItr itr2(itr);
					return ResultType(FXD::STD::lower_bound(first, itr, val, pred), FXD::STD::upper_bound(++itr2, last, val, pred));
				}
			}
			return ResultType(first, first);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_EQUALRANGEIF_H