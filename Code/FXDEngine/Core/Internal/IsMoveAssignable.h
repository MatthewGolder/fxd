#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISMOVEASSIGNABLE_H
#define FXDENGINE_CORE_INTERNAL_ISMOVEASSIGNABLE_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/IsAssignable.h"
#include "FXDEngine/Core/Internal/AddLValueReference.h"
#include "FXDEngine/Core/Internal/AddRValueReference.h"

namespace FXD
{
	namespace STD
	{
		// is_move_assignable
#		define FXD_SUPPORTS_IS_MOVE_ASSIGNABLE 1

		template < typename T >
		struct is_move_assignable : public FXD::STD::is_assignable< typename FXD::STD::add_lvalue_reference< T >::type, typename FXD::STD::add_rvalue_reference< T >::type >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool is_move_assignable_v = FXD::STD::is_move_assignable< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISMOVEASSIGNABLE_H