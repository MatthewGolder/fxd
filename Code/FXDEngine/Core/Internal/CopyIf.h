#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_COPYIF_H
#define FXDENGINE_CORE_INTERNAL_COPYIF_H

#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// copy_if
#		define FXD_SUPPORTS_COPY_IF 1

		template < typename InputItr, typename OutputItr, typename Predicate >
		inline OutputItr copy_if(InputItr first, InputItr last, OutputItr dst, Predicate pred)
		{
			for (; first != last; ++first)
			{
				if (pred(*first))
				{
					*dst++ = (*first);
				}
			}
			return dst;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_COPYIF_H