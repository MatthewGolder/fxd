#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_NEGATION_H
#define FXDENGINE_CORE_INTERNAL_NEGATION_H

#include "FXDEngine/Core/Internal/Constants.h"

namespace FXD
{
	namespace STD
	{
		// negation
#		define FXD_SUPPORTS_NEGATION 1

		template < typename T >
		struct negation : FXD::STD::integral_constant< bool, !bool(T::value) >
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T >
		CONSTEXPR bool negation_v = FXD::STD::negation< T >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_NEGATION_H