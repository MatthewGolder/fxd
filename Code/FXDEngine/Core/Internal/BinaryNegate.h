// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_BINARYNEGATE_H
#define FXDENGINE_CORE_INTERNAL_BINARYNEGATE_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/BinaryFunction.h"

namespace FXD
{
	namespace STD
	{
		// binary_negate
#		define FXD_SUPPORTS_BINARY_NEGATE 1

		template < typename Predicate >
		class binary_negate : public FXD::STD::binary_function< typename Predicate::first_argument_type, typename Predicate::second_argument_type, bool >
		{
		public:
			using first_argument_type = typename Predicate::first_argument_type;
			using second_argument_type = typename Predicate::second_argument_type;
			using result_type = bool;

			EXPLICIT binary_negate(const Predicate& pred)
				: m_pred(pred)
			{}

			CONSTEXPR14 bool operator()(const typename Predicate::first_argument_type& lhs, const typename Predicate::second_argument_type& rhs) const
			{
				return !m_pred(lhs, rhs);
			}

		protected:
			Predicate m_pred;
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_BINARYNEGATE_H