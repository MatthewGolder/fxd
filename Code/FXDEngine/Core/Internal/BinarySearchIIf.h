// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_BINARYSEARCHIIF_H
#define FXDENGINE_CORE_INTERNAL_BINARYSEARCHIIF_H

#include "FXDEngine/Core/Internal/LowerBound.h"

namespace FXD
{
	namespace STD
	{
		// binary_search_i_if
#		define FXD_SUPPORTS_BINARY_SEARCH_I_IF 1

		template < typename ForwardItr, typename T, typename Predicate >
		inline ForwardItr binary_search_i_if(ForwardItr first, ForwardItr last, const T& val, Predicate pred)
		{
			ForwardItr itr(FXD::STD::lower_bound< ForwardItr, T, Predicate >(first, last, val, pred));
			if ((itr != last) && !pred(val, *itr))
			{
				return itr;
			}
			return last;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_BINARYSEARCHIIF_H