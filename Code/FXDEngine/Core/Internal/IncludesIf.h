// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_INCLUDESIF_H
#define FXDENGINE_CORE_INTERNAL_INCLUDESIF_H

namespace FXD
{
	namespace STD
	{
		// includes_if
#		define FXD_SUPPORTS_INCLUDES_IF 1

		template < typename InputItr1, typename InputItr2, typename Predicate >
		inline bool includes_if(InputItr1 first1, InputItr1 last1, InputItr2 first2, InputItr2 last2, Predicate pred)
		{
			for (; first2 != last2; ++first1)
			{
				if (first1 == last1 || pred(*first2, *first1))
				{
					return false;
				}
				if (!pred(*first1, *first2))
				{
					++first2;
				}
			}
			return true;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_INCLUDESIF_H