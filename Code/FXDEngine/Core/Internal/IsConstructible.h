#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISCONSTRUCTIBLE_H
#define FXDENGINE_CORE_INTERNAL_ISCONSTRUCTIBLE_H

#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Declval.h"
#include "FXDEngine/Core/Internal/IsAbstract.h"
#include "FXDEngine/Core/Internal/IsArrayOfKnownBounds.h"
#include "FXDEngine/Core/Internal/IsDestructible.h"
#include "FXDEngine/Core/Internal/IsFunction.h"
#include "FXDEngine/Core/Internal/IsReference.h"
#include "FXDEngine/Core/Internal/IsScalar.h"
#include "FXDEngine/Core/Internal/Identity.h"
#include "FXDEngine/Core/Internal/HasVoidArg.h"
#include "FXDEngine/Core/Internal/RemoveAllExtents.h"
#include "FXDEngine/Core/Internal/RemoveReference.h"

namespace FXD
{
	namespace STD
	{
		// is_constructible
#		if FXD_COMPILER_INTRINSIC_TYPE_TRAITS_AVAILABLE && (defined(COMPILER_MSVC) || defined(COMPILER_CLANG))
#		define FXD_SUPPORTS_IS_CONSTRUCTIBLE 1

		template < typename T, typename... Args >
		struct is_constructible : public FXD::STD::integral_constant< bool, __is_constructible(T, Args...) >
		{};

#		else
#		define FXD_SUPPORTS_IS_CONSTRUCTIBLE 1

		namespace Internal
		{
			template < typename T >
			inline typename FXD::STD::remove_reference<T>::type&& _move_internal(T&& val)NOEXCEPT
			{
				return ((typename FXD::STD::remove_reference<T>::type&&)val);
			}

			template < typename T >
			struct _can_construct_scalar
			{
				static FXD::STD::true_type value(T);
				static FXD::STD::false_type value(...);
			};

			template < typename T, typename ...Args >
			typename FXD::STD::Internal::first_type_select< FXD::STD::true_type, decltype(FXD::STD::Internal::_move_internal(T(FXD::STD::declval< Args >()...))) >::type _is(T&&, Args&& ...);

			template < typename ...Args >
			FXD::STD::false_type _is(FXD::STD::Internal::argument_sink, Args&& ...);

			template < bool, typename T, typename... Args >
			struct _is_constructible_2 : public FXD::STD::identity< decltype(FXD::STD::Internal::_is(FXD::STD::declval< T >(), FXD::STD::declval< Args >()...)) >::type
			{};

			template < typename T >
			struct _is_constructible_2< true, T > : public FXD::STD::is_scalar< T >
			{};

			template < typename T, typename Arg0 >
			struct _is_constructible_2< true, T, Arg0 > : public FXD::STD::identity< decltype(FXD::STD::Internal::_can_construct_scalar< T >::value(FXD::STD::declval< Arg0 >())) >::type
			{};

			template < typename T, typename Arg0, typename ...Args >
			struct _is_constructible_2< true, T, Arg0, Args... > : public FXD::STD::false_type
			{};

			template < bool, typename T, typename... Args >
			struct _is_constructible_1 : public FXD::STD::Internal::_is_constructible_2< FXD::STD::is_scalar< T >::value || FXD::STD::is_reference< T >::value, T, Args... >
			{};

			template < typename T, typename... Args >
			struct _is_constructible_1< true, T, Args... > : public FXD::STD::false_type
			{};
		} //namespace Internal

		// is_constructible
		template < typename T, typename... Args >
		struct is_constructible : public FXD::STD::Internal::_is_constructible_1< (FXD::STD::is_abstract< typename FXD::STD::remove_all_extents <T >::type >::value ||
			FXD::STD::is_array_of_unknown_bounds< T >::value ||
			FXD::STD::is_function< typename FXD::STD::remove_all_extents< T >::type >::value ||
			FXD::STD::has_void_arg< T, Args... >::value), T, Args... >
		{};

		namespace Internal
		{
			template < typename T, size_t SIZE >
			struct _is_constructible_2< false, T[SIZE] > : public FXD::STD::is_constructible< typename FXD::STD::remove_all_extents< T >::type >
			{};

			template < typename T, size_t SIZE, typename ...Args >
			struct _is_constructible_2< false, T[SIZE], Args... > : public FXD::STD::false_type
			{};
		} //namespace Internal
#		endif

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T, typename... Args >
		CONSTEXPR bool is_constructible_v = FXD::STD::is_constructible< T, Args... >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISCONSTRUCTIBLE_H