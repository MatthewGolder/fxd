// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_INTERNAL_ISPERMUTATION_H
#define FXDENGINE_CORE_INTERNAL_ISPERMUTATION_H

#include "FXDEngine/Core/Internal/IsPermutationIf.h"
#include "FXDEngine/Core/Internal/EqualTo.h"

namespace FXD
{
	namespace STD
	{
		// is_permutation
#		define FXD_SUPPORTS_IS_PERMUTATION 1

		template < typename ForwardItr1, typename ForwardItr2, typename Predicate >
		inline bool is_permutation(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2, Predicate pred)
		{
			return FXD::STD::is_permutation_if(first1, last1, first2, pred);
		}

		template < typename ForwardItr1, typename ForwardItr2 >
		inline bool is_permutation(ForwardItr1 first1, ForwardItr1 last1, ForwardItr2 first2)
		{
			return FXD::STD::is_permutation(first1, last1, first2, FXD::STD::equal_to<>());
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CORE_INTERNAL_ISPERMUTATION_H