// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_EXPRESSIONTREE_H
#define FXDENGINE_CORE_EXPRESSIONTREE_H

#include "FXDEngine/Core/Expression.h"
#include "FXDEngine/Core/ExpressionTokeniser.h"

#include <memory>

namespace FXD
{
	namespace Core
	{
		// ------
		// ExpressionTree
		// -
		// ------
		class ExpressionTree
		{
		private:
			// ------
			// IExpressionNode
			// -
			// ------
			class IExpressionNode
			{
			public:
				IExpressionNode(void)
				{}
				virtual ~IExpressionNode(void)
				{}

				virtual void compile(Memory::StreamMemory& source, const Core::IVariableProvider& vars) const PURE;
				virtual Core::E_ExpressionType get_type(const Core::IVariableProvider& vars) const PURE;
			};

		public:
			ExpressionTree(void);
			ExpressionTree(const Core::ExpressionTokeniser& tokens);
			~ExpressionTree(void);

			void build_tree(const Core::ExpressionTokeniser& tokens);
			void build_compiled_expression(Core::Expression& expression, const Core::IVariableProvider& vars) const;

		private:			
			using expression_node = std::shared_ptr< IExpressionNode >;

			// Node types.
			class IExpressionNode_Literal;
			class IExpressionNode_Variable;
			class IExpressionNode_BinaryOp;
			class IExpressionNode_UnaryOp;

			// Root node.
			expression_node m_rootNode;

			// Expression parsing.
			static expression_node parse_expression(const Core::ExpressionTokeniser& tokens, FXD::U16& nTokenID);
			static expression_node parse_logical_expression(const Core::ExpressionTokeniser& tokens, FXD::U16& nTokenID);
			static expression_node parse_equality_expression(const Core::ExpressionTokeniser& tokens, FXD::U16& nTokenID);
			static expression_node parse_comparison_expression(const Core::ExpressionTokeniser& tokens, FXD::U16& nTokenID);
			static expression_node parse_additive_expression(const Core::ExpressionTokeniser& tokens, FXD::U16& nTokenID);
			static expression_node parse_multiplicative_expression(const Core::ExpressionTokeniser& tokens, FXD::U16& nTokenID);
			static ExpressionTree::expression_node parse_primary_expression(const Core::ExpressionTokeniser& tokens, FXD::U16& nTokenID);

			// Helper methods.
			static expression_node create_binary_expression_node(const expression_node& eLeftNode, const expression_node& eRightNode, Core::E_TokenType eOp);
			static expression_node create_literal_expression_node(FXD::F32 fValue);
			static expression_node create_variable_expression_node(const Core::String8& strNode);
			static expression_node create_unary_expression_node(const expression_node& expressionNode, Core::E_TokenType eOp);
		};

	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_EXPRESSIONTREE_H
