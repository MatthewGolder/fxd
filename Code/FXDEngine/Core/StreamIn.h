// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_STREAMIN_H
#define FXDENGINE_CORE_STREAMIN_H

#include "FXDEngine/Core/Compression.h"
#include "FXDEngine/Core/Seek.h"
#include <memory>

namespace FXD
{
	namespace Core
	{
		class ISerializeIn;
		class IStreamDecompressor;
		// ------
		// IStreamIn
		// -
		// ------
		class IStreamIn : public Core::IStreamSeekable
		{
		public:
			friend class Core::ISerializeIn;

		public:
			IStreamIn(Core::ISerializeIn* pSerializeIn, Core::CompressParams compressParams = Core::CompressParams::Default);
			virtual ~IStreamIn(void);

			// Stream methods.
			Core::IStreamIn& operator>>(FXD::UTF8* pVal);
			Core::IStreamIn& operator>>(FXD::UTF16* pVal);

			Core::IStreamIn& operator>>(bool& val);
			Core::IStreamIn& operator>>(FXD::UTF8& val);

			Core::IStreamIn& operator>>(FXD::S8& nVal);
			Core::IStreamIn& operator>>(FXD::U8& nVal);

			Core::IStreamIn& operator>>(FXD::S16& nVal);
			Core::IStreamIn& operator>>(FXD::U16& nVal);

			Core::IStreamIn& operator>>(FXD::S32& nVal);
			Core::IStreamIn& operator>>(FXD::U32& nVal);

			Core::IStreamIn& operator>>(FXD::S64& nVal);
			Core::IStreamIn& operator>>(FXD::U64& nVal);

			Core::IStreamIn& operator>>(FXD::F32& fVal);
			Core::IStreamIn& operator>>(FXD::F64& fVal);

			virtual FXD::U64 read_size(void* pData, const FXD::U64 nSize) PURE;
			FXD::U64 decompress_size(void* pData, const FXD::U64 nSize);

		protected:
			void _set_serialize_in(Core::ISerializeIn* pSerializeIn);
			void _set_compressor_in(Core::CompressParams compressParams);

		protected:
			Core::ISerializeIn* m_pSerializeIn;
			Core::IStreamDecompressor* m_pDecompressor;
		};
		using StreamIn = std::shared_ptr< Core::IStreamIn >;

	} //namespace Core
} //namespace FXD
#endif //FXDENGINE_CORE_STREAMIN_H