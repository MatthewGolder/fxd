// Creator - MatthewGolder
#include "FXDEngine/Core/Types.h"

#if IsSystemWindows()
#include "FXDEngine/Core/Timer.h"
#include "FXDEngine/Core/Win32/Win32.h"

using namespace FXD;
using namespace Core;

static FXD::S64 g_nTimeFreq = 0;
// ------
// Timer
// -
// ------
Timer::Timer(bool bStart)
{
	if (bStart)
	{
		start_counter();
	}
}

Timer::~Timer(void)
{
}

void Timer::start_counter(void)
{
	m_nStartTime = Funcs::GetRawTime();
}

FXD::F32 Timer::elapsed_time(void) const
{
	FXD::S64 nCurTime = Funcs::GetRawTime();
	FXD::S64 nTimeDif = (nCurTime - m_nStartTime);
	m_nStartTime = nCurTime;
	FXD::F64 fTimeDifD = (FXD::F64)nTimeDif;
	FXD::F64 fTimeFreqD = (FXD::F64)Funcs::GetRawTimeFreq();
	FXD::F64 fElapsedD = (fTimeDifD / fTimeFreqD);
	FXD::F32 fElapsed = (FXD::F32)fElapsedD;
	if (fElapsed < 0.0f)
	{
		fElapsed = 0.0f;
	}
	return fElapsed;
}

FXD::S64 FXD::Core::Funcs::GetRawTime(void)
{
	LARGE_INTEGER timeVal;
	QueryPerformanceCounter(&timeVal);
	return timeVal.QuadPart;
}

FXD::S64 FXD::Core::Funcs::GetRawTimeFreq(void)
{
	if (!g_nTimeFreq)
	{
		LARGE_INTEGER timeFreq;
		QueryPerformanceFrequency(&timeFreq);
		g_nTimeFreq = timeFreq.QuadPart;
	}
	return g_nTimeFreq;
}

FXD::F32 FXD::Core::Funcs::GetTimeSecsF(void)
{
	return ((FXD::F32)Funcs::GetRawTime() / (FXD::F32)Funcs::GetRawTimeFreq());
}

FXD::F64 FXD::Core::Funcs::GetTimeSecsD(void)
{
	return ((FXD::F64)Funcs::GetRawTime() / (FXD::F64)Funcs::GetRawTimeFreq());
}
#endif //IsSystemWindows()