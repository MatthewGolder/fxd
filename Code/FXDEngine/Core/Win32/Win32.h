// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_WIN32_WIN32_H
#define FXDENGINE_CORE_WIN32_WIN32_H

#include "FXDEngine/Core/Types.h"

#if IsOSPC()
	#ifndef WIN32_LEAN_AND_MEAN
		#define WIN32_LEAN_AND_MEAN
	#endif
	#ifndef NOMINMAX
		#define NOMINMAX
	#endif
	#include <windows.h>
	#ifdef NOMINMAX
		#undef NOMINMAX
	#endif
#endif //IsOSPC()

#endif //FXDENGINE_CORE_WIN32_WIN32_H