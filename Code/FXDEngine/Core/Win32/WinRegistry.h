// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_WIN32_WINREGISTRY_H
#define FXDENGINE_CORE_WIN32_WINREGISTRY_H

#include "FXDEngine/Core/Types.h"

#if IsOSPC()
#include "FXDEngine/Core/String.h"
#include "FXDEngine/Core/Win32/Win32.h"

namespace FXD
{
	namespace Core
	{
		// ------
		// WinRegistryKey
		// -
		// ------
		class WinRegistryKey
		{
		public:
			WinRegistryKey(HKEY hKey, bool bDeleteOnClose = true);
			~WinRegistryKey(void);

			WinRegistryKey create_registry(const FXD::UTF8* pName) const;
			WinRegistryKey get_sub_key(const FXD::UTF8* pName) const;
			WinRegistryKey get_sub_key(DWORD nID, Core::String8& strName) const;

			Core::String8 get_string(const FXD::UTF8* pName) const;
			FXD::S32 get_int(const FXD::UTF8* pName, FXD::S32 nDef) const;
			bool get_bool(const FXD::UTF8* pName, bool bDef) const;

			bool set_string(const FXD::UTF8* pName, const FXD::UTF8* pValue) const;
			bool set_int(const FXD::UTF8* pName, FXD::S32 nVal) const;
			bool setBool(const FXD::UTF8* pName, bool bValue) const;

			bool is_valid(void) const;

		private:
			HKEY m_hKey;
			bool m_bDeleteOnClose;
		};
	} //namespace Core
} //namespace FXD
#endif //IsOSPC()
#endif //FXDENGINE_CORE_WIN32_WINREGISTRY_H
