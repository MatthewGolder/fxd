// Creator - MatthewGolder
#include "FXDEngine/Core/Types.h"

#if IsOSPC()
#include "FXDEngine/Core/Win32/WinRegistry.h"

using namespace FXD;
using namespace Core;

// ------
// WinRegistryKey
// -
// ------
WinRegistryKey::WinRegistryKey(HKEY hKey, bool bDeleteOnClose)
	: m_hKey(hKey)
	, m_bDeleteOnClose(bDeleteOnClose)
{
}

WinRegistryKey::~WinRegistryKey(void)
{
	if ((!is_valid()) || (!m_bDeleteOnClose))
	{
		return;
	}
	RegCloseKey(m_hKey);
	m_hKey = 0;
}

WinRegistryKey WinRegistryKey::create_registry(const FXD::UTF8* pName) const
{
	if (!is_valid())
	{
		return 0;
	}

	HKEY hKey;
	if (RegCreateKeyExA(m_hKey, pName, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL) != ERROR_SUCCESS)
	{
		return 0;
	}
	return hKey;
}

WinRegistryKey WinRegistryKey::get_sub_key(const FXD::UTF8* pName) const
{
	if (!is_valid())
	{
		return 0;
	}

	HKEY hKey;
	if (RegOpenKeyExA(m_hKey, pName, 0, KEY_ALL_ACCESS, &hKey) != ERROR_SUCCESS)
	{
		return create_registry(pName);
	}
	return hKey;
}

WinRegistryKey WinRegistryKey::get_sub_key(DWORD nID, Core::String8& strName) const
{
	if (!is_valid())
	{
		return 0;
	}

	DWORD nSize = 256;
	FXD::UTF8 buf[256];
	FILETIME tm;
	if (RegEnumKeyExA(m_hKey, nID, buf, &nSize, 0, NULL, NULL, &tm) != ERROR_SUCCESS)
	{
		return 0;
	}
	strName = (FXD::UTF8*)buf;

	HKEY hKey;
	if (RegOpenKeyExA(m_hKey, buf, 0, KEY_ALL_ACCESS, &hKey) != ERROR_SUCCESS)
	{
		hKey = 0;
	}
	return hKey;
}

Core::String8 WinRegistryKey::get_string(const FXD::UTF8* pName) const
{
	if (!is_valid())
	{
		return Core::String8();
	}

	DWORD nSize = 256;
	FXD::UTF8 buf[256];
	DWORD type = RRF_RT_REG_SZ;
	if (RegQueryValueExA(m_hKey, pName, NULL, &type, (LPBYTE)buf, &nSize) != ERROR_SUCCESS)
	{
		return Core::String8();
	}
	buf[nSize] = 0;
	return (FXD::UTF8*)buf;
}

FXD::S32 WinRegistryKey::get_int(const FXD::UTF8* pName, FXD::S32 nDef) const
{
	if (!is_valid())
	{
		return 0;
	}

	FXD::S32 nRetVal;
	DWORD nSize = sizeof(nRetVal);
	DWORD type = RRF_RT_REG_DWORD;
	if (RegQueryValueExA(m_hKey, pName, NULL, &type, (LPBYTE)&nRetVal, &nSize) != ERROR_SUCCESS)
	{
		return nDef;
	}
	return nRetVal;
}

bool WinRegistryKey::get_bool(const FXD::UTF8* pName, bool bDef) const
{
	if (!is_valid())
	{
		return 0;
	}

	DWORD nSize = 256;
	FXD::UTF8 buf[256];
	DWORD type = RRF_RT_REG_SZ;
	if (RegQueryValueExA(m_hKey, pName, NULL, &type, (LPBYTE)buf, &nSize) != ERROR_SUCCESS)
	{
		return bDef;
	}
	buf[nSize] = 0;

	Core::String8 retStr = (FXD::UTF8*)buf;
	return retStr.to_bool();
}

bool WinRegistryKey::set_string(const FXD::UTF8* pName, const FXD::UTF8* pValue) const
{
	if (!is_valid())
	{
		return 0;
	}

	DWORD nSize = Core::CharTraits< FXD::UTF8 >::length(pValue) * sizeof(FXD::UTF8);
	if (RegSetValueExA(m_hKey, pName, 0, REG_SZ, (LPBYTE)pValue, nSize) != ERROR_SUCCESS)
	{
		return false;
	}
	return true;
}

bool WinRegistryKey::setBool(const FXD::UTF8* pName, bool bValue) const
{
	if (!is_valid())
	{
		return 0;
	}

	const FXD::UTF8* pValue = (bValue) ? UTF_8("True") : UTF_8("False");
	DWORD nSize = Core::CharTraits< FXD::UTF8 >::length(pValue) * sizeof(FXD::UTF8);
	if (RegSetValueExA(m_hKey, pName, 0, REG_SZ, (LPBYTE)pValue, nSize) != ERROR_SUCCESS)
	{
		return false;
	}
	return true;
}

bool WinRegistryKey::set_int(const FXD::UTF8* pName, FXD::S32 nVal) const
{
	if (!is_valid())
	{
		return 0;
	}

	DWORD nSize = sizeof(nVal);
	if (RegSetValueExA(m_hKey, pName, 0, REG_DWORD, (BYTE*)&nVal, nSize) != ERROR_SUCCESS)
	{
		return false;
	}
	return true;
}

bool WinRegistryKey::is_valid(void) const
{
	return (m_hKey != 0);
}
#endif //IsOSPC()