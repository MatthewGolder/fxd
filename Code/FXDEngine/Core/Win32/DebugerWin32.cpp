// Creator - MatthewGolder
#include "FXDEngine/Core/Types.h"

#if IsSystemWindows()
#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Core/Internal/Debugger.h"
#include "FXDEngine/Core/Win32/Win32.h"

//#define _WIN32_WINNT 0x0501
#include <intrin.h>
#include <cstdio>

using namespace FXD;
using namespace Core;

#if defined(ENABLE_DEBUGGER)
// ------
// DBGStreamOut
// -
// ------
FXD::U64 DBGStreamOut::write_size(const void* pData, const FXD::U64 /*nSize*/)
{
	m_impl->m_str.append((const FXD::UTF8*)pData);
	return 0;
}

void DBGStreamOut::apply(Core::E_DBGLevel eLevel)
{
	m_eLevel = eLevel;
	switch (m_eLevel)
	{
		case FXD::Core::E_DBGLevel::None:
		{
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		}
		case FXD::Core::E_DBGLevel::Info:
		{
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_BLUE | FOREGROUND_INTENSITY);
			_do_print("[Info] ");
			break;
		}
		case FXD::Core::E_DBGLevel::Warn:
		{
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN | FOREGROUND_INTENSITY);
			_do_print("[Warn] ");
			break;
		}
		case FXD::Core::E_DBGLevel::Error:
		{
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_INTENSITY);
			_do_print("[Error] ");
			break;
		}
		case FXD::Core::E_DBGLevel::Assert:
		{
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_INTENSITY);
			_do_print("[Assert] ");
			break;
		}
		default:
		{
			break;
		}
	}
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
}

void DBGStreamOut::_do_print(const FXD::UTF8* pStr) const
{
	//if(!IsDebuggerPresent())
	{
		std::printf("%s", pStr);
	}
}

void DBGStreamOut::_break_application(void) const
{
	if (IsDebuggerPresent())
	{
		__debugbreak();
	}
	else
	{
		MessageBoxA(NULL, "An assertion failed in the FXD app.\nPlease attach a debugger", "FXD App assertion failed", MB_OK);
	}
}
#endif //defined(ENABLE_DEBUGGER)
#endif //IsSystemWindows()