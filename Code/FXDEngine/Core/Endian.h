// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CORE_ENDIAN_H
#define FXDENGINE_CORE_ENDIAN_H

#include "FXDEngine/Core/Types.h"

#define FXD_LIL_ENDIAN 1234
#define FXD_BIG_ENDIAN 4321

#ifndef FXD_BYTEORDER
#	if IsOSPC()
#		define FXD_BYTEORDER FXD_LIL_ENDIAN
#	elif IsOSAndroid()
#		define FXD_BYTEORDER FXD_LIL_ENDIAN
#	else
#		error "Undefined System"
#	endif
#endif

	inline FXD::U16 FXD_Swap16(FXD::U16 x)
	{
		return FXD::U16(((x << 8) | (x >> 8)));
	}
	inline FXD::U32 FXD_Swap32(FXD::U32 x)
	{
		return (FXD::U32)(((x << 24) | ((x << 8) & 0x00FF0000) | ((x >> 8) & 0x0000FF00) | (x >> 24)));
	}
	inline FXD::U64 FXD_Swap64(FXD::U64 x)
	{
		// Separate into high and low 32-bit values and swap them
		FXD::U32 lo = (FXD::U32)(x & 0xFFFFFFFF);
		x >>= 32;
		FXD::U32 hi = (FXD::U32)(x & 0xFFFFFFFF);
		x = FXD_Swap32(lo);
		x <<= 32;
		x |= FXD_Swap32(hi);
		return (x);
	}

#if FXD_BYTEORDER == FXD_LIL_ENDIAN
#	define FXD_SwapLE16(X) (X)
#	define FXD_SwapLE32(X) (X)
#	define FXD_SwapLE64(X) (X)
#	define FXD_SwapFloatLE(X) (X)
#	define FXD_SwapBE16(X) FXD_Swap16(X)
#	define FXD_SwapBE32(X) FXD_Swap32(X)
#	define FXD_SwapBE64(X) FXD_Swap64(X)
#	define FXD_SwapFloatBE(X) FXD_SwapFloat(X)
#else
#	define FXD_SwapLE16(X) FXD_Swap16(X)
#	define FXD_SwapLE32(X) FXD_Swap32(X)
#	define FXD_SwapLE64(X) FXD_Swap64(X)
#	define FXD_SwapFloatLE(X) FXD_SwapFloat(X)
#	define FXD_SwapBE16(X) (X)
#	define FXD_SwapBE32(X) (X)
#	define FXD_SwapBE64(X) (X)
#	define FXD_SwapFloatBE(X) (X)
#endif
#endif //FXDENGINE_CORE_ENDIAN_H