// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_PLAYER_H
#define FXDENGINE_ACCOUNTS_PLAYER_H

#include "FXDEngine/Accounts/Types.h"
#include "FXDEngine/Core/String.h"

namespace FXD
{
	namespace Accounts
	{
		// ------
		// IPlayer
		// -
		// ------
		class IPlayer : public Core::NonCopyable
		{
		public:
			friend class IAccountSystem;
			class Impl;

		public:
			EXPLICIT IPlayer(Accounts::E_AccountsApi eAccountApi);
			virtual ~IPlayer(void);

			virtual bool is_local(void) const PURE;
			virtual bool is_valid(void) const PURE;

			virtual Core::String16 persona(void) const PURE;
			virtual Core::String8 platform_id(void) const PURE;
			Core::String8 globalID(void) const;

			GET_R(E_AccountsApi, eAccountApi, accountApi);

		protected:
			void _update(FXD::F32 dt);
			virtual void _update_os(FXD::F32 dt) PURE;

		protected:
			Accounts::E_AccountsApi m_eAccountApi;
			mutable Core::String16 m_strPersona;
			mutable Core::String8 m_strPlatformID;
		};

	} //namespace Accounts
} //namespace FXD
#endif //FXDENGINE_ACCOUNTS_PLAYER_H