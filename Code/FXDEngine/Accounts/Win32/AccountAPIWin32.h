// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_WIN32_ACCOUNTAPIWIN32_H
#define FXDENGINE_ACCOUNTS_WIN32_ACCOUNTAPIWIN32_H

#include "FXDEngine/Accounts/Types.h"

#if IsAccountWindows()
#include "FXDEngine/Accounts/AccountApi.h"

namespace FXD
{
	namespace Accounts
	{
		// ------
		// IAccountsApiWin32
		// -
		// ------
		class IAccountsApiWin32 FINAL : public IAccountsApi
		{
		public:
			IAccountsApiWin32(void);
			~IAccountsApiWin32(void);

			bool create_api(void) FINAL;
			bool release_api(void) FINAL;
		};

	} //namespace Accounts
} //namespace FXD

#endif //IsAccountWindows()
#endif //FXDENGINE_ACCOUNTS_WIN32_ACCOUNTAPIWIN32_H
