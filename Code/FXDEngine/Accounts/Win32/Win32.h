// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_WIN32_WIN32_H
#define FXDENGINE_ACCOUNTS_WIN32_WIN32_H

#include "FXDEngine/Accounts/Types.h"

#if IsAccountWindows()

namespace FXD
{
	namespace Accounts
	{
		class IAccountsApiWin32;
		class IPlayerLocalWin32;
	} //namespace Accounts
} //namespace FXD

#endif //IsAccountWindows()
#endif //FXDENGINE_ACCOUNTS_WIN32_WIN32_H