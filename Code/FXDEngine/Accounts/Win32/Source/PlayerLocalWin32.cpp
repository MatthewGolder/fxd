// Creator - MatthewGolder
#include "FXDEngine/Accounts/Types.h"

#if IsAccountWindows()
#include "FXDEngine/Core/Win32/Win32.h"
#include "FXDEngine/Accounts/Win32/PlayerLocalWin32.h"
#include "FXDEngine/Accounts/Win32/PlayerRemoteWin32.h"
#include "FXDEngine/Container/Bitset.h"

using namespace FXD;
using namespace Accounts;

FXD::Container::Bitset< 16 > nUnusedGuestIndices;

FXD::U32 get_unsed_guess_index(void)
{
	for (FXD::U32 n1 = 0; n1 < nUnusedGuestIndices.size(); ++n1)
	{
		if (!nUnusedGuestIndices.test(n1))
		{
			return n1;
		}
	}
	return 0;
}

void use_guest_index(FXD::U32 nIndex)
{
	nUnusedGuestIndices.set(nIndex);
}

void unuse_guest_index(FXD::U32 nIndex)
{
	nUnusedGuestIndices.reset(nIndex);
}

// ------
// IPlayerLocalWin32
// - 
// ------
IPlayerLocalWin32::IPlayerLocalWin32(const PlayerContext& context)
	: IPlayerLocal(E_AccountsApi::Win32)
	, m_bPrimary(context.m_bPrimary)
	, m_nGuestID(0)
{
	if (m_bPrimary)
	{
		m_nGuestID = get_unsed_guess_index();
		use_guest_index(m_nGuestID);
	}
	else
	{
	}
}

IPlayerLocalWin32::~IPlayerLocalWin32(void)
{
	if (m_bPrimary)
	{
		unuse_guest_index(m_nGuestID);
	}
}

bool IPlayerLocalWin32::is_valid(void) const
{
	return false;
}

Core::String16 IPlayerLocalWin32::persona(void) const
{
	if (m_strPersona.empty())
	{
		if (m_bPrimary)
		{
			FXD::UTF16 strBuf[128];
			DWORD nSize = sizeof(strBuf);
			GetUserNameW((wchar_t*)strBuf, &nSize);
			m_strPersona = Core::String16(&strBuf[0]);
		}
	}
	return m_strPersona;
}

Core::String8 IPlayerLocalWin32::platform_id(void) const
{
	if (m_strPlatformID.empty())
	{
		if (m_bPrimary)
		{
			FXD::U64 nInt = 0;
			Core::StringBuilder8 strB;
			strB << nInt;
			m_strPlatformID = strB.str();
		}
	}
	return m_strPlatformID;
}

Container::Vector< Accounts::PlayerRemote > IPlayerLocalWin32::_friends(void) const
{
	return Container::Vector< Accounts::PlayerRemote >();
}

void IPlayerLocalWin32::_update_os(FXD::F32 /*dt*/)
{
}
#endif //IsAccountWindows()