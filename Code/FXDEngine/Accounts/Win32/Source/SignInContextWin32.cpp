// Creator - MatthewGolder
#include "FXDEngine/Accounts/Types.h"

#if IsAccountWindows()
#include "FXDEngine/Accounts/SignInContext.h"

using namespace FXD;
using namespace Accounts;

// ------
// Core::Impl::Impl< SignInContext >
// -
// ------
template <>
class Core::Impl::Impl< SignInContext >
{
public:
	friend class SignInContext;

public:
	Impl(void)
	{
	}
	~Impl(void)
	{
	}

private:
};

// ------
// SignInContext
// -
// ------
SignInContext::SignInContext(void)
{
}

SignInContext::~SignInContext(void)
{
}

Accounts::E_SignIn SignInContext::signedin_status(void) const
{
	return Accounts::E_SignIn::NotSignedIn;
}
#endif //IsAccountWindows()