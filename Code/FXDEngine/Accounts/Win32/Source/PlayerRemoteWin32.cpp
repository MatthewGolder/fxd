// Creator - MatthewGolder
#include "FXDEngine/Accounts/Types.h"

#if IsAccountWindows()
#include "FXDEngine/Accounts/Win32/PlayerRemoteWin32.h"

using namespace FXD;
using namespace Accounts;

// ------
// IPlayerRemoteWin32
// - 
// ------
IPlayerRemoteWin32::IPlayerRemoteWin32(void)
	: IPlayerRemote(E_AccountsApi::Win32)
{
}

IPlayerRemoteWin32::~IPlayerRemoteWin32(void)
{
}

bool IPlayerRemoteWin32::is_valid(void) const
{
	return false;
}

Core::String16 IPlayerRemoteWin32::persona(void) const
{
	return m_strPersona;
}

Core::String8 IPlayerRemoteWin32::platform_id(void) const
{
	return m_strPlatformID;
}

void IPlayerRemoteWin32::_update_os(FXD::F32 /*dt*/)
{
}
#endif //IsAccountWindows()