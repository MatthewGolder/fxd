// Creator - MatthewGolder
#include "FXDEngine/Accounts/Types.h"

#if IsAccountWindows()
#include "FXDEngine/Accounts/Win32/AccountApiWin32.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/App/Win32/GlobalsWin32.h"

using namespace FXD;
using namespace Accounts;

// ------
// IAccountsApiWin32
// - 
// ------
IAccountsApiWin32::IAccountsApiWin32(void)
	: IAccountsApi(E_AccountsApi::Win32)
{
}

IAccountsApiWin32::~IAccountsApiWin32(void)
{
}

bool IAccountsApiWin32::create_api(void)
{
	return true;
}

bool IAccountsApiWin32::release_api(void)
{
	return true;
}
#endif //IsAccountWindows()