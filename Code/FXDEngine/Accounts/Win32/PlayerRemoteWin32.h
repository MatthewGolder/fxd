// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_WIN32_PLAYERLOCALREMOTEWIN32_H
#define FXDENGINE_ACCOUNTS_WIN32_PLAYERLOCALREMOTEWIN32_H

#include "FXDEngine/Accounts/Types.h"

#if IsAccountWindows()
#include "FXDEngine/Accounts/PlayerRemote.h"
#include "FXDEngine/Accounts/Win32/Win32.h"

namespace FXD
{
	namespace Accounts
	{
		// ------
		// IPlayerRemoteWin32
		// -
		// ------
		class IPlayerRemoteWin32 : public IPlayerRemote
		{
		public:
			EXPLICIT IPlayerRemoteWin32(void);
			virtual ~IPlayerRemoteWin32(void);

			bool is_valid(void) const FINAL;

			Core::String16 persona(void) const FINAL;
			Core::String8 platform_id(void) const FINAL;

		protected:
			void _update_os(FXD::F32 dt) FINAL;
		};

	} //namespace Accounts
} //namespace FXD

#endif //IsAccountWindows()
#endif //FXDENGINE_ACCOUNTS_WIN32_PLAYERLOCALREMOTEWIN32_H