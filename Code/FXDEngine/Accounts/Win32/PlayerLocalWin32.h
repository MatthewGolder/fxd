// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_WIN32_PLAYERLOCALWIN32_H
#define FXDENGINE_ACCOUNTS_WIN32_PLAYERLOCALWIN32_H

#include "FXDEngine/Accounts/Types.h"

#if IsAccountWindows()
#include "FXDEngine/Accounts/PlayerLocal.h"
#include "FXDEngine/Accounts/Win32/Win32.h"

namespace FXD
{
	namespace Accounts
	{
		// ------
		// IPlayerLocalWin32
		// -
		// ------
		class IPlayerLocalWin32 : public IPlayerLocal
		{
		public:
			class PlayerContext
			{
			public:
				PlayerContext(void)
					: m_bPrimary(false)
				{
				}
				~PlayerContext(void)
				{
				}

			public:
				bool m_bPrimary;
			};

		public:
			EXPLICIT IPlayerLocalWin32(const PlayerContext& context);
			virtual ~IPlayerLocalWin32(void);

			bool is_valid(void) const FINAL;

			Core::String16 persona(void) const FINAL;
			Core::String8 platform_id(void) const FINAL;

		protected:
			Container::Vector< Accounts::PlayerRemote > _friends(void) const;

			void _update_os(FXD::F32 dt) FINAL;

		protected:
			bool m_bPrimary;
			FXD::U32 m_nGuestID;
		};

	} //namespace Accounts
} //namespace FXD

#endif //IsAccountWindows()
#endif //FXDENGINE_ACCOUNTS_WIN32_PLAYERLOCALWIN32_H