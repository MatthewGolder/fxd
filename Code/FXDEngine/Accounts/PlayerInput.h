// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_PLAYERINPUT_H
#define FXDENGINE_ACCOUNTS_PLAYERINPUT_H

#include "FXDEngine/Accounts/Types.h"
#include "FXDEngine/Input/InputMap.h"
#include "FXDEngine/Thread/RWLock.h"
#include "FXDEngine/Core/String.h"

namespace FXD
{
	namespace Accounts
	{
		// ------
		// PlayerInput
		// -
		// ------
		class PlayerInput
		{
		public:
			friend class IAccountSystem;

		public:
			PlayerInput(void);
			~PlayerInput(void);

			void set_inputmap_def(const Input::InputMapDesc& inputMapDef, const Core::String8& strControlScheme);

			bool is_any_button_pressed(void) const;
			bool is_action_button_pressed(void) const;
			bool is_start_button_pressed(void) const;
			bool is_mouse_connected(void) const;
			bool is_keyboard_connected(void) const;
			bool is_controller_connected(void) const;

			GET_REF_W(Container::Vector< Input::InputDevice >, inputDevices, input_devices);

			Input::InputMap m_inputMap;
		protected:
			void _update(FXD::F32 dt);

			void _build_inputmap_instance(void);

		protected:
			Thread::RWLock m_lock;
			bool m_bMouseAvailable;
			bool m_bGamepadAvailable;
			bool m_bKeyboardAvailable;
			Input::InputMapDesc m_inputMapDesc;
			Core::String8 m_strControlScheme;			
			Container::Vector< Input::InputDevice > m_inputDevices;
		};

	} //namespace Accounts
} //namespace FXD
#endif //FXDENGINE_ACCOUNTS_PLAYERINPUT_H