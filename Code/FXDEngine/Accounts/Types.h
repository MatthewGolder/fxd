// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_TYPES_H
#define FXDENGINE_ACCOUNTS_TYPES_H

#include "FXDEngine/Core/Types.h"

#include <memory>

#if IsOSPC()
#	if defined(FXD_PLATFORM_STEAM)
#		define IsAccountSteam() 1
#		define IsAccountWindows() 0
#		define IsAccountGooglePlay() 0
#	else
#		define IsAccountSteam() 0
#		define IsAccountWindows() 1
#		define IsAccountGooglePlay() 0
#	endif
#elif IsOSAndroid()
#	define IsAccountSteam() 0
#	define IsAccountWindows() 0
#	define IsAccountGooglePlay() 1
#endif

namespace FXD
{
	namespace Accounts
	{
		enum class E_AccountsApi
		{
			Win32,
			Steam,
			GooglePlay,
			Count,
			Unknown = 0xffff
		};

		enum class E_SignIn
		{
			SignedIn,
			NotSignedIn,
			Count,
			Unknown = 0xffff
		};

		enum class E_PlayerJoinButton
		{
			Any,
			Action,
			Start,
			Count,
			Unknown = 0xffff
		};

		class IAccountsApi;
		using AccountsApi = std::unique_ptr< IAccountsApi >;

		class IAccountSystem;
		using AccountSystem = std::unique_ptr< IAccountSystem >;

		class IPlayer;
		typedef std::shared_ptr< IPlayer > Player;

		class IPlayerLocal;
		typedef std::shared_ptr< IPlayerLocal > PlayerLocal;

		class IPlayerRemote;
		typedef std::shared_ptr< IPlayerRemote > PlayerRemote;

	} //namespace Accounts
} //namespace FXD
#endif //FXDENGINE_ACCOUNTS_TYPES_H