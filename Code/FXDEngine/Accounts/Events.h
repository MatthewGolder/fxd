// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_EVENTS_H
#define FXDENGINE_ACCOUNTS_EVENTS_H

#include "FXDEngine/Job/Event.h"

namespace FXD
{
	namespace Accounts
	{
		class OnPlayerJoinedEvent FINAL : public Job::IEvent
		{
		public:
			OnPlayerJoinedEvent(const Accounts::PlayerLocal& player)
				: m_player(player)
			{}
			~OnPlayerJoinedEvent(void)
			{}

		public:
			Accounts::PlayerLocal m_player;
		};

	} //namespace Accounts
} //namespace FXD
#endif //FXDENGINE_ACCOUNTS_EVENTS_H