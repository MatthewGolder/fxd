
// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_ACCOUNTAPI_H
#define FXDENGINE_ACCOUNTS_ACCOUNTAPI_H

#include "FXDEngine/Accounts/Types.h"
#include "FXDEngine/Core/StringView.h"

namespace FXD
{
	namespace Accounts
	{
		// ------
		// IAccountsApi
		// -
		// ------
		class IAccountsApi : public Core::NonCopyable
		{
		public:
			IAccountsApi(Accounts::E_AccountsApi eAccountApi);
			virtual ~IAccountsApi(void);

			virtual bool create_api(void) PURE;
			virtual bool release_api(void) PURE;

			GET_R(Accounts::E_AccountsApi, eAccountApi, accountApi);

		protected:
			Accounts::E_AccountsApi m_eAccountApi;
		};

		namespace Funcs
		{
			extern const Accounts::E_AccountsApi AccountType(void);
			extern const Core::StringView8 ToString(const Accounts::E_AccountsApi eApi);
			extern Accounts::E_AccountsApi ToApiType(const Core::StringView8 strType);
		} //namespace Funcs

	} //namespace Accounts
} //namespace FXD
#endif //FXDENGINE_ACCOUNTS_ACCOUNTAPI_H