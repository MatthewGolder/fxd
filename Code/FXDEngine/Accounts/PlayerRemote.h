// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_PLAYERREMOTE_H
#define FXDENGINE_ACCOUNTS_PLAYERREMOTE_H

#include "FXDEngine/Accounts/Player.h"

namespace FXD
{
	namespace Accounts
	{
		// ------
		// IPlayerRemote
		// -
		// ------
		class IPlayerRemote : public IPlayer
		{
		public:
			friend class IAccountSystem;

		public:
			EXPLICIT IPlayerRemote(Accounts::E_AccountsApi eAccountApi);
			virtual ~IPlayerRemote(void);

			bool is_local(void) const FINAL;
			virtual bool is_valid(void) const PURE;

			virtual Core::String16 persona(void) const PURE;
			virtual Core::String8 platform_id(void) const PURE;

		protected:
			virtual void _update_os(FXD::F32 dt) PURE;
		};

	} //namespace Accounts
} //namespace FXD
#endif //FXDENGINE_ACCOUNTS_PLAYERREMOTE_H