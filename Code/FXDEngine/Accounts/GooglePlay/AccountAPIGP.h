// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_GOOGLEPLAY_ACCOUNTAPIGP_H
#define FXDENGINE_ACCOUNTS_GOOGLEPLAY_ACCOUNTAPIGP_H

#include "FXDEngine/Accounts/Types.h"

#if IsAccountGooglePlay()
#include "FXDEngine/Accounts/AccountApi.h"
#include "FXDEngine/Accounts/GooglePlay/GooglePlay.h"

namespace FXD
{
	namespace Accounts
	{
		// ------
		// IAccountsApiGP
		// -
		// ------
		class IAccountsApiGP FINAL : public IAccountsApi
		{
		public:
			IAccountsApiGP(void);
			~IAccountsApiGP(void);

			bool create_api(void) FINAL;
			bool release_api(void) FINAL;
		};

	} //namespace Accounts
} //namespace FXD

#endif //IsAccountGooglePlay()
#endif //FXDENGINE_ACCOUNTS_GOOGLEPLAY_ACCOUNTAPIGP_H