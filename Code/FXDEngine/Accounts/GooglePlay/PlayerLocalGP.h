// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_GOOGLEPLAY_PLAYERLOCALGP_H
#define FXDENGINE_ACCOUNTS_GOOGLEPLAY_PLAYERLOCALGP_H

#include "FXDEngine/Accounts/Types.h"

#if IsAccountGooglePlay()
#include "FXDEngine/Accounts/PlayerLocal.h"
#include "FXDEngine/Accounts/GooglePlay/GooglePlay.h"

namespace FXD
{
	namespace Accounts
	{
		// ------
		// IPlayerLocalGP
		// -
		// ------
		class IPlayerLocalGP : public IPlayerLocal
		{
		public:
			class PlayerContext
			{
			public:
				PlayerContext(void)
				{
				}
				~PlayerContext(void)
				{
				}
			};

		public:
			EXPLICIT	IPlayerLocalGP(const PlayerContext& context);
			virtual ~IPlayerLocalGP(void);

			bool is_valid(void) const FINAL;

			Core::String16 persona(void) const FINAL;
			Core::String8 platform_id(void) const FINAL;

		protected:
			Container::Vector< Accounts::PlayerRemote > _friends(void) const;

			void _update_os(FXD::F32 dt) FINAL;

		protected:
		};

	} //namespace Accounts
} //namespace FXD

#endif //IsAccountGooglePlay()
#endif //FXDENGINE_ACCOUNTS_GOOGLEPLAY_PLAYERLOCALGP_H