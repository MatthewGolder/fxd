// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_GOOGLEPLAY_PLAYERREMOTEGP_H
#define FXDENGINE_ACCOUNTS_GOOGLEPLAY_PLAYERREMOTEGP_H

#include "FXDEngine/Accounts/Types.h"

#if IsAccountGooglePlay()
#include "FXDEngine/Accounts/PlayerRemote.h"
#include "FXDEngine/Accounts/GooglePlay/GooglePlay.h"

namespace FXD
{
	namespace Accounts
	{
		// ------
		// IPlayerRemoteGP
		// -
		// ------
		class IPlayerRemoteGP : public IPlayerRemote
		{
		public:
			EXPLICIT	IPlayerRemoteGP(void);
			virtual ~IPlayerRemoteGP(void);

			bool is_valid(void) const FINAL;

			Core::String16 persona(void) const FINAL;
			Core::String8 platform_id(void) const FINAL;

		protected:
			void _update_os(FXD::F32 dt) FINAL;
		};

	} //namespace Accounts
} //namespace FXD

#endif //IsAccountGooglePlay()
#endif //FXDENGINE_ACCOUNTS_GOOGLEPLAY_PLAYERREMOTEGP_H