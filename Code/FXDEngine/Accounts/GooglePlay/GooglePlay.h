// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_GOOGLEPLAY_GOOGLEPLAY_H
#define FXDENGINE_ACCOUNTS_GOOGLEPLAY_GOOGLEPLAY_H

#include "FXDEngine/Accounts/Types.h"

#if IsAccountGooglePlay()

namespace FXD
{
	namespace Accounts
	{
		class IAccountsApiGP;
		class IPlayerLocalGP;
	} //namespace Accounts
} //namespace FXD

#endif //IsAccountGooglePlay()
#endif //FXDENGINE_ACCOUNTS_GOOGLEPLAY_GOOGLEPLAY_H