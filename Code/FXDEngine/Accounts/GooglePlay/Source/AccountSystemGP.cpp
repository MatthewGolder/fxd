// Creator - MatthewGolder
#include "FXDEngine/Accounts/Types.h"

#if IsAccountGooglePlay()
#include "FXDEngine/Accounts/AccountSystem.h"
#include "FXDEngine/Accounts/Events.h"
#include "FXDEngine/Accounts/GooglePlay/AccountApiGP.h"
#include "FXDEngine/Accounts/GooglePlay/PlayerLocalGP.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/Input/InputJPThread.h"
#include "FXDEngine/Input/InputDevice.h"
#include "FXDEngine/Input/InputSystem.h"
#include "FXDEngine/Job/EventHandler.h"

using namespace FXD;
using namespace Accounts;

// ------
// IAccountSystem
// - 
// ------
bool IAccountSystem::_create_system(void)
{
	// 1. Early out if the api already exists
	if (m_accountApi)
	{
		return false;
	}

	// 2. Create the desired api
	FXD::FXDPlatform()->globals()[UTF_8("api_account")] = E_AccountsApi::GooglePlay;
	m_accountApi = std::make_unique< Accounts::IAccountsApiGP >();

	// 3. Create the desired api 
	bool bRetVal = m_accountApi->create_api();

	return bRetVal;
}

bool IAccountSystem::_release_system(void)
{
	FXD_RELEASE(m_accountApi, release_api());
	return true;
}

void IAccountSystem::_process_os(FXD::F32 dt)
{
}

void IAccountSystem::_process_signin(FXD::F32 dt)
{
	_check_player_joined();
}

void IAccountSystem::_check_player_joined(void)
{
	Thread::CSLock::LockGuard lock(m_lock);
	if (FXDInput() != nullptr)
	{
		/*
		Container::Vector< Input::InputDevice > canBeUsedInputs = FXDInput()->input_system()->unused_devices();
		fxd_for(const auto& unusedInput, unusedInputs)
		{
			bool bPessed = false;
			switch (eButton)
			{
				case E_PlayerJoinButton::Any:
				{
					bPessed = unusedInput->is_any_button_pressed();
					break;
				}
				case E_PlayerJoinButton::Action:
				{
					bPessed = unusedInput->is_action_button_pressed();
					break;
				}
				case E_PlayerJoinButton::Start:
				{
					bPessed = unusedInput->is_start_button_pressed();
					break;
				}
				default:
				{
					break;
				}
			}
			if (bPessed)
			{
				Input::InputDevice pressedDevice = unusedInput;

				Accounts::IPlayerLocalGP::PlayerContext context;
				context.m_pSteamUser = (m_activePlayers.size() == 0) ? SteamUser() : nullptr;

				PlayerLocal player = std::make_shared< Accounts::IPlayerLocalSteam >(context);
				player->player_input().input_devices().push_back(pressedDevice);
				FXDInput()->input_system()->unused_devices().find_erase(pressedDevice);
				FXDInput()->input_system()->input_devices().push_back(pressedDevice);
				m_activePlayers.push_back(player);

				FXDEvent()->raise(Accounts::OnPlayerJoinedEvent(player));
			}
		}
		*/
	}
}
#endif //IsAccountGooglePlay()