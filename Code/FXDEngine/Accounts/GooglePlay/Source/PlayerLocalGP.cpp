// Creator - MatthewGolder
#include "FXDEngine/Accounts/Types.h"

#if IsAccountGooglePlay()
#include "FXDEngine/Accounts/GooglePlay/PlayerLocalGP.h"
#include "FXDEngine/Accounts/GooglePlay/PlayerRemoteGP.h"

using namespace FXD;
using namespace Accounts;

// ------
// IPlayerLocalGP
// - 
// ------
IPlayerLocalGP::IPlayerLocalGP(const PlayerContext& context)
	: IPlayerLocal(E_AccountsApi::GooglePlay)
{
}

IPlayerLocalGP::~IPlayerLocalGP(void)
{
}

bool IPlayerLocalGP::is_valid(void) const
{
	return false;
}

Core::String16 IPlayerLocalGP::persona(void) const
{
	return m_strPersona;
}

Core::String8 IPlayerLocalGP::platform_id(void) const
{
	return m_strPlatformID;
}

Container::Vector< Accounts::PlayerRemote > IPlayerLocalGP::_friends(void) const
{
	return Container::Vector< Accounts::PlayerRemote >();
}

void IPlayerLocalGP::_update_os(FXD::F32 /*dt*/)
{
}
#endif //IsAccountGooglePlay()