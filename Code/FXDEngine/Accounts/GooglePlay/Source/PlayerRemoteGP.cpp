// Creator - MatthewGolder
#include "FXDEngine/Accounts/Types.h"

#if IsAccountGooglePlay()
#include "FXDEngine/Accounts/GooglePlay/PlayerRemoteGP.h"
#include "FXDEngine/Accounts/GooglePlay/GooglePlay.h"

using namespace FXD;
using namespace Accounts;

// ------
// IPlayerRemoteGP
// - 
// ------
IPlayerRemoteGP::IPlayerRemoteGP(void)
	: IPlayerRemote(E_AccountsApi::GooglePlay)
{
}

IPlayerRemoteGP::~IPlayerRemoteGP(void)
{
}

bool IPlayerRemoteGP::is_valid(void) const
{
	return false;
}

Core::String16 IPlayerRemoteGP::persona(void) const
{
	return m_strPersona;
}

Core::String8 IPlayerRemoteGP::platform_id(void) const
{
	return m_strPlatformID;
}

void IPlayerRemoteGP::_update_os(FXD::F32 /*dt*/)
{
}
#endif //IsAccountGooglePlay()