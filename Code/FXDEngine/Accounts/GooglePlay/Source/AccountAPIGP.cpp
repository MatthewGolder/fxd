// Creator - MatthewGolder
#include "FXDEngine/Accounts/Types.h"

#if IsAccountGooglePlay()
#include "FXDEngine/Accounts/GooglePlay/AccountApiGP.h"

using namespace FXD;
using namespace Accounts;

// ------
// IAccountsApiGP
// - 
// ------
IAccountsApiGP::IAccountsApiGP(void)
	: IAccountsApi(E_AccountsApi::GooglePlay)
{
}

IAccountsApiGP::~IAccountsApiGP(void)
{
}

bool IAccountsApiGP::create_api(void)
{
	return true;
}

bool IAccountsApiGP::release_api(void)
{
	return true;
}
#endif //IsAccountGooglePlay()