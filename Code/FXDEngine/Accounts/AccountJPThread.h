// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_ACCOUNTJPTHREAD_H
#define FXDENGINE_ACCOUNTS_ACCOUNTJPTHREAD_H

#include "FXDEngine/Accounts/Types.h"
#include "FXDEngine/Core/FPS.h"
#include "FXDEngine/Core/Timer.h"
#include "FXDEngine/Job/AsyncJob.h"

namespace FXD
{
	namespace Accounts
	{
		// ------
		// IAccountJPThread
		// -
		// ------
		class IAccountJPThread : public Job::IAsyncTaskPoolThread
		{
		public:
			IAccountJPThread(void);
			virtual ~IAccountJPThread(void);

			const Core::StringView8 display_name(void) const FINAL { return UTF_8("AccountsThread"); }

			GET_REF_R(AccountSystem, accountSystem, account_system);

		protected:
			bool init_pool(void) FINAL;
			bool shutdown_pool(void) FINAL;
			void idle_process(void) FINAL;
			bool is_thread_restricted(void) const FINAL;

		private:
			Core::FPS m_fps;
			Core::Timer m_timer;
			AccountSystem m_accountSystem;
		};

		namespace Funcs
		{
			extern bool IsAccountsThreadRestricted(void);
			extern void RestrictAccountsThread(void);
			extern void UnrestrictAccountsThread(void);
		} //namespace Funcs

	} //namespace Accounts
} //namespace FXD
#endif //FXDENGINE_ACCOUNTS_ACCOUNTJPTHREAD_H