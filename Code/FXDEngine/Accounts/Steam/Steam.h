// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_STEAM_STEAM_H
#define FXDENGINE_ACCOUNTS_STEAM_STEAM_H

#include "FXDEngine/Accounts/Types.h"

#if IsAccountSteam()
	#include <steam/steam_api.h>
	#include <steam/ISteamUtils.h>
	#include <steam/ISteamFriends.h>
	#pragma comment(lib, "steam_api64.lib")
	#pragma comment(lib, "sdkencryptedappticket64.lib")

namespace FXD
{
	namespace Accounts
	{
		class IAccountsApiSteam;
		class IPlayerLocalSteam;
	} //namespace Accounts
} //namespace FXD
#endif //IsAccountSteam()
#endif //FXDENGINE_ACCOUNTS_STEAM_STEAM_H