// Creator - MatthewGolder
#include "FXDEngine/Accounts/Types.h"

#if IsAccountSteam()
#include "FXDEngine/Accounts/AccountSystem.h"
#include "FXDEngine/Accounts/Events.h"
#include "FXDEngine/Accounts/Steam/AccountApiSteam.h"
#include "FXDEngine/Accounts/Steam/PlayerLocalSteam.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/Input/InputJPThread.h"
#include "FXDEngine/Input/InputDevice.h"
#include "FXDEngine/Input/InputSystem.h"
#include "FXDEngine/Job/EventHandler.h"

using namespace FXD;
using namespace Accounts;

// ------
// IAccountSystem
// - 
// ------
bool IAccountSystem::_create_system(void)
{
	// 1. Early out if the api already exists
	if (m_accountApi)
	{
		return false;
	}

	// 2. Create the desired api
	FXD::FXDPlatform()->globals()[UTF_8("api_account")] = E_AccountsApi::Steam;
	m_accountApi = std::make_unique< Accounts::IAccountsApiSteam >();

	// 3. Create the desired api 
	bool bRetVal = m_accountApi->create_api();

	return bRetVal;
}

bool IAccountSystem::_release_system(void)
{
	m_activePlayers.clear();
	FXD_RELEASE(m_accountApi, release_api());
	return true;
}

void IAccountSystem::_process_os(FXD::F32 dt)
{
	if (m_accountApi != nullptr)
	{
		bool bRet1 = SteamAPI_IsSteamRunning();
		if (bRet1)
		{
			ISteamUtils* pSteamU = SteamUtils();

			bool bRet2 = pSteamU->IsOverlayEnabled();
			bool bRet3 = pSteamU->BOverlayNeedsPresent();
			bool bRet4 = pSteamU->IsSteamRunningInVR();
			bool bRet5 = pSteamU->IsSteamInBigPictureMode();
		}
		SteamAPI_RunCallbacks();
	}
}

void IAccountSystem::_process_signin(FXD::F32 dt)
{
	_check_player_joined();
}

void IAccountSystem::_check_player_joined(void)
{
	Thread::CSLock::LockGuard lock(m_lock);
	if (FXDInput() != nullptr)
	{
		Container::Vector< Input::InputDevice > unusedInputs = FXDInput()->input_system()->unused_devices();
		fxd_for(const auto& unusedInput, unusedInputs)
		{
			bool bPessed = false;
			switch (m_eSignInButton)
			{
				case E_PlayerJoinButton::Any:
				{
					bPessed = unusedInput->is_any_button_pressed();
					break;
				}
				case E_PlayerJoinButton::Action:
				{
					bPessed = unusedInput->is_action_button_pressed();
					break;
				}
				case E_PlayerJoinButton::Start:
				{
					bPessed = unusedInput->is_start_button_pressed();
					break;
				}
				default:
				{
					break;
				}
			}
			if (bPessed)
			{
				Input::InputDevice pressedDevice = unusedInput;

				Accounts::IPlayerLocalSteam::PlayerContext context;
				context.m_pSteamUser = (m_activePlayers.size() == 0) ? SteamUser() : nullptr;
				context.m_steamID = SteamUser()->GetSteamID().ConvertToUint64();

				Accounts::PlayerLocal player = std::make_shared< Accounts::IPlayerLocalSteam >(context);
				player->player_input().input_devices().push_back(pressedDevice);
				FXDInput()->input_system()->unused_devices().find_erase(pressedDevice);
				FXDInput()->input_system()->input_devices().push_back(pressedDevice);
				m_activePlayers.push_back(player);

				FXDEvent()->raise(Accounts::OnPlayerJoinedEvent(player));
			}
		}
	}
}
#endif //IsAccountSteam()