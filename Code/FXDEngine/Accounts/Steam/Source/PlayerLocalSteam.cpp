// Creator - MatthewGolder
#include "FXDEngine/Accounts/Types.h"

#if IsAccountSteam()
#include "FXDEngine/Accounts/Steam/PlayerLocalSteam.h"
#include "FXDEngine/Accounts/Steam/PlayerRemoteSteam.h"
#include "FXDEngine/Container/Bitset.h"

using namespace FXD;
using namespace Accounts;

FXD::Container::Bitset< 16 > nUnusedGuestIndices;

FXD::U32 get_unsed_guess_index(void)
{
	for (FXD::U32 n1 = 0; n1 < nUnusedGuestIndices.size(); ++n1)
	{
		if (!nUnusedGuestIndices.test(n1))
		{
			return n1;
		}
	}
	return 0;
}

void use_guest_index(FXD::U32 nIndex)
{
	nUnusedGuestIndices.set(nIndex);
}

void unuse_guest_index(FXD::U32 nIndex)
{
	nUnusedGuestIndices.reset(nIndex);
}


// ------
// IPlayerLocalSteam
// - 
// ------
IPlayerLocalSteam::IPlayerLocalSteam(const PlayerContext& context)
	: IPlayerLocal(E_AccountsApi::Steam)
	, m_pSteamUser(context.m_pSteamUser)
	, m_steamID(context.m_steamID)
	, m_nGuestID(0)
{
	if (m_pSteamUser != nullptr)
	{
		m_nGuestID = get_unsed_guess_index();
		use_guest_index(m_nGuestID);
	}
	else
	{
	}
}

IPlayerLocalSteam::~IPlayerLocalSteam(void)
{
	if (m_pSteamUser != nullptr)
	{
		unuse_guest_index(m_nGuestID);
	}
}

bool IPlayerLocalSteam::is_valid(void) const
{
	return false;
}

Core::String16 IPlayerLocalSteam::persona(void) const
{
	if (m_strPersona.empty())
	{
		if (m_pSteamUser != nullptr)
		{
			const FXD::UTF8* pName = SteamFriends()->GetPersonaName();
			m_strPersona = Core::String8(pName).to_string< FXD::UTF16 >();
		}
	}
	return m_strPersona;
}

Core::String8 IPlayerLocalSteam::platform_id(void) const
{
	if (m_strPlatformID.empty())
	{
		if (m_pSteamUser != nullptr)
		{
			FXD::U64 nInt = m_steamID.ConvertToUint64();
			Core::StringBuilder8 strB;
			strB << nInt;
			m_strPlatformID = strB.str();
		}
	}
	return m_strPlatformID;
}

Container::Vector< Accounts::PlayerRemote > IPlayerLocalSteam::_friends(void) const
{
	Container::Vector< Accounts::PlayerRemote > friends;

	if (m_pSteamUser == nullptr)
	{
		return friends;
	}

	FXD::U32 nFriendCount = SteamFriends()->GetFriendCount(k_EFriendFlagImmediate);
	for (FXD::U32 n1 = 0; n1 < nFriendCount; n1++)
	{
		Accounts::IPlayerRemoteSteam::PlayerContext context;
		context.m_steamID = SteamFriends()->GetFriendByIndex(n1, k_EFriendFlagImmediate);
		context.m_strPersona = Core::String8(SteamFriends()->GetFriendPersonaName(context.m_steamID)).to_string< FXD::UTF16 >();

		Core::StringBuilder8 strB;
		strB << context.m_steamID.ConvertToUint64();
		context.m_strPlatformID = strB.str();

		PlayerRemote player = std::make_shared< Accounts::IPlayerRemoteSteam >(context);
		friends.push_back(player);
	}
	return friends;
}

void IPlayerLocalSteam::_update_os(FXD::F32 /*dt*/)
{
}
#endif //IsAccountSteam()