// Creator - MatthewGolder
#include "FXDEngine/Accounts/Types.h"

#if IsAccountSteam()
#include "FXDEngine/Accounts/SignInContext.h"
#include "FXDEngine/Accounts/Steam/Steam.h"

using namespace FXD;
using namespace Accounts;

// ------
// Core::Impl::Impl< SignInContext >
// -
// ------
template <>
class Core::Impl::Impl< SignInContext >
{
public:
	friend class SignInContext;

public:
	Impl(void)
		: m_pSteamUser(nullptr)
	{
	}
	~Impl(void)
	{
	}

private:
	ISteamUser* m_pSteamUser;
};

// ------
// SignInContext
// -
// ------
SignInContext::SignInContext(void)
{
}

SignInContext::~SignInContext(void)
{
}

Accounts::E_SignIn SignInContext::signedin_status(void) const
{
	Accounts::E_SignIn eStatus = (m_impl->m_pSteamUser->BLoggedOn()) ? Accounts::E_SignIn::SignedIn : Accounts::E_SignIn::NotSignedIn;
	return eStatus;
}
#endif //IsAccountSteam()