// Creator - MatthewGolder
#include "FXDEngine/Accounts/Types.h"

#if IsAccountSteam()
#include "FXDEngine/Accounts/Steam/PlayerRemoteSteam.h"
#include "FXDEngine/Accounts/Steam/Steam.h"

using namespace FXD;
using namespace Accounts;

// ------
// IPlayerRemoteSteam
// - 
// ------
IPlayerRemoteSteam::IPlayerRemoteSteam(const PlayerContext& context)
	: IPlayerRemote(E_AccountsApi::Steam)
	, m_steamID(context.m_steamID)
{
	m_strPersona = context.m_strPersona;
	m_strPlatformID = context.m_strPlatformID;
}

IPlayerRemoteSteam::~IPlayerRemoteSteam(void)
{
}

bool IPlayerRemoteSteam::is_valid(void) const
{
	return false;
}

Core::String16 IPlayerRemoteSteam::persona(void) const
{
	return m_strPersona;
}

Core::String8 IPlayerRemoteSteam::platform_id(void) const
{
	return m_strPlatformID;
}

void IPlayerRemoteSteam::_update_os(FXD::F32 /*dt*/)
{
}
#endif //IsAccountSteam()