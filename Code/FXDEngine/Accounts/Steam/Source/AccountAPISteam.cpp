// Creator - MatthewGolder
#include "FXDEngine/Accounts/Types.h"

#if IsAccountSteam()
#include "FXDEngine/Accounts/Steam/AccountApiSteam.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/App/Steam/GlobalsSteam.h"

using namespace FXD;
using namespace Accounts;

// ------
// IAccountsApiSteam
// - 
// ------
IAccountsApiSteam::IAccountsApiSteam(void)
	: IAccountsApi(E_AccountsApi::Steam)
	, m_CallbackSteamShutdown(this, &IAccountsApiSteam::OnSteamShutdown)
	, m_CallbackPersonaNameChange(this, &IAccountsApiSteam::OnPersonaNameChanged)
	, m_CallbackGameOverlayActivated(this, &IAccountsApiSteam::OnGameOverlayActivated)
{
}

IAccountsApiSteam::~IAccountsApiSteam(void)
{
}

bool IAccountsApiSteam::create_api(void)
{
	bool bRetVal = SteamAPI_RestartAppIfNecessary(FXD::App::GetGlobalData().m_nAppID);
	if (!bRetVal)
	{
		PRINT_WARN << "App: SteamAPI_RestartAppIfNecessary() Failed";
		return false;
	}

	bRetVal = SteamAPI_Init();
	if (!bRetVal)
	{
		PRINT_WARN << "App: SteamAPI_Init() Failed";
		return 0;
	}
	SteamUtils()->SetOverlayNotificationPosition(FXD::App::GetGlobalData().m_steamOverlayPos);
	return bRetVal;
}

bool IAccountsApiSteam::release_api(void)
{
	SteamAPI_Shutdown();
	return true;
}

void IAccountsApiSteam::OnSteamShutdown(SteamShutdown_t* pCallback)
{
}

void IAccountsApiSteam::OnPersonaNameChanged(SetPersonaNameResponse_t* pCallback)
{
}

void IAccountsApiSteam::OnGameOverlayActivated(GameOverlayActivated_t* pCallback)
{
}
#endif //IsAccountSteam()