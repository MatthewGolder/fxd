// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_STEAM_ACCOUNTAPISTEAM_H
#define FXDENGINE_ACCOUNTS_STEAM_ACCOUNTAPISTEAM_H

#include "FXDEngine/Accounts/Types.h"

#if IsAccountSteam()
#include "FXDEngine/Accounts/AccountApi.h"
#include "FXDEngine/Accounts/Steam/Steam.h"

namespace FXD
{
	namespace Accounts
	{
		// ------
		// IAccountsApiSteam
		// -
		// ------
		class IAccountsApiSteam FINAL : public IAccountsApi
		{
		public:
			IAccountsApiSteam(void);
			~IAccountsApiSteam(void);

			bool create_api(void) FINAL;
			bool release_api(void) FINAL;

		private:
			STEAM_CALLBACK(IAccountsApiSteam, OnSteamShutdown, SteamShutdown_t, m_CallbackSteamShutdown);
			STEAM_CALLBACK(IAccountsApiSteam, OnPersonaNameChanged, SetPersonaNameResponse_t, m_CallbackPersonaNameChange);
			STEAM_CALLBACK(IAccountsApiSteam, OnGameOverlayActivated, GameOverlayActivated_t, m_CallbackGameOverlayActivated);
		};

	} //namespace Accounts
} //namespace FXD

#endif //IsAccountSteam()
#endif //FXDENGINE_ACCOUNTS_STEAM_ACCOUNTAPISTEAM_H