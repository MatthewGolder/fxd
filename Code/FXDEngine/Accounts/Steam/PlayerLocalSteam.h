// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_STEAM_PLAYERLOCALSTEAM_H
#define FXDENGINE_ACCOUNTS_STEAM_PLAYERLOCALSTEAM_H

#include "FXDEngine/Accounts/Types.h"

#if IsAccountSteam()
#include "FXDEngine/Accounts/PlayerLocal.h"
#include "FXDEngine/Accounts/Steam/Steam.h"

namespace FXD
{
	namespace Accounts
	{
		// ------
		// IPlayerLocalSteam
		// -
		// ------
		class IPlayerLocalSteam : public IPlayerLocal
		{
		public:
			class PlayerContext
			{
			public:
				PlayerContext(void)
					: m_pSteamUser(nullptr)
				{
				}
				~PlayerContext(void)
				{
				}

			public:
				ISteamUser* m_pSteamUser;
				CSteamID m_steamID;
			}; 

		public:
			EXPLICIT	IPlayerLocalSteam(const PlayerContext& context);
			virtual ~IPlayerLocalSteam(void);

			bool is_valid(void) const FINAL;

			Core::String16 persona(void) const FINAL;
			Core::String8 platform_id(void) const FINAL;

		protected:
			Container::Vector< Accounts::PlayerRemote > _friends(void) const;
		
			void _update_os(FXD::F32 dt) FINAL;

		protected:
			ISteamUser* m_pSteamUser;
			CSteamID m_steamID;
			FXD::U32 m_nGuestID;
		};

	} //namespace Accounts
} //namespace FXD

#endif //IsAccountSteam()
#endif //FXDENGINE_ACCOUNTS_STEAM_PLAYERLOCALSTEAM_H