// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_STEAM_PLAYERLOCALREMOTESTEAM_H
#define FXDENGINE_ACCOUNTS_STEAM_PLAYERLOCALREMOTESTEAM_H

#include "FXDEngine/Accounts/Types.h"

#if IsAccountSteam()
#include "FXDEngine/Accounts/PlayerRemote.h"
#include "FXDEngine/Accounts/Steam/Steam.h"

namespace FXD
{
	namespace Accounts
	{
		// ------
		// IPlayerRemoteSteam
		// -
		// ------
		class IPlayerRemoteSteam : public IPlayerRemote
		{
		public:
			class PlayerContext
			{
			public:
				PlayerContext(void)
				{
				}
				~PlayerContext(void)
				{
				}

			public:
				CSteamID m_steamID;
				Core::String16 m_strPersona;
				Core::String8 m_strPlatformID;
			};

		public:
			EXPLICIT IPlayerRemoteSteam(const PlayerContext& context);
			virtual ~IPlayerRemoteSteam(void);

			bool is_valid(void) const FINAL;

			Core::String16 persona(void) const FINAL;
			Core::String8 platform_id(void) const FINAL;

		protected:
			void _update_os(FXD::F32 dt) FINAL;

		protected:
			CSteamID m_steamID;
		};

	} //namespace Accounts
} //namespace FXD

#endif //IsAccountSteam()
#endif //FXDENGINE_ACCOUNTS_STEAM_PLAYERLOCALREMOTESTEAM_H