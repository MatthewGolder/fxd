// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_ACCOUNTSYSTEM_H
#define FXDENGINE_ACCOUNTS_ACCOUNTSYSTEM_H

#include "FXDEngine/Job/AsyncJob.h"
#include "FXDEngine/Accounts/AccountApi.h"

namespace FXD
{
	namespace Accounts
	{
		// ------
		// IAccountSystem
		// - 
		// ------
		class IAccountSystem FINAL : protected Core::NonCopyable
		{
		public:
			friend class IAccountJPThread;

		public:
			IAccountSystem(void);
			~IAccountSystem(void);

			Job::Future< bool > create_system(void);
			Job::Future< bool > release_system(void);

			Job::Future< void > player_joined(Accounts::E_PlayerJoinButton eButton);

			Job::Future< bool > player_deactivate(const Accounts::PlayerLocal& player);
			Job::Future< Accounts::PlayerLocal > player_from_puid(const Core::String8& strPUI);

		private:
			bool _create_system(void);
			bool _release_system(void);

			void _player_joined(const Accounts::E_PlayerJoinButton eButton);


			void _check_player_joined(void);
			bool _player_deactivate(const Accounts::PlayerLocal& player);
			Accounts::PlayerLocal _player_from_puid(const Core::String8& strPUI);

			void _process(FXD::F32 dt);
			void _process_os(FXD::F32 dt);
			void _process_signin(FXD::F32 dt);

			GET_REF_W(Container::Vector< Accounts::PlayerLocal >, activePlayers, active_players);

		private:
			Thread::CSLock m_lock;
			Accounts::AccountsApi m_accountApi;
			Accounts::E_PlayerJoinButton m_eSignInButton;
			Container::Vector< Accounts::PlayerLocal > m_activePlayers;
		};

	} //namespace Accounts
} //namespace FXD
#endif //FXDENGINE_ACCOUNTS_ACCOUNTSYSTEM_H