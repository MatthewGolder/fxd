// Creator - MatthewGolder
#include "FXDEngine/Accounts/AccountSystem.h"
#include "FXDEngine/Accounts/AccountJPThread.h"
#include "FXDEngine/Accounts/PlayerLocal.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Core/WatchDogThread.h"
#include "FXDEngine/Job/Async.h"
#include "FXDEngine/Input/InputJPThread.h"
#include "FXDEngine/Input/InputSystem.h"

using namespace FXD;
using namespace Accounts;

// ------
// IAccountSystem
// - 
// ------
IAccountSystem::IAccountSystem(void)
	: m_accountApi(nullptr)
	, m_eSignInButton(Accounts::E_PlayerJoinButton::Any)
{
}

IAccountSystem::~IAccountSystem(void)
{
	PRINT_COND_ASSERT((m_accountApi == nullptr), "Accounts: m_accountApi is not shutdown");
	PRINT_COND_ASSERT((m_activePlayers.size() == 0), "Accounts: m_activePlayers are not shutdown");
}

Job::Future< bool > IAccountSystem::create_system(void)
{
	auto funcRef = [&]() { return _create_system(); };

	return Job::Async(FXDAccounts(), [=]() { return funcRef(); });
}

Job::Future< bool > IAccountSystem::release_system(void)
{
	auto funcRef = [&]() { return _release_system(); };

	return Job::Async(FXDAccounts(), [=]() { return funcRef(); });
}

Job::Future< void > IAccountSystem::player_joined(Accounts::E_PlayerJoinButton eButton)
{
	auto funcRef = [&](Accounts::E_PlayerJoinButton eButton) { _player_joined(eButton); };

	return Job::Async(FXDAccounts(), [=]() { return funcRef(eButton); });
}

Job::Future< bool > IAccountSystem::player_deactivate(const Accounts::PlayerLocal& player)
{
	auto funcRef = [&](const Accounts::PlayerLocal& player) { return _player_deactivate(player); };

	return Job::Async(FXDAccounts(), [=]() { return funcRef(player); });
}

Job::Future< Accounts::PlayerLocal > IAccountSystem::player_from_puid(const Core::String8& strPUI)
{
	auto funcRef = [&](const Core::String8& strPUI) { return _player_from_puid(strPUI); };

	return Job::Async(FXDAccounts(), [=]() { return funcRef(strPUI); });
}

void IAccountSystem::_player_joined(const Accounts::E_PlayerJoinButton eButton)
{
	Thread::CSLock::LockGuard lock(m_lock);

	m_eSignInButton = eButton;
}

bool IAccountSystem::_player_deactivate(const Accounts::PlayerLocal& player)
{
	Thread::CSLock::LockGuard lock(m_lock);

	if (!m_activePlayers.contains(player))
	{
		return false;
	}

	fxd_for(auto& device, player->player_input().input_devices())
	{
		FXD_ERROR("Fix this");
		//FXDInput()->input_system()->input_devices().find_erase(device);
		//FXDInput()->input_system()->unused_devices().push_back(device);
	}
	m_activePlayers.find_erase(player);
	return true;
}

Accounts::PlayerLocal IAccountSystem::_player_from_puid(const Core::String8& strPUI)
{
	Thread::CSLock::LockGuard lock(m_lock);

	fxd_for(const auto& activePlayer, m_activePlayers)
	{
		if (activePlayer->platform_id() == strPUI)
		{
			return activePlayer;
		}
	}
	return nullptr;
}

void IAccountSystem::_process(FXD::F32 dt)
{
	PRINT_COND_ASSERT((!Accounts::Funcs::IsAccountsThreadRestricted()), "Accounts: Not allowed on current thread");

#if defined(FXD_ENABLE_WATCHDOG)
	static Core::WatchDogThread sWDThread("AccountSystem - WatchDog", 500);
	sWDThread.increment();
#endif

	_process_os(dt);
	_process_signin(dt);

	fxd_for(auto& activePlayer, m_activePlayers)
	{
		activePlayer->_update(dt);
	}
}