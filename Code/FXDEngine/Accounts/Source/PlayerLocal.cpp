// Creator - MatthewGolder
#include "FXDEngine/Accounts/PlayerLocal.h"
#include "FXDEngine/Accounts/AccountJPThread.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace Accounts;

// ------
// IPlayerLocal
// -
// ------
IPlayerLocal::IPlayerLocal(Accounts::E_AccountsApi eAccountApi)
	: IPlayer(eAccountApi)
{
}

IPlayerLocal::~IPlayerLocal(void)
{
}

bool IPlayerLocal::is_local(void) const
{
	return true;
}

Job::Future< Container::Vector< Accounts::PlayerRemote > > IPlayerLocal::friends(void) const
{
	auto funcRef = [&](void) { return _friends(); };

	return Job::Async(FXDAccounts(), [=]() { return funcRef(); });
}