// Creator - MatthewGolder
#include "FXDEngine/Accounts/Player.h"
#include "FXDEngine/Accounts/AccountApi.h"

using namespace FXD;
using namespace Accounts;

// ------
// IPlayer
// -
// ------
IPlayer::IPlayer(E_AccountsApi eAccountApi)
	: m_eAccountApi(eAccountApi)
{
}

IPlayer::~IPlayer(void)
{
}

Core::String8 IPlayer::globalID(void) const
{
	return Accounts::Funcs::ToString(m_eAccountApi).c_str() + FXD::Core::to_string8("|") + platform_id();
}

void IPlayer::_update(FXD::F32 dt)
{
	_update_os(dt);
}