// Creator - MatthewGolder
#include "FXDEngine/Accounts/PlayerInput.h"
#include "FXDEngine/Input/InputDevice.h"

using namespace FXD;
using namespace Accounts;

// ------
// PlayerInput
// -
// ------
PlayerInput::PlayerInput(void)
	: m_bGamepadAvailable(false)
	, m_bKeyboardAvailable(false)
	, m_inputMapDesc(nullptr)
{
}

PlayerInput::~PlayerInput(void)
{
}

void PlayerInput::set_inputmap_def(const Input::InputMapDesc& inputMapDef, const Core::String8& strControlScheme)
{
	Thread::RWLock::ScopedWriteLock lock(m_lock);
	if ((m_inputMapDesc == inputMapDef) && (m_strControlScheme == strControlScheme))
	{
		return;
	}
	m_inputMapDesc = inputMapDef;
	m_strControlScheme = strControlScheme;
	_build_inputmap_instance();
}

bool PlayerInput::is_any_button_pressed(void) const
{
	FXD::Thread::RWLock::ScopedReadLock lock(m_lock);

	return FXD::STD::any_of(m_inputDevices.begin(), m_inputDevices.end(), [](const auto& device)
	{
		return device->is_any_button_pressed();
	});
}

bool PlayerInput::is_action_button_pressed(void) const
{
	FXD::Thread::RWLock::ScopedReadLock lock(m_lock);

	return FXD::STD::any_of(m_inputDevices.begin(), m_inputDevices.end(), [](const auto& device)
	{
		return device->is_action_button_pressed();
	});
}

bool PlayerInput::is_start_button_pressed(void) const
{
	FXD::Thread::RWLock::ScopedReadLock lock(m_lock);

	return FXD::STD::any_of(m_inputDevices.begin(), m_inputDevices.end(), [](const auto& device)
	{
		return device->is_start_button_pressed();
	});
}

bool PlayerInput::is_mouse_connected(void) const
{
	FXD::Thread::RWLock::ScopedReadLock lock(m_lock);

	return FXD::STD::any_of(m_inputDevices.begin(), m_inputDevices.end(), [](const auto& device)
	{
		return device->is_mouse_available();
	});
}

bool PlayerInput::is_keyboard_connected(void) const
{
	FXD::Thread::RWLock::ScopedReadLock lock(m_lock);

	return FXD::STD::any_of(m_inputDevices.begin(), m_inputDevices.end(), [](const auto& device)
	{
		return device->is_keyboard_available();
	});
}

bool PlayerInput::is_controller_connected(void) const
{
	FXD::Thread::RWLock::ScopedReadLock lock(m_lock);

	return FXD::STD::any_of(m_inputDevices.begin(), m_inputDevices.end(), [](const auto& device)
	{
		return device->is_connected();
	});
}

void PlayerInput::_update(FXD::F32 /*dt*/)
{
	Thread::RWLock::ScopedWriteLock lock(m_lock);

	bool bMouseAvailable = false;
	bool bGamepadAvailable = false;
	bool bKeyboardAvailable = false;
	fxd_for(const auto& device, m_inputDevices)
	{
		//(*itr)->_update(dt);
		bMouseAvailable |= device->is_gamepad_available();
		bGamepadAvailable |= device->is_gamepad_available();
		bKeyboardAvailable |= device->is_keyboard_available();
	}
	m_bMouseAvailable = bMouseAvailable;
	m_bGamepadAvailable = bGamepadAvailable;
	m_bKeyboardAvailable = bKeyboardAvailable;
	//m_isInitialised = true;
}

void PlayerInput::_build_inputmap_instance(void)
{
	Thread::RWLock::ScopedWriteLock lock(m_lock);

	m_inputMap.build(m_inputMapDesc, m_strControlScheme, m_inputDevices);
}