// Creator - MatthewGolder
#include "FXDEngine/Accounts/PlayerRemote.h"

using namespace FXD;
using namespace Accounts;

// ------
// IPlayerRemote
// -
// ------
IPlayerRemote::IPlayerRemote(E_AccountsApi eAccountApi)
	: IPlayer(eAccountApi)
{
}

IPlayerRemote::~IPlayerRemote(void)
{
}

bool IPlayerRemote::is_local(void) const
{
	return false;
}