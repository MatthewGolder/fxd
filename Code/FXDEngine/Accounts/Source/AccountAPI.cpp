// Creator - MatthewGolder
#include "FXDEngine/Accounts/AccountApi.h"
#include "FXDEngine/Core/TypeTraits.h"

using namespace FXD;
using namespace Accounts;

// ------
// IAccountsApi
// -
// ------
IAccountsApi::IAccountsApi(Accounts::E_AccountsApi eAccountApi)
	: m_eAccountApi(eAccountApi)
{
}

IAccountsApi::~IAccountsApi(void)
{
}

// ------
// Funcs
// -
// ------
const Core::StringView8 sAccountApis[] =
{
	"Win32",			// E_AccountsApi::Win32
	"Steam",			// E_AccountsApi::Steam
	"GooglePlay",	// E_AccountsApi::GooglePlay
	"Count"			// E_AccountsApi::Count
};

extern const Accounts::E_AccountsApi FXD::Accounts::Funcs::AccountType(void)
{
#if IsAccountWindows()
	return Accounts::E_AccountsApi::Win32;
#elif IsAccountSteam()
	return Accounts::E_AccountsApi::Steam;
#elif IsAccountGooglePlay()
	return Accounts::E_AccountsApi::GooglePlay;
#endif
}

const Core::StringView8 FXD::Accounts::Funcs::ToString(const Accounts::E_AccountsApi eApi)
{
	return sAccountApis[FXD::STD::to_underlying(eApi)];
}

Accounts::E_AccountsApi FXD::Accounts::Funcs::ToApiType(const Core::StringView8 strType)
{
	for (FXD::S32 n1 = 0; n1 < FXD::STD::to_underlying(Accounts::E_AccountsApi::Count); ++n1)
	{
		if (strType.compare_no_case(sAccountApis[n1]) == 0)
		{
			return (Accounts::E_AccountsApi)n1;
		}
	}
	return Accounts::E_AccountsApi::Unknown;
}