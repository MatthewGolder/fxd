// Creator - MatthewGolder
#include "FXDEngine/Accounts/AccountJPThread.h"
#include "FXDEngine/Accounts/AccountSystem.h"

using namespace FXD;
using namespace Accounts;

// 0 - Not Initialised
// 1 - Initialised
// 2 - ShutDown
FXD::S32 g_nAccountStatus = 0;

// ------
// IAccountJPThread
// -
// ------
IAccountJPThread::IAccountJPThread(void)
	: IAsyncTaskPoolThread(display_name())
	, m_accountSystem(nullptr)
{
	if (g_nAccountStatus == 2)
	{
		PRINT_ASSERT << "Accounts: IAccountJPThread accessed after shutdown";
	}
}

IAccountJPThread::~IAccountJPThread(void)
{
	stop_and_wait_for_exit();
}

bool IAccountJPThread::init_pool(void)
{
	Accounts::Funcs::RestrictAccountsThread();
	PRINT_INFO << "Accounts: IAccountJPThread::init_pool()";

	m_accountSystem = std::make_unique< Accounts::IAccountSystem >();
	m_timer.start_counter();
	g_nAccountStatus = 1;
	m_shared.get_quick_write_lock()->m_eState = Job::E_AsyncState::Running;
	return true;
}

bool IAccountJPThread::shutdown_pool(void)
{
	m_accountSystem = nullptr;
	Accounts::Funcs::UnrestrictAccountsThread();
	PRINT_INFO << "Accounts: IAccountJPThread::shutdown_pool()";

	g_nAccountStatus = 2;
	m_shared.get_quick_write_lock()->m_eState = Job::E_AsyncState::ShutDown;
	return true;
}

void IAccountJPThread::idle_process(void)
{
	if (is_shuttingdown())
	{
		return;
	}

	const FXD::S32 nProcessFrequency = 10;

	// 1. Get initial time stamp.
	FXD::F32 ts1 = (FXD::F32)Core::Funcs::GetTimeSecsF();

	FXD::F32 dt = m_timer.elapsed_time();
	m_fps.update(dt);
	if (m_accountSystem)
	{
		m_accountSystem->_process(dt);
	}

	// 2. Get final time stamp and wait for next update interval.
	FXD::F32 ts2 = (FXD::F32)Core::Funcs::GetTimeSecsF();
	FXD::S32 nToWait = nProcessFrequency - (FXD::S32)((ts2 - ts1) * 1000.0f);
	if (nToWait > 0)
	{
		Thread::Funcs::Sleep(nToWait);
	}
}

bool IAccountJPThread::is_thread_restricted(void) const
{
	return Accounts::Funcs::IsAccountsThreadRestricted();
}

// ------
// Funcs
// -
// ------
static FXD::S32 g_ContextThread = 0;
bool FXD::Accounts::Funcs::IsAccountsThreadRestricted(void)
{
	return (g_ContextThread != 0) && (g_ContextThread != Thread::Funcs::GetCurrentThreadID());
}

void FXD::Accounts::Funcs::RestrictAccountsThread(void)
{
	PRINT_COND_ASSERT((!Accounts::Funcs::IsAccountsThreadRestricted()), "Accounts: Not allowed on current thread" << Thread::Funcs::GetCurrentThreadID());
	g_ContextThread = Thread::Funcs::GetCurrentThreadID();
}

void FXD::Accounts::Funcs::UnrestrictAccountsThread(void)
{
	PRINT_COND_ASSERT((!Accounts::Funcs::IsAccountsThreadRestricted()), "Accounts: Not allowed on current thread" << Thread::Funcs::GetCurrentThreadID());
	g_ContextThread = 0;
}