// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_SIGNINCONTEXT_H
#define FXDENGINE_ACCOUNTS_SIGNINCONTEXT_H

#include "FXDEngine/Accounts/Types.h"
#include "FXDEngine/Core/Pimpl.h"

namespace FXD
{
	namespace Accounts
	{
		// ------
		// SignInContext
		// -
		// ------
		class SignInContext : public Core::HasImpl< SignInContext, 128 >
		{
		public:
			friend class IPlayerLocal;

		protected:
			SignInContext(void);
			~SignInContext(void);

			Accounts::E_SignIn signedin_status(void) const;

			//void handle_signedin(void) const;
			//void handle_signedout(void) const;

		public:
		};

	} //namespace Accounts
} //namespace FXD
#endif //FXDENGINE_ACCOUNTS_SIGNINCONTEXT_H