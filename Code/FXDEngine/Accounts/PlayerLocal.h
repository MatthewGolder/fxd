// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_ACCOUNTS_PLAYERLOCAL_H
#define FXDENGINE_ACCOUNTS_PLAYERLOCAL_H

#include "FXDEngine/Accounts/Player.h"
#include "FXDEngine/Accounts/PlayerInput.h"
#include "FXDEngine/Accounts/SignInContext.h"
#include "FXDEngine/IO/DataRegistry.h"
#include "FXDEngine/Job/AsyncJob.h"

namespace FXD
{
	namespace Accounts
	{
		// ------
		// IPlayerLocal
		// -
		// ------
		class IPlayerLocal : public IPlayer
		{
		public:
			friend class IAccountSystem;

		public:
			EXPLICIT IPlayerLocal(Accounts::E_AccountsApi eAccountApi);
			virtual ~IPlayerLocal(void);

			bool is_local(void) const FINAL;
			virtual bool is_valid(void) const PURE;

			virtual Core::String16 persona(void) const PURE;
			virtual Core::String8 platform_id(void) const PURE;

			Job::Future< Container::Vector< Accounts::PlayerRemote > > friends(void) const;

			GET_REF_W(IO::DataRegistry, dataRegistry, data_registry);
			GET_REF_W(Accounts::PlayerInput, playerInput, player_input);
			GET_REF_W(Accounts::SignInContext, signInContext, sign_in_context);

		protected:
			virtual Container::Vector< Accounts::PlayerRemote > _friends(void) const PURE;
		
			virtual void _update_os(FXD::F32 dt) PURE;

		protected:
			IO::DataRegistry m_dataRegistry;
			Accounts::PlayerInput m_playerInput;
			Accounts::SignInContext m_signInContext;
		};

	} //namespace Accounts
} //namespace FXD
#endif //FXDENGINE_ACCOUNTS_PLAYERLOCAL_H