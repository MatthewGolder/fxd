// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_NEW_H
#define FXDENGINE_MEMORY_NEW_H

#include "FXDEngine/Memory/Internal/AlignValT.h"
#include "FXDEngine/Memory/Internal/New.h"

#endif //FXDENGINE_MEMORY_NEW_H