// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_OBJECTPERMANENT_H
#define FXDENGINE_MEMORY_OBJECTPERMANENT_H

#include "FXDEngine/Core/CompileTimeChecks.h"
#include "FXDEngine/Core/Types.h"

namespace FXD
{
	namespace Memory
	{
		// ------
		// ObjectPermanent
		// -
		// ------
		template < typename Type >
		class ObjectPermanent
		{
		public:
			ObjectPermanent(void)
			{
				FXD::PtrSizedInt ptr = (FXD::PtrSizedInt)&m_buffer[0];
				size_t nAlign = FXD_ALIGN_OF(Type);
				ptr = (ptr + nAlign - 1) & ~(nAlign - 1);
				m_pType = (Type*)ptr;
				FXD_PLACEMENT_NEW(m_pType, Type);
			}
			~ObjectPermanent(void)
			{}

			inline bool is_valid(void) const
			{
				return (m_pType != nullptr);
			}

			inline Type& operator*() const { return *m_pType; }
			inline Type* operator->() const { return m_pType; }

		private:
			FXD::U8 m_buffer[sizeof(Type) + FXD_ALIGN_OF(Type)];
			Type* m_pType;
		};

	} //namespace Memory
} //namespace FXD
#endif //FXDENGINE_MEMORY_OBJECTPERMANENT_H