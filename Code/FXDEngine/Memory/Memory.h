// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_MEMORY_H
#define FXDENGINE_MEMORY_MEMORY_H

#include "FXDEngine/Memory/Internal/AddressOf.h"
#include "FXDEngine/Memory/Internal/AllocatorArgT.h"
#include "FXDEngine/Memory/Internal/Destroy.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Copy.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Copy_Copy.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Copy_n.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Copy_Ptr.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Default_Construct.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Default_Construct_n.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Default_Fill_Ptr_n.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Fill.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Fill_Move.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Fill_n.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Fill_Ptr_n.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Move.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Move_Fill.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Move_If_Noexcept.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Move_n.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Move_Ptr.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Move_Ptr_If_Noexcept.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Value_Construct.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Value_Construct_n.h"
#include "FXDEngine/Memory/Internal/UsesAllocator.h"

#endif //FXDENGINE_MEMORY_MEMORY_H