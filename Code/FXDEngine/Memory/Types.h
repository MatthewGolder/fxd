// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_TYPES_H
#define FXDENGINE_MEMORY_TYPES_H

#include <memory>

#if defined(FXD_DEBUG)
#	define FXD_ENABLE_MEMORY_TRACKING 1
#	define FXD_ENABLE_MEMORY_BOUNDS_CHECK 1
#endif

namespace FXD
{
	namespace Memory
	{
		class MemHandle;

		class StreamMemory;

		class IStreamMemHandle;
		typedef std::shared_ptr< Memory::IStreamMemHandle > StreamMemHandle;

	} //namespace Memory
} //namespace FXD

#endif //FXDENGINE_MEMORY_TYPES_H