// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_STREAMMEMORY_H
#define FXDENGINE_MEMORY_STREAMMEMORY_H

#include "FXDEngine/Core/StreamIn.h"
#include "FXDEngine/Core/StreamOut.h"
#include "FXDEngine/Core/SerializeInBinary.h"
#include "FXDEngine/Core/SerializeOutBinary.h"

namespace FXD
{
	namespace Memory
	{
		// ------
		// StreamMemory
		// -
		// ------
		class StreamMemory FINAL : public Core::IStreamIn, public Core::ISerializeInBinary, public Core::IStreamOut, public Core::ISerializeOutBinary
		{
		public:
			StreamMemory(Core::CompressParams compressParams = Core::CompressParams::Default);
			~StreamMemory(void);

			FXD::U64 read_size(void* pData, const FXD::U64 nSize) FINAL;
			FXD::U64 write_size(const void* pData, const FXD::U64 nSize) FINAL;

			FXD::U64 length(void) FINAL;
			FXD::S32 tell(void) FINAL;
			FXD::U64 seek_to(FXD::U64 nOffset, Core::E_SeekOffset eFrom) FINAL;

		private:
			void* m_pBuffer;			// Memory buffer.
			FXD::U64 m_nBufferSize;	// Size of buffer.
			FXD::U64 m_nDataSize;	// Size of valid data.
			FXD::U64 m_nIOOffset;	// Current IO offset.
		};
	} //namespace Memory
} //namespace FXD
#endif //FXDENGINE_MEMORY_STREAMMEMORY_H