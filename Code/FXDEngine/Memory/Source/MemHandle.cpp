// Creator - MatthewGolder
#include "FXDEngine/Memory/MemHandle.h"
#include "FXDEngine/Memory/Memory.h"
#include "FXDEngine/Memory/ObjectPermanent.h"
#include "FXDEngine/Memory/Allocator/Aligned.h"
#include "FXDEngine/Thread/AtomicInt.h"
#include "FXDEngine/Core/Debugger.h"

#include "3rdParty/zlib/zlib.h"
#include <3rdParty/xxHash/xxhash.h>

using namespace FXD;
using namespace Memory;

const FXD::U64 kMemSeed = 100;

// ------
// MemHandleHeader
// -
// ------
class MemHandleHeader
{
public:
	MemHandleHeader(void* pMem)
		: m_ref(0)
		, m_lockCount(0)
		, m_pMem(pMem)
	{}
	~MemHandleHeader(void)
	{}

	inline void increment_ref(void)
	{
		m_ref.increment();
	}
	inline bool release_ref(void)
	{
		return (m_ref.decrement() == 0);
	}
	void* lock(void)
	{
		m_lockCount.increment();
		return m_pMem;
	}
	void unlock(void)
	{
		m_lockCount.decrement();
	}

public:
	Thread::AtomicInt m_ref;
	Thread::AtomicInt m_lockCount;
	void* m_pMem;
};
using MemHandleAllocator = Memory::Alloc::AlignedType< Memory::Alloc::Heap, MemHandleHeader >;

// ------
// VoidMemHandlePolicy
// -
// ------
class VoidMemHandlePolicy : public Memory::IMemPolicy
{
public:
	VoidMemHandlePolicy(void)
	{}
	~VoidMemHandlePolicy(void)
	{}

	void increment_ref(void* /*pData*/) FINAL
	{
	}

	void release_ref(void* /*pData*/) FINAL
	{
	}

	void* lock(void* pData) FINAL
	{
		return pData;
	}

	void unlock(void* /*pData*/) FINAL
	{
	}
};

Memory::IMemPolicy* IMemPolicy::get_void_mem_policy(void)
{
	static Memory::ObjectPermanent< VoidMemHandlePolicy > voidWrapper;
	return &*voidWrapper;
}

// ------
// DefaultMemPolicy
// -
// ------
class DefaultMemPolicy : public Memory::IMemPolicy
{
public:
	DefaultMemPolicy(void)
	{}
	~DefaultMemPolicy(void)
	{}

	void increment_ref(void* pData) FINAL
	{
		((MemHandleHeader*)pData)->increment_ref();
	}
	void release_ref(void* pData) FINAL
	{
		if (((MemHandleHeader*)pData)->release_ref())
		{
			MemHandleAllocator alloc;
			alloc.deallocate(pData/*, "deallocate"*/);
		}
	}
	void* lock(void* pData) FINAL
	{
		return ((MemHandleHeader*)pData)->lock();
	}
	void unlock(void* pData) FINAL
	{
		((MemHandleHeader*)pData)->unlock();
	}
};

static DefaultMemPolicy& get_default_memhandle_policy(void)
{
	static Memory::ObjectPermanent< DefaultMemPolicy > g_memHandlePolicy;
	return *g_memHandlePolicy;
}

// ------
// MemHandle::LockGuard
// -
// ------
MemHandle::LockGuard::LockGuard(const Memory::MemHandle& rhs)
	: m_memHandle(rhs)
	, m_pMem(rhs.m_pPolicy->lock(rhs.m_pData))
{
}

MemHandle::LockGuard::LockGuard(LockGuard&& rhs)
	: m_memHandle(std::move(rhs.m_memHandle))
	, m_pMem(rhs.m_pMem)
{
	rhs.m_pMem = nullptr;
}

MemHandle::LockGuard::~LockGuard(void)
{
	m_memHandle.m_pPolicy->unlock(m_memHandle.m_pData);
}

void* MemHandle::LockGuard::get_mem(void) const
{
	return m_pMem;
}

void* MemHandle::LockGuard::operator+(size_t nSize) const
{
	return (FXD::U8*)get_mem() + nSize;
}

// ------
// MemHandle
// -
// ------
void MemHandle::deallocate(void)
{
	if (m_pPolicy != nullptr)
	{
		m_pPolicy->release_ref(m_pData);
	}
	m_pData = nullptr;
	m_pPolicy = nullptr;
}

bool MemHandle::compare_memhandle(const MemHandle& memHandle) const
{
	if (get_size() != memHandle.get_size())
	{
		return (get_size() < memHandle.get_size());
	}

	void* pLHS = MemHandle::LockGuard(*this).get_mem();
	void* pRHS = MemHandle::LockGuard(memHandle).get_mem();
	return (Memory::MemCompare(pLHS, pRHS, get_size()) == 0);
}

bool MemHandle::is_vaild(void) const
{
	return (m_pData != nullptr);
}

bool MemHandle::operator<(const MemHandle& rhs) const
{
	return compare_memhandle(rhs);
}

bool MemHandle::operator==(const MemHandle& rhs) const
{
	return (m_pData == rhs.m_pData) && (m_nSize == rhs.m_nSize) && (m_pPolicy == rhs.m_pPolicy);
}

bool MemHandle::operator!=(const MemHandle& rhs) const
{
	return (!operator == (rhs));
}

MemHandle MemHandle::compress(void) const
{
	if (get_size() == 0)
	{
		return (*this);
	}

	uLong nBoundL = ::compressBound((uInt)get_size());

	Memory::MemHandle memInter = Memory::MemHandle::allocate(nBoundL);

	{
		MemHandle::LockGuard memLockThis(*this);
		MemHandle::LockGuard memLockInter(memInter);
		::compress((Bytef*)memLockInter.get_mem(), &nBoundL, (Bytef*)memLockThis.get_mem(), (uInt)get_size());
	}

	Memory::MemHandle memRet = Memory::MemHandle::allocate(nBoundL + 4);
	{
		MemHandle::LockGuard memLockRet(memRet);
		MemHandle::LockGuard memLockInter(memInter);

		*(FXD::S32*)memLockRet.get_mem() = (FXD::S32)get_size();
		Memory::MemCopy((FXD::S8*)memLockRet.get_mem() + 4, memLockInter.get_mem(), nBoundL);
	}
	return memRet;
}

MemHandle MemHandle::decompress(void) const
{
	if (get_size() == 0)
	{
		return (*this);
	}
	MemHandle::LockGuard memLockThis(*this);

	uInt nSize1 = *(uInt*)memLockThis.get_mem();
	uLong nSize2 = nSize1;
	Memory::MemHandle memRet = Memory::MemHandle::allocate(nSize1);
	MemHandle::LockGuard memLockRet(memRet);

	::uncompress((Bytef*)memLockRet.get_mem(), &nSize2, (Bytef*)((FXD::S8*)memLockThis.get_mem() + 4), (uInt)get_size());
	return memRet;
}

FXD::HashValue MemHandle::to_hash(void) const
{
	const FXD::U8* pPtr = (FXD::U8*)MemHandle::LockGuard(*this).get_mem();
	const FXD::S32 nSize = get_size();
	FXD::HashValue retHash = XXH64(pPtr, nSize, kMemSeed);
	return retHash;
}

void MemHandle::clear_memory(void)
{
	FXD::U8* pPtr = (FXD::U8*)MemHandle::LockGuard(*this).get_mem();
	const FXD::S32 nSize = get_size();
	Memory::MemZero(pPtr, nSize);
}

MemHandle MemHandle::allocate(size_t nSize, size_t nAlign)
{
	MemHandleAllocator alloc;

	size_t nReqSize = (nSize + sizeof(MemHandleHeader));
	if (nAlign > alloc.alignment())
	{
		nReqSize += nAlign - alloc.alignment();
	}
	else
	{
		nAlign = 1;
	}

	void* pMem = alloc.allocate(nReqSize);

	FXD::PtrSizedInt nAligned = (FXD::PtrSizedInt)pMem + sizeof(MemHandleHeader);
	if (nAlign)
	{
		nAligned = (nAligned + nAlign - 1) & ~(nAlign - 1);
	}

	FXD_PLACEMENT_NEW((MemHandleHeader*)pMem, MemHandleHeader)((void*)nAligned);
	return Memory::MemHandle((void*)pMem, nSize, &get_default_memhandle_policy());
}

Memory::MemHandle MemHandle::allocate_copy(void* pData, size_t nSize, size_t nAlign)
{
	Memory::MemHandle retMem = MemHandle::allocate(nSize, nAlign);
	Memory::MemCopy(Memory::MemHandle::LockGuard(retMem).get_mem(), pData, retMem.get_size());
	return retMem;
}

Memory::MemHandle MemHandle::read_to_end(Core::IStreamIn& istr, bool bSeekBegin)
{
	//1. Seek to beginning
	if (bSeekBegin)
	{
		istr.seek_begin();
	}

	//2. Check size
	FXD::U32 nSize = istr.remaining();
	Memory::MemHandle memHandle;
	if (nSize > 0)
	{
		//3. Allocate memory and read
		memHandle = Memory::MemHandle::allocate(nSize);
		Memory::MemHandle::LockGuard lock(memHandle);
		FXD::U32 nSizeRead = istr.read_size(lock.get_mem(), nSize);
		if (nSizeRead != nSize)
		{
			PRINT_WARN << "Memory: Did not read all data! " << (FXD::S32)nSizeRead << " of " << (FXD::S32)nSize << "bytes were read";
		}
	}
	return memHandle;
}