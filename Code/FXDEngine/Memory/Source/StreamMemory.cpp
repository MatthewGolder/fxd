// Creator - MatthewGolder
#include "FXDEngine/Memory/StreamMemory.h"
#include "FXDEngine/Memory/Memory.h"
#include "FXDEngine/Memory/Allocator/Aligned.h"

using namespace FXD;
using namespace Memory;

// ------
// StreamMemory
// -
// ------
StreamMemory::StreamMemory(Core::CompressParams compressParam)
	: Core::IStreamIn(this, compressParam)
	, Core::IStreamOut(this, compressParam)
	, m_pBuffer(nullptr)
	, m_nBufferSize(0)
	, m_nDataSize(0)
	, m_nIOOffset(0)
{
}

StreamMemory::~StreamMemory(void)
{
	if (m_pBuffer != nullptr)
	{
		Memory::Alloc::Default.deallocate(m_pBuffer/*, "deallocate"*/);
		m_pBuffer = nullptr;
	}
}

FXD::U64 StreamMemory::read_size(void* pData, const FXD::U64 nSize)
{
	if (m_nIOOffset >= m_nBufferSize)
	{
		return 0;
	}

	FXD::U64 nToRead = (m_nBufferSize - m_nIOOffset);
	if (nToRead > nSize)
	{
		nToRead = nSize;
	}
	if (nToRead == 0)
	{
		return 0;
	}

	const FXD::U8* pSrc = (((const FXD::U8*)m_pBuffer) + m_nIOOffset);
	Memory::MemCopy(pData, pSrc, nToRead);
	m_nIOOffset += nToRead;
	return nToRead;
}

FXD::U64 StreamMemory::write_size(const void* pData, const FXD::U64 nSize)
{
	const FXD::U64 nDataEnd = (m_nIOOffset + nSize);
	if (nDataEnd > m_nBufferSize)
	{
		FXD::U32 nNewBufSize = m_nBufferSize;
		if (nNewBufSize < 8)
		{
			nNewBufSize = 8;
		}
		while (nNewBufSize < nDataEnd)
		{
			nNewBufSize <<= 1;
		}
		if ((m_pBuffer != nullptr) && (Memory::Alloc::Default.grow_memory(m_pBuffer, nNewBufSize) != false))
		{
			m_nBufferSize = nNewBufSize;
		}
		else
		{
			void* pNewBuf = Memory::Alloc::Default.allocate(nNewBufSize);
			if (m_nDataSize != 0)
			{
				Memory::MemCopy(pNewBuf, m_pBuffer, m_nDataSize);
			}

			m_nBufferSize = nNewBufSize;
			if (m_pBuffer != nullptr)
			{
				Memory::Alloc::Default.deallocate(m_pBuffer/*, "deallocate"*/);
			}
			m_pBuffer = pNewBuf;
		}
	}

	FXD::U8* pDst = (((FXD::U8*)m_pBuffer) + m_nIOOffset);
	Memory::MemCopy(pDst, pData, nSize);
	m_nIOOffset += nSize;
	if (m_nIOOffset > m_nDataSize)
	{
		m_nDataSize = m_nIOOffset;
	}
	return nSize;
}

FXD::U64 StreamMemory::length(void)
{
	return m_nDataSize;
}

FXD::S32 StreamMemory::tell(void)
{
	return (FXD::S32)m_nIOOffset;
}

FXD::U64 StreamMemory::seek_to(FXD::U64 nOffset, Core::E_SeekOffset eFrom)
{
	switch (eFrom)
	{
		case Core::E_SeekOffset::Begin:
		{
			m_nIOOffset = nOffset;
			break;
		}
		case Core::E_SeekOffset::Current:
		{
			m_nIOOffset += nOffset;
			break;
		}
		case Core::E_SeekOffset::End:
		{
			m_nIOOffset = (m_nDataSize + nOffset);
			break;
		}
	}
	return m_nIOOffset;
}