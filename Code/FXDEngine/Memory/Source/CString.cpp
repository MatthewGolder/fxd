// Creator - MatthewGolder
#include "FXDEngine/Memory/CString.h"
#include <cstring>

using namespace FXD;
using namespace Memory;

void* FXD::Memory::Internal::_MemMove(void* const pDst, const void* const pSrc, const size_t nSize)
{
	return ::memmove(pDst, pSrc, nSize);
}

void* FXD::Memory::Internal::_MemCopy(void* const pDst, const void* const pSrc, const size_t nSize)
{
	return ::memcpy(pDst, pSrc, nSize);
}

void* FXD::Memory::Internal::_MemSet(void* const pDst, const FXD::S32 nVal, const size_t nSize)
{
	return ::memset(pDst, nVal, nSize);
}

void* FXD::Memory::Internal::_MemZero(void* const pDst, const size_t nSize)
{
	return ::memset(pDst, 0x00, nSize);
}

FXD::S32 FXD::Memory::Internal::_MemCompare(const void* const pDst, const void* const pSrc, const size_t nSize)
{
	return (FXD::S32)::memcmp(pDst, pSrc, nSize);
}

const void* FXD::Memory::Internal::_Memcheck8(const void* const pDst, FXD::U8 nVal, size_t nByteCount)
{
	for (const FXD::U8* p8 = (const FXD::U8*)pDst; nByteCount > 0; ++p8, --nByteCount)
	{
		if (*p8 != nVal)
		{
			return p8;
		}
	}
	return nullptr;
}

const void* FXD::Memory::Internal::_Memcheck16(const void* const pDst, FXD::U16 nVal, size_t nByteCount)
{
	union U16
	{
		FXD::U16 c16;
		FXD::U8 c8[2];
	};

	const U16 nUnionVal = { nVal };
	size_t nSize = (size_t)((FXD::PtrSizedInt)pDst % 2);

	for (const FXD::U8* p8 = (const FXD::U8*)pDst, *p8End = (const FXD::U8*)pDst + nByteCount; p8 != p8End; ++p8, nSize ^= 1)
	{
		if (*p8 != nUnionVal.c8[nSize])
		{
			return p8;
		}
	}
	return nullptr;
}

const void* FXD::Memory::Internal::_Memcheck32(const void* const pDst, FXD::U32 nVal, size_t nByteCount)
{
	union U32
	{
		FXD::U32 c32;
		FXD::U8 c8[4];
	};

	const U32 nUnionVal = { nVal };
	size_t nSize = (size_t)((FXD::PtrSizedInt)pDst % 4);

	for (const FXD::U8* p8 = (const FXD::U8*)pDst, *p8End = (const FXD::U8*)pDst + nByteCount; p8 != p8End; ++p8, nSize = (nSize + 1) % 4)
	{
		if (*p8 != nUnionVal.c8[nSize])
		{
			return p8;
		}
	}

	return nullptr;
}

const void* FXD::Memory::Internal::_Memcheck64(const void* const pDst, FXD::U64 nVal, size_t nByteCount)
{
	union U64
	{
		FXD::U64 c64;
		FXD::U8 c8[8];
	};

	const U64 nUnionVal = { nVal };
	size_t nSize = (size_t)((FXD::PtrSizedInt)pDst % 8);

	for (const FXD::U8* p8 = (const FXD::U8*)pDst, *p8End = (const FXD::U8*)pDst + nByteCount; p8 != p8End; ++p8, nSize = (nSize + 1) % 8)
	{
		if (*p8 != nUnionVal.c8[nSize])
		{
			return p8;
		}
	}

	return nullptr;
}