// Creator - MatthewGolder
#include "FXDEngine/Memory/StreamMemHandle.h"
#include "FXDEngine/Memory/Memory.h"
#include "FXDEngine/Core/Debugger.h"

using namespace FXD;
using namespace Memory;

// ------
// IStreamMemHandle
// -
// ------
IStreamMemHandle::IStreamMemHandle(const Memory::MemHandle& memHandle)
	: Core::IStreamIn(this, Core::CompressParams::Default)
	, Core::IStreamOut(this, Core::CompressParams::Default)
	, m_nPos(0)
	, m_nSize(memHandle.get_size())
	, m_memHandle(memHandle)
	, m_memLock(m_memHandle)
{
}

IStreamMemHandle::~IStreamMemHandle(void)
{
}

FXD::U64 IStreamMemHandle::read_size(void* pData, const FXD::U64 nSize)
{
	const FXD::U64 nToRead = ((m_nPos + nSize) >= m_nSize) ? (m_nSize - m_nPos) : nSize;
	const FXD::U8* pSrc = (((const FXD::U8*)m_memLock.get_mem()) + m_nPos);

	Memory::MemCopy(pData, pSrc, nToRead);

	m_nPos += nToRead;
	return nToRead;
}

FXD::U64 IStreamMemHandle::write_size(const void* pData, const FXD::U64 nSize)
{
	const FXD::U64 nToCopy = ((m_nPos + nSize) >= m_nSize) ? (m_nSize - m_nPos) : nSize;
	PRINT_COND_ASSERT((nToCopy == nSize), "Memory: Writing past end of memhandle");
	FXD::U8* pDst = (((FXD::U8*)m_memLock.get_mem()) + m_nPos);

	Memory::MemCopy(pDst, pData, nToCopy);

	m_nPos += nToCopy;
	return nToCopy;
}

FXD::U64 IStreamMemHandle::length(void)
{
	return m_nSize;
}

FXD::S32 IStreamMemHandle::tell(void)
{
	return (FXD::S32)m_nPos;
}

FXD::U64 IStreamMemHandle::seek_to(FXD::U64 nOffset, Core::E_SeekOffset eFrom)
{
	switch (eFrom)
	{
		case Core::E_SeekOffset::Begin:
		{
			m_nPos = nOffset;
			break;
		}
		case Core::E_SeekOffset::Current:
		{
			m_nPos += nOffset;
			break;
		}
		case Core::E_SeekOffset::End:
		{
			m_nPos = (m_nSize - nOffset);
			break;
		}
	}
	return m_nPos;
}