// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_STREAMMEMHANDLE_H
#define FXDENGINE_MEMORY_STREAMMEMHANDLE_H

#include "FXDEngine/Core/StreamIn.h"
#include "FXDEngine/Core/StreamOut.h"
#include "FXDEngine/Core/SerializeInBinary.h"
#include "FXDEngine/Core/SerializeOutBinary.h"
#include "FXDEngine/Memory/MemHandle.h"

namespace FXD
{
	namespace Memory
	{
		// ------
		// IStreamMemHandle
		// -
		// ------
		class IStreamMemHandle FINAL : public Core::IStreamIn, public Core::IStreamOut, public Core::ISerializeInBinary, public Core::ISerializeOutBinary
		{
		public:
			IStreamMemHandle(const Memory::MemHandle& memHandle);
			~IStreamMemHandle(void);

			FXD::U64 read_size(void* pData, const FXD::U64 nSize) FINAL;
			FXD::U64 write_size(const void* pData, const FXD::U64 nSize) FINAL;

			FXD::U64 length(void) FINAL;
			FXD::S32 tell(void) FINAL;
			FXD::U64 seek_to(FXD::U64 nOffset, Core::E_SeekOffset eFrom) FINAL;

		private:
			FXD::U64 m_nPos;
			FXD::U64 m_nSize;
			Memory::MemHandle m_memHandle;
			Memory::MemHandle::LockGuard m_memLock;
		};

	} //namespace Memory
} //namespace FXD
#endif //FXDENGINE_MEMORY_STREAMMEMHANDLE_H