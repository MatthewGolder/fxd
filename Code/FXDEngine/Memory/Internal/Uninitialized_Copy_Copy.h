// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_COPY_COPY_H
#define FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_COPY_COPY_H

#include "FXDEngine/Memory/Internal/Uninitialized_Copy.h"

namespace FXD
{
	namespace STD
	{
		template < typename InputIterator1, typename InputIterator2, typename ForwardItr >
		inline ForwardItr uninitialized_copy_copy(InputIterator1 first1, InputIterator1 last1, InputIterator2 first2, InputIterator2 last2, ForwardItr result)
		{
			const ForwardItr mid(FXD::STD::uninitialized_copy(first1, last1, result));
			return FXD::STD::uninitialized_copy(first2, last2, mid);
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_COPY_COPY_H