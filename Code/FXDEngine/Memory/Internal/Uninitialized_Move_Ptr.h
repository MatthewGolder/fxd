// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_MOVE_PTR_H
#define FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_MOVE_PTR_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/Copy.h"
#include "FXDEngine/Core/Internal/IsTriviallyCopyAssignable.h"
#include "FXDEngine/Memory/Internal/New.h"

namespace FXD
{
	namespace STD
	{
		namespace Internal
		{
			template < typename InputItr, typename ForwardItr >
			inline ForwardItr _uninitialized_move_ptr(InputItr first, InputItr last, ForwardItr dst, FXD::STD::true_type)
			{
				return FXD::STD::copy(first, last, dst);
			}

			template < typename InputItr, typename ForwardItr >
			inline ForwardItr _uninitialized_move_ptr(InputItr first, InputItr last, ForwardItr dst, FXD::STD::false_type)
			{
				using value_type = typename FXD::STD::iterator_traits< ForwardItr >::value_type;

				ForwardItr curDst(dst);
				for (; first != last; ++first, ++curDst)
				{
					FXD_PLACEMENT_NEW(FXD::STD::addressof(*curDst), value_type)(std::move(*first));
				}
				return curDst;
			}
		} //namespace Internal

		template < typename First, typename Last, typename Result >
		inline Result uninitialized_move_ptr(First first, Last last, Result dst)
		{
			using value_type = typename FXD::STD::iterator_traits< Result >::value_type;

			dst = FXD::STD::Internal::_uninitialized_move_ptr(first, last, dst, FXD::STD::is_trivially_copy_assignable< value_type >());
			return dst;
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_MOVE_PTR_H