// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_MOVE_IF_NOEXCEPT_H
#define FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_MOVE_IF_NOEXCEPT_H

#include "FXDEngine/Memory/Internal/Uninitialized_Copy.h"

namespace FXD
{
	namespace STD
	{
		template < typename InputItr, typename ForwardItr >
		inline ForwardItr uninitialized_move_if_noexcept(InputItr first, InputItr last, ForwardItr dst)
		{
			return FXD::STD::uninitialized_copy(FXD::STD::make_move_if_noexcept_iterator(first), FXD::STD::make_move_if_noexcept_iterator(last), dst);
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_MOVE_IF_NOEXCEPT_H