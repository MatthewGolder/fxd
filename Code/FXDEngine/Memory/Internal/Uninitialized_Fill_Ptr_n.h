// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_FILL_PTR_N_H
#define FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_FILL_PTR_N_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/IsTriviallyCopyAssignable.h"
#include "FXDEngine/Memory/Internal/New.h"

namespace FXD
{
	namespace STD
	{
		namespace Internal
		{
			template < typename ForwardItr, typename Count, typename T >
			inline void _uninitialized_fill_ptr_n(ForwardItr first, Count nCount, const T& val, FXD::STD::true_type)
			{
				ForwardItr curDst(first);
				for (; nCount > 0; --nCount, ++curDst)
				{
					(*curDst) = val;
				}
			}
			template < typename ForwardItr, typename Count, typename T >
			void _uninitialized_fill_ptr_n(ForwardItr first, Count nCount, const T& val, FXD::STD::false_type)
			{
				using value_type = typename FXD::STD::iterator_traits< ForwardItr >::value_type;

				ForwardItr curDst(first);
				for (; nCount > 0; --nCount, ++curDst)
				{
					FXD_PLACEMENT_NEW(FXD::STD::addressof(*curDst), value_type)(val);
				}
			}
		} //namespace Internal


		template < typename T, typename Count >
		inline void uninitialized_fill_ptr_n(T* pFirst, Count nCount, const T& val)
		{
			FXD::STD::Internal::_uninitialized_fill_ptr_n(pFirst, nCount, val, FXD::STD::is_trivially_copy_assignable< T >());
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_FILL_PTR_N_H