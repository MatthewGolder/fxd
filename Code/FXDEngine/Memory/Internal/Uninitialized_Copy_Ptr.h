// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_COPY_PTR_H
#define FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_COPY_PTR_H

#include "FXDEngine/Memory/Internal/Uninitialized_Copy.h"
#include "FXDEngine/Core/Internal/IsTriviallyCopyAssignable.h"

namespace FXD
{
	namespace STD
	{
		template < typename First, typename Last, typename Result >
		inline Result uninitialized_copy_ptr(First first, Last last, Result dst)
		{
			using value_type = typename FXD::STD::iterator_traits< Result >::value_type;

			dst = FXD::STD::Internal::_uninitialized_copy(first, last, dst, FXD::STD::is_trivially_copy_assignable< value_type >());
			return dst;
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_COPY_PTR_H