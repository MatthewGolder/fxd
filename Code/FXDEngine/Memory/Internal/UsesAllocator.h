#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_USESALLOCATOR_H
#define FXDENGINE_MEMORY_INTERNAL_USESALLOCATOR_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/IsConvertible.h"
#include "FXDEngine/Core/Internal/Void_t.h"

namespace FXD
{
	namespace STD
	{
		// uses_allocator
#		define FXD_SUPPORTS_USES_ALLOCATOR 1

		namespace Internal
		{
			template < typename T, typename Alloc, class = void >
			struct _has_allocator_type : FXD::STD::false_type
			{};

			template < typename T, typename Alloc >
			struct _has_allocator_type< T, Alloc, FXD::STD::void_t< typename T::allocator_type > > : FXD::STD::is_convertible< Alloc, typename T::allocator_type >::type
			{};
		} //namespace Internal

		template < typename T, typename Alloc >
		struct uses_allocator : FXD::STD::Internal::_has_allocator_type< T, Alloc >::type
		{};

#		if FXD_COMPILER_VARIABLE_TEMPLATES_ENABLED

		template < typename T, typename Alloc >
		CONSTEXPR bool uses_allocator_v = FXD::STD::uses_allocator< T, Alloc >::value;

#		endif

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_MEMORY_INTERNAL_USESALLOCATOR_H