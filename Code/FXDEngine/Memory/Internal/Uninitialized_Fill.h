// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_FILL_H
#define FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_FILL_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/Fill.h"
#include "FXDEngine/Core/Internal/IsTriviallyCopyAssignable.h"
#include "FXDEngine/Memory/Types.h"

namespace FXD
{
	namespace STD
	{
		namespace Internal
		{
			template < typename ForwardItr, typename T >
			inline void _uninitialized_fill(ForwardItr first, ForwardItr last, const T& val, FXD::STD::true_type)
			{
				FXD::STD::fill(first, last, val);
			}

			template < typename ForwardItr, typename T >
			void _uninitialized_fill(ForwardItr first, ForwardItr last, const T& val, FXD::STD::false_type)
			{
				using value_type = typename FXD::STD::iterator_traits< ForwardItr >::value_type;

				ForwardItr curDst(first);
				for (; curDst != last; ++curDst)
				{
					FXD_PLACEMENT_NEW(FXD::STD::addressof(*curDst), value_type)(val);
				}
			}
		}

		template < typename ForwardItr, typename T >
		inline void uninitialized_fill(ForwardItr first, ForwardItr last, const T& val)
		{
			using value_type = typename FXD::STD::iterator_traits< ForwardItr >::value_type;

			FXD::STD::Internal::_uninitialized_fill(first, last, val, FXD::STD::is_trivially_copy_assignable< value_type >());
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_FILL_H