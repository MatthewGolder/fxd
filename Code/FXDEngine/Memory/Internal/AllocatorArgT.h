#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_ALLOCATORARGT_H
#define FXDENGINE_MEMORY_INTERNAL_ALLOCATORARGT_H

#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// allocator_arg_t
		struct allocator_arg_t
		{	// tag type for added allocator argument
			EXPLICIT allocator_arg_t() = default;
		};

		CONSTEXPR FXD::STD::allocator_arg_t allocator_arg
		{};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_MEMORY_INTERNAL_ALLOCATORARGT_H