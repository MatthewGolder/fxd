// Creator - MatthewGolder
#include "FXDEngine/Memory/Internal/New.h"
#include "FXDEngine/Memory/Allocator/Aligned.h"

using namespace FXD;
using namespace Memory;

#if FXD_ENABLE_MEMORY_TRACKING
void* operator new(size_t nSize)
{
	Memory::Alloc::Aligned< Memory::Alloc::Heap, 8 > alloc;
	return alloc.allocate(nSize, __FILE__, __LINE__);
}

void* operator new(size_t nSize, FXD::STD::align_val_t alignment)
{
	Memory::Alloc::Allocator< Memory::Alloc::Heap > alloc(alignment);
	return alloc.allocate(nSize, __FILE__, __LINE__);
}

void* operator new(size_t nSize, const FXD::UTF8* pFileName, const FXD::U32 nLine, const FXD::UTF8* pTypeName)
{
	Memory::Alloc::Aligned< Memory::Alloc::Heap, 8 > alloc;
	return alloc.allocate(nSize, pFileName, nLine, pTypeName);
}

void* operator new(size_t nSize, FXD::STD::align_val_t alignment, const FXD::UTF8* pFileName, const FXD::U32 nLine, const FXD::UTF8* pTypeName)
{
	Memory::Alloc::Allocator< Memory::Alloc::Heap > alloc(alignment);
	return alloc.allocate(nSize, pFileName, nLine, pTypeName);
}

void* operator new[](size_t nSize)
{
	Memory::Alloc::Aligned< Memory::Alloc::Heap, 8 > alloc;
	return alloc.allocate(nSize, __FILE__, __LINE__);
}

void* operator new[](size_t nSize, FXD::STD::align_val_t alignment)
{
	Memory::Alloc::Allocator< Memory::Alloc::Heap > alloc(alignment);
	return alloc.allocate(nSize, __FILE__, __LINE__);
}

void* operator new[](size_t nSize, const FXD::UTF8* pFileName, const FXD::U32 nLine, const FXD::UTF8* pTypeName)
{
	Memory::Alloc::Aligned< Memory::Alloc::Heap, 8 > alloc;
	return alloc.allocate(nSize, pFileName, nLine, pTypeName);
}

void* operator new[](size_t nSize, FXD::STD::align_val_t alignment, const FXD::UTF8* pFileName, const FXD::U32 nLine, const FXD::UTF8* pTypeName)
{
	Memory::Alloc::Allocator< Memory::Alloc::Heap > alloc(alignment);
	return alloc.allocate(nSize, pFileName, nLine, pTypeName);
}

#else
void* operator new(size_t nSize)
{
	Memory::Alloc::Aligned< Memory::Alloc::Heap, 8 > alloc;
	return alloc.allocate(nSize);
}

void* operator new(size_t nSize, FXD::STD::align_val_t alignment)
{
	Memory::Alloc::Allocator< Memory::Alloc::Heap > alloc(alignment);
	return alloc.allocate(nSize);
}

void* operator new(size_t nSize, const FXD::UTF8* /*pFileName*/, const FXD::U32 /*nLine*/, const FXD::UTF8* /*pTypeName*/)
{
	Memory::Alloc::Aligned< Memory::Alloc::Heap, 8 > alloc;
	return alloc.allocate(nSize);
}

void* operator new(size_t nSize, FXD::STD::align_val_t alignment, const FXD::UTF8* /*pFileName*/, const FXD::U32 /*nLine*/, const FXD::UTF8* /*pTypeName*/)
{
	Memory::Alloc::Allocator< Memory::Alloc::Heap > alloc(alignment);
	return alloc.allocate(nSize);
}

void* operator new[](size_t nSize)
{
	Memory::Alloc::Aligned< Memory::Alloc::Heap, 8 > alloc;
	return alloc.allocate(nSize);
}

void* operator new[](size_t nSize, FXD::STD::align_val_t alignment)
{
	Memory::Alloc::Allocator< Memory::Alloc::Heap > alloc(alignment);
	return alloc.allocate(nSize);
}

void* operator new[](size_t nSize, const FXD::UTF8* /*pFileName*/, const FXD::U32 /*nLine*/, const FXD::UTF8* /*pTypeName*/)
{
	Memory::Alloc::Aligned< Memory::Alloc::Heap, 8 > alloc;
	return alloc.allocate(nSize);
}

void* operator new[](size_t nSize, FXD::STD::align_val_t alignment, const FXD::UTF8* /*pFileName*/, const FXD::U32 /*nLine*/, const FXD::UTF8* /*pTypeName*/)
{
	Memory::Alloc::Allocator< Memory::Alloc::Heap > alloc(alignment);
	return alloc.allocate(nSize);
}

#endif //FXD_ENABLE_MEMORY_TRACKING

void operator delete(void* pMem)
{
	if (pMem != nullptr)
	{
		Memory::Alloc::Aligned< Memory::Alloc::Heap, 8 > alloc;
		alloc.deallocate(pMem/*, "deallocate"*/);
	}
}

void operator delete(void* pMem, FXD::STD::align_val_t alignment)
{
	if (pMem != nullptr)
	{
		Memory::Alloc::Allocator< Memory::Alloc::Heap > alloc(alignment);
		alloc.deallocate(pMem/*, "deallocate"*/);
	}
}

void operator delete(void* pMem, const FXD::UTF8* /*pFileName*/, const FXD::U32 /*nLine*/, const FXD::UTF8* /*pTypeName*/)
{
	if (pMem != nullptr)
	{
		Memory::Alloc::Aligned< Memory::Alloc::Heap, 8 > alloc;
		alloc.deallocate(pMem/*, "deallocate"*/);
	}
}

void operator delete(void* pMem, FXD::STD::align_val_t alignment, const FXD::UTF8* /*pFileName*/, const FXD::U32 /*nLine*/, const FXD::UTF8* /*pTypeName*/)
{
	if (pMem != nullptr)
	{
		Memory::Alloc::Allocator< Memory::Alloc::Heap > alloc(alignment);
		alloc.deallocate(pMem/*, "deallocate"*/);
	}
}

void operator delete[](void* pMem)
{
	if (pMem != nullptr)
	{
		Memory::Alloc::Aligned< Memory::Alloc::Heap, 8 > alloc;
		alloc.deallocate(pMem/*, "deallocate"*/);
	}
}

void operator delete[](void* pMem, FXD::STD::align_val_t alignment)
{
	if (pMem != nullptr)
	{
		Memory::Alloc::Allocator< Memory::Alloc::Heap > alloc(alignment);
		alloc.deallocate(pMem/*, "deallocate"*/);
	}
}

void operator delete[](void* pMem, const FXD::UTF8* /*pFileName*/, const FXD::U32 /*nLine*/, const FXD::UTF8* /*pTypeName*/)
{
	if (pMem != nullptr)
	{
		Memory::Alloc::Aligned< Memory::Alloc::Heap, 8 > alloc;
		alloc.deallocate(pMem/*, "deallocate"*/);
	}
}

void operator delete[](void* pMem, FXD::STD::align_val_t alignment, const FXD::UTF8* /*pFileName*/, const FXD::U32 /*nLine*/, const FXD::UTF8* /*pTypeName*/)
{
	if (pMem != nullptr)
	{
		Memory::Alloc::Allocator< Memory::Alloc::Heap > alloc(alignment);
		alloc.deallocate(pMem/*, "deallocate"*/);
	}
}