#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_DESTROY_H
#define FXDENGINE_MEMORY_INTERNAL_DESTROY_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"

namespace FXD
{
	namespace STD
	{
		// destruct
		template < typename T >
		inline void destruct(T* p)
		{
			p->~T();
		}

		// destruct
		template < typename ForwardItr >
		inline void destruct(ForwardItr first, ForwardItr last)
		{
			using value_type = typename FXD::STD::iterator_traits< ForwardItr >::value_type;

			for (; first != last; ++first)
			{
				(*first).~value_type();
			}
		}

		// destroy_at
		template < typename T >
		inline void destroy_at(T* p)
		{
			p->~T();
		}

		// destroy
		template < typename ForwardItr >
		inline void destroy(ForwardItr first, ForwardItr last)
		{
			for (; first != last; ++first)
			{
				FXD::STD::destroy_at(FXD::STD::addressof(*first));
			}
		}

		// destroy_n
		template < typename ForwardItr, typename Size >
		ForwardItr destroy_n(ForwardItr first, Size nCount)
		{
			for (; nCount > 0; ++first, --nCount)
			{
				FXD::STD::destroy_at(FXD::STD::addressof(*first));
			}
			return first;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_MEMORY_INTERNAL_DESTROY_H