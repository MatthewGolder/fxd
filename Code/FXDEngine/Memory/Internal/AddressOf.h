#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_ADDRESSOF_H
#define FXDENGINE_MEMORY_INTERNAL_ADDRESSOF_H

#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// addressof
		template < typename T>
		CONSTEXPR T* addressof(T& val) NOEXCEPT
		{
			return (__builtin_addressof(val));
		}

		template < typename T >
		const T* addressof(const T&&) = delete;

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_MEMORY_INTERNAL_ADDRESSOF_H