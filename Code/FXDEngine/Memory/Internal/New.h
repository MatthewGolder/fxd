#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_NEW_H
#define FXDENGINE_MEMORY_INTERNAL_NEW_H

#include "FXDEngine/Core/Types.h"
#include "FXDEngine/Memory/Types.h"
#include "FXDEngine/Memory/Internal/AlignValT.h"

#if FXD_ENABLE_MEMORY_TRACKING
#	include <typeinfo>
#endif

	void* operator new(size_t nSize);
	void* operator new(size_t nSize, FXD::STD::align_val_t alignment);
	void* operator new(size_t nSize, const FXD::UTF8* pFileName, const FXD::U32 nLine, const FXD::UTF8* pTypeName);
	void* operator new(size_t nSize, FXD::STD::align_val_t alignment, const FXD::UTF8* pFileName, const FXD::U32 nLine, const FXD::UTF8* pTypeName);
	void* operator new[](size_t nSize);
	void* operator new[](size_t nSize, FXD::STD::align_val_t alignment);
	void* operator new[](size_t nSize, const FXD::UTF8* pFileName, const FXD::U32 nLine, const FXD::UTF8* pTypeName);
	void* operator new[](size_t nSize, FXD::STD::align_val_t alignment, const FXD::UTF8* pFileName, const FXD::U32 nLine, const FXD::UTF8* pTypeName);

	void operator delete(void* pMem);
	void operator delete(void* pMem, FXD::STD::align_val_t alignment);
	void operator delete(void* pMem, const FXD::UTF8* pFileName, const FXD::U32 nLine, const FXD::UTF8* pTypeName);
	void operator delete(void* pMem, FXD::STD::align_val_t alignment, const FXD::UTF8* pFileName, const FXD::U32 nLine, const FXD::UTF8* pTypeName);
	void operator delete[](void* pMem);
	void operator delete[](void* pMem, FXD::STD::align_val_t alignment);
	void operator delete[](void* pMem, const FXD::UTF8* pFileName, const FXD::U32 nLine, const FXD::UTF8* pTypeName);
	void operator delete[](void* pMem, FXD::STD::align_val_t alignment, const FXD::UTF8* pFileName, const FXD::U32 nLine, const FXD::UTF8* pTypeName);

#if FXD_ENABLE_MEMORY_TRACKING
#	define FXD_NEW(Type) new(__FILE__, __LINE__, typeid(Type).name()) Type
#	define FXD_NEW_ARRAY(Type, Count) new(__FILE__, __LINE__, typeid(Type).name()) Type[Count]
#	define FXD_NEW_ALIGN(Type, Align) new(Align, __FILE__, __LINE__, typeid(Type).name()) Type
#	define FXD_NEW_ALIGN_ARRAY(Type, Count, Align) new(Align, __FILE__, __LINE__, typeid(Type).name()) Type[Count]
#else
#	define FXD_NEW(Type) new Type
#	define FXD_NEW_ARRAY(Type, Count) new Type[Count]
#endif

#define FXD_PLACEMENT_NEW(data, Type) new(data) Type
//#define FXD_PLACEMENT_DESTRUCT(data, Type) data.~Type()

#define FXD_DELETE(pMem) operator delete(pMem)
#define FXD_DELETE_ARRAY(pMem) operator delete[] (pMem)
#define FXD_DELETE_ALIGN(pMem, Align) operator delete(pMem, Align)
#define FXD_DELETE_ALIGN_ARRAY(pMem, Align) operator delete[] (pMem, Align)

#define FXD_RELEASE(pShared, func) if (pShared != nullptr){ pShared -> func; pShared = nullptr;}
#define FXD_SAFE_DELETE(pMem) if (pMem != nullptr){ delete pMem; pMem = nullptr;}
#define FXD_SAFE_DELETE_ARRAY(pMem) if (pMem != nullptr){ delete[] pMem; pMem = nullptr;}

#endif //FXDENGINE_MEMORY_INTERNAL_NEW_H