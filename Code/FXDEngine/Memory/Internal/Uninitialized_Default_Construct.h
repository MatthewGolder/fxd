// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_DEFAULT_CONSTRUCT_H
#define FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_DEFAULT_CONSTRUCT_H

#include "FXDEngine/Memory/Types.h"
#include "FXDEngine/Container/Internal/IteratorTraits.h"

namespace FXD
{
	namespace STD
	{
		template < typename ForwardItr >
		inline void uninitialized_default_construct(ForwardItr first, ForwardItr last)
		{
			using value_type = typename FXD::STD::iterator_traits< ForwardItr >::value_type;
			ForwardItr curDst(first);

			for (; curDst != last; ++curDst)
			{
				FXD_PLACEMENT_NEW(curDst, value_type);
			}
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_DEFAULT_CONSTRUCT_H