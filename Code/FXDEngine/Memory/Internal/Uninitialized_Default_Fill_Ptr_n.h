// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_DEFAULT_FILL_PTR_N_H
#define FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_DEFAULT_FILL_PTR_N_H

#include "FXDEngine/Memory/Types.h"
#include "FXDEngine/Memory/Memory.h"
#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/TypeTraits.h"

namespace FXD
{
	namespace STD
	{
		namespace Internal
		{
			template < typename ForwardItr, typename Count >
			inline void _uninitialized_default_fill_ptr_n(ForwardItr first, Count nCount, FXD::STD::true_type)
			{
				using value_type = typename FXD::STD::iterator_traits< ForwardItr >::value_type;

				FXD::Memory::MemZero(first, (sizeof(value_type) * nCount));
			}
			template < typename ForwardItr, typename Count >
			void _uninitialized_default_fill_ptr_n(ForwardItr first, Count nCount, FXD::STD::false_type)
			{
				using value_type = typename FXD::STD::iterator_traits< ForwardItr >::value_type;

				ForwardItr curDst(first);
				for (; nCount > 0; --nCount, ++curDst)
				{
					FXD_PLACEMENT_NEW(FXD::STD::addressof(*curDst), value_type);
				}
			}
		} //namespace Internal

		template < typename ForwardItr, typename Count >
		inline void uninitialized_default_fill_ptr_n(ForwardItr first, Count nCount)
		{
			using value_type = typename FXD::STD::iterator_traits< ForwardItr >::value_type;

			FXD::STD::Internal::_uninitialized_default_fill_ptr_n(first, nCount, FXD::STD::is_scalar< value_type >());
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_DEFAULT_FILL_PTR_N_H