#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_ALIGNVALT_H
#define FXDENGINE_MEMORY_INTERNAL_ALIGNVALT_H

namespace FXD
{
	namespace STD
	{
		// align_val_t
		enum class align_val_t : size_t
		{};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_MEMORY_INTERNAL_ALIGNVALT_H