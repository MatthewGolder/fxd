// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_MOVE_FILL_H
#define FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_MOVE_FILL_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Fill.h"
#include "FXDEngine/Memory/Types.h"

namespace FXD
{
	namespace STD
	{
		template < typename InputItr, typename ForwardItr, typename T >
		inline void uninitialized_move_fill(InputItr first1, InputItr last1, ForwardItr first2, ForwardItr last2, const T& val)
		{
			const ForwardItr mid(FXD::STD::uninitialized_move(first1, last1, first2));
			FXD::STD::uninitialized_fill(mid, last2, val);
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_MOVE_FILL_H