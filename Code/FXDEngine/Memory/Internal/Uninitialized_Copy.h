// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_COPY_H
#define FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_COPY_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/Copy.h"
#include "FXDEngine/Core/Internal/IsTrivial.h"
#include "FXDEngine/Memory/Internal/New.h"

namespace FXD
{
	namespace STD
	{
		namespace Internal
		{
			template < typename InputItr, typename ForwardItr >
			inline ForwardItr _uninitialized_copy(InputItr first, InputItr last, ForwardItr dst, FXD::STD::true_type)
			{
				return FXD::STD::copy(first, last, dst);
			}

			template < typename InputItr, typename ForwardItr >
			inline ForwardItr _uninitialized_copy(InputItr first, InputItr last, ForwardItr dst, FXD::STD::false_type)
			{
				using value_type = typename FXD::STD::iterator_traits< ForwardItr >::value_type;

				ForwardItr curDst(dst);
				for (; first != last; ++first, ++curDst)
				{
					FXD_PLACEMENT_NEW(FXD::STD::addressof(*curDst), value_type)(*first);
				}
				return curDst;
			}
		} //namespace Internal

		template < typename InputItr, typename ForwardItr >
		inline ForwardItr uninitialized_copy(InputItr first, InputItr last, ForwardItr result)
		{
			using value_type = typename FXD::STD::iterator_traits< ForwardItr >::value_type;

			return FXD::STD::Internal::_uninitialized_copy(first, last, result, FXD::STD::is_trivial< value_type >());
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_COPY_H