// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_FILL_MOVE_H
#define FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_FILL_MOVE_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Fill.h"
#include "FXDEngine/Memory/Internal/Uninitialized_Move.h"

namespace FXD
{
	namespace STD
	{
		template < typename ForwardItr, typename T, typename InputItr >
		inline ForwardItr uninitialized_fill_move(ForwardItr result, ForwardItr mid, const T& value, InputItr first, InputItr last)
		{
			FXD::STD::uninitialized_fill(result, mid, value);
			return FXD::STD::uninitialized_move(first, last, mid);
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_FILL_MOVE_H