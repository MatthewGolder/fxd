// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_DEFAULT_CONSTRUCT_N_H
#define FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_DEFAULT_CONSTRUCT_N_H

#include "FXDEngine/Memory/Types.h"
#include "FXDEngine/Container/Internal/IteratorTraits.h"

namespace FXD
{
	namespace STD
	{
		template < typename ForwardItr, typename Count >
		inline ForwardItr uninitialized_default_construct_n(ForwardItr first, Count nCount)
		{
			using value_type = typename FXD::STD::iterator_traits< ForwardItr >::value_type;

			ForwardItr curDst(first);
			for (; nCount > 0; --nCount, ++curDst)
			{
				FXD_PLACEMENT_NEW(FXD::STD::addressof(*curDst), value_type);
			}
			return curDst;
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_DEFAULT_CONSTRUCT_N_H