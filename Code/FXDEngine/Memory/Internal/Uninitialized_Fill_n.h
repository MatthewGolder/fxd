// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_FILL_N_H
#define FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_FILL_N_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/FillN.h"
#include "FXDEngine/Core/Internal/IsTriviallyCopyAssignable.h"
#include "FXDEngine/Memory/Types.h"

namespace FXD
{
	namespace STD
	{
		namespace Internal
		{
			template < typename ForwardItr, typename Count, typename T >
			inline void _uninitialized_fill_n(ForwardItr first, Count nCount, const T& value, FXD::STD::true_type)
			{
				FXD::STD::fill_n(first, nCount, value);
			}

			template < typename ForwardItr, typename Count, typename T >
			void _uninitialized_fill_n(ForwardItr first, Count nCount, const T& value, FXD::STD::false_type)
			{
				using value_type = typename FXD::STD::iterator_traits< ForwardItr >::value_type;

				ForwardItr curDst(first);
				for (; nCount > 0; --nCount, ++curDst)
				{
					FXD_PLACEMENT_NEW(FXD::STD::addressof(*curDst), value_type)(value);
				}
			}
		} //namespace Internal

		template < typename ForwardItr, typename Count, typename T >
		inline void uninitialized_fill_n(ForwardItr first, Count nCount, const T& val)
		{
			using value_type = typename FXD::STD::iterator_traits< ForwardItr >::value_type;

			FXD::STD::Internal::_uninitialized_fill_n(first, nCount, val, FXD::STD::is_trivially_copy_assignable< value_type >());
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_FILL_N_H