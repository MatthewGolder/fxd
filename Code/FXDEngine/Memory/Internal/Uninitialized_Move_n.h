// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_MOVE_N_H
#define FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_MOVE_N_H

#include "FXDEngine/Memory/Internal/Uninitialized_Copy.h"

namespace FXD
{
	namespace STD
	{
		namespace Internal
		{
			template < typename InputItr, typename Count, typename ForwardItr, typename IteratorTag >
			struct _uninitialized_move_n
			{
				static ForwardItr impl(InputItr first, Count nCount, ForwardItr dst)
				{
					using value_type = typename FXD::STD::iterator_traits< ForwardItr >::value_type;

					ForwardItr curDst(dst);
					for (; nCount > 0; --nCount, ++first, ++curDst)
					{
						FXD_PLACEMENT_NEW(FXD::STD::addressof(*curDst), value_type)(*first);
					}
					return curDst;
				}
			};

			template < typename InputItr, typename Count, typename ForwardItr >
			struct _uninitialized_move_n< InputItr, Count, ForwardItr, FXD::STD::random_access_iterator_tag >
			{
				static inline ForwardItr impl(InputItr first, Count nCount, ForwardItr dst)
				{
					return FXD::STD::uninitialized_copy(first, (first + nCount), dst);
				}
			};
		} //namespace Internal

		template < typename InputItr, typename Count, typename ForwardItr >
		inline ForwardItr uninitialized_move_n(InputItr first, Count nCount, ForwardItr dst)
		{
			using IC = typename FXD::STD::iterator_traits< InputItr >::iterator_category;

			return FXD::STD::Internal::_uninitialized_move_n< InputItr, Count, ForwardItr, IC >::impl(first, nCount, dst);
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_MOVE_N_H