// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_MOVE_PTR_IF_NOEXCEPT_H
#define FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_MOVE_PTR_IF_NOEXCEPT_H

#include "FXDEngine/Memory/Internal/Uninitialized_Move_Ptr.h"

namespace FXD
{
	namespace STD
	{
		template < typename First, typename Last, typename Result >
		inline Result uninitialized_move_ptr_if_noexcept(First first, Last last, Result dst)
		{
			return FXD::STD::uninitialized_move_ptr(first, last, dst);
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_MEMORY_INTERNAL_UNINITIALIZED_MOVE_PTR_IF_NOEXCEPT_H