// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_MEMHANDLE_H
#define FXDENGINE_MEMORY_MEMHANDLE_H

#include "FXDEngine/Core/NonCopyable.h"
#include "FXDEngine/Core/StreamIn.h"
#include "FXDEngine/Core/StreamOut.h"

#include <memory>

namespace FXD
{
	namespace Memory
	{
		// ------
		// IMemPolicy
		// -
		// ------
		class IMemPolicy
		{
		public:
			IMemPolicy(void)
			{}
			virtual ~IMemPolicy(void)
			{}

			virtual void increment_ref(void* pData) PURE;
			virtual void release_ref(void* pData) PURE;
			virtual void* lock(void* pData) PURE;
			virtual void unlock(void* pData) PURE;

			static Memory::IMemPolicy* get_void_mem_policy(void);
		};

		// ------
		// MemHandle
		// -
		// ------
		class MemHandle
		{
		public:
			// ------
			// LockGuard
			// -
			// ------
			class LockGuard FINAL : public Core::NonCopyable
			{
			public:
				LockGuard(const Memory::MemHandle& rhs);
				LockGuard(LockGuard&& rhs);
				~LockGuard(void);

				void* get_mem(void) const;
				void* operator+(size_t nSize) const;

			private:
				const Memory::MemHandle& m_memHandle;
				void* m_pMem;
			};

		public:
			MemHandle(void)
				: m_nSize(0)
				, m_pData(nullptr)
				, m_pPolicy(nullptr)
			{}

			EXPLICIT MemHandle(void* pData, FXD::U32 nSize, Memory::IMemPolicy* pPolicy = Memory::IMemPolicy::get_void_mem_policy())
				: m_nSize(nSize)
				, m_pData(pData)
				, m_pPolicy(pPolicy)
			{
				if (m_pPolicy != nullptr)
				{
					m_pPolicy->increment_ref(m_pData);
				}
			}
			template < typename Type >
			MemHandle(Type* pData, Memory::IMemPolicy* pPolicy = Memory::IMemPolicy::get_void_mem_policy())
				: m_nSize(sizeof(Type))
				, m_pData(pData)
				, m_pPolicy(pPolicy)
			{
				if (m_pPolicy != nullptr)
				{
					m_pPolicy->increment_ref(m_pData);
				}
			}

			~MemHandle(void)
			{
				if (m_pPolicy != nullptr)
				{
					m_pPolicy->release_ref(m_pData);
				}
			}

			inline MemHandle(const Memory::MemHandle& rhs)
				: m_nSize(rhs.m_nSize)
				, m_pData(rhs.m_pData)
				, m_pPolicy(rhs.m_pPolicy)
			{
				if (m_pPolicy != nullptr)
				{
					m_pPolicy->increment_ref(m_pData);
				}
			}
			inline MemHandle(Memory::MemHandle&& rhs)
				: m_nSize(rhs.m_nSize)
				, m_pData(rhs.m_pData)
				, m_pPolicy(rhs.m_pPolicy)
			{
				rhs.m_nSize = 0;
				rhs.m_pData = nullptr;
				rhs.m_pPolicy = nullptr;
			}

			inline MemHandle& operator=(const Memory::MemHandle& rhs)
			{
				if (rhs.m_pPolicy != nullptr)
				{
					rhs.m_pPolicy->increment_ref(rhs.m_pData);
				}
				if (m_pPolicy != nullptr)
				{
					m_pPolicy->release_ref(m_pData);
				}
				m_nSize = rhs.m_nSize;
				m_pData = rhs.m_pData;
				m_pPolicy = rhs.m_pPolicy;
				return (*this);
			}

			void deallocate(void);

			bool compare_memhandle(const Memory::MemHandle& rhs) const;
			bool is_vaild(void) const;

			bool operator<(const Memory::MemHandle& rhs) const;
			bool operator==(const Memory::MemHandle& rhs) const;
			bool operator!=(const Memory::MemHandle& rhs) const;

			inline FXD::U32 get_size(void) const					{ return m_nSize; }
			inline const void* get_data(void) const				{ return m_pData; }
			inline Memory::IMemPolicy* get_policy(void) const	{ return m_pPolicy; }

			MemHandle compress(void) const;
			MemHandle decompress(void) const;
			FXD::HashValue to_hash(void) const;

			void clear_memory(void);

		protected:
			FXD::U32 m_nSize;
			void* m_pData;
			Memory::IMemPolicy* m_pPolicy;

		public:
			static Memory::MemHandle allocate(size_t nSize, size_t nAlign = sizeof(FXD::PtrDiff));
			static Memory::MemHandle allocate_copy(void* pData, size_t nSize, size_t nAlign = sizeof(FXD::PtrDiff));
			static Memory::MemHandle read_to_end(Core::IStreamIn& stream, bool bSeekBegin = false);
		};


		// Core::IStreamOut
		inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, Memory::MemHandle const& rhs)
		{
			FXD::U32 nSize = rhs.get_size();
			ostr << nSize;
			Memory::MemHandle::LockGuard lock(rhs);
			ostr.write_size(lock.get_mem(), nSize);
			return ostr;
		}

		// Core::IStreamIn
		inline Core::IStreamIn& operator>>(Core::IStreamIn& istr, Memory::MemHandle& rhs)
		{
			FXD::U32 nSize;
			istr >> nSize;
			rhs = Memory::MemHandle::allocate(nSize);
			Memory::MemHandle::LockGuard lock(rhs);
			istr.read_size(lock.get_mem(), nSize);
			return istr;
		}

	} //namespace Memory
} //namespace FXD
#endif //FXDENGINE_MEMORY_MEMHANDLE_H