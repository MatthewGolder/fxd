// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_LOG_LOG_H
#define FXDENGINE_MEMORY_LOG_LOG_H

#include "FXDEngine/Memory/Types.h"
#include "FXDEngine/Memory/New.h"

namespace FXD
{
	namespace Memory
	{
		namespace Log
		{
			class AllocStats;
			class Logger;

			extern void RegisterAlloc(void* pPtr, size_t nAllocSize, const FXD::UTF8* pFile, FXD::U32 nLine, const FXD::UTF8* pTypeName);
			extern void UnregisterAlloc(void* pPtr);

			extern void PrintTotalStats(void);
			extern void PrintFrameStats(void);
			extern const Log::AllocStats& GetTotalStats(void);
			extern const Log::AllocStats& GetFrameStats(void);
			extern void ClearTotalDtats(void);
			extern void ClearFrameStats(void);
			extern void DumpMemory(const FXD::UTF8* pFileName);

#		if FXD_ENABLE_MEMORY_TRACKING
			extern Log::Logger& GetLogger(void);
#		endif

		} //namespace Log
	} //namespace Memory
} //namespace FXD
#endif //FXDENGINE_MEMORY_LOG_LOG_H