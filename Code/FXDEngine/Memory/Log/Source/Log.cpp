// Creator - MatthewGolder
#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Memory/Log/Log.h"
#include "FXDEngine/Memory/Log/AllocStats.h"
#include "FXDEngine/Memory/Log/MemLogger.h"
#include "FXDEngine/Memory/Allocator/Heap.h"
#include "FXDEngine/Thread/ThreadLocalStorage.h"
#include "FXDEngine/Thread/CriticalSection.h"
#include <stdio.h>

using namespace FXD;
using namespace Memory;
using namespace Log;

#if FXD_ENABLE_MEMORY_TRACKING
// ------
// Logger
// -
// ------
class FXD::Memory::Log::Logger
{
public:
	Logger(void)
	{}
	~Logger(void)
	{}

public:
	FXD::Thread::CSLock g_memCS;
	Memory::Log::MemLogger* g_pMemLog = nullptr;
};

FXD::Memory::Log::Logger& FXD::Memory::Log::GetLogger(void)
{
	static FXD::Memory::Log::Logger logger;
	return logger;
}
#endif
Memory::Log::AllocStats g_memStatsTotal;
Memory::Log::AllocStats g_memStatsFrame;

// ------
// Memory
// -
// ------
#if FXD_ENABLE_MEMORY_TRACKING
void FXD::Memory::Log::RegisterAlloc(void* pPtr, size_t nAllocSize, const FXD::UTF8* pFile, FXD::U32 nLine, const FXD::UTF8* pTypeName)
{
	FXD::Thread::CSLock::LockGuard lock(Log::GetLogger().g_memCS);

	Memory::Log::MemLogger* pMemLog = (Memory::Log::MemLogger*)pPtr;
	pMemLog->m_nAllocSize = nAllocSize;
	pMemLog->m_pFileName = pFile;
	pMemLog->m_nLineNumber = nLine;
	pMemLog->m_pTypeName = pTypeName;
	pMemLog->m_pNextMem = Log::GetLogger().g_pMemLog;
	pMemLog->m_pPrevMem = nullptr;
	if (Log::GetLogger().g_pMemLog != nullptr)
	{
		Log::GetLogger().g_pMemLog->m_pPrevMem = pMemLog;
	}
	Log::GetLogger().g_pMemLog = pMemLog;

	g_memStatsTotal.add_allocation(pMemLog->m_nAllocSize);
	g_memStatsFrame.add_allocation(pMemLog->m_nAllocSize);
}

void FXD::Memory::Log::UnregisterAlloc(void* pPtr)
{
	FXD::Thread::CSLock::LockGuard lock(Log::GetLogger().g_memCS);

	Memory::Log::MemLogger* pMemLog = (Memory::Log::MemLogger*)pPtr;
	g_memStatsTotal.remove_allocation(pMemLog->m_nAllocSize);
	g_memStatsFrame.remove_allocation(pMemLog->m_nAllocSize);

	if (pMemLog->m_pNextMem != nullptr)
	{
		pMemLog->m_pNextMem->m_pPrevMem = pMemLog->m_pPrevMem;
	}
	if (pMemLog->m_pPrevMem != nullptr)
	{
		pMemLog->m_pPrevMem->m_pNextMem = pMemLog->m_pNextMem;
	}
	else
	{
		Log::GetLogger().g_pMemLog = pMemLog->m_pNextMem;
	}

	pMemLog->m_pFileName = nullptr;
	pMemLog->m_pTypeName = nullptr;
	pMemLog->m_nLineNumber = 0;
	pMemLog->m_nAllocSize = 0;
	pMemLog->m_pPrevMem = nullptr;
	pMemLog->m_pNextMem = nullptr;
}
#else
void FXD::Memory::Log::RegisterAlloc(void* /*pPtr*/, size_t /*nAllocSize*/, const FXD::UTF8* /*pFile*/, FXD::U32 /*nLine*/, const FXD::UTF8* /*pTypeName*/)
{
}

void FXD::Memory::Log::UnregisterAlloc(void* /*pPtr*/)
{
}
#endif

void FXD::Memory::Log::PrintTotalStats(void)
{
	const Memory::Log::AllocStats& totalMem = Memory::Log::GetTotalStats();
	const size_t nAcc = totalMem.total_bytes_allocated();
	const size_t nAll = totalMem.total_allocations();
	const size_t nDea = totalMem.total_deallocations();

	PRINT_LINE;
	PRINT_INFO << "Memory: TOTAL STATS";
	PRINT_INFO << "Memory: [Total Bytes: " << nAcc << "]";
	PRINT_INFO << "Memory: [Total Allocs: " << nAll - nDea << "]";
	//PRINT_INFO << "Memory: [Total Deallocs: " << nDea << "]";
}

void FXD::Memory::Log::PrintFrameStats(void)
{
	const Memory::Log::AllocStats& totalMem = Memory::Log::GetFrameStats();
	const size_t nAcc = totalMem.total_bytes_allocated();
	const size_t nAll = totalMem.total_allocations();
	const size_t nDea = totalMem.total_deallocations();

	PRINT_LINE;
	PRINT_INFO << "Memory: FRAME STATS";
	PRINT_INFO << "Memory: [Frame Bytes: " << nAcc << "]";
	PRINT_INFO << "Memory: [Frame Allocs: " << nAll << "]";
	PRINT_INFO << "Memory: [Frame Deallocs: " << nDea << "]";
}

const Log::AllocStats& FXD::Memory::Log::GetTotalStats(void)
{
	return g_memStatsTotal;
}

const Log::AllocStats& FXD::Memory::Log::GetFrameStats(void)
{
	return g_memStatsFrame;
}

#if FXD_ENABLE_MEMORY_TRACKING
void FXD::Memory::Log::ClearTotalDtats(void)
{
	g_memStatsTotal.m_nTotalHeapBytesAllocated = 0;
	g_memStatsTotal.m_nTotalHeapAllocations = 0;
	g_memStatsTotal.m_nTotalHeapDeallocations = 0;
}

void FXD::Memory::Log::ClearFrameStats(void)
{
	g_memStatsFrame.m_nTotalHeapBytesAllocated = 0;
	g_memStatsFrame.m_nTotalHeapAllocations = 0;
	g_memStatsFrame.m_nTotalHeapDeallocations = 0;
}

void FXD::Memory::Log::DumpMemory(const FXD::UTF8* pFileName)
{
	FILE* pFile = fopen(pFileName, "wb");
	if (pFile == NULL)
	{
		return;
	}

	fprintf(pFile, "[LOGGED STATS] \r\n");
	const Memory::Log::AllocStats& totalMem = Memory::Log::GetTotalStats();
	fprintf(pFile, "[Bytes %d] \r\n", totalMem.total_bytes_allocated());
	fprintf(pFile, "[Allocs %d] \r\n", totalMem.total_allocations());
	fprintf(pFile, "[Deallos %d] \r\n", totalMem.total_deallocations());

	fprintf(pFile, "\r\n[MEMORY] \r\n");
	Memory::Log::MemLogger* pLog;
	FXD::S32 nTotalSize = 0;
	FXD::S32 nNumBlocks = 0;
	for (nNumBlocks = 0, pLog = Log::GetLogger().g_pMemLog; pLog; pLog = pLog->m_pNextMem, nNumBlocks++)
	{
		//FXD::UTF8* pPtr = ((FXD::UTF8*) pLog) + sizeof(FXDMemLog);
		nTotalSize += pLog->m_nAllocSize;
		fprintf(pFile, "[SIZE: %d] [ADDRESS: %p] [FILE: %s] [LINE: %d] [TYPE: %s] \r\n", pLog->m_nAllocSize, pLog, pLog->m_pFileName, pLog->m_nLineNumber, pLog->m_pTypeName);
	}
	fprintf(pFile, "\r\n\r\n[COUNTED STATS] \r\n");
	fprintf(pFile, "[Bytes %d] \r\n", nTotalSize);
	fprintf(pFile, "[Blocks %d] \r\n", nNumBlocks);
	fclose(pFile);
}

#else
void FXD::Memory::Log::ClearTotalDtats(void)
{
}
void FXD::Memory::Log::ClearFrameStats(void)
{
}
void FXD::Memory::Log::DumpMemory(const FXD::UTF8* /*pFileName*/)
{
}
#endif