// Creator - MatthewGolder
#include "FXDEngine/Memory/Log/MemLogger.h"
#include "FXDEngine/Core/Debugger.h"

using namespace FXD;
using namespace Memory;
using namespace Log;

// ------
// MemLogger
// -
// ------
MemLogger::MemLogger(void)
	: m_nAllocSize(0)
	, m_nLineNumber(0)
	, m_pFileName(nullptr)
	, m_pTypeName(nullptr)
	, m_pPrevMem(nullptr)
	, m_pNextMem(nullptr)
{
}

MemLogger::~MemLogger(void)
{
}


// ------
// MemBounds
// -
// ------
MemBounds::MemBounds(void)
{
}

MemBounds::~MemBounds(void)
{
}

void MemBounds::set(size_t nSize)
{
	void* p = &m_deadcafe;
	m_deadcafe = 0xdeadcafe;
	m_nSize = nSize;
}

void MemBounds::validate(void) const
{
	if (m_deadcafe != 0xdeadcafe)
	{
		PRINT_ASSERT << "Memory: Overwrite of memory at address " << (void*)&m_deadcafe;
	}
}