// Creator - MatthewGolder
#include "FXDEngine/Memory/Log/AllocStats.h"
#include "FXDEngine/Memory/Types.h"

using namespace FXD;
using namespace Memory;
using namespace Log;

// ------
// AllocStats
// -
// ------
AllocStats::AllocStats(void)
	: m_nTotalHeapBytesAllocated(0)
	, m_nTotalHeapAllocations(0)
	, m_nTotalHeapDeallocations(0)
{
}

AllocStats::~AllocStats(void)
{
}

#if FXD_ENABLE_MEMORY_TRACKING
void AllocStats::add_allocation(const size_t nAllocSize)
{
	m_nTotalHeapBytesAllocated.add(nAllocSize);
	m_nTotalHeapAllocations.increment();
}
void AllocStats::grow_allocation(const size_t nAllocSize)
{
	m_nTotalHeapBytesAllocated.add(nAllocSize);
}
void AllocStats::remove_allocation(const size_t nAllocSize)
{
	m_nTotalHeapBytesAllocated.add(-nAllocSize);
	m_nTotalHeapDeallocations.increment();
}

size_t AllocStats::total_bytes_allocated(void) const
{
	return m_nTotalHeapBytesAllocated.get_instant_value();
}
size_t AllocStats::total_allocations(void) const
{
	return m_nTotalHeapAllocations.get_instant_value();
}
size_t AllocStats::total_deallocations(void) const
{
	return m_nTotalHeapDeallocations.get_instant_value();
}
#else
void AllocStats::add_allocation(const size_t /*nAllocSize*/)
{
}
void AllocStats::grow_allocation(const size_t /*nAllocSize*/)
{
}
void AllocStats::remove_allocation(const size_t /*nAllocSize*/)
{
}

size_t AllocStats::total_bytes_allocated(void) const
{
	return 0;
}
size_t AllocStats::total_allocations(void) const
{
	return 0;
}
size_t AllocStats::total_deallocations(void) const
{
	return 0;
}
#endif