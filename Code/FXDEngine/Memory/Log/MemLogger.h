// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_LOG_MEMLOGGER_H
#define FXDENGINE_MEMORY_LOG_MEMLOGGER_H

#include "FXDEngine/Core/Types.h"

namespace FXD
{
	namespace Memory
	{
		namespace Log
		{
			// ------
			// MemLogger
			// - Store information about memory allocated.
			// ------
			class MemLogger
			{
			public:
				MemLogger(void);
				~MemLogger(void);

			public:
				FXD::U32 m_nAllocSize;
				FXD::U16 m_nLineNumber;
				const FXD::UTF8* m_pFileName;
				const FXD::UTF8* m_pTypeName;
				Log::MemLogger* m_pPrevMem;
				Log::MemLogger* m_pNextMem;
			};

			// ------
			// MemBounds
			// -
			// ------
			class MemBounds
			{
			public:
				MemBounds(void);
				~MemBounds(void);

				void set(size_t nSize);
				void validate(void) const;

			public:
				FXD::U32 m_deadcafe;
				FXD::U32 m_nSize;
			};

		} //namespace Log
	} //namespace Memory
} //namespace FXD
#endif //FXDENGINE_MEMORY_LOG_MEMLOGGER_H