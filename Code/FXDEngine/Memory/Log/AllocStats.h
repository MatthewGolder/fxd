// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_LOG_ALLOCSTATS_H
#define FXDENGINE_MEMORY_LOG_ALLOCSTATS_H

#include "FXDEngine/Thread/AtomicInt.h"

namespace FXD
{
	namespace Memory
	{
		namespace Log
		{
			// ------
			// AllocStats
			// -
			// ------
			class AllocStats
			{
			public:
				AllocStats(void);
				~AllocStats(void);

				void add_allocation(const size_t nAllocSize);
				void grow_allocation(const size_t nAllocSize);
				void remove_allocation(const size_t nAllocSize);

				size_t total_bytes_allocated(void) const;
				size_t total_allocations(void) const;
				size_t total_deallocations(void) const;

			public:
				Thread::AtomicInt m_nTotalHeapBytesAllocated;
				Thread::AtomicInt m_nTotalHeapAllocations;
				Thread::AtomicInt m_nTotalHeapDeallocations;
			};

		} //namespace Log
	} //namespace Memory
} //namespace FXD
#endif //FXDENGINE_MEMORY_LOG_ALLOCSTATS_H