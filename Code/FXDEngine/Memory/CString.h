// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_CSTRING_H
#define FXDENGINE_MEMORY_CSTRING_H

#include "FXDEngine/Core/Types.h"
#include "FXDEngine/Core/CompileTimeChecks.h"

namespace FXD
{
	namespace Memory
	{
		namespace Internal
		{
			extern void* _MemMove(void* const pDst, const void* const pSrc, const size_t nSize);
			extern void* _MemCopy(void* const pDst, const void* const pSrc, const size_t nSize);
			extern void* _MemSet(void* const pDst, const FXD::S32 nVal, const size_t nSize);
			extern void* _MemZero(void* const pDst, const size_t nSize);
			extern FXD::S32 _MemCompare(const void* const pDst, const void* const pSrc, const size_t nSize);
			extern const void* _Memcheck8(const void* const pDst, FXD::U8 nVal, size_t nByteCount);
			extern const void* _Memcheck16(const void* const pDst, FXD::U16 nVal, size_t nByteCount);
			extern const void* _Memcheck32(const void* const pDst, FXD::U32 nVal, size_t nByteCount);
			extern const void* _Memcheck64(const void* const pDst, FXD::U64 nVal, size_t nByteCount);
		} //namespace Internal

		static inline void* MemMove(void* const pDst, const void* const pSrc, const size_t nSize)
		{
			return Memory::Internal::_MemMove(pDst, pSrc, nSize);
		}
		template < class T >
		static inline void* MemMove_T(T& Src, const FXD::S32 nVal)
		{
			CTC_IS_TYPE_POINTER(T);
			return Memory::MemMove(&Src, nVal, sizeof(T));
		}

		static inline void* MemCopy(void* const pDst, const void* const pSrc, const size_t nSize)
		{
			return Memory::Internal::_MemCopy(pDst, pSrc, nSize);
		}
		template < class T >
		static inline void* MemCopy_T(T& dst, const T& src)
		{
			CTC_IS_TYPE_POINTER(T);
			return Memory::MemCopy(&dst, &src, sizeof(T));
		}

		static inline void* MemSet(void* const pDst, const FXD::S32 nVal, const size_t nSize)
		{
			return Memory::Internal::_MemSet(pDst, nVal, nSize);
		}
		template < class T >
		static inline void* MemSet_T(T& dst, const FXD::S32 nVal)
		{
			CTC_IS_TYPE_POINTER(T);
			return Memory::MemSet(&dst, nVal, sizeof(T));
		}

		static inline void* MemZero(void* const pDst, const size_t nSize)
		{
			return Memory::Internal::_MemZero(pDst, nSize);
		}
		template < class T >
		static inline void* MemZero_T(T& dst)
		{
			CTC_IS_TYPE_POINTER(T);
			return Memory::MemZero(&dst, sizeof(T));
		}

		static inline FXD::S32 MemCompare(const void* const pDst, const void* const pSrc, const size_t nSize)
		{
			return Memory::Internal::_MemCompare(pDst, pSrc, nSize);
		}
		template < class T >
		static inline FXD::S32 MemCompare_T(const T& dst, const T& src)
		{
			CTC_IS_TYPE_POINTER(T);
			return Memory::MemCompare(&dst, &src, sizeof(T));
		}

		static inline const void* Memcheck8(const void* const pDst, FXD::U8 nVal, size_t nByteCount)
		{
			return Memory::Internal::_Memcheck8(pDst, nVal, nByteCount);
		}
		template < class T >
		static inline const void* Memcheck8_T(const T& dst, FXD::U8 nVal)
		{
			CTC_IS_TYPE_POINTER(T);
			return Memory::Memcheck8(&dst, nVal, sizeof(T));
		}

		static inline const void* Memcheck16(const void* const pDst, FXD::U16 nVal, size_t nByteCount)
		{
			return Memory::Internal::_Memcheck16(pDst, nVal, nByteCount);
		}
		template < class T >
		static inline const void* Memcheck16_T(const T& dst, FXD::U16 nVal)
		{
			CTC_IS_TYPE_POINTER(T);
			return Memory::Memcheck16(&dst, nVal, sizeof(T));
		}

		static inline const void* Memcheck32(const void* const pDst, FXD::U32 nVal, size_t nByteCount)
		{
			return Memory::Internal::_Memcheck32(pDst, nVal, nByteCount);
		}
		template < class T >
		static inline const void* Memcheck32_T(const T& dst, FXD::U32 nVal)
		{
			CTC_IS_TYPE_POINTER(T);
			return Memory::Memcheck32(&dst, nVal, sizeof(T));
		}

		static inline const void* Memcheck64(const void* const pDst, FXD::U64 nVal, size_t nByteCount)
		{
			return Memory::Internal::_Memcheck64(pDst, nVal, nByteCount);
		}
		template < class T >
		static inline const void* Memcheck64_T(const T& dst, FXD::U64 nVal)
		{
			CTC_IS_TYPE_POINTER(T);
			return Memory::Memcheck64(&dst, nVal, sizeof(T));
		}
	} //namespace Memory
} //namespace FXD
#endif //FXDENGINE_MEMORY_CSTRING_H