// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_ALLOCATOR_ALIGNED_H
#define FXDENGINE_MEMORY_ALLOCATOR_ALIGNED_H

#include "FXDEngine/Memory/Allocator/Heap.h"
#include "FXDEngine/Memory/Allocator/Tracked.h"
#include "FXDEngine/Memory/Types.h"

namespace FXD
{
	namespace Memory
	{
		namespace Alloc
		{
			// ------
			// Allocator
			// -
			// ------
			template < typename AllocType >
			class Allocator : public Alloc::Internal::Tracked< AllocType >
			{
			public:
				using my_type = Allocator< AllocType >;
				using base_type = Alloc::Internal::Tracked< AllocType >;
				using size_type = typename base_type::size_type;
				using difference_type = typename base_type::difference_type;
				using allocator_type = typename base_type::allocator_type;

			public:
				Allocator(const FXD::STD::align_val_t align = FXD::STD::align_val_t(8))
					: base_type(align)
				{}
				EXPLICIT Allocator(const FXD::STD::align_val_t align, const allocator_type& alloc)
					: base_type(align, alloc)
				{}
				~Allocator(void)
				{}
			};

			// ------
			// Aligned
			// -
			// ------
			template < typename AllocType, size_t AlignSize >
			class Aligned : public Alloc::Allocator< AllocType >
			{
			public:
				using my_type = Aligned< AllocType, AlignSize >;
				using base_type = Alloc::Allocator< AllocType >;
				using size_type = typename base_type::size_type;
				using difference_type = typename base_type::difference_type;
				using allocator_type = typename base_type::allocator_type;

				CTC_ASSERT(Alloc::Internal::is_power_of_2(AlignSize), "Memory: Value is not aligned.");

			public:
				Aligned(void)
					: base_type(FXD::STD::align_val_t(AlignSize))
				{}
				EXPLICIT Aligned(const allocator_type& alloc)
					: base_type(FXD::STD::align_val_t(AlignSize), alloc)
				{}
				~Aligned(void)
				{}
			};

			// ------
			// AlignedType
			// -
			// ------
			template < typename T, typename AlignType >
			class AlignedType : public Alloc::Aligned< T, FXD_ALIGN_OF(AlignType) >
			{
			public:
				using my_type = AlignedType< T, AlignType >;
				using base_type = Alloc::Aligned< T, FXD_ALIGN_OF(AlignType) >;
				using value_type = T;
				using size_type = typename base_type::size_type;
				using difference_type = typename base_type::difference_type;
				using allocator_type = typename base_type::allocator_type;

			public:
				AlignedType(void)
					: base_type()
				{}
				EXPLICIT AlignedType(const allocator_type& alloc)
					: base_type(alloc)
				{}
				~AlignedType(void)
				{}

			private:
				//NO_COPY(AlignedType);
				NO_ASSIGNMENT(AlignedType);
			};

			// ------
			// AlignedType
			// -
			// ------
			template < typename Allocator, typename AlignType >
			inline bool operator==(const AlignedType< Allocator, AlignType >&, const AlignedType< Allocator, AlignType >&)
			{
				return true;
			}
			template < typename Allocator, typename AlignType >
			inline bool operator!=(const AlignedType< Allocator, AlignType >&, const AlignedType< Allocator, AlignType >&)
			{
				return false;
			}

			static Memory::Alloc::Internal::Tracked< Memory::Alloc::Heap > Default;

		} //namespace Alloc
	} //namespace Memory
} //namespace FXD
#endif //FXDENGINE_MEMORY_ALLOCATOR_ALIGNED_H