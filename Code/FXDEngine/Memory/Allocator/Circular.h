// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_ALLOCATOR_CIRCULAR_H
#define FXDENGINE_MEMORY_ALLOCATOR_CIRCULAR_H

#include "FXDEngine/Memory/Allocator/Aligned.h"
#include "FXDEngine/Memory/Allocator/Tracked.h"
#include "FXDEngine/Core/NonCopyable.h"

#include <memory>

namespace FXD
{
	namespace Memory
	{
		namespace Alloc
		{
			/*
			class ICircular;
			typedef std::shared_ptr< ICircular > CircularPtr;

			// ------
			// ICircular
			// -
			// ------
			class ICircular : public Core::NonCopyable
			{
			public:
				// ------
				// AllocEntry
				// -
				// ------
				class AllocEntry
				{
				public:
					AllocEntry(void)
					{}
					~AllocEntry(void)
					{}

					inline void initialise(void) { m_nNext = 0; }
					inline void set_next(FXD::U32 nNext) { m_nNext = nNext | (m_nNext & kTopBit); }
					inline FXD::U32 get_next(void) { return m_nNext & (kTopBit - 1); }
					inline void set_unused(void) { m_nNext |= kTopBit; }
					inline bool is_unused(void) { return (m_nNext & kTopBit) != 0; }

				private:
					static const FXD::U32 kTopBit = 0x80000000;
					FXD::U32 m_nNext; // kTopBit is set if the block is free.
				};

			public:
				enum
				{
					DEFAULT_ALIGNMENT = FXD_ALIGN_OF(AllocEntry)
				};

				using alloc_type = AlignedType< Memory::Alloc::Tracked< Memory::Alloc::Heap >, AllocEntry >;

				ICircular(FXD::U32 nSize);
				~ICircular(void);

				void* allocate(FXD::U32 nSize);
				bool grow_memory(void* pMem, FXD::U32 nSize);
				void deallocate(void* pMem);

			private:
				void* _offset_address(FXD::U32 nOffset);
				FXD::U32 _address_offset(void* pAddr);
				void _advance_first_offset(void);
				FXD::U32 _get_memory_size(void* pMem);

			private:
				alloc_type m_alloc;
				void* m_pBuffer;				// Pointer to the circular buffer.
				FXD::U32 m_nBufferSize;		// Size of the circular buffer.
				FXD::U32 m_nFirstOffset;	// Offset of the first entry in the list.			
				FXD::U32 m_nLastOffset;		// Offset of the last entry in the list.			
				FXD::U32 m_nLastSize;		// Size of the last entry in the list.			
				FXD::U32 m_nNumAllocs;		// Number of outstanding allocations.
			};


			// ------
			// Circular
			// -
			// ------
			class Circular
			{
			public:
				enum
				{
					DEFAULT_ALIGNMENT = ICircular::DEFAULT_ALIGNMENT
				};

			public:
				Circular(size_t nSize);
				~Circular(void);

				void* allocate(FXD::U32 nSize);
				bool grow_memory(void* pMem, FXD::U32 nSize);
				void deallocate(void* pMem);

			private:
				CircularPtr m_circularPtr;
			};
			*/

		} //namespace Alloc
	} //namespace Memory
} //namespace FXD
#endif //FXDENGINE_MEMORY_ALLOCATOR_CIRCULAR_H