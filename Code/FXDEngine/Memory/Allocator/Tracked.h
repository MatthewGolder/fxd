// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_ALLOCATOR_INTERNAL_TRACKED_H
#define FXDENGINE_MEMORY_ALLOCATOR_INTERNAL_TRACKED_H

#include "FXDEngine/Core/CompileTimeChecks.h"
#include "FXDEngine/Core/Internal/ToUnderlying.h"
#include "FXDEngine/Memory/New.h"
#include "FXDEngine/Memory/Log/Log.h"
#include "FXDEngine/Memory/Log/MemLogger.h"

namespace FXD
{
	namespace Memory
	{
		namespace Alloc
		{
			namespace Internal
			{
				CONSTEXPR bool is_power_of_2(size_t nVal)
				{
					return !(nVal == 0) && !(nVal & (nVal - 1));
				}

				inline bool is_aligned(const void* pPtr, FXD::PtrSizedInt alignment) NOEXCEPT
				{
					auto iptr = reinterpret_cast< FXD::PtrSizedInt >(pPtr);
					return !(iptr % alignment);
				}

				// ------
				// Tracked
				// -
				// ------
				template < typename AllocType >
				class Tracked
				{
				public:
					using my_type = Tracked< AllocType >;
					using size_type = size_t;
					using difference_type = std::ptrdiff_t;
					using allocator_type = AllocType;

				public:
					Tracked(const FXD::STD::align_val_t align = FXD::STD::align_val_t(8), const allocator_type& alloc = allocator_type());
					~Tracked(void);

#if FXD_ENABLE_MEMORY_TRACKING
					inline void* allocate(size_type nSize, const FXD::UTF8* pFileName = __FILE__, const FXD::U32 nLine = __LINE__, const FXD::UTF8* pTypeName = nullptr);
					inline bool grow_memory(void* pMemIn, size_type nSize, const FXD::UTF8* pFileName = __FILE__, const FXD::U32 nLine = __LINE__, const FXD::UTF8* pTypeName = nullptr);
					void deallocate(void* pMem, size_type nSize = 1);
#else
					void* allocate(size_type nSize);
					bool grow_memory(void* pMemIn, size_type nSize);
					void deallocate(void* pMem, size_type nSize = 1);
#endif
					size_type alignment(void)const;

				private:
					size_type _required_size(size_type nSize)const;

				public:
					const FXD::STD::align_val_t m_align;
					allocator_type m_alloc;
				};

				// ------
				// Tracked
				// -
				// ------
				template < typename Allocator >
				Tracked< Allocator >::Tracked(const FXD::STD::align_val_t align, const Allocator& alloc)
					: m_align(align)
					, m_alloc(alloc)
				{}
				template < typename Allocator >
				Tracked< Allocator >::~Tracked(void)
				{}

#if FXD_ENABLE_MEMORY_TRACKING
				template < typename Allocator >
				void* Tracked< Allocator >::allocate(size_type nSize, const FXD::UTF8* pFileName, const FXD::U32 nLine, const FXD::UTF8* pTypeName)
#else
				template < typename Allocator >
				void* Tracked< Allocator >::allocate(size_type nSize)
#endif
				{
					const size_type nReqSize = _required_size(nSize);
					size_type nAllocSize = nReqSize;
					size_type nMemOffset = 0;
#if FXD_ENABLE_MEMORY_BOUNDS_CHECK
					size_type nBoundOffset = 0;
#endif
#if FXD_ENABLE_MEMORY_TRACKING
					const size_type nLogSize = _required_size(sizeof(Log::MemLogger));
					nAllocSize += nLogSize;
					nMemOffset += nLogSize;
#if FXD_ENABLE_MEMORY_BOUNDS_CHECK
					nBoundOffset += nLogSize;
#endif
#endif
#if FXD_ENABLE_MEMORY_BOUNDS_CHECK
					const size_type nBoundsSize = _required_size(sizeof(Log::MemBounds));
					nAllocSize += (nBoundsSize * 2);
					nMemOffset += nBoundsSize;
#endif

					FXD::U8* pPtr = (FXD::U8*)m_alloc.allocate(nAllocSize);
					bool b = Memory::Alloc::Internal::is_aligned(pPtr, alignment());
#if FXD_ENABLE_MEMORY_TRACKING
					Memory::Log::RegisterAlloc(pPtr, nAllocSize, pFileName, nLine, pTypeName);
#endif

#if FXD_ENABLE_MEMORY_BOUNDS_CHECK
					Log::MemBounds* pStart = (Log::MemBounds*)(pPtr + nBoundOffset);
					Log::MemBounds* pEnd = (Log::MemBounds*)(pPtr + nMemOffset + nReqSize);
					pStart->set(nReqSize);
					pEnd->set(nReqSize);
#endif
					void* pRetPtr = (pPtr + nMemOffset);
					b = Memory::Alloc::Internal::is_aligned(pRetPtr, alignment());
					return pRetPtr;
				}


#if FXD_ENABLE_MEMORY_TRACKING
				template < typename Allocator >
				bool Tracked< Allocator >::grow_memory(void* pMem, size_type nSize, const FXD::UTF8* pFileName, const FXD::U32 nLine, const FXD::UTF8* pTypeName)
#else
				template < typename Allocator >
				bool Tracked< Allocator >::grow_memory(void* pMem, size_type nSize)
#endif
				{
					const size_type nReqSize = _required_size(nSize);
					size_type nAllocSize = nReqSize;
					size_type nMemOffset = 0;
#if FXD_ENABLE_MEMORY_BOUNDS_CHECK
					size_type nBoundOffset = 0;
#endif
#if FXD_ENABLE_MEMORY_TRACKING
					const size_type nLogSize = _required_size(sizeof(Log::MemLogger));
					nAllocSize += nLogSize;
					nMemOffset += nLogSize;
#if FXD_ENABLE_MEMORY_BOUNDS_CHECK
					nBoundOffset += nLogSize;
#endif
#endif

					FXD::U8* pPtr = (FXD::U8*)pMem;

#if FXD_ENABLE_MEMORY_BOUNDS_CHECK
					const size_type nBoundsSize = _required_size(sizeof(Log::MemBounds));
					nAllocSize += (nBoundsSize * 2);
					nMemOffset += nBoundsSize;

					{
						const Log::MemBounds* pStart = (const Log::MemBounds*)(pPtr - nMemOffset + nBoundOffset);
						const Log::MemBounds* pEnd = (const Log::MemBounds*)(pPtr + pStart->m_nSize);
						pStart->validate();
						pEnd->validate();
					}
#endif

					pPtr -= nMemOffset;
#if FXD_ENABLE_MEMORY_TRACKING
					Memory::Log::UnregisterAlloc(pPtr);
#endif
					bool bRetGrew = m_alloc.grow_memory(pPtr, nAllocSize);
#if FXD_ENABLE_MEMORY_TRACKING
					Memory::Log::RegisterAlloc(pPtr, nAllocSize, pFileName, nLine, pTypeName);
#endif
#if FXD_ENABLE_MEMORY_BOUNDS_CHECK
					if (bRetGrew)
					{
						Log::MemBounds* pStart = (Log::MemBounds*)(pPtr + nBoundOffset);
						Log::MemBounds* pEnd = (Log::MemBounds*)(pPtr + nMemOffset + nReqSize);
						pStart->set(nReqSize);
						pEnd->set(nReqSize);
					}
#endif
					//pMem = (void*)(pPtr + nMemOffset);
					return bRetGrew;
				}

				template < typename Allocator >
				void Tracked< Allocator >::deallocate(void* pMem, size_type nSize)
				{
					size_type nMemOffset = 0;
#if FXD_ENABLE_MEMORY_BOUNDS_CHECK
					size_type nBoundOffset = 0;
#endif
#if FXD_ENABLE_MEMORY_TRACKING
					const size_type nLogSize = _required_size(sizeof(Log::MemLogger));
					nMemOffset += nLogSize;
#if FXD_ENABLE_MEMORY_BOUNDS_CHECK
					nBoundOffset += nLogSize;
#endif
#endif
					FXD::U8* pPtr = (FXD::U8*)pMem;

#if FXD_ENABLE_MEMORY_BOUNDS_CHECK
					const size_type nBoundsSize = _required_size(sizeof(Log::MemBounds));
					nMemOffset += nBoundsSize;

					const Log::MemBounds* pStart = (const Log::MemBounds*)(pPtr - nMemOffset + nBoundOffset);
					const Log::MemBounds* pEnd = (const Log::MemBounds*)(pPtr + pStart->m_nSize);
					pStart->validate();
					pEnd->validate();
#endif
					pPtr -= nMemOffset;
#if FXD_ENABLE_MEMORY_TRACKING
					Memory::Log::UnregisterAlloc(pPtr);
#endif
					m_alloc.deallocate(pPtr, nSize);
				}

				template < typename Allocator >
				typename Tracked< Allocator >::size_type Tracked< Allocator >::alignment(void)const
				{
					return FXD::STD::to_underlying(m_align);
				}

				template < typename Allocator >
				typename Tracked< Allocator >::size_type Tracked< Allocator >::_required_size(size_type nSize)const
				{
					return (nSize + alignment() - 1) & ~(alignment() - 1);
				}

			} //namespace Internal
		} //namespace Alloc
	} //namespace Memory
} //namespace FXD
#endif //FXDENGINE_MEMORY_ALLOCATOR_INTERNAL_TRACKED_H