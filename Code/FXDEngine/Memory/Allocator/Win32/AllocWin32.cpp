// Creator - MatthewGolder
#include "FXDEngine/Core/Types.h"

#if IsSystemWindows()
#include "FXDEngine/Memory/Allocator/Alloc.h"
#include "FXDEngine/Core/Win32/Win32.h"

using namespace FXD;
using namespace Memory;
using namespace Alloc;

// ------
// Alloc
// -
// ------
void* FXD::Memory::Alloc::FXD_Malloc(size_t nSize)
{
	return ::HeapAlloc(GetProcessHeap(), 0, nSize);
	//return ::malloc(nSize);
}

void FXD::Memory::Alloc::FXD_Dealloc(void* pMem)
{
	::HeapFree(GetProcessHeap(), 0, pMem);
	//::free(pMem);
}

bool FXD::Memory::Alloc::FXD_Realloc(void* pMem, size_t nSize)
{
	//size_t nOldSize = Memory::MemSize(pMem);
	void* pRetMem = ::HeapReAlloc(GetProcessHeap(), HEAP_REALLOC_IN_PLACE_ONLY, pMem, nSize);
	//size_t nNewSize = Memory::MemSize(pMem);
	return (pRetMem != nullptr);
	//return ::realloc(pMem, nSize);
}

size_t FXD::Memory::Alloc::FXD_MemSize(void* pMem)
{
	return (size_t)::HeapSize(GetProcessHeap(), 0, pMem);
	//return ::_msize(pMem);
}
#endif