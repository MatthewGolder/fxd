// Creator - MatthewGolder
#include "FXDEngine/Memory/Allocator/Circular.h"
#include "FXDEngine/Core/Debugger.h"

using namespace FXD;
using namespace Memory;
using namespace Alloc;

// ------
// ICircular
// -
// ------
/*
ICircular::ICircular(FXD::U32 nSize)
	: m_alloc(Memory::Alloc::Default)
	, m_pBuffer(nullptr)
	, m_nBufferSize(nSize)
	, m_nFirstOffset(0)
	, m_nLastOffset(0)
	, m_nLastSize(0)
	, m_nNumAllocs(0)
{
	PRINT_COND_ASSERT((m_nBufferSize != 0), "Memory: Circular buffer size must not be zero");
	m_pBuffer = m_alloc.allocate(m_nBufferSize);
	PRINT_COND_ASSERT((m_pBuffer != nullptr), "Memory: Circular buffer could not be allocated");
	//PRINT_INFO << "Memory: ********************CIRCULAR CREATE*********** = " << (void*)this << " Sz=" << m_nBufferSize;
}

ICircular::~ICircular(void)
{
	//PRINT_INFO << "Memory: ********************CIRCULAR DESTROY*********** = " << (void*)this;
	PRINT_COND_ASSERT((m_nNumAllocs == 0), "Memory: There are " << (FXD::U32)m_nNumAllocs << " outstanding allocs in the Circular allocator, which is now being freed");
	m_alloc.deallocate(m_pBuffer, m_nBufferSize);
}

void* ICircular::allocate(FXD::U32 nSize)
{
	if (nSize == 0)
	{
		return nullptr;
	}

	FXD::U32 nReqSize = (nSize + sizeof(AllocEntry));
	nReqSize = (nReqSize + DEFAULT_ALIGNMENT - 1) & ~(DEFAULT_ALIGNMENT - 1);

	bool bAllocFromHeap = false;
	FXD::U32 nStartOffset = (m_nLastOffset + m_nLastSize);
	if ((nStartOffset + nReqSize) > m_nBufferSize)
	{
		if (nStartOffset < m_nFirstOffset)
		{
			bAllocFromHeap = true;
		}
		else if (m_nFirstOffset == 0)
		{
			bAllocFromHeap = true;
		}
		nStartOffset = 0;
	}
	if ((nStartOffset < m_nFirstOffset) && ((nStartOffset + nReqSize) >= m_nFirstOffset))
	{
		bAllocFromHeap = true;
	}
	if (bAllocFromHeap)
	{
		m_nNumAllocs++;
		return m_alloc.allocate(nSize);
	}
	else
	{
		AllocEntry* pLastEnt = (AllocEntry*)_offset_address(m_nLastOffset);
		AllocEntry* pNextEnt = (AllocEntry*)_offset_address(nStartOffset);
		pNextEnt->initialise();
		pLastEnt->set_next(nStartOffset);
		m_nLastOffset = nStartOffset;
		m_nLastSize = nReqSize;
		m_nNumAllocs++;
		return pNextEnt + 1;
	}
}

void ICircular::deallocate(void* pMem)
{
	if (pMem == nullptr)
	{
		return;
	}

	if ((pMem < m_pBuffer) || (pMem >= (FXD::U8*)m_pBuffer + m_nBufferSize))
	{
		m_alloc.deallocate(pMem, m_nBufferSize);
		m_nNumAllocs--;
		return;
	}

	AllocEntry* pEnt = (AllocEntry*)pMem - 1;
	PRINT_COND_ASSERT((pEnt->is_unused() == false), "Memory: Memory already deallocated");
	pEnt->set_unused();
	_advance_first_offset();
	m_nNumAllocs--;
}

bool ICircular::grow_memory(void* pMem, FXD::U32 nSize)
{
	if (pMem == nullptr)
	{
		return false;
	}

	if ((pMem < m_pBuffer) || (pMem >= (FXD::U8*)m_pBuffer + m_nBufferSize))
	{
		return m_alloc.grow_memory(pMem, nSize);
	}

	// Compute total required size, including header and padding for alignment.
	FXD::U32 nReqSize = (nSize + sizeof(AllocEntry));
	nReqSize = (nReqSize + DEFAULT_ALIGNMENT - 1) & ~(DEFAULT_ALIGNMENT - 1);

	// Get the memory header and it's offset.
	AllocEntry* pEnt = (AllocEntry*)pMem - 1;
	FXD::U32 nThisOffset = _address_offset(pEnt);
	PRINT_COND_ASSERT((pEnt->is_unused() == false), "Memory: Memory not allocated");

	// Get the offset of the next used block, and expand this block into any free blocks in between.
	FXD::U32 nNextOffset = pEnt->get_next();
	bool bNextIsUsed = false;
	while ((!bNextIsUsed) && (nNextOffset > nThisOffset))
	{
		AllocEntry* pNext = (AllocEntry*)_offset_address(nNextOffset);
		if (pNext->is_unused())
		{
			if (nNextOffset == m_nLastOffset)
			{
				m_nLastOffset = nThisOffset;
				m_nLastSize = _get_memory_size(pMem) + sizeof(AllocEntry);
			}
			nNextOffset = pNext->get_next();
			pEnt->set_next(nNextOffset);
		}
		else
		{
			bNextIsUsed = true;
		}
	}

	// Make sure we don't run past the first block if it's ahead of us, or past the end of the buffer.
	if (nNextOffset == 0)
	{
		if (m_nFirstOffset > nThisOffset)
		{
			nNextOffset = m_nFirstOffset - 1;// We subtract one because we never want to catch up with the first block - doing so could be confused with the buffer being empty (end==first).
		}
		else
		{
			nNextOffset = m_nBufferSize;
		}
	}

	// We can grow if the required size fit's within this block.
	bool nRet = (nReqSize <= (nNextOffset - nThisOffset));
	if (nRet == 0)
	{
		// If this is the last alloc, update the size of it.
		if (nThisOffset == m_nLastOffset)
		{
			m_nLastSize = nReqSize;
		}
	}
	return nRet;
}

void* ICircular::_offset_address(FXD::U32 nOffset)
{
	PRINT_COND_ASSERT((nOffset < m_nBufferSize), "Memory: Buffer offset outside of range");
	PRINT_COND_ASSERT((nOffset & (DEFAULT_ALIGNMENT - 1)) == 0, "Memory: Offset is not aligned");
	return (FXD::U8*)m_pBuffer + nOffset;
}

FXD::U32 ICircular::_address_offset(void* pAddr)
{
	PRINT_COND_ASSERT((pAddr >= m_pBuffer), "Memory: Negative offsets illegal");

	FXD::U32 nOffset = (FXD::U32)((FXD::U8*)pAddr - (FXD::U8*)m_pBuffer);
	PRINT_COND_ASSERT((nOffset < m_nBufferSize), "Memory: Buffer offset outside of range");
	PRINT_COND_ASSERT((nOffset & (DEFAULT_ALIGNMENT - 1)) == 0, "Memory: Offset is not aligned");
	return nOffset;
}

void ICircular::_advance_first_offset(void)
{
	while (m_nFirstOffset != m_nLastOffset)
	{
		AllocEntry* pFirstEnt = (AllocEntry*)_offset_address(m_nFirstOffset);
		if (!pFirstEnt->is_unused())
		{
			return;
		}
		m_nFirstOffset = pFirstEnt->get_next();
	}
}

FXD::U32 ICircular::_get_memory_size(void* pMem)
{
	if ((pMem < m_pBuffer) || (pMem >= (FXD::U8*)m_pBuffer + m_nBufferSize))
	{
		return 0; // Heap alloc - unknown size.
	}

	AllocEntry* pEnt = (AllocEntry*)pMem - 1;
	FXD::U32 nMemOffset = _address_offset(pMem);
	FXD::U32 nNextOffset = pEnt->get_next();
	if (nNextOffset == 0)
	{
		nNextOffset = m_nFirstOffset;
		if (nNextOffset < nMemOffset)
		{
			nNextOffset = m_nBufferSize;
		}
	}
	return (nNextOffset - nMemOffset);
}


// ------
// ICircular
// -
// ------
Circular::Circular(size_t nSize)
{
	m_circularPtr.reset(FXD_NEW(FXD::Memory::Alloc::ICircular)(nSize));
}

Circular::~Circular(void)
{
}

void* Circular::allocate(FXD::U32 nSize)
{
	PRINT_COND_ASSERT(m_circularPtr, "Memory: Circular allocator not initialised");
	return m_circularPtr->allocate(nSize);
}

bool Circular::grow_memory(void* pMem, FXD::U32 nSize)
{
	PRINT_COND_ASSERT(m_circularPtr, "Memory: Circular allocator not initialised");
	return m_circularPtr->grow_memory(pMem, nSize);
}

void Circular::deallocate(void* pMem)
{
	PRINT_COND_ASSERT(m_circularPtr, "Memory: Circular allocator not initialised");
	m_circularPtr->deallocate(pMem);
}
*/