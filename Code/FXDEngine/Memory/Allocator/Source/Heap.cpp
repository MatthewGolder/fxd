// Creator - MatthewGolder
#include "FXDEngine/Memory/Allocator/Heap.h"
#include "FXDEngine/Memory/Allocator/Alloc.h"
#include "FXDEngine/Thread/Thread.h"

using namespace FXD;
using namespace Memory;
using namespace Alloc;

// ------
// Heap
// -
// ------
void* Heap::allocate(size_t nSize)
{
	void* pMem = nullptr;
	size_t nCount = 0;
	while ((pMem == nullptr) && (nCount < 20))
	{
		pMem = Memory::Alloc::FXD_Malloc(nSize);
		if (pMem == nullptr)
		{
			Thread::Funcs::Sleep(50 + (nCount * 100));
			nCount++;
		}
	}
	return pMem;
}

bool Heap::grow_memory(void* pMem, size_t nSize)
{
	return Memory::Alloc::FXD_Realloc(pMem, nSize);
}

void Heap::deallocate(void* pMem, size_t /*nSize*/)
{
	Memory::Alloc::FXD_Dealloc(pMem);
}