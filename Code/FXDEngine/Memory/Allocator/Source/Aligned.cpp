// Creator - MatthewGolder
#include "FXDEngine/Memory/Allocator/Aligned.h"

using namespace FXD;
using namespace Memory;
using namespace Alloc;

Memory::Alloc::Internal::Tracked< Memory::Alloc::Heap > Default;