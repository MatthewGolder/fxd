// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_ALLOCATOR_ALLOC_H
#define FXDENGINE_MEMORY_ALLOCATOR_ALLOC_H

#include "FXDEngine/Memory/Types.h"

namespace FXD
{
	namespace Memory
	{
		namespace Alloc
		{
			extern void* FXD_Malloc(size_t nSize);
			extern void FXD_Dealloc(void* pMem);
			extern bool FXD_Realloc(void* pMem, size_t nSize);
			extern size_t FXD_MemSize(void* pMem);

		} //namespace Alloc
	} //namespace Memory
} //namespace FXD
#endif //FXDENGINE_MEMORY_ALLOCATOR_ALLOC_H