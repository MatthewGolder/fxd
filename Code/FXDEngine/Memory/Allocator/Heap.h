// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MEMORY_ALLOCATOR_HEAP_H
#define FXDENGINE_MEMORY_ALLOCATOR_HEAP_H

#include "FXDEngine/Core/CompileTimeChecks.h"
#include "FXDEngine/Core/Types.h"

namespace FXD
{
	namespace Memory
	{
		namespace Alloc
		{
			// ------
			// Heap
			// -
			// ------
			class Heap
			{
			public:
				void* allocate(size_t nSize);
				bool grow_memory(void* pMem, size_t nSize);
				void deallocate(void* pMem, size_t nSize);
			};

		} //namespace Alloc
	} //namespace Memory
} //namespace FXD
#endif //FXDENGINE_MEMORY_ALLOCATOR_HEAP_H