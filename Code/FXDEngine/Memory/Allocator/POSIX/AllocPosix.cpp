// Creator - MatthewGolder
#include "FXDEngine/Core/Types.h"

#if IsSystemPosix()
#include "FXDEngine/Memory/Allocator/Alloc.h"
#include "FXDEngine/Core/Debugger.h"

#include <malloc.h>
#include <stdlib.h>

using namespace FXD;
using namespace Memory;
using namespace Alloc;

// ------
// Alloc
// -
// ------
void* FXD::Memory::Alloc::FXD_Malloc(size_t nSize)
{
	return ::malloc(nSize);
}

void FXD::Memory::Alloc::FXD_Dealloc(void* pMem)
{
	::free(pMem);
}

bool FXD::Memory::Alloc::FXD_Realloc(void* pMem, size_t nSize)
{
	//size_t nOldSize = Memory::MemSize(pMem);
	void* pRetMem = ::realloc(pMem, nSize);
	//size_t nNewSize = Memory::MemSize(pMem);
	return (pRetMem != nullptr);
}

size_t FXD::Memory::Alloc::FXD_MemSize(void* pMem)
{
	PRINT_ASSERT << "Memory: malloc_usable_size not avaliable on NDK";
	return 0;//malloc_usable_size(pMem);
}
#endif