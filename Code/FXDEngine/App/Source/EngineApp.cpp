// Creator - MatthewGolder
#include "FXDEngine/App/EngineApp.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/SND/SndJPThread.h"

using namespace FXD;
using namespace App;

// ------
// IEngineApp
// -
// ------
bool IEngineApp::init_game(FXD::S32 argc, FXD::UTF8* argv[], App::E_ApiFlags eFlags)
{
	// 1. Create FXDApi
	m_eGameState = App::E_GameState::Init;
	if (!App::Funcs::FXDAPI_Init(this, argc, argv, eFlags))
	{
		return false;
	}

	// 2. Load game assets
	load();
	return true;
}

void IEngineApp::run_game(void)
{
	// 3. Start the game timer 
	m_eGameState = App::E_GameState::Running;
	m_timer.start_counter();
	m_timer.elapsed_time();
}

void IEngineApp::shutdown_game(void)
{
	// 1. Unload game assets
	unload();

	// 2. Shutdown FXDApi
	App::Funcs::FXDAPI_Shutdown();
}

void IEngineApp::load(void)
{
}

void IEngineApp::unload(void)
{
	m_displayEngine.clear_layers();
}

void IEngineApp::update(void)
{
	FXD::F32 dt = m_timer.elapsed_time();

	m_fps.update(dt);
	m_displayEngine.update(dt);
}

void IEngineApp::render(const GFX::IViewport* pDisplay) const
{
	if (is_shuttingdown())
	{
		m_eGameState = App::E_GameState::Stopped;
	}
	else if ((m_eGameState == App::E_GameState::Init) || (m_eGameState == App::E_GameState::Running))
	{
		m_displayEngine.render(pDisplay);
	}
}

void IEngineApp::on_resize(void)
{
	PRINT_INFO << "Game: Game::on_resize";
	m_displayEngine.on_resize();
}