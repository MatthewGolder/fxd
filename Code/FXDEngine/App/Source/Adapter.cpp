// Creator - MatthewGolder
#include "FXDEngine/App/Adapter.h"

using namespace FXD;
using namespace App;

// ------
// IAdapter
// -
// ------
IAdapter::IAdapter(const App::AdapterDesc& adapterDesc, const App::IApi* pApi)
	: m_adapterDesc(adapterDesc)
	, m_pApi(pApi)
{
}

IAdapter::~IAdapter(void)
{
}
