// Creator - MatthewGolder
#include "FXDEngine/App/Api.h"

using namespace FXD;
using namespace App;

// ------
// IApi
// -
// ------
IApi::IApi(E_Api eApi)
	: m_eApi(eApi)
{
}

IApi::~IApi(void)
{
}

App::AdapterDesc IApi::find_default_adapter(void) const
{
	fxd_for(auto& adapter, m_adapterDescs)
	{
		if (adapter.second->isDefault())
		{
			return adapter.second;
		}
	}
	return nullptr;
}

App::AdapterDesc IApi::find_adapter(const Core::String8& strName) const
{
	auto itr = m_adapterDescs.find(strName);
	if (itr != m_adapterDescs.end())
	{
		return (*itr).second;
	}
	return (*m_adapterDescs.begin()).second;
}