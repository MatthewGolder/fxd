// Creator - MatthewGolder
#include "FXDEngine/App/DisplayLayer.h"
#include "FXDEngine/Thread/Thread.h"

using namespace FXD;
using namespace App;

// ------
// IDisplayLayer
// -
// ------
IDisplayLayer::IDisplayLayer(App::E_DisplayLayerType eLayerType)
	: m_pDisplayEngine(nullptr)
	, m_eState(E_DisplayState::FullyOff)
	, m_eLayerType(eLayerType)
	, m_fTransitionValue(0.0f)
{
}

IDisplayLayer::~IDisplayLayer(void)
{
	PRINT_COND_ASSERT((m_pDisplayEngine == nullptr), "Game: IDisplayLayer is being destruced while still displayed - call hide and remove first!");
}

void IDisplayLayer::show(App::DisplayEngine& engine, bool bWait)
{
	if (m_pDisplayEngine == nullptr)
	{
		m_pDisplayEngine = &engine;
		m_pDisplayEngine->add_layer(this);
	}
	if (is_fully_visible())
	{
		return;
	}

	m_eState = E_DisplayState::TransitioningOn;
	while ((bWait) && (!is_fully_visible()))
	{
		FXD::Thread::Funcs::Sleep(50);
	}
}

void IDisplayLayer::hide(bool bWait)
{
	if (m_pDisplayEngine == nullptr)
	{
		return;	
	}
	if (is_hidden())
	{
		return;
	}

	m_eState = E_DisplayState::TransitioningOff;
	while ((bWait) && (!is_hidden()))
	{
		FXD::Thread::Funcs::Sleep(50);
	}
}

void IDisplayLayer::remove(void)
{
	if (m_pDisplayEngine != nullptr)
	{
		_unload();
		m_pDisplayEngine->remove_layer(this);
		m_pDisplayEngine = nullptr;
	}
}

bool IDisplayLayer::is_hidden(void) const
{
	return (m_eState == E_DisplayState::FullyOff);
}

bool IDisplayLayer::is_visible(void) const
{
	return (m_eState != E_DisplayState::FullyOff);
}

bool IDisplayLayer::is_remove_requested(void) const
{
	return (m_eState == E_DisplayState::RequestRemove);
}

bool IDisplayLayer::is_fully_visible(void) const
{
	return (m_eState == E_DisplayState::FullyOn);
}

void IDisplayLayer::request_remove(void)
{
	m_eState = E_DisplayState::RequestRemove;
}

void IDisplayLayer::_update_state(FXD::F32 dt)
{
	switch (m_eState)
	{
	case E_DisplayState::FullyOff:
	case E_DisplayState::FullyOn:
	{
		break;
	}
	case E_DisplayState::TransitioningOn:
	{
		m_fTransitionValue += dt;
		if (m_fTransitionValue >= 1.0f)
		{
			m_fTransitionValue = 1.0f;
			m_eState = E_DisplayState::FullyOn;
			//_onFullyOn();
		}
		break;
	}
	case E_DisplayState::TransitioningOff:
	{
		m_fTransitionValue -= dt;
		if (m_fTransitionValue <= 0.0f)
		{
			m_fTransitionValue = 0.0f;
			m_eState = E_DisplayState::FullyOff;
			//_onFullyOff();
		}
		break;
	}
	case E_DisplayState::RequestRemove:
	{
		break;
	}
	default:
	{
		break;
	}
	}
}

// ------
// DisplayEngine
// -
// ------
DisplayEngine::DisplayEngine(void)
{
}

DisplayEngine::~DisplayEngine(void)
{
}

void DisplayEngine::update(FXD::F32 dt)
{
	Thread::CSLock::LockGuard lock(m_cs);
	fxd_for(auto& layer, m_layers)
	{
		layer->_update_state(dt);
		if (layer->is_fully_visible())
		{
			layer->_update(dt);
		}
	}
}

void DisplayEngine::render(const FXD::GFX::IViewport* pDisplay) const
{
	Thread::CSLock::LockGuard lock(m_cs);
	fxd_for(const auto& layer, m_layers)
	{
		layer->_render(pDisplay);
	}
	//clearColour = m_layers.back()->colour();
}

void DisplayEngine::on_resize(void)
{
	Thread::CSLock::LockGuard lock(m_cs);
}

void DisplayEngine::clear_layers(void)
{
	Thread::CSLock::LockGuard lock(m_cs);
	while (m_layers.size() != 0)
	{
		auto itr = m_layers.begin();
		(*itr)->hide(false);
		(*itr)->remove();
	}
}

void DisplayEngine::add_layer(App::IDisplayLayer* pLayer)
{
	Thread::CSLock::LockGuard lock(m_cs);

	PRINT_COND_ASSERT((m_layers.contains(pLayer) == false), "Game: IDisplayLayer pLayer already displayed");
	m_layers.push_back(pLayer);
	pLayer->_on_focus_gain();
}

void DisplayEngine::remove_layer(App::IDisplayLayer* pLayer)
{
	Thread::CSLock::LockGuard lock(m_cs);

	//auto bTopLayer = (m_layers.back() == pLayer);
	m_layers.find_erase(pLayer);
}

void DisplayEngine::bring_layer_to_front(App::IDisplayLayer* pLayer)
{
	Thread::CSLock::LockGuard lock(m_cs);

	//auto bTopLayer = (m_layers.back() == pLayer);
	m_layers.bring_to_front(m_layers.find(pLayer));
}