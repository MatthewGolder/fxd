// Creator - MatthewGolder
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/App/EngineApp.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/Accounts/AccountJPThread.h"
#include "FXDEngine/Accounts/AccountSystem.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/GfxSystem.h"
#include "FXDEngine/Input/InputJPThread.h"
#include "FXDEngine/Input/InputSystem.h"
#include "FXDEngine/Input/HIDAPI/HidApiJPThread.h"
#include "FXDEngine/Input/HIDAPI/HidApiManager.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOSystem.h"
#include "FXDEngine/Job/EventHandler.h"
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/SndSystem.h"
#include "3rdParty/google/protobuf/stubs/common.h"

using namespace FXD;
using namespace App;

// 0 - Not Initialised
// 1 - Initialised
// 2 - ShutDown
FXD::S32 nFXDStatus = 0;
// ------
// FXDApi
// -
// ------
FXDApi::FXDApi(void)
	: m_pEngineApp(nullptr)
	, m_pEventHandler(nullptr)
	, m_pAsyncJobRunner(nullptr)
	, m_pPlatform(nullptr)
	, m_pIOThread(nullptr)
	, m_pSndThread(nullptr)
	, m_pGfxThread(nullptr)
	, m_pInputThread(nullptr)
	, m_pHidApiThread(nullptr)
	, m_pAccounts(nullptr)
{
	if (nFXDStatus == 2)
	{
		PRINT_ASSERT << "App: FXDApi Accessed after shutdown";
	}
}

FXDApi::~FXDApi(void)
{
	PRINT_COND_ASSERT((m_pEngineApp == nullptr), "App: m_pEngineApp has not been shutdown");
	PRINT_COND_ASSERT((m_pEventHandler == nullptr), "App: m_pEventHandler has not been shutdown");
	PRINT_COND_ASSERT((m_pAsyncJobRunner == nullptr), "App: m_pAsyncJobRunner has not been shutdown");
	PRINT_COND_ASSERT((m_pPlatform == nullptr), "App: m_pPlatform has not been shutdown");
	PRINT_COND_ASSERT((m_pIOThread == nullptr), "App: m_pIOThread has not been shutdown");
	PRINT_COND_ASSERT((m_pSndThread == nullptr), "App: m_pSndThread has not been shutdown");
	PRINT_COND_ASSERT((m_pGfxThread == nullptr), "App: m_pGfxThread has not been shutdown");
	PRINT_COND_ASSERT((m_pInputThread == nullptr), "App: m_pInputThread has not been shutdown");
	PRINT_COND_ASSERT((m_pHidApiThread == nullptr), "App: m_pHidApiThread has not been shutdown");
	PRINT_COND_ASSERT((m_pAccounts == nullptr), "App: m_pAccounts has not been shutdown");
}

bool FXDApi::init(App::IEngineApp* pEngine, FXD::S32 argc, FXD::UTF8* argv[], App::E_ApiFlags eFlags)
{
	GOOGLE_PROTOBUF_VERIFY_VERSION;

	// 1. Store IEngineApp
	if (m_pEngineApp != nullptr)
	{
		return false;
	}
	m_pEngineApp = pEngine;

	// 2. Create EventHandler
	m_pEventHandler = FXD_NEW(Job::EventHandler);

	// 3. Create AsyncJobRunner
	m_pAsyncJobRunner = FXD_NEW(Job::AsyncJobRunner);

	// 4. Create IPlatform
	m_pPlatform = FXD_NEW(App::IPlatform);
	m_pPlatform->create_platform(argc, argv);

	// 5. Create IIOJPThread
	{
		m_pIOThread = FXD_NEW(IO::IIOJPThread);
		while (!m_pIOThread->is_initialized())
		{
			Thread::Funcs::Sleep(50);
		}
		m_pIOThread->io_system()->create_system().get();
	}

	m_pPlatform->load();

	// 6. Create ISndJPThread
	if (eFlags.is_flag_set(App::E_Api::SND))
	{
		m_pSndThread = FXD_NEW(SND::ISndJPThread);
		while (!m_pSndThread->is_initialized())
		{
			Thread::Funcs::Sleep(50);
		}
		m_pSndThread->snd_system()->create_system().get();
	}

	// 7. Create IGfxJPThread
	if (eFlags.is_flag_set(App::E_Api::GFX))
	{
		m_pGfxThread = FXD_NEW(GFX::IGfxJPThread);
		while (!m_pGfxThread->is_initialized())
		{
			Thread::Funcs::Sleep(50);
		}
		m_pGfxThread->gfx_system()->create_system().get();
	}

	// 8. Create IAccountJPThread
	if (eFlags.is_flag_set(App::E_Api::Accounts))
	{
		m_pAccounts = FXD_NEW(Accounts::IAccountJPThread);
		while (!m_pAccounts->is_initialized())
		{
			Thread::Funcs::Sleep(50);
		}
		m_pAccounts->account_system()->create_system().get();
	}

	// 9. Create IInputJPThread && IHidApiJPThread
	if (eFlags.is_flag_set(App::E_Api::Input))
	{
#if IsInputHIDAPI()
		m_pHidApiThread = FXD_NEW(Input::HidApi::IHidApiJPThread);
		while (!m_pHidApiThread->is_initialized())
		{
			Thread::Funcs::Sleep(50);
		}
		m_pHidApiThread->hidApiManager()->create_system().get();
#endif //IsInputHIDAPI()

		m_pInputThread = FXD_NEW(Input::IInputJPThread);
		while (!m_pInputThread->is_initialized())
		{
			Thread::Funcs::Sleep(50);
		}
		m_pInputThread->input_system()->create_system().get();
		m_pInputThread->input_system()->create_devices().get();
	}

	return true;
}

bool FXDApi::shutdown(void)
{
	// 1. Destroy IInputJPThread && IHidApiJPThread
#if IsInputHIDAPI()
	if (m_pHidApiThread != nullptr)
	{
		m_pHidApiThread->hidApiManager()->release_system().get();
		m_pHidApiThread->issue_stop_signal();
		m_pHidApiThread->wait_for_shutdown();
		FXD_SAFE_DELETE(m_pHidApiThread);
	}
#endif //IsInputHIDAPI()
	if (m_pInputThread != nullptr)
	{
		m_pInputThread->input_system()->release_devices().get();
		m_pInputThread->input_system()->release_system().get();
		m_pInputThread->issue_stop_signal();
		m_pInputThread->wait_for_shutdown();
		FXD_SAFE_DELETE(m_pInputThread);
	}

	// 2. Destroy IAccountJPThread
	if (m_pAccounts != nullptr)
	{
		m_pAccounts->account_system()->release_system().get();
		m_pAccounts->issue_stop_signal();
		m_pAccounts->wait_for_shutdown();
		FXD_SAFE_DELETE(m_pAccounts);
	}

	// 3. Destroy ISndJPThread
	if (m_pSndThread != nullptr)
	{
		m_pSndThread->snd_system()->release_system().get();
		m_pSndThread->issue_stop_signal();
		m_pSndThread->wait_for_shutdown();
		FXD_SAFE_DELETE(m_pSndThread);
	}

	// 4. Destroy IGfxJPThread
	if (m_pGfxThread != nullptr)
	{
		m_pGfxThread->gfx_system()->release_system().get();
		m_pGfxThread->issue_stop_signal();
		m_pGfxThread->wait_for_shutdown();
		FXD_SAFE_DELETE(m_pGfxThread);
	}

	// 5. Destroy IIOJPThread
	if (m_pIOThread != nullptr)
	{
		m_pIOThread->io_system()->release_system().get();
		m_pIOThread->issue_stop_signal();
		m_pIOThread->wait_for_shutdown();
		FXD_SAFE_DELETE(m_pIOThread);
	}

	// 6. Destroy IPlatform
	if (m_pPlatform != nullptr)
	{
		m_pPlatform->release_platform();
		FXD_SAFE_DELETE(m_pPlatform);
	}

	// 7. Destroy AsyncJobRunner
	if (m_pAsyncJobRunner != nullptr)
	{
		m_pAsyncJobRunner->begin_shutdown();
		m_pAsyncJobRunner->wait_for_shutdown();
		FXD::Thread::Funcs::Sleep(1000);
		FXD_SAFE_DELETE(m_pAsyncJobRunner);
	}

	// 8. Destroy EventHandler
	if (m_pEventHandler != nullptr)
	{
		FXD_SAFE_DELETE(m_pEventHandler);
	}

	// 9. Null IEngineApp
	if (m_pEngineApp != nullptr)
	{
		m_pEngineApp = nullptr;
	}
	google::protobuf::ShutdownProtobufLibrary();
	return true;
}

// ------
// Funcs
// -
// ------
bool FXD::App::Funcs::FXDAPI_Init(App::IEngineApp* pEngine, FXD::S32 argc, FXD::UTF8* argv[], App::E_ApiFlags eFlags)
{
	bool bRetVal = App::FXDApi::instance()->init(pEngine, argc, argv, eFlags);
	return bRetVal;
}

bool FXD::App::Funcs::FXDAPI_Shutdown(void)
{
	App::FXDApi::quick_instance()->shutdown();
	App::FXDApi::quick_instance()->kill_instance();
	return (App::FXDApi::quick_instance() == nullptr);
}

App::FXDApi* FXD::FXDApi(void)
{
	return App::FXDApi::quick_instance();
}
App::IEngineApp* FXD::FXDApp(void)
{
	return FXD::FXDApi()->FXDEngineApp();
}
Job::EventHandler* FXD::FXDEvent(void)
{
	return FXD::FXDApi()->FXDEvent();
}
Job::AsyncJobRunner* FXD::FXDAsync(void)
{
	return FXD::FXDApi()->FXDAsync();
}
App::IPlatform* FXD::FXDPlatform(void)
{
	return FXD::FXDApi()->FXDPlatform();
}
IO::IIOJPThread* FXD::FXDIO(void)
{
	return FXD::FXDApi()->FXDIO();
}
SND::ISndJPThread* FXD::FXDSND(void)
{
	return FXD::FXDApi()->FXDSND();
}
GFX::IGfxJPThread* FXD::FXDGFX(void)
{
	return FXD::FXDApi()->FXDGFX();
}
Input::IInputJPThread* FXD::FXDInput(void)
{
	return FXD::FXDApi()->FXDInput();
}
Input::HidApi::IHidApiJPThread* FXD::FXDHidApi(void)
{
	return FXD::FXDApi()->FXDHidApi();
}
Accounts::IAccountJPThread* FXD::FXDAccounts(void)
{
	return FXD::FXDApi()->FXDAccounts();
}