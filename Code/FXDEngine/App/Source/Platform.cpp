// Creator - MatthewGolder
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/Core/Random.h"

using namespace FXD;
using namespace App;

// ------
// GlobalData
// -
// ------
FXD::U64 FXD::App::GetRandSeed(void)
{
	static FXD::U64 nRand = 0;
	if (nRand == 0)
	{
		nRand = FXD::Core::RandomGen(1000).rand();
	}
	return nRand;
}