// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_APP_FXDAPI_H
#define FXDENGINE_APP_FXDAPI_H

#include "FXDEngine/App/Types.h"
#include "FXDEngine/Container/Singleton.h"
#include "FXDEngine/Core/NonCopyable.h"

namespace FXD
{
	namespace Job
	{
		class AsyncJobRunner;
		class EventHandler;
	} //namespace Job
	namespace IO
	{
		class IIOJPThread;
	} //namespace IO
	namespace SND
	{
		class ISndJPThread;
	} //namespace SND
	namespace GFX
	{
		class IGfxJPThread;
	} //namespace GFX
	namespace Input
	{
		class IInputJPThread;
		namespace HidApi
		{
			class IHidApiJPThread;
		} //namespace HidApi
	} //namespace Input
	namespace Accounts
	{
		class IAccountJPThread;
	} //namespace Accounts

	namespace App
	{
		class IEngineApp;
		class IPlatform;
	} //namespace App

	namespace App
	{
		// ------
		// FXDApi
		// -
		// ------
		class FXDApi : public Container::Singleton< FXDApi >, public Core::NonCopyable
		{
		private:
			friend Container::Singleton< FXDApi >;

		public:
			bool init(App::IEngineApp* pEngine, FXD::S32 argc, FXD::UTF8* argv[], App::E_ApiFlags eFlags);
			bool shutdown(void);

			GET_W(App::IEngineApp*, pEngineApp, FXDEngineApp);
			GET_W(Job::EventHandler*, pEventHandler, FXDEvent);
			GET_W(Job::AsyncJobRunner*, pAsyncJobRunner, FXDAsync);
			GET_W(App::IPlatform*, pPlatform, FXDPlatform);
			GET_W(IO::IIOJPThread*, pIOThread, FXDIO);
			GET_W(SND::ISndJPThread*, pSndThread, FXDSND);
			GET_W(GFX::IGfxJPThread*, pGfxThread, FXDGFX);
			GET_W(Input::IInputJPThread*, pInputThread, FXDInput);
			GET_W(Input::HidApi::IHidApiJPThread*, pHidApiThread, FXDHidApi);
			GET_W(Accounts::IAccountJPThread*, pAccounts, FXDAccounts);

		private:
			FXDApi(void);
			~FXDApi(void);

			App::IEngineApp* m_pEngineApp;
			Job::EventHandler* m_pEventHandler;
			Job::AsyncJobRunner* m_pAsyncJobRunner;
			App::IPlatform* m_pPlatform;
			IO::IIOJPThread* m_pIOThread;
			SND::ISndJPThread* m_pSndThread;
			GFX::IGfxJPThread* m_pGfxThread;
			Input::IInputJPThread* m_pInputThread;
			Input::HidApi::IHidApiJPThread* m_pHidApiThread;
			Accounts::IAccountJPThread* m_pAccounts;
		};

		namespace Funcs
		{
			extern bool FXDAPI_Init(App::IEngineApp* pEngine, FXD::S32 argc, FXD::UTF8* argv[], App::E_ApiFlags eFlags);
			extern bool FXDAPI_Shutdown(void);
		} //namespace Funcs

	} //namespace App

	extern App::FXDApi* FXDApi(void);
	extern App::IEngineApp* FXDApp(void);
	extern Job::EventHandler* FXDEvent(void);
	extern Job::AsyncJobRunner* FXDAsync(void);
	extern App::IPlatform* FXDPlatform(void);
	extern IO::IIOJPThread* FXDIO(void);
	extern SND::ISndJPThread* FXDSND(void);
	extern GFX::IGfxJPThread* FXDGFX(void);
	extern Input::IInputJPThread* FXDInput(void);
	extern Input::HidApi::IHidApiJPThread* FXDHidApi(void);
	extern Accounts::IAccountJPThread* FXDAccounts(void);

} //namespace FXD
#endif //FXDENGINE_APP_FXDAPI_H