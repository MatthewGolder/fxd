// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_APP_TYPES_H
#define FXDENGINE_APP_TYPES_H

#include "FXDEngine/Container/Flag.h"

#include <memory>

namespace FXD
{
	namespace App
	{
		// ------
		// E_Api
		// - 
		// ------
		enum class E_Api
		{
			Accounts = 1,
			Input = 2,
			SND = 3,
			GFX = 4,
			Count,
			Unknown = 0xffff
		};
		using E_ApiFlags = FXD::Container::EnumFlag< App::E_Api >;
		ENUM_CLASS_FLAGS(FXD::App::E_Api);

		// IAdapter
		class IAdapter;
		class IAdapterDesc;
		typedef std::shared_ptr< IAdapter > Adapter;
		typedef std::shared_ptr< IAdapterDesc > AdapterDesc;

		// IApi
		class IApi;
		typedef std::unique_ptr< IApi > Api;

	} //namespace App
} //namespace FXD
#endif //FXDENGINE_APP_TYPES_H