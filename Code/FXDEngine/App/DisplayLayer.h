// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_APP_DISPLAYLAYER_H
#define FXDENGINE_APP_DISPLAYLAYER_H

#include "FXDEngine/App/Types.h"
#include "FXDEngine/Container/LinkedList.h"
#include "FXDEngine/Core/Colour.h"
#include "FXDEngine/GFX/Types.h"
#include "FXDEngine/Job/Async.h"
#include "FXDEngine/Job/AsyncJob.h"
#include "FXDEngine/Thread/CriticalSection.h"

namespace FXD
{
	namespace App
	{
		// ------
		// E_DisplayLayerType
		// - 
		// ------
		enum class E_DisplayLayerType
		{
			Opaque,
			Alpha,
			Overlay,
			Count,
			Unknown = 0xffff
		};

		// ------
		// E_DisplayState
		// - 
		// ------
		enum class E_DisplayState
		{
			FullyOff,
			FullyOn,
			TransitioningOn,
			TransitioningOff,
			RequestRemove,
			Count,
			Unknown = 0xffff
		};


		class DisplayEngine;
		// ------
		// IDisplayLayer
		// - 
		// ------
		class IDisplayLayer
		{
		public:
			friend class DisplayEngine;

		public:
			IDisplayLayer(App::E_DisplayLayerType eLayerType);
			virtual ~IDisplayLayer(void);

		public:
			void show(App::DisplayEngine& engine, bool bWait = true);
			void hide(bool bWait = true);
			void remove(void);

			bool is_hidden(void) const;
			bool is_visible(void) const;
			bool is_remove_requested(void) const;
			bool is_fully_visible(void) const;
			void request_remove(void);

			virtual Core::ColourRGBA colour(void) PURE;

		protected:
			virtual void _load(void) PURE;
			virtual void _unload(void) PURE;
			virtual void _update(FXD::F32 dt) PURE;
			virtual void _render(const GFX::IViewport* pDisplay) const PURE;

			virtual void _on_focus_gain(void) PURE;
			virtual void _on_focus_lost(void) PURE;
			//virtual void _on_will_show(void) PURE;
			//virtual void _on_will_hide(void) PURE;

		private:
			void _update_state(FXD::F32 dt);

		protected:
			App::DisplayEngine* m_pDisplayEngine;
			App::E_DisplayState m_eState;
			App::E_DisplayLayerType m_eLayerType;
			FXD::F32 m_fTransitionValue;
		};


		template < typename DisplayLayer, typename... ArgTypes >
		void _RunDisplayLayerAsync(Job::AsyncCancel& cancel, App::DisplayEngine& displayEngine, ArgTypes&&... inArgs)
		{
			DisplayLayer layer(cancel, inArgs...);
			layer.show(displayEngine, true);
			while (cancel.is_cancelled() == false)
			{
				if (layer.is_remove_requested())
				{
					break;
				}
				Thread::Funcs::Sleep(50);
			}
			layer.hide(true);
			layer.remove();
		}

		template < typename DisplayLayer, typename... ArgTypes >
		FXD::Job::Future< void > RunDisplayLayerAsync(Job::AsyncJobRunner* pJRunner, Job::AsyncCancel& cancel, App::DisplayEngine& displayEngine, ArgTypes&&... inArgs)
		{
			return std::move(Job::Async(pJRunner, [&]()
			{
				_RunDisplayLayerAsync< DisplayLayer, ArgTypes... >(cancel, displayEngine, inArgs...);
			}));
		}


		// ------
		// DisplayEngine
		// - 
		// ------
		class DisplayEngine
		{
		private:
			friend class IDisplayLayer;

		public:
			DisplayEngine(void);
			~DisplayEngine(void);

			void update(FXD::F32 dt);
			void render(const FXD::GFX::IViewport* pDisplay) const;
			void on_resize(void);
			void clear_layers(void);

		private:
			void add_layer(App::IDisplayLayer* pLayer);
			void remove_layer(App::IDisplayLayer* pLayer);
			void bring_layer_to_front(App::IDisplayLayer* pLayer);

		private:
			mutable Thread::CSLock m_cs;
			Container::LinkedList< App::IDisplayLayer* > m_layers;
		};

	} //namespace App
} //namespace FXD
#endif //FXDENGINE_APP_DISPLAYLAYER_H