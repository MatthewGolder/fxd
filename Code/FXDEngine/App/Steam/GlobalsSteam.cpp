// Creator - MatthewGolder
#include "FXDEngine/Accounts/Types.h"

#if IsOSPC() && IsAccountSteam()
#include "FXDEngine/App/Steam/GlobalsSteam.h"

using namespace FXD;
using namespace App;

// ------
// GlobalData
// -
// ------
GlobalData::GlobalData(FXD::S32 nAppID, bool bAllowPlayer1MultipleDevices, ENotificationPosition steamOverlayPos)
	: m_nAppID(nAppID)
	, m_bAllowPlayer1MultipleDevices(bAllowPlayer1MultipleDevices)
	, m_steamOverlayPos(steamOverlayPos)
{
}
GlobalData::~GlobalData(void)
{
}
#endif //IsOSPC() && IsAccountSteam()
