// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_APP_STEAM_GLOBALSSTEAM_H
#define FXDENGINE_APP_STEAM_GLOBALSSTEAM_H

#include "FXDEngine/Accounts/Types.h"

#if IsOSPC() && IsAccountSteam()
#include "FXDEngine/Accounts/Steam/Steam.h"
#include "FXDEngine/Core/NonCopyable.h"

namespace FXD
{
	namespace App
	{
		// ------
		// GlobalData
		// -
		// ------
		class GlobalData FINAL : public Core::NonCopyable
		{
		public:
			GlobalData(FXD::S32 nAppID, bool bAllowPlayer1MultipleDevices, ENotificationPosition steamOverlayPos);
			~GlobalData(void);

		public:
			const FXD::S32 m_nAppID;
			const bool m_bAllowPlayer1MultipleDevices;
			const ENotificationPosition m_steamOverlayPos;
		};
	} //namespace App
} //namespace FXD
#endif //IsOSPC() && IsAccountSteam()
#endif //FXDENGINE_APP_STEAM_GLOBALSSTEAM_H