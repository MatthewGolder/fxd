// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_APP_PC_WINMSGPROCESSPC_H
#define FXDENGINE_APP_PC_WINMSGPROCESSPC_H

#include "FXDEngine/Accounts/Types.h"

#if IsOSPC()
#include "FXDEngine/Core/String.h"
#include "FXDEngine/Core/Win32/Win32.h"

namespace FXD
{
	namespace App
	{
		// ------
		// MsgProcessPC
		// -
		// ------
		class MsgProcessPC
		{
		public:
			MsgProcessPC(void);
			virtual ~MsgProcessPC(void);

			bool are_window_messages_enabled(void) const;
			void enable_message_process(bool bVal);

			virtual bool process_win_msg(HWND hWnd, UINT msg, const FXD::UTF8* pMsgStr, WPARAM wParam, LPARAM lParam) PURE;

		private:
			bool m_bEnabled;
		};

	} //namespace App
} //namespace FXD
#endif //IsOSPC()
#endif //FXDENGINE_APP_PC_WINMSGPROCESSPC_H