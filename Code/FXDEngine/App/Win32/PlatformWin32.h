// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_APP_WIN32_PLATFORMWIN32_H
#define FXDENGINE_APP_WIN32_PLATFORMWIN32_H

#include "FXDEngine/Accounts/Types.h"

#if IsOSPC()
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/App/Win32/MsgProcessWin32.h"
#include "FXDEngine/Thread/RWLock.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< App::IPlatform >
			// -
			// ------
			template <>
			class Impl< App::IPlatform > FINAL
			{
			public:
				friend class App::IPlatform;

				class Shared
				{
				public:
					Shared(void);
					~Shared(void);

				public:
					Container::Vector< App::MsgProcessPC* > m_msgProcs;
				};

			public:
				Impl(void);
				~Impl(void);

				bool create_platform(void);
				bool release_platform(void);

				void push_msg_proc(App::MsgProcessPC* pMsgProc);
				void remove_msg_proc(App::MsgProcessPC* pMsgProc);

			private:
				static LRESULT CALLBACK window_process(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

			public:
				ATOM m_hWndClass;
				Thread::RWLockData< Shared > m_shared;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsOSPC()
#endif //FXDENGINE_APP_WIN32_PLATFORMWIN32_H