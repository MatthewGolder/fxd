// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_APP_WIN32_FUNCSWIN32_H
#define FXDENGINE_APP_WIN32_FUNCSWIN32_H

#include "FXDEngine/Accounts/Types.h"

#if IsOSPC()
#include "FXDEngine/IO/Types.h"
#include "FXDEngine/Core/Win32/WinRegistry.h"

namespace FXD
{
	namespace App
	{
		// ------
		// FuncsWin32
		// -
		// ------
		class FuncsWin32
		{
		public:
			static bool runProcess(const IO::Path& path, const Core::String8& strAppPath, const Core::String8& strCmdLine);
			static Core::WinRegistryKey getGameRegistryKey(void);
		};

	} //namespace App
} //namespace FXD
#endif //IsOSPC()
#endif //FXDENGINE_APP_WIN32_FUNCSWIN32_H