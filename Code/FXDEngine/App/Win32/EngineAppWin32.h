// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_APP_WIN32_ENGINEAPPWIN32_H
#define FXDENGINE_APP_WIN32_ENGINEAPPWIN32_H

#include "FXDEngine/Accounts/Types.h"

#if IsOSPC()
#include "FXDEngine/App/EngineApp.h"
#include "FXDEngine/Core/Win32/Win32.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< App::IEngineApp >
			// -
			// ------
			template <>
			class Impl< App::IEngineApp >
			{
			public:
				friend class App::IEngineApp;

			public:
				Impl(void)
					: m_bTriggerShutdown(false)
					, m_bAppShuttingDown(false)
				{
				}
				~Impl(void)
				{
				}

				void trigger_shutdown_actual(void)
				{
					m_bAppShuttingDown = true;
				}

			private:
				bool m_bTriggerShutdown;
				bool m_bAppShuttingDown;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsOSPC()
#endif //FXDENGINE_APP_WIN32_ENGINEAPPWIN32_H