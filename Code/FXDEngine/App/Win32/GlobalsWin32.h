// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_APP_WIN32_GLOBALSWIN32_H
#define FXDENGINE_APP_WIN32_GLOBALSWIN32_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Accounts/Types.h"

#if IsOSPC() && IsAccountWindows()
#include "FXDEngine/Accounts/Win32/Win32.h"
#include "FXDEngine/Core/NonCopyable.h"

namespace FXD
{
	namespace App
	{
		// ------
		// GlobalData
		// -
		// ------
		class GlobalData FINAL : public Core::NonCopyable
		{
		public:
			GlobalData(bool bAllowPlayer1MultipleDevices);
			~GlobalData(void);

		public:
			const bool m_bAllowPlayer1MultipleDevices;
		};
	} //namespace App
} //namespace FXD
#endif //IsOSPC() && IsAccountWindows()
#endif //FXDENGINE_APP_WIN32_GLOBALSWIN32_H