// Creator - MatthewGolder
#include "FXDEngine/App/Types.h"

#if IsOSPC()
#include "FXDEngine/App/EngineApp.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/App/Win32/GlobalsWin32.h"
#include "FXDEngine/App/Win32/PlatformWin32.h"
#include "FXDEngine/GFX/Viewport.h"
#include "FXDEngine/GFX/Window.h"
#include "FXDEngine/IO/IODevice.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOSystem.h"
#include "FXDEngine/IO/Path.h"
#include "FXDEngine/Job/EventHandler.h"

using namespace FXD;
using namespace App;

extern const FXD::UTF8* TranslateWMessage(UINT uMsg);

// ------
// IPlatform::Impl::Shared
// - 
// ------
IPlatform::Impl::Shared::Shared()
{
}

IPlatform::Impl::Shared::~Shared()
{
}

// ------
// IPlatform::Impl
// -
// ------
IPlatform::Impl::Impl::Impl(void)
{
	Memory::MemZero_T(m_hWndClass);
}

IPlatform::Impl::~Impl(void)
{
}

bool IPlatform::Impl::create_platform(void)
{
	bool bRetVal = true;

	WNDCLASSEXW windowClass = {};
	Memory::MemZero_T(windowClass);
	windowClass.cbSize = sizeof(WNDCLASSEXW);
	windowClass.style = (CS_OWNDC | CS_HREDRAW | CS_VREDRAW);
	windowClass.lpfnWndProc = IPlatform::Impl::window_process;
	windowClass.cbClsExtra = 0;
	windowClass.cbWndExtra = 0;
	windowClass.hCursor = LoadCursor(nullptr, IDC_ARROW);
	windowClass.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
	windowClass.hbrBackground = nullptr;
	windowClass.lpszMenuName = nullptr;
	windowClass.hIconSm = nullptr;
	windowClass.lpszClassName = L"FXDWindow";

	m_hWndClass = RegisterClassExW(&windowClass);
	if (m_hWndClass == 0)
	{
		PRINT_ASSERT << "App: Failed to register Window";
		bRetVal = false;
	}
	return bRetVal;
}

bool IPlatform::Impl::release_platform(void)
{
	if (m_hWndClass)
	{
		UnregisterClassW(L"FXDWindow", GetModuleHandle(nullptr));
		Memory::MemZero_T(m_hWndClass);
	}
	return true;
}

void IPlatform::Impl::push_msg_proc(App::MsgProcessPC* pMsgProc)
{
	m_shared.get_quick_write_lock()->m_msgProcs.push_back(pMsgProc);
}

void IPlatform::Impl::remove_msg_proc(App::MsgProcessPC* pMsgProc)
{
	m_shared.get_quick_write_lock()->m_msgProcs.find_erase(pMsgProc);
}

LRESULT CALLBACK IPlatform::Impl::window_process(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	// 1. Handle message handlers
	const FXD::UTF8* pMsgStr = TranslateWMessage(msg);
	bool bAllowDefProc = false;
	{
		Thread::RWLockData< IPlatform::Impl::Shared >::ScopedReadLock rwLock(FXDPlatform()->impl().m_shared);
		const FXD::U16 nSize = rwLock->m_msgProcs.size();
		for (FXD::U16 n1 = 0; n1 < nSize; n1++)
		{
			if (rwLock->m_msgProcs[n1]->are_window_messages_enabled())
			{
				if (!rwLock->m_msgProcs[n1]->process_win_msg(hWnd, msg, pMsgStr, wParam, lParam))
				{
					bAllowDefProc = false;
				}
			}
		}
	}

	// 2. Handle window events
	GFX::IWindow* pWindow = (hWnd) ? (GFX::IWindow*)GetWindowLongPtr(hWnd, GWLP_USERDATA) : nullptr;
	switch (msg)
	{
		case WM_CREATE:
		{
			SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR)(((LPCREATESTRUCT)lParam)->lpCreateParams));

			const GFX::IWindow* pWindowLongPtr = (hWnd) ? (const GFX::IWindow*)GetWindowLongPtr(hWnd, GWLP_USERDATA) : nullptr;
			if (pWindowLongPtr != nullptr)
			{
				if (pWindowLongPtr->window_desc().bVisible)
				{
					ShowWindow(hWnd, SW_SHOWNOACTIVATE);
				}
			}
			else
			{
				ShowWindow(hWnd, SW_SHOWNOACTIVATE);
			}
			return 0;
		}
		case WM_SETFOCUS:
		{
			if (pWindow)
			{
				FXDEvent()->raise(GFX::OnWindowEvent(pWindow->windowHandle(), GFX::E_WindowEvent::FocusReceived));
			}
			return 0;
		}
		case WM_KILLFOCUS:
		{
			if (pWindow)
			{
				FXDEvent()->raise(GFX::OnWindowEvent(pWindow->windowHandle(), GFX::E_WindowEvent::FocusLost));
			}
			return 0;
		}
		case WM_MOVE:
		{
			if (pWindow)
			{
				FXDEvent()->raise(GFX::OnWindowEvent(pWindow->windowHandle(), GFX::E_WindowEvent::Moved));
			}
			return 0;
		}
		case WM_SIZE:
		{
			if (wParam == SIZE_RESTORED || wParam == SIZE_MAXIMIZED)
			{
				if (pWindow)
				{
					FXDEvent()->raise(GFX::OnWindowEvent(pWindow->windowHandle(), GFX::E_WindowEvent::Resized));
				}
			}
			return 0;
		}
		case WM_CLOSE:
		{
			FXD::FXDApp()->trigger_shutdown();
			break;
		}
		default:
		{
			if (!bAllowDefProc)
			{
				return DefWindowProc(hWnd, msg, wParam, lParam);
			}
		}
	}
	return 0;
}

// 0 - Not Initialised
// 1 - Initialised
// 2 - ShutDown
FXD::S32 nAppStatus = 0;
// ------
// IPlatform
// -
// ------
IPlatform::IPlatform(void)
{
	if (nAppStatus == 2)
	{
		PRINT_ASSERT << "App: IPlatform accessed after shutdown";
	}
}

IPlatform::~IPlatform(void)
{
}

bool IPlatform::create_platform(FXD::S32 argc, FXD::UTF8* argv[])
{
	bool bRetVal = m_impl->create_platform();
	m_cmdArgs.set_args(argc, argv);
	nAppStatus = 1;
	return bRetVal;
}

bool IPlatform::release_platform(void)
{
	bool bRetVal = m_impl->release_platform();
	nAppStatus = 2;
	return bRetVal;
}

bool IPlatform::load(void)
{
	if (m_dataRegistry)
	{
		return true;
	}
	else
	{
		// Load the data.
		m_dataRegistry = std::make_shared< IO::IDataRegistry >();
		IO::Path ioPath = IO::FS::GetUtilityDirectory(UTF_8("platform.save"));
		bool bResult = FXDIO()->io_system()->io_device()->load(m_dataRegistry.get(), ioPath, true, { }).get();
		return bResult;
	}
}

bool IPlatform::save(void)
{
	if (!m_dataRegistry)
	{
		return false;
	}
	else
	{
		// Save the data.
		IO::Path ioPath = IO::FS::GetUtilityDirectory(UTF_8("platform.save"));
		bool bResult = FXDIO()->io_system()->io_device()->save(m_dataRegistry.get(), ioPath, false, { }).get();
		return bResult;
	}
}
#endif //IsOSPC()