// Creator - MatthewGolder
#include "FXDEngine/Accounts/Types.h"

#if IsOSPC() && IsAccountWindows()
#include "FXDEngine/App/Win32/GlobalsWin32.h"

using namespace FXD;
using namespace App;

// ------
// GlobalData
// -
// ------
GlobalData::GlobalData(bool bAllowPlayer1MultipleDevices)
	: m_bAllowPlayer1MultipleDevices(bAllowPlayer1MultipleDevices)
{
}
GlobalData::~GlobalData(void)
{
}
#endif //IsOSPC() && IsAccountWindows()
