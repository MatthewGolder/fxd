// Creator - MatthewGolder
#include "FXDEngine/App/Types.h"

#if IsOSPC()
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/App/Win32/FuncsWin32.h"
#include "FXDEngine/IO/Path.h"

using namespace FXD;
using namespace App;

// ------
// FuncsWin32
// -
// ------
bool FuncsWin32::runProcess(const IO::Path& path, const Core::String8& strFileName, const Core::String8& strCmdLine)
{
	IO::Path pathExpanded = path / strFileName.c_str();

	bool bExists = pathExpanded.file_exists();
	if (!bExists)
	{
		return false;
	}

	STARTUPINFOA startupInfo = {};
	Memory::MemZero_T(startupInfo);

	PROCESS_INFORMATION processInfo;
	if (CreateProcessA((LPSTR)pathExpanded.c_str(), (LPSTR)strCmdLine.c_str(), NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &startupInfo, &processInfo) == FALSE)
	{
		return false;
	}

	WaitForSingleObject(processInfo.hProcess, INFINITE);
	CloseHandle(processInfo.hProcess);
	CloseHandle(processInfo.hThread);
	return true;
}

Core::WinRegistryKey FuncsWin32::getGameRegistryKey(void)
{
	return Core::WinRegistryKey(HKEY_CURRENT_USER, false).get_sub_key(UTF_8("Software\\FXD")).get_sub_key(App::GetShortNameUTF8().c_str());
}
#endif //IsOSPC()