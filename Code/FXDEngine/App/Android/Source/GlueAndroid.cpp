// Creator - MatthewGolder
#include "FXDEngine/App/Types.h"

#if IsOSAndroid()
#include "FXDEngine/App/Android/GlueAndroid.h"
#include "FXDEngine/App/Android/GlobalsAndroid.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/Core/Debugger.h"

using namespace FXD;
using namespace App;

extern FXD::S32 fxd_main(FXD::S32 argc, FXD::UTF8* argv[]);

// ------
// _FXDThread
// -
// ------
_FXDThread::_FXDThread(const FXD::UTF8* pName)
	: IThread(pName, false)
{
}

_FXDThread::~_FXDThread(void)
{
	if (!join())
	{
		PRINT_ASSERT << "App: _FXDThread took too long to shutdown";
	}
}

bool _FXDThread::thread_init_func(void)
{
	return true;
}

bool _FXDThread::thread_process_func(void)
{
	fxd_main(0, 0);
	return false;
}

bool _FXDThread::thread_shutdown_func(void)
{
	return true;
}

// ------
// FXDAndroidApp2
// -
// ------
FXDAndroidApp2::FXDAndroidApp2(ANativeActivity* pActivity, void* pSavedState, size_t nSavedStateSize)
	: m_fxdThread("Main Thread")
	, m_pActivity(pActivity)
	, m_pSavedState(pSavedState)
	, m_nSavedStateSize(nSavedStateSize)
	, m_bRunning(false)
{
	m_pActivity->callbacks->onStart = _on_start;
	m_pActivity->callbacks->onDestroy = _on_destroy;
	m_pActivity->callbacks->onResume = _on_resume;
	m_pActivity->callbacks->onPause = _on_pause;
	m_pActivity->callbacks->onStop = _on_stop;
	m_pActivity->callbacks->onLowMemory = _on_low_memory;
	m_pActivity->callbacks->onConfigurationChanged = _on_configuration_changed;
	m_pActivity->callbacks->onWindowFocusChanged = _on_window_focus_changed;
	m_pActivity->callbacks->onNativeWindowCreated = _on_native_window_created;
	m_pActivity->callbacks->onNativeWindowDestroyed = _on_native_window_destroyed;
	m_pActivity->callbacks->onInputQueueCreated = _on_input_queue_created;
	m_pActivity->callbacks->onInputQueueDestroyed = _on_input_queue_destroyed;
	m_pActivity->callbacks->onSaveInstanceState = _on_save_instance_state;
	m_pActivity->instance = this;
	FXD::App::GetGlobalData().m_pActivity = pActivity;

	m_bRunning = true;
	m_fxdThread.start_thread();
	//android_app_create(pActivity, pSavedState, nSavedStateSize);

	while (m_bRunning == true)
	{
		m_event.wait_for(50);
	}
}

FXDAndroidApp2::~FXDAndroidApp2()
{
}

void FXDAndroidApp2::_on_start(ANativeActivity* pActivity)
{
	PRINT_INFO << "App: Start -- " << pActivity;
	//android_app_set_activity_state((struct FXDAndroidApp2*)pActivity->instance, APP_CMD_START);
}

void FXDAndroidApp2::_on_destroy(ANativeActivity* pActivity)
{
	PRINT_INFO << "App: Destroy -- " << pActivity;
	//android_app_free((struct FXDAndroidApp2*)pActivity->instance);
}

void FXDAndroidApp2::_on_resume(ANativeActivity* pActivity)
{
	PRINT_INFO << "App: Resume -- " << pActivity;
	//android_app_set_activity_state((struct FXDAndroidApp2*)pActivity->instance, APP_CMD_RESUME);
}

void FXDAndroidApp2::_on_pause(ANativeActivity* pActivity)
{
	PRINT_INFO << "App: Pause -- " << pActivity;
	//android_app_set_activity_state((struct FXDAndroidApp2*)pActivity->instance, APP_CMD_PAUSE);
}

void FXDAndroidApp2::_on_stop(ANativeActivity* pActivity)
{
	PRINT_INFO << "App: Stop -- " << pActivity;
	//android_app_set_activity_state((struct FXDAndroidApp2*)pActivity->instance, APP_CMD_STOP);
}

void* FXDAndroidApp2::_on_save_instance_state(ANativeActivity* pActivity, size_t* pNLen)
{
	PRINT_INFO << "App: SaveInstanceState -- " << pActivity;
	/*
	struct FXDAndroidApp2* pApp = (struct FXDAndroidApp2*)pActivity->instance;

	pthread_mutex_lock(&pApp->m_mutex);
	pApp->stateSaved = 0;
	android_app_write_cmd(pApp, APP_CMD_SAVE_STATE);
	while (!pApp->stateSaved)
	{
		pthread_cond_wait(&pApp->cond, &pApp->m_mutex);
	}

	void* pSavedState = NULL;
	if (pApp->m_pSavedState != NULL)
	{
		pSavedState = pApp->m_pSavedState;
		*outLen = pApp->m_nSavedStateSize;
		pApp->m_pSavedState = NULL;
		pApp->m_nSavedStateSize = 0;
	}

	pthread_mutex_unlock(&pApp->m_mutex);
	return pSavedState;
	*/
	return nullptr;
}

void FXDAndroidApp2::_on_low_memory(ANativeActivity* pActivity)
{
	PRINT_WARN << "App: LowMemory -- " << pActivity;
	//android_app_write_cmd((struct FXDAndroidApp2*)pActivity->instance, APP_CMD_LOW_MEMORY);
}

void FXDAndroidApp2::_on_configuration_changed(ANativeActivity* pActivity)
{
	PRINT_WARN << "App: ConfigurationChanged -- " << pActivity;
	//android_app_write_cmd((struct FXDAndroidApp2*)pActivity->instance, APP_CMD_CONFIG_CHANGED);
}

void FXDAndroidApp2::_on_window_focus_changed(ANativeActivity* pActivity, FXD::S32 nFocused)
{
	PRINT_WARN << "App: WindowFocusChanged -- " << pActivity;
	//android_app_write_cmd((struct FXDAndroidApp2*)pActivity->instance, focused ? APP_CMD_GAINED_FOCUS : APP_CMD_LOST_FOCUS);
}

void FXDAndroidApp2::_on_native_window_created(ANativeActivity* pActivity, ANativeWindow* pWindow)
{
	PRINT_INFO << "App: NativeWindowCreated -- " << pActivity;
	//android_app_set_window((struct FXDAndroidApp2*)pActivity->instance, pWindow);
}

void FXDAndroidApp2::_on_native_window_destroyed(ANativeActivity* pActivity, ANativeWindow* pWindow)
{
	PRINT_INFO << "App: NativeWindowDestroyed -- " << pActivity;
	//android_app_set_window((struct FXDAndroidApp2*)pActivity->instance, NULL);
}

void FXDAndroidApp2::_on_input_queue_created(ANativeActivity* pActivity, AInputQueue* pQueue)
{
	PRINT_INFO << "App: InputQueueCreated -- " << pActivity;
	//android_app_set_input((struct FXDAndroidApp2*)pActivity->instance, pQueue);
}

void FXDAndroidApp2::_on_input_queue_destroyed(ANativeActivity* pActivity, AInputQueue* pQueue)
{
	PRINT_INFO << "App: InputQueueDestroyed -- " << pActivity;
	//android_app_set_input((struct FXDAndroidApp2*)pActivity->instance, NULL);
}

/*
static void free_saved_state(struct FXDAndroidApp* pApp)
{
	pthread_mutex_lock(&pApp->m_mutex);
	if (pApp->m_pSavedState != NULL)
	{
		free(pApp->m_pSavedState);
		pApp->m_pSavedState = NULL;
		pApp->m_nSavedStateSize = 0;
	}
	pthread_mutex_unlock(&pApp->m_mutex);
}

int8_t android_app_read_cmd(struct FXDAndroidApp* pApp)
{
	int8_t cmd;
	if (read(pApp->msgread, &cmd, sizeof(cmd)) == sizeof(cmd))
	{
		switch (cmd)
		{
			case APP_CMD_SAVE_STATE:
			{	
				free_saved_state(pApp);
				break;
			}
		}
		return cmd;
	}
	else
	{
		LOGE("No data on command pipe!");
	}
	return -1;
}

static void print_cur_config(struct FXDAndroidApp* pApp)
{
	char lang[2];
	char country[2];
	AConfiguration_getLanguage(pApp->m_pConfig, lang);
	AConfiguration_getCountry(pApp->m_pConfig, country);

	LOGV("Config: mcc=%d mnc=%d lang=%c%c cnt=%c%c orien=%d touch=%d dens=%d "
		"keys=%d nav=%d keysHid=%d navHid=%d sdk=%d size=%d long=%d "
		"modetype=%d modenight=%d",
		AConfiguration_getMcc(pApp->m_pConfig),
		AConfiguration_getMnc(pApp->m_pConfig),
		lang[0], lang[1], country[0], country[1],
		AConfiguration_getOrientation(pApp->m_pConfig),
		AConfiguration_getTouchscreen(pApp->m_pConfig),
		AConfiguration_getDensity(pApp->m_pConfig),
		AConfiguration_getKeyboard(pApp->m_pConfig),
		AConfiguration_getNavigation(pApp->m_pConfig),
		AConfiguration_getKeysHidden(pApp->m_pConfig),
		AConfiguration_getNavHidden(pApp->m_pConfig),
		AConfiguration_getSdkVersion(pApp->m_pConfig),
		AConfiguration_getScreenSize(pApp->m_pConfig),
		AConfiguration_getScreenLong(pApp->m_pConfig),
		AConfiguration_getUiModeType(pApp->m_pConfig),
		AConfiguration_getUiModeNight(pApp->m_pConfig));
}

void android_app_pre_exec_cmd(struct FXDAndroidApp* pApp, int8_t cmd)
{
	switch (cmd)
	{
		case APP_CMD_INPUT_CHANGED:
		{
			LOGV("APP_CMD_INPUT_CHANGED\n");
			pthread_mutex_lock(&pApp->m_mutex);
			if (pApp->m_pInputQueue != NULL)
			{
				AInputQueue_detachLooper(pApp->m_pInputQueue);
			}
			pApp->m_pInputQueue = pApp->m_pPendingInputQueue;
			if (pApp->m_pInputQueue != NULL)
			{
				LOGV("Attaching input queue to looper");
			//	AInputQueue_attachLooper(pApp->m_pInputQueue, pApp->m_pLooper, LOOPER_ID_INPUT, NULL, &pApp->m_inputPollSource);
			}
			pthread_cond_broadcast(&pApp->cond);
			pthread_mutex_unlock(&pApp->m_mutex);
			break;
		}
		case APP_CMD_INIT_WINDOW:
		{
			LOGV("APP_CMD_INIT_WINDOW\n");
			pthread_mutex_lock(&pApp->m_mutex);
			pApp->m_pWindow = pApp->pendingWindow;
			pthread_cond_broadcast(&pApp->cond);
			pthread_mutex_unlock(&pApp->m_mutex);
			break;
		}
		case APP_CMD_TERM_WINDOW:
		{
			LOGV("APP_CMD_TERM_WINDOW\n");
			pthread_cond_broadcast(&pApp->cond);
			break;
		}
		case APP_CMD_RESUME:
		case APP_CMD_START:
		case APP_CMD_PAUSE:
		case APP_CMD_STOP:
		{
			LOGV("activityState=%d\n", cmd);
			pthread_mutex_lock(&pApp->m_mutex);
			pApp->activityState = cmd;
			pthread_cond_broadcast(&pApp->cond);
			pthread_mutex_unlock(&pApp->m_mutex);
			break;
		}
		case APP_CMD_CONFIG_CHANGED:
		{
			LOGV("APP_CMD_CONFIG_CHANGED\n");
			AConfiguration_fromAssetManager(pApp->m_pConfig, pApp->m_pActivity->assetManager);
			print_cur_config(pApp);
			break;
		}
		case APP_CMD_DESTROY:
		{
			LOGV("APP_CMD_DESTROY\n");
			pApp->m_nDestroyRequested = 1;
			break;
		}
	}
}

void android_app_post_exec_cmd(struct FXDAndroidApp* pApp, int8_t cmd)
{
	switch (cmd)
	{
		case APP_CMD_TERM_WINDOW:
		{
			LOGV("APP_CMD_TERM_WINDOW\n");
			pthread_mutex_lock(&pApp->m_mutex);
			pApp->m_pWindow = NULL;
			pthread_cond_broadcast(&pApp->cond);
			pthread_mutex_unlock(&pApp->m_mutex);
			break;
		}
		case APP_CMD_SAVE_STATE:
		{
			LOGV("APP_CMD_SAVE_STATE\n");
			pthread_mutex_lock(&pApp->m_mutex);
			pApp->stateSaved = 1;
			pthread_cond_broadcast(&pApp->cond);
			pthread_mutex_unlock(&pApp->m_mutex);
			break;
		}
		case APP_CMD_RESUME:
		{
			free_saved_state(pApp);
			break;
		}
	}
}

static void android_app_destroy(struct FXDAndroidApp* pApp)
{
	LOGV("android_app_destroy!");

	free_saved_state(pApp);
	pthread_mutex_lock(&pApp->m_mutex);
	if (pApp->m_pInputQueue != NULL)
	{
		AInputQueue_detachLooper(pApp->m_pInputQueue);
	}
	AConfiguration_delete(pApp->m_pConfig);
	pApp->m_nDestroyed = 1;
	pthread_cond_broadcast(&pApp->cond);
	pthread_mutex_unlock(&pApp->m_mutex);
	// Can't touch pApp object after this.
}

static void process_input(struct FXDAndroidApp* pApp, struct FXDAndroidPollSource* pPollSource)
{
	AInputEvent* pEvent = NULL;
	while (AInputQueue_getEvent(pApp->m_pInputQueue, &pEvent) >= 0)
	{
		LOGV("New input event: type=%d\n", AInputEvent_getType(pEvent));
		if (AInputQueue_preDispatchEvent(pApp->m_pInputQueue, pEvent))
		{
			continue;
		}
		int32_t handled = 0;
		if (pApp->onInputEvent != NULL)
		{
			handled = pApp->onInputEvent(pApp, pEvent);
		}
		AInputQueue_finishEvent(pApp->m_pInputQueue, pEvent, handled);
	}
}

static void process_cmd(struct FXDAndroidApp* pApp, struct FXDAndroidPollSource* pPollSource)
{
	int8_t cmd = android_app_read_cmd(pApp);
	android_app_pre_exec_cmd(pApp, cmd);
	if (pApp->onAppCmd != NULL)
	{
		pApp->onAppCmd(pApp, cmd);
	}
	android_app_post_exec_cmd(pApp, cmd);
}

static void* android_app_entry(void* pParam)
{
	struct FXDAndroidApp* pApp = (struct FXDAndroidApp*)pParam;

	pApp->m_pConfig = AConfiguration_new();
	AConfiguration_fromAssetManager(pApp->m_pConfig, pApp->m_pActivity->assetManager);

	print_cur_config(pApp);

	//pApp->m_cmdPollSource.m_nID = LOOPER_ID_MAIN;
	//pApp->m_cmdPollSource.m_pApp = pApp;
	//pApp->m_cmdPollSource.process = process_cmd;

	//pApp->m_inputPollSource.m_nID = LOOPER_ID_INPUT;
	//pApp->m_inputPollSource.m_pApp = pApp;
	//pApp->m_inputPollSource.process = process_input;

	//ALooper* pLooper = ALooper_prepare(ALOOPER_PREPARE_ALLOW_NON_CALLBACKS);
	//ALooper_addFd(pLooper, pApp->msgread, LOOPER_ID_MAIN, ALOOPER_EVENT_INPUT, NULL, &pApp->m_cmdPollSource);
	//pApp->m_pLooper = pLooper;

	pthread_mutex_lock(&pApp->m_mutex);
	pApp->m_nRunning = 1;
	pthread_cond_broadcast(&pApp->cond);
	pthread_mutex_unlock(&pApp->m_mutex);

	android_main(pApp);

	android_app_destroy(pApp);
	return NULL;
}

// --------------------------------------------------------------------
// Native activity interaction (called from main thread)
// --------------------------------------------------------------------
static struct FXDAndroidApp* android_app_create(ANativeActivity* pActivity, void* pSavedState, size_t nSavedStateSize)
{
	struct FXDAndroidApp* pApp = (struct FXDAndroidApp*)malloc(sizeof(struct FXDAndroidApp));
	memset(pApp, 0, sizeof(struct FXDAndroidApp));

	pApp->m_pActivity = pActivity;
	pthread_mutex_init(&pApp->m_mutex, NULL);
	pthread_cond_init(&pApp->cond, NULL);

	if (pSavedState != NULL)
	{
		pApp->m_pSavedState = malloc(nSavedStateSize);
		pApp->m_nSavedStateSize = nSavedStateSize;
		memcpy(pApp->m_pSavedState, pSavedState, nSavedStateSize);
	}

	int msgpipe[2];
	if (pipe(msgpipe))
	{
		LOGE("could not create pipe: %s", strerror(errno));
		return NULL;
	}
	pApp->msgread = msgpipe[0];
	pApp->msgwrite = msgpipe[1];

	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	pthread_create(&pApp->m_thread, &attr, android_app_entry, pApp);

	// Wait for thread to start.
	pthread_mutex_lock(&pApp->m_mutex);
	while (pApp->m_nRunning == 0)
	{
		pthread_cond_wait(&pApp->cond, &pApp->m_mutex);
	}
	pthread_mutex_unlock(&pApp->m_mutex);

	return pApp;
}

static void android_app_write_cmd(struct FXDAndroidApp* pApp, int8_t cmd)
{
	if (write(pApp->msgwrite, &cmd, sizeof(cmd)) != sizeof(cmd))
	{
		LOGE("Failure writing FXDAndroidApp cmd: %s\n", strerror(errno));
	}
}

static void android_app_set_input(struct FXDAndroidApp* pApp, AInputQueue* pQueue)
{
	pthread_mutex_lock(&pApp->m_mutex);
	pApp->m_pPendingInputQueue = pQueue;
	android_app_write_cmd(pApp, APP_CMD_INPUT_CHANGED);
	while (pApp->m_pInputQueue != pApp->m_pPendingInputQueue)
	{
		pthread_cond_wait(&pApp->cond, &pApp->m_mutex);
	}
	pthread_mutex_unlock(&pApp->m_mutex);
}

static void android_app_set_window(struct FXDAndroidApp* pApp, ANativeWindow* pWindow)
{
	pthread_mutex_lock(&pApp->m_mutex);
	if (pApp->pendingWindow != NULL)
	{
		android_app_write_cmd(pApp, APP_CMD_TERM_WINDOW);
	}
	pApp->pendingWindow = pWindow;
	if (pWindow != NULL)
	{
		android_app_write_cmd(pApp, APP_CMD_INIT_WINDOW);
	}
	while (pApp->m_pWindow != pApp->pendingWindow)
	{
		pthread_cond_wait(&pApp->cond, &pApp->m_mutex);
	}
	pthread_mutex_unlock(&pApp->m_mutex);
}

static void android_app_set_activity_state(struct FXDAndroidApp* pApp, int8_t cmd)
{
	pthread_mutex_lock(&pApp->m_mutex);
	android_app_write_cmd(pApp, cmd);
	while (pApp->activityState != cmd)
	{
		pthread_cond_wait(&pApp->cond, &pApp->m_mutex);
	}
	pthread_mutex_unlock(&pApp->m_mutex);
}

static void android_app_free(struct FXDAndroidApp* pApp)
{
	pthread_mutex_lock(&pApp->m_mutex);
	android_app_write_cmd(pApp, APP_CMD_DESTROY);
	while (pApp->m_nDestroyed == 0)
	{
		pthread_cond_wait(&pApp->cond, &pApp->m_mutex);
	}
	pthread_mutex_unlock(&pApp->m_mutex);

	close(pApp->msgread);
	close(pApp->msgwrite);
	pthread_cond_destroy(&pApp->cond);
	pthread_mutex_destroy(&pApp->m_mutex);
	free(pApp);
}
*/
#endif //IsOSAndroid()