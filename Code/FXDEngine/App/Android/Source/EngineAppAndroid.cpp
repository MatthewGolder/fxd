// Creator - MatthewGolder
#include "FXDEngine/App/Types.h"

#if IsOSAndroid()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/App/Android/EngineAppAndroid.h"

using namespace FXD;
using namespace App;

// ------
// IEngineApp
// -
// ------
IEngineApp::IEngineApp(void)
	: m_eGameState(App::E_GameState::Unknown)
{
}

IEngineApp::~IEngineApp(void)
{
}

void IEngineApp::trigger_shutdown(void)
{
	if ((!m_impl->m_bTriggerShutdown) && (!m_impl->m_bAppShuttingDown))
	{
		m_impl->m_bTriggerShutdown = true;
	}
}

bool IEngineApp::is_shuttingdown(void) const
{
	return (m_impl->m_bAppShuttingDown);
}

bool IEngineApp::is_running(void) const
{
	return (m_eGameState == E_GameState::Running);
}
#endif //IsOSAndroid()