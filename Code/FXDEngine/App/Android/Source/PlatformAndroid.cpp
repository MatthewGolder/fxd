// Creator - MatthewGolder
#include "FXDEngine/App/Types.h"

#if IsOSAndroid()
#include "FXDEngine/App/EngineApp.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/App/Android/PlatformAndroid.h"
#include "FXDEngine/App/Android/GlobalsAndroid.h"
#include "FXDEngine/GFX/Viewport.h"
#include "FXDEngine/IO/IODevice.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOSystem.h"
#include "FXDEngine/IO/Path.h"
#include "FXDEngine/Job/EventHandler.h"

using namespace FXD;
using namespace App;

// ------
// IPlatform::Impl::Impl
// -
// ------
IPlatform::Impl::Impl::Impl(void)
{
}

IPlatform::Impl::~Impl(void)
{
}

bool IPlatform::Impl::create_platform(void)
{
	return true;
}

bool IPlatform::Impl::release_platform(void)
{
	return true;
}


// 0 - Not Initialised
// 1 - Initialised
// 2 - ShutDown
FXD::S32 nAppStatus = 0;
// ------
// IPlatform
// -
// ------
IPlatform::IPlatform(void)
{
	if (nAppStatus == 2)
	{
		PRINT_ASSERT << "App: Accessed after shutdown";
	}
}

IPlatform::~IPlatform(void)
{
}

bool IPlatform::create_platform(FXD::S32 argc, FXD::UTF8* argv[])
{
	bool bRetVal = m_impl->create_platform();
	m_cmdArgs.set_args(argc, argv);
	nAppStatus = 1;
	return bRetVal;
}

bool IPlatform::release_platform(void)
{
	bool bRetVal = m_impl->release_platform();
	nAppStatus = 2;
	return bRetVal;
}

bool IPlatform::load(void)
{
	if (m_dataRegistry)
	{
		return true;
	}
	else
	{
		// Load the data.
		m_dataRegistry = std::make_shared< IO::IDataRegistry >();
		IO::Path ioPath = IO::FS::GetUtilityDirectory(UTF_8("platform.save"));
		bool bResult = FXDIO()->io_system()->io_device()->load(m_dataRegistry.get(), ioPath, true, { }).get();
		return bResult;
	}
}

bool IPlatform::save(void)
{
	if (!m_dataRegistry)
	{
		return false;
	}
	else
	{
		// Save the data.
		IO::Path ioPath = IO::FS::GetUtilityDirectory(UTF_8("platform.save"));
		bool bResult = FXDIO()->io_system()->io_device()->save(m_dataRegistry.get(), ioPath, false, { }).get();
		return bResult;
	}
}
#endif //IsOSAndroid()