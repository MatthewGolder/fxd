// Creator - MatthewGolder
#include "FXDEngine/App/Types.h"

#if IsOSAndroid()
#include "FXDEngine/App/Android/GlobalsAndroid.h"
#include "FXDEngine/App/Platform.h"

using namespace FXD;
using namespace App;

// ------
// GlobalData
// -
// ------
GlobalData::GlobalData(FXD::S32 nAppID, bool bAllowPlayer1MultipleDevices, ANativeActivity* pActivity)
	: m_nAppID(nAppID)
	, m_bAllowPlayer1MultipleDevices(bAllowPlayer1MultipleDevices)
	, m_pActivity(pActivity)
{
}
GlobalData::~GlobalData(void)
{
}

/*
jmethodID GlobalData::Shared::getMethodID(JNIEnv* pEnv, jobject* pJavaObject, const FXD::UTF8* pMethodName, const FXD::UTF8* pParameterList)
{
	jclass jClass = pEnv->GetObjectClass(*(pJavaObject));
	jmethodID jMethodToCall = pEnv->GetMethodID(jClass, pMethodName, pParameterList);
	if (jMethodToCall == 0)
	{
		PRINT_ASSERT << pMethodName << " not found!";
	}
	return jMethodToCall;
}
*/
#endif //IsOSAndroid()