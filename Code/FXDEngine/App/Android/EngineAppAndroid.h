// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_APP_ANRDOID_ENGINEAPPANRDOID_H
#define FXDENGINE_APP_ANRDOID_ENGINEAPPANRDOID_H

#include "FXDEngine/Accounts/Types.h"

#if IsOSAndroid()
#include "FXDEngine/App/EngineApp.h"
#include "FXDEngine/Core/Android/Android.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< App::IEngineApp >
			// -
			// ------
			template <>
			class Impl< App::IEngineApp >
			{
			public:
				friend class App::IEngineApp;

			public:
				Impl(void)
					: m_bTriggerShutdown(false)
					, m_bAppShuttingDown(false)
				{}
				~Impl(void)
				{}

				void trigger_shutdown_actual(void)
				{
					m_bAppShuttingDown = true;
				}

			private:
				bool m_bTriggerShutdown;
				bool m_bAppShuttingDown;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsOSAndroid()
#endif //FXDENGINE_APP_ANRDOID_ENGINEAPPANRDOID_H
