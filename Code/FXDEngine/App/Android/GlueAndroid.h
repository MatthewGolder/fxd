// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_APP_ANDROID_GLUEANDROID_H
#define FXDENGINE_APP_ANDROID_GLUEANDROID_H

#include "FXDEngine/App/Types.h"

#if IsOSAndroid()
#include "FXDEngine/Thread/Event.h"
#include "FXDEngine/Thread/Thread.h"
#include <android/configuration.h>
#include <android/looper.h>
#include <android/native_activity.h>

namespace FXD
{
	namespace App
	{
		class _FXDThread : public Thread::IThread
		{
		public:
			_FXDThread(const FXD::UTF8* pName);
			~_FXDThread(void);

			bool thread_init_func(void) FINAL;
			bool thread_process_func(void) FINAL;
			bool thread_shutdown_func(void) FINAL;
		};


		struct FXDAndroidApp2
		{
			FXDAndroidApp2(ANativeActivity* pActivity, void* pSavedState, size_t nSavedStateSize);
			~FXDAndroidApp2(void);

		private:
			static void _on_start(ANativeActivity* pActivity);
			static void _on_destroy(ANativeActivity* pActivity);
			static void _on_resume(ANativeActivity* pActivity);
			static void _on_pause(ANativeActivity* pActivity);
			static void _on_stop(ANativeActivity* pActivity);
			static void _on_low_memory(ANativeActivity* pActivity);
			static void _on_configuration_changed(ANativeActivity* pActivity);
			static void _on_window_focus_changed(ANativeActivity* pActivity, FXD::S32 nFocused);
			static void _on_native_window_created(ANativeActivity* pActivity, ANativeWindow* pWindow);
			static void _on_native_window_destroyed(ANativeActivity* pActivity, ANativeWindow* pWindow);
			static void _on_input_queue_created(ANativeActivity* pActivity, AInputQueue* pQueue);
			static void _on_input_queue_destroyed(ANativeActivity* pActivity, AInputQueue* pQueue);
			static void* _on_save_instance_state(ANativeActivity* pActivity, size_t* pNLen);

		private:
			FXD::Thread::Event m_event;
			FXD::App::_FXDThread m_fxdThread;

			ANativeActivity* m_pActivity;
			void* m_pSavedState;
			size_t m_nSavedStateSize;
			bool m_bRunning;

		};
		/*
	#ifdef __cplusplus
	extern "C"
	{
	#endif

		#	define JAVA_TOUCH_MAX 8

		struct FXDAndroidApp;
		struct FXDAndroidPollSource;
		struct FXDAndroidTouchData;

		// Data associated with an ALooper fd that will be returned as the "outData" when that source has data ready.
		struct FXDAndroidPollSource
		{
			void(*process)(struct FXDAndroidApp* pApp, struct FXDAndroidPollSource* pSource);	// Function to call to perform the standard processing of data from this source.

			int32_t m_nID;																								// The identifier of this source. May be LOOPER_ID_MAIN or LOOPER_ID_INPUT.
			struct FXDAndroidApp* m_pApp;																			// The FXDAndroidApp this ident is associated with.
		};
		struct FXDAndroidTouchData
		{
			int m_nativeIsTouching;
			float m_nativeTouchX;
			float m_nativeTouchY;
			float m_nativeTouchPresure;
			int m_nativeTouchUpdateCount;
		};

		// This is the interface for the standard glue code of a threaded application.
		// In this model, the application's code is running in its own thread separate from the main thread of the process.
		// It is not required that this thread be associated with the Java VM, although it will need to be in order to make JNI calls any Java objects.
		struct FXDAndroidApp
		{
			// Fill this in with the function to process main app commands (APP_CMD_*)
			void(*onAppCmd)(struct FXDAndroidApp* pApp, int32_t cmd);

			// Fill this in with the function to process input events. At this point  the event has already been pre-dispatched, and it will be finished upon
			// return. Return 1 if you have handled the event, 0 for any default dispatching.
			int32_t(*onInputEvent)(struct FXDAndroidApp* pApp, AInputEvent* pEvent);

			// The application can place a pointer to its own state object here if it likes.
			void* m_pUserData;

			// The ANativeActivity object instance that this app is running in.
			ANativeActivity* m_pActivity;

			// The current configuration the app is running in.
			AConfiguration* m_pConfig;

			// This is the last instance's saved state, as provided at creation time.
			// It is NULL if there was no state.  You can use this as you need; the
			// memory will remain around until you call android_app_exec_cmd() for
			// APP_CMD_RESUME, at which point it will be freed and savedState set to NULL.
			// These variables should only be changed when processing a APP_CMD_SAVE_STATE,
			// at which point they will be initialized to NULL and you can malloc your
			// state and place the information here.  In that case the memory will be
			// freed for you later.
			void* m_pSavedState;
			size_t m_nSavedStateSize;

			//ALooper* m_pLooper;								// The ALooper associated with the app's thread.
			AInputQueue* m_pInputQueue;					// When non-NULL, this is the input queue from which the app will receive user input events.
			ANativeWindow* m_pWindow;						// When non-NULL, this is the window surface that the app can draw in.

			// Current content rectangle of the window; this is the area where the window's content should be placed to be seen by the user.
			ARect contentRect;

			// Current state of the app's activity. May be either APP_CMD_START, APP_CMD_RESUME, APP_CMD_PAUSE, or APP_CMD_STOP; see below.
			int activityState;

			// -------------------------------------------------
			// Below are "private" implementation of the glue code.
			pthread_mutex_t m_mutex;
			pthread_cond_t cond;
			pthread_t m_thread;

			int msgread;
			int msgwrite;

			//struct FXDAndroidPollSource m_cmdPollSource;
			//struct FXDAndroidPollSource m_inputPollSource;

			int m_nRunning;
			int stateSaved;
			int m_nDestroyed;
			int m_nDestroyRequested; // This is non-zero when the application's NativeActivity is being  destroyed and waiting for the app thread to complete.
			int redrawNeeded;
			AInputQueue* m_pPendingInputQueue;
			ANativeWindow* pendingWindow;
			ARect pendingContentRect;

			struct FXDAndroidTouchData m_nativeTouchData[JAVA_TOUCH_MAX];
		};

		enum
		{
			// Looper data ID of commands coming from the app's main thread, which is returned as an identifier from ALooper_pollOnce().
			// The data for this identifier is a pointer to an FXDAndroidPollSource structure.
			// These can be retrieved and processed with android_app_read_cmd() and android_app_exec_cmd().
			LOOPER_ID_MAIN = 1,

			// Looper data ID of events coming from the AInputQueue of the application's window, which is returned as an identifier from ALooper_pollOnce().
			// The data for this identifier is a pointer to an FXDAndroidPollSource structure. These can be read via the inputQueue object of FXDAndroidApp.
			LOOPER_ID_INPUT = 2,

			// Start of user-defined ALooper identifiers.
			LOOPER_ID_USER = 3,
		};

		enum
		{
			// Command from main thread: the AInputQueue has changed.  Upon processing this command, FXDAndroidApp->inputQueue will be updated to the new queue (or NULL).
			APP_CMD_INPUT_CHANGED,

			// Command from main thread: a new ANativeWindow is ready for use. Upon receiving this command, FXDAndroidApp->window will contain the new window surface.
			APP_CMD_INIT_WINDOW,

			// Command from main thread: the existing ANativeWindow needs to be
			// terminated.  Upon receiving this command, FXDAndroidApp->window still
			// contains the existing window; after calling android_app_exec_cmd
			// it will be set to NULL.
			APP_CMD_TERM_WINDOW,

			// Command from main thread: the current ANativeWindow has been resized. Please redraw with its new size.
			APP_CMD_WINDOW_RESIZED,

			// Command from main thread: the system needs that the current ANativeWindow
			// be redrawn.  You should redraw the window before handing this to
			// android_app_exec_cmd() in order to avoid transient drawing glitches.
			APP_CMD_WINDOW_REDRAW_NEEDED,

			// Command from main thread: the content area of the window has changed,
			// such as from the soft input window being shown or hidden. You can
			// find the new content rect in FXDAndroidApp::contentRect.
			APP_CMD_CONTENT_RECT_CHANGED,

			// Command from main thread: the app's activity window has gaine input focus.
			APP_CMD_GAINED_FOCUS,

			// Command from main thread: the app's activity window has lost input focus.
			APP_CMD_LOST_FOCUS,

			// Command from main thread: the current device configuration has changed.
			APP_CMD_CONFIG_CHANGED,

			// Command from main thread: the system is running low on memor Try to reduce your memory use.
			APP_CMD_LOW_MEMORY,

			// Command from main thread: the app's activity has been started.
			APP_CMD_START,

			// Command from main thread: the app's activity has been resumed.
			APP_CMD_RESUME,

			// Command from main thread: the app should generate a new saved state
			// for itself, to restore from later if needed.  If you have saved state,
			// allocate it with malloc and place it in FXDAndroidApp.savedState with
			// the size in FXDAndroidApp.savedStateSize. The will be freed for you later.
			APP_CMD_SAVE_STATE,

			// Command from main thread: the app's activity has been paused.
			APP_CMD_PAUSE,

			// Command from main thread: the app's activity has been stopped.
			APP_CMD_STOP,

			// Command from main thread: the app's activity is being destroyed, and waiting for the app thread to clean up and exit before proceeding.
			APP_CMD_DESTROY,
		};

		// Call when ALooper_pollAll() returns LOOPER_ID_MAIN, reading the next app command message.
		int8_t android_app_read_cmd(struct FXDAndroidApp* pApp);

		// Call with the command returned by android_app_read_cmd() to do the  initial pre-processing of the given command.
		// You can perform your own actions for the command after calling this function.
		void android_app_pre_exec_cmd(struct FXDAndroidApp* pApp, int8_t cmd);

		// Call with the command returned by android_app_read_cmd() to do the final post-processing of the given command.
		// You must have done your own actions for the command before calling this function.
		void android_app_post_exec_cmd(struct FXDAndroidApp* pApp, int8_t cmd);

		// This is the function that application code must implement, representin the main entry to the app.
		extern void android_main(struct FXDAndroidApp* pApp);

	#ifdef __cplusplus
	}
	#endif
		*/

	} //namespace App
} //namespace FXD
#endif //IsOSAndroid()
#endif //FXDENGINE_APP_ANDROID_GLUEANDROID_H