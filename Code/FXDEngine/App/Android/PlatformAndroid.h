// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_APP_ANDROID_PLATFORMANDROID_H
#define FXDENGINE_APP_ANDROID_PLATFORMANDROID_H

#include "FXDEngine/Accounts/Types.h"

#if IsOSAndroid()
#include "FXDEngine/App/Platform.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< App::IPlatform >
			// -
			// ------
			template <>
			class Impl< App::IPlatform > FINAL
			{
			public:
				friend class App::IPlatform;

			public:
				Impl(void);
				~Impl(void);

				bool create_platform(void);
				bool release_platform(void);
			};
		} //namespace Impl
	} //namespace Core

	namespace App
	{
		//static void run_test();
	} //namespace App
} //namespace FXD
#endif //IsOSAndroid()
#endif //FXDENGINE_APP_ANDROID_PLATFORMANDROID_H