// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_APP_ANDROID_GLOBALSANDROID_H
#define FXDENGINE_APP_ANDROID_GLOBALSANDROID_H

#include "FXDEngine/Accounts/Types.h"

#if IsOSAndroid()
#include "FXDEngine/Core/NonCopyable.h"
#include <android/native_activity.h>

namespace FXD
{
	namespace App
	{
		// ------
		// GlobalData
		// -
		// ------
		class GlobalData
		{
		public:
			GlobalData(FXD::S32 nAppID, bool bAllowPlayer1MultipleDevices, ANativeActivity* pActivity);
			~GlobalData(void);

		public:
			const FXD::S32 m_nAppID;
			const bool m_bAllowPlayer1MultipleDevices;
			ANativeActivity* m_pActivity;
		};
	} //namespace App
} //namespace FXD
#endif //IsOSAndroid()
#endif //FXDENGINE_APP_ANDROID_GLOBALSANDROID_H