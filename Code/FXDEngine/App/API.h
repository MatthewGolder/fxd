// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_APP_API_H
#define FXDENGINE_APP_API_H

#include "FXDEngine/App/Adapter.h"
#include "FXDEngine/Container/Map.h"

namespace FXD
{
	namespace App
	{
		// ------
		// IApi
		// -
		// ------
		class IApi : public Core::NonCopyable
		{
		public:
			IApi(App::E_Api eApi);
			virtual ~IApi(void);

			virtual bool create_api(void) PURE;
			virtual bool release_api(void) PURE;
			
			App::E_Api api_type(void) { return m_eApi; }
			App::AdapterDesc find_default_adapter(void) const;
			App::AdapterDesc find_adapter(const Core::String8& strName) const;

		protected:
			virtual void _create_adapter_list(void) PURE;

		protected:
			App::E_Api m_eApi;
			Container::Map< Core::String8, App::AdapterDesc > m_adapterDescs;
		};

	} //namespace App
} //namespace FXD
#endif //FXDENGINE_APP_API_H