// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_APP_PLATFORM_H
#define FXDENGINE_APP_PLATFORM_H

#include "FXDEngine/Container/Any.h"
#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/Core/CmdArgs.h"
#include "FXDEngine/IO/DataRegistry.h"

namespace FXD
{
	namespace App
	{
		// ------
		// GlobalAnys
		// -
		// ------
		using GlobalAnys = Container::Map< Core::String8, Container::Any >;

		// ------
		// IPlatform
		// -
		// ------
		class IPlatform : public Core::HasImpl< App::IPlatform, 192 >, public Core::NonCopyable
		{
		public:
			friend class Core::Impl::Impl< IPlatform >;
	
		public:
			IPlatform(void);
			virtual ~IPlatform(void);

			bool create_platform(FXD::S32 nArgc, FXD::UTF8* argv[]);
			bool release_platform(void);
			bool load(void);
			bool save(void);

			GET_REF_W(Core::CmdArgs, cmdArgs, cmd_args);
			GET_REF_W(App::GlobalAnys, globals, globals);
			GET_REF_W(IO::DataRegistry, dataRegistry, data_registry);

		private:
			Core::CmdArgs m_cmdArgs;
			App::GlobalAnys m_globals;
			IO::DataRegistry m_dataRegistry;
		};

		class GlobalData;

		// Game provided functions.
		extern Core::String8 GetGameNameUTF8(void);
		extern Core::String16 GetGameNameUTF16(void);
		extern Core::String8 GetDisplayGameNameUTF8(void);
		extern Core::String16 GetDisplayGameNameUTF16(void);
		extern Core::String8 GetShortNameUTF8(void);
		extern Core::String16 GetShortNameUTF16(void);
		extern Core::String16 GetShortNameUTF16(void);
		extern App::GlobalData& GetGlobalData(void);
		extern FXD::U64 GetRandSeed(void);
	} //namespace App

} //namespace FXD
#endif //FXDENGINE_APP_PLATFORM_H