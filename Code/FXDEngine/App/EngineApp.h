// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_APP_ENGINEAPP_H
#define FXDENGINE_APP_ENGINEAPP_H

#include "FXDEngine/App/DisplayLayer.h"
#include "FXDEngine/Core/FPS.h"
#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/Core/Timer.h"

namespace FXD
{
	namespace App
	{
		// ------
		// E_GameState
		// - 
		// ------
		enum class E_GameState
		{
			Init,
			Running,
			Stopped,
			Count,
			Unknown = 0xffff
		};

		// ------
		// IEngineApp
		// - 
		// ------
		class IEngineApp : public Core::HasImpl< IEngineApp, 32 >, public GFX::IGfxFrameItem
		{
		public:
			IEngineApp(void);
			virtual ~IEngineApp(void);

			bool init_game(FXD::S32 argc, FXD::UTF8* argv[], App::E_ApiFlags eFlags);
			virtual void run_game(void);
			void shutdown_game(void);

			virtual void load(void);
			virtual void unload(void);
			void update(void);
			void render(const GFX::IViewport* pDisplay) const;
			void on_resize(void);

			void trigger_shutdown(void);
			bool is_shuttingdown(void) const;
			bool is_running(void) const;

		protected:
			Core::FPS m_fps;
			Core::Timer m_timer;
			App::DisplayEngine m_displayEngine;
			mutable App::E_GameState m_eGameState;
		};

	} //namespace App
} //namespace FXD
#endif //FXDENGINE_APP_ENGINEAPP_H