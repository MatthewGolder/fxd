// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_APP_ADAPTER_H
#define FXDENGINE_APP_ADAPTER_H

#include "FXDEngine/App/Types.h"
#include "FXDEngine/Core/String.h"
#include "FXDEngine/Job/Future.h"

namespace FXD
{
	namespace App
	{
		using DeviceIndex = FXD::S32;

		// ------
		// IAdapterDesc
		// - 
		// ------
		class IAdapterDesc : public Core::NonCopyable
		{
		public:
			IAdapterDesc(const App::DeviceIndex nDeviceID)
				: m_bDefault(false)
				, m_nDeviceID(nDeviceID)
			{}
			virtual ~IAdapterDesc(void)
			{}

			SET(bool, bDefault, isDefault);
			SET(App::DeviceIndex, nDeviceID, deviceID);
			SET_REF(Core::String8, strDriver, driver);
			SET_REF(Core::String8, strName, name);

			GET_R(bool, bDefault, isDefault);
			GET_R(App::DeviceIndex, nDeviceID, deviceID);
			GET_REF_R(Core::String8, strDriver, driver);
			GET_REF_R(Core::String8, strName, name);

		protected:
			bool m_bDefault;
			App::DeviceIndex m_nDeviceID;
			Core::String8 m_strName;
			Core::String8 m_strDriver;
		};

		// ------
		// IAdapter
		// -
		// ------
		class IAdapter : public Core::NonCopyable
		{
		public:
			IAdapter(const App::AdapterDesc& adapterDesc, const App::IApi* pApi);
			virtual ~IAdapter(void);

			virtual auto create(void)->Job::Future< bool > PURE;
			virtual auto release(void)->Job::Future< bool > PURE;

			GET_REF_R(App::AdapterDesc, adapterDesc, adapter_desc);
			GET_R(App::IApi*, pApi, api);

		protected:
			virtual void _update(FXD::F32 dt) PURE;

		protected:			
			App::AdapterDesc m_adapterDesc;
			const App::IApi* m_pApi;
		};

	} //namespace App
} //namespace FXD
#endif //FXDENGINE_APP_ADAPTER_H