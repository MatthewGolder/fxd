// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FENCE_H
#define FXDENGINE_GFX_FENCE_H

#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/GFX/Types.h"
#include "FXDEngine/Job/Future.h"

namespace FXD
{
	namespace GFX
	{
		// ------
		// IFence
		// -
		// ------
		class IFence : public Core::HasImpl< GFX::IFence, 64 >, public Core::NonCopyable
		{
		public:
			IFence(GFX::IGfxAdapter* pAdapter);
			virtual ~IFence(void);

			Job::Future< bool > create(bool bSignaled);
			Job::Future< bool > release(void);

			bool is_signalled(void) const;
			bool wait(void);
			bool reset(void);

		protected:
			bool _create(bool bSignaled);
			bool _release(void);

			bool _is_signalled(void) const;
			bool _wait(void);
			bool _reset(void);

		protected:
			GFX::IGfxAdapter* m_pAdapter;
		};

	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_FENCE_H