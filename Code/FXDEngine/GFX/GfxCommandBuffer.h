// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_GFXCOMMANDBUFFER_H
#define FXDENGINE_GFX_GFXCOMMANDBUFFER_H

#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/GFX/Types.h"
#include "FXDEngine/Job/Future.h"

namespace FXD
{
	namespace GFX
	{
		enum class E_CommandBufferState : FXD::U16
		{
			Ready,
			Recording,
			RecordingRenderPass,
			RecordingDone,
			Submitted,
			Count,
			Unknown = 0xffff
		};

		// ------
		// IGfxCommandBuffer
		// - 
		// ------
		class IGfxCommandBuffer : public Core::HasImpl< GFX::IGfxCommandBuffer, 128 >
		{
		public:
			IGfxCommandBuffer(GFX::IGfxAdapter* pAdapter);
			~IGfxCommandBuffer(void);

			Job::Future< bool > create(GFX::E_GfxQueueType eQueueType, FXD::U32 nQueueID = 0, GFX::E_CommandBufferType eType = GFX::E_CommandBufferType::Primary);
			Job::Future< bool > release(void);

			Job::Future< void > begin(void);
			Job::Future< void > begin_render_pass(void);
			Job::Future< void > end_render_pass(void);
			Job::Future< void > end(void);

			GET_R(GFX::E_GfxQueueType, eQueueType, eQueueType);
			GET_R(FXD::U32, nQueueID, nQueueID);
			GET_R(GFX::E_CommandBufferType, eType, eType);
			GET_R(GFX::E_CommandBufferState, eState, eState);

		protected:
			bool _create(GFX::E_GfxQueueType eQueueType, FXD::U32 nQueueID, GFX::E_CommandBufferType eType);
			bool _release(void);

			void _begin(void);
			void _begin_render_pass(void);
			void _end_render_pass(void);
			void _end(void);

		protected:
			GFX::IGfxAdapter* m_pAdapter;
			GFX::E_GfxQueueType m_eQueueType;
			FXD::U32 m_nQueueID;
			GFX::E_CommandBufferType m_eType;
			GFX::E_CommandBufferState m_eState;
		};

		// ------
		// IGfxCommandBufferManager
		// - 
		// ------
		class IGfxCommandBufferManager : public Core::HasImpl< GFX::IGfxCommandBufferManager, 128 >
		{
		public:
			IGfxCommandBufferManager(void);
			~IGfxCommandBufferManager(void);

			Job::Future< bool > create(void);
			Job::Future< bool > release(void);

		protected:
			bool _create(void);
			bool _release(void);
		};

	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_GFXCOMMANDBUFFER_H