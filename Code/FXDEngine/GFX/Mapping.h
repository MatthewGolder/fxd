// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_MAPPING_H
#define FXDENGINE_GFX_MAPPING_H

#include "FXDEngine/GFX/Mapping.inl"

namespace FXD
{
	namespace GFX
	{
		namespace Api
		{
			CONSTEXPR Core::StringView8 ToString(GFX::E_GfxApi eApi)
			{
				return kGfxApis[FXD::STD::to_underlying(eApi)];
			}

			CONSTEXPR GFX::E_GfxApi ToApiType(const Core::StringView8 strType)
			{
				for (FXD::U16 n1 = 0; n1 < FXD::STD::to_underlying(GFX::E_GfxApi::Count); ++n1)
				{
					if (strType.compare_no_case(kGfxApis[n1]) == 0)
					{
						return (GFX::E_GfxApi)n1;
					}
				}
				return GFX::E_GfxApi::Unknown;
			}
		} //namespace Api

		namespace Pixel
		{
			CONSTEXPR const FXD::UTF8* GetFormatName(GFX::E_PixelFormat eFormat)
			{
				return Pixel::getPixelDesc(eFormat).pName;
			}
			CONSTEXPR FXD::U32 GetFlags(GFX::E_PixelFormat eFormat)
			{
				return Pixel::getPixelDesc(eFormat).nFlags;
			}
			CONSTEXPR FXD::U32 GetNumElemBytes(GFX::E_PixelFormat eFormat)
			{
				return (Pixel::getPixelDesc(eFormat).nBitsPerElem / 8);
			}
			CONSTEXPR FXD::U32 GetNumElemBits(GFX::E_PixelFormat eFormat)
			{
				return Pixel::getPixelDesc(eFormat).nBitsPerElem;
			}
			CONSTEXPR FXD::U32 GetNumElements(GFX::E_PixelFormat eFormat)
			{
				return Pixel::getPixelDesc(eFormat).nComponentCount;
			}
			CONSTEXPR GFX::E_PixelComponentType GetElementType(GFX::E_PixelFormat eFormat)
			{
				return Pixel::getPixelDesc(eFormat).eComponentType;
			}
			CONSTEXPR bool HasAlpha(GFX::E_PixelFormat eFormat)
			{
				return (Pixel::GetFlags(eFormat) & E_PixelFormatFlags::PF_HASALPHA) > 0;
			}
			CONSTEXPR bool IsFloatingPoint(GFX::E_PixelFormat eFormat)
			{
				return (Pixel::GetFlags(eFormat) & E_PixelFormatFlags::PF_FLOAT) > 0;
			}
			CONSTEXPR bool IsCompressed(GFX::E_PixelFormat eFormat)
			{
				return (Pixel::GetFlags(eFormat) & E_PixelFormatFlags::PF_COMPRESSED) > 0;
			}
			CONSTEXPR bool IsNormalized(GFX::E_PixelFormat eFormat)
			{
				return (Pixel::GetFlags(eFormat) & E_PixelFormatFlags::PF_NORMALIZED) > 0;
			}
			CONSTEXPR bool IsDepth(GFX::E_PixelFormat eFormat)
			{
				return (Pixel::GetFlags(eFormat) & E_PixelFormatFlags::PF_DEPTH) > 0;
			}
			CONSTEXPR bool IsAccessible(GFX::E_PixelFormat eFormat)
			{
				return ((Pixel::GetFlags(eFormat) & E_PixelFormatFlags::PF_COMPRESSED) || (!(Pixel::GetFlags(eFormat) & E_PixelFormatFlags::PF_DEPTH)));
			}
			CONSTEXPR14 bool IsValidExtent(FXD::U32 nWidth, FXD::U32 nHeight, FXD::U32 nDepth, GFX::E_PixelFormat eFormat)
			{
				if (Pixel::IsCompressed(eFormat))
				{
					switch (eFormat)
					{
						case GFX::E_PixelFormat::BC1:
						case GFX::E_PixelFormat::BC2:
						case GFX::E_PixelFormat::BC3:
						case GFX::E_PixelFormat::BC4:
						case GFX::E_PixelFormat::BC5:
						case GFX::E_PixelFormat::BC6H:
						case GFX::E_PixelFormat::BC7:
						{
							return ((nWidth & 3) == 0 && (nHeight & 3) == 0 && nDepth == 1);
						}
						default:
						{
							return true;
						}
					}
				}
				else
				{
					return true;
				}
			}
			CONSTEXPR14 FXD::U32 GetMemorySize(FXD::U32 nWidth, FXD::U32 nHeight, FXD::U32 nDepth, GFX::E_PixelFormat eFormat)
			{
				if (Pixel::IsCompressed(eFormat))
				{
					switch (eFormat)
					{
						// BC formats work by dividing the image into 4x4 blocks, then encoding each
						// 4x4 block with a certain number of bytes. 
						case GFX::E_PixelFormat::BC1:
						case GFX::E_PixelFormat::BC4:
						{
							return ((nWidth + 3) / 4) * ((nHeight + 3) / 4) * 8 * nDepth;
						}
						case GFX::E_PixelFormat::BC2:
						case GFX::E_PixelFormat::BC3:
						case GFX::E_PixelFormat::BC5:
						case GFX::E_PixelFormat::BC6H:
						case GFX::E_PixelFormat::BC7:
						{
							return ((nWidth + 3) / 4) * ((nHeight + 3) / 4) * 16 * nDepth;
						}
						default:
						{
							PRINT_ASSERT << "GFX: Invalid compressed pixel format";
							return 0;
						}
					}
				}
				return (nWidth * nHeight * nDepth * Pixel::GetNumElemBytes(eFormat));
			}
			CONSTEXPR14 FXD::U32 GetMaxMipmaps(FXD::U32 nWidth, FXD::U32 nHeight, FXD::U32 nDepth)
			{
				FXD::U32 nCount = 0;
				if ((nWidth > 0) && (nHeight > 0))
				{
					while (((nWidth == 1) && (nHeight == 1) && (nDepth == 1)) == false)
					{
						if (nWidth > 1)
						{
							nWidth = nWidth / 2;
						}
						if (nHeight > 1)
						{
							nHeight = nHeight / 2;
						}
						if (nDepth > 1)
						{
							nDepth = nDepth / 2;
						}
						nCount++;
					}
				}
				return nCount;
			}
		} //namespace Pixel

		namespace Index
		{
			CONSTEXPR FXD::U32 GetNumElemBytes(GFX::E_IndexType eType)
			{
				return (Index::getIndexDesc(eType).nBitsPerElem / 8);
			}
			CONSTEXPR FXD::U32 GetNumElemBits(GFX::E_IndexType eType)
			{
				return Index::getIndexDesc(eType).nBitsPerElem;
			}
			CONSTEXPR FXD::U32 GetNumElements(GFX::E_IndexType eType)
			{
				return Index::getIndexDesc(eType).nComponentCount;
			}
		} //namespace Index

		namespace Vertex
		{
			CONSTEXPR FXD::U32 GetNumElemBytes(GFX::E_VertexElementType eType)
			{
				return (Vertex::getVertexDesc(eType).nBitsPerElem / 8);
			}
			CONSTEXPR FXD::U32 GetNumElemBits(GFX::E_VertexElementType eType)
			{
				return Vertex::getVertexDesc(eType).nBitsPerElem;
			}
			CONSTEXPR FXD::U32 GetNumElements(GFX::E_VertexElementType eType)
			{
				return Vertex::getVertexDesc(eType).nComponentCount;
			}
		} //namespace Vertex

		namespace ImageF
		{
			CONSTEXPR Core::StringView8 ToString(const GFX::E_ImageType eApi)
			{
				return ImageF::kImageTypeStrings[FXD::STD::to_underlying(eApi)];
			}
			CONSTEXPR GFX::E_ImageType ToImageType(const Core::StringView8 strType)
			{
				for (FXD::U16 n1 = 0; n1 < FXD::STD::to_underlying(GFX::E_ImageType::Count); ++n1)
				{
					if (strType.compare_no_case(ImageF::kImageTypeStrings[n1]) == 0)
					{
						return (GFX::E_ImageType)n1;
					}
				}
				return GFX::E_ImageType::Unknown;
			}
		} //namespace ImageF

	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_MAPPING_H