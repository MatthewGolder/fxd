// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_RENDERTARGET_H
#define FXDENGINE_GFX_RENDERTARGET_H

#include "FXDEngine/GFX/Image.h"
#include "FXDEngine/Core/Colour.h"

namespace FXD
{
	namespace GFX
	{
		struct RENDER_SURFACE_DESC
		{
			GFX::Image Image = nullptr;
			FXD::U32 Face = 0;		// First face of the texture to bind (array index in texture arrays, or Z slice in 3D textures).
			FXD::U32 NumFaces = 0;	// Number of faces to bind (entries in a texture array, or Z slices in 3D textures). When zero the entire resource will be bound.
			FXD::U32 MipLevel = 0;	// If the texture has multiple mips, which one to bind (only one can be bound for rendering).
		};

		struct RENDER_TEXTURE_DESC
		{
			RENDER_SURFACE_DESC ColorSurfaces[GFX::kMaxMultipleRenderTargets];
			RENDER_SURFACE_DESC DepthStencilSurface;
		};

		struct RENDER_TARGET_EXTENT
		{
			FXD::U32 Width = 0;
			FXD::U32 Height = 0;
			FXD::U32 Depth = 0;
		};

		// ------
		// IRenderTarget
		// -
		// ------
		class IRenderTarget : public Core::HasImpl< GFX::IRenderTarget, 256 >, public Core::NonCopyable
		{
		public:
			IRenderTarget(GFX::IGfxAdapter* pAdapter);
			virtual ~IRenderTarget(void);

			Job::Future< bool > create(const GFX::RENDER_TEXTURE_DESC& rtDesc);
			Job::Future< bool > release(void);

			bool activate_and_clear(const Core::ColourRGBA& colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil) const;
			bool activate(void) const;
			bool clear(const Core::ColourRGBA& colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil) const;

			//bool hasColour(void) const;
			//bool hasDepthStencil(void) const;

		protected:
			bool _create(const GFX::RENDER_TEXTURE_DESC& rtDesc);
			bool _release(void);

			bool _activate_and_clear(const Core::ColourRGBA& colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil) const;
			bool _activate(void) const;
			bool _clear(const Core::ColourRGBA& colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil) const;

		protected:
			GFX::IGfxAdapter* m_pAdapter;
			GFX::RENDER_TEXTURE_DESC m_rtDesc;
			GFX::RENDER_TARGET_EXTENT m_rtExtent;
			GFX::ImageView m_colorSurfaceView[GFX::kMaxMultipleRenderTargets];
			GFX::ImageView m_depthSurfaceView;
		};

		namespace Funcs
		{
			extern void ValidateExtents(const GFX::RENDER_TEXTURE_DESC& rtDesc, GFX::RENDER_TARGET_EXTENT& rtExtent);
		} //namespace Funcs

	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_RENDERTARGET_H