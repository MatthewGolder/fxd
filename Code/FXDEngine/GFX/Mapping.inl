// Creator - MatthewGolder
#include "FXDEngine/Core/StringView.h"
#include "FXDEngine/Core/TypeTraits.h"
#include "FXDEngine/GFX/Types.h"

namespace FXD
{
	namespace GFX
	{
		// ------
		// Api
		// -
		// ------
		CONSTEXPR const Core::StringView8 kGfxApis[] =
		{
			"DX11",		// E_GfxApi::DX11
			"OpenGL",	// E_GfxApi::OpenGL
			"Vulkan",	// E_GfxApi::Vulkan
			"Count"		// E_GfxApi::Count
		};

		// ------
		// Pixel
		// -
		// ------
		enum E_PixelFormatFlags
		{
			PF_HASALPHA = (1 << 0),		// This format has an alpha channel.
			PF_COMPRESSED = (1 << 1),	// This format is compressed. This invalidates the values in elemBytes, elemBits and the bit counts as these might not be fixed in a compressed format.
			PF_FLOAT = (1 << 2),			// This is a floating point format.
			PF_DEPTH = (1 << 3),			// This is a depth format (for depth textures).
			PF_INTEGER = (1 << 4),		// This format stores data internally as integers.
			PF_SIGNED = (1 << 5),		// Format contains signed data. Absence of this flag implies unsigned data.
			PF_NORMALIZED = (1 << 6)	// Format contains normalized data. This will be [0, 1] for unsigned, and [-1,1] for signed formats.
		};

		namespace Pixel
		{
			struct _PixelFormatDesc
			{
				const GFX::E_PixelFormat ePixelFormat;				// PixelFormat.
				const FXD::UTF8* pName;									// Name of the format.
				const FXD::U8 nBitsPerElem;							// Number of bits.
				const FXD::U32 nFlags;									// E_PixelFormatFlags set by the pixel format.
				const E_PixelComponentType eComponentType;		// Data type of a single element of the format.
				const FXD::U8 nComponentCount;						// Number of elements in the format.
				const FXD::U8 rbits, gbits, bbits, abits;			// Number of bits per element in the format.
				const FXD::U8 rShift, gShift, bShift, aShift;	// Shifts used by packers/unpackers.
			};
			CONSTEXPR const _PixelFormatDesc kPixelFormatDescs[] =
			{
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::R8, "R8",
					8,																	// Bits per element
					PF_INTEGER | PF_NORMALIZED,								// Flags
					E_PixelComponentType::Byte, 1,							// Component type and count
					8, 0, 0, 0,														// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::R8U, "R8U",
					8,																	// Bits per element
					PF_INTEGER,														// Flags
					E_PixelComponentType::Byte, 1,							// Component type and count
					8, 0, 0, 0,														// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::R8S, "R8S",
					8,																	// Bits per element
					PF_INTEGER | PF_NORMALIZED | PF_SIGNED,				// Flags
					E_PixelComponentType::Byte, 1,							// Component type and count
					8, 0, 0, 0,														// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::R8I, "R8I",
					8,																	// Bits per element
					PF_INTEGER | PF_SIGNED,										// Flags
					E_PixelComponentType::Byte, 1,							// Component type and count
					8, 0, 0, 0,														// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},

				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RG8, "RG8",
					16,																// Bits per element
					PF_INTEGER | PF_NORMALIZED,								// Flags
					E_PixelComponentType::Byte, 2,							// Component type and count
					8, 8, 0, 0,														// rbits, gbits, bbits, abits
					0, 8, 0, 0,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RG8U, "RG8U",
					16,																// Bits per element
					PF_INTEGER,														// Flags
					E_PixelComponentType::Byte, 2,							// Component type and count
					8, 8, 0, 0,														// rbits, gbits, bbits, abits
					0, 8, 0, 0,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RG8S, "RG8S",
					16,																// Bits per element
					PF_INTEGER | PF_NORMALIZED | PF_SIGNED,				// Flags
					E_PixelComponentType::Byte, 2,							// Component type and count
					8, 8, 0, 0,														// rbits, gbits, bbits, abits
					0, 8, 0, 0,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RG8I, "RG8I",
					16,																// Bits per element
					PF_INTEGER | PF_SIGNED,										// Flags
					E_PixelComponentType::Byte, 2,							// Component type and count
					8, 8, 0, 0,														// rbits, gbits, bbits, abits
					0, 8, 0, 0,														// Shifts
				},

				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::BGR8, "BGR8",
					24,																// Bits per element
					PF_INTEGER | PF_NORMALIZED,								// Flags
					E_PixelComponentType::Byte, 3,							// Component type and count
					8, 8, 8, 0,														// rbits, gbits, bbits, abits
					16, 8, 0, 0,													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RGB8, "RGB8",
					24,																// Bits per element
					PF_INTEGER | PF_NORMALIZED,								// Flags
					E_PixelComponentType::Byte, 3,							// Component type and count
					8, 8, 8, 0,														// rbits, gbits, bbits, abits
					0, 8, 16, 0,													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RGB8U, "RGB8U",
					24,																// Bits per element
					PF_INTEGER,														// Flags
					E_PixelComponentType::Byte, 3,							// Component type and count
					8, 8, 8, 0,														// rbits, gbits, bbits, abits
					0, 8, 16, 0,													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RGB8S, "RGB8S",
					24,																// Bits per element
					PF_INTEGER | PF_NORMALIZED | PF_SIGNED,				// Flags
					E_PixelComponentType::Byte, 3,							// Component type and count
					8, 8, 8, 0,														// rbits, gbits, bbits, abits
					0, 8, 16, 0,													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RGB8I, "RGB8I",
					24,																// Bits per element
					PF_INTEGER | PF_SIGNED,									// Flags
					E_PixelComponentType::Byte, 3,							// Component type and count
					8, 8, 8, 0,														// rbits, gbits, bbits, abits
					0, 8, 16, 0,													// Shifts
				},

				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::BGRA8, "BGRA8",
					32,																// Bits per element
					PF_HASALPHA | PF_INTEGER | PF_NORMALIZED,				// Flags
					E_PixelComponentType::Byte, 4,							// Component type and count
					8, 8, 8, 8,														// rbits, gbits, bbits, abits
					16, 8, 0, 24,													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RGBA8, "RGBA8",
					32,																// Bits per element
					PF_HASALPHA | PF_INTEGER | PF_NORMALIZED,				// Flags
					E_PixelComponentType::Byte, 4,							// Component type and count
					8, 8, 8, 8,														// rbits, gbits, bbits, abits
					0, 8, 16, 24,													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RGBA8U, "RGBA8U",
					32,																// Bits per element
					PF_INTEGER | PF_HASALPHA,									// Flags
					E_PixelComponentType::Byte, 4,							// Component type and count
					8, 8, 8, 8,														// rbits, gbits, bbits, abits
					0, 8, 16, 24,													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RGBA8S, "RGBA8S",
					32,																	// Bits per element
					PF_INTEGER | PF_NORMALIZED | PF_SIGNED | PF_HASALPHA,	// Flags
					E_PixelComponentType::Byte, 4,								// Component type and count
					8, 8, 8, 8,															// rbits, gbits, bbits, abits
					0, 8, 16, 24,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RGBA8I, "RGBA8I",
					32,																// Bits per element
					PF_INTEGER | PF_SIGNED | PF_HASALPHA,					// Flags
					E_PixelComponentType::Byte, 4,							// Component type and count
					8, 8, 8, 8,														// rbits, gbits, bbits, abits
					0, 8, 16, 24,													// Shifts
				},

				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::R16, "R16",
					16,																// Bits per element
					PF_INTEGER | PF_NORMALIZED,								// Flags
					E_PixelComponentType::Short, 1,							// Component type and count
					16, 0, 0, 0,													// rbits, gbits, bbits, abits
					0, 0, 0, 0														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::R16U, "R16U",
					16,																// Bits per element
					PF_INTEGER,														// Flags
					E_PixelComponentType::Short, 1,							// Component type and count
					16, 0, 0, 0,													// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::R16S, "R16S",
					16,																// Bits per element
					PF_INTEGER | PF_NORMALIZED | PF_SIGNED,				// Flags
					E_PixelComponentType::Short, 1,							// Component type and count
					16, 0, 0, 0,													// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::R16I, "R16I",
					16,																// Bits per element
					PF_INTEGER | PF_SIGNED,										// Flags
					E_PixelComponentType::Short, 1,							// Component type and count
					16, 0, 0, 0,													// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::R16F, "R16F",
					16,																// Bits per element
					PF_FLOAT,														// Flags
					E_PixelComponentType::Float16, 1,						// Component type and count
					16, 0, 0, 0,													// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},

				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RG16, "RG16",
					32,																// Bits per element
					PF_INTEGER | PF_NORMALIZED,								// Flags
					E_PixelComponentType::Short, 2,							// Component type and count
					16, 16, 0, 0,													// rbits, gbits, bbits, abits
					0, 16, 0, 0														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RG16U, "RG16U",
					32,																// Bits per element
					PF_INTEGER,														// Flags
					E_PixelComponentType::Short, 2,							// Component type and count
					16, 16, 0, 0,													// rbits, gbits, bbits, abits
					0, 16, 0, 0,													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RG16S, "RG16S",
					32,																// Bits per element
					PF_INTEGER | PF_NORMALIZED | PF_SIGNED,				// Flags
					E_PixelComponentType::Short, 2,							// Component type and count
					16, 16, 0, 0,													// rbits, gbits, bbits, abits
					0, 16, 0, 0,													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RG16I, "RG16I",
					32,																// Bits per element
					PF_INTEGER | PF_SIGNED,										// Flags
					E_PixelComponentType::Short, 2,							// Component type and count
					16, 16, 0, 0,													// rbits, gbits, bbits, abits
					0, 16, 0, 0,													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RG16F, "RG16F",
					32,																// Bits per element
					PF_FLOAT,														// Flags
					E_PixelComponentType::Float16, 2,						// Component type and count
					16, 16, 0, 0,													// rbits, gbits, bbits, abits
					0, 16, 0, 0,													// Shifts
				},

				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RGBA16, "RGBA16",
					64,																// Bits per element
					PF_INTEGER | PF_NORMALIZED | PF_HASALPHA,				// Flags
					E_PixelComponentType::Short, 4,							// Component type and count
					16, 16, 16, 16,												// rbits, gbits, bbits, abits
					0, 16, 32, 48													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RGBA16U, "RGBA16U",
					64,																// Bits per element
					PF_INTEGER | PF_HASALPHA,									// Flags
					E_PixelComponentType::Short, 4,							// Component type and count
					16, 16, 16, 16,												// rbits, gbits, bbits, abits
					0, 16, 32, 48													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RGBA16S, "RGBA16S",
					64,																	// Bits per element
					PF_INTEGER | PF_NORMALIZED | PF_SIGNED | PF_HASALPHA,	// Flags
					E_PixelComponentType::Short, 4,								// Component type and count
					16, 16, 16, 16,													// rbits, gbits, bbits, abits
					0, 16, 32, 48														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RGBA16I, "RGBA16I",
					64,																// Bits per element
					PF_INTEGER | PF_SIGNED | PF_HASALPHA,					// Flags
					E_PixelComponentType::Short, 4,							// Component type and count
					16, 16, 16, 16,												// rbits, gbits, bbits, abits
					0, 16, 32, 48													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RGBA16F, "RGBA16F",
					64,																// Bits per element
					PF_FLOAT | PF_HASALPHA,										// Flags
					E_PixelComponentType::Float16, 4,						// Component type and count
					16, 16, 16, 16,												// rbits, gbits, bbits, abits
					0, 16, 32, 48													// Shifts
				},

				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::R32U, "R32U",
					32,																// Bits per element
					PF_INTEGER,														// Flags
					E_PixelComponentType::Int, 1,								// Component type and count
					32, 0, 0, 0,													// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::R32I, "R32I",
					32,																// Bits per element
					PF_INTEGER,														// Flags
					E_PixelComponentType::Int, 1,								// Component type and count
					32, 0, 0, 0,													// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::R32F, "R32F",
					32,																// Bits per element
					PF_FLOAT,														// Flags
					E_PixelComponentType::Float32, 1,						// Component type and count
					32, 0, 0, 0,													// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},

				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RG32U, "RG32U",
					64,																// Bits per element
					PF_INTEGER,														// Flags
					E_PixelComponentType::Int, 2,								// Component type and count
					32, 32, 0, 0,													// rbits, gbits, bbits, abits
					0, 32, 0, 0,													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RG32I, "RG32I",
					64,																// Bits per element
					PF_INTEGER | PF_SIGNED,										// Flags
					E_PixelComponentType::Int, 2,								// Component type and count
					32, 32, 0, 0,													// rbits, gbits, bbits, abits
					0, 32, 0, 0,													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RG32F, "RG32F",
					64,																// Bits per element
					PF_FLOAT,														// Flags
					E_PixelComponentType::Float32, 2,						// Component type and count
					32, 32, 0, 0,													// rbits, gbits, bbits, abits
					0, 32, 0, 0,													// Shifts
				},

				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RGB32U, "RGB32U",
					96,																// Bits per element
					PF_INTEGER,														// Flags
					E_PixelComponentType::Int, 3,								// Component type and count
					32, 32, 32, 0,													// rbits, gbits, bbits, abits
					0, 32, 64, 0,													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RGB32I, "RGB32I",
					96,																// Bits per element
					PF_INTEGER | PF_SIGNED,										// Flags
					E_PixelComponentType::Int, 3,								// Component type and count
					32, 32, 32, 0,													// rbits, gbits, bbits, abits
					0, 32, 64, 0,													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RGB32F, "RGB32F",
					96,																// Bits per element
					PF_FLOAT,														// Flags
					E_PixelComponentType::Float32, 3,						// Component type and count
					32, 32, 32, 0,													// rbits, gbits, bbits, abits
					0, 32, 64, 0,													// Shifts
				},

				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RGBA32U, "RGBA32U",
					128,																// Bits per element
					PF_INTEGER | PF_HASALPHA,									// Flags
					E_PixelComponentType::Int, 4,								// Component type and count
					32, 32, 32, 32,												// rbits, gbits, bbits, abits
					0, 32, 64, 96,													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RGBA32I, "RGBA32I",
					128,																// Bits per element
					PF_INTEGER | PF_SIGNED | PF_HASALPHA,					// Flags
					E_PixelComponentType::Int, 4,								// Component type and count
					32, 32, 32, 32,												// rbits, gbits, bbits, abits
					0, 32, 64, 96,													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RGBA32F, "RGBA32F",
					128,																// Bits per element
					PF_FLOAT | PF_HASALPHA,										// Flags
					E_PixelComponentType::Float32, 4,						// Component type and count
					32, 32, 32, 32,												// rbits, gbits, bbits, abits
					0, 32, 64, 96,													// Shifts
				},

				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RG11B10F, "RG11B10F",
					32,																// Bits per element
					PF_FLOAT,														// Flags
					E_PixelComponentType::Packed_R11G11B10, 1,			// Component type and count
					11, 11, 10, 0,													// rbits, gbits, bbits, abits
					0, 11, 22, 0,													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::RGB10A2, "RGB10A2",
					32,																// Bits per element
					PF_INTEGER | PF_NORMALIZED | PF_HASALPHA,				// Flags
					E_PixelComponentType::Packed_R10G10B10A2, 1,			// Component type and count
					10, 10, 10, 2,													// rbits, gbits, bbits, abits
					0, 10, 20, 30,													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::B5G5R5A1, "B5G5R5A1",
					16,																// Bits per element
					PF_INTEGER | PF_NORMALIZED | PF_HASALPHA,				// Flags
					E_PixelComponentType::Packed_B5G5R5A1, 1,				// Component type and count
					5, 5, 5, 1,														// rbits, gbits, bbits, abits
					0, 5, 10, 15,													// Shifts
				},

				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::D16, "D16",
					16,																// Bits per element
					PF_DEPTH | PF_INTEGER | PF_NORMALIZED,					// Flags
					E_PixelComponentType::Short, 1,							// Component type and count
					16, 0, 0, 0,													// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::D32, "D32",
					32,																// Bits per element
					PF_DEPTH | PF_FLOAT,											// Flags
					E_PixelComponentType::Float32, 1,						// Component type and count
					32, 0, 0, 0,													// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::D24_S8, "D24_S8",
					32,																// Bits per element
					PF_DEPTH | PF_INTEGER | PF_NORMALIZED,					// Flags
					E_PixelComponentType::Int, 2,								// Component type and count
					24, 8, 0, 0,													// rbits, gbits, bbits, abits
					0, 24, 0, 0,													// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::D32_S8X24, "D32_S8X24",
					64,																// Bits per element
					PF_DEPTH | PF_NORMALIZED,									// Flags
					E_PixelComponentType::Float32, 2,						// Component type and count
					32, 8, 0, 0,													// rbits, gbits, bbits, abits
					0, 32, 40, 0,													// Shifts
				},

				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::BC1, "BC1",
					0,																	// Bits per element
					PF_COMPRESSED | PF_HASALPHA,								// Flags
					E_PixelComponentType::Byte, 3,							// Component type and count
					0, 0, 0, 0,														// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::BC2, "BC2",
					0,																	// Bits per element
					PF_COMPRESSED | PF_HASALPHA,								// Flags
					E_PixelComponentType::Byte, 4,							// Component type and count
					0, 0, 0, 0,														// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::BC3, "BC3",
					0,																	// Bits per element
					PF_COMPRESSED | PF_HASALPHA,								// Flags
					E_PixelComponentType::Byte, 4,							// Component type and count
					0, 0, 0, 0,														// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::BC4, "BC4",
					0,																	// Bits per element
					PF_COMPRESSED,													// Flags
					E_PixelComponentType::Byte, 1,							// Component type and count
					0, 0, 0, 0,														// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::BC5, "BC5",
					0,																	// Bits per element
					PF_COMPRESSED,													// Flags
					E_PixelComponentType::Byte, 2,							// Component type and count
					0, 0, 0, 0,														// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::BC6H, "BC6H",
					0,																	// Bits per element
					PF_COMPRESSED,													// Flags
					E_PixelComponentType::Float16, 3,						// Component type and count
					0, 0, 0, 0,														// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::BC7, "BC7",
					0,																	// Bits per element
					PF_COMPRESSED | PF_HASALPHA,								// Flags
					E_PixelComponentType::Byte, 4,							// Component type and count
					0, 0, 0, 0,														// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},

				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::Count, "Count",
					0,																	// Bits per element
					0,																	// Flags
					E_PixelComponentType::Short, 0,							// Component type and count
					0, 0, 0, 0,														// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				},
				//-----------------------------------------------------------------------
				{
					GFX::E_PixelFormat::Unknown, "Unknown",
					0,																	// Bits per element
					0,																	// Flags 
					E_PixelComponentType::Short, 0,							// Component type and count
					0, 0, 0, 0,														// rbits, gbits, bbits, abits
					0, 0, 0, 0,														// Shifts
				}
			};
			CONSTEXPR const _PixelFormatDesc& getPixelDesc(GFX::E_PixelFormat eFormat)
			{
				return kPixelFormatDescs[FXD::STD::to_underlying(eFormat)];
			}
		} //namespace Pixel

		namespace Index
		{
			// ------
			// Index
			// -
			// ------
			class _IndexFormatDesc
			{
			public:
				const E_IndexType eIndexType;
				const FXD::UTF8* pName;					// Name of the format.
				const FXD::U8 nBitsPerElem;			// Number of bits.
				const FXD::U8 nComponentCount;		// Number of elements in the format.
			};

			CONSTEXPR const _IndexFormatDesc kIndexFormatDescs[] =
			{
				//-----------------------------------------------------------------------
				{
					E_IndexType::Bit_16, "Bit_16",
					16,												// Bits per element
					1													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_IndexType::Bit_32, "Bit_32",
					32,												// Bits per element
					1													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_IndexType::Count, "Count",
					0,													// Bits per element
					0													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_IndexType::Unknown, "Unknown",
					0,													// Bits per element
					0													// Component count
				}
			};
			CONSTEXPR const Index::_IndexFormatDesc& getIndexDesc(const GFX::E_IndexType eFormat)
			{
				return Index::kIndexFormatDescs[FXD::STD::to_underlying(eFormat)];
			}
		} //namespace Index

		namespace Vertex
		{
			// ------
			// Vertex
			// -
			// ------
			class _VertexFormatDesc
			{
			public:
				const E_VertexElementType eVertexType;
				const FXD::UTF8* pName;				// Name of the format.
				const FXD::U8 nBitsPerElem;		// Number of bits.
				const FXD::U8 nComponentCount;	// Number of elements in the format.
			};
			CONSTEXPR const _VertexFormatDesc kVertexFormatDescs[] =
			{
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::Float1, "Float1",
					32,												// Bits per element
					1													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::Float2, "Float2",
					64,												// Bits per element
					2													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::Float3, "Float3",
					96,												// Bits per element
					3													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::Float4, "Float4",
					128,												// Bits per element
					4													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::Colour, "Colour",
					128,												// Bits per element
					4													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::Colour_ABGR, "Colour_ABGR",
					128,												// Bits per element
					4													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::Colour_ARGB, "Colour_ARGB",
					128,												// Bits per element
					4													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::Short1, "Short1",
					16,												// Bits per element
					1													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::Short2, "Short2",
					32,												// Bits per element
					2													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::Short4, "Short4",
					64,												// Bits per element
					4													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::Short1, "UShort1",
					16,												// Bits per element
					1													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::UShort2, "UShort2",
					32,												// Bits per element
					2													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::UShort4, "UShort4",
					64,												// Bits per element
					4													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::Int1, "Int1",
					32,												// Bits per element
					1													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::Int2, "Int2",
					64,												// Bits per element
					2													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::Int3, "Int3",
					96,												// Bits per element
					3													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::Int4, "Int4",
					128,												// Bits per element
					4													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::UInt1, "UInt1",
					32,												// Bits per element
					1													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::UInt2, "UInt2",
					64,												// Bits per element
					2													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::UInt3, "UInt3",
					96,												// Bits per element
					3													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::UInt4, "UInt4",
					128,												// Bits per element
					4													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::UByte4, "UByte4",
					32,												// Bits per element
					4													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::UByte4_Norm, "UByte4_Norm",
					32,												// Bits per element
					4													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::Count, "Count",
					0,													// Bits per element
					0													// Component count
				},
				//-----------------------------------------------------------------------
				{
					E_VertexElementType::Unknown, "Unknown",
					0,													// Bits per element
					0													// Component count
				}
			};

			CONSTEXPR const Vertex::_VertexFormatDesc& getVertexDesc(const GFX::E_VertexElementType eFormat)
			{
				return Vertex::kVertexFormatDescs[FXD::STD::to_underlying(eFormat)];
			}
		} //namespace Vertex

		namespace ImageF
		{
			// ------
			// ImageF
			// -
			// ------
			CONSTEXPR Core::StringView8 kImageTypeStrings[] =
			{
				"One",	// GFX::E_ImageType::One
				"Two",	// GFX::E_ImageType::Two
				"Three",	// GFX::E_ImageType::Three
				"Cube",	// GFX::E_ImageType::Cube
				"Count"	// GFX::E_ImageType::Count
			};
		} //namespace ImageF
	} //namespace GFX
} //namespace FXD