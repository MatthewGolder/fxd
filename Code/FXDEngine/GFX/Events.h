// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_EVENTS_H
#define FXDENGINE_GFX_EVENTS_H

#include "FXDEngine/Core/String.h"
#include "FXDEngine/GFX/Types.h"
#include "FXDEngine/Math/Vector2I.h"
#include "FXDEngine/Job/Event.h"

namespace FXD
{
	namespace GFX
	{
		class OnWindowResize FINAL : public Job::IEvent
		{
		public:
			OnWindowResize(FXD::U64 windowHandle, const Math::Vector2I& pos)
				: WindowHandle(windowHandle)
				, Pos(pos)
			{
			}
			~OnWindowResize(void) = default;

		public:
			const FXD::U64 WindowHandle;
			const Math::Vector2I Pos;
		};

		enum class E_WindowEvent
		{
			Resized = 0,
			Moved,
			FocusReceived,
			FocusLost,
			CloseRequested
		};
		class OnWindowEvent FINAL : public Job::IEvent
		{
		public:
			OnWindowEvent(FXD::U64 windowHandle, GFX::E_WindowEvent eEvent)
				: WindowHandle(windowHandle)
				, Event(eEvent)
			{
			}
			~OnWindowEvent(void) = default;

		public:
			const FXD::U64 WindowHandle;
			const GFX::E_WindowEvent Event;
		};

	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_EVENTS_H