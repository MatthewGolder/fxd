// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_GFXSYSTEM_H
#define FXDENGINE_GFX_GFXSYSTEM_H

#include "FXDEngine/GFX/GfxApi.h"
#include "FXDEngine/IO/FileCache.h"
#include "FXDEngine/Scene/Types.h"

namespace FXD
{
	namespace GFX
	{
		// ------
		// IGfxSystem
		// - 
		// ------
		class IGfxSystem FINAL : public Core::NonCopyable
		{
		public:
			friend class IGfxJPThread;

		public:
			IGfxSystem(void);
			~IGfxSystem(void);

			Job::Future< bool > create_system(void);
			Job::Future< bool > release_system(void);

			Job::Future< GFX::GfxAdapter > create_adapter(const App::AdapterDesc& adapterDesc);
			Job::Future< bool > release_adapter(GFX::GfxAdapter& gfxAdapter);

			Job::Future< Scene::Geo > load_geo(const IO::Path& ioPath);
			Job::Future< GFX::Texture > load_texture(const IO::Path& ioPath);

			void load_geo_async(const IO::Path& ioPath, FXD::STD::function< void(Scene::Geo) > callback);
			void load_texture_async(const IO::Path& ioPath, FXD::STD::function< void(GFX::Texture) > callback);

			GET_REF_W(GFX::GfxApi, gfxApi, gfx_api);
			GET_REF_W(IO::FileCache< Scene::Geo >, geoCache, geo_cache);
			GET_REF_W(IO::FileCache< GFX::Texture >, textureCache, texture_cache);

		private:
			bool _create_system(void);
			bool _release_system(void);

			GFX::GfxAdapter _create_adapter(const App::AdapterDesc& adapterDesc);
			bool _release_adapter(GFX::GfxAdapter& gfxAdapter);

			void _process(FXD::F32 dt);

		private:
			GFX::GfxApi m_gfxApi;
			IO::FileCache< Scene::Geo > m_geoCache;
			IO::FileCache< GFX::Texture > m_textureCache;
			Container::Vector< GFX::GfxAdapter > m_gfxAdapters;
		};

	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_GFXSYSTEM_H