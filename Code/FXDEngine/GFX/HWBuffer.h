// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_HWBUFFER_H
#define FXDENGINE_GFX_HWBUFFER_H

#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/GFX/Types.h"
#include "FXDEngine/Job/Future.h"
#include "FXDEngine/Memory/MemHandle.h"

namespace FXD
{
	namespace GFX
	{
		class LockedKey
		{
		public:
			LockedKey(void)
				: SourceObject(nullptr)
			{
			}
			LockedKey(void* pSource)
				: SourceObject(pSource)
			{
			}
			bool operator==(const LockedKey& rhs) const
			{
				return (SourceObject == rhs.SourceObject);
			}
			bool operator!=(const LockedKey& rhs) const
			{
				return (SourceObject != rhs.SourceObject);
			}
			LockedKey& operator=(const LockedKey& rhs)
			{
				SourceObject = rhs.SourceObject;
				return (*this);
			}
			bool operator<(const LockedKey& rhs) const
			{
				return (SourceObject < rhs.SourceObject);
			}

		public:
			void* SourceObject;
		};

		struct LockedData
		{
		public:
			LockedData(void)
				: StagingResource(nullptr)
				, Pitch(0)
				, Offset(0)
				, m_pData(nullptr)
				, m_bAllocDataWasUsed(false)
			{}
			~LockedData(void)
			{}

			void alloc_data(FXD::U32 nSize)
			{
				m_memHandle = Memory::MemHandle::allocate(nSize, 16);
				m_pData = (FXD::U8*)Memory::MemHandle::LockGuard(m_memHandle).get_mem();
				m_bAllocDataWasUsed = true;
			}

			void set_data(void* pInData)
			{
				PRINT_COND_ASSERT(!m_bAllocDataWasUsed, "GFX: LockedData Cannot set data");
				m_pData = (FXD::U8*)pInData;
			}

			FXD::U8* get_data(void) const
			{
				return m_pData;
			}

			void free_data(void)
			{
				PRINT_COND_ASSERT(m_bAllocDataWasUsed, "GFX: LockedData Cannot free data");
				m_memHandle = Memory::MemHandle();
				m_pData = nullptr;
			}

		public:
			void* StagingResource;
			FXD::U32 Pitch;
			FXD::U32 Offset;

		private:
			FXD::U8* m_pData;
			Memory::MemHandle m_memHandle;
			bool m_bAllocDataWasUsed;
		};
		
		// ------
		// IHWBuffer
		// -
		// ------
		class IHWBuffer : public Core::HasImpl< GFX::IHWBuffer, 128 >, public Core::NonCopyable
		{
		private:
			// ------
			// LockGuard
			// -
			// ------
			class LockGuard FINAL : public Core::NonCopyable
			{
			public:
				LockGuard(GFX::HWBuffer& buffer, GFX::E_BufferLockOptions eLockMode, FXD::U32 nQueueID);
				LockGuard(GFX::HWBuffer& buffer, FXD::U32 nOffset, FXD::U32 nLength, GFX::E_BufferLockOptions eLockMode, FXD::U32 nQueueID);

				~LockGuard(void);

			private:
				GFX::HWBuffer& m_buffer;
			#if defined(FXD_DEBUG)
				bool m_bLocked;
			#endif
			};

		public:
			IHWBuffer(GFX::IGfxAdapter* pAdapter);
			virtual ~IHWBuffer(void);

			Job::Future< bool > create(const GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc = nullptr);
			Job::Future< bool > release(void);

			void* lock(FXD::U32 nOffset, FXD::U32 nLength, GFX::E_BufferLockOptions eLockMode, FXD::U32 nQueueID = 0);
			void* lock(GFX::E_BufferLockOptions eLockMode, FXD::U32 nQueueID = 0);
			void unlock(void);

			void read_data(FXD::U32 nOffset, FXD::U32 nLength, void* pDst, FXD::U32 nQueueID = 0);
			void write_data(FXD::U32 nOffset, FXD::U32 nLength, const void* pSrc, FXD::U32 nQueueID = 0);

			bool is_dynamic(void) const;

			/*
			void copy_data(HWBuffer& srcBuffer, FXD::U32 nSrcOffset, FXD::U32 nDstOffset, FXD::U32 nLength, bool bDiscardWholeBuffer = false, const std::shared_ptr< GFX::CmdBuffer >& commandBuffer = nullptr);
			void copy_data(HWBuffer& srcBuffer, const std::shared_ptr< GFX::CmdBuffer >& commandBuffer = nullptr)
			{
				FXD::U32 nSize = FXD::STD::Min(get_size(), srcBuffer.get_size());
				copy_data(srcBuffer, 0, 0, nSize, true, commandBuffer);
			}
			*/

			GET_R(bool, bLocked, is_locked);
			GET_R(FXD::U32, nCount, Count);
			GET_R(FXD::U32, nStride, Stride);
			GET_R(GFX::E_BufferUsageFlags, eBufferUsage, buffer_usage);
			FXD::U32 get_size(void) const { return (m_nCount * m_nStride); }

		protected:
			bool _create(const GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc);
			bool _release(void);

			void* _map(FXD::U32 nOffset, FXD::U32 nLength, GFX::E_BufferLockOptions eLockMode, FXD::U32 nQueueID);
			void _unmap(void);

		protected:
			GFX::IGfxAdapter* m_pAdapter;
			bool m_bLocked;
			FXD::U32 m_nCount;
			FXD::U32 m_nStride;
			GFX::E_BufferUsageFlags m_eBufferUsage;
		};

		// ------
		// IIndexBuffer
		// -
		// ------
		class IIndexBuffer : public GFX::IHWBuffer
		{
		public:
			IIndexBuffer(GFX::IGfxAdapter* pAdapter);
			~IIndexBuffer(void);

			Job::Future< bool > create(const GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, GFX::E_IndexType eIndexType, const void* pSrc = nullptr);
			Job::Future< bool > create(const GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc = nullptr);
		
			bool activate(FXD::U32 nOffset, const GFX::GfxCommandBuffer& commandBuffer = nullptr) const;

		protected:
			bool _activate(FXD::U32 nOffset) const;
		};

		// ------
		// IVertexBuffer
		// -
		// ------
		class IVertexBuffer : public GFX::IHWBuffer
		{
		public:
			IVertexBuffer(GFX::IGfxAdapter* pAdapter);
			~IVertexBuffer(void);

			Job::Future< bool > create(const GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, GFX::E_VertexElementType eVertexType, const void* pSrc = nullptr);
			Job::Future< bool > create(const GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc = nullptr);

			bool activate(FXD::U32 nOffset, FXD::U32 nStrideOverride = 0, const GFX::GfxCommandBuffer& commandBuffer = nullptr) const;

		protected:
			bool _activate(FXD::U32 nOffset, FXD::U32 nStrideOverride) const;
		};

		// ------
		// IUniformBuffer
		// -
		// ------
		class IUniformBuffer : public GFX::IHWBuffer
		{
		public:
			IUniformBuffer(GFX::IGfxAdapter* pAdapter);
			~IUniformBuffer(void);

			Job::Future< bool > create(const GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc = nullptr);
		};

	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_HWBUFFER_H