// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_GFXADAPTER_H
#define FXDENGINE_GFX_GFXADAPTER_H

#include "FXDEngine/App/Adapter.h"
#include "FXDEngine/Core/Colour.h"
#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/Container/RefCounting.h"
#include "FXDEngine/GFX/FX/Types.h"

namespace FXD
{
	namespace GFX
	{
		struct IMAGE_DESC;
		struct IMAGEVIEW_DESC;
		struct RENDER_SURFACE_DESC;
		struct RENDER_TEXTURE_DESC;

		namespace FX
		{
			struct SAMPLER_DESC;
			struct BLEND_STATE_DESC;
			struct RASTER_STATE_DESC;
			struct DEPTHSTENCIL_STATE_DESC;
			struct PIPELINE_DESC;
		} //namespace FX

		// ------
		// IGfxAdapterDesc
		// - 
		// ------
		class IGfxAdapterDesc : public Core::HasImpl< GFX::IGfxAdapterDesc, 1024 >, public App::IAdapterDesc
		{
		public:
			IGfxAdapterDesc(const App::DeviceIndex nDeviceID);
			~IGfxAdapterDesc(void);

			SET(GFX::E_DeviceType, eDeviceType, deviceType);
			GET_R(GFX::E_DeviceType, eDeviceType, deviceType);

		protected:
			GFX::E_DeviceType m_eDeviceType;
		};

		// ------
		// IGfxAdapter
		// -
		// ------
		class IGfxAdapter : public Core::HasImpl< GFX::IGfxAdapter, 2048 >, public App::IAdapter
		{
		public:
			EXPLICIT IGfxAdapter(const App::AdapterDesc& adapterDesc, const App::IApi* pApi);
			virtual ~IGfxAdapter(void);

			Job::Future< bool > create(void) FINAL;
			Job::Future< bool > release(void) FINAL;

			Job::Future< GFX::Viewport > create_viewport(GFX::IWindow* pWindow);
			Job::Future< GFX::GfxCommandBuffer > create_command_buffer(GFX::E_GfxQueueType eQueueType, FXD::U32 nQueueID, GFX::E_CommandBufferType eType);
			Job::Future< GFX::Fence > create_fence(bool bSignaled);

			Job::Future< GFX::Image > create_image(const GFX::IMAGE_DESC& imageDesc);
			Job::Future< GFX::Image > create_image(const GFX::Texture& texture, GFX::E_BufferUsageFlags eUsage);
			Job::Future< GFX::Image > create_image(const GFX::SwapChain& swapChain, FXD::U32 nBuffer);
			Job::Future< GFX::ImageView > create_image_view(GFX::IImage* pImage, const GFX::IMAGEVIEW_DESC& imageViewDesc);

			Job::Future< GFX::RenderTarget > create_render_target(const GFX::RENDER_TEXTURE_DESC& rtDesc);

			Job::Future< GFX::IndexBuffer > create_index_buffer(GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, GFX::E_IndexType eIndexType, const void* pSrc = nullptr);
			Job::Future< GFX::IndexBuffer > create_index_buffer(GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc = nullptr);
			Job::Future< GFX::UniformBuffer > create_uniform_buffer(GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc = nullptr);
			Job::Future< GFX::VertexBuffer > create_vertex_buffer(GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc = nullptr);

			/*
			Job::Future< FX::SamplerState > create_sampler_state(const FX::SAMPLER_DESC& sampleDesc) const;
			Job::Future< FX::BlendState > create_blend_state(const FX::BLEND_STATE_DESC& blendDesc) const;
			Job::Future< FX::RasterState > create_raster_state(const FX::RASTER_STATE_DESC& rasterDesc) const;
			Job::Future< FX::DepthStencilState > create_depth_stencil_state(const FX::DEPTHSTENCIL_STATE_DESC& dsStateDesc) const;
			*/
			Job::Future< FX::Pipeline > create_pipeline(const FX::PIPELINE_DESC& pipelineDesc) const;

			//void RHISetBlendFactor();
			//void RHISetBlendState();
			//void RHISetDepthStencilState();
			//void RHISetRasterizerState();
			//void RHISetSamplerStates();
			//void RHISetGraphicsPipelineState();

			//void RHISetIndexBuffer();
			//void RHISetVertexBuffer();

			//void RHISetRenderTargets(const GFX::RenderTarget& renderTarget);
			//void RHISetScissorRect(bool bEnable, FXD::U32 nMinX, FXD::U32 nMinY, FXD::U32 nMaxX, FXD::U32 nMaxY);

			void RHIBeginDrawingViewport(GFX::Viewport& viewport, const GFX::GfxCommandBuffer& cmdBuffer = nullptr);
			void RHIEndDrawingViewport(const GFX::Viewport& viewport, const GFX::GfxCommandBuffer& cmdBuffer = nullptr);
			void RHIClearViewport(GFX::Viewport& viewport, Core::ColourRGBA colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil, const GFX::GfxCommandBuffer& cmdBuffer = nullptr);
			void RHIFrame(GFX::IGfxFrameItem* pFrame, const GFX::GfxCommandBuffer& cmdBuffer = nullptr);
			void RHIExecuteCommandList(const GFX::GfxCommandBuffer& cmdBuffer);

			GET_REF_W(GFX::FX::StateManager, fxStateManager, fx_state_manager);
			GET_REF_W(GFX::FX::FXLibrary, fxLibrary, fx_library);

		protected:
			void _update(FXD::F32 dt);

			bool _create(void);
			bool _release(void);

		protected:
			GFX::FX::StateManager m_fxStateManager;
			GFX::FX::FXLibrary m_fxLibrary;
			GFX::IViewport* m_pDrawingViewport;
		};

	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_GFXADAPTER_H