// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_GFXAPI_H
#define FXDENGINE_GFX_GFXAPI_H

#include "FXDEngine/App/Api.h"
#include "FXDEngine/Core/StringView.h"
#include "FXDEngine/GFX/Types.h"

namespace FXD
{
	namespace GFX
	{
		// ------
		// IGfxApi
		// -
		// ------
		class IGfxApi : public Core::HasImpl< GFX::IGfxApi, 128 >, public App::IApi
		{
		public:
			IGfxApi(void);
			~IGfxApi(void);

			bool create_api(void);
			bool release_api(void);

			GET_R(GFX::E_GfxApi, eGfxApi, gfxApiType);

		protected:
			void _create_adapter_list(void) FINAL;

		protected:
			GFX::E_GfxApi m_eGfxApi;
		};

	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_GFXAPI_H