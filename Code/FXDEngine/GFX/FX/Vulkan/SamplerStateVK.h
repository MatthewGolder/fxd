// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_VULKAN_SAMPLERSTATEVK_H
#define FXDENGINE_GFX_FX_VULKAN_SAMPLERSTATEVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/FX/SamplerState.h"
#include "FXDEngine/GFX/Vulkan/Vulkan.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::FX::ISamplerState >
			// -
			// ------
			template <>
			class Impl< GFX::FX::ISamplerState >
			{
			public:
				friend class GFX::FX::ISamplerState;

			public:
				Impl(void);
				~Impl(void);

			protected:
				VkSampler m_vkSampler;
			};
		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_FX_VULKAN_SAMPLERSTATEVK_H