// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_VULKAN_DEPTHSTENCILSTATEVK_H
#define FXDENGINE_GFX_FX_VULKAN_DEPTHSTENCILSTATEVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/FX/DepthStencilState.h"
#include "FXDEngine/GFX/Vulkan/Vulkan.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::FX::IDepthStencilState >
			// -
			// ------
			template <>
			class Impl< GFX::FX::IDepthStencilState >
			{
			public:
				friend class GFX::FX::IDepthStencilState;

			public:
				Impl(void);
				~Impl(void);
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_FX_VULKAN_DEPTHSTENCILSTATEVK_H