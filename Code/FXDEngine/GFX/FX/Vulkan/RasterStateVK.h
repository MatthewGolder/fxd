// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_VULKAN_RASTERSTATEVK_H
#define FXDENGINE_GFX_FX_VULKAN_RASTERSTATEVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/FX/RasterState.h"
#include "FXDEngine/GFX/Vulkan/Vulkan.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::FX::IRasterState >
			// -
			// ------
			template <>
			class Impl< GFX::FX::IRasterState >
			{
			public:
				friend class GFX::FX::IRasterState;

			public:
				Impl(void);
				~Impl(void);

			protected:
			};
		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_FX_VULKAN_RASTERSTATEVK_H