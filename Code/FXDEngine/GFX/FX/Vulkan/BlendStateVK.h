// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_VULKAN_BLENDSTATEVK_H
#define FXDENGINE_GFX_FX_VULKAN_BLENDSTATEVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/FX/BlendState.h"
#include "FXDEngine/GFX/Vulkan/Vulkan.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::FX::IBlendState >
			// -
			// ------
			template <>
			class Impl< GFX::FX::IBlendState >
			{
			public:
				friend class GFX::FX::IBlendState;

			public:
				Impl(void);
				~Impl(void);
			};
		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_FX_VULKAN_BLENDSTATEVK_H