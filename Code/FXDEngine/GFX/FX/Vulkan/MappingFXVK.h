// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_VULKAN_MAPPINGFXVK_H
#define FXDENGINE_GFX_FX_VULKAN_MAPPINGFXVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/FX/Types.h"
#include "FXDEngine/GFX/Vulkan/Vulkan.h"

namespace FXD
{
	namespace GFX
	{
		namespace FX
		{
			namespace Sampler
			{
				extern const VkSamplerMipmapMode GetMipFilter(const FX::E_FilterMode eType);
				extern const VkFilter GetMagFilter(const FX::E_FilterMode eType);
				extern const VkFilter GetMinFilter(const FX::E_FilterMode eType);

				extern const VkSamplerAddressMode GetAddressMode(const FX::E_AddressMode eType, bool bSupportsMirrorClampToEdge);

				extern const VkCompareOp GetCompareFunction(const FX::E_CompareOp eMode);
				extern const VkStencilOp GetStencilOp(const FX::E_StencilOp eMode);

				extern const VkBlendOp GetBlendOp(const FX::E_BlendOp eMode);
				extern const VkBlendFactor GetBlendFactor(const FX::E_BlendFactor eMode);
			}
		} //namespace FX
	} //namespace GFX
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_FX_VULKAN_MAPPINGFXVK_H