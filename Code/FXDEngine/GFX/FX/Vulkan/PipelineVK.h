// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_VULKAN_PIPELINEVK_H
#define FXDENGINE_GFX_FX_VULKAN_PIPELINEVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/FX/Pipeline.h"
#include "FXDEngine/GFX/Vulkan/Vulkan.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::FX::IPipeline >
			// -
			// ------
			template <>
			class Impl< GFX::FX::IPipeline >
			{
			public:
				friend class GFX::FX::IPipeline;

			public:
				Impl(void);
				~Impl(void);

			protected:
			};
		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_FX_VULKAN_PIPELINEVK_H