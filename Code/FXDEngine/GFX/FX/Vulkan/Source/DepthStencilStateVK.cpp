// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/Vulkan/GfxAdapterVK.h"
#include "FXDEngine/GFX/FX/Vulkan/MappingFXVK.h"
#include "FXDEngine/GFX/FX/Vulkan/DepthStencilStateVK.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// IDepthStencilState::Impl::Impl
// - 
// ------
IDepthStencilState::Impl::Impl::Impl(void)
{
}
IDepthStencilState::Impl::~Impl(void)
{
}

// ------
// IDepthStencilState
// - 
// ------
IDepthStencilState::IDepthStencilState(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IDepthStencilState::~IDepthStencilState(void)
{
}

bool IDepthStencilState::_create(const FX::DEPTHSTENCIL_STATE_DESC& dsStateDesc)
{
	m_dsStateDesc = dsStateDesc;
	VkPipelineDepthStencilStateCreateInfo vkDSStateInfo = {};
	GFX::MemZeroVulkanStruct(vkDSStateInfo, VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO);

	vkDSStateInfo.depthTestEnable = ((m_dsStateDesc.DepthTest != E_CompareOp::Always) || m_dsStateDesc.EnableDepthWrite) ? VK_TRUE : VK_FALSE;
	vkDSStateInfo.depthWriteEnable = m_dsStateDesc.EnableDepthWrite ? VK_TRUE : VK_FALSE;
	vkDSStateInfo.depthCompareOp = FX::Sampler::GetCompareFunction(m_dsStateDesc.DepthTest);

	vkDSStateInfo.depthBoundsTestEnable = VK_FALSE;
	vkDSStateInfo.minDepthBounds = 0;
	vkDSStateInfo.maxDepthBounds = 1.0f;
	vkDSStateInfo.stencilTestEnable = (m_dsStateDesc.EnableFrontFaceStencil || m_dsStateDesc.EnableBackFaceStencil) ? VK_TRUE : VK_FALSE;

	// Front
	vkDSStateInfo.back.compareMask = m_dsStateDesc.StencilReadMask;
	vkDSStateInfo.back.writeMask = m_dsStateDesc.StencilWriteMask;
	vkDSStateInfo.back.compareOp = FX::Sampler::GetCompareFunction(m_dsStateDesc.FrontFaceStencilTest);
	vkDSStateInfo.back.failOp = FX::Sampler::GetStencilOp(m_dsStateDesc.FrontFaceStencilFailStencilOp);
	vkDSStateInfo.back.depthFailOp = FX::Sampler::GetStencilOp(m_dsStateDesc.FrontFaceDepthFailStencilOp);
	vkDSStateInfo.back.passOp = FX::Sampler::GetStencilOp(m_dsStateDesc.FrontFacePassStencilOp);
	vkDSStateInfo.back.reference = 0;

	if (m_dsStateDesc.EnableBackFaceStencil)
	{
		// Back
		vkDSStateInfo.front.compareMask = m_dsStateDesc.StencilReadMask;
		vkDSStateInfo.front.writeMask = m_dsStateDesc.StencilWriteMask;
		vkDSStateInfo.front.compareOp = FX::Sampler::GetCompareFunction(m_dsStateDesc.BackFaceStencilTest);
		vkDSStateInfo.front.failOp = FX::Sampler::GetStencilOp(m_dsStateDesc.BackFaceStencilFailStencilOp);
		vkDSStateInfo.front.depthFailOp = FX::Sampler::GetStencilOp(m_dsStateDesc.BackFaceDepthFailStencilOp);
		vkDSStateInfo.front.passOp = FX::Sampler::GetStencilOp(m_dsStateDesc.BackFacePassStencilOp);
		vkDSStateInfo.front.reference = 0;
	}
	else
	{
		vkDSStateInfo.front = vkDSStateInfo.back;
	}
	
	return true;
}

bool IDepthStencilState::_release(void)
{
	m_dsStateDesc = FX::DEPTHSTENCIL_STATE_DESC();
	return true;
}

void IDepthStencilState::_attach(void)
{
}

void IDepthStencilState::_detach(void)
{
}
#endif //IsGFXVulkan()