// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/Vulkan/GfxAdapterVK.h"
#include "FXDEngine/GFX/FX/Vulkan/BlendStateVK.h"
#include "FXDEngine/GFX/FX/Vulkan/MappingFXVK.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// IBlendState::Impl::Impl
// - 
// ------
IBlendState::Impl::Impl::Impl(void)
{
}
IBlendState::Impl::~Impl(void)
{
}

// ------
// IBlendState
// - 
// ------
IBlendState::IBlendState(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IBlendState::~IBlendState(void)
{
}

bool IBlendState::_create(const FX::BLEND_STATE_DESC& blendDesc)
{
	m_blendDesc = blendDesc;

	VkPipelineColorBlendAttachmentState vkBlendStates[GFX::kMaxMultipleRenderTargets];

	for (FXD::U16 n1 = 0; n1 < GFX::kMaxMultipleRenderTargets; n1++)
	{
		const BLEND_STATE_DESC::RT_DESC& rtBlendDesc = m_blendDesc.RenderTargets[n1];
		VkPipelineColorBlendAttachmentState& vkBlendState = vkBlendStates[n1];
		Memory::MemZero_T(vkBlendState);

		vkBlendState.blendEnable = (
			(rtBlendDesc.ColorBlendOp != E_BlendOp::Add) || (rtBlendDesc.ColorDestBlend != E_BlendFactor::Zero) || (rtBlendDesc.ColorSrcBlend != E_BlendFactor::One) ||
			(rtBlendDesc.AlphaBlendOp != E_BlendOp::Add) || (rtBlendDesc.AlphaDestBlend != E_BlendFactor::Zero) ||( rtBlendDesc.AlphaSrcBlend != E_BlendFactor::One)) ? VK_TRUE : VK_FALSE;

		vkBlendState.colorBlendOp = FX::Sampler::GetBlendOp(rtBlendDesc.ColorBlendOp);
		vkBlendState.srcColorBlendFactor = FX::Sampler::GetBlendFactor(rtBlendDesc.ColorSrcBlend);
		vkBlendState.dstColorBlendFactor = FX::Sampler::GetBlendFactor(rtBlendDesc.ColorDestBlend);

		vkBlendState.alphaBlendOp = FX::Sampler::GetBlendOp(rtBlendDesc.AlphaBlendOp);
		vkBlendState.srcAlphaBlendFactor = FX::Sampler::GetBlendFactor(rtBlendDesc.AlphaSrcBlend);
		vkBlendState.dstAlphaBlendFactor = FX::Sampler::GetBlendFactor(rtBlendDesc.AlphaDestBlend);

		vkBlendState.colorWriteMask = (rtBlendDesc.ColorWriteMask & E_ColorWriteMask::CW_RED) ? VK_COLOR_COMPONENT_R_BIT : 0;
		vkBlendState.colorWriteMask |= (rtBlendDesc.ColorWriteMask & E_ColorWriteMask::CW_GREEN) ? VK_COLOR_COMPONENT_G_BIT : 0;
		vkBlendState.colorWriteMask |= (rtBlendDesc.ColorWriteMask & E_ColorWriteMask::CW_BLUE) ? VK_COLOR_COMPONENT_B_BIT : 0;
		vkBlendState.colorWriteMask |= (rtBlendDesc.ColorWriteMask & E_ColorWriteMask::CW_ALPHA) ? VK_COLOR_COMPONENT_A_BIT : 0;
	}
	return true;
}

bool IBlendState::_release(void)
{
	m_blendDesc = FX::BLEND_STATE_DESC();
	return true;
}

void IBlendState::_attach(void)
{
}

void IBlendState::_detach(void)
{
}
#endif //IsGFXVulkan()