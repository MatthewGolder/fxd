// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/FX/Vulkan/MappingFXVK.h"
#include "FXDEngine/Core/TypeTraits.h"

using namespace FXD;
using namespace GFX;

// ------
// E_FilterMode
// -
// ------
const VkSamplerMipmapMode sVKMipFilter[] =
{
	VK_SAMPLER_MIPMAP_MODE_NEAREST,	// FX::E_FilterMode::Point
	VK_SAMPLER_MIPMAP_MODE_NEAREST,	// FX::E_FilterMode::Bilinear
	VK_SAMPLER_MIPMAP_MODE_LINEAR,	// FX::E_FilterMode::Trilinear
	VK_SAMPLER_MIPMAP_MODE_LINEAR,	// FX::E_FilterMode::AnisotropicPoint
	VK_SAMPLER_MIPMAP_MODE_LINEAR		// FX::E_FilterMode::AnisotropicLinear
};
const VkSamplerMipmapMode FXD::GFX::FX::Sampler::GetMipFilter(const FX::E_FilterMode eType)
{
	return sVKMipFilter[FXD::STD::to_underlying(eType)];
}
const VkFilter sVKMagFilter[] =
{
	VK_FILTER_NEAREST,	// FX::E_FilterMode::Point
	VK_FILTER_LINEAR,		// FX::E_FilterMode::Bilinear
	VK_FILTER_LINEAR,		// FX::E_FilterMode::Trilinear
	VK_FILTER_LINEAR,		// FX::E_FilterMode::AnisotropicPoint
	VK_FILTER_LINEAR		// FX::E_FilterMode::AnisotropicLinear
};
const VkFilter FXD::GFX::FX::Sampler::GetMagFilter(const FX::E_FilterMode eType)
{
	return sVKMagFilter[FXD::STD::to_underlying(eType)];
}
const VkFilter sVKMinFilter[] =
{
	VK_FILTER_NEAREST,	// FX::E_FilterMode::Point
	VK_FILTER_LINEAR,		// FX::E_FilterMode::Bilinear
	VK_FILTER_LINEAR,		// FX::E_FilterMode::Trilinear
	VK_FILTER_LINEAR,		// FX::E_FilterMode::AnisotropicPoint
	VK_FILTER_LINEAR		// FX::E_FilterMode::AnisotropicLinear
};
const VkFilter FXD::GFX::FX::Sampler::GetMinFilter(const FX::E_FilterMode eType)
{
	return sVKMinFilter[FXD::STD::to_underlying(eType)];
}

// ------
// E_AddressMode
// -
// ------
const VkSamplerAddressMode sVKAddressModesSupportMirrorClamp[] =
{
	VK_SAMPLER_ADDRESS_MODE_REPEAT,						// FX::E_AddressMode::Wrap
	VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE,	// FX::E_AddressMode::Mirror
	VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,				// FX::E_AddressMode::Clamp
	VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER			// FX::E_AddressMode::ClampBorder
};
const VkSamplerAddressMode sVKAddressModes[] =
{
	VK_SAMPLER_ADDRESS_MODE_REPEAT,						// FX::E_AddressMode::Wrap
	VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,				// FX::E_AddressMode::Mirror
	VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,				// FX::E_AddressMode::Clamp
	VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER			// FX::E_AddressMode::ClampBorder
};
const VkSamplerAddressMode FXD::GFX::FX::Sampler::GetAddressMode(const FX::E_AddressMode eType, bool bSupportsMirrorClampToEdge)
{
	return bSupportsMirrorClampToEdge ? sVKAddressModesSupportMirrorClamp[FXD::STD::to_underlying(eType)] : sVKAddressModes[FXD::STD::to_underlying(eType)];
}

// ------
// E_CompareOp
// -
// ------
const VkCompareOp sVKCompareFunctions[] =
{
	VK_COMPARE_OP_LESS,					// FX::E_CompareOp::Less
	VK_COMPARE_OP_LESS_OR_EQUAL,		// FX::E_CompareOp::LessEqual
	VK_COMPARE_OP_GREATER,				// FX::E_CompareOp::Greater
	VK_COMPARE_OP_GREATER_OR_EQUAL,	// FX::E_CompareOp::GreaterEqual
	VK_COMPARE_OP_EQUAL,					// FX::E_CompareOp::Equal
	VK_COMPARE_OP_NOT_EQUAL,			// FX::E_CompareOp::NotEqual
	VK_COMPARE_OP_NEVER,					// FX::E_CompareOp::Never
	VK_COMPARE_OP_ALWAYS					// FX::E_CompareOp::Always
};
const VkCompareOp FXD::GFX::FX::Sampler::GetCompareFunction(const FX::E_CompareOp eMode)
{
	return sVKCompareFunctions[FXD::STD::to_underlying(eMode)];
}

// ------
// E_StencilOp
// -
// ------
const VkStencilOp sVKStencilOps[] =
{
	VK_STENCIL_OP_KEEP,						// FX::E_StencilOp::Keep
	VK_STENCIL_OP_ZERO,						// FX::E_StencilOp::Zero
	VK_STENCIL_OP_REPLACE,					// FX::E_StencilOp::Replace
	VK_STENCIL_OP_INCREMENT_AND_CLAMP,	// FX::E_StencilOp::SaturatedIncrement
	VK_STENCIL_OP_DECREMENT_AND_CLAMP,	// FX::E_StencilOp::SaturatedDecrement
	VK_STENCIL_OP_INCREMENT_AND_WRAP,	// FX::E_StencilOp::Increment
	VK_STENCIL_OP_DECREMENT_AND_WRAP,	// FX::E_StencilOp::Decrement
	VK_STENCIL_OP_INVERT						// FX::E_StencilOp::Invert
};
const VkStencilOp FXD::GFX::FX::Sampler::GetStencilOp(const FX::E_StencilOp eMode)
{
	return sVKStencilOps[FXD::STD::to_underlying(eMode)];
}

// ------
// E_BlendOp
// -
// ------
const VkBlendOp sVKBlendOps[] =
{
	VK_BLEND_OP_ADD,					// FX::E_BlendOp::Add
	VK_BLEND_OP_SUBTRACT,			// FX::E_BlendOp::Subtract
	VK_BLEND_OP_MIN,					// FX::E_BlendOp::Min
	VK_BLEND_OP_MAX,					// FX::E_BlendOp::Max
	VK_BLEND_OP_REVERSE_SUBTRACT,	// FX::E_BlendOp::ReverseSubtract
};
const VkBlendOp FXD::GFX::FX::Sampler::GetBlendOp(const FX::E_BlendOp eMode)
{
	return sVKBlendOps[FXD::STD::to_underlying(eMode)];
}

// ------
// E_BlendFactor
// -
// ------
const VkBlendFactor sVKBlendFactors[] =
{
	VK_BLEND_FACTOR_ZERO,						// FX::E_BlendFactor::Zero
	VK_BLEND_FACTOR_ONE,						// FX::E_BlendFactor::One
	VK_BLEND_FACTOR_SRC_COLOR,					// FX::E_BlendFactor::SourceColor
	VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR,		// FX::E_BlendFactor::InverseSourceColor
	VK_BLEND_FACTOR_SRC_ALPHA,					// FX::E_BlendFactor::SourceAlpha
	VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,		// FX::E_BlendFactor::InverseSourceAlpha
	VK_BLEND_FACTOR_DST_ALPHA,					// FX::E_BlendFactor::DestAlpha
	VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA,		// FX::E_BlendFactor::InverseDestAlpha
	VK_BLEND_FACTOR_DST_COLOR,					// FX::E_BlendFactor::DestColor
	VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR,		// FX::E_BlendFactor::InverseDestColor
	VK_BLEND_FACTOR_CONSTANT_COLOR,				// FX::E_BlendFactor::ConstantBlendFactor
	VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR	// FX::E_BlendFactor::InverseConstantBlendFactor
};
const VkBlendFactor FXD::GFX::FX::Sampler::GetBlendFactor(const FX::E_BlendFactor eMode)
{
	return sVKBlendFactors[FXD::STD::to_underlying(eMode)];
}

/*
// ------
// Shader
// -
// ------
CONSTEXPR const VkShaderStageFlagBits sVKShaderFlag[] =
{
	VK_SHADER_STAGE_VERTEX_BIT,						// E_ShaderType::Vertex
	VK_SHADER_STAGE_GEOMETRY_BIT,						// E_ShaderType::Geometry
	VK_SHADER_STAGE_FRAGMENT_BIT,						// E_ShaderType::Fragment
	VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT,	// E_ShaderType::Domain
	VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT,		// E_ShaderType::Hull
	VK_SHADER_STAGE_COMPUTE_BIT,						// E_ShaderType::Compute
	VK_SHADER_STAGE_VERTEX_BIT							// E_ShaderType::Count
};
VkShaderStageFlagBits FXD::GFX::FX::ShaderF::GetShaderStage(const GFX::FX::E_ShaderType eType)
{
	return sVKShaderFlag[FXD::STD::to_underlying(eType)];
}
*/
#endif //IsGFXVulkan()