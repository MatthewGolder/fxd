// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/Vulkan/GfxAdapterVK.h"
#include "FXDEngine/GFX/FX/Vulkan/SamplerStateVK.h"
#include "FXDEngine/GFX/FX/Vulkan/MappingFXVK.h"
#include "FXDEngine/Core/Algorithm.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// ISamplerState::Impl::Impl
// - 
// ------
ISamplerState::Impl::Impl::Impl(void)
	: m_vkSampler(VK_NULL_HANDLE)
{
}
ISamplerState::Impl::~Impl(void)
{
}

// ------
// ISamplerState
// - 
// ------
ISamplerState::ISamplerState(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

ISamplerState::~ISamplerState(void)
{
}

bool ISamplerState::_create(const FX::SAMPLER_DESC& sampleDesc)
{
	m_samplerDesc = sampleDesc;

	VkSamplerCreateInfo vkSamplerInfo = {};
	GFX::MemZeroVulkanStruct(vkSamplerInfo, VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO);

	vkSamplerInfo.flags = 0;
	vkSamplerInfo.pNext = nullptr;
	vkSamplerInfo.magFilter = FX::Sampler::GetMagFilter(m_samplerDesc.FilterMode);
	vkSamplerInfo.minFilter = FX::Sampler::GetMinFilter(m_samplerDesc.FilterMode);
	vkSamplerInfo.mipmapMode = FX::Sampler::GetMipFilter(m_samplerDesc.FilterMode);

	const bool bSupportsMirrorClampToEdge = true;
	vkSamplerInfo.addressModeU = FX::Sampler::GetAddressMode(m_samplerDesc.AddressModeU, bSupportsMirrorClampToEdge);
	vkSamplerInfo.addressModeV = FX::Sampler::GetAddressMode(m_samplerDesc.AddressModeV, bSupportsMirrorClampToEdge);
	vkSamplerInfo.addressModeW = FX::Sampler::GetAddressMode(m_samplerDesc.AddressModeW, bSupportsMirrorClampToEdge);
	vkSamplerInfo.mipLodBias = m_samplerDesc.MipBias;
	vkSamplerInfo.maxAnisotropy = FXD::STD::clamp< FXD::U32 >(m_samplerDesc.MaxAnisotropy, 0, 16);
	vkSamplerInfo.anisotropyEnable = (m_samplerDesc.MaxAnisotropy > 1);
	vkSamplerInfo.compareEnable = m_samplerDesc.CompareOp != FX::E_CompareOp::Never ? VK_TRUE : VK_FALSE;
	vkSamplerInfo.compareOp = FX::Sampler::GetCompareFunction(m_samplerDesc.CompareOp);
	vkSamplerInfo.minLod = m_samplerDesc.MinMipLevel;
	vkSamplerInfo.maxLod = m_samplerDesc.MaxMipLevel;
	vkSamplerInfo.borderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;

	//VERIFYVULKANRESULT(VulkanRHI::vkCreateSampler(Device.GetInstanceHandle(), &SamplerInfo, nullptr, &Sampler));
	return true;
}

bool ISamplerState::_release(void)
{
	m_samplerDesc = FX::SAMPLER_DESC();

	m_impl->m_vkSampler = VK_NULL_HANDLE;
	return true;
}

void ISamplerState::_attach(void)
{
}

void ISamplerState::_detach(void)
{
}
#endif //IsGFXVulkan()