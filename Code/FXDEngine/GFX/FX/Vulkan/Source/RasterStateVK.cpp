// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/Vulkan/GfxAdapterVK.h"
#include "FXDEngine/GFX/Vulkan/MappingVK.h"
#include "FXDEngine/GFX/FX/Vulkan/RasterStateVK.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// IRasterState::Impl::Impl
// - 
// ------
IRasterState::Impl::Impl::Impl(void)
{
}
IRasterState::Impl::~Impl(void)
{
}

// ------
// IRasterState
// - 
// ------
IRasterState::IRasterState(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IRasterState::~IRasterState(void)
{
}

bool IRasterState::_create(const FX::RASTER_STATE_DESC& rasterDesc)
{
	m_rasterDesc = rasterDesc;

	VkPipelineRasterizationStateCreateInfo vkRasterStateInfo = {};
	GFX::MemZeroVulkanStruct(vkRasterStateInfo, VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO);

	vkRasterStateInfo.pNext = nullptr;
	vkRasterStateInfo.flags = 0;
	vkRasterStateInfo.depthClampEnable = true;
	vkRasterStateInfo.rasterizerDiscardEnable = VK_FALSE;
	vkRasterStateInfo.polygonMode = GFX::Vulkan::GetFillMode(m_rasterDesc.PolygonMode);
	vkRasterStateInfo.cullMode = GFX::Vulkan::GetCullMode(m_rasterDesc.CullMode);
	vkRasterStateInfo.frontFace = VK_FRONT_FACE_CLOCKWISE;

	vkRasterStateInfo.depthBiasEnable = (m_rasterDesc.DepthBias != 0.0f) ? VK_TRUE : VK_FALSE;
	vkRasterStateInfo.depthBiasConstantFactor = m_rasterDesc.DepthBias;
	vkRasterStateInfo.depthBiasSlopeFactor = m_rasterDesc.SlopeScaledDepthBias;
	vkRasterStateInfo.depthBiasClamp = 0.0f;
	vkRasterStateInfo.lineWidth = 1.0f;

	return true;
}

bool IRasterState::_release(void)
{
	m_rasterDesc = FX::RASTER_STATE_DESC();
	return true;
}

void IRasterState::_attach(void)
{
}

void IRasterState::_detach(void)
{
}
#endif //IsGFXVulkan()