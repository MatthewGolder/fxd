// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/Vulkan/GfxAdapterVK.h"
#include "FXDEngine/GFX/FX/Vulkan/ShaderModuleVK.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// IShaderModule::Impl::Impl
// - 
// ------
IShaderModule::Impl::Impl::Impl(void)
{
}
IShaderModule::Impl::~Impl(void)
{
}

// ------
// IShaderModule
// - 
// ------
IShaderModule::IShaderModule(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
	, m_eType(GFX::FX::E_ShaderType::Unknown)
{
}

IShaderModule::~IShaderModule(void)
{
}

bool IShaderModule::_create(const GFX::FX::E_ShaderType eType, const Memory::MemHandle& mem)
{
	m_eType = eType;

	return true;
}

bool IShaderModule::_release(void)
{
	return true;
}

bool IShaderModule::_set_uniform(GFX::UniformBuffer& uniformBuffer)
{
	switch (m_eType)
	{
		case GFX::FX::E_ShaderType::Vertex:
		{
			break;
		}
		case GFX::FX::E_ShaderType::Geometry:
		{
			break;
		}
		case GFX::FX::E_ShaderType::Fragment:
		{
			break;
		}
		default:
		{
			PRINT_ASSERT << "GFX: Shader Buffer Type";
		}
	}
	return true;
}

void IShaderModule::_attach(void)
{
}

void IShaderModule::_detach(void)
{
}

#endif //IsGFXVulkan()