// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/Vulkan/GfxAdapterVK.h"
#include "FXDEngine/GFX/FX/Vulkan/PipelineVK.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// IPipeline::Impl::Impl
// - 
// ------
IPipeline::Impl::Impl::Impl(void)
{
}
IPipeline::Impl::~Impl(void)
{
}

// ------
// IPipeline
// - 
// ------
IPipeline::IPipeline(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IPipeline::~IPipeline(void)
{
}

bool IPipeline::_create(const FX::PIPELINE_DESC& pipelineDesc)
{
	m_pipelineDesc = pipelineDesc;
	return true;
}

bool IPipeline::_release(void)
{
	m_pipelineDesc = FX::PIPELINE_DESC();
	return true;
}

void IPipeline::_attach(void)
{
}

void IPipeline::_detach(void)
{
}
#endif //IsGFXVulkan()