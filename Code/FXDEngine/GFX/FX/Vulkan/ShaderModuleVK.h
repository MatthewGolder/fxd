// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_VULKAN_SHADERVK_H
#define FXDENGINE_GFX_FX_VULKAN_SHADERVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/FX/ShaderModule.h"
#include "FXDEngine/GFX/Vulkan/Vulkan.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::FX::IShaderModule >
			// -
			// ------
			template <>
			class Impl< GFX::FX::IShaderModule >
			{
			public:
				friend class GFX::FX::IShaderModule;

			public:
				Impl(void);
				~Impl(void);

			private:
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_FX_VULKAN_SHADERVK_H