// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/FX/OpenGL/MappingFXOGL.h"

using namespace FXD;
using namespace GFX;

#endif //IsGFXOpenGL()