// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/OpenGL/GfxAdapterOGL.h"
#include "FXDEngine/GFX/FX/OpenGL/DepthStencilStateOGL.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// IDepthStencilState::Impl::Impl
// - 
// ------
IDepthStencilState::Impl::Impl::Impl(void)
{
}
IDepthStencilState::Impl::~Impl(void)
{
}

// ------
// IDepthStencilState
// - 
// ------
IDepthStencilState::IDepthStencilState(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IDepthStencilState::~IDepthStencilState(void)
{
}

bool IDepthStencilState::_create(const FX::DEPTHSTENCIL_STATE_DESC& dsStateDesc)
{
	m_dsStateDesc = dsStateDesc;
	return true;
}

bool IDepthStencilState::_release(void)
{
	m_dsStateDesc = FX::DEPTHSTENCIL_STATE_DESC();
	return true;
}

void IDepthStencilState::_attach(void)
{
}

void IDepthStencilState::_detach(void)
{
}
#endif //IsGFXOpenGL()