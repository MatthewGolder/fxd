// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/OpenGL/GfxAdapterOGL.h"
#include "FXDEngine/GFX/FX/OpenGL/SamplerStateOGL.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// ISamplerState::Impl::Impl
// - 
// ------
ISamplerState::Impl::Impl::Impl(void)
{
}
ISamplerState::Impl::~Impl(void)
{
}

// ------
// ISamplerState
// - 
// ------
ISamplerState::ISamplerState(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

ISamplerState::~ISamplerState(void)
{
}

bool ISamplerState::_create(const FX::SAMPLER_DESC& sampleDesc)
{
	m_samplerDesc = sampleDesc;
	return true;
}

bool ISamplerState::_release(void)
{
	m_samplerDesc = FX::SAMPLER_DESC();
	return true;
}

void ISamplerState::_attach(void)
{
}

void ISamplerState::_detach(void)
{
}
#endif //IsGFXOpenGL()