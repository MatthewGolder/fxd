// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/OpenGL/GfxAdapterOGL.h"
#include "FXDEngine/GFX/FX/OpenGL/BlendStateOGL.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// IBlendState::Impl::Impl
// - 
// ------
IBlendState::Impl::Impl::Impl(void)
{
}
IBlendState::Impl::~Impl(void)
{
}

// ------
// IBlendState
// - 
// ------
IBlendState::IBlendState(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IBlendState::~IBlendState(void)
{
}

bool IBlendState::_create(const FX::BLEND_STATE_DESC& blendDesc)
{
	m_blendDesc = blendDesc;
	return true;
}

bool IBlendState::_release(void)
{
	m_blendDesc = FX::BLEND_STATE_DESC();
	return true;
}

void IBlendState::_attach(void)
{
}

void IBlendState::_detach(void)
{
}
#endif //IsGFXOpenGL()