// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/OpenGL/GfxAdapterOGL.h"
#include "FXDEngine/GFX/OpenGL/HWBufferOGL.h"
#include "FXDEngine/GFX/FX/OpenGL/ShaderModuleOGL.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// IShaderModule::Impl::Impl
// - 
// ------
IShaderModule::Impl::Impl::Impl(void)
	: m_glShader(0)
{
}
IShaderModule::Impl::~Impl(void)
{
}

// ------
// IShaderModule
// - 
// ------
IShaderModule::IShaderModule(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
	, m_eType(FX::E_ShaderType::Unknown)
{
}

IShaderModule::~IShaderModule(void)
{
}

bool IShaderModule::_create(const FX::E_ShaderType eType, const Memory::MemHandle& mem)
{
	m_eType = eType;

	switch (m_eType)
	{
		case FX::E_ShaderType::Vertex:
		{
			m_impl->m_glShader = glCreateShader(GL_VERTEX_SHADER);
			break;
		}
		/*
		case FX::E_ShaderType::Geometry:
		{
		m_impl->m_glShader = glCreateShader(GL_GEOMETRY_SHADER);
		break;
		}
		*/
		case FX::E_ShaderType::Fragment:
		{
			m_impl->m_glShader = glCreateShader(GL_FRAGMENT_SHADER);
			break;
		}
		default:
		{
			PRINT_ASSERT << "GFX: Shader Buffer Type";
		}
	}
	const GLchar* const* pGLChar = (const GLchar* const*)Memory::MemHandle::LockGuard(mem).get_mem();
	glShaderSource(m_impl->m_glShader, 1, pGLChar, 0);
	glCompileShader(m_impl->m_glShader);

	/*
	// Create the program
	p = glCreateProgram();

	// Attach shaders to program
	glAttachShader(p, v);
	glAttachShader(p, g);
	glAttachShader(p, f);

	// Link and set program to use
	glLinkProgram(p);
	glUseProgram(p);
	*/

	return true;
}

bool IShaderModule::_release(void)
{
	glDeleteShader(m_impl->m_glShader);
	return true;
}

bool IShaderModule::_set_uniform(GFX::UniformBuffer& uniformBuffer)
{
	switch (m_eType)
	{
		case FX::E_ShaderType::Vertex:
		{
			break;
		}
		case FX::E_ShaderType::Geometry:
		{
			break;
		}
		case FX::E_ShaderType::Fragment:
		{
			break;
		}
		default:
		{
			PRINT_ASSERT << "GFX: Shader Buffer Type";
		}
	}
	return true;
}

void IShaderModule::_attach(void)
{
	switch (m_eType)
	{
		case FX::E_ShaderType::Vertex:
		{
			break;
		}
		case FX::E_ShaderType::Geometry:
		{
			break;
		}
		case FX::E_ShaderType::Fragment:
		{
			break;
		}
		default:
		{
			PRINT_ASSERT << "GFX: Shader Buffer Type";
		}
	}
}

void IShaderModule::_detach(void)
{
	switch (m_eType)
	{
		case FX::E_ShaderType::Vertex:
		{
			break;
		}
		case FX::E_ShaderType::Geometry:
		{
			break;
		}
		case FX::E_ShaderType::Fragment:
		{
			break;
		}
		default:
		{
			PRINT_ASSERT << "GFX: Shader Buffer Type";
		}
	}
}
#endif //IsGFXOpenGL()