// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/OpenGL/GfxAdapterOGL.h"
#include "FXDEngine/GFX/FX/OpenGL/RasterStateOGL.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// IRasterState::Impl::Impl
// - 
// ------
IRasterState::Impl::Impl::Impl(void)
{
}
IRasterState::Impl::~Impl(void)
{
}

// ------
// IRasterState
// - 
// ------
IRasterState::IRasterState(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IRasterState::~IRasterState(void)
{
}

bool IRasterState::_create(const FX::RASTER_STATE_DESC& rasterDesc)
{
	m_rasterDesc = rasterDesc;
	return true;
}

bool IRasterState::_release(void)
{
	m_rasterDesc = FX::RASTER_STATE_DESC();
	return true;
}

void IRasterState::_attach(void)
{
}

void IRasterState::_detach(void)
{
}
#endif //IsGFXOpenGL()