// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_OPENGL_SHADERFXOGL_H
#define FXDENGINE_GFX_FX_OPENGL_SHADERFXOGL_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/FX/ShaderModule.h"
#include "FXDEngine/GFX/OpenGL/OpenGL.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::FX::IShaderModule >
			// -
			// ------
			template <>
			class Impl< GFX::FX::IShaderModule >
			{
			public:
				friend class GFX::FX::IShaderModule;

			public:
				Impl(void);
				~Impl(void);

			protected:
				GLuint m_glShader;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXOpenGL()
#endif //FXDENGINE_GFX_FX_OPENGL_SHADERFXOGL_H