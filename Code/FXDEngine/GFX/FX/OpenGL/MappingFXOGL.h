// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_OPENGL_MAPPINGFXOGL_H
#define FXDENGINE_GFX_FX_OPENGL_MAPPINGFXOGL_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/FX/Types.h"
#include "FXDEngine/GFX/OpenGL/OpenGL.h"

namespace FXD
{
	namespace GFX
	{
		namespace FX
		{
			namespace Sampler
			{
			}
		} //namespace FX
	} //namespace GFX
} //namespace FXD
#endif //IsGFXOpenGL()
#endif //FXDENGINE_GFX_FX_OPENGL_MAPPINGFXOGL_H