// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_OPENGL_BLENDSTATEOGL_H
#define FXDENGINE_GFX_FX_OPENGL_BLENDSTATEOGL_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/FX/BlendState.h"
#include "FXDEngine/GFX/OpenGL/OpenGL.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::FX::IBlendState >
			// -
			// ------
			template <>
			class Impl< GFX::FX::IBlendState >
			{
			public:
				friend class GFX::FX::IBlendState;

			public:
				Impl(void);
				~Impl(void);

			protected:
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXOpenGL()
#endif //FXDENGINE_GFX_FX_OPENGL_BLENDSTATEOGL_H