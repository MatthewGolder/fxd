// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_OPENGL_DEPTHSTENCILSTATEOGL_H
#define FXDENGINE_GFX_FX_OPENGL_DEPTHSTENCILSTATEOGL_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/FX/DepthStencilState.h"
#include "FXDEngine/GFX/OpenGL/OpenGL.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::FX::IDepthStencilState >
			// -
			// ------
			template <>
			class Impl< GFX::FX::IDepthStencilState >
			{
			public:
				friend class GFX::FX::IDepthStencilState;

			public:
				Impl(void);
				~Impl(void);

			protected:
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXOpenGL()
#endif //FXDENGINE_GFX_FX_OPENGL_DEPTHSTENCILSTATEOGL_H