// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_OPENGL_RASTERSTATEOGL_H
#define FXDENGINE_GFX_FX_OPENGL_RASTERSTATEOGL_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/FX/RasterState.h"
#include "FXDEngine/GFX/OpenGL/OpenGL.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::FX::IRasterState >
			// -
			// ------
			template <>
			class Impl< GFX::FX::IRasterState >
			{
			public:
				friend class GFX::FX::IRasterState;

			public:
				Impl(void);
				~Impl(void);

			protected:
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXOpenGL()
#endif //FXDENGINE_GFX_FX_OPENGL_RASTERSTATEOGL_H