// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_OPENGL_SAMLERSTATEOGL_H
#define FXDENGINE_GFX_FX_OPENGL_SAMLERSTATEOGL_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/FX/SamplerState.h"
#include "FXDEngine/GFX/OpenGL/OpenGL.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::FX::ISamplerState >
			// -
			// ------
			template <>
			class Impl< GFX::FX::ISamplerState >
			{
			public:
				friend class GFX::FX::ISamplerState;

			public:
				Impl(void);
				~Impl(void);

			protected:
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXOpenGL()
#endif //FXDENGINE_GFX_FX_OPENGL_SAMLERSTATEOGL_H