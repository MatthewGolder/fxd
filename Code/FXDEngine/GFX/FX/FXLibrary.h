// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_FXLIBRARY_H
#define FXDENGINE_GFX_FX_FXLIBRARY_H

#include "FXDEngine/GFX/FX/Types.h"
#include "FXDEngine/Job/Future.h"
#include "FXDEngine/IO/FileCache.h"

namespace FXD
{
	namespace GFX
	{
		namespace FX
		{
			// ------
			// IFXLibrary
			// -
			// ------
			class IFXLibrary : public Core::NonCopyable
			{
			public:
				IFXLibrary(GFX::IGfxAdapter* pAdapter);
				virtual ~IFXLibrary(void);

				//Job::Future< FX::Program > load_material(const IO::Path& ioPath);
				Job::Future< FX::ShaderResource > load_shader(const IO::Path& ioPath);

				GET_REF_W(IO::FileCache< FX::ShaderResource >, shaderCache, shader_cache);

			private:
				GFX::IGfxAdapter* m_pAdapter;

				IO::FileCache< FX::ShaderResource > m_shaderCache;
			};
		} //namespace FX
	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_FX_FXLIBRARY_H