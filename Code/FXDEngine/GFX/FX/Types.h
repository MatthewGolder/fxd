// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_TYPES_H
#define FXDENGINE_GFX_FX_TYPES_H

#include "FXDEngine/GFX/Types.h"

namespace FXD
{
	namespace GFX
	{
		namespace FX
		{
			// ------
			// E_FXDataType
			// -
			// ------
			enum class E_FXDataType : FXD::U16
			{
				Bool = 0,
				Int,
				Float,
				Float2,
				Float3,
				Float4,
				Float3x3,
				Float4x3,
				Float4x4,
				Count,
				Unknown = 0xffff
			};

			// ------
			// E_SemanticType
			// -
			// ------
			enum class E_SemanticType : FXD::U16
			{
				Position = 0,
				SV_Position,
				Color0,
				Color1,
				Normal0,
				Normal1,
				Binormal0,
				Binormal1,
				Tangent0,
				Tangent1,
				TexCoord0,
				TexCoord1,
				TexCoord2,
				TexCoord3,
				TexCoord4,
				TexCoord5,
				TexCoord6,
				TexCoord7,
				TexCoord8,
				Count,
				Unknown = 0xffff
			};

			// ------
			// E_FilterMode
			// -
			// ------
			enum class E_FilterMode : FXD::U16
			{
				Point,
				Bilinear,
				Trilinear,
				AnisotropicPoint,
				AnisotropicLinear,
				Count,
				Unknown = 0xffff
			};

			// ------
			// E_AddressMode
			// -
			// ------
			enum class E_AddressMode : FXD::U16
			{
				Wrap,
				Mirror,
				Clamp,
				ClampBorder,
				Count,
				Unknown = 0xffff
			};

			// ------
			// E_CompareOp
			// -
			// ------
			enum class E_CompareOp : FXD::U16
			{
				Less,
				LessEqual,
				Greater,
				GreaterEqual,
				Equal,
				NotEqual,
				Never,
				Always,
				Count,
				Unknown = 0xffff
			};

			// ------
			// E_StencilOp
			// -
			// ------
			enum class E_StencilOp : FXD::U16
			{
				Keep,
				Zero,
				Replace,
				SaturatedIncrement,
				SaturatedDecrement,
				Increment,
				Decrement,
				Invert,
				Count,
				Unknown = 0xffff
			};

			// ------
			// E_BlendOp
			// -
			// ------
			enum class E_BlendOp : FXD::U16
			{
				Add,
				Subtract,
				Min,
				Max,
				ReverseSubtract,
				Count,
				Unknown = 0xffff
			};

			// ------
			// E_BlendFactor
			// -
			// ------
			enum class E_BlendFactor : FXD::U16
			{
				Zero,
				One,
				SourceColor,
				InverseSourceColor,
				SourceAlpha,
				InverseSourceAlpha,
				DestAlpha,
				InverseDestAlpha,
				DestColor,
				InverseDestColor,
				ConstantBlendFactor,
				InverseConstantBlendFactor,
				Count,
				Unknown = 0xffff
			};

			enum E_ColorWriteMask
			{
				CW_NONE = 0,
				CW_RED = 0x01,
				CW_GREEN = 0x02,
				CW_BLUE = 0x04,
				CW_ALPHA = 0x08,

				CW_RGB = CW_RED | CW_GREEN | CW_BLUE,
				CW_RGBA = CW_RED | CW_GREEN | CW_BLUE | CW_ALPHA,
				CW_RG = CW_RED | CW_GREEN,
				CW_BA = CW_BLUE | CW_ALPHA
			};

			// ------
			// E_ShaderType
			// -
			// ------
			enum class E_ShaderType
			{
				Vertex = 0,		// Vertex program.
				Geometry,		// Geometry program.
				Fragment,		// Fragment(pixel) program.
				//Domain,		// Domain (tesselation evaluation) program.
				//Hull,			// Hull (tesselation control) program.
				//Compute,		// Compute program.
				Count,
				Unknown = 0xffff
			};

			class IStateManager;
			typedef std::shared_ptr< FX::IStateManager > StateManager;

			class IBlendState;
			class IDepthStencilState;
			class IRasterState;
			class ISamplerState;
			class IShaderModule;
			class IPipeline;
			typedef std::shared_ptr< FX::IBlendState > BlendState;
			typedef std::shared_ptr< FX::IDepthStencilState > DepthStencilState;
			typedef std::shared_ptr< FX::IRasterState > RasterState;
			typedef std::shared_ptr< FX::ISamplerState > SamplerState;
			typedef std::shared_ptr< FX::IShaderModule > ShaderModule;
			typedef std::shared_ptr< FX::IPipeline > Pipeline;
			
			class IShaderResource;
			typedef std::shared_ptr< FX::IShaderResource > ShaderResource;

			class IMaterial;
			typedef std::shared_ptr< FX::IMaterial > Material;

			class IFXLibrary;
			typedef std::shared_ptr< FX::IFXLibrary > FXLibrary;

		} //namespace FX
	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_FX_TYPES_H