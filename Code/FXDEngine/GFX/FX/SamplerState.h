// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_SAMPLERSTATE_H
#define FXDENGINE_GFX_FX_SAMPLERSTATE_H

#include "FXDEngine/Core/Colour.h"
#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/GFX/FX/Types.h"
#include "FXDEngine/Job/Future.h"
#include "FXDEngine/Math/Limits.h"

namespace FXD
{
	namespace GFX
	{
		namespace FX
		{
			struct SAMPLER_DESC
			{
				FX::E_FilterMode FilterMode = FX::E_FilterMode::Unknown;
				FX::E_AddressMode AddressModeU = FX::E_AddressMode::Unknown;
				FX::E_AddressMode AddressModeV = FX::E_AddressMode::Unknown;
				FX::E_AddressMode AddressModeW = FX::E_AddressMode::Unknown;
				FX::E_CompareOp CompareOp = FX::E_CompareOp::Never;
				FXD::F32 MipBias = 0;
				FXD::U32 MaxAnisotropy = 1;
				FXD::F32 MinMipLevel = 0;
				FXD::F32 MaxMipLevel = FXD::Math::MAX_F32;
				Core::ColourRGBA Border = Core::ColourRGBA::Null;
			};

			// ------
			// ISamplerState
			// -
			// ------
			class ISamplerState : public Core::HasImpl< FX::ISamplerState, 64 >, public Core::NonCopyable
			{
			public:
				ISamplerState(GFX::IGfxAdapter* pAdapter);
				virtual ~ISamplerState(void);

				Job::Future< bool > create(const FX::SAMPLER_DESC& sampleDesc);
				Job::Future< bool > release(void);

				Job::Future< void > attach(void);
				Job::Future< void > detach(void);

			protected:
				bool _create(const FX::SAMPLER_DESC& sampleDesc);
				bool _release(void);

				void _attach(void);
				void _detach(void);

			private:
				GFX::IGfxAdapter* m_pAdapter;
				FX::SAMPLER_DESC m_samplerDesc;
			};

		} //namespace FX
	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_FX_SAMPLERSTATE_H