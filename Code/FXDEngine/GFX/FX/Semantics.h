// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_SEMANTICS_H
#define FXDENGINE_GFX_FX_SEMANTICS_H

#include "FXDEngine/Container/Array.h"
#include "FXDEngine/Core/String.h"
#include "FXDEngine/GFX/FX/Types.h"

namespace FXD
{
	namespace GFX
	{
		namespace FX
		{
			// ------
			// TextureDef
			// -
			// ------
			struct TextureDef
			{
				Core::String8 Name;
				Core::String8 Sampler;
				GFX::E_ImageType Type = GFX::E_ImageType::Unknown;
				FXD::U16 RegisterSlot = 0;
			};

			// ------
			// ConstantBufferDef
			// -
			// ------
			struct ConstantBufferDef
			{
				struct Param
				{
					Core::String8 Name;
					FX::E_FXDataType DataType = FX::E_FXDataType::Unknown;
				};

				Core::String8 Name;
				FXD::U16 RegisterSlot = 0;
				Container::Vector< Param > Params;
			};

			// ------
			// VaryingListDef
			// -
			// ------
			struct VaryingListDef
			{
				struct Param
				{
					Core::String8 Name;
					FX::E_FXDataType DataType = FX::E_FXDataType::Unknown;
					FX::E_SemanticType SemanticType = FX::E_SemanticType::Unknown;
				};
				FXD::Core::String8 Name;
				Container::Array< Param, kMaxVertexElements > Params;
			};
		} //namespace FX
	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_FX_SEMANTICS_H