// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_SHADER_H
#define FXDENGINE_GFX_FX_SHADER_H

#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/Core/String.h"
#include "FXDEngine/GFX/FX/Types.h"
#include "FXDEngine/GFX/VertexDec.h"
#include "FXDEngine/IO/FileCache.h"
#include "FXDEngine/Job/Future.h"
#include "FXDEngine/Math/Matrix3.h"
#include "FXDEngine/Math/Matrix4.h"

namespace FXD
{
	namespace GFX
	{
		namespace FX
		{
			struct TEMP_VS_CONSTANT_BUFFER
			{
				Math::Matrix4F mWorld;
				Math::Matrix4F mView;
				Math::Matrix4F mProjection;
			};

			// ------
			// IShaderModule
			// -
			// ------
			class IShaderModule : public Core::HasImpl< FX::IShaderModule, 64 >, public Core::NonCopyable
			{
			public:
				EXPLICIT IShaderModule(GFX::IGfxAdapter* pAdapter);
				virtual ~IShaderModule(void);

				Job::Future< bool > create(const FX::E_ShaderType eType, const Memory::MemHandle& mem);
				Job::Future< bool > release(void);

				Job::Future< bool > set_uniform(GFX::UniformBuffer& buffer);
				Job::Future< void > attach(void);
				Job::Future< void > detach(void);

			protected:
				bool _create(const FX::E_ShaderType eType, const Memory::MemHandle& mem);
				bool _release(void);

				bool _set_uniform(GFX::UniformBuffer& uniformBuffer);
				void _attach(void);
				void _detach(void);

			protected:
				GFX::IGfxAdapter* m_pAdapter;
				FX::E_ShaderType m_eType;
			};

			// ------
			// IShaderResource
			// -
			// ------
			class IShaderResource FINAL : public IO::IAsset
			{
			public:
				IShaderResource(GFX::IGfxAdapter* pAdapter, Core::StreamIn& streamIn, const Core::String8& strFileName);
				~IShaderResource(void);

				bool is_platform_supported(void) const FINAL;
				const Core::StringView8 get_asset_type(void) const FINAL { return UTF_8("ShaderResource"); }

				GET_REF_W(FXD::GFX::VertexDecleration, vertexDecleration, vertex_decleration);
				GET_REF_W(FXD::GFX::UniformBuffer, uniform, uniform);

			private:
				FX::ShaderModule m_shaders[FXD::STD::to_underlying(FX::E_ShaderType::Count)];
				GFX::VertexDecleration m_vertexDecleration;
				GFX::UniformBuffer m_uniform;
			};

		} //namespace FX
	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_FX_SHADER_H