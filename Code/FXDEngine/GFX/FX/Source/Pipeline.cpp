// Creator - MatthewGolder
#include "FXDEngine/GFX/FX/Pipeline.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// IPipeline
// - 
// ------
Job::Future< bool > IPipeline::create(const FX::PIPELINE_DESC& pipelineDesc)
{
	auto funcRef = [&](const FX::PIPELINE_DESC& pipelineDesc) { return _create(pipelineDesc); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(pipelineDesc); });
}

Job::Future< bool > IPipeline::release(void)
{
	auto funcRef = [&](void) { return _release(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< void > IPipeline::attach(void)
{
	auto funcRef = [&](void) { _attach(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< void > IPipeline::detach(void)
{
	auto funcRef = [&](void) { _detach(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}