// Creator - MatthewGolder
#include "FXDEngine/GFX/FX/BlendState.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// BLEND_STATE_DESC
// - 
// ------
bool BLEND_STATE_DESC::operator==(const BLEND_STATE_DESC& rhs) const
{
	bool bEquals = (UseIndependentRenderTargetBlendStates == rhs.UseIndependentRenderTargetBlendStates);
	if (bEquals)
	{
		for (FXD::U16 n1 = 0; n1 < GFX::kMaxMultipleRenderTargets; n1++)
		{
			bEquals &= RenderTargets[n1] == rhs.RenderTargets[n1];
		}
	}
	return (bEquals);
}

// ------
// BLEND_STATE_DESC::RT_DESC
// - 
// ------
bool BLEND_STATE_DESC::RT_DESC::operator==(const BLEND_STATE_DESC::RT_DESC& rhs) const
{
	return
		(ColorBlendOp == rhs.ColorBlendOp) &&
		(ColorSrcBlend == rhs.ColorSrcBlend) &&
		(ColorDestBlend == rhs.ColorDestBlend) &&
		(AlphaBlendOp == rhs.AlphaBlendOp) &&
		(AlphaSrcBlend == rhs.AlphaSrcBlend) &&
		(AlphaDestBlend == rhs.AlphaDestBlend) &&
		(ColorWriteMask == rhs.ColorWriteMask);
}

// ------
// IBlendState
// - 
// ------
Job::Future< bool > IBlendState::create(const FX::BLEND_STATE_DESC& blendDesc)
{
	auto funcRef = [&](const FX::BLEND_STATE_DESC& blendDesc) { return _create(blendDesc); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(blendDesc); });
}

Job::Future< bool > IBlendState::release(void)
{
	auto funcRef = [&](void) { return _release(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< void > IBlendState::attach(void)
{
	auto funcRef = [&](void) { _attach(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< void > IBlendState::detach(void)
{
	auto funcRef = [&](void) { _detach(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}