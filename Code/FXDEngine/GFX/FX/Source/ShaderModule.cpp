// Creator - MatthewGolder
#include "FXDEngine/GFX/FX/MappingFX.h"
#include "FXDEngine/GFX/GfxAdapter.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/FX/ShaderModule.h"
#include "FXDEngine/GFX/FX/ShaderFileDef.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// IShaderModule
// - 
// ------
Job::Future< bool > IShaderModule::create(const FX::E_ShaderType eType, const Memory::MemHandle& mem)
{
	auto funcRef = [&](const FX::E_ShaderType eType, const Memory::MemHandle& mem) { return _create(eType, mem); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(eType, mem); });
}

Job::Future< bool > IShaderModule::release(void)
{
	auto funcRef = [&](void) { return _release(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< bool > IShaderModule::set_uniform(GFX::UniformBuffer& buffer)
{
	auto funcRef = [&](GFX::UniformBuffer buffer) { return _set_uniform(buffer); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(buffer); });
}

Job::Future< void > IShaderModule::attach(void)
{
	auto funcRef = [&](void) { _attach(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< void > IShaderModule::detach(void)
{
	auto funcRef = [&](void) { _detach(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

// ------
// IShaderResource
// - 
// ------
IShaderResource::IShaderResource(GFX::IGfxAdapter* pAdapter, Core::StreamIn& streamIn, const Core::String8& strFileName)
	: IAsset(strFileName)
{
	FX::ShaderFileTagCB callback;
	IO::StreamParser streamParser(streamIn);
	IO::XMLSaxParser::parse(streamParser, callback);
	FX::ShaderFileDef& sfd = callback.m_FXFileDef;

	Memory::MemHandle mhVS = Core::decode_string(sfd.m_vertexShader);
	Memory::MemHandle mhPS = Core::decode_string(sfd.m_pixelShader);

	// Construct
	m_shaders[FXD::STD::to_underlying(FX::E_ShaderType::Vertex)] = std::make_shared< FX::IShaderModule >(pAdapter);
	//m_shaders[FXD::STD::to_underlying(FX::E_ShaderType::Geometry)] = std::make_shared< FX::IShaderModule >(pAdapter);
	m_shaders[FXD::STD::to_underlying(FX::E_ShaderType::Fragment)] = std::make_shared< FX::IShaderModule >(pAdapter);
	m_vertexDecleration = std::make_shared< GFX::IVertexDecleration >(pAdapter);

	// Create
	m_shaders[FXD::STD::to_underlying(FX::E_ShaderType::Vertex)]->create(FX::E_ShaderType::Vertex, mhVS);
	//m_shaders[FXD::STD::to_underlying(FX::E_ShaderType::Geometry)]->create(FX::E_ShaderType::Fragment, mhPS);
	m_shaders[FXD::STD::to_underlying(FX::E_ShaderType::Fragment)]->create(FX::E_ShaderType::Fragment, mhPS);
	m_vertexDecleration->create(sfd.m_vShaderIn, mhVS);

	FXD::U32 nSizeT = 0;
	fxd_for(const auto& itr, sfd.m_constantBuffers.begin()->Params)
	{
		nSizeT += Funcs::ToBytes(itr.DataType);
	}
	m_uniform = pAdapter->create_uniform_buffer((GFX::E_BufferUsageFlag::BIND_UNIFORM_BUFFER | GFX::E_BufferUsageFlag::USAGE_CPU_WRITE), nSizeT, 1).get();
}

IShaderResource::~IShaderResource(void)
{
}

bool IShaderResource::is_platform_supported(void) const
{
	return true;
}