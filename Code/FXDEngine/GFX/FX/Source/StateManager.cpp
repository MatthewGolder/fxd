// Creator - MatthewGolder
#include "FXDEngine/GFX/FX/StateManager.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/FX/BlendState.h"
#include "FXDEngine/GFX/FX/DepthStencilState.h"
#include "FXDEngine/GFX/FX/RasterState.h"
#include "FXDEngine/GFX/FX/SamplerState.h"
#include "FXDEngine/GFX/FX/Pipeline.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Job/Async.h"
#include <3rdParty/xxHash/xxhash.h>

using namespace FXD;
using namespace GFX;
using namespace FX;

const FXD::U64 kStateSeed = 100;

// ------
// IStateManager
// - 
// ------
IStateManager::IStateManager(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}
IStateManager::~IStateManager(void)
{
}

Job::Future< bool > IStateManager::create(void)
{
	auto funcRef = [&](void) { return _create(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< bool > IStateManager::release(void)
{
	auto funcRef = [&](void) { return _release(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

const FX::SamplerState& IStateManager::get_default_sampler_state(void) const
{
	return m_defaultSamplerState;
}

const FX::BlendState& IStateManager::get_default_blend_state(void) const
{
	return m_defaultBlendState;
}

const FX::RasterState& IStateManager::get_default_raster_state(void) const
{
	return m_defaultRasterState;
}

const FX::DepthStencilState& IStateManager::get_default_depth_stencil_state(void) const
{
	return m_defaultDepthStencilState;
}

bool IStateManager::_create(void)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	m_defaultSamplerState = _create_sampler_state(FX::SAMPLER_DESC());
	m_defaultBlendState = _create_blend_state(FX::BLEND_STATE_DESC());
	m_defaultRasterState = _create_raster_state(FX::RASTER_STATE_DESC());
	m_defaultDepthStencilState = _create_depth_stencil_state(FX::DEPTHSTENCIL_STATE_DESC());

	return true;
}

bool IStateManager::_release(void)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");
	return true;
}

FX::SamplerState IStateManager::_create_sampler_state(const FX::SAMPLER_DESC& sampleDesc)
{
	const FXD::HashValue retHash = XXH64(&sampleDesc, (sizeof(FX::SAMPLER_DESC)), kStateSeed);

	FX::SamplerState state = _find_sampler_state(retHash);
	if (state == nullptr)
	{
		state = std::make_shared< FX::ISamplerState >(m_pAdapter);
		if (!state->create(sampleDesc).get())
		{
			return nullptr;
		}
		m_samplerStateCache[retHash] = state;
	}
	return state;
}

FX::BlendState IStateManager::_create_blend_state(const FX::BLEND_STATE_DESC& blendDesc)
{
	const FXD::HashValue retHash = XXH64(&blendDesc, (sizeof(FX::BLEND_STATE_DESC)), kStateSeed);

	FX::BlendState state = _find_blend_state(retHash);
	if (state == nullptr)
	{
		state = std::make_shared< FX::IBlendState >(m_pAdapter);
		if (!state->create(blendDesc).get())
		{
			return nullptr;
		}
		m_blendStateCache[retHash] = state;
	}
	return state;
}

FX::RasterState IStateManager::_create_raster_state(const FX::RASTER_STATE_DESC& rasterDesc)
{
	const FXD::HashValue retHash = XXH64(&rasterDesc, (sizeof(FX::RASTER_STATE_DESC)), kStateSeed);

	FX::RasterState state = _find_raster_state(retHash);
	if (state == nullptr)
	{
		state = std::make_shared< FX::IRasterState >(m_pAdapter);
		if (!state->create(rasterDesc).get())
		{
			return nullptr;
		}
		m_rasterStateCache[retHash] = state;
	}
	return state;
}

FX::DepthStencilState IStateManager::_create_depth_stencil_state(const FX::DEPTHSTENCIL_STATE_DESC& dsStateDesc)
{
	const FXD::HashValue retHash = XXH64(&dsStateDesc, (sizeof(FX::DEPTHSTENCIL_STATE_DESC)), kStateSeed);

	FX::DepthStencilState state = _find_depth_stencil_state(retHash);
	if (state == nullptr)
	{
		state = std::make_shared< FX::IDepthStencilState >(m_pAdapter);
		if (!state->create(dsStateDesc).get())
		{
			return nullptr;
		}
		m_depthStencilStateCache[retHash] = state;
	}
	return state;
}

FX::Pipeline IStateManager::_create_pipeline_state(const FX::PIPELINE_DESC& pipelineDesc)
{
	FX::Pipeline state = std::make_shared< FX::IPipeline >(m_pAdapter);
	if (!state->create(pipelineDesc).get())
	{
		return nullptr;
	}
	return state;
}

FX::SamplerState IStateManager::_find_sampler_state(const FXD::HashValue sampleStateHash) const
{
	FX::SamplerState samplerState = nullptr;
	m_samplerStateCache.try_get_value(sampleStateHash, samplerState);
	return samplerState;
}

FX::BlendState IStateManager::_find_blend_state(const FXD::HashValue blendStateHash) const
{
	FX::BlendState blendState = nullptr;
	m_blendStateCache.try_get_value(blendStateHash, blendState);
	return blendState;
}

FX::RasterState IStateManager::_find_raster_state(const FXD::HashValue rasterStateHash) const
{
	FX::RasterState rasterState = nullptr;
	m_rasterStateCache.try_get_value(rasterStateHash, rasterState);
	return rasterState;
}

FX::DepthStencilState IStateManager::_find_depth_stencil_state(const FXD::HashValue dsStateHash) const
{
	FX::DepthStencilState depthStencilState = nullptr;
	m_depthStencilStateCache.try_get_value(dsStateHash, depthStencilState);
	return depthStencilState;
}