// Creator - MatthewGolder
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/GfxSystem.h"
#include "FXDEngine/GFX/Mapping.h"
#include "FXDEngine/GFX/FX/FXLibrary.h"
#include "FXDEngine/GFX/FX/ShaderModule.h"
#include "FXDEngine/IO/IODevice.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOSystem.h"
#include "FXDEngine/Job/Async.h"
#include "FXDEngine/Memory/StreamMemHandle.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

/*
FX::ShaderResource LoadMaterialFunctor(const IO::Path& ioPath, GFX::IGfxAdapter* pAdapter)
{
	// 1. Open the shader file.
	Core::StreamIn stream = FXD::FXDIO()->io_system()->io_device()->file_open_memory_stream(ioPath, false).get();
	Core::String8 strFileName = ioPath.filename();
	Core::String8 strFileExt = ioPath.extension();

	// 2. Parse the sound file.
	if ((strFileExt.ends_with_no_case(UTF_8(".shaderDX11"))) || (strFileExt.ends_with_no_case(UTF_8(".shaderOpenGL"))) || (strFileExt.ends_with_no_case(UTF_8(".shaderVulkan"))))
	{
		return std::make_shared< FX::IShaderResource >(pAdapter, stream, strFileName);
	}
	else
	{
		PRINT_WARN << "GFX: Unknown file format tag";
	}
	return FX::ShaderResource();
}
*/

FX::ShaderResource LoadShaderFunctor(const IO::Path& ioPath, GFX::IGfxAdapter* pAdapter)
{
	// 1. Open the shader file.
	Core::StreamIn stream = FXD::FXDIO()->io_system()->io_device()->file_open_memory_stream(ioPath, false, { }).get();
	Core::String8 strFileName = ioPath.filename();
	Core::String8 strFileExt = ioPath.extension();

	// 2. Parse the sound file.
	if ((strFileExt.ends_with_no_case(UTF_8(".shaderDX11"))) || (strFileExt.ends_with_no_case(UTF_8(".shaderOpenGL"))) || (strFileExt.ends_with_no_case(UTF_8(".shaderVulkan"))))
	{
		return std::make_shared< FX::IShaderResource >(pAdapter, stream, strFileName);
	}
	else
	{
		PRINT_WARN << "GFX: Unknown file format tag";
	}
	return FX::ShaderResource();
}

// ------
// IFXLibrary
// - 
// ------
IFXLibrary::IFXLibrary(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}
IFXLibrary::~IFXLibrary(void)
{
}

/*
Job::Future< FX::Program > IFXLibrary::load_material(const IO::Path& ioPath)
{
	auto funcRef = [&](const IO::Path& ioPath)
	{
		IO::Path ioPathEXP = ioPath.parent() / (ioPath.filename() + FuncsGfx::ToString(FXDGFX()->gfx_system()->gfx_api()->gfxApiType()));
		return fxCache().load(ioPathEXP, LoadMaterialFunctor, m_pAdapter);
	};

	return Job::Async(FXDGFX(), [=]() { return funcRef(ioPath); });
}
*/

Job::Future< FX::ShaderResource > IFXLibrary::load_shader(const IO::Path& ioPath)
{
	auto funcRef = [&](const IO::Path& ioPath)
	{
		IO::Path ioPathEXP = ioPath.parent_path() / (ioPath.filename().string() + GFX::Api::ToString(FXDGFX()->gfx_system()->gfx_api()->gfxApiType()).c_str()).c_str();
		return shader_cache().load(ioPathEXP, LoadShaderFunctor, m_pAdapter);
	};

	return Job::Async(FXDGFX(), [=]() { return funcRef(ioPath); });
}