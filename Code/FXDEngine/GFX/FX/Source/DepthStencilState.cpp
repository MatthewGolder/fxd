// Creator - MatthewGolder
#include "FXDEngine/GFX/FX/DepthStencilState.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// IDepthStencilState
// - 
// ------
Job::Future< bool > IDepthStencilState::create(const FX::DEPTHSTENCIL_STATE_DESC& dsStateDesc)
{
	auto funcRef = [&](const FX::DEPTHSTENCIL_STATE_DESC& dsStateDesc) { return _create(dsStateDesc); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(dsStateDesc); });
}

Job::Future< bool > IDepthStencilState::release(void)
{
	auto funcRef = [&](void) { return _release(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< void > IDepthStencilState::attach(void)
{
	auto funcRef = [&](void) { _attach(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< void > IDepthStencilState::detach(void)
{
	auto funcRef = [&](void) { _detach(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}