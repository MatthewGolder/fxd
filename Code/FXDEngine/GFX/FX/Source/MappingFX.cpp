// Creator - MatthewGolder
#include "FXDEngine/GFX/FX/MappingFX.h"
#include "FXDEngine/Core/TypeTraits.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// FXDataTypeDesc
// -
// ------

class FXDataTypeDesc
{
public:
	const FXD::UTF8* pName;		// Name of the format.
	const FXD::U32 nNumBytes;	// Number of bytes one element (color value) uses.
};
const FXDataTypeDesc sFXDataTypes[] =
{
	{ "bool", 1 },			// E_FXDataType::Bool
	{ "int", 4 },			// E_FXDataType::Int
	{ "float", 4 },		// E_FXDataType::Float
	{ "float2", 8 },		// E_FXDataType::Float2
	{ "float3", 12 },		// E_FXDataType::Float3
	{ "float4", 16 },		// E_FXDataType::Float4
	{ "float3x3", 36 },	// E_FXDataType::Float3x3
	{ "float4x3", 48 },	// E_FXDataType::Float4x3
	{ "float4x4", 64 }	// E_FXDataType::Float4x4
};
const FXDataTypeDesc& getDescriptionFor(const FX::E_FXDataType eType)
{
	PRINT_INFO << sFXDataTypes[FXD::STD::to_underlying(eType)].pName;
	return sFXDataTypes[FXD::STD::to_underlying(eType)];
}
FXD::U32 FXD::GFX::FX::Funcs::ToBytes(const FX::E_FXDataType eType)
{
	return getDescriptionFor(eType).nNumBytes;
}
Core::StringView8 FXD::GFX::FX::Funcs::ToString(const FX::E_FXDataType eType)
{
	return getDescriptionFor(eType).pName;
}
const FX::E_FXDataType FXD::GFX::FX::Funcs::ToDataType(const Core::StringView8 strType)
{
	for (FXD::U16 n1 = 0; n1 < FXD::STD::array_size(sFXDataTypes); n1++)
	{
		if (strType.compare_no_case(sFXDataTypes[n1].pName) == 0)
		{
			return (FX::E_FXDataType)n1;
		}
	}
	return FX::E_FXDataType::Count;
}

// ------
// E_SemanticType
// -
// ------
const Core::StringView8 sSemanticTypes[] =
{
	"Position",		// FX::E_SemanticType::Position
	"SV_Position",	// FX::E_SemanticType::SV_Position
	"Color0",		// FX::E_SemanticType::Color0
	"Color1",		// FX::E_SemanticType::Color1
	"Normal0",		// FX::E_SemanticType::Normal0
	"Normal1",		// FX::E_SemanticType::Normal1
	"Binormal0",	// FX::E_SemanticType::Binormal0
	"Binormal1",	// FX::E_SemanticType::Binormal1
	"Tangent0",		// FX::E_SemanticType::Tangent0
	"Tangent1",		// FX::E_SemanticType::Tangent1
	"TexCoord0",	// FX::E_SemanticType::TexCoord0
	"TexCoord1",	// FX::E_SemanticType::TexCoord1
	"TexCoord2",	// FX::E_SemanticType::TexCoord2
	"TexCoord3",	// FX::E_SemanticType::TexCoord3
	"TexCoord4",	// FX::E_SemanticType::TexCoord4
	"TexCoord5",	// FX::E_SemanticType::TexCoord5
	"TexCoord6",	// FX::E_SemanticType::TexCoord6
	"TexCoord7",	// FX::E_SemanticType::TexCoord7
	"TexCoord8",	// FX::E_SemanticType::TexCoord8
	 "unknown"
};
Core::StringView8 FXD::GFX::FX::Funcs::ToString(const FX::E_SemanticType eType)
{
	return sSemanticTypes[FXD::STD::to_underlying(eType)];
}
const FX::E_SemanticType FXD::GFX::FX::Funcs::ToSemanticType(const Core::StringView8 strType)
{
	for (FXD::U16 n1 = 0; n1 < FXD::STD::array_size(sSemanticTypes); n1++)
	{
		if (strType.compare_no_case(sSemanticTypes[n1].c_str()) == 0)
		{
			return (FX::E_SemanticType)n1;
		}
	}
	return FX::E_SemanticType::Count;
}

// ------
// E_FilterMode
// -
// ------
const Core::StringView8 sFilterModes[] =
{
	"Point",					// FX::E_FilterMode::Point
	"Bilinear",				// FX::E_FilterMode::Bilinear
	"Trilinear",			// FX::E_FilterMode::Trilinear
	"AnisotropicPoint",	// FX::E_FilterMode::AnisotropicPoint
	"AnisotropicLinear",	// FX::E_FilterMode::AnisotropicLinear
	"unknown"
};
extern Core::StringView8 FXD::GFX::FX::Funcs::ToString(const FX::E_FilterMode eType)
{
	return sFilterModes[FXD::STD::to_underlying(eType)];
}
extern const FX::E_FilterMode FXD::GFX::FX::Funcs::toFilterMode(const Core::StringView8 strType)
{
	for (FXD::U16 n1 = 0; n1 < FXD::STD::array_size(sFilterModes); n1++)
	{
		if (strType.compare_no_case(sFilterModes[n1].c_str()) == 0)
		{
			return (FX::E_FilterMode)n1;
		}
	}
	return FX::E_FilterMode::Count;
}

// ------
// E_AddressMode
// -
// ------
const Core::StringView8 sAddressModes[] =
{
	"Wrap",			// FX::E_AddressMode::Wrap
	"Mirror",		// FX::E_AddressMode::Mirror
	"Clamp",			// FX::E_AddressMode::Clamp
	"ClampBorder",	// FX::E_AddressMode::ClampBorder
	"unknown"
};
extern Core::StringView8 FXD::GFX::FX::Funcs::ToString(const FX::E_AddressMode eType)
{
	return sAddressModes[FXD::STD::to_underlying(eType)];
}
extern const FX::E_AddressMode FXD::GFX::FX::Funcs::ToAddressMode(const Core::StringView8 strType)
{
	for (FXD::U16 n1 = 0; n1 < FXD::STD::array_size(sAddressModes); n1++)
	{
		if (strType.compare_no_case(sAddressModes[n1].c_str()) == 0)
		{
			return (FX::E_AddressMode)n1;
		}
	}
	return FX::E_AddressMode::Count;
}

// ------
// E_StencilOp
// -
// ------
const Core::StringView8 sCompareFunctions[] =
{
	"Less",				// FX::E_CompareOp::Less
	"LessEqual",		// FX::E_CompareOp::LessEqual
	"Greater",			// FX::E_CompareOp::Greater
	"GreaterEqual",	// FX::E_CompareOp::GreaterEqual
	"Equal",				// FX::E_CompareOp::Equal
	"NotEqual",			// FX::E_CompareOp::NotEqual
	"Never",				// FX::E_CompareOp::Never
	"Always",			// FX::E_CompareOp::Always
	"unknown"
};
extern Core::StringView8 FXD::GFX::FX::Funcs::ToString(const FX::E_CompareOp eType)
{
	return sCompareFunctions[FXD::STD::to_underlying(eType)];
}
extern const FX::E_CompareOp FXD::GFX::FX::Funcs::ToCompareOp(const Core::StringView8 strType)
{
	for (FXD::U16 n1 = 0; n1 < FXD::STD::array_size(sCompareFunctions); n1++)
	{
		if (strType.compare_no_case(sCompareFunctions[n1].c_str()) == 0)
		{
			return (FX::E_CompareOp)n1;
		}
	}
	return FX::E_CompareOp::Count;
}

// ------
// E_StencilOp
// -
// ------
const Core::StringView8 sStencilOps[] =
{
	"Keep",						// FX::E_StencilOp::Keep
	"Zero",						// FX::E_StencilOp::Zero
	"Replace",					// FX::E_StencilOp::Replace
	"SaturatedIncrement",	// FX::E_StencilOp::SaturatedIncrement
	"SaturatedDecrement",	// FX::E_StencilOp::SaturatedDecrement
	"Increment",				// FX::E_StencilOp::Increment
	"Decrement",				// FX::E_StencilOp::Decrement
	"Invert",					// FX::E_StencilOp::Invert
	"unknown"
};
extern Core::StringView8 FXD::GFX::FX::Funcs::ToString(const FX::E_StencilOp eType)
{
	return sStencilOps[FXD::STD::to_underlying(eType)];
}
extern const FX::E_StencilOp FXD::GFX::FX::Funcs::ToStencilOp(const Core::StringView8 strType)
{
	for (FXD::U16 n1 = 0; n1 < FXD::STD::array_size(sStencilOps); n1++)
	{
		if (strType.compare_no_case(sStencilOps[n1].c_str()) == 0)
		{
			return (FX::E_StencilOp)n1;
		}
	}
	return FX::E_StencilOp::Count;
}

// ------
// E_BlendOps
// -
// ------
const Core::StringView8 sBlendOps[] =
{
	"Add",					// FX::E_BlendOp::Add
	"Subtract",				// FX::E_BlendOp::Subtract
	"Min",					// FX::E_BlendOp::Min
	"Max",					// FX::E_BlendOp::Max
	"ReverseSubtract",	// FX::E_BlendOp::ReverseSubtract
	"unknown"
};
extern Core::StringView8 FXD::GFX::FX::Funcs::ToString(const FX::E_BlendOp eType)
{
	return sBlendOps[FXD::STD::to_underlying(eType)];
}
extern const FX::E_BlendOp FXD::GFX::FX::Funcs::ToBlendOps(const Core::StringView8 strType)
{
	for (FXD::U16 n1 = 0; n1 < FXD::STD::array_size(sBlendOps); n1++)
	{
		if (strType.compare_no_case(sBlendOps[n1].c_str()) == 0)
		{
			return (FX::E_BlendOp)n1;
		}
	}
	return FX::E_BlendOp::Count;
}

// ------
// E_BlendFactor
// -
// ------
const Core::StringView8 sBlendFactors[] =
{
	"Zero",								// FX::E_BlendFactor::Zero
	"One",								// FX::E_BlendFactor::One
	"SourceColor",						// FX::E_BlendFactor::SourceColor
	"InverseSourceColor",			// FX::E_BlendFactor::InverseSourceColor
	"SourceAlpha",						// FX::E_BlendFactor::SourceAlpha
	"InverseSourceAlpha",			// FX::E_BlendFactor::InverseSourceAlpha
	"DestAlpha",						// FX::E_BlendFactor::DestAlpha
	"InverseDestAlpha",				// FX::E_BlendFactor::InverseDestAlpha
	"DestColor",						// FX::E_BlendFactor::DestColor
	"InverseDestColor",				// FX::E_BlendFactor::InverseDestColor
	"ConstantBlendFactor",			// FX::E_BlendFactor::ConstantBlendFactor
	"InverseConstantBlendFactor",	// FX::E_BlendFactor::InverseConstantBlendFactor
	"unknown"
};
extern Core::StringView8 FXD::GFX::FX::Funcs::ToString(const FX::E_BlendFactor eType)
{
	return sBlendFactors[FXD::STD::to_underlying(eType)];
}
extern const FX::E_BlendFactor FXD::GFX::FX::Funcs::ToBlendFactor(const Core::StringView8 strType)
{
	for (FXD::U16 n1 = 0; n1 < FXD::STD::array_size(sBlendFactors); n1++)
	{
		if (strType.compare_no_case(sBlendFactors[n1].c_str()) == 0)
		{
			return (FX::E_BlendFactor)n1;
		}
	}
	return FX::E_BlendFactor::Count;
}