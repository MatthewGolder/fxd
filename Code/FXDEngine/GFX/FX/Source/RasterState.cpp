// Creator - MatthewGolder
#include "FXDEngine/GFX/FX/RasterState.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// IRasterState
// - 
// ------
Job::Future< bool > IRasterState::create(const FX::RASTER_STATE_DESC& rasterDesc)
{
	auto funcRef = [&](const FX::RASTER_STATE_DESC& rasterDesc) { return _create(rasterDesc); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(rasterDesc); });
}

Job::Future< bool > IRasterState::release(void)
{
	auto funcRef = [&](void) { return _release(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< void > IRasterState::attach(void)
{
	auto funcRef = [&](void) { _attach(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< void > IRasterState::detach(void)
{
	auto funcRef = [&](void) { _detach(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}