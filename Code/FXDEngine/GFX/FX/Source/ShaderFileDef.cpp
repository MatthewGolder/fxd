// Creator - MatthewGolder
#include "FXDEngine/GFX/GfxApi.h"
#include "FXDEngine/GFX/Mapping.h"
#include "FXDEngine/GFX/FX/MappingFX.h"
#include "FXDEngine/GFX/FX/ShaderFileDef.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// ShaderFileDef
// -
// ------
ShaderFileDef::ShaderFileDef(void)
	: m_eType(GFX::E_GfxApi::Unknown)
{
}

ShaderFileDef::~ShaderFileDef(void)
{
}

void ShaderFileDef::writeXml(Core::StreamOut& stream) const
{
	IO::XMLWriter8::XMLAttribute attrName("name", m_strFileName);
	IO::XMLWriter8::XMLAttribute attrType("type", GFX::Api::ToString(m_eType).c_str());
	IO::XMLWriter8::XMLAttribute* pAttrs[] = { &attrName, &attrType };
	IO::XMLWriter8::XMLNode xmlNode(stream, UTF_8("Shader"), UTF_8(""), nullptr, pAttrs, 2);
	_write_xml_textures(stream, &xmlNode);
	_write_xml_constant_buffer(stream, &xmlNode);
	_write_xml_vertex_shader_input(stream, &xmlNode);
	_write_xml_vertex_shader_output(stream, &xmlNode);
	_write_xml_pixel_shader_output(stream, &xmlNode);
	_write_xml_vertex_shader(stream, &xmlNode);
	_write_xml_pixel_shader(stream, &xmlNode);
}

void ShaderFileDef::_write_xml_textures(Core::StreamOut& stream, IO::XMLWriter8::XMLNode* pParent) const
{
	fxd_for(const auto& texDef, m_textures)
	{
		IO::XMLWriter8::XMLAttribute attrType("type", FXD::GFX::ImageF::ToString(texDef.Type).c_str());
		IO::XMLWriter8::XMLAttribute attrName("name", texDef.Name);
		IO::XMLWriter8::XMLAttribute attrSampler("sampler", texDef.Sampler);
		IO::XMLWriter8::XMLAttribute attrReg("register", Core::to_string8(texDef.RegisterSlot));
		IO::XMLWriter8::XMLAttribute* pAttrsCB[] = { &attrType, &attrName, &attrSampler, &attrReg };
		IO::XMLWriter8::XMLNode nodeCB(stream, "texture", "", pParent, pAttrsCB, 4, true, false);
	}
}

void ShaderFileDef::_write_xml_constant_buffer(Core::StreamOut& stream, IO::XMLWriter8::XMLNode* pParent) const
{
	fxd_for(const auto& constantBuffer, m_constantBuffers)
	{
		IO::XMLWriter8::XMLAttribute attrName("name", constantBuffer.Name);
		IO::XMLWriter8::XMLAttribute attrReg("register", Core::to_string8(constantBuffer.RegisterSlot));
		IO::XMLWriter8::XMLAttribute* pAttrsCB[] = { &attrName, &attrReg };
		IO::XMLWriter8::XMLNode nodeCB(stream, "Constant_Buffer", "", pParent, pAttrsCB, 2);
		fxd_for(const auto& param, constantBuffer.Params)
		{
			IO::XMLWriter8::XMLAttribute attrType("name", param.Name);
			IO::XMLWriter8::XMLAttribute attrName("type", FX::Funcs::ToString(param.DataType).c_str());
			IO::XMLWriter8::XMLAttribute* pAttrsSem[] = { &attrName, &attrType };
			IO::XMLWriter8::XMLNode nodeParam(stream, "Param", "", &nodeCB, pAttrsSem, 2, true, false);
		}
	}
}

void ShaderFileDef::_write_xml_vertex_shader_input(Core::StreamOut& stream, IO::XMLWriter8::XMLNode* pParent) const
{
	IO::XMLWriter8::XMLNode nodeCB(stream, UTF_8("Vertex_Shader_Input"), UTF_8(""), pParent, nullptr, 0);
	fxd_for(const auto& param, m_vShaderIn.Params)
	{
		if (param.DataType == FXD::GFX::FX::E_FXDataType::Unknown)
		{
			break;
		}
		IO::XMLWriter8::XMLAttribute attrType(UTF_8("name"), param.Name);
		IO::XMLWriter8::XMLAttribute attrName(UTF_8("type"), FX::Funcs::ToString(param.DataType).c_str());
		IO::XMLWriter8::XMLAttribute attrSemantic(UTF_8("semantic"), FX::Funcs::ToString(param.SemanticType).c_str());
		IO::XMLWriter8::XMLAttribute* pAttrsSems[] = { &attrName, &attrType, &attrSemantic };
		IO::XMLWriter8::XMLNode nodeParam(stream, UTF_8("Param"), UTF_8(""), &nodeCB, pAttrsSems, 3, true, false);
	}
}

void ShaderFileDef::_write_xml_vertex_shader_output(Core::StreamOut& stream, IO::XMLWriter8::XMLNode* pParent) const
{
	IO::XMLWriter8::XMLNode nodeCB(stream, UTF_8("Vertex_Shader_Output"), UTF_8(""), pParent, nullptr, 0);
	fxd_for(const auto& param, m_vShaderOut.Params)
	{
		if (param.DataType == FXD::GFX::FX::E_FXDataType::Unknown)
		{
			break;
		}
		IO::XMLWriter8::XMLAttribute attrType(UTF_8("name"), param.Name);
		IO::XMLWriter8::XMLAttribute attrName(UTF_8("type"), FX::Funcs::ToString(param.DataType).c_str());
		IO::XMLWriter8::XMLAttribute attrSemantic(UTF_8("semantic"), FX::Funcs::ToString(param.SemanticType).c_str());
		IO::XMLWriter8::XMLAttribute* pAttrsSems[] = { &attrName, &attrType, &attrSemantic };
		IO::XMLWriter8::XMLNode nodeParam(stream, UTF_8("Param"), UTF_8(""), &nodeCB, pAttrsSems, 3, true, false);
	}
}

void ShaderFileDef::_write_xml_pixel_shader_output(Core::StreamOut& stream, IO::XMLWriter8::XMLNode* pParent) const
{
	IO::XMLWriter8::XMLNode nodeCB(stream, UTF_8("Pixel_Shader_Output"), UTF_8(""), pParent, nullptr, 0);
	fxd_for(const auto& param, m_pShaderOut.Params)
	{
		if (param.DataType == FXD::GFX::FX::E_FXDataType::Unknown)
		{
			break;
		}
		IO::XMLWriter8::XMLAttribute attrType(UTF_8("name"), param.Name);
		IO::XMLWriter8::XMLAttribute attrName(UTF_8("type"), FX::Funcs::ToString(param.DataType).c_str());
		IO::XMLWriter8::XMLAttribute attrSemantic(UTF_8("semantic"), FX::Funcs::ToString(param.SemanticType).c_str());
		IO::XMLWriter8::XMLAttribute* pAttrsSems[] = { &attrName, &attrType, &attrSemantic };
		IO::XMLWriter8::XMLNode nodeParam(stream, UTF_8("Param"), UTF_8(""), &nodeCB, pAttrsSems, 3, true, false);
	}
}

void ShaderFileDef::_write_xml_vertex_shader(Core::StreamOut& stream, IO::XMLWriter8::XMLNode* pParent) const
{
	IO::XMLWriter8::XMLNode nodeCB(stream, "Vertex_Shader", m_vertexShader, pParent, nullptr, 0);
}

void ShaderFileDef::_write_xml_pixel_shader(Core::StreamOut& stream, IO::XMLWriter8::XMLNode* pParent) const
{
	IO::XMLWriter8::XMLNode nodeCB(stream, "Pixel_Shader", m_pixelShader, pParent, nullptr, 0);
}

// ------
// ShaderTextureDefTagCB
// -
// ------
class ShaderTextureDefTagCB FINAL : public IO::XMLSaxParser::ITagCB, public Core::NonCopyable
{
public:
	enum class E_Tag
	{
		Unknown = 0xffff
	};

public:
	ShaderTextureDefTagCB(IO::XMLSaxParser::ITagCB* pParent, FX::TextureDef& textureDef)
		: m_eTag(ShaderTextureDefTagCB::E_Tag::Unknown)
		, m_pParent(pParent)
		, m_textureDef(textureDef)
	{
	}
	~ShaderTextureDefTagCB(void)
	{
	}

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL
	{
		pTagCB = this;
		switch (m_eTag)
		{
			default:
			{
				break;
			}
		}
	}
	void end_tag(const Core::String8& /*strName*/) FINAL
	{
		switch (m_eTag)
		{
			default:
			{
				break;
			}
		}
	}
	void handle_text(const Core::String8& /*strText*/, const IO::StreamParser::Position& /*pos*/) FINAL
	{
		switch (m_eTag)
		{
			default:
			{
				break;
			}
		}
	}
	void handle_attribute(const Core::String8& strName, const Core::String8& strValue) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("name"))
				{
					m_textureDef.Name = strValue;
				}
				else if (strName == UTF_8("sampler"))
				{
					m_textureDef.Sampler = strValue;
				}
				else if (strName == UTF_8("type"))
				{
					m_textureDef.Type = FXD::GFX::ImageF::ToImageType(strValue.c_str());
				}
				else if (strName == UTF_8("register"))
				{
					m_textureDef.RegisterSlot = (FXD::U16)strValue.to_S16();
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}

public:
	ShaderTextureDefTagCB::E_Tag m_eTag;
	IO::XMLSaxParser::ITagCB* m_pParent;
	FX::TextureDef& m_textureDef;
};

// ------
// ShaderConstantBufferTagCB
// -
// ------
class ShaderConstantBufferTagCB FINAL : public IO::XMLSaxParser::ITagCB, public Core::NonCopyable
{
public:
	enum class E_Tag
	{
		ConstantBufferParam = 0,
		Unknown = 0xffff
	};
public:
	ShaderConstantBufferTagCB(IO::XMLSaxParser::ITagCB* pParent, FX::ConstantBufferDef& constantBuffer)
		: m_eTag(ShaderConstantBufferTagCB::E_Tag::Unknown)
		, m_pParent(pParent)
		, m_constantBuffer(constantBuffer)
	{
	}
	~ShaderConstantBufferTagCB(void)
	{
	}

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL
	{
		pTagCB = this;
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("Param"))
				{
					m_constantBuffer.Params.emplace_back();
					m_eTag = E_Tag::ConstantBufferParam;
					break;
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void end_tag(const Core::String8& /*strName*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::ConstantBufferParam:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_text(const Core::String8& /*strText*/, const IO::StreamParser::Position& /*pos*/) FINAL
	{
		switch (m_eTag)
		{
			default:
			{
				break;
			}
		}
	}
	void handle_attribute(const Core::String8& strName, const Core::String8& strValue) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("name"))
				{
					m_constantBuffer.Name = strValue;
				}
				else if (strName == UTF_8("register"))
				{
					m_constantBuffer.RegisterSlot = (FXD::U16)strValue.to_S16();
				}
				break;
			}
			case E_Tag::ConstantBufferParam:
			{
				if (strName == UTF_8("name"))
				{
					m_constantBuffer.Params.back().Name = strValue;
				}
				else if (strName == UTF_8("type"))
				{
					m_constantBuffer.Params.back().DataType = FX::Funcs::ToDataType(strValue.c_str());
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}

public:
	ShaderConstantBufferTagCB::E_Tag m_eTag;
	IO::XMLSaxParser::ITagCB* m_pParent;
	FX::ConstantBufferDef& m_constantBuffer;
};

// ------
// ShaderVaryingTagCB
// -
// ------
class ShaderVaryingTagCB FINAL : public IO::XMLSaxParser::ITagCB, public Core::NonCopyable
{
public:
	enum class E_Tag
	{
		Param = 0,
		Unknown = 0xffff
	};
public:
	ShaderVaryingTagCB(IO::XMLSaxParser::ITagCB* pParent, FX::VaryingListDef& varyingList)
		: m_eTag(ShaderVaryingTagCB::E_Tag::Unknown)
		, m_pParent(pParent)
		, m_varyingList(varyingList)
		, m_nID(0)
	{
	}
	~ShaderVaryingTagCB(void)
	{
	}

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL
	{
		pTagCB = this;
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("Param"))
				{
					m_eTag = E_Tag::Param;
					break;
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void end_tag(const Core::String8& /*strName*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Param:
			{
				m_nID++;
				m_eTag = E_Tag::Unknown;
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_text(const Core::String8& /*strText*/, const IO::StreamParser::Position& /*pos*/) FINAL
	{
		switch (m_eTag)
		{
			default:
			{
				break;
			}
		}
	}
	void handle_attribute(const Core::String8& strName, const Core::String8& strValue) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("name"))
				{
					m_varyingList.Name = strValue;
				}
				break;
			}
			case E_Tag::Param:
			{
				if (strName == UTF_8("name"))
				{
					m_varyingList.Params[m_nID].Name = strValue;
				}
				else if (strName == UTF_8("type"))
				{
					m_varyingList.Params[m_nID].DataType = FX::Funcs::ToDataType(strValue.c_str());
				}
				else if (strName == UTF_8("semantic"))
				{
					m_varyingList.Params[m_nID].SemanticType = FX::Funcs::ToSemanticType(strValue.c_str());
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}

public:
	ShaderVaryingTagCB::E_Tag m_eTag;
	IO::XMLSaxParser::ITagCB* m_pParent;
	FX::VaryingListDef& m_varyingList;
	FXD::U32 m_nID;
};

// ------
// ShaderFileTagCB
// -
// ------
ShaderFileTagCB::ShaderFileTagCB(void)
	: m_pChild(nullptr)
	, m_eTag(E_Tag::Unknown)
{
}

ShaderFileTagCB::~ShaderFileTagCB(void)
{
}

void ShaderFileTagCB::handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB)
{
	pTagCB = this;
	switch (m_eTag)
	{
		case E_Tag::Unknown:
		{
			if (strName == UTF_8("Shader"))
			{
				m_eTag = E_Tag::Effect;
			}
			break;
		}
		case E_Tag::Effect:
		{
			if (strName == UTF_8("texture"))
			{
				m_pChild = pTagCB = FXD_NEW(ShaderTextureDefTagCB)(this, m_FXFileDef.m_textures.emplace_back());
				m_eTag = E_Tag::TextureDef;
				break;
			}
			else if (strName == UTF_8("Constant_Buffer"))
			{
				m_pChild = pTagCB = FXD_NEW(ShaderConstantBufferTagCB)(this, m_FXFileDef.m_constantBuffers.emplace_back());
				m_eTag = E_Tag::ConstantBuffer;
				break;
			}
			else if (strName == UTF_8("Vertex_Shader_Input"))
			{
				m_pChild = pTagCB = FXD_NEW(ShaderVaryingTagCB)(this, m_FXFileDef.m_vShaderIn);
				m_eTag = E_Tag::VertexShaderInput;
				break;
			}
			else if (strName == UTF_8("Vertex_Shader_Output"))
			{
				m_pChild = pTagCB = FXD_NEW(ShaderVaryingTagCB)(this, m_FXFileDef.m_vShaderOut);
				m_eTag = E_Tag::VertexShaderOutput;
				break;
			}
			else if (strName == UTF_8("Pixel_Shader_Output"))
			{
				m_pChild = pTagCB = FXD_NEW(ShaderVaryingTagCB)(this, m_FXFileDef.m_pShaderOut);
				m_eTag = E_Tag::PixelShaderOutput;
				break;
			}
			else if (strName == UTF_8("Vertex_Shader"))
			{
				m_eTag = E_Tag::VertexShader;
				break;
			}
			else if (strName == UTF_8("Pixel_Shader"))
			{
				m_eTag = E_Tag::PixelShader;
				break;
			}
			break;
		}
		default:
		{
			break;
		}
	}
}

void ShaderFileTagCB::end_tag(const Core::String8& /*strName*/)
{
	switch (m_eTag)
	{
		case E_Tag::Effect:
		{
			m_eTag = E_Tag::Unknown;
			break;
		}
		case E_Tag::SamplerDef:
		{
			m_eTag = E_Tag::Effect;
			FXD_SAFE_DELETE(m_pChild);
			break;
		}
		case E_Tag::TextureDef:
		{
			m_eTag = E_Tag::Effect;
			FXD_SAFE_DELETE(m_pChild);
			break;
		}
		case E_Tag::ConstantBuffer:
		{
			m_eTag = E_Tag::Effect;
			FXD_SAFE_DELETE(m_pChild);
			break;
		}
		case E_Tag::VertexShaderInput:
		{
			m_eTag = E_Tag::Effect;
			FXD_SAFE_DELETE(m_pChild);
			break;
		}
		case E_Tag::VertexShaderOutput:
		{
			m_eTag = E_Tag::Effect;
			FXD_SAFE_DELETE(m_pChild);
			break;
		}
		case E_Tag::PixelShaderOutput:
		{
			m_eTag = E_Tag::Effect;
			FXD_SAFE_DELETE(m_pChild);
			break;
		}
		case E_Tag::VertexShader:
		{
			m_eTag = E_Tag::Effect;
			break;
		}
		case E_Tag::PixelShader:
		{
			m_eTag = E_Tag::Effect;
			break;
		}
		default:
		{
			break;
		}
	}
}

void ShaderFileTagCB::handle_text(const Core::String8& strText, const IO::StreamParser::Position& /*pos*/)
{
	switch (m_eTag)
	{
		case E_Tag::VertexShader:
		{
			m_FXFileDef.m_vertexShader = strText;
			break;
		}
		case E_Tag::PixelShader:
		{
			m_FXFileDef.m_pixelShader = strText;
			break;
		}
		default:
		{
			break;
		}
	}
}

void ShaderFileTagCB::handle_attribute(const Core::String8& strName, const Core::String8& strValue)
{
	switch (m_eTag)
	{
		case E_Tag::Effect:
		{
			if (strName == UTF_8("name"))
			{
				m_FXFileDef.m_strFileName = strValue;
			}
			else if (strName == UTF_8("type"))
			{
				m_FXFileDef.m_eType = GFX::Api::ToApiType(strValue.c_str());
			}
			break;
		}
		default:
		{
			break;
		}
	}
}