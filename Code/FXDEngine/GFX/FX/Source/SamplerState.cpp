// Creator - MatthewGolder
#include "FXDEngine/GFX/FX/SamplerState.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// ISamplerState
// - 
// ------
Job::Future< bool > ISamplerState::create(const FX::SAMPLER_DESC& sampleDesc)
{
	auto funcRef = [&](const FX::SAMPLER_DESC& sampleDesc) { return _create(sampleDesc); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(sampleDesc); });
}

Job::Future< bool > ISamplerState::release(void)
{
	auto funcRef = [&](void) { return _release(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< void > ISamplerState::attach(void)
{
	auto funcRef = [&](void) { _attach(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< void > ISamplerState::detach(void)
{
	auto funcRef = [&](void) { _detach(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}