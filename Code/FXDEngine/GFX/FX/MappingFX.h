// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_MAPPING_H
#define FXDENGINE_GFX_FX_MAPPING_H

#include "FXDEngine/GFX/FX/Types.h"
#include "FXDEngine/Core/StringView.h"

namespace FXD
{
	namespace GFX
	{
		namespace FX
		{
			namespace Funcs
			{
				extern FXD::U32 ToBytes(const FX::E_FXDataType eType);
				extern Core::StringView8 ToString(const FX::E_FXDataType eType);
				extern const FX::E_FXDataType ToDataType(const Core::StringView8 strType);

				extern Core::StringView8 ToString(const FX::E_SemanticType eType);
				extern const FX::E_SemanticType ToSemanticType(const Core::StringView8 strType);

				extern Core::StringView8 ToString(const FX::E_FilterMode eType);
				extern const FX::E_FilterMode toFilterMode(const Core::StringView8 strType);

				extern Core::StringView8 ToString(const FX::E_AddressMode eType);
				extern const FX::E_AddressMode ToAddressMode(const Core::StringView8 strType);

				extern Core::StringView8 ToString(const FX::E_CompareOp eType);
				extern const FX::E_CompareOp ToCompareOp(const Core::StringView8 strType);

				extern Core::StringView8 ToString(const FX::E_StencilOp eType);
				extern const FX::E_StencilOp ToStencilOp(const Core::StringView8 strType);

				extern Core::StringView8 ToString(const FX::E_BlendOp eType);
				extern const FX::E_BlendOp ToBlendOps(const Core::StringView8 strType);

				extern Core::StringView8 ToString(const FX::E_BlendFactor eType);
				extern const FX::E_BlendFactor ToBlendFactor(const Core::StringView8 strType);
			}

		} //namespace FX
	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_FX_MAPPING_H