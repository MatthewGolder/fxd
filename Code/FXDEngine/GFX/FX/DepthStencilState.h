// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_DEPTHSTENCILSTATE_H
#define FXDENGINE_GFX_FX_DEPTHSTENCILSTATE_H

#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/GFX/FX/Types.h"
#include "FXDEngine/Job/Future.h"

namespace FXD
{
	namespace GFX
	{
		namespace FX
		{
			struct DEPTHSTENCIL_STATE_DESC
			{
				bool EnableDepthWrite;
				FX::E_CompareOp DepthTest;

				bool EnableFrontFaceStencil;
				FX::E_CompareOp FrontFaceStencilTest;
				FX::E_StencilOp FrontFaceStencilFailStencilOp;
				FX::E_StencilOp FrontFaceDepthFailStencilOp;
				FX::E_StencilOp FrontFacePassStencilOp;

				bool EnableBackFaceStencil;
				FX::E_CompareOp BackFaceStencilTest;
				FX::E_StencilOp BackFaceStencilFailStencilOp;
				FX::E_StencilOp BackFaceDepthFailStencilOp;
				FX::E_StencilOp BackFacePassStencilOp;
				FXD::U8 StencilReadMask;
				FXD::U8 StencilWriteMask;
			};

			// ------
			// IBlendState
			// -
			// ------
			class IDepthStencilState : public Core::HasImpl< FX::IDepthStencilState, 64 >, public Core::NonCopyable
			{
			public:
				IDepthStencilState(GFX::IGfxAdapter* pAdapter);
				virtual ~IDepthStencilState(void);

				Job::Future< bool > create(const FX::DEPTHSTENCIL_STATE_DESC& dsStateDesc);
				Job::Future< bool > release(void);

				Job::Future< void > attach(void);
				Job::Future< void > detach(void);

			protected:
				bool _create(const FX::DEPTHSTENCIL_STATE_DESC& dsStateDesc);
				bool _release(void);

				void _attach(void);
				void _detach(void);

			private:
				GFX::IGfxAdapter* m_pAdapter;
				FX::DEPTHSTENCIL_STATE_DESC m_dsStateDesc;
			};

		} //namespace FX
	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_FX_DEPTHSTENCILSTATE_H