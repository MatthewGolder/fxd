// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_BLENDSTATE_H
#define FXDENGINE_GFX_FX_BLENDSTATE_H

#include "FXDEngine/Container/Array.h"
#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/GFX/FX/Types.h"
#include "FXDEngine/Job/Future.h"

namespace FXD
{
	namespace GFX
	{
		namespace FX
		{
			struct BLEND_STATE_DESC
			{
				bool operator==(const BLEND_STATE_DESC& rhs) const;

				struct RT_DESC
				{
					bool operator==(const BLEND_STATE_DESC::RT_DESC& rhs) const;
				
					FX::E_BlendOp ColorBlendOp = FX::E_BlendOp::Add;
					FX::E_BlendFactor ColorSrcBlend = FX::E_BlendFactor::One;
					FX::E_BlendFactor ColorDestBlend = FX::E_BlendFactor::Zero;
					FX::E_BlendOp AlphaBlendOp = FX::E_BlendOp::Add;
					FX::E_BlendFactor AlphaSrcBlend = FX::E_BlendFactor::One;
					FX::E_BlendFactor AlphaDestBlend = FX::E_BlendFactor::Zero;
					FX::E_ColorWriteMask ColorWriteMask = FX::E_ColorWriteMask::CW_RGBA;
				};

				//bool AlphaToCoverageEnable = false;
				bool UseIndependentRenderTargetBlendStates = false;
				Container::Array< RT_DESC, GFX::kMaxMultipleRenderTargets > RenderTargets;
			};

			// ------
			// IBlendState
			// -
			// ------
			class IBlendState : public Core::HasImpl< FX::IBlendState, 64 >, public Core::NonCopyable
			{
			public:
				IBlendState(GFX::IGfxAdapter* pAdapter);
				virtual ~IBlendState(void);

				Job::Future< bool > create(const FX::BLEND_STATE_DESC& blendDesc);
				Job::Future< bool > release(void);

				Job::Future< void > attach(void);
				Job::Future< void > detach(void);

			protected:
				bool _create(const FX::BLEND_STATE_DESC& blendDesc);
				bool _release(void);

				void _attach(void);
				void _detach(void);

			private:
				GFX::IGfxAdapter* m_pAdapter;
				FX::BLEND_STATE_DESC m_blendDesc;
			};

		} //namespace FX
	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_FX_BLENDSTATE_H