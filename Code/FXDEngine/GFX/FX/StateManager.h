// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_STATEMANAGER_H
#define FXDENGINE_GFX_FX_STATEMANAGER_H

#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/Container/Map.h"
#include "FXDEngine/GFX/FX/Types.h"
#include "FXDEngine/Job/Future.h"

namespace FXD
{
	namespace GFX
	{
		namespace FX
		{
			struct SAMPLER_DESC;
			struct DEPTHSTENCIL_STATE_DESC;
			struct RASTER_STATE_DESC;
			struct BLEND_STATE_DESC;
			struct PIPELINE_DESC;

			// ------
			// IStateManager
			// -
			// ------
			class IStateManager// : public Core::HasImpl< FX::IStateManager, 64 >
			{
			public:
				friend class GFX::IGfxAdapter;

			public:
				IStateManager(GFX::IGfxAdapter* pAdapter);
				virtual ~IStateManager(void);

				Job::Future< bool > create(void);
				Job::Future< bool > release(void);

				const FX::SamplerState& get_default_sampler_state(void) const;
				const FX::BlendState& get_default_blend_state(void) const;
				const FX::RasterState& get_default_raster_state(void) const;
				const FX::DepthStencilState& get_default_depth_stencil_state(void) const;

			protected:
				bool _create(void);
				bool _release(void);

				FX::SamplerState _create_sampler_state(const FX::SAMPLER_DESC& sampleDesc);
				FX::BlendState _create_blend_state(const FX::BLEND_STATE_DESC& blendDesc);
				FX::RasterState _create_raster_state(const FX::RASTER_STATE_DESC& rasterDesc);
				FX::DepthStencilState _create_depth_stencil_state(const FX::DEPTHSTENCIL_STATE_DESC& dsStateDesc);
				FX::Pipeline _create_pipeline_state(const FX::PIPELINE_DESC& pipelineDesc);

				FX::SamplerState _find_sampler_state(const FXD::HashValue sampleHash) const;
				FX::BlendState _find_blend_state(const FXD::HashValue blendStateHash) const;
				FX::RasterState _find_raster_state(const FXD::HashValue rasterStateHash) const;
				FX::DepthStencilState _find_depth_stencil_state(const FXD::HashValue dsStateHash) const;

			protected:
				GFX::IGfxAdapter* m_pAdapter;

				FX::SamplerState m_defaultSamplerState;
				FX::BlendState m_defaultBlendState;
				FX::RasterState m_defaultRasterState;
				FX::DepthStencilState m_defaultDepthStencilState;

				Container::Map< const FXD::HashValue, FX::SamplerState > m_samplerStateCache;
				Container::Map< const FXD::HashValue, FX::BlendState > m_blendStateCache;
				Container::Map< const FXD::HashValue, FX::RasterState > m_rasterStateCache;
				Container::Map< const FXD::HashValue, FX::DepthStencilState > m_depthStencilStateCache;
			};

		} //namespace FX
	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_FX_STATEMANAGER_H