// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_DX11_MAPPINGFXDX11_H
#define FXDENGINE_GFX_FX_DX11_MAPPINGFXDX11_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/FX/MappingFX.h"
#include "FXDEngine/GFX/DX11/DX11.h"

namespace FXD
{
	namespace GFX
	{
		namespace FX
		{
			namespace Sampler
			{
				extern const D3D11_FILTER GetFilterMode(const FX::E_FilterMode eMode, bool bComparisonEnabled, FXD::F32 fMaxAnisotropy);
				extern const D3D11_TEXTURE_ADDRESS_MODE GetAddressMode(const FX::E_AddressMode eMode);

				extern const D3D11_COMPARISON_FUNC GetCompareFunction(const FX::E_CompareOp eMode);
				extern const D3D11_STENCIL_OP GetStencilOp(const FX::E_StencilOp eMode);

				extern const D3D11_BLEND_OP GetBlendOp(const FX::E_BlendOp eMode);
				extern const D3D11_BLEND GetBlendFactor(const FX::E_BlendFactor eMode);
			}
		} //namespace FX
	} //namespace GFX
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_FX_DX11_MAPPINGFXDX11_H