// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_DX11_BLENDSTATEDX11_H
#define FXDENGINE_GFX_FX_DX11_BLENDSTATEDX11_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/FX/BlendState.h"
#include "FXDEngine/GFX/DX11/DX11.h"
#include "FXDEngine/Container/RefCounting.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::FX::IBlendState >
			// -
			// ------
			template <>
			class Impl< GFX::FX::IBlendState >
			{
			public:
				friend class GFX::FX::IBlendState;

			public:
				Impl(void);
				~Impl(void);

			protected:
				ID3D11BlendState* m_pD3DBlendState;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_FX_DX11_BLENDSTATEDX11_H