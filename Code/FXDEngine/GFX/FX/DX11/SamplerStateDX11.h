// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_DX11_SAMLERSTATEDX11_H
#define FXDENGINE_GFX_FX_DX11_SAMLERSTATEDX11_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/FX/SamplerState.h"
#include "FXDEngine/GFX/DX11/DX11.h"
#include "FXDEngine/Container/RefCounting.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::FX::ISamplerState >
			// -
			// ------
			template <>
			class Impl< GFX::FX::ISamplerState >
			{
			public:
				friend class GFX::FX::ISamplerState;

			public:
				Impl(void);
				~Impl(void);

			protected:
				ID3D11SamplerState* m_pD3DSamplerState;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_FX_DX11_SAMLERSTATEDX11_H