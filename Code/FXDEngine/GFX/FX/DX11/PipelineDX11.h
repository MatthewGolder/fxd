// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_DX11_PIPELINEDX11_H
#define FXDENGINE_GFX_FX_DX11_PIPELINEDX11_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/FX/Pipeline.h"
#include "FXDEngine/GFX/DX11/DX11.h"
#include "FXDEngine/Container/RefCounting.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::FX::IPipeline >
			// -
			// ------
			template <>
			class Impl< GFX::FX::IPipeline >
			{
			public:
				friend class GFX::FX::IPipeline;

			public:
				Impl(void);
				~Impl(void);

			protected:
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_FX_DX11_PIPELINEDX11_H