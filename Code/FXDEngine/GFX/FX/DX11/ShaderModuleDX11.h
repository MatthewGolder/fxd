// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_DX11_SHADERDX11_H
#define FXDENGINE_GFX_FX_DX11_SHADERDX11_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/FX/ShaderModule.h"
#include "FXDEngine/GFX/DX11/DX11.h"
#include "FXDEngine/Container/RefCounting.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::FX::IShaderModule >
			// -
			// ------
			template <>
			class Impl< GFX::FX::IShaderModule >
			{
			public:
				friend class GFX::FX::IShaderModule;

			public:
				Impl(void);
				~Impl(void);

			protected:
				ID3D11DeviceChild* m_pD3DShader;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_FX_DX11_SHADERDX11_H