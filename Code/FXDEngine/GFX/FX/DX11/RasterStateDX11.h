// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_DX11_RASTERSTATEDX11_H
#define FXDENGINE_GFX_FX_DX11_RASTERSTATEDX11_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/FX/RasterState.h"
#include "FXDEngine/GFX/DX11/DX11.h"
#include "FXDEngine/Container/RefCounting.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::FX::IRasterState >
			// -
			// ------
			template <>
			class Impl< GFX::FX::IRasterState >
			{
			public:
				friend class GFX::FX::IRasterState;

			public:
				Impl(void);
				~Impl(void);

			protected:
				ID3D11RasterizerState* m_pD3DRasterState;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_FX_DX11_RASTERSTATEDX11_H