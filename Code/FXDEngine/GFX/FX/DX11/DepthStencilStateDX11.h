// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_DX11_DEPTHSTENCILSTATEDX11_H
#define FXDENGINE_GFX_FX_DX11_DEPTHSTENCILSTATEDX11_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/FX/DepthStencilState.h"
#include "FXDEngine/GFX/DX11/DX11.h"
#include "FXDEngine/Container/RefCounting.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::FX::IDepthStencilState >
			// -
			// ------
			template <>
			class Impl< GFX::FX::IDepthStencilState >
			{
			public:
				friend class GFX::FX::IDepthStencilState;

			public:
				Impl(void);
				~Impl(void);

			protected:
				ID3D11DepthStencilState* m_pD3DDSState;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_FX_DX11_DEPTHSTENCILSTATEDX11_H