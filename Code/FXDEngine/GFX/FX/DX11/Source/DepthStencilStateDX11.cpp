// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/DX11/GfxAdapterDX11.h"
#include "FXDEngine/GFX/FX/DX11/MappingFXDX11.h"
#include "FXDEngine/GFX/FX/DX11/DepthStencilStateDX11.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// IDepthStencilState::Impl::Impl
// - 
// ------
IDepthStencilState::Impl::Impl::Impl(void)
	: m_pD3DDSState(nullptr)
{
}

IDepthStencilState::Impl::~Impl(void)
{
	PRINT_COND_ASSERT((m_pD3DDSState == nullptr), "GFX: m_pD3DDSState is not shutdown");
}

// ------
// IDepthStencilState
// - 
// ------
IDepthStencilState::IDepthStencilState(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IDepthStencilState::~IDepthStencilState(void)
{
}

bool IDepthStencilState::_create(const FX::DEPTHSTENCIL_STATE_DESC& dsStateDesc)
{
	m_dsStateDesc = dsStateDesc;

	D3D11_DEPTH_STENCIL_DESC d3dDSDesc = {};
	Memory::MemZero_T(d3dDSDesc);

	// Depth part
	d3dDSDesc.DepthEnable = (m_dsStateDesc.DepthTest != E_CompareOp::Always) || m_dsStateDesc.EnableDepthWrite;
	d3dDSDesc.DepthWriteMask = m_dsStateDesc.EnableDepthWrite ? D3D11_DEPTH_WRITE_MASK_ALL : D3D11_DEPTH_WRITE_MASK_ZERO;
	d3dDSDesc.DepthFunc = FX::Sampler::GetCompareFunction(m_dsStateDesc.DepthTest);

	// Stencil part
	d3dDSDesc.StencilEnable = (m_dsStateDesc.EnableFrontFaceStencil || m_dsStateDesc.EnableBackFaceStencil);
	d3dDSDesc.StencilReadMask = m_dsStateDesc.StencilReadMask;
	d3dDSDesc.StencilWriteMask = m_dsStateDesc.StencilWriteMask;
	d3dDSDesc.FrontFace.StencilFunc = FX::Sampler::GetCompareFunction(m_dsStateDesc.FrontFaceStencilTest);
	d3dDSDesc.FrontFace.StencilFailOp = FX::Sampler::GetStencilOp(m_dsStateDesc.FrontFaceStencilFailStencilOp);
	d3dDSDesc.FrontFace.StencilDepthFailOp = FX::Sampler::GetStencilOp(m_dsStateDesc.FrontFaceDepthFailStencilOp);
	d3dDSDesc.FrontFace.StencilPassOp = FX::Sampler::GetStencilOp(m_dsStateDesc.FrontFacePassStencilOp);
	if (m_dsStateDesc.EnableBackFaceStencil)
	{
		d3dDSDesc.BackFace.StencilFunc = FX::Sampler::GetCompareFunction(m_dsStateDesc.BackFaceStencilTest);
		d3dDSDesc.BackFace.StencilFailOp = FX::Sampler::GetStencilOp(m_dsStateDesc.BackFaceStencilFailStencilOp);
		d3dDSDesc.BackFace.StencilDepthFailOp = FX::Sampler::GetStencilOp(m_dsStateDesc.BackFaceDepthFailStencilOp);
		d3dDSDesc.BackFace.StencilPassOp = FX::Sampler::GetStencilOp(m_dsStateDesc.BackFacePassStencilOp);
	}
	else
	{
		d3dDSDesc.BackFace = d3dDSDesc.FrontFace;
	}

	if (DX11ResultCheckFail(m_pAdapter->impl().d3dDevice()->CreateDepthStencilState(&d3dDSDesc, &m_impl->m_pD3DDSState)))
	{
		PRINT_ASSERT << "GFX: Failed CreateSamplerState()";
	}
	return true;
}

bool IDepthStencilState::_release(void)
{
	m_dsStateDesc = FX::DEPTHSTENCIL_STATE_DESC();
	FXD_RELEASE(m_impl->m_pD3DDSState, Release());
	return true;
}

void IDepthStencilState::_attach(void)
{
	GFX::Cache::DepthStencil::Set(m_pAdapter->impl().d3dDevContext(), m_impl->m_pD3DDSState, 1);
}

void IDepthStencilState::_detach(void)
{
	GFX::Cache::DepthStencil::Set(m_pAdapter->impl().d3dDevContext(), nullptr, 1);
}
#endif //IsGFXDX11()