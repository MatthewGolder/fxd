// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/DX11/GfxAdapterDX11.h"
#include "FXDEngine/GFX/DX11/HWBufferDX11.h"
#include "FXDEngine/GFX/FX/DX11/ShaderModuleDX11.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// IShaderModule::Impl::Impl
// - 
// ------
IShaderModule::Impl::Impl::Impl(void)
	: m_pD3DShader(nullptr)
{
}

IShaderModule::Impl::~Impl(void)
{
	PRINT_COND_ASSERT((m_pD3DShader == nullptr), "GFX: m_pD3DShader is not shutdown");
}

// ------
// IShaderModule
// - 
// ------
IShaderModule::IShaderModule(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
	, m_eType(FX::E_ShaderType::Unknown)
{
}

IShaderModule::~IShaderModule(void)
{
}

bool IShaderModule::_create(const FX::E_ShaderType eType, const Memory::MemHandle& mem)
{
	m_eType = eType;

	switch (m_eType)
	{
		case FX::E_ShaderType::Vertex:
		{
			if (DX11ResultCheckFail(m_pAdapter->impl().d3dDevice()->CreateVertexShader(Memory::MemHandle::LockGuard(mem).get_mem(), mem.get_size(), m_pAdapter->impl().d3dClassLinkage(), (ID3D11VertexShader**)&m_impl->m_pD3DShader)))
			{
				PRINT_ASSERT << "GFX: Failed CreateVertexShader()";
			}
			break;
		}
		/*
		case FX::E_ShaderType::Geometry:
		{
			if (DX11ResultCheckFail(m_pAdapter->impl().d3dDevice()->CreateGeometryShader(Memory::MemHandle::LockGuard(mem).get_mem(), mem.get_size(), m_pAdapter->impl().d3dClassLinkage(), (ID3D11GeometryShader**)&m_impl->m_pD3DShader)))
			{
				PRINT_ASSERT << "GFX: Failed CreatePixelShader()";
			}
			break;
		}
		*/
		case FX::E_ShaderType::Fragment:
		{
			if (DX11ResultCheckFail(m_pAdapter->impl().d3dDevice()->CreatePixelShader(Memory::MemHandle::LockGuard(mem).get_mem(), mem.get_size(), m_pAdapter->impl().d3dClassLinkage(), (ID3D11PixelShader**)&m_impl->m_pD3DShader)))
			{
				PRINT_ASSERT << "GFX: Failed CreatePixelShader()";
			}
			break;
		}
		default:
		{
			PRINT_ASSERT << "GFX: Shader Buffer Type";
		}
	}
	return true;
}

bool IShaderModule::_release(void)
{
	FXD_RELEASE(m_impl->m_pD3DShader, Release());
	return true;
}

bool IShaderModule::_set_uniform(GFX::UniformBuffer& uniformBuffer)
{
	switch (m_eType)
	{
		case FX::E_ShaderType::Vertex:
		{
			GFX::Cache::UniformBuffer::SetVertex(m_pAdapter->impl().d3dDevContext(), uniformBuffer->impl().m_pD3DBuffer, 0);
			break;
		}
		/*
		case FX::E_ShaderType::Geometry:
		{
			GFX::Cache::UniformBuffer::SetGeometry(m_pAdapter->impl().d3dDevContext(),uniformBuffer->impl().m_pD3DBuffer, 0);
			break;
		}
		*/
		case FX::E_ShaderType::Fragment:
		{
			GFX::Cache::UniformBuffer::SetFragment(m_pAdapter->impl().d3dDevContext(), uniformBuffer->impl().m_pD3DBuffer, 0);
			break;
		}
		default:
		{
			PRINT_ASSERT << "GFX: Shader Buffer Type";
		}
	}
	return true;
}

void IShaderModule::_attach(void)
{
	switch (m_eType)
	{
		case FX::E_ShaderType::Vertex:
		{
			GFX::Cache::Shader::Set< FX::E_ShaderType::Vertex >(m_pAdapter->impl().d3dDevContext(), m_impl->m_pD3DShader);
			break;
		}
		/*
		case FX::E_ShaderType::Geometry:
		{
			GFX::Cache::Shader::Set< FX::E_ShaderType::Geometry >(m_pAdapter->impl().d3dDevContext(), m_impl->m_pD3DShader);
			break;
		}
		*/
		case FX::E_ShaderType::Fragment:
		{
			GFX::Cache::Shader::Set< FX::E_ShaderType::Fragment >(m_pAdapter->impl().d3dDevContext(), m_impl->m_pD3DShader);
			break;
		}
		default:
		{
			PRINT_ASSERT << "GFX: Shader Buffer Type";
		}
	}
}

void IShaderModule::_detach(void)
{
	switch (m_eType)
	{
		case FX::E_ShaderType::Vertex:
		{
			GFX::Cache::Shader::SetVertex(m_pAdapter->impl().d3dDevContext(), nullptr);
			break;
		}
		/*
		case FX::E_ShaderType::Geometry:
		{
			GFX::Cache::Shader::SetGeometry(m_pAdapter->impl().d3dDevContext(),nullptr);
			break;
		}
		*/
		case FX::E_ShaderType::Fragment:
		{
			GFX::Cache::Shader::SetFragment(m_pAdapter->impl().d3dDevContext(), nullptr);
			break;
		}
		default:
		{
			PRINT_ASSERT << "GFX: Shader Buffer Type";
		}
	}
}
#endif //IsGFXDX11()