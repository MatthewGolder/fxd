// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/DX11/GfxAdapterDX11.h"
#include "FXDEngine/GFX/FX/DX11/MappingFXDX11.h"
#include "FXDEngine/GFX/FX/DX11/SamplerStateDX11.h"
#include "FXDEngine/Core/Algorithm.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// ISamplerState::Impl::Impl
// - 
// ------
ISamplerState::Impl::Impl::Impl(void)
	: m_pD3DSamplerState(nullptr)
{
}

ISamplerState::Impl::~Impl(void)
{
	PRINT_COND_ASSERT((m_pD3DSamplerState == nullptr), "GFX: m_pD3DSamplerState is not shutdown");
}

// ------
// ISamplerState
// - 
// ------
ISamplerState::ISamplerState(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

ISamplerState::~ISamplerState(void)
{
}

bool ISamplerState::_create(const FX::SAMPLER_DESC& sampleDesc)
{
	m_samplerDesc = sampleDesc;

	D3D11_SAMPLER_DESC d3dSamplerDesc = {};
	Memory::MemZero_T(d3dSamplerDesc);
	d3dSamplerDesc.AddressU = FX::Sampler::GetAddressMode(m_samplerDesc.AddressModeU);
	d3dSamplerDesc.AddressV = FX::Sampler::GetAddressMode(m_samplerDesc.AddressModeV);
	d3dSamplerDesc.AddressW = FX::Sampler::GetAddressMode(m_samplerDesc.AddressModeW);
	d3dSamplerDesc.BorderColor[0] = m_samplerDesc.Border.getR();
	d3dSamplerDesc.BorderColor[1] = m_samplerDesc.Border.getG();
	d3dSamplerDesc.BorderColor[2] = m_samplerDesc.Border.getB();
	d3dSamplerDesc.BorderColor[3] = m_samplerDesc.Border.getA();
	d3dSamplerDesc.MaxAnisotropy = FXD::STD::clamp< FXD::U32 >(m_samplerDesc.MaxAnisotropy, 0, 16);
	d3dSamplerDesc.MinLOD = m_samplerDesc.MinMipLevel;
	d3dSamplerDesc.MaxLOD = m_samplerDesc.MaxMipLevel;
	d3dSamplerDesc.MipLODBias = m_samplerDesc.MipBias;

	const bool bComparisonEnabled = (m_samplerDesc.CompareOp != FX::E_CompareOp::Never);
	d3dSamplerDesc.ComparisonFunc = FX::Sampler::GetCompareFunction(m_samplerDesc.CompareOp);
	d3dSamplerDesc.Filter = FX::Sampler::GetFilterMode(m_samplerDesc.FilterMode, bComparisonEnabled, d3dSamplerDesc.MaxAnisotropy);

	if (DX11ResultCheckFail(m_pAdapter->impl().d3dDevice()->CreateSamplerState(&d3dSamplerDesc, &m_impl->m_pD3DSamplerState)))
	{
		PRINT_ASSERT << "GFX: Failed CreateSamplerState()";
	}
	return true;
}

bool ISamplerState::_release(void)
{
	m_samplerDesc = FX::SAMPLER_DESC();
	FXD_RELEASE(m_impl->m_pD3DSamplerState, Release());
	return true;
}

void ISamplerState::_attach(void)
{
	//m_pAdapter->impl().dx11State().setVertexSamplerState(m_pAdapter->impl().d3dDevContext(), m_impl->m_pD3DSamplerState);
}

void ISamplerState::_detach(void)
{
	//m_pAdapter->impl().dx11State().setVertexSamplerState(m_pAdapter->impl().d3dDevContext(), nullptr);
}
#endif //IsGFXDX11()