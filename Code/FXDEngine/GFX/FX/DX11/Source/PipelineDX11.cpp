// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/DX11/GfxAdapterDX11.h"
#include "FXDEngine/GFX/FX/DX11/PipelineDX11.h"
#include "FXDEngine/GFX/FX/BlendState.h"
#include "FXDEngine/GFX/FX/DepthStencilState.h"
#include "FXDEngine/GFX/FX/RasterState.h"
#include "FXDEngine/GFX/FX/SamplerState.h"
#include "FXDEngine/GFX/FX/ShaderModule.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// IPipeline::Impl::Impl
// - 
// ------
IPipeline::Impl::Impl::Impl(void)
{
}
IPipeline::Impl::~Impl(void)
{
}

// ------
// IPipeline
// - 
// ------
IPipeline::IPipeline(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IPipeline::~IPipeline(void)
{
}

bool IPipeline::_create(const FX::PIPELINE_DESC& pipelineDesc)
{
	m_pipelineDesc = pipelineDesc;
	return true;
}

bool IPipeline::_release(void)
{
	m_pipelineDesc = FX::PIPELINE_DESC();
	return true;
}

void IPipeline::_attach(void)
{
	if (m_pipelineDesc.BlendState != nullptr)
	{
		m_pipelineDesc.BlendState->attach();
	}
	if (m_pipelineDesc.RasterizerState != nullptr)
	{
		m_pipelineDesc.RasterizerState->attach();
	}
	if (m_pipelineDesc.DepthStencilState != nullptr)
	{
		m_pipelineDesc.DepthStencilState->attach();
	}

	if (m_pipelineDesc.vertexProgram != nullptr)
	{
		m_pipelineDesc.vertexProgram->attach();
	}
	else
	{
		m_pipelineDesc.vertexProgram->detach();
	}
	if (m_pipelineDesc.geometryProgram != nullptr)
	{
		m_pipelineDesc.geometryProgram->attach();
	}
	else
	{
		m_pipelineDesc.geometryProgram->detach();
	}
	if (m_pipelineDesc.fragmentProgram != nullptr)
	{
		m_pipelineDesc.fragmentProgram->attach();
	}
	else
	{
		m_pipelineDesc.fragmentProgram->detach();
	}

	/*
	D3D11BlendState* d3d11BlendState;
	D3D11RasterizerState* d3d11RasterizerState;

	D3D11GpuFragmentProgram* d3d11FragmentProgram;
	D3D11GpuGeometryProgram* d3d11GeometryProgram;
	D3D11GpuDomainProgram* d3d11DomainProgram;
	D3D11GpuHullProgram* d3d11HullProgram;

	if (pipelineState != nullptr)
	{
		d3d11BlendState = static_cast<D3D11BlendState*>(pipelineState->getBlendState().get());
		d3d11RasterizerState = static_cast<D3D11RasterizerState*>(pipelineState->getRasterizerState().get());
		mActiveDepthStencilState = std::static_pointer_cast<D3D11DepthStencilState>(pipelineState->getDepthStencilState());

		mActiveVertexShader = std::static_pointer_cast<D3D11GpuVertexProgram>(pipelineState->getVertexProgram());
		d3d11FragmentProgram = static_cast<D3D11GpuFragmentProgram*>(pipelineState->getFragmentProgram().get());
		d3d11GeometryProgram = static_cast<D3D11GpuGeometryProgram*>(pipelineState->getGeometryProgram().get());
		d3d11DomainProgram = static_cast<D3D11GpuDomainProgram*>(pipelineState->getDomainProgram().get());
		d3d11HullProgram = static_cast<D3D11GpuHullProgram*>(pipelineState->getHullProgram().get());

		if (d3d11BlendState == nullptr)
		d3d11BlendState = static_cast<D3D11BlendState*>(BlendState::getDefault().get());

		if (d3d11RasterizerState == nullptr)
		{
			d3d11RasterizerState = static_cast<D3D11RasterizerState*>(RasterizerState::getDefault().get());
		}

		if (mActiveDepthStencilState == nullptr)
		{
			mActiveDepthStencilState = std::static_pointer_cast<D3D11DepthStencilState>(DepthStencilState::getDefault());
		}
	}
	else
	{
		d3d11BlendState = static_cast<D3D11BlendState*>(BlendState::getDefault().get());
		d3d11RasterizerState = static_cast<D3D11RasterizerState*>(RasterizerState::getDefault().get());
		mActiveDepthStencilState = std::static_pointer_cast<D3D11DepthStencilState>(DepthStencilState::getDefault());

		mActiveVertexShader = nullptr;
		d3d11FragmentProgram = nullptr;
		d3d11GeometryProgram = nullptr;
		d3d11DomainProgram = nullptr;
		d3d11HullProgram = nullptr;
	}

	ID3D11DeviceContext* d3d11Context = mDevice->getImmediateContext();
	d3d11Context->OMSetBlendState(d3d11BlendState->getInternal(), nullptr, 0xFFFFFFFF);
	d3d11Context->RSSetState(d3d11RasterizerState->getInternal());
	d3d11Context->OMSetDepthStencilState(mActiveDepthStencilState->getInternal(), mStencilRef);

	if (mActiveVertexShader != nullptr)
	{
		D3D11GpuVertexProgram* vertexProgram = static_cast<D3D11GpuVertexProgram*>(mActiveVertexShader.get());
		d3d11Context->VSSetShader(vertexProgram->getVertexShader(), nullptr, 0);
	}
	else
	{
		d3d11Context->VSSetShader(nullptr, nullptr, 0);
	}

	if (d3d11FragmentProgram != nullptr)
	{
		d3d11Context->PSSetShader(d3d11FragmentProgram->getPixelShader(), nullptr, 0);
	}
	else
	{
		d3d11Context->PSSetShader(nullptr, nullptr, 0);
	}

	if (d3d11GeometryProgram != nullptr)
	{
		d3d11Context->GSSetShader(d3d11GeometryProgram->getGeometryShader(), nullptr, 0);
	}
	else
	{
		d3d11Context->GSSetShader(nullptr, nullptr, 0);
	}

	if (d3d11DomainProgram != nullptr)
	{
		d3d11Context->DSSetShader(d3d11DomainProgram->getDomainShader(), nullptr, 0);
	}
	else
	{
		d3d11Context->DSSetShader(nullptr, nullptr, 0);
	}

	if (d3d11HullProgram != nullptr)
	{
		d3d11Context->HSSetShader(d3d11HullProgram->getHullShader(), nullptr, 0);
	}
	else
	{
		d3d11Context->HSSetShader(nullptr, nullptr, 0);
	}
	*/
}

void IPipeline::_detach(void)
{
	/*
	m_pipelineDesc.BlendState->detach();
	m_pipelineDesc.RasterizerState->detach();
	m_pipelineDesc.DepthStencilState->detach();
	m_pipelineDesc.vertexProgram->detach();
	m_pipelineDesc.fragmentProgram->detach();
	m_pipelineDesc.geometryProgram->detach();
	*/
}
#endif //IsGFXDX11()