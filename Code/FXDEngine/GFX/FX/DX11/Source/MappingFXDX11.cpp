// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/FX/DX11/MappingFXDX11.h"

using namespace FXD;
using namespace GFX;

// ------
// E_FilterMode
// -
// ------
const D3D11_FILTER FXD::GFX::FX::Sampler::GetFilterMode(const FX::E_FilterMode eMode, bool bComparisonEnabled, FXD::F32 fMaxAnisotropy)
{
	switch (eMode)
	{
		case FX::E_FilterMode::Point:
		{
			return bComparisonEnabled ? D3D11_FILTER_COMPARISON_MIN_MAG_MIP_POINT : D3D11_FILTER_MIN_MAG_MIP_POINT;
			break;
		}
		case FX::E_FilterMode::Bilinear:
		{
			return bComparisonEnabled ? D3D11_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT : D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT;
			break;
		}
		case FX::E_FilterMode::Trilinear:
		{
			return bComparisonEnabled ? D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR : D3D11_FILTER_MIN_MAG_MIP_LINEAR;
			break;
		}
		case FX::E_FilterMode::AnisotropicPoint:
		case FX::E_FilterMode::AnisotropicLinear:
		{
			if (fMaxAnisotropy == 1)
			{
				return bComparisonEnabled ? D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR : D3D11_FILTER_MIN_MAG_MIP_LINEAR;
			}
			else
			{
				return bComparisonEnabled ? D3D11_FILTER_COMPARISON_ANISOTROPIC : D3D11_FILTER_ANISOTROPIC;
			}
			break;
		}
	}
	return D3D11_FILTER_MIN_MAG_MIP_POINT;
}

// ------
// E_AddressMode
// -
// ------
const D3D11_TEXTURE_ADDRESS_MODE sD3DAddressModes[] =
{
	D3D11_TEXTURE_ADDRESS_WRAP,	// FX::E_AddressMode::Wrap
	D3D11_TEXTURE_ADDRESS_MIRROR,	// FX::E_AddressMode::Mirror
	D3D11_TEXTURE_ADDRESS_CLAMP,	// FX::E_AddressMode::Clamp
	D3D11_TEXTURE_ADDRESS_BORDER	// FX::E_AddressMode::ClampBorder
};
const D3D11_TEXTURE_ADDRESS_MODE FXD::GFX::FX::Sampler::GetAddressMode(const FX::E_AddressMode eMode)
{
	return sD3DAddressModes[FXD::STD::to_underlying(eMode)];
}

// ------
// E_CompareOp
// -
// ------
const D3D11_COMPARISON_FUNC sD3DCompareFunctions[] =
{
	D3D11_COMPARISON_LESS,				// FX::E_CompareOp::Less
	D3D11_COMPARISON_LESS_EQUAL,		// FX::E_CompareOp::LessEqual
	D3D11_COMPARISON_GREATER,			// FX::E_CompareOp::Greater
	D3D11_COMPARISON_GREATER_EQUAL,	// FX::E_CompareOp::GreaterEqual
	D3D11_COMPARISON_EQUAL,				// FX::E_CompareOp::Equal
	D3D11_COMPARISON_NOT_EQUAL,		// FX::E_CompareOp::NotEqual
	D3D11_COMPARISON_NEVER,				// FX::E_CompareOp::Never
	D3D11_COMPARISON_ALWAYS				// FX::E_CompareOp::Always
};
const D3D11_COMPARISON_FUNC FXD::GFX::FX::Sampler::GetCompareFunction(const FX::E_CompareOp eMode)
{
	return sD3DCompareFunctions[FXD::STD::to_underlying(eMode)];
}

// ------
// E_StencilOp
// -
// ------
const D3D11_STENCIL_OP sD3DStencilOps[] =
{
	D3D11_STENCIL_OP_KEEP,			// FX::E_StencilOp::Keep
	D3D11_STENCIL_OP_ZERO,			// FX::E_StencilOp::Zero
	D3D11_STENCIL_OP_REPLACE,		// FX::E_StencilOp::Replace
	D3D11_STENCIL_OP_INCR_SAT,		// FX::E_StencilOp::SaturatedIncrement
	D3D11_STENCIL_OP_DECR_SAT,		// FX::E_StencilOp::SaturatedDecrement
	D3D11_STENCIL_OP_INCR,			// FX::E_StencilOp::Increment
	D3D11_STENCIL_OP_DECR,			// FX::E_StencilOp::Decrement
	D3D11_STENCIL_OP_INVERT			// FX::E_StencilOp::Invert
};
const D3D11_STENCIL_OP FXD::GFX::FX::Sampler::GetStencilOp(const FX::E_StencilOp eMode)
{
	return sD3DStencilOps[FXD::STD::to_underlying(eMode)];
}

// ------
// E_BlendOp
// -
// ------
const D3D11_BLEND_OP sD3DBlendOps[] =
{
	D3D11_BLEND_OP_ADD,				// FX::E_BlendOp::Add
	D3D11_BLEND_OP_SUBTRACT,		// FX::E_BlendOp::Subtract
	D3D11_BLEND_OP_MIN,				// FX::E_BlendOp::Min
	D3D11_BLEND_OP_MAX,				// FX::E_BlendOp::Max
	D3D11_BLEND_OP_REV_SUBTRACT,	// FX::E_BlendOp::ReverseSubtract
};
const D3D11_BLEND_OP FXD::GFX::FX::Sampler::GetBlendOp(const FX::E_BlendOp eMode)
{
	return sD3DBlendOps[FXD::STD::to_underlying(eMode)];
}

// ------
// E_BlendFactor
// -
// ------
const D3D11_BLEND sD3DBlendFactors[] =
{
	D3D11_BLEND_ZERO,					// FX::E_BlendFactor::Zero
	D3D11_BLEND_ONE,					// FX::E_BlendFactor::One
	D3D11_BLEND_SRC_COLOR,			// FX::E_BlendFactor::SourceColor
	D3D11_BLEND_INV_SRC_COLOR,		// FX::E_BlendFactor::InverseSourceColor
	D3D11_BLEND_SRC_ALPHA,			// FX::E_BlendFactor::SourceAlpha
	D3D11_BLEND_INV_SRC_ALPHA,		// FX::E_BlendFactor::InverseSourceAlpha
	D3D11_BLEND_DEST_ALPHA,			// FX::E_BlendFactor::DestAlpha
	D3D11_BLEND_INV_DEST_ALPHA,	// FX::E_BlendFactor::InverseDestAlpha
	D3D11_BLEND_DEST_COLOR,			// FX::E_BlendFactor::DestColor
	D3D11_BLEND_INV_DEST_COLOR,	// FX::E_BlendFactor::InverseDestColor
	D3D11_BLEND_BLEND_FACTOR,		// FX::E_BlendFactor::ConstantBlendFactor
	D3D11_BLEND_INV_BLEND_FACTOR	// FX::E_BlendFactor::InverseConstantBlendFactor
};
const D3D11_BLEND FXD::GFX::FX::Sampler::GetBlendFactor(const FX::E_BlendFactor eMode)
{
	return sD3DBlendFactors[FXD::STD::to_underlying(eMode)];
}
#endif //IsGFXDX11()