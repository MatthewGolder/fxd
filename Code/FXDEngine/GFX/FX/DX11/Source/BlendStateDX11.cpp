// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/DX11/GfxAdapterDX11.h"
#include "FXDEngine/GFX/FX/DX11/BlendStateDX11.h"
#include "FXDEngine/GFX/FX/DX11/MappingFXDX11.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// IBlendState::Impl::Impl
// - 
// ------
IBlendState::Impl::Impl::Impl(void)
	: m_pD3DBlendState(nullptr)
{
}

IBlendState::Impl::~Impl(void)
{
	PRINT_COND_ASSERT((m_pD3DBlendState == nullptr), "GFX: m_pD3DBlendState is not shutdown");
}

// ------
// IBlendState
// - 
// ------
IBlendState::IBlendState(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IBlendState::~IBlendState(void)
{
}

bool IBlendState::_create(const FX::BLEND_STATE_DESC& blendDesc)
{
	m_blendDesc = blendDesc;

	D3D11_BLEND_DESC d3dBlendDesc = {};
	Memory::MemZero_T(d3dBlendDesc);

	d3dBlendDesc.AlphaToCoverageEnable = false;
	d3dBlendDesc.IndependentBlendEnable = m_blendDesc.UseIndependentRenderTargetBlendStates;

	for (FXD::U16 n1 = 0; n1 < GFX::kMaxMultipleRenderTargets; n1++)
	{
		const BLEND_STATE_DESC::RT_DESC& rtBlendDesc = m_blendDesc.RenderTargets[n1];
		D3D11_RENDER_TARGET_BLEND_DESC& d3dRTBlendDesc = d3dBlendDesc.RenderTarget[n1];
		Memory::MemZero_T(d3dRTBlendDesc);

		d3dRTBlendDesc.BlendEnable =
			(rtBlendDesc.ColorBlendOp != E_BlendOp::Add) || (rtBlendDesc.ColorDestBlend != E_BlendFactor::Zero) || (rtBlendDesc.ColorSrcBlend != E_BlendFactor::One) ||
			(rtBlendDesc.AlphaBlendOp != E_BlendOp::Add) || (rtBlendDesc.AlphaDestBlend != E_BlendFactor::Zero) || (rtBlendDesc.AlphaSrcBlend != E_BlendFactor::One);
		
		d3dRTBlendDesc.BlendOp = FX::Sampler::GetBlendOp(rtBlendDesc.ColorBlendOp);
		d3dRTBlendDesc.SrcBlend = FX::Sampler::GetBlendFactor(rtBlendDesc.ColorSrcBlend);
		d3dRTBlendDesc.DestBlend = FX::Sampler::GetBlendFactor(rtBlendDesc.ColorDestBlend);

		d3dRTBlendDesc.BlendOpAlpha = FX::Sampler::GetBlendOp(rtBlendDesc.AlphaBlendOp);
		d3dRTBlendDesc.SrcBlendAlpha = FX::Sampler::GetBlendFactor(rtBlendDesc.AlphaSrcBlend);
		d3dRTBlendDesc.DestBlendAlpha = FX::Sampler::GetBlendFactor(rtBlendDesc.AlphaDestBlend);

		d3dRTBlendDesc.RenderTargetWriteMask =
			((rtBlendDesc.ColorWriteMask & FX::E_ColorWriteMask::CW_RED) ? D3D11_COLOR_WRITE_ENABLE_RED : 0) |
			((rtBlendDesc.ColorWriteMask & FX::E_ColorWriteMask::CW_GREEN) ? D3D11_COLOR_WRITE_ENABLE_GREEN : 0) |
			((rtBlendDesc.ColorWriteMask & FX::E_ColorWriteMask::CW_BLUE) ? D3D11_COLOR_WRITE_ENABLE_BLUE : 0) |
			((rtBlendDesc.ColorWriteMask & FX::E_ColorWriteMask::CW_ALPHA) ? D3D11_COLOR_WRITE_ENABLE_ALPHA : 0);
	}

	if (DX11ResultCheckFail(m_pAdapter->impl().d3dDevice()->CreateBlendState(&d3dBlendDesc, &m_impl->m_pD3DBlendState)))
	{
		PRINT_ASSERT << "GFX: Failed CreateSamplerState()";
	}
	return true;
}

bool IBlendState::_release(void)
{
	m_blendDesc = FX::BLEND_STATE_DESC();
	FXD_RELEASE(m_impl->m_pD3DBlendState, Release());
	return true;
}

void IBlendState::_attach(void)
{
	GFX::Cache::BlendState::Set(m_pAdapter->impl().d3dDevContext(), m_impl->m_pD3DBlendState, 0, 0xFFFFFFFF);
}

void IBlendState::_detach(void)
{
	GFX::Cache::BlendState::Set(m_pAdapter->impl().d3dDevContext(), nullptr, 0, 0xFFFFFFFF);
}
#endif //IsGFXDX11()