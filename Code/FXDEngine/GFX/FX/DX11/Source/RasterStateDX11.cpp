// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/HWBuffer.h"
#include "FXDEngine/GFX/DX11/GfxAdapterDX11.h"
#include "FXDEngine/GFX/DX11/MappingDX11.h"
#include "FXDEngine/GFX/FX/DX11/RasterStateDX11.h"
#include "FXDEngine/Math/Math.h"

using namespace FXD;
using namespace GFX;
using namespace FX;

// ------
// IRasterState::Impl::Impl
// - 
// ------
IRasterState::Impl::Impl::Impl(void)
	: m_pD3DRasterState(nullptr)
{
}

IRasterState::Impl::~Impl(void)
{
	PRINT_COND_ASSERT((m_pD3DRasterState == nullptr), "GFX: m_pD3DRasterState is not shutdown");
}

// ------
// IRasterState
// - 
// ------
IRasterState::IRasterState(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IRasterState::~IRasterState(void)
{
}

bool IRasterState::_create(const FX::RASTER_STATE_DESC& rasterDesc)
{
	m_rasterDesc = rasterDesc;

	D3D11_RASTERIZER_DESC d3dRasterDesc = {};
	Memory::MemZero_T(d3dRasterDesc);
	d3dRasterDesc.CullMode = GFX::DX11::GetCullMode(m_rasterDesc.CullMode);
	d3dRasterDesc.FillMode = GFX::DX11::GetFillMode(m_rasterDesc.PolygonMode);
	d3dRasterDesc.SlopeScaledDepthBias = m_rasterDesc.SlopeScaledDepthBias;
	d3dRasterDesc.FrontCounterClockwise = true;
	d3dRasterDesc.DepthBias = (INT)Math::FloorI(m_rasterDesc.DepthBias * (FXD::F32)(1 << 24));
	d3dRasterDesc.DepthClipEnable = true;
	d3dRasterDesc.MultisampleEnable = m_rasterDesc.MultisampleEnable;

	if (DX11ResultCheckFail(m_pAdapter->impl().d3dDevice()->CreateRasterizerState(&d3dRasterDesc, &m_impl->m_pD3DRasterState)))
	{
		PRINT_ASSERT << "GFX: Failed CreateSamplerState()";
	}
	return true;
}

bool IRasterState::_release(void)
{
	m_rasterDesc = FX::RASTER_STATE_DESC();
	FXD_RELEASE(m_impl->m_pD3DRasterState, Release());
	return true;
}

void IRasterState::_attach(void)
{
	GFX::Cache::RasterState::Set(m_pAdapter->impl().d3dDevContext(), m_impl->m_pD3DRasterState);
}

void IRasterState::_detach(void)
{
	GFX::Cache::RasterState::Set(m_pAdapter->impl().d3dDevContext(), nullptr);
}
#endif //IsGFXDX11()