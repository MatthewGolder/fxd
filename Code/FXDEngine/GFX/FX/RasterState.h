// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_RASTERSTATE_H
#define FXDENGINE_GFX_FX_RASTERSTATE_H

#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/GFX/FX/Types.h"
#include "FXDEngine/Job/Future.h"

namespace FXD
{
	namespace GFX
	{
		namespace FX
		{
			struct RASTER_STATE_DESC
			{
				GFX::E_FillMode PolygonMode = E_FillMode::Solid;
				GFX::E_CullMode CullMode = E_CullMode::CounterClockwise;
				FXD::F32 DepthBias = 0.0f;
				FXD::F32 SlopeScaledDepthBias = 0.0f;
				bool MultisampleEnable = true;
				bool AntialiasedLineEnable = false;
			};

			// ------
			// IRasterState
			// -
			// ------
			class IRasterState : public Core::HasImpl< FX::IRasterState, 64 >, public Core::NonCopyable
			{
			public:
				IRasterState(GFX::IGfxAdapter* pAdapter);
				virtual ~IRasterState(void);

				Job::Future< bool > create(const FX::RASTER_STATE_DESC& rasterDesc);
				Job::Future< bool > release(void);

				Job::Future< void > attach(void);
				Job::Future< void > detach(void);

			protected:
				bool _create(const FX::RASTER_STATE_DESC& rasterDesc);
				bool _release(void);

				void _attach(void);
				void _detach(void);

			private:
				GFX::IGfxAdapter* m_pAdapter;
				FX::RASTER_STATE_DESC m_rasterDesc;
			};

		} //namespace FX
	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_FX_RASTERSTATE_H