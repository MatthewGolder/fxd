// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_SHADERFILEDEF_H
#define FXDENGINE_GFX_FX_SHADERFILEDEF_H

#include "FXDEngine/Core/NonCopyable.h"
#include "FXDEngine/GFX/FX/Semantics.h"
#include "FXDEngine/IO/XML/XMLSaxParser.h"
#include "FXDEngine/IO/XML/XMLWriter.h"

namespace FXD
{
	namespace GFX
	{
		namespace FX
		{
			// ------
			// ShaderFileDef
			// -
			// ------
			class ShaderFileDef
			{
			public:
				ShaderFileDef(void);
				~ShaderFileDef(void);

				void writeXml(Core::StreamOut& stream) const;

			private:
				void _write_xml_textures(Core::StreamOut& stream, IO::XMLWriter8::XMLNode* pParent) const;
				void _write_xml_constant_buffer(Core::StreamOut& stream, IO::XMLWriter8::XMLNode* pParent) const;
				void _write_xml_vertex_shader_input(Core::StreamOut& stream, IO::XMLWriter8::XMLNode* pParent) const;
				void _write_xml_vertex_shader_output(Core::StreamOut& stream, IO::XMLWriter8::XMLNode* pParent) const;
				void _write_xml_pixel_shader_output(Core::StreamOut& stream, IO::XMLWriter8::XMLNode* pParent) const;
				void _write_xml_vertex_shader(Core::StreamOut& stream, IO::XMLWriter8::XMLNode* pParent) const;
				void _write_xml_pixel_shader(Core::StreamOut& stream, IO::XMLWriter8::XMLNode* pParent) const;

			public:
				Core::String8 m_strFileName;
				GFX::E_GfxApi m_eType;

				Container::Vector< FX::TextureDef > m_textures;
				Container::Vector< FX::ConstantBufferDef > m_constantBuffers;
				FX::VaryingListDef m_vShaderIn;
				FX::VaryingListDef m_vShaderOut;
				FX::VaryingListDef m_pShaderOut;
				Core::String8 m_vertexShader;
				Core::String8 m_pixelShader;
			};


			// ------
			// ShaderFileTagCB
			// -
			// ------
			class ShaderFileTagCB FINAL : public IO::XMLSaxParser::ITagCB, public Core::NonCopyable
			{
			public:
				enum class E_Tag
				{
					Effect = 0,
					SamplerDef,
					TextureDef,
					ConstantBuffer,
					VertexShaderInput,
					VertexShaderOutput,
					PixelShaderOutput,
					VertexShader,
					PixelShader,
					Count,
					Unknown = 0xffff
				};

			public:
				ShaderFileTagCB(void);
				~ShaderFileTagCB(void);

				void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL;
				void end_tag(const Core::String8& strName) FINAL;
				void handle_text(const Core::String8& strText, const IO::StreamParser::Position& pos) FINAL;
				void handle_attribute(const Core::String8& strName, const Core::String8& strValue) FINAL;

			public:
				IO::XMLSaxParser::ITagCB* m_pChild;
				ShaderFileTagCB::E_Tag m_eTag;
				ShaderFileDef m_FXFileDef;
			};

		} //namespace FX
	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_FX_SHADERFILEDEF_H