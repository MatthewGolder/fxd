// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_FX_PIPELINE_H
#define FXDENGINE_GFX_FX_PIPELINE_H

#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/GFX/FX/Types.h"
#include "FXDEngine/Job/Future.h"

namespace FXD
{
	namespace GFX
	{
		namespace FX
		{
			struct PIPELINE_DESC
			{
				FX::BlendState BlendState;
				FX::RasterState RasterizerState;
				FX::DepthStencilState DepthStencilState;

				FX::ShaderModule vertexProgram;
				FX::ShaderModule fragmentProgram;
				FX::ShaderModule geometryProgram;
			};

			// ------
			// IPipeline
			// -
			// ------
			class IPipeline : public Core::HasImpl< FX::IPipeline, 64 >, public Core::NonCopyable
			{
			public:
				IPipeline(GFX::IGfxAdapter* pAdapter);
				virtual ~IPipeline(void);

				Job::Future< bool > create(const FX::PIPELINE_DESC& pipelineDesc);
				Job::Future< bool > release(void);

				bool has_vertex_program(void) const		{ return (m_pipelineDesc.vertexProgram != nullptr); }
				bool has_fragment_program(void) const	{ return (m_pipelineDesc.fragmentProgram != nullptr); }
				bool has_geometry_program(void) const	{ return (m_pipelineDesc.geometryProgram != nullptr); }

				Job::Future< void > attach(void);
				Job::Future< void > detach(void);

			protected:
				bool _create(const FX::PIPELINE_DESC& pipelineDesc);
				bool _release(void);

				void _attach(void);
				void _detach(void);

			private:
				GFX::IGfxAdapter* m_pAdapter;
				FX::PIPELINE_DESC m_pipelineDesc;
			};

		} //namespace FX
	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_FX_PIPELINE_H