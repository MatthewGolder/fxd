// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_WINDOWMANAGER_H
#define FXDENGINE_GFX_WINDOWMANAGER_H

#include "FXDEngine/Container/Map.h"
#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/GFX/Types.h"

namespace FXD
{
	namespace GFX
	{
		struct WINDOW_DESC;

		// ------
		// IWindowManager
		// -
		// ------
		class IWindowManager : public Core::HasImpl< GFX::IWindowManager, 64 >, public Core::NonCopyable
		{
		public:
			IWindowManager(void);
			virtual ~IWindowManager(void);

			GFX::Window create_window(const WINDOW_DESC& windowDesc, const GFX::Window& parent = nullptr);
			void destroy_window(GFX::Window& window);

		protected:
			GFX::Window _create_window(const WINDOW_DESC& windowDesc, const GFX::Window& parent = nullptr);
			void _destroy_window(GFX::Window& window);

		protected:
			Container::Map< FXD::U32, GFX::Window > m_windows;
		};

	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_WINDOWMANAGER_H