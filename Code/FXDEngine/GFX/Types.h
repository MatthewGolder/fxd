// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_TYPES_H
#define FXDENGINE_GFX_TYPES_H

#include "FXDEngine/Container/Flag.h"
#include "FXDEngine/Core/NonCopyable.h"
#include "FXDEngine/Math/Vector2I.h"

#include <memory>

#if IsOSPC()
#	define IsGFXDX11() 1
#	define IsGFXOpenGL() 0
#	define IsGFXOpenGLES() 0
#	define IsGFXVulkan() 0
#endif //IsOSPC()
#if IsOSAndroid()
#	define IsGFXDX11() 0
#	define IsGFXOpenGL() 1
#	define IsGFXOpenGLES() 1
#	define IsGFXVulkan() 0
#endif //IsOSAndroid()

namespace FXD
{
	namespace GFX
	{
		CONSTEXPR FXD::U32 kMaxMultipleRenderTargets = 8;
		CONSTEXPR FXD::U32 kMaxVertexElements = 16;
		CONSTEXPR FXD::U32 kMaxQueuesPerType = 8;
		CONSTEXPR FXD::S32 kMipUnlimited = 0x7FFFFFFF;

		// ------
		// E_GfxApi
		// -
		// ------
		enum class E_GfxApi : FXD::U16
		{
			DX11 = 0,
			OpenGL,
			Vulkan,
			Count,
			Unknown = 0xffff
		};


		// ------
		// E_DeviceType
		// -
		// ------
		enum class E_DeviceType : FXD::U16
		{
			IntegratedGPU,		// GPU is integrated on the motherboard.
			DiscreteGPU,		// GPU is dedicated hardware.
			VirtualGPU,			// GPU is remote or virtual.
			SoftwareRenderer,	// CPU is utilised for software rendering.
			Count,
			Unknown = 0xffff
		};


		// ------
		// E_GfxQueueType
		// - Queue used for rendering. Allows the use of draw commands, but also all commands supported by compute or upload buffers
		// ------
		enum class E_GfxQueueType : FXD::U16
		{
			Graphics,
			Compute,		// Discrete queue used for compute operations. Allows the use of dispatch and upload commands.
			Upload,		// Queue used for memory transfer operations only. No rendering or compute dispatch allowed.
			Count,
			Unknown = 0xffff
		};


		// ------
		// E_CommandBufferType
		// -
		// ------
		enum class E_CommandBufferType : FXD::U16
		{
			Primary = 0,	// Primary level command buffers can be added to queues.
			Secondary,		// Secondary level command buffers can be called from primary command buffers.
			Count,
			Unknown = 0xffff
		};


		// ------
		// E_ImageType
		// -
		// ------
		enum class E_ImageType : FXD::U16
		{
			One,
			Two,
			Three,
			Cube,
			Count,
			Unknown = 0xffff
		};


		// ------
		// E_PixelFormat
		// -
		// ------
		enum class E_PixelFormat : FXD::U16
		{
			// 8-bit
			R8 = 0,		// 1-channel pixel format, unsigned normalized.
			R8U,			// 1-channel pixel format, unsigned integer.
			R8S,			// 1-channel pixel format, signed normalized.
			R8I,			// 1-channel pixel format, signed integer.

			RG8,			// 2-channel pixel format, unsigned normalized.
			RG8U,			// 2-channel pixel format, unsigned integer.
			RG8S,			// 2-channel pixel format, signed normalized.
			RG8I,			// 2-channel pixel format, signed integer.

			BGR8,			// 3-channel pixel format, unsigned normalized.
			RGB8,			// 3-channel pixel format, unsigned normalized.
			RGB8U,		// 3-channel pixel format, unsigned integer.
			RGB8S,		// 3-channel pixel format, signed normalized.
			RGB8I,		// 3-channel pixel format, signed integer.

			BGRA8,		// 4-channel pixel format, unsigned normalized.
			RGBA8,		// 4-channel pixel format, unsigned normalized.
			RGBA8U,		// 4-channel pixel format, unsigned integer.
			RGBA8S,		// 4-channel pixel format, signed normalized.
			RGBA8I,		// 4-channel pixel format, signed integer.

			// 16-bit
			R16,			// 1-channel pixel format, unsigned normalized.
			R16U,			// 1-channel pixel format, unsigned integer.
			R16S,			// 1-channel pixel format, signed normalized.
			R16I,			// 1-channel pixel format, signed integer.
			R16F,			// 1-channel pixel format, signed float.

			RG16,			// 2-channel pixel format, unsigned normalized.
			RG16U,		// 2-channel pixel format, unsigned integer.
			RG16S,		// 2-channel pixel format, signed normalized.
			RG16I,		// 2-channel pixel format, signed integer.
			RG16F,		// 2-channel pixel format, signed float.

			RGBA16,		// 4-channel pixel format, unsigned normalized.
			RGBA16U,		// 4-channel pixel format, unsigned integer.
			RGBA16S,		// 4-channel pixel format, signed normalized.
			RGBA16I,		// 4-channel pixel format, signed integer.
			RGBA16F,		// 4-channel pixel format, signed float.

			R32U,			// 1-channel pixel format, unsigned integer.
			R32I,			// 1-channel pixel format, signed integer.
			R32F,			// 1-channel pixel format, signed float.

			// 32-bit
			RG32U,		// 2-channel pixel format, unsigned integer.
			RG32I,		// 2-channel pixel format, signed integer.
			RG32F,		// 2-channel pixel format, signed float.

			RGB32U,		// 3-channel pixel format, unsigned integer.
			RGB32I,		// 3-channel pixel format, signed integer.
			RGB32F,		// 3-channel pixel format, signed float.

			RGBA32U,		// 4-channel pixel format, unsigned integer.
			RGBA32I,		// 4-channel pixel format, signed integer.
			RGBA32F,		// 4-channel pixel format, signed float.

			RG11B10F,	// Packed unsigned float format, 11 bits for red, 11 bits for green, 10 bits for blue.
			RGB10A2,		// Packed unsigned normalized format, 10 bits for red, 10 bits for green, 10 bits for blue, and 2 bits for alpha.
			B5G5R5A1,	// Packed unsigned normalized format, 5 bits for blue, 5 bits for green, 5 bits for red, and 1 bit for alpha.

			D16,			// Depth format, 16bits. Unsigned normalized.
			D32,			// Depth format, 32bits. Signed float.
			D24_S8,		// Depth stencil fomrat, 24bit depth + 8bit stencil. Depth stored as unsigned normalized.
			D32_S8X24,	// Depth stencil format, 32bit depth + 8bit stencil + 24 unused. Depth stored as signed float.

			BC1,			// DXT1/BC1 format containing opaque RGB or 1-bit alpha RGB. 4 bits per pixel.
			BC2,			// DXT3/BC2 format containing RGB with explicit alpha. 8 bits per pixel.
			BC3,			// DXT5/BC2 format containing RGB with explicit alpha. 8 bits per pixel. Better alpha gradients than BC2.
			BC4,			// One channel compressed format. 4 bits per pixel.
			BC5,			// Two channel compressed format. 8 bits per pixel.
			BC6H,			// Format storing RGB in half (16-bit) floating point format usable for HDR. 8 bits per pixel.
			BC7,			// Format storing RGB with optional alpha channel. Similar to BC1/BC2/BC3 formats but with higher quality and higher decompress overhead. 8 bits per pixel.
	
			Count,
			Unknown = 0xffff
		};


		// ------
		// E_PixelComponentType
		// -
		// ------
		enum class E_PixelComponentType : FXD::U16
		{
			Byte = 0,				// 8-bit integer per component.
			Short,					// 16-bit integer per component.
			Int,						// 32-bit integer per component.
			Float16,					// 16 bit float per component.
			Float32,					// 32 bit float per component.
			Packed_R11G11B10,		// 11 bits for first two components, 10 for third component.
			Packed_R10G10B10A2,	// 10 bits for first three components, 2 bits for last component.
			Packed_B5G5R5A1,		// 5 bits for first three components, 1 bit for last component.
			Count,
			Unknown = 0xffff
		};


		// ------
		// E_ClearFlag
		// -
		// ------
		enum class E_ClearFlag : FXD::U16
		{
			Colour = 0,
			Depth,
			Stencil
		};
		typedef FXD::Container::EnumFlag< GFX::E_ClearFlag > E_ClearFlags;
		ENUM_CLASS_FLAGS(FXD::GFX::E_ClearFlag);


		// ------
		// E_BufferLockOptions
		// -
		// ------
		enum class E_BufferLockOptions : FXD::U16
		{
			ReadOnly,
			WriteOnly,
			Count,
			Unknown = 0xffff
		};


		// ------
		// E_BufferUsageFlag
		// -
		// ------
		enum E_BufferUsageFlag : FXD::U16
		{
			BIND_VERTEX_BUFFER,		// Bind flag
			BIND_INDEX_BUFFER,		// Bind flag
			BIND_UNIFORM_BUFFER,		// Bind flag

			BIND_DEPTH_STENCIL,		// Bind flag
			BIND_RENDER_TARGET,		// Bind flag
			BIND_SHADER_RESOURCE,	// Bind flag, any shader stage

			// CPU_READ	| CPU_WRITE	-> D3D HEAP
			// no			| no			-> DEFAULT
			// no			| yes			-> DYNAMIC
			// yes		| yes or no	-> STAGING
			USAGE_CPU_READ,
			USAGE_CPU_WRITE
		};
		typedef FXD::Container::EnumFlag< GFX::E_BufferUsageFlag > E_BufferUsageFlags;
		ENUM_CLASS_FLAGS(FXD::GFX::E_BufferUsageFlag);


		// ------
		// E_Primitive
		// -
		// ------
		enum class E_Primitive : FXD::U16
		{
			LineList = 0,
			LineStrip,
			TriStrip,
			PointList,
			TriList,
			TriFan,
			QuadList,
			Count,
			Unknown = 0xffff
		};


		// ------
		// E_IndexType
		// -
		// ------
		enum class E_IndexType : FXD::U16
		{
			Bit_16 = 0,		// 16-bit indices.
			Bit_32,			// 32-bit indices.
			Count,
			Unknown = 0xffff
		};


		// ------
		// E_VertexElementType
		// -
		// ------
		enum class E_VertexElementType : FXD::U16
		{
			Float1 = 0,		// 1D floating point value
			Float2,			// 2D floating point value
			Float3,			// 3D floating point value
			Float4,			// 4D floating point value
			Colour, 			// Colour encoded in 32-bits (8-bits per channel).
			Colour_ARGB,	// Colour encoded in 32-bits (8-bits per channel) in ARGB order)
			Colour_ABGR,	// Colour encoded in 32-bits (8-bits per channel) in ABGR order)
			Short1, 			// 1D 16-bit signed integer value
			Short2, 			// 2D 16-bit signed integer value
			Short4, 			// 4D 16-bit signed integer value
			UShort1,			// 1D 16-bit unsigned integer value
			UShort2,			// 2D 16-bit unsigned integer value
			UShort4,			// 4D 16-bit unsigned integer value
			Int1,				// 1D 32-bit signed integer value
			Int2,				// 2D 32-bit signed integer value
			Int3,				// 3D 32-bit signed integer value
			Int4,				// 4D 32-bit signed integer value
			UInt1,			// 1D 32-bit signed integer value
			UInt2,			// 2D 32-bit signed integer value
			UInt3,			// 3D 32-bit signed integer value
			UInt4,			// 4D 32-bit unsigned integer value
			UByte4, 			// 4D 8-bit unsigned integer value
			UByte4_Norm,	// 4D 8-bit unsigned integer interpreted as a normalized value in [0, 1] range.
			Count,
			Unknown = 0xffff
		};


		// ------
		// E_FillMode
		// -
		// ------
		enum class E_FillMode : FXD::U16
		{
			Point = 0,
			WireFrame,
			Solid,
			Count,
			Unknown = 0xffff
		};


		// ------
		// E_CullMode
		// -
		// ------
		enum class E_CullMode : FXD::U16
		{
			None = 0,			// Hardware performs no culling and renders both sides.
			Clockwise,			// Hardware culls faces that have a clockwise vertex ordering.
			CounterClockwise,	// Hardware culls faces that have a counter-clockwise vertex ordering.	
			Count,
			Unknown = 0xffff
		};


		// ------
		// DisplaySettings
		// -
		// ------
		class DisplaySettings
		{
		public:
			DisplaySettings(void)
				: m_bWindowed(true)
				, m_bSyncToRefresh(true)
				, m_bEnableAntialiasing(true)
				//, m_size(1280, 720)
				, m_size(600, 400)
				, m_nRefreshRate(60)
			{
			}
			~DisplaySettings(void)
			{
			}

		public:
			bool m_bWindowed;
			bool m_bSyncToRefresh;
			bool m_bEnableAntialiasing;
			Math::Vector2I m_size;
			FXD::S32 m_nRefreshRate;
		};

		// IGfxApi
		class IGfxApi;
		typedef std::unique_ptr< GFX::IGfxApi > GfxApi;

		// IGfxSystem
		class IGfxSystem;
		typedef std::unique_ptr< GFX::IGfxSystem > GfxSystem;

		// IGfxAdapter
		class IGfxAdapter;
		class IGfxAdapterDesc;
		typedef std::shared_ptr< GFX::IGfxAdapter > GfxAdapter;
		typedef std::shared_ptr< GFX::IGfxAdapterDesc > GfxAdapterDesc;

		// IViewport
		class IViewport;
		typedef std::shared_ptr< GFX::IViewport > Viewport;

		// ISwapChain
		class ISwapChain;
		typedef std::shared_ptr< GFX::ISwapChain > SwapChain;

		// IRenderTarget
		class IRenderTarget;
		typedef std::shared_ptr< GFX::IRenderTarget > RenderTarget;

		// IFence
		class IFence;
		typedef std::shared_ptr< GFX::IFence > Fence;

		class IGfxCommandBuffer;
		class IGfxCommandBufferManager;
		typedef std::shared_ptr< GFX::IGfxCommandBuffer > GfxCommandBuffer;
		typedef std::shared_ptr< GFX::IGfxCommandBufferManager > GfxCommandBufferManager;

		class ITexture;
		class ITextureBmp;
		class ITexturePng;
		class ITextureTga;
		typedef std::shared_ptr< GFX::ITexture > Texture;

		// IImage
		class IImage;
		typedef std::shared_ptr< GFX::IImage > Image;

		// IImageView
		class IImageView;
		typedef std::shared_ptr< GFX::IImageView > ImageView;

		// IVertexDecleration
		class IVertexDecleration;
		typedef std::shared_ptr< GFX::IVertexDecleration > VertexDecleration;

		class IHWBuffer;
		class IIndexBuffer;
		class IUniformBuffer;
		class IVertexBuffer;
		typedef std::shared_ptr< GFX::IHWBuffer > HWBuffer;
		typedef std::shared_ptr< GFX::IIndexBuffer > IndexBuffer;
		typedef std::shared_ptr< GFX::IUniformBuffer > UniformBuffer;
		typedef std::shared_ptr< GFX::IVertexBuffer > VertexBuffer;

		class IWindow;
		class IWindowManager;
		typedef std::shared_ptr< GFX::IWindow > Window;
		typedef std::shared_ptr< GFX::IWindowManager > WindowManager;

		// ------
		// IGfxFrameItem
		// -
		// ------
		class IGfxFrameItem : public Core::NonCopyable
		{
		public:
			IGfxFrameItem(void)
			{}
			virtual ~IGfxFrameItem(void)
			{}

			virtual void update(void) PURE;
			virtual void render(const GFX::IViewport* pDisplay) const PURE;
		};

	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_TYPES_H