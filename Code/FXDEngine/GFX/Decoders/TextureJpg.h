// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DECODERS_TEXTUREJPG_H
#define FXDENGINE_GFX_DECODERS_TEXTUREJPG_H
#pragma once

#include "FXDEngine/GFX/Decoders/Texture.h"

namespace FXD
{
	namespace GFX
	{
		// ------
		// ITextureJpg
		// -
		// ------
		class ITextureJpg FINAL : public GFX::ITexture
		{
		public:
			ITextureJpg(Core::StreamIn& streamIn, const Core::String8& strFileName);
			~ITextureJpg(void);

			void read(FXD::U8* pDst) const FINAL;
			void read_row(FXD::U8* pDst, const FXD::U32 nRow) const FINAL;

			bool is_platform_supported(void) const FINAL;
		};
	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_DECODERS_TEXTUREJPG_H
