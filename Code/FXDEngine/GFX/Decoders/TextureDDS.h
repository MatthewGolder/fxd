// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DECODERS_TEXTUREDDS_H
#define FXDENGINE_GFX_DECODERS_TEXTUREDDS_H
#pragma once

#include "FXDEngine/GFX/Decoders/Texture.h"

namespace FXD
{
	namespace GFX
	{
		// ------
		// ITextureDDS
		// -
		// ------
		class ITextureDDS FINAL : public GFX::ITexture
		{
		public:
			ITextureDDS(Core::StreamIn& streamIn, const Core::String8& strFileName);
			~ITextureDDS(void);

			void read(FXD::U8* pDst) const FINAL;
			void read_row(FXD::U8* pDst, const FXD::U32 nRow) const FINAL;

			bool is_platform_supported(void) const FINAL;
		};
	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_DECODERS_TEXTUREDDS_H
