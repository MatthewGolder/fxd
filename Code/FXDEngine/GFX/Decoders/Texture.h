// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DECODERS_TEXTURE_H
#define FXDENGINE_GFX_DECODERS_TEXTURE_H

#include "FXDEngine/IO/FileCache.h"
#include "FXDEngine/Memory/StreamMemHandle.h"
#include "FXDEngine/GFX/Types.h"

namespace FXD
{
	namespace GFX
	{
		// ------
		// E_TextureAsset
		// -
		// ------
		enum class E_TextureAsset
		{
			Bmp,
			DDS,
			Jpg,
			Png,
			Tga,
			Count,
			Unknown = 0xffff
		};

		// ------
		// ITexture
		// -
		// ------
		class ITexture : public IO::IAsset
		{
		public:
			ITexture(GFX::E_TextureAsset eAsset, Core::StreamIn& streamIn, const Core::String8& strFileName);
			virtual ~ITexture(void);

			virtual void read(FXD::U8* pDst) const PURE;
			virtual void read_row(FXD::U8* pDst, const FXD::U32 nRow) const PURE;

			virtual bool is_platform_supported(void) const PURE;
			const Core::StringView8 get_asset_type(void) const FINAL { return UTF_8("Texture"); }

			bool is_compressed(void) const;
			bool is_depth(void) const;
			bool has_alpha(void) const;

			GET_R(FXD::U32, nWidth, width);
			GET_R(FXD::U32, nHeight, height);
			GET_R(FXD::U32, nNumMips, numMips);
			GET_R(GFX::E_PixelFormat, eFormat, format);
			GET_R(FXD::U32, nRowByteIncrement, rowByteIncrement);
			GET_R(GFX::E_TextureAsset, eAsset, eAsset);

		protected:
			FXD::U32 m_nWidth;
			FXD::U32 m_nHeight;
			FXD::U32 m_nNumMips;
			GFX::E_PixelFormat m_eFormat;
			FXD::U32 m_nRowByteIncrement;
			GFX::E_TextureAsset m_eAsset;
			Core::StreamIn m_streamIn;
		};

		namespace File
		{
			extern const Core::StringView8 ToString(GFX::E_TextureAsset eResource);
			extern GFX::E_TextureAsset ToFileType(const Core::StringView8 strType);
		} //namespace File

	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_DECODERS_TEXTURE_H