// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DECODERS_TEXTUREBMP_H
#define FXDENGINE_GFX_DECODERS_TEXTUREBMP_H
#pragma once

#include "FXDEngine/GFX/Decoders/Texture.h"

namespace FXD
{
	namespace GFX
	{
		// ------
		// ITextureBmp
		// -
		// ------
		class ITextureBmp FINAL : public GFX::ITexture
		{
		public:
			ITextureBmp(Core::StreamIn& streamIn, const Core::String8& strFileName);
			~ITextureBmp(void);

			void read(FXD::U8* pDst) const FINAL;
			void read_row(FXD::U8* pDst, const FXD::U32 nRow) const FINAL;

			bool is_platform_supported(void) const FINAL;

		private:
			const FXD::U8* get_row_ptr(FXD::U32 nRow) const;
		};
	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_DECODERS_TEXTUREBMP_H
