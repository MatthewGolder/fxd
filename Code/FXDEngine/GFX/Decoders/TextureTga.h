// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DECODERS_TEXTURETGA_H
#define FXDENGINE_GFX_DECODERS_TEXTURETGA_H
#pragma once

#include "FXDEngine/GFX/Decoders/Texture.h"

namespace FXD
{
	namespace GFX
	{
		// ------
		// ITextureTga
		// -
		// ------
		class ITextureTga FINAL : public GFX::ITexture
		{
		public:
			ITextureTga(Core::StreamIn& streamIn, const Core::String8& strFileName);
			~ITextureTga(void);

			void read(FXD::U8* pDst) const FINAL;
			void read_row(FXD::U8* pDst, const FXD::U32 nRow) const FINAL;

			bool is_platform_supported(void) const FINAL;

		private:
			void _read_compressed_row(FXD::U8* pDst, const FXD::U32 nRow) const;
			void _read_uncompressed_row(FXD::U8* pDst, const FXD::U32 nRow) const;
			FXD::U32 _row_offset(const FXD::U32 nRow) const;

		private:
			FXD::S32 m_nStartOffset;
			FXD::U32 m_nFlags;
		};
	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_DECODERS_TEXTURETGA_H
