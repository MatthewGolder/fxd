// Creator - MatthewGolder
#include "FXDEngine/GFX/Decoders/Texture.h"
#include "FXDEngine/GFX/Mapping.h"

using namespace FXD;
using namespace GFX;

// ------
// ITexture
// -
// ------
ITexture::ITexture(GFX::E_TextureAsset eAsset, Core::StreamIn& streamIn, const Core::String8& strFileName)
	: IAsset(strFileName)
	, m_eAsset(eAsset)
	, m_streamIn(streamIn)
	, m_nWidth(0)
	, m_nHeight(0)
	, m_nNumMips(0)
	, m_eFormat(GFX::E_PixelFormat::Unknown)
	, m_nRowByteIncrement(0)
{
}

ITexture::~ITexture(void)
{
}

bool ITexture::is_compressed(void) const
{
	return GFX::Pixel::IsCompressed(m_eFormat);
}

bool ITexture::is_depth(void) const
{
	return GFX::Pixel::IsDepth(m_eFormat);
}

bool ITexture::has_alpha(void) const
{
	return GFX::Pixel::HasAlpha(m_eFormat);
}

// ------
// File
// -
// ------
const Core::StringView8 kTextureAssetTypes[] =
{
	"Bmp",	// GFX::E_TextureAsset::Bmp
	"Jpg",	// GFX::E_TextureAsset::Jpg
	"Png",	// GFX::E_TextureAsset::Png
	"Tga",	// GFX::E_TextureAsset::Tga
	"Count"	// GFX::E_TextureAsset::Count
};

const Core::StringView8 FXD::GFX::File::ToString(GFX::E_TextureAsset eAsset)
{
	return kTextureAssetTypes[FXD::STD::to_underlying(eAsset)];
}

GFX::E_TextureAsset FXD::GFX::File::ToFileType(const Core::StringView8 strType)
{
	for (FXD::U16 n1 = 0; n1 < FXD::STD::to_underlying(GFX::E_TextureAsset::Count); ++n1)
	{
		if (strType.ends_with_no_case(kTextureAssetTypes[n1]))
		{
			return (GFX::E_TextureAsset)n1;
		}
	}
	return GFX::E_TextureAsset::Unknown;
}