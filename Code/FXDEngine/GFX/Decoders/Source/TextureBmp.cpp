// Creator - MatthewGolder
#include "FXDEngine/GFX/Decoders/TextureBmp.h"
#include "FXDEngine/GFX/Mapping.h"

// https://github.com/mozilla/newtab-dev/blob/master/image/decoders/nsBMPDecoder.cpp

using namespace FXD;
using namespace GFX;

struct InfoHeaderLength
{
	enum
	{
		WIN_V2 = 12,
		WIN_V3 = 40,
		WIN_V4 = 108,
		WIN_V5 = 124,

		// OS2_V1 is omitted; it's the same as WIN_V2.
		OS2_V2_MIN = 16,	// Minimum allowed value for OS2v2.
		OS2_V2_MAX = 64,	// Maximum allowed value for OS2v2.

		WIN_ICO = WIN_V3
	};
};

struct Compression
{
	enum
	{
		RGB = 0,
		RLE8 = 1,
		RLE4 = 2,
		BITFIELDS = 3
	};
};

struct RLE
{
	enum
	{
		ESCAPE = 0,
		ESCAPE_EOL = 0,
		ESCAPE_EOF = 1,
		ESCAPE_DELTA = 2,
		SEGMENT_LENGTH = 2,
		DELTA_LENGTH = 2
	};
};


class BMPHeader
{
public:
	FXD::U16 bfType;
	FXD::U32 bfSize;
	FXD::U16 bfReserved1;
	FXD::U16 bfReserved2;
	FXD::U32 bfOffBits;
};

struct BMPWinHeader
{
	FXD::U32 biWidth;
	FXD::U32 biHeight;
	FXD::U16 biPlanes;
	FXD::U16 biBitCount;
	FXD::U32 biCompression;
	FXD::U32 biSizeImage;
	FXD::U32 biXPelsPerMeter;
	FXD::U32 biYPelsPerMeter;
	FXD::U32 biClrUsed;
	FXD::U32 biClrImportant;
};


// ------
// ITextureBmp
// -
// ------
ITextureBmp::ITextureBmp(Core::StreamIn& streamIn, const Core::String8& strFileName)
	: ITexture(GFX::E_TextureAsset::Bmp, streamIn, strFileName)
{
	BMPHeader header = {};
	Memory::MemZero_T(header);

	auto BMPHeaderChunk = [&]()
	{
		(*m_streamIn) >> header.bfType;
		(*m_streamIn) >> header.bfSize;
		(*m_streamIn) >> header.bfReserved1;
		(*m_streamIn) >> header.bfReserved2;
		(*m_streamIn) >> header.bfOffBits;
	};
	BMPHeaderChunk();
	if (header.bfType != 0x4D42)
	{
		PRINT_ASSERT << "bitmap_image::load_bitmap() ERROR: bitmap_image - Invalid type value " << header.bfType << " expected 19778";
	}

	FXD::U32 dibSize = 0;
	(*m_streamIn) >> dibSize; 
	bool bihSizeOk = (dibSize == InfoHeaderLength::WIN_V2) || (dibSize == InfoHeaderLength::WIN_V3) || (dibSize == InfoHeaderLength::WIN_V4) || (dibSize == InfoHeaderLength::WIN_V5) || ((dibSize >= InfoHeaderLength::OS2_V2_MIN) && (dibSize <= InfoHeaderLength::OS2_V2_MAX));
	PRINT_COND_ASSERT(bihSizeOk, "");


	BMPWinHeader info = {};
	Memory::MemZero_T(info);
	auto DIBHeaderChunk = [&]()
	{
		if (dibSize == InfoHeaderLength::WIN_V2)
		{
			(*m_streamIn) >> info.biWidth;
			(*m_streamIn) >> info.biHeight;
			(*m_streamIn) >> info.biPlanes;
			(*m_streamIn) >> info.biBitCount;
		}
		else
		{
			(*m_streamIn) >> info.biWidth;
			(*m_streamIn) >> info.biHeight;
			(*m_streamIn) >> info.biPlanes;
			(*m_streamIn) >> info.biBitCount;
		}
		(*m_streamIn) >> info.biCompression;
		(*m_streamIn) >> info.biSizeImage; 
		(*m_streamIn) >> info.biXPelsPerMeter;
		(*m_streamIn) >> info.biYPelsPerMeter;
		(*m_streamIn) >> info.biClrUsed;
		(*m_streamIn) >> info.biClrImportant;
	};
	DIBHeaderChunk();

	bool bCompressionOk =
		((info.biCompression == Compression::RGB) && (info.biBitCount == 8 || info.biBitCount == 16 || info.biBitCount == 24 || info.biBitCount == 32)) ||
		((info.biCompression == Compression::RLE8) && (info.biBitCount == 8)) ||
		((info.biCompression == Compression::RLE4) && (info.biBitCount == 4)) ||
		((info.biCompression == Compression::BITFIELDS) &&
		// For BITFIELDS compression we require an exact match for one of the WinBMP BIH sizes; this clearly isn't an OS2 BMP.
		(dibSize == InfoHeaderLength::WIN_V3 || dibSize == InfoHeaderLength::WIN_V4 || dibSize == InfoHeaderLength::WIN_V5) && (info.biBitCount == 16 || info.biBitCount == 32));
	PRINT_COND_ASSERT(bCompressionOk, "");


	FXD::U32 nBitFieldsLengthStillToRead = 0;
	/*
	if (info.biCompression == Compression::BITFIELDS)
	{
		if (dibSize >= InfoHeaderLength::WIN_V4)
		{
			//mBitFields.ReadFromHeader(aData + 36, true);
			float f = 0;
		}
		else
		{
			//nBitFieldsLengthStillToRead = BitFields::LENGTH;
			float f = 0;
		}
	}
	else if (info.biBitCount == 8)
	{
		m_eFormat = GFX::E_PixelFormat::R8;
	}
	else if (info.biBitCount == 16)
	{
		m_eFormat = GFX::E_PixelFormat::RG8;
	}
	else if (info.biBitCount == 24)
	{
		m_eFormat = GFX::E_PixelFormat::RGB8;
	}
	else if (info.biBitCount == 32)
	{
		m_eFormat = GFX::E_PixelFormat::RGBA8;
	}
	*/
	float f = 0;

}

ITextureBmp::~ITextureBmp(void)
{
}

void ITextureBmp::read(FXD::U8* /*pDst*/) const
{
}

void ITextureBmp::read_row(FXD::U8* /*pDst*/, const FXD::U32 /*nRow*/) const
{
}

bool ITextureBmp::is_platform_supported(void) const
{
	return true;
}

const FXD::U8* ITextureBmp::get_row_ptr(FXD::U32 /*nRow*/) const
{
	//Memory::MemHandle::LockGuard sl(m_memory);
	//return (&((FXD::U8*)sl.get_mem())[(nRow * m_nRowByteIncrement)]);
	return 0;
}