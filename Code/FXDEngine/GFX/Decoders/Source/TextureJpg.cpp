// Creator - MatthewGolder
#include "FXDEngine/GFX/Decoders/TextureJpg.h"

using namespace FXD;
using namespace GFX;

// ------
// ITextureJpg
// -
// ------
ITextureJpg::ITextureJpg(Core::StreamIn& streamIn, const Core::String8& strFileName)
	: ITexture(GFX::E_TextureAsset::Jpg, streamIn, strFileName)
{
	FXD_ERROR("ITextureJpg");
}

ITextureJpg::~ITextureJpg(void)
{
}

void ITextureJpg::read(FXD::U8* /*pDst*/) const
{
}

void ITextureJpg::read_row(FXD::U8* /*pDst*/, const FXD::U32 /*nRow*/) const
{
}

bool ITextureJpg::is_platform_supported(void) const
{
	return true;
}