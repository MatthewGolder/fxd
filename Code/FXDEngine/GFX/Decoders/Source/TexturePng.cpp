// Creator - MatthewGolder
#include "FXDEngine/GFX/Decoders/TexturePng.h"
#include "FXDEngine/GFX/Mapping.h"
#include "3rdParty/libpng/pngstruct.h"
#include "3rdParty/libpng/pnginfo.h"

using namespace FXD;
using namespace GFX;

// ------
// GFXPng
// -
// ------
namespace GFXPng
{
	void read_func(png_structp pDatasource, png_bytep pPtr, png_size_t nSize)
	{
		png_voidp pPNGDatasource = png_get_io_ptr(pDatasource);
		if (pPNGDatasource != nullptr)
		{
			Core::IStreamIn* pStreamIn = (Core::IStreamIn*)pPNGDatasource;
			size_t nBytesRead = 0;
			if (pStreamIn != nullptr)
			{
				nBytesRead = pStreamIn->read_size(pPtr, nSize);
			}
			PRINT_COND_ASSERT((nBytesRead == nSize), "");
		}
	}

	png_voidp malloc_func(png_structp /*pPng*/, png_size_t nSize)
	{
		png_voidp pData = (png_voidp)Memory::Alloc::Default.allocate(nSize);
		return pData;
	}

	void free_func(png_structp /*pPng*/, png_voidp pPtr)
	{
		Memory::Alloc::Default.deallocate(pPtr/*, "deallocate"*/);
	}

	void warn_func(png_structp /*pPng*/, png_const_charp pMsg)
	{
		PRINT_WARN << "GFX: " << pMsg;
	}

	void error_func(png_structp /*pPng*/, png_const_charp pMsg)
	{
		PRINT_ASSERT << "GFX: " << pMsg;
	}
} //namespace GFXPng

// ------
// ITexturePng
// -
// ------
ITexturePng::ITexturePng(Core::StreamIn& streamIn, const Core::String8& strFileName)
	: ITexture(GFX::E_TextureAsset::Png, streamIn, strFileName)
	, m_pPng(nullptr)
	, m_pInfo(nullptr)
{
	m_pPng = png_create_read_struct_2(PNG_LIBPNG_VER_STRING, nullptr, GFXPng::error_func, GFXPng::warn_func, nullptr, GFXPng::malloc_func, GFXPng::free_func);
	PRINT_COND_ASSERT((m_pPng != nullptr), "GFX: png_create_read_struct_2() failed");

	m_pInfo = png_create_info_struct(m_pPng);
	PRINT_COND_ASSERT((m_pInfo != nullptr), "GFX: png_create_info_struct() failed");

	png_set_read_fn(m_pPng, (png_voidp)streamIn.get(), GFXPng::read_func);
	png_set_sig_bytes(m_pPng, 0);
	png_read_info(m_pPng, m_pInfo);

	m_nWidth = m_pInfo->width;
	m_nHeight = m_pInfo->height;
	m_nRowByteIncrement = m_pInfo->rowbytes;

	switch (m_pInfo->channels)
	{
		case 1:
		{
			m_eFormat = (m_pInfo->bit_depth == 8) ? GFX::E_PixelFormat::R8 : (m_pInfo->bit_depth == 16) ? GFX::E_PixelFormat::R16 : GFX::E_PixelFormat::Unknown;
			break;
		}
		case 2:
		{
			m_eFormat = (m_pInfo->bit_depth == 8) ? GFX::E_PixelFormat::RG8 : (m_pInfo->bit_depth == 16) ? GFX::E_PixelFormat::RG16 : GFX::E_PixelFormat::Unknown;
			break;
		}
		case 3:
		{
			m_eFormat = (m_pInfo->bit_depth == 8) ? GFX::E_PixelFormat::RGB8 : GFX::E_PixelFormat::Unknown;
			break;
		}
		case 4:
		{
			m_eFormat = (m_pInfo->bit_depth == 8) ? GFX::E_PixelFormat::RGBA8 : (m_pInfo->bit_depth == 16) ? GFX::E_PixelFormat::RGBA16 : GFX::E_PixelFormat::Unknown;
			break;
		}
		default:
		{
			PRINT_ASSERT << "GFX: Unsupported png format";
			break;
		}
	}
	PRINT_COND_ASSERT((m_eFormat != GFX::E_PixelFormat::Unknown), "GFX: png_create_read_struct_2() failed");
}

ITexturePng::~ITexturePng(void)
{
	if (m_pPng != nullptr)
	{
		png_read_end(m_pPng, nullptr);
		png_destroy_read_struct(&m_pPng, &m_pInfo, nullptr);
	}

	m_pPng = nullptr;
	m_pInfo = nullptr;
}

void ITexturePng::read(FXD::U8* pDst) const
{
	for (FXD::U32 nRow = 0; nRow < m_nHeight; nRow++)
	{
		const FXD::U32 nRowOffset = (m_nRowByteIncrement * ((false) ? (m_nHeight - nRow - 1) : nRow));
		FXD::U8* pRow = &pDst[nRowOffset];
		png_read_row(m_pPng, pRow, nullptr);
	}
}

void ITexturePng::read_row(FXD::U8* /*pDst*/, const FXD::U32 /*nRow*/) const
{
	PRINT_ASSERT << "GFX: ITexturePng::read_row() is unsupported";
}

bool ITexturePng::is_platform_supported(void) const
{
	return true;
}