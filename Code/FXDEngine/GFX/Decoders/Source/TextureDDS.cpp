// Creator - MatthewGolder
#include "FXDEngine/GFX/Decoders/TextureDDS.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/GfxSystem.h"
#include "FXDEngine/GFX/Mapping.h"
#include "FXDEngine/App/FXDApi.h"

using namespace FXD;
using namespace GFX;

// ------
// ITextureDDS
// -
// ------
ITextureDDS::ITextureDDS(Core::StreamIn& streamIn, const Core::String8& strFileName)
	: ITexture(GFX::E_TextureAsset::DDS, streamIn, strFileName)
{
}

ITextureDDS::~ITextureDDS(void)
{
}

void ITextureDDS::read(FXD::U8* /*pDst*/) const
{
}

void ITextureDDS::read_row(FXD::U8* /*pDst*/, const FXD::U32 /*nRow*/) const
{
}

bool ITextureDDS::is_platform_supported(void) const
{
	const GFX::E_GfxApi eCurrApi = FXDGFX()->gfx_system()->gfx_api()->gfxApiType();
	switch (eCurrApi)
	{
		case GFX::E_GfxApi::DX11:
		{
			return true;
		}
		default:
		{
			return false;
		}
	}
}