// Creator - MatthewGolder
#include "FXDEngine/GFX/Decoders/TextureTga.h"
#include "FXDEngine/GFX/Mapping.h"

using namespace FXD;
using namespace GFX;

enum E_TGAImageType
{
	TGA_NO_IMAGE = 0,
	TGA_COLOR_MAPPED = 1,
	TGA_TRUECOLOR = 2,
	TGA_BLACK_AND_WHITE = 3,
	TGA_COLOR_MAPPED_RLE = 9,
	TGA_TRUECOLOR_RLE = 10,
	TGA_BLACK_AND_WHITE_RLE = 11,
};

enum E_TGADescriptorFlags
{
	TGA_FLAGS_INVERTX = 0x10,
	TGA_FLAGS_INVERTY = 0x20,
	TGA_FLAGS_INTERLEAVED_2WAY = 0x40, // Deprecated
	TGA_FLAGS_INTERLEAVED_4WAY = 0x80, // Deprecated
};

enum E_TGAFlags
{
	CONV_FLAGS_NONE = 0x0,
	CONV_FLAGS_INVERTX = 0x1,			// If set, scanlines are right-to-left
	CONV_FLAGS_INVERTY = 0x2,			// If set, scanlines are top-to-bottom
	CONV_FLAGS_COMPRESSED = 0x4,		// Source data is RLE compressed

	CONV_FLAGS_SWIZZLE = 0x10000,		// Swizzle BGR<->RGB data
};

struct TGA_HEADER
{
	FXD::U8 idLength;
	FXD::U8 colorMapType;

	// The image type.
	FXD::U8 imageType;

	// Color Map Specification.
	// We need to separately specify high and low bytes to avoid endianness and alignment problems.
	FXD::U8 colorMapIndexLo, colorMapIndexHi;
	FXD::U8 colorMapLengthLo, colorMapLengthHi;
	FXD::U8 colorMapSize;

	// Image Specification.
	FXD::U8 xOriginLo, xOriginHi;
	FXD::U8 yOriginLo, yOriginHi;

	FXD::U8 widthLo, widthHi;
	FXD::U8 heightLo, heightHi;

	FXD::U8 bpp;

	// Image descriptor.
	// 3-0: attribute bpp
	// 4:		left-to-right ordering
	// 5:		top-to-bottom ordering
	// 7-6: zero
	FXD::U8 descriptor;
};

struct TGA_FOOTER
{
	FXD::U16 dwExtensionOffset;
	FXD::U16 dwDeveloperOffset;
	FXD::U8 Signature[18];
};

// ------
// ITextureTga
// -
// ------
ITextureTga::ITextureTga(Core::StreamIn& streamIn, const Core::String8& strFileName)
	: ITexture(GFX::E_TextureAsset::Tga, streamIn, strFileName)
	, m_nStartOffset(0)
	, m_nFlags(0)
{
	TGA_HEADER tgaHeader = {};
	Memory::MemZero_T(tgaHeader);
	auto TGAHeaderChunk = [&]()
	{
		m_streamIn->read_size(&tgaHeader, sizeof(TGA_HEADER));
	};
	TGAHeaderChunk();

	m_nWidth = (tgaHeader.widthHi << 8) | tgaHeader.widthLo;
	m_nHeight = (tgaHeader.heightHi << 8) | tgaHeader.heightLo;
	switch (tgaHeader.imageType)
	{
		case TGA_TRUECOLOR:
		case TGA_TRUECOLOR_RLE:
		{
			switch (tgaHeader.bpp)
			{
				case 16:
				{
					m_eFormat = GFX::E_PixelFormat::B5G5R5A1;
					break;
				}
				case 24:
				{
					m_eFormat = GFX::E_PixelFormat::BGR8;
					break;
				}
				case 32:
				{
					m_eFormat = GFX::E_PixelFormat::BGRA8;
					break;
				}
				default:
				{
					PRINT_ASSERT << "GFX: Unsupported tga format";
					break;
				}
			}
			if (tgaHeader.imageType == TGA_TRUECOLOR_RLE)
			{
				m_nFlags |= CONV_FLAGS_COMPRESSED;
			}
			break;
		}
		case TGA_BLACK_AND_WHITE:
		case TGA_BLACK_AND_WHITE_RLE:
		{
			switch (tgaHeader.bpp)
			{
				case 8:
				{
					m_eFormat = GFX::E_PixelFormat::R8;
					break;
				}
				default:
				{
					PRINT_ASSERT << "GFX: Unsupported tga format";
					break;
				}
			}
			if (tgaHeader.imageType == TGA_BLACK_AND_WHITE_RLE)
			{
				m_nFlags |= CONV_FLAGS_COMPRESSED;
			}
			break;
		}
		case TGA_NO_IMAGE:
		case TGA_COLOR_MAPPED:
		case TGA_COLOR_MAPPED_RLE:
		default:
		{
			PRINT_ASSERT << "GFX: Unsupported tga format";
			break;
		}
	}
	m_nRowByteIncrement = (m_nHeight * Pixel::GetNumElemBytes(m_eFormat));
	m_nStartOffset = (*m_streamIn).tell();


	if (tgaHeader.descriptor & TGA_FLAGS_INVERTX)
	{
		m_nFlags |= CONV_FLAGS_INVERTX;
	}
	if (tgaHeader.descriptor & TGA_FLAGS_INVERTY)
	{
		m_nFlags |= CONV_FLAGS_INVERTY;
	}

	if (tgaHeader.descriptor & (TGA_FLAGS_INTERLEAVED_2WAY | TGA_FLAGS_INTERLEAVED_4WAY))
	{
		PRINT_ASSERT << "GFX: Unsupported tga format";
	}
	
	PRINT_COND_ASSERT((m_nWidth > 0) && (m_nHeight > 0), "GFX: Unsupported tga image dimensions");
}

ITextureTga::~ITextureTga(void)
{
}

void ITextureTga::read(FXD::U8* pDst) const
{
	for (FXD::U32 nRow = 0; nRow < m_nHeight; nRow++)
	{
		FXD::U8* pRow = &pDst[nRow * rowByteIncrement()];
		read_row(pRow, nRow);
	}
}

void ITextureTga::read_row(FXD::U8* pDst, const FXD::U32 nRow) const
{
	if (m_nFlags & CONV_FLAGS_COMPRESSED)
	{
		_read_compressed_row(pDst, nRow);
	}
	else
	{
		_read_uncompressed_row(pDst, nRow);
	}
}

bool ITextureTga::is_platform_supported(void) const
{
	return true;
}

void ITextureTga::_read_compressed_row(FXD::U8* pDst, const FXD::U32 nRow) const
{
	const FXD::U32 nRowOffset = ((m_nFlags & CONV_FLAGS_INVERTX) ? (m_nWidth - 1) : 0);
	PRINT_COND_ASSERT(((nRowOffset * Pixel::GetNumElemBytes(m_eFormat)) < m_nRowByteIncrement), "GFX: Error reading tga file");

	m_streamIn->seek_to(_row_offset(nRow), Core::E_SeekOffset::Begin);

	FXD::U8* pPtr = (pDst + nRowOffset);
	const FXD::U8* pEnd = (pDst + m_nRowByteIncrement);

	switch (m_eFormat)
	{
		case GFX::E_PixelFormat::R8:
		{
			for (FXD::U32 nCol = 0; nCol < m_nWidth; )
			{
				PRINT_COND_ASSERT((pPtr < pEnd), "GFX: Error reading tga file");

				FXD::U8 read = 0;
				m_streamIn->read_size(&read, 1);

				if (read & 0x80)
				{
					FXD::U32 nRepeat = FXD::U32(read & 0x7F) + 1;
					m_streamIn->read_size(&read, 1);

					FXD::U8 temp = 0;
					temp = read;

					for (; nRepeat > 0; --nRepeat, ++nCol)
					{
						PRINT_COND_ASSERT((nCol < m_nWidth), "GFX: Error reading tga file");
						pPtr[0] = temp;
						(m_nFlags & CONV_FLAGS_INVERTX) ? (pPtr--) : (pPtr++);
					}
				}
				else
				{
					FXD::U32 nRepeat = FXD::U32(read & 0x7F) + 1;
					for (; nRepeat > 0; --nRepeat, ++nCol)
					{
						PRINT_COND_ASSERT((pPtr < pEnd), "GFX: Error reading tga file");
						PRINT_COND_ASSERT((nCol < m_nWidth), "GFX: Error reading tga file");

						m_streamIn->read_size(&read, 1);
						pPtr[0] = read;
						(m_nFlags & CONV_FLAGS_INVERTX) ? (pPtr--) : (pPtr++);
					}
				}
			}
			break;
		}
		case GFX::E_PixelFormat::B5G5R5A1:
		{
			for (FXD::U32 nCol = 0; nCol < m_nWidth; )
			{
				PRINT_COND_ASSERT((pPtr < pEnd), "GFX: Error reading tga file");

				FXD::U8 read[2] = {};
				m_streamIn->read_size(&read, 1);

				if (*read & 0x80)
				{
					PRINT_COND_ASSERT((pPtr + 1 < pEnd), "GFX: Error reading tga file");

					FXD::U32 nRepeat = FXD::U32(*read & 0x7F) + 1;
					m_streamIn->read_size(read, 2);

					FXD::U8 temp[2] = {};
					temp[0] = read[0];
					temp[1] = read[1];

					for (; nRepeat > 0; --nRepeat, ++nCol)
					{
						PRINT_COND_ASSERT((nCol < m_nWidth), "GFX: Error reading tga file");
						pPtr[0] = temp[0];
						pPtr[1] = temp[1];
						(m_nFlags & CONV_FLAGS_INVERTX) ? (pPtr -= 2) : (pPtr += 2);
					}
				}
				else
				{
					FXD::U32 nRepeat = FXD::U32(*read & 0x7F) + 1;
					for (; nRepeat > 0; --nRepeat, ++nCol)
					{
						PRINT_COND_ASSERT((pPtr + 1 < pEnd), "GFX: Error reading tga file");
						PRINT_COND_ASSERT((nCol < m_nWidth), "GFX: Error reading tga file");
						m_streamIn->read_size(read, 2);
						pPtr[0] = read[0];
						pPtr[1] = read[1];
						(m_nFlags & CONV_FLAGS_INVERTX) ? (pPtr -= 2) : (pPtr += 2);
					}
				}
			}
			break;
		}
		case GFX::E_PixelFormat::BGR8:
		case GFX::E_PixelFormat::RGB8:
		{
			for (FXD::U32 nCol = 0; nCol < m_nWidth; )
			{
				PRINT_COND_ASSERT((pPtr < pEnd), "GFX: Error reading tga file");

				FXD::U8 read[3] = {};
				m_streamIn->read_size(&read, 1);

				if (*read & 0x80)
				{
					PRINT_COND_ASSERT((pPtr + 2 < pEnd), "GFX: Error reading tga file");

					FXD::U32 nRepeat = FXD::U32(*read & 0x7F) + 1;
					m_streamIn->read_size(read, 3);

					// BGR -> RGB
					FXD::U8 temp[3] = {};
					if (m_nFlags & CONV_FLAGS_SWIZZLE)
					{
						temp[0] = read[2];
						temp[1] = read[1];
						temp[2] = read[0];
					}
					else
					{
						temp[0] = read[0];
						temp[1] = read[1];
						temp[2] = read[2];
					}

					for (; nRepeat > 0; --nRepeat, ++nCol)
					{
						PRINT_COND_ASSERT((nCol < m_nWidth), "GFX: Error reading tga file");
						pPtr[0] = temp[0];
						pPtr[1] = temp[1];
						pPtr[2] = temp[2];
						(m_nFlags & CONV_FLAGS_INVERTX) ? (pPtr -= 3) : (pPtr += 3);
					}
				}
				else
				{
					FXD::U32 nRepeat = FXD::U32(*read & 0x7F) + 1;
					for (; nRepeat > 0; --nRepeat, ++nCol)
					{
						PRINT_COND_ASSERT((pPtr + 2 < pEnd), "GFX: Error reading tga file");
						PRINT_COND_ASSERT((nCol < m_nWidth), "GFX: Error reading tga file");
						m_streamIn->read_size(read, 3);

						// BGR -> RGB
						if (m_nFlags & CONV_FLAGS_SWIZZLE)
						{
							pPtr[0] = read[2];
							pPtr[1] = read[1];
							pPtr[2] = read[0];
						}
						else
						{
							pPtr[0] = read[0];
							pPtr[1] = read[1];
							pPtr[2] = read[2];
						}
						(m_nFlags & CONV_FLAGS_INVERTX) ? (pPtr -= 3) : (pPtr += 3);
					}
				}
			}
			break;
		}
		case GFX::E_PixelFormat::BGRA8:
		case GFX::E_PixelFormat::RGBA8:
		{
			FXD::U8 read[4] = {};
			for (FXD::U32 nCol = 0; nCol < m_nWidth; )
			{
				PRINT_COND_ASSERT((pPtr < pEnd), "GFX: Error reading tga file");
				m_streamIn->read_size(&read, 1);

				FXD::U32 nRepeat = FXD::U32(*read & 0x7F) + 1;
				if (*read & 0x80)
				{
					PRINT_COND_ASSERT((pPtr + 3 < pEnd), "GFX: Error reading tga file");
					m_streamIn->read_size(read, 4);

					// BGRA -> RGBA
					FXD::U8 temp[4] = {};
					if (m_nFlags & CONV_FLAGS_SWIZZLE)
					{
						temp[0] = read[2];
						temp[1] = read[1];
						temp[2] = read[0];
						temp[3] = read[3];
					}
					else
					{
						temp[0] = read[0];
						temp[1] = read[1];
						temp[2] = read[2];
						temp[3] = read[3];
					}

					for (; nRepeat > 0; --nRepeat, ++nCol)
					{
						PRINT_COND_ASSERT((nCol < m_nWidth), "GFX: Error reading tga file");
						pPtr[0] = temp[0];
						pPtr[1] = temp[1];
						pPtr[2] = temp[2];
						pPtr[3] = temp [3];
						(m_nFlags & CONV_FLAGS_INVERTX) ? (pPtr -= 4) : (pPtr += 4);
					}
				}
				else
				{
					for (; nRepeat > 0; --nRepeat, ++nCol)
					{
						PRINT_COND_ASSERT((pPtr + 3 < pEnd), "GFX: Error reading tga file");
						PRINT_COND_ASSERT((nCol < m_nWidth), "GFX: Error reading tga file");
						m_streamIn->read_size(read, 4);

						// BGRA -> RGBA
						if (m_nFlags & CONV_FLAGS_SWIZZLE)
						{
							pPtr[0] = read[2];
							pPtr[1] = read[1];
							pPtr[2] = read[0];
							pPtr[3] = read[3];
						}
						else
						{
							pPtr[0] = read[0];
							pPtr[1] = read[1];
							pPtr[2] = read[2];
							pPtr[3] = read[3];
						}
						(m_nFlags & CONV_FLAGS_INVERTX) ? (pPtr -= 4) : (pPtr += 4);
					}
				}
			}
			break;
		}
		default:
		{
			PRINT_ASSERT << "GFX: Unsupported tga format";
			break;
		}
	}
}

void ITextureTga::_read_uncompressed_row(FXD::U8* pDst, const FXD::U32 nRow) const
{
	const FXD::U32 nRowOffset = ((m_nFlags & CONV_FLAGS_INVERTX) ? (m_nWidth - 1) : 0);
	PRINT_COND_ASSERT(((nRowOffset * Pixel::GetNumElemBytes(m_eFormat)) < m_nRowByteIncrement), "GFX: Error reading tga file");

	m_streamIn->seek_to(_row_offset(nRow), Core::E_SeekOffset::Begin);

	FXD::U8* pPtr = (pDst + nRowOffset);
	const FXD::U8* pEnd = (pDst + m_nRowByteIncrement);

	switch (m_eFormat)
	{
		case GFX::E_PixelFormat::R8:
		{
			for (FXD::U32 nCol = 0; nCol < m_nWidth; nCol++)
			{
				FXD::U8 read = 0;
				m_streamIn->read_size(&read, 1);

				pPtr[0] = read;
				(m_nFlags & CONV_FLAGS_INVERTX) ? (pPtr--) : (pPtr++);
			}
			break;
		}
		case GFX::E_PixelFormat::B5G5R5A1:
		{
			for (FXD::U32 nCol = 0; nCol < m_nWidth; nCol++)
			{
				FXD::U8 read[2] = {};
				m_streamIn->read_size(&read, 2);

				pPtr[0] = read[0];
				pPtr[1] = read[1];
				(m_nFlags & CONV_FLAGS_INVERTX) ? (pPtr -= 2) : (pPtr += 2);
			}
			break;
		}
		case GFX::E_PixelFormat::BGR8:
		case GFX::E_PixelFormat::RGB8:
		{
			for (FXD::U32 nCol = 0; nCol < m_nWidth; nCol++)
			{
				FXD::U8 read[3] = {};
				m_streamIn->read_size(read, 3);

				// BGRA -> RGBA
				if (m_nFlags & CONV_FLAGS_SWIZZLE)
				{
					pPtr[0] = read[2];
					pPtr[1] = read[1];
					pPtr[2] = read[0];
				}
				else
				{
					pPtr[0] = read[0];
					pPtr[1] = read[1];
					pPtr[2] = read[2];
				}
				(m_nFlags & CONV_FLAGS_INVERTX) ? (pPtr -= 3) : (pPtr += 3);
			}
			break;
		}
		case GFX::E_PixelFormat::BGRA8:
		case GFX::E_PixelFormat::RGBA8:
		{
			for (FXD::U32 nCol = 0; nCol < m_nWidth; nCol++)
			{
				FXD::U8 read[4] = {};
				m_streamIn->read_size(read, 4);

				// BGRA -> RGBA
				if (m_nFlags & CONV_FLAGS_SWIZZLE)
				{
					pPtr[0] = read[2];
					pPtr[1] = read[1];
					pPtr[2] = read[0];
					pPtr[3] = read[3];
				}
				else
				{
					pPtr[0] = read[0];
					pPtr[1] = read[1];
					pPtr[2] = read[2];
					pPtr[3] = read[3];
				}
				(m_nFlags & CONV_FLAGS_INVERTX) ? (pPtr -= 4) : (pPtr += 4);
			}
			break;
		}
		default:
		{
			PRINT_ASSERT << "GFX: Unsupported tga format";
			break;
		}
	}
}

FXD::U32 ITextureTga::_row_offset(const FXD::U32 nTargetRow) const
{
	FXD::U32 nRetVal = 0;
	if (m_nFlags & CONV_FLAGS_COMPRESSED)
	{
		const FXD::U32 nActualRow = (m_nFlags & CONV_FLAGS_INVERTY) ? (m_nHeight - nTargetRow) : nTargetRow;
		m_streamIn->seek_to(m_nStartOffset, Core::E_SeekOffset::Begin);

		for (FXD::U32 nRow = 0; nRow < nActualRow; nRow++)
		{
			switch (m_eFormat)
			{
				case GFX::E_PixelFormat::R8:
				{
					FXD::U8 read = 0;
					for (FXD::U32 nCol = 0; nCol < m_nWidth; )
					{
						m_streamIn->read_size(&read, 1);
						nRetVal += 1;

						if (read & 0x80)
						{
							FXD::U32 nRepeat = FXD::U32(read & 0x7F) + 1;
							m_streamIn->read_size(&read, 1);
							nRetVal += 1;
							for (; nRepeat > 0; --nRepeat, ++nCol)
							{
							}
						}
						else
						{
							FXD::U32 nRepeat = FXD::U32(read & 0x7F) + 1;
							for (; nRepeat > 0; --nRepeat, ++nCol)
							{
								PRINT_COND_ASSERT((nCol < m_nWidth), "GFX: Error reading tga file");
								m_streamIn->read_size(&read, 1);
								nRetVal += 1;
							}
						}
					}
					break;
				}
				case GFX::E_PixelFormat::B5G5R5A1:
				{
					FXD::U8 read[2] = {};
					for (FXD::U32 nCol = 0; nCol < m_nWidth; )
					{
						m_streamIn->read_size(&read, 1);
						nRetVal += 1;

						if (*read & 0x80)
						{
							FXD::U32 nRepeat = FXD::U32(*read & 0x7F) + 1;
							m_streamIn->read_size(read, 2);
							nRetVal += 2;
							for (; nRepeat > 0; --nRepeat, ++nCol)
							{
							}
						}
						else
						{
							FXD::U32 nRepeat = FXD::U32(*read & 0x7F) + 1;
							for (; nRepeat > 0; --nRepeat, ++nCol)
							{
								PRINT_COND_ASSERT((nCol < m_nWidth), "GFX: Error reading tga file");
								m_streamIn->read_size(read, 2);
								nRetVal += 2;
							}
						}
					}
					break;
				}
				case GFX::E_PixelFormat::BGR8:
				case GFX::E_PixelFormat::RGB8:
				{
					FXD::U8 read[3] = {};
					for (FXD::U32 nCol = 0; nCol < m_nWidth; )
					{
						m_streamIn->read_size(&read, 1);
						nRetVal += 1;

						if (*read & 0x80)
						{
							FXD::U32 nRepeat = FXD::U32(*read & 0x7F) + 1;
							m_streamIn->read_size(read, 3);
							nRetVal += 3;
							for (; nRepeat > 0; --nRepeat, ++nCol)
							{
							}
						}
						else
						{
							FXD::U32 nRepeat = FXD::U32(*read & 0x7F) + 1;
							for (; nRepeat > 0; --nRepeat, ++nCol)
							{
								PRINT_COND_ASSERT((nCol < m_nWidth), "GFX: Error reading tga file");
								m_streamIn->read_size(read, 3);
								nRetVal += 3;
							}
						}
					}
					break;
				}
				case GFX::E_PixelFormat::BGRA8:
				case GFX::E_PixelFormat::RGBA8:
				{
					FXD::U8 read[4] = {};
					for (FXD::U32 nCol = 0; nCol < m_nWidth; )
					{
						m_streamIn->read_size(&read, 1);
						nRetVal += 1;

						FXD::U32 nRepeat = FXD::U32(*read & 0x7F) + 1;
						if (*read & 0x80)
						{
							m_streamIn->read_size(read, 4);
							nRetVal += 4;
							for (; nRepeat > 0; --nRepeat, ++nCol)
							{
							}
						}
						else
						{
							for (; nRepeat > 0; --nRepeat, ++nCol)
							{
								PRINT_COND_ASSERT((nCol < m_nWidth), "GFX: Error reading tga file");
								m_streamIn->read_size(read, 4);
								nRetVal += 4;
							}
						}
					}
					break;
				}
				default:
				{
					PRINT_ASSERT << "GFX: Unsupported tga format";
					break;
				}
			}
		}
	}
	else
	{
		nRetVal = (m_nRowByteIncrement * ((m_nFlags & CONV_FLAGS_INVERTY) ? (m_nHeight - nTargetRow - 1) : nTargetRow));
	}
	return (nRetVal + m_nStartOffset);
}