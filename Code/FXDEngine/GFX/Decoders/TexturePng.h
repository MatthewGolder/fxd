// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DECODERS_TEXTUREPNG_H
#define FXDENGINE_GFX_DECODERS_TEXTUREPNG_H
#pragma once

#include "FXDEngine/GFX/Decoders/Texture.h"
#include "3rdParty/libpng/png.h"

namespace FXD
{
	namespace GFX
	{
		// ------
		// ITexturePng
		// -
		// ------
		class ITexturePng FINAL : public GFX::ITexture
		{
		public:
			ITexturePng(Core::StreamIn& streamIn, const Core::String8& strFileName);
			~ITexturePng(void);

			void read(FXD::U8* pDst) const FINAL;
			void read_row(FXD::U8* pDst, const FXD::U32 nRow) const FINAL;

			bool is_platform_supported(void) const FINAL;

		private:
			png_struct* m_pPng;
			png_info* m_pInfo;
		};
	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_DECODERS_TEXTUREPNG_H
