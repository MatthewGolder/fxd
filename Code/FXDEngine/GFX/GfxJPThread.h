// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_GFXJPTHREAD_H
#define FXDENGINE_GFX_GFXJPTHREAD_H

#include "FXDEngine/Core/FPS.h"
#include "FXDEngine/Core/Timer.h"
#include "FXDEngine/GFX/Types.h"
#include "FXDEngine/Job/AsyncJob.h"

namespace FXD
{
	namespace GFX
	{
		// ------
		// IGfxJPThread
		// -
		// ------
		class IGfxJPThread FINAL : public Job::IAsyncTaskPoolThread
		{
		public:
			IGfxJPThread(void);
			virtual ~IGfxJPThread(void);

			const Core::StringView8 display_name(void) const FINAL { return UTF_8("GfxThread"); }

			GET_REF_W(GFX::GfxSystem, gfxSystem, gfx_system);
			GET_REF_W(GFX::WindowManager, windowManager, window_manager);

		protected:
			bool init_pool(void) FINAL;
			bool shutdown_pool(void) FINAL;
			void idle_process(void) FINAL;
			bool is_thread_restricted(void) const FINAL;

		private:
			Core::FPS m_fps;
			Core::Timer m_timer;
			GFX::GfxSystem m_gfxSystem;
			GFX::WindowManager m_windowManager;
		};

		namespace Funcs
		{
			extern bool IsGfxThreadRestricted(void);
			extern void RestrictGfxThread(void);
			extern void UnRestrictGfxThread(void);
		} //namespace Funcs

	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_GFXJPTHREAD_H