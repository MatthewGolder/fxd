// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_VULKAN_COMMANDPOOLVK_H
#define FXDENGINE_GFX_VULKAN_COMMANDPOOLVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/Vulkan/Vulkan.h"

namespace FXD
{
	namespace GFX
	{
		class VKCommandPool
		{
		public:
			VKCommandPool(GFX::IGfxAdapter* pAdapter);
			~VKCommandPool(void);

			bool create(FXD::U32 nQueueFamily);
			bool release(void);

			GET_R(VkCommandPool, vkCmdPool, vkCmdPool);

		public:
			GFX::IGfxAdapter* m_pAdapter;
			VkCommandPool m_vkCmdPool;
		};

	} //namespace GFX
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_VULKAN_COMMANDPOOLVK_H