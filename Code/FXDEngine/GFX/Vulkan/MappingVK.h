// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_VULKAN_MAPPINGVK_H
#define FXDENGINE_GFX_VULKAN_MAPPINGVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/Vulkan/Vulkan.h"

namespace FXD
{
	namespace GFX
	{
		// ------
		// Vulkan
		// -
		// ------
		namespace Vulkan
		{
			extern bool CheckError(void);
			extern bool CheckExtension(const GFX::IGfxAdapter* pAdapter, const FXD::UTF8* pExtension);
			extern void PrintExtension(const GFX::IGfxAdapter* pAdapter);

			extern bool SupportsBCTextureFormats(void);
			extern bool SupportsASTCTextureFormats(void);

			extern VkPrimitiveTopology GetPrimitiveType(GFX::E_Primitive eType);
			extern VkPolygonMode GetFillMode(GFX::E_FillMode eMode);
			extern VkCullModeFlagBits GetCullMode(GFX::E_CullMode eMode);
			extern VkSampleCountFlagBits GetSampleFlags(FXD::U16 nNumSamples);
		}

		namespace Buffer
		{
			extern const VkImageUsageFlagBits ConvertToVKImageUsageBits(GFX::E_BufferUsageFlags eFlags);
			extern const VkBufferUsageFlagBits ConvertToVKBufferUsageBits(GFX::E_BufferUsageFlags eFlags);
		}
		namespace Pixel
		{
			extern const VkFormat GetFormat(GFX::E_PixelFormat eFormat);
			extern const VkComponentMapping GetMapping(GFX::E_PixelFormat eFormat);
			extern const VkImageAspectFlagBits GetAspectMask(GFX::E_BufferUsageFlags eFlags);
		}
		namespace Index
		{
			extern const VkIndexType GetFormat(GFX::E_IndexType eType);
		}
		namespace Vertex
		{
			extern const VkFormat GetFormat(GFX::E_VertexElementType eType);
		}
	} //namespace GFX
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_VULKAN_MAPPINGVK_H