// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_VULKAN_VULKAN_H
#define FXDENGINE_GFX_VULKAN_VULKAN_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#if IsOSPC()
	#define VK_USE_PLATFORM_WIN32_KHR
	#define VK_NO_PROTOTYPES
	#include <vulkan/vulkan.h>
#endif //IsOSPC()
#if IsOSAndroid()
	#define VK_USE_PLATFORM_ANDROID_KHR
	#define VK_NO_PROTOTYPES
	#include <vulkan/vulkan.h>
#endif //IsOSAndroid()

namespace FXD
{
	namespace GFX
	{
		template < class T >
		static inline void MemZeroVulkanStruct(T& vkStruct, const VkStructureType vkType)
		{
			CTC_IS_TYPE_POINTER(T);
			CTC_IS_NOT_EQUAL(STRUCT_OFFSET(T, sType), 0);
			vkStruct.sType = vkType;
			FXD::Memory::MemZero(((FXD::U8*)&vkStruct) + sizeof(VkStructureType), sizeof(T) - sizeof(VkStructureType));
		}

		// ------
		// CheckVKResult
		// -
		// ------
		class CheckVKResult
		{
		public:
			CheckVKResult(VkResult vkResult, const FXD::UTF8* pText, const FXD::UTF8* pFile, FXD::U32 nLine);

			bool operator ()();

			VkResult m_vkResult;
			FXD::U32 m_nLine;
			const FXD::UTF8* m_pText;
			const FXD::UTF8* m_pFile;
		};
		#define VKResultCheckFail(c) FXD::GFX::CheckVKResult(c, #c, __FILE__, __LINE__)()
		#define VKResultCheckSuccess(c) !VKResultCheckFail(c)

	} //namespace GFX
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_VULKAN_VULKAN_H