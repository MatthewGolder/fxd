// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_VULKAN_GPUCOMMANDBUFFERVK_H
#define FXDENGINE_GFX_VULKAN_GPUCOMMANDBUFFERVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/GfxCommandBuffer.h"
#include "FXDEngine/GFX/Vulkan/Vulkan.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IGfxCommandBuffer >
			// -
			// ------
			template <>
			class Impl< GFX::IGfxCommandBuffer >
			{
			public:
				friend class GFX::IGfxCommandBuffer;

			public:
				Impl(void);
				~Impl(void);

			private:
				VkCommandBuffer m_vkCmdBuffer;
			};

			// ------
			// Impl< GFX::IGfxCommandBufferManager >
			// -
			// ------
			template <>
			class Impl< GFX::IGfxCommandBufferManager >
			{
			public:
				friend class GFX::IGfxCommandBufferManager;

			public:
				Impl(void);
				~Impl(void);
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_VULKAN_GPUCOMMANDBUFFERVK_H