// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_VULKAN_IMAGeVK_H
#define FXDENGINE_GFX_VULKAN_IMAGEVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/Image.h"
#include "FXDEngine/GFX/Vulkan/Vulkan.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IImage >
			// -
			// ------
			template <>
			class Impl< GFX::IImageView >
			{
			public:
				friend class GFX::IImageView;

			public:
				Impl(void);
				~Impl(void);

				GET_R(VkImageView, vkImageViewRenderTarget, vkImageViewRenderTarget);
				SET(VkImageView, vkImageViewRenderTarget, vkImageViewRenderTarget);

				GET_R(VkImageView, vkImageViewDepth, vkImageViewDepth);
				SET(VkImageView, vkImageViewDepth, vkImageViewDepth);

				GET_R(VkImageView, vkImageViewShader, vkImageViewShader);
				SET(VkImageView, vkImageViewShader, vkImageViewShader);

			private:
				VkImageView m_vkImageViewRenderTarget;
				VkImageView m_vkImageViewDepth;
				VkImageView m_vkImageViewShader;
			};

			// ------
			// Impl< GFX::IImage >
			// -
			// ------
			template <>
			class Impl< GFX::IImage >
			{
			public:
				friend class GFX::IImage;

			public:
				Impl(void);
				~Impl(void);

				GET_R(VkImage, vkImage, vkImage);
				GET_R(VkDeviceMemory, vkDeviceMemory, vkDeviceMemory);
				GET_R(bool, bSwapChain, bSwapChain);

				SET(VkImage, vkImage, vkImage);
				SET(VkDeviceMemory, vkDeviceMemory, vkDeviceMemory);
				SET(bool, bSwapChain, bSwapChain);

			private:
				bool m_bSwapChain;
				VkImage m_vkImage;
				VkDeviceMemory m_vkDeviceMemory;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_DX11_IMAGEDX11_H