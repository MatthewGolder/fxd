// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_VULKAN_HWBUFFERVK_H
#define FXDENGINE_GFX_VULKAN_HWBUFFERVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/HWBuffer.h"
#include "FXDEngine/GFX/Vulkan/Vulkan.h"
#include "FXDEngine/Container/Map.h"
#include "FXDEngine/Container/RefCounting.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IHWBuffer >
			// -
			// ------
			template <>
			class Impl< GFX::IHWBuffer >
			{
			public:
				friend class GFX::IHWBuffer;

			public:
				Impl(void);
				~Impl(void);

			private:
				VkBuffer m_vkBuffer;
				VkBufferUsageFlags m_vkUsageFlags;
				VkDeviceMemory m_vkDeviceMemory;
				Container::Map< GFX::LockedKey, GFX::LockedData > m_outstandingLocks;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD

#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_VULKAN_HWBUFFERVK_H