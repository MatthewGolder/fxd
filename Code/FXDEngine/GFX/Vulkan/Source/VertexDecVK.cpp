// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/Vulkan/GfxAdapterVK.h"
#include "FXDEngine/GFX/Vulkan/VertexDecVK.h"

using namespace FXD;
using namespace GFX;

// ------
// IVertexDecleration::Impl::Impl
// - 
// ------
IVertexDecleration::Impl::Impl::Impl(void)
{
}

IVertexDecleration::Impl::~Impl(void)
{
}

// ------
// IVertexDecleration
// - 
// ------
IVertexDecleration::IVertexDecleration(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IVertexDecleration::~IVertexDecleration(void)
{
}

bool IVertexDecleration::_create(const FX::VaryingListDef& varyingList, const Memory::MemHandle& mem)
{
	return true;
}

bool IVertexDecleration::_release(void)
{
	return true;
}

void IVertexDecleration::_attach(void)
{
}

void IVertexDecleration::_detach(void)
{
}

#endif //IsGFXDX11()