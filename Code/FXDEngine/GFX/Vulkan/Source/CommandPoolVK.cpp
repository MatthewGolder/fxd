// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/Vulkan/CommandPoolVK.h"
#include "FXDEngine/GFX/Vulkan/GfxAdapterVK.h"
#include "FXDEngine/GFX/Vulkan/GfxApiVK.h"

using namespace FXD;
using namespace GFX;

// ------
// VKCommandPool
// - 
// ------
VKCommandPool::VKCommandPool(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
	, m_vkCmdPool(VK_NULL_HANDLE)
{
}

VKCommandPool::~VKCommandPool(void)
{
	PRINT_COND_ASSERT((m_vkCmdPool == VK_NULL_HANDLE), "GFX: m_vkCmdPool is not shutdown");
}

bool VKCommandPool::create(FXD::U32 nQueueFamily)
{
	VkDevice vkDevice = m_pAdapter->impl().vkDevice();
	auto pGfxApi = static_cast< const GFX::IGfxApi* >(m_pAdapter->api());

	VkCommandPoolCreateInfo vkCmdPoolInfo = {};
	GFX::MemZeroVulkanStruct(vkCmdPoolInfo, VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO);
	vkCmdPoolInfo.queueFamilyIndex = nQueueFamily;
	vkCmdPoolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
	VKResultCheckFail(m_pAdapter->impl().vkCreateCommandPool(vkDevice, &vkCmdPoolInfo, &pGfxApi->impl().vkAllocCallbacks(), &m_vkCmdPool));

	return true;
}

bool VKCommandPool::release(void)
{
	VkDevice vkDevice = m_pAdapter->impl().vkDevice();
	auto pGfxApi = static_cast< const GFX::IGfxApi* >(m_pAdapter->api());

	if (m_vkCmdPool != VK_NULL_HANDLE)
	{
		m_pAdapter->impl().vkDestroyCommandPool(vkDevice, m_vkCmdPool, &pGfxApi->impl().vkAllocCallbacks());
		m_vkCmdPool = VK_NULL_HANDLE;
	}
	return true;
}
#endif //IsGFXVulkan()