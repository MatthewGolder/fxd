// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/Vulkan/FenceVK.h"
#include "FXDEngine/GFX/Vulkan/GfxAdapterVK.h"
#include "FXDEngine/GFX/Vulkan/GfxApiVK.h"

using namespace FXD;
using namespace GFX;

// ------
// IFence::Impl::Impl
// - 
// ------
IFence::Impl::Impl::Impl(void)
	: m_vkFence(VK_NULL_HANDLE)
{
}
IFence::Impl::~Impl(void)
{
	PRINT_COND_ASSERT((m_vkFence == VK_NULL_HANDLE), "GFX: m_vkFence has not been shutdown");
}

// ------
// IFence
// - 
// ------
IFence::IFence(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}
IFence::~IFence(void)
{
}

bool IFence::_create(bool bSignaled)
{
	auto pGfxApi = static_cast< const GFX::IGfxApi* >(m_pAdapter->api());
	VkDevice vkDevice = m_pAdapter->impl().vkDevice();

	VkFenceCreateInfo vkFenceInfo = {};
	GFX::MemZeroVulkanStruct(vkFenceInfo, VK_STRUCTURE_TYPE_FENCE_CREATE_INFO);
	vkFenceInfo.pNext = nullptr;
	vkFenceInfo.flags = bSignaled ? VK_FENCE_CREATE_SIGNALED_BIT : 0;

	VKResultCheckFail(m_pAdapter->impl().vkCreateFence(vkDevice, &vkFenceInfo, &pGfxApi->impl().vkAllocCallbacks(), &m_impl->m_vkFence));
	return true;
}

bool IFence::_release(void)
{
	auto pGfxApi = static_cast< const GFX::IGfxApi* >(m_pAdapter->api());

	if (m_impl->m_vkFence != VK_NULL_HANDLE)
	{
		m_pAdapter->impl().vkDestroyFence(m_pAdapter->impl().vkDevice(), m_impl->m_vkFence, &pGfxApi->impl().vkAllocCallbacks());
		m_impl->m_vkFence = VK_NULL_HANDLE;
	}
	return true;
}

bool IFence::_is_signalled(void) const
{
	return m_pAdapter->impl().vkGetFenceStatus(m_pAdapter->impl().vkDevice(), m_impl->m_vkFence) == VK_SUCCESS;
}

bool IFence::_wait(void)
{
	VKResultCheckFail(m_pAdapter->impl().vkWaitForFences(m_pAdapter->impl().vkDevice(), 1, &m_impl->m_vkFence, VK_TRUE, UINT64_MAX));
	return true;
}

bool IFence::_reset(void)
{
	VKResultCheckFail(m_pAdapter->impl().vkResetFences(m_pAdapter->impl().vkDevice(), 1, &m_impl->m_vkFence));
	return true;
}
#endif //IsGFXVulkan()