// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"
#include "FXDEngine/GFX/Vulkan/GfxAdapterVK.h"

#if IsGFXVulkan()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/Vulkan/GfxAdapterVK.h"
#include "FXDEngine/GFX/Vulkan/GfxApiVK.h"
#include "FXDEngine/GFX/Vulkan/GfxCommandBufferVK.h"
#include "FXDEngine/GFX/FX/FXLibrary.h"
#include "FXDEngine/GFX/FX/StateManager.h"

using namespace FXD;
using namespace GFX;

CONSTEXPR FXD::UTF8* vkDeviceExtensions[] =
{
	VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

// ------
// IGfxAdapterDesc::Impl::Impl
// - 
// ------
IGfxAdapterDesc::Impl::Impl::Impl(void)
	: m_vkPhysicalDevice(VK_NULL_HANDLE)
{
}

IGfxAdapterDesc::Impl::~Impl(void)
{
}

// ------
// IGfxAdapterDesc::IGfxAdapterDesc
// - 
// ------
IGfxAdapterDesc::IGfxAdapterDesc(const App::DeviceIndex nDeviceID)
	: IAdapterDesc(nDeviceID)
	, m_eDeviceType(GFX::E_DeviceType::Unknown)
{
}
IGfxAdapterDesc::~IGfxAdapterDesc(void)
{
}

// ------
// IGfxAdapter::Impl::Impl
// - 
// ------
IGfxAdapter::Impl::Impl::Impl(GFX::IGfxAdapter* pAdapter)
	: m_vkDevice(VK_NULL_HANDLE)
	, m_vkPipelineCache(VK_NULL_HANDLE)
	, m_vkDescriptorPool(VK_NULL_HANDLE)
	, m_commandPool(pAdapter)

	, vkGetDeviceQueue(nullptr)
	, vkCreateBuffer(nullptr)
	, vkDestroyBuffer(nullptr)
	, vkGetBufferMemoryRequirements(nullptr)
	, vkGetImageMemoryRequirements(nullptr)
	, vkBindBufferMemory(nullptr)
	, vkBindImageMemory(nullptr)
	, vkCreateImage(nullptr)
	, vkDestroyImage(nullptr)
	, vkCreateImageView(nullptr)
	, vkDestroyImageView(nullptr)
	, vkCmdPipelineBarrier(nullptr)
	, vkCmdClearColorImage(nullptr)
	, vkQueuePresentKHR(nullptr)
	, vkQueueWaitIdle(nullptr)
	, vkDeviceWaitIdle(nullptr)
	, vkCmdSetViewport(nullptr)
	, vkCmdSetScissor(nullptr)
	, vkCmdClearAttachments(nullptr)
	// Command Buffer Submission
	, vkQueueSubmit(nullptr)
	// Command Pools
	, vkCreateCommandPool(nullptr)
	, vkDestroyCommandPool(nullptr)
	, vkResetCommandPool(nullptr)
	// Command Buffer Allocation and Management
	, vkAllocateCommandBuffers(nullptr)
	, vkFreeCommandBuffers(nullptr)
	, vkResetCommandBuffer(nullptr)
	// Command Buffer Recording
	, vkBeginCommandBuffer(nullptr)
	, vkEndCommandBuffer(nullptr)
	// Fences
	, vkCreateFence(nullptr)
	, vkDestroyFence(nullptr)
	, vkGetFenceStatus(nullptr)
	, vkResetFences(nullptr)
	, vkWaitForFences(nullptr)
	// Events
	, vkCreateEvent(nullptr)
	, vkDestroyEvent(nullptr)
	, vkGetEventStatus(nullptr)
	, vkSetEvent(nullptr)
	, vkResetEvent(nullptr)
	, vkCmdSetEvent(nullptr)
	, vkCmdResetEvent(nullptr)
	, vkCmdWaitEvents(nullptr)
	// Render Pass Creation
	, vkCreateRenderPass(nullptr)
	, vkDestroyRenderPass(nullptr)
	// Framebuffers
	, vkCreateFramebuffer(nullptr)
	, vkDestroyFramebuffer(nullptr)
	// Render Pass Commands
	, vkCmdBeginRenderPass(nullptr)
	, vkCmdEndRenderPass(nullptr)
	, vkCmdNextSubpass(nullptr)
	, vkGetRenderAreaGranularity(nullptr)
	// Shader modules
	, vkCreateShaderModule(nullptr)
	, vkDestroyShaderModule(nullptr)
	// Graphice Pipelines
	, vkCreateGraphicsPipelines(nullptr)
	// Pipeline Destruction
	, vkDestroyPipeline(nullptr)
	// Pipeline Buinding
	, vkCmdBindPipeline(nullptr)
	// Device Memory
	, vkAllocateMemory(nullptr)
	, vkFreeMemory(nullptr)
	, vkMapMemory(nullptr)
	, vkFlushMappedMemoryRanges(nullptr)
	, vkInvalidateMappedMemoryRanges(nullptr)
	, vkUnmapMemory(nullptr)
	, vkGetDeviceMemoryCommitment(nullptr)
	// Samplers
	, vkCreateSampler(nullptr)
	, vkDestroySampler(nullptr)
	// Descriptor Sets
	, vkCreateDescriptorSetLayout(nullptr)
	, vkDestroyDescriptorSetLayout(nullptr)
	, vkCreatePipelineLayout(nullptr)
	, vkDestroyPipelineLayout(nullptr)
	, vkCreateDescriptorPool(nullptr)
	, vkDestroyDescriptorPool(nullptr)
	, vkAllocateDescriptorSets(nullptr)
	, vkFreeDescriptorSets(nullptr)
	, vkResetDescriptorPool(nullptr)
	, vkUpdateDescriptorSets(nullptr)
	, vkCmdBindDescriptorSets(nullptr)
	, vkCmdPushConstants(nullptr)
	// Copying Data Between Buffers
	, vkCmdCopyBuffer(nullptr)
	// Copying Data Between Buffers and Images
	, vkCmdCopyBufferToImage(nullptr)
	, vkCmdCopyImageToBuffer(nullptr)
	// Programmable Primitive Shading
	, vkCmdBindIndexBuffer(nullptr)
	, vkCmdDraw(nullptr)
	, vkCmdDrawIndexed(nullptr)
	, vkCmdDrawIndirect(nullptr)
	, vkCmdDrawIndexedIndirect(nullptr)
	// Vertex Input Description
	, vkCmdBindVertexBuffers(nullptr)
	// WSI Swapchain
	, vkCreateSwapchainKHR(nullptr)
	, vkDestroySwapchainKHR(nullptr)
	, vkGetSwapchainImagesKHR(nullptr)
	, vkAcquireNextImageKHR(nullptr)
#if IsOSPC()
	, vkCreateWin32SurfaceKHR(nullptr)
	, vkGetPhysicalDeviceSurfaceSupportKHR(nullptr)
	, vkGetPhysicalDeviceSurfaceCapabilitiesKHR(nullptr)
	, vkGetPhysicalDeviceSurfacePresentModesKHR(nullptr)
	, vkGetPhysicalDeviceSurfaceFormatsKHR(nullptr)
#elif IsOSAndroid()
	, vkCreateAndroidSurfaceKHR(nullptr)
#endif
{
}

IGfxAdapter::Impl::~Impl(void)
{
	PRINT_COND_ASSERT((m_vkDevice == nullptr), "GFX: m_vkDevice has not been shutdown");
	//PRINT_COND_ASSERT((m_vkQueue == VK_NULL_HANDLE), "GFX: m_vkQueue has not been shutdown");
	//PRINT_COND_ASSERT((m_commandPool == nullptr), "GFX: m_commandPool has not been shutdown");
}

VkDeviceMemory IGfxAdapter::Impl::allocate_memory(GFX::IGfxAdapter* pAdapter, VkImage& vkImage, const VkMemoryPropertyFlags vkMemoryPropertyFlags)
{
	VkMemoryRequirements vkMemReq = {};
	Memory::MemZero_T(vkMemReq);

	vkGetImageMemoryRequirements(m_vkDevice, vkImage, &vkMemReq);

	VkDeviceMemory vkDeviceMemory = allocate_memory(pAdapter, vkMemReq, vkMemoryPropertyFlags);
	VKResultCheckFail(vkBindImageMemory(m_vkDevice, vkImage, vkDeviceMemory, 0));
	return vkDeviceMemory;
}

VkDeviceMemory IGfxAdapter::Impl::allocate_memory(GFX::IGfxAdapter* pAdapter, VkBuffer& vkBuffer, const VkMemoryPropertyFlags vkMemoryPropertyFlags)
{
	VkMemoryRequirements vkMemReq = {};
	Memory::MemZero_T(vkMemReq);

	vkGetBufferMemoryRequirements(m_vkDevice, vkBuffer, &vkMemReq);

	VkDeviceMemory vkDeviceMemory = allocate_memory(pAdapter, vkMemReq, vkMemoryPropertyFlags);
	VKResultCheckFail(vkBindBufferMemory(m_vkDevice, vkBuffer, vkDeviceMemory, 0));
	return vkDeviceMemory;
}

VkDeviceMemory IGfxAdapter::Impl::allocate_memory(GFX::IGfxAdapter* pAdapter, const VkMemoryRequirements vkMemReq, const VkMemoryPropertyFlags vkMemoryPropertyFlags)
{
	auto pGfxApi = static_cast< const GFX::IGfxApi* >(pAdapter->api());

	VkMemoryAllocateInfo vkMemoryAllocateInfo = {};
	GFX::MemZeroVulkanStruct(vkMemoryAllocateInfo, VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO);
	vkMemoryAllocateInfo.pNext = nullptr;
	vkMemoryAllocateInfo.memoryTypeIndex = find_memory_type(pAdapter, vkMemReq.memoryTypeBits, vkMemoryPropertyFlags);
	vkMemoryAllocateInfo.allocationSize = vkMemReq.size;
	if (vkMemoryAllocateInfo.memoryTypeIndex == -1)
	{
		return VK_NULL_HANDLE;
	}

	VkDeviceMemory vkDeviceMemory = {};
	VKResultCheckFail(vkAllocateMemory(m_vkDevice, &vkMemoryAllocateInfo, &pGfxApi->impl().vkAllocCallbacks(), &vkDeviceMemory));
	return vkDeviceMemory;
}

void IGfxAdapter::Impl::free_memory(GFX::IGfxAdapter* pAdapter, const VkDeviceMemory vkDeviceMemory)
{
	auto pGfxApi = static_cast< const GFX::IGfxApi* >(pAdapter->api());

	vkFreeMemory(m_vkDevice, vkDeviceMemory, &pGfxApi->impl().vkAllocCallbacks());
}

uint32_t IGfxAdapter::Impl::find_memory_type(GFX::IGfxAdapter* pAdapter, const uint32_t nRequirementBits, const VkMemoryPropertyFlags vkMemoryPropertyFlags)
{
	auto pAdapterDesc = static_cast< GFX::IGfxAdapterDesc* >(pAdapter->adapter_desc().get());
	auto& vkMemProps = pAdapterDesc->impl().vkMemoryProperties();

	for (uint32_t n1 = 0; n1 < vkMemProps.memoryTypeCount; n1++)
	{
		if (nRequirementBits & (1 << n1))
		{
			if ((vkMemProps.memoryTypes[n1].propertyFlags & vkMemoryPropertyFlags) == vkMemoryPropertyFlags)
			{
				return n1;
			}
		}
	}
	return -1;
}

bool IGfxAdapter::Impl::_create_function_pointers(VkInstance vkInstance)
{
	bool bRetVal = true;
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkGetDeviceQueue, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCreateBuffer, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkDestroyBuffer, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkGetBufferMemoryRequirements, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkGetImageMemoryRequirements, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkBindBufferMemory, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkBindImageMemory, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCreateImage, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkDestroyImage, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCreateImageView, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkDestroyImageView, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdPipelineBarrier, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdClearColorImage, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkQueuePresentKHR, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkQueueWaitIdle, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkDeviceWaitIdle, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdSetViewport, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdSetScissor, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdClearAttachments, bRetVal);
	// Command Buffer Submission
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkQueueSubmit, bRetVal);
	// Command Pools
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCreateCommandPool, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkDestroyCommandPool, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkResetCommandPool, bRetVal);
	// Command Buffer Allocation and Management
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkAllocateCommandBuffers, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkFreeCommandBuffers, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkResetCommandBuffer, bRetVal);
	// Command Buffer Recording
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkBeginCommandBuffer, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkEndCommandBuffer, bRetVal);
	// Fences
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCreateFence, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkDestroyFence, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkGetFenceStatus, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkResetFences, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkWaitForFences, bRetVal);
	// Events
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCreateEvent, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkDestroyEvent, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkGetEventStatus, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkSetEvent, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkResetEvent, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdSetEvent, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdResetEvent, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdWaitEvents, bRetVal);
	// Render Pass Creation
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCreateRenderPass, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkDestroyRenderPass, bRetVal);
	// Framebuffers
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCreateFramebuffer, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkDestroyFramebuffer, bRetVal);
	// Render Pass Commands
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdBeginRenderPass, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdEndRenderPass, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdNextSubpass, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkGetRenderAreaGranularity, bRetVal);
	// Shader modules
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCreateShaderModule, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkDestroyShaderModule, bRetVal);
	// Graphice Pipelines
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCreateGraphicsPipelines, bRetVal);
	// Pipeline Destruction
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkDestroyPipeline, bRetVal);
	// Pipeline Buinding
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdBindPipeline, bRetVal);
	// Device Memory
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkAllocateMemory, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkFreeMemory, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkMapMemory, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkFlushMappedMemoryRanges, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkInvalidateMappedMemoryRanges, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkUnmapMemory, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkGetDeviceMemoryCommitment, bRetVal);
	// Samplers
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCreateSampler, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkDestroySampler, bRetVal);
	// Descriptor Sets
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCreateDescriptorSetLayout, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkDestroyDescriptorSetLayout, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCreatePipelineLayout, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkDestroyPipelineLayout, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCreateDescriptorPool, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkDestroyDescriptorPool, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkAllocateDescriptorSets, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkFreeDescriptorSets, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkResetDescriptorPool, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkUpdateDescriptorSets, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdBindDescriptorSets, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdPushConstants, bRetVal);
	// Copying Data Between Buffers
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdCopyBuffer, bRetVal);
	// Copying Data Between Buffers and Images
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdCopyBufferToImage, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdCopyImageToBuffer, bRetVal);
	// Programmable Primitive Shading
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdBindIndexBuffer, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdDraw, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdDrawIndexed, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdDrawIndirect, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdDrawIndexedIndirect, bRetVal);
	// Vertex Input Description
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCmdBindVertexBuffers, bRetVal);
	// WSI Swapchain
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCreateSwapchainKHR, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkDestroySwapchainKHR, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkGetSwapchainImagesKHR, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkAcquireNextImageKHR, bRetVal);
#if IsOSPC()
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCreateWin32SurfaceKHR, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkGetPhysicalDeviceSurfaceSupportKHR, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkGetPhysicalDeviceSurfaceCapabilitiesKHR, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkGetPhysicalDeviceSurfacePresentModesKHR, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkGetPhysicalDeviceSurfaceFormatsKHR, bRetVal);
#elif IsOSAndroid()
	GET_VULKAN_INSTANCE_PROC_CHECK(vkInstance, vkCreateAndroidSurfaceKHR, bRetVal);
#endif
	return bRetVal;
}

bool IGfxAdapter::Impl::_release_function_pointers(void)
{
	vkGetDeviceQueue = nullptr;
	vkCreateBuffer = nullptr;
	vkDestroyBuffer = nullptr;
	vkGetBufferMemoryRequirements = nullptr;
	vkGetImageMemoryRequirements = nullptr;
	vkBindBufferMemory = nullptr;
	vkBindImageMemory = nullptr;
	vkCreateImage = nullptr;
	vkDestroyImage = nullptr;
	vkCreateImageView = nullptr;
	vkDestroyImageView = nullptr;
	vkCmdPipelineBarrier = nullptr;
	vkCmdClearColorImage = nullptr;
	vkQueuePresentKHR = nullptr;
	vkQueueWaitIdle = nullptr;
	vkDeviceWaitIdle = nullptr;
	vkCmdSetViewport = nullptr;
	vkCmdSetScissor = nullptr;
	vkCmdClearAttachments = nullptr;
	// Command Buffer Submission
	vkQueueSubmit = nullptr;
	// Command Pools
	vkCreateCommandPool = nullptr;
	vkDestroyCommandPool = nullptr;
	vkResetCommandPool = nullptr;
	// Command Buffer Allocation and Management
	vkAllocateCommandBuffers = nullptr;
	vkFreeCommandBuffers = nullptr;
	vkResetCommandBuffer = nullptr;
	// Command Buffer Recording
	vkBeginCommandBuffer = nullptr;
	vkEndCommandBuffer = nullptr;
	// Fences
	vkCreateFence = nullptr;
	vkDestroyFence = nullptr;
	vkGetFenceStatus = nullptr;
	vkResetFences = nullptr;
	vkWaitForFences = nullptr;
	// Events
	vkCreateEvent = nullptr;
	vkDestroyEvent = nullptr;
	vkGetEventStatus = nullptr;
	vkSetEvent = nullptr;
	vkResetEvent = nullptr;
	vkCmdSetEvent = nullptr;
	vkCmdResetEvent = nullptr;
	vkCmdWaitEvents = nullptr;
	// Render Pass Creation
	vkCreateRenderPass = nullptr;
	vkDestroyRenderPass = nullptr;
	// Framebuffers
	vkCreateFramebuffer = nullptr;
	vkDestroyFramebuffer = nullptr;
	// Render Pass Commands
	vkCmdBeginRenderPass = nullptr;
	vkCmdEndRenderPass = nullptr;
	vkCmdNextSubpass = nullptr;
	vkGetRenderAreaGranularity = nullptr;
	// Shader modules
	vkCreateShaderModule = nullptr;
	vkDestroyShaderModule = nullptr;
	// Graphice Pipelines
	vkCreateGraphicsPipelines = nullptr;
	// Pipeline Destruction
	vkDestroyPipeline = nullptr;
	// Pipeline Buinding
	vkCmdBindPipeline = nullptr;
	// Device Memory
	vkAllocateMemory = nullptr;
	vkFreeMemory = nullptr;
	vkMapMemory = nullptr;
	vkFlushMappedMemoryRanges = nullptr;
	vkInvalidateMappedMemoryRanges = nullptr;
	vkUnmapMemory = nullptr;
	vkGetDeviceMemoryCommitment = nullptr;
	// Samplers
	vkCreateSampler = nullptr;
	vkDestroySampler = nullptr;
	// Descriptor Sets
	vkCreateDescriptorSetLayout = nullptr;
	vkDestroyDescriptorSetLayout = nullptr;
	vkCreatePipelineLayout = nullptr;
	vkDestroyPipelineLayout = nullptr;
	vkCreateDescriptorPool = nullptr;
	vkDestroyDescriptorPool = nullptr;
	vkAllocateDescriptorSets = nullptr;
	vkFreeDescriptorSets = nullptr;
	vkResetDescriptorPool = nullptr;
	vkUpdateDescriptorSets = nullptr;
	vkCmdBindDescriptorSets = nullptr;
	vkCmdPushConstants = nullptr;
	// Copying Data Between Buffers
	vkCmdCopyBuffer = nullptr;
	// Copying Data Between Buffers and Images
	vkCmdCopyBufferToImage = nullptr;
	vkCmdCopyImageToBuffer = nullptr;
	// Programmable Primitive Shading
	vkCmdBindIndexBuffer = nullptr;
	vkCmdDraw = nullptr;
	vkCmdDrawIndexed = nullptr;
	vkCmdDrawIndirect = nullptr;
	vkCmdDrawIndexedIndirect = nullptr;
	// Vertex Input Description
	vkCmdBindVertexBuffers = nullptr;
	// WSI Swapchain
	vkCreateSwapchainKHR = nullptr;
	vkDestroySwapchainKHR = nullptr;
	vkGetSwapchainImagesKHR = nullptr;
	vkAcquireNextImageKHR = nullptr;
#if IsOSPC()
	vkCreateWin32SurfaceKHR = nullptr;
	vkGetPhysicalDeviceSurfaceSupportKHR = nullptr;
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR = nullptr;
	vkGetPhysicalDeviceSurfacePresentModesKHR = nullptr;
	vkGetPhysicalDeviceSurfaceFormatsKHR = nullptr;
#elif IsOSAndroid()
	vkCreateAndroidSurfaceKHR = nullptr;
#endif
	return true;
}

// ------
// IGfxAdapter
// - 
// ------
IGfxAdapter::IGfxAdapter(const App::AdapterDesc& adapterDesc, const App::IApi* pApi)
	: IAdapter(adapterDesc, pApi)
	, HasImpl(this)
	, m_pDrawingViewport(nullptr)
{
}

IGfxAdapter::~IGfxAdapter(void)
{
}

bool IGfxAdapter::_create(void)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	auto pAdapterDesc = static_cast< GFX::IGfxAdapterDesc* >(adapter_desc().get());
	auto pGfxApi = static_cast< const GFX::IGfxApi* >(api());

	VkInstance vkInstance = pGfxApi->impl().vkInstance();
	VkPhysicalDevice vkPhysicalDevice = pAdapterDesc->impl().vkPhysicalDevice();

	// 1. Get the queue and extension information
	FXD::U32 nQueueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(vkPhysicalDevice, &nQueueFamilyCount, nullptr);
	m_impl->m_vkQueueFamilyProperties.resize(nQueueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(vkPhysicalDevice, &nQueueFamilyCount, m_impl->m_vkQueueFamilyProperties.data());

	FXD::U32 nDeviceExtensionCount = 0;
	vkEnumerateDeviceExtensionProperties(vkPhysicalDevice, nullptr, &nDeviceExtensionCount, nullptr);
	m_impl->m_vkExtensionProperties.resize(nDeviceExtensionCount);
	vkEnumerateDeviceExtensionProperties(vkPhysicalDevice, nullptr, &nDeviceExtensionCount, m_impl->m_vkExtensionProperties.data());


	// 2. Create the device
	const FXD::F32 fDefaultQueuePriority = 0.0f;
	Container::Vector< VkDeviceQueueCreateInfo > vkDeviceQueueInfos;
	auto populateQueueInfo = [&](GFX::E_GfxQueueType eType, FXD::U16 nFamilyIdx)
	{
		VkDeviceQueueCreateInfo vkQueueInfo = {};
		GFX::MemZeroVulkanStruct(vkQueueInfo, VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO);
		vkQueueInfo.queueFamilyIndex = nFamilyIdx;
		vkQueueInfo.queueCount = FXD::STD::Min(m_impl->m_vkQueueFamilyProperties[nFamilyIdx].queueCount, (uint32_t)GFX::kMaxQueuesPerType);
		vkQueueInfo.pQueuePriorities = &fDefaultQueuePriority;
		vkDeviceQueueInfos.push_back(vkQueueInfo);

		m_impl->m_vkQueueInfos[FXD::STD::to_underlying(eType)].familyIdx = nFamilyIdx;
		m_impl->m_vkQueueInfos[FXD::STD::to_underlying(eType)].vkQueues = Container::Vector< VkQueue >(vkQueueInfo.queueCount);
	};

	// 3. Create the queues
	// 3.1. Compute and not Graphics
	FXD::U16 nCount = 0;
	fxd_for(const auto& itr, m_impl->m_vkQueueFamilyProperties)
	{
		if ((itr.queueFlags & VK_QUEUE_COMPUTE_BIT) && (itr.queueFlags & VK_QUEUE_GRAPHICS_BIT) == 0)
		{
			populateQueueInfo(GFX::E_GfxQueueType::Compute, nCount);
			break;
		}
		nCount++;
	}

	// 3.2. Upload and not Compute or Graphics
	nCount = 0;
	fxd_for(const auto& itr, m_impl->m_vkQueueFamilyProperties)
	{
		if ((itr.queueFlags & VK_QUEUE_TRANSFER_BIT) && ((itr.queueFlags & VK_QUEUE_GRAPHICS_BIT) == 0) && ((itr.queueFlags & VK_QUEUE_COMPUTE_BIT) == 0))
		{
			populateQueueInfo(GFX::E_GfxQueueType::Upload, nCount);
			break;
		}
		nCount++;
	}

	// 3.3. Graphics
	nCount = 0;
	fxd_for(const auto& itr, m_impl->m_vkQueueFamilyProperties)
	{
		if (itr.queueFlags & VK_QUEUE_GRAPHICS_BIT)
		{
			populateQueueInfo(GFX::E_GfxQueueType::Graphics, nCount);
			break;
		}
		nCount++;
	}

	VkDeviceCreateInfo vkDeviceInfo = {};
	GFX::MemZeroVulkanStruct(vkDeviceInfo, VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO);
	vkDeviceInfo.queueCreateInfoCount = vkDeviceQueueInfos.size();
	vkDeviceInfo.pQueueCreateInfos = vkDeviceQueueInfos.data();
	vkDeviceInfo.enabledExtensionCount = FXD::STD::array_size(vkDeviceExtensions);
	vkDeviceInfo.ppEnabledExtensionNames = &vkDeviceExtensions[0];
	vkDeviceInfo.pEnabledFeatures = nullptr;
	if (VKResultCheckFail(vkCreateDevice(pAdapterDesc->impl().vkPhysicalDevice(), &vkDeviceInfo, &pGfxApi->impl().vkAllocCallbacks(), &m_impl->m_vkDevice)))
	{
		PRINT_WARN << "GFX: vkCreateDevice()";
		return false;
	}

	// 4. Create the function pointers
	bool bRetVal = m_impl->_create_function_pointers(vkInstance);

	// 5. Create a queue to graphics
	fxd_for(auto& vkQueueInfo, m_impl->m_vkQueueInfos)
	{
		for (uint32_t n1 = 0; n1 < vkQueueInfo.vkQueues.size(); n1++)
		{
			m_impl->vkGetDeviceQueue(m_impl->m_vkDevice, vkQueueInfo.familyIdx, n1, &vkQueueInfo.vkQueues[n1]);
		}
	}

	// 6. Create a device command pool
	m_impl->m_commandPool.create(m_impl->get_queue_family(GFX::E_GfxQueueType::Graphics));

	m_fxLibrary = std::make_shared< GFX::FX::IFXLibrary >(this);
	return bRetVal;
}

bool IGfxAdapter::_release(void)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	auto pGfxApi = static_cast< const GFX::IGfxApi* >(api());

	m_impl->_release_function_pointers();
	m_impl->m_commandPool.release();

	for (FXD::U16 n1 = 0; n1 < FXD::STD::to_underlying(E_GfxQueueType::Count); n1++)
	{
		for (FXD::U16 n2 = 0; n2 < m_impl->m_vkQueueInfos[n1].vkQueues.size(); n2++)
		{
			m_impl->m_vkQueueInfos[n1].vkQueues[n2] = VK_NULL_HANDLE;
		}
	}
	if (m_impl->m_vkDevice != nullptr)
	{
		vkDestroyDevice(m_impl->m_vkDevice, &pGfxApi->impl().vkAllocCallbacks());
		m_impl->m_vkDevice = nullptr;
	}
	return true;
}

#endif //IsGFXVulkan()