// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/Window.h"
#include "FXDEngine/GFX/RenderTarget.h"
#include "FXDEngine/GFX/Vulkan/GfxAdapterVK.h"
#include "FXDEngine/GFX/Vulkan/GfxApiVK.h"
#include "FXDEngine/GFX/Vulkan/ImageVK.h"
#include "FXDEngine/GFX/Vulkan/RenderTargetVK.h"
#include "FXDEngine/GFX/Vulkan/SwapChainVK.h"
#include "FXDEngine/GFX/Vulkan/ViewportVK.h"
#include "FXDEngine/Job/EventHandler.h"

using namespace FXD;
using namespace GFX;

// ------
// IViewport::Impl::Impl
// - 
// ------
IViewport::Impl::Impl::Impl(void)
	: m_nCurrentBuffer(0)
{
}

IViewport::Impl::~Impl(void)
{
}

// ------
// IViewport
// - 
// ------
IViewport::IViewport(IGfxAdapter* pAdapter, GFX::IWindow* pWindow)
	: m_pAdapter(pAdapter)
	, m_pWindow(pWindow)
	, m_size(pWindow->window_desc().size)
	, OnWindowResize(nullptr)
{
	OnWindowResize = FXDEvent()->attach< GFX::OnWindowResize >([&](auto evt)
	{
		if (evt.WindowHandle == m_pWindow->windowHandle())
		{
			resize(evt.Pos);
		}
	});
}

IViewport::~IViewport(void)
{
	if (OnWindowResize != nullptr)
	{
		FXDEvent()->detach(OnWindowResize);
		OnWindowResize = nullptr;
	}
}

bool IViewport::_create(void)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	// 1. Create a swap chain
	m_swapChain = std::make_shared< GFX::ISwapChain >(m_pAdapter, this);
	PRINT_COND_ASSERT(m_swapChain->create(), "GFX: Failed m_swapChain->create()");

	// 2. Create the command buffers
	/*m_impl->m_commandBuffer =*/ m_pAdapter->create_command_buffer(GFX::E_GfxQueueType::Graphics, 1, GFX::E_CommandBufferType::Primary).get();

	// 3. Create the fences
	/*
	m_impl->m_fence = m_pAdapter->create_fence(true).get();
	m_impl->m_fence->reset();
	*/
	return true;
}

bool IViewport::_release(void)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	m_pWindow = nullptr;
	FXD_RELEASE(m_swapChain, release());
	return true;
}

bool IViewport::_resize(const Math::Vector2I newSize)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	if ((newSize.x() == 0) || (newSize.y() == 0) || (m_size == newSize))
	{
		return true;
	}
	else
	{
		m_size = newSize;
		VKResultCheckFail(m_pAdapter->impl().vkDeviceWaitIdle(m_pAdapter->impl().vkDevice()));
		if (m_swapChain)
		{
			m_swapChain->resize(newSize);
		}
		VKResultCheckFail(m_pAdapter->impl().vkDeviceWaitIdle(m_pAdapter->impl().vkDevice()));
	}
	return true;
}

void IViewport::_clear(const Core::ColourRGBA& colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil) const
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	m_swapChain->render_target()->clear(colour, eClearFlags, fZ, fDepth, nStencil);
}

void IViewport::_begin_drawing_viewport(void) const
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	m_swapChain->render_target()->activate();

	// 1. Get the next buffer and wait for it to finish
	/*
	VKResultCheckFail(m_pAdapter->impl().vkAcquireNextImageKHR(m_pAdapter->impl().vkDevice(), m_swapChain->impl().vkSwapChain(), UINT64_MAX, VK_NULL_HANDLE, m_impl->m_fence->impl().vkFence(), &m_impl->m_nCurrentBuffer));
	m_impl->m_fence->wait();
	m_impl->m_fence->reset();

	m_impl->m_commandBuffer->begin(m_impl->m_nCurrentBuffer, true);

	VkClearValue vkClearValues[2];
	vkClearValues[0].color = { { 0.0f, 0.0f, 0.0f, 1.0f } };
	vkClearValues[1].depthStencil = { 1.0f, 0 };
	*/

	/*
	VkRenderPassBeginInfo vkRenderPassBeginInfo = {};
	GFX::MemZeroVulkanStruct(vkRenderPassBeginInfo, VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO);
	vkRenderPassBeginInfo.pNext = nullptr;
	vkRenderPassBeginInfo.renderPass = m_impl->m_vkRenderPass;
	vkRenderPassBeginInfo.renderArea.offset.x = 0;
	vkRenderPassBeginInfo.renderArea.offset.y = 0;
	vkRenderPassBeginInfo.renderArea.extent.width = (uint32_t)m_settings.m_size.x();
	vkRenderPassBeginInfo.renderArea.extent.height = (uint32_t)m_settings.m_size.y();
	vkRenderPassBeginInfo.clearValueCount = FXD::STD::array_size(vkClearValues);
	vkRenderPassBeginInfo.pClearValues = vkClearValues;
	vkRenderPassBeginInfo.framebuffer = m_impl->m_vkFrameBuffers[m_impl->m_nCurrentBuffer];
	*/

	// Update dynamic viewport state
	/*
	VkViewport vkViewport = {};
	Memory::MemZero_T(vkViewport);
	vkViewport.x = 0.0f;
	vkViewport.y = 0.0f;
	vkViewport.width = (FXD::F32)m_settings.m_size.x();
	vkViewport.height = (FXD::F32)m_settings.m_size.y();
	vkViewport.minDepth = (FXD::F32)0.0f;
	vkViewport.maxDepth = (FXD::F32)1.0f;
	m_pAdapter->impl().vkCmdSetViewport(m_impl->m_commandBuffer->impl().vkCommandBuffer(m_impl->m_nCurrentBuffer), 0, 1, &vkViewport);

	// Update dynamic scissor state
	VkRect2D vkRect2D = {};
	Memory::MemZero_T(vkRect2D);
	vkRect2D.offset.x = 0;
	vkRect2D.offset.y = 0;
	vkRect2D.extent.width = (uint32_t)m_settings.m_size.x();
	vkRect2D.extent.height = (uint32_t)m_settings.m_size.y();
	m_pAdapter->impl().vkCmdSetScissor(m_impl->m_commandBuffer->impl().vkCommandBuffer(m_impl->m_nCurrentBuffer), 0, 1, &vkRect2D);

	m_pAdapter->impl().vkCmdBeginRenderPass(m_impl->m_commandBuffer->impl().vkCommandBuffer(m_impl->m_nCurrentBuffer), &vkRenderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
	*/
}

void IViewport::_end_drawing_viewport(void) const
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	/*
	m_pAdapter->impl().vkCmdEndRenderPass(m_impl->m_commandBuffer->impl().vkCommandBuffer(m_impl->m_nCurrentBuffer));
	m_impl->m_commandBuffer->end(m_impl->m_nCurrentBuffer);

	m_impl->m_commandBuffer->submitToQueue(m_impl->m_nCurrentBuffer, m_impl->m_fence);
	m_impl->m_fence->wait();
	m_impl->m_fence->reset();

	m_swapChain->swap(m_settings.m_bSyncToRefresh);

	m_impl->m_nCurrentBuffer++;
	m_impl->m_nCurrentBuffer %= m_impl->m_vkFrameBuffers.size();
	*/
}
#endif //IsGFXVulkan()