// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/Vulkan/GfxApiVK.h"
#include "FXDEngine/GFX/Vulkan/GfxAdapterVK.h"
#include "FXDEngine/GFX/Mapping.h"
#include "FXDEngine/GFX/Vulkan/MappingVK.h"

using namespace FXD;
using namespace GFX;

// Golbal Vulkan Functions
PFN_vkCreateInstance vkCreateInstance = nullptr;
PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr = nullptr;
PFN_vkGetDeviceProcAddr vkGetDeviceProcAddr = nullptr;
PFN_vkEnumerateInstanceExtensionProperties vkEnumerateInstanceExtensionProperties = nullptr;
PFN_vkEnumerateInstanceLayerProperties vkEnumerateInstanceLayerProperties = nullptr;

// Vulkan instance functions.
PFN_vkDestroyInstance vkDestroyInstance = nullptr;
PFN_vkEnumeratePhysicalDevices vkEnumeratePhysicalDevices = nullptr;
PFN_vkGetPhysicalDeviceProperties vkGetPhysicalDeviceProperties = nullptr;
PFN_vkGetPhysicalDeviceFeatures vkGetPhysicalDeviceFeatures = nullptr;
PFN_vkGetPhysicalDeviceQueueFamilyProperties vkGetPhysicalDeviceQueueFamilyProperties = nullptr;
PFN_vkCreateDevice vkCreateDevice = nullptr;
PFN_vkDestroyDevice vkDestroyDevice = nullptr;
PFN_vkEnumerateDeviceExtensionProperties vkEnumerateDeviceExtensionProperties = nullptr;
PFN_vkGetPhysicalDeviceMemoryProperties vkGetPhysicalDeviceMemoryProperties = nullptr;
PFN_vkGetPhysicalDeviceFormatProperties vkGetPhysicalDeviceFormatProperties = nullptr;

// Vulkan Instance Functions
#if defined(FXD_DEBUG)
PFN_vkDebugReportCallbackEXT vkDebugReportCallbackEXT = nullptr;
PFN_vkCreateDebugReportCallbackEXT vkCreateDebugReportCallbackEXT = nullptr;
PFN_vkDestroyDebugReportCallbackEXT vkDestroyDebugReportCallbackEXT = nullptr;
PFN_vkDebugReportMessageEXT vkDebugReportMessageEXT = nullptr;
#endif

// Vulkan Extensions
CONSTEXPR FXD::UTF8* vkExtensions[] =
{
	VK_KHR_SURFACE_EXTENSION_NAME
#if IsOSPC()
	, VK_KHR_WIN32_SURFACE_EXTENSION_NAME
#if defined(FXD_DEBUG)
	, VK_EXT_DEBUG_REPORT_EXTENSION_NAME
#endif
#elif IsOSAndroid()
	, VK_KHR_ANDROID_SURFACE_EXTENSION_NAME
#if defined(FXD_DEBUG)
	, VK_EXT_DEBUG_REPORT_EXTENSION_NAME
#endif
#endif
};

// Vulkan Validation Layers
CONSTEXPR FXD::UTF8* vkValidationLayerNames[] =
{
#if IsOSPC()
	"VK_LAYER_LUNARG_standard_validation"
#elif IsOSAndroid()
	"VK_LAYER_GOOGLE_threading",
	"VK_LAYER_LUNARG_parameter_validation",
	"VK_LAYER_LUNARG_object_tracker",
	"VK_LAYER_LUNARG_core_validation",
	"VK_LAYER_LUNARG_swapchain",
	"VK_LAYER_GOOGLE_unique_objects"
#endif
};

// ------
// VKApi
// -
// ------
namespace VKApi
{
	void* VKAPI_PTR malloc_func(void* /*pPtr*/, size_t nSize, size_t nAlignment, VkSystemAllocationScope vkSystemAllocationScope)
	{
		if (nSize == 0)
		{
			return nullptr;
		}
		void* pRet = Memory::Alloc::Default.allocate(nSize);//, nAlignment);
		return pRet;
	}

	void VKAPI_PTR free_func(void* /*pPtr*/, void* pMemory)
	{
		if (pMemory == nullptr)
		{
			return;
		}
		Memory::Alloc::Default.deallocate(pMemory/*, "deallocate"*/);
	}

	void* VKAPI_PTR realloc_func(void* pPtr, void* pOriginal, size_t nSize, size_t nAlignment, VkSystemAllocationScope vkSystemAllocationScope)
	{
		if (pOriginal == nullptr)
		{
			void* pRet = VKApi::malloc_func(pPtr, nSize, nAlignment, vkSystemAllocationScope);
			return pRet;
		}
		if (Memory::Alloc::Default.grow_memory(pOriginal, nSize))
		{
			return pOriginal;
		}

		void* pRet = VKApi::malloc_func(pPtr, nSize, nAlignment, vkSystemAllocationScope);
		Memory::MemCopy(pRet, pOriginal, nSize);
		VKApi::free_func(pPtr, pOriginal);
		return pRet;
	}

	void VKAPI_PTR internal_malloc_func(void* /*pPtr*/, size_t nSize, VkInternalAllocationType vkInternalAllocationType, VkSystemAllocationScope vkSystemAllocationScope)
	{
		FXD_ERROR("internal_malloc_func");
	}

	void VKAPI_PTR internal_free_func(void* /*pPtr*/, size_t nSize, VkInternalAllocationType vkInternalAllocationType, VkSystemAllocationScope vkSystemAllocationScope)
	{
		FXD_ERROR("internal_free_func");
	}

	VkBool32 VKAPI_PTR debug_func(VkDebugReportFlagsEXT vkDebugReportFlags, VkDebugReportObjectTypeEXT vkDebugReportObjectType, uint64_t object, size_t nLocation, int32_t nMessageCode, const char* pLayerPrefix, const char* pMessage, void* pUserData)
	{
		if (vkDebugReportFlags & VK_DEBUG_REPORT_ERROR_BIT_EXT)
		{
			PRINT_ASSERT << "GFX: ERROR [" << pLayerPrefix << "]" << pMessage;
		}
		else if (vkDebugReportFlags & VK_DEBUG_REPORT_WARNING_BIT_EXT)
		{
			PRINT_WARN << "GFX: WARNING [" << pLayerPrefix << "]" << pMessage;
		}
		else if (vkDebugReportFlags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT)
		{
			PRINT_WARN << "GFX: PERFORMANCE [" << pLayerPrefix << "]" << pMessage;
		}
		else if (vkDebugReportFlags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT)
		{
			PRINT_WARN << "GFX: INFO [" << pLayerPrefix << "]" << pMessage;
		}
		else if (vkDebugReportFlags & VK_DEBUG_REPORT_DEBUG_BIT_EXT)
		{
			PRINT_WARN << "GFX: DEBUG [" << pLayerPrefix << "]" << pMessage;
		}
		else
		{
			PRINT_WARN << "GFX: Failed [" << pLayerPrefix << "]" << pMessage;
		}	return VK_FALSE;
	}
} //namespace VKApi

// ------
// IGfxApi::Impl::Impl
// - 
// ------
IGfxApi::Impl::Impl::Impl(void)
	: m_dllHandle(nullptr)
	, m_vkInstance(nullptr)
#if defined(FXD_DEBUG)
	, m_vkMsgCallback(nullptr)
#endif
{
}

IGfxApi::Impl::~Impl(void)
{
}

// ------
// IGfxApi
// -
// ------
IGfxApi::IGfxApi(void)
	: IApi(App::E_Api::GFX)
	, m_eGfxApi(GFX::E_GfxApi::Vulkan)
{
}

IGfxApi::~IGfxApi(void)
{
}

bool IGfxApi::create_api(void)
{
	// 1. Create the library
	bool bRetVal = true;

#if IsOSPC()
	m_impl->m_dllHandle = LoadLibrary(TEXT("vulkan-1.dll"));
#elif IsOSAndroid()
	m_dllHandle = dlopen("libvulkan.so", RTLD_NOW);
#endif
	if (m_impl->m_dllHandle == nullptr)
	{
		PRINT_ASSERT << "GFX: Failed to load vulkan-1 library";
	}

	// 2. Golbal Vulkan Functions
	GET_VULKAN_PROC_CHECK(vkCreateInstance, bRetVal);
	GET_VULKAN_PROC_CHECK(vkGetInstanceProcAddr, bRetVal);
	GET_VULKAN_PROC_CHECK(vkGetDeviceProcAddr, bRetVal);
	GET_VULKAN_PROC_CHECK(vkEnumerateInstanceExtensionProperties, bRetVal);
	GET_VULKAN_PROC_CHECK(vkEnumerateInstanceLayerProperties, bRetVal);

	// 3. Create instance
	VkApplicationInfo vkApplicationInfo = {};
	GFX::MemZeroVulkanStruct(vkApplicationInfo, VK_STRUCTURE_TYPE_APPLICATION_INFO);
	vkApplicationInfo.pNext = nullptr;
	vkApplicationInfo.pApplicationName = "ApplicationName";
	vkApplicationInfo.pEngineName = "Engine";
	vkApplicationInfo.apiVersion = VK_MAKE_VERSION(1, 0, 3);

	VkInstanceCreateInfo vkInstanceInfo = {};
	GFX::MemZeroVulkanStruct(vkInstanceInfo, VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO);
	vkInstanceInfo.pNext = nullptr;
	vkInstanceInfo.flags = 0;
	vkInstanceInfo.pApplicationInfo = &vkApplicationInfo;
	vkInstanceInfo.enabledExtensionCount = (uint32_t)FXD::STD::array_size(vkExtensions);
	vkInstanceInfo.ppEnabledExtensionNames = (vkInstanceInfo.enabledExtensionCount > 0) ? &vkExtensions[0] : nullptr;
	vkInstanceInfo.enabledLayerCount = (uint32_t)FXD::STD::array_size(vkValidationLayerNames);
	vkInstanceInfo.ppEnabledLayerNames = (vkInstanceInfo.enabledLayerCount > 0) ? &vkValidationLayerNames[0] : nullptr;

	m_impl->m_vkAllocCallbacks.pUserData = this;
	m_impl->m_vkAllocCallbacks.pfnAllocation = (PFN_vkAllocationFunction)VKApi::malloc_func;
	m_impl->m_vkAllocCallbacks.pfnReallocation = (PFN_vkReallocationFunction)VKApi::realloc_func;
	m_impl->m_vkAllocCallbacks.pfnFree = (PFN_vkFreeFunction)VKApi::free_func;
	m_impl->m_vkAllocCallbacks.pfnInternalAllocation = (PFN_vkInternalAllocationNotification)VKApi::internal_malloc_func;
	m_impl->m_vkAllocCallbacks.pfnInternalFree = (PFN_vkInternalFreeNotification)VKApi::internal_free_func;
	if (VKResultCheckFail(vkCreateInstance(&vkInstanceInfo, &m_impl->m_vkAllocCallbacks, &m_impl->m_vkInstance)))
	{
		PRINT_ASSERT << "GFX: Failed vkCreateInstance()";
	}

	// 4. Vulkan Instance Functions
	GET_VULKAN_INSTANCE_PROC_CHECK(m_impl->m_vkInstance, vkDestroyInstance, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(m_impl->m_vkInstance, vkEnumeratePhysicalDevices, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(m_impl->m_vkInstance, vkGetPhysicalDeviceProperties, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(m_impl->m_vkInstance, vkGetPhysicalDeviceFeatures, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(m_impl->m_vkInstance, vkGetPhysicalDeviceQueueFamilyProperties, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(m_impl->m_vkInstance, vkCreateDevice, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(m_impl->m_vkInstance, vkDestroyDevice, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(m_impl->m_vkInstance, vkEnumerateDeviceExtensionProperties, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(m_impl->m_vkInstance, vkGetPhysicalDeviceMemoryProperties, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(m_impl->m_vkInstance, vkGetPhysicalDeviceFormatProperties, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(m_impl->m_vkInstance, vkGetDeviceProcAddr, bRetVal);
#if defined(FXD_DEBUG)
	GET_VULKAN_INSTANCE_PROC_CHECK(m_impl->m_vkInstance, vkDebugReportCallbackEXT, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(m_impl->m_vkInstance, vkCreateDebugReportCallbackEXT, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(m_impl->m_vkInstance, vkDestroyDebugReportCallbackEXT, bRetVal);
	GET_VULKAN_INSTANCE_PROC_CHECK(m_impl->m_vkInstance, vkDebugReportMessageEXT, bRetVal);

	VkDebugReportCallbackCreateInfoEXT vkDebugCallbackInfo = {};
	GFX::MemZeroVulkanStruct(vkDebugCallbackInfo, VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT);
	vkDebugCallbackInfo.pfnCallback = (PFN_vkDebugReportCallbackEXT)VKApi::debug_func;
	vkDebugCallbackInfo.flags = (VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT);
	if (VKResultCheckFail(vkCreateDebugReportCallbackEXT(m_impl->m_vkInstance, &vkDebugCallbackInfo, &m_impl->m_vkAllocCallbacks, &m_impl->m_vkMsgCallback)))
	{
		PRINT_ASSERT << "GFX: Failed vkCreateDebugReportCallbackEXT()";
	}
#endif

	// 5. Create the list of devices
	_create_adapter_list();

	return bRetVal;
}

bool IGfxApi::release_api(void)
{
#if defined(FXD_DEBUG)
	if (m_impl->m_vkMsgCallback != nullptr)
	{
		vkDestroyDebugReportCallbackEXT(m_impl->m_vkInstance, m_impl->m_vkMsgCallback, &m_impl->m_vkAllocCallbacks);
		m_impl->m_vkMsgCallback = nullptr;
	}
#endif
	if (m_impl->m_vkInstance != nullptr)
	{
		vkDestroyInstance(m_impl->m_vkInstance, &m_impl->m_vkAllocCallbacks);
		m_impl->m_vkInstance = nullptr;
	}

	// Golbal Vulkan Functions
	vkCreateInstance = nullptr;
	vkGetInstanceProcAddr = nullptr;
	vkGetDeviceProcAddr = nullptr;
	vkEnumerateInstanceExtensionProperties = nullptr;
	vkEnumerateInstanceLayerProperties = nullptr;

	// Golbal Vulkan instance functions.
	vkDestroyInstance = nullptr;
	vkEnumeratePhysicalDevices = nullptr;
	vkGetPhysicalDeviceProperties = nullptr;
	vkGetPhysicalDeviceFeatures = nullptr;
	vkGetPhysicalDeviceQueueFamilyProperties = nullptr;
	vkCreateDevice = nullptr;
	vkDestroyDevice = nullptr;
	vkEnumerateDeviceExtensionProperties = nullptr;
	vkGetPhysicalDeviceMemoryProperties = nullptr;
	vkGetPhysicalDeviceFormatProperties = nullptr;
	vkGetDeviceProcAddr;
#if defined(FXD_DEBUG)
	vkDebugReportCallbackEXT = nullptr;
	vkCreateDebugReportCallbackEXT = nullptr;
	vkDestroyDebugReportCallbackEXT = nullptr;
	vkDebugReportMessageEXT = nullptr;
#endif

	if (m_impl->m_dllHandle != nullptr)
	{
#if IsOSPC()
		FreeLibrary(m_impl->m_dllHandle);
#elif IsOSAndroid()
		dlclose(m_dllHandle);
#endif
	}
	m_impl->m_dllHandle = nullptr;
	return true;
}

void IGfxApi::_create_adapter_list(void)
{
	// 1. Find physical devices.
	uint32_t nDeviceCount = 0;
	VKResultCheckFail(vkEnumeratePhysicalDevices(m_impl->m_vkInstance, &nDeviceCount, nullptr));
	PRINT_COND_ASSERT((nDeviceCount > 0), "GFX: Failed vkEnumeratePhysicalDevices()");

	Container::Vector< VkPhysicalDevice > vkPhysicalDevices(nDeviceCount);
	VKResultCheckFail(vkEnumeratePhysicalDevices(m_impl->m_vkInstance, &nDeviceCount, vkPhysicalDevices.data()));

	UINT nAdapter = 0;
	fxd_for(const auto& vkPhysicalDevice, vkPhysicalDevices)
	{
		VkPhysicalDeviceProperties vkProperties = {};
		VkPhysicalDeviceMemoryProperties vkMemoryProperties = {};
		vkGetPhysicalDeviceProperties(vkPhysicalDevice, &vkProperties);
		vkGetPhysicalDeviceMemoryProperties(vkPhysicalDevice, &vkMemoryProperties);

		GFX::E_DeviceType eDeviceType = GFX::E_DeviceType::Unknown;
		if (vkProperties.driverVersion == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU)
		{
			eDeviceType = GFX::E_DeviceType::IntegratedGPU;
		}
		else if (vkProperties.driverVersion == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
		{
			eDeviceType = GFX::E_DeviceType::DiscreteGPU;
		}
		else if (vkProperties.driverVersion == VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU)
		{
			eDeviceType = GFX::E_DeviceType::VirtualGPU;
		}

		GFX::GfxAdapterDesc desc = std::make_shared< GFX::IGfxAdapterDesc >(nAdapter);
		desc->driver(GFX::Api::ToString(E_GfxApi::Vulkan).c_str());
		desc->isDefault(nAdapter == 0);
		desc->name(vkProperties.deviceName);
		desc->deviceType(eDeviceType);
		desc->impl().vkPhysicalDevice(vkPhysicalDevice);
		desc->impl().vkMemoryProperties(vkMemoryProperties);

		m_adapterDescs[desc->name()] = desc;
		nAdapter++;
	}
}
#endif //IsGFXVulkan()