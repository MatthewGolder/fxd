// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/Vulkan/GfxAdapterVK.h"
#include "FXDEngine/GFX/Vulkan/GfxApiVK.h"
#include "FXDEngine/GFX/Vulkan/GPUMemoryVK.h"
#include "FXDEngine/GFX/Vulkan/ImageVK.h"
#include "FXDEngine/GFX/Vulkan/MappingVK.h"
#include "FXDEngine/GFX/Vulkan/SwapChainVK.h"

using namespace FXD;
using namespace GFX;

// ------
// VKImage
// -
// ------
namespace VKImage
{
	bool create_image_view(GFX::IGfxAdapter* pAdapter, GFX::IImage* pImage, GFX::IImageView* pImageView, GFX::IMAGEVIEW_DESC& imageViewDesc)
	{
		auto pGfxApi = static_cast< const GFX::IGfxApi* >(pAdapter->api());

		const GFX::IMAGE_DESC& imageDesc = pImage->imageDesc();
		const FXD::U32 nNumFaces = imageDesc.num_faces();

		VkImageView vkImageView = VK_NULL_HANDLE;

		// 1. Create a view for the image
		VkImageAspectFlags vkImageAspectFlags = 0;
		if (imageDesc.Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_RENDER_TARGET))
		{
			vkImageAspectFlags = VK_IMAGE_ASPECT_COLOR_BIT;
		}
		else if (imageDesc.Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL))
		{
			vkImageAspectFlags |= (VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT);
		}
		if (imageDesc.Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_SHADER_RESOURCE))
		{
			FXD_ERROR("");
		}

		// 2. Create a view for the image
		VkImageViewCreateInfo vkImageViewInfo = {};
		GFX::MemZeroVulkanStruct(vkImageViewInfo, VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO);

		vkImageViewInfo.pNext = nullptr;
		vkImageViewInfo.image = pImage->impl().vkImage();
		switch (imageDesc.Type)
		{
			case E_ImageType::One:
			{
				vkImageViewInfo.viewType = (nNumFaces > 1) ? VK_IMAGE_VIEW_TYPE_1D_ARRAY : VK_IMAGE_VIEW_TYPE_1D;
				break;
			}
			case E_ImageType::Two:
			{
				vkImageViewInfo.viewType = (nNumFaces > 1) ? VK_IMAGE_VIEW_TYPE_2D_ARRAY : VK_IMAGE_VIEW_TYPE_2D;
				break;
			}
			case E_ImageType::Three:
			{
				vkImageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_3D;
				break;
			}
			case E_ImageType::Cube:
			{
				if (nNumFaces == 1)
				{
					vkImageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
				}
				else if (nNumFaces % 6 == 0)
				{
					vkImageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
				}
				vkImageViewInfo.flags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
				break;
			}
			default:
			{
				PRINT_ASSERT << "GFX: Invalid image type for this view type";
				break;
			}
		}

		VkImageSubresourceRange vkImageSubresourceRange = {};
		Memory::MemZero_T(vkImageSubresourceRange);

		vkImageSubresourceRange.aspectMask = Pixel::GetAspectMask(imageDesc.Usage);
		if (imageDesc.Type == E_ImageType::Three)
		{
			vkImageSubresourceRange.baseMipLevel = imageViewDesc.MostDetailMip;
			vkImageSubresourceRange.levelCount = (imageViewDesc.NumMips == 0) ? VK_REMAINING_MIP_LEVELS : imageViewDesc.NumMips;
			vkImageSubresourceRange.baseArrayLayer = 0;
			vkImageSubresourceRange.layerCount = 1;
		}
		else
		{
			vkImageSubresourceRange.baseMipLevel = imageViewDesc.MostDetailMip;
			vkImageSubresourceRange.levelCount = (imageViewDesc.NumMips == 0) ? VK_REMAINING_MIP_LEVELS : imageViewDesc.NumMips;
			vkImageSubresourceRange.baseArrayLayer = imageViewDesc.FirstArraySlice;
			vkImageSubresourceRange.layerCount = (imageViewDesc.NumArraySlices == 0) ? VK_REMAINING_ARRAY_LAYERS : imageViewDesc.NumArraySlices;
		}

		vkImageViewInfo.format = Pixel::GetFormat(imageDesc.Format);
		vkImageViewInfo.components = Pixel::GetMapping(imageDesc.Format);
		vkImageViewInfo.subresourceRange = vkImageSubresourceRange;

		VKResultCheckFail(pAdapter->impl().vkCreateImageView(pAdapter->impl().vkDevice(), &vkImageViewInfo, &pGfxApi->impl().vkAllocCallbacks(), &vkImageView));

		if (imageDesc.Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_RENDER_TARGET))
		{
			pImageView->impl().vkImageViewRenderTarget(vkImageView);
		}
		else if (imageDesc.Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL))
		{
			pImageView->impl().vkImageViewDepth(vkImageView);
		}
		if (imageDesc.Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_SHADER_RESOURCE))
		{
			FXD_ERROR("");
		}

		return true;
	}

	bool createImage1D(GFX::IGfxAdapter* pAdapter, const GFX::IMAGE_DESC& imageDesc, VkImage& vkImage, VkDeviceMemory& vkDeviceMemory)
	{
		FXD_ERROR("");
		PRINT_COND_ASSERT((imageDesc.Width > 0), "GFX: Width is invalid");

		// 1. Create an optimal image used as the depth stencil attachment
		VkImageCreateInfo vkImageInfo = {};
		GFX::MemZeroVulkanStruct(vkImageInfo, VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO);

		vkImageInfo.pNext = nullptr;
		vkImageInfo.flags = 0;
		vkImageInfo.imageType = VK_IMAGE_TYPE_1D;
		if (imageDesc.Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_RENDER_TARGET))
		{
			//vkImageInfo.usage;
		}
		else if (imageDesc.Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL))
		{
			//vkImageInfo.usage;
		}
		else
		{
			//vkImageInfo.usage;
		}

		return true;
	}
	bool createImage2D(GFX::IGfxAdapter* pAdapter, const GFX::IMAGE_DESC& imageDesc, VkImage& vkImage, VkDeviceMemory& vkDeviceMemory)
	{
		PRINT_COND_ASSERT((imageDesc.Width > 0), "GFX: Width is invalid");
		PRINT_COND_ASSERT((imageDesc.Height > 0), "GFX: Height is invalid");

		auto pGfxApi = static_cast< const GFX::IGfxApi* >(pAdapter->api());

		// 1. Create an optimal image used as the depth stencil attachment
		VkImageCreateInfo vkImageInfo = {};
		GFX::MemZeroVulkanStruct(vkImageInfo, VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO);

		vkImageInfo.pNext = nullptr;
		vkImageInfo.flags = 0;
		vkImageInfo.imageType = VK_IMAGE_TYPE_2D;
		vkImageInfo.usage = (VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT);

		if (imageDesc.Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_RENDER_TARGET))
		{
			vkImageInfo.usage |= VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
		}
		else if (imageDesc.Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL))
		{
			vkImageInfo.usage |= VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
		}

		vkImageInfo.format = Pixel::GetFormat(imageDesc.Format);
		vkImageInfo.samples = ::Vulkan::GetSampleFlags(imageDesc.NumSamples);
		vkImageInfo.extent.width = imageDesc.Width;
		vkImageInfo.extent.height = imageDesc.Height;
		vkImageInfo.extent.depth = imageDesc.Depth;
		vkImageInfo.mipLevels = imageDesc.NumMips;
		vkImageInfo.arrayLayers = imageDesc.NumArraySlices;
		vkImageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
		vkImageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		vkImageInfo.queueFamilyIndexCount = 0;
		vkImageInfo.pQueueFamilyIndices = nullptr;
		vkImageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

		VKResultCheckFail(pAdapter->impl().vkCreateImage(pAdapter->impl().vkDevice(), &vkImageInfo, &pGfxApi->impl().vkAllocCallbacks(), &vkImage));

		// 2. Allocate memory for the image (device local) and bind it to our image
		VkMemoryPropertyFlags vkMemoryFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
		vkDeviceMemory = pAdapter->impl().allocate_memory(pAdapter, vkImage, vkMemoryFlags);

		return true;
	}
	bool createImage3D(GFX::IGfxAdapter* pAdapter, const GFX::IMAGE_DESC& imageDesc, VkImage& vkImage, VkDeviceMemory& vkDeviceMemory)
	{
		FXD_ERROR("");
		PRINT_COND_ASSERT((imageDesc.Width > 0), "GFX: Width is invalid");
		PRINT_COND_ASSERT((imageDesc.Height > 0), "GFX: Height is invalid");
		PRINT_COND_ASSERT((imageDesc.Depth > 0), "GFX: Depth is invalid");

		// 1. Create an optimal image used as the depth stencil attachment
		VkImageCreateInfo vkImageInfo = {};
		GFX::MemZeroVulkanStruct(vkImageInfo, VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO);

		vkImageInfo.pNext = nullptr;
		vkImageInfo.flags = 0;
		vkImageInfo.imageType = VK_IMAGE_TYPE_3D;
		//vkImageInfo.usage |= VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;

		/*
		vkImageInfo.format = Pixel::GetFormat(imageDesc.Format);
		vkImageInfo.extent.width = imageDesc.Width;
		vkImageInfo.extent.height = imageDesc.Height;
		vkImageInfo.extent.depth = imageDesc.Depth;
		vkImageInfo.mipLevels = imageDesc.NumMips;
		vkImageInfo.arrayLayers = imageDesc.NumArraySlices;
		vkImageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
		vkImageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
		vkImageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		vkImageInfo.queueFamilyIndexCount = 0;
		vkImageInfo.pQueueFamilyIndices = nullptr;
		vkImageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		//VKResultCheckFail(pAdapter->impl().vkCreateImage(pAdapter->impl().vkDevice(), &vkImageInfo, &pGfxApi->impl().vkAllocCallbacks(), &vkImage));
		*/
		
		// 2. Allocate memory for the image (device local) and bind it to our image
		VkMemoryPropertyFlags vkMemoryFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
		vkDeviceMemory = pAdapter->impl().allocate_memory(pAdapter, vkImage, vkMemoryFlags);

		return true;
	}
	bool createImageCube(GFX::IGfxAdapter* pAdapter, const GFX::IMAGE_DESC& imageDesc, VkImage& vkImage, VkDeviceMemory& vkDeviceMemory)
	{
		FXD_ERROR("");
		PRINT_COND_ASSERT((imageDesc.Width > 0), "GFX: Width is invalid");
		PRINT_COND_ASSERT((imageDesc.Height > 0), "GFX: Height is invalid");
		PRINT_COND_ASSERT((imageDesc.Depth > 0), "GFX: Depth is invalid");

		// 1. Create an optimal image used as the depth stencil attachment
		VkImageCreateInfo vkImageInfo = {};
		GFX::MemZeroVulkanStruct(vkImageInfo, VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO);

		vkImageInfo.pNext = nullptr;
		vkImageInfo.flags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
		vkImageInfo.imageType = VK_IMAGE_TYPE_2D;
		//vkImageInfo.usage |= VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
		
		return true;
	}
	bool create_image(GFX::IGfxAdapter* pAdapter, GFX::IImage* pImage, const GFX::IMAGE_DESC& imageDesc)
	{
		VkImage vkImage = VK_NULL_HANDLE;
		VkDeviceMemory vkDeviceMemory = VK_NULL_HANDLE;
		if (imageDesc.Type == GFX::E_ImageType::One)
		{
			::VKImage::createImage1D(pAdapter, imageDesc, vkImage, vkDeviceMemory);
		}
		else if (imageDesc.Type == GFX::E_ImageType::Two)
		{
			::VKImage::createImage2D(pAdapter, imageDesc, vkImage, vkDeviceMemory);
		}
		else if (imageDesc.Type == GFX::E_ImageType::Three)
		{
			::VKImage::createImage3D(pAdapter, imageDesc, vkImage, vkDeviceMemory);
		}
		else if (imageDesc.Type == GFX::E_ImageType::Cube)
		{
			::VKImage::createImageCube(pAdapter, imageDesc, vkImage, vkDeviceMemory);
		}
		pImage->impl().vkImage(vkImage);
		pImage->impl().vkDeviceMemory(vkDeviceMemory);

		return true;
	}
} //namespace VKImage

// ------
// IImageView::Impl::Impl
// - 
// ------
IImageView::Impl::Impl::Impl(void)
	: m_vkImageViewRenderTarget(VK_NULL_HANDLE)
	, m_vkImageViewDepth(VK_NULL_HANDLE)
	, m_vkImageViewShader(VK_NULL_HANDLE)
{
}

IImageView::Impl::~Impl(void)
{
	PRINT_COND_ASSERT((m_vkImageViewRenderTarget == VK_NULL_HANDLE), "GFX: m_vkImageViewRenderTarget is not shutdown");
	PRINT_COND_ASSERT((m_vkImageViewDepth == VK_NULL_HANDLE), "GFX: m_vkImageViewDepth is not shutdown");
	PRINT_COND_ASSERT((m_vkImageViewShader == VK_NULL_HANDLE), "GFX: m_vkImageViewShader is not shutdown");
}

// ------
// IImageView
// -
// ------
IImageView::IImageView(GFX::IGfxAdapter* pAdapter, GFX::IImage* pImage)
	: m_pAdapter(pAdapter)
	, m_pImage(pImage)
{
}

IImageView::~IImageView(void)
{
}

bool IImageView::_create(const GFX::IMAGEVIEW_DESC& imageViewDesc)
{
	// 
	m_imageViewDesc = imageViewDesc;
	::VKImage::create_image_view(m_pAdapter, m_pImage, this, m_imageViewDesc);
	return true;
}

bool IImageView::_release(void)
{
	auto pGfxApi = static_cast< const GFX::IGfxApi* >(m_pAdapter->api());

	m_imageViewDesc = IMAGEVIEW_DESC();

	if (m_impl->m_vkImageViewRenderTarget != VK_NULL_HANDLE)
	{
		m_pAdapter->impl().vkDestroyImageView(m_pAdapter->impl().vkDevice(), m_impl->m_vkImageViewRenderTarget, &pGfxApi->impl().vkAllocCallbacks());
		m_impl->m_vkImageViewRenderTarget = VK_NULL_HANDLE;
	}
	if (m_impl->m_vkImageViewDepth != VK_NULL_HANDLE)
	{
		m_pAdapter->impl().vkDestroyImageView(m_pAdapter->impl().vkDevice(), m_impl->m_vkImageViewDepth, &pGfxApi->impl().vkAllocCallbacks());
		m_impl->m_vkImageViewDepth = VK_NULL_HANDLE;
	}
	if (m_impl->m_vkImageViewShader != VK_NULL_HANDLE)
	{
		m_pAdapter->impl().vkDestroyImageView(m_pAdapter->impl().vkDevice(), m_impl->m_vkImageViewShader, &pGfxApi->impl().vkAllocCallbacks());
		m_impl->m_vkImageViewShader = VK_NULL_HANDLE;
	}
	return true;
}


// ------
// IImage::Impl::Impl
// - 
// ------
IImage::Impl::Impl::Impl(void)
	: m_vkImage(VK_NULL_HANDLE)
	, m_vkDeviceMemory(VK_NULL_HANDLE)
	, m_bSwapChain(false)
{
}

IImage::Impl::~Impl(void)
{
	FXD_ERROR("");
	PRINT_COND_ASSERT((m_vkImage == VK_NULL_HANDLE), "GFX: m_vkImage is not shutdown");
	PRINT_COND_ASSERT((m_vkDeviceMemory == VK_NULL_HANDLE), "GFX: m_vkDeviceMemory is not shutdown");
}

// ------
// IImage
// -
// ------
IImage::IImage(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IImage::~IImage(void)
{
}

bool IImage::_create(const GFX::IMAGE_DESC& imageDesc, void* pSrc)
{
	PRINT_COND_ASSERT((pSrc == nullptr), "GFX: Failed _create()");

	m_imageDesc = imageDesc;
	::VKImage::create_image(m_pAdapter, this, m_imageDesc);
	return true;
}

bool IImage::_create(const GFX::SwapChain& swapChain, FXD::U32 nBuffer)
{
	auto pAdapterDesc = static_cast<const GFX::IGfxAdapterDesc*>(m_pAdapter->adapter_desc().get());
	VkSurfaceKHR vkSurface = swapChain->impl().vkSurface();
	VkPhysicalDevice vkPhysicalDevice = pAdapterDesc->impl().vkPhysicalDevice();

	VkSurfaceCapabilitiesKHR vkSurfaceCaps = {};
	VKResultCheckFail(m_pAdapter->impl().vkGetPhysicalDeviceSurfaceCapabilitiesKHR(vkPhysicalDevice, vkSurface, &vkSurfaceCaps));

	m_imageDesc.Width = vkSurfaceCaps.currentExtent.width;
	m_imageDesc.Height = vkSurfaceCaps.currentExtent.height;
	m_imageDesc.Type = GFX::E_ImageType::Two;
	m_imageDesc.Format = (swapChain)->color_format();
	m_imageDesc.Usage = (GFX::E_BufferUsageFlag::BIND_RENDER_TARGET);
	m_impl->bSwapChain(true);

	PRINT_COND_ASSERT((swapChain->impl().nImageCount() > nBuffer), "GFX: Failed vkGetSwapchainImagesKHR()");
	m_impl->vkImage(swapChain->impl().m_vkImages[nBuffer]);

	return true;
}

bool IImage::_release(void)
{
	m_imageDesc = IMAGE_DESC();

	auto pGfxApi = static_cast< const GFX::IGfxApi* >(m_pAdapter->api());
	if ((m_impl->m_vkImage != VK_NULL_HANDLE) && (!m_impl->m_bSwapChain))
	{
		m_pAdapter->impl().vkDestroyImage(m_pAdapter->impl().vkDevice(), m_impl->m_vkImage, &pGfxApi->impl().vkAllocCallbacks());
		m_impl->m_vkImage = VK_NULL_HANDLE;
		m_impl->m_bSwapChain = false;
	}
	if (m_impl->m_vkDeviceMemory != VK_NULL_HANDLE)
	{
		m_pAdapter->impl().vkFreeMemory(m_pAdapter->impl().vkDevice(), m_impl->m_vkDeviceMemory, &pGfxApi->impl().vkAllocCallbacks());
		m_impl->m_vkDeviceMemory = VK_NULL_HANDLE;
	}
	return true;
}
#endif //IsGFXVulkan()