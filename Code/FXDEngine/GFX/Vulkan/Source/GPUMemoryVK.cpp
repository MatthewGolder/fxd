// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/Vulkan/GPUMemoryVK.h"

using namespace FXD;
using namespace GFX;

// ------
// HeapPolicyVK
// - 
// ------
HeapPolicyVK::HeapPolicyVK(void)
{
}
HeapPolicyVK::~HeapPolicyVK(void)
{
}

void HeapPolicyVK::increment_ref(void* pData)
{
	((VKMemHdr*)pData)->add_ref();
}

void HeapPolicyVK::release_ref(void* pData)
{
	((VKMemHdr*)pData)->release();
}

void* HeapPolicyVK::lock(void* /*pData*/)
{
	PRINT_WARN << "This memory block cannot be locked";
	return nullptr;
}

void HeapPolicyVK::unlock(void* /*pData*/)
{
	PRINT_WARN << "This memory block is not locked";
}

VkDeviceMemory HeapPolicyVK::getVKDeviceMem(const Memory::MemHandle& mh)
{
	return ((VKMemHdr*)mh.get_data())->m_vkMem;
}

// ------
// VKMemHdr
// - 
// ------
VKMemHdr::VKMemHdr(VkDeviceMemory vkMem, GFX::IGfxAdapter* pAdapter)
	: m_vkMem(vkMem)
	, m_pAdapter(pAdapter)
	, m_ref(0)
{
}
VKMemHdr::~VKMemHdr(void)
{
}

void VKMemHdr::add_ref(void)
{
	m_ref.increment();
}

void VKMemHdr::release(void)
{
	if (!m_ref.decrement())
	{
		//destroyMyself();
		PRINT_ASSERT << "VKMemHdr::release";
	}
}

// ------
// MemoryHeapSpec::Impl::Impl
// - 
// ------
MemoryHeapSpec::Impl::Impl::Impl(void)
	: m_nMemTypeIdx((FXD::U32)-1)
{
}
MemoryHeapSpec::Impl::~Impl(void)
{
}

// ------
// MemoryHeapSpec
// - 
// ------
MemoryHeapSpec::MemoryHeapSpec(void)
{
}
MemoryHeapSpec::MemoryHeapSpec(const Impl& i)
	: HasImpl(i)
{
}
MemoryHeapSpec::~MemoryHeapSpec(void)
{
}

// ------
// ResourceSet::Impl::Impl
// - 
// ------
ResourceSet::Impl::Impl::Impl(void)
	: m_nMemTypeBits(0)
	, m_nBoundMemoryOffset(0)
{
}
ResourceSet::Impl::~Impl(void)
{
}

AllocSpec ResourceSet::get_allocation_spec(void) const
{
	return m_impl->m_allocInfo;
}

FXD::U32 ResourceSet::resource_count(void) const
{
	return m_impl->m_resources.size();
}

FXD::U32 ResourceSet::resource_memory_offset(FXD::U32 nResourceID) const
{
	return m_impl->m_nBoundMemoryOffset + m_impl->m_resources[nResourceID].m_nByteOffset;
}

// ------
// ResourceSet
// - 
// ------
ResourceSet::ResourceSet(void)
{
}
ResourceSet::~ResourceSet(void)
{
}
#endif //IsGFXVulkan()