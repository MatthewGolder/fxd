// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/Window.h"
#include "FXDEngine/GFX/Vulkan/GfxAdapterVK.h"
#include "FXDEngine/GFX/Vulkan/GfxApiVK.h"
#include "FXDEngine/GFX/Vulkan/ImageVK.h"
#include "FXDEngine/GFX/Vulkan/MappingVK.h"
#include "FXDEngine/GFX/Vulkan/RenderTargetVK.h"
#include "FXDEngine/GFX/Vulkan/SwapChainVK.h"
#include "FXDEngine/GFX/Vulkan/ViewportVK.h"
#include <vector>

using namespace FXD;
using namespace GFX;

CONSTEXPR GFX::E_PixelFormat vkRTFormats[] =
{
	GFX::E_PixelFormat::RGBA8,
	GFX::E_PixelFormat::BGRA8
};
CONSTEXPR GFX::E_PixelFormat vkDepthFormats[] =
{
	GFX::E_PixelFormat::D24_S8,
	GFX::E_PixelFormat::D32,
	GFX::E_PixelFormat::D16
};
CONSTEXPR VkCompositeAlphaFlagBitsKHR vkCompositeAlphaFlags[] =
{
	VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
	VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR,
	VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR,
	VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR,
};

// ------
// VKSwap
// -
// ------
namespace VKSwap
{
	bool GetSupportedDepthFormat(const VkPhysicalDevice& vkPhysicalDevice, GFX::E_PixelFormat& eDepthFormat)
	{
		// Since all depth formats may be optional, we need to find a suitable depth format to use
		for (const auto vkFormat : vkDepthFormats)
		{
			VkFormatProperties vkFormatProperties = {};
			vkGetPhysicalDeviceFormatProperties(vkPhysicalDevice, Pixel::GetFormat(vkFormat), &vkFormatProperties);
			if (vkFormatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT)
			{
				eDepthFormat = vkFormat;
				return true;
			}
		}
		return false;
	}
	bool GetSupportedSurfaceFormat(GFX::IGfxAdapter* pAdapter, const VkPhysicalDevice vkPhysicalDevice, const VkSurfaceKHR& vkSurface, GFX::E_PixelFormat& eColorFormat, VkColorSpaceKHR& vkColorSpace)
	{
		PRINT_COND_ASSERT((vkSurface != VK_NULL_HANDLE), "GFX: vkSurface should not be VK_NULL_HANDLE");

		// 1.1. Get number of supported surface formats
		uint32_t nFormatCount = 0;
		VKResultCheckFail(pAdapter->impl().vkGetPhysicalDeviceSurfaceFormatsKHR(vkPhysicalDevice, vkSurface, &nFormatCount, nullptr));
		PRINT_COND_ASSERT((nFormatCount > 0), "GFX: Failed vkGetPhysicalDeviceSurfaceFormatsKHR()");

		// 1.2. Get list of supported surface formats
		Container::Vector< VkSurfaceFormatKHR > vkSurfaceFormats(nFormatCount);
		VKResultCheckFail(pAdapter->impl().vkGetPhysicalDeviceSurfaceFormatsKHR(vkPhysicalDevice, vkSurface, &nFormatCount, vkSurfaceFormats.data()));

		// 2. Since all depth formats may be optional, we need to find a suitable depth format to use, Start with the highest precision packed format
		for (const auto vkRTFormat : vkRTFormats)
		{
			for (const auto& vkSurfaceFormat : vkSurfaceFormats)
			{
				if (vkSurfaceFormat.format == Pixel::GetFormat(vkRTFormat))
				{
					eColorFormat = vkRTFormat;
					vkColorSpace = vkSurfaceFormat.colorSpace;
					return true;
				}
			}
		}
		return false;
	}
	void SetupSwapChainParams(GFX::IGfxAdapter* pAdapter, const GFX::IViewport* pViewport, const GFX::E_PixelFormat eColorFormat, const GFX::E_PixelFormat eDepthFormat, const uint32_t nPresentQueueFamily, const VkColorSpaceKHR& vkColorSpace, const VkSurfaceKHR& vkSurface, VkSwapchainCreateInfoKHR& outVKSwapChainInfo)
	{
		auto pAdapterDesc = static_cast< const GFX::IGfxAdapterDesc* >(pAdapter->adapter_desc().get());
		auto pGfxApi = static_cast< const GFX::IGfxApi* >(pAdapter->api());
		VkPhysicalDevice vkPhysicalDevice = pAdapterDesc->impl().vkPhysicalDevice();

		// 1.1. Get the number of vk present mode
		uint32_t nPresentModeCount = 0;
		VKResultCheckFail(pAdapter->impl().vkGetPhysicalDeviceSurfacePresentModesKHR(vkPhysicalDevice, vkSurface, &nPresentModeCount, nullptr));

		// 1.2. Get the vk present mode
		Container::Vector< VkPresentModeKHR > vkPresentModeKHRs(nPresentModeCount);
		VKResultCheckFail(pAdapter->impl().vkGetPhysicalDeviceSurfacePresentModesKHR(vkPhysicalDevice, vkSurface, &nPresentModeCount, vkPresentModeKHRs.data()));

		VkPresentModeKHR vkPresentModeKHR = VK_PRESENT_MODE_FIFO_KHR;
		if (true)// !vsync)
		{
			fxd_for(const auto& itr, vkPresentModeKHRs)
			{
				if (itr == VK_PRESENT_MODE_IMMEDIATE_KHR)
				{
					vkPresentModeKHR = VK_PRESENT_MODE_IMMEDIATE_KHR;
					break;
				}
				if ((vkPresentModeKHR != VK_PRESENT_MODE_MAILBOX_KHR) && (itr == VK_PRESENT_MODE_IMMEDIATE_KHR))
				{
					vkPresentModeKHR = VK_PRESENT_MODE_FIFO_RELAXED_KHR;
				}
			}
		}

		// 1.3. Get the vk transform flag
		VkSurfaceCapabilitiesKHR vkSurfaceCapabilities = {};
		VKResultCheckFail(pAdapter->impl().vkGetPhysicalDeviceSurfaceCapabilitiesKHR(vkPhysicalDevice, vkSurface, &vkSurfaceCapabilities));

		VkSurfaceTransformFlagBitsKHR vkSurfaceTransformFlagBitsKHR;
		if (vkSurfaceCapabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
		{
			vkSurfaceTransformFlagBitsKHR = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
		}
		else
		{
			vkSurfaceTransformFlagBitsKHR = vkSurfaceCapabilities.currentTransform;
		}


		// 1.4 Get the vk alpha flag
		VkCompositeAlphaFlagBitsKHR vkCompositeAlphaFlagBitsKHR = VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR;
		fxd_for(const auto vkCompositeAlphaFlag, vkCompositeAlphaFlags)
		{
			if (vkSurfaceCapabilities.supportedCompositeAlpha & vkCompositeAlphaFlag)
			{
				vkCompositeAlphaFlagBitsKHR = vkCompositeAlphaFlag;
				break;
			};
		}

		// 2. Create the swap chain
		VkSwapchainCreateInfoKHR vkSwapChainInfo = {};
		GFX::MemZeroVulkanStruct(vkSwapChainInfo, VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR);

		vkSwapChainInfo.surface = vkSurface;
		vkSwapChainInfo.minImageCount = vkSurfaceCapabilities.minImageCount;
		vkSwapChainInfo.imageFormat = Pixel::GetFormat(eColorFormat);
		vkSwapChainInfo.imageColorSpace = vkColorSpace;
		vkSwapChainInfo.imageExtent.width = (uint32_t)pViewport->size().x();
		vkSwapChainInfo.imageExtent.height = (uint32_t)pViewport->size().y();
		vkSwapChainInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
		vkSwapChainInfo.preTransform = vkSurfaceTransformFlagBitsKHR;
		vkSwapChainInfo.imageArrayLayers = 1;
		vkSwapChainInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		vkSwapChainInfo.queueFamilyIndexCount = 0;
		vkSwapChainInfo.pQueueFamilyIndices = nullptr;
		vkSwapChainInfo.presentMode = vkPresentModeKHR;
		vkSwapChainInfo.oldSwapchain = VK_NULL_HANDLE;
		vkSwapChainInfo.clipped = VK_TRUE;
		vkSwapChainInfo.compositeAlpha = vkCompositeAlphaFlagBitsKHR;
		outVKSwapChainInfo = vkSwapChainInfo;
	}
} //namespace VKSwap

// ------
// ISwapChain::Impl::Impl
// - 
// ------
ISwapChain::Impl::Impl::Impl(void)
	: m_nGraphicsQueueFamily(UINT32_MAX)
	, m_vkColorSpace(VK_COLOR_SPACE_MAX_ENUM_KHR)
	, m_vkSurface(VK_NULL_HANDLE)
	, m_vkSwapChain(VK_NULL_HANDLE)
	, m_nImageCount(0)
{
	Memory::MemZero(&m_vkImages[0], (GFX::kMaxMultipleRenderTargets * sizeof(VkImage)));
}

ISwapChain::Impl::~Impl(void)
{
	PRINT_COND_ASSERT((m_vkColorSpace == VK_COLOR_SPACE_MAX_ENUM_KHR), "GFX: m_vkColorSpace is not shutdown");
	PRINT_COND_ASSERT((m_vkSurface == VK_NULL_HANDLE), "GFX: m_vkSurface is not shutdown");
	PRINT_COND_ASSERT((m_vkSwapChain == VK_NULL_HANDLE), "GFX: m_vkSwapChain is not shutdown");
}

// ------
// ISwapChain
// -
// ------
ISwapChain::ISwapChain(GFX::IGfxAdapter* pAdapter, GFX::IViewport* pViewport)
	: m_pAdapter(pAdapter)
	, m_pViewport(pViewport)
	, m_eColorFormat(GFX::E_PixelFormat::Unknown)
	, m_eDepthFormat(GFX::E_PixelFormat::Unknown)
{
}

ISwapChain::~ISwapChain(void)
{
	PRINT_COND_ASSERT(!m_renderTarget, "GFX: m_renderTarget is not shutdown");
}

bool ISwapChain::_create(void)
{
	auto pAdapterDesc = static_cast< const GFX::IGfxAdapterDesc* >(m_pAdapter->adapter_desc().get());
	auto pGfxApi = static_cast< const GFX::IGfxApi* >(m_pAdapter->api());
	VkInstance vkInstance = pGfxApi->impl().vkInstance();
	VkDevice vkDevice = m_pAdapter->impl().vkDevice();
	VkPhysicalDevice vkPhysicalDevice = pAdapterDesc->impl().vkPhysicalDevice();

	bool bRetVal = true;

	// 1. Create the surface
#if IsOSPC()
	VkWin32SurfaceCreateInfoKHR vkSurfaceeInfo = {};
	GFX::MemZeroVulkanStruct(vkSurfaceeInfo, VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR);

	vkSurfaceeInfo.hinstance = (HINSTANCE)GetModuleHandleW(nullptr);
	vkSurfaceeInfo.hwnd = (HWND)m_pViewport->window()->windowHandle();
	VKResultCheckFail(m_pAdapter->impl().vkCreateWin32SurfaceKHR(vkInstance, &vkSurfaceeInfo, &pGfxApi->impl().vkAllocCallbacks(), &m_impl->m_vkSurface));
#elif IsOSAndroid()
	VkAndroidSurfaceCreateInfoKHR vkSurfaceeInfo = {};
	GFX::MemZeroVulkanStruct(vkSurfaceeInfo, VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR);

	vkSurfaceeInfo.window = window;
	VKResultCheckFail(m_pAdapter->impl().vkCreateAndroidSurfaceKHR(vkInstance, &vkSurfaceeInfo, &pGfxApi->impl().vkAllocCallbacks(), &m_impl->m_vkSurface));
#endif


	// 2. Check the surface
	m_impl->m_nGraphicsQueueFamily = m_pAdapter->impl().get_queue_family(GFX::E_GfxQueueType::Graphics);
	VkBool32 bSupportsPresent = VK_FALSE;
	m_pAdapter->impl().vkGetPhysicalDeviceSurfaceSupportKHR(vkPhysicalDevice, m_impl->m_nGraphicsQueueFamily, m_impl->m_vkSurface, &bSupportsPresent);
	PRINT_COND_ASSERT(bSupportsPresent, "GFX: Cannot find a graphics queue that also supports present operations");


	// 3. Get the supported surface formats
	PRINT_COND_ASSERT(VKSwap::GetSupportedDepthFormat(vkPhysicalDevice, m_eDepthFormat), "GFX: Failed getSupportedDepthFormat()");
	PRINT_COND_ASSERT(VKSwap::GetSupportedSurfaceFormat(m_pAdapter, vkPhysicalDevice, m_impl->m_vkSurface, m_eColorFormat, m_impl->m_vkColorSpace), "GFX: Failed getSupportedSurfaceFormat()");


	// 4. Set up the surface parameters
	VkSwapchainCreateInfoKHR vkSwapChainInfo = {};
	Memory::MemZero_T(vkSwapChainInfo);
	VKSwap::SetupSwapChainParams(m_pAdapter, m_pViewport, m_eColorFormat, m_eDepthFormat, m_impl->m_nGraphicsQueueFamily, m_impl->m_vkColorSpace, m_impl->m_vkSurface, vkSwapChainInfo);
	VKResultCheckFail(m_pAdapter->impl().vkCreateSwapchainKHR(vkDevice, &vkSwapChainInfo, &pGfxApi->impl().vkAllocCallbacks(), &m_impl->m_vkSwapChain));


	// 5. Create the swapchain
	VKResultCheckFail(m_pAdapter->impl().vkGetSwapchainImagesKHR(vkDevice, m_impl->m_vkSwapChain, &m_impl->m_nImageCount, nullptr));
	PRINT_COND_ASSERT((m_impl->m_nImageCount > 0), "GFX: Failed vkGetSwapchainImagesKHR()");
	m_pAdapter->impl().vkGetSwapchainImagesKHR(vkDevice, m_impl->m_vkSwapChain, &m_impl->m_nImageCount, m_impl->m_vkImages);


	// 6. Create the images
	GFX::Image imgColours[GFX::kMaxMultipleRenderTargets];
	GFX::Image imgDepth = nullptr;
	for (uint32_t n1 = 0; n1 < m_impl->m_nImageCount; n1++)
	{
		imgColours[n1] = m_pAdapter->create_image(shared_from_this(), n1).get();
		PRINT_COND_ASSERT(imgColours[n1], "GFX: Failed imgColours->create_image()");
	}

	if (m_pViewport->window()->window_desc().bDepthBuffer)
	{
		GFX::IMAGE_DESC imgDesc = {};
		imgDesc.Type = GFX::E_ImageType::Two;
		imgDesc.Usage = GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL;
		imgDesc.Format = m_eDepthFormat;
		imgDesc.Width = m_pViewport->size().x();
		imgDesc.Height = m_pViewport->size().y();
		imgDepth = m_pAdapter->create_image(imgDesc).get();
		PRINT_COND_ASSERT(imgDepth, "GFX: Failed m_imgDepth->create_image()");
	}

	// 7. Create the render target
	GFX::RENDER_TEXTURE_DESC rtDesc = {};
	for (uint32_t n1 = 0; n1 < m_impl->m_nImageCount; n1++)
	{
		rtDesc.ColorSurfaces[n1].Image = imgColours[n1];
		rtDesc.ColorSurfaces[n1].Face = 0;
		rtDesc.ColorSurfaces[n1].NumFaces = imgColours[n1]->imageDesc().num_faces();
		rtDesc.ColorSurfaces[n1].MipLevel = 0;
	}
	if (m_pViewport->window()->window_desc().bDepthBuffer)
	{
		rtDesc.DepthStencilSurface.Image = imgDepth;
		rtDesc.DepthStencilSurface.Face = 0;
		rtDesc.DepthStencilSurface.NumFaces = imgDepth->imageDesc().num_faces();
		rtDesc.DepthStencilSurface.MipLevel = 0;
	}
	m_renderTarget = m_pAdapter->create_render_target(rtDesc).get();

	return bRetVal;
}

bool ISwapChain::_release(void)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	FXD_ERROR("vulkan ISwapChain::_release");

	m_impl->m_nGraphicsQueueFamily = UINT32_MAX;
	m_impl->m_vkColorSpace = VK_COLOR_SPACE_MAX_ENUM_KHR;
	m_impl->m_vkSurface = VK_NULL_HANDLE;
	m_impl->m_vkSwapChain = VK_NULL_HANDLE;
	return true;
}

bool ISwapChain::_resize(const Math::Vector2I newSize)
{
	return true;
}

void ISwapChain::_swap(bool bVSync)
{
	VkPresentInfoKHR vkPresentInfo = {};
	GFX::MemZeroVulkanStruct(vkPresentInfo, VK_STRUCTURE_TYPE_PRESENT_INFO_KHR);
	vkPresentInfo.pWaitSemaphores = VK_NULL_HANDLE;
	vkPresentInfo.waitSemaphoreCount = 0;
	vkPresentInfo.pNext = nullptr;
	vkPresentInfo.swapchainCount = 1;
	vkPresentInfo.pSwapchains = &m_impl->m_vkSwapChain;
	//vkPresentInfo.pImageIndices = &m_impl->m_nCurrentBuffer;

	VkQueue vkQueue = m_pAdapter->impl().get_queue(GFX::E_GfxQueueType::Graphics, 0);
	VKResultCheckFail(m_pAdapter->impl().vkQueuePresentKHR(vkQueue, &vkPresentInfo));
}
#endif //IsGFXVulkan()