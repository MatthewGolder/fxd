// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/Vulkan/MappingVK.h"
#include "FXDEngine/GFX/Vulkan/GfxAdapterVK.h"

using namespace FXD;
using namespace GFX;

// ------
// Vulkan
// -
// ------
bool FXD::GFX::Vulkan::CheckError(void)
{
	return false;
}

bool FXD::GFX::Vulkan::CheckExtension(const GFX::IGfxAdapter* pAdapter, const FXD::UTF8* pExtension)
{
	fxd_for(const auto& vkExtProperty, pAdapter->impl().vkExtensionProperties())
	{
		if (Core::CharTraits< FXD::UTF8 >::compare_no_case(vkExtProperty.extensionName, pExtension) == 0)
		{
			return true;
		}
	}
	return false;
}

void FXD::GFX::Vulkan::PrintExtension(const GFX::IGfxAdapter* pAdapter)
{
	fxd_for(const auto& vkExtProperty, pAdapter->impl().vkExtensionProperties())
	{
		PRINT_INFO << vkExtProperty.extensionName;
	}
}

bool FXD::GFX::Vulkan::SupportsBCTextureFormats(void)
{
	return !IsOSAndroid();
}

bool FXD::GFX::Vulkan::SupportsASTCTextureFormats(void)
{
	return IsOSAndroid();
}

CONSTEXPR const VkPrimitiveTopology sVKPrimitives[] =
{
	VK_PRIMITIVE_TOPOLOGY_LINE_LIST,			// E_Primitive::LineList
	VK_PRIMITIVE_TOPOLOGY_LINE_STRIP,		// E_Primitive::LineStrip
	VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP,	// E_Primitive::TriStrip
	VK_PRIMITIVE_TOPOLOGY_POINT_LIST,		// E_Primitive::PointList
	VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,	// E_Primitive::TriList
	VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN,		// E_Primitive::TriFan
	VK_PRIMITIVE_TOPOLOGY_MAX_ENUM,			// E_Primitive::QuadList
	VK_PRIMITIVE_TOPOLOGY_MAX_ENUM			// E_Primitive::Count
};
VkPrimitiveTopology FXD::GFX::Vulkan::GetPrimitiveType(GFX::E_Primitive eType)
{
	return kVKPrimitives[FXD::STD::to_underlying(eType)];
}

CONSTEXPR const VkPolygonMode kVKFillMode[] =
{
	VK_POLYGON_MODE_POINT,	// E_FillMode::Point
	VK_POLYGON_MODE_LINE,	// E_FillMode::WireFrame
	VK_POLYGON_MODE_FILL,	// E_FillMode::Solid
	VK_POLYGON_MODE_FILL		// E_FillMode::Count
};
VkPolygonMode FXD::GFX::Vulkan::GetFillMode(GFX::E_FillMode eMode)
{
	return kVKFillMode[FXD::STD::to_underlying(eMode)];
}

CONSTEXPR const VkCullModeFlagBits kVKCullMode[] =
{
	VK_CULL_MODE_NONE,		// E_CullMode::None
	VK_CULL_MODE_FRONT_BIT,	// E_CullMode::Clockwise
	VK_CULL_MODE_BACK_BIT,	// E_CullMode::CounterClockwise
	VK_CULL_MODE_NONE			// E_CullMode::Count
};
VkCullModeFlagBits FXD::GFX::Vulkan::GetCullMode(GFX::E_CullMode eMode)
{
	return kVKCullMode[FXD::STD::to_underlying(eMode)];
}

VkSampleCountFlagBits FXD::GFX::Vulkan::GetSampleFlags(FXD::U16 nNumSamples)
{
	switch (nNumSamples)
	{
		case 0:
		case 1:
		{
			return VK_SAMPLE_COUNT_1_BIT;
		}
		case 2:
		{	
			return VK_SAMPLE_COUNT_2_BIT;
		}
		case 4:
		{
			return VK_SAMPLE_COUNT_4_BIT;
		}
		case 8:
		{	
			return VK_SAMPLE_COUNT_8_BIT;
		}
		case 16:
		{
			return VK_SAMPLE_COUNT_16_BIT;
		}
		case 32:
		{
			return VK_SAMPLE_COUNT_32_BIT;
		}
		case 64:
		{
			return VK_SAMPLE_COUNT_64_BIT;
		}
	}
	return VK_SAMPLE_COUNT_1_BIT;
}


// ------
// Buffer
// -
// ------
const VkImageUsageFlagBits FXD::GFX::Buffer::ConvertToVKImageUsageBits(GFX::E_BufferUsageFlags eFlags)
{
	PRINT_COND_ASSERT((eFlags & (GFX::E_BufferUsageFlag::BIND_RENDER_TARGET | GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL)) != (GFX::E_BufferUsageFlag::BIND_RENDER_TARGET | GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL), "RenderTarget and DepthStencil can't be requested together!");

	return VkImageUsageFlagBits(
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL) ? VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT : 0) |
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_RENDER_TARGET) ? VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT : 0) |
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_SHADER_RESOURCE) ? VK_IMAGE_USAGE_SAMPLED_BIT : 0)
	);
}

const VkBufferUsageFlagBits FXD::GFX::Buffer::ConvertToVKBufferUsageBits(GFX::E_BufferUsageFlags eFlags)
{
	PRINT_COND_ASSERT((eFlags & (GFX::E_BufferUsageFlag::BIND_RENDER_TARGET | GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL)) != (GFX::E_BufferUsageFlag::BIND_RENDER_TARGET | GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL), "RenderTarget and DepthStencil can't be requested together!");

	return VkBufferUsageFlagBits(
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_VERTEX_BUFFER) ? VK_BUFFER_USAGE_VERTEX_BUFFER_BIT : 0) |
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_INDEX_BUFFER) ? VK_BUFFER_USAGE_INDEX_BUFFER_BIT : 0) |
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_UNIFORM_BUFFER) ? VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT : 0) |
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_SHADER_RESOURCE) ? VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT | VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT : 0)
	);
}


// ------
// Pixel
// -
// ------
struct VkPixelFormat
{
	const VkFormat format;
	const VkComponentMapping map;
};
CONSTEXPR const VkPixelFormat kVKPixelFormats[] =
{
	{ VK_FORMAT_R8_UNORM,						{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },		// E_PixelFormat::R8
	{ VK_FORMAT_R8_UINT,							{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },		// E_PixelFormat::R8U
	{ VK_FORMAT_R8_SNORM,						{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },		// E_PixelFormat::R8S
	{ VK_FORMAT_R8_SINT,							{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },		// E_PixelFormat::R8I

	{ VK_FORMAT_R8G8_UNORM,						{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },			// E_PixelFormat::RG8
	{ VK_FORMAT_R8G8_UINT,						{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },			// E_PixelFormat::RG8U
	{ VK_FORMAT_R8G8_SNORM,						{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },			// E_PixelFormat::RG8S
	{ VK_FORMAT_R8G8_SINT,						{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },			// E_PixelFormat::RG8I

	{ VK_FORMAT_B8G8R8_UNORM,					{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_ZERO } },				// E_PixelFormat::BGR8
	{ VK_FORMAT_R8G8B8_UNORM,					{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_ZERO } },				// E_PixelFormat::RGB8
	{ VK_FORMAT_R8G8B8_SINT,					{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_ZERO } },				// E_PixelFormat::RGB8U
	{ VK_FORMAT_R8G8B8_SNORM,					{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_ZERO } },				// E_PixelFormat::RGB8S
	{ VK_FORMAT_R8G8B8_UINT,					{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B,	VK_COMPONENT_SWIZZLE_ZERO } },			// E_PixelFormat::RGB8I

	{ VK_FORMAT_B8G8R8A8_UNORM,				{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::BGRA8
	{ VK_FORMAT_R8G8B8A8_UNORM,				{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::RGBA8
	{ VK_FORMAT_R8G8B8A8_UINT,					{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::RGBA8U
	{ VK_FORMAT_R8G8B8A8_SNORM,				{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::RGBA8S
	{ VK_FORMAT_R8G8B8A8_SINT,					{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::RGBA8I

	{ VK_FORMAT_R16_UNORM,						{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },		// E_PixelFormat::R16
	{ VK_FORMAT_R16_UINT,						{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },		// E_PixelFormat::R16U
	{ VK_FORMAT_R16_SNORM,						{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },		// E_PixelFormat::R16S
	{ VK_FORMAT_R16_SINT,						{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },		// E_PixelFormat::R16I
	{ VK_FORMAT_R16_SFLOAT,						{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },		// E_PixelFormat::R16F

	{ VK_FORMAT_R16G16_UNORM,					{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },			// E_PixelFormat::RG16
	{ VK_FORMAT_R16G16_UINT,					{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },			// E_PixelFormat::RG16U
	{ VK_FORMAT_R16G16_SNORM,					{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },			// E_PixelFormat::RG16S
	{ VK_FORMAT_R16G16_SINT,					{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },			// E_PixelFormat::RG16I
	{ VK_FORMAT_R16G16_SFLOAT,					{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },			// E_PixelFormat::RG16F

	{ VK_FORMAT_R16G16B16A16_UNORM,			{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::RGBA16
	{ VK_FORMAT_R16G16B16A16_UINT,			{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::RGBA16U
	{ VK_FORMAT_R16G16B16A16_SNORM,			{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::RGBA16S
	{ VK_FORMAT_R16G16B16A16_SINT,			{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::RGBA16I
	{ VK_FORMAT_R16G16B16A16_SFLOAT,			{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::RGBA16F

	{ VK_FORMAT_R32_UINT,						{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },		// E_PixelFormat::R32U
	{ VK_FORMAT_R32_SINT,						{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },		// E_PixelFormat::R32I
	{ VK_FORMAT_R32_SFLOAT,						{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },		// E_PixelFormat::R32F
	
	{ VK_FORMAT_R32G32_UINT,					{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },			// E_PixelFormat::RG32U
	{ VK_FORMAT_R32G32_SINT,					{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },			// E_PixelFormat::RG32I
	{ VK_FORMAT_R32G32_SFLOAT,					{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },			// E_PixelFormat::RG32F
	
	{ VK_FORMAT_R32G32B32_UINT,				{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_ZERO } },				// E_PixelFormat::RGB32U
	{ VK_FORMAT_R32G32B32_SINT,				{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_ZERO } },				// E_PixelFormat::RGB32I
	{ VK_FORMAT_R32G32B32_SFLOAT,				{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_ZERO } },				// E_PixelFormat::RGB32F

	{ VK_FORMAT_R32G32B32A32_UINT,			{ VK_COMPONENT_SWIZZLE_R,	VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },				// E_PixelFormat::RGBA32U
	{ VK_FORMAT_R32G32B32A32_SINT,			{ VK_COMPONENT_SWIZZLE_R,	VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },				// E_PixelFormat::RGBA32I
	{ VK_FORMAT_R32G32B32A32_SFLOAT,			{ VK_COMPONENT_SWIZZLE_R,	VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },				// E_PixelFormat::RGBA32F

	{ VK_FORMAT_B10G11R11_UFLOAT_PACK32,	{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_ZERO } },				// E_PixelFormat::RG11B10F
	{ VK_FORMAT_A2B10G10R10_UNORM_PACK32,	{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::RGB10A2
	{ VK_FORMAT_B5G5R5A1_UNORM_PACK16,		{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::B5G5R5A1
	
	{ VK_FORMAT_D16_UNORM,						{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },		// E_PixelFormat::D16
	{ VK_FORMAT_D32_SFLOAT,						{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } }, 				// E_PixelFormat::D32
	{ VK_FORMAT_D24_UNORM_S8_UINT,			{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::D24_S8
	{ VK_FORMAT_D32_SFLOAT_S8_UINT,			{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::D32_S8X24	
	
	{ VK_FORMAT_BC1_RGB_UNORM_BLOCK,			{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::BC1
	{ VK_FORMAT_BC2_UNORM_BLOCK,				{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::BC2
	{ VK_FORMAT_BC3_UNORM_BLOCK,				{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::BC3
	{ VK_FORMAT_BC4_UNORM_BLOCK,				{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::BC4
	{ VK_FORMAT_BC5_UNORM_BLOCK,				{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::BC5
	{ VK_FORMAT_BC6H_UFLOAT_BLOCK,			{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::BC6H
	{ VK_FORMAT_BC7_UNORM_BLOCK,				{ VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A } },					// E_PixelFormat::BC7

	{ VK_FORMAT_UNDEFINED,						{ VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } },	// E_PixelFormat::Count
	{ VK_FORMAT_UNDEFINED,						{ VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO } }	// E_PixelFormat::Unknown
};
const VkFormat FXD::GFX::Pixel::GetFormat(GFX::E_PixelFormat eFormat)
{
	return kVKPixelFormats[FXD::STD::to_underlying(eFormat)].format;
}
const VkComponentMapping FXD::GFX::Pixel::GetMapping(GFX::E_PixelFormat eFormat)
{
	return kVKPixelFormats[FXD::STD::to_underlying(eFormat)].map;
}
const VkImageAspectFlagBits FXD::GFX::Pixel::GetAspectMask(GFX::E_BufferUsageFlags eFlags)
{
	PRINT_COND_ASSERT((eFlags & (GFX::E_BufferUsageFlag::BIND_RENDER_TARGET | GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL)) != (GFX::E_BufferUsageFlag::BIND_RENDER_TARGET | GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL), "RenderTarget and DepthStencil can't be requested together!");

	return VkImageAspectFlagBits(
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL) ? (VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT) : 0) |
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_RENDER_TARGET) ? VK_IMAGE_ASPECT_COLOR_BIT : 0)
	);
}

// ------
// Index
// -
// ------
CONSTEXPR const VkIndexType kVKIndexTypesDescs[] =
{
	{ VK_INDEX_TYPE_UINT16 },		// E_BufferType::Bit_16
	{ VK_INDEX_TYPE_UINT32 },		// E_BufferType::Bit_32
	{ VK_INDEX_TYPE_MAX_ENUM },	// E_BufferType::Count
	{ VK_INDEX_TYPE_MAX_ENUM },	// E_BufferType::Unknown
};
const VkIndexType FXD::GFX::Index::GetFormat(GFX::E_IndexType eType)
{
	return kVKIndexTypesDescs[FXD::STD::to_underlying(eType)];
}


// ------
// Vertex
// -
// ------
CONSTEXPR const VkFormat kVKVertexTypesDescs[] =
{
	{ VK_FORMAT_R32_SFLOAT },					// E_VertexElementType::Float1
	{ VK_FORMAT_R32G32_SFLOAT },				// E_VertexElementType::Float2
	{ VK_FORMAT_R32G32B32_SFLOAT },			// E_VertexElementType::Float3
	{ VK_FORMAT_R32G32B32A32_SFLOAT },		// E_VertexElementType::Float4
	{ VK_FORMAT_R8G8B8A8_UNORM },				// E_VertexElementType::Colour
	{ VK_FORMAT_R8G8B8A8_UNORM },				// E_VertexElementType::Colour_ARGB
	{ VK_FORMAT_R8G8B8A8_UNORM },				// E_VertexElementType::Colour_ABGR
	{ VK_FORMAT_R16_SINT },						// E_VertexElementType::Short1
	{ VK_FORMAT_R16G16_SINT },					// E_VertexElementType::Short2
	{ VK_FORMAT_R16G16B16A16_SINT },			// E_VertexElementType::Short4
	{ VK_FORMAT_R16_UINT },						// E_VertexElementType::UShort1
	{ VK_FORMAT_R16G16_UINT },					// E_VertexElementType::UShort2
	{ VK_FORMAT_R16G16B16A16_UINT },			// E_VertexElementType::UShort4
	{ VK_FORMAT_R32_SINT },						// E_VertexElementType::Int1
	{ VK_FORMAT_R32G32_SINT },					// E_VertexElementType::Int2
	{ VK_FORMAT_R32G32B32_SINT },				// E_VertexElementType::Int3
	{ VK_FORMAT_R32G32B32A32_SINT },			// E_VertexElementType::Int4
	{ VK_FORMAT_R32_UINT },						// E_VertexElementType::UInt1
	{ VK_FORMAT_R32G32B32_UINT },				// E_VertexElementType::UInt2
	{ VK_FORMAT_R32G32B32A32_UINT },			// E_VertexElementType::UInt3
	{ VK_FORMAT_R8G8B8A8_UINT },				// E_VertexElementType::UInt4
	{ VK_FORMAT_R8G8B8A8_UNORM },				// E_VertexElementType::UByte4_Norm
	{ VK_FORMAT_UNDEFINED },					// E_VertexElementType::Count
	{ VK_FORMAT_UNDEFINED }						// E_VertexElementType::Unknown
};
const VkFormat FXD::GFX::Vertex::GetFormat(GFX::E_VertexElementType eType)
{
	return kVKVertexTypesDescs[FXD::STD::to_underlying(eType)];
}
#endif //IsGFXVulkan()