// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/Vulkan/GfxAdapterVK.h"
#include "FXDEngine/GFX/Vulkan/HWBufferVK.h"
#include "FXDEngine/GFX/Vulkan/GfxApiVK.h"
#include "FXDEngine/GFX/Vulkan/MappingVK.h"

using namespace FXD;
using namespace GFX;

// ------
// IHWBuffer::Impl::Impl
// - 
// ------
IHWBuffer::Impl::Impl::Impl(void)
	: m_vkBuffer(VK_NULL_HANDLE)
	, m_vkUsageFlags(0)
	, m_vkDeviceMemory(VK_NULL_HANDLE)
{
}

IHWBuffer::Impl::~Impl(void)
{
	FXD_ERROR("");
	PRINT_COND_ASSERT((m_vkBuffer == VK_NULL_HANDLE), "GFX: m_vkBuffer is not shutdown");
	PRINT_COND_ASSERT((m_vkDeviceMemory == VK_NULL_HANDLE), "GFX: m_vkDeviceMemory is not shutdown");
}

// ------
// IHWBuffer
// - 
// ------
IHWBuffer::IHWBuffer(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
	, m_bLocked(false)
{
}

IHWBuffer::~IHWBuffer(void)
{
}

bool IHWBuffer::_create(const GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc)
{
	auto pGfxApi = static_cast< const GFX::IGfxApi* >(m_pAdapter->api());
	VkDevice vkDevice = m_pAdapter->impl().vkDevice();

	m_nCount = nCount;
	m_nStride = nStride;
	m_eBufferUsage = eBufferUsage;

	// Describe the buffer.
	m_impl->m_vkUsageFlags |= Buffer::ConvertToVKBufferUsageBits(m_eBufferUsage);

	VkBufferCreateInfo vkBufferInfo = {};
	GFX::MemZeroVulkanStruct(vkBufferInfo, VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO);
	vkBufferInfo.pNext = nullptr;
	vkBufferInfo.flags = 0;
	vkBufferInfo.size = get_size();
	vkBufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	vkBufferInfo.usage = m_impl->m_vkUsageFlags;
	vkBufferInfo.queueFamilyIndexCount = 0;
	vkBufferInfo.pQueueFamilyIndices = nullptr;

	VKResultCheckFail(m_pAdapter->impl().vkCreateBuffer(vkDevice, &vkBufferInfo, &pGfxApi->impl().vkAllocCallbacks(), &m_impl->m_vkBuffer));
	GFX::MemZeroVulkanStruct(vkBufferInfo, VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO);
	return true;
}

bool IHWBuffer::_release(void)
{
	//vkDestroyBuffer(device, vertexBuffer, nullptr); 
	
	return true;
}

void* IHWBuffer::_map(FXD::U32 nOffset, FXD::U32 nLength, GFX::E_BufferLockOptions eLockMode, FXD::U32 nQueueID)
{
	PRINT_COND_ASSERT((nLength <= get_size()), "GFX: Provided length " << nLength << " which is larger than the buffer " << get_size() << "");
	PRINT_COND_ASSERT(((nOffset + nLength) <= get_size()), "GFX: Provided offset " << nOffset << " + length " << nLength << " is larger than the buffer " << get_size() << "");
	PRINT_COND_ASSERT((nLength > 0), "GFX: Provided length of zero to the buffer");

	VkDevice vkDevice = m_pAdapter->impl().vkDevice();
	LockedKey lockedKey(m_impl->m_vkBuffer);
	LockedData lockedData;

	if (is_dynamic())
	{
		PRINT_COND_ASSERT((eLockMode == GFX::E_BufferLockOptions::WriteOnly), "GFX: Attempting to read to a non dynamic buffer");

		void* pData = nullptr;
		VKResultCheckFail(m_pAdapter->impl().vkMapMemory(vkDevice, m_impl->m_vkDeviceMemory, nOffset, nLength, 0, &pData));

		lockedData.set_data(pData);
		//lockedData.Pitch = d3dSubresource.RowPitch;
		lockedData.Offset = nOffset;
	}
	else
	{
		if (eLockMode == GFX::E_BufferLockOptions::ReadOnly)
		{
		}
		else
		{
			// If the static buffer is being locked for writing, allocate memory for the contents to be written to.
			lockedData.alloc_data(nLength);
			lockedData.Pitch = nLength;
			lockedData.Offset = nOffset;
		}
	}

	m_impl->m_outstandingLocks[lockedKey] = lockedData;
	return lockedData.get_data();
}

void IHWBuffer::_unmap(void)
{
	VkDevice vkDevice = m_pAdapter->impl().vkDevice();
	LockedKey lockedKey(m_impl->m_vkBuffer);
	{
		LockedData lockedData;
		PRINT_COND_ASSERT(m_impl->m_outstandingLocks.try_get_value(lockedKey, lockedData), "GFX: Could not find lock data");

		if (is_dynamic())
		{
			// If the lock is dynamic, its memory was mapped directly; unmap it.
			m_pAdapter->impl().vkUnmapMemory(vkDevice, m_impl->m_vkDeviceMemory);
		}
		else
		{
			// If the lock is involved a staging resource, it was locked for reading.
			if (lockedData.StagingResource != nullptr)
			{
			}
			else
			{
				// Copy the contents of the temporary memory buffer allocated for writing into the VB.

				// Free the temporary memory buffer.
				lockedData.free_data();
			}
		}
	}

	// Remove the LockedData from the lock map. If the lock involved a staging resource, this releases it.
	m_impl->m_outstandingLocks.find_erase(lockedKey);
}

/*
void IHWBuffer::copy_data(HWBuffer& srcBuffer, FXD::U32 nSrcOffset, FXD::U32 nDstOffset, FXD::U32 nLength, bool bDiscardWholeBuffer, const std::shared_ptr< GFX::CmdBuffer >& commandBuffer)
{
	auto executeRef = [this](HWBuffer& srcBuffer, FXD::U32 nSrcOffset, FXD::U32 nDstOffset, FXD::U32 nLength)
	{
		// If we're copying same-size buffers in their entirety
		if (nSrcOffset == 0 && nDstOffset == 0 && nLength == m_nSize && m_nSize == srcBuffer.get_size())
		{
			mDevice.getImmediateContext()->CopyResource(mD3DBuffer, static_cast<HWBufferDX11&>(srcBuffer).getD3DBuffer());
			if (mDevice.hasError())
			{
				Core::String8 errorDescription = mDevice.getErrorDescription();
				FXD_ERROR("BS_EXCEPT");
				//BS_EXCEPT(RenderingAPIException, "Cannot copy D3D11 resource\nError Description:" + errorDescription);
			}
		}
		else
		{
			// Copy subregion
			D3D11_BOX srcBox;
			srcBox.left = (UINT)nSrcOffset;
			srcBox.right = (UINT)nSrcOffset + nLength;
			srcBox.top = 0;
			srcBox.bottom = 1;
			srcBox.front = 0;
			srcBox.back = 1;

			mDevice.getImmediateContext()->CopySubresourceRegion(mD3DBuffer, 0, (UINT)nDstOffset, 0, 0, static_cast<HWBufferDX11&>(srcBuffer).getD3DBuffer(), 0, &srcBox);
			if (mDevice.hasError())
			{
				Core::String8 errorDescription = mDevice.getErrorDescription();
				FXD_ERROR("BS_EXCEPT");
				//BS_EXCEPT(RenderingAPIException, "Cannot copy D3D11 subresource region\nError Description:" + errorDescription);
			}
		}
	};

	if (commandBuffer == nullptr)
	{
		executeRef(srcBuffer, nSrcOffset, nDstOffset, nLength);
	}
	else
	{
		auto execute = [&]()
		{
			executeRef(srcBuffer, nSrcOffset, nDstOffset, nLength);
		};
		std::shared_ptr< D3D11CommandBuffer > cb = std::static_pointer_cast< D3D11CommandBuffer >(commandBuffer);
		cb->queue_command(execute);
	}
}
*/

bool IIndexBuffer::_activate(FXD::U32 nOffset) const
{
	return true;
}

bool IVertexBuffer::_activate(FXD::U32 nOffset, FXD::U32 nStrideOverride) const
{
	return true;
}
#endif //IsGFXVulkan()