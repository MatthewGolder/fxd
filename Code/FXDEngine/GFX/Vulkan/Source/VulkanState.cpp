// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/Vulkan/VulkanState.h"
#include "FXDEngine/GFX/Vulkan/MappingVK.h"
#include "FXDEngine/Memory/Memory.h"

using namespace FXD;
using namespace GFX;

// ------
// Viewport
// -
// ------
struct _VKViewport
{
	FXD::U32 NumOfViewports = 0;
	VkViewport Viewports[16];
};
static _VKViewport s_viewports = {};

void FXD::GFX::Cache::Viewport::Set(GFX::IGfxAdapter* pAdapter, VkViewport* pViewports, FXD::U32 nCount)
{
	//VK_STATE_CACHE_VERIFY_PRE();

	if ((s_viewports.NumOfViewports != nCount) || Memory::MemCompare(&s_viewports.Viewports[0], &pViewports, (sizeof(VkViewport) * nCount)))
	{
		s_viewports.NumOfViewports = nCount;
		Memory::MemCopy(&s_viewports.Viewports[0], pViewports, (sizeof(VkViewport) * nCount));
		//pAdapter->impl().vkCmdSetViewport(mCmdBuffer, 0, nCount, &pViewports);
	}
	//VK_STATE_CACHE_VERIFY_POST();
}

// ------
// ScissorRect
// -
// ------
struct _VKScissorRects
{
	FXD::U32 NumOfRects = 0;
	VkRect2D Rects[16];
};
static _VKScissorRects s_scissorRects = {};

void FXD::GFX::Cache::ScissorRect::Set(GFX::IGfxAdapter* pAdapter, VkRect2D* pRects, FXD::U32 nCount)
{
	//VK_STATE_CACHE_VERIFY_PRE();

	if ((s_scissorRects.NumOfRects != nCount) || Memory::MemCompare(&s_scissorRects.Rects[0], &pRects, (sizeof(VkRect2D) * nCount)))
	{
		s_scissorRects.NumOfRects = nCount;
		Memory::MemCopy(&s_scissorRects.Rects[0], pRects, (sizeof(VkRect2D) * nCount));
		//pAdapter->impl().vkCmdSetScissor(mCmdBuffer, 0, nCount, &pRects);
	}
	//VK_STATE_CACHE_VERIFY_POST();
}
#endif //IsGFXVulkan()