// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/Vulkan/GfxAdapterVK.h"
#include "FXDEngine/GFX/Vulkan/GfxApiVK.h"
#include "FXDEngine/GFX/Vulkan/GPUMemoryVK.h"
#include "FXDEngine/GFX/Vulkan/ImageVK.h"
#include "FXDEngine/GFX/Vulkan/MappingVK.h"
#include "FXDEngine/GFX/Vulkan/RenderTargetVK.h"
#include "FXDEngine/GFX/Vulkan/SwapChainVK.h"

using namespace FXD;
using namespace GFX;

// ------
// IRenderTarget::Impl::Impl
// - 
// ------
IRenderTarget::Impl::Impl::Impl(void)
	//: m_vkSemaphore(VK_NULL_HANDLE)
	: m_vkFrameBuffer(VK_NULL_HANDLE)
	, m_vkRenderPass(VK_NULL_HANDLE)
	, m_nNumColorViews(0)
{
	Memory::MemZero(&m_vkColorReferences[0], (GFX::kMaxMultipleRenderTargets * sizeof(VkAttachmentReference)));
	Memory::MemZero_T(m_vkDepthReference);
	Memory::MemZero(&m_vkAttachmentViews[0], ((GFX::kMaxMultipleRenderTargets + 1) * sizeof(VkImageView)));
}

IRenderTarget::Impl::~Impl(void)
{
	//PRINT_COND_ASSERT(m_vkSemaphore == VK_NULL_HANDLE, "GFX: m_vkSemaphore is not shutdown");
	PRINT_COND_ASSERT(m_vkFrameBuffer == VK_NULL_HANDLE, "GFX: m_vkFrameBuffer is not shutdown");
	PRINT_COND_ASSERT(m_vkRenderPass == VK_NULL_HANDLE, "GFX: m_vkRenderPass is not shutdown");
}

// ------
// IRenderTarget
// -
// ------
IRenderTarget::IRenderTarget(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IRenderTarget::~IRenderTarget(void)
{
}

bool IRenderTarget::_create(const GFX::RENDER_TEXTURE_DESC& rtDesc)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	m_rtDesc = rtDesc;
	Funcs::ValidateExtents(m_rtDesc, m_rtExtent);

	VkAttachmentDescription vkAttachDescs[GFX::kMaxMultipleRenderTargets + 1];
	Memory::MemZero(&vkAttachDescs[0], (GFX::kMaxMultipleRenderTargets + 1) * sizeof(VkAttachmentDescription));

	FXD::U16 nAttachmentIdx = 0;
	for (FXD::U16 n1 = 0; n1 < GFX::kMaxMultipleRenderTargets; n1++)
	{
		const GFX::RENDER_SURFACE_DESC& surfaceDesc = m_rtDesc.ColorSurfaces[n1];
		if (surfaceDesc.Image == nullptr)
		{
			continue;
		}
		if (!surfaceDesc.Image->imageDesc().Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_RENDER_TARGET))
		{
			PRINT_ASSERT << "GFX: Provided texture is not created with render target usage";
		}

		GFX::IMAGEVIEW_DESC imageViewDesc = {};
		imageViewDesc.MostDetailMip = surfaceDesc.MipLevel;
		imageViewDesc.NumMips = 1;
		imageViewDesc.FirstArraySlice = surfaceDesc.Face;
		imageViewDesc.NumArraySlices = surfaceDesc.NumFaces;
		m_colorSurfaceView[nAttachmentIdx] = m_pAdapter->create_image_view(surfaceDesc.Image.get(), imageViewDesc).get();

		VkAttachmentDescription& vkAttachDesc = vkAttachDescs[nAttachmentIdx];
		vkAttachDesc.flags = 0;
		vkAttachDesc.format = Pixel::GetFormat(surfaceDesc.Image->imageDesc().Format);
		vkAttachDesc.samples = ::Vulkan::GetSampleFlags(surfaceDesc.Image->imageDesc().NumSamples);
		vkAttachDesc.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		vkAttachDesc.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		vkAttachDesc.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		vkAttachDesc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		vkAttachDesc.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		vkAttachDesc.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

		VkAttachmentReference& vkAttachmentRef = m_impl->m_vkColorReferences[nAttachmentIdx];
		vkAttachmentRef.attachment = nAttachmentIdx;
		vkAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		
		m_impl->m_vkAttachmentViews[nAttachmentIdx] = m_colorSurfaceView[nAttachmentIdx]->impl().vkImageViewRenderTarget();

		nAttachmentIdx++;
	}
	m_impl->m_nNumColorViews = nAttachmentIdx;

	{
		const GFX::RENDER_SURFACE_DESC& surfaceDesc = m_rtDesc.DepthStencilSurface;
		if (surfaceDesc.Image != nullptr)
		{
			if (!surfaceDesc.Image->imageDesc().Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL))
			{
				PRINT_ASSERT << "GFX: Provided texture is not created with depth stencil usage";
			}

			GFX::IMAGEVIEW_DESC imageViewDesc = {};
			imageViewDesc.MostDetailMip = surfaceDesc.MipLevel;
			imageViewDesc.NumMips = 1;
			imageViewDesc.FirstArraySlice = surfaceDesc.Face;
			imageViewDesc.NumArraySlices = surfaceDesc.NumFaces;
			m_depthSurfaceView = m_pAdapter->create_image_view(surfaceDesc.Image.get(), imageViewDesc).get();

			VkAttachmentDescription& vkAttachDesc = vkAttachDescs[nAttachmentIdx];
			vkAttachDesc.flags = 0;
			vkAttachDesc.format = Pixel::GetFormat(surfaceDesc.Image->imageDesc().Format);
			vkAttachDesc.samples = ::Vulkan::GetSampleFlags(surfaceDesc.Image->imageDesc().NumSamples);
			vkAttachDesc.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			vkAttachDesc.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
			vkAttachDesc.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			vkAttachDesc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_STORE;
			vkAttachDesc.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			vkAttachDesc.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

			VkAttachmentReference& vkAttachmentRef = m_impl->m_vkDepthReference;
			vkAttachmentRef.attachment = nAttachmentIdx;
			vkAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;

			m_impl->m_vkAttachmentViews[nAttachmentIdx] = m_depthSurfaceView->impl().vkImageViewDepth();
			nAttachmentIdx++;
		}
	}

	auto pGfxApi = static_cast< const GFX::IGfxApi* >(m_pAdapter->api());
	VkDevice vkDevice = m_pAdapter->impl().vkDevice();

	VkSubpassDescription vkSubpassDesc = {};
	Memory::MemZero_T(vkSubpassDesc);
	vkSubpassDesc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	vkSubpassDesc.flags = 0;
	vkSubpassDesc.inputAttachmentCount = 0;
	vkSubpassDesc.pInputAttachments = nullptr;
	vkSubpassDesc.colorAttachmentCount = m_impl->m_nNumColorViews;
	vkSubpassDesc.pColorAttachments = (m_impl->m_nNumColorViews > 0) ? m_impl->m_vkColorReferences : nullptr;
	vkSubpassDesc.pResolveAttachments = nullptr;
	vkSubpassDesc.pDepthStencilAttachment = (m_depthSurfaceView != nullptr) ? &m_impl->m_vkDepthReference : nullptr;
	vkSubpassDesc.preserveAttachmentCount = 0;
	vkSubpassDesc.pPreserveAttachments = nullptr;


	VkRenderPassCreateInfo vkRenderPassInfo = {};
	GFX::MemZeroVulkanStruct(vkRenderPassInfo, VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO);
	vkRenderPassInfo.pNext = nullptr;
	vkRenderPassInfo.flags = 0;
	vkRenderPassInfo.attachmentCount = nAttachmentIdx;
	vkRenderPassInfo.pAttachments = vkAttachDescs;
	vkRenderPassInfo.subpassCount = 1;
	vkRenderPassInfo.pSubpasses = &vkSubpassDesc;
	vkRenderPassInfo.dependencyCount = 0;
	vkRenderPassInfo.pDependencies = nullptr;
	VKResultCheckFail(m_pAdapter->impl().vkCreateRenderPass(vkDevice, &vkRenderPassInfo, &pGfxApi->impl().vkAllocCallbacks(), &m_impl->m_vkRenderPass));


	VkFramebufferCreateInfo vkFramebufferInfo = {};
	GFX::MemZeroVulkanStruct(vkFramebufferInfo, VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO);
	vkFramebufferInfo.pNext = nullptr;
	vkFramebufferInfo.flags = 0;
	vkFramebufferInfo.renderPass = m_impl->m_vkRenderPass;
	vkFramebufferInfo.attachmentCount = nAttachmentIdx;
	vkFramebufferInfo.pAttachments = m_impl->m_vkAttachmentViews;
	vkFramebufferInfo.width = m_rtExtent.Width;
	vkFramebufferInfo.height = m_rtExtent.Height;
	vkFramebufferInfo.layers = m_rtExtent.Depth;
	VKResultCheckFail(m_pAdapter->impl().vkCreateFramebuffer(vkDevice, &vkFramebufferInfo, &pGfxApi->impl().vkAllocCallbacks(), &m_impl->m_vkFrameBuffer));
	return true;
}

bool IRenderTarget::_release(void)
{
	//auto pAdapterDesc = static_cast< const GFX::IGfxAdapterDesc* >(m_pAdapter->adapter_desc().get());
	//auto pGfxApi = static_cast< const GFX::IGfxApi* >(m_pAdapter->api());

	m_rtDesc = GFX::RENDER_TEXTURE_DESC();
	m_rtExtent = GFX::RENDER_TARGET_EXTENT();

	if (m_impl->m_vkFrameBuffer != VK_NULL_HANDLE)
	{
		//m_pAdapter->impl().vkDestroyFramebuffer(m_pAdapter->impl().vkDevice(), m_impl->m_vkFrameBuffer, &pGfxApi->impl().vkAllocCallbacks());
		//m_impl->m_vkFrameBuffer = VK_NULL_HANDLE;
	}
	if (m_impl->m_vkRenderPass != VK_NULL_HANDLE)
	{
		//m_pAdapter->impl().vkDestroyRenderPass(m_pAdapter->impl().vkDevice(), m_impl->m_vkRenderPass, &pGfxApi->impl().vkAllocCallbacks());
		//m_impl->m_vkRenderPass = VK_NULL_HANDLE;
	}
	return true;
}

bool IRenderTarget::_activate_and_clear(const Core::ColourRGBA& colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil) const
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	_activate();
	_clear(colour, eClearFlags, fZ, fDepth, nStencil);

	return true;
}

bool IRenderTarget::_activate(void) const
{
	//GFX::Cache::RenderTarget::Set(m_pAdapter, m_impl->m_, m_impl->m_nNumColorViews, m_impl->m_pDepthStencilView);

	return true;
}

bool IRenderTarget::_clear(const Core::ColourRGBA& colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil) const
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	VkClearAttachment vkClearAttachments[GFX::kMaxMultipleRenderTargets + 1] = {};
	VkClearValue vkClearValue = {};
	Memory::MemZero_T(vkClearValue);
	VkClearDepthStencilValue vkClearDepthStencilValue = {};
	Memory::MemZero_T(vkClearDepthStencilValue);

	if (eClearFlags.is_flag_set(GFX::E_ClearFlag::Colour))
	{
		VkClearColorValue vkColor = {};
		Memory::MemZero_T(vkColor);
		vkColor.float32[0] = colour.getR();
		vkColor.float32[1] = colour.getG();
		vkColor.float32[2] = colour.getB();
		vkColor.float32[3] = colour.getA();

		vkClearValue.color = vkColor;

		for (FXD::U32 n1 = 0; n1 < GFX::kMaxMultipleRenderTargets; n1++)
		{
			if (m_colorSurfaceView[n1] != nullptr)
			{
				//m_pAdapter->impl().vkCmdClearColorImage(m_cmdBufs[i], m_images[i], VK_IMAGE_LAYOUT_GENERAL, &clearColor, 1, &imageRange);
			}
		}
	}

	if (m_depthSurfaceView != nullptr)
	{
		//vkClearValue.depthStencil = vkClearDepthStencilValue;
		if (eClearFlags.is_flag_set(GFX::E_ClearFlag::Depth))
		{
		}
		if (eClearFlags.is_flag_set(GFX::E_ClearFlag::Stencil))
		{
		}
		//if (nDepthstencilFlag != 0)
		{
		}
	}

	/*
	VkClearRect vkClearRect = {};
	Memory::MemZero_T(vkClearRect);

	vkClearRect.layerCount = 1;
	vkClearRect.rect.offset.x = 0;
	vkClearRect.rect.offset.y = 0;
	vkClearRect.rect.extent.width = (uint32_t)m_settings.m_size.x();
	vkClearRect.rect.extent.height = (uint32_t)m_settings.m_size.y();
	m_pAdapter->impl().vkCmdClearAttachments(m_impl->m_commandBuffer->impl().vkCommandBuffer(m_impl->m_nCurrentBuffer), FXD::STD::array_size(vkClearAttachments), vkClearAttachments, 1, &vkClearRect);
	*/

	return true;
}
#endif //IsGFXVulkan()