// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/Vulkan/Vulkan.h"
#include "FXDEngine/Core/Debugger.h"

using namespace FXD;
using namespace GFX;

// ------
// CheckVKResult
// -
// ------
inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, CheckVKResult const& rhs)
{
	switch (rhs.m_vkResult)
	{
	#define HANDLE_STR(h) case h: ostr << #h; break;
		HANDLE_STR(VK_NOT_READY)
		HANDLE_STR(VK_TIMEOUT);
		HANDLE_STR(VK_EVENT_SET);
		HANDLE_STR(VK_EVENT_RESET);
		HANDLE_STR(VK_INCOMPLETE);
		HANDLE_STR(VK_ERROR_OUT_OF_HOST_MEMORY);
		HANDLE_STR(VK_ERROR_OUT_OF_DEVICE_MEMORY);
		HANDLE_STR(VK_ERROR_INITIALIZATION_FAILED);
		HANDLE_STR(VK_ERROR_DEVICE_LOST);
		HANDLE_STR(VK_ERROR_MEMORY_MAP_FAILED);
		HANDLE_STR(VK_ERROR_LAYER_NOT_PRESENT);
		HANDLE_STR(VK_ERROR_EXTENSION_NOT_PRESENT);
		HANDLE_STR(VK_ERROR_FEATURE_NOT_PRESENT);
		HANDLE_STR(VK_ERROR_INCOMPATIBLE_DRIVER);
		HANDLE_STR(VK_ERROR_TOO_MANY_OBJECTS);
		HANDLE_STR(VK_ERROR_FORMAT_NOT_SUPPORTED);
		HANDLE_STR(VK_ERROR_SURFACE_LOST_KHR);
		HANDLE_STR(VK_ERROR_NATIVE_WINDOW_IN_USE_KHR);
		HANDLE_STR(VK_SUBOPTIMAL_KHR);
		HANDLE_STR(VK_ERROR_OUT_OF_DATE_KHR);
		HANDLE_STR(VK_ERROR_INCOMPATIBLE_DISPLAY_KHR);
		HANDLE_STR(VK_ERROR_VALIDATION_FAILED_EXT);
		HANDLE_STR(VK_ERROR_INVALID_SHADER_NV);
	#undef HANDLE_STR
	default:
		{
			FXD_ERROR("Unrecognised VkResult - please find the correct string and add to this case");
			break;
		}
	}
	return ostr;
}

CheckVKResult::CheckVKResult(VkResult vkResult, const FXD::UTF8* pText, const FXD::UTF8* pFile, FXD::U32 nLine)
	: m_vkResult(vkResult)
	, m_pText(pText)
	, m_pFile(pFile)
	, m_nLine(nLine)
{
}

bool CheckVKResult::operator()()
{
	if (m_vkResult == VK_SUCCESS)
	{
		return false;
	}
	PRINT_WARN << "[" << m_pFile << " , " << m_nLine << "] " << m_pText << " - failed with error: " << (*this);
	return true;
}
#endif //IsGFXVulkan()