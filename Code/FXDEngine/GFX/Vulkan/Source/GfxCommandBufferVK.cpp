// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/Vulkan/GfxCommandBufferVK.h"
#include "FXDEngine/GFX/Vulkan/GfxAdapterVK.h"

using namespace FXD;
using namespace GFX;

// ------
// IGfxCommandBuffer::Impl::Impl
// - 
// ------
IGfxCommandBuffer::Impl::Impl::Impl(void)
	: m_vkCmdBuffer(VK_NULL_HANDLE)
{
}
IGfxCommandBuffer::Impl::~Impl(void)
{
}

// ------
// IGfxCommandBuffer
// - 
// ------
IGfxCommandBuffer::IGfxCommandBuffer(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
	, m_eQueueType(GFX::E_GfxQueueType::Unknown)
	, m_nQueueID(0)
	, m_eType(GFX::E_CommandBufferType::Unknown)
	, m_eState(GFX::E_CommandBufferState::Unknown)
{
}
IGfxCommandBuffer::~IGfxCommandBuffer(void)
{
}

bool IGfxCommandBuffer::_create(GFX::E_GfxQueueType eQueueType, FXD::U32 nQueueID, GFX::E_CommandBufferType eType)
{
	m_eQueueType = eQueueType;
	m_nQueueID = nQueueID;
	m_eType = eType;

	VkDevice vkDevice = m_pAdapter->impl().vkDevice();

	VkCommandBufferAllocateInfo vkCommandBufferAllocate = {};
	GFX::MemZeroVulkanStruct(vkCommandBufferAllocate, VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO);
	vkCommandBufferAllocate.pNext = nullptr;
	vkCommandBufferAllocate.level = (m_eType == GFX::E_CommandBufferType::Secondary) ? VK_COMMAND_BUFFER_LEVEL_SECONDARY : VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	vkCommandBufferAllocate.commandBufferCount = 1;
	vkCommandBufferAllocate.commandPool = m_pAdapter->impl().commandPool().vkCmdPool();

	VKResultCheckFail(m_pAdapter->impl().vkAllocateCommandBuffers(vkDevice, &vkCommandBufferAllocate, &m_impl->m_vkCmdBuffer));

	return true;
}

bool IGfxCommandBuffer::_release(void)
{
	VkDevice vkDevice = m_pAdapter->impl().vkDevice();

	if (m_impl->m_vkCmdBuffer != VK_NULL_HANDLE)
	{
		m_pAdapter->impl().vkFreeCommandBuffers(vkDevice, m_pAdapter->impl().commandPool().vkCmdPool(), 1, &m_impl->m_vkCmdBuffer);
	}
	return true;
}

void IGfxCommandBuffer::_begin(void)
{
	PRINT_COND_ASSERT(m_eState == E_CommandBufferState::Ready, "");

	VkCommandBufferBeginInfo vkBeginInfo = {};
	GFX::MemZeroVulkanStruct(vkBeginInfo, VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO);
	vkBeginInfo.pNext = nullptr;
	vkBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	vkBeginInfo.pInheritanceInfo = nullptr;
	VKResultCheckFail(m_pAdapter->impl().vkBeginCommandBuffer(m_impl->m_vkCmdBuffer, &vkBeginInfo));

	m_eState = E_CommandBufferState::Recording;
}

void IGfxCommandBuffer::_end(void)
{
	PRINT_COND_ASSERT(m_eState == E_CommandBufferState::Recording, "");

	VKResultCheckFail(m_pAdapter->impl().vkEndCommandBuffer(m_impl->m_vkCmdBuffer));

	m_eState = E_CommandBufferState::RecordingDone;
}

void IGfxCommandBuffer::_begin_render_pass(void)
{
	PRINT_COND_ASSERT(m_eState == E_CommandBufferState::Ready, "");

	m_eState = E_CommandBufferState::Recording;
}

void IGfxCommandBuffer::_end_render_pass(void)
{
	PRINT_COND_ASSERT(m_eState == E_CommandBufferState::Ready, "");

	m_eState = E_CommandBufferState::Recording;
}

// ------
// IGfxCommandBufferManager::Impl::Impl
// - 
// ------
IGfxCommandBufferManager::Impl::Impl::Impl(void)
{
}
IGfxCommandBufferManager::Impl::~Impl(void)
{
}

// ------
// IGfxCommandBufferManager
// - 
// ------
IGfxCommandBufferManager::IGfxCommandBufferManager(void)
{
}
IGfxCommandBufferManager::~IGfxCommandBufferManager(void)
{
}

bool IGfxCommandBufferManager::_create(void)
{
	return true;
}

bool IGfxCommandBufferManager::_release(void)
{
	return true;
}
#endif //IsGFXVulkan()