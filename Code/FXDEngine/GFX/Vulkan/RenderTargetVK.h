// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_VULKAN_RENDERTARGETVK_H
#define FXDENGINE_GFX_VULKAN_RENDERTARGETVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/RenderTarget.h"
#include "FXDEngine/GFX/Vulkan/Vulkan.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IRenderTarget >
			// -
			// ------
			template <>
			class Impl< GFX::IRenderTarget >
			{
			public:
				friend class GFX::IRenderTarget;

			public:
				Impl(void);
				~Impl(void);

			private:
				//VkSemaphore m_vkSemaphore;
				VkFramebuffer m_vkFrameBuffer;
				VkRenderPass m_vkRenderPass;

				VkAttachmentReference m_vkColorReferences[GFX::kMaxMultipleRenderTargets];
				VkAttachmentReference m_vkDepthReference;

				FXD::U16 m_nNumColorViews;
				VkImageView m_vkAttachmentViews[GFX::kMaxMultipleRenderTargets + 1];
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_VULKAN_RENDERTARGETVK_H