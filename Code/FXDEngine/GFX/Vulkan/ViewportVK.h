// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_VULKAN_DISPLAYVK_H
#define FXDENGINE_GFX_VULKAN_DISPLAYVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/Fence.h"
#include "FXDEngine/GFX/Viewport.h"
#include "FXDEngine/GFX/Vulkan/Vulkan.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IViewport >
			// -
			// ------
			template <>
			class Impl< GFX::IViewport >
			{
			public:
				friend class GFX::IViewport;

			public:
				Impl(void);
				~Impl(void);

				GET_R(VkRenderPass, vkRenderPass, vkRenderPass);

			protected:
				FXD::U32 m_nCurrentBuffer;
				VkRenderPass m_vkRenderPass;
				Container::Vector< VkFramebuffer > m_vkFrameBuffers;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_VULKAN_DISPLAYVK_H