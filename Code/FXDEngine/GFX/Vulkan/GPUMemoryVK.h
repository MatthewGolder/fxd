// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_VULKAN_GPUMEMORYVK_H
#define FXDENGINE_GFX_VULKAN_GPUMEMORYVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/Container/Vector.h"
#include "FXDEngine/GFX/GfxMemory.h"
#include "FXDEngine/GFX/Vulkan/Vulkan.h"
#include "FXDEngine/Memory/MemHandle.h"
#include "FXDEngine/Thread/AtomicInt.h"

namespace FXD
{
	namespace GFX
	{
		// ------
		// HeapPolicyVK
		// -
		// ------
		class HeapPolicyVK : public Memory::IMemPolicy
		{
		public:
			HeapPolicyVK(void);
			~HeapPolicyVK(void);

			void increment_ref(void* pData) FINAL;
			void release_ref(void* pData) FINAL;

			void* lock(void* pData) FINAL;
			void unlock(void* pData) FINAL;

			static VkDeviceMemory getVKDeviceMem(const Memory::MemHandle& mh);
		};

		// ------
		// VKMemHdr
		// -
		// ------
		class VKMemHdr
		{
		public:
			VKMemHdr(VkDeviceMemory vkMem, GFX::IGfxAdapter* pAdapter);
			~VKMemHdr(void);

			void add_ref(void);
			void release(void);

			VkDeviceMemory m_vkMem;
			GFX::IGfxAdapter* m_pAdapter;
			Thread::AtomicInt m_ref;
		};
	} //namespace GFX

	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::MemoryHeapSpec >
			// -
			// ------
			template <>
			class Impl< GFX::MemoryHeapSpec >
			{
			public:
				friend class GFX::MemoryHeapSpec;

			public:
				Impl(void);
				~Impl(void);

				FXD::U32 m_nMemTypeIdx;
			};

			// ------
			// Impl< GFX::ResourceSet >
			// -
			// ------
			template <>
			class Impl< GFX::ResourceSet >
			{
			public:
				friend class GFX::ResourceSet;

			public:
				Impl(void);
				~Impl(void);

			private:
				struct ResourceInfo
				{
					ResourceInfo(void)
						: m_nByteOffset(0)
						, m_nByteSize(0)
					{
					}
					~ResourceInfo(void)
					{
					}

					FXD::U32 m_nByteOffset;
					FXD::U32 m_nByteSize;
				};

			public:
				GFX::AllocSpec m_allocInfo;
				FXD::U32 m_nMemTypeBits;
				FXD::U32 m_nBoundMemoryOffset;
				Memory::MemHandle m_memBounds;
				Container::Vector< ResourceInfo > m_resources;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_VULKAN_GPUMEMORYVK_H