// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_VULKAN_FENCEVK_H
#define FXDENGINE_GFX_VULKAN_FENCEVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/Fence.h"
#include "FXDEngine/GFX/Vulkan/Vulkan.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IFence >
			// -
			// ------
			template <>
			class Impl< GFX::IFence >
			{
			public:
				friend class GFX::IFence;

			public:
				Impl(void);
				~Impl(void);

				GET_R(VkFence, vkFence, vkFence);

			protected:
				VkFence m_vkFence;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_VULKAN_FENCEVK_H