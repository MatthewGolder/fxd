// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_VULKAN_GFXADAPTERVK_H
#define FXDENGINE_GFX_VULKAN_GFXADAPTERVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/GfxAdapter.h"
#include "FXDEngine/GFX/Vulkan/Vulkan.h"
#include "FXDEngine/Thread/AtomicInt.h"
#include "FXDEngine/GFX/Vulkan/CommandPoolVK.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			struct VKQueueInfo
			{
				uint32_t familyIdx = VK_NULL_HANDLE;
				Container::Vector< VkQueue > vkQueues;
			};

			// ------
			// Impl< GFX::IGfxAdapterDesc >
			// - 
			// ------
			template <>
			class Impl< GFX::IGfxAdapterDesc >
			{
			public:
				friend class GFX::IGfxAdapterDesc;

			public:
				Impl(void);
				~Impl(void);

				SET(VkPhysicalDevice, vkPhysicalDevice, vkPhysicalDevice);
				SET(VkPhysicalDeviceMemoryProperties, vkMemoryProperties, vkMemoryProperties);

				GET_R(VkPhysicalDevice, vkPhysicalDevice, vkPhysicalDevice);
				GET_R(VkPhysicalDeviceMemoryProperties, vkMemoryProperties, vkMemoryProperties);

			public:
				VkPhysicalDevice m_vkPhysicalDevice;
				//VkPhysicalDeviceProperties m_vkProperties;
				//VkPhysicalDeviceFeatures m_vkFeatures;
				VkPhysicalDeviceMemoryProperties m_vkMemoryProperties;
			};

			// ------
			// Impl< GFX::IGfxAdapter >
			// -
			// ------
			template <>
			class Impl< GFX::IGfxAdapter >
			{
			public:
				friend class GFX::IGfxAdapter;

			public:
				Impl(GFX::IGfxAdapter* pAdapter);
				~Impl(void);

				VkDeviceMemory allocate_memory(GFX::IGfxAdapter* pAdapter, VkImage& vkImage, const VkMemoryPropertyFlags vkMemoryPropertyFlags);
				VkDeviceMemory allocate_memory(GFX::IGfxAdapter* pAdapter, VkBuffer& vkBuffer, const VkMemoryPropertyFlags vkMemoryPropertyFlags);
				VkDeviceMemory allocate_memory(GFX::IGfxAdapter* pAdapter, const VkMemoryRequirements vkMemReq, const VkMemoryPropertyFlags vkMemoryPropertyFlags);

				void free_memory(GFX::IGfxAdapter* pAdapter, const VkDeviceMemory vkDeviceMemory);
				uint32_t find_memory_type(GFX::IGfxAdapter* pAdapter, const uint32_t nRequirementBits, const VkMemoryPropertyFlags vkMemoryPropertyFlags);

				GET_R(VkDevice, vkDevice, vkDevice);
				GET_R(VkPipelineCache, vkPipelineCache, vkPipelineCache);
				GET_R(VkDescriptorPool, vkDescriptorPool, vkDescriptorPool);
				GET_REF_R(GFX::VKCommandPool, commandPool, commandPool);
				GET_REF_R(Container::Vector< VkQueueFamilyProperties >, vkQueueFamilyProperties, vkQueueFamilyProperties);
				GET_REF_R(Container::Vector< VkExtensionProperties >, vkExtensionProperties, vkExtensionProperties);

				const VKQueueInfo& get_queue_info(const GFX::E_GfxQueueType eType) const { return m_vkQueueInfos[FXD::STD::to_underlying(eType)]; }
				uint32_t get_queue_family(const GFX::E_GfxQueueType eType) const { return get_queue_info(eType).familyIdx; }
				VkQueue get_queue(const GFX::E_GfxQueueType eType, const FXD::U32 nID) const { return get_queue_info(eType).vkQueues[nID]; }

			private:
				bool _create_function_pointers(VkInstance vkInstance);
				bool _release_function_pointers(void);

			private:
				VkDevice m_vkDevice;
				VkPipelineCache m_vkPipelineCache;
				VkDescriptorPool m_vkDescriptorPool;
				Container::Vector< VkQueueFamilyProperties > m_vkQueueFamilyProperties;
				Container::Vector< VkExtensionProperties > m_vkExtensionProperties;

				VKQueueInfo m_vkQueueInfos[FXD::STD::to_underlying(GFX::E_GfxQueueType::Count)];
				GFX::VKCommandPool m_commandPool;

			public:
				PFN_vkGetDeviceQueue vkGetDeviceQueue;										// void vkGetDeviceQueue(VkDevice device, uint32_t queueFamilyIndex, uint32_t queueIndex, VkQueue* pQueue);
				PFN_vkCreateBuffer vkCreateBuffer;											// VkResult vkCreateBuffer(VkDevice device, const VkBufferCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkBuffer* pBuffer);
				PFN_vkDestroyBuffer vkDestroyBuffer;										// void vkDestroyBuffer(VkDevice device, VkBuffer buffer, const VkAllocationCallbacks* pAllocator);
				PFN_vkGetBufferMemoryRequirements vkGetBufferMemoryRequirements;	// void vkGetBufferMemoryRequirements(VkDevice device, VkBuffer buffer, VkMemoryRequirements* pMemoryRequirements);
				PFN_vkGetImageMemoryRequirements vkGetImageMemoryRequirements;		// void vkGetImageMemoryRequirements(VkDevice device, VkImage image, VkMemoryRequirements* pMemoryRequirements);
				PFN_vkBindBufferMemory vkBindBufferMemory;								// VkResult vkBindBufferMemory(VkDevice device, VkBuffer buffer, VkDeviceMemory memory, VkDeviceSize memoryOffset);
				PFN_vkBindImageMemory vkBindImageMemory;									// VkResult vkBindImageMemory(VkDevice device, VkImage image, VkDeviceMemory memory, VkDeviceSize memoryOffset);
				PFN_vkCreateImage vkCreateImage;												// VkResult vkCreateImage(VkDevice device, const VkImageCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkImage* pImage);
				PFN_vkDestroyImage vkDestroyImage;											// void vkDestroyImage(VkDevice device, VkImage image, const VkAllocationCallbacks* pAllocator);
				PFN_vkCreateImageView vkCreateImageView;									// VkResult vkCreateImageView(VkDevice device, const VkImageViewCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkImageView* pView);
				PFN_vkDestroyImageView vkDestroyImageView;								// void vkDestroyImageView(VkDevice device, VkImageView imageView, const VkAllocationCallbacks* pAllocator);
				PFN_vkCmdPipelineBarrier vkCmdPipelineBarrier;							// void vkCmdPipelineBarrier(VkCommandBuffer commandBuffer, VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask, VkDependencyFlags dependencyFlags, uint32_t memoryBarrierCount, const VkMemoryBarrier* pMemoryBarriers, uint32_t bufferMemoryBarrierCount, const VkBufferMemoryBarrier* pBufferMemoryBarriers, uint32_t imageMemoryBarrierCount, const VkImageMemoryBarrier* pImageMemoryBarriers);
				PFN_vkCmdClearColorImage vkCmdClearColorImage;							// void vkCmdClearColorImage(VkCommandBuffer commandBuffer, VkImage image, VkImageLayout imageLayout, const VkClearColorValue* pColor, uint32_t rangeCount, const VkImageSubresourceRange* pRanges);
				PFN_vkQueuePresentKHR vkQueuePresentKHR;									// VkResult vkQueuePresentKHR(VkQueue queue, const VkPresentInfoKHR* pPresentInfo);
				PFN_vkQueueWaitIdle vkQueueWaitIdle;										// VkResult vkQueueWaitIdle(VkQueue queue);
				PFN_vkDeviceWaitIdle vkDeviceWaitIdle;										// VkResult vkDeviceWaitIdle(VkDevice device);
				PFN_vkCmdSetViewport vkCmdSetViewport;										// void vkCmdSetViewport(VkCommandBuffer commandBuffer, uint32_t firstViewport, uint32_t viewportCount, const VkViewport* pViewports);
				PFN_vkCmdSetScissor vkCmdSetScissor;										// void vkCmdSetScissor(VkCommandBuffer commandBuffer, uint32_t firstScissor, uint32_t scissorCount, const VkRect2D* pScissors);
				PFN_vkCmdClearAttachments vkCmdClearAttachments;						// void vkCmdClearAttachments(VkCommandBuffer commandBuffer, uint32_t attachmentCount, const VkClearAttachment* pAttachments, uint32_t rectCount, const VkClearRect* pRects);

																						// Command Buffer Submission
				PFN_vkQueueSubmit vkQueueSubmit;												// VkResult vkQueueSubmit(VkQueue queue, uint32_t submitCount, const VkSubmitInfo* pSubmits, VkFence fence);

																								// Command Pools
				PFN_vkCreateCommandPool vkCreateCommandPool;								// VkResult vkCreateCommandPool(VkDevice device, const VkCommandPoolCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkCommandPool* pCommandPool);
				PFN_vkDestroyCommandPool vkDestroyCommandPool;							// void vkDestroyCommandPool(VkDevice device, VkCommandPool commandPool, const VkAllocationCallbacks* pAllocator);
				PFN_vkResetCommandPool vkResetCommandPool;								// VkResult vkResetCommandPool(VkDevice device, VkCommandPool commandPool, VkCommandPoolResetFlags flags);

																						// Command Buffer Allocation and Management
				PFN_vkAllocateCommandBuffers vkAllocateCommandBuffers;				// VkResult vkAllocateCommandBuffers(VkDevice device, const VkCommandBufferAllocateInfo* pAllocateInfo, VkCommandBuffer* pCommandBuffers);
				PFN_vkFreeCommandBuffers vkFreeCommandBuffers;							// void vkFreeCommandBuffers(VkDevice device, VkCommandPool commandPool, uint32_t commandBufferCount, const VkCommandBuffer* pCommandBuffers);
				PFN_vkResetCommandBuffer vkResetCommandBuffer;							// VkResult vkResetCommandBuffer(VkCommandBuffer commandBuffer, VkCommandBufferResetFlags flags);

																						// Command Buffer Recording
				PFN_vkBeginCommandBuffer vkBeginCommandBuffer;							// VkResult vkBeginCommandBuffer(VkCommandBuffer commandBuffer, const VkCommandBufferBeginInfo* pBeginInfo);
				PFN_vkEndCommandBuffer vkEndCommandBuffer;								// VkResult vkEndCommandBuffer(VkCommandBuffer commandBuffer);

																						// Fences
				PFN_vkCreateFence vkCreateFence;												// VkResult vkCreateFence(VkDevice device, const VkFenceCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkFence* pFence);
				PFN_vkDestroyFence vkDestroyFence;											// void vkDestroyFence(VkDevice device, VkFence fence, const VkAllocationCallbacks* pAllocator);
				PFN_vkGetFenceStatus vkGetFenceStatus;										// VkResult vkGetFenceStatus(VkDevice device, VkFence fence);	https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkGetFenceStatus.html
				PFN_vkResetFences vkResetFences;												// VkResult vkResetFences(VkDevice device, uint32_t fenceCount, const VkFence* pFences);
				PFN_vkWaitForFences vkWaitForFences;										// VkResult vkWaitForFences(VkDevice device, uint32_t fenceCount, const VkFence* pFences, VkBool32 waitAll, uint64_t timeout);

																							// Events
				PFN_vkCreateEvent vkCreateEvent;												// VkResult vkCreateEvent(VkDevice device, const VkEventCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkEvent* pEvent);
				PFN_vkDestroyEvent vkDestroyEvent;											// void vkDestroyEvent(VkDevice device, VkEvent event, const VkAllocationCallbacks* pAllocator);
				PFN_vkGetEventStatus vkGetEventStatus;										// VkResult vkGetEventStatus(VkDevice device, VkEvent event);
				PFN_vkSetEvent vkSetEvent;														// VkResult vkSetEvent(VkDevice device, VkEvent event);
				PFN_vkResetEvent vkResetEvent;												// VkResult vkResetEvent(VkDevice device, VkEvent event);
				PFN_vkCmdSetEvent vkCmdSetEvent;												// void vkCmdSetEvent(VkCommandBuffer commandBuffer, VkEvent event, VkPipelineStageFlags stageMask);
				PFN_vkCmdResetEvent vkCmdResetEvent;										// void vkCmdResetEvent(VkCommandBuffer commandBuffer, VkEvent event, VkPipelineStageFlags stageMask);
				PFN_vkCmdWaitEvents vkCmdWaitEvents;										// void vkCmdWaitEvents(VkCommandBuffer commandBuffer, uint32_t eventCount, const VkEvent* pEvents, VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask, uint32_t memoryBarrierCount, const VkMemoryBarrier* pMemoryBarriers, uint32_t bufferMemoryBarrierCount, const VkBufferMemoryBarrier* pBufferMemoryBarriers, uint32_t imageMemoryBarrierCount, const VkImageMemoryBarrier* pImageMemoryBarriers);

																							// Render Pass Creation
				PFN_vkCreateRenderPass vkCreateRenderPass;								// VkResult vkCreateRenderPass(VkDevice device, const VkRenderPassCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkRenderPass* pRenderPass);
				PFN_vkDestroyRenderPass vkDestroyRenderPass;								// void vkDestroyRenderPass(VkDevice device, VkRenderPass renderPass, const VkAllocationCallbacks* pAllocator);

																							// Framebuffers
				PFN_vkCreateFramebuffer vkCreateFramebuffer;								// VkResult vkCreateFramebuffer(VkDevice device, const VkFramebufferCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkFramebuffer* pFramebuffer);
				PFN_vkDestroyFramebuffer vkDestroyFramebuffer;							// void vkDestroyFramebuffer(VkDevice device, VkFramebuffer framebuffer, const VkAllocationCallbacks* pAllocator);

																						// Render Pass Commands
				PFN_vkCmdBeginRenderPass vkCmdBeginRenderPass;							// void vkCmdBeginRenderPass(VkCommandBuffer commandBuffer, const VkRenderPassBeginInfo* pRenderPassBegin, VkSubpassContents contents);
				PFN_vkCmdEndRenderPass vkCmdEndRenderPass;								// void vkCmdEndRenderPass(VkCommandBuffer commandBuffer);
				PFN_vkCmdNextSubpass vkCmdNextSubpass;										// void vkCmdNextSubpass(VkCommandBuffer commandBuffer, VkSubpassContents contents);
				PFN_vkGetRenderAreaGranularity vkGetRenderAreaGranularity;			// void vkGetRenderAreaGranularity(VkDevice device, VkRenderPass renderPass, VkExtent2D* pGranularity);

																					// Shader modules
				PFN_vkCreateShaderModule vkCreateShaderModule;							// VkResult vkCreateShaderModule(VkDevice device, const VkShaderModuleCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkShaderModule* pShaderModule);
				PFN_vkDestroyShaderModule vkDestroyShaderModule;						// void vkDestroyShaderModule(VkDevice device, VkShaderModule shaderModule, const VkAllocationCallbacks* pAllocator);

																						// Graphice Pipelines
				PFN_vkCreateGraphicsPipelines vkCreateGraphicsPipelines;				// VkResult vkCreateGraphicsPipelines(VkDevice device, VkPipelineCache pipelineCache, uint32_t createInfoCount, const VkGraphicsPipelineCreateInfo* pCreateInfos, const VkAllocationCallbacks* pAllocator, VkPipeline* pPipelines);

																						// Pipeline Destruction
				PFN_vkDestroyPipeline vkDestroyPipeline;									// void vkDestroyPipeline(VkDevice device, VkPipeline pipeline, const VkAllocationCallbacks* pAllocator);

																							// Pipeline Buinding
				PFN_vkCmdBindPipeline vkCmdBindPipeline;									// void vkCmdBindPipeline(VkCommandBuffer commandBuffer, VkPipelineBindPoint pipelineBindPoint, VkPipeline pipeline);

																							// Device Memory
				PFN_vkAllocateMemory vkAllocateMemory;										// VkResult vkAllocateMemory(VkDevice device, const VkMemoryAllocateInfo* pAllocateInfo, const VkAllocationCallbacks* pAllocator, VkDeviceMemory* pMemory);
				PFN_vkFreeMemory vkFreeMemory;												// void vkFreeMemory(VkDevice device, VkDeviceMemory memory, const VkAllocationCallbacks* pAllocator);
				PFN_vkMapMemory vkMapMemory;													// VkResult vkMapMemory(VkDevice device, VkDeviceMemory memory, VkDeviceSize offset, VkDeviceSize size, VkMemoryMapFlags flags, void** ppData);
				PFN_vkFlushMappedMemoryRanges vkFlushMappedMemoryRanges;				// VkResult vkFlushMappedMemoryRanges(VkDevice device, uint32_t memoryRangeCount, const VkMappedMemoryRange* pMemoryRanges);
				PFN_vkInvalidateMappedMemoryRanges vkInvalidateMappedMemoryRanges;// VkResult vkInvalidateMappedMemoryRanges(VkDevice device, uint32_t memoryRangeCount, const VkMappedMemoryRange* pMemoryRanges);
				PFN_vkUnmapMemory vkUnmapMemory;												// void vkUnmapMemory(VkDevice device, VkDeviceMemory memory);
				PFN_vkGetDeviceMemoryCommitment vkGetDeviceMemoryCommitment;		// void vkGetDeviceMemoryCommitment(VkDevice device, VkDeviceMemory memory, VkDeviceSize* pCommittedMemoryInBytes);

																					// Samplers
				PFN_vkCreateSampler vkCreateSampler;										// VkResult vkCreateSampler(VkDevice device, const VkSamplerCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkSampler* pSampler);
				PFN_vkDestroySampler vkDestroySampler;										// void vkDestroySampler(VkDevice device, VkSampler sampler, const VkAllocationCallbacks* pAllocator);

																							// Descriptor Sets
				PFN_vkCreateDescriptorSetLayout vkCreateDescriptorSetLayout;		// VkResult vkCreateDescriptorSetLayout(VkDevice device, const VkDescriptorSetLayoutCreateInfo *pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDescriptorSetLayout* pSetLayout);
				PFN_vkDestroyDescriptorSetLayout vkDestroyDescriptorSetLayout;		// void vkDestroyDescriptorSetLayout(VkDevice device, VkDescriptorSetLayout descriptorSetLayout, const VkAllocationCallbacks* pAllocator);
				PFN_vkCreatePipelineLayout vkCreatePipelineLayout;						// VkResult vkCreatePipelineLayout(VkDevice device, const VkPipelineLayoutCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkPipelineLayout* pPipelineLayout);
				PFN_vkDestroyPipelineLayout vkDestroyPipelineLayout;					// void vkDestroyPipelineLayout(VkDevice device, VkPipelineLayout pipelineLayout, const VkAllocationCallbacks* pAllocator);
				PFN_vkCreateDescriptorPool vkCreateDescriptorPool;						// VkResult vkCreateDescriptorPool(VkDevice device, const VkDescriptorPoolCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDescriptorPool* pDescriptorPool);
				PFN_vkDestroyDescriptorPool vkDestroyDescriptorPool;					// void vkDestroyDescriptorPool(VkDevice device, VkDescriptorPool descriptorPool, const VkAllocationCallbacks* pAllocator);
				PFN_vkAllocateDescriptorSets vkAllocateDescriptorSets;				// VkResult vkAllocateDescriptorSets(VkDevice device, const VkDescriptorSetAllocateInfo* pAllocateInfo, VkDescriptorSet* pDescriptorSets);
				PFN_vkFreeDescriptorSets vkFreeDescriptorSets;							// VkResult vkFreeDescriptorSets(VkDevice device, VkDescriptorPool descriptorPool, uint32_t descriptorSetCount, const VkDescriptorSet* pDescriptorSets);
				PFN_vkResetDescriptorPool vkResetDescriptorPool;						// VkResult vkResetDescriptorPool(VkDevice device, VkDescriptorPool descriptorPool, VkDescriptorPoolResetFlags flags);
				PFN_vkUpdateDescriptorSets vkUpdateDescriptorSets;						// void vkUpdateDescriptorSets(VkDevice device, uint32_t descriptorWriteCount, const VkWriteDescriptorSet* pDescriptorWrites, uint32_t descriptorCopyCount, const VkCopyDescriptorSet* pDescriptorCopies);
				PFN_vkCmdBindDescriptorSets vkCmdBindDescriptorSets;					// void vkCmdBindDescriptorSets(VkCommandBuffer commandBuffer, VkPipelineBindPoint pipelineBindPoint, VkPipelineLayout layout, uint32_t firstSet, uint32_t descriptorSetCount, const VkDescriptorSet* pDescriptorSets, uint32_t dynamicOffsetCount, const uint32_t* pDynamicOffsets);
				PFN_vkCmdPushConstants vkCmdPushConstants;								// void vkCmdPushConstants(VkCommandBuffer commandBuffer, VkPipelineLayout layout, VkShaderStageFlags stageFlags, uint32_t offset, uint32_t size, const void* pValues);

																						// Copying Data Between Buffers
				PFN_vkCmdCopyBuffer vkCmdCopyBuffer;										// void vkCmdCopyBuffer(VkCommandBuffer commandBuffer, VkBuffer srcBuffer, VkBuffer dstBuffer, uint32_t regionCount, VkBufferCopy* pRegions);

																							// Copying Data Between Buffers and Images
				PFN_vkCmdCopyBufferToImage vkCmdCopyBufferToImage;						// void vkCmdCopyBufferToImage(VkCommandBuffer commandBuffer, VkBuffer srcBuffer, VkImage dstImage, VkImageLayout dstImageLayout, uint32_t regionCount, const VkBufferImageCopy* pRegions);
				PFN_vkCmdCopyImageToBuffer vkCmdCopyImageToBuffer;						// void vkCmdCopyImageToBuffer(VkCommandBuffer commandBuffer, VkImage srcImage, VkImageLayout srcImageLayout, VkBuffer dstBuffer, uint32_t regionCount, const VkBufferImageCopy* pRegions);

																						// Programmable Primitive Shading
				PFN_vkCmdBindIndexBuffer vkCmdBindIndexBuffer;							// void vkCmdBindIndexBuffer(VkCommandBuffer commandBuffer, VkBuffer buffer, VkDeviceSize offset, VkIndexType indexType);
				PFN_vkCmdDraw vkCmdDraw;														// void vkCmdDraw(VkCommandBuffer commandBuffer, uint32_t vertexCount, uint32_t instanceCount, uint32_t firstVertex uint32_t firstInstance);
				PFN_vkCmdDrawIndexed vkCmdDrawIndexed;										// void vkCmdDrawIndexed(VkCommandBuffer commandBuffer, uint32_t indexCount, uint32_t instanceCount, uint32_t firstIndex int32_t vertexOffset, uint32_t firstInstance);
				PFN_vkCmdDrawIndirect vkCmdDrawIndirect;									// void vkCmdDrawIndirect(VkCommandBuffer commandBuffer, VkBuffer buffer, VkDeviceSize offset, uint32_t drawCount, uint32_t stride);
				PFN_vkCmdDrawIndexedIndirect vkCmdDrawIndexedIndirect;				// void vkCmdDrawIndexedIndirect(VkCommandBuffer commandBuffer, VkBuffer buffer, VkDeviceSize offset, uint32_t drawCount, uint32_t stride);

																					// Vertex Input Description
				PFN_vkCmdBindVertexBuffers vkCmdBindVertexBuffers;						// void vkCmdBindVertexBuffers(VkCommandBuffer commandBuffer, uint32_t firstBinding, uint32_t bindingCount, const VkBuffer* pBuffers, const VkDeviceSize* pOffsets);

																						// WSI Swapchain
				PFN_vkCreateSwapchainKHR vkCreateSwapchainKHR;							// VkResult vkCreateSwapchainKHR(VkDevice device, const VkSwapchainCreateInfoKHR* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkSwapchainKHR* pSwapchain);
				PFN_vkDestroySwapchainKHR vkDestroySwapchainKHR;						// void vkDestroySurfaceKHR(VkInstance instance, VkSurfaceKHR surface, const VkAllocationCallbacks* pAllocator);
				PFN_vkGetSwapchainImagesKHR vkGetSwapchainImagesKHR;					// VkResult vkGetSwapchainImagesKHR(VkDevice device, VkSwapchainKHR swapchain, uint32_t* pSwapchainImageCount, VkImage* pSwapchainImages);
				PFN_vkAcquireNextImageKHR vkAcquireNextImageKHR;						// VkResult vkAcquireNextImageKHR(VkDevice device, VkSwapchainKHR swapchain, uint64_t timeout, VkSemaphore semaphore, VkFence fence, uint32_t* pImageIndex);
#if IsOSPC()
				PFN_vkCreateWin32SurfaceKHR vkCreateWin32SurfaceKHR;													// VkResult vkCreateWin32SurfaceKHR(VkInstance instance, const VkWin32SurfaceCreateInfoKHR* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkSurfaceKHR* pSurface);
				PFN_vkGetPhysicalDeviceSurfaceSupportKHR vkGetPhysicalDeviceSurfaceSupportKHR;				// VkResult vkGetPhysicalDeviceSurfaceSupportKHR(VkPhysicalDevice physicalDevice, uint32_t queueFamilyIndex, VkSurfaceKHR surface, VkBool32* pSupported);
				PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR vkGetPhysicalDeviceSurfaceCapabilitiesKHR;	// VkResult vkGetPhysicalDeviceSurfaceCapabilitiesKHR(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface, VkSurfaceCapabilitiesKHR* pSurfaceCapabilities);
				PFN_vkGetPhysicalDeviceSurfacePresentModesKHR vkGetPhysicalDeviceSurfacePresentModesKHR;	// VkResult vkGetPhysicalDeviceSurfacePresentModesKHR(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface, uint32_t* pPresentModeCount, VkPresentModeKHR* pPresentModes);
				PFN_vkGetPhysicalDeviceSurfaceFormatsKHR vkGetPhysicalDeviceSurfaceFormatsKHR;				// VkResult vkGetPhysicalDeviceSurfaceFormatsKHR(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface, uint32_t* pPresentModeCount, VkPresentModeKHR* pPresentModes);
#elif IsOSAndroid()
				PFN_vkCreateAndroidSurfaceKHR vkCreateAndroidSurfaceKHR;
#endif
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_VULKAN_GFXADAPTERVK_H