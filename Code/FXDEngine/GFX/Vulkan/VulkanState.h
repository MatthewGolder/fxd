// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_VULKAN_VULKANSTATE_H
#define FXDENGINE_GFX_VULKAN_VULKANSTATE_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/Gfx/Vulkan/Vulkan.h"

namespace FXD
{
	namespace GFX
	{
		namespace Cache
		{
			namespace Viewport
			{
				extern void Set(GFX::IGfxAdapter* pAdapter, VkViewport* pViewports, FXD::U32 nCount);
			}

			namespace ScissorRect
			{
				extern void Set(GFX::IGfxAdapter* pAdapter, VkRect2D* pRects, FXD::U32 nCount);
			}
		} //namespace Cache
	} //namespace GFX
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_VULKAN_VULKANSTATE_H