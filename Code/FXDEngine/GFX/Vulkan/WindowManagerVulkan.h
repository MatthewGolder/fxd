// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_VULKAN_WINDOWMANAGERVK_H
#define FXDENGINE_GFX_VULKAN_WINDOWMANAGERVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/WindowManager.h"
#include "FXDEngine/GFX/Vulkan/Vulkan.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IWindowManager >
			// -
			// ------
			template <>
			class Impl< GFX::IWindowManager >
			{
			public:
				Impl(void);
				~Impl(void);
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_VULKAN_WINDOWMANAGERVK_H