// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_VULKAN_VERTEXDECVK_H
#define FXDENGINE_GFX_VULKAN_VERTEXDECVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/VertexDec.h"
#include "FXDEngine/GFX/Vulkan/Vulkan.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IVertexDecleration >
			// -
			// ------
			template <>
			class Impl< GFX::IVertexDecleration >
			{
			public:
				friend class GFX::IVertexDecleration;

			public:
				Impl(void);
				~Impl(void);
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_VULKAN_VERTEXDECVK_H