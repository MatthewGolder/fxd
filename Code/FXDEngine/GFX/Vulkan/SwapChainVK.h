// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_VULKAN_SWAPCHAINVK_H
#define FXDENGINE_GFX_VULKAN_SWAPCHAINVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/SwapChain.h"
#include "FXDEngine/GFX/Vulkan/Vulkan.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::ISwapChain >
			// -
			// ------
			template <>
			class Impl< GFX::ISwapChain >
			{
			public:
				friend class GFX::ISwapChain;

			public:
				Impl(void);
				~Impl(void);

				GET_R(VkColorSpaceKHR, vkColorSpace, vkColorSpace);
				GET_RW(VkSwapchainKHR, vkSwapChain, vkSwapChain);
				GET_R(VkSurfaceKHR, vkSurface, vkSurface);
				GET_R(uint32_t, nImageCount, nImageCount);

			public:
				uint32_t m_nGraphicsQueueFamily;
				VkColorSpaceKHR m_vkColorSpace;
				VkSurfaceKHR m_vkSurface;
				VkSwapchainKHR m_vkSwapChain;
				uint32_t m_nImageCount;
				VkImage m_vkImages[GFX::kMaxMultipleRenderTargets];
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_VULKAN_SWAPCHAINVK_H