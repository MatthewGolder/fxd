// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_VULKAN_GFXAPIVK_H
#define FXDENGINE_GFX_VULKAN_GFXAPIVK_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXVulkan()
#include "FXDEngine/GFX/GfxApi.h"
#include "FXDEngine/GFX/Vulkan/Vulkan.h"

// Golbal Vulkan Functions
extern PFN_vkCreateInstance vkCreateInstance;																	// VkResult vkCreateInstance(const VkInstanceCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkInstance* pInstance);
extern PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr;														// PFN_vkVoidFunction vkGetInstanceProcAddr(VkInstance instance, const char* pName);
extern PFN_vkGetDeviceProcAddr vkGetDeviceProcAddr;															// PFN_vkVoidFunction vkGetDeviceProcAddr(VkDevice device, const char* pName);
extern PFN_vkEnumerateInstanceExtensionProperties vkEnumerateInstanceExtensionProperties;			// VkResult vkEnumerateInstanceExtensionProperties(const char* pLayerName, uint32_t* pPropertyCount, VkExtensionProperties* pProperties);
extern PFN_vkEnumerateInstanceLayerProperties vkEnumerateInstanceLayerProperties;					// VkResult vkEnumerateInstanceLayerProperties(uint32_t* pPropertyCount, VkLayerProperties* pProperties);

// Golbal Vulkan instance functions.
extern PFN_vkDestroyInstance vkDestroyInstance;																	// void vkDestroyInstance(VkInstance instance, const VkAllocationCallbacks* pAllocator);
extern PFN_vkEnumeratePhysicalDevices vkEnumeratePhysicalDevices;											// VkResult vkEnumeratePhysicalDevices(VkInstance instance, uint32_t* pPhysicalDeviceCount, VkPhysicalDevice* pPhysicalDevices);
extern PFN_vkGetPhysicalDeviceProperties vkGetPhysicalDeviceProperties;									// void vkGetPhysicalDeviceProperties(VkPhysicalDevice physicalDevice, VkPhysicalDeviceProperties* pProperties);
extern PFN_vkGetPhysicalDeviceFeatures vkGetPhysicalDeviceFeatures;										// void vkGetPhysicalDeviceFeatures(VkPhysicalDevice physicalDevice, VkPhysicalDeviceFeatures* pFeatures)
extern PFN_vkGetPhysicalDeviceQueueFamilyProperties vkGetPhysicalDeviceQueueFamilyProperties;	// void vkGetPhysicalDeviceQueueFamilyProperties(VkPhysicalDevice physicalDevice, uint32_t* pQueueFamilyPropertyCount, VkQueueFamilyProperties* pQueueFamilyProperties);
extern PFN_vkCreateDevice vkCreateDevice;																			// VkResult vkCreateDevice(VkPhysicalDevice physicalDevice, const VkDeviceCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDevice* pDevice);
extern PFN_vkDestroyDevice vkDestroyDevice;																		// void vkDestroyDevice(VkDevice device, const VkAllocationCallbacks* pAllocator);
extern PFN_vkEnumerateDeviceExtensionProperties vkEnumerateDeviceExtensionProperties;				// VkResult vkEnumerateDeviceExtensionProperties(VkPhysicalDevice physicalDevice, const char* pLayerName, uint32_t* pPropertyCount, VkExtensionProperties* pProperties);
extern PFN_vkGetPhysicalDeviceMemoryProperties vkGetPhysicalDeviceMemoryProperties;					// void vkGetPhysicalDeviceMemoryProperties(VkPhysicalDevice physicalDevice, VkPhysicalDeviceMemoryProperties* pMemoryProperties);
extern PFN_vkGetPhysicalDeviceFormatProperties vkGetPhysicalDeviceFormatProperties;					// void vkGetPhysicalDeviceFormatProperties(VkPhysicalDevice physicalDevice, VkFormat format, VkFormatProperties* pFormatProperties);
extern PFN_vkGetDeviceProcAddr vkGetDeviceProcAddr;
#if defined(FXD_DEBUG)
extern PFN_vkDebugReportCallbackEXT vkDebugReportCallbackEXT;
extern PFN_vkCreateDebugReportCallbackEXT vkCreateDebugReportCallbackEXT;
extern PFN_vkDestroyDebugReportCallbackEXT vkDestroyDebugReportCallbackEXT;
extern PFN_vkDebugReportMessageEXT vkDebugReportMessageEXT;
#endif

// Helpers
#if IsOSPC()
#define GET_VULKAN_PROC_PLATFORM(t) GetProcAddress(m_impl->m_dllHandle, t)
#elif IsOSAndroid()
#define GET_VULKAN_PROC_PLATFORM(t) dlsym(m_impl->m_dllHandle, t)
#endif

#define GET_VULKAN_PROC(v)								(PFN_##v)GET_VULKAN_PROC_PLATFORM(#v)
#define GET_VULKAN_PROC_CHECK(v, b)					v = GET_VULKAN_PROC(v); if (v == nullptr) { b = false; }

#define GET_VULKAN_INSTANCE_PROC(i, v, t)			(PFN_##v)(vkGetInstanceProcAddr(i, t))
#define GET_VULKAN_INSTANCE_PROC_CHECK(i, v, b)	v = GET_VULKAN_INSTANCE_PROC(i, v, #v); if (v == nullptr) { b = false; }

#define GET_VULKAN_DEVICE_PROC(d, v, t)			(PFN_##v)(vkGetDeviceProcAddr(d, t))
#define GET_VULKAN_DEVICE_PROC_CHECK(d, v, b)	v = GET_VULKAN_DEVICE_PROC(d, v, #v); if (v == nullptr) { b = false; }

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IGfxApi >
			// -
			// ------
			template <>
			class Impl< GFX::IGfxApi >
			{
			public:
				friend class GFX::IGfxApi;
#if IsOSPC()
				using DLLHandle = HMODULE;
#elif IsOSAndroid()
				using DLLHandle = void*;
#endif

			public:
				Impl(void);
				~Impl(void);

				GET_R(VkInstance, vkInstance, vkInstance);
				GET_R(VkAllocationCallbacks, vkAllocCallbacks, vkAllocCallbacks);

			private:
				DLLHandle m_dllHandle;
				VkInstance m_vkInstance;
				VkAllocationCallbacks m_vkAllocCallbacks;
#if defined(FXD_DEBUG)
				VkDebugReportCallbackEXT m_vkMsgCallback;
#endif
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXVulkan()
#endif //FXDENGINE_GFX_VULKAN_GFXAPIVK_H