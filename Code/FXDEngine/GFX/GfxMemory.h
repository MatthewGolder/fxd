// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_GFXMEMORY_H
#define FXDENGINE_GFX_GFXMEMORY_H

#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/GFX/Types.h"

namespace FXD
{
	namespace GFX
	{
		// ------
		// AllocSpec
		// -
		// ------
		struct AllocSpec
		{
			size_t m_size;
			size_t m_align;
		};

		// ------
		// MemoryHeapSpec
		// -
		// ------
		class MemoryHeapSpec : public Core::HasImpl< GFX::MemoryHeapSpec, 256 >
		{
		public:
			MemoryHeapSpec(void);
			MemoryHeapSpec(const Impl& i);
			~MemoryHeapSpec(void);

		private:
		};

		// ------
		// ResourceSet
		// -
		// ------
		class ResourceSet : public Core::HasImpl< GFX::ResourceSet, 256 >, public Core::NonCopyable
		{
		public:
			enum class Usage
			{
				GPUOptimal,		// The GPU will be accessing the resource most frequently.
				Staging,			// The CPU will write to the resource very infrequently, and copy the data to a GPUOptimal resource.
				ReadBack,		// Resource data will be copied back from a GPUOptimal resource for the CPU to read.
			};

		public:
			EXPLICIT ResourceSet(void);
			virtual ~ResourceSet(void);

			AllocSpec get_allocation_spec(void) const;						// Query the memory allocation requirements.
			FXD::U32 resource_count(void) const;								// Query resource set attributes.
			FXD::U32 resource_memory_offset(FXD::U32 nResourceID) const;
		};

	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_GFXMEMORY_H