// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_ANDROID_WINDOWANDROID_H
#define FXDENGINE_GFX_ANDROID_WINDOWANDROID_H

#include "FXDEngine/GFX/Types.h"

#if IsOSAndroid()
#include "FXDEngine/GFX/Window.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IWindow >
			// -
			// ------
			template <>
			class Impl< GFX::IWindow >
			{
			public:
				friend class GFX::IWindow;

			public:
				Impl(void);
				~Impl(void);

			public:
			};
		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsOSAndroid()
#endif //FXDENGINE_GFX_ANDROID_WINDOWANDROID_H