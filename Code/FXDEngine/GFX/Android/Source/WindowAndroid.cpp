// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsOSAndroid()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/GFX/Android/WindowAndroid.h"
#include "FXDEngine/GFX/Events.h"
#include "FXDEngine/Job/EventHandler.h"

using namespace FXD;
using namespace GFX;

// ------
// IWindow::Impl::Impl::Impl
// - 
// ------
IWindow::Impl::Impl::Impl(void)
{
}

IWindow::Impl::~Impl(void)
{
}

// ------
// IWindowAndroid
// - 
// ------
IWindow::IWindow(WINDOW_DESC windowDesc, const GFX::Window& parent)
	: m_windowDesc(windowDesc)
	, OnWindowEvent(nullptr)
{
	OnWindowEvent = FXDEvent()->attach< GFX::OnWindowEvent >([&](auto evt)
	{
		if (evt.WindowHandle == windowHandle())
		{
			_notify_window_event(evt.Event);
		}
	});
}

IWindow::~IWindow(void)
{
	if (OnWindowEvent != nullptr)
	{
		FXDEvent()->detach(OnWindowEvent);
		OnWindowEvent = nullptr;
	}
}

void IWindow::move(FXD::U32 nLeft, FXD::U32 nTop)
{
}

void IWindow::resize(FXD::U32 nWidth, FXD::U32 nHeight)
{
}

FXD::U64 IWindow::windowHandle(void) const
{
	return (FXD::U64)0;
}

bool IWindow::init(void)
{
	return true;
}

void IWindow::close(void)
{
}

void IWindow::set_fullscreen(FXD::U32 nWidth, FXD::U32 nHeight, bool bFullscreen)
{
	m_windowDesc.bFullscreen = bFullscreen;
}

void IWindow::_notify_window_event(GFX::E_WindowEvent eEvent)
{
	//m_size = newSize;
	//FXDEvent()->raise(GFX::OnWindowResize(Math::Vector2I(newSize)));
}
#endif //IsOSAndroid()