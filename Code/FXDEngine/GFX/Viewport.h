// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DISPLAY_H
#define FXDENGINE_GFX_DISPLAY_H

#include "FXDEngine/Core/Colour.h"
#include "FXDEngine/Job/Future.h"
#include "FXDEngine/GFX/Events.h"
#include "FXDEngine/GFX/Types.h"
#include "FXDEngine/Math/Geometry/Box2D.h"

namespace FXD
{
	namespace GFX
	{
		// ------
		// IViewport
		// -
		// ------
		class IViewport : public Core::HasImpl< GFX::IViewport, 256 >, public Core::NonCopyable
		{
		public:
			friend class GFX::IGfxAdapter;

		public:
			IViewport(GFX::IGfxAdapter* pAdapter, GFX::IWindow* pWindow);
			virtual ~IViewport(void);

			Job::Future< bool > create(void);
			Job::Future< bool > release(void);

			bool resize(const Math::Vector2I newSize);

			GET_R(GFX::IWindow*, pWindow, window);
			GET_R(Math::Vector2I, size, size);

			//FXD::F32 aspectRatio(void) const;
			//Math::Box2D bounds(void) const;
			//Math::Box2D safeBounds(void) const;

	protected:
			void _update(FXD::F32 dt);

			bool _create(void);
			bool _release(void);
			bool _resize(const Math::Vector2I newSize);

			void _clear(const Core::ColourRGBA& colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil) const;
			void _begin_drawing_viewport(void) const;
			void _end_drawing_viewport(void) const;

		protected:
			GFX::IGfxAdapter* m_pAdapter;
			GFX::IWindow* m_pWindow;
			GFX::SwapChain m_swapChain;
			Math::Vector2I m_size;

			const Job::Handler< GFX::OnWindowResize >* OnWindowResize;
		};

	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_DISPLAY_H