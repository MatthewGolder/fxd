// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_OPENGL_RENDERTARGETOGL_H
#define FXDENGINE_GFX_OPENGL_RENDERTARGETOGL_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/RenderTarget.h"
#include "FXDEngine/GFX/OpenGL/OpenGL.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IRenderTarget >
			// -
			// ------
			template <>
			class Impl< GFX::IRenderTarget >
			{
			public:
				friend class GFX::IRenderTarget;

			public:
				Impl(void);
				~Impl(void);

			private:
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXOpenGL()
#endif //FXDENGINE_GFX_OPENGL_RENDERTARGETOGL_H