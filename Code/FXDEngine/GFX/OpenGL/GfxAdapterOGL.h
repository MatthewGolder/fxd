// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_OPENGL_GFXADAPTEROGL_H
#define FXDENGINE_GFX_OPENGL_GFXADAPTEROGL_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/GfxAdapter.h"
#include "FXDEngine/GFX/OpenGL/OpenGL.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IGfxAdapterDesc >
			// - 
			// ------
			template <>
			class Impl< GFX::IGfxAdapterDesc >
			{
			public:
				friend class GFX::IGfxAdapterDesc;

			public:
				Impl(void);
				~Impl(void);

				SET_REF(GLuint, nMajorVersion, majorVersion);
				GET_REF_RW(GLuint, nMajorVersion, majorVersion);

				SET_REF(GLuint, nMinorVersion, minorVersion);
				GET_REF_RW(GLuint, nMinorVersion, minorVersion);

			private:
				GLuint m_nMajorVersion;
				GLuint m_nMinorVersion;
			};

			// ------
			// Impl< GFX::IGfxAdapter >
			// -
			// ------
			template <>
			class Impl< GFX::IGfxAdapter >
			{
			public:
				friend class GFX::IGfxAdapter;

			public:
				Impl(void);
				~Impl(void);
			};
		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXOpenGL()
#endif //FXDENGINE_GFX_OPENGL_GFXADAPTEROGL_H