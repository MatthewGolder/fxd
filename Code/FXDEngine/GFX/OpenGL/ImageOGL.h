// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_OPENGL_IMAGEOGL_H
#define FXDENGINE_GFX_OPENGL_IMAGEOGL_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/Image.h"
#include "FXDEngine/GFX/OpenGL/OpenGL.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IImage >
			// -
			// ------
			template <>
			class Impl< GFX::IImageView >
			{
			public:
				friend class GFX::IImageView;

			public:
				Impl(void);
				~Impl(void);

			private:
			};

			// ------
			// Impl< GFX::IImage >
			// -
			// ------
			template <>
			class Impl< GFX::IImage >
			{
			public:
				friend class GFX::IImage;

			public:
				Impl(void);
				~Impl(void);

			private:
				GLuint m_nTexture;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXOpenGL()
#endif //FXDENGINE_GFX_OPENGL_IMAGEOGL_H