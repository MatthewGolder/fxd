// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_OPENGL_GFXAPIOGL_H
#define FXDENGINE_GFX_OPENGL_GFXAPIOGL_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/GfxApi.h"
#include "FXDEngine/GFX/OpenGL/OpenGL.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IGfxApi >
			// -
			// ------
			template <>
			class Impl< GFX::IGfxApi >
			{
			public:
				friend class GFX::IGfxApi;

			public:
				Impl(void);
				~Impl(void);

#if IsGFXOpenGLES()
#else
				GET_R(HDC, hdc, hdc);
				GET_R(HGLRC, hglrc, hglrc);
#endif

			private:
#if IsGFXOpenGLES()
#else
				HDC m_hdc;
				HGLRC m_hglrc;
#endif
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXOpenGL()
#endif //FXDENGINE_GFX_OPENGL_GFXAPIOGL_H