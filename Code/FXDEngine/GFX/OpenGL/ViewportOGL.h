// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_OPENGL_DISPLAYOGL_H
#define FXDENGINE_GFX_OPENGL_DISPLAYOGL_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/Viewport.h"
#include "FXDEngine/GFX/OpenGL/OpenGL.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IViewport >
			// -
			// ------
			template <>
			class Impl< GFX::IViewport >
			{
			public:
				friend class GFX::IViewport;

			public:
				Impl(void);
				~Impl(void);

			protected:
				GFX::Image m_imgColour;
				GFX::Image m_imgDepth;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXOpenGL()
#endif //FXDENGINE_GFX_OPENGL_DISPLAYOGL_H