// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_OPENGL_GPUCOMMANDBUFFEROGL_H
#define FXDENGINE_GFX_OPENGL_GPUCOMMANDBUFFEROGL_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/GfxCommandBuffer.h"
#include "FXDEngine/GFX/OpenGL/OpenGL.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IGfxCommandBuffer >
			// -
			// ------
			template <>
			class Impl< GFX::IGfxCommandBuffer >
			{
			public:
				friend class GFX::IGfxCommandBuffer;

			public:
				Impl(void);
				~Impl(void);

			private:
			};

			// ------
			// Impl< GFX::IGfxCommandBufferManager >
			// -
			// ------
			template <>
			class Impl< GFX::IGfxCommandBufferManager >
			{
			public:
				friend class GFX::IGfxCommandBufferManager;

			public:
				Impl(void);
				~Impl(void);

			private:
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXOpenGL()
#endif //FXDENGINE_GFX_OPENGL_GPUCOMMANDBUFFEROGL_H