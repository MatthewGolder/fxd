// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_OPENGL_WINDOWMANAGEROGL_H
#define FXDENGINE_GFX_OPENGL_WINDOWMANAGEROGL_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/WindowManager.h"
#include "FXDEngine/GFX/OpenGL/OpenGL.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IWindowManager >
			// -
			// ------
			template <>
			class Impl< GFX::IWindowManager >
			{
			public:
				Impl(void);
				~Impl(void);

			protected:
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXOpenGL()
#endif //FXDENGINE_GFX_OPENGL_WINDOWMANAGEROGL_H