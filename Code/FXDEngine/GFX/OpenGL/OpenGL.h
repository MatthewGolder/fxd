// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_OPENGL_OPENGL_H
#define FXDENGINE_GFX_OPENGL_OPENGL_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#	if IsOSPC()
#		include "3rdParty/glad/glad.h"
#		pragma comment(lib,"opengl32.lib")
#	elif IsOSAndroid()
#		include "FXDEngine/GFX/OpenGL/OpenGLES.h"
#	endif

namespace FXD
{
	namespace GFX
	{
		// ------
		// GLResult
		// -
		// ------
		class GLResult
		{
		public:
			GLResult(const GLenum glEnum, const FXD::UTF8* pText, const FXD::UTF8* pFile, const FXD::U32 nLine);
			~GLResult(void);

			bool operator()();

		public:
			const GLenum m_glEnum;
			const FXD::U32 m_nLine;
			const FXD::UTF8* m_pText;
			const FXD::UTF8* m_pFile;
		};
		#define GLCheckFail(c) FXD::GFX::GLResult(c, #c, __FILE__, __LINE__)()
		#define GLCheckSuccess(c) !GLCheckFail(c)

	} //namespace GFX
} //namespace FXD
#endif //IsGFXOpenGL()
#endif //FXDENGINE_GFX_OPENGL_OPENGL_H