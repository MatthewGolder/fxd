// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_OPENGL_HWBUFFEROGL_H
#define FXDENGINE_GFX_OPENGL_HWBUFFEROGL_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/Container/Map.h"
#include "FXDEngine/GFX/HWBuffer.h"
#include "FXDEngine/GFX/OpenGL/OpenGL.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IHWBuffer >
			// -
			// ------
			template <>
			class Impl< GFX::IHWBuffer >
			{
			public:
				friend class GFX::IHWBuffer;

			public:
				Impl(void);
				~Impl(void);

			public:
				GLuint m_glBuffer;
				GLenum m_glBufferType;
				GLenum m_glUsage;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD

#endif //IsGFXOpenGL()
#endif //FXDENGINE_GFX_OPENGL_HWBUFFEROGL_H