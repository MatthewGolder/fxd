// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_OPENGL_FENCEOGL_H
#define FXDENGINE_GFX_OPENGL_FENCEOGL_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/Fence.h"
#include "FXDEngine/GFX/OpenGL/OpenGL.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IFence >
			// -
			// ------
			template <>
			class Impl< GFX::IFence >
			{
			public:
				friend class GFX::IFence;

			public:
				Impl(void);
				~Impl(void);

			protected:
				bool _is_signalled(void) const;
				bool _wait(void);
				bool _reset(void);

			protected:
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXOpenGL()
#endif //FXDENGINE_GFX_OPENGL_FENCEOGL_H