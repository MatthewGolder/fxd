// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/OpenGL/MappingOGL.h"

using namespace FXD;
using namespace GFX;

bool FXD::GFX::OGL::CheckExtension(const FXD::UTF8* pExtension)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

#if IsGFXOpenGLES()
	return false;
#else
	GLint nNumberOfExtensions;
	glGetIntegerv(GL_NUM_EXTENSIONS, &nNumberOfExtensions);
	for (GLint n1 = 0; n1 < nNumberOfExtensions; n1++)
	{
		const GLubyte* pGLExtension = glGetStringi(GL_EXTENSIONS, n1);
		bool bVal = (Core::CharTraits<FXD::UTF8>::compare((FXD::UTF8*)pGLExtension, pExtension) == 0);
		if (bVal)
		{
			return true;
		}
	}
	return false;
#endif
}

void FXD::GFX::OGL::PrintExtension(void)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

#if IsGFXOpenGLES()
#else
	GLint nNumberOfExtensions;
	glGetIntegerv(GL_NUM_EXTENSIONS, &nNumberOfExtensions);
	for (GLint n1 = 0; n1 < nNumberOfExtensions; n1++)
	{
		const GLubyte* pGLExtension = glGetStringi(GL_EXTENSIONS, n1);
		PRINT_INFO << (FXD::UTF8*)pGLExtension;
	}
#endif
}

CONSTEXPR const GLenum kOGLPrimitives[] =
{
	GL_LINES,				// E_Primitive::LineList
	GL_LINE_STRIP,			// E_Primitive::LineStrip
	GL_TRIANGLE_STRIP,	// E_Primitive::TriStrip
	GL_POINTS,				// E_Primitive::PointList
	GL_TRIANGLES,			// E_Primitive::TriList
	GL_TRIANGLE_FAN,		// E_Primitive::TriFan
#if IsGFXOpenGLES()
	GL_INVALID_ENUM,		// E_Primitive::QuadList
#else
	GL_QUADS,				// E_Primitive::QuadList
#endif
	GL_INVALID_ENUM		// E_Primitive::Count
};
GLenum FXD::GFX::OGL::GetPrimitiveType(GFX::E_Primitive eType)
{
	return kOGLPrimitives[FXD::STD::to_underlying(eType)];
}

CONSTEXPR const GLenum kOGLFillMode[] =
{
	GL_POINT,			// E_FillMode::Point
	GL_LINE,				// E_FillMode::WireFrame
	GL_FILL,				// E_FillMode::Solid
	GL_INVALID_ENUM	// E_FillMode::Count
};
GLenum FXD::GFX::OGL::GetFillMode(GFX::E_FillMode eMode)
{
	return kOGLFillMode[FXD::STD::to_underlying(eMode)];
}

CONSTEXPR const GLenum kOGLCullMode[] =
{
	GL_NONE,		// E_CullMode::None
	GL_FRONT,	// E_CullMode::Clockwise
	GL_BACK,		// E_CullMode::CounterClockwise
	GL_NONE		// E_CullMode::Count
};
GLenum FXD::GFX::OGL::GetCullMode(GFX::E_CullMode eMode)
{
	return kOGLCullMode[FXD::STD::to_underlying(eMode)];
}


// ------
// Buffer
// -
// ------
CONSTEXPR const GLuint kOGLBindTypeDescs[] =
{
	GL_ARRAY_BUFFER,					// E_BufferUsageFlag::BIND_VERTEX_BUFFER
	GL_ELEMENT_ARRAY_BUFFER,		// E_BufferUsageFlag::BIND_INDEX_BUFFER
#if IsGFXOpenGLES()
	GL_NONE,								// E_BufferUsageFlag::BIND_UNIFORM_BUFFER
#else
	GL_UNIFORM_BUFFER,				// E_BufferUsageFlag::BIND_UNIFORM_BUFFER
#endif
	GL_TEXTURE_BUFFER,				// E_BufferUsageFlag::BIND_DEPTH_STENCIL
	GL_TEXTURE_BUFFER,				// E_BufferUsageFlag::BIND_RENDER_TARGET
	GL_TEXTURE_BUFFER,				// E_BufferUsageFlag::BIND_SHADER_RESOURCE
	GL_NONE,								// E_PixelFormat::Count
	GL_NONE								// E_PixelFormat::Unknown
};
const GLenum FXD::GFX::Buffer::ConvertToOGLBindFlags(GFX::E_BufferUsageFlags eFlags)
{
	PRINT_COND_ASSERT((eFlags & (GFX::E_BufferUsageFlag::BIND_RENDER_TARGET | GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL)) != (GFX::E_BufferUsageFlag::BIND_RENDER_TARGET | GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL), "RenderTarget and DepthStencil can't be requested together!");

	if (eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_VERTEX_BUFFER))
	{
		return kOGLBindTypeDescs[E_BufferUsageFlag::BIND_VERTEX_BUFFER];
	}
	else if (eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_INDEX_BUFFER))
	{
		return kOGLBindTypeDescs[E_BufferUsageFlag::BIND_INDEX_BUFFER];
	}
	else if (eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_UNIFORM_BUFFER))
	{
		return kOGLBindTypeDescs[E_BufferUsageFlag::BIND_UNIFORM_BUFFER];
	}

	FXD_ERROR("");
	return GL_NONE;
	/*
	return GLenum(
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_VERTEX_BUFFER) ? GL_ARRAY_BUFFER : 0) |
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_INDEX_BUFFER) ? GL_ELEMENT_ARRAY_BUFFER : 0) |
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_UNIFORM_BUFFER) ? GL_UNIFORM_BUFFER : 0) |
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL) ? D3D11_BIND_DEPTH_STENCIL : 0) |
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_RENDER_TARGET) ? GL_TEXTURE_BUFFER : 0) |
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_SHADER_RESOURCE) ? GL_SHADER_STORAGE_BUFFER : 0)
	);
	*/
}
extern const GLenum FXD::GFX::Buffer::ConvertToOGLUsage(GFX::E_BufferUsageFlags eFlags)
{
	PRINT_COND_ASSERT((eFlags & (GFX::E_BufferUsageFlag::USAGE_CPU_READ | GFX::E_BufferUsageFlag::USAGE_CPU_WRITE)) != (GFX::E_BufferUsageFlag::USAGE_CPU_READ | GFX::E_BufferUsageFlag::USAGE_CPU_WRITE), "CPU Read and Write can't be requested together!");

	FXD_ERROR("");
	return GL_NONE;
}

// ------
// Pixel
// -
// ------
CONSTEXPR const GLuint kOGLPixelTypeDescs[] =
{
	GL_R8,						// E_PixelFormat::R8
	GL_R8,						// E_PixelFormat::R8U
	GL_R8,						// E_PixelFormat::R8S
	GL_R8,						// E_PixelFormat::R8I

	GL_RG8,						// E_PixelFormat::RG8
	GL_RG8,						// E_PixelFormat::RG8U
	GL_RG8,						// E_PixelFormat::RG8S
	GL_RG8,						// E_PixelFormat::RG8I

	GL_RGB8,						// E_PixelFormat::BGR8
	GL_RGB8,						// E_PixelFormat::RGB8
	GL_RGB8,						// E_PixelFormat::RGB8U
	GL_RGB8,						// E_PixelFormat::RGB8S
	GL_RGB8,						// E_PixelFormat::RGB8I

	GL_BGRA,						// E_PixelFormat::BGRA8
	GL_RGBA8,					// E_PixelFormat::RGBA8
	GL_RGBA8,					// E_PixelFormat::RGBA8U
	GL_RGBA8,					// E_PixelFormat::RGBA8S
	GL_RGBA8,					// E_PixelFormat::RGBA8I

	GL_R16,						// E_PixelFormat::R16
	GL_R16,						// E_PixelFormat::R16U
	GL_R16,						// E_PixelFormat::R16S
	GL_R16,						// E_PixelFormat::R16I
	GL_R16,						// E_PixelFormat::R16F

	GL_RG16,						// E_PixelFormat::RG16
	GL_RG16,						// E_PixelFormat::RG16U
	GL_RG16,						// E_PixelFormat::RG16S
	GL_RG16,						// E_PixelFormat::RG16I
	GL_RG16,						// E_PixelFormat::RG16F

	GL_RGBA16,					// E_PixelFormat::RGBA16
	GL_RGBA16,					// E_PixelFormat::RGBA16U
	GL_RGBA16,					// E_PixelFormat::RGBA16S
	GL_RGBA16,					// E_PixelFormat::RGBA16I
	GL_RGBA16,					// E_PixelFormat::RGBA16F

	GL_R32F,						// E_PixelFormat::R32U
	GL_R32F,						// E_PixelFormat::R32I
	GL_R32F,						// E_PixelFormat::R32F

	GL_RG32F,					// E_PixelFormat::RG32U
	GL_RG32F,					// E_PixelFormat::RG32I
	GL_RG32F,					// E_PixelFormat::RG32F

	GL_RGB32F,					// E_PixelFormat::RGB32U
	GL_RGB32F,					// E_PixelFormat::RGB32I
	GL_RGB32F,					// E_PixelFormat::RGB32F

	GL_UNSIGNED_SHORT,		// E_PixelFormat::RGBA32U
	GL_UNSIGNED_SHORT,		// E_PixelFormat::RGBA32I
	GL_UNSIGNED_SHORT,		// E_PixelFormat::RGBA32F

	GL_UNSIGNED_SHORT,		// E_PixelFormat::RG11B10F
	GL_UNSIGNED_SHORT,		// E_PixelFormat::RGB10A2
	GL_UNSIGNED_SHORT,		// E_PixelFormat::B5G5R5A1

	GL_UNSIGNED_SHORT,		// E_PixelFormat::D16
	GL_UNSIGNED_SHORT,		// E_PixelFormat::D32
#if IsGFXOpenGLES()
	GL_NONE,						// E_PixelFormat::D24_S8
#else
	GL_UNSIGNED_INT_24_8,	// E_PixelFormat::D24_S8
#endif
	GL_UNSIGNED_SHORT,		// E_PixelFormat::D32_S8X24	

	GL_UNSIGNED_SHORT,		// E_PixelFormat::BC1
	GL_UNSIGNED_SHORT,		// E_PixelFormat::BC2
	GL_UNSIGNED_SHORT,		// E_PixelFormat::BC3
	GL_UNSIGNED_SHORT,		// E_PixelFormat::BC4
	GL_UNSIGNED_SHORT,		// E_PixelFormat::BC5
	GL_UNSIGNED_SHORT,		// E_PixelFormat::BC6H
	GL_UNSIGNED_SHORT,		// E_PixelFormat::BC7

	GL_NONE,						// E_PixelFormat::Count
	GL_NONE						// E_PixelFormat::Unknown
};
const GLuint FXD::GFX::Pixel::GetFormat(const GFX::E_PixelFormat eType)
{
	return kOGLPixelTypeDescs[FXD::STD::to_underlying(eType)];
}

// ------
// Index
// -
// ------
CONSTEXPR const GLuint kOGLIndexTypesDescs[] =
{
	GL_UNSIGNED_SHORT,	// E_BufferType::Bit_16
	GL_UNSIGNED_INT,		// E_BufferType::Bit_32
	GL_NONE,					// E_BufferType::Count
	GL_NONE					// E_BufferType::Unknown
};
const GLuint FXD::GFX::Index::GetFormat(GFX::E_IndexType eType)
{
	return kOGLIndexTypesDescs[FXD::STD::to_underlying(eType)];
}

// ------
// Vertex
// -
// ------
CONSTEXPR const GLuint kOGLVertexTypeDescs[] =
{
	GL_UNSIGNED_INT,		// E_VertexElementType::Float1
	GL_UNSIGNED_INT,		// E_VertexElementType::Float2
	GL_UNSIGNED_INT,		// E_VertexElementType::Float3
	GL_UNSIGNED_INT,		// E_VertexElementType::Float4
	GL_UNSIGNED_INT,		// E_VertexElementType::Colour
	GL_UNSIGNED_INT,		// E_VertexElementType::Colour_ARGB
	GL_UNSIGNED_INT,		// E_VertexElementType::Colour_ABGR
	GL_UNSIGNED_INT,		// E_VertexElementType::Short1
	GL_UNSIGNED_INT,		// E_VertexElementType::Short2
	GL_UNSIGNED_INT,		// E_VertexElementType::Short4
	GL_UNSIGNED_INT,		// E_VertexElementType::UShort1
	GL_UNSIGNED_INT,		// E_VertexElementType::UShort2
	GL_UNSIGNED_INT,		// E_VertexElementType::UShort4
	GL_UNSIGNED_INT,		// E_VertexElementType::Int1
	GL_UNSIGNED_INT,		// E_VertexElementType::Int2
	GL_UNSIGNED_INT,		// E_VertexElementType::Int3
	GL_UNSIGNED_INT,		// E_VertexElementType::Int4
	GL_UNSIGNED_INT,		// E_VertexElementType::UInt1
	GL_UNSIGNED_INT,		// E_VertexElementType::UInt2
	GL_UNSIGNED_INT,		// E_VertexElementType::UInt3
	GL_UNSIGNED_INT,		// E_VertexElementType::UInt4
	GL_UNSIGNED_INT,		// E_VertexElementType::UByte4_Norm
	GL_NONE,					// E_VertexElementType::Count
	GL_NONE					// E_VertexElementType::Unknown
};
const GLuint FXD::GFX::Vertex::GetFormat(GFX::E_VertexElementType eType)
{
	return kOGLVertexTypeDescs[FXD::STD::to_underlying(eType)];
}
#endif //IsGFXOpenGL()