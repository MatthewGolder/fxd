// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/GFX/OpenGL/GfxApiOGL.h"
#include "FXDEngine/GFX/OpenGL/GfxAdapterOGL.h"
#include "FXDEngine/GFX/Mapping.h"

using namespace FXD;
using namespace GFX;

#if IsGFXOpenGLES()
#else
namespace
{
	void CreatePixelFormat(PIXELFORMATDESCRIPTOR* pPFD, FXD::S32 nColorBits, FXD::S32 nDepthBits, FXD::S32 nStencilBits, bool bStereo)
	{
		PIXELFORMATDESCRIPTOR src =
		{
			sizeof(PIXELFORMATDESCRIPTOR),	// size of this pfd
			1,											// version number
			PFD_DRAW_TO_WINDOW |					// support window
			PFD_SUPPORT_OPENGL |					// support OpenGL
			PFD_DOUBLEBUFFER,						// double buffered
			PFD_TYPE_RGBA,							// RGBA type
			nColorBits,								// color depth
			0, 0, 0, 0, 0, 0,						// color bits ignored
			0,											// no alpha buffer
			0,											// shift bit ignored
			0,											// no accumulation buffer
			0, 0, 0, 0,								// accum bits ignored
			nDepthBits,								// z-buffer
			nStencilBits,							// stencil buffer
			0,											// no auxiliary buffer
			PFD_MAIN_PLANE,						// main layer
			0,											// reserved
			0, 0, 0									// layer masks ignored
		};

		if (bStereo)
		{
			src.dwFlags |= PFD_STEREO;
		}
		else
		{
		}
		*pPFD = src;
	}

	const CHAR* pTempName = "Temp-WinClass";

	class TempWind
	{
	public:
		TempWind(void)
			: m_hWnd(0)
			, m_hInstance(0)
			, m_hdc(0)
			, m_hglrc(0)
		{
			// Get Window handle
			m_hInstance = GetModuleHandleW(nullptr);

			WNDCLASS windowclass = {};
			Memory::MemZero_T(windowclass);

			windowclass.lpszClassName = pTempName;
			windowclass.style = CS_OWNDC;
			windowclass.lpfnWndProc = DefWindowProc;
			windowclass.hInstance = m_hInstance;

			if (RegisterClass(&windowclass) == false)
			{
				PRINT_ASSERT << "GFX: failed RegisterClass()";
			}

			// Now create a window
			m_hWnd = CreateWindowEx(WS_EX_APPWINDOW, pTempName, App::GetDisplayGameNameUTF8().c_str(), WS_POPUP, 0, 0, 640, 480, NULL, NULL, m_hInstance, NULL);
			PRINT_COND_ASSERT((m_hWnd != NULL), "GFX: failed CreateWindow()");

			// Create a device context
			m_hdc = GetDC(m_hWnd);
			PRINT_COND_ASSERT((m_hdc != NULL), "GFX: failed RegisterClass()");

			// Create pixel format descriptor...
			PIXELFORMATDESCRIPTOR pfd;
			CreatePixelFormat(&pfd, 32, 0, 0, false);
			if (SetPixelFormat(m_hdc, ChoosePixelFormat(m_hdc, &pfd), &pfd) == false)
			{
				PRINT_ASSERT << "GFX: failed SetPixelFormat()";
			}

			// Create a rendering context!
			HGLRC m_hglrc = wglCreateContext(m_hdc);
			if (wglMakeCurrent(m_hdc, m_hglrc) == false)
			{
				PRINT_ASSERT << "GFX: failed wglMakeCurrent()";
			}

			// Add the GL renderer
			GLint glErr = gladLoadGL();
			if (glErr == 0)
			{
				PRINT_ASSERT << "GFX: failed gladLoadGL()";
			}

		}
		~TempWind(void)
		{
			// Cleanup our window
			wglMakeCurrent(NULL, NULL);
			wglDeleteContext(m_hglrc);
			ReleaseDC(m_hWnd, m_hdc);
			DestroyWindow(m_hWnd);
			UnregisterClass(pTempName, m_hInstance);
		}

	public:
		HWND m_hWnd;
		HINSTANCE m_hInstance;
		HDC m_hdc;
		HGLRC m_hglrc;
	};
}
#endif

// ------
// IGfxApi::Impl::Impl
// - 
// ------
IGfxApi::Impl::Impl::Impl(void)
#if IsGFXOpenGLES()
#else
	: m_hdc(0)
	, m_hglrc(0)
#endif
{
}

IGfxApi::Impl::~Impl(void)
{
}

// ------
// IGfxApi
// -
// ------
IGfxApi::IGfxApi(void)
	: IApi(App::E_Api::GFX)
	, m_eGfxApi(GFX::E_GfxApi::OpenGL)
{
}

IGfxApi::~IGfxApi(void)
{
}

bool IGfxApi::create_api(void)
{
	// 1. Create the library
	bool bRetVal = true;

#if IsGFXOpenGLES()
#else
	TempWind tempWindow;

	m_impl->m_hdc = tempWindow.m_hdc;
	m_impl->m_hglrc = tempWindow.m_hglrc;
#endif

	// 2. Create the list of devices
	_create_adapter_list();

	return bRetVal;
}

bool IGfxApi::release_api(void)
{
	return true;
}

void IGfxApi::_create_adapter_list(void)
{
	const FXD::UTF8* pRenderer; pRenderer = (const char*)glGetString(GL_RENDERER);

#if IsGFXOpenGLES()
	GLint nMajor = 2;
	GLint nMinor = 0;
#else
	GLint nMajor = 0; glGetIntegerv(GL_MAJOR_VERSION, &nMajor);
	GLint nMinor = 0; glGetIntegerv(GL_MINOR_VERSION, &nMinor);
#endif

	GFX::GfxAdapterDesc desc = std::make_shared< GFX::IGfxAdapterDesc >(0);
	desc->driver(GFX::Api::ToString(E_GfxApi::OpenGL).c_str() + Core::to_string8(nMajor) + Core::to_string8(nMinor));
	desc->isDefault(true);
	desc->name(pRenderer);
	desc->deviceType(GFX::E_DeviceType::Unknown);
	desc->impl().majorVersion(nMajor);
	desc->impl().minorVersion(nMinor);
	m_adapterDescs[desc->name()] = desc;
}
#endif //IsGFXOpenGL()