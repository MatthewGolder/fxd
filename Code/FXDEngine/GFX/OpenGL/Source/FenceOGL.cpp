// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/OpenGL/FenceOGL.h"
#include "FXDEngine/GFX/OpenGL/GfxAdapterOGL.h"
#include "FXDEngine/Thread/Thread.h"

using namespace FXD;
using namespace GFX;

// ------
// IFence::Impl::Impl
// - 
// ------
IFence::Impl::Impl::Impl(void)
{
}

IFence::Impl::~Impl(void)
{
}

// ------
// IFence
// - 
// ------
IFence::IFence(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IFence::~IFence(void)
{
}

bool IFence::_create(bool bSignaled)
{
	return true;
}

bool IFence::_release(void)
{
	return true;
}

bool IFence::_is_signalled(void) const
{
	return true;
}

bool IFence::_wait(void)
{
	return true;
}

bool IFence::_reset(void)
{
	return true;
}
#endif //IsGFXOpenGL()