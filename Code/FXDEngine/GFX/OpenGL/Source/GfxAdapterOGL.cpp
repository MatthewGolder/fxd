// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/OpenGL/GfxAdapterOGL.h"
#include "FXDEngine/GFX/OpenGL/GfxCommandBufferOGL.h"
#include "FXDEngine/GFX/FX/FXLibrary.h"
#include "FXDEngine/GFX/FX/StateManager.h"

using namespace FXD;
using namespace GFX;

// ------
// IGfxAdapterDesc::Impl::Impl
// - 
// ------
IGfxAdapterDesc::Impl::Impl::Impl(void)
	: m_nMajorVersion(0)
	, m_nMinorVersion(0)
{
}

IGfxAdapterDesc::Impl::~Impl(void)
{
}

// ------
// IGfxAdapterDesc::IGfxAdapterDesc
// - 
// ------
IGfxAdapterDesc::IGfxAdapterDesc(const App::DeviceIndex nDeviceID)
	: IAdapterDesc(nDeviceID)
	, m_eDeviceType(GFX::E_DeviceType::Unknown)
{
}

IGfxAdapterDesc::~IGfxAdapterDesc(void)
{
}

// ------
// IGfxAdapter::Impl::Impl
// - 
// ------
IGfxAdapter::Impl::Impl::Impl(void)
{
}

IGfxAdapter::Impl::~Impl(void)
{
}

// ------
// IGfxAdapter
// - 
// ------
IGfxAdapter::IGfxAdapter(const App::AdapterDesc& adapterDesc, const App::IApi* pApi)
	: IAdapter(adapterDesc, pApi)
	, m_pDrawingViewport(nullptr)
{
}
IGfxAdapter::~IGfxAdapter(void)
{
}

bool IGfxAdapter::_create(void)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	auto pAdapterDesc = static_cast< GFX::IGfxAdapterDesc* >(m_adapterDesc.get());

	// 1. Create Device
	m_fxLibrary = std::make_shared< GFX::FX::IFXLibrary >(this);
	return true;
}

bool IGfxAdapter::_release(void)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	return true;
}
#endif //IsGFXOpenGL()