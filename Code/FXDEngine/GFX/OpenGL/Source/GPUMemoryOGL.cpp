// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/OpenGL/GPUMemoryOGL.h"

using namespace FXD;
using namespace GFX;

// ------
// MemoryHeapSpec::Impl::Impl
// - 
// ------
MemoryHeapSpec::Impl::Impl::Impl(void)
{
}
MemoryHeapSpec::Impl::~Impl(void)
{
}

// ------
// MemoryHeapSpec
// - 
// ------
MemoryHeapSpec::MemoryHeapSpec(void)
{
}
MemoryHeapSpec::MemoryHeapSpec(const Impl& i)
	: HasImpl(i)
{
}
MemoryHeapSpec::~MemoryHeapSpec(void)
{
}

// ------
// ResourceSet::Impl::Impl
// - 
// ------
ResourceSet::Impl::Impl::Impl(void)
{
}
ResourceSet::Impl::~Impl(void)
{
}

AllocSpec ResourceSet::get_allocation_spec(void) const
{
	return m_impl->m_allocInfo;
}

FXD::U32 ResourceSet::resource_count(void) const
{
	return 0;
}

FXD::U32 ResourceSet::resource_memory_offset(FXD::U32 nResourceID) const
{
	return 0;
}

// ------
// ResourceSet
// - 
// ------
ResourceSet::ResourceSet(void)
{
}
ResourceSet::~ResourceSet(void)
{
}
#endif //IsGFXOpenGL()