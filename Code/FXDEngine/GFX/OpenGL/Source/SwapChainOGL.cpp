// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/OpenGL/GfxAdapterOGL.h"
#include "FXDEngine/GFX/OpenGL/ImageOGL.h"
#include "FXDEngine/GFX/OpenGL/SwapChainOGL.h"
#include "FXDEngine/GFX/OpenGL/ViewportOGL.h"
#include "FXDEngine/GFX/Window.h"

using namespace FXD;
using namespace GFX;

// ------
// ISwapChain::Impl::Impl
// - 
// ------
ISwapChain::Impl::Impl::Impl(void)
{
}

ISwapChain::Impl::~Impl(void)
{
}

// ------
// ISwapChain
// -
// ------
ISwapChain::ISwapChain(GFX::IGfxAdapter* pAdapter, GFX::IViewport* pViewport)
	: m_pAdapter(pAdapter)
	, m_pViewport(pViewport)
	, m_eColorFormat(E_PixelFormat::Unknown)
	, m_eDepthFormat(E_PixelFormat::Unknown)
{
}

ISwapChain::~ISwapChain(void)
{
	PRINT_COND_ASSERT((!m_renderTarget), "GFX: m_renderTarget is not shutdown");
}

bool ISwapChain::_create(void)
{
	auto pAdapterDesc = static_cast< GFX::IGfxAdapterDesc* >(m_pAdapter->adapter_desc().get());

	// 1. Create a SwapChain
	return true;
}

bool ISwapChain::_release(void)
{
	return true;
}

bool ISwapChain::_resize(const Math::Vector2I newSize)
{
	return true;
}

void ISwapChain::_swap(bool bVSync)
{
	//glSwapBuffers();
}
#endif //IsGFXOpenGL()