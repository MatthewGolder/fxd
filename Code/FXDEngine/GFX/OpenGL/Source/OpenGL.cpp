// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/OpenGL/OpenGL.h"

using namespace FXD;
using namespace GFX;

// ------
// GLResult
// -
// ------
inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, GLResult const& rhs)
{
	switch (rhs.m_glEnum)
	{
#define HANDLE_STR(h) case h: ostr << #h; break;
		HANDLE_STR(GL_INVALID_ENUM)
		HANDLE_STR(GL_INVALID_VALUE)
		HANDLE_STR(GL_INVALID_OPERATION)
		HANDLE_STR(GL_STACK_OVERFLOW)
		HANDLE_STR(GL_STACK_UNDERFLOW)
		HANDLE_STR(GL_OUT_OF_MEMORY)
		HANDLE_STR(GL_INVALID_FRAMEBUFFER_OPERATION)
#undef HANDLE_STR
		default:
		FXD_ERROR("Unrecognised GLenum - please find the correct string and add to this case");
		break;
	}
	return ostr;
}

GLResult::GLResult(const GLenum glEnum, const FXD::UTF8* pText, const FXD::UTF8* pFile, const FXD::U32 nLine)
	: m_glEnum(glEnum)
	, m_nLine(nLine)
	, m_pText(pText)
	, m_pFile(pFile)
{
}

GLResult::~GLResult(void)
{
}

bool GLResult::operator()()
{
	if (m_glEnum != GL_NO_ERROR)
	{
		return false;
	}
	PRINT_WARN << "[" << m_pFile << " , " << m_nLine << "] " << m_pText << " - failed with error: " << (*this);
	return true;
}
#endif //IsGFXOpenGL()