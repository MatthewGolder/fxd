// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/OpenGL/WindowManagerOGL.h"

#if IsOSPC()
#include "FXDEngine/GFX/Win32/WindowWin32.h"
#elif IsOSAndroid()
#include "FXDEngine/GFX/Android/WindowAndroid.h"
#endif

using namespace FXD;
using namespace GFX;

// ------
// IWindowManager::Impl::Impl
// - 
// ------
IWindowManager::Impl::Impl::Impl(void)
{
}

IWindowManager::Impl::~Impl(void)
{
}

// ------
// IWindowManager
// - 
// ------
IWindowManager::IWindowManager(void)
{
}

IWindowManager::~IWindowManager(void)
{
}

GFX::Window IWindowManager::_create_window(const WINDOW_DESC& windowDesc, const GFX::Window& parent)
{
	GFX::Window window = std::make_shared< GFX::IWindow >(windowDesc, parent);
	PRINT_COND_ASSERT(window->init(), "GFX: Failed m_window->init()");
	return window;
}

void IWindowManager::_destroy_window(GFX::Window& window)
{
	window->close();
}
#endif //IsGFXOpenGL()