// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/OpenGL/GfxAdapterOGL.h"
#include "FXDEngine/GFX/OpenGL/ImageOGL.h"
#include "FXDEngine/GFX/OpenGL/SwapChainOGL.h"

using namespace FXD;
using namespace GFX;

// ------
// OGL
// -
// ------
namespace OGL
{
	bool create_image(GFX::IGfxAdapter* pAdapter, GFX::IImage* pImage, IMAGE_DESC& imageDesc)
	{
		return false;
	}
	bool create_image_view(GFX::IGfxAdapter* pAdapter, GFX::IImage* pImage, GFX::IImageView* pImageView, IMAGEVIEW_DESC& imageViewDesc)
	{
		return false;
	}
} //namespace OGL


// ------
// IImageView::Impl::Impl
// - 
// ------
IImageView::Impl::Impl::Impl(void)
{
}

IImageView::Impl::~Impl(void)
{
}

// ------
// IImageView
// -
// ------
IImageView::IImageView(GFX::IGfxAdapter* pAdapter, GFX::IImage* pImage)
	: m_pAdapter(pAdapter)
	, m_pImage(pImage)
{
}

IImageView::~IImageView(void)
{
}

bool IImageView::_create(const GFX::IMAGEVIEW_DESC& imageViewDesc)
{
	m_imageViewDesc = imageViewDesc;
	return true;
}

bool IImageView::_release(void)
{
	m_imageViewDesc = IMAGEVIEW_DESC();
	return true;
}


// ------
// IImage::Impl::Impl
// - 
// ------
IImage::Impl::Impl::Impl(void)
	: m_nTexture(0)
{
}

IImage::Impl::~Impl(void)
{
}

// ------
// IImage
// -
// ------
IImage::IImage(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IImage::~IImage(void)
{
}

bool IImage::_create(const GFX::IMAGE_DESC& imageDesc, void* pSrc)
{
	// 
	m_imageDesc = imageDesc;
	::OGL::create_image(m_pAdapter, this, m_imageDesc);
	return true;
}

bool IImage::_create(const GFX::SwapChain& swapChain, FXD::U32 nBuffer)
{
	// 
	GFX::IMAGEVIEW_DESC imageViewDesc;
	return true;
}

bool IImage::_release(void)
{
	m_imageDesc = IMAGE_DESC();
	return true;
}
#endif //IsGFXOpenGL()