// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/OpenGL/GfxAdapterOGL.h"
#include "FXDEngine/GFX/OpenGL/GfxApiOGL.h"
#include "FXDEngine/GFX/OpenGL/GPUMemoryOGL.h"
#include "FXDEngine/GFX/OpenGL/ImageOGL.h"
#include "FXDEngine/GFX/OpenGL/MappingOGL.h"
#include "FXDEngine/GFX/OpenGL/RenderTargetOGL.h"
#include "FXDEngine/GFX/OpenGL/SwapChainOGL.h"

using namespace FXD;
using namespace GFX;

// ------
// IRenderTarget::Impl::Impl
// - 
// ------
IRenderTarget::Impl::Impl::Impl(void)
{
}

IRenderTarget::Impl::~Impl(void)
{
}

// ------
// IRenderTarget
// -
// ------
IRenderTarget::IRenderTarget(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IRenderTarget::~IRenderTarget(void)
{
}

bool IRenderTarget::_create(const GFX::RENDER_TEXTURE_DESC& rtDesc)
{
	return true;
}

bool IRenderTarget::_release(void)
{
	return true;
}

bool IRenderTarget::_activate_and_clear(const Core::ColourRGBA& colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil) const
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	_activate();
	_clear(colour, eClearFlags, fZ, fDepth, nStencil);

	return true;
}

bool IRenderTarget::_activate(void) const
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	/*
	GFX::Cache::RenderTarget::Set(m_pAdapter->impl().d3dDevContext(), m_impl->m_pRenderTargetViews, m_impl->m_nNumColorViews, m_impl->m_pDepthStencilView);

	D3D11_VIEWPORT d3dViewport =
	{
		0.0f, 0.0f,
		(FLOAT)m_rtExtent.Width, (FLOAT)m_rtExtent.Height,
		0.0f, 1.0f
	};
	GFX::Cache::Viewport::Set(m_pAdapter->impl().d3dDevContext(), &d3dViewport, 1);
	*/
	return true;
}

bool IRenderTarget::_clear(const Core::ColourRGBA& colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil) const
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	/*
	if (eClearFlags.is_flag_set(GFX::E_ClearFlag::Colour))
	{
		const FLOAT fClearColor[] = { colour.getR(), colour.getG(), colour.getB(), colour.getA() };
		for (FXD::U32 n1 = 0; n1 < GFX::kMaxMultipleRenderTargets; n1++)
		{
			if (m_impl->m_pRenderTargetViews[n1] != nullptr)
			{
				m_pAdapter->impl().d3dDevContext()->ClearRenderTargetView(m_impl->m_pRenderTargetViews[n1], fClearColor);
			}
		}
	}

	if (m_impl->m_pDepthStencilView != nullptr)
	{
		UINT nDepthstencilFlag = 0;
		if (eClearFlags.is_flag_set(GFX::E_ClearFlag::Depth))
		{
			nDepthstencilFlag |= D3D11_CLEAR_DEPTH;
		}
		if (eClearFlags.is_flag_set(GFX::E_ClearFlag::Stencil))
		{
			nDepthstencilFlag |= D3D11_CLEAR_STENCIL;
		}
		if (nDepthstencilFlag != 0)
		{
			m_pAdapter->impl().d3dDevContext()->ClearDepthStencilView(m_impl->m_pDepthStencilView, nDepthstencilFlag, fZ, (UINT8)nStencil);
		}
	}
	*/
	return true;
}
#endif //IsGFXOpenGL()