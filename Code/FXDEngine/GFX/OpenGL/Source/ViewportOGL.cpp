// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/Window.h"
#include "FXDEngine/GFX/OpenGL/GfxAdapterOGL.h"
#include "FXDEngine/GFX/OpenGL/ImageOGL.h"
#include "FXDEngine/GFX/OpenGL/SwapChainOGL.h"
#include "FXDEngine/GFX/OpenGL/ViewportOGL.h"
#include "FXDEngine/Job/EventHandler.h"

using namespace FXD;
using namespace GFX;

#if IsGFXOpenGLES()
#else
extern void CreatePixelFormat(PIXELFORMATDESCRIPTOR* pPFD, FXD::S32 nColorBits, FXD::S32 nDepthBits, FXD::S32 nStencilBits, bool bStereo);
#endif

// ------
// IViewport::Impl::Impl
// - 
// ------
IViewport::Impl::Impl::Impl(void)
{
}

IViewport::Impl::~Impl(void)
{
	PRINT_COND_ASSERT((!m_imgColour), "GFX: m_imgColour is not shutdown");
	PRINT_COND_ASSERT((!m_imgDepth), "GFX: m_imgDepth is not shutdown");
}

// ------
// IViewport
// - 
// ------
IViewport::IViewport(IGfxAdapter* pAdapter, GFX::IWindow* pWindow)
	: m_pAdapter(pAdapter)
	, m_pWindow(pWindow)
	, m_size(pWindow->window_desc().size)
	, OnWindowResize(nullptr)
{
	OnWindowResize = FXDEvent()->attach< GFX::OnWindowResize >([&](auto evt)
	{
		resize(evt.Pos);
	});
}

IViewport::~IViewport(void)
{
	if (OnWindowResize != nullptr)
	{
		FXDEvent()->detach(OnWindowResize);
		OnWindowResize = nullptr;
	}
}

bool IViewport::_create(void)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	return true;
}

bool IViewport::_release(void)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	FXD_RELEASE(m_impl->m_imgColour, release().get());
	FXD_RELEASE(m_impl->m_imgDepth, release().get());
	FXD_RELEASE(m_swapChain, release());
	return true;
}

bool IViewport::_resize(const Math::Vector2I newSize)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	return true;
}

void IViewport::_clear(const Core::ColourRGBA& colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil) const
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

}

void IViewport::_begin_drawing_viewport(void) const
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");
}

void IViewport::_end_drawing_viewport(void) const
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

}
#endif //IsGFXOpenGL()