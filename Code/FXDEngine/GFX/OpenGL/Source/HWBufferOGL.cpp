// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/OpenGL/GfxAdapterOGL.h"
#include "FXDEngine/GFX/OpenGL/HWBufferOGL.h"
#include "FXDEngine/GFX/OpenGL/MappingOGL.h"

using namespace FXD;
using namespace GFX;

// ------
// IHWBuffer::Impl::Impl
// - 
// ------
IHWBuffer::Impl::Impl::Impl(void)
	: m_glBuffer(0)
	, m_glBufferType(0)
	, m_glUsage(0)
{
}

IHWBuffer::Impl::~Impl(void)
{
}

// ------
// IHWBuffer
// - 
// ------
IHWBuffer::IHWBuffer(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
	, m_bLocked(false)
{
}

IHWBuffer::~IHWBuffer(void)
{
}

bool IHWBuffer::_create(const GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc)
{
	m_nCount = nCount;
	m_nStride = nStride;
	m_eBufferUsage = eBufferUsage;

	// Describe the vertex buffer.
	m_impl->m_glBufferType = Buffer::ConvertToOGLBindFlags(m_eBufferUsage);
	m_impl->m_glUsage = Buffer::ConvertToOGLBindFlags(m_eBufferUsage);

	glGenBuffers(1, &m_impl->m_glBuffer);

	glBindBuffer(m_impl->m_glBufferType, m_impl->m_glBuffer);
	if (GLCheckFail(m_impl->m_glBufferType))
	{
		return false;
	}

	if (pSrc != nullptr)
	{
		const FXD::U32 nSize = get_size();
		glBufferData(m_impl->m_glBufferType, nSize, pSrc, m_impl->m_glUsage);
		if (GLCheckFail(m_impl->m_glBufferType))
		{
			return false;
		}
	}
	return true;
}

bool IHWBuffer::_release(void)
{
	glDeleteBuffers(1, &m_impl->m_glBufferType);

	return true;
}

void* IHWBuffer::_map(FXD::U32 nOffset, FXD::U32 nLength, GFX::E_BufferLockOptions eLockMode, FXD::U32 nQueueID)
{
	PRINT_COND_ASSERT((nLength <= get_size()), "GFX: Provided length " << nLength << " which is larger than the buffer " << get_size() << "");
	PRINT_COND_ASSERT(((nOffset + nLength) <= get_size()), "GFX: Provided offset " << nOffset << " + length " << nLength << " is larger than the buffer " << get_size() << "");
	PRINT_COND_ASSERT((nLength > 0), "GFX: Provided length of zero to the buffer");

	LockedKey lockedKey(&m_impl->m_glBuffer);
	LockedData lockedData;

	if (is_dynamic())
	{
		PRINT_COND_ASSERT((eLockMode == GFX::E_BufferLockOptions::WriteOnly), "GFX: Attempting to read to a non dynamic buffer");

		//void* pGLData = glMapBuffer(m_impl->m_glBufferType, GL_WRITE_ONLY);
		void* pGLData = nullptr;

		FXD::U8* pData = ((FXD::U8*)pGLData) + nOffset;
		lockedData.set_data(pData);
		//lockedData.Pitch = d3dSubresource.RowPitch;
		lockedData.Offset = nOffset;
	}
	else
	{
		if (eLockMode == GFX::E_BufferLockOptions::ReadOnly)
		{
		}
		else
		{
			// If the static buffer is being locked for writing, allocate memory for the contents to be written to.
			lockedData.alloc_data(nLength);
			lockedData.Pitch = nLength;
			lockedData.Offset = nOffset;
		}
	}

	//m_impl->m_outstandingLocks[lockedKey] = lockedData;
	return lockedData.get_data();
}

void IHWBuffer::_unmap(void)
{
	//LockedKey lockedKey(m_impl->m_d3dBuffer.get());
	{
		LockedData lockedData;
		if (is_dynamic())
		{
			// If the lock is dynamic, its memory was mapped directly; unmap it.
			//glUnmapBuffer(m_impl->m_glBufferType);
		}
		else
		{
			// If the lock is involved a staging resource, it was locked for reading.
			if (lockedData.StagingResource != nullptr)
			{
				// Unmap the staging buffer's memory.
			}
			else
			{
				// Copy the contents of the temporary memory buffer allocated for writing into the VB.

				// Free the temporary memory buffer.
				lockedData.free_data();
			}
		}
	}
	//m_impl->m_outstandingLocks.find_erase(lockedKey);
}

/*
void IHWBuffer::copy_data(HWBuffer& srcBuffer, FXD::U32 nSrcOffset, FXD::U32 nDstOffset, FXD::U32 nLength, bool bDiscardWholeBuffer, const std::shared_ptr< GFX::CmdBuffer >& commandBuffer)
{
}
*/

bool IIndexBuffer::_activate(FXD::U32 nOffset) const
{
	GLuint nGLOffset = (GLuint)nOffset;
	//GFX::Cache::IndexBuffer::Set(m_pAdapter->impl().d3dDevContext(), m_impl->m_d3dBuffer, GFX::E_IndexType::Bit_16, nD3DOffset);
	return true;
}

bool IVertexBuffer::_activate(FXD::U32 nOffset, FXD::U32 nStrideOverride) const
{
	GLuint nGLOffset = (GLuint)nOffset;
	GLuint nGLStride = (nStrideOverride != 0) ? (GLuint)nStrideOverride : (GLuint)m_nStride;
	//GFX::Cache::VertexBuffer::Set(m_pAdapter->impl().d3dDevContext(), m_impl->m_d3dBuffer, 0, nD3DStride, nD3DOffset);
	return true;
}
#endif //IsGFXOpenGL()