// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/OpenGL/GfxCommandBufferOGL.h"
#include "FXDEngine/GFX/OpenGL/GfxAdapterOGL.h"

using namespace FXD;
using namespace GFX;

// ------
// IGfxCommandBuffer::Impl::Impl
// - 
// ------
IGfxCommandBuffer::Impl::Impl::Impl(void)
{
}
IGfxCommandBuffer::Impl::~Impl(void)
{
}

// ------
// IGfxCommandBuffer
// - 
// ------
IGfxCommandBuffer::IGfxCommandBuffer(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
	, m_eQueueType(GFX::E_GfxQueueType::Unknown)
	//, m_nDeviceID(0)
	, m_nQueueID(0)
	, m_eType(GFX::E_CommandBufferType::Unknown)
{
}
IGfxCommandBuffer::~IGfxCommandBuffer(void)
{
}

bool IGfxCommandBuffer::_create(GFX::E_GfxQueueType eQueueType, FXD::U32 nQueueID, GFX::E_CommandBufferType eType)
{
	m_eQueueType = eQueueType;
	m_nQueueID = nQueueID;
	m_eType = eType;

	return true;
}

bool IGfxCommandBuffer::_release(void)
{
	return true;
}

void IGfxCommandBuffer::_begin(void)
{
	PRINT_COND_ASSERT(m_eState == E_CommandBufferState::Ready, "");

	m_eState = E_CommandBufferState::Recording;
}

void IGfxCommandBuffer::_end(void)
{
	PRINT_COND_ASSERT(m_eState == E_CommandBufferState::Recording, "");

	m_eState = E_CommandBufferState::RecordingDone;
}

void IGfxCommandBuffer::_begin_render_pass(void)
{
	PRINT_COND_ASSERT(m_eState == E_CommandBufferState::Ready, "");

	m_eState = E_CommandBufferState::Recording;
}

void IGfxCommandBuffer::_end_render_pass(void)
{
	PRINT_COND_ASSERT(m_eState == E_CommandBufferState::Ready, "");

	m_eState = E_CommandBufferState::Recording;
}

// ------
// IGfxCommandBufferManager::Impl::Impl
// - 
// ------
IGfxCommandBufferManager::Impl::Impl::Impl(void)
{
}
IGfxCommandBufferManager::Impl::~Impl(void)
{
}

// ------
// IGfxCommandBufferManager
// - 
// ------
IGfxCommandBufferManager::IGfxCommandBufferManager(void)
{
}
IGfxCommandBufferManager::~IGfxCommandBufferManager(void)
{
}

bool IGfxCommandBufferManager::_create(void)
{
	return true;
}

bool IGfxCommandBufferManager::_release(void)
{
	return true;
}
#endif //IsGFXOpenGL()