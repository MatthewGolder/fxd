// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_OPENGL_OPENGLES_H
#define FXDENGINE_GFX_OPENGL_OPENGLES_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGLES()

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

typedef GLfloat GLdouble;

#	ifdef GL_AMD_debug_output
#		undef GL_AMD_debug_output
#	endif

// Unreal tokens that maps to different OpenGL tokens by platform.
#	undef UGL_ABGR8
#		define UGL_ABGR8 GL_UNSIGNED_BYTE
#	undef UGL_ANY_SAMPLES_PASSED
#		define UGL_ANY_SAMPLES_PASSED	GL_ANY_SAMPLES_PASSED
#	undef UGL_CLAMP_TO_BORDER
#		define UGL_CLAMP_TO_BORDER GL_CLAMP_TO_EDGE
#	undef UGL_TIME_ELAPSED
#		define UGL_TIME_ELAPSED GL_TIME_ELAPSED_EXT

// Official OpenGL definitions 
#	ifndef GL_FILL
#		define GL_FILL 0x1B02
#	endif
#	ifndef GL_GEOMETRY_SHADER
#		define GL_GEOMETRY_SHADER 0x8DD9
#	endif
#	ifndef GL_SAMPLER_1D
#		define GL_SAMPLER_1D 0x8B5D
#	endif
#	ifndef GL_SAMPLER_1D_SHADOW
#		define GL_SAMPLER_1D_SHADOW 0x8B61
#	endif
#	ifndef GL_DOUBLE
#		define GL_DOUBLE 0x140A
#	endif
#	ifndef GL_BGRA
#		define GL_BGRA	GL_BGRA_EXT
#	endif
#	ifndef GL_TEXTURE_BUFFER
#		define GL_TEXTURE_BUFFER 0x8C2A
#	endif
#	ifndef GL_R8
#		define GL_R8 GL_R8_EXT
#	endif
#	ifndef GL_RG8
#		define GL_RG8 GL_RG8_EXT
#	endif
#	ifndef GL_RGB8
#		define GL_RGB8 GL_RGB
#	endif
#	ifndef GL_RGBA8
#		define GL_RGBA8 GL_RGBA
#	endif
#	ifndef GL_RG16
#		define GL_RG16 GL_RG16_EXT
#	endif
#	ifndef GL_R16
#		define GL_R16 GL_R16_EXT
#	endif
#	ifndef GL_RGBA16
#		define GL_RGBA16 GL_RGBA16_EXT
#	endif
#	ifndef GL_R32F
#		define GL_R32F GL_R32F_EXT
#	endif
#	ifndef GL_RG32F
#		define GL_RG32F GL_RG32F_EXT
#	endif
#	ifndef GL_RGB32F
#		define GL_RGB32F GL_RGB32F_EXT
#	endif
#	ifndef GL_TEXTURE_BUFFER_OFFSET_ALIGNMENT
#		define GL_TEXTURE_BUFFER_OFFSET_ALIGNMENT 0x919F
#	endif
#	ifndef GL_POLYGON_OFFSET_LINE
#		define GL_POLYGON_OFFSET_LINE 0x2A02
#	endif
#	ifndef GL_POLYGON_OFFSET_POINT
#		define GL_POLYGON_OFFSET_POINT 0x2A01
#	endif
#	ifndef GL_TEXTURE_LOD_BIAS
#		define GL_TEXTURE_LOD_BIAS 0x8501
#	endif
#	ifndef GL_FRAMEBUFFER_SRGB
#		define GL_FRAMEBUFFER_SRGB 0x8DB9
#	endif
#	ifndef GL_SAMPLES_PASSED
#		define GL_SAMPLES_PASSED 0x8914
#	endif
#	ifndef GL_POINT
#		define GL_POINT 0x1B00
#	endif
#	ifndef GL_LINE
#		define GL_LINE 0x1B01
#	endif
#	ifndef GL_TEXTURE_1D
#		define GL_TEXTURE_1D 0x0DE0
#	endif
#	ifndef GL_TEXTURE_1D_ARRAY
#		define GL_TEXTURE_1D_ARRAY 0x8C18
#	endif
#	ifndef GL_TEXTURE_RECTANGLE
#		define GL_TEXTURE_RECTANGLE 0x84F5
#	endif

// For the shader stage bits that don't exist just use 0
#	define GL_GEOMETRY_SHADER_BIT 0x00000000
#	define GL_TESS_CONTROL_SHADER_BIT 0x00000000
#	define GL_TESS_EVALUATION_SHADER_BIT 0x00000000

// Normalize debug macros due to naming differences across GL versions
#	if defined(GL_KHR_debug) && GL_KHR_debug
#		define GL_DEBUG_SOURCE_OTHER_ARB GL_DEBUG_SOURCE_OTHER_KHR
#		define GL_DEBUG_SOURCE_API_ARB GL_DEBUG_SOURCE_API_KHR
#		define GL_DEBUG_TYPE_ERROR_ARB GL_DEBUG_TYPE_ERROR_KHR
#		define GL_DEBUG_TYPE_OTHER_ARB GL_DEBUG_TYPE_OTHER_KHR
#		define GL_DEBUG_TYPE_MARKER GL_DEBUG_TYPE_MARKER_KHR
#		define GL_DEBUG_TYPE_PUSH_GROUP GL_DEBUG_TYPE_PUSH_GROUP_KHR
#		define GL_DEBUG_TYPE_POP_GROUP GL_DEBUG_TYPE_POP_GROUP_KHR
#		define GL_DEBUG_SEVERITY_HIGH_ARB GL_DEBUG_SEVERITY_HIGH_KHR
#		define GL_DEBUG_SEVERITY_LOW_ARB GL_DEBUG_SEVERITY_LOW_KHR
#		define GL_DEBUG_SEVERITY_NOTIFICATION GL_DEBUG_SEVERITY_NOTIFICATION_KHR
#	endif

#endif //IsGFXOpenGLES()
#endif //FXDENGINE_GFX_OPENGL_OPENGLES_H