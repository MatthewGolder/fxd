// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_OPENGL_VERTEXDECOGL_H
#define FXDENGINE_GFX_OPENGL_VERTEXDECOGL_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/VertexDec.h"
#include "FXDEngine/GFX/OpenGL/OpenGL.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IVertexDecleration >
			// -
			// ------
			template <>
			class Impl< GFX::IVertexDecleration >
			{
			public:
				friend class GFX::IVertexDecleration;

			public:
				Impl(void);
				~Impl(void);
			};
		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXOpenGL()
#endif //FXDENGINE_GFX_OPENGL_VERTEXDECOGL_H