// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_OPENGL_GPUMEMORYOGL_H
#define FXDENGINE_GFX_OPENGL_GPUMEMORYOGL_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/GfxMemory.h"
#include "FXDEngine/GFX/OpenGL/OpenGL.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::MemoryHeapSpec >
			// -
			// ------
			template <>
			class Impl< GFX::MemoryHeapSpec >
			{
			public:
				friend class GFX::MemoryHeapSpec;

			public:
				Impl(void);
				~Impl(void);

			private:
				FXD::U32 m_nMemTypeIdx;
			};

			// ------
			// Impl< GFX::ResourceSet >
			// -
			// ------
			template <>
			class Impl< GFX::ResourceSet >
			{
			public:
				friend class GFX::ResourceSet;

			public:
				Impl(void);
				~Impl(void);

			private:
				GFX::AllocSpec m_allocInfo;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXOpenGL()
#endif //FXDENGINE_GFX_OPENGL_GPUMEMORYOGL_H