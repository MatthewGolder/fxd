// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_OPENGL_SWAPCHAINOGL_H
#define FXDENGINE_GFX_OPENGL_SWAPCHAINOGL_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/SwapChain.h"
#include "FXDEngine/GFX/OpenGL/OpenGL.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::ISwapChain >
			// -
			// ------
			template <>
			class Impl< GFX::ISwapChain >
			{
			public:
				friend class GFX::ISwapChain;

			public:
				Impl(void);
				~Impl(void);

			protected:
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXOpenGL()
#endif //FXDENGINE_GFX_OPENGL_SWAPCHAINOGL_H