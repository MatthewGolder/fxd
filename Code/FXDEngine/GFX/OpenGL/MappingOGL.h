// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_OPENGL_MAPPINGOGL_H
#define FXDENGINE_GFX_OPENGL_MAPPINGOGL_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXOpenGL()
#include "FXDEngine/GFX/OpenGL/OpenGL.h"

namespace FXD
{
	namespace GFX
	{
		namespace OGL
		{
			extern bool CheckExtension(const FXD::UTF8* pExtension);
			extern void PrintExtension(void);

			extern GLenum GetPrimitiveType(GFX::E_Primitive eType);
			extern GLenum GetFillMode(GFX::E_FillMode eMode);
			extern GLenum GetCullMode(GFX::E_CullMode eMode);
		}
		namespace Buffer
		{
			extern const GLenum ConvertToOGLBindFlags(GFX::E_BufferUsageFlags eFlags);
			//extern const D3D11_CPU_ACCESS_FLAG ConvertToDX11CPUAccessFlags(GFX::E_BufferUsageFlags eFlags);
			extern const GLenum ConvertToOGLUsage(GFX::E_BufferUsageFlags eFlags);
		}
		namespace Pixel
		{
			extern const GLuint GetFormat(GFX::E_PixelFormat eFormat);
		}
		namespace Index
		{
			extern const GLuint GetFormat(GFX::E_IndexType eType);
		}
		namespace Vertex
		{
			extern const GLuint GetFormat(GFX::E_VertexElementType eType);
		}

	} //namespace GFX
} //namespace FXD
#endif //IsGFXOpenGL()
#endif //FXDENGINE_GFX_OPENGL_MAPPINGOGL_H