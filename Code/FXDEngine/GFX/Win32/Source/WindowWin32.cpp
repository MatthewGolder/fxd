// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsOSPC()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/App/Win32/GlobalsWin32.h"
#include "FXDEngine/Core/Win32/Win32.h"
#include "FXDEngine/GFX/Events.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/Win32/WindowWin32.h"
#include "FXDEngine/Job/EventHandler.h"

using namespace FXD;
using namespace GFX;

DWORD CalcStyle(bool bFullScreen, bool bResizable)
{
	DWORD style = 0;
	if (bFullScreen)
	{
		style = (WS_CLIPSIBLINGS | WS_GROUP | WS_TABSTOP | WS_VISIBLE);
	}
	else
	{
		style = (WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_CLIPSIBLINGS | WS_BORDER | WS_DLGFRAME | WS_GROUP | WS_VISIBLE);
		if (bResizable)
		{
			style |= (WS_SIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_TABSTOP);
		}
	}
	return style;
}


// ------
// IBuffer::Impl::IWindow
// - 
// ------
IWindow::Impl::Impl::Impl(void)
	: m_pos(Math::Vector2I::kZero)
{
	Memory::MemZero(&m_hWindow, sizeof(HWND));
	Memory::MemZero_T(m_style);
}

IWindow::Impl::~Impl(void)
{
	PRINT_COND_ASSERT(!m_hWindow, "GFX: m_hWindow has not been shutdown");
}

// ------
// IWindow
// - 
// ------
IWindow::IWindow(WINDOW_DESC windowDesc, const GFX::Window& parent)
	: m_windowDesc(windowDesc)
	, OnWindowEvent(nullptr)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	OnWindowEvent = FXDEvent()->attach< GFX::OnWindowEvent >([&](auto evt)
	{
		if (evt.WindowHandle == windowHandle())
		{
			_notify_window_event(evt.Event);
		}
	});
}

IWindow::~IWindow(void)
{
	if (OnWindowEvent != nullptr)
	{
		FXDEvent()->detach(OnWindowEvent);
		OnWindowEvent = nullptr;
	}
}

void IWindow::move(FXD::U32 nLeft, FXD::U32 nTop)
{
	if (m_impl->m_hWindow)
	{
		m_impl->m_pos.set(nLeft, nTop);
		SetWindowPos(m_impl->m_hWindow, HWND_TOP, m_impl->m_pos.x(), m_impl->m_pos.y(), m_windowDesc.size.x(), m_windowDesc.size.y(), SWP_NOSIZE);
	}
}

void IWindow::resize(FXD::U32 nWidth, FXD::U32 nHeight)
{
	if (m_impl->m_hWindow)
	{
		RECT rect =
		{
			0,
			0,
			(LONG)nWidth,
			(LONG)nHeight
		};
		AdjustWindowRect(&rect, GetWindowLong(m_impl->m_hWindow, GWL_STYLE), false);
		nWidth = (rect.right - rect.left);
		nHeight = (rect.bottom - rect.top);

		m_windowDesc.size.set(nWidth, nHeight);
		SetWindowPos(m_impl->m_hWindow, HWND_TOP, m_impl->m_pos.x(), m_impl->m_pos.y(), m_windowDesc.size.x(), m_windowDesc.size.y(), SWP_NOSIZE);
	}
}

FXD::U64 IWindow::windowHandle(void) const
{
	return (FXD::U64)m_impl->m_hWindow;
}

bool IWindow::init(void)
{
	// 1. Construct the window class
	m_impl->m_style = CalcStyle(m_windowDesc.bFullscreen, m_windowDesc.bResizable);

	RECT rect =
	{
		m_impl->m_pos.x(),
		m_impl->m_pos.y(),
		m_windowDesc.size.x(),
		m_windowDesc.size.y()
	};
	AdjustWindowRect(&rect, m_impl->m_style, FALSE);

	const FXD::U32 nX = (rect.left <= 0) ? CW_USEDEFAULT : rect.left;
	const FXD::U32 nY = (rect.top <= 0) ? CW_USEDEFAULT : rect.top;
	const FXD::U32 nWidth = ((rect.right - rect.left) <= 0) ? CW_USEDEFAULT : (rect.right - rect.left);
	const FXD::U32 nHeight = ((rect.bottom - rect.top) <= 0) ? CW_USEDEFAULT : (rect.bottom - rect.top);

	// 2. Create the screen
	HINSTANCE hInstance = GetModuleHandleW(nullptr);
	m_impl->m_hWindow = CreateWindowExW(WS_EX_APPWINDOW, L"FXDWindow", (const wchar_t*)App::GetDisplayGameNameUTF16().c_str(), m_impl->m_style, nX, nY, nWidth, nHeight, nullptr, nullptr, hInstance, this);
	if (!m_impl->m_hWindow)
	{
		PRINT_ASSERT << "GFX: Failed CreateWindowExW";
		return false;
	}
	_window_moved_or_resized();

	// 3. Update the screen
	set_fullscreen(nWidth, nHeight, m_windowDesc.bFullscreen);

	return true;
}

void IWindow::close(void)
{
	if (m_impl->m_hWindow)
	{
		DestroyWindow(m_impl->m_hWindow);
		Memory::MemZero(&m_impl->m_hWindow, sizeof(HWND));
		Memory::MemZero_T(m_impl->m_style);
	}
}

void IWindow::set_fullscreen(FXD::U32 nWidth, FXD::U32 nHeight, bool bFullscreen)
{
	//if (mIsChild && bFullscreen)
	//{
	//	return;
	//}

	m_windowDesc.bFullscreen = bFullscreen;
	m_impl->m_style = CalcStyle(m_windowDesc.bFullscreen, m_windowDesc.bResizable);

	SetWindowLong(m_impl->m_hWindow, GWL_STYLE, m_impl->m_style);

	if (m_windowDesc.bFullscreen)
	{
		DEVMODE devMode = {};
		Memory::MemZero_T(devMode);
		devMode.dmBitsPerPel = 32;
		devMode.dmPelsWidth = nWidth;
		devMode.dmPelsHeight = nHeight;
		devMode.dmDisplayFrequency = m_windowDesc.nRefreshRate;
		devMode.dmFields = (DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT | DM_DISPLAYFREQUENCY);

		HMONITOR hMonitor = MonitorFromWindow(m_impl->m_hWindow, MONITOR_DEFAULTTONULL);
		MONITORINFOEX monitorInfo = {};
		Memory::MemZero_T(monitorInfo);
		monitorInfo.cbSize = sizeof(MONITORINFO);
		GetMonitorInfo(hMonitor, &monitorInfo);

		if (ChangeDisplaySettingsEx(monitorInfo.szDevice, &devMode, NULL, CDS_FULLSCREEN, NULL) != DISP_CHANGE_SUCCESSFUL)
		{
			PRINT_ASSERT << "GFX: Failed ChangeDisplaySettingsEx";
		}

		const FXD::U32 nLeft = monitorInfo.rcMonitor.left;
		const FXD::U32 nTop = monitorInfo.rcMonitor.top;
		SetWindowPos(m_impl->m_hWindow, HWND_TOP, nLeft, nTop, nWidth, nHeight, (SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER));
	}
	else
	{
		const FXD::U32 nLeft = m_impl->m_pos.x();
		const FXD::U32 nTop = m_impl->m_pos.y();
		nWidth = m_windowDesc.size.x();
		nHeight = m_windowDesc.size.y();
		SetWindowPos(m_impl->m_hWindow, HWND_TOP, nLeft, nTop, nWidth, nHeight, (SWP_FRAMECHANGED | SWP_NOZORDER | SWP_NOOWNERZORDER));
	}
	_window_moved_or_resized();
}

void IWindow::_notify_window_event(GFX::E_WindowEvent eEvent)
{
	switch (eEvent)
	{
		case GFX::E_WindowEvent::Resized:
		{
			_window_moved_or_resized();
			FXDEvent()->raise(GFX::OnWindowResize(windowHandle(), m_windowDesc.size));
			break;
		}
		case GFX::E_WindowEvent::Moved:
		{
			_window_moved_or_resized();
			break;
		}
		case GFX::E_WindowEvent::FocusReceived:
		{
			m_windowDesc.bFocused = true;
			break;
		}
		case GFX::E_WindowEvent::FocusLost:
		{
			m_windowDesc.bFocused = false;
			break;
		}
		case GFX::E_WindowEvent::CloseRequested:
		{
			break;
		}
	}
}

void IWindow::_window_moved_or_resized(void)
{
	if (!m_impl->m_hWindow || IsIconic(m_impl->m_hWindow))
	{
		return;
	}

	RECT rect = {};
	GetWindowRect(m_impl->m_hWindow, &rect);
	m_impl->m_pos.x(rect.top);
	m_impl->m_pos.y(rect.left);

	GetClientRect(m_impl->m_hWindow, &rect);
	m_windowDesc.size.x(rect.right - rect.left);
	m_windowDesc.size.y(rect.bottom - rect.top);
}
#endif //IsOSPC()