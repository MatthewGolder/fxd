// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsOSPC()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/GfxAdapter.h"
#include "FXDEngine/GFX/GfxSystem.h"

using namespace FXD;
using namespace GFX;

// ------
// IGfxSystem
// - 
// ------
bool IGfxSystem::_create_system(void)
{
	// 1. Early out if the api already exists
	if (m_gfxApi)
	{
		return false;
	}

	// 2. Create the desired api
	m_gfxApi = std::make_unique< GFX::IGfxApi >();
	bool bRetVal = m_gfxApi->create_api();
	return bRetVal;
}

bool IGfxSystem::_release_system(void)
{
	if (m_gfxApi)
	{
		m_gfxApi->release_api();
		m_gfxApi = nullptr;
	}
	return true;
}

GFX::GfxAdapter IGfxSystem::_create_adapter(const App::AdapterDesc& adapterDesc)
{
	// 1. Create the desired adapter
	GFX::GfxAdapter gfxAdapter = std::make_shared< GFX::IGfxAdapter >(adapterDesc, m_gfxApi.get());
	bool bRetVal = gfxAdapter->create().get();
	if (bRetVal == false)
	{
		return nullptr;
	}

	m_gfxAdapters.push_back(gfxAdapter);
	return gfxAdapter;
}

bool IGfxSystem::_release_adapter(GFX::GfxAdapter& gfxAdapter)
{
	if (gfxAdapter)
	{
		gfxAdapter->release().get();
		m_gfxAdapters.find_erase(gfxAdapter);
	}
	return true;
}
#endif //IsOSPC()