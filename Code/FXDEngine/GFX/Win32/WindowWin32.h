// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_WIN32_WINDOWWIN32_H
#define FXDENGINE_GFX_WIN32_WINDOWWIN32_H

#include "FXDEngine/GFX/Types.h"

#if IsOSPC()
#include "FXDEngine/GFX/Window.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IWindow >
			// -
			// ------
			template <>
			class Impl< GFX::IWindow >
			{
			public:
				friend class GFX::IWindow;

			public:
				Impl(void);
				~Impl(void);

			public:
				HWND m_hWindow;
				DWORD m_style;
				Math::Vector2I m_pos;
			};
		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsOSPC()
#endif //FXDENGINE_GFX_WIN32_WINDOWWIN32_H