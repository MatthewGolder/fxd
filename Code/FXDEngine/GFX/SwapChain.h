// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_SWAPCHAIN_H
#define FXDENGINE_GFX_SWAPCHAIN_H

#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/GFX/Types.h"

namespace FXD
{
	namespace GFX
	{
		// ------
		// ISwapChain
		// -
		// ------
		class ISwapChain : public Core::HasImpl< GFX::ISwapChain, 128 >, public Core::NonCopyable, public std::enable_shared_from_this< GFX::ISwapChain >
		{
		public:
			ISwapChain(GFX::IGfxAdapter* pAdapter, GFX::IViewport* pViewport);
			virtual ~ISwapChain(void);

			bool create(void);
			bool release(void);

			bool resize(const Math::Vector2I newSize);
			void swap(bool bVSync);

			GET_R(GFX::E_PixelFormat, eColorFormat, color_format);
			GET_R(GFX::E_PixelFormat, eDepthFormat, depth_format);
			GET_R(GFX::RenderTarget, renderTarget, render_target);

		protected:
			bool _create(void);
			bool _release(void);

			bool _resize(const Math::Vector2I newSize);
			void _swap(bool bVSync);

		protected:
			GFX::IGfxAdapter* m_pAdapter;
			GFX::IViewport* m_pViewport;
			GFX::E_PixelFormat m_eColorFormat;
			GFX::E_PixelFormat m_eDepthFormat;
			GFX::RenderTarget m_renderTarget;
		};

	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_SWAPCHAIN_H