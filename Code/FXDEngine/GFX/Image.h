// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_IMAGE_H
#define FXDENGINE_GFX_IMAGE_H

#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/GFX/Types.h"
#include "FXDEngine/GFX/GfxMemory.h"
#include "FXDEngine/Job/Future.h"

namespace FXD
{
	namespace GFX
	{
		struct IMAGE_DESC
		{
			FXD::U32 Width = 1;													// Width of the texture in pixels.
			FXD::U32 Height = 1;													// Height of the texture in pixels.
			FXD::U32 Depth = 1;													// Depth of the texture in pixels (Must be 1 for 2D textures).
			FXD::U16 NumMips = 1;												// Number of mip-maps the texture has. This number excludes the full resolution map.
			FXD::U16 NumSamples = 0;											// Number of samples per pixel. Set to 1 or 0 to use the default of a single sample per pixel.
			FXD::U16 NumArraySlices = 1;										// Number of texture slices to create if creating a texture array. Ignored for 3D textures.
			GFX::E_ImageType Type = GFX::E_ImageType::Two;				// Type of the texture.
			GFX::E_PixelFormat Format = GFX::E_PixelFormat::RGBA8;	// Format of pixels in the texture.
			GFX::E_BufferUsageFlags Usage;									// Describes how the caller plans on using the texture in the pipeline.

			FXD::U32 num_faces(void) const
			{
				const FXD::U32 nFacesPerSlice = (Type == GFX::E_ImageType::Cube) ? 6 : 1;
				return (nFacesPerSlice * NumArraySlices);
			}
		};

		struct IMAGEVIEW_DESC
		{
			FXD::U32 MostDetailMip;		// First mip level of the parent texture the view binds (0 - base level). This applied to all array slices specified below.
			FXD::U32 NumMips;				// Number of mip levels to bind to the view. This applied to all array slices specified below.
			FXD::U32 FirstArraySlice;	// First array slice the view binds to. This will be array index for 1D and 2D array textures, texture slice index for 3D textures, and face index for cube textures(cube index * 6).
			FXD::U32 NumArraySlices;	// Number of array slices to bind tot he view. This will be number of array elements for 1D and 2D array textures, number of slices for 3D textures, and number of cubes for cube textures.
		};

		// ------
		// IImageView
		// -
		// ------
		class IImageView : public Core::HasImpl< GFX::IImageView, 32 >
		{
		public:
			IImageView(GFX::IGfxAdapter* pAdapter, GFX::IImage* pImage);
			virtual ~IImageView(void);

			Job::Future< bool > create(const GFX::IMAGEVIEW_DESC& imageViewDesc);
			Job::Future< bool > release(void);

			GET_REF_R(GFX::IMAGEVIEW_DESC, imageViewDesc, image_view_desc);

		protected:
			bool _create(const GFX::IMAGEVIEW_DESC& imageViewDesc);
			bool _release(void);

		protected:
			GFX::IGfxAdapter* m_pAdapter;
			GFX::IImage* m_pImage;
			GFX::IMAGEVIEW_DESC m_imageViewDesc;
		};


		// ------
		// IImage
		// -
		// ------
		class IImage : public Core::HasImpl< GFX::IImage, 64 >
		{
		public:
			IImage(GFX::IGfxAdapter* pAdapter);
			virtual ~IImage(void);

			Job::Future< bool > create(const GFX::IMAGE_DESC& imageDesc);
			Job::Future< bool > create(const GFX::Texture& texture, GFX::E_BufferUsageFlags eUsage);
			Job::Future< bool > create(const GFX::SwapChain& swapChain, FXD::U32 nBuffer);
			Job::Future< bool > release(void);

			GET_REF_R(GFX::IMAGE_DESC, imageDesc, imageDesc);

		protected:
			bool _create(const GFX::IMAGE_DESC& imageDesc, void* pSrc);
			bool _create(const GFX::Texture& texture, GFX::E_BufferUsageFlags eUsage);
			bool _create(const GFX::SwapChain& swapChain, FXD::U32 nBuffer);
			bool _release(void);

		protected:
			GFX::IGfxAdapter* m_pAdapter;
			GFX::IMAGE_DESC m_imageDesc;
			//GFX::ResourceSet m_resourceSet;
		};

	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_IMAGE_H