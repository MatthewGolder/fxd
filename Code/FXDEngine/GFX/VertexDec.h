// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_VERTEXDEC_H
#define FXDENGINE_GFX_VERTEXDEC_H

#include "FXDEngine/Container/Array.h"
#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/GFX/FX/Types.h"
#include "FXDEngine/GFX/FX/Semantics.h"
#include "FXDEngine/Job/Future.h"
#include "FXDEngine/Memory/MemHandle.h"

namespace FXD
{
	namespace GFX
	{
		struct FVertexElement
		{
			FXD::U8 StreamIndex = 0;
			FXD::U8 Offset = 0;
			GFX::E_VertexElementType Type = GFX::E_VertexElementType::Unknown;
			FXD::U8 AttributeIndex = 0;
			FXD::U16 Stride = 0;
		};
		typedef Container::Array< FVertexElement, GFX::kMaxVertexElements > VertexElements;

		struct FVertexElements
		{
			GFX::VertexElements m_vertexElements;
		};

		// ------
		// IVertexDecleration
		// -
		// ------
		class IVertexDecleration : public Core::HasImpl< GFX::IVertexDecleration, 768 >, public Core::NonCopyable
		{
		public:
			IVertexDecleration(GFX::IGfxAdapter* pAdapter);
			virtual ~IVertexDecleration(void);

			Job::Future< bool > create(const FX::VaryingListDef& varyingList, const Memory::MemHandle& mem);
			Job::Future< bool > release(void);
			Job::Future< void > attach(void);
			Job::Future< void > detach(void);

		protected:
			bool _create(const FX::VaryingListDef& varyingList, const Memory::MemHandle& mem);
			bool _release(void);
			void _attach(void);
			void _detach(void);

		protected:
			GFX::IGfxAdapter* m_pAdapter;
			GFX::VertexElements m_vertexElements;
		};

	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_VERTEXDEC_H