// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

using namespace FXD;
using namespace GFX;

// ------
// GFXVideoMode
// - 
// ------
/*
GFXVideoMode::GFXVideoMode(void)
	: bitDepth(32)
	, fullScreen(false)
	, refreshRate(60)
	, wideScreen(false)
	//, resolution(800, 600)
	, antialiasLevel(0)
{
}

GFXVideoMode::~GFXVideoMode(void)
{
}

bool GFXVideoMode::operator==(const GFXVideoMode& rhs) const
{
	if (rhs.fullScreen != fullScreen)
		return false;
	//if (rhs.resolution != resolution)
	//	return false;
	if (rhs.bitDepth != bitDepth)
		return false;
	if (rhs.refreshRate != refreshRate)
		return false;
	if (rhs.wideScreen != wideScreen)
		return false;
	if (rhs.antialiasLevel != antialiasLevel)
		return false;
	return true;
}

bool GFXVideoMode::operator!=(const GFXVideoMode& rhs) const
{
	return !(*this == rhs);
}*/