// Creator - MatthewGolder
#include "FXDEngine/GFX/GfxSystem.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/Decoders/TextureBmp.h"
#include "FXDEngine/GFX/Decoders/TextureDDS.h"
#include "FXDEngine/GFX/Decoders/TextureJpg.h"
#include "FXDEngine/GFX/Decoders/TexturePng.h"
#include "FXDEngine/GFX/Decoders/TextureTga.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Core/WatchDogThread.h"
#include "FXDEngine/IO/IODevice.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOSystem.h"
#include "FXDEngine/Job/Async.h"
#include "FXDEngine/Memory/StreamMemHandle.h"
#include "FXDEngine/Scene/Assets/GeoCollada.h"

#if IsOSPC()
#include "FXDEngine/App/Win32/EngineAppWin32.h"
#endif

using namespace FXD;
using namespace GFX;

Scene::Geo GeoLoadFunc(const IO::Path& ioPath)
{
	// 1. Open the geo file.
	auto futStream = FXDIO()->io_system()->io_device()->file_open_memory_stream(ioPath, false, { });

	auto futGeo = futStream.then(FXDGFX(), [&](auto& fut)
	{
		Core::String8 strFileName = ioPath.filename();
		Core::StreamIn stream = fut.get();

		// 2. Parse the geo file.
		Scene::Geo geo;
		if (strFileName.ends_with_no_case(UTF_8(".dae")))
		{
			geo = std::make_shared< Scene::IGeoCollada >(stream, strFileName);
		}
		else
		{
			PRINT_WARN << "GFX: Unsupported file format";
		}
		return geo;
	});
	return futGeo.get();
}


GFX::Texture ImageLoadFunc(const IO::Path& ioPath)
{
	// 1. Open the texture file.
	auto futStream = FXDIO()->io_system()->io_device()->file_open_memory_stream(ioPath, false, { });

	auto futTex = futStream.then(FXDGFX(), [&](auto& fut)
	{
		Core::String8 strFileName = ioPath.filename();
		Core::StreamIn stream = fut.get();

		// 2. Parse the texture file.
		GFX::Texture texture;
		if (strFileName.ends_with_no_case(UTF_8(".bmp")))
		{
			texture = std::make_shared< GFX::ITextureBmp >(stream, strFileName);
		}
		else if (strFileName.ends_with_no_case(UTF_8(".dds")))
		{
			texture = std::make_shared< GFX::ITextureDDS >(stream, strFileName);
		}
		else if (strFileName.ends_with_no_case(UTF_8(".jpg")))
		{
			texture = std::make_shared< GFX::ITextureJpg >(stream, strFileName);
		}
		else if (strFileName.ends_with_no_case(UTF_8(".png")))
		{
			texture = std::make_shared< GFX::ITexturePng >(stream, strFileName);
		}
		else if (strFileName.ends_with_no_case(UTF_8(".tga")))
		{
			texture = std::make_shared< GFX::ITextureTga >(stream, strFileName);
		}
		else
		{
			PRINT_WARN << "GFX: Unsupported file format";
		}
		return texture;
	});
	return futTex.get();
}

// ------
// IGfxSystem
// - 
// ------
IGfxSystem::IGfxSystem(void)
	: m_gfxApi(nullptr)
{
}

IGfxSystem::~IGfxSystem(void)
{
	PRINT_COND_ASSERT((m_gfxApi == nullptr), "GFX: m_gfxApi is not shutdown");
	PRINT_COND_ASSERT((m_gfxAdapters.size() == 0), "GFX: m_gfxAdapters are not shutdown");
}

Job::Future< bool > IGfxSystem::create_system(void)
{
	auto funcRef = [&](void) { return _create_system(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< bool > IGfxSystem::release_system(void)
{
	auto funcRef = [&](void) { return _release_system(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< GFX::GfxAdapter > IGfxSystem::create_adapter(const App::AdapterDesc& adapterDesc)
{
	auto funcRef = [&](const App::AdapterDesc& adapterDesc) { return _create_adapter(adapterDesc); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(adapterDesc); });
}

Job::Future< bool > IGfxSystem::release_adapter(GFX::GfxAdapter& gfxAdapter)
{
	auto funcRef = [&](::GfxAdapter gfxAdapter) { return _release_adapter(gfxAdapter); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(gfxAdapter); });
}

Job::Future< Scene::Geo > IGfxSystem::load_geo(const IO::Path& ioPath)
{
	auto funcRef = [&](const IO::Path& ioPath) { return geo_cache().load(ioPath, GeoLoadFunc); };

	return Job::Async(FXDAsync(), [=]() { return funcRef(ioPath); });
}

Job::Future< GFX::Texture > IGfxSystem::load_texture(const IO::Path& ioPath)
{
	auto funcRef = [&](const IO::Path& ioPath) { return texture_cache().load(ioPath, ImageLoadFunc); };

	return Job::Async(FXDAsync(), [=]() { return funcRef(ioPath); });
}

void IGfxSystem::load_geo_async(const IO::Path& ioPath, FXD::STD::function< void(Scene::Geo) > callback)
{
	callback(load_geo(ioPath).get());
}

void IGfxSystem::load_texture_async(const IO::Path& ioPath, FXD::STD::function< void(GFX::Texture) > callback)
{
	callback(load_texture(ioPath).get());
}

void IGfxSystem::_process(FXD::F32 /*dt*/)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

#if defined(FXD_ENABLE_WATCHDOG)
	static Core::WatchDogThread sWDThread("GfxSystem - WatchDog", 500);
	sWDThread.increment();
#endif

#if IsOSPC()
	{
		MSG msg = { 0 };
		bool bWait = true;
		FXD::S32 nCount = 10;
		while (nCount-- && PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			bWait = false;
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			if (msg.message == WM_QUIT)
			{
				FXD::FXDApp()->impl().trigger_shutdown_actual();
			}
		}
		if (bWait)
		{
			Thread::Funcs::Sleep(0);
		}
	}
#endif
	geo_cache().remove_unique();
	texture_cache().remove_unique();
}