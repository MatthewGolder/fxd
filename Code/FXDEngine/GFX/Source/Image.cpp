// Creator - MatthewGolder
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/Image.h"
#include "FXDEngine/GFX/Decoders/Texture.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace GFX;

// ------
// IImageView
// - 
// ------
Job::Future< bool > IImageView::create(const GFX::IMAGEVIEW_DESC& imageViewDesc)
{
	auto funcRef = [&](const GFX::IMAGEVIEW_DESC& desc) { return _create(desc); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(imageViewDesc); });
}

Job::Future< bool > IImageView::release(void)
{
	auto funcRef = [&](void) { return _release(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

// ------
// IImage
// - 
// ------
Job::Future< bool > IImage::create(const GFX::IMAGE_DESC& imageDesc)
{
	auto funcRef = [&](const GFX::IMAGE_DESC& imageDesc) { return _create(imageDesc, nullptr); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(imageDesc); });
}

Job::Future< bool > IImage::create(const GFX::Texture& texture, GFX::E_BufferUsageFlags eUsage)
{
	auto funcRef = [&](const GFX::Texture& texture, GFX::E_BufferUsageFlags eUsage) { return _create(texture, eUsage); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(texture, eUsage); });
}

Job::Future< bool > IImage::create(const GFX::SwapChain& swapChain, FXD::U32 nBuffer)
{
	auto funcRef = [&](const GFX::SwapChain& swapChain, FXD::U32 nBuffer) { return _create(swapChain, nBuffer); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(swapChain, nBuffer); });
}

Job::Future< bool > IImage::release(void)
{
	auto funcRef = [&](void) { return _release(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

bool IImage::_create(const GFX::Texture& texture, GFX::E_BufferUsageFlags eUsage)
{
	GFX::IMAGE_DESC desc = {};
	desc.Width = texture->width();
	desc.Height = texture->height();
	desc.NumMips = texture->numMips();
	//desc.Depth = texture->depth();
	desc.Type = GFX::E_ImageType::Two;
	desc.Format = texture->format();
	desc.Usage = eUsage;

	Memory::MemHandle mem = Memory::MemHandle::allocate(texture->rowByteIncrement() * texture->height());
	Memory::MemHandle::LockGuard lock(mem);
	FXD::U8* pMem = (FXD::U8*)lock.get_mem();
	texture->read(pMem);

	return _create(desc, pMem);
}