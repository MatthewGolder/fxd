// Creator - MatthewGolder
#include "FXDEngine/GFX/WindowManager.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/Window.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace GFX;

// ------
// IWindowManager
// -
// ------
GFX::Window IWindowManager::create_window(const GFX::WINDOW_DESC& windowDesc, const GFX::Window& parent)
{
	auto funcRef = [&](const GFX::WINDOW_DESC& windowDesc, const GFX::Window& parent) { return _create_window(windowDesc, parent); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(windowDesc, parent); }).get();
}

void IWindowManager::destroy_window(GFX::Window& window)
{
	auto funcRef = [&](GFX::Window window) { _destroy_window(window); };

	Job::Async(FXDGFX(), [=]() { funcRef(window); }).wait();
}