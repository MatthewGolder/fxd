// Creator - MatthewGolder
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/Fence.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace GFX;

// ------
// IFence
// - 
// ------
Job::Future< bool > IFence::create(bool bSignaled)
{
	auto funcRef = [&](bool bSignaled) { return _create(bSignaled); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(bSignaled); });
}

Job::Future< bool > IFence::release(void)
{
	auto funcRef = [&](void) { return _release(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

bool IFence::is_signalled(void) const
{
	auto funcRef = [&](void) { return _is_signalled(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); }).get();
}

bool IFence::wait(void)
{
	auto funcRef = [&](void) { return _wait(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); }).get();
}

bool IFence::reset(void)
{
	auto funcRef = [&](void) { return _reset(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); }).get();
}