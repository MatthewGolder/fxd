// Creator - MatthewGolder
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/GfxAdapter.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/Viewport.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace GFX;

// ------
// IViewport
// -
// ------
Job::Future< bool > IViewport::create(void)
{
	auto funcRef = [&](void) { return _create(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< bool > IViewport::release(void)
{
	auto funcRef = [&](void) { return _release(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

bool IViewport::resize(const Math::Vector2I newSize)
{
	auto funcRef = [&](const Math::Vector2I newSize) { return _resize(newSize); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(newSize); }).get();
}

/*
FXD::F32 IViewport::aspectRatio(void) const
{
	return (size().x() / size().y());
}

Math::Box2D IViewport::bounds(void) const
{
	return Math::Box2D();
}

Math::Box2D IViewport::safeBounds(void) const
{
	return Math::Box2D();
}
*/

void IViewport::_update(FXD::F32 /*dt*/)
{
}