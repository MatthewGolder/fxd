// Creator - MatthewGolder
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/RenderTarget.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace GFX;

// ------
// IRenderTarget
// -
// ------
Job::Future< bool > IRenderTarget::create(const GFX::RENDER_TEXTURE_DESC& rtDesc)
{
	auto funcRef = [&](const GFX::RENDER_TEXTURE_DESC& desc) { return _create(desc); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(rtDesc); });
}

Job::Future< bool > IRenderTarget::release(void)
{
	auto funcRef = [&](void)
	{
		return _release();
	};

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

bool IRenderTarget::activate_and_clear(const Core::ColourRGBA& colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil) const
{
	auto funcRef = [&](const Core::ColourRGBA& colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil) { return _activate_and_clear(colour, eClearFlags, fZ, fDepth, nStencil); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(colour, eClearFlags, fZ, fDepth, nStencil); }).get();
}

bool IRenderTarget::activate(void) const
{
	auto funcRef = [&](void) { return _activate(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); }).get();
}

bool IRenderTarget::clear(const Core::ColourRGBA& colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil) const
{
	auto funcRef = [&](const Core::ColourRGBA& colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil) { return _clear(colour, eClearFlags, fZ, fDepth, nStencil); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(colour, eClearFlags, fZ, fDepth, nStencil); }).get();
}


// ------
// Funcs
// -
// ------
void FXD::GFX::Funcs::ValidateExtents(const GFX::RENDER_TEXTURE_DESC& rtDesc, GFX::RENDER_TARGET_EXTENT& rtExtent)
{
	bool bSetExtent = false;
	for (FXD::U16 n1 = 0; n1 < GFX::kMaxMultipleRenderTargets; n1++)
	{
		const GFX::RENDER_SURFACE_DESC& surfaceDesc = rtDesc.ColorSurfaces[n1];
		if (surfaceDesc.Image == nullptr)
		{
			continue;
		}

		const GFX::IMAGE_DESC& imageDesc = surfaceDesc.Image->imageDesc();
		if (bSetExtent)
		{
			PRINT_COND_ASSERT((rtExtent.Width == FXD::STD::Max(1u, imageDesc.Width >> surfaceDesc.MipLevel)), "GFX: RenderTarget Mip level height is incorrect");;
			PRINT_COND_ASSERT((rtExtent.Height == FXD::STD::Max(1u, imageDesc.Height >> surfaceDesc.MipLevel)), "GFX: RenderTarget Mip level width is incorrect");
			PRINT_COND_ASSERT((rtExtent.Depth == imageDesc.Depth), "");
		}
		else
		{
			bSetExtent = true;
			rtExtent.Width = FXD::STD::Max(1u, imageDesc.Width >> surfaceDesc.MipLevel);
			rtExtent.Height = FXD::STD::Max(1u, imageDesc.Height >> surfaceDesc.MipLevel);
			rtExtent.Depth = imageDesc.Depth;
		}
	}

	{
		const GFX::RENDER_SURFACE_DESC& surfaceDesc = rtDesc.DepthStencilSurface;
		if (surfaceDesc.Image != nullptr)
		{
			const GFX::IMAGE_DESC& imageDesc = surfaceDesc.Image->imageDesc();
			if (!imageDesc.Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL))
			{
				PRINT_ASSERT << "GFX: Provided texture is not created with depth stencil usage";
			}

			if (bSetExtent)
			{
				// Depth can be greater or equal to color
				PRINT_COND_ASSERT((imageDesc.Width >= rtExtent.Width), "GFX: RenderTarget Mip level height is incorrect");
				PRINT_COND_ASSERT((imageDesc.Height >= rtExtent.Height), "GFX: RenderTarget Mip level width is incorrect");
			}
			else
			{
				bSetExtent = true;
				rtExtent.Width = imageDesc.Width;
				rtExtent.Height = imageDesc.Height;
				rtExtent.Depth = 1;
			}
		}
	}
}