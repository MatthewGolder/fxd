// Creator - MatthewGolder
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/VertexDec.h"
#include "FXDEngine/Job/Async.h"
//#include "FXDEngine/Container/Map.h"

using namespace FXD;
using namespace GFX;

//FXD::Container::Map< FXD::HashValue, >;

// ------
// IVertexDecleration
// -
// ------
Job::Future< bool > IVertexDecleration::create(const FX::VaryingListDef& varyingList, const Memory::MemHandle& mem)
{
	auto funcRef = [&](const FX::VaryingListDef& varyingList, const Memory::MemHandle& mem) { return _create(varyingList, mem); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(varyingList, mem); });
}

Job::Future< bool > IVertexDecleration::release(void)
{
	auto funcRef = [&](void) { return _release(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< void > IVertexDecleration::attach(void)
{
	auto funcRef = [&](void) { _attach(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< void > IVertexDecleration::detach(void)
{
	auto funcRef = [&](void) { _detach(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}