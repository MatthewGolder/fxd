// Creator - MatthewGolder
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/GfxCommandBuffer.h"
#include "FXDEngine/GFX/Fence.h"
#include "FXDEngine/GFX/GfxAdapter.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/HWBuffer.h"
#include "FXDEngine/GFX/Image.h"
#include "FXDEngine/GFX/RenderTarget.h"
#include "FXDEngine/GFX/Viewport.h"
#include "FXDEngine/GFX/FX/ShaderModule.h"
#include "FXDEngine/GFX/FX/BlendState.h"
#include "FXDEngine/GFX/FX/DepthStencilState.h"
#include "FXDEngine/GFX/FX/Pipeline.h"
#include "FXDEngine/GFX/FX/RasterState.h"
#include "FXDEngine/GFX/FX/SamplerState.h"
#include "FXDEngine/GFX/FX/StateManager.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace GFX;

// ------
// IGfxAdapter
// - 
// ------
Job::Future< bool > IGfxAdapter::create(void)
{
	auto funcRef = [&](void) { return _create(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< bool > IGfxAdapter::release(void)
{
	auto funcRef = [&](void) { return _release(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< GFX::Viewport > IGfxAdapter::create_viewport(GFX::IWindow* pWindow)
{
	auto funcRef = [&](void)
	{
		auto vp = std::make_shared< GFX::IViewport >(this, pWindow);
		if (!vp->create().get())
		{
			return GFX::Viewport();
		}
		return vp;
	};

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< GFX::GfxCommandBuffer > IGfxAdapter::create_command_buffer(GFX::E_GfxQueueType eQueueType, FXD::U32 nQueueID, GFX::E_CommandBufferType eType)
{
	auto funcRef = [&](void)
	{
		auto cb = std::make_shared< GFX::IGfxCommandBuffer >(this);
		if (!cb->create(eQueueType, nQueueID, eType).get())
		{
			return GFX::GfxCommandBuffer();
		}
		return cb;
	};

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< GFX::Fence > IGfxAdapter::create_fence(bool bSignaled)
{
	auto funcRef = [&](bool bSignaled)
	{
		auto fn = std::make_shared< GFX::IFence >(this);
		if (!fn->create(bSignaled).get())
		{
			return GFX::Fence();
		}
		return fn;
	};

	return Job::Async(FXDGFX(), [=]() { return funcRef(bSignaled); });
}

Job::Future< GFX::Image > IGfxAdapter::create_image(const GFX::IMAGE_DESC& imageDesc)
{
	auto funcRef = [&](const GFX::IMAGE_DESC& imageDesc)
	{
		auto img = std::make_shared< GFX::IImage >(this);
		if (!img->create(imageDesc).get())
		{
			return GFX::Image();
		}
		return img;
	};

	return Job::Async(FXDGFX(), [=]() { return funcRef(imageDesc); });
}

Job::Future< GFX::Image > IGfxAdapter::create_image(const GFX::Texture& texture, GFX::E_BufferUsageFlags eUsage)
{
	auto funcRef = [&](const GFX::Texture& texture, GFX::E_BufferUsageFlags eUsage)
	{
		auto img = std::make_shared< GFX::IImage >(this);
		if (!img->create(texture, eUsage).get())
		{
			return GFX::Image();
		}
		return img;
	};

	return Job::Async(FXDGFX(), [=]() { return funcRef(texture, eUsage); });
}

Job::Future< GFX::Image > IGfxAdapter::create_image(const GFX::SwapChain& swapChain, FXD::U32 nBuffer)
{
	auto funcRef = [&](const GFX::SwapChain& swapChain, FXD::U32 nBuffer)
	{
		auto img = std::make_shared< GFX::IImage >(this);
		if (!img->create(swapChain, nBuffer).get())
		{
			return GFX::Image();
		}
		return img;
	};

	return Job::Async(FXDGFX(), [=]() { return funcRef(swapChain, nBuffer); });
}

Job::Future< GFX::ImageView > IGfxAdapter::create_image_view(GFX::IImage* pImage, const GFX::IMAGEVIEW_DESC& imageViewDesc)
{
	auto funcRef = [&](GFX::IImage* pImage, const GFX::IMAGEVIEW_DESC& imageViewDesc)
	{
		auto imgView = std::make_shared< GFX::IImageView >(this, pImage);
		if (!imgView->create(imageViewDesc).get())
		{
			return GFX::ImageView();
		}
		return imgView;
	};

	return Job::Async(FXDGFX(), [=]() { return funcRef(pImage, imageViewDesc); });
}

Job::Future< GFX::RenderTarget > IGfxAdapter::create_render_target(const GFX::RENDER_TEXTURE_DESC& rtDesc)
{
	auto funcRef = [&](const GFX::RENDER_TEXTURE_DESC& rtDesc)
	{
		auto rt = std::make_shared< GFX::IRenderTarget >(this);
		if (!rt->create(rtDesc).get())
		{
			return GFX::RenderTarget();
		}
		return rt;
	};

	return Job::Async(FXDGFX(), [=]() { return funcRef(rtDesc); });
}

Job::Future< GFX::IndexBuffer > IGfxAdapter::create_index_buffer(GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, GFX::E_IndexType eIndexType, const void* pSrc)
{
	auto funcRef = [&](GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, GFX::E_IndexType eIndexType, const void* pSrc)
	{
		auto iBuf = std::make_shared< GFX::IIndexBuffer >(this);
		if (!iBuf->create(eBufferUsage, nCount, eIndexType, pSrc).get())
		{
			return GFX::IndexBuffer();
		}
		return iBuf;
	};

	return Job::Async(FXDGFX(), [=]() { return funcRef(eBufferUsage, nCount, eIndexType, pSrc); });
}

Job::Future< GFX::IndexBuffer > IGfxAdapter::create_index_buffer(GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc)
{
	auto funcRef = [&](GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc)
	{
		auto iBuf = std::make_shared< GFX::IIndexBuffer >(this);
		if (!iBuf->create(eBufferUsage, nCount, nStride, pSrc).get())
		{
			return GFX::IndexBuffer();
		}
		return iBuf;
	};

	return Job::Async(FXDGFX(), [=]() { return funcRef(eBufferUsage, nCount, nStride, pSrc); });
}

Job::Future< GFX::UniformBuffer > IGfxAdapter::create_uniform_buffer(GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc)
{
	auto funcRef = [&](GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc)
	{
		auto uBuf = std::make_shared< GFX::IUniformBuffer >(this);
		if (!uBuf->create(eBufferUsage, nCount, nStride, pSrc).get())
		{
			return GFX::UniformBuffer();
		}
		return uBuf;
	};

	return Job::Async(FXDGFX(), [=]() { return funcRef(eBufferUsage, nCount, nStride, pSrc); });
}

Job::Future< GFX::VertexBuffer > IGfxAdapter::create_vertex_buffer(GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc)
{
	auto funcRef = [&](GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc)
	{
		auto vBuf = std::make_shared< GFX::IVertexBuffer >(this);
		if (!vBuf->create(eBufferUsage, nCount, nStride, pSrc).get())
		{
			return GFX::VertexBuffer();
		}
		return vBuf;
	};

	return Job::Async(FXDGFX(), [=]() { return funcRef(eBufferUsage, nCount, nStride, pSrc); });
}

/*
Job::Future< FX::SamplerState > IGfxAdapter::create_sampler_state(const FX::SAMPLER_DESC& sampleDesc) const
{
auto funcRef = [&](const FX::SAMPLER_DESC& sampleDesc) { return m_fxStateManager->_create_sampler_state(sampleDesc); };

return Job::Async(FXDGFX(), [=]() { return funcRef(sampleDesc); });
}

Job::Future< GFX::FX::BlendState > IGfxAdapter::create_blend_state(const FX::BLEND_STATE_DESC& blendDesc) const
{
auto funcRef = [&](const FX::BLEND_STATE_DESC& blendDesc) { return m_fxStateManager->_create_blend_state(blendDesc); };

return Job::Async(FXDGFX(), [=]() { return funcRef(blendDesc); });
}

Job::Future< GFX::FX::RasterState > IGfxAdapter::create_raster_state(const FX::RASTER_STATE_DESC& rasterDesc) const
{
auto funcRef = [&](const FX::RASTER_STATE_DESC& rasterDesc) { return m_fxStateManager->_create_raster_state(rasterDesc); };

return Job::Async(FXDGFX(), [=]() { return funcRef(rasterDesc); });
}

Job::Future< GFX::FX::DepthStencilState > IGfxAdapter::create_depth_stencil_state(const FX::DEPTHSTENCIL_STATE_DESC& dsStateDesc) const
{
auto funcRef = [&](const FX::DEPTHSTENCIL_STATE_DESC& dsStateDesc) { return m_fxStateManager->_create_depth_stencil_state(dsStateDesc); };

return Job::Async(FXDGFX(), [=]() { return funcRef(dsStateDesc); });
}
*/

Job::Future< GFX::FX::Pipeline > IGfxAdapter::create_pipeline(const FX::PIPELINE_DESC& pipelineDesc) const
{
	auto funcRef = [&](const FX::PIPELINE_DESC& pipelineDesc) { return m_fxStateManager->_create_pipeline_state(pipelineDesc); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(pipelineDesc); });
}

void IGfxAdapter::RHIBeginDrawingViewport(GFX::Viewport& viewport, const GFX::GfxCommandBuffer& cmdBuffer)
{
	auto funcRef = [&](const GFX::Viewport& viewport)
	{
		PRINT_COND_ASSERT((m_pDrawingViewport == nullptr), "GFX: A Viewport has already been set for drawing");
		m_pDrawingViewport = viewport.get();
		viewport->_begin_drawing_viewport();
	};

	if (cmdBuffer == nullptr)
	{
		Job::Async(FXDGFX(), [=]() { funcRef(viewport); }).get();
	}
	else
	{
		auto func = [=]() { funcRef(viewport); };
		//cmdBuffer->queue_command(func);
	}
}

void IGfxAdapter::RHIEndDrawingViewport(const GFX::Viewport& viewport, const GFX::GfxCommandBuffer& cmdBuffer)
{
	auto funcRef = [&](GFX::Viewport viewport)
	{
		PRINT_COND_ASSERT((m_pDrawingViewport == viewport.get()), "GFX: A Viewport has not been set for drawing");
		m_pDrawingViewport = nullptr;
		viewport->_end_drawing_viewport();
	};

	if (cmdBuffer == nullptr)
	{
		Job::Async(FXDGFX(), [=]() { funcRef(viewport); }).get();
	}
	else
	{
		auto func = [=]() { funcRef(viewport); };
		//cmdBuffer->queue_command(func);
	}
}

void IGfxAdapter::RHIClearViewport(GFX::Viewport& viewport, Core::ColourRGBA colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil, const GFX::GfxCommandBuffer& cmdBuffer)
{
	auto funcRef = [&](const GFX::Viewport& viewport, Core::ColourRGBA colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil)
	{
		viewport->_clear(colour, eClearFlags, fZ, fDepth, nStencil);
	};

	if (cmdBuffer == nullptr)
	{
		Job::Async(FXDGFX(), [=]() { funcRef(viewport, colour, eClearFlags, fZ, fDepth, nStencil); });
	}
	else
	{
		auto func = [=]() { funcRef(viewport, colour, eClearFlags, fZ, fDepth, nStencil); };
		//cmdBuffer->queue_command(func);
	}
}

void IGfxAdapter::RHIFrame(GFX::IGfxFrameItem* pFrame, const GFX::GfxCommandBuffer& cmdBuffer)
{
	auto funcRef = [&](GFX::IGfxFrameItem* pFrame)
	{
		pFrame->update();
		pFrame->render(m_pDrawingViewport);
	};

	if (cmdBuffer == nullptr)
	{
		Job::Async(FXDGFX(), [=]() { funcRef(pFrame); });
	}
	else
	{
		auto func = [=]() { funcRef(pFrame); };
		//cmdBuffer->queue_command(func);
	}
}

void IGfxAdapter::RHIExecuteCommandList(const GFX::GfxCommandBuffer& cmdBuffer)
{
	auto funcRef = [&](const GFX::GfxCommandBuffer& cmdBuffer)
	{
		//cmdBuffer->
	};

	Job::Async(FXDGFX(), [=]() { funcRef(cmdBuffer); }).wait();
}

void IGfxAdapter::_update(FXD::F32 /*dt*/)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");
}