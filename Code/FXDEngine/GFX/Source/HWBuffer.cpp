// Creator - MatthewGolder
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/HWBuffer.h"
#include "FXDEngine/GFX/Mapping.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace GFX;

// ------
// IHWBuffer::LockGuard
// -
// ------
IHWBuffer::LockGuard::LockGuard(GFX::HWBuffer& buffer, GFX::E_BufferLockOptions eLockMode, FXD::U32 nQueueID)
	: IHWBuffer::LockGuard(buffer, 0, buffer->get_size(), eLockMode, nQueueID)
{
}

IHWBuffer::LockGuard::LockGuard(GFX::HWBuffer& buffer, FXD::U32 nOffset, FXD::U32 nLength, GFX::E_BufferLockOptions eLockMode, FXD::U32 nQueueID)
	: m_buffer(buffer)
#if defined(FXD_DEBUG)
	, m_bLocked(false)
#endif
{
#if defined(FXD_DEBUG)
	PRINT_COND_ASSERT((!m_bLocked), "GFX: ");
	m_bLocked = true;
#endif
	m_buffer->_map(nOffset, nLength, eLockMode, nQueueID);
}

IHWBuffer::LockGuard::~LockGuard(void)
{
#if defined(FXD_DEBUG)
	PRINT_COND_ASSERT((m_bLocked), "GFX: ");
	m_bLocked = false;
#endif
	m_buffer->_unmap();
}

// ------
// HWBuffer
// - 
// ------
Job::Future< bool > IHWBuffer::create(const GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc)
{
	auto funcRef = [&](const GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc) { return _create(eBufferUsage, nCount, nStride, pSrc); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(eBufferUsage, nCount, nStride, pSrc); });
}

Job::Future< bool > IHWBuffer::release(void)
{
	auto funcRef = [&](void) { return _release(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

void* IHWBuffer::lock(FXD::U32 nOffset, FXD::U32 nLength, GFX::E_BufferLockOptions eLockMode, FXD::U32 nQueueID)
{
	PRINT_COND_ASSERT(!is_locked(), "GFX: Cannot lock this buffer, it is already locked!");

	void* pRet = _map(nOffset, nLength, eLockMode, nQueueID);
	m_bLocked = true;
	return pRet;
}

void* IHWBuffer::lock(GFX::E_BufferLockOptions eLockMode, FXD::U32 nQueueID)
{
	return lock(0, get_size(), eLockMode, nQueueID);
}

void IHWBuffer::unlock(void)
{
	PRINT_COND_ASSERT(is_locked(), "GFX: Cannot unlock this buffer, it is not locked!");
	_unmap();
	m_bLocked = false;
}

void IHWBuffer::read_data(FXD::U32 nOffset, FXD::U32 nLength, void* pDst, FXD::U32 nQueueID)
{
	void* pSrc = lock(nOffset, nLength, GFX::E_BufferLockOptions::ReadOnly, nQueueID);
	Memory::MemCopy(pDst, pSrc, nLength);
	unlock();
}

void IHWBuffer::write_data(FXD::U32 nOffset, FXD::U32 nLength, const void* pSrc, FXD::U32 nQueueID)
{
	void* pDst = lock(nOffset, nLength, GFX::E_BufferLockOptions::WriteOnly, nQueueID);
	Memory::MemCopy(pDst, pSrc, nLength);
	unlock();
}

bool IHWBuffer::is_dynamic(void) const
{
	return buffer_usage().is_flag_set(GFX::E_BufferUsageFlag::USAGE_CPU_WRITE);
}

// ------
// IIndexBuffer
// - 
// ------
IIndexBuffer::IIndexBuffer(GFX::IGfxAdapter* pAdapter)
	: IHWBuffer(pAdapter)
{
}

IIndexBuffer::~IIndexBuffer(void)
{
}

Job::Future< bool > IIndexBuffer::create(const GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, GFX::E_IndexType eIndexType, const void* pSrc)
{
	return IIndexBuffer::create(eBufferUsage, nCount, GFX::Index::GetNumElemBytes(eIndexType), pSrc);
}

Job::Future< bool > IIndexBuffer::create(const GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc)
{
	return IHWBuffer::create(eBufferUsage, nCount, nStride, pSrc);
}

bool IIndexBuffer::activate(FXD::U32 nOffset, const GFX::GfxCommandBuffer& commandBuffer) const
{
	auto funcRef = [&](FXD::U32 nOffset) { return _activate(nOffset); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(nOffset); }).get();
}


// ------
// IVertexBuffer
// - 
// ------
IVertexBuffer::IVertexBuffer(GFX::IGfxAdapter* pAdapter)
	: IHWBuffer(pAdapter)
{
}

IVertexBuffer::~IVertexBuffer(void)
{
}

Job::Future< bool > IVertexBuffer::create(const GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, GFX::E_VertexElementType eVertexType, const void* pSrc)
{
	return IVertexBuffer::create(eBufferUsage, nCount, GFX::Vertex::GetNumElemBytes(eVertexType), pSrc);
}

Job::Future< bool > IVertexBuffer::create(const GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc)
{
	return IHWBuffer::create(eBufferUsage, nCount, nStride, pSrc);
}

bool IVertexBuffer::activate(FXD::U32 nOffset, FXD::U32 nStrideOverride, const GFX::GfxCommandBuffer& commandBuffer) const
{
	auto funcRef = [&](FXD::U32 nOffset, FXD::U32 nStrideOverride) { return _activate(nOffset, nStrideOverride); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(nOffset, nStrideOverride); }).get();
}

// ------
// IUniformBuffer
// - 
// ------
IUniformBuffer::IUniformBuffer(GFX::IGfxAdapter* pAdapter)
	: IHWBuffer(pAdapter)
{
}

IUniformBuffer::~IUniformBuffer(void)
{
}

Job::Future< bool > IUniformBuffer::create(const GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc)
{
	return IHWBuffer::create(eBufferUsage, nCount, nStride, pSrc);
}