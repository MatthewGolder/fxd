// Creator - MatthewGolder
#include "FXDEngine/GFX/GfxCommandBuffer.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace GFX;

// ------
// IGfxCommandBuffer
// - 
// ------
Job::Future< bool > IGfxCommandBuffer::create(GFX::E_GfxQueueType eQueueType, FXD::U32 nQueueID, GFX::E_CommandBufferType eType)
{
	auto funcRef = [&](GFX::E_GfxQueueType eQueueType, FXD::U32 nQueueID, GFX::E_CommandBufferType eType)
	{
		return _create(eQueueType, nQueueID, eType);
	};

	return Job::Async(FXDGFX(), [=]() { return funcRef(eQueueType, nQueueID, eType); });
}

Job::Future< bool > IGfxCommandBuffer::release(void)
{
	auto funcRef = [&](void) { return _release(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< void > IGfxCommandBuffer::begin(void)
{
	auto funcRef = [&](void) { _begin(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< void > IGfxCommandBuffer::end(void)
{
	auto funcRef = [&](void) { _end(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< void > IGfxCommandBuffer::begin_render_pass(void)
{
	auto funcRef = [&](void) { _begin_render_pass(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< void > IGfxCommandBuffer::end_render_pass(void)
{
	auto funcRef = [&](void) { _end_render_pass(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

// ------
// IGfxCommandBufferManager
// - 
// ------
Job::Future< bool > IGfxCommandBufferManager::create(void)
{
	auto funcRef = [&](void) { return _create(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}

Job::Future< bool > IGfxCommandBufferManager::release(void)
{
	auto funcRef = [&](void) { return _release(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); });
}
