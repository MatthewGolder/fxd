// Creator - MatthewGolder
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/GfxSystem.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/WindowManager.h"

using namespace FXD;
using namespace GFX;

// 0 - Not Initialised
// 1 - Initialised
// 2 - ShutDown
FXD::S32 g_nGFXStatus = 0;

// ------
// IGfxJPThread
// -
// ------
IGfxJPThread::IGfxJPThread(void)
	: IAsyncTaskPoolThread(display_name())
	, m_gfxSystem(nullptr)
{
	if (g_nGFXStatus == 2)
	{
		PRINT_ASSERT << "GFX: Accessed after shutdown";
	}
}

IGfxJPThread::~IGfxJPThread(void)
{
	stop_and_wait_for_exit();
}

bool IGfxJPThread::init_pool(void)
{
	GFX::Funcs::RestrictGfxThread();
	PRINT_INFO << "GFX: IGfxJPThread::init_pool()";

	m_gfxSystem = std::make_unique< GFX::IGfxSystem >();
	m_windowManager = std::make_unique< GFX::IWindowManager >();
	m_timer.start_counter();
	g_nGFXStatus = 1;
	m_shared.get_quick_write_lock()->m_eState = Job::E_AsyncState::Running;
	return true;
}

bool IGfxJPThread::shutdown_pool(void)
{
	m_gfxSystem = nullptr;
	m_windowManager = nullptr;
	GFX::Funcs::UnRestrictGfxThread();
	PRINT_INFO << "GFX: IGfxJPThread::shutdown_pool()";

	g_nGFXStatus = 2;
	m_shared.get_quick_write_lock()->m_eState = Job::E_AsyncState::ShutDown;
	return true;
}

void IGfxJPThread::idle_process(void)
{
	if (is_shuttingdown())
	{
		return;
	}

	const FXD::S32 nProcessFrequency = 5;

	// 1. Get initial time stamp.
	FXD::F32 ts1 = (FXD::F32)Core::Funcs::GetTimeSecsF();

	FXD::F32 dt = m_timer.elapsed_time();
	m_fps.update(dt);
	if (m_gfxSystem)
	{
		m_gfxSystem->_process(dt);
	}

	// 2. Get final time stamp and wait for next update interval.
	FXD::F32 ts2 = (FXD::F32)Core::Funcs::GetTimeSecsF();
	FXD::S32 nToWait = nProcessFrequency - (FXD::S32)((ts2 - ts1) * 1000.0f);
	if (nToWait > 0)
	{
		Thread::Funcs::Sleep(nToWait);
	}
}

bool IGfxJPThread::is_thread_restricted(void) const
{
	return GFX::Funcs::IsGfxThreadRestricted();
}

// ------
// Funcs
// -
// ------
static FXD::S32 g_ContextThread = 0;
bool FXD::GFX::Funcs::IsGfxThreadRestricted(void)
{
	return (g_ContextThread != 0) && (g_ContextThread != Thread::Funcs::GetCurrentThreadID());
}

void FXD::GFX::Funcs::RestrictGfxThread(void)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread" << Thread::Funcs::GetCurrentThreadID());
	g_ContextThread = Thread::Funcs::GetCurrentThreadID();
}

void FXD::GFX::Funcs::UnRestrictGfxThread(void)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread" << Thread::Funcs::GetCurrentThreadID());
	g_ContextThread = 0;
}