// Creator - MatthewGolder
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/SwapChain.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace GFX;

// ------
// ISwapChain
// -
// ------
bool ISwapChain::create(void)
{
	auto funcRef = [&](void) { return _create(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); }).get();
}

bool ISwapChain::release(void)
{
	auto funcRef = [&](void) { return _release(); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(); }).get();
}

bool ISwapChain::resize(const Math::Vector2I newSize)
{
	auto funcRef = [&](const Math::Vector2I newSize) { return _resize(newSize); };

	return Job::Async(FXDGFX(), [=]() { return funcRef(newSize); }).get();
}

void ISwapChain::swap(bool bVSync)
{
	auto funcRef = [&](bool bVSync) { _swap(bVSync); };

	Job::Async(FXDGFX(), [=]() { funcRef(bVSync); }).get();
}