// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_WINDOW_H
#define FXDENGINE_GFX_WINDOW_H

#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/GFX/Events.h"
#include "FXDEngine/GFX/Types.h"
#include "FXDEngine/Math/Geometry/Box2D.h"

namespace FXD
{
	namespace GFX
	{
		struct WINDOW_DESC
		{
			bool bVSync = false;
			bool bFocused = true;
			bool bResizable = true;
			bool bFullscreen = false;
			bool bVisible = true;
			bool bDepthBuffer = false;
			Math::Vector2I size = Math::Vector2I(800, 600);
			FXD::U32 nRefreshRate = 60;
		};

		// ------
		// IWindow
		// -
		// ------
		class IWindow : public Core::HasImpl< GFX::IWindow, 64 >, public Core::NonCopyable
		{
		public:
			friend class GFX::IWindowManager;

		public:
			IWindow(WINDOW_DESC windowDesc, const GFX::Window& parent);
			~IWindow(void);

			void move(FXD::U32 nLeft, FXD::U32 nTop);
			void resize(FXD::U32 nWidth, FXD::U32 nHeight);
			void set_fullscreen(FXD::U32 nWidth, FXD::U32 nHeight, bool bFullscreen);

			GET_REF_R(GFX::WINDOW_DESC, windowDesc, window_desc);
			FXD::U64 windowHandle(void) const;

		private:
			bool init(void);
			void close(void);

			void _notify_window_event(const GFX::E_WindowEvent eEvent);
			void _window_moved_or_resized(void);

		private:
			GFX::WINDOW_DESC m_windowDesc;

			const Job::Handler< GFX::OnWindowEvent >* OnWindowEvent;
		};

	} //namespace GFX
} //namespace FXD
#endif //FXDENGINE_GFX_WINDOW_H