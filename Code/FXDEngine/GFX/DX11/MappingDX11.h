// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DX11_MAPPINGDX11_H
#define FXDENGINE_GFX_DX11_MAPPINGDX11_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/DX11/DX11.h"

namespace FXD
{
	namespace GFX
	{
		namespace DX11
		{
			extern const D3D11_PRIMITIVE_TOPOLOGY GetPrimitiveType(GFX::E_Primitive eType);
			extern const D3D11_FILL_MODE GetFillMode(GFX::E_FillMode eMode);
			extern const D3D11_CULL_MODE GetCullMode(GFX::E_CullMode eMode);
		}
		namespace Buffer
		{
			extern const D3D11_BIND_FLAG ConvertToDX11BindFlags(GFX::E_BufferUsageFlags eFlags);
			extern const D3D11_CPU_ACCESS_FLAG ConvertToDX11CPUAccessFlags(GFX::E_BufferUsageFlags eFlags);
			extern const D3D11_USAGE ConvertToDX11Usage(GFX::E_BufferUsageFlags eFlags);
		}
		namespace Pixel
		{
			extern const DXGI_FORMAT GetFormat(GFX::E_PixelFormat eFormat);
		}
		namespace Index
		{
			extern const DXGI_FORMAT GetFormat(GFX::E_IndexType eType);
		}
		namespace Vertex
		{
			extern const DXGI_FORMAT GetFormat(GFX::E_VertexElementType eType);
		}

	} //namespace GFX
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_DX11_MAPPINGDX11_H