// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DX11_DX11_H
#define FXDENGINE_GFX_DX11_DX11_H

#include "FXDEngine/GFX/Types.h"
#include "FXDEngine/Core/StringView.h"

#if IsGFXDX11()
#if IsOSPC()
	#include <d3d11_1.h>
	#pragma comment(lib,"d3d11.lib")
	#pragma comment(lib,"d3dcompiler.lib")
	#pragma comment(lib,"dxgi.lib")
	#pragma comment(lib,"dxguid.lib")
#endif

struct DX11FXSemantic
{
	const DXGI_FORMAT format;
	const FXD::U16	size;
	const FXD::U16	id;
	const FXD::Core::StringView8 name;
	const FXD::Core::StringView8 semanticName;
};

extern const DX11FXSemantic DX11FXSemantics[];

namespace FXD
{
	namespace GFX
	{
		// ------
		// CheckDX11Result
		// -
		// ------
		class CheckDX11Result
		{
		public:
			CheckDX11Result(HRESULT hr, const FXD::UTF8* pText, const FXD::UTF8* pFile, FXD::U32 nLine);

			bool operator()();

		public:
			HRESULT m_hr;
			FXD::U32 m_nLine;
			const FXD::UTF8* m_pText;
			const FXD::UTF8* m_pFile;
		};
		#define DX11ResultCheckFail(c) FXD::GFX::CheckDX11Result(c, #c, __FILE__, __LINE__)()
		#define DX11ResultCheckSuccess(c) !DX11ResultCheckFail(c)

	} //namespace GFX
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_DX11_DX11_H