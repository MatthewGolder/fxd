// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DX11_DX11STATE_H
#define FXDENGINE_GFX_DX11_DX11STATE_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/Gfx/DX11/DX11.h"
#include "FXDEngine/Gfx/FX/Types.h"
#include "FXDEngine/Container/RefCounting.h"

namespace FXD
{
	namespace GFX
	{
		namespace Cache
		{
			namespace InputLayout
			{
				void Set(ID3D11DeviceContext* pDevContext, ID3D11InputLayout* pInputLayout);
			}
			namespace VertexBuffer
			{
				void Set(ID3D11DeviceContext* pDevContext, ID3D11Buffer* pVertexBuffer, FXD::U32 nStreamIndex, FXD::U32 nStride, FXD::U32 nOffset);
			}
			namespace IndexBuffer
			{
				void Set(ID3D11DeviceContext* pDevContext, ID3D11Buffer* pIndexBuffer, GFX::E_IndexType eType, FXD::U32 nOffset);
			}
			namespace ShaderResourceView
			{
				void SetVertex(ID3D11DeviceContext* pDevContext, ID3D11ShaderResourceView* pSRV, FXD::U32 nResourceIndex);
				void SetGeometry(ID3D11DeviceContext* pDevContext, ID3D11ShaderResourceView* pSRV, FXD::U32 nResourceIndex);
				void SetFragment(ID3D11DeviceContext* pDevContext, ID3D11ShaderResourceView* pSRV, FXD::U32 nResourceIndex);
				template < FX::E_ShaderType ShaderType >
				void Set(ID3D11DeviceContext* pDevContext, ID3D11ShaderResourceView* pSRV, FXD::U32 nResourceIndex)
				{
					switch (ShaderType)
					{
						case FX::E_ShaderType::Vertex:
						{
							SetVertex(pDevContext, pSRV, nResourceIndex);
							break;
						}
						case FX::E_ShaderType::Geometry:
						{
							SetGeometry(pDevContext, pSRV, nResourceIndex);
							break;
						}
						case FX::E_ShaderType::Fragment:
						{
							SetFragment(pDevContext, pSRV, nResourceIndex);
							break;
						}
					}
				}
			}
			namespace Shader
			{
				void SetVertex(ID3D11DeviceContext* pDevContext, ID3D11VertexShader* pShader);
				void SetGeometry(ID3D11DeviceContext* pDevContext, ID3D11GeometryShader* pShader);
				void SetFragment(ID3D11DeviceContext* pDevContext, ID3D11PixelShader* pShader);
				template < FX::E_ShaderType ShaderType >
				void Set(ID3D11DeviceContext* pDevContext, ID3D11DeviceChild* pShader)
				{
					switch (ShaderType)
					{
						case FX::E_ShaderType::Vertex:
						{
							SetVertex(pDevContext, (ID3D11VertexShader*)pShader);
							break;
						}
						case FX::E_ShaderType::Geometry:
						{
							SetGeometry(pDevContext, (ID3D11GeometryShader*)pShader);
							break;
						}
						case FX::E_ShaderType::Fragment:
						{
							SetFragment(pDevContext, (ID3D11PixelShader*)pShader);
							break;
						}
					}
				}
			}
			namespace UniformBuffer
			{
				void SetVertex(ID3D11DeviceContext* pDevContext, ID3D11Buffer* pUniformBuffer, FXD::U32 nSlotIndex);
				void SetGeometry(ID3D11DeviceContext* pDevContext, ID3D11Buffer* pUniformBuffer, FXD::U32 nSlotIndex);
				void SetFragment(ID3D11DeviceContext* pDevContext, ID3D11Buffer* pUniformBuffer, FXD::U32 nSlotIndex);
				template < FX::E_ShaderType ShaderType >
				void Set(ID3D11DeviceContext* pDevContext, ID3D11Buffer* pUniformBuffer, FXD::U32 nSlotIndex)
				{
					switch (ShaderType)
					{
						case FX::E_ShaderType::Vertex:
						{
							SetVertex(pDevContext, pUniformBuffer, nSlotIndex);
							break;
						}
						case FX::E_ShaderType::Geometry:
						{
							SetGeometry(pDevContext, pUniformBuffer, nSlotIndex);
							break;
						}
						case FX::E_ShaderType::Fragment:
						{
							SetFragment(pDevContext, pUniformBuffer, nSlotIndex);
							break;
						}
					}
				}
			}
			namespace SamplerState
			{
				extern void SetVertex(ID3D11DeviceContext* pDevContext, ID3D11SamplerState* pSamplerState, FXD::U32 nSamplerIndex);
				extern void SetGeometry(ID3D11DeviceContext* pDevContext, ID3D11SamplerState* pSamplerState, FXD::U32 nSamplerIndex);
				extern void SetFragment(ID3D11DeviceContext* pDevContext, ID3D11SamplerState* pSamplerState, FXD::U32 nSamplerIndex);

				template < FX::E_ShaderType ShaderType >
				void Set(ID3D11DeviceContext* pDevContext, ID3D11SamplerState* pSamplerState, FXD::U32 nSamplerIndex)
				{
					switch (ShaderType)
					{
						case FX::E_ShaderType::Vertex:
						{
							SetVertex(pDevContext, pSamplerState, nSamplerIndex);
							break;
						}
						case FX::E_ShaderType::Geometry:
						{
							SetGeometry(pDevContext, pSamplerState, nSamplerIndex);
							break;
						}
						case FX::E_ShaderType::Fragment:
						{
							SetFragment(pDevContext, pSamplerState, nSamplerIndex);
							break;
						}
					}
				}
			}
			namespace RenderTarget
			{
				extern void Set(ID3D11DeviceContext* pDevContext, ID3D11RenderTargetView** pRTViews, FXD::U32 nCount, ID3D11DepthStencilView* pDSView);
			}
			namespace DepthStencil
			{
				extern void Set(ID3D11DeviceContext* pDevContext, ID3D11DepthStencilState* pState, FXD::U32 nRefStencil);
			}
			namespace BlendState
			{
				extern void Set(ID3D11DeviceContext* pDevContext, ID3D11BlendState* pState, const FXD::F32 fBlendFactor[4], FXD::U32 nSampleMask);
				extern void Set(ID3D11DeviceContext* pDevContext, const FXD::F32 fBlendFactor[4], FXD::U32 nSampleMask);
			}
			namespace RasterState
			{
				extern void Set(ID3D11DeviceContext* pDevContext, ID3D11RasterizerState* pState);
			}
			namespace Topology
			{
				extern void Set(ID3D11DeviceContext* pDevContext, const GFX::E_Primitive ePrimitive);
			}
			namespace Viewport
			{
				extern void Set(ID3D11DeviceContext* pDevContext, D3D11_VIEWPORT* pViewports, FXD::U32 nCount);
			}
			namespace ScissorRect
			{
				extern void Set(ID3D11DeviceContext* pDevContext, D3D11_RECT* pRects, FXD::U32 nCount);
			}
		} //namespace Cache
	} //namespace GFX
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_DX11_DX11STATE_H