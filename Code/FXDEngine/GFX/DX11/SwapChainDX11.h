// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DX11_SWAPCHAINDX11_H
#define FXDENGINE_GFX_DX11_SWAPCHAINDX11_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/SwapChain.h"
#include "FXDEngine/GFX/DX11/DX11.h"
#include "FXDEngine/Container/RefCounting.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::ISwapChain >
			// -
			// ------
			template <>
			class Impl< GFX::ISwapChain >
			{
			public:
				friend class GFX::ISwapChain;

			public:
				Impl(void);
				~Impl(void);

				GET_REF_R(Container::Ptr< IDXGISwapChain >, dxSwapChain, dxSwapChain);
				GET_REF_R(Container::Ptr< IDXGISwapChain1 >, dxSwapChain1, dxSwapChain1);

			protected:
				Container::Ptr< IDXGISwapChain > m_dxSwapChain;
				Container::Ptr< IDXGISwapChain1 > m_dxSwapChain1;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_DX11_SWAPCHAINDX11_H