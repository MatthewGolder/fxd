// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/DX11/GfxAdapterDX11.h"
#include "FXDEngine/GFX/DX11/ImageDX11.h"
#include "FXDEngine/GFX/DX11/MappingDX11.h"
#include "FXDEngine/GFX/DX11/RenderTargetDX11.h"
#include "FXDEngine/GFX/DX11/SwapChainDX11.h"
#include "FXDEngine/GFX/DX11/ViewportDX11.h"
#include "FXDEngine/GFX/Window.h"

using namespace FXD;
using namespace GFX;

#if !defined(COM_IID)
#define COM_IID(cp) __uuidof(cp), cp
#endif

// ------
// DX11Swap
// -
// ------
namespace DX11Swap
{
	void SetupSwapChainParams(GFX::IViewport* pViewport, GFX::E_PixelFormat& eColorFormat, GFX::E_PixelFormat& eDepthFormat, DXGI_SWAP_CHAIN_DESC& swapDesc)
	{
		eColorFormat = GFX::E_PixelFormat::RGBA8;
		eDepthFormat = GFX::E_PixelFormat::D24_S8;

		DXGI_SWAP_CHAIN_DESC d3dSwapChainDesc = {};
		Memory::MemZero_T(d3dSwapChainDesc);
		d3dSwapChainDesc.BufferDesc.Width = pViewport->size().x();
		d3dSwapChainDesc.BufferDesc.Height = pViewport->size().y();
		d3dSwapChainDesc.BufferDesc.Format = Pixel::GetFormat(eColorFormat);
		d3dSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		d3dSwapChainDesc.BufferCount = 2;
		d3dSwapChainDesc.OutputWindow = (const HWND)pViewport->window()->windowHandle();
		d3dSwapChainDesc.SampleDesc.Count = 1;
		d3dSwapChainDesc.SampleDesc.Quality = 0;
		d3dSwapChainDesc.Windowed = !pViewport->window()->window_desc().bFullscreen;
		d3dSwapChainDesc.BufferDesc.RefreshRate.Numerator = pViewport->window()->window_desc().nRefreshRate;
		d3dSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
		d3dSwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
		if (d3dSwapChainDesc.Windowed == FALSE)
		{
			d3dSwapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
			d3dSwapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
			d3dSwapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
		}
		swapDesc = d3dSwapChainDesc;
	}
	void SetupSwapChain1Params(GFX::IViewport* pViewport, GFX::E_PixelFormat& eColorFormat, GFX::E_PixelFormat& eDepthFormat, DXGI_SWAP_CHAIN_DESC1& swapDesc, DXGI_SWAP_CHAIN_FULLSCREEN_DESC& fullscreenDesc)
	{
		eColorFormat = E_PixelFormat::RGBA8;
		eDepthFormat = E_PixelFormat::D24_S8;

		DXGI_SWAP_CHAIN_DESC1 d3dSwapChainDesc = {};
		Memory::MemZero_T(d3dSwapChainDesc);
		d3dSwapChainDesc.Width = pViewport->size().x();
		d3dSwapChainDesc.Height = pViewport->size().y();
		d3dSwapChainDesc.Format = Pixel::GetFormat(eColorFormat);
		d3dSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		d3dSwapChainDesc.BufferCount = 2;
		d3dSwapChainDesc.SampleDesc.Count = 1;
		d3dSwapChainDesc.SampleDesc.Quality = 0;
		d3dSwapChainDesc.Stereo = FALSE;
		d3dSwapChainDesc.Scaling = DXGI_SCALING_STRETCH;
		d3dSwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
		d3dSwapChainDesc.AlphaMode = DXGI_ALPHA_MODE_IGNORE;
		d3dSwapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

		DXGI_SWAP_CHAIN_FULLSCREEN_DESC d3dFullscreenDesc ={};
		Memory::MemZero_T(d3dFullscreenDesc);
		d3dFullscreenDesc.RefreshRate.Denominator = 0;
		d3dFullscreenDesc.RefreshRate.Numerator = 0;
		d3dFullscreenDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		d3dFullscreenDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
		d3dFullscreenDesc.Windowed = !pViewport->window()->window_desc().bFullscreen;

		swapDesc = d3dSwapChainDesc;
		fullscreenDesc = d3dFullscreenDesc;
	}
} //namespace DX11Swap

// ------
// ISwapChain::Impl::Impl
// - 
// ------
ISwapChain::Impl::Impl::Impl(void)
{
}

ISwapChain::Impl::~Impl(void)
{
	PRINT_COND_ASSERT((!m_dxSwapChain), "GFX: m_swapChain is not shutdown");
	PRINT_COND_ASSERT((!m_dxSwapChain1), "GFX: m_swapChain1 is not shutdown");
}

// ------
// ISwapChain
// -
// ------
ISwapChain::ISwapChain(GFX::IGfxAdapter* pAdapter, GFX::IViewport* pViewport)
	: m_pAdapter(pAdapter)
	, m_pViewport(pViewport)
	, m_eColorFormat(GFX::E_PixelFormat::Unknown)
	, m_eDepthFormat(GFX::E_PixelFormat::Unknown)
{
}

ISwapChain::~ISwapChain(void)
{
	PRINT_COND_ASSERT((!m_renderTarget), "GFX: m_renderTarget is not shutdown");
}

bool ISwapChain::_create(void)
{
	auto pAdapterDesc = static_cast< GFX::IGfxAdapterDesc* >(m_pAdapter->adapter_desc().get());

	// 1. Create a SwapChain
	Container::Ptr< IDXGIFactory1 > factory;
	if (DX11ResultCheckFail(pAdapterDesc->impl().dxgiAdapter()->GetParent(COM_IID(factory))))
	{
		PRINT_WARN << "GFX: Failed to get IDXGIFactory1";
		return false;
	}

	Container::Ptr< IDXGIFactory2 > factory2;
	if (DX11ResultCheckSuccess(pAdapterDesc->impl().dxgiAdapter()->GetParent(COM_IID(factory2))))
	{
		DXGI_SWAP_CHAIN_DESC1 d3dSwapChainDesc1 = {};
		DXGI_SWAP_CHAIN_FULLSCREEN_DESC d3dFullscreenDesc = {};
		Memory::MemZero_T(d3dSwapChainDesc1);
		Memory::MemZero_T(d3dFullscreenDesc);

		DX11Swap::SetupSwapChain1Params(m_pViewport, m_eColorFormat, m_eDepthFormat, d3dSwapChainDesc1, d3dFullscreenDesc);
		if (DX11ResultCheckFail(factory2->CreateSwapChainForHwnd(m_pAdapter->impl().d3dDevice(), (const HWND)m_pViewport->window()->windowHandle(), &d3dSwapChainDesc1, &d3dFullscreenDesc, nullptr, m_impl->m_dxSwapChain1)))
		{
			PRINT_WARN << "GFX: Failed to CreateSwapChainForHwnd()";
			return false;
		}
		if (DX11ResultCheckFail(m_impl->m_dxSwapChain1->QueryInterface(__uuidof(IDXGISwapChain), reinterpret_cast< void** >(&m_impl->m_dxSwapChain))))
		{
			PRINT_WARN << "GFX: Failed m_swapChain1->QueryInterface()";
			return false;
		}
	}
	else
	{
		DXGI_SWAP_CHAIN_DESC d3dSwapChainDesc = {};
		Memory::MemZero_T(d3dSwapChainDesc);
		DX11Swap::SetupSwapChainParams(m_pViewport, m_eColorFormat, m_eDepthFormat, d3dSwapChainDesc);
		if (DX11ResultCheckFail(factory->CreateSwapChain(m_pAdapter->impl().d3dDevice(), &d3dSwapChainDesc, m_impl->m_dxSwapChain)))
		{
			PRINT_WARN << "GFX: Failed to CreateSwapChain()";
			return false;
		}
	}

	// 2. Make window association
	static bool bCreated = false;
	if (!bCreated)
	{
		if (DX11ResultCheckFail(factory->MakeWindowAssociation((const HWND)m_pViewport->window()->windowHandle(), DXGI_MWA_NO_WINDOW_CHANGES)))
		{
			PRINT_WARN << "GFX: Failed to MakeWindowAssociation()";
			return false;
		}
		bCreated = true;
	}

	// 3. Create the swap images
	GFX::Image imgColour = nullptr;
	GFX::Image imgDepth = nullptr;
	imgColour = m_pAdapter->create_image(shared_from_this(), 0).get();
	PRINT_COND_ASSERT(imgColour, "GFX: Failed imgColour->create_image()");

	if (m_pViewport->window()->window_desc().bDepthBuffer)
	{
		GFX::IMAGE_DESC imgDepthDesc = {};
		imgDepthDesc.Type = GFX::E_ImageType::Two;
		imgDepthDesc.Usage = (GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL);
		imgDepthDesc.Format = m_eDepthFormat;
		imgDepthDesc.Width = m_pViewport->size().x();
		imgDepthDesc.Height = m_pViewport->size().y();
		imgDepth = m_pAdapter->create_image(imgDepthDesc).get();
		PRINT_COND_ASSERT(imgDepth, "GFX: Failed imgDepth->create_image()");
	}

	// 4. Create the render target
	GFX::RENDER_TEXTURE_DESC rtDesc = {};
	rtDesc.ColorSurfaces[0].Image = imgColour;
	rtDesc.ColorSurfaces[0].Face = 0;
	rtDesc.ColorSurfaces[0].NumFaces = imgColour->imageDesc().num_faces();
	rtDesc.ColorSurfaces[0].MipLevel = 0;

	if (m_pViewport->window()->window_desc().bDepthBuffer)
	{
		rtDesc.DepthStencilSurface.Image = imgDepth;
		rtDesc.DepthStencilSurface.Face = 0;
		rtDesc.DepthStencilSurface.NumFaces = imgDepth->imageDesc().num_faces();
		rtDesc.DepthStencilSurface.MipLevel = 0;
	}
	m_renderTarget = m_pAdapter->create_render_target(rtDesc).get();

	return true;
}

bool ISwapChain::_release(void)
{
	m_eColorFormat = GFX::E_PixelFormat::Unknown;
	m_eDepthFormat = GFX::E_PixelFormat::Unknown;
	FXD_RELEASE(m_renderTarget, release().get());

	m_impl->m_dxSwapChain = nullptr;
	m_impl->m_dxSwapChain1 = nullptr;
	return true;
}

bool ISwapChain::_resize(const Math::Vector2I newSize)
{
	FXD_RELEASE(m_renderTarget, release().get());

	if (!m_impl->m_dxSwapChain1 == false)
	{
		DXGI_SWAP_CHAIN_DESC d3dSwapChainDesc = {};
		Memory::MemZero_T(d3dSwapChainDesc);
		DX11Swap::SetupSwapChainParams(m_pViewport, m_eColorFormat, m_eDepthFormat, d3dSwapChainDesc);
		if (DX11ResultCheckFail(m_impl->m_dxSwapChain1->ResizeBuffers(d3dSwapChainDesc.BufferCount, newSize.x(), newSize.y(), DXGI_FORMAT_UNKNOWN, DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH)))
		{
			PRINT_ASSERT << "GFX: m_swapChain1->ResizeBuffers()";
		}
	}
	else
	{
		DXGI_SWAP_CHAIN_DESC1 d3dSwapChainDesc1 = {};
		DXGI_SWAP_CHAIN_FULLSCREEN_DESC d3dFullscreenDesc = {};
		Memory::MemZero_T(d3dSwapChainDesc1);
		Memory::MemZero_T(d3dFullscreenDesc);
		DX11Swap::SetupSwapChain1Params(m_pViewport, m_eColorFormat, m_eDepthFormat, d3dSwapChainDesc1, d3dFullscreenDesc);
		if (DX11ResultCheckFail(m_impl->m_dxSwapChain->ResizeBuffers(d3dSwapChainDesc1.BufferCount, newSize[0], newSize[1], DXGI_FORMAT_UNKNOWN, DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH)))
		{
			PRINT_ASSERT << "GFX: m_swapChain->ResizeBuffers()";
		}
	}

	// 3. Create the swap images
	GFX::Image imgColour = nullptr;
	GFX::Image imgDepth = nullptr;
	imgColour = m_pAdapter->create_image(shared_from_this(), 0).get();
	PRINT_COND_ASSERT(imgColour, "GFX: Failed imgColour->create_image()");

	if (m_pViewport->window()->window_desc().bDepthBuffer)
	{
		GFX::IMAGE_DESC imgDepthDesc = {};
		imgDepthDesc.Type = GFX::E_ImageType::Two;
		imgDepthDesc.Usage = (GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL);
		imgDepthDesc.Format = m_eDepthFormat;
		imgDepthDesc.Height = m_pViewport->size().x();
		imgDepthDesc.Width = m_pViewport->size().y();
		imgDepth = m_pAdapter->create_image(imgDepthDesc).get();
		PRINT_COND_ASSERT(imgDepth, "GFX: Failed imgDepth->create_image()");
	}

	GFX::RENDER_TEXTURE_DESC rtDesc = {};
	rtDesc.ColorSurfaces[0].Image = imgColour;
	rtDesc.ColorSurfaces[0].Face = 0;
	rtDesc.ColorSurfaces[0].NumFaces = imgColour->imageDesc().num_faces();
	rtDesc.ColorSurfaces[0].MipLevel = 0;

	if (m_pViewport->window()->window_desc().bDepthBuffer)
	{
		rtDesc.DepthStencilSurface.Image = imgDepth;
		rtDesc.DepthStencilSurface.Face = 0;
		rtDesc.DepthStencilSurface.NumFaces = imgDepth->imageDesc().num_faces();
		rtDesc.DepthStencilSurface.MipLevel = 0;
	}
	m_renderTarget = m_pAdapter->create_render_target(rtDesc).get();
	return true;
}

void ISwapChain::_swap(bool bVSync)
{
	m_impl->m_dxSwapChain->Present(bVSync ? 1 : 0, 0);
}
#endif //IsGFXDX11()