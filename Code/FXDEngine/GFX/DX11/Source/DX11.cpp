// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/DX11/DX11.h"

using namespace FXD;
using namespace GFX;

// ------
// CheckDX11Result
// -
// ------
inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, CheckDX11Result const& rhs)
{
	switch (rhs.m_hr)
	{
	#define HANDLE_STR(h) case h: ostr << #h; break;
		HANDLE_STR(DXGI_ERROR_INVALID_CALL)
		HANDLE_STR(E_INVALIDARG)
	#undef HANDLE_STR
		default:
		{
			FXD_ERROR("Unrecognised HResult - please find the correct string and add to this case");
			break;
		}
	}
	return ostr;
}

CheckDX11Result::CheckDX11Result(HRESULT hr, const FXD::UTF8* pText, const FXD::UTF8* pFile, FXD::U32 nLine)
	: m_hr(hr)
	, m_nLine(nLine)
	, m_pText(pText)
	, m_pFile(pFile)
{
}

bool CheckDX11Result::operator()()
{
	if (SUCCEEDED(m_hr))
	{
		return false;
	}
	PRINT_WARN << "[" << m_pFile << " , " << m_nLine << "] " << m_pText << " - failed with error: " << (*this);
	return true;
}
#endif //IsGFXDX11()