// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/DX11/GfxAdapterDX11.h"
#include "FXDEngine/GFX/DX11/GfxApiDX11.h"
#include "FXDEngine/GFX/DX11/GPUMemoryDX11.h"
#include "FXDEngine/GFX/DX11/ImageDX11.h"
#include "FXDEngine/GFX/DX11/MappingDX11.h"
#include "FXDEngine/GFX/DX11/RenderTargetDX11.h"
#include "FXDEngine/GFX/DX11/SwapChainDX11.h"

using namespace FXD;
using namespace GFX;

// ------
// IRenderTarget::Impl::Impl
// - 
// ------
IRenderTarget::Impl::Impl::Impl(void)
	: m_nNumColorViews(0)
	, m_pDepthStencilView(nullptr)
{
	Memory::MemZero(&m_pRenderTargetViews[0], (GFX::kMaxMultipleRenderTargets * sizeof(ID3D11RenderTargetView)));
}

IRenderTarget::Impl::~Impl(void)
{
}

// ------
// IRenderTarget
// -
// ------
IRenderTarget::IRenderTarget(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IRenderTarget::~IRenderTarget(void)
{
}

bool IRenderTarget::_create(const GFX::RENDER_TEXTURE_DESC& rtDesc)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	m_rtDesc = rtDesc;
	Funcs::ValidateExtents(m_rtDesc, m_rtExtent);

	FXD::U16 nAttachIdx = 0;
	for (FXD::U16 n1 = 0; n1 < GFX::kMaxMultipleRenderTargets; n1++)
	{
		const GFX::RENDER_SURFACE_DESC& surfaceDesc = m_rtDesc.ColorSurfaces[n1];
		if (surfaceDesc.Image == nullptr)
		{
			continue;
		}
		if (!surfaceDesc.Image->imageDesc().Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_RENDER_TARGET))
		{
			PRINT_ASSERT << "GFX: Provided texture is not created with render target usage";
		}

		GFX::IMAGEVIEW_DESC imageViewDesc = {};
		imageViewDesc.MostDetailMip = surfaceDesc.MipLevel;
		imageViewDesc.NumMips = 1;
		imageViewDesc.FirstArraySlice = surfaceDesc.Face;
		imageViewDesc.NumArraySlices = surfaceDesc.NumFaces;
		m_colorSurfaceView[nAttachIdx] = m_pAdapter->create_image_view(surfaceDesc.Image.get(), imageViewDesc).get();

		m_impl->m_pRenderTargetViews[nAttachIdx] = m_colorSurfaceView[nAttachIdx]->impl().view_render_target();
		nAttachIdx++;
	}

	m_impl->m_nNumColorViews = nAttachIdx;

	{
		const GFX::RENDER_SURFACE_DESC& surfaceDesc = m_rtDesc.DepthStencilSurface;
		if (surfaceDesc.Image != nullptr)
		{
			if (!surfaceDesc.Image->imageDesc().Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL))
			{
				PRINT_ASSERT << "GFX: Provided texture is not created with depth stencil usage";
			}

			GFX::IMAGEVIEW_DESC imageViewDesc = {};
			imageViewDesc.MostDetailMip = surfaceDesc.MipLevel;
			imageViewDesc.NumMips = 1;
			imageViewDesc.FirstArraySlice = surfaceDesc.Face;
			imageViewDesc.NumArraySlices = surfaceDesc.NumFaces;
			m_depthSurfaceView = m_pAdapter->create_image_view(surfaceDesc.Image.get(), imageViewDesc).get();

			m_impl->m_pDepthStencilView = m_depthSurfaceView->impl().view_depth();
			nAttachIdx++;
		}
	}

	return true;
}

bool IRenderTarget::_release(void)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	m_rtDesc = GFX::RENDER_TEXTURE_DESC();
	m_rtExtent = GFX::RENDER_TARGET_EXTENT();

	m_impl->m_nNumColorViews = 0;
	for (FXD::U16 n1 = 0; n1 < GFX::kMaxMultipleRenderTargets; n1++)
	{
		FXD_RELEASE(m_impl->m_pRenderTargetViews[n1], Release());
		FXD_RELEASE(m_colorSurfaceView[n1], release().get());
	}

	FXD_RELEASE(m_impl->m_pDepthStencilView, Release());
	FXD_RELEASE(m_depthSurfaceView, release().get());
	return true;
}

bool IRenderTarget::_activate_and_clear(const Core::ColourRGBA& colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil) const
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	_activate();
	_clear(colour, eClearFlags, fZ, fDepth, nStencil);

	return true;
}

bool IRenderTarget::_activate(void) const
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	GFX::Cache::RenderTarget::Set(m_pAdapter->impl().d3dDevContext(), m_impl->m_pRenderTargetViews, m_impl->m_nNumColorViews, m_impl->m_pDepthStencilView);

	D3D11_VIEWPORT d3dViewport =
	{
		0.0f, 0.0f,
		(FLOAT)m_rtExtent.Width, (FLOAT)m_rtExtent.Height,
		0.0f, 1.0f
	};
	GFX::Cache::Viewport::Set(m_pAdapter->impl().d3dDevContext(), &d3dViewport, 1);
	return true;
}

bool IRenderTarget::_clear(const Core::ColourRGBA& colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil) const
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	if (eClearFlags.is_flag_set(GFX::E_ClearFlag::Colour))
	{
		const FLOAT fClearColor[] = { colour.getR(), colour.getG(), colour.getB(), colour.getA() };
		for (FXD::U32 n1 = 0; n1 < GFX::kMaxMultipleRenderTargets; n1++)
		{
			if (m_impl->m_pRenderTargetViews[n1] != nullptr)
			{
				m_pAdapter->impl().d3dDevContext()->ClearRenderTargetView(m_impl->m_pRenderTargetViews[n1], fClearColor);
			}
		}
	}

	if (m_impl->m_pDepthStencilView != nullptr)
	{
		UINT nDepthStencilFlag = 0;
		if (eClearFlags.is_flag_set(GFX::E_ClearFlag::Depth))
		{
			nDepthStencilFlag |= D3D11_CLEAR_DEPTH;
		}
		if (eClearFlags.is_flag_set(GFX::E_ClearFlag::Stencil))
		{
			nDepthStencilFlag |= D3D11_CLEAR_STENCIL;
		}
		if (nDepthStencilFlag != 0)
		{
			m_pAdapter->impl().d3dDevContext()->ClearDepthStencilView(m_impl->m_pDepthStencilView, nDepthStencilFlag, fZ, (UINT8)nStencil);
		}
	}
	return true;
}
#endif //IsGFXDX11()