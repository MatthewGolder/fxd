// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/Window.h"
#include "FXDEngine/GFX/RenderTarget.h"
#include "FXDEngine/GFX/DX11/GfxAdapterDX11.h"
#include "FXDEngine/GFX/DX11/GfxApiDX11.h"
#include "FXDEngine/GFX/DX11/ImageDX11.h"
#include "FXDEngine/GFX/DX11/RenderTargetDX11.h"
#include "FXDEngine/GFX/DX11/SwapChainDX11.h"
#include "FXDEngine/GFX/DX11/ViewportDX11.h"
#include "FXDEngine/Job/EventHandler.h"

using namespace FXD;
using namespace GFX;

// ------
// IViewport::Impl::Impl
// - 
// ------
IViewport::Impl::Impl::Impl(void)
{
}

IViewport::Impl::~Impl(void)
{
}

// ------
// IViewport
// - 
// ------
IViewport::IViewport(IGfxAdapter* pAdapter, GFX::IWindow* pWindow)
	: m_pAdapter(pAdapter)
	, m_pWindow(pWindow)
	, m_size(pWindow->window_desc().size)
	, OnWindowResize(nullptr)
{
	OnWindowResize = FXDEvent()->attach< GFX::OnWindowResize >([&](auto evt)
	{
		if (evt.WindowHandle == m_pWindow->windowHandle())
		{
			resize(evt.Pos);
		}
	});
}

IViewport::~IViewport(void)
{
	if (OnWindowResize != nullptr)
	{
		FXDEvent()->detach(OnWindowResize);
		OnWindowResize = nullptr;
	}
}

bool IViewport::_create(void)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	// 1. Create a swap chain
	m_swapChain = std::make_shared< GFX::ISwapChain >(m_pAdapter, this);
	PRINT_COND_ASSERT(m_swapChain->create(), "GFX: Failed m_swapChain->create()");
	return true;
}

bool IViewport::_release(void)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	m_pWindow = nullptr;
	FXD_RELEASE(m_swapChain, release());
	return true;
}

bool IViewport::_resize(const Math::Vector2I newSize)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	if ((newSize.x() == 0) || (newSize.y() == 0) || (m_size == newSize))
	{
		return true;
	}
	else
	{
		m_size = newSize;
		if (m_swapChain)
		{
			m_swapChain->resize(newSize);
		}
	}
	return true;
}

void IViewport::_clear(const Core::ColourRGBA& colour, GFX::E_ClearFlags eClearFlags, FXD::F32 fZ, FXD::F32 fDepth, FXD::U32 nStencil) const
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	m_swapChain->render_target()->clear(colour, eClearFlags, fZ, fDepth, nStencil);
}

void IViewport::_begin_drawing_viewport(void) const
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	m_swapChain->render_target()->activate();
	//GFX::ScissorRect::set(false, 0, 0, 0, 0);
}

void IViewport::_end_drawing_viewport(void) const
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	m_swapChain->swap(m_pWindow->window_desc().bVSync);
}
#endif //IsGFXDX11()