// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/DX11/DX11State.h"
#include "FXDEngine/GFX/DX11/MappingDX11.h"
#include "FXDEngine/Memory/Memory.h"

using namespace FXD;
using namespace GFX;

// ------
// Index Layout
// -
// ------
struct _DX11InputLayout
{
	ID3D11InputLayout* Layout = nullptr;
} m_indexLayout;
static _DX11InputLayout s_indexLayout = {};

void FXD::GFX::Cache::InputLayout::Set(ID3D11DeviceContext* pDevContext, ID3D11InputLayout* pInputLayout)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();
	if (s_indexLayout.Layout != pInputLayout)
	{
		s_indexLayout.Layout = pInputLayout;
		pDevContext->IASetInputLayout(pInputLayout);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

// ------
// Vertex Buffer
// -
// ------
struct _DX11VertexBuffer
{
	FXD::U32 Stride = 0;
	FXD::U32 Offset = 0;
	ID3D11Buffer* Buffer = nullptr;
};
static _DX11VertexBuffer s_vertexBuffers[D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT] = {};

void FXD::GFX::Cache::VertexBuffer::Set(ID3D11DeviceContext* pDevContext, ID3D11Buffer* pVertexBuffer, FXD::U32 nStreamIndex, FXD::U32 nStride, FXD::U32 nOffset)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();
	PRINT_COND_ASSERT(nStreamIndex < FXD::STD::array_size(s_vertexBuffers), "GFX: Error out of bounds");
	_DX11VertexBuffer& vbSlot = s_vertexBuffers[nStreamIndex];
	if ((vbSlot.Stride != nStride) || (vbSlot.Offset != nOffset) || (vbSlot.Buffer != pVertexBuffer))
	{
		vbSlot.Stride = nStride;
		vbSlot.Offset = nOffset;
		vbSlot.Buffer = pVertexBuffer;
		pDevContext->IASetVertexBuffers(nStreamIndex, 1, &pVertexBuffer, &nStride, &nOffset);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

// ------
// Index Buffer
// -
// ------
struct _DX11IndexBuffer
{
	DXGI_FORMAT Format = DXGI_FORMAT_UNKNOWN;
	FXD::U32 Offset = 0;
	ID3D11Buffer* Buffer = nullptr;
};
static _DX11IndexBuffer s_indexBuffer = {};

void FXD::GFX::Cache::IndexBuffer::Set(ID3D11DeviceContext* pDevContext, ID3D11Buffer* pIndexBuffer, GFX::E_IndexType eType, FXD::U32 nOffset)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();
	DXGI_FORMAT d3dFormat = GFX::Index::GetFormat(eType);
	if ((s_indexBuffer.Format != d3dFormat) || (s_indexBuffer.Offset != nOffset) || (s_indexBuffer.Buffer != pIndexBuffer))
	{
		s_indexBuffer.Format = d3dFormat;
		s_indexBuffer.Offset = nOffset;
		s_indexBuffer.Buffer = pIndexBuffer;
		pDevContext->IASetIndexBuffer(pIndexBuffer, d3dFormat, nOffset);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

// ------
// Shader Resource
// -
// ------
struct _DX11ShaderResource
{
	ID3D11ShaderResourceView* Views[D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT];
};
static _DX11ShaderResource s_shaderResources[FXD::STD::to_underlying(FX::E_ShaderType::Count)] = {};

void FXD::GFX::Cache::ShaderResourceView::SetVertex(ID3D11DeviceContext* pDevContext, ID3D11ShaderResourceView* pSRV, FXD::U32 nResourceIndex)
{	
	//D3D11_STATE_CACHE_VERIFY_PRE();
	PRINT_COND_ASSERT(nResourceIndex < FXD::STD::array_size(s_shaderResources[FXD::STD::to_underlying(FX::E_ShaderType::Vertex)].Views), "");

	if (s_shaderResources[FXD::STD::to_underlying(FX::E_ShaderType::Vertex)].Views[nResourceIndex] != pSRV)
	{
		if (pSRV != nullptr)
		{
			pSRV->AddRef();
		}
		if (s_shaderResources[FXD::STD::to_underlying(FX::E_ShaderType::Vertex)].Views[nResourceIndex] != nullptr)
		{
			s_shaderResources[FXD::STD::to_underlying(FX::E_ShaderType::Vertex)].Views[nResourceIndex]->Release();
		}
		s_shaderResources[FXD::STD::to_underlying(FX::E_ShaderType::Vertex)].Views[nResourceIndex] = pSRV;
		pDevContext->VSSetShaderResources(nResourceIndex, 1, &pSRV);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

void FXD::GFX::Cache::ShaderResourceView::SetGeometry(ID3D11DeviceContext* pDevContext, ID3D11ShaderResourceView* pSRV, FXD::U32 nResourceIndex)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();
	PRINT_COND_ASSERT(nResourceIndex < FXD::STD::array_size(s_shaderResources[FXD::STD::to_underlying(FX::E_ShaderType::Geometry)].Views), "");

	if (s_shaderResources[FXD::STD::to_underlying(FX::E_ShaderType::Geometry)].Views[nResourceIndex] != pSRV)
	{
		if (pSRV != nullptr)
		{
			pSRV->AddRef();
		}
		if (s_shaderResources[FXD::STD::to_underlying(FX::E_ShaderType::Geometry)].Views[nResourceIndex] != nullptr)
		{
			s_shaderResources[FXD::STD::to_underlying(FX::E_ShaderType::Geometry)].Views[nResourceIndex]->Release();
		}
		s_shaderResources[FXD::STD::to_underlying(FX::E_ShaderType::Geometry)].Views[nResourceIndex] = pSRV;
		pDevContext->GSSetShaderResources(nResourceIndex, 1, &pSRV);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

void FXD::GFX::Cache::ShaderResourceView::SetFragment(ID3D11DeviceContext* pDevContext, ID3D11ShaderResourceView* pSRV, FXD::U32 nResourceIndex)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();
	PRINT_COND_ASSERT(nResourceIndex < FXD::STD::array_size(s_shaderResources[FXD::STD::to_underlying(FX::E_ShaderType::Fragment)].Views), "");

	if ((s_shaderResources[FXD::STD::to_underlying(FX::E_ShaderType::Fragment)].Views[nResourceIndex] != pSRV))
	{
		if (pSRV != nullptr)
		{
			pSRV->AddRef();
		}
		if (s_shaderResources[FXD::STD::to_underlying(FX::E_ShaderType::Fragment)].Views[nResourceIndex] != nullptr)
		{
			s_shaderResources[FXD::STD::to_underlying(FX::E_ShaderType::Fragment)].Views[nResourceIndex]->Release();
		}
		s_shaderResources[FXD::STD::to_underlying(FX::E_ShaderType::Fragment)].Views[nResourceIndex] = pSRV;
		pDevContext->PSSetShaderResources(nResourceIndex, 1, &pSRV);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

// ------
// Shader
// -
// ------
struct _DX11Shaders
{
	ID3D11DeviceChild* Shader;
};
static _DX11Shaders s_shaders[FXD::STD::to_underlying(FX::E_ShaderType::Count)] = {};

void FXD::GFX::Cache::Shader::SetVertex(ID3D11DeviceContext* pDevContext, ID3D11VertexShader* pShader)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();
	if (s_shaders[FXD::STD::to_underlying(FX::E_ShaderType::Vertex)].Shader != pShader)
	{
		s_shaders[FXD::STD::to_underlying(FX::E_ShaderType::Vertex)].Shader = pShader;
		pDevContext->VSSetShader(pShader, nullptr, 0);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

void FXD::GFX::Cache::Shader::SetGeometry(ID3D11DeviceContext* pDevContext, ID3D11GeometryShader* pShader)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();
	if (s_shaders[FXD::STD::to_underlying(FX::E_ShaderType::Geometry)].Shader != pShader)
	{
		s_shaders[FXD::STD::to_underlying(FX::E_ShaderType::Geometry)].Shader = pShader;
		pDevContext->GSSetShader(pShader, nullptr, 0);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

void FXD::GFX::Cache::Shader::SetFragment(ID3D11DeviceContext* pDevContext, ID3D11PixelShader* pShader)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();
	if (s_shaders[FXD::STD::to_underlying(FX::E_ShaderType::Fragment)].Shader != pShader)
	{
		s_shaders[FXD::STD::to_underlying(FX::E_ShaderType::Fragment)].Shader = pShader;
		pDevContext->PSSetShader(pShader, nullptr, 0);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

// ------
// Uniform Buffers
// -
// ------
struct _DX11UniformBuffers
{
	ID3D11DeviceChild* Buffers[D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT];
};
static _DX11UniformBuffers s_uniformBuffers[FXD::STD::to_underlying(FX::E_ShaderType::Count)] = {};

void FXD::GFX::Cache::UniformBuffer::SetVertex(ID3D11DeviceContext* pDevContext, ID3D11Buffer* pUniformBuffer, FXD::U32 nSlotIndex)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();

	ID3D11DeviceChild* pCurrentBuffer = s_uniformBuffers[FXD::STD::to_underlying(FX::E_ShaderType::Vertex)].Buffers[nSlotIndex];
	if (pCurrentBuffer != pUniformBuffer)
	{
		pCurrentBuffer = pUniformBuffer;
		pDevContext->VSSetConstantBuffers(nSlotIndex, 1, &pUniformBuffer);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

void FXD::GFX::Cache::UniformBuffer::SetGeometry(ID3D11DeviceContext* pDevContext, ID3D11Buffer* pUniformBuffer, FXD::U32 nSlotIndex)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();

	ID3D11DeviceChild* pCurrentBuffer = s_uniformBuffers[FXD::STD::to_underlying(FX::E_ShaderType::Geometry)].Buffers[nSlotIndex];
	if (pCurrentBuffer != pUniformBuffer)
	{
		pCurrentBuffer = pUniformBuffer;
		pDevContext->GSSetConstantBuffers(nSlotIndex, 1, &pUniformBuffer);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

void FXD::GFX::Cache::UniformBuffer::SetFragment(ID3D11DeviceContext* pDevContext, ID3D11Buffer* pUniformBuffer, FXD::U32 nSlotIndex)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();

	ID3D11DeviceChild* pCurrentBuffer = s_uniformBuffers[FXD::STD::to_underlying(FX::E_ShaderType::Fragment)].Buffers[nSlotIndex];
	if (pCurrentBuffer != pUniformBuffer)
	{
		pCurrentBuffer = pUniformBuffer;
		pDevContext->PSSetConstantBuffers(nSlotIndex, 1, &pUniformBuffer);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

// ------
// Sampler States
// -
// ------
struct _DX11SamplerStates
{
	ID3D11SamplerState* States[D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT];
};
static _DX11SamplerStates s_samplerStates[FXD::STD::to_underlying(FX::E_ShaderType::Count)] = {};

void FXD::GFX::Cache::SamplerState::SetVertex(ID3D11DeviceContext* pDevContext, ID3D11SamplerState* pSamplerState, FXD::U32 nSamplerIndex)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();

	ID3D11SamplerState* pCurrent = s_samplerStates[FXD::STD::to_underlying(FX::E_ShaderType::Vertex)].States[nSamplerIndex];
	if (pCurrent != pSamplerState)
	{
		pCurrent = pSamplerState;
		pDevContext->VSSetSamplers(nSamplerIndex, 1, &pSamplerState);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

void FXD::GFX::Cache::SamplerState::SetGeometry(ID3D11DeviceContext* pDevContext, ID3D11SamplerState* pSamplerState, FXD::U32 nSamplerIndex)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();

	ID3D11SamplerState* pCurrent = s_samplerStates[FXD::STD::to_underlying(FX::E_ShaderType::Geometry)].States[nSamplerIndex];
	if (pCurrent != pSamplerState)
	{
		pCurrent = pSamplerState;
		pDevContext->GSSetSamplers(nSamplerIndex, 1, &pSamplerState);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

void FXD::GFX::Cache::SamplerState::SetFragment(ID3D11DeviceContext* pDevContext, ID3D11SamplerState* pSamplerState, FXD::U32 nSamplerIndex)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();

	ID3D11SamplerState* pCurrent = s_samplerStates[FXD::STD::to_underlying(FX::E_ShaderType::Fragment)].States[nSamplerIndex];
	if (pCurrent != pSamplerState)
	{
		pCurrent = pSamplerState;
		pDevContext->PSSetSamplers(nSamplerIndex, 1, &pSamplerState);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

// ------
// Render Target
// -
// ------
struct _DX11RenderTarget
{
	FXD::U32 Count = 0;
	ID3D11RenderTargetView* RTViews[D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT];
	ID3D11DepthStencilView* DSView = nullptr;
};
static _DX11RenderTarget s_renderTarget = {};

void FXD::GFX::Cache::RenderTarget::Set(ID3D11DeviceContext* pDevContext, ID3D11RenderTargetView** pRTViews, FXD::U32 nCount, ID3D11DepthStencilView* pDSView)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();
	if (s_renderTarget.Count != nCount || Memory::MemCompare(&s_renderTarget.RTViews[0], &pRTViews, (sizeof(ID3D11RenderTargetView) * nCount)) || s_renderTarget.DSView != pDSView)
	{
		s_renderTarget.Count = nCount;
		Memory::MemCopy(s_renderTarget.RTViews, pRTViews, (sizeof(ID3D11RenderTargetView) * nCount));
		s_renderTarget.DSView = pDSView;
		pDevContext->OMSetRenderTargets(nCount, pRTViews, pDSView);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

// ------
// Depth Stencil State
// -
// ------
struct _DX11DepthStencilState
{
	FXD::U32 ReferenceStencil = 0;
	ID3D11DepthStencilState* State = nullptr;
};
static _DX11DepthStencilState s_depthStencilState = {};

void FXD::GFX::Cache::DepthStencil::Set(ID3D11DeviceContext* pDevContext, ID3D11DepthStencilState* pState, FXD::U32 nRefStencil)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();
	DXGI_FORMAT d3dFormat = GFX::Index::GetFormat(GFX::E_IndexType::Bit_16);
	if ((s_depthStencilState.State != pState) || (s_depthStencilState.ReferenceStencil != nRefStencil))
	{
		s_depthStencilState.ReferenceStencil = nRefStencil;
		s_depthStencilState.State = pState;
		pDevContext->OMSetDepthStencilState(pState, nRefStencil);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

// ------
// Blend State
// -
// ------
struct _DX11BlendState
{
	FXD::F32 Factor[4];
	FXD::U32 SampleMask = 0;
	ID3D11BlendState* State = nullptr;
};
static _DX11BlendState s_blendState = {};

void FXD::GFX::Cache::BlendState::Set(ID3D11DeviceContext* pDevContext, ID3D11BlendState* pState, const FXD::F32 fBlendFactor[4], FXD::U32 nSampleMask)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();
	if (Memory::MemCompare(s_blendState.Factor, fBlendFactor, sizeof(s_blendState.Factor)) || (s_blendState.SampleMask != nSampleMask) || (s_blendState.State != pState))
	{
		Memory::MemCopy(s_blendState.Factor, fBlendFactor, sizeof(s_blendState.Factor));
		s_blendState.SampleMask = nSampleMask;
		s_blendState.State = pState;
		pDevContext->OMSetBlendState(pState, fBlendFactor, nSampleMask);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

void FXD::GFX::Cache::BlendState::Set(ID3D11DeviceContext* pDevContext, const FXD::F32 fBlendFactor[4], FXD::U32 nSampleMask)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();
	if (Memory::MemCompare(s_blendState.Factor, fBlendFactor, sizeof(s_blendState.Factor)) || (s_blendState.SampleMask != nSampleMask))
	{
		Memory::MemCopy(s_blendState.Factor, fBlendFactor, sizeof(s_blendState.Factor));
		s_blendState.SampleMask = nSampleMask;
		pDevContext->OMSetBlendState(s_blendState.State, fBlendFactor, nSampleMask);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

// ------
// Raster State
// -
// ------
struct _DX11RasterState
{
	ID3D11RasterizerState* State = nullptr;
};
static _DX11RasterState s_rasterState = {};

void FXD::GFX::Cache::RasterState::Set(ID3D11DeviceContext* pDevContext, ID3D11RasterizerState* pState)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();
	if (s_rasterState.State != pState)
	{
		s_rasterState.State = pState;
		pDevContext->RSSetState(pState);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

// ------
// Topology
// -
// ------
struct _DX11Topology
{
	D3D11_PRIMITIVE_TOPOLOGY Topology = D3D_PRIMITIVE_TOPOLOGY_UNDEFINED;
};
static _DX11Topology s_topology = {};

void FXD::GFX::Cache::Topology::Set(ID3D11DeviceContext* pDevContext, const GFX::E_Primitive ePrimitive)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();

	D3D11_PRIMITIVE_TOPOLOGY d3dPrimitive = DX11::GetPrimitiveType(ePrimitive);
	if (s_topology.Topology != d3dPrimitive)
	{
		s_topology.Topology = d3dPrimitive;
		pDevContext->IASetPrimitiveTopology(d3dPrimitive);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

// ------
// Viewport
// -
// ------
struct _DX11Viewport
{
	FXD::U32 NumOfViewports = 0;
	D3D11_VIEWPORT Viewports[D3D11_VIEWPORT_AND_SCISSORRECT_OBJECT_COUNT_PER_PIPELINE];
};
static _DX11Viewport s_viewports = {};

void FXD::GFX::Cache::Viewport::Set(ID3D11DeviceContext* pDevContext, D3D11_VIEWPORT* pViewports, FXD::U32 nCount)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();

	if ((s_viewports.NumOfViewports != nCount) || Memory::MemCompare(&s_viewports.Viewports[0], &pViewports, (sizeof(D3D11_VIEWPORT) * nCount)))
	{
		s_viewports.NumOfViewports = nCount;
		Memory::MemCopy(&s_viewports.Viewports[0], pViewports, (sizeof(D3D11_VIEWPORT) * nCount));
		pDevContext->RSSetViewports(nCount, pViewports);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

// ------
// ScissorRect
// -
// ------
struct _DX11ScissorRects
{
	FXD::U32 NumOfRects = 0;
	D3D11_RECT Rects[D3D11_VIEWPORT_AND_SCISSORRECT_OBJECT_COUNT_PER_PIPELINE];
};
static _DX11ScissorRects s_scissorRects = {};

void FXD::GFX::Cache::ScissorRect::Set(ID3D11DeviceContext* pDevContext, D3D11_RECT* pRects, FXD::U32 nCount)
{
	//D3D11_STATE_CACHE_VERIFY_PRE();

	if ((s_scissorRects.NumOfRects != nCount) || Memory::MemCompare(&s_scissorRects.Rects[0], &pRects, (sizeof(D3D11_RECT) * nCount)))
	{
		s_scissorRects.NumOfRects = nCount;
		Memory::MemCopy(s_scissorRects.Rects, pRects, (sizeof(D3D11_RECT) * nCount));
		pDevContext->RSSetScissorRects(nCount, pRects);
	}
	//D3D11_STATE_CACHE_VERIFY_POST();
}

#endif //IsGFXDX11()