// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/DX11/GfxCommandBufferDX11.h"
#include "FXDEngine/GFX/DX11/GfxAdapterDX11.h"

using namespace FXD;
using namespace GFX;

// ------
// IGfxCommandBuffer::Impl::Impl
// - 
// ------
IGfxCommandBuffer::Impl::Impl::Impl(GFX::IGfxCommandBuffer* pParent)
	: m_pParent(pParent)
{
}
IGfxCommandBuffer::Impl::~Impl(void)
{
}

void IGfxCommandBuffer::Impl::queue_command(const FXD::STD::function< void() > command)
{
	m_commands.push_back(command);
}

void IGfxCommandBuffer::Impl::append_secondary(const GFX::GfxCommandBuffer& secondaryBuffer)
{
#if defined(FXD_DEBUG)
	//PRINT_COND_ASSERT(!secondaryBuffer->bSecondary(), "GFX: The secondary buffer is not set up to be a secondary buffer");
	//PRINT_COND_ASSERT(m_pParent->bSecondary(), "GFX: The buffer is not set up to be a secondary buffer");
#endif

	for (auto& command : secondaryBuffer->impl().m_commands)
	{
		m_commands.push_back(command);
	}
}

void IGfxCommandBuffer::Impl::execute_commands(void)
{
#if defined(FXD_DEBUG)
	/*
	if (m_pParent->bSecondary())
	{
		PRINT_ASSERT << "GFX: Cannot execute commands on a secondary buffer";
		return;
	}
	*/
#endif

	for (auto& command : m_commands)
	{
		command();
	}
}

void IGfxCommandBuffer::Impl::clear(void)
{
	m_commands.clear();
}

// ------
// IGfxCommandBuffer
// - 
// ------
IGfxCommandBuffer::IGfxCommandBuffer(GFX::IGfxAdapter* pAdapter)
	: HasImpl(Core::_Pimpl_Pack(), this)
	, m_pAdapter(pAdapter)
	, m_eQueueType(GFX::E_GfxQueueType::Unknown)
	//, m_nDeviceID(0)
	, m_nQueueID(0)
	, m_eType(GFX::E_CommandBufferType::Unknown)
{
}
IGfxCommandBuffer::~IGfxCommandBuffer(void)
{
}

bool IGfxCommandBuffer::_create(GFX::E_GfxQueueType eQueueType, FXD::U32 nQueueID, GFX::E_CommandBufferType eType)
{
	m_eQueueType = eQueueType;
	m_nQueueID = nQueueID;
	m_eType = eType;

	return true;
}

bool IGfxCommandBuffer::_release(void)
{
	return true;
}

void IGfxCommandBuffer::_begin(void)
{
	PRINT_COND_ASSERT(m_eState == E_CommandBufferState::Ready, "");

	m_eState = E_CommandBufferState::Recording;
}

void IGfxCommandBuffer::_end(void)
{
	PRINT_COND_ASSERT(m_eState == E_CommandBufferState::Recording, "");

	m_eState = E_CommandBufferState::RecordingDone;
}

void IGfxCommandBuffer::_begin_render_pass(void)
{
	PRINT_COND_ASSERT(m_eState == E_CommandBufferState::Ready, "");

	m_eState = E_CommandBufferState::Recording;
}

void IGfxCommandBuffer::_end_render_pass(void)
{
	PRINT_COND_ASSERT(m_eState == E_CommandBufferState::Ready, "");

	m_eState = E_CommandBufferState::Recording;
}

// ------
// IGfxCommandBufferManager::Impl::Impl
// - 
// ------
IGfxCommandBufferManager::Impl::Impl::Impl(void)
{
}
IGfxCommandBufferManager::Impl::~Impl(void)
{
}

// ------
// IGfxCommandBufferManager
// - 
// ------
IGfxCommandBufferManager::IGfxCommandBufferManager(void)
{
}
IGfxCommandBufferManager::~IGfxCommandBufferManager(void)
{
}

bool IGfxCommandBufferManager::_create(void)
{
	return true;
}

bool IGfxCommandBufferManager::_release(void)
{
	return true;
}
#endif //IsGFXDX11()