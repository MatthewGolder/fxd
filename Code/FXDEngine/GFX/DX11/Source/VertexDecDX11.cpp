// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/DX11/GfxAdapterDX11.h"
#include "FXDEngine/GFX/DX11/MappingDX11.h"
#include "FXDEngine/GFX/DX11/VertexDecDX11.h"

using namespace FXD;
using namespace GFX;

//const ID3D11InputLayout* pD3DCurrentInputLayout = nullptr;

// ------
// IVertexDecleration::Impl::Impl::Impl
// - 
// ------
IVertexDecleration::Impl::Impl::Impl(void)
{
	Memory::MemZero_T(m_d3dVertexElements);
}

IVertexDecleration::Impl::~Impl(void)
{
	PRINT_COND_ASSERT((!m_vertexLayout), "GFX: m_vertexLayout is not shutdown");
}

// ------
// IVertexDecleration
// - 
// ------
IVertexDecleration::IVertexDecleration(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IVertexDecleration::~IVertexDecleration(void)
{
}

bool IVertexDecleration::_create(const FX::VaryingListDef& varyingList, const Memory::MemHandle& mem)
{	
	FXD::U32 nOffSet = 0;
	FXD::U32 nID = 0;
	fxd_for(const auto& param, varyingList.Params)
	{
		if (param.DataType == FX::E_FXDataType::Unknown)
		{
			break;
		}

		D3D11_INPUT_ELEMENT_DESC& element = m_impl->m_d3dVertexElements[nID];
		element.SemanticIndex =		DX11FXSemantics[FXD::STD::to_underlying(param.SemanticType)].id;
		element.SemanticName =		DX11FXSemantics[FXD::STD::to_underlying(param.SemanticType)].semanticName.c_str();
		element.Format =				DX11FXSemantics[FXD::STD::to_underlying(param.SemanticType)].format;
		element.InputSlot =			nOffSet;
		element.InputSlotClass =	D3D11_INPUT_PER_VERTEX_DATA;

		nOffSet += DX11FXSemantics[FXD::STD::to_underlying(param.SemanticType)].size;
		nID++;
	}

	if (DX11ResultCheckFail(m_pAdapter->impl().d3dDevice()->CreateInputLayout(&m_impl->m_d3dVertexElements[0], nID, Memory::MemHandle::LockGuard(mem).get_mem(), mem.get_size(), m_impl->m_vertexLayout)))
	{
		PRINT_ASSERT << "GFX: Failed CreateInputLayout()";
	}
	return true;
}

bool IVertexDecleration::_release(void)
{
	m_impl->m_vertexLayout = nullptr;
	return true;
}

void IVertexDecleration::_attach(void)
{
	//pD3DCurrentInputLayout = m_impl->m_vertexLayout;

	// 36 vertices needed for 12 triangles in a triangle list
	GFX::Cache::InputLayout::Set(m_pAdapter->impl().d3dDevContext(), m_impl->m_vertexLayout);
	GFX::Cache::Topology::Set(m_pAdapter->impl().d3dDevContext(), GFX::E_Primitive::TriList);
	m_pAdapter->impl().d3dDevContext()->DrawIndexed(36, 0, 0);
}

void IVertexDecleration::_detach(void)
{
	GFX::Cache::InputLayout::Set(m_pAdapter->impl().d3dDevContext(), nullptr);
}

#endif //IsGFXDX11()