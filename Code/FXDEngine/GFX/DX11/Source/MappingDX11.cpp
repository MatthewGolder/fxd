// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/DX11/MappingDX11.h"

using namespace FXD;
using namespace GFX;

// DX11FXSemantics
const DX11FXSemantic DX11FXSemantics[] =
{
	{ DXGI_FORMAT_R32G32B32_FLOAT,		12, 0, "Position",		"Position" },	// E_SemanticType::Position
	{ DXGI_FORMAT_R32G32B32_FLOAT,		12, 0, "SV_Position",	"Position" },	// E_SemanticType::SV_Position
	{ DXGI_FORMAT_R32G32B32A32_FLOAT,	16, 0, "Color0",			"Color" },		// E_SemanticType::Color0
	{ DXGI_FORMAT_R32G32B32A32_FLOAT,	16, 1, "Color1",			"Color" },		// E_SemanticType::Color1
	{ DXGI_FORMAT_R32G32B32_FLOAT,		12, 0, "Normal0",			"Normal" },		// E_SemanticType::Normal0
	{ DXGI_FORMAT_R32G32B32_FLOAT,		12, 1, "Normal1",			"Normal" },		// E_SemanticType::Normal1
	{ DXGI_FORMAT_R32G32B32_FLOAT,		12, 0, "Binormal0",		"Binormal" },	// E_SemanticType::Binormal0
	{ DXGI_FORMAT_R32G32B32_FLOAT,		12, 1, "Binormal1",		"Binormal" },	// E_SemanticType::Binormal1
	{ DXGI_FORMAT_R32G32B32_FLOAT,		12, 0, "Tangent0",		"Tangent" },	// E_SemanticType::Tangent0
	{ DXGI_FORMAT_R32G32B32_FLOAT,		12, 1, "Tangent1",		"Tangent" },	// E_SemanticType::Tangent1
	{ DXGI_FORMAT_R32G32_FLOAT,			 8, 0, "TexCoord0",		"TexCoord" },	// E_SemanticType::TexCoord0
	{ DXGI_FORMAT_R32G32_FLOAT,			 8, 1, "TexCoord1",		"TexCoord" },	// E_SemanticType::TexCoord1
	{ DXGI_FORMAT_R32G32_FLOAT,			 8, 2, "TexCoord2",		"TexCoord" },	// E_SemanticType::TexCoord2
	{ DXGI_FORMAT_R32G32_FLOAT,			 8, 3, "TexCoord3",		"TexCoord" },	// E_SemanticType::TexCoord3
	{ DXGI_FORMAT_R32G32_FLOAT,			 8, 4, "TexCoord4",		"TexCoord" },	// E_SemanticType::TexCoord4
	{ DXGI_FORMAT_R32G32_FLOAT,			 8, 5, "TexCoord5",		"TexCoord" },	// E_SemanticType::TexCoord5
	{ DXGI_FORMAT_R32G32_FLOAT,			 8, 6, "TexCoord6",		"TexCoord" },	// E_SemanticType::TexCoord6
	{ DXGI_FORMAT_R32G32_FLOAT,			 8, 7, "TexCoord7",		"TexCoord" },	// E_SemanticType::TexCoord7
	{ DXGI_FORMAT_R32G32_FLOAT,			 8, 8, "TexCoord8",		"TexCoord" }	// E_SemanticType::TexCoord8
};

CONSTEXPR const D3D11_PRIMITIVE_TOPOLOGY kD3DPrimitives[] =
{
	D3D11_PRIMITIVE_TOPOLOGY_LINELIST,			// E_Primitive::LineList
	D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP,			// E_Primitive::LineStrip
	D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP,	// E_Primitive::TriStrip
	D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,			// E_Primitive::PointList
	D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,		// E_Primitive::TriList
	D3D_PRIMITIVE_TOPOLOGY_UNDEFINED,			// E_Primitive::TriFan
	D3D_PRIMITIVE_TOPOLOGY_UNDEFINED,			// E_Primitive::QuadList
	D3D_PRIMITIVE_TOPOLOGY_UNDEFINED				// E_Primitive::Count
};
const D3D11_PRIMITIVE_TOPOLOGY FXD::GFX::DX11::GetPrimitiveType(GFX::E_Primitive eType)
{
	return kD3DPrimitives[FXD::STD::to_underlying(eType)];
}

CONSTEXPR const D3D11_FILL_MODE kD3DFillMode[] =
{
	D3D11_FILL_SOLID,			// E_FillMode::Point
	D3D11_FILL_WIREFRAME,	// E_FillMode::WireFrame
	D3D11_FILL_SOLID,			// E_FillMode::Solid
	D3D11_FILL_SOLID			// E_FillMode::Count
};
const D3D11_FILL_MODE FXD::GFX::DX11::GetFillMode(GFX::E_FillMode eMode)
{
	return kD3DFillMode[FXD::STD::to_underlying(eMode)];
}

CONSTEXPR const D3D11_CULL_MODE kD3DCullMode[] =
{
	D3D11_CULL_NONE,		// E_CullMode::None
	D3D11_CULL_FRONT,		// E_CullMode::Clockwise
	D3D11_CULL_BACK,		// E_CullMode::CounterClockwise
	D3D11_CULL_NONE		// E_CullMode::Count
};
const D3D11_CULL_MODE FXD::GFX::DX11::GetCullMode(GFX::E_CullMode eMode)
{
	return kD3DCullMode[FXD::STD::to_underlying(eMode)];
}


// ------
// Buffer
// -
// ------
const D3D11_BIND_FLAG FXD::GFX::Buffer::ConvertToDX11BindFlags(GFX::E_BufferUsageFlags eFlags)
{
	PRINT_COND_ASSERT((eFlags & (GFX::E_BufferUsageFlag::BIND_RENDER_TARGET | GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL)) != (GFX::E_BufferUsageFlag::BIND_RENDER_TARGET | GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL), "RenderTarget and DepthStencil can't be requested together!");

	return D3D11_BIND_FLAG(
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_VERTEX_BUFFER) ? D3D11_BIND_VERTEX_BUFFER : 0) |
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_INDEX_BUFFER) ? D3D11_BIND_INDEX_BUFFER : 0) |
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_UNIFORM_BUFFER) ? D3D11_BIND_CONSTANT_BUFFER : 0) |
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL) ? D3D11_BIND_DEPTH_STENCIL : 0) |
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_RENDER_TARGET) ? D3D11_BIND_RENDER_TARGET : 0) |
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::BIND_SHADER_RESOURCE) ? D3D11_BIND_SHADER_RESOURCE : 0)
	);
}

const D3D11_CPU_ACCESS_FLAG FXD::GFX::Buffer::ConvertToDX11CPUAccessFlags(GFX::E_BufferUsageFlags eFlags)
{
	return D3D11_CPU_ACCESS_FLAG(
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::USAGE_CPU_READ) ? D3D11_CPU_ACCESS_READ : 0) |
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::USAGE_CPU_WRITE) ? D3D11_CPU_ACCESS_WRITE : 0)
	);
}

const D3D11_USAGE FXD::GFX::Buffer::ConvertToDX11Usage(GFX::E_BufferUsageFlags eFlags)
{
	PRINT_COND_ASSERT((eFlags & (GFX::E_BufferUsageFlag::USAGE_CPU_READ | GFX::E_BufferUsageFlag::USAGE_CPU_WRITE)) != (GFX::E_BufferUsageFlag::USAGE_CPU_READ | GFX::E_BufferUsageFlag::USAGE_CPU_WRITE), "CPU Read and Write can't be requested together!");

	return D3D11_USAGE(
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::USAGE_CPU_READ) ? D3D11_USAGE_STAGING : D3D11_USAGE_DEFAULT) |
		(eFlags.is_flag_set(GFX::E_BufferUsageFlag::USAGE_CPU_WRITE) ? D3D11_USAGE_DYNAMIC : D3D11_USAGE_DEFAULT)
	);
}

// ------
// Pixel
// -
// ------
CONSTEXPR const DXGI_FORMAT kD3DPixelTypeDescs[] =
{
	{ DXGI_FORMAT_R8_UNORM },					// E_PixelFormat::R8
	{ DXGI_FORMAT_R8_UINT },					// E_PixelFormat::R8U
	{ DXGI_FORMAT_R8_SNORM },					// E_PixelFormat::R8S
	{ DXGI_FORMAT_R8_SINT },					// E_PixelFormat::R8I

	{ DXGI_FORMAT_R8G8_UNORM },				// E_PixelFormat::RG8
	{ DXGI_FORMAT_R8G8_UINT },					// E_PixelFormat::RG8U
	{ DXGI_FORMAT_R8G8_SNORM },				// E_PixelFormat::RG8S
	{ DXGI_FORMAT_R8G8_SINT },					// E_PixelFormat::RG8I

	{ DXGI_FORMAT_B8G8R8X8_UNORM },			// E_PixelFormat::BGR8
	{ DXGI_FORMAT_R8G8B8A8_UNORM },			// E_PixelFormat::RGB8
	{ DXGI_FORMAT_R8G8B8A8_UINT },			// E_PixelFormat::RGB8U
	{ DXGI_FORMAT_R8G8B8A8_SNORM },			// E_PixelFormat::RGB8S
	{ DXGI_FORMAT_R8G8B8A8_SINT },			// E_PixelFormat::RGB8I

	{ DXGI_FORMAT_B8G8R8A8_UNORM },			// E_PixelFormat::BGRA8
	{ DXGI_FORMAT_R8G8B8A8_UNORM },			// E_PixelFormat::RGBA8
	{ DXGI_FORMAT_R8G8B8A8_UINT },			// E_PixelFormat::RGBA8U
	{ DXGI_FORMAT_R8G8B8A8_SNORM },			// E_PixelFormat::RGBA8S
	{ DXGI_FORMAT_R8G8B8A8_SINT },			// E_PixelFormat::RGBA8I

	{ DXGI_FORMAT_R16_UNORM },					// E_PixelFormat::R16
	{ DXGI_FORMAT_R16_UINT },					// E_PixelFormat::R16U
	{ DXGI_FORMAT_R16_SNORM },					// E_PixelFormat::R16S
	{ DXGI_FORMAT_R16_SINT },					// E_PixelFormat::R16I
	{ DXGI_FORMAT_R16_FLOAT },					// E_PixelFormat::R16F

	{ DXGI_FORMAT_R16G16_UNORM },				// E_PixelFormat::RG16
	{ DXGI_FORMAT_R16G16_UINT },				// E_PixelFormat::RG16U
	{ DXGI_FORMAT_R16G16_SNORM },				// E_PixelFormat::RG16S
	{ DXGI_FORMAT_R16G16_SINT },				// E_PixelFormat::RG16I
	{ DXGI_FORMAT_R16G16_FLOAT },				// E_PixelFormat::RG16F

	{ DXGI_FORMAT_R16G16B16A16_UNORM },		// E_PixelFormat::RGBA16
	{ DXGI_FORMAT_R16G16B16A16_UINT },		// E_PixelFormat::RGBA16U
	{ DXGI_FORMAT_R16G16B16A16_SNORM },		// E_PixelFormat::RGBA16S
	{ DXGI_FORMAT_R16G16B16A16_SINT },		// E_PixelFormat::RGBA16I
	{ DXGI_FORMAT_R16G16B16A16_FLOAT },		// E_PixelFormat::RGBA16F

	{ DXGI_FORMAT_R32_UINT },					// E_PixelFormat::R32U
	{ DXGI_FORMAT_R32_SINT },					// E_PixelFormat::R32I
	{ DXGI_FORMAT_R32_FLOAT },					// E_PixelFormat::R32F

	{ DXGI_FORMAT_R32G32_UINT },				// E_PixelFormat::RG32U
	{ DXGI_FORMAT_R32G32_SINT },				// E_PixelFormat::RG32I
	{ DXGI_FORMAT_R32G32_FLOAT },				// E_PixelFormat::RG32F

	{ DXGI_FORMAT_R32G32B32_UINT },			// E_PixelFormat::RGB32U
	{ DXGI_FORMAT_R32G32B32_SINT },			// E_PixelFormat::RGB32I
	{ DXGI_FORMAT_R32G32B32_FLOAT },			// E_PixelFormat::RGB32F

	{ DXGI_FORMAT_R32G32B32A32_UINT },		// E_PixelFormat::RGBA32U
	{ DXGI_FORMAT_R32G32B32A32_SINT },		// E_PixelFormat::RGBA32I
	{ DXGI_FORMAT_R32G32B32A32_FLOAT },		// E_PixelFormat::RGBA32F

	{ DXGI_FORMAT_R11G11B10_FLOAT },			// E_PixelFormat::RG11B10F
	{ DXGI_FORMAT_R10G10B10A2_UNORM },		// E_PixelFormat::RGB10A2
	{ DXGI_FORMAT_B5G5R5A1_UNORM },			// E_PixelFormat::B5G5R5A1

	{ DXGI_FORMAT_D16_UNORM },					// E_PixelFormat::D16
	{ DXGI_FORMAT_D32_FLOAT },					// E_PixelFormat::D32
	{ DXGI_FORMAT_D24_UNORM_S8_UINT },		// E_PixelFormat::D24_S8
	{ DXGI_FORMAT_D32_FLOAT_S8X24_UINT },	// E_PixelFormat::D32_S8X24	

	{ DXGI_FORMAT_BC1_UNORM },					// E_PixelFormat::BC1
	{ DXGI_FORMAT_BC2_UNORM },					// E_PixelFormat::BC2
	{ DXGI_FORMAT_BC3_UNORM },					// E_PixelFormat::BC3
	{ DXGI_FORMAT_BC4_UNORM },					// E_PixelFormat::BC4
	{ DXGI_FORMAT_BC5_UNORM },					// E_PixelFormat::BC5
	{ DXGI_FORMAT_BC6H_UF16 },					// E_PixelFormat::BC6H
	{ DXGI_FORMAT_BC7_UNORM },					// E_PixelFormat::BC7

	{ DXGI_FORMAT_UNKNOWN },					// E_PixelFormat::Count
	{ DXGI_FORMAT_UNKNOWN }						// E_PixelFormat::Unknown
};
const DXGI_FORMAT FXD::GFX::Pixel::GetFormat(GFX::E_PixelFormat eFormat)
{
	return kD3DPixelTypeDescs[FXD::STD::to_underlying(eFormat)];
}

// ------
// Index
// -
// ------
CONSTEXPR const DXGI_FORMAT kD3DIndexTypesDescs[] =
{
	{ DXGI_FORMAT_R16_UINT },	// E_BufferType::Bit_16
	{ DXGI_FORMAT_R32_UINT },	// E_BufferType::Bit_32
	{ DXGI_FORMAT_UNKNOWN },	// E_BufferType::Count
	{ DXGI_FORMAT_UNKNOWN },	// E_BufferType::Unknown
};
const DXGI_FORMAT FXD::GFX::Index::GetFormat(GFX::E_IndexType eType)
{
	return kD3DIndexTypesDescs[FXD::STD::to_underlying(eType)];
}

// ------
// Vertex
// -
// ------
CONSTEXPR const DXGI_FORMAT kD3DVertexTypeDescs[] =
{
	{ DXGI_FORMAT_R32_FLOAT },					// E_VertexElementType::Float1
	{ DXGI_FORMAT_R32G32_FLOAT },				// E_VertexElementType::Float2
	{ DXGI_FORMAT_R32G32B32_FLOAT },			// E_VertexElementType::Float3
	{ DXGI_FORMAT_R32G32B32A32_FLOAT },		// E_VertexElementType::Float4
	{ DXGI_FORMAT_R8G8B8A8_UNORM },			// E_VertexElementType::Colour
	{ DXGI_FORMAT_R8G8B8A8_UNORM },			// E_VertexElementType::Colour_ARGB
	{ DXGI_FORMAT_R8G8B8A8_UNORM },			// E_VertexElementType::Colour_ABGR
	{ DXGI_FORMAT_R16_SINT },					// E_VertexElementType::Short1
	{ DXGI_FORMAT_R16G16_SINT },				// E_VertexElementType::Short2
	{ DXGI_FORMAT_R16G16B16A16_SINT },		// E_VertexElementType::Short4
	{ DXGI_FORMAT_R16_UINT },					// E_VertexElementType::UShort1
	{ DXGI_FORMAT_R16G16_UINT },				// E_VertexElementType::UShort2
	{ DXGI_FORMAT_R16G16B16A16_UINT },		// E_VertexElementType::UShort4
	{ DXGI_FORMAT_R32_SINT },					// E_VertexElementType::Int1
	{ DXGI_FORMAT_R32G32_SINT },				// E_VertexElementType::Int2
	{ DXGI_FORMAT_R32G32B32_SINT },			// E_VertexElementType::Int3
	{ DXGI_FORMAT_R32G32B32A32_SINT },		// E_VertexElementType::Int4
	{ DXGI_FORMAT_R32_UINT },					// E_VertexElementType::UInt1
	{ DXGI_FORMAT_R32G32_UINT },				// E_VertexElementType::UInt2
	{ DXGI_FORMAT_R32G32B32_UINT },			// E_VertexElementType::UInt3
	{ DXGI_FORMAT_R32G32B32A32_UINT },		// E_VertexElementType::UInt4
	{ DXGI_FORMAT_R8G8B8A8_UINT },			// E_VertexElementType::UByte4_Norm
	{ DXGI_FORMAT_UNKNOWN },					// E_VertexElementType::Count
	{ DXGI_FORMAT_UNKNOWN }						// E_VertexElementType::Unknown
};
const DXGI_FORMAT FXD::GFX::Vertex::GetFormat(GFX::E_VertexElementType eType)
{
	return kD3DVertexTypeDescs[FXD::STD::to_underlying(eType)];
}
#endif //IsGFXDX11()