// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/Mapping.h"
#include "FXDEngine/GFX/DX11/GfxAdapterDX11.h"
#include "FXDEngine/GFX/DX11/ImageDX11.h"
#include "FXDEngine/GFX/DX11/MappingDX11.h"
#include "FXDEngine/GFX/DX11/SwapChainDX11.h"

using namespace FXD;
using namespace GFX;

// ------
// DX11Image
// -
// ------
namespace DX11Image
{
	D3D11_SUBRESOURCE_DATA* ConvertToDX11Subresource(FXD::U16 nArrays, FXD::U16 nMips, FXD::U16 nWidth, FXD::U16 nHeight, GFX::E_PixelFormat eFormat, const FXD::U8* pSrc, D3D11_SUBRESOURCE_DATA* pDst)
	{
		if (!pSrc)
		{
			return nullptr;
		}

		FXD::U32 nSliceOffset = 0;
		for (FXD::U16 nSliceIndex = 0; nSliceIndex < nArrays; ++nSliceIndex)
		{
			FXD::U32 nMipOffset = 0;
			for (FXD::U16 nMipIndex = 0; nMipIndex < nMips; ++nMipIndex)
			{
				const FXD::U32 nDataOffset = (nSliceOffset + nMipOffset);
				const FXD::U32 nSubResIndex = (nSliceIndex * nMips + nMipIndex);
				const FXD::U32 nMipWidth = FXD::STD::Max(1, (nWidth >> nMipIndex));
				const FXD::U32 nMipHeight = FXD::STD::Max(1, (nHeight >> nMipIndex));
				const FXD::U32 nMemPitch = nMipWidth * Pixel::GetNumElemBytes(eFormat);
				const FXD::U32 nSlicePitch = nMipWidth * nMipHeight * Pixel::GetNumElemBytes(eFormat);

				pDst[nSubResIndex].pSysMem = &pSrc[nDataOffset];
				pDst[nSubResIndex].SysMemPitch = nMemPitch;
				pDst[nSubResIndex].SysMemSlicePitch = nSlicePitch;
				nMipOffset += nSlicePitch;
			}
			nSliceOffset += nMipOffset;
		}

		// Terminator
		pDst[nMips].pSysMem = nullptr;
		pDst[nMips].SysMemPitch = 0;
		pDst[nMips].SysMemSlicePitch = 0;
		return pDst;
	}

	bool create_image_view_rt(GFX::IGfxAdapter* pAdapter, const IMAGE_DESC& imageDesc, const IMAGEVIEW_DESC& imageViewDesc, ID3D11Resource* pResource, ID3D11RenderTargetView*& pView)
	{
		const FXD::U32 nNumFaces = imageDesc.num_faces();

		D3D11_RENDER_TARGET_VIEW_DESC d3dRTViewDesc = {};
		Memory::MemZero_T(d3dRTViewDesc);
		d3dRTViewDesc.Format = Pixel::GetFormat(imageDesc.Format);

		switch (imageDesc.Type)
		{
			case E_ImageType::One:
			{
				d3dRTViewDesc.ViewDimension = (nNumFaces > 1) ? D3D11_RTV_DIMENSION_TEXTURE1DARRAY : D3D11_RTV_DIMENSION_TEXTURE1D;
				if (nNumFaces > 1)
				{
					d3dRTViewDesc.Texture1DArray.MipSlice = imageViewDesc.MostDetailMip;
					d3dRTViewDesc.Texture1DArray.FirstArraySlice = imageViewDesc.FirstArraySlice;
					d3dRTViewDesc.Texture1DArray.ArraySize = imageViewDesc.NumArraySlices;
				}
				else
				{
					d3dRTViewDesc.Texture1D.MipSlice = imageViewDesc.MostDetailMip;
				}
				break;
			}
			case E_ImageType::Two:
			{
				if (imageDesc.NumSamples > 1)
				{
					d3dRTViewDesc.ViewDimension = (nNumFaces > 1) ? D3D11_RTV_DIMENSION_TEXTURE2DMSARRAY : D3D11_RTV_DIMENSION_TEXTURE2DMS;
					if (nNumFaces > 1)
					{
						d3dRTViewDesc.Texture2DMSArray.FirstArraySlice = imageViewDesc.FirstArraySlice;
						d3dRTViewDesc.Texture2DMSArray.ArraySize = imageViewDesc.NumArraySlices;
					}
				}
				else
				{
					d3dRTViewDesc.ViewDimension = (nNumFaces > 1) ? D3D11_RTV_DIMENSION_TEXTURE2DARRAY : D3D11_RTV_DIMENSION_TEXTURE2D;
					if (nNumFaces > 1)
					{
						d3dRTViewDesc.Texture2DArray.MipSlice = imageViewDesc.MostDetailMip;
						d3dRTViewDesc.Texture2DArray.FirstArraySlice = imageViewDesc.FirstArraySlice;
						d3dRTViewDesc.Texture2DArray.ArraySize = imageViewDesc.NumArraySlices;
					}
					else
					{
						d3dRTViewDesc.Texture2D.MipSlice = imageViewDesc.MostDetailMip;
					}
				}
				break;
			}
			case E_ImageType::Three:
			{
				d3dRTViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE3D;
				d3dRTViewDesc.Texture3D.MipSlice = imageViewDesc.MostDetailMip;
				d3dRTViewDesc.Texture3D.FirstWSlice = 0;
				d3dRTViewDesc.Texture3D.WSize = imageDesc.Depth;
				break;
			}
			case E_ImageType::Cube:
			{
				d3dRTViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
				d3dRTViewDesc.Texture2DArray.MipSlice = imageViewDesc.MostDetailMip;
				d3dRTViewDesc.Texture2DArray.FirstArraySlice = imageViewDesc.FirstArraySlice;
				d3dRTViewDesc.Texture2DArray.ArraySize = imageViewDesc.NumArraySlices;
				break;
			}
			default:
			{
				PRINT_ASSERT << "GFX: Invalid image type for this view type";
				break;
			}
		}

		if (DX11ResultCheckFail(pAdapter->impl().d3dDevice()->CreateRenderTargetView(pResource, &d3dRTViewDesc, &pView)))
		{
			PRINT_ASSERT << "GFX: CreateRenderTargetView() failed";
		}
		return true;
	}
	bool create_image_view_dsv(GFX::IGfxAdapter* pAdapter, const IMAGE_DESC& imageDesc, const IMAGEVIEW_DESC& imageViewDesc, ID3D11Resource* pResource, ID3D11DepthStencilView*& pView)
	{
		const FXD::U32 nNumFaces = imageDesc.num_faces();

		D3D11_DEPTH_STENCIL_VIEW_DESC d3dStencilViewDesc = {};
		Memory::MemZero_T(d3dStencilViewDesc);
		d3dStencilViewDesc.Format = Pixel::GetFormat(imageDesc.Format);

		switch (imageDesc.Type)
		{
			case E_ImageType::One:
			{
				d3dStencilViewDesc.ViewDimension = (nNumFaces > 1) ? D3D11_DSV_DIMENSION_TEXTURE1DARRAY : D3D11_DSV_DIMENSION_TEXTURE1D;
				if (nNumFaces > 1)
				{
					d3dStencilViewDesc.Texture1DArray.MipSlice = imageViewDesc.MostDetailMip;
					d3dStencilViewDesc.Texture1DArray.FirstArraySlice = imageViewDesc.FirstArraySlice;
					d3dStencilViewDesc.Texture1DArray.ArraySize = imageViewDesc.NumArraySlices;
				}
				else
				{
					d3dStencilViewDesc.Texture1D.MipSlice = imageViewDesc.MostDetailMip;
				}
				break;
			}
			case E_ImageType::Two:
			{
				if (imageDesc.NumSamples > 1)
				{
					d3dStencilViewDesc.ViewDimension = (nNumFaces > 1) ? D3D11_DSV_DIMENSION_TEXTURE2DMSARRAY : D3D11_DSV_DIMENSION_TEXTURE2DMS;
					if (nNumFaces > 1)
					{
						d3dStencilViewDesc.Texture2DMSArray.FirstArraySlice = imageViewDesc.FirstArraySlice;
						d3dStencilViewDesc.Texture2DMSArray.ArraySize = imageViewDesc.NumArraySlices;
					}
				}
				else
				{
					d3dStencilViewDesc.ViewDimension = (nNumFaces > 1) ? D3D11_DSV_DIMENSION_TEXTURE2DARRAY : D3D11_DSV_DIMENSION_TEXTURE2D;
					if (nNumFaces > 1)
					{
						d3dStencilViewDesc.Texture2DArray.MipSlice = imageViewDesc.MostDetailMip;
						d3dStencilViewDesc.Texture2DArray.FirstArraySlice = imageViewDesc.FirstArraySlice;
						d3dStencilViewDesc.Texture2DArray.ArraySize = imageViewDesc.NumArraySlices;
					}
					else
					{
						d3dStencilViewDesc.Texture2D.MipSlice = imageViewDesc.MostDetailMip;
					}
				}
				break;
			}
			case E_ImageType::Three:
			{
				d3dStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DARRAY;
				d3dStencilViewDesc.Texture2DArray.MipSlice = imageViewDesc.MostDetailMip;
				d3dStencilViewDesc.Texture2DArray.FirstArraySlice = 0;
				d3dStencilViewDesc.Texture2DArray.ArraySize = imageDesc.Depth;
				break;
			}
			case E_ImageType::Cube:
			{
				d3dStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DARRAY;
				d3dStencilViewDesc.Texture2DArray.MipSlice = imageViewDesc.MostDetailMip;
				d3dStencilViewDesc.Texture2DArray.FirstArraySlice = imageViewDesc.FirstArraySlice;
				d3dStencilViewDesc.Texture2DArray.ArraySize = imageViewDesc.NumArraySlices;
				break;
			}
			default:
			{
				PRINT_ASSERT << "GFX: Invalid image type for this view type";
				break;
			}
		}
		if (DX11ResultCheckFail(pAdapter->impl().d3dDevice()->CreateDepthStencilView(pResource, &d3dStencilViewDesc, &pView)))
		{
			PRINT_ASSERT << "GFX: CreateDepthStencilView() failed";
		}
		return true;
	}
	bool create_image_view_srv(GFX::IGfxAdapter* pAdapter, const IMAGE_DESC& imageDesc, const IMAGEVIEW_DESC& imageViewDesc, ID3D11Resource* pResource, ID3D11ShaderResourceView*& pView)
	{
		const FXD::U32 nNumFaces = imageDesc.num_faces();

		D3D11_SHADER_RESOURCE_VIEW_DESC d3dShaderViewDesc = {};
		Memory::MemZero_T(d3dShaderViewDesc);
		d3dShaderViewDesc.Format = Pixel::GetFormat(imageDesc.Format);

		switch (imageDesc.Type)
		{
			case E_ImageType::One:
			{
				d3dShaderViewDesc.ViewDimension = (nNumFaces > 1) ? D3D11_SRV_DIMENSION_TEXTURE1DARRAY : D3D11_SRV_DIMENSION_TEXTURE1D;
				if (nNumFaces > 1)
				{
					d3dShaderViewDesc.Texture1DArray.MipLevels = imageViewDesc.NumMips;
					d3dShaderViewDesc.Texture1DArray.MostDetailedMip = imageViewDesc.MostDetailMip;
					d3dShaderViewDesc.Texture1DArray.FirstArraySlice = imageViewDesc.FirstArraySlice;
					d3dShaderViewDesc.Texture1DArray.ArraySize = imageViewDesc.NumArraySlices;
				}
				else
				{
					d3dShaderViewDesc.Texture1D.MipLevels = imageViewDesc.MostDetailMip;
					d3dShaderViewDesc.Texture1D.MostDetailedMip = imageViewDesc.MostDetailMip;
				}
				break;
			}
			case E_ImageType::Two:
			{
				if (imageDesc.NumSamples > 1)
				{
					d3dShaderViewDesc.ViewDimension = (nNumFaces > 1) ? D3D11_SRV_DIMENSION_TEXTURE2DMSARRAY : D3D11_SRV_DIMENSION_TEXTURE2DMS;
					if (nNumFaces > 1)
					{
						d3dShaderViewDesc.Texture2DMSArray.FirstArraySlice = imageViewDesc.FirstArraySlice;
						d3dShaderViewDesc.Texture2DMSArray.ArraySize = imageViewDesc.NumArraySlices;
						//CRY_ASSERT(srvDesc.Texture2DMSArray.ArraySize == ~0 || (srvDesc.Texture2DMSArray.ArraySize > 0 && srvDesc.Texture2DMSArray.ArraySize <= nSliceAvailable));
					}
				}
				else
				{
					d3dShaderViewDesc.ViewDimension = (nNumFaces > 1) ? D3D11_SRV_DIMENSION_TEXTURE2DARRAY : D3D11_SRV_DIMENSION_TEXTURE2D;
					if (nNumFaces > 1)
					{
						d3dShaderViewDesc.Texture2DArray.MipLevels = imageViewDesc.NumMips;
						d3dShaderViewDesc.Texture2DArray.MostDetailedMip = imageViewDesc.MostDetailMip;
						d3dShaderViewDesc.Texture2DArray.FirstArraySlice = imageViewDesc.FirstArraySlice;
						d3dShaderViewDesc.Texture2DArray.ArraySize = imageViewDesc.NumArraySlices;
					}
					else
					{
						d3dShaderViewDesc.Texture2D.MipLevels = imageViewDesc.NumMips;
						d3dShaderViewDesc.Texture2D.MostDetailedMip = imageViewDesc.MostDetailMip;
					}
				}
				break;
			}
			case E_ImageType::Three:
			{
				d3dShaderViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE3D;
				d3dShaderViewDesc.Texture3D.MipLevels = imageViewDesc.NumMips;
				d3dShaderViewDesc.Texture3D.MostDetailedMip = imageViewDesc.MostDetailMip;
				break;
			}
			case E_ImageType::Cube:
			{
				if (nNumFaces % 6 == 0)
				{
					d3dShaderViewDesc.ViewDimension = (nNumFaces == 6) ? D3D11_SRV_DIMENSION_TEXTURECUBE : D3D11_SRV_DIMENSION_TEXTURECUBEARRAY;
					if (nNumFaces == 6)
					{
						d3dShaderViewDesc.TextureCube.MipLevels = imageViewDesc.NumMips;
						d3dShaderViewDesc.TextureCube.MostDetailedMip = imageViewDesc.MostDetailMip;
					}
					else
					{
						d3dShaderViewDesc.TextureCubeArray.MipLevels = imageViewDesc.NumMips;
						d3dShaderViewDesc.TextureCubeArray.MostDetailedMip = imageViewDesc.MostDetailMip;
						d3dShaderViewDesc.TextureCubeArray.First2DArrayFace = imageViewDesc.FirstArraySlice;
						d3dShaderViewDesc.TextureCubeArray.NumCubes = (imageViewDesc.NumArraySlices / 6);
					}
				}
				else
				{
					d3dShaderViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
					d3dShaderViewDesc.Texture2DArray.MipLevels = imageViewDesc.NumMips;
					d3dShaderViewDesc.Texture2DArray.MostDetailedMip = imageViewDesc.MostDetailMip;
					d3dShaderViewDesc.Texture2DArray.FirstArraySlice = imageViewDesc.FirstArraySlice;
					d3dShaderViewDesc.Texture2DArray.ArraySize = imageViewDesc.NumArraySlices;
				}
				break;
			}
			default:
			{
				PRINT_ASSERT << "GFX: Invalid image type for this view type";
				break;
			}
		}

		if (DX11ResultCheckFail(pAdapter->impl().d3dDevice()->CreateShaderResourceView(pResource, &d3dShaderViewDesc, &pView)))
		{
			PRINT_ASSERT << "GFX: CreateDepthStencilView() failed";
		}
		return true;
	}
	bool create_image_views(GFX::IGfxAdapter* pAdapter, GFX::IImage* pImage, GFX::IImageView* pImageView, IMAGEVIEW_DESC& imageViewDesc)
	{
		const GFX::IMAGE_DESC& imageDesc = pImage->imageDesc();
		ID3D11RenderTargetView* pViewRenderTarget = nullptr;
		ID3D11DepthStencilView* pViewDepth = nullptr;
		ID3D11ShaderResourceView* pViewShader = nullptr;

		if (imageDesc.Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_RENDER_TARGET))
		{
			::DX11Image::create_image_view_rt(pAdapter, imageDesc, imageViewDesc, pImage->impl().resource(), pViewRenderTarget);
		}
		else if (imageDesc.Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL))
		{
			::DX11Image::create_image_view_dsv(pAdapter, imageDesc, imageViewDesc, pImage->impl().resource(), pViewDepth);
		}
		if (imageDesc.Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_SHADER_RESOURCE))
		{
			::DX11Image::create_image_view_srv(pAdapter, imageDesc, imageViewDesc, pImage->impl().resource(), pViewShader);
		}
		pImageView->impl().view_render_target(pViewRenderTarget);
		pImageView->impl().view_depth(pViewDepth);
		pImageView->impl().view_shader(pViewShader);
		
		return true;
	}

	bool createImage1D(GFX::IGfxAdapter* pAdapter, const IMAGE_DESC& imageDesc, FXD::U8* pSrc, ID3D11Resource*& pResource)
	{
		// 1. Initial checks
		PRINT_COND_ASSERT((imageDesc.Width > 0), "GFX: Width is invalid");

		// 2. Initiate data
		D3D11_SUBRESOURCE_DATA d3dSubData[15];
		D3D11_TEXTURE1D_DESC d3dTexDesc = {};
		Memory::MemZero_T(d3dSubData);
		Memory::MemZero_T(d3dTexDesc);
		const FXD::U32 nNumFaces = imageDesc.NumArraySlices;

		// 3. Set up the texture description
		d3dTexDesc.Width = imageDesc.Width;
		d3dTexDesc.Format = Pixel::GetFormat(imageDesc.Format);
		d3dTexDesc.ArraySize = (nNumFaces == 0) ? 1 : nNumFaces;
		d3dTexDesc.MiscFlags = 0;
		d3dTexDesc.MipLevels = (imageDesc.NumMips == kMipUnlimited || (1U << imageDesc.NumMips) > imageDesc.Width) ? 0 : imageDesc.NumMips + 1;
		d3dTexDesc.BindFlags = Buffer::ConvertToDX11BindFlags(imageDesc.Usage);
		d3dTexDesc.Usage = Buffer::ConvertToDX11Usage(imageDesc.Usage);
		d3dTexDesc.CPUAccessFlags = Buffer::ConvertToDX11CPUAccessFlags(imageDesc.Usage);
		
		// 4. Create the texture
		D3D11_SUBRESOURCE_DATA* pD3dData = DX11Image::ConvertToDX11Subresource(d3dTexDesc.ArraySize, d3dTexDesc.MipLevels, d3dTexDesc.Width, 0, imageDesc.Format, pSrc, &d3dSubData[0]);
		if (DX11ResultCheckFail(pAdapter->impl().d3dDevice()->CreateTexture1D(&d3dTexDesc, pD3dData, (ID3D11Texture1D**)&pResource)))
		{
			PRINT_ASSERT << "GFX: CreateTexture2D() failed";
		}

		// 5. Sanity check
		((ID3D11Texture1D*)pResource)->GetDesc(&d3dTexDesc);
		if (imageDesc.NumMips != (d3dTexDesc.MipLevels - 1))
		{
			PRINT_ASSERT << "Driver returned different number of mip maps than requested. Requested: " << imageDesc.NumMips << ". Got: " << (d3dTexDesc.MipLevels - 1) << "";
		}
		return true;
	}
	bool createImage2D(GFX::IGfxAdapter* pAdapter, const IMAGE_DESC& imageDesc, FXD::U8* pSrc, ID3D11Resource*& pResource)
	{
		// 1. Initial checks
		PRINT_COND_ASSERT((imageDesc.Width > 0), "GFX: Width is invalid");
		PRINT_COND_ASSERT((imageDesc.Height > 0), "GFX: Height is invalid");

		// 2. Initiate data
		D3D11_SUBRESOURCE_DATA d3dSubData[15] = {};
		D3D11_TEXTURE2D_DESC d3dTexDesc = {};
		Memory::MemZero_T(d3dSubData);
		Memory::MemZero_T(d3dTexDesc);
		const FXD::U32 nNumFaces = imageDesc.NumArraySlices;

		// 3. Set up the texture description
		d3dTexDesc.Width = imageDesc.Width;
		d3dTexDesc.Height = imageDesc.Height;
		d3dTexDesc.Format = Pixel::GetFormat(imageDesc.Format);
		d3dTexDesc.ArraySize = (nNumFaces == 0) ? 1 : nNumFaces;
		d3dTexDesc.MiscFlags = 0;
		d3dTexDesc.MipLevels = (imageDesc.NumMips == kMipUnlimited || (1U << imageDesc.NumMips) > imageDesc.Width) ? 0 : imageDesc.NumMips + 1;
		d3dTexDesc.BindFlags = Buffer::ConvertToDX11BindFlags(imageDesc.Usage);
		d3dTexDesc.Usage = Buffer::ConvertToDX11Usage(imageDesc.Usage);
		d3dTexDesc.CPUAccessFlags = Buffer::ConvertToDX11CPUAccessFlags(imageDesc.Usage);

		if (imageDesc.Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_RENDER_TARGET))
		{
			d3dTexDesc.SampleDesc.Count = 1;
			d3dTexDesc.SampleDesc.Quality = 0;
		}
		else if (imageDesc.Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_DEPTH_STENCIL))
		{
			d3dTexDesc.SampleDesc.Count = 1;
			d3dTexDesc.SampleDesc.Quality = 0;
		}
		else if (imageDesc.Usage.is_flag_set(GFX::E_BufferUsageFlag::BIND_SHADER_RESOURCE))
		{
			d3dTexDesc.SampleDesc.Count = 1;
			d3dTexDesc.SampleDesc.Quality = 0;
		}

		// 4. Create the texture
		D3D11_SUBRESOURCE_DATA* pD3dData = DX11Image::ConvertToDX11Subresource(d3dTexDesc.ArraySize, d3dTexDesc.MipLevels, d3dTexDesc.Width, d3dTexDesc.Height, imageDesc.Format, pSrc, &d3dSubData[0]);
		if (DX11ResultCheckFail(pAdapter->impl().d3dDevice()->CreateTexture2D(&d3dTexDesc, pD3dData, (ID3D11Texture2D**)&pResource)))
		{
			PRINT_ASSERT << "GFX: CreateTexture2D() failed";
		}

		// 5. Sanity check
		((ID3D11Texture2D*)pResource)->GetDesc(&d3dTexDesc);
		if (imageDesc.NumMips != (d3dTexDesc.MipLevels - 1))
		{
			PRINT_ASSERT << "GFX: Driver returned different number of mip maps than requested. Requested: " << imageDesc.NumMips << ". Got: " << (d3dTexDesc.MipLevels - 1) << "";
		}
		return true;
	}
	bool createImage3D(GFX::IGfxAdapter* pAdapter, const IMAGE_DESC& imageDesc, FXD::U8* pSrc, ID3D11Resource*& pResource)
	{
		// 1. Initial checks
		PRINT_COND_ASSERT((imageDesc.Width > 0), "GFX: Width is invalid");
		PRINT_COND_ASSERT((imageDesc.Height > 0), "GFX: Height is invalid");
		PRINT_COND_ASSERT((imageDesc.Depth > 0), "GFX: Depth is invalid");

		// 2. Initiate data
		D3D11_SUBRESOURCE_DATA d3dSubData[15];
		D3D11_TEXTURE3D_DESC d3dTexDesc = {};
		Memory::MemZero_T(d3dSubData);
		Memory::MemZero_T(d3dTexDesc);
		FXD::U32 nNumFaces = imageDesc.NumArraySlices;

		// 3. Set up the texture description
		d3dTexDesc.Width = imageDesc.Width;
		d3dTexDesc.Height = imageDesc.Height;
		d3dTexDesc.Depth = imageDesc.Depth;
		d3dTexDesc.Format = Pixel::GetFormat(imageDesc.Format);
		d3dTexDesc.MiscFlags = (imageDesc.Type == GFX::E_ImageType::Cube) ? D3D11_RESOURCE_MISC_TEXTURECUBE : 0;
		d3dTexDesc.MipLevels = (imageDesc.NumMips == kMipUnlimited || (1U << imageDesc.NumMips) > imageDesc.Width) ? 0 : imageDesc.NumMips + 1;
		d3dTexDesc.Usage = Buffer::ConvertToDX11Usage(imageDesc.Usage);
		d3dTexDesc.BindFlags = Buffer::ConvertToDX11BindFlags(imageDesc.Usage);
		d3dTexDesc.CPUAccessFlags = Buffer::ConvertToDX11CPUAccessFlags(imageDesc.Usage);

		// 4. Create the texture
		D3D11_SUBRESOURCE_DATA* pD3dData = DX11Image::ConvertToDX11Subresource(d3dTexDesc.MipLevels, d3dTexDesc.MipLevels, d3dTexDesc.Width, d3dTexDesc.Height, imageDesc.Format, pSrc, &d3dSubData[0]);
		if (DX11ResultCheckFail(pAdapter->impl().d3dDevice()->CreateTexture3D(&d3dTexDesc, pD3dData, (ID3D11Texture3D**)&pResource)))
		{
			PRINT_ASSERT << "GFX: CreateTexture2D() failed";
		}

		// 5. Sanity check
		((ID3D11Texture3D*)pResource)->GetDesc(&d3dTexDesc);
		if (imageDesc.NumMips != (d3dTexDesc.MipLevels - 1))
		{
			PRINT_ASSERT << "Driver returned different number of mip maps than requested. Requested: " << imageDesc.NumMips << ". Got: " << (d3dTexDesc.MipLevels - 1) << "";
		}
		return true;
	}

	bool create_image(GFX::IGfxAdapter* pAdapter, GFX::IImage* pImage, const IMAGE_DESC& imageDesc, FXD::U8* pSrc)
	{
		ID3D11Resource* pResource = nullptr;
		if (imageDesc.Type == GFX::E_ImageType::One)
		{
			::DX11Image::createImage1D(pAdapter, imageDesc, pSrc, pResource);
		}
		else if (imageDesc.Type == GFX::E_ImageType::Two)
		{
			::DX11Image::createImage2D(pAdapter, imageDesc, pSrc, pResource);
		}
		else if (imageDesc.Type == GFX::E_ImageType::Three)
		{
			::DX11Image::createImage3D(pAdapter, imageDesc, pSrc, pResource);
		}
		else if (imageDesc.Type == GFX::E_ImageType::Cube)
		{
			::DX11Image::createImage2D(pAdapter, imageDesc, pSrc, pResource);
		}
		pImage->impl().resource(pResource);
		return true;
	}
} //namespace DX11Image


// ------
// IImageView::Impl::Impl
// - 
// ------
IImageView::Impl::Impl::Impl(void)
	: m_pViewRenderTarget(nullptr)
	, m_pViewDepth(nullptr)
	, m_pViewShader(nullptr)
{
}

IImageView::Impl::~Impl(void)
{
	PRINT_COND_ASSERT((m_pViewRenderTarget == nullptr), "GFX: m_pViewRenderTarget is not shutdown");
	PRINT_COND_ASSERT((m_pViewDepth == nullptr), "GFX: m_pViewDepth is not shutdown");
	PRINT_COND_ASSERT((m_pViewShader == nullptr), "GFX: m_pViewShader is not shutdown");
}

// ------
// IImageView
// -
// ------
IImageView::IImageView(GFX::IGfxAdapter* pAdapter, GFX::IImage* pImage)
	: m_pAdapter(pAdapter)
	, m_pImage(pImage)
{
}

IImageView::~IImageView(void)
{
}

bool IImageView::_create(const GFX::IMAGEVIEW_DESC& imageViewDesc)
{
	//
	m_imageViewDesc = imageViewDesc;
	::DX11Image::create_image_views(m_pAdapter, m_pImage, this, m_imageViewDesc);
	return true;
}

bool IImageView::_release(void)
{
	m_imageViewDesc = GFX::IMAGEVIEW_DESC();
	FXD_RELEASE(m_impl->m_pViewRenderTarget, Release());
	FXD_RELEASE(m_impl->m_pViewDepth, Release());
	FXD_RELEASE(m_impl->m_pViewShader, Release());
	return true;
}


// ------
// IImage::Impl::Impl
// - 
// ------
IImage::Impl::Impl::Impl(void)
	: m_pResource(nullptr)
{
}

IImage::Impl::~Impl(void)
{
	PRINT_COND_ASSERT((m_pResource == nullptr), "GFX: m_pResource is not shutdown");
}

// ------
// IImage
// -
// ------
IImage::IImage(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IImage::~IImage(void)
{
}

bool IImage::_create(const GFX::IMAGE_DESC& imageDesc, void* pSrc)
{
	// 
	m_imageDesc = imageDesc;
	::DX11Image::create_image(m_pAdapter, this, m_imageDesc, (FXD::U8*)pSrc);
	return true;
}

bool IImage::_create(const GFX::SwapChain& swapChain, const FXD::U32 /*nBuffer*/)
{
	// 
	if (DX11ResultCheckFail(swapChain->impl().dxSwapChain()->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&m_impl->m_pResource)))
	{
		PRINT_ASSERT << "GFX: Could not retrieve backbuffer ref";
	}
	D3D11_TEXTURE2D_DESC d3dTexDesc = {};
	((ID3D11Texture2D*)m_impl->m_pResource)->GetDesc(&d3dTexDesc);

	m_imageDesc.Width = d3dTexDesc.Width;
	m_imageDesc.Height = d3dTexDesc.Height;
	m_imageDesc.Depth = 1;
	m_imageDesc.NumMips = d3dTexDesc.MipLevels;
	m_imageDesc.NumArraySlices = d3dTexDesc.ArraySize;
	m_imageDesc.NumSamples = 0;
	m_imageDesc.Type = GFX::E_ImageType::Two;
	m_imageDesc.Format = (swapChain)->color_format();
	m_imageDesc.Usage = (GFX::E_BufferUsageFlag::BIND_RENDER_TARGET);
	return true;
}

bool IImage::_release(void)
{
	m_imageDesc = IMAGE_DESC();
	FXD_RELEASE(m_impl->m_pResource, Release());
	return true;
}
#endif //IsGFXDX11()