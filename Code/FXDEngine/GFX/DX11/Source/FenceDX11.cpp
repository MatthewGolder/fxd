// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/DX11/FenceDX11.h"
#include "FXDEngine/GFX/DX11/GfxAdapterDX11.h"
#include "FXDEngine/Thread/Thread.h"

using namespace FXD;
using namespace GFX;

// ------
// IFence::Impl::Impl
// - 
// ------
IFence::Impl::Impl::Impl(void)
{
}

IFence::Impl::~Impl(void)
{
	PRINT_COND_ASSERT((!m_d3dQuery), "GFX: m_query is not shutdown");
}

// ------
// IFence
// - 
// ------
IFence::IFence(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
{
}

IFence::~IFence(void)
{
}

bool IFence::_create(bool bSignaled)
{
	D3D11_QUERY_DESC d3dQueryDesc = {};
	Memory::MemZero_T(d3dQueryDesc);

	d3dQueryDesc.Query = D3D11_QUERY_EVENT;
	d3dQueryDesc.MiscFlags = 0;

	HRESULT hr = m_pAdapter->impl().d3dDevice()->CreateQuery(&d3dQueryDesc, m_impl->m_d3dQuery);
	return (hr == S_OK);
}

bool IFence::_release(void)
{
	m_impl->m_d3dQuery = nullptr;
	return true;
}

bool IFence::_is_signalled(void) const
{
	if (!m_impl->m_d3dQuery)
	{
		return false;
	}

	HRESULT hr = m_pAdapter->impl().d3dDevContext()->GetData(m_impl->m_d3dQuery, nullptr, 0, 0);
	return (hr == S_OK);
}

bool IFence::_wait(void)
{
	HRESULT hRes;
	while ((hRes = m_pAdapter->impl().d3dDevContext()->GetData(m_impl->m_d3dQuery, nullptr, 0, 0)) == S_FALSE);
	{
		Thread::Funcs::Sleep(50);
	}
	return true;
}

bool IFence::_reset(void)
{
	return true;
}
#endif //IsGFXDX11()