// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/Mapping.h"
#include "FXDEngine/GFX/DX11/GfxApiDX11.h"
#include "FXDEngine/GFX/DX11/GfxAdapterDX11.h"

using namespace FXD;
using namespace GFX;

// ------
// IGfxApi::Impl::Impl
// - 
// ------
IGfxApi::Impl::Impl::Impl(void)
	: m_d3dDLL(nullptr)
	, m_dxgiDLL(nullptr)
	, m_d3dcompilerDLL(nullptr)
{
}

IGfxApi::Impl::~Impl(void)
{
}

// ------
// IGfxApi
// -
// ------
IGfxApi::IGfxApi(void)
	: IApi(App::E_Api::GFX)
	, m_eGfxApi(GFX::E_GfxApi::DX11)
{
}

IGfxApi::~IGfxApi(void)
{
}

bool IGfxApi::create_api(void)
{
	// 1. Create the library
	bool bRetVal = true;
	m_impl->m_d3dDLL = LoadLibrary(TEXT("d3d11.dll"));
	if (!m_impl->m_d3dDLL)
	{
		PRINT_WARN << "GFX: Failed to load D3D11 library";
		bRetVal = false;
	}
	m_impl->m_dxgiDLL = LoadLibrary(TEXT("dxgi.dll"));
	if (!m_impl->m_dxgiDLL)
	{
		PRINT_WARN << "GFX: Failed to load DXGI library";
		bRetVal = false;
	}
	m_impl->m_d3dcompilerDLL = LoadLibrary(TEXT("d3dcompiler_43.dll"));
	if (!m_impl->m_d3dcompilerDLL)
	{
		PRINT_WARN << "GFX: Failed to load d3dcompiler library";
		bRetVal = false;
	}

	// 2. Create the list of devices
	_create_adapter_list();

	return bRetVal;
}

bool IGfxApi::release_api(void)
{
	if (m_impl->m_d3dDLL)
	{
		FreeLibrary(m_impl->m_d3dDLL);
		m_impl->m_d3dDLL = nullptr;
	}
	if (m_impl->m_dxgiDLL)
	{
		FreeLibrary(m_impl->m_dxgiDLL);
		m_impl->m_dxgiDLL = nullptr;
	}
	if (m_impl->m_d3dcompilerDLL)
	{
		FreeLibrary(m_impl->m_d3dcompilerDLL);
		m_impl->m_d3dcompilerDLL = nullptr;
	}
	return true;
}

void IGfxApi::_create_adapter_list(void)
{
	Container::Ptr< IDXGIFactory1 > dxgIFactory;
	if (DX11ResultCheckFail(CreateDXGIFactory1(IID_IDXGIFactory, (void**)(dxgIFactory))))
	{
		return;
	}

	UINT nAdapter = 0;
	Container::Ptr< IDXGIAdapter1 > currentAdapter;
	while (dxgIFactory->EnumAdapters1(nAdapter, currentAdapter) != DXGI_ERROR_NOT_FOUND)
	{
		if (!currentAdapter == false)
		{
			DXGI_ADAPTER_DESC1 adaptDesc;
			if (DX11ResultCheckSuccess(currentAdapter->GetDesc1(&adaptDesc)))
			{
				bool bDefault = (nAdapter == 0);
				Core::StringBuilder8 srtDeviceName;
				srtDeviceName << (const FXD::UTF16*)adaptDesc.Description;
				GFX::E_DeviceType eDeviceType = GFX::E_DeviceType::Unknown;

				GFX::GfxAdapterDesc desc = std::make_shared< GFX::IGfxAdapterDesc >(nAdapter);
				desc->driver(GFX::Api::ToString(E_GfxApi::DX11).c_str());
				desc->isDefault(bDefault);
				desc->name(srtDeviceName.str());
				desc->deviceType(eDeviceType);
				desc->impl().dxgiAdapter(currentAdapter);
				m_adapterDescs[desc->name()] = desc;
			}
		}
		nAdapter++;
	}
}
#endif //IsGFXDX11()