// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/DX11/GfxAdapterDX11.h"
#include "FXDEngine/GFX/DX11/HWBufferDX11.h"
#include "FXDEngine/GFX/DX11/MappingDX11.h"

using namespace FXD;
using namespace GFX;

// ------
// IHWBuffer::Impl::Impl
// - 
// ------
IHWBuffer::Impl::Impl::Impl(void)
	: m_pD3DBuffer(nullptr)
{
	Memory::MemZero_T(m_d3dDesc);
}

IHWBuffer::Impl::~Impl(void)
{
	PRINT_COND_ASSERT((m_pD3DBuffer == nullptr), "GFX: m_pD3DBuffer is not shutdown");
}

// ------
// IHWBuffer
// - 
// ------
IHWBuffer::IHWBuffer(GFX::IGfxAdapter* pAdapter)
	: m_pAdapter(pAdapter)
	, m_bLocked(false)
{
}

IHWBuffer::~IHWBuffer(void)
{
}

bool IHWBuffer::_create(const GFX::E_BufferUsageFlags eBufferUsage, FXD::U32 nCount, FXD::U32 nStride, const void* pSrc)
{
	m_nCount = nCount;
	m_nStride = nStride;
	m_eBufferUsage = eBufferUsage;

	// Describe the buffer.
	m_impl->m_d3dDesc.ByteWidth = get_size();
	m_impl->m_d3dDesc.StructureByteStride = Stride();
	m_impl->m_d3dDesc.BindFlags = Buffer::ConvertToDX11BindFlags(m_eBufferUsage);
	m_impl->m_d3dDesc.CPUAccessFlags = Buffer::ConvertToDX11CPUAccessFlags(m_eBufferUsage);
	m_impl->m_d3dDesc.Usage = Buffer::ConvertToDX11Usage(m_eBufferUsage);
	m_impl->m_d3dDesc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA d3dData = {};
	Memory::MemZero_T(d3dData);
	D3D11_SUBRESOURCE_DATA* pD3dData = nullptr;
	if (pSrc != nullptr)
	{
		d3dData.pSysMem = pSrc;
		d3dData.SysMemPitch = get_size();
		d3dData.SysMemSlicePitch = 0;
		pD3dData = &d3dData;
	}
	if (DX11ResultCheckFail(m_pAdapter->impl().d3dDevice()->CreateBuffer(&m_impl->m_d3dDesc, pD3dData, &m_impl->m_pD3DBuffer)))
	{
		return false;
	}
	return true;
}

bool IHWBuffer::_release(void)
{
	m_impl->m_d3dDesc = {};
	FXD_RELEASE(m_impl->m_pD3DBuffer, Release());
	return true;
}

void* IHWBuffer::_map(FXD::U32 nOffset, FXD::U32 nLength, GFX::E_BufferLockOptions eLockMode, FXD::U32 nQueueID)
{
	PRINT_COND_ASSERT((nLength <= get_size()), "GFX: Provided length " << nLength << " which is larger than the buffer " << get_size() << "");
	PRINT_COND_ASSERT(((nOffset + nLength) <= get_size()), "GFX: Provided offset " << nOffset << " + length " << nLength << " is larger than the buffer " << get_size() << "");
	PRINT_COND_ASSERT((nLength > 0), "GFX: Provided length of zero to the buffer");

	LockedKey lockedKey(m_impl->m_pD3DBuffer);
	LockedData lockedData;

	if (is_dynamic())
	{
		PRINT_COND_ASSERT((eLockMode == GFX::E_BufferLockOptions::WriteOnly), "GFX: Attempting to read to a non dynamic buffer");

		// If the buffer is dynamic, map its memory for writing.
		D3D11_MAPPED_SUBRESOURCE d3dSubresource = {};
		if (DX11ResultCheckFail(m_pAdapter->impl().d3dDevContext()->Map(m_impl->m_pD3DBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dSubresource)))
		{
			PRINT_ASSERT << "GFX: Failed Map()";
		}

		FXD::U8* pData = ((FXD::U8*)d3dSubresource.pData) + nOffset;
		lockedData.set_data(pData);
		lockedData.Pitch = d3dSubresource.RowPitch;
		lockedData.Offset = nOffset;
	}
	else
	{
		if (eLockMode == GFX::E_BufferLockOptions::ReadOnly)
		{
			// If the static buffer is being locked for reading, create a staging buffer.
			D3D11_BUFFER_DESC d3dBufferDesc = {};
			Memory::MemZero_T(d3dBufferDesc);
			d3dBufferDesc.ByteWidth = nLength;
			d3dBufferDesc.StructureByteStride = 0;
			d3dBufferDesc.BindFlags = 0;
			d3dBufferDesc.CPUAccessFlags = Buffer::ConvertToDX11CPUAccessFlags(GFX::E_BufferUsageFlag::USAGE_CPU_READ);
			d3dBufferDesc.Usage = Buffer::ConvertToDX11Usage(GFX::E_BufferUsageFlag::USAGE_CPU_READ);
			d3dBufferDesc.MiscFlags = 0;

			ID3D11Buffer* pStagingBuffer = nullptr;
			if (DX11ResultCheckFail(m_pAdapter->impl().d3dDevice()->CreateBuffer(&d3dBufferDesc, nullptr, &pStagingBuffer)))
			{
				PRINT_ASSERT << "GFX: Failed CreateBuffer()";
			}
			lockedData.StagingResource = pStagingBuffer;

			// Copy the contents of the buffer to the staging buffer.
			D3D11_BOX d3dSourceBox = {};
			Memory::MemZero_T(d3dSourceBox);
			d3dSourceBox.left = nOffset;
			d3dSourceBox.right = (nOffset + nLength);
			d3dSourceBox.top = d3dSourceBox.front = 0;
			d3dSourceBox.bottom = d3dSourceBox.back = 1;
			m_pAdapter->impl().d3dDevContext()->CopySubresourceRegion(pStagingBuffer, 0, 0, 0, 0, m_impl->m_pD3DBuffer, 0, &d3dSourceBox);

			// Map the staging buffer's memory for reading.
			D3D11_MAPPED_SUBRESOURCE d3dSubresource = {};
			if (DX11ResultCheckFail(m_pAdapter->impl().d3dDevContext()->Map(pStagingBuffer, 0, D3D11_MAP_READ, 0, &d3dSubresource)))
			{
				PRINT_ASSERT << "GFX: Failed Map()";
			}
			lockedData.set_data(d3dSubresource.pData);
			lockedData.Pitch = d3dSubresource.RowPitch;
			lockedData.Offset = nOffset;
		}
		else
		{
			// If the static buffer is being locked for writing, allocate memory for the contents to be written to.
			lockedData.alloc_data(nLength);
			lockedData.Pitch = nLength;
			lockedData.Offset = nOffset;
		}
	}

	m_impl->m_outstandingLocks[lockedKey] = lockedData;
	return lockedData.get_data();
}

void IHWBuffer::_unmap(void)
{
	LockedKey lockedKey(m_impl->m_pD3DBuffer);
	{
		LockedData lockedData;
		PRINT_COND_ASSERT(m_impl->m_outstandingLocks.try_get_value(lockedKey, lockedData), "GFX: Could not find lock data");

		if (is_dynamic())
		{
			// If the lock is dynamic, its memory was mapped directly; unmap it.
			m_pAdapter->impl().d3dDevContext()->Unmap(m_impl->m_pD3DBuffer, 0);
		}
		else
		{
			// If the lock is involved a staging resource, it was locked for reading.
			if (lockedData.StagingResource != nullptr)
			{
				// Unmap the staging buffer's memory.
				m_pAdapter->impl().d3dDevContext()->Unmap((ID3D11Buffer*)lockedData.StagingResource, 0);
			}
			else
			{
				// Copy the contents of the temporary memory buffer allocated for writing into the VB.
				D3D11_BOX d3dSourceBox = {};
				Memory::MemZero_T(d3dSourceBox);
				d3dSourceBox.left = lockedData.Offset;
				d3dSourceBox.right = (lockedData.Offset + lockedData.Pitch);
				d3dSourceBox.top = d3dSourceBox.front = 0;
				d3dSourceBox.bottom = d3dSourceBox.back = 1;
				m_pAdapter->impl().d3dDevContext()->UpdateSubresource(m_impl->m_pD3DBuffer, 0, &d3dSourceBox, lockedData.get_data(), 0, 0);
				//m_pAdapter->impl().d3dDevContext()->Flush();

				// Free the temporary memory buffer.
				lockedData.free_data();
			}
		}
	}

	// Remove the LockedData from the lock map. If the lock involved a staging resource, this releases it.
	m_impl->m_outstandingLocks.find_erase(lockedKey);
}

/*
void IHWBuffer::copy_data(HWBuffer& srcBuffer, FXD::U32 nSrcOffset, FXD::U32 nDstOffset, FXD::U32 nLength, bool bDiscardWholeBuffer, const std::shared_ptr< GFX::CmdBuffer >& commandBuffer)
{
	auto executeRef = [this](HWBuffer& srcBuffer, FXD::U32 nSrcOffset, FXD::U32 nDstOffset, FXD::U32 nLength)
	{
		// If we're copying same-size buffers in their entirety
		if (nSrcOffset == 0 && nDstOffset == 0 && nLength == m_nSize && m_nSize == srcBuffer.get_size())
		{
			mDevice.getImmediateContext()->CopyResource(mD3DBuffer, static_cast<HWBufferDX11&>(srcBuffer).getD3DBuffer());
			if (mDevice.hasError())
			{
				Core::String8 errorDescription = mDevice.getErrorDescription();
				FXD_ERROR("BS_EXCEPT");
				//BS_EXCEPT(RenderingAPIException, "Cannot copy D3D11 resource\nError Description:" + errorDescription);
			}
		}
		else
		{
			// Copy subregion
			D3D11_BOX srcBox;
			srcBox.left = (UINT)nSrcOffset;
			srcBox.right = (UINT)nSrcOffset + nLength;
			srcBox.top = 0;
			srcBox.bottom = 1;
			srcBox.front = 0;
			srcBox.back = 1;

			mDevice.getImmediateContext()->CopySubresourceRegion(mD3DBuffer, 0, (UINT)nDstOffset, 0, 0, static_cast<HWBufferDX11&>(srcBuffer).getD3DBuffer(), 0, &srcBox);
			if (mDevice.hasError())
			{
				Core::String8 errorDescription = mDevice.getErrorDescription();
				FXD_ERROR("BS_EXCEPT");
				//BS_EXCEPT(RenderingAPIException, "Cannot copy D3D11 subresource region\nError Description:" + errorDescription);
			}
		}
	};
	if (commandBuffer == nullptr)
	{
		executeRef(srcBuffer, nSrcOffset, nDstOffset, nLength);
	}
	else
	{
		auto execute = [&]()
		{
			executeRef(srcBuffer, nSrcOffset, nDstOffset, nLength);
		};
		std::shared_ptr< D3D11CommandBuffer > cb = std::static_pointer_cast< D3D11CommandBuffer >(commandBuffer);
		cb->queue_command(execute);
	}
}
*/

bool IIndexBuffer::_activate(FXD::U32 nOffset) const
{
	UINT nD3DOffset = (UINT)nOffset;
	GFX::Cache::IndexBuffer::Set(m_pAdapter->impl().d3dDevContext(), m_impl->m_pD3DBuffer, GFX::E_IndexType::Bit_16, nD3DOffset);
	return true;
}

bool IVertexBuffer::_activate(FXD::U32 nOffset, FXD::U32 nStrideOverride) const
{
	UINT nD3DOffset = (UINT)nOffset;
	UINT nD3DStride = (nStrideOverride != 0) ? (UINT)nStrideOverride : (UINT)m_nStride;
	GFX::Cache::VertexBuffer::Set(m_pAdapter->impl().d3dDevContext(), m_impl->m_pD3DBuffer, 0, nD3DStride, nD3DOffset);
	return true;
}
#endif //IsGFXDX11()