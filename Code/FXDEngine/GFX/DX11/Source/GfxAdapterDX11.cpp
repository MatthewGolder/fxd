// Creator - MatthewGolder
#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/GFX/GfxJPThread.h"
#include "FXDEngine/GFX/DX11/GfxAdapterDX11.h"
#include "FXDEngine/GFX/DX11/GfxCommandBufferDX11.h"
#include "FXDEngine/GFX/FX/FXLibrary.h"
#include "FXDEngine/GFX/FX/StateManager.h"

using namespace FXD;
using namespace GFX;

CONSTEXPR D3D_DRIVER_TYPE d3dDriverTypes[] =
{
	D3D_DRIVER_TYPE_HARDWARE,
	D3D_DRIVER_TYPE_WARP,
	D3D_DRIVER_TYPE_REFERENCE
};

CONSTEXPR D3D_FEATURE_LEVEL d3dFeatureLevels[] =
{
	D3D_FEATURE_LEVEL_11_0,
	D3D_FEATURE_LEVEL_10_1,
	D3D_FEATURE_LEVEL_10_0,
	D3D_FEATURE_LEVEL_9_3
};

// ------
// IGfxAdapterDesc::Impl::Impl
// - 
// ------
IGfxAdapterDesc::Impl::Impl::Impl(void)
{
}

IGfxAdapterDesc::Impl::~Impl(void)
{
}

// ------
// IGfxAdapterDesc::IGfxAdapterDesc
// - 
// ------
IGfxAdapterDesc::IGfxAdapterDesc(const App::DeviceIndex nDeviceID)
	: IAdapterDesc(nDeviceID)
	, m_eDeviceType(GFX::E_DeviceType::Unknown)
{
}

IGfxAdapterDesc::~IGfxAdapterDesc(void)
{
}

// ------
// IGfxAdapter::Impl::Impl
// - 
// ------
IGfxAdapter::Impl::Impl::Impl(void)
	: m_d3dDriverType(D3D_DRIVER_TYPE_NULL)
	, m_d3dFeatureLevel(D3D_FEATURE_LEVEL_11_0)
{
}

IGfxAdapter::Impl::~Impl(void)
{
	PRINT_COND_ASSERT((!m_d3dDevice), "GFX: m_d3dDevice is not shutdown");
	PRINT_COND_ASSERT((!m_d3dDevContext), "GFX: m_d3dDevContext is not shutdown");
	PRINT_COND_ASSERT((!m_d3dClassLinkage), "GFX: m_d3dClassLinkage is not shutdown");
	PRINT_COND_ASSERT((!m_d3dInfoQueue), "GFX: m_d3dInfoQueue is not shutdown");
}

// ------
// IGfxAdapter
// - 
// ------
IGfxAdapter::IGfxAdapter(const App::AdapterDesc& adapterDesc, const App::IApi* pApi)
	: IAdapter(adapterDesc, pApi)
	, m_pDrawingViewport(nullptr)
{
}

IGfxAdapter::~IGfxAdapter(void)
{
}

bool IGfxAdapter::_create(void)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	auto pAdapterDesc = static_cast< GFX::IGfxAdapterDesc* >(m_adapterDesc.get());

	// 1. Create Device
	DWORD dCreateFlags = 0;
#if defined(FXD_DEBUG)
	dCreateFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	HRESULT hr = S_OK;
	for (const auto d3dDriverType : d3dDriverTypes)
	{
		m_impl->m_d3dDriverType = d3dDriverType;
		hr = D3D11CreateDevice(pAdapterDesc->impl().dxgiAdapter(), m_impl->m_d3dDriverType, nullptr, dCreateFlags, &d3dFeatureLevels[0], FXD::STD::array_size(d3dFeatureLevels), D3D11_SDK_VERSION, m_impl->m_d3dDevice, &m_impl->m_d3dFeatureLevel, m_impl->m_d3dDevContext);
		if (hr == E_INVALIDARG)
		{
			// DirectX 11.0 platforms will not recognize D3D_FEATURE_LEVEL_11_1 so we need to retry without it
			hr = D3D11CreateDevice(nullptr, m_impl->m_d3dDriverType, nullptr, dCreateFlags, &d3dFeatureLevels[0], FXD::STD::array_size(d3dFeatureLevels), D3D11_SDK_VERSION, m_impl->m_d3dDevice, &m_impl->m_d3dFeatureLevel, m_impl->m_d3dDevContext);
		}
		if (SUCCEEDED(hr))
		{
			break;
		}
	}
	if (DX11ResultCheckFail(hr))
	{
		PRINT_WARN << "GFX: Failed to create D3D11 device";
		return false;
	}

	// Get the ClassLinkage if you can
	if (m_impl->m_d3dDevice->GetFeatureLevel() == D3D_FEATURE_LEVEL_11_0)
	{
		if (DX11ResultCheckFail(m_impl->m_d3dDevice->CreateClassLinkage(m_impl->m_d3dClassLinkage)))
		{
			PRINT_WARN << "GFX: Unable to create class linkage";
		}
	}

	if (DX11ResultCheckFail(m_impl->m_d3dDevice->QueryInterface(__uuidof(ID3D11InfoQueue), (LPVOID*)&m_impl->m_d3dInfoQueue)))
	{
		PRINT_WARN << "GFX: Unable to query D3D11InfoQueue";
	}

	m_fxLibrary = std::make_shared< GFX::FX::IFXLibrary >(this);
	return true;
}

bool IGfxAdapter::_release(void)
{
	PRINT_COND_ASSERT(!GFX::Funcs::IsGfxThreadRestricted(), "GFX: Not allowed on current thread");

	m_impl->m_d3dInfoQueue = nullptr;
	m_impl->m_d3dClassLinkage = nullptr;
	m_impl->m_d3dDevice = nullptr;
	m_impl->m_d3dDevContext = nullptr;
	return true;
}

/*
void IGfxAdapter::setVertexBuffers(const GFX::VertexBuffer& buffer, FXD::U32 nOffset, FXD::U32 nStrideOverride, const GFX::GfxCommandBuffer& commandBuffer)
{
	auto executeRef = [&](const GFX::VertexBuffer&, FXD::U32 nOffset, FXD::U32 nStrideOverride)
	{
		buffer->activate(nOffset, nStrideOverride);
	};

	if (commandBuffer == nullptr)
	{
		executeRef(buffer, nOffset, nStrideOverride);
	}
	else
	{
		auto execute = [=]() { executeRef(buffer, nOffset, nStrideOverride); };
		commandBuffer->impl().queue_command(execute);
	}
}

void IGfxAdapter::set_indexBuffer(const GFX::IndexBuffer& buffer, FXD::U32 nOffset, const GFX::GfxCommandBuffer& commandBuffer)
{
	auto executeRef = [&](const GFX::IndexBuffer& buffer, FXD::U32 nOffset)
	{
		buffer->activate(nOffset);
	};

	if (commandBuffer == nullptr)
	{
		executeRef(buffer, nOffset);
	}
	else
	{
		auto execute = [=]() { executeRef(buffer, nOffset); };
		commandBuffer->impl().queue_command(execute);
	}
}
*/
#endif //IsGFXDX11()