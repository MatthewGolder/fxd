// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DX11_GPUCOMMANDBUFFERDX11_H
#define FXDENGINE_GFX_DX11_GPUCOMMANDBUFFERDX11_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/GfxCommandBuffer.h"
#include "FXDEngine/GFX/DX11/DX11.h"
#include "FXDEngine/Container/Vector.h"

#include <functional>

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IGfxCommandBuffer >
			// -
			// ------
			template <>
			class Impl< GFX::IGfxCommandBuffer >
			{
			public:
				friend class GFX::IGfxCommandBuffer;

			public:
				Impl(GFX::IGfxCommandBuffer* pParent);
				~Impl(void);

				void queue_command(const FXD::STD::function< void() > command);

				void append_secondary(const GFX::GfxCommandBuffer& secondaryBuffer);

				void execute_commands(void);

				void clear(void);

			private:
				GFX::IGfxCommandBuffer* m_pParent;
				Container::Vector< FXD::STD::function< void() > > m_commands;
			};

			// ------
			// Impl< GFX::IGfxCommandBufferManager >
			// -
			// ------
			template <>
			class Impl< GFX::IGfxCommandBufferManager >
			{
			public:
				friend class GFX::IGfxCommandBufferManager;

			public:
				Impl(void);
				~Impl(void);
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_DX11_GPUCOMMANDBUFFERDX11_H