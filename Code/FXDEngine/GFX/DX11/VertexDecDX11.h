// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DX11_VERTEXDECDX11_H
#define FXDENGINE_GFX_DX11_VERTEXDECDX11_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/VertexDec.h"
#include "FXDEngine/GFX/DX11/DX11.h"
#include "FXDEngine/Container/RefCounting.h"

namespace FXD
{
	namespace GFX
	{
		using D3D11VertexElements = Container::Array< D3D11_INPUT_ELEMENT_DESC, kMaxVertexElements >;
	} //namespace GFX

	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IVertexDecleration >
			// -
			// ------
			template <>
			class Impl< GFX::IVertexDecleration >
			{
			public:
				friend class GFX::IVertexDecleration;

			public:
				Impl(void);
				~Impl(void);

			protected:
				Container::Ptr< ID3D11InputLayout > m_vertexLayout;
				GFX::D3D11VertexElements m_d3dVertexElements;
			};
		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_DX11_VERTEXDECDX11_H