// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DX11_DISPLAYDX11_H
#define FXDENGINE_GFX_DX11_DISPLAYDX11_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/Viewport.h"
#include "FXDEngine/GFX/DX11/DX11.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IViewport >
			// -
			// ------
			template <>
			class Impl< GFX::IViewport >
			{
			public:
				friend class GFX::IViewport;

			public:
				Impl(void);
				~Impl(void);
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_DX11_DISPLAYDX11_H