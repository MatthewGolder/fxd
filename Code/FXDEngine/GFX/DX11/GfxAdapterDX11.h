// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DX11_GFXADAPTERDX11_H
#define FXDENGINE_GFX_DX11_GFXADAPTERDX11_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/GfxAdapter.h"
#include "FXDEngine/GFX/DX11/DX11.h"
#include "FXDEngine/GFX/DX11/DX11State.h"
#include "FXDEngine/Container/RefCounting.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IGfxAdapterDesc >
			// - 
			// ------
			template <>
			class Impl< GFX::IGfxAdapterDesc >
			{
			public:
				friend class GFX::IGfxAdapterDesc;

			public:
				Impl(void);
				~Impl(void);

				SET_REF(Container::Ptr< IDXGIAdapter1 >, dxgiAdapter, dxgiAdapter);
				GET_REF_RW(Container::Ptr< IDXGIAdapter1 >, dxgiAdapter, dxgiAdapter);

			private:
				Container::Ptr< IDXGIAdapter1 > m_dxgiAdapter;
			};

			// ------
			// Impl< GFX::IGfxAdapter >
			// -
			// ------
			template <>
			class Impl< GFX::IGfxAdapter >
			{
			public:
				friend class GFX::IGfxAdapter;

			public:
				Impl(void);
				~Impl(void);

				GET_REF_RW(Container::Ptr< ID3D11Device >, d3dDevice, d3dDevice);
				GET_REF_RW(Container::Ptr< ID3D11DeviceContext >, d3dDevContext, d3dDevContext);
				GET_REF_RW(Container::Ptr< ID3D11ClassLinkage >, d3dClassLinkage, d3dClassLinkage);

			private:
				D3D_DRIVER_TYPE m_d3dDriverType;
				D3D_FEATURE_LEVEL m_d3dFeatureLevel;
				Container::Ptr< ID3D11Device > m_d3dDevice;
				Container::Ptr< ID3D11DeviceContext > m_d3dDevContext;
				Container::Ptr< ID3D11ClassLinkage > m_d3dClassLinkage;
				Container::Ptr< ID3D11InfoQueue > m_d3dInfoQueue;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_DX11_GFXADAPTERDX11_H