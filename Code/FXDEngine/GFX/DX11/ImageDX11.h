// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DX11_IMAGEDX11_H
#define FXDENGINE_GFX_DX11_IMAGEDX11_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/Image.h"
#include "FXDEngine/GFX/DX11/DX11.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IImage >
			// -
			// ------
			template <>
			class Impl< GFX::IImageView >
			{
			public:
				friend class GFX::IImageView;

			public:
				Impl(void);
				~Impl(void);

				GET_RW(ID3D11RenderTargetView*, pViewRenderTarget, view_render_target);
				SET(ID3D11RenderTargetView*, pViewRenderTarget, view_render_target);

				GET_RW(ID3D11DepthStencilView*, pViewDepth, view_depth);
				SET(ID3D11DepthStencilView*, pViewDepth, view_depth);

				GET_RW(ID3D11ShaderResourceView*, pViewShader, view_shader);
				SET(ID3D11ShaderResourceView*, pViewShader, view_shader);

			private:
				ID3D11RenderTargetView* m_pViewRenderTarget;
				ID3D11DepthStencilView* m_pViewDepth;
				ID3D11ShaderResourceView* m_pViewShader;
			};

			// ------
			// Impl< GFX::IImage >
			// -
			// ------
			template <>
			class Impl< GFX::IImage >
			{
			public:
				friend class GFX::IImage;

			public:
				Impl(void);
				~Impl(void);

				GET_RW(ID3D11Resource*, pResource, resource);
				SET(ID3D11Resource*, pResource, resource);

			private:
				ID3D11Resource* m_pResource;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_DX11_IMAGEDX11_H