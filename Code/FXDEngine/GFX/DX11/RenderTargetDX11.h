// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DX11_RENDERTARGETDX11_H
#define FXDENGINE_GFX_DX11_RENDERTARGETDX11_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/RenderTarget.h"
#include "FXDEngine/GFX/DX11/DX11.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IRenderTarget >
			// -
			// ------
			template <>
			class Impl< GFX::IRenderTarget >
			{
			public:
				friend class GFX::IRenderTarget;

			public:
				Impl(void);
				~Impl(void);

			private:
				FXD::U16 m_nNumColorViews;
				ID3D11RenderTargetView* m_pRenderTargetViews[GFX::kMaxMultipleRenderTargets];
				ID3D11DepthStencilView* m_pDepthStencilView;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_DX11_RENDERTARGETDX11_H