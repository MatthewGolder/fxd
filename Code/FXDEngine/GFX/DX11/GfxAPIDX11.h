// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DX11_GFXAPIDX11_H
#define FXDENGINE_GFX_DX11_GFXAPIDX11_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/GfxApi.h"
#include "FXDEngine/GFX/DX11/DX11.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IGfxApi >
			// -
			// ------
			template <>
			class Impl< GFX::IGfxApi >
			{
			public:
				friend class GFX::IGfxApi;

			public:
				Impl(void);
				~Impl(void);

			private:
				HMODULE m_d3dDLL;
				HMODULE m_dxgiDLL;
				HMODULE m_d3dcompilerDLL;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_DX11_GFXAPIDX11_H