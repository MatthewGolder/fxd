// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DX11_WINDOWMANAGERDX11_H
#define FXDENGINE_GFX_DX11_WINDOWMANAGERDX11_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/WindowManager.h"
#include "FXDEngine/GFX/DX11/DX11.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IWindowManager >
			// -
			// ------
			template <>
			class Impl< GFX::IWindowManager >
			{
			public:
				Impl(void);
				~Impl(void);

			protected:
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_DX11_WINDOWMANAGERDX11_H