// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DX11_FENCEDX11_H
#define FXDENGINE_GFX_DX11_FENCEDX11_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/Fence.h"
#include "FXDEngine/GFX/DX11/DX11.h"
#include "FXDEngine/Container/RefCounting.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IFence >
			// -
			// ------
			template <>
			class Impl< GFX::IFence >
			{
			public:
				friend class GFX::IFence;

			public:
				Impl(void);
				~Impl(void);

				GET_REF_R(Container::Ptr< ID3D11Query >, d3dQuery, d3dQuery);

			protected:
				bool _is_signalled(void) const;
				bool _wait(void);
				bool _reset(void);

			protected:
				Container::Ptr< ID3D11Query > m_d3dQuery;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_DX11_FENCEDX11_H