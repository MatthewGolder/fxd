// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DX11_HWBUFFERDX11_H
#define FXDENGINE_GFX_DX11_HWBUFFERDX11_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/HWBuffer.h"
#include "FXDEngine/GFX/DX11/DX11.h"
#include "FXDEngine/Container/Map.h"
#include "FXDEngine/Container/RefCounting.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::IHWBuffer >
			// -
			// ------
			template <>
			class Impl< GFX::IHWBuffer >
			{
			public:
				friend class GFX::IHWBuffer;

			public:
				Impl(void);
				~Impl(void);

			public:
				D3D11_BUFFER_DESC m_d3dDesc;
				ID3D11Buffer* m_pD3DBuffer;
				Container::Map< GFX::LockedKey, GFX::LockedData > m_outstandingLocks;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD

#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_DX11_HWBUFFERDX11_H