// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_GFX_DX11_GPUMEMORYDX11_H
#define FXDENGINE_GFX_DX11_GPUMEMORYDX11_H

#include "FXDEngine/GFX/Types.h"

#if IsGFXDX11()
#include "FXDEngine/GFX/GfxMemory.h"
#include "FXDEngine/GFX/DX11/DX11.h"

namespace FXD
{
	namespace Core
	{
		namespace Impl
		{
			// ------
			// Impl< GFX::MemoryHeapSpec >
			// -
			// ------
			template <>
			class Impl< GFX::MemoryHeapSpec >
			{
			public:
				friend class GFX::MemoryHeapSpec;

			public:
				Impl(void);
				~Impl(void);

			private:
				FXD::U32 m_nMemTypeIdx;
			};

			// ------
			// Impl< GFX::ResourceSet >
			// -
			// ------
			template <>
			class Impl< GFX::ResourceSet >
			{
			public:
				friend class GFX::ResourceSet;

			public:
				Impl(void);
				~Impl(void);

			private:
				GFX::AllocSpec m_allocInfo;
			};

		} //namespace Impl
	} //namespace Core
} //namespace FXD
#endif //IsGFXDX11()
#endif //FXDENGINE_GFX_DX11_GPUMEMORYDX11_H