// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_THREAD_THREAD_H
#define FXDENGINE_THREAD_THREAD_H

#include "FXDEngine/Thread/Types.h"
#include "FXDEngine/Core/Pimpl.h"
#include "FXDEngine/Core/String.h"

namespace FXD
{
	namespace Thread
	{
		// ------
		// E_ThreadPriority
		// -
		// ------
		enum class E_ThreadPriority
		{
			Normal,
			AboveNormal,
			BelowNormal,
			Highest,
			Lowest,
			SlightlyBelowNormal,
			TimeCritical,
			Count,
			Unknown = 0xffff
		};

		// ------
		// IThread
		// -
		// ------
		class IThread : public Core::HasImpl< Thread::IThread, 64 >, public Core::NonCopyable
		{
		public:
			friend class Core::Impl::Impl< Thread::IThread >;

		public:
			IThread(const FXD::UTF8* pName, bool bManageThreadData = true, Thread::E_ThreadPriority eThreadPri = Thread::E_ThreadPriority::Normal, FXD::U32 nStackSize = (64 * 1024));
			virtual ~IThread(void);
				
			void start_thread	(void);
			void pause_thread	(void);
			void resume_thread(void);
			bool join			(void);
			bool is_active		(void);
			bool is_valid		(void);

			void set_thread_name						(const FXD::UTF8* pName);
			void set_thread_priority				(Thread::E_ThreadPriority ePriority);

			virtual bool thread_init_func			(void) PURE;
			virtual bool thread_process_func		(void) PURE;
			virtual bool thread_shutdown_func	(void) PURE;

		private:
			FXD::U32 m_nStackSize;
			FXD::S32 m_nThreadID;
			FXD::Core::String8 m_strName;
			Thread::E_ThreadPriority m_eThreadPri;
			const bool m_bManageThreadData;
		};
		
		namespace Funcs
		{
			extern int GetCurrentThreadID(void);

			extern void SetCurrentThreadName(const FXD::UTF8* pName);
			extern void SetThreadNameByID(unsigned long dwThreadID, const FXD::UTF8* pThreadName);

			extern void Sleep(FXD::U32 nMilliseconds);
			extern FXD::S32 TranslateThreadPriority(Thread::E_ThreadPriority ePriority);
		} //namespace Funcs

	} //namespace Thread
} //namespace FXD
#endif //FXDENGINE_THREAD_THREAD_H