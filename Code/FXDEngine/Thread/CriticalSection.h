// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_THREAD_CRITICALSECTION_H
#define FXDENGINE_THREAD_CRITICALSECTION_H

#include "FXDEngine/Thread/Types.h"
#include "FXDEngine/Core/NonCopyable.h"
#include "FXDEngine/Core/Pimpl.h"

namespace FXD
{
	namespace Thread
	{
		// ------
		// CSLock
		// -
		// ------
		class CSLock FINAL : public Core::HasImpl< Thread::CSLock, 40 >, public Core::NonCopyable
		{
		public:
			// ------
			// LockGuard
			// -
			// ------
			class LockGuard FINAL : public Core::NonCopyable
			{
			public:
				LockGuard(Thread::CSLock& cs);
				~LockGuard(void);

			private:
				CSLock& m_cs;
#if defined(FXD_DEBUG)
				bool m_bLocked;
#endif
			};

		public:
			CSLock(void);
			~CSLock(void);

			void acquire_lock(void);
			void release_lock(void);

		private:
			FXD::S32 m_nOwnerID;
			FXD::S32 m_nLockCount;
		};

		// ------
		// CSLockData
		// -
		// ------
		template < typename Data >
		class CSLockData FINAL
		{
		public:
			// ------
			// LockGuard
			// -
			// ------
			class LockGuard FINAL : public Core::NonCopyable
			{
			public:
				LockGuard(CSLockData& lockable)
					: m_lockable(lockable)
				{
					m_lockable.m_base.acquire_lock();
				}
				~LockGuard(void)
				{
					m_lockable.m_base.release_lock();
				}

				Data* operator->() const { return &m_lockable.m_data; }
				Data& operator*() const { return m_lockable.m_data; }

			private:
				CSLockData& m_lockable;
			};

			// ------
			// QuickReadLock
			// -
			// ------
			class QuickReadLock FINAL : public Core::NonCopyable
			{
			public:
				QuickReadLock(const CSLockData* pLockable)
					: m_pLockable(pLockable)
				{
					m_pLockable->m_base.acquire_lock();
				}
				QuickReadLock(QuickReadLock&& ql)
					: m_pLockable(ql.m_pLockable)
				{
					ql.m_pLockable = nullptr;
				}
				~QuickReadLock(void)
				{
					if (m_pLockable != nullptr)
					{
						m_pLockable->m_base.release_lock();
					}
				}

				const Data* operator->() const { return &m_pLockable->m_data; }
				const Data& operator*() const { return m_pLockable->m_data; }

			private:
				const CSLockData* m_pLockable;
			};

			// ------
			// QuickWriteLock
			// -
			// ------
			class QuickWriteLock FINAL : public Core::NonCopyable
			{
			public:
				QuickWriteLock(CSLockData* pLockable)
					: m_pLockable(pLockable)
				{
					m_pLockable->m_base.acquire_lock();
				}
				QuickWriteLock(QuickWriteLock&& ql)
					: m_pLockable(ql.m_pLockable)
				{
					ql.m_pLockable = nullptr;
				}
				~QuickWriteLock(void)
				{
					if (m_pLockable != nullptr)
					{
						m_pLockable->m_base.release_lock();
					}
				}

				Data* operator->() const { return &m_pLockable->m_data; }
				Data& operator*() const	{ return m_pLockable->m_data; }

			private:
				CSLockData* m_pLockable;
			};

		public:
			template < typename... Types >
			CSLockData(Types&&... inArgs)
				: m_data(inArgs...)
			{}
			~CSLockData(void)
			{}

			inline QuickReadLock get_quick_read_lock(void) const	{ return QuickReadLock(this); }
			inline QuickWriteLock get_quick_write_lock(void)		{ return QuickWriteLock(this); }

		private:
			Data m_data;
			mutable Thread::CSLock m_base;
		};

	} //namespace Thread
} //namespace FXD
#endif //FXDENGINE_THREAD_CRITICALSECTION_H