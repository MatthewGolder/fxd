// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_THREAD_RWLOCK_H
#define FXDENGINE_THREAD_RWLOCK_H

#include "FXDEngine/Thread/Types.h"
#include "FXDEngine/Core/NonCopyable.h"
#include "FXDEngine/Core/Pimpl.h"

namespace FXD
{
	namespace Thread
	{
		// ------
		// RWLock
		// -
		// ------
		class RWLock FINAL : public Core::HasImpl< Thread::RWLock, 64 >, public Core::NonCopyable
		{
		public:
			// ------
			// ScopedReadLock
			// -
			// ------
			class ScopedReadLock FINAL : public Core::NonCopyable
			{
			public:
				ScopedReadLock(const RWLock& lockable);
				~ScopedReadLock(void);

			private:
				const RWLock& m_lockable;
			};

			// ------
			// ScopedWriteLock
			// -
			// ------
			class ScopedWriteLock FINAL : public Core::NonCopyable
			{
			public:
				ScopedWriteLock(RWLock& lockable);
				~ScopedWriteLock(void);

			private:
				RWLock& m_lockable;
			};

		public:
			RWLock(void);
			~RWLock(void);

			void acquire_read_lock(void) const;
			void acquire_write_lock(void);

			void release_read_lock(void) const;
			void release_write_lock(void);

		private:
			mutable FXD::S32 m_nReadLockCount;
			mutable FXD::S32 m_nWriteLockCount;
			mutable FXD::S32 m_nWriterID;
		};

		// ------
		// RWLockData
		// -
		// ------
		template < typename Data >
		class RWLockData FINAL
		{
		public:
			// ------
			// ScopedReadLock
			// -
			// ------
			class ScopedReadLock FINAL : public Core::NonCopyable
			{
			public:
				ScopedReadLock(const RWLockData& lockable)
					: m_lockable(lockable)
				{
					m_lockable.m_base.acquire_read_lock();
				}
				~ScopedReadLock(void)
				{
					m_lockable.m_base.release_read_lock();
				}

				const Data* operator->() const { return &m_lockable.m_data; }
				const Data& operator*() const	{ return m_lockable.m_data; }

			private:
				const RWLockData& m_lockable;
			};

			// ------
			// ScopedWriteLock
			// -
			// ------
			class ScopedWriteLock FINAL : public Core::NonCopyable
			{
			public:
				ScopedWriteLock(RWLockData& lockable)
					: m_lockable(lockable)
				{
					m_lockable.m_base.acquire_write_lock();
				}
				~ScopedWriteLock(void)
				{
					m_lockable.m_base.release_write_lock();
				}

				Data* operator->() const	{ return &m_lockable.m_data; }
				Data& operator*() const	{ return m_lockable.m_data; }

			private:
				RWLockData& m_lockable;
			};

			// ------
			// QuickReadLock
			// -
			// ------
			class QuickReadLock FINAL : public Core::NonCopyable
			{
			public:
				QuickReadLock(const RWLockData* pLockable)
					: m_pLockable(pLockable)
				{
					m_pLockable->m_base.acquire_read_lock();
				}
				QuickReadLock(QuickReadLock&& rhs)
					: m_pLockable(rhs.m_pLockable)
				{
					rhs.m_pLockable = nullptr;
				}
				~QuickReadLock(void)
				{
					if (m_pLockable != nullptr)
					{
						m_pLockable->m_base.release_read_lock();
					}
				}

				const Data* operator->() const { return &m_pLockable->m_data; }
				const Data& operator*() const	{ return m_pLockable->m_data; }

			private:
				const RWLockData* m_pLockable;
			};

			// ------
			// QuickWriteLock
			// -
			// ------
			class QuickWriteLock FINAL : public Core::NonCopyable
			{
			public:
				QuickWriteLock(RWLockData* pLockable)
					: m_pLockable(pLockable)
				{
					m_pLockable->m_base.acquire_write_lock();
				}
				QuickWriteLock(QuickWriteLock&& rhs)
					: m_pLockable(rhs.m_pLockable)
				{
					rhs.m_pLockable = nullptr;
				}
				~QuickWriteLock(void)
				{
					if (m_pLockable != nullptr)
					{
						m_pLockable->m_base.release_write_lock();
					}
				}

				Data* operator->() const	{ return &m_pLockable->m_data; }
				Data& operator*() const	{ return m_pLockable->m_data; }

			private:
				RWLockData* m_pLockable;
			};

		public:
			template < typename... Types >
			RWLockData(Types&&... inArgs)
				: m_data(inArgs...)
			{}

			~RWLockData(void)
			{}

			inline QuickReadLock get_quick_read_lock(void) const { return QuickReadLock(this); }
			inline QuickWriteLock get_quick_write_lock(void)	 { return QuickWriteLock(this); }

		private:
			Data m_data;
			RWLock m_base;
		};

	} //namespace Thread
} //namespace FXD
#endif //FXDENGINE_THREAD_RWLOCK_H