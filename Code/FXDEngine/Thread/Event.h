// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_THREAD_EVENT_H
#define FXDENGINE_THREAD_EVENT_H

#include "FXDEngine/Thread/Types.h"
#include "FXDEngine/Core/NonCopyable.h"
#include "FXDEngine/Core/Pimpl.h"

namespace FXD
{
	namespace Thread
	{
		// ------
		// Event
		// -
		// ------
		class Event FINAL : public Core::HasImpl< Thread::Event, 256 >, public Core::NonCopyable
		{
		public:
			Event(bool bAutoReset = true);
			~Event(void);

			bool wait(void) const;
			bool wait_for(FXD::U32 nTimeOutMS) const;
			void set(void);
			void reset(void);
		};

	} //namespace Thread
} //namespace FXD
#endif //FXDENGINE_THREAD_EVENT_H