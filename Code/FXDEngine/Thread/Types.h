// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_THREAD_TYPES_H
#define FXDENGINE_THREAD_TYPES_H

#include "FXDEngine/Core/Types.h"

#if defined(FXD_DEBUG)
#	define FXD_ENABLE_THREAD_PERFORMANCE
#endif

#endif //FXDENGINE_THREAD_TYPES_H