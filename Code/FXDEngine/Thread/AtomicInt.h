// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_THREAD_ATOMICINT_H
#define FXDENGINE_THREAD_ATOMICINT_H

#include "FXDEngine/Thread/Types.h"

namespace FXD
{
	namespace Thread
	{
		// ------
		// AtomicInt
		// -
		// ------
		class AtomicInt
		{
		public:
			using Type = volatile long;

		public:
			inline AtomicInt(void)
				: m_value(0)
			{}
			inline AtomicInt(long v)
				: m_value(v)
			{}
			~AtomicInt(void)
			{}

			AtomicInt& operator=(long rhs);

			long increment(void);
			long decrement(void);
			long add(long v);
			long test_and_set(long& v);
			long test(long nExchange, long nCompare);
			long get_instant_value(void) const { return m_value; }

		private:
			Type m_value;
		};

	} //namespace Thread
} //namespace FXD
#endif //FXDENGINE_CORE_ATOMICINT_H