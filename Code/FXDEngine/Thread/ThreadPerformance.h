// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_THREAD_THREADPERFORMANCE_H
#define FXDENGINE_THREAD_THREADPERFORMANCE_H

#include "FXDEngine/Thread/Types.h"
#include "FXDEngine/Core/String.h"

namespace FXD
{
	namespace Thread
	{
		// ------
		// ThreadPerformance
		// -
		// ------
		class ThreadPerformance
		{
		public:
			ThreadPerformance(void);
			~ThreadPerformance(void);

			static void initialise(const Core::String8& strThreadName);
			static void shutdown(void);
			static bool is_initialised(void);
			static void process_thread_counters(bool bPrint = false);
		};

	} //namespace Thread
} //namespace FXD
#endif //FXDENGINE_THREAD_THREADPERFORMANCE_H