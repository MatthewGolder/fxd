// Creator - MatthewGolder
#include "FXDEngine/Thread/Types.h"

#if IsSystemWindows()
#include "FXDEngine/Thread/Thread.h"
#include "FXDEngine/Thread/ThreadLocalStorage.h"
#include "FXDEngine/Thread/ThreadPerformance.h"
#include "FXDEngine/Core/Win32/Win32.h"

using namespace FXD;
using namespace Thread;

#define MS_VC_EXCEPTION 0x406d1388
class THREADNAME_INFO
{
public:
	DWORD dwType; // Must be 0x1000.
	LPCSTR szName; // Pointer to name (in user addr space).
	DWORD dwThreadID; // Thread ID (-1=caller thread).
	DWORD dwFlags; // Reserved for future use, must be zero.
};

// ------
// Core::Impl::Impl< IThread >
// -
// ------
template <>
class Core::Impl::Impl< IThread >
{
public:
	friend class IThread;

public:
	Impl(void)
		: m_bPaused(true)
		, m_threadHandle(INVALID_HANDLE_VALUE)
	{
	}
	~Impl(void)
	{
	}

	static DWORD __stdcall THREADKERNEL(void* pData)
	{
		// 1. Set up thread
		Thread::IThread* pThread = (Thread::IThread*)pData;
		pThread->m_nThreadID = Thread::Funcs::GetCurrentThreadID();
		pThread->set_thread_name(pThread->m_strName.c_str());
		pThread->set_thread_priority(pThread->m_eThreadPri);

		// 2. Initialise thread data

		if (pThread->m_bManageThreadData)
		{
			Thread::ThreadLocalData::initialise();
			Thread::ThreadPerformance::initialise(pThread->m_strName);
		}

		// 3. Process thread func
		if (!pThread->thread_init_func())
		{
			PRINT_ASSERT << "Thread: thread_init_func() returned false";
		}
		while (pThread->thread_process_func())
		{
			Thread::ThreadPerformance::process_thread_counters();
			//Memory::Log::PrintFrameStats();
			//Memory::Log::ClearFrameStats();
		}
		if (!pThread->thread_shutdown_func())
		{
			PRINT_ASSERT << "Thread: thread_shutdown_func() returned false";
		}

		// 4. Shutdown thread data

		if (pThread->m_bManageThreadData)
		{
			FXD::Thread::ThreadPerformance::shutdown();
			FXD::Thread::ThreadLocalData::shutdown();
		}
		return S_OK;
	}

private:
	bool m_bPaused;
	HANDLE m_threadHandle;
};

// ------
// IThread::IThread
// -
// ------
IThread::IThread(const FXD::UTF8* pName, bool bManageThreadData, Thread::E_ThreadPriority eThreadPri, FXD::U32 nStackSize)
	: m_nStackSize(nStackSize)
	, m_nThreadID(-1)
	, m_strName(pName)
	, m_eThreadPri(eThreadPri)
	, m_bManageThreadData(bManageThreadData)
{
}

IThread::~IThread(void)
{
	join();
	if (is_valid())
	{
		CloseHandle(m_impl->m_threadHandle);
	}
}

void IThread::start_thread(void)
{
	PRINT_COND_ASSERT((!is_valid()), "Thread: Thread is already valid IThread::start_thread()");

	m_impl->m_threadHandle = CreateThread(NULL, m_nStackSize, Impl::THREADKERNEL, this, 0, (::DWORD*)&m_nThreadID);
	m_impl->m_bPaused = false;
}

void IThread::pause_thread(void)
{
	if (!is_valid())
	{
		return;
	}

	SuspendThread(m_impl->m_threadHandle);
	m_impl->m_bPaused = true;
}

void IThread::resume_thread(void)
{
	if (!is_valid())
	{
		return;
	}

	ResumeThread(m_impl->m_threadHandle);
	m_impl->m_bPaused = false;
}

bool IThread::join(void)
{
	if (!is_valid())
	{
		return false;
	}

	bool bRetVal = (WaitForSingleObject(m_impl->m_threadHandle, (DWORD)INFINITE) == WAIT_OBJECT_0);
	return bRetVal;
}

bool IThread::is_active(void)
{
	if (m_impl->m_bPaused)
	{
		return false;
	}

	DWORD dExitCode;
	GetExitCodeThread(m_impl->m_threadHandle, &dExitCode);
	return (dExitCode == STILL_ACTIVE);
}

bool IThread::is_valid(void)
{
	bool bRetVal = (m_impl->m_threadHandle != INVALID_HANDLE_VALUE);
	return bRetVal;
}

void IThread::set_thread_name(const FXD::UTF8* pName)
{
	m_strName = pName;
	Funcs::SetThreadNameByID(m_nThreadID, pName);
}

void IThread::set_thread_priority(Thread::E_ThreadPriority ePriority)
{
	m_eThreadPri = ePriority;
	::SetThreadPriority(m_impl->m_threadHandle, Funcs::TranslateThreadPriority(ePriority));
}

// ------
// Funcs
// -
// ------
FXD::S32 FXD::Thread::Funcs::GetCurrentThreadID(void)
{
	return GetCurrentThreadId();
}

void FXD::Thread::Funcs::SetCurrentThreadName(const FXD::UTF8* pName)
{
	Thread::Funcs::SetThreadNameByID(Funcs::GetCurrentThreadID(), pName);
}

void FXD::Thread::Funcs::SetThreadNameByID(unsigned long dwThreadID, const FXD::UTF8* pThreadName)
{
	THREADNAME_INFO info;
	info.dwType = 0x1000;
	info.szName = pThreadName;
	info.dwThreadID = dwThreadID;
	info.dwFlags = 0;

	__try
	{
		RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
	}
}

void FXD::Thread::Funcs::Sleep(FXD::U32 nMilliseconds)
{
	::Sleep((DWORD)nMilliseconds);
}

FXD::S32 FXD::Thread::Funcs::TranslateThreadPriority(Thread::E_ThreadPriority ePriority)
{
	switch (ePriority)
	{
		// 0 is the Lowest, 31 is the highest possible priority for pthread
		case E_ThreadPriority::AboveNormal:
		{
			return THREAD_PRIORITY_ABOVE_NORMAL;
		}
		case E_ThreadPriority::Normal:
		{
			return THREAD_PRIORITY_NORMAL;
		}
		case E_ThreadPriority::BelowNormal:
		{
			return THREAD_PRIORITY_BELOW_NORMAL;
		}
		case E_ThreadPriority::Highest:
		{
			return THREAD_PRIORITY_HIGHEST;
		}
		case E_ThreadPriority::Lowest:
		{
			return THREAD_PRIORITY_LOWEST;
		}
		case E_ThreadPriority::SlightlyBelowNormal:
		{
			return THREAD_PRIORITY_NORMAL - 1;
		}
		default:
		{
			PRINT_ASSERT << "Thread: Unknown Priority passed to TranslateThreadPriority()";
			return THREAD_PRIORITY_NORMAL;
		}
	}
}

#endif