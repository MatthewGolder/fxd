// Creator - MatthewGolder
#include "FXDEngine/Thread/Types.h"

#if IsSystemWindows()
#include "FXDEngine/Thread/Event.h"
#include "FXDEngine/Core/Win32/Win32.h"

#include <time.h>

using namespace FXD;
using namespace Thread;

// ------
// Core::Impl::Impl< Event >
// -
// ------
template <>
class Core::Impl::Impl< Event >
{
public:
	friend class Event;

public:
	Impl(bool bAutoReset)
		: m_bAutoReset(bAutoReset)
	{
#ifdef WINDOWS_USE_CRITICAL_SECTION
		m_bSignaled = false;
		InitializeCriticalSection(&m_mutex);
		InitializeConditionVariable(&m_cond);
#else
		m_eventHandle = CreateEvent(NULL, !m_bAutoReset, FALSE, NULL);
#endif
	}
	~Impl(void)
	{
#ifdef WINDOWS_USE_CRITICAL_SECTION
		DeleteCriticalSection(&m_mutex);
#else
		CloseHandle(m_eventHandle);
#endif
	}

private:
	bool m_bAutoReset;
#ifdef WINDOWS_USE_CRITICAL_SECTION
	bool m_bSignaled;
	CONDITION_VARIABLE m_cond;
	CRITICAL_SECTION m_mutex;
#else
	HANDLE m_eventHandle;
#endif
};

// ------
// Event
// -
// ------
Event::Event(bool bAutoReset)
	: HasImpl(Core::_Pimpl_Pack(), bAutoReset)
{
}

Event::~Event(void)
{
}

bool Event::wait_for(FXD::U32 nTimeOutMS) const
{
#ifdef WINDOWS_USE_CRITICAL_SECTION
	EnterCriticalSection(&m_impl->m_mutex);
	while (!m_impl->m_bSignaled)
	{
		SleepConditionVariableCS(&m_impl->m_cond, &m_impl->m_mutex, nTimeOutMS);
		break;
	}
	bool bRet = m_impl->m_bSignaled;
	if (m_impl->m_bAutoReset)
	{
		m_impl->m_bSignaled = false;
	}
	LeaveCriticalSection(&m_impl->m_mutex);
#else
	bool bRet = (WaitForSingleObject(m_impl->m_eventHandle, (DWORD)nTimeOutMS) == WAIT_OBJECT_0);
#endif
	return bRet;
}

void Event::set(void)
{
#ifdef WINDOWS_USE_CRITICAL_SECTION
	EnterCriticalSection(&m_impl->m_mutex);
	if (!m_impl->m_bSignaled)
	{
		m_impl->m_bSignaled = true;
		WakeConditionVariable(&m_impl->m_cond);
	}
	LeaveCriticalSection(&m_impl->m_mutex);
#else
	SetEvent(m_impl->m_eventHandle);
#endif
}

void Event::reset(void)
{
#ifdef WINDOWS_USE_CRITICAL_SECTION
	EnterCriticalSection(&m_impl->m_mutex);
	m_impl->m_bSignaled = false;
	LeaveCriticalSection(&m_impl->m_mutex);
#else
	ResetEvent(m_impl->m_eventHandle);
#endif
}
#endif