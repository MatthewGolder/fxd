// Creator - MatthewGolder
#include "FXDEngine/Thread/Types.h"

#if IsSystemWindows()
#include "FXDEngine/Thread/ThreadLocalStorage.h"
#include "FXDEngine/Core/String.h"
#include "FXDEngine/Memory/ObjectPermanent.h"
#include "FXDEngine/Core/Win32/Win32.h"

using namespace FXD;
using namespace Thread;

//__declspec(thread) void* m_pThreadLocalData = nullptr;
//__declspec(thread) FXD::U32 g_nThreadDataOffset = 0;
static const FXD::U32 kThreadDataMaxSize = 1024;

// ------
// ObjectPermanent
// -
// ------
Memory::ObjectPermanent< FXD::U32 >& getThreadDataOffset(void)
{
	static Memory::ObjectPermanent< FXD::U32 > g_nThreadDataOffset;
	return g_nThreadDataOffset;
}

Memory::ObjectPermanent< DWORD >& getThreadDataIndex(void)
{
	static Memory::ObjectPermanent< DWORD > g_threadDataIndex;
	return g_threadDataIndex;
}

// ------
// ThreadLocalData
// -
// ------
ThreadLocalData::ThreadLocalData(void)
{
	m_pData = TlsGetValue(*getThreadDataIndex());
	PRINT_COND_ASSERT((m_pData != nullptr), "Thread: Thread local data not initialised for this thread");
}

ThreadLocalData::~ThreadLocalData(void)
{
}

bool ThreadLocalData::initialise(void)
{
	if ((*getThreadDataIndex()) == (DWORD)-1)
	{
		(*getThreadDataIndex()) = TlsAlloc();
	}

	void* pData = TlsGetValue((*getThreadDataIndex()));
	if (pData != nullptr)
	{
		return true;
	}

	pData = Memory::Alloc::Default.allocate(kThreadDataMaxSize);
	Memory::MemZero(pData, kThreadDataMaxSize);
	TlsSetValue((*getThreadDataIndex()), pData);
	return false;
}

void ThreadLocalData::shutdown(void)
{
	ThreadLocalData tld;
	Memory::Alloc::Default.deallocate(tld.m_pData/*, "deallocate"*/);
	TlsSetValue((*getThreadDataIndex()), 0);
}

bool ThreadLocalData::is_initialised(void)
{
	Memory::ObjectPermanent< DWORD >& tdi = getThreadDataIndex();
	if (!tdi.is_valid())
	{
		return false;
	}
	return (TlsGetValue(*tdi) != 0);
}


// ------
// ThreadLocalBase
// -
// ------
ThreadLocalBase::ThreadLocalBase(FXD::U32 nSize, FXD::U32 nAlign)
{
	(*getThreadDataOffset()) = ((*getThreadDataOffset()) + nAlign - 1) & ~(nAlign - 1);
	m_nOffset = (*getThreadDataOffset());
	PRINT_COND_ASSERT(((*getThreadDataOffset()) <= kThreadDataMaxSize), "Thread: Not enough thread local data - increase limit");
	(*getThreadDataOffset()) += nSize;
}

ThreadLocalBase::~ThreadLocalBase(void)
{
}

void* ThreadLocalBase::get_data(const ThreadLocalData& tld) const
{
	return ((FXD::U8*)(tld.m_pData) + m_nOffset);
}
#endif