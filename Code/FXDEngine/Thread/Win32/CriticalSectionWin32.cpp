// Creator - MatthewGolder
#include "FXDEngine/Thread/Types.h"

#if IsSystemWindows()
#include "FXDEngine/Thread/CriticalSection.h"
#include "FXDEngine/Thread/Thread.h"
#include "FXDEngine/Core/Win32/Win32.h"

using namespace FXD;
using namespace Thread;

// ------
// Core::Impl::Impl< CSLock >
// -
// ------
template <>
class Core::Impl::Impl< CSLock >
{
public:
	friend class CSLock;

public:
	Impl(void)
	{
#ifdef WINDOWS_USE_CRITICAL_SECTION
		InitializeCriticalSection(&m_cs);
#else
		m_handle = CreateMutex(NULL, FALSE, NULL);
#endif
	}
	~Impl(void)
	{
#ifdef WINDOWS_USE_CRITICAL_SECTION
		DeleteCriticalSection(&m_cs);
#else
		CloseHandle(m_handle);
#endif
	}

private:
#ifdef WINDOWS_USE_CRITICAL_SECTION
	CRITICAL_SECTION m_cs;
#else
	HANDLE m_handle;
#endif
};

// ------
// CSLock
// -
// ------
CSLock::CSLock(void)
	: m_nOwnerID(-1)
	, m_nLockCount(0)
{
}

CSLock::~CSLock(void)
{
	PRINT_COND_ASSERT((m_nLockCount == 0), "Thread: Expected zero lock count when shutdown");
}

void CSLock::acquire_lock(void)
{
	FXD::S32 nCurrentID = Thread::Funcs::GetCurrentThreadID();
	if (m_nOwnerID != nCurrentID)
	{
#ifdef WINDOWS_USE_CRITICAL_SECTION
		EnterCriticalSection(&m_impl->m_cs);
#else
		WaitForSingleObject(m_impl->m_handle, INFINITE);
#endif
		m_nOwnerID = nCurrentID;
	}
	m_nLockCount++;
}

void CSLock::release_lock(void)
{
	PRINT_COND_ASSERT((m_nLockCount != 0), "Thread: Expected non zero lock count");
	PRINT_COND_ASSERT((m_nOwnerID != -1), "Thread: Expected non zero thread ID");

	m_nLockCount--;
	if (m_nLockCount == 0)
	{
		m_nOwnerID = -1;
#ifdef WINDOWS_USE_CRITICAL_SECTION
		LeaveCriticalSection(&m_impl->m_cs);
#else
		ReleaseMutex(m_impl->m_handle);
#endif
	}
}
#endif