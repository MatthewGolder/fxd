// Creator - MatthewGolder
#include "FXDEngine/Thread/Types.h"

#if IsSystemWindows()
#include "FXDEngine/Thread/AtomicInt.h"
#include "FXDEngine/Core/Win32/Win32.h"

using namespace FXD;
using namespace Thread;

// ------
// AtomicInt
// -
// ------
AtomicInt& AtomicInt::operator=(long rhs)
{
	InterlockedExchange(&m_value, rhs);
	return (*this);
}

long AtomicInt::increment(void)
{
	return InterlockedIncrement(&m_value);
}

long AtomicInt::decrement(void)
{
	return InterlockedDecrement(&m_value);
}

long AtomicInt::add(long v)
{
	return InterlockedExchangeAdd(&m_value, v) + v;
}

long AtomicInt::test_and_set(long& v)
{
	return InterlockedCompareExchange(&v, 1, 0);
}

long AtomicInt::test(long nExchange, long nCompare)
{
	return InterlockedCompareExchange(&m_value, nExchange, nCompare);
}
#endif