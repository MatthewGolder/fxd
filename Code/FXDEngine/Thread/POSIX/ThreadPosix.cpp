// Creator - MatthewGolder
#include "FXDEngine/Thread/Types.h"

#if IsSystemPosix()
#include "FXDEngine/Thread/Thread.h"
#include "FXDEngine/Thread/ThreadLocalStorage.h"
#include "FXDEngine/Thread/ThreadPerformance.h"

#if IsOSPC()
#	include <3rdParty/pthread2/pthread.h>
#elif IsOSAndroid()
#	include <pthread.h>

#	include <unistd.h>
#	include <sys/types.h>
#	include <sys/syscall.h>
#	include <sys/prctl.h>
#endif
#include <atomic>

using namespace FXD;
using namespace Thread;

// ------
// Core::Impl::Impl< IThread >
// -
// ------
template <>
class Core::Impl::Impl< IThread >
{
public:
	friend class IThread;

public:
	Impl(void)
		: m_bRunning(false)
		, m_bPaused(true)
	{}
	~Impl(void)
	{}

	static void* ThreadProc(void* pData)
	{
		// 1. Set up thread
		Thread::IThread* pThread = (Thread::IThread*)pData;
		pThread->m_nThreadID = Thread::Funcs::GetCurrentThreadID();
		pThread->set_thread_name(pThread->m_strName.c_str());
		pThread->set_thread_priority(pThread->m_eThreadPri);
		pThread->impl().m_bRunning = true;

		// 2. Initialise thread data
		if (pThread->m_bManageThreadData)
		{
			Thread::ThreadLocalData::initialise();
			Thread::ThreadPerformance::initialise(pThread->m_strName);
		}

		// 3. Process thread func
		if (!pThread->thread_init_func())
		{
			PRINT_ASSERT << "Thread: thread_init_func() returned false";
		}
		while (pThread->thread_process_func())
		{
			Thread::ThreadPerformance::process_thread_counters();
			//Memory::Log::PrintFrameStats();
			//Memory::Log::ClearFrameStats();
		}
		if (!pThread->thread_shutdown_func())
		{
			PRINT_ASSERT << "Thread: thread_shutdown_func() returned false";
		}

		// 4. Shutdown thread data
		if (pThread->m_bManageThreadData)
		{
			FXD::Thread::ThreadPerformance::shutdown();
			FXD::Thread::ThreadLocalData::shutdown();
		}

		pThread->impl().m_bRunning = false;
		pthread_exit(0);
		return 0;
	}

	pthread_t m_pthread;
	std::atomic< bool > m_bRunning;
	bool m_bPaused;
};

// ------
// IThread::IThread
// -
// ------
IThread::IThread(const FXD::UTF8* pName, bool bManageThreadData, Thread::E_ThreadPriority eThreadPri, FXD::U32 nStackSize)
	: m_nStackSize(nStackSize)
	, m_nThreadID(-1)
	, m_strName(pName)
	, m_eThreadPri(eThreadPri)
	, m_bManageThreadData(bManageThreadData)
{
}

IThread::~IThread(void)
{
	join();
	if (is_valid())
	{
		m_impl->m_pthread = pthread_t();
	}
}

void IThread::start_thread(void)
{
	pthread_attr_t* pAttrPtr = nullptr;
	if (m_nStackSize != 0)
	{
		pthread_attr_t attrStack;
		if (pthread_attr_init(&attrStack) == 0)
		{
			const size_t StackSize = (size_t)m_nStackSize;
			if (pthread_attr_setstacksize(&attrStack, StackSize) == 0)
			{
				pAttrPtr = &attrStack;
			}
		}
	}
	pthread_create(&m_impl->m_pthread, pAttrPtr, Impl::ThreadProc, this);
	if (pAttrPtr != nullptr)
	{
		pthread_attr_destroy(pAttrPtr);
	}
	m_impl->m_bPaused = false;
}

void IThread::pause_thread(void)
{
	if (!is_valid())
	{
		return;
	}

	PRINT_ASSERT << "Thread: Threads cannot be paused on Posix";
	m_impl->m_bPaused = true;
}

void IThread::resume_thread(void)
{
	if (!is_valid())
	{
		return;
	}

	PRINT_ASSERT << "Thread: Threads cannot be redumed on Posix";
	m_impl->m_bPaused = false;
}

bool IThread::is_active(void)
{
	if (m_impl->m_bPaused)
	{
		return false;
	}

	bool bRetVal = m_impl->m_bRunning;
	return bRetVal;
}

bool IThread::is_valid(void)
{
	bool bRetVal = m_impl->m_bRunning;
	return bRetVal;
}

bool IThread::join(void)
{
	bool bRetVal = (pthread_join(m_impl->m_pthread, 0) == 0);
	return bRetVal;
}

void IThread::set_thread_name(const FXD::UTF8* pName)
{
	m_strName = pName;
	Funcs::SetThreadNameByID(m_nThreadID, pName);
}

void IThread::set_thread_priority(Thread::E_ThreadPriority ePriority)
{
	m_eThreadPri = ePriority;
	/*
	sched_param Sched;
	Memory::MemZero(&Sched, sizeof(struct sched_param));

	// Read the current policy
	FXD::S32 Policy = SCHED_RR;
	pthread_getschedparam(impl().m_pthread, &Policy, &Sched);

	// set the priority appropriately
	Sched.sched_priority = Thread::Funcs::TranslateThreadPriority(ePriority);
	pthread_setschedparam(impl().m_pthread, Policy, &Sched);
	*/
}

// ------
// Funcs
// -
// ------
FXD::S32 FXD::Thread::Funcs::GetCurrentThreadID(void)
{
#if IsOSPC()
	pthread_t p = pthread_self();
	return p->id;
#else
	pid_t x = gettid();
	return x;
#endif
}

void FXD::Thread::Funcs::SetCurrentThreadName(const FXD::UTF8* pName)
{
	Thread::Funcs::SetThreadNameByID(Thread::Funcs::GetCurrentThreadID(), pName);
}

void FXD::Thread::Funcs::SetThreadNameByID(unsigned long dwThreadID, const FXD::UTF8* pThreadName)
{
	pthread_setname_np(dwThreadID, pThreadName);
}

void FXD::Thread::Funcs::Sleep(FXD::U32 nMilliseconds)
{
#if IsOSPC()
	::Sleep((DWORD)nMilliseconds);
#else
	::usleep(nMilliseconds * 1000);
#endif
}

FXD::S32 FXD::Thread::Funcs::TranslateThreadPriority(Thread::E_ThreadPriority ePriority)
{
	switch (ePriority)
	{
		// 0 is the Lowest, 31 is the highest possible priority for pthread
		case E_ThreadPriority::Highest:
		{
			return 30;
		}
		case E_ThreadPriority::AboveNormal:
		{
			return 25;
		}
		case E_ThreadPriority::Normal:
		{
			return 15;
		}
		case E_ThreadPriority::BelowNormal:
		{
			return 5;
		}
		case E_ThreadPriority::Lowest:
		{
			return 1;
		}
		case E_ThreadPriority::SlightlyBelowNormal:
		{
			return 14;
		}
		default:
		{
			PRINT_ASSERT << "Thread: Unknown Priority passed to TranslateThreadPriority()";
			return 15;
		}
	}
}

#endif