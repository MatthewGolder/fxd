// Creator - MatthewGolder
#include "FXDEngine/Thread/Types.h"

#if IsSystemPosix()
#include "FXDEngine/Thread/RWLock.h"
#include "FXDEngine/Thread/Thread.h"

#if IsOSPC()
#	include <3rdParty/pthread2/pthread.h>
#elif IsOSAndroid()
#	include <pthread.h>
#endif

using namespace FXD;
using namespace Thread;

// ------
// Core::Impl::Impl< RWLock >
// -
// ------
template <>
class Core::Impl::Impl< RWLock >
{
public:
	friend class RWLock;

public:
	Impl(void)
	{
		pthread_rwlock_init(&m_lock, NULL);
	}
	~Impl(void)
	{
		pthread_rwlock_destroy(&m_lock);
	}

	pthread_rwlock_t m_lock;
};

// ------
// RWLock
// -
// ------
RWLock::RWLock(void)
	: m_nReadLockCount(0)
	, m_nWriteLockCount(0)
	, m_nWriterID(0)
{
}

RWLock::~RWLock(void)
{
	PRINT_COND_ASSERT((m_nReadLockCount == 0), "Thread: Expected zero lock count when shutdown");
	PRINT_COND_ASSERT((m_nWriteLockCount == 0), "Thread: Expected zero lock count when shutdown");
}

void RWLock::acquire_read_lock(void) const
{
	pthread_rwlock_rdlock(&m_impl->m_lock);
	m_nReadLockCount++;
}

void RWLock::acquire_write_lock(void)
{
	const FXD::S32 nCurrentID = Thread::Funcs::GetCurrentThreadID();
	if (m_nWriterID != nCurrentID)
	{
		pthread_rwlock_wrlock(&m_impl->m_lock);
		m_nWriterID = nCurrentID;
	}
	m_nWriteLockCount++;
}

void RWLock::release_read_lock(void) const
{
	PRINT_COND_ASSERT((m_nReadLockCount != 0), "Thread: Expected non zero lock count");

	pthread_rwlock_unlock(&m_impl->m_lock);
	m_nReadLockCount--;
}

void RWLock::release_write_lock(void)
{
	PRINT_COND_ASSERT((m_nWriteLockCount != 0), "Thread: Expected non zero lock count");
	PRINT_COND_ASSERT((m_nWriterID != 0), "Thread: Expected non zero thread ID");

	m_nWriteLockCount--;
	if (m_nWriteLockCount == 0)
	{
		m_nWriterID = 0;
		pthread_rwlock_unlock(&m_impl->m_lock);
	}
}
#endif