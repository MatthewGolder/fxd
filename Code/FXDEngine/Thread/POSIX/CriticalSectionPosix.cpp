// Creator - MatthewGolder
#include "FXDEngine/Thread/Types.h"

#if IsSystemPosix()
#include "FXDEngine/Thread/CriticalSection.h"
#include "FXDEngine/Thread/Thread.h"

#if IsOSPC()
#	include "3rdParty/pthread2/pthread.h"
#elif IsOSAndroid()
#	include <pthread.h>
#endif

using namespace FXD;
using namespace Thread;

// ------
// Core::Impl::Impl< CSLock >
// -
// ------
template <>
class Core::Impl::Impl< CSLock >
{
public:
	friend class CSLock;

public:
	Impl(void)
	{
		pthread_mutexattr_t attr;
		pthread_mutexattr_init(&attr);
		pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
		pthread_mutex_init(&m_mutex, &attr);
	}
	~Impl(void)
	{
		pthread_mutex_destroy(&m_mutex);
	}

	pthread_mutex_t m_mutex;
};

// ------
// CSLock
// -
// ------
CSLock::CSLock(void)
	: m_nOwnerID(-1)
	, m_nLockCount(0)
{
}

CSLock::~CSLock(void)
{
	PRINT_COND_ASSERT((m_nLockCount == 0), "Thread: Expected zero lock count when shutdown");
}

void CSLock::acquire_lock(void)
{
	//Core::String8 str = Thread::Funcs::GetCurrentThreadID();
	FXD::S32 nCurrentID = Thread::Funcs::GetCurrentThreadID();
	if (m_nOwnerID != nCurrentID)
	{
		pthread_mutex_lock(&m_impl->m_mutex);
		m_nOwnerID = nCurrentID;
	}
	m_nLockCount++;
}

void CSLock::release_lock(void)
{
	PRINT_COND_ASSERT((m_nLockCount != 0), "Thread: Expected non zero lock count");
	PRINT_COND_ASSERT((m_nOwnerID != -1), "Thread: Expected non zero thread ID");

	m_nLockCount--;
	if (m_nLockCount == 0)
	{
		m_nOwnerID = -1;
		pthread_mutex_unlock(&m_impl->m_mutex);
	}
}
#endif