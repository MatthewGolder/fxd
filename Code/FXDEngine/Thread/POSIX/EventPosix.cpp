// Creator - MatthewGolder
#include "FXDEngine/Thread/Types.h"

#if IsSystemPosix()
#include "FXDEngine/Thread/Event.h"

#if IsOSPC()
#	include <3rdParty/pthread2/pthread.h>
#elif IsOSAndroid()
#	include <pthread.h>
#endif

using namespace FXD;
using namespace Thread;

// ------
// Core::Impl::Impl< Event >
// -
// ------
template <>
class Core::Impl::Impl< Event >
{
public:
	friend class Event;

public:
	Impl(bool bAutoReset)
		: m_bSignaled(false)
		, m_bAutoReset(bAutoReset)
	{
		pthread_mutex_init(&m_mutex, NULL);
		pthread_cond_init(&m_cond, NULL);
	}
	~Impl(void)
	{
		pthread_mutex_destroy(&m_mutex);
		pthread_cond_destroy(&m_cond);
	}

	bool m_bSignaled;
	bool m_bAutoReset;
	pthread_cond_t m_cond;
	pthread_mutex_t m_mutex;
};

// ------
// Event
// -
// ------
Event::Event(bool bAutoReset)
	: HasImpl(Core::_Pimpl_Pack(), bAutoReset)
{}

Event::~Event(void)
{}

bool Event::wait_for(FXD::U32 nTimeOutMS) const
{
	pthread_mutex_lock(&m_impl->m_mutex);
	while (!m_impl->m_bSignaled)
	{
		timespec ts;
#if IsOSPC()
		timespec_get(&ts, TIME_UTC);
#else
		clock_gettime(CLOCK_REALTIME, &ts);
#endif
		ts.tv_sec += (nTimeOutMS / 1000);
		ts.tv_nsec += ((nTimeOutMS % 1000) * 1000000LL);
		if (ts.tv_nsec > 1000000000LL)
		{
			ts.tv_sec += 1;
			ts.tv_nsec -= 1000000000LL;
		}
		pthread_cond_timedwait(&m_impl->m_cond, &m_impl->m_mutex, &ts);
		break;
	}

	bool bRet = m_impl->m_bSignaled;
	if (m_impl->m_bAutoReset)
	{
		m_impl->m_bSignaled = false;
	}
	pthread_mutex_unlock(&m_impl->m_mutex);
	return bRet;
}

void Event::set(void)
{
	pthread_mutex_lock(&m_impl->m_mutex);
	if (!m_impl->m_bSignaled)
	{
		m_impl->m_bSignaled = true;
		pthread_cond_signal(&m_impl->m_cond);
	}
	pthread_mutex_unlock(&m_impl->m_mutex);
}

void Event::reset(void)
{
	pthread_mutex_lock(&m_impl->m_mutex);
	m_impl->m_bSignaled = false;
	pthread_mutex_unlock(&m_impl->m_mutex);
}
#endif