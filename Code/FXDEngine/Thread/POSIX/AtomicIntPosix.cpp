// Creator - MatthewGolder
#include "FXDEngine/Thread/Types.h"

#if IsSystemPosix()
#include "FXDEngine/Thread/AtomicInt.h"

#if IsOSPC()
#	include <3rdParty/pthread2/pthread.h>
#elif IsOSAndroid()
#	include <pthread.h>
#endif

using namespace FXD;
using namespace Thread;

// ------
// AtomicInt
// -
// ------
AtomicInt& AtomicInt::operator=(long rhs)
{
#if IsOSPC()
	InterlockedExchange(&m_value, rhs);
#else
	m_value = rhs;
#endif
	return (*this);
}

long AtomicInt::increment(void)
{
#if IsOSPC()
	return InterlockedIncrement(&m_value);
#else
	return __sync_fetch_and_add(&m_value, 1) + 1;
#endif
}

long AtomicInt::decrement(void)
{
#if IsOSPC()
	return InterlockedDecrement(&m_value);
#else
	return __sync_fetch_and_sub(&m_value, 1) - 1;
#endif
}

long AtomicInt::add(long v)
{
#if IsOSPC()
	return InterlockedExchangeAdd(&m_value, v) + v;
#else
	return __sync_fetch_and_add(&m_value, v) + v;
#endif
}

long AtomicInt::test_and_set(long& v)
{
#if IsOSPC()
	return InterlockedCompareExchange(&v, 1, 0);
#else
	return __sync_val_compare_and_swap(&v, 0, 1);
#endif
}

long AtomicInt::test(long nExchange, long nCompare)
{
#if IsOSPC()
	return InterlockedCompareExchange(&m_value, nExchange, nCompare);
#else
	return __sync_val_compare_and_swap(&m_value, nExchange, nCompare);
#endif
}
#endif