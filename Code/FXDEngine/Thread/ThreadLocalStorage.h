// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_THREAD_THREADLOCALSTORAGE_H
#define FXDENGINE_THREAD_THREADLOCALSTORAGE_H

#include "FXDEngine/Thread/Types.h"
#include "FXDEngine/Core/CompileTimeChecks.h"
#include "FXDEngine/Core/NonCopyable.h"

namespace FXD
{
	namespace Thread
	{
		// ------
		// ThreadLocalData
		// -
		// ------
		class ThreadLocalData
		{
		public:
			ThreadLocalData(void);
			~ThreadLocalData(void);

			static bool initialise(void);
			static void shutdown(void);
			static bool is_initialised(void);

		public:
			void* m_pData;
		};
		
		// ------
		// ThreadLocalBase
		// -
		// ------
		class ThreadLocalBase
		{
		public:
			ThreadLocalBase(FXD::U32 nSize, FXD::U32 nAlign);
			~ThreadLocalBase(void);

			void* get_data(const ThreadLocalData& tld) const;

		public:
#if defined(__APPLE__)
			pthread_key_t m_key;
#endif
			FXD::U32 m_nOffset;
		};
		
		// ------
		// ThreadLocal
		// -
		// ------
		template < typename T >
		class ThreadLocal FINAL : public ThreadLocalBase, public Core::NonCopyable
		{
		public:
			ThreadLocal(void)
				: ThreadLocalBase(sizeof(T), FXD_ALIGN_OF(T))
			{}
			~ThreadLocal(void)
			{}

			T& get(const ThreadLocalData& tld)
			{
				return *(T*)get_data(tld);
			}
			const T& get(const ThreadLocalData& tld) const
			{
				return *(T*)get_data(tld);
			}
			void set(const ThreadLocalData& tld, const T& t)
			{
				*(T*)get_data(tld) = t;
			}
			T& operator*()
			{
				return get(ThreadLocalData());
			}
			const T& operator*() const
			{
				return get(ThreadLocalData());
			}
			T& operator->()
			{
				return get(ThreadLocalData());
			}
			const T& operator->() const
			{
				return get(ThreadLocalData());
			}
		};

	} //namespace Thread
} //namespace FXD
#endif //FXDENGINE_THREAD_THREADLOCALSTORAGE_H