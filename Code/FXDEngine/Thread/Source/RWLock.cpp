// Creator - MatthewGolder
#include "FXDEngine/Thread/RWLock.h"

using namespace FXD;
using namespace Thread;

// ------
// RWLock::ScopedReadLock
// -
// ------
RWLock::ScopedReadLock::ScopedReadLock(const RWLock& lockable)
	: m_lockable(lockable)
{
	m_lockable.acquire_read_lock();
}
RWLock::ScopedReadLock::~ScopedReadLock(void)
{
	m_lockable.release_read_lock();
}

// ------
// RWLock::ScopedWriteLock
// -
// ------
RWLock::ScopedWriteLock::ScopedWriteLock(RWLock& lockable)
	: m_lockable(lockable)
{
	m_lockable.acquire_write_lock();
}
RWLock::ScopedWriteLock::~ScopedWriteLock(void)
{
	m_lockable.release_write_lock();
}