// Creator - MatthewGolder
#include "FXDEngine/Thread/Event.h"
#include "FXDEngine/Math/Limits.h"

using namespace FXD;
using namespace Thread;

// ------
// Event
// -
// ------
bool Event::wait(void) const
{
	bool bRet = wait_for(Math::NumericLimits< FXD::U32 >::max());
	return bRet;
}