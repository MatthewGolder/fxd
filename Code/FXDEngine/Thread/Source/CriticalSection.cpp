// Creator - MatthewGolder
#include "FXDEngine/Thread/CriticalSection.h"
#include "FXDEngine/Core/Debugger.h"

using namespace FXD;
using namespace Thread;

// ------
// CSLock::LockGuard
// -
// ------
CSLock::LockGuard::LockGuard(CSLock& cs)
	: m_cs(cs)
#if defined(FXD_DEBUG)
	, m_bLocked(false)
#endif
{
#if defined(FXD_DEBUG)
	PRINT_COND_ASSERT(!m_bLocked, "Thread: This critical section is already locked");
	m_bLocked = true;
#endif
	m_cs.acquire_lock();
}

CSLock::LockGuard::~LockGuard(void)
{
#if defined(FXD_DEBUG)
	PRINT_COND_ASSERT(m_bLocked, "Thread: This critical section is not locked and cannot be unlocked");
	m_bLocked = false;
#endif
	m_cs.release_lock();
}