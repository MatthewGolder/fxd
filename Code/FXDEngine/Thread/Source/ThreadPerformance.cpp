// Creator - MatthewGolder
#include "FXDEngine/Thread/ThreadPerformance.h"
#include "FXDEngine/Thread/ThreadLocalStorage.h"
#include "FXDEngine/Core/Timer.h"
#include "FXDEngine/Thread/CriticalSection.h"

using namespace FXD;
using namespace Thread;

// ------
// ThreadPerfCounters
// -
// ------
class ThreadPerfCounters
{
public:
	ThreadPerfCounters(const Core::String8& strThreadName)
		: m_strThreadName(strThreadName)
	{}
	~ThreadPerfCounters(void)
	{}

	void process(bool bPrint)
	{
		if (bPrint)
		{
			FXD::F32 dt = m_timer.elapsed_time();
			PRINT_INFO << "Thread: " << m_strThreadName << " - " << dt;
		}
	}

public:
	Core::Timer m_timer;
	Core::String8 m_strThreadName;
};


// ------
// ThreadPerformance
// -
// ------
#if defined(FXD_ENABLE_THREAD_PERFORMANCE)
static Thread::ThreadLocal< ThreadPerfCounters* > g_pThreadPerfCounters;
static CSLock g_threadPerfCS;
#endif

ThreadPerformance::ThreadPerformance(void)
{
}

ThreadPerformance::~ThreadPerformance(void)
{
}

void ThreadPerformance::initialise(const Core::String8& strThreadName)
{
#if defined(FXD_ENABLE_THREAD_PERFORMANCE)
	CSLock::LockGuard lock(g_threadPerfCS);
	PRINT_COND_ASSERT(((*g_pThreadPerfCounters) == nullptr), "Thread: Performance counters already initialised");

	ThreadPerfCounters* pPerf = FXD_NEW(ThreadPerfCounters)(strThreadName);
	(*g_pThreadPerfCounters) = pPerf;
#endif
}

void ThreadPerformance::shutdown(void)
{
#if defined(FXD_ENABLE_THREAD_PERFORMANCE)
	CSLock::LockGuard lock(g_threadPerfCS);
	PRINT_COND_ASSERT(((*g_pThreadPerfCounters) != nullptr), "Thread: Performance counters already initialised");

	FXD_DELETE((*g_pThreadPerfCounters));
	(*g_pThreadPerfCounters) = nullptr;
#endif
}

bool ThreadPerformance::is_initialised(void)
{
#if defined(FXD_ENABLE_THREAD_PERFORMANCE)
	CSLock::LockGuard lock(g_threadPerfCS);

	bool bInit = ((*g_pThreadPerfCounters) != nullptr);
	return bInit;
#else
	return true;
#endif
}

void ThreadPerformance::process_thread_counters(bool bPrint)
{
#if defined(FXD_ENABLE_THREAD_PERFORMANCE)
	if (is_initialised())
	{
		(*g_pThreadPerfCounters)->process(bPrint);
	}
#endif
}