// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_BITSET_H
#define FXDENGINE_CONTAINER_BITSET_H

#include "FXDEngine/Core/String.h"

namespace FXD
{
	namespace Container
	{
		// ------
		// Bitset
		// -
		// ------
		template < size_t BitCount >
		class Bitset
		{
		public:
			// ------
			// Reference
			// -
			// ------
			class Reference
			{
			public:
				friend Bitset< BitCount >;

			private:
				Reference(void) NOEXCEPT
					: m_pBitset(nullptr)
					, m_nPos(0)
				{}

				Reference(Bitset< BitCount >& rhs, size_t nPos)
					: m_pBitset(&rhs)
					, m_nPos(nPos)
				{}

			public:
				~Reference(void) NOEXCEPT
				{}

				Reference& operator=(bool bVal) NOEXCEPT
				{
					m_pBitset->set(m_nPos, bVal);
					return (*this);
				}

				Reference& operator=(const Reference& ref) NOEXCEPT
				{
					m_pBitset->set(m_nPos, bool(ref));
					return (*this);
				}

				Reference& flip(void) NOEXCEPT
				{
					m_pBitset->flip(m_nPos);
					return (*this);
				}

				bool operator~(void) const NOEXCEPT
				{
					return (!m_pBitset->test(m_nPos));
				}

				operator bool(void) const NOEXCEPT
				{
					return (m_pBitset->test(m_nPos));
				}

			private:
				Bitset< BitCount >* m_pBitset;
				size_t m_nPos;
			};

		public:
			using my_type = Bitset< BitCount >;
			using value_type = typename FXD::STD::conditional< BitCount <= sizeof(unsigned long) * CHAR_BIT, unsigned long, unsigned long long >::type;
			using reference = Reference;
			using difference_type = std::ptrdiff_t;
			static CONSTEXPR bool _NeedMask = BitCount < CHAR_BIT * sizeof(unsigned long long);
			static CONSTEXPR unsigned long long _Mask = (1ULL << (_NeedMask ? BitCount : 0)) - 1ULL;

		private:
			enum : difference_type
			{
				_Bitsperword = (difference_type)(CHAR_BIT * sizeof(value_type)),
				_Words = (difference_type)(BitCount == 0 ? 0 : (BitCount - 1) / _Bitsperword)
			};

		public:
			CONSTEXPR Bitset(void) NOEXCEPT;
			CONSTEXPR Bitset(unsigned long long nVal) NOEXCEPT;
			template < class charT, class traits >
			EXPLICIT Bitset(const Core::StringBase< charT, traits >& str, typename Core::StringBase< charT, traits >::size_type nPos = 0, typename Core::StringBase< charT, traits >::size_type nCount = Core::StringBase< charT, traits >::npos, charT ch0 = (charT)'0', charT ch1 = (charT)'1');
			template < class charT >
			EXPLICIT Bitset(const charT* pPtr, typename Core::StringBase< charT >::size_type nCount = Core::StringBase< charT >::npos, charT ch0 = (charT)'0', charT ch1 = (charT)'1');

			bool operator==(const my_type& rhs) const NOEXCEPT;
			bool operator!=(const my_type& rhs) const NOEXCEPT;
			my_type operator~(void) const NOEXCEPT;
			my_type operator<<(size_t nPos) const NOEXCEPT;
			my_type operator>>(size_t nPos) const NOEXCEPT;
			my_type& operator<<=(size_t nPos) NOEXCEPT;
			my_type& operator>>=(size_t nPos) NOEXCEPT;
			my_type& operator&=(const my_type& rhs) NOEXCEPT;
			my_type& operator|=(const my_type& rhs) NOEXCEPT;
			my_type& operator^=(const my_type& rhs) NOEXCEPT;

			// Access
			CONSTEXPR bool operator[](size_t nPos) const
			{
				return (BitCount <= nPos ? (_validate(nPos), false) : _subscript(nPos));
			}

			reference operator[](size_t nPos)
			{
				_validate(nPos);
				return (reference(*this, nPos));
			}

			bool test(size_t nPos) const;

			// Assign
			my_type& set(void) NOEXCEPT;
			my_type& set(size_t nPos, bool bVal = true);
			my_type& reset(void) NOEXCEPT;
			my_type& reset(size_t nPos);
			my_type& flip(void) NOEXCEPT;
			my_type& flip(size_t nPos);

			unsigned long to_ulong(void) const;
			unsigned long long to_ullong(void) const;
			template < class charT = FXD::UTF8, class traits = Core::CharTraits< charT > >
			Core::StringBase< charT, traits > to_string(charT ch0 = (charT)'0', charT ch1 = (charT)'1') const;

			// Searches
			bool any(void) const NOEXCEPT;
			bool none(void) const NOEXCEPT;
			bool all(void) const NOEXCEPT;
			size_t count(void) const NOEXCEPT;
			CONSTEXPR size_t size(void) const NOEXCEPT;

		private:

			template < class charT, class traits >
			void _construct(const Core::StringBase< charT, traits >& str, typename Core::String8::size_type nPos, typename Core::String8::size_type nCount, charT ch0, charT ch1);

			void _tidy(value_type nVal = 0);
			void _trim(void);
			void _trim_if(FXD::STD::true_type);
			void _trim_if(FXD::STD::false_type);
			CONSTEXPR bool _subscript(size_t nPos) const;
			value_type _get_word(size_t nPos) const;
			static void _validate(size_t nPos)
			{
				PRINT_COND_ASSERT(nPos < BitCount, "Bitset index outside range");
			}

		private:
			value_type m_array[_Words + 1];	// the set of bits
		};


		template < size_t BitCount >
		CONSTEXPR Bitset< BitCount >::Bitset(void) NOEXCEPT
			: m_array()
		{
		}
		template < size_t BitCount >
		CONSTEXPR Bitset< BitCount >::Bitset(unsigned long long nVal) NOEXCEPT
			: m_array{ static_cast< value_type >(_NeedMask ? nVal & _Mask : nVal) }
		{
		}
		template < size_t BitCount >
		template < class charT, class traits >
		Bitset< BitCount >::Bitset(const Core::StringBase< charT, traits >& str, typename Core::StringBase< charT, traits >::size_type nPos, typename Core::StringBase< charT, traits >::size_type nCount, charT ch0, charT ch1)
		{
			_construct(str, nPos, nCount, ch0, ch1);
		}
		template < size_t BitCount >
		template < class charT >
		Bitset< BitCount >::Bitset(const charT* pPtr, typename Core::StringBase< charT >::size_type nCount, charT ch0, charT ch1)
		{
			_construct(nCount == Core::StringBase< charT >::npos
				? Core::StringBase< charT >(pPtr)
				: Core::StringBase< charT >(pPtr, nCount), 0, nCount, ch0, ch1);
		}

		template < size_t BitCount >
		bool Bitset< BitCount >::operator==(const Bitset& rhs) const NOEXCEPT
		{
			for (difference_type nWPos = _Words; 0 <= nWPos; --nWPos)
			{
				if (m_array[nWPos] != rhs._get_word(static_cast<size_t >(nWPos)))
				{
					return false;
				}
			}
			return true;
		}
		template < size_t BitCount >
		bool Bitset< BitCount >::operator!=(const Bitset& rhs) const NOEXCEPT
		{
			return (!(*this == rhs));
		}
		template < size_t BitCount >
		typename Bitset < BitCount >::my_type Bitset< BitCount >::operator~(void) const NOEXCEPT
		{
			return (Bitset(*this).flip());
		}
		template < size_t BitCount >
		typename Bitset < BitCount >::my_type Bitset< BitCount >::operator<<(size_t nPos) const NOEXCEPT
		{
			return (Bitset(*this) <<= nPos);
		}
		template < size_t BitCount >
		typename Bitset < BitCount >::my_type Bitset< BitCount >::operator>>(size_t nPos) const NOEXCEPT
		{
			return (Bitset(*this) >>= nPos);
		}
		template < size_t BitCount >
		typename Bitset < BitCount >::my_type& Bitset< BitCount >::operator<<=(size_t nPos) NOEXCEPT
		{
			const difference_type nWordshift = (difference_type)(nPos / _Bitsperword);
			if (nWordshift != 0)
			{
				for (difference_type nWPos = _Words; 0 <= nWPos; --nWPos)
				{
					m_array[nWPos] = nWordshift <= nWPos ? m_array[nWPos - nWordshift] : (value_type)0;
				}
			}

			if ((nPos %= _Bitsperword) != 0)
			{
				for (difference_type nWPos = _Words; 0 < nWPos; --nWPos)
				{
					m_array[nWPos] = (value_type)((m_array[nWPos] << nPos) | (m_array[nWPos - 1] >> (_Bitsperword - nPos)));
				}
				m_array[0] <<= nPos;
			}
			_trim();
			return (*this);
		}
		template < size_t BitCount >
		typename Bitset < BitCount >::my_type& Bitset< BitCount >::operator>>=(size_t nPos) NOEXCEPT
		{
			const difference_type nWordshift = (difference_type)(nPos / _Bitsperword);
			if (nWordshift != 0)
			{
				for (difference_type nWPos = 0; nWPos <= _Words; ++nWPos)
				{
					m_array[nWPos] = nWordshift <= _Words - nWPos ? m_array[nWPos + nWordshift] : (value_type)0;
				}
			}

			if ((nPos %= _Bitsperword) != 0)
			{
				for (difference_type nWPos = 0; nWPos < _Words; ++nWPos)
				{
					m_array[nWPos] = (value_type)((m_array[nWPos] >> nPos) | (m_array[nWPos + 1] << (_Bitsperword - nPos)));
				}
				m_array[_Words] >>= nPos;
			}
			return (*this);
		}
		template < size_t BitCount >
		typename Bitset < BitCount >::my_type& Bitset< BitCount >::operator&=(const Bitset& rhs) NOEXCEPT
		{
			for (difference_type nWPos = _Words; 0 <= nWPos; --nWPos)
			{
				m_array[nWPos] &= rhs._get_word(static_cast<size_t >(nWPos));
			}
			return (*this);
		}
		template < size_t BitCount >
		typename Bitset < BitCount >::my_type& Bitset< BitCount >::operator|=(const Bitset& rhs) NOEXCEPT
		{
			for (difference_type nWPos = _Words; 0 <= nWPos; --nWPos)
			{
				m_array[nWPos] |= rhs._get_word(static_cast<size_t >(nWPos));
			}
			return (*this);
		}
		template < size_t BitCount >
		typename Bitset < BitCount >::my_type& Bitset< BitCount >::operator^=(const Bitset& rhs) NOEXCEPT
		{
			for (difference_type nWPos = _Words; 0 <= nWPos; --nWPos)
			{
				m_array[nWPos] ^= rhs._get_word(static_cast<size_t >(nWPos));
			}
			return (*this);
		}

		// Access
		template < size_t BitCount >
		bool Bitset< BitCount >::test(size_t nPos) const
		{
			_validate(nPos);
			return (_subscript(nPos));
		}

		// Assign
		template < size_t BitCount >
		typename Bitset < BitCount >::my_type& Bitset< BitCount >::set(void) NOEXCEPT
		{
			_tidy((value_type)~0);
			return (*this);
		}
		template < size_t BitCount >
		typename Bitset < BitCount >::my_type& Bitset< BitCount >::set(size_t nPos, bool bVal)
		{
			_validate(nPos);
			if (bVal)
			{
				m_array[nPos / _Bitsperword] |= (value_type)1 << nPos % _Bitsperword;
			}
			else
			{
				m_array[nPos / _Bitsperword] &= ~((value_type)1 << nPos % _Bitsperword);
			}
			return (*this);
		}
		template < size_t BitCount >
		typename Bitset < BitCount >::my_type& Bitset< BitCount >::reset(void) NOEXCEPT
		{
			_tidy();
			return (*this);
		}
		template < size_t BitCount >
		typename Bitset < BitCount >::my_type& Bitset< BitCount >::reset(size_t nPos)
		{
			return (set(nPos, false));
		}
		template < size_t BitCount >
		typename Bitset < BitCount >::my_type& Bitset< BitCount >::flip(void) NOEXCEPT
		{
			for (difference_type nPos = _Words; 0 <= nPos; --nPos)
			{
				m_array[nPos] = (value_type)~m_array[nPos];
			}

			_trim();
			return (*this);
		}
		template < size_t BitCount >
		typename Bitset < BitCount >::my_type& Bitset< BitCount >::flip(size_t nPos)
		{
			_validate(nPos);
			m_array[nPos / _Bitsperword] ^= (value_type)1 << nPos % _Bitsperword;
			return (*this);
		}

		template < size_t BitCount >
		unsigned long Bitset< BitCount >::to_ulong(void) const
		{
			unsigned long long nVal = to_ullong();
			unsigned long nAns = (unsigned long)nVal;
			if (nAns != nVal)
			{
				PRINT_ASSERT << "converted value too big to represent";
			}
			return (nAns);
		}
		template < size_t BitCount >
		unsigned long long Bitset< BitCount >::to_ullong(void) const
		{
			CTC_ASSERT((sizeof(unsigned long long) % sizeof(value_type) == 0), "unsigned long long not multiple of value_type");

			difference_type nWPos = _Words;
			for (; (difference_type)(sizeof(unsigned long long) / sizeof(value_type)) <= nWPos; --nWPos)
			{
				if (m_array[nWPos] != 0)
				{
					PRINT_ASSERT << "converted value too big to represent";
				}
			}

			unsigned long long nVal = m_array[nWPos];
			while (0 <= --nWPos)
			{
				nVal = ((nVal << (_Bitsperword - 1)) << 1) | m_array[nWPos];
			}

			return (nVal);
		}
		template < size_t BitCount >
		template < class charT, class traits >
		Core::StringBase< charT, traits > Bitset< BitCount >::to_string(charT ch0, charT ch1) const
		{
			typename Core::StringBase< charT, traits >::size_type nPos;

			Core::StringBase< charT, traits > str;
			str.reserve(BitCount);

			for (nPos = BitCount; 0 < nPos; )
			{
				if (test(--nPos))
				{
					str += ch1;
				}
				else
				{
					str += ch0;
				}
			}
			return str;
		}

		// Searches
		template < size_t BitCount >
		CONSTEXPR size_t Bitset< BitCount >::size(void) const NOEXCEPT
		{
			return (BitCount);
		}
		template < size_t BitCount >
		bool Bitset< BitCount >::any(void) const NOEXCEPT
		{
			for (difference_type nWPos = _Words; 0 <= nWPos; --nWPos)
			{
				if (m_array[nWPos] != 0)
				{
					return true;
				}
			}
			return false;
		}
		template < size_t BitCount >
		bool Bitset< BitCount >::none(void) const NOEXCEPT
		{
			return (!any());
		}
		template < size_t BitCount >
		bool Bitset< BitCount >::all(void) const NOEXCEPT
		{
			return (count() == size());
		}
		template < size_t BitCount >
		size_t Bitset< BitCount >::count(void) const NOEXCEPT
		{
			const FXD::UTF8* const pBitsperbyte =
				"\0\1\1\2\1\2\2\3\1\2\2\3\2\3\3\4"
				"\1\2\2\3\2\3\3\4\2\3\3\4\3\4\4\5"
				"\1\2\2\3\2\3\3\4\2\3\3\4\3\4\4\5"
				"\2\3\3\4\3\4\4\5\3\4\4\5\4\5\5\6"
				"\1\2\2\3\2\3\3\4\2\3\3\4\3\4\4\5"
				"\2\3\3\4\3\4\4\5\3\4\4\5\4\5\5\6"
				"\2\3\3\4\3\4\4\5\3\4\4\5\4\5\5\6"
				"\3\4\4\5\4\5\5\6\4\5\5\6\5\6\6\7"
				"\1\2\2\3\2\3\3\4\2\3\3\4\3\4\4\5"
				"\2\3\3\4\3\4\4\5\3\4\4\5\4\5\5\6"
				"\2\3\3\4\3\4\4\5\3\4\4\5\4\5\5\6"
				"\3\4\4\5\4\5\5\6\4\5\5\6\5\6\6\7"
				"\2\3\3\4\3\4\4\5\3\4\4\5\4\5\5\6"
				"\3\4\4\5\4\5\5\6\4\5\5\6\5\6\6\7"
				"\3\4\4\5\4\5\5\6\4\5\5\6\5\6\6\7"
				"\4\5\5\6\5\6\6\7\5\6\6\7\6\7\7\x8";

			const FXD::U8* pPtr = &reinterpret_cast< const FXD::U8& >(m_array);
			const FXD::U8* const pEnd = (pPtr + sizeof(m_array));
			size_t nVal = 0;
			for (; pPtr != pEnd; ++pPtr)
			{
				nVal += pBitsperbyte[*pPtr];
			}
			return (nVal);
		}

		template < size_t BitCount >
		template < class charT, class traits >
		void Bitset< BitCount >::_construct(const Core::StringBase< charT, traits >& str, typename Core::String8::size_type nPos, typename Core::String8::size_type nCount, charT ch0, charT ch1)
		{
			if (str.size() < nPos)
			{
				PRINT_ASSERT << "Bitset index outside range";

			}
			if (str.size() - nPos < nCount)
			{
				nCount = str.size() - nPos;
			}

			typename Core::StringBase< charT, traits >::size_type nNum;
			for (nNum = 0; nNum < nCount; ++nNum)
			{
				if (!traits::eq(str[nPos + nNum], ch0) && !traits::eq(str[nPos + nNum], ch1))
				{
					PRINT_ASSERT << "invalid bitset<N> char";
				}
			}

			if (BitCount < nCount)
			{
				nCount = BitCount;
			}
			_tidy();

			for (nPos += nCount, nNum = 0; nNum < nCount; ++nNum)
			{
				if (traits::eq(str[--nPos], ch1))
				{
					set(nNum);
				}
			}
		}

		template < size_t BitCount >
		void Bitset< BitCount >::_tidy(value_type nVal)
		{
			for (difference_type nWPos = _Words; 0 <= nWPos; --nWPos)
			{
				m_array[nWPos] = nVal;
			}
			if (nVal != 0)
			{
				_trim();
			}
		}

		template < size_t BitCount >
		void Bitset< BitCount >::_trim(void)
		{
			_trim_if(FXD::STD::integral_constant< bool, BitCount == 0 || BitCount % _Bitsperword != 0 >{});
		}
		template < size_t BitCount >
		void Bitset< BitCount >::_trim_if(FXD::STD::true_type)
		{
			m_array[_Words] &= ((value_type)1 << BitCount % _Bitsperword) - 1;
		}
		template < size_t BitCount >
		void Bitset< BitCount >::_trim_if(FXD::STD::false_type)
		{
		}
		template < size_t BitCount >
		CONSTEXPR bool Bitset< BitCount >::_subscript(size_t nPos) const
		{
			return ((m_array[nPos / _Bitsperword] & ((value_type)1 << nPos % _Bitsperword)) != 0);
		}
		template < size_t BitCount >
		typename Bitset< BitCount >::value_type Bitset< BitCount >::_get_word(size_t nPos) const
		{
			return (m_array[nPos]);
		}

		// Non-member functions
		template < size_t BitCount >
		inline Bitset< BitCount > operator&(const Bitset< BitCount >& lhs, const Bitset< BitCount >& rhs) NOEXCEPT
		{
			Bitset< BitCount > ret = lhs;
			return (ret &= rhs);
		}

		template < size_t BitCount >
		inline Bitset< BitCount > operator|(const Bitset< BitCount >& lhs, const Bitset< BitCount >& rhs) NOEXCEPT
		{
			Bitset< BitCount > ret = lhs;
			return (ret |= rhs);
		}

		template < size_t BitCount >
		inline Bitset< BitCount > operator^(const Bitset< BitCount >& lhs, const Bitset< BitCount >& rhs) NOEXCEPT
		{
			Bitset< BitCount > ret = lhs;
			return (ret ^= rhs);
		}

	} //namespace Container
} //namespace FXD
#endif //FXDENGINE_CONTAINER_BITSET_H