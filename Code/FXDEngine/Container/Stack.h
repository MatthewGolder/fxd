// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_STACK_H
#define FXDENGINE_CONTAINER_STACK_H

#include "FXDEngine/Container/Deque.h"
#include "FXDEngine/Core/Internal/IsNoThrowSwappable.h"

namespace FXD
{
	namespace Container
	{
		// ------
		// Stack
		// - LIFO queue.
		// ------
		template <
			typename T,
			typename Container = Container::Deque< T > >
		class Stack
		{
		public:
			using my_type = Stack< T, Container >;
			using container_type = Container;
			using value_type = typename Container::value_type;
			using reference = typename Container::reference;
			using const_reference = typename Container::const_reference;
			using size_type = typename Container::size_type;

		public:
			Stack(void);
			Stack(size_type nSize);
			Stack(std::initializer_list< value_type > iList);
			~Stack(void);

			Stack(const my_type& rhs);
			Stack(my_type&& rhs);
			EXPLICIT Stack(const container_type& rhs);
			EXPLICIT Stack(container_type&& rhs);

			// Insert
			reference top(void);
			const_reference top(void) const;

			void push(const value_type& val);
			void push(value_type&& rhs);

			template < class... Args >
			reference emplace(Args&&... args);

			// Erase
			void pop(void);
			void clear(void);

			// Adjust
			void swap(my_type& rhs) NOEXCEPT_IF(FXD::STD::is_nothrow_swappable< my_type::container_type >::value);

			// Searches
			bool contains(const value_type& key) const;
			const bool empty(void) const NOEXCEPT;
			const bool not_empty(void) const NOEXCEPT;
			const size_type size(void) const NOEXCEPT;
			const size_type count(const value_type& key) const NOEXCEPT;

		protected:
			container_type m_container;
		};

		// ------
		// Stack
		// -
		// ------
		template < typename T, typename Container >
		inline Stack< T, Container >::Stack(void)
			: Stack(0)
		{}

		template < typename T, typename Container >
		inline Stack< T, Container >::Stack(size_type nSize)
			: m_container(nSize)
		{}

		template < typename T, typename Container >
		inline Stack< T, Container >::Stack(std::initializer_list< value_type > iList)
			: m_container(iList)
		{}

		template < typename T, typename Container >
		inline Stack< T, Container >::~Stack(void)
		{}

		template < typename T, typename Container >
		inline Stack< T, Container >::Stack(const my_type& rhs)
			: m_container(rhs.m_container)
		{}

		template < typename T, typename Container >
		inline Stack< T, Container >::Stack(my_type&& rhs)
			: m_container(std::move(rhs.m_container))
		{}

		template < typename T, typename Container >
		inline Stack< T, Container >::Stack(const container_type& rhs)
			: m_container(rhs)
		{}

		template < typename T, typename Container >
		inline Stack< T, Container >::Stack(container_type&& rhs)
			: m_container(std::move(rhs))
		{}

		// Insert
		template < typename T, typename Container >
		inline typename Stack< T, Container >::reference Stack< T, Container >::top(void)
		{
			return m_container.back();
		}
		template < typename T, typename Container >
		inline typename Stack< T, Container >::const_reference Stack< T, Container >::top(void) const
		{
			return m_container.back();
		}

		template < typename T, typename Container >
		void Stack< T, Container >::push(const value_type& val)
		{
			m_container.push_back(val);
		}
		template < typename T, typename Container >
		void Stack< T, Container >::push(value_type&& val)
		{
			m_container.push_back(std::move(val));
		}

		template < typename T, typename Container >
		template < class... Args >
		typename Stack< T, Container >::reference Stack< T, Container >::emplace(Args&&... args)
		{
			return m_container.emplace_back(std::forward< Args >(args)...);
		}

		// Erase
		template < typename T, typename Container >
		void Stack< T, Container >::pop(void)
		{
			m_container.pop_back();
		}

		template < typename T, typename Container >
		void Stack< T, Container >::clear(void)
		{
			m_container.clear();
		}

		// Adjust
		template < typename T, typename Container >
		void Stack< T, Container >::swap(my_type& rhs) NOEXCEPT_IF(FXD::STD::is_nothrow_swappable< my_type::container_type >::value)
		{
			using FXD::STD::swap;
			swap(m_container, rhs.m_container);
		}

		// Searches
		template < typename T, typename Container >
		bool Stack< T, Container >::contains(const value_type& key) const
		{
			return m_container.contains(key);
		}
		template < typename T, typename Container >
		const bool Stack< T, Container >::empty(void) const NOEXCEPT
		{
			return m_container.empty();
		}
		template < typename T, typename Container >
		const bool Stack< T, Container >::not_empty(void) const NOEXCEPT
		{
			return m_container.not_empty();
		}
		template < typename T, typename Container >
		typename const Stack< T, Container >::size_type Stack< T, Container >::size(void) const NOEXCEPT
		{
			return m_container.size();
		}
		template < typename T, typename Container >
		typename const Stack< T, Container >::size_type Stack< T, Container >::count(const value_type& key) const NOEXCEPT
		{
			return m_container.count(key);
		}
	} //namespace Container

	namespace STD
	{
		template < typename T, typename Container >
		inline void swap(FXD::Container::Stack< T, Container >& lhs, FXD::Container::Stack< T, Container >& rhs) NOEXCEPT_IF((FXD::STD::is_nothrow_swappable< typename FXD::Container::Stack< T, Container >::container_type >::value))
		{
			lhs.swap(rhs);
		}
	} //namespace STD

} //namespace FXD
#endif //FXDENGINE_CONTAINER_STACK_H