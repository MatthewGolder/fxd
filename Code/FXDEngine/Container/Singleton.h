// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_SINGLETON_H
#define FXDENGINE_CONTAINER_SINGLETON_H

#include "FXDEngine/Thread/CriticalSection.h"

namespace FXD
{
	namespace Container
	{
		// ------
		// ISingleton
		// -
		// ------
		class ISingleton
		{
		public:
			ISingleton(void);
			~ISingleton(void);

		protected:
			static FXD::Thread::CSLock g_singletonCS;
		};

		// ------
		// Singleton
		// -
		// ------
		template < typename Type >
		class Singleton : public ISingleton
		{
		public:
			static Type* instance(void)
			{
				FXD::Thread::CSLock::LockGuard lock(FXD::Container::ISingleton::g_singletonCS);
				if (m_pInstance == nullptr)
				{
					m_pInstance = FXD_NEW(Type);
				}
				return m_pInstance;
			}
			static Type* quick_instance(void)
			{
				return m_pInstance;
			}
			static void destroy_instance(void)
			{
				FXD_SAFE_DELETE(m_pInstance);
			}
			void kill_instance(void)
			{
				FXD_SAFE_DELETE(m_pInstance);
			}
			inline operator bool(void) const
			{
				return (quick_instance() != nullptr);
			}

		protected:
			Singleton(void)
			{
			}
			virtual ~Singleton(void)
			{
			}

		private:
			static Type* m_pInstance;
		};
		template < typename Type > Type* Singleton< Type >::m_pInstance = nullptr;

	} //namespace Container
} //namespace FXD
#endif //FXDENGINE_CONTAINER_SINGLETON_H
