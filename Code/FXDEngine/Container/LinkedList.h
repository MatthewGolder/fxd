// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_LINKEDLIST_H
#define FXDENGINE_CONTAINER_LINKEDLIST_H

#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Core/TypeTraits.h"
#include "FXDEngine/Core/Internal/Count.h"
#include "FXDEngine/Core/Internal/Find.h"
#include "FXDEngine/Core/Internal/LexicographicalCompare.h"
#include "FXDEngine/Core/Internal/Sort.h"
#include "FXDEngine/Core/Internal/Remove.h"
#include "FXDEngine/Core/Internal/RemoveIf.h"
#include "FXDEngine/Container/Iterator.h"
#include "FXDEngine/Memory/Allocator/Aligned.h"
#include "FXDEngine/Memory/Memory.h"

namespace FXD
{
	namespace Container
	{
		namespace Internal
		{
			// ------
			// LListNodeBase
			// -
			// ------
			struct _LListNodeBase
			{
			public:
				void insert(_LListNodeBase* pNext) NOEXCEPT;
				void remove(void) NOEXCEPT;
				void splice(_LListNodeBase* pFirst, _LListNodeBase* pLast) NOEXCEPT;
				void reverse(void) NOEXCEPT;

				static void swap(_LListNodeBase& lhs, _LListNodeBase& rhs) NOEXCEPT;

			public:
				_LListNodeBase* m_pNext;
				_LListNodeBase* m_pPrev;
			};

			// ------
			// _LListNodeBase
			// -
			// ------
			inline void _LListNodeBase::insert(_LListNodeBase* pNext) NOEXCEPT
			{
				m_pNext = pNext;
				m_pPrev = pNext->m_pPrev;
				pNext->m_pPrev->m_pNext = this;
				pNext->m_pPrev = this;
			}
			inline void _LListNodeBase::remove(void) NOEXCEPT
			{
				m_pNext->m_pPrev = m_pPrev;
				m_pPrev->m_pNext = m_pNext;
			}
			inline void _LListNodeBase::splice(_LListNodeBase* pFirst, _LListNodeBase* pLast) NOEXCEPT
			{
				pLast->m_pPrev->m_pNext = this;
				pFirst->m_pPrev->m_pNext = pLast;
				this->m_pPrev->m_pNext = pFirst;

				_LListNodeBase* const pTemp = this->m_pPrev;
				this->m_pPrev = pLast->m_pPrev;
				pLast->m_pPrev = pFirst->m_pPrev;
				pFirst->m_pPrev = pTemp;
			}
			inline void _LListNodeBase::reverse(void) NOEXCEPT
			{
				_LListNodeBase* pNode = this;
				do
				{
					_LListNodeBase* const pTemp = pNode->m_pNext;
					pNode->m_pNext = pNode->m_pPrev;
					pNode->m_pPrev = pTemp;
					pNode = pNode->m_pPrev;
				} while (pNode != this);
			}

			inline void _LListNodeBase::swap(_LListNodeBase& lhs, _LListNodeBase& rhs) NOEXCEPT
			{
				const _LListNodeBase temp(lhs);
				lhs = rhs;
				rhs = temp;

				if (lhs.m_pNext == &rhs)
				{
					lhs.m_pNext = lhs.m_pPrev = &lhs;
				}
				else
				{
					lhs.m_pNext->m_pPrev = lhs.m_pPrev->m_pNext = &lhs;
				}

				if (rhs.m_pNext == &lhs)
				{
					rhs.m_pNext = rhs.m_pPrev = &rhs;
				}
				else
				{
					rhs.m_pNext->m_pPrev = rhs.m_pPrev->m_pNext = &rhs;
				}
			}

			// ------
			// _LListNode
			// -
			// ------
			template < typename T >
			struct _LListNode : public Internal::_LListNodeBase
			{
			public:
				T m_val;
			};

			// ------
			// _LinkedListBase
			// -
			// ------
			template <
				typename T,
				typename Allocator = Memory::Alloc::AlignedType< Memory::Alloc::Heap, Internal::_LListNode< T > > >
			class _LinkedListBase
			{
			public:
				using my_type = Internal::_LinkedListBase< T, Allocator >;
				using node_type = Internal::_LListNode< T >;
				using base_node_type = Internal::_LListNodeBase;
				using value_type = T;
				using size_type = FXD::U32;
				using difference_type = std::ptrdiff_t;
				using allocator_type = Allocator;

			protected:
				_LinkedListBase(void);
				_LinkedListBase(const allocator_type& alloc);
				virtual ~_LinkedListBase(void);

				node_type* _allocate_node(void);
				void _free_node(node_type* pNode);

				void _init(void) NOEXCEPT;
				void _clear(void);

			protected:
				base_node_type m_node;
				size_type m_nSize;
				allocator_type m_alloc;
			};

			// ------
			// _LinkedListBase
			// -
			// ------
			template < typename T, typename Allocator >
			_LinkedListBase< T, Allocator >::_LinkedListBase(void)
				: m_node()
				, m_nSize(0)
			{
				_init();
			}
			template < typename T, typename Allocator >
			_LinkedListBase< T, Allocator >::_LinkedListBase(const allocator_type& alloc)
				: m_node()
				, m_nSize(0)
				, m_alloc(alloc)
			{
				_init();
			}

			template < typename T, typename Allocator >
			_LinkedListBase< T, Allocator >::~_LinkedListBase(void)
			{
				_clear();
			}
			template < typename T, typename Allocator >
			typename _LinkedListBase< T, Allocator >::node_type* _LinkedListBase< T, Allocator >::_allocate_node(void)
			{
				node_type* pNode = (node_type*)m_alloc.allocate(sizeof(node_type));
				PRINT_COND_ASSERT((pNode != nullptr), "Container: allocate() failed");
				return pNode;
			}

			template < typename T, typename Allocator >
			void _LinkedListBase< T, Allocator >::_free_node(node_type* pPtr)
			{
				if (pPtr != nullptr)
				{
					m_alloc.deallocate(pPtr, sizeof(node_type));
				}
			}

			template < typename T, typename Allocator >
			void _LinkedListBase< T, Allocator >::_init(void) NOEXCEPT
			{
				m_node.m_pNext = (Internal::_LListNode< T >*)&m_node;
				m_node.m_pPrev = (Internal::_LListNode< T >*)&m_node;
			}

			template < typename T, typename Allocator >
			void _LinkedListBase< T, Allocator >::_clear(void)
			{
				node_type* pPtr = static_cast< node_type* >(m_node.m_pNext);

				while (pPtr != &m_node)
				{
					node_type* const pTemp = pPtr;
					pPtr = static_cast< node_type* >(pPtr->m_pNext);
					FXD::STD::destruct(pTemp);
					m_alloc.deallocate(pTemp, sizeof(node_type));
				}
			}

			// ------
			// _linkedList_iterator
			// -
			// ------
			template < typename T, typename Pointer, typename Reference >
			class _linkedList_iterator
			{
			public:
				friend struct Internal::_LListNodeBase;

				template < typename >
				friend struct Internal::_LListNode;

			public:
				using my_type = _linkedList_iterator< T, Pointer, Reference >;
				using node_type = Internal::_LListNode< T >;
				using base_node_type = Internal::_LListNodeBase;
				using value_type = T;
				using reference = Reference;
				using pointer = Pointer;
				using difference_type = std::ptrdiff_t;
				using iterator_category = FXD::STD::bidirectional_iterator_tag;

				using iterator = _linkedList_iterator< T, T*, T& >;
				using const_iterator = _linkedList_iterator< T, const T*, const T& >;

			public:
				_linkedList_iterator(void);
				_linkedList_iterator(const base_node_type* pNode) NOEXCEPT;
				_linkedList_iterator(const iterator& rhs) NOEXCEPT;

				_linkedList_iterator& operator++() NOEXCEPT;
				_linkedList_iterator operator++(int) NOEXCEPT;

				_linkedList_iterator& operator--() NOEXCEPT;
				_linkedList_iterator operator--(int) NOEXCEPT;

				_linkedList_iterator& operator+=(difference_type nOff) NOEXCEPT;
				_linkedList_iterator& operator-=(difference_type nOff) NOEXCEPT;

				_linkedList_iterator operator+(difference_type nOff) const NOEXCEPT;
				_linkedList_iterator operator-(difference_type nOff) const NOEXCEPT;

				reference operator*(void) const NOEXCEPT;
				pointer operator->(void) const NOEXCEPT;

			private:
				// Non-member comparison functions
				// Equality
				template < typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB >
				friend bool operator==(const _linkedList_iterator< T2, PtrA, RefA >& lhs, const _linkedList_iterator< T2, PtrB, RefB >& rhs) NOEXCEPT;
				template < typename T2, typename PtrA, typename RefA >
				friend bool operator==(const _linkedList_iterator< T2, PtrA, RefA >& lhs, const _linkedList_iterator< T2, PtrA, RefA >& rhs) NOEXCEPT;
				// Inequality
				template < typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB >
				friend bool operator!=(const _linkedList_iterator< T2, PtrA, RefA >& lhs, const _linkedList_iterator< T2, PtrB, RefB >& rhs) NOEXCEPT;
				template < typename T2, typename PtrA, typename RefA >
				friend bool operator!=(const _linkedList_iterator< T2, PtrA, RefA >& lhs, const _linkedList_iterator< T2, PtrA, RefA >& rhs) NOEXCEPT;

				template < typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB >
				friend difference_type operator+(const _linkedList_iterator< T2, PtrA, RefA >& lhs, const _linkedList_iterator< T2, PtrB, RefB >& rhs) NOEXCEPT;
				template < typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB >
				friend difference_type operator-(const _linkedList_iterator< T2, PtrA, RefA >& lhs, const _linkedList_iterator< T2, PtrB, RefB >& rhs) NOEXCEPT;

			public:
				node_type* m_pNode;
			};

			// ------
			// _linkedList_iterator
			// -
			// ------
			template < typename T, typename Pointer, typename Reference >
			_linkedList_iterator< T, Pointer, Reference >::_linkedList_iterator(void)
			{}

			template < typename T, typename Pointer, typename Reference >
			_linkedList_iterator< T, Pointer, Reference >::_linkedList_iterator(const base_node_type* pNode) NOEXCEPT
				: m_pNode(static_cast< node_type* >((Internal::_LListNode< T >*)const_cast< base_node_type* >(pNode)))
			{}

			template < typename T, typename Pointer, typename Reference >
			_linkedList_iterator< T, Pointer, Reference >::_linkedList_iterator(const iterator& rhs) NOEXCEPT
				: m_pNode(static_cast< node_type* >(rhs.m_pNode))
			{}

			template < typename T, typename Pointer, typename Reference >
			inline typename _linkedList_iterator< T, Pointer, Reference >::my_type& _linkedList_iterator< T, Pointer, Reference >::operator++() NOEXCEPT
			{
				m_pNode = static_cast< node_type* >(m_pNode->m_pNext);
				return (*this);
			}
			template < typename T, typename Pointer, typename Reference >
			inline typename _linkedList_iterator< T, Pointer, Reference >::my_type _linkedList_iterator< T, Pointer, Reference >::operator++(int) NOEXCEPT
			{
				const my_type tmp(*this);
				operator++();
				return (tmp);
			}

			template < typename T, typename Pointer, typename Reference >
			inline typename _linkedList_iterator< T, Pointer, Reference >::my_type& _linkedList_iterator< T, Pointer, Reference >::operator--() NOEXCEPT
			{
				m_pNode = static_cast< node_type* >(m_pNode->m_pPrev);
				return (*this);
			}
			template < typename T, typename Pointer, typename Reference >
			inline typename _linkedList_iterator< T, Pointer, Reference >::my_type _linkedList_iterator< T, Pointer, Reference >::operator--(int) NOEXCEPT
			{
				const my_type tmp(*this);
				operator--();
				return (tmp);
			}

			template < typename T, typename Pointer, typename Reference >
			inline typename _linkedList_iterator< T, Pointer, Reference >::my_type& _linkedList_iterator< T, Pointer, Reference >::operator+=(difference_type nOff) NOEXCEPT
			{
				FXD::STD::advance((*this), nOff);
				return (*this);
			}
			template < typename T, typename Pointer, typename Reference >
			inline typename _linkedList_iterator< T, Pointer, Reference >::my_type& _linkedList_iterator< T, Pointer, Reference >::operator-=(difference_type nOff) NOEXCEPT
			{
				FXD::STD::advance((*this), -nOff);
				return (*this);
			}

			template < typename T, typename Pointer, typename Reference >
			inline typename _linkedList_iterator< T, Pointer, Reference >::my_type _linkedList_iterator< T, Pointer, Reference >::operator+(difference_type nOff) const NOEXCEPT
			{
				return FXD::STD::next((*this), nOff);
			}
			template < typename T, typename Pointer, typename Reference >
			inline typename _linkedList_iterator< T, Pointer, Reference >::my_type _linkedList_iterator< T, Pointer, Reference >::operator-(difference_type nOff) const NOEXCEPT
			{
				return FXD::STD::prev((*this), nOff);
			}

			template < typename T, typename Pointer, typename Reference >
			inline typename _linkedList_iterator< T, Pointer, Reference >::reference _linkedList_iterator< T, Pointer, Reference >::operator*(void) const NOEXCEPT
			{
				return m_pNode->m_val;
			}
			template < typename T, typename Pointer, typename Reference >
			inline typename _linkedList_iterator< T, Pointer, Reference >::pointer _linkedList_iterator< T, Pointer, Reference >::operator->(void) const NOEXCEPT
			{
				return &(operator*());
			}

			// Non-member comparison functions
			// Equality
			template < typename T, typename PtrA, typename RefA, typename PtrB, typename RefB >
			inline bool operator==(const _linkedList_iterator< T, PtrA, RefA >& lhs, const _linkedList_iterator< T, PtrB, RefB >& rhs) NOEXCEPT
			{
				return (lhs.m_pNode == rhs.m_pNode);
			}
			template < typename T, typename PtrA, typename RefA >
			inline bool operator==(const _linkedList_iterator< T, PtrA, RefA >& lhs, const _linkedList_iterator< T, PtrA, RefA >& rhs) NOEXCEPT
			{
				return (lhs.m_pNode == rhs.m_pNode);
			}
			// Inequality
			template < typename T, typename PtrA, typename RefA, typename PtrB, typename RefB >
			inline bool operator!=(const _linkedList_iterator< T, PtrA, RefA >& lhs, const _linkedList_iterator< T, PtrB, RefB >& rhs) NOEXCEPT
			{
				return (lhs.m_pNode != rhs.m_pNode);
			}
			template < typename T, typename PtrA, typename RefA >
			inline bool operator!=(const _linkedList_iterator< T, PtrA, RefA >& lhs, const _linkedList_iterator< T, PtrA, RefA >& rhs) NOEXCEPT
			{
				return (lhs.m_pNode != rhs.m_pNode);
			}
		} //namespace Internal

		// ------
		// LinkedList
		// -
		// ------
		template <
			typename T,
			typename Allocator = Memory::Alloc::AlignedType< Memory::Alloc::Heap, Internal::_LListNode< T > > >
		class LinkedList : public Internal::_LinkedListBase< T, Allocator >
		{
		public:
			using my_type = LinkedList< T, Allocator >;
			using base_type = Internal::_LinkedListBase< T, Allocator >;
			using node_type = Internal::_LListNode< T >;
			using base_node_type = Internal::_LListNodeBase;
			using value_type = T;
			using pointer = T*;
			using const_pointer = const T*;
			using reference = T&;
			using const_reference = const T&;
			using size_type = FXD::U32;
			using difference_type = std::ptrdiff_t;
			using allocator_type = Allocator;

			using iterator = Internal::_linkedList_iterator< T, T*, T& >;
			using const_iterator = Internal::_linkedList_iterator< T, const T*, const T& >;
			using reverse_iterator = FXD::STD::reverse_iterator< iterator >;
			using const_reverse_iterator = FXD::STD::reverse_iterator< const_iterator >;

		public:
			LinkedList(void);
			LinkedList(const allocator_type& alloc);
			LinkedList(size_type nCount, const allocator_type& alloc = allocator_type());
			LinkedList(size_type nCount, const value_type& val, const allocator_type& alloc = allocator_type());
			template < typename InputItr, class = typename FXD::STD::enable_if< FXD::Internal::_Is_iterator< InputItr >::value, void >::type >
			LinkedList(InputItr first, InputItr last, const allocator_type& alloc = allocator_type());
			LinkedList(std::initializer_list< value_type > iList, const allocator_type& alloc = allocator_type());

			~LinkedList(void);

			LinkedList(const my_type& rhs);
			LinkedList(const my_type& rhs, const allocator_type& alloc);
			LinkedList(my_type&& rhs);
			LinkedList(my_type&& rhs, const allocator_type& alloc);

			my_type& operator=(const my_type& rhs);
			my_type& operator=(my_type&& rhs);
			my_type& operator=(std::initializer_list< value_type > iList);

			// Iterator Support
			iterator begin(void);
			const_iterator begin(void) const;
			const_iterator cbegin(void) const;
			iterator end(void);
			const_iterator end(void) const;
			const_iterator cend(void) const;

			reverse_iterator rbegin(void);
			const_reverse_iterator rbegin(void) const;
			const_reverse_iterator crbegin(void) const;
			reverse_iterator rend(void);
			const_reverse_iterator rend(void) const;
			const_reverse_iterator crend(void) const;

			// Access
			reference front(void);
			const_reference front(void) const;
			reference back(void);
			const_reference back(void) const;

			// Assign
			void assign(size_type nCount, const value_type& val);
			template < typename InputItr, class = typename FXD::STD::enable_if< FXD::Internal::_Is_iterator< InputItr >::value, void >::type >
			void assign(InputItr first, InputItr last);
			void assign(std::initializer_list< value_type > iList);

			// Resize
			void resize(size_type nCount, const value_type& val);
			void resize(size_type nCount);

			// Insert
			iterator insert(const_iterator pos, const value_type& val);
			iterator insert(const_iterator pos, value_type&& val);
			iterator insert(const_iterator pos, size_type nCount, const value_type& val);
			template < typename InputItr, class = typename FXD::STD::enable_if< FXD::Internal::_Is_iterator< InputItr >::value, void >::type >
			iterator insert(const_iterator pos, InputItr first, InputItr last);
			iterator insert(const_iterator pos, std::initializer_list< value_type > iList);

			void push_front(const value_type& val);
			void push_front(value_type&& val);

			void push_back(const value_type& val);
			void push_back(value_type&& val);

			template < class... Args >
			iterator emplace(const_iterator pos, Args&&... args);
			template < class... Args >
			reference emplace_front(Args&&... args);
			template < class... Args >
			reference emplace_back(Args&&... args);

			// Searches
			const_iterator find(const value_type& src) const;
			bool contains(const value_type& src) const;

			// Erase
			iterator erase(const_iterator pos);
			iterator erase(const_iterator first, const_iterator last);
			reverse_iterator erase(const_reverse_iterator pos);
			reverse_iterator erase(const_reverse_iterator first, const_reverse_iterator last);
			size_type find_erase(const value_type& src);

			void remove(const value_type& val);
			template < typename Predicate >
			void remove_if(Predicate pred);

			void pop_front(void);
			void pop_back(void);
			void clear(void);


			void reverse(void) NOEXCEPT;

			// Adjust
			bool bring_to_front(const_iterator itr);

			void sort(void);
			template < typename Pr >
			void sort(Pr cmp);
			void swap(my_type& rhs);

			void merge(my_type& rhs);
			template < typename Compare >
			void merge(my_type& rhs, Compare compare);

			void merge(my_type&& rhs);
			template < typename Compare >
			void merge(my_type&& rhs, Compare compare);

			void unique(void);
			template < typename BinaryPredicate >
			void unique(BinaryPredicate pred);

			void splice(const_iterator pos, my_type& rhs);
			void splice(const_iterator pos, my_type& rhs, const_iterator first);
			void splice(const_iterator pos, my_type& rhs, const_iterator first, const_iterator last);
			void splice(const_iterator pos, my_type&& rhs);
			void splice(const_iterator pos, my_type&& rhs, const_iterator first);
			void splice(const_iterator pos, my_type&& rhs, const_iterator first, const_iterator last);

			// Searches
			bool empty(void) const NOEXCEPT;
			bool not_empty(void) const NOEXCEPT;

			size_type size(void) const NOEXCEPT;
			size_type count(const value_type& key) const NOEXCEPT;
			allocator_type allocator(void) const NOEXCEPT;
			bool validate(void) const NOEXCEPT;

		private:
			node_type* _create_node(void);

			template < typename... Args >
			node_type* _create_node(Args&&... args);

			template < typename Integer >
			void _assign(Integer nCount, Integer val, FXD::STD::true_type);

			template < typename InputItr >
			void _assign(InputItr first, InputItr last, FXD::STD::false_type);

			void _assign_values(size_type nCount, const value_type& val);

			template < typename Integer >
			void _insert(base_node_type* pNode, Integer nCount, Integer val, FXD::STD::true_type);

			template < typename InputItr >
			void _insert(base_node_type* pNode, InputItr first, InputItr last, FXD::STD::false_type);

			void _insert_values(base_node_type* pNode, size_type nCount, const value_type& val);

			template < typename... Args >
			void _insert_value(base_node_type* pNode, Args&&... args);

			void _erase(base_node_type* pNode);

			void _swap(my_type& rhs);
		};

		template < typename T, typename Allocator >
		LinkedList< T, Allocator >::LinkedList(void)
		{}
		template < typename T, typename Allocator >
		LinkedList< T, Allocator >::LinkedList(const allocator_type& alloc)
			: base_type(alloc)
		{}
		template < typename T, typename Allocator >
		LinkedList< T, Allocator >::LinkedList(size_type nCount, const allocator_type& alloc)
			: base_type(alloc)
		{
			_insert_values((base_node_type*)&this->m_node, nCount, value_type());
		}
		template < typename T, typename Allocator >
		LinkedList< T, Allocator >::LinkedList(size_type nCount, const value_type& val, const allocator_type& alloc)
			: base_type(alloc)
		{
			_insert_values((base_node_type*)&this->m_node, nCount, val);
		}
		template < typename T, typename Allocator >
		template < typename InputItr, class >
		LinkedList< T, Allocator >::LinkedList(InputItr first, InputItr last, const allocator_type& alloc)
			: base_type(alloc)
		{
			_insert((base_node_type*)&this->m_node, first, last, FXD::STD::is_integral< InputItr >());
		}
		template < typename T, typename Allocator >
		LinkedList< T, Allocator >::LinkedList(std::initializer_list< value_type > iList, const allocator_type& alloc)
			: base_type(alloc)
		{
			_insert((base_node_type*)&this->m_node, iList.begin(), iList.end(), FXD::STD::false_type());
		}

		template < typename T, typename Allocator >
		LinkedList< T, Allocator >::~LinkedList(void)
		{}

		template < typename T, typename Allocator >
		inline LinkedList< T, Allocator >::LinkedList(const my_type& rhs)
			: base_type(rhs.m_alloc)
		{
			_insert((base_node_type*)&this->m_node, const_iterator((base_node_type*)rhs.m_node.m_pNext), const_iterator((base_node_type*)&rhs.m_node), FXD::STD::false_type());
		}
		template < typename T, typename Allocator >
		inline LinkedList< T, Allocator >::LinkedList(const my_type& rhs, const allocator_type& alloc)
			: base_type(alloc)
		{
			_insert((base_node_type*)&this->m_node, const_iterator((base_node_type*)rhs.m_node.m_pNext), const_iterator((base_node_type*)&rhs.m_node), FXD::STD::false_type());
		}

		template < typename T, typename Allocator >
		inline LinkedList< T, Allocator >::LinkedList(my_type&& rhs)
			: base_type(rhs.m_alloc)
		{
			swap(rhs);
		}
		template < typename T, typename Allocator >
		inline LinkedList< T, Allocator >::LinkedList(my_type&& rhs, const allocator_type& alloc)
			: base_type(rhs.m_alloc)
		{}

		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::my_type& LinkedList< T, Allocator >::operator=(const my_type& rhs)
		{
			if (this != &rhs)
			{
				bool bSlowerPathwayRequired = false;
				if (bSlowerPathwayRequired)
				{
					clear();
				}
				_assign(rhs.begin(), rhs.end(), FXD::STD::false_type());
			}
			return (*this);
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::my_type& LinkedList< T, Allocator >::operator=(my_type&& rhs)
		{
			if (this != &rhs)
			{
				clear();
				swap(rhs);
			}
			return (*this);
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::my_type& LinkedList< T, Allocator >::operator=(std::initializer_list< value_type > iList)
		{
			_assign(iList.begin(), iList.end(), FXD::STD::false_type());
			return (*this);
		}

		// Iterator Support
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::iterator LinkedList< T, Allocator >::begin(void)
		{
			return iterator((base_node_type*)this->m_node.m_pNext);
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::const_iterator LinkedList< T, Allocator >::begin(void) const
		{
			return const_iterator((base_node_type*)this->m_node.m_pNext);
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::const_iterator LinkedList< T, Allocator >::cbegin(void) const
		{
			return const_iterator((base_node_type*)this->m_node.m_pNext);
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::iterator LinkedList< T, Allocator >::end(void)
		{
			return iterator((base_node_type*)&this->m_node);
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::const_iterator LinkedList< T, Allocator >::end(void) const
		{
			return const_iterator((base_node_type*)&this->m_node);
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::const_iterator LinkedList< T, Allocator >::cend(void) const
		{
			return const_iterator((base_node_type*)&this->m_node);
		}

		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::reverse_iterator LinkedList< T, Allocator >::rbegin(void)
		{
			return reverse_iterator(iterator(end()));
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::const_reverse_iterator LinkedList< T, Allocator >::rbegin(void) const
		{
			return const_reverse_iterator(const_iterator(end()));
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::const_reverse_iterator LinkedList< T, Allocator >::crbegin(void) const
		{
			return const_reverse_iterator(const_iterator(end()));
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::reverse_iterator LinkedList< T, Allocator >::rend(void)
		{
			return reverse_iterator(iterator(begin()));
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::const_reverse_iterator LinkedList< T, Allocator >::rend(void) const
		{
			return const_reverse_iterator(const_iterator(begin()));
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::const_reverse_iterator LinkedList< T, Allocator >::crend(void) const
		{
			return const_reverse_iterator(const_iterator(begin()));
		}

		// Access
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::reference LinkedList< T, Allocator >::front(void)
		{
			return (*begin());
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::const_reference LinkedList< T, Allocator >::front(void) const
		{
			return (*begin());
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::reference LinkedList< T, Allocator >::back(void)
		{
			return (*rbegin());
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::const_reference LinkedList< T, Allocator >::back(void) const
		{
			return (*rbegin());
		}

		// Assign
		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::assign(size_type nCount, const value_type& val)
		{
			_assign_values(nCount, val);
		}
		template < typename T, typename Allocator >
		template < typename InputItr, class >
		void LinkedList< T, Allocator >::assign(InputItr first, InputItr last)
		{
			_assign(first, last, FXD::STD::is_integral< InputItr >());
		}
		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::assign(std::initializer_list< T > iList)
		{
			_assign(iList.begin(), iList.end(), FXD::STD::false_type());
		}

		// Resize
		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::resize(size_type nCount, const value_type& val)
		{
			iterator current((base_node_type*)this->m_node.m_pNext);
			size_type n1 = 0;
			while ((current.m_pNode != &this->m_node) && (n1 < nCount))
			{
				++current;
				++n1;
			}
			if (n1 == nCount)
			{
				erase(current, (base_node_type*)&this->m_node);
			}
			else
			{
				insert((base_node_type*)&this->m_node, (nCount - n1), val);
			}
		}
		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::resize(size_type nCount)
		{
			resize(nCount, value_type());
		}

		// Insert
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::iterator LinkedList< T, Allocator >::insert(const_iterator pos, const value_type& val)
		{
			node_type* const pNode = _create_node(val);
			((base_node_type*)pNode)->insert((base_node_type*)pos.m_pNode);
			++this->m_nSize;
			return (base_node_type*)pNode;
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::iterator LinkedList< T, Allocator >::insert(const_iterator pos, value_type&& val)
		{
			return emplace(pos, std::move(val));
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::iterator LinkedList< T, Allocator >::insert(const_iterator pos, size_type nCount, const value_type& val)
		{
			if (pos == begin())
			{
				_insert_values((base_node_type*)pos.m_pNode, nCount, val);
				return (begin());
			}
			else
			{
				iterator prev(pos.m_pNode);
				--prev;
				_insert_values((base_node_type*)pos.m_pNode, nCount, val);
				return (++prev);
			}
		}
		template < typename T, typename Allocator >
		template < typename InputItr, class >
		typename LinkedList< T, Allocator >::iterator LinkedList< T, Allocator >::insert(const_iterator pos, InputItr first, InputItr last)
		{
			if (pos == begin())
			{
				_insert((base_node_type*)pos.m_pNode, first, last, FXD::STD::is_integral< InputItr >());
				return (begin());
			}
			else
			{
				iterator prev(pos.m_pNode);
				--prev;
				_insert((base_node_type*)pos.m_pNode, first, last, FXD::STD::is_integral< InputItr >());
				return (++prev);
			}
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::iterator LinkedList< T, Allocator >::insert(const_iterator pos, std::initializer_list< value_type > iList)
		{
			iterator itPrev(pos.m_pNode);
			--itPrev;
			_insert((base_node_type*)pos.m_pNode, iList.begin(), iList.end(), FXD::STD::false_type());
			return ++itPrev;
		}

		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::push_front(const value_type& val)
		{
			_insert_value((base_node_type*)this->m_node.m_pNext, val);
			++this->m_nSize;
		}
		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::push_front(value_type&& val)
		{
			emplace(begin(), std::move(val));
		}
		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::push_back(const value_type& val)
		{
			_insert_value((base_node_type*)&this->m_node, val);
			++this->m_nSize;
		}
		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::push_back(value_type&& val)
		{
			emplace(end(), std::move(val));
		}

		template < typename T, typename Allocator >
		template < class... Args >
		typename LinkedList< T, Allocator >::iterator LinkedList< T, Allocator >::emplace(const_iterator pos, Args&&... args)
		{
			_insert_value(pos.m_pNode, std::forward< Args >(args)...);
			return iterator(pos.m_pNode->m_pPrev);
		}
		template < typename T, typename Allocator >
		template < class... Args >
		typename LinkedList< T, Allocator >::reference LinkedList< T, Allocator >::emplace_front(Args&&... args)
		{
			_insert_value((base_node_type*)this->m_node.m_pNext, std::forward< Args >(args)...);
			return front();
		}
		template < typename T, typename Allocator >
		template < class... Args >
		typename LinkedList< T, Allocator >::reference LinkedList< T, Allocator >::emplace_back(Args&&... args)
		{
			_insert_value((base_node_type*)&this->m_node, std::forward< Args >(args)...);
			return back();
		}

		// Searches
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::const_iterator LinkedList< T, Allocator >::find(const value_type& src) const
		{
			return FXD::STD::find(begin(), end(), src);
		}

		template < typename T, typename Allocator >
		inline bool LinkedList< T, Allocator >::contains(const value_type& src) const
		{
			return (find(src) != end());
		}

		// Erase
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::iterator LinkedList< T, Allocator >::erase(const_iterator pos)
		{
			++pos;
			_erase((base_node_type*)pos.m_pNode->m_pPrev);
			return iterator(pos.m_pNode);
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::iterator LinkedList< T, Allocator >::erase(const_iterator first, const_iterator last)
		{
			while (first != last)
			{
				first = erase(first);
			}
			return iterator(last.m_pNode);
		}

		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::reverse_iterator LinkedList< T, Allocator >::erase(const_reverse_iterator pos)
		{
			return reverse_iterator(erase((++pos).base()));
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::reverse_iterator LinkedList< T, Allocator >::erase(const_reverse_iterator first, const_reverse_iterator last)
		{
			const_iterator itLastBase((++last).base());
			const_iterator itFirstBase((++first).base());

			return reverse_iterator(erase(itLastBase, itFirstBase));
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::size_type LinkedList< T, Allocator >::find_erase(const value_type& src)
		{
			const_iterator itr = find(src);
			if (itr != end())
			{
				erase(itr);
				return 1;
			}
			return 0;
		}

		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::remove(const value_type& val)
		{
			iterator current((base_node_type*)this->m_node.m_pNext);

			while (current.m_pNode != &this->m_node)
			{
				if ((!(*current == val)))
				{
					++current;
				}
				else
				{
					++current;
					_erase((base_node_type*)current.m_pNode->m_pPrev);
				}
			}
		}
		template < typename T, typename Allocator >
		template < typename Predicate >
		void LinkedList< T, Allocator >::remove_if(Predicate pred)
		{
			for (iterator first((base_node_type*)this->m_node.m_pNext), last((base_node_type*)&this->m_node); first != last; )
			{
				iterator temp(first);
				++temp;
				if (pred(first.m_pNode->m_val))
				{
					_erase((base_node_type*)first.m_pNode);
				}
				first = temp;
			}
		}

		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::pop_front(void)
		{
			_erase((base_node_type*)this->m_node.m_pNext);
		}
		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::pop_back(void)
		{
			_erase((base_node_type*)this->m_node.m_pPrev);
		}

		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::clear(void)
		{
			this->_clear();
			this->_init();
			this->m_nSize = 0;
		}

		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::reverse(void) NOEXCEPT
		{
			((base_node_type&)this->m_node).reverse();
		}

		// Adjust
		template < typename T, typename Allocator >
		bool LinkedList< T, Allocator >::bring_to_front(const_iterator itr)
		{
			if (itr == cend())
			{
				return false;
			}
			if (itr != begin())
			{
				splice(begin(), (*this), itr, FXD::STD::next(itr, 1));
			}
			return true;
		}


		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::sort(void)
		{
			sort(FXD::STD::less< T >());
		}
		template < typename T, typename Allocator >
		template < typename Pr >
		void LinkedList< T, Allocator >::sort(Pr cmp)
		{
			FXD::STD::sort(begin(), end(), cmp, size());
		}
		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::swap(my_type& rhs)
		{
			if (this->m_alloc == rhs.m_alloc)
			{
				_swap(rhs);
			}
			else // else swap the contents.
			{
				const my_type temp(*this);
				*this = rhs;
				rhs = temp;
			}
		}


		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::merge(my_type& rhs)
		{
			if (this != &rhs)
			{
				iterator first(begin());
				iterator firstX(rhs.begin());
				const iterator last(end());
				const iterator lastX(rhs.end());

				while ((first != last) && (firstX != lastX))
				{
					if (*firstX < *first)
					{
						iterator next(firstX);

						splice(first, rhs, firstX, ++next);
						firstX = next;
					}
					else
					{
						++first;
					}
				}

				if (firstX != lastX)
				{
					splice(last, rhs, firstX, lastX);
				}
			}
		}
		template < typename T, typename Allocator >
		template < typename Compare >
		void LinkedList< T, Allocator >::merge(my_type& rhs, Compare compare)
		{
			if (this != &rhs)
			{
				iterator first(begin());
				iterator firstX(rhs.begin());
				const iterator last(end());
				const iterator lastX(rhs.end());

				while ((first != last) && (firstX != lastX))
				{
					if (compare(*firstX, *first))
					{
						iterator next(firstX);

						splice(first, rhs, firstX, ++next);
						firstX = next;
					}
					else
					{
						++first;
					}
				}

				if (firstX != lastX)
				{
					splice(last, rhs, firstX, lastX);
				}
			}
		}

		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::merge(my_type&& rhs)
		{
			return merge(rhs);
		}
		template < typename T, typename Allocator >
		template < typename Compare >
		void LinkedList< T, Allocator >::merge(my_type&& rhs, Compare compare)
		{
			return merge(rhs, compare);
		}

		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::unique(void)
		{
			unique(FXD::STD::equal_to<>());
		}
		template < typename T, typename Allocator >
		template < typename BinaryPredicate >
		void LinkedList< T, Allocator >::unique(BinaryPredicate pred)
		{
			iterator first(begin());
			const iterator last(end());

			if (first != last)
			{
				iterator next(first);

				while (++next != last)
				{
					if (pred(*first, *next))
					{
						_erase((base_node_type*)next.m_pNode);
					}
					else
					{
						first = next;
					}
					next = first;
				}
			}
		}

		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::splice(const_iterator pos, my_type& rhs)
		{
			if (this->m_alloc == rhs.m_alloc)
			{
				if (rhs.not_empty())
				{
					((base_node_type*)pos.m_pNode)->splice((base_node_type*)rhs.m_node.m_pNext, (base_node_type*)&rhs.m_node);
					this->m_nSize += rhs.m_nSize;
					rhs.m_nSize = 0;
				}
			}
			else
			{
				insert(pos, rhs.begin(), rhs.end());
				rhs.clear();
			}
		}
		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::splice(const_iterator pos, my_type& rhs, const_iterator first)
		{
			if (this->m_alloc == rhs.m_alloc)
			{
				iterator first2(first.m_pNode);
				++first2;
				if ((pos != first) && (pos != first2))
				{
					((base_node_type*)pos.m_pNode)->splice((base_node_type*)first.m_pNode, (base_node_type*)first2.m_pNode);
					++this->m_nSize;
					--rhs.m_nSize;
				}
			}
			else
			{
				insert(pos, *first);
				rhs.erase(first);
			}
		}
		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::splice(const_iterator pos, my_type& rhs, const_iterator first, const_iterator last)
		{
			if (this->m_alloc == rhs.m_alloc)
			{
				const size_type nCount = (size_type)FXD::STD::distance(first, last);
				if (nCount > 0)
				{
					((base_node_type*)pos.m_pNode)->splice((base_node_type*)first.m_pNode, (base_node_type*)last.m_pNode);
					this->m_nSize += nCount;
					rhs.m_nSize -= nCount;
				}
			}
			else
			{
				insert(pos, first, last);
				rhs.erase(first, last);
			}
		}
		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::splice(const_iterator pos, my_type&& rhs)
		{
			splice(pos, rhs);
		}
		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::splice(const_iterator pos, my_type&& rhs, const_iterator first)
		{
			splice(pos, rhs, first);
		}
		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::splice(const_iterator pos, my_type&& rhs, const_iterator first, const_iterator last)
		{
			splice(pos, rhs, first, last);
		}

		// Searches
		template < typename T, typename Allocator >
		inline bool LinkedList< T, Allocator >::empty(void) const NOEXCEPT
		{
			return (this->m_nSize == 0);
		}
		template < typename T, typename Allocator >
		inline bool LinkedList< T, Allocator >::not_empty(void) const NOEXCEPT
		{
			return (this->m_nSize != 0);
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::size_type LinkedList< T, Allocator >::size(void) const NOEXCEPT
		{
			return (size_type)FXD::STD::distance(const_iterator((base_node_type*)this->m_node.m_pNext), const_iterator((base_node_type*)&this->m_node));
		}
		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::size_type LinkedList< T, Allocator >::count(const value_type& key) const NOEXCEPT
		{
			return (size_type)FXD::STD::count(begin(), end(), key);
		}
		template < typename T, typename Allocator >
		inline Allocator LinkedList< T, Allocator >::allocator(void) const NOEXCEPT
		{
			return this->m_alloc;
		}
		template < typename T, typename Allocator >
		inline bool LinkedList< T, Allocator >::validate(void) const NOEXCEPT
		{
			size_type n1 = 0;
			for (const_iterator itr(begin()), iEnd(end()); itr != iEnd; ++itr)
			{
				++n1;
			}
			if (n1 != this->m_nSize)
			{
				return false;
			}
			return true;
		}

		template < typename T, typename Allocator >
		typename LinkedList< T, Allocator >::node_type* LinkedList< T, Allocator >::_create_node(void)
		{
			node_type* const pNode = this->_allocate_node();
			FXD_PLACEMENT_NEW((void*)&pNode->m_val, value_type);
			return pNode;
		}

		template < typename T, typename Allocator >
		template < typename... Args >
		typename LinkedList< T, Allocator >::node_type* LinkedList< T, Allocator >::_create_node(Args&&... args)
		{
			node_type* const pNode = this->_allocate_node();
			FXD_PLACEMENT_NEW((void*)&pNode->m_val, value_type)(std::forward< Args >(args)...);
			return pNode;
		}

		template < typename T, typename Allocator >
		template < typename Integer >
		void LinkedList< T, Allocator >::_assign(Integer nCount, Integer val, FXD::STD::true_type)
		{
			_assign_values(static_cast< size_type >(nCount), static_cast< value_type >(val));
		}

		template < typename T, typename Allocator >
		template < typename InputItr >
		void LinkedList< T, Allocator >::_assign(InputItr first, InputItr last, FXD::STD::false_type)
		{
			node_type* pNode = static_cast< node_type* >(this->m_node.m_pNext);
			for (; (pNode != &this->m_node) && (first != last); ++first)
			{
				pNode->m_val = *first;
				pNode = static_cast< node_type* >(pNode->m_pNext);
			}

			if (first == last)
			{
				erase(const_iterator((base_node_type*)pNode), (base_node_type*)&this->m_node);
			}
			else
			{
				_insert((base_node_type*)&this->m_node, first, last, FXD::STD::false_type());
			}
		}


		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::_assign_values(size_type nCount, const value_type& val)
		{
			node_type* pNode = static_cast< node_type* >(this->m_node.m_pNext);

			for (; (pNode != &this->m_node) && (nCount > 0); --nCount)
			{
				pNode->m_val = val;
				pNode = static_cast< node_type* >(pNode->m_pNext);
			}

			if (nCount != 0)
			{
				_insert_values((base_node_type*)&this->m_node, nCount, val);
			}
			else
			{
				erase(const_iterator((base_node_type*)pNode), (base_node_type*)&this->m_node);
			}
		}

		template < typename T, typename Allocator >
		template < typename Integer >
		void LinkedList< T, Allocator >::_insert(base_node_type* pNode, Integer nCount, Integer val, FXD::STD::true_type)
		{
			_insert_values(pNode, static_cast< size_type >(nCount), static_cast< value_type >(val));
		}

		template < typename T, typename Allocator >
		template < typename InputItr >
		void LinkedList< T, Allocator >::_insert(base_node_type* pNode, InputItr first, InputItr last, FXD::STD::false_type)
		{
			for (; first != last; ++first)
			{
				_insert_value(pNode, *first);
			}
		}

		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::_insert_values(base_node_type* pNode, size_type nCount, const value_type& val)
		{
			for (; nCount > 0; --nCount)
			{
				_insert_value(pNode, val);
			}
		}

		template < typename T, typename Allocator >
		template < typename... Args >
		void LinkedList< T, Allocator >::_insert_value(base_node_type* pNode, Args&&... args)
		{
			node_type* const pNodeNew = _create_node(std::forward< Args >(args)...);
			((base_node_type*)pNodeNew)->insert(pNode);
			++this->m_nSize;
		}

		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::_erase(base_node_type* pNode)
		{
			pNode->remove();
			FXD::STD::destruct((node_type*)pNode);
			this->_free_node(((node_type*)pNode));
			--this->m_nSize;
		}

		template < typename T, typename Allocator >
		void LinkedList< T, Allocator >::_swap(my_type& rhs)
		{
			base_node_type::swap((base_node_type&)this->m_node, (base_node_type&)rhs.m_node);
			FXD::STD::swap(this->m_nSize, rhs.m_nSize);
		}

		// Non-member comparison functions
		// Equality
		template < typename T, typename Allocator >
		bool operator==(const LinkedList< T, Allocator >& lhs, const LinkedList< T, Allocator >& rhs)
		{
			return ((lhs.size() == rhs.size()) && (FXD::STD::equal(lhs.begin(), lhs.end(), rhs.begin())));
		}
		// Inequality
		template < typename T, typename Allocator >
		bool operator!=(const LinkedList< T, Allocator >& lhs, const LinkedList< T, Allocator >& rhs)
		{
			return (!(lhs == rhs));
		}
		// Less than
		template < typename T, typename Allocator >
		inline bool operator<(const LinkedList< T, Allocator >& lhs, const LinkedList< T, Allocator >& rhs)
		{
			return FXD::STD::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
		}
		// Greater than
		template < typename T, typename Allocator >
		inline bool operator>(const LinkedList< T, Allocator >& lhs, const LinkedList< T, Allocator >& rhs)
		{
			return (rhs < lhs);
		}
		// Less than or equal to
		template < typename T, typename Allocator >
		inline bool operator<=(const LinkedList< T, Allocator >& lhs, const LinkedList< T, Allocator >& rhs)
		{
			return !(rhs < lhs);
		}
		// Greater than or equal to
		template < typename T, typename Allocator >
		inline bool operator>=(const LinkedList< T, Allocator >& lhs, const LinkedList< T, Allocator >& rhs)
		{
			return !(lhs < rhs);
		}

		// Inserters and extractors
		template < typename T, typename Allocator >
		inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, const Container::LinkedList< T, Allocator >& list)
		{
			fxd_for(auto& i, list)
			{
				ostr << " " << i;
			}
			return ostr;
		}
	} //namespace Container

	namespace STD
	{
		template < typename T, typename Allocator >
		inline void swap(FXD::Container::LinkedList< T, Allocator >& lhs, FXD::Container::LinkedList< T, Allocator >& rhs)
		{
			lhs.swap(rhs);
		}

		template < typename T, typename Allocator, typename Pred >
		void erase_if(FXD::Container::LinkedList< T, Allocator >& container, Pred pred)
		{
			container.erase(FXD::STD::remove_if(container.begin(), container.end(), pred), container.end());
		}

		template < typename T, typename Allocator, typename U >
		void erase(FXD::Container::LinkedList< T, Allocator >& container, const U& val)
		{
			container.erase(FXD::STD::remove(container.begin(), container.end(), val), container.end());
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_LINKEDLIST_H