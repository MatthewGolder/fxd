// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_ARRAY_H
#define FXDENGINE_CONTAINER_ARRAY_H

#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Core/Internal/LexicographicalCompare.h"
#include "FXDEngine/Core/Internal/Sort.h"
#include "FXDEngine/Container/Iterator.h"

namespace FXD
{
	namespace Container
	{
		namespace Internal
		{
			// ------
		// _array_iterator
		// -
		// ------
			template < typename T, FXD::U32 SIZE, typename Pointer, typename Reference >
			class _array_iterator
			{
			public:
				using my_type = _array_iterator< T, SIZE, Pointer, Reference >;
				using value_type = T;
				using reference = Reference;
				using pointer = Pointer;
				using size_type = FXD::U32;
				using difference_type = std::ptrdiff_t;
				using iterator_category = FXD::STD::random_access_iterator_tag;

			public:
				CONSTEXPR14 _array_iterator(void);
				CONSTEXPR14 _array_iterator(pointer pType, size_type nOff = 0);
				CONSTEXPR14 _array_iterator(const my_type& rhs);

				CONSTEXPR14 my_type& operator++();
				CONSTEXPR14 my_type operator++(int);

				CONSTEXPR14 my_type& operator--();
				CONSTEXPR14 my_type operator--(int);

				CONSTEXPR14 my_type& operator+=(const difference_type nOff) NOEXCEPT;
				CONSTEXPR14 my_type& operator-=(const difference_type nOff) NOEXCEPT;

				CONSTEXPR14 my_type operator+(const difference_type nOff) const;
				CONSTEXPR14 my_type operator-(const difference_type nOff) const;

				CONSTEXPR14 reference operator*(void) const;
				CONSTEXPR14 pointer operator->(void) const;
				CONSTEXPR14 reference operator[](const difference_type nOff)const;

			private:
				CONSTEXPR14 bool _is_valid(void) const NOEXCEPT;

				// Non-member comparison functions
				// Equality
				template < typename T2, FXD::U32 SIZE2, typename PtrA, typename RefA, typename PtrB, typename RefB >
				CONSTEXPR14 friend bool operator==(const _array_iterator< T2, SIZE2, PtrA, RefA >& lhs, const _array_iterator< T2, SIZE2, PtrB, RefB >& rhs) NOEXCEPT;
				template < typename T2, FXD::U32 SIZE2, typename PtrA, typename RefA >
				CONSTEXPR14 friend bool operator==(const _array_iterator< T2, SIZE2, PtrA, RefA >& lhs, const _array_iterator< T2, SIZE2, PtrA, RefA >& rhs) NOEXCEPT;

				// Inequality
				template < typename T2, FXD::U32 SIZE2, typename PtrA, typename RefA, typename PtrB, typename RefB >
				CONSTEXPR14 friend bool operator!=(const _array_iterator< T2, SIZE2, PtrA, RefA >& lhs, const _array_iterator< T2, SIZE2, PtrB, RefB >& rhs) NOEXCEPT;
				template < typename T2, FXD::U32 SIZE2, typename PtrA, typename RefA >
				CONSTEXPR14 friend bool operator!=(const _array_iterator< T2, SIZE2, PtrA, RefA >& lhs, const _array_iterator< T2, SIZE2, PtrA, RefA >& rhs) NOEXCEPT;

				// Less than
				template < typename T2, FXD::U32 SIZE2, typename PtrA, typename RefA, typename PtrB, typename RefB >
				CONSTEXPR14 friend bool operator<(const _array_iterator< T2, SIZE2, PtrA, RefA >& lhs, const _array_iterator< T2, SIZE2, PtrB, RefB >& rhs) NOEXCEPT;

				// Greater than
				template < typename T2, FXD::U32 SIZE2, typename PtrA, typename RefA, typename PtrB, typename RefB >
				CONSTEXPR14 friend bool operator>(const _array_iterator< T2, SIZE2, PtrA, RefA >& lhs, const _array_iterator< T2, SIZE2, PtrB, RefB >& rhs) NOEXCEPT;

				// Less than or equal to
				template < typename T2, FXD::U32 SIZE2, typename PtrA, typename RefA, typename PtrB, typename RefB >
				CONSTEXPR14 friend bool operator<=(const _array_iterator< T2, SIZE2, PtrA, RefA >& lhs, const _array_iterator< T2, SIZE2, PtrB, RefB >& rhs) NOEXCEPT;

				// Greater than or equal to
				template < typename T2, FXD::U32 SIZE2, typename PtrA, typename RefA, typename PtrB, typename RefB >
				CONSTEXPR14 friend bool operator>=(const _array_iterator< T2, SIZE2, PtrA, RefA >& lhs, const _array_iterator< T2, SIZE2, PtrB, RefB >& rhs) NOEXCEPT;

				template < typename T2, FXD::U32 SIZE2, typename PtrA, typename RefA, typename PtrB, typename RefB >
				CONSTEXPR14 friend difference_type operator+(const _array_iterator< T2, SIZE2, PtrA, RefA >& lhs, const _array_iterator< T2, SIZE2, PtrB, RefB >& rhs) NOEXCEPT;

			public:
				Pointer m_pType;
				size_type m_nOff;
			};

			// ------
			// _array_iterator
			// -
			// ------
			template < typename T, FXD::U32 SIZE, typename Pointer, typename Reference >
			CONSTEXPR14 _array_iterator< T, SIZE, Pointer, Reference >::_array_iterator(void)
				: m_pType(nullptr)
				, m_nOff(0)
			{}

			template < typename T, FXD::U32 SIZE, typename Pointer, typename Reference >
			CONSTEXPR14 _array_iterator< T, SIZE, Pointer, Reference >::_array_iterator(pointer pType, size_type nOff)
				: m_pType(pType)
				, m_nOff(nOff)
			{}

			template < typename T, FXD::U32 SIZE, typename Pointer, typename Reference >
			CONSTEXPR14 _array_iterator< T, SIZE, Pointer, Reference >::_array_iterator(const my_type& rhs)
				: m_pType(rhs.m_pType)
				, m_nOff(rhs.m_nOff)
			{}

			template < typename T, FXD::U32 SIZE, typename Pointer, typename Reference >
			CONSTEXPR14 typename _array_iterator< T, SIZE, Pointer, Reference >::my_type& _array_iterator< T, SIZE, Pointer, Reference >::operator++()
			{
				++m_nOff;
				return (*this);
			}
			template < typename T, FXD::U32 SIZE, typename Pointer, typename Reference >
			CONSTEXPR14 typename _array_iterator< T, SIZE, Pointer, Reference >::my_type _array_iterator< T, SIZE, Pointer, Reference >::operator++(int)
			{
				const my_type tmp(*this);
				operator++();
				return (tmp);
			}

			template < typename T, FXD::U32 SIZE, typename Pointer, typename Reference >
			CONSTEXPR14 typename _array_iterator< T, SIZE, Pointer, Reference >::my_type& _array_iterator< T, SIZE, Pointer, Reference >::operator--()
			{
				--m_nOff;
				return (*this);
			}
			template < typename T, FXD::U32 SIZE, typename Pointer, typename Reference >
			CONSTEXPR14 typename _array_iterator< T, SIZE, Pointer, Reference >::my_type _array_iterator< T, SIZE, Pointer, Reference >::operator--(int)
			{
				const my_type tmp(*this);
				operator--();
				return (tmp);
			}

			template < typename T, FXD::U32 SIZE, typename Pointer, typename Reference >
			CONSTEXPR14 typename _array_iterator< T, SIZE, Pointer, Reference >::my_type& _array_iterator< T, SIZE, Pointer, Reference >::operator+=(const difference_type nOff) NOEXCEPT
			{
				m_nOff += nOff;
				return (*this);
			}
			template < typename T, FXD::U32 SIZE, typename Pointer, typename Reference >
			CONSTEXPR14 typename _array_iterator< T, SIZE, Pointer, Reference >::my_type& _array_iterator< T, SIZE, Pointer, Reference >::operator-=(const difference_type nOff) NOEXCEPT
			{
				m_nOff -= nOff;
				return (*this);
			}

			template < typename T, FXD::U32 SIZE, typename Pointer, typename Reference >
			CONSTEXPR14 typename _array_iterator< T, SIZE, Pointer, Reference >::my_type _array_iterator< T, SIZE, Pointer, Reference >::operator+(const difference_type nOff) const
			{
				return FXD::STD::next((*this), nOff);
			}
			template < typename T, FXD::U32 SIZE, typename Pointer, typename Reference >
			CONSTEXPR14 typename _array_iterator< T, SIZE, Pointer, Reference >::my_type _array_iterator< T, SIZE, Pointer, Reference >::operator-(const difference_type nOff) const
			{
				return FXD::STD::prev((*this), nOff);
			}

			template < typename T, FXD::U32 SIZE, typename Pointer, typename Reference >
			CONSTEXPR14 typename _array_iterator< T, SIZE, Pointer, Reference >::reference _array_iterator< T, SIZE, Pointer, Reference >::operator*(void) const
			{
				PRINT_COND_ASSERT((_is_valid()), "Container: Iterator is invalid");
				return (m_pType[m_nOff]);
			}
			template < typename T, FXD::U32 SIZE, typename Pointer, typename Reference >
			CONSTEXPR14 typename _array_iterator< T, SIZE, Pointer, Reference >::pointer _array_iterator< T, SIZE, Pointer, Reference >::operator->(void) const
			{
				return &(operator*());
			}

			template < typename T, FXD::U32 SIZE, typename Pointer, typename Reference >
			CONSTEXPR14 typename _array_iterator< T, SIZE, Pointer, Reference >::reference _array_iterator< T, SIZE, Pointer, Reference >::operator[](const difference_type nOff) const
			{
				return (*((*this) + nOff));
			}

			template < typename T, FXD::U32 SIZE, typename Pointer, typename Reference >
			CONSTEXPR14 bool _array_iterator< T, SIZE, Pointer, Reference >::_is_valid(void) const NOEXCEPT
			{
				if ((m_pType == nullptr) || (m_nOff < 0) || (m_nOff > SIZE))
				{
					return false;
				}
				return true;
			}

			// Non-member comparison functions
			// Equality
			template < typename T, FXD::U32 SIZE, typename PtrA, typename RefA, typename PtrB, typename RefB >
			CONSTEXPR14 inline bool operator==(const _array_iterator< T, SIZE, PtrA, RefA >& lhs, const _array_iterator< T, SIZE, PtrB, RefB >& rhs) NOEXCEPT
			{
				return (lhs.m_pType == rhs.m_pType) && (lhs.m_nOff == rhs.m_nOff);
			}
			template < typename T, FXD::U32 SIZE, typename PtrA, typename RefA >
			CONSTEXPR14 inline bool operator==(const _array_iterator< T, SIZE, PtrA, RefA >& lhs, const _array_iterator< T, SIZE, PtrA, RefA >& rhs) NOEXCEPT
			{
				return (lhs.m_pType == rhs.m_pType) && (lhs.m_nOff == rhs.m_nOff);
			}
			// Inequality
			template < typename T, FXD::U32 SIZE, typename PtrA, typename RefA, typename PtrB, typename RefB >
			CONSTEXPR14 inline bool operator!=(const _array_iterator< T, SIZE, PtrA, RefA >& lhs, const _array_iterator< T, SIZE, PtrB, RefB >& rhs) NOEXCEPT
			{
				return (lhs.m_pType != rhs.m_pType) || (lhs.m_nOff != rhs.m_nOff);
			}
			template < typename T, FXD::U32 SIZE, typename PtrA, typename RefA >
			CONSTEXPR14 inline bool operator!=(const _array_iterator< T, SIZE, PtrA, RefA >& lhs, const _array_iterator< T, SIZE, PtrA, RefA >& rhs) NOEXCEPT
			{
				return (lhs.m_pType != rhs.m_pType) || (lhs.m_nOff != rhs.m_nOff);
			}
			// Less than
			template < typename T, FXD::U32 SIZE, typename PtrA, typename RefA, typename PtrB, typename RefB >
			CONSTEXPR14 inline bool operator<(const _array_iterator< T, SIZE, PtrA, RefA >& lhs, const _array_iterator< T, SIZE, PtrB, RefB >& rhs) NOEXCEPT
			{
				return (&lhs.m_pType[lhs.m_nOff] < &rhs.m_pType[rhs.m_nOff]);
			}
			// Greater than
			template < typename T, FXD::U32 SIZE, typename PtrA, typename RefA, typename PtrB, typename RefB >
			CONSTEXPR14 inline bool operator>(const _array_iterator< T, SIZE, PtrA, RefA >& lhs, const _array_iterator< T, SIZE, PtrB, RefB >& rhs) NOEXCEPT
			{
				return (&lhs.m_pType[lhs.m_nOff] > &rhs.m_pType[rhs.m_nOff]);
			}
			// Less than or equal to
			template < typename T, FXD::U32 SIZE, typename PtrA, typename RefA, typename PtrB, typename RefB >
			CONSTEXPR14 inline bool operator<=(const _array_iterator< T, SIZE, PtrA, RefA >& lhs, const _array_iterator< T, SIZE, PtrB, RefB >& rhs) NOEXCEPT
			{
				return (&lhs.m_pType[lhs.m_nOff] <= &rhs.m_pType[rhs.m_nOff]);
			}
			// Greater than or equal to
			template < typename T, FXD::U32 SIZE, typename PtrA, typename RefA, typename PtrB, typename RefB >
			CONSTEXPR14 inline bool operator>=(const _array_iterator< T, SIZE, PtrA, RefA >& lhs, const _array_iterator< T, SIZE, PtrB, RefB >& rhs) NOEXCEPT
			{
				return (&lhs.m_pType[lhs.m_nOff] >= &rhs.m_pType[rhs.m_nOff]);
			}

			template < typename T, FXD::U32 SIZE, typename PtrA, typename RefA, typename PtrB, typename RefB >
			CONSTEXPR14 inline typename _array_iterator< T, SIZE, PtrA, RefA >::difference_type operator+(const _array_iterator< T, SIZE, PtrA, RefA >& lhs, const _array_iterator< T, SIZE, PtrB, RefB >& rhs) NOEXCEPT
			{
				return (&lhs.m_pType[lhs.m_nOff] + &rhs.m_pType[rhs.m_nOff]);
			}
			template < typename T, FXD::U32 SIZE, typename PtrA, typename RefA, typename PtrB, typename RefB >
			CONSTEXPR14 inline typename _array_iterator< T, SIZE, PtrA, RefA >::difference_type operator-(const _array_iterator< T, SIZE, PtrA, RefA >& lhs, const _array_iterator< T, SIZE, PtrB, RefB >& rhs) NOEXCEPT
			{
				return (&lhs.m_pType[lhs.m_nOff] - &rhs.m_pType[rhs.m_nOff]);
			}

		} //namespace Internal

		// ------
		// Array
		// -
		// ------
		template < typename T, FXD::U32 SIZE >
		class Array
		{
		public:
			using my_type = Array< T, SIZE >;
			using value_type = T;
			using pointer = T * ;
			using const_pointer = const T*;
			using reference = T & ;
			using const_reference = const T&;
			using size_type = FXD::U32;
			using difference_type = std::ptrdiff_t;

			using iterator = Internal::_array_iterator< value_type, SIZE, pointer, reference >;
			using const_iterator = Internal::_array_iterator< value_type, SIZE, const_pointer, const_reference >;
			using reverse_iterator = FXD::STD::reverse_iterator< iterator >;
			using const_reverse_iterator = FXD::STD::reverse_iterator< const_iterator >;

			friend iterator;
			friend const_iterator;
			friend reverse_iterator;
			friend const_reverse_iterator;

		public:
			// Iterator Support
			CONSTEXPR14 iterator begin(void) NOEXCEPT;
			CONSTEXPR14 const_iterator begin(void) const NOEXCEPT;
			CONSTEXPR14 const_iterator cbegin(void) const NOEXCEPT;

			CONSTEXPR14 iterator end(void) NOEXCEPT;
			CONSTEXPR14 const_iterator end(void) const NOEXCEPT;
			CONSTEXPR14 const_iterator cend(void) const NOEXCEPT;

			CONSTEXPR14 reverse_iterator rbegin(void) NOEXCEPT;
			CONSTEXPR14 const_reverse_iterator rbegin(void) const NOEXCEPT;
			CONSTEXPR14 const_reverse_iterator crbegin(void) const NOEXCEPT;

			CONSTEXPR14 reverse_iterator rend(void) NOEXCEPT;
			CONSTEXPR14 const_reverse_iterator rend(void) const NOEXCEPT;
			CONSTEXPR14 const_reverse_iterator crend(void) const NOEXCEPT;

			// Access
			CONSTEXPR14 reference operator[](size_type nPos);
			CONSTEXPR14 const_reference operator[](size_type nPos) const;

			CONSTEXPR14 reference at(size_type nPos);
			CONSTEXPR14 const_reference at(size_type nPos) const;

			CONSTEXPR14 reference front(void);
			CONSTEXPR14 const_reference front(void) const;

			CONSTEXPR14 reference back(void);
			CONSTEXPR14 const_reference back(void) const;

			CONSTEXPR14 pointer data(void) NOEXCEPT;
			CONSTEXPR14 const_pointer data(void) const NOEXCEPT;

			// Insert
			void fill(const value_type& val);
			void swap(my_type& rhs);

			void sort(void);
			template < typename Pr >
			void sort(Pr cmp);

			// Searches
			const_iterator find(const T& src) const;
			bool contains(const T& src) const;

			CONSTEXPR14 bool empty(void) const NOEXCEPT;
			CONSTEXPR14 bool not_empty(void) const NOEXCEPT;
			CONSTEXPR14 size_type size(void) const NOEXCEPT;
			CONSTEXPR14 size_type capacity(void) const NOEXCEPT;


			template < typename T2, FXD::U32 SIZE2 >
			CONSTEXPR friend bool operator==(const FXD::Container::Array< T2, SIZE2 >& lhs, const FXD::Container::Array< T2, SIZE2 >& rhs);

			template < typename T2, FXD::U32 SIZE2 >
			CONSTEXPR14 friend bool operator!=(const FXD::Container::Array< T2, SIZE2 >& lhs, const FXD::Container::Array< T2, SIZE2 >& rhs);

			template < typename T2, FXD::U32 SIZE2 >
			CONSTEXPR14 friend bool operator<(const FXD::Container::Array< T2, SIZE2 >& lhs, const FXD::Container::Array< T2, SIZE2 >& rhs);

			template < typename T2, FXD::U32 SIZE2 >
			CONSTEXPR14 friend bool operator>(const FXD::Container::Array< T2, SIZE2 >& lhs, const FXD::Container::Array< T2, SIZE2 >& rhs);

			template < typename T2, FXD::U32 SIZE2 >
			CONSTEXPR14 friend bool operator<=(const FXD::Container::Array< T2, SIZE2 >& lhs, const FXD::Container::Array< T2, SIZE2 >& rhs);

			template < typename T2, FXD::U32 SIZE2 >
			CONSTEXPR14 friend bool operator>=(const FXD::Container::Array< T2, SIZE2 >& lhs, const FXD::Container::Array< T2, SIZE2 >& rhs);

		private:
			CONSTEXPR14 pointer _first(void) NOEXCEPT;
			CONSTEXPR14 const_pointer _first(void) const NOEXCEPT;
			CONSTEXPR14 pointer _last(void) NOEXCEPT;
			CONSTEXPR14 const_pointer _last(void) const NOEXCEPT;

		public:
			T m_elems[SIZE];
		};

		// Iterator Support
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::iterator Array< Type, SIZE >::begin(void) NOEXCEPT
		{
			return iterator(_first());
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::const_iterator Array< Type, SIZE >::begin(void) const NOEXCEPT
		{
			return const_iterator(_first());
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::const_iterator Array< Type, SIZE >::cbegin(void) const NOEXCEPT
		{
			return const_iterator(_first());
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::iterator Array< Type, SIZE >::end(void) NOEXCEPT
		{
			return iterator(_first(), SIZE);
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::const_iterator Array< Type, SIZE >::end(void) const NOEXCEPT
		{
			return const_iterator(_first(), SIZE);
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::const_iterator Array< Type, SIZE >::cend(void) const NOEXCEPT
		{
			return const_iterator(_first(), SIZE);
		}

		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::reverse_iterator Array< Type, SIZE >::rbegin(void) NOEXCEPT
		{
			return reverse_iterator(iterator(end()));
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::const_reverse_iterator Array< Type, SIZE >::rbegin(void) const NOEXCEPT
		{
			return const_reverse_iterator(const_iterator(end()));
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::const_reverse_iterator Array< Type, SIZE >::crbegin(void) const NOEXCEPT
		{
			return const_reverse_iterator(const_iterator(end()));
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::reverse_iterator Array< Type, SIZE >::rend(void) NOEXCEPT
		{
			return reverse_iterator(iterator(begin()));
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::const_reverse_iterator Array< Type, SIZE >::rend(void) const NOEXCEPT
		{
			return const_reverse_iterator(const_iterator(begin()));
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::const_reverse_iterator Array< Type, SIZE >::crend(void) const NOEXCEPT
		{
			return const_reverse_iterator(const_iterator(begin()));
		}

		// Access
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::reference Array< Type, SIZE >::operator[](size_type nPos)
		{
			return at(nPos);
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::const_reference Array< Type, SIZE >::operator[](size_type nPos) const
		{
			return at(nPos);
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::reference Array< Type, SIZE >::at(size_type nPos)
		{
			return (m_elems[nPos]);
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::const_reference Array< Type, SIZE >::at(size_type nPos) const
		{
			return m_elems[nPos];
		}

		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::reference Array< Type, SIZE >::front(void)
		{
			return at(0);
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::const_reference Array< Type, SIZE >::front(void) const
		{
			return at(0);
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::reference Array< Type, SIZE >::back(void)
		{
			return at(SIZE - 1);
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::const_reference Array< Type, SIZE >::back(void) const
		{
			return at(SIZE - 1);
		}

		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::pointer Array< Type, SIZE >::data(void) NOEXCEPT
		{
			return m_elems;
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::const_pointer Array< Type, SIZE >::data(void) const NOEXCEPT
		{
			return m_elems;
		}

		// Insert
		template < typename Type, FXD::U32 SIZE >
		void Array< Type, SIZE >::fill(const value_type& val)
		{
			FXD::STD::fill_n(&m_elems[0], SIZE, val);
		}
		template < typename Type, FXD::U32 SIZE >
		void Array< Type, SIZE >::swap(my_type& rhs)
		{
			FXD::STD::swap_ranges(&m_elems[0], &m_elems[SIZE], &rhs.m_elems[0]);
		}

		template < typename Type, FXD::U32 SIZE >
		void Array< Type, SIZE >::sort(void)
		{
			sort(FXD::STD::less< Type >());
		}
		template < typename Type, FXD::U32 SIZE >
		template < typename Pr >
		void Array< Type, SIZE >::sort(Pr cmp)
		{
			FXD::STD::sort(begin(), end(), cmp, size());
		}

		// Searches
		template < typename Type, FXD::U32 SIZE >
		inline typename Array< Type, SIZE >::const_iterator Array< Type, SIZE >::find(const Type& src) const
		{
			fxd_for_itr(itr, (*this))
			{
				if ((*itr) == src)
				{
					return itr;
				}
			}
			return end();
		}
		template < typename Type, FXD::U32 SIZE >
		inline bool Array< Type, SIZE >::contains(const Type& src) const
		{
			return (find(src) != end());
		}

		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline bool Array< Type, SIZE >::empty(void) const NOEXCEPT
		{
			return (SIZE == 0);
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline bool Array< Type, SIZE >::not_empty(void) const NOEXCEPT
		{
			return (SIZE != 0);
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::size_type Array< Type, SIZE >::size(void) const NOEXCEPT
		{
			return SIZE;
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::size_type Array< Type, SIZE >::capacity(void) const NOEXCEPT
		{
			return SIZE;
		}

		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::pointer Array< Type, SIZE >::_first(void) NOEXCEPT
		{
			return &data()[0];
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::const_pointer Array< Type, SIZE >::_first(void) const NOEXCEPT
		{
			return &data()[0];
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::pointer Array< Type, SIZE >::_last(void) NOEXCEPT
		{
			return &data()[SIZE];
		}
		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline typename Array< Type, SIZE >::const_pointer Array< Type, SIZE >::_last(void) const NOEXCEPT
		{
			return &data()[SIZE];
		}

		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline bool operator==(const FXD::Container::Array< Type, SIZE >& lhs, const FXD::Container::Array< Type, SIZE >& rhs)
		{
			return FXD::STD::equal(lhs.begin(), lhs.end(), rhs.begin());
		}

		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline bool operator!=(const FXD::Container::Array< Type, SIZE >& lhs, const FXD::Container::Array< Type, SIZE >& rhs)
		{
			return (!(lhs == rhs));
		}

		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline bool operator<(const FXD::Container::Array< Type, SIZE >& lhs, const FXD::Container::Array< Type, SIZE >& rhs)
		{
			return FXD::STD::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
		}

		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline bool operator>(const FXD::Container::Array< Type, SIZE >& lhs, const FXD::Container::Array< Type, SIZE >& rhs)
		{
			return (rhs < lhs);
		}

		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline bool operator<=(const FXD::Container::Array< Type, SIZE >& lhs, const FXD::Container::Array< Type, SIZE >& rhs)
		{
			return !FXD::STD::lexicographical_compare(&rhs.m_elems[0], &rhs.m_elems[SIZE], &lhs.m_elems[0], &lhs.m_elems[SIZE]);
		}

		template < typename Type, FXD::U32 SIZE >
		CONSTEXPR14 inline bool operator>=(const FXD::Container::Array< Type, SIZE >& lhs, const FXD::Container::Array< Type, SIZE >& rhs)
		{
			return !FXD::STD::lexicographical_compare(&lhs.m_elems[0], &lhs.m_elems[SIZE], &rhs.m_elems[0], &rhs.m_elems[SIZE]);
		}

		// Inserters and extractors
		template < typename Type, FXD::U32 SIZE >
		inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, const Container::Array< Type, SIZE >& vec)
		{
			fxd_for(auto& i, vec)
			{
				ostr << " " << i;
			}
			return ostr;
		}
	} //namespace Container

	namespace STD
	{
		template < typename Type, FXD::U32 SIZE >
		inline void swap(FXD::Container::Array< Type, SIZE >& lhs, FXD::Container::Array< Type, SIZE >& rhs)
		{
			FXD::STD::swap_ranges(&lhs.m_elems[0], &lhs.m_elems[SIZE], &rhs.m_elems[0]);
		}

		namespace Internal
		{
			template < typename Type, size_t N, size_t... I >
			CONSTEXPR auto _to_array(Type(&a)[N], FXD::STD::index_sequence< I... >)
			{
				return FXD::Container::Array< FXD::STD::remove_cv_t< Type >, N >{ {a[I]...}};
			}

			template < typename Type, size_t N, size_t... I >
			CONSTEXPR auto _to_array(Type(&&a)[N], FXD::STD::index_sequence< I... >)
			{
				return FXD::Container::Array< FXD::STD::remove_cv_t< Type >, N >{ {std::move(a[I])...}};
			}
		}

		template < typename Type, size_t N >
		CONSTEXPR FXD::Container::Array< FXD::STD::remove_cv_t< Type >, N > to_array(Type(&a)[N])
		{
			static_assert(FXD::STD::is_constructible_v< Type, Type& >, "element type Type must be copy-initializable");
			static_assert(!FXD::STD::is_array_v< Type >, "passing multidimensional arrays to to_array is ill-formed");
			return FXD::STD::Internal::_to_array(a, FXD::STD::make_index_sequence< N >{});
		}

		template < typename Type, size_t N >
		CONSTEXPR FXD::Container::Array< FXD::STD::remove_cv_t< Type >, N > to_array(Type(&&a)[N])
		{
			static_assert(FXD::STD::is_move_constructible_v< Type >, "element type Type must be move-constructible");
			static_assert(!FXD::STD::is_array_v< Type >, "passing multidimensional arrays to to_array is ill-formed");
			return FXD::STD::Internal::_to_array(std::move(a), FXD::STD::make_index_sequence< N >{});
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_ARRAY_H