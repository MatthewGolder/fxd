// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_VECMAP_H
#define FXDENGINE_CONTAINER_VECMAP_H

#include "FXDEngine/Container/Vector.h"
#include "FXDEngine/Core/Utility.h"

namespace FXD
{
	namespace Container
	{
		// ------
		// VecMap
		// -
		// ------
		template < typename K, typename V >
		class VecMap
		{
		public:
			VecMap(void)
			{}
			~VecMap(void)
			{}

			Pair< K, V >& at(FXD::U16 nID) { return m_mapTable[nID]; }
			const Pair< K, V >& at(FXD::U16 nID) const { return m_mapTable[nID]; }
			Pair< K, V >& operator[](FXD::U16 nID) { return at(nID); }
			const Pair< K, V >& operator[](FXD::U16 nID) const { return at(nID); }

			void insert(const Pair< K, V > entry)
			{
				if (findKey(entry.m_first) == -1)
				{
					m_mapTable.push_back(entry);
				}
			}
			void insert(const K& key, const V& value)
			{
				Pair< K, V > entry(key, value);
				if (findKey(entry.m_first) == -1)
				{
					m_mapTable.push_back(entry);
				}
			}
			FXD::S32 findKey(const K& key) const
			{
				FXD::U32 nSize = m_mapTable.size();
				for (FXD::U16 c1 = 0; c1 < nSize; c1++)
				{
					if (m_mapTable[c1].m_first.compare_no_case(key) == 0)
					{
						return c1;
					}
				}
				return -1;
			}
			const bool contains(const K& key) const
			{
				return (findKey(key) >= 0);
			}
			const bool compare(const Pair< K, V >& lhs, const Pair< K, V >& rhs) const
			{
				return (lhs == rhs);
			}
			const bool compareKey(const K& lhs, const K& rhs) const
			{
				return (lhs == rhs);
			}
			const bool compareValue(const V& lhs, const V& rhs) const
			{
				return (lhs == rhs);
			}
			void find_erase(const K& k)
			{
				m_mapTable.find_erase(k);
			}
			void clear(void)
			{
				m_mapTable.clear();
			}

			FXD::U16 size(void) const
			{
				return m_mapTable.size();
			}
			FXD::U16 capacity(void) const
			{
				return m_mapTable.capacity();
			}

		private:
			Container::Vector< Pair< K, V > > m_mapTable;
		};

	} //namespace Container
} //namespace FXD
#endif //FXDENGINE_CONTAINER_VECMAP_H