// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_ANY_H
#define FXDENGINE_CONTAINER_ANY_H

#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Core/Internal/AlignmentOf.h"
#include "FXDEngine/Core/Internal/BadAnyCast.h"
#include "FXDEngine/Core/Internal/Conditional.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Decay.h"
#include "FXDEngine/Core/Internal/InPlaceT.h"
#include "FXDEngine/Core/Internal/IsConstructible.h"
#include "FXDEngine/Core/Internal/IsCopyConstructible.h"
#include "FXDEngine/Core/Internal/IsNoThrowMoveConstructible.h"
#include "FXDEngine/Core/Internal/Swap.h"
#include "FXDEngine/Core/Internal/RemoveCV.h"
#include "FXDEngine/Core/Internal/RemoveReference.h"
#include "FXDEngine/Memory/New.h"
#include "FXDEngine/Memory/Allocator/Aligned.h"

#include <initializer_list>
#include <type_traits>

namespace FXD
{
	namespace Container
	{
		class Any;

		namespace Internal
		{
			inline void AnyDoBadCast(void)
			{
				throw FXD::STD::bad_any_cast();
			}

			template < typename T, typename... Args >
			void* _AnyDefaultConstruct(Args&&... args)
			{
				void* pMem = Memory::Alloc::Default.allocate(sizeof(T));
				return FXD_PLACEMENT_NEW(pMem, T)(std::forward< Args >(args)...);
			}

			template < typename T >
			void _AnyDefaultDestroy(T* pData)
			{
				pData->~T();
				Memory::Alloc::Default.deallocate(pData, sizeof(T));
				pData = nullptr;
			}

			enum class E_StorageOperation : FXD::U8
			{
				Get,
				Destroy,
				Copy,
				Move,
				TypeInfo
			};

			union _AnyStorage
			{
				typedef std::aligned_storage_t< 4 * sizeof(void*), FXD::STD::alignment_of< void* >::value > internal_storage_type;

				void* m_pExternalStorage = nullptr;
				internal_storage_type m_internalStorage;
			};

			template < typename T >
			using _AnyInternalStorage = FXD::STD::integral_constant< bool,
					FXD::STD::is_nothrow_move_constructible< T >::value &&
					(sizeof(T) <= sizeof(Internal::_AnyStorage)) &&
					(FXD::STD::alignment_of< Internal::_AnyStorage >::value % FXD::STD::alignment_of< T >::value == 0)
				>;

			// ------
			// _AnyStorageHandlerInternal
			// -
			// ------
			template < typename T >
			struct _AnyStorageHandlerInternal
			{
				template < typename V >
				static void construct(Internal::_AnyStorage& storage, V&& val);

				template < typename... Args >
				static void construct_inplace(Internal::_AnyStorage& storage, Args... args);

				template < typename NT, typename U, typename... Args >
				static void construct_inplace(Internal::_AnyStorage& storage, std::initializer_list< U > iList, Args&&... args);

				static inline void destroy(FXD::Container::Any& refAny);

				static void* handler_func(E_StorageOperation eOp, const FXD::Container::Any* pThis, FXD::Container::Any* pOther);
			};

			// ------
			// _AnyStorageHandlerExternal
			// -
			// ------
			template < typename T >
			struct _AnyStorageHandlerExternal
			{
				template < typename V >
				static inline void construct(Internal::_AnyStorage& storage, V&& val);

				template < typename... Args >
				static inline void construct_inplace(Internal::_AnyStorage& storage, Args... args);

				template < typename NT, typename U, typename... Args >
				static inline void construct_inplace(Internal::_AnyStorage& storage, std::initializer_list< U > iList, Args&&... args);

				static inline void destroy(FXD::Container::Any& refAny);

				static void* handler_func(E_StorageOperation eOp, const FXD::Container::Any* pThis, FXD::Container::Any* pOther);
			};

			using _AnyStorageHandlerPtr = void* (*)(E_StorageOperation, const FXD::Container::Any*, FXD::Container::Any*);

			template < typename T >
			using _AnyStorageHandler = typename FXD::STD::conditional< Internal::_AnyInternalStorage< T >::value,
				Internal::_AnyStorageHandlerInternal< T >,
				Internal::_AnyStorageHandlerExternal< T > >::type;
		} //namespace Internal
	} //namespace Container

	namespace STD
	{
		template < typename T >
		const T* any_cast(const FXD::Container::Any* pAny) NOEXCEPT;

		template < typename T >
		T* any_cast(FXD::Container::Any* pAny) NOEXCEPT;

		template < typename T >
		T any_cast(const FXD::Container::Any& operand);

		template < typename T >
		T any_cast(FXD::Container::Any& operand);

		template < typename T >
		T any_cast(FXD::Container::Any&& operand);
	} //namespace STD

	namespace Container
	{
		// ------
		// Any
		// -
		// ------
		class Any
		{
		public:
			template < typename T >
			friend struct Internal::_AnyStorageHandlerInternal;
			template < typename T >
			friend struct Internal::_AnyStorageHandlerExternal;

			template < typename T >
			friend const T* FXD::STD::any_cast(const FXD::Container::Any* pAny) NOEXCEPT;
			template < typename T >
			friend T* FXD::STD::any_cast(FXD::Container::Any* pAny) NOEXCEPT;
			template < typename T >
			friend T FXD::STD::any_cast(const FXD::Container::Any& operand);
			template < typename T >
			friend T FXD::STD::any_cast(FXD::Container::Any& operand);
			template < typename T >
			friend T FXD::STD::any_cast(FXD::Container::Any&& operand);

		public:
			Any(void) NOEXCEPT;
			template < typename T >
			Any(T&& val, typename FXD::STD::enable_if< !FXD::STD::is_same< typename FXD::STD::decay< T >::type, Container::Any >::value >::type* = 0);
			template < typename T, typename... Args >
			EXPLICIT Any(FXD::STD::in_place_type_t< T >, Args&&... args);
			template < typename T, typename U, typename... Args >
			Any(FXD::STD::in_place_type_t< T >, std::initializer_list< U > il, Args&&... args, typename FXD::STD::enable_if< FXD::STD::is_constructible< T, std::initializer_list< U >&, Args... >::value, void >::type* = 0);

			~Any(void);

			Any(const Any& rhs);
			Any(Any&& rhs) NOEXCEPT;

			Any& operator=(const Any& rhs);
			Any& operator=(Any&& rhs) NOEXCEPT;
			template < typename T >
			Any& operator=(T&& val);

			void reset(void) NOEXCEPT;
			void swap(Any& rhs) NOEXCEPT;
			bool has_value(void) const NOEXCEPT;
			const std::type_info& type(void) const NOEXCEPT;

#if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED
			template < typename T, typename... Args >
			void emplace(Args&&... args);
			template < typename NT, typename U, typename... Args >
			typename FXD::STD::enable_if< FXD::STD::is_constructible< NT, std::initializer_list< U >&, Args... >::value, void >::type emplace(std::initializer_list< U > iList, Args&&... args);
#endif
		private:
			Internal::_AnyStorage m_storage;
			Internal::_AnyStorageHandlerPtr m_handler;
		};

		// ------
		// Any
		// -
		// ------
		inline Any::Any(void) NOEXCEPT
			: m_storage()
			, m_handler(nullptr)
		{}

		template < typename T >
		inline Any::Any(T&& val, typename FXD::STD::enable_if< !FXD::STD::is_same< typename FXD::STD::decay< T >::type, Container::Any >::value >::type*)
		{
			using decayed_type = FXD::STD::decay_t< T >;
			using storage_handler_type = Internal::_AnyStorageHandler< decayed_type >;

			CTC_ASSERT(FXD::STD::is_copy_constructible< decayed_type >::value, "Container: T must be copy-constructible");

			storage_handler_type::construct(m_storage, std::forward< T >(val));
			m_handler = &storage_handler_type::handler_func;
		}

		template < typename T, typename... Args >
		inline Any::Any(FXD::STD::in_place_type_t< T >, Args&&... args)
		{
			using decayed_type = FXD::STD::decay_t< T >;
			using storage_handler_type = Internal::_AnyStorageHandler< decayed_type >;

			CTC_ASSERT((FXD::STD::is_constructible< T, Args... >::value), "Container: T must be constructible with Args...");

			storage_handler_type::construct_inplace(m_storage, std::forward< Args >(args)...);
			m_handler = &storage_handler_type::handler_func;
		}

		template < typename T, typename U, typename... Args >
		inline Any::Any(FXD::STD::in_place_type_t< T >, std::initializer_list< U > il, Args&&... args, typename FXD::STD::enable_if< FXD::STD::is_constructible< T, std::initializer_list< U >&, Args... >::value, void >::type*)
		{
			using decayed_type = FXD::STD::decay_t< T >;
			using storage_handler_type = Internal::_AnyStorageHandler< decayed_type >;

			storage_handler_type::construct_inplace(m_storage, il, std::forward< Args >(args)...);
			m_handler = &storage_handler_type::handler_func;
		}

		inline Any::~Any(void)
		{
			reset();
		}

		inline Any::Any(const Any& rhs)
			: m_handler(nullptr)
		{
			if (rhs.m_handler)
			{
				rhs.m_handler(Internal::E_StorageOperation::Copy, &rhs, this);
				m_handler = rhs.m_handler;
			}
		}

		inline Any::Any(Any&& rhs) NOEXCEPT
			: m_handler(nullptr)
		{
			if (rhs.m_handler)
			{
				m_handler = std::move(rhs.m_handler);
				rhs.m_handler(Internal::E_StorageOperation::Move, &rhs, this);
			}
		}

		inline Any& Any::operator=(const Any& rhs)
		{
			Any(rhs).swap(*this);
			return (*this);
		}

		inline Any& Any::operator=(Any&& rhs) NOEXCEPT
		{
			Any(std::move(rhs)).swap(*this);
			return (*this);
		}

		template < typename T >
		inline Any& Any::operator=(T&& val)
		{
			CTC_ASSERT(FXD::STD::is_copy_constructible< FXD::STD::decay_t< T > >::value, "Container: T must be copy-constructible");

			Any(std::forward< T >(val)).swap(*this);
			return (*this);
		}

#if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED
		template < typename T, typename... Args >
		inline void Any::emplace(Args&&... args)
		{
			using decayed_type = FXD::STD::decay_t< T >;
			using storage_handler_type = Internal::_AnyStorageHandler< decayed_type >;

			CTC_ASSERT((FXD::STD::is_constructible< T, Args... >::value), "Container: T must be constructible with Args...");

			reset();
			storage_handler_type::construct_inplace(m_storage, std::forward< Args >(args)...);
			m_handler = &storage_handler_type::handler_func;
		}

		template < typename NT, typename U, typename... Args >
		inline typename FXD::STD::enable_if< FXD::STD::is_constructible< NT, std::initializer_list< U >&, Args... >::value, void >::type Any::emplace(std::initializer_list< U > iList, Args&&... args)
		{
			using decayed_type = FXD::STD::decay_t< NT >;
			using storage_handler_type = Internal::_AnyStorageHandler< decayed_type >;

			reset();
			storage_handler_type::construct_inplace(m_storage, iList, std::forward< Args >(args)...);
			m_handler = &storage_handler_type::handler_func;
		}
#endif

		inline void Any::reset(void) NOEXCEPT
		{
			if (m_handler)
			{
				m_handler(Internal::E_StorageOperation::Destroy, this, nullptr);
			}
		}

		inline void Any::swap(Any& rhs) NOEXCEPT
		{
			if (this == &rhs)
			{
				return;
			}

			if (m_handler && rhs.m_handler)
			{
				Any tmp;
				tmp.m_handler = rhs.m_handler;
				rhs.m_handler(Internal::E_StorageOperation::Move, &rhs, &tmp);

				rhs.m_handler = m_handler;
				m_handler(Internal::E_StorageOperation::Move, this, &rhs);

				m_handler = tmp.m_handler;
				tmp.m_handler(Internal::E_StorageOperation::Move, &tmp, this);
			}
			else if (m_handler == nullptr && rhs.m_handler)
			{
				FXD::STD::swap(m_handler, rhs.m_handler);
				m_handler(Internal::E_StorageOperation::Move, &rhs, this);
			}
			else if (m_handler && rhs.m_handler == nullptr)
			{
				FXD::STD::swap(m_handler, rhs.m_handler);
				rhs.m_handler(Internal::E_StorageOperation::Move, this, &rhs);
			}
			//else if (m_handler == nullptr && rhs.m_handler == nullptr)
			//{
			//	// nothing to swap 
			//}
		}

		inline bool Any::has_value(void) const NOEXCEPT
		{
			return (m_handler != nullptr);
		}

		inline const std::type_info& Any::type(void) const NOEXCEPT
		{
			if (m_handler)
			{
				auto* pTypeInfo = m_handler(Internal::E_StorageOperation	::TypeInfo, this, nullptr);
				return *static_cast< const std::type_info* >(pTypeInfo);
			}
			else
			{
				return typeid(void);
			}
		}

		namespace Internal
		{
			// ------
			// _AnyStorageHandlerInternal
			// -
			// ------
			template < typename T >
			template < typename V >
			inline void _AnyStorageHandlerInternal< T >::construct(Internal::_AnyStorage& storage, V&& val)
			{
				FXD_PLACEMENT_NEW(&storage.m_internalStorage, T)(std::forward< V >(val));
			}

			template < typename T >
			template < typename... Args >
			inline void _AnyStorageHandlerInternal< T >::construct_inplace(Internal::_AnyStorage& storage, Args... args)
			{
				FXD_PLACEMENT_NEW(&storage.m_internalStorage, T)(std::forward< Args >(args)...);
			}

			template < typename T >
			template < typename NT, typename U, typename... Args >
			inline void _AnyStorageHandlerInternal< T >::construct_inplace(Internal::_AnyStorage& storage, std::initializer_list< U > iList, Args&&... args)
			{
				FXD_PLACEMENT_NEW(&storage.m_internalStorage, NT)(std::forward< Args >(args)...);
			}

			template < typename T >
			inline void _AnyStorageHandlerInternal< T >::destroy(FXD::Container::Any& refAny)
			{
				T& t = *static_cast<T*>(static_cast<void*>(&refAny.m_storage.m_internalStorage));
				t.~T();
				refAny.m_handler = nullptr;
			}

			template < typename T >
			void* _AnyStorageHandlerInternal< T >::handler_func(E_StorageOperation eOp, const FXD::Container::Any* pThis, FXD::Container::Any* pOther)
			{
				switch (eOp)
				{
					case E_StorageOperation::Get:
					{
						PRINT_COND_ASSERT(pThis != nullptr, "Container: pThis is nullptr");
						return (void*)(&pThis->m_storage.m_internalStorage);
						break;
					}
					case E_StorageOperation::Destroy:
					{
						PRINT_COND_ASSERT(pThis != nullptr, "Container: pThis is nullptr");
						destroy(const_cast< FXD::Container::Any& >(*pThis));
						break;
					}
					case E_StorageOperation::Copy:
					{
						PRINT_COND_ASSERT(pThis != nullptr, "Container: pThis is nullptr");
						PRINT_COND_ASSERT(pOther != nullptr, "Container: pOther is nullptr");
						construct(pOther->m_storage, *(T*)(&pThis->m_storage.m_internalStorage));
						break;
					}
					case E_StorageOperation::Move:
					{
						PRINT_COND_ASSERT(pThis != nullptr, "Container: pThis is nullptr");
						PRINT_COND_ASSERT(pOther != nullptr, "Container: pOther is nullptr");
						construct(pOther->m_storage, std::move(*(T*)(&pThis->m_storage.m_internalStorage)));
						destroy(const_cast< FXD::Container::Any& >(*pThis));
						break;
					}

					case E_StorageOperation::TypeInfo:
					{
						return (void*)&typeid(T);
					}
					default:
					{
						PRINT_ASSERT << "Container: Unknown storage operation";
						break;
					}
				};
				return nullptr;
			}

			// ------
			// _AnyStorageHandlerExternal
			// -
			// ------
			template < typename T >
			template < typename V >
			inline void _AnyStorageHandlerExternal< T >::construct(Internal::_AnyStorage& storage, V&& val)
			{
				storage.m_pExternalStorage = Internal::_AnyDefaultConstruct< T >(std::forward< V >(val));
			}

			template < typename T >
			template < typename... Args >
			inline void _AnyStorageHandlerExternal< T >::construct_inplace(Internal::_AnyStorage& storage, Args... args)
			{
				storage.m_pExternalStorage = Internal::_AnyDefaultConstruct< T >(std::forward< Args >(args)...);
			}

			template < typename T >
			template < typename NT, typename U, typename... Args >
			inline void _AnyStorageHandlerExternal< T >::construct_inplace(Internal::_AnyStorage& storage, std::initializer_list< U > iList, Args&&... args)
			{
				storage.m_pExternalStorage = Internal::_AnyDefaultConstruct< NT >(iList, std::forward< Args >(args)...);
			}

			template < typename T >
			inline void _AnyStorageHandlerExternal< T >::destroy(FXD::Container::Any& refAny)
			{
				Internal::_AnyDefaultDestroy(static_cast< T* >(refAny.m_storage.m_pExternalStorage));
				refAny.m_handler = nullptr;
			}

			template < typename T >
			void* _AnyStorageHandlerExternal< T >::handler_func(E_StorageOperation eOp, const FXD::Container::Any* pThis, FXD::Container::Any* pOther)
			{
				switch (eOp)
				{
					case E_StorageOperation::Get:
					{
						PRINT_COND_ASSERT(pThis != nullptr, "Container: pThis is nullptr");
						PRINT_COND_ASSERT(pThis->m_storage.m_pExternalStorage != nullptr, "Container: pThis->m_storage.m_pExternalStorage is nullptr");
						return static_cast< void* >(pThis->m_storage.m_pExternalStorage);
					}
					case E_StorageOperation::Destroy:
					{
						PRINT_COND_ASSERT(pThis != nullptr, "Container: pThis is nullptr");
						destroy(*const_cast< FXD::Container::Any* >(pThis));
					}
					case E_StorageOperation::Copy:
					{
						PRINT_COND_ASSERT(pThis != nullptr, "Container: pThis is nullptr");
						PRINT_COND_ASSERT(pOther != nullptr, "Container: pThis is nullptr");
						construct(pOther->m_storage, *static_cast< T* >(pThis->m_storage.m_pExternalStorage));
						break;
					}
					case E_StorageOperation::Move:
					{
						PRINT_COND_ASSERT(pThis != nullptr, "Container: pThis is nullptr");
						PRINT_COND_ASSERT(pOther != nullptr, "Container: pThis is nullptr");
						construct(pOther->m_storage, std::move(*(T*)(pThis->m_storage.m_pExternalStorage)));
						destroy(const_cast< FXD::Container::Any& >(*pThis));
						break;
					}
					case E_StorageOperation::TypeInfo:
					{
						return (void*)&typeid(T);
						break;
					}
					default:
					{
						PRINT_ASSERT << "Container: Unknown storage operation";
						break;
					}
				};
				return nullptr;
			}
		} //namespace Internal
	} //namespace Container

	namespace STD
	{
		inline void swap(FXD::Container::Any& rhs, FXD::Container::Any& lhs) NOEXCEPT
		{
			rhs.swap(lhs);
		}

		template < typename T >
		inline const T* any_cast(const FXD::Container::Any* pAny) NOEXCEPT
		{
			if (	pAny != nullptr &&
					pAny->m_handler == &Container::Internal::_AnyStorageHandler< FXD::STD::decay_t< T > >::handler_func &&
					pAny->type() == typeid(typename FXD::STD::remove_reference< T >::type))
			{
				return static_cast< const T* >(pAny->m_handler(Container::Internal::E_StorageOperation::Get, pAny, nullptr));
			}
			else
			{
				return nullptr;
			}
		}

		template < typename T >
		inline T* any_cast(FXD::Container::Any* pAny) NOEXCEPT
		{
			if (	pAny != nullptr &&
					pAny->m_handler == &Container::Internal::_AnyStorageHandler< FXD::STD::decay_t< T > >::handler_func &&
					pAny->type() == typeid(typename FXD::STD::remove_reference< T >::type))
			{
				return static_cast< T* >(pAny->m_handler(Container::Internal::E_StorageOperation::Get, pAny, nullptr));
			}
			else
			{
				return nullptr;
			}
		}

		template < typename T >
		inline T any_cast(const FXD::Container::Any& operand)
		{
			CTC_ASSERT(FXD::STD::is_reference< T >::value || FXD::STD::is_copy_constructible< T >::value, "Container: T must be a reference or copy constructible");

			auto* pData = FXD::STD::any_cast< typename FXD::STD::add_const< typename FXD::STD::remove_reference< T >::type >::type >(&operand);
			if (pData == nullptr)
			{
				Container::Internal::AnyDoBadCast();
			}

			return (*pData);
		}

		template < typename T >
		inline T any_cast(FXD::Container::Any& operand)
		{
			CTC_ASSERT(FXD::STD::is_reference< T >::value || FXD::STD::is_copy_constructible< T >::value, "Container: T must be a reference or copy constructible");

			auto* pData = FXD::STD::any_cast< typename FXD::STD::remove_reference< T >::type >(&operand);
			if (pData == nullptr)
			{
				Container::Internal::AnyDoBadCast();
			}

			return (*pData);
		}

		template < typename T >
		inline T any_cast(FXD::Container::Any&& operand)
		{
			CTC_ASSERT(FXD::STD::is_reference< T >::value || FXD::STD::is_copy_constructible< T >::value, "Container: T must be a reference or copy constructible");

			auto* pData = FXD::STD::any_cast< typename FXD::STD::remove_reference< T >::type >(&operand);
			if (pData == nullptr)
			{
				Container::Internal::AnyDoBadCast();
			}

			return (*pData);
		}

#if FXD_COMPILER_VARIADIC_TEMPLATES_ENABLED
		template < typename T, typename... Args >
		inline FXD::Container::Any make_any(Args&&... args)
		{
			return FXD::Container::Any(FXD::STD::in_place_type< T >, std::forward< Args >(args)...);
		}

		template < typename T, typename U, typename... Args >
		inline FXD::Container::Any make_any(std::initializer_list< U > iList, Args&&... args)
		{
			return FXD::Container::Any(FXD::STD::in_place_type< T >, iList, std::forward< Args >(args)...);
		}
#endif
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_ANY_H