// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_DEQUE_H
#define FXDENGINE_CONTAINER_DEQUE_H

#include "FXDEngine/Core/Algorithm.h"
#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Core/TypeTraits.h"
#include "FXDEngine/Container/Iterator.h"
#include "FXDEngine/Memory/Allocator/Aligned.h"
#include "FXDEngine/Memory/Memory.h"
#include <initializer_list>

namespace FXD
{
	namespace Container
	{
		namespace Internal
		{
			template < typename, typename, FXD::U32 >
			class _DequeBase;

			enum class E_Side : FXD::U8
			{
				Front,	// Identifies the front side of the Deque.
				Back	// Identifies the back side of the Deque.
			};

			const size_t kMinPtrArraySize = 8;

#			define DEQUE_SUBARRAY_SIZE(T) ((sizeof(T) <= 4) ? 64 : ((sizeof(T) <= 8) ? 32 : ((sizeof(T) <= 16) ? 16 : ((sizeof(T) <= 32) ? 8 : 4))))
		} //namespace Internal

		// ------
		// _deque_iterator
		// -
		// ------
		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		class _deque_iterator
		{
		public:
			template < typename, typename, typename, FXD::U32 >
			friend class _deque_iterator;

			template < typename, typename, FXD::U32 >
			friend class Internal::_DequeBase;

			template < typename, typename, FXD::U32>
			friend class Deque;

		public:

			using my_type = _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >;
			using value_type = T;
			using reference = T&;
			using pointer = T*;
			using size_type = FXD::U32;
			using difference_type = std::ptrdiff_t;
			using iterator_category = FXD::STD::random_access_iterator_tag;
			using iterator = _deque_iterator< T, T*, T&, kDequeSubarraySize >;
			using const_iterator = _deque_iterator< T, const T*, const T&, kDequeSubarraySize >;

		public:
			_deque_iterator(void);
			_deque_iterator(const iterator& rhs);

			my_type& operator++();
			my_type operator++(int);

			my_type& operator--();
			my_type operator--(int);

			my_type& operator+=(difference_type nDiff) NOEXCEPT;
			my_type& operator-=(difference_type nDiff) NOEXCEPT;

			my_type operator+(difference_type nDiff) const;
			my_type operator-(difference_type nDiff) const;

			reference operator*(void) const;
			pointer operator->(void) const;

		protected:
			struct Increment
			{};
			struct Decrement
			{};
			struct FromConst
			{};
			_deque_iterator(value_type** pCurrentArrayPtr, value_type* pCurrent);
			_deque_iterator(const const_iterator& rhs, FromConst);
			_deque_iterator(const iterator& rhs, Increment);
			_deque_iterator(const iterator& rhs, Decrement);

			// Non-member comparison functions
			// Equality
			template < typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB, FXD::U32 kDequeSubarraySizeU >
			friend bool operator==(const _deque_iterator< T2, PtrA, RefA, kDequeSubarraySizeU >& lhs, const _deque_iterator< T2, PtrB, RefB, kDequeSubarraySizeU>& rhs) NOEXCEPT;
			template < typename T2, typename PtrA, typename RefA, FXD::U32 kDequeSubarraySizeU >
			friend bool operator==(const _deque_iterator< T2, PtrA, RefA, kDequeSubarraySizeU >& lhs, const _deque_iterator< T2, PtrA, RefA, kDequeSubarraySizeU>& rhs) NOEXCEPT;
			// Inequality
			template < typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB, FXD::U32 kDequeSubarraySizeU >
			friend bool operator!=(const _deque_iterator< T2, PtrA, RefA, kDequeSubarraySizeU>& lhs, const _deque_iterator< T2, PtrB, RefB, kDequeSubarraySizeU>& rhs) NOEXCEPT;
			template < typename T2, typename PtrA, typename RefA, FXD::U32 kDequeSubarraySizeU >
			friend bool operator!=(const _deque_iterator< T2, PtrA, RefA, kDequeSubarraySizeU >& lhs, const _deque_iterator< T2, PtrA, RefA, kDequeSubarraySizeU >& rhs) NOEXCEPT;
			// Less than
			template < typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB, FXD::U32 kDequeSubarraySizeU >
			friend bool operator<(const _deque_iterator< T2, PtrA, RefA, kDequeSubarraySizeU >& lhs, const _deque_iterator< T2, PtrB, RefB, kDequeSubarraySizeU >& rhs) NOEXCEPT;
			// Greater than
			template < typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB, FXD::U32 kDequeSubarraySizeU >
			friend bool operator>(const _deque_iterator< T2, PtrA, RefA, kDequeSubarraySizeU >& lhs, const _deque_iterator< T2, PtrB, RefB, kDequeSubarraySizeU >& rhs) NOEXCEPT;
			// Less than or equal to
			template < typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB, FXD::U32 kDequeSubarraySizeU >
			friend bool operator<=(const _deque_iterator< T2, PtrA, RefA, kDequeSubarraySizeU>& lhs, const _deque_iterator< T2, PtrB, RefB, kDequeSubarraySizeU>& rhs) NOEXCEPT;
			// Greater than or equal to
			template < typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB, FXD::U32 kDequeSubarraySizeU >
			friend bool operator>=(const _deque_iterator< T2, PtrA, RefA, kDequeSubarraySizeU>& lhs, const _deque_iterator< T2, PtrB, RefB, kDequeSubarraySizeU>& rhs) NOEXCEPT;

			template < typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB, FXD::U32 kDequeSubarraySizeU >
			friend typename _deque_iterator< T2, PtrA, RefA, kDequeSubarraySizeU >::difference_type operator-(const _deque_iterator< T2, PtrA, RefA, kDequeSubarraySizeU >& lhs, const _deque_iterator< T2, PtrB, RefB, kDequeSubarraySizeU >& rhs);

		protected:
			my_type _copy(const iterator& first, const iterator& last, FXD::STD::true_type);
			my_type _copy(const iterator& first, const iterator& last, FXD::STD::false_type);

			void _copy_backward(const iterator& first, const iterator& last, FXD::STD::true_type);
			void _copy_backward(const iterator& first, const iterator& last, FXD::STD::false_type);
			void _set_subarray(value_type** pCurrentArrayPtr);

		protected:
			value_type* m_pCurrent;			// Where we currently point.
			value_type* m_pBegin;			// The beginning of the current subarray.
			value_type* m_pEnd;				// The end of the current subarray.
			value_type** m_pCurrentArrayPtr;// Pointer to current subarray.
		};

		// ------
		// _deque_iterator
		// -
		// ------
		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		_deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::_deque_iterator(void)
			: m_pCurrent(nullptr)
			, m_pBegin(nullptr)
			, m_pEnd(nullptr)
			, m_pCurrentArrayPtr(nullptr)
		{}

		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		_deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::_deque_iterator(const iterator& rhs)
			: m_pCurrent(rhs.m_pCurrent)
			, m_pBegin(rhs.m_pBegin)
			, m_pEnd(rhs.m_pEnd)
			, m_pCurrentArrayPtr(rhs.m_pCurrentArrayPtr)
		{}

		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		_deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::_deque_iterator(value_type** pCurrentArrayPtr, value_type* pCurrent)
			: m_pCurrent(pCurrent)
			, m_pBegin(*pCurrentArrayPtr)
			, m_pEnd(pCurrent + kDequeSubarraySize)
			, m_pCurrentArrayPtr(pCurrentArrayPtr)
		{}

		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		_deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::_deque_iterator(const const_iterator& rhs, FromConst)
			: m_pCurrent(rhs.m_pCurrent)
			, m_pBegin(rhs.m_pBegin)
			, m_pEnd(rhs.m_pEnd)
			, m_pCurrentArrayPtr(rhs.m_pCurrentArrayPtr)
		{}

		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		_deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::_deque_iterator(const iterator& rhs, Increment)
			: m_pCurrent(rhs.m_pCurrent)
			, m_pBegin(rhs.m_pBegin)
			, m_pEnd(rhs.m_pEnd)
			, m_pCurrentArrayPtr(rhs.m_pCurrentArrayPtr)
		{
			operator++();
		}

		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		_deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::_deque_iterator(const iterator& rhs, Decrement)
			: m_pCurrent(rhs.m_pCurrent)
			, m_pBegin(rhs.m_pBegin)
			, m_pEnd(rhs.m_pEnd)
			, m_pCurrentArrayPtr(rhs.m_pCurrentArrayPtr)
		{
			operator--();
		}

		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		typename _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::my_type& _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::operator++()
		{
			if (++m_pCurrent == m_pEnd)
			{
				m_pBegin = *(++m_pCurrentArrayPtr);
				m_pEnd = (m_pBegin + kDequeSubarraySize);
				m_pCurrent = m_pBegin;
			}
			return (*this);
		}

		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		typename _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::my_type _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::operator++(int)
		{
			const my_type tmp(*this);
			operator++();
			return tmp;
		}

		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		typename _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::my_type& _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::operator--()
		{
			if (m_pCurrent == m_pBegin)
			{
				m_pBegin = *(--m_pCurrentArrayPtr);
				m_pEnd = (m_pBegin + kDequeSubarraySize);
				m_pCurrent = m_pEnd;
			}
			--m_pCurrent;
			return (*this);
		}

		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		typename _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::my_type _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::operator--(int)
		{
			const my_type tmp(*this);
			operator--();
			return tmp;
		}

		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		typename _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::my_type& _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::operator+=(difference_type nDiff) NOEXCEPT
		{
			const difference_type subarrayPosition = (m_pCurrent - m_pBegin) + nDiff;

			if ((size_t)subarrayPosition < (size_t)kDequeSubarraySize)
			{
				m_pCurrent += nDiff;
			}
			else
			{
				PRINT_COND_ASSERT((kDequeSubarraySize & (kDequeSubarraySize - 1)) == 0, "Container: Verify that it is a power of 2");

				const difference_type nSubarrayIdx = (((16777216 + subarrayPosition) / (difference_type)kDequeSubarraySize)) - (16777216 / (difference_type)kDequeSubarraySize);
				_set_subarray(m_pCurrentArrayPtr + nSubarrayIdx);
				m_pCurrent = m_pBegin + (subarrayPosition - (nSubarrayIdx * (difference_type)kDequeSubarraySize));
			}
			return (*this);
		}

		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		typename _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::my_type& _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::operator-=(difference_type nDiff) NOEXCEPT
		{
			return (*this).operator+=(-nDiff);
		}

		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		typename _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::my_type _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::operator+(difference_type nDiff) const
		{
			return my_type(*this).operator+=(nDiff);
		}

		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		typename _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::my_type _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::operator-(difference_type nDiff) const
		{
			return my_type(*this).operator+=(-nDiff);
		}

		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		typename _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::reference _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::operator*(void) const
		{
			return (*m_pCurrent);
		}

		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		typename _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::pointer _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::operator->(void) const
		{
			return &(operator*());
		}

		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		typename _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::my_type _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::_copy(const iterator& first, const iterator& last, FXD::STD::true_type)
		{
			if ((first.m_pBegin == last.m_pBegin) && (first.m_pBegin == m_pBegin)) // If all operations are within the same subarray, implement the operation as a memmove.
			{
				Memory::MemMove(m_pCurrent, first.m_pCurrent, (size_t)((FXD::PtrSizedInt)last.m_pCurrent - (FXD::PtrSizedInt)first.m_pCurrent));
				return (*this) + (last.m_pCurrent - first.m_pCurrent);
			}
			return FXD::STD::copy(FXD::STD::make_move_iterator(first), FXD::STD::make_move_iterator(last), FXD::STD::make_move_iterator(*this)).base();
		}

		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		typename _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::my_type _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::_copy(const iterator& first, const iterator& last, FXD::STD::false_type)
		{
			return FXD::STD::copy(FXD::STD::make_move_iterator(first), FXD::STD::make_move_iterator(last), FXD::STD::make_move_iterator(*this)).base();
		}

		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		void _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::_copy_backward(const iterator& first, const iterator& last, FXD::STD::true_type)
		{
			if ((first.m_pBegin == last.m_pBegin) && (first.m_pBegin == m_pBegin)) // If all operations are within the same subarray, implement the operation as a memcpy.
			{
				Memory::MemMove(m_pCurrent - (last.m_pCurrent - first.m_pCurrent), first.m_pCurrent, (size_t)((FXD::PtrSizedInt)last.m_pCurrent - (FXD::PtrSizedInt)first.m_pCurrent));
			}
			else
			{
				FXD::STD::copy_backward(FXD::STD::make_move_iterator(first), FXD::STD::make_move_iterator(last), FXD::STD::make_move_iterator(*this));
			}
		}

		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		void _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::_copy_backward(const iterator& first, const iterator& last, FXD::STD::false_type)
		{
			FXD::STD::copy_backward(FXD::STD::make_move_iterator(first), FXD::STD::make_move_iterator(last), FXD::STD::make_move_iterator(*this)).base();
		}

		template < typename T, typename Pointer, typename Reference, FXD::U32 kDequeSubarraySize >
		void _deque_iterator< T, Pointer, Reference, kDequeSubarraySize >::_set_subarray(value_type** pCurrentArrayPtr)
		{
			m_pCurrentArrayPtr = pCurrentArrayPtr;
			m_pBegin = *pCurrentArrayPtr;
			m_pEnd = (m_pBegin + kDequeSubarraySize);
		}

		// Non-member comparison functions
		// Equality
		template < typename T, typename PtrA, typename RefA, typename PtrB, typename RefB, FXD::U32 kDequeSubarraySizeU >
		inline bool operator==(const _deque_iterator< T, PtrA, RefA, kDequeSubarraySizeU >& lhs, const _deque_iterator< T, PtrB, RefB, kDequeSubarraySizeU >& rhs) NOEXCEPT
		{
			return (lhs.m_pCurrent == rhs.m_pCurrent);
		}
		template < typename T, typename PtrA, typename RefA, FXD::U32 kDequeSubarraySizeU >
		inline bool operator==(const _deque_iterator< T, PtrA, RefA, kDequeSubarraySizeU>& lhs, const _deque_iterator< T, PtrA, RefA, kDequeSubarraySizeU>& rhs) NOEXCEPT
		{
			return (lhs.m_pCurrent == rhs.m_pCurrent);
		}
		// Inequality
		template < typename T, typename PtrA, typename RefA, typename PtrB, typename RefB, FXD::U32 kDequeSubarraySizeU >
		inline bool operator!=(const _deque_iterator< T, PtrA, RefA, kDequeSubarraySizeU >& lhs, const _deque_iterator< T, PtrB, RefB, kDequeSubarraySizeU>& rhs) NOEXCEPT
		{
			return (lhs.m_pCurrent != rhs.m_pCurrent);
		}
		template < typename T, typename PtrA, typename RefA, FXD::U32 kDequeSubarraySizeU >
		inline bool operator!=(const _deque_iterator< T, PtrA, RefA, kDequeSubarraySizeU >& lhs, const _deque_iterator< T, PtrA, RefA, kDequeSubarraySizeU>& rhs) NOEXCEPT
		{
			return (lhs.m_pCurrent != rhs.m_pCurrent);
		}
		// Less than
		template < typename T, typename PtrA, typename RefA, typename PtrB, typename RefB, FXD::U32 kDequeSubarraySizeU >
		inline bool operator<(const _deque_iterator< T, PtrA, RefA, kDequeSubarraySizeU >& lhs, const _deque_iterator< T, PtrB, RefB, kDequeSubarraySizeU >& rhs) NOEXCEPT
		{
			return (lhs.m_pCurrentArrayPtr == rhs.m_pCurrentArrayPtr) ? (lhs.m_pCurrent < rhs.m_pCurrent) : (lhs.m_pCurrentArrayPtr < rhs.m_pCurrentArrayPtr);
		}
		// Greater than
		template < typename T, typename PtrA, typename RefA, typename PtrB, typename RefB, FXD::U32 kDequeSubarraySizeU >
		inline bool operator>(const _deque_iterator< T, PtrA, RefA, kDequeSubarraySizeU >& lhs, const _deque_iterator< T, PtrB, RefB, kDequeSubarraySizeU >& rhs) NOEXCEPT
		{
			return (lhs.m_pCurrentArrayPtr == rhs.m_pCurrentArrayPtr) ? (lhs.m_pCurrent > rhs.m_pCurrent) : (lhs.m_pCurrentArrayPtr > rhs.m_pCurrentArrayPtr);
		}
		// Less than or equal to
		template < typename T, typename PtrA, typename RefA, typename PtrB, typename RefB, FXD::U32 kDequeSubarraySizeU >
		inline bool operator<=(const _deque_iterator< T, PtrA, RefA, kDequeSubarraySizeU >& lhs, const _deque_iterator< T, PtrB, RefB, kDequeSubarraySizeU >& rhs) NOEXCEPT
		{
			return (lhs.m_pCurrentArrayPtr == rhs.m_pCurrentArrayPtr) ? (lhs.m_pCurrent <= rhs.m_pCurrent) : (lhs.m_pCurrentArrayPtr <= rhs.m_pCurrentArrayPtr);
		}
		// Greater than or equal to
		template < typename T, typename PtrA, typename RefA, typename PtrB, typename RefB, FXD::U32 kDequeSubarraySizeU >
		inline bool operator>=(const _deque_iterator< T, PtrA, RefA, kDequeSubarraySizeU >& lhs, const _deque_iterator< T, PtrB, RefB, kDequeSubarraySizeU>& rhs) NOEXCEPT
		{
			return (lhs.m_pCurrentArrayPtr == rhs.m_pCurrentArrayPtr) ? (lhs.m_pCurrent >= rhs.m_pCurrent) : (lhs.m_pCurrentArrayPtr >= rhs.m_pCurrentArrayPtr);
		}

		template < typename T, typename PtrA, typename RefA, typename PtrB, typename RefB, FXD::U32 kDequeSubarraySizeU >
		inline typename _deque_iterator< T, PtrA, RefA, kDequeSubarraySizeU >::difference_type operator-(const _deque_iterator< T, PtrA, RefA, kDequeSubarraySizeU >& lhs, const _deque_iterator< T, PtrB, RefB, kDequeSubarraySizeU>& rhs)
		{
			using difference_type = typename _deque_iterator< T, PtrA, RefA, kDequeSubarraySizeU >::difference_type;
			return ((difference_type)kDequeSubarraySizeU * ((lhs.m_pCurrentArrayPtr - rhs.m_pCurrentArrayPtr) - 1)) + (lhs.m_pCurrent - lhs.m_pBegin) + (rhs.m_pEnd - rhs.m_pCurrent);
		}

		namespace Internal
		{
			// ------
			// _DequeBase
			// -
			// ------
			template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
			class _DequeBase
			{
			public:
				using value_type = T;
				using allocator_type = Allocator;
				using size_type = FXD::U32;
				using difference_type = std::ptrdiff_t;
				using iterator = _deque_iterator< T, T*, T&, kDequeSubarraySize >;
				using const_iterator = _deque_iterator< T, const T*, const T&, kDequeSubarraySize >;

				static const size_type npos = (size_type)-1;				// 'npos' means non-valid position or simply non-position.
				static const size_type kMaxSize = (size_type)-2;		// -1 is reserved for 'npos'. It also happens to be slightly beneficial that kMaxSize is a value less than -1, as it helps us deal with potential integer wraparound issues.

			public:
				_DequeBase(void);
				_DequeBase(size_type nCount);
				_DequeBase(const allocator_type& allocator);
				_DequeBase(size_type nCount, const allocator_type& allocator);
				virtual ~_DequeBase(void);

			protected:
				value_type* _allocate_sub_array(void);
				void _free_sub_array(value_type* pData);
				void _free_sub_arrays(value_type** pBegin, value_type** pEnd);

				value_type** _allocate_ptr_array(size_type nCount);
				void _free_ptr_array(value_type** pData, size_type nCount);

				iterator _realloc_sub_array(size_type nAdditionalCapacity, Internal::E_Side eAllocSide);
				void _realloc_ptr_array(size_type nAdditionalCapacity, Internal::E_Side eAllocSide);

				void _init(const size_type nCount);

			protected:
				value_type** m_pPtrArray;
				size_type m_nPtrArraySize;
				iterator m_itrBegin;
				iterator m_itrEnd;
				allocator_type m_alloc;
			};

			// ------
			// _DequeBase
			// -
			// ------
			template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
			_DequeBase< T, Allocator, kDequeSubarraySize >::_DequeBase(void)
				: m_pPtrArray(nullptr)
				, m_nPtrArraySize(0)
				, m_itrBegin()
				, m_itrEnd()
			{}
			template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
			_DequeBase< T, Allocator, kDequeSubarraySize >::_DequeBase(size_type nCount)
				: m_pPtrArray(nullptr)
				, m_nPtrArraySize(0)
				, m_itrBegin()
				, m_itrEnd()
			{
				_init(nCount);
			}
			template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
			_DequeBase< T, Allocator, kDequeSubarraySize >::_DequeBase(const allocator_type& allocator)
				: m_pPtrArray(nullptr)
				, m_nPtrArraySize(0)
				, m_itrBegin()
				, m_itrEnd()
				, m_alloc(allocator)
			{
				_init(0);
			}
			template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
			_DequeBase< T, Allocator, kDequeSubarraySize >::_DequeBase(size_type nCount, const allocator_type& allocator)
				: m_pPtrArray(nullptr)
				, m_nPtrArraySize(0)
				, m_itrBegin()
				, m_itrEnd()
				, m_alloc(allocator)
			{
				_init(nCount);
			}

			template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
			_DequeBase< T, Allocator, kDequeSubarraySize >::~_DequeBase(void)
			{
				if (m_pPtrArray != nullptr)
				{
					_free_sub_arrays(m_itrBegin.m_pCurrentArrayPtr, (m_itrEnd.m_pCurrentArrayPtr + 1));
					_free_ptr_array(m_pPtrArray, m_nPtrArraySize);
					m_pPtrArray = nullptr;
				}
			}

			template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
			typename _DequeBase< T, Allocator, kDequeSubarraySize >::value_type* _DequeBase< T, Allocator, kDequeSubarraySize >::_allocate_sub_array(void)
			{
				size_type nAllocSize = (kDequeSubarraySize * sizeof(value_type));
				value_type* pPtr = (value_type*)m_alloc.allocate(nAllocSize);
				PRINT_COND_ASSERT((pPtr != nullptr), "Container: Failed to allocate memory");
				return pPtr;
			}

			template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
			void _DequeBase< T, Allocator, kDequeSubarraySize >::_free_sub_array(value_type* pData)
			{
				if (pData != nullptr)
				{
					m_alloc.deallocate(pData, (kDequeSubarraySize * sizeof(value_type)));
				}
			}

			template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
			void _DequeBase< T, Allocator, kDequeSubarraySize >::_free_sub_arrays(value_type** pBegin, value_type** pEnd)
			{
				while (pBegin < pEnd)
				{
					_free_sub_array(*pBegin++);
				}
			}

			template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
			typename _DequeBase< T, Allocator, kDequeSubarraySize >::value_type** _DequeBase< T, Allocator, kDequeSubarraySize >::_allocate_ptr_array(size_type nCount)
			{
				size_type nAllocSize = (nCount * sizeof(value_type*));
				value_type** pPtr = (value_type**)m_alloc.allocate(nAllocSize);
				PRINT_COND_ASSERT((pPtr != nullptr), "Container: Failed to allocate memory");
				return pPtr;
			}

			template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
			void _DequeBase< T, Allocator, kDequeSubarraySize >::_free_ptr_array(value_type** ppPtr, size_type nCount)
			{
				if (ppPtr != nullptr)
				{
					m_alloc.deallocate(ppPtr, (nCount * sizeof(value_type*)));
				}
			}

			template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
			typename _DequeBase< T, Allocator, kDequeSubarraySize >::iterator _DequeBase< T, Allocator, kDequeSubarraySize >::_realloc_sub_array(size_type nAdditionalCapacity, Internal::E_Side eAllocSide)
			{
				if (eAllocSide == Internal::E_Side::Front)
				{
					const size_type nCurrentAdditionalCapacity = (size_type)(m_itrBegin.m_pCurrent - m_itrBegin.m_pBegin);
					if (nCurrentAdditionalCapacity < nAdditionalCapacity)
					{
						const difference_type nSubarrayIncrease = (difference_type)(((nAdditionalCapacity - nCurrentAdditionalCapacity) + kDequeSubarraySize - 1) / kDequeSubarraySize);
						if (nSubarrayIncrease > (m_itrBegin.m_pCurrentArrayPtr - m_pPtrArray)) // If there are not enough pointers in front of the current (first) one...
						{
							_realloc_ptr_array((size_type)(nSubarrayIncrease - (m_itrBegin.m_pCurrentArrayPtr - m_pPtrArray)), Internal::E_Side::Front);
						}
						for (difference_type n1 = 1; n1 <= nSubarrayIncrease; ++n1)
						{
							m_itrBegin.m_pCurrentArrayPtr[-n1] = _allocate_sub_array();
						}
					}
					return (m_itrBegin - (difference_type)nAdditionalCapacity);
				}
				else // else Internal::E_Side::Back
				{
					const size_type nCurrentAdditionalCapacity = (size_type)((m_itrEnd.m_pEnd - 1) - m_itrEnd.m_pCurrent);

					if (nCurrentAdditionalCapacity < nAdditionalCapacity)
					{
						const difference_type nSubarrayIncrease = (difference_type)(((nAdditionalCapacity - nCurrentAdditionalCapacity) + kDequeSubarraySize - 1) / kDequeSubarraySize);
						if (nSubarrayIncrease > ((m_pPtrArray + m_nPtrArraySize) - m_itrEnd.m_pCurrentArrayPtr) - 1) // If there are not enough pointers after the current (last) one...
						{
							_realloc_ptr_array((size_type)(nSubarrayIncrease - (((m_pPtrArray + m_nPtrArraySize) - m_itrEnd.m_pCurrentArrayPtr) - 1)), Internal::E_Side::Back);
						}
						for (difference_type n1 = 1; n1 <= nSubarrayIncrease; ++n1)
						{
							m_itrEnd.m_pCurrentArrayPtr[n1] = _allocate_sub_array();
						}
					}
					return (m_itrEnd + (difference_type)nAdditionalCapacity);
				}
			}

			template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
			void _DequeBase< T, Allocator, kDequeSubarraySize >::_realloc_ptr_array(size_type nAdditionalCapacity, Internal::E_Side eAllocSide)
			{
				// This function is not called unless the capacity is known to require a resize.
				//
				// We have an array of pointers (m_pPtrArray), of which a segment of them are in use and 
				// at either end of the array are zero or more unused pointers. This function is being
				// called because we need to extend the capacity on either side of this array by 
				// nAdditionalCapacity pointers. However, it's possible that if the user is continually
				// using push_back and pop_front then the pointer array will continue to be extended 
				// on the back side and unused on the front side. So while we are doing this resizing 
				// here we also take the opportunity to recenter the pointers and thus be balanced.
				// It man turn out that we don't even need to reallocate the pointer array in order
				// to increase capacity on one side, as simply moving the pointers to the center may
				// be enough to open up the requires space.
				//
				// Balanced pointer array Unbalanced pointer array (unused space at front, no free space at back)

				const size_type nUnusedPtrCountAtFront = (size_type)(m_itrBegin.m_pCurrentArrayPtr - m_pPtrArray);
				const size_type nUsedPtrCount = (size_type)(m_itrEnd.m_pCurrentArrayPtr - m_itrBegin.m_pCurrentArrayPtr) + 1;
				const size_type nUsedPtrSpace = (nUsedPtrCount * sizeof(void*));
				const size_type nUnusedPtrCountAtBack = (m_nPtrArraySize - nUnusedPtrCountAtFront) - nUsedPtrCount;
				value_type** pPtrArrayBegin = nullptr;

				if ((eAllocSide == Internal::E_Side::Back) && (nAdditionalCapacity <= nUnusedPtrCountAtFront)) // If we can take advantage of unused pointers at the front without doing any reallocation...
				{
					if (nAdditionalCapacity < (nUnusedPtrCountAtFront / 2)) // Possibly use more space than required, if there's a lot of extra space.
					{
						nAdditionalCapacity = (nUnusedPtrCountAtFront / 2);
					}
					pPtrArrayBegin = m_pPtrArray + (nUnusedPtrCountAtFront - nAdditionalCapacity);
					FXD::Memory::MemMove(pPtrArrayBegin, m_itrBegin.m_pCurrentArrayPtr, nUsedPtrSpace);
				}
				else if ((eAllocSide == Internal::E_Side::Front) && (nAdditionalCapacity <= nUnusedPtrCountAtBack)) // If we can take advantage of unused pointers at the back without doing any reallocation...
				{
					if (nAdditionalCapacity < (nUnusedPtrCountAtBack / 2)) // Possibly use more space than required, if there's a lot of extra space.
					{
						nAdditionalCapacity = (nUnusedPtrCountAtBack / 2);
					}
					pPtrArrayBegin = m_itrBegin.m_pCurrentArrayPtr + nAdditionalCapacity;
					FXD::Memory::MemMove(pPtrArrayBegin, m_itrBegin.m_pCurrentArrayPtr, nUsedPtrSpace);
				}
				else
				{
					// In this case we will have to do a reallocation.
					const size_type nNewPtrArraySize = (m_nPtrArraySize + FXD::STD::Max(m_nPtrArraySize, nAdditionalCapacity) + 2); // Allocate extra capacity.
					value_type** const pNewPtrArray = _allocate_ptr_array(nNewPtrArraySize);

					pPtrArrayBegin = pNewPtrArray + (m_itrBegin.m_pCurrentArrayPtr - m_pPtrArray) + ((eAllocSide == Internal::E_Side::Front) ? nAdditionalCapacity : 0);

					if (m_pPtrArray != nullptr)
					{
						FXD::Memory::MemCopy(pPtrArrayBegin, m_itrBegin.m_pCurrentArrayPtr, nUsedPtrSpace);
					}
					_free_ptr_array(m_pPtrArray, m_nPtrArraySize);

					m_pPtrArray = pNewPtrArray;
					m_nPtrArraySize = nNewPtrArraySize;
				}

				// We need to reset the begin and end iterators, as code that calls this expects them to *not* be invalidated.
				m_itrBegin._set_subarray(pPtrArrayBegin);
				m_itrEnd._set_subarray((pPtrArrayBegin + nUsedPtrCount) - 1);
			}

			template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
			void _DequeBase< T, Allocator, kDequeSubarraySize >::_init(const size_type nCount)
			{
				const size_type nNewPtrArraySize = (size_type)((nCount / kDequeSubarraySize) + 1);
				const size_type kMyMinPtrArraySize = Internal::kMinPtrArraySize;

				m_nPtrArraySize = FXD::STD::Max(kMyMinPtrArraySize, (nNewPtrArraySize + 2));
				m_pPtrArray = _allocate_ptr_array(m_nPtrArraySize);

				value_type** const pPtrArrayBegin = (m_pPtrArray + ((m_nPtrArraySize - nNewPtrArraySize) / 2)); // Try to place it in the middle.
				value_type** const pPtrArrayEnd = (pPtrArrayBegin + nNewPtrArraySize);
				value_type** pPtrArrayCurrent = pPtrArrayBegin;

				FXD::S32 nCountTest = 0;
				while (pPtrArrayCurrent < pPtrArrayEnd)
				{
					*pPtrArrayCurrent++ = _allocate_sub_array();
					nCountTest++;
				}

				m_itrBegin._set_subarray(pPtrArrayBegin);
				m_itrBegin.m_pCurrent = m_itrBegin.m_pBegin;

				m_itrEnd._set_subarray(pPtrArrayEnd - 1);
				m_itrEnd.m_pCurrent = m_itrEnd.m_pBegin + (difference_type)(nCount % kDequeSubarraySize);
			}
		} //namespace Internal


		// ------
		// Deque
		// -
		// ------
		template <
			typename T,
			typename Allocator = Memory::Alloc::AlignedType< Memory::Alloc::Heap, T >,
			FXD::U32 kDequeSubarraySize = DEQUE_SUBARRAY_SIZE(T) >
		class Deque : public Internal::_DequeBase< T, Allocator, kDequeSubarraySize >
		{
		public:
			using base_type = Internal::_DequeBase< T, Allocator, kDequeSubarraySize >;
			using my_type = Deque< T, Allocator, kDequeSubarraySize >;

			using value_type = T;
			using pointer = T * ;
			using const_pointer = const T*;
			using reference = T & ;
			using const_reference = const T&;
			using iterator = _deque_iterator< T, T*, T&, kDequeSubarraySize >;
			using const_iterator = _deque_iterator< T, const T*, const T&, kDequeSubarraySize >;
			using reverse_iterator = FXD::STD::reverse_iterator< iterator >;
			using const_reverse_iterator = FXD::STD::reverse_iterator< const_iterator >;
			using size_type = typename base_type::size_type;
			using difference_type = typename base_type::difference_type;
			using allocator_type = typename base_type::allocator_type;
			enum
			{
				kSubarraySize = kDequeSubarraySize
			};

		public:
			Deque(void);
			EXPLICIT Deque(const allocator_type& alloc);
			EXPLICIT Deque(size_type nCount, const allocator_type& alloc = allocator_type());
			EXPLICIT Deque(size_type nCount, const value_type& val, const allocator_type& alloc = allocator_type());
			template < typename InputItr, class = typename FXD::STD::enable_if< FXD::Internal::_Is_iterator< InputItr >::value, void >::type >
			Deque(InputItr first, InputItr last, const allocator_type& alloc = allocator_type());
			Deque(std::initializer_list< value_type > iList, const allocator_type& alloc = allocator_type());

			~Deque(void);

			Deque(const my_type& rhs);
			Deque(my_type&& rhs);
			Deque(my_type&& rhs, const allocator_type& allocator);

			my_type& operator=(const my_type& rhs);
			my_type& operator=(my_type&& rhs);
			my_type& operator=(std::initializer_list< value_type > iList);

			// Iterator Support
			iterator begin(void) NOEXCEPT;
			const_iterator begin(void) const NOEXCEPT;
			const_iterator cbegin(void) const NOEXCEPT;

			iterator end(void) NOEXCEPT;
			const_iterator end(void) const NOEXCEPT;
			const_iterator cend(void) const NOEXCEPT;

			reverse_iterator rbegin(void) NOEXCEPT;
			const_reverse_iterator rbegin(void) const NOEXCEPT;
			const_reverse_iterator crbegin(void) const NOEXCEPT;

			reverse_iterator rend(void) NOEXCEPT;
			const_reverse_iterator rend(void) const NOEXCEPT;
			const_reverse_iterator crend(void) const NOEXCEPT;

			// Access
			reference operator[](size_type nID);
			const_reference operator[](size_type nID) const;
			reference at(size_type nID);
			const_reference at(size_type nID) const;

			reference front(void);
			const_reference front(void) const;
			reference back(void);
			const_reference back(void) const;

			// Assign
			void assign(size_type nCount, const value_type& val);
			template < typename InputItr, class = typename FXD::STD::enable_if< FXD::Internal::_Is_iterator< InputItr >::value, void >::type >
			void assign(InputItr first, InputItr last);
			void assign(std::initializer_list< value_type > iList);

			// Resize
			void resize(size_type nSize, const value_type& val);
			void resize(size_type nSize);

			// Insert
			iterator insert(const_iterator pos, const value_type& val);
			iterator insert(const_iterator pos, value_type&& val);
			void insert(const_iterator pos, size_type nCount, const value_type& val);
			iterator insert(const_iterator pos, std::initializer_list< value_type > iList);
			template < typename InputItr, class = typename FXD::STD::enable_if< FXD::Internal::_Is_iterator< InputItr >::value, void >::type >
			void insert(const_iterator pos, InputItr first, InputItr last);

			reference push_back(void);
			void push_back(const value_type& val);
			void push_back(value_type&& val);

			reference push_front(void);
			void push_front(const value_type& val);
			void push_front(value_type&& val);

			template < class... Args >
			iterator emplace(const_iterator pos, Args&&... args);

			template < class... Args >
			reference emplace_front(Args&&... args);

			template < class... Args >
			reference emplace_back(Args&&... args);

			// Erase
			iterator erase(const_iterator pos);
			iterator erase(const_iterator first, const_iterator last);
			reverse_iterator erase(reverse_iterator pos);
			reverse_iterator erase(reverse_iterator first, reverse_iterator last);

			void pop_front(void);
			void pop_back(void);
			void clear(void);

			// Adjust
			void swap(my_type& rhs);

			// Searches
			const_iterator find(const value_type& key) const;
			bool contains(const value_type& key) const;

			bool empty(void) const NOEXCEPT;
			bool not_empty(void) const NOEXCEPT;
			const size_type size(void) const NOEXCEPT;
			const size_type count(const value_type& key) const NOEXCEPT;
			allocator_type allocator(void) const NOEXCEPT;

			void set_capacity(size_type nSize = base_type::npos);

		protected:
			template < typename Integer >
			void _init(Integer nCount, Integer val, FXD::STD::true_type);
			template < typename InputItr >
			void _init(InputItr first, InputItr last, FXD::STD::false_type);

			template < typename InputItr >
			void _init_iterator(InputItr first, InputItr last, FXD::STD::input_iterator_tag);
			template < typename ForwardItr >
			void _init_iterator(ForwardItr first, ForwardItr last, FXD::STD::forward_iterator_tag);

			void _init_fill(const value_type& val);

			template < typename Integer >
			void _assign(Integer nCount, Integer val, FXD::STD::true_type);

			template < typename InputItr >
			void _assign(InputItr first, InputItr last, FXD::STD::false_type);

			void _assign_values(size_type n, const value_type& val);

			template < typename Integer >
			void _insert(const const_iterator& pos, Integer nCount, Integer val, FXD::STD::true_type);

			template < typename InputItr >
			void _insert(const const_iterator& pos, const InputItr& first, const InputItr& last, FXD::STD::false_type);

			template < typename InputItr >
			void _insert_from_iterator(const_iterator pos, const InputItr& first, const InputItr& last, FXD::STD::forward_iterator_tag);

			void _insert_values(const_iterator pos, size_type nCount, const value_type& val);

			void _swap(my_type& rhs);
		};

		// ------
		// Deque
		// -
		// ------
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline Deque< T, Allocator, kDequeSubarraySize >::Deque(void)
			: base_type((size_type)0)
		{}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline Deque< T, Allocator, kDequeSubarraySize >::Deque(const allocator_type& allocator)
			: base_type((size_type)0, allocator)
		{}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline Deque< T, Allocator, kDequeSubarraySize >::Deque(size_type nCount, const allocator_type& allocator)
			: base_type(nCount, allocator)
		{
			_init_fill(value_type());
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline Deque< T, Allocator, kDequeSubarraySize >::Deque(size_type nCount, const value_type& val, const allocator_type& allocator)
			: base_type(nCount, allocator)
		{
			_init_fill(val);
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		template < typename InputItr, class >
		inline Deque< T, Allocator, kDequeSubarraySize >::Deque(InputItr first, InputItr last, const allocator_type& allocator)
			: base_type((size_type)0, allocator)
		{
			_init(first, last, FXD::STD::is_integral< InputItr >());
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline Deque< T, Allocator, kDequeSubarraySize >::Deque(std::initializer_list< value_type > iList, const allocator_type& allocator)
			: base_type((size_type)0, allocator)
		{
			_init(iList.begin(), iList.end(), FXD::STD::false_type());
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline Deque< T, Allocator, kDequeSubarraySize >::~Deque(void)
		{
			for (iterator itCurrent(this->m_itrBegin); itCurrent != this->m_itrEnd; ++itCurrent)
			{
				FXD::STD::destruct(itCurrent.m_pCurrent);
			}
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline Deque< T, Allocator, kDequeSubarraySize >::Deque(const my_type& rhs)
			: base_type(rhs.size(), rhs.m_alloc)
		{
			FXD::STD::uninitialized_copy(rhs.m_itrBegin, rhs.m_itrEnd, this->m_itrBegin);
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline Deque< T, Allocator, kDequeSubarraySize >::Deque(my_type&& rhs)
			: base_type((size_type)0, rhs.m_alloc)
		{
			swap(rhs);
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline Deque< T, Allocator, kDequeSubarraySize >::Deque(my_type&& rhs, const allocator_type& allocator)
			: base_type((size_type)0, allocator)
		{
			swap(rhs);
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		typename Deque< T, Allocator, kDequeSubarraySize >::my_type& Deque< T, Allocator, kDequeSubarraySize >::operator=(const my_type& rhs)
		{
			if (&rhs != this)
			{
				_assign(rhs.begin(), rhs.end(), FXD::STD::false_type());
			}
			return (*this);
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline typename Deque< T, Allocator, kDequeSubarraySize >::my_type& Deque< T, Allocator, kDequeSubarraySize >::operator=(my_type&& rhs)
		{
			if (this != &rhs)
			{
				set_capacity(0);
				swap(rhs);
			}
			return (*this);
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline typename Deque< T, Allocator, kDequeSubarraySize >::my_type& Deque< T, Allocator, kDequeSubarraySize >::operator=(std::initializer_list< value_type > iList)
		{
			_assign(iList.begin(), iList.end(), FXD::STD::false_type());
			return (*this);
		}

		// Iterator Support
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline typename Deque< T, Allocator, kDequeSubarraySize >::iterator Deque< T, Allocator, kDequeSubarraySize >::begin(void) NOEXCEPT
		{
			return this->m_itrBegin;
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline typename Deque< T, Allocator, kDequeSubarraySize >::const_iterator Deque< T, Allocator, kDequeSubarraySize >::begin(void) const NOEXCEPT
		{
			return this->m_itrBegin;
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline typename Deque< T, Allocator, kDequeSubarraySize >::const_iterator Deque< T, Allocator, kDequeSubarraySize >::cbegin(void) const NOEXCEPT
		{
			return this->m_itrBegin;
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline typename Deque< T, Allocator, kDequeSubarraySize >::iterator Deque< T, Allocator, kDequeSubarraySize >::end(void) NOEXCEPT
		{
			return this->m_itrEnd;
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		typename Deque< T, Allocator, kDequeSubarraySize >::const_iterator Deque< T, Allocator, kDequeSubarraySize >::end(void) const NOEXCEPT
		{
			return this->m_itrEnd;
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline typename Deque< T, Allocator, kDequeSubarraySize >::const_iterator Deque< T, Allocator, kDequeSubarraySize >::cend(void) const NOEXCEPT
		{
			return this->m_itrEnd;
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline typename Deque< T, Allocator, kDequeSubarraySize >::reverse_iterator Deque< T, Allocator, kDequeSubarraySize >::rbegin(void) NOEXCEPT
		{
			return reverse_iterator(this->m_itrEnd);
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline typename Deque< T, Allocator, kDequeSubarraySize >::const_reverse_iterator Deque< T, Allocator, kDequeSubarraySize >::rbegin(void) const NOEXCEPT
		{
			return const_reverse_iterator(this->m_itrEnd);
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline typename Deque< T, Allocator, kDequeSubarraySize >::const_reverse_iterator Deque< T, Allocator, kDequeSubarraySize >::crbegin(void) const NOEXCEPT
		{
			return const_reverse_iterator(this->m_itrEnd);
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline typename Deque< T, Allocator, kDequeSubarraySize >::reverse_iterator Deque< T, Allocator, kDequeSubarraySize >::rend(void) NOEXCEPT
		{
			return reverse_iterator(this->m_itrBegin);
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline typename Deque< T, Allocator, kDequeSubarraySize >::const_reverse_iterator Deque< T, Allocator, kDequeSubarraySize >::rend(void) const NOEXCEPT
		{
			return const_reverse_iterator(this->m_itrBegin);
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline typename Deque< T, Allocator, kDequeSubarraySize >::const_reverse_iterator Deque< T, Allocator, kDequeSubarraySize >::crend(void) const NOEXCEPT
		{
			return const_reverse_iterator(this->m_itrBegin);
		}

		// Access
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		typename Deque< T, Allocator, kDequeSubarraySize >::reference Deque< T, Allocator, kDequeSubarraySize >::operator[](size_type nID)
		{
			iterator itr(this->m_itrBegin);

			const difference_type subarrayPosition = (difference_type)((itr.m_pCurrent - itr.m_pBegin) + (difference_type)nID);
			const difference_type subarrayIndex = (((16777216 + subarrayPosition) / (difference_type)kDequeSubarraySize)) - (16777216 / (difference_type)kDequeSubarraySize);

			return *(*(itr.m_pCurrentArrayPtr + subarrayIndex) + (subarrayPosition - (subarrayIndex * (difference_type)kDequeSubarraySize)));
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		typename Deque< T, Allocator, kDequeSubarraySize >::const_reference Deque< T, Allocator, kDequeSubarraySize >::operator[](size_type nID) const
		{
			iterator itr(this->m_itrBegin);

			const difference_type subarrayPosition = (itr.m_pCurrent - itr.m_pBegin) + (difference_type)nID;
			const difference_type subarrayIndex = (((16777216 + subarrayPosition) / (difference_type)kDequeSubarraySize)) - (16777216 / (difference_type)kDequeSubarraySize);

			return *(*(itr.m_pCurrentArrayPtr + subarrayIndex) + (subarrayPosition - (subarrayIndex * (difference_type)kDequeSubarraySize)));
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		typename Deque< T, Allocator, kDequeSubarraySize >::reference Deque< T, Allocator, kDequeSubarraySize >::at(size_type nID)
		{
			if (nID >= (size_type)(this->m_itrEnd - this->m_itrBegin))
			{
				PRINT_ASSERT << "Deque::at -- out of range";
			}
			return *(this->m_itrBegin.operator+((difference_type)nID));
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		typename Deque< T, Allocator, kDequeSubarraySize >::const_reference Deque< T, Allocator, kDequeSubarraySize >::at(size_type nID) const
		{
			if (nID >= (size_type)(this->m_itrEnd - this->m_itrBegin))
			{
				PRINT_ASSERT << "Deque::at -- out of range";
			}
			return *(this->m_itrBegin.operator+((difference_type)nID));
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		typename Deque< T, Allocator, kDequeSubarraySize >::reference Deque< T, Allocator, kDequeSubarraySize >::front(void)
		{
			return *(this->m_itrBegin);
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		typename Deque< T, Allocator, kDequeSubarraySize >::const_reference Deque< T, Allocator, kDequeSubarraySize >::front(void) const
		{
			return *(this->m_itrBegin);
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		typename Deque< T, Allocator, kDequeSubarraySize >::reference Deque< T, Allocator, kDequeSubarraySize >::back(void)
		{
			reference ref = *iterator(this->m_itrEnd, typename iterator::Decrement());
			return ref;
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		typename Deque< T, Allocator, kDequeSubarraySize >::const_reference Deque< T, Allocator, kDequeSubarraySize >::back(void) const
		{
			return *iterator(this->m_itrEnd, typename iterator::Decrement());
		}

		// Assign
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline void Deque< T, Allocator, kDequeSubarraySize >::assign(size_type nCount, const value_type& val)
		{
			_assign_values(nCount, val);
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		template < typename InputItr, class >
		inline void Deque< T, Allocator, kDequeSubarraySize >::assign(InputItr first, InputItr last)
		{
			_assign(first, last, FXD::STD::is_integral< InputItr >());
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline void Deque< T, Allocator, kDequeSubarraySize >::assign(std::initializer_list< value_type > iList)
		{
			_assign(iList.begin(), iList.end(), FXD::STD::false_type());
		}

		// Resize
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline void Deque< T, Allocator, kDequeSubarraySize >::resize(size_type nSize, const value_type& val)
		{
			size_type nSizeCurrent = size();
			if (nSize > nSizeCurrent)
			{
				insert(this->m_itrEnd, (nSize - nSizeCurrent), val);
			}
			else
			{
				erase(this->m_itrBegin + (difference_type)nSize, this->m_itrEnd);
			}
			nSizeCurrent = size();
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline void Deque< T, Allocator, kDequeSubarraySize >::resize(size_type nSize)
		{
			resize(nSize, value_type());
		}

		// Insert
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		typename Deque< T, Allocator, kDequeSubarraySize >::iterator Deque< T, Allocator, kDequeSubarraySize >::insert(const_iterator pos, const value_type& val)
		{
			return emplace(pos, val);
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		typename Deque< T, Allocator, kDequeSubarraySize >::iterator Deque< T, Allocator, kDequeSubarraySize >::insert(const_iterator pos, value_type&& val)
		{
			return emplace(pos, std::move(val));
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		void Deque< T, Allocator, kDequeSubarraySize >::insert(const_iterator pos, size_type nCount, const value_type& val)
		{
			_insert_values(pos, nCount, val);
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		typename Deque< T, Allocator, kDequeSubarraySize >::iterator Deque< T, Allocator, kDequeSubarraySize >::insert(const_iterator pos, std::initializer_list< value_type > iList)
		{
			const difference_type nDiff(pos - this->m_itrBegin);
			_insert(pos, iList.begin(), iList.end(), FXD::STD::false_type());
			return (this->m_itrBegin + nDiff);
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		template < typename InputItr, class >
		void Deque< T, Allocator, kDequeSubarraySize >::insert(const_iterator pos, InputItr first, InputItr last)
		{
			_insert(pos, first, last, FXD::STD::is_integral< InputItr >());
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		typename Deque< T, Allocator, kDequeSubarraySize >::reference Deque< T, Allocator, kDequeSubarraySize >::push_back(void)
		{
			emplace_back();
			return back();
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		void Deque< T, Allocator, kDequeSubarraySize >::push_back(const value_type& val)
		{
			emplace_back(val);
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		void Deque< T, Allocator, kDequeSubarraySize >::push_back(value_type&& val)
		{
			emplace_back(std::move(val));
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		typename Deque< T, Allocator, kDequeSubarraySize >::reference Deque< T, Allocator, kDequeSubarraySize >::push_front(void)
		{
			emplace_front();
			return front();
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		void Deque< T, Allocator, kDequeSubarraySize >::push_front(const value_type& val)
		{
			emplace_front(val);
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		void Deque< T, Allocator, kDequeSubarraySize >::push_front(value_type&& val)
		{
			emplace_front(std::move(val));
		}


		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		template < class... Args >
		typename Deque< T, Allocator, kDequeSubarraySize >::iterator Deque< T, Allocator, kDequeSubarraySize >::emplace(const_iterator pos, Args&&... args)
		{
			if (pos.m_pCurrent == this->m_itrEnd.m_pCurrent)
			{
				emplace_back(std::forward< Args >(args)...);
				return iterator(this->m_itrEnd, typename iterator::Decrement());
			}
			else if (pos.m_pCurrent == this->m_itrBegin.m_pCurrent)
			{
				emplace_front(std::forward< Args >(args)...);
				return this->m_itrBegin;
			}

			iterator itrPosition(pos, typename iterator::FromConst());
			value_type valSaved(std::forward< Args >(args)...);
			const difference_type nDiff(itrPosition - this->m_itrBegin);

			if (nDiff < (difference_type)(size() / 2))
			{
				emplace_front(std::move(*this->m_itrBegin));

				itrPosition = (this->m_itrBegin + nDiff);

				const iterator itrNewPosition(itrPosition, typename iterator::Increment());
				iterator itrOldBegin(this->m_itrBegin, typename iterator::Increment());
				const iterator itrOldBeginPlus1(itrOldBegin, typename iterator::Increment());

				itrOldBegin._copy(itrOldBeginPlus1, itrNewPosition, FXD::STD::has_trivial_relocate< value_type >());
			}
			else
			{
				emplace_back(std::move(*iterator(this->m_itrEnd, typename iterator::Decrement())));

				itrPosition = (this->m_itrBegin + nDiff);
				iterator itrOldBack(this->m_itrEnd, typename iterator::Decrement());
				const iterator itrOldBackMinus1(itrOldBack, typename iterator::Decrement());
				itrOldBack._copy_backward(itrPosition, itrOldBackMinus1, FXD::STD::has_trivial_relocate< value_type >());
			}

			*itrPosition = std::move(valSaved);
			return itrPosition;
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		template < class... Args >
		typename Deque< T, Allocator, kDequeSubarraySize >::reference Deque< T, Allocator, kDequeSubarraySize >::emplace_front(Args&&... args)
		{
			if (this->m_itrBegin.m_pCurrent != this->m_itrBegin.m_pBegin)
			{
				FXD_PLACEMENT_NEW((void*)--this->m_itrBegin.m_pCurrent, value_type)(std::forward< Args >(args)...);
			}
			else
			{
				value_type valSaved(std::forward< Args >(args)...);
				if (this->m_itrBegin.m_pCurrentArrayPtr == this->m_pPtrArray)
				{
					this->_realloc_ptr_array(1, Internal::E_Side::Front);
				}
				this->m_itrBegin.m_pCurrentArrayPtr[-1] = this->_allocate_sub_array();
				this->m_itrBegin._set_subarray(this->m_itrBegin.m_pCurrentArrayPtr - 1);
				this->m_itrBegin.m_pCurrent = (this->m_itrBegin.m_pEnd - 1);
				FXD_PLACEMENT_NEW((void*)this->m_itrBegin.m_pCurrent, value_type)(std::move(valSaved));
			}
			return front();
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		template < class... Args >
		typename Deque< T, Allocator, kDequeSubarraySize >::reference Deque< T, Allocator, kDequeSubarraySize >::emplace_back(Args&&... args)
		{
			if ((this->m_itrEnd.m_pCurrent + 1) != this->m_itrEnd.m_pEnd)
			{
				FXD_PLACEMENT_NEW((void*)this->m_itrEnd.m_pCurrent++, value_type)(std::forward< Args >(args)...);
			}
			else
			{
				value_type valSaved(std::forward< Args >(args)...);
				if (((this->m_itrEnd.m_pCurrentArrayPtr - this->m_pPtrArray) + 1) >= (difference_type)this->m_nPtrArraySize)
				{
					this->_realloc_ptr_array(1, Internal::E_Side::Back);
				}

				this->m_itrEnd.m_pCurrentArrayPtr[1] = this->_allocate_sub_array();

				FXD_PLACEMENT_NEW((void*)this->m_itrEnd.m_pCurrent, value_type)(std::move(valSaved));
				this->m_itrEnd._set_subarray(this->m_itrEnd.m_pCurrentArrayPtr + 1);
				this->m_itrEnd.m_pCurrent = this->m_itrEnd.m_pBegin;
			}
			return back();
		}


		// Erase
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		typename Deque< T, Allocator, kDequeSubarraySize >::iterator Deque< T, Allocator, kDequeSubarraySize >::erase(const_iterator pos)
		{
			iterator itrPosition(pos, typename iterator::FromConst());
			iterator itrNext(itrPosition, typename iterator::Increment());
			const difference_type nDif(itrPosition - this->m_itrBegin);

			if (nDif < (difference_type)(size() / 2)) // Should we move the front entries forward or the back entries backward? We divide the range in half.
			{
				itrNext._copy_backward(this->m_itrBegin, itrPosition, FXD::STD::has_trivial_relocate< value_type >());
				pop_front();
			}
			else
			{
				itrPosition._copy(itrNext, this->m_itrEnd, FXD::STD::has_trivial_relocate< value_type >());
				pop_back();
			}
			return (this->m_itrBegin + nDif);
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		typename Deque< T, Allocator, kDequeSubarraySize >::iterator Deque< T, Allocator, kDequeSubarraySize >::erase(const_iterator first, const_iterator last)
		{
			iterator itFirst(first, typename iterator::FromConst());
			iterator itLast(last, typename iterator::FromConst());

			if ((itFirst != this->m_itrBegin) || (itLast != this->m_itrEnd)) // If not erasing everything... (We expect that the user won't call erase(begin, end) because instead the user would just call clear.)
			{
				const difference_type n(itLast - itFirst);
				const difference_type i(itFirst - this->m_itrBegin);

				if (i < (difference_type)((size() - n) / 2)) // Should we move the front entries forward or the back entries backward? We divide the range in half.
				{
					const iterator itrNewBegin(this->m_itrBegin + n);
					value_type** const pPtrArrayBegin = this->m_itrBegin.m_pCurrentArrayPtr;

					itLast._copy_backward(this->m_itrBegin, itFirst, FXD::STD::has_trivial_relocate< value_type >());

					for (; this->m_itrBegin != itrNewBegin; ++this->m_itrBegin)
					{
						FXD::STD::destruct(this->m_itrBegin.m_pCurrent);
					}
					this->_free_sub_arrays(pPtrArrayBegin, itrNewBegin.m_pCurrentArrayPtr);
				}
				else // Else we will be moving back entries backward.
				{
					iterator itrNewEnd(this->m_itrEnd - n);
					value_type** const pPtrArrayEnd = (itrNewEnd.m_pCurrentArrayPtr + 1);

					itFirst._copy(itLast, this->m_itrEnd, FXD::STD::has_trivial_relocate< value_type >());

					for (iterator itTemp(itrNewEnd); itTemp != this->m_itrEnd; ++itTemp)
					{
						FXD::STD::destruct(itTemp.m_pCurrent);
					}
					this->_free_sub_arrays(pPtrArrayEnd, this->m_itrEnd.m_pCurrentArrayPtr + 1);

					this->m_itrEnd = itrNewEnd;
				}
				return (this->m_itrBegin + i);
			}

			clear();
			return this->m_itrEnd;
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		typename Deque< T, Allocator, kDequeSubarraySize >::reverse_iterator Deque< T, Allocator, kDequeSubarraySize >::erase(reverse_iterator pos)
		{
			return reverse_iterator(erase((++pos).base()));
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		typename Deque< T, Allocator, kDequeSubarraySize >::reverse_iterator Deque< T, Allocator, kDequeSubarraySize >::erase(reverse_iterator first, reverse_iterator last)
		{
			return reverse_iterator(erase(last.base(), first.base()));
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		void Deque< T, Allocator, kDequeSubarraySize >::pop_front(void)
		{
			if ((this->m_itrBegin.m_pCurrent + 1) != this->m_itrBegin.m_pEnd)
			{
				FXD::STD::destruct(this->m_itrBegin.m_pCurrent++);
			}
			else
			{
				FXD::STD::destruct(this->m_itrBegin.m_pCurrent);
				this->_free_sub_array(this->m_itrBegin.m_pBegin);
				this->m_itrBegin._set_subarray(this->m_itrBegin.m_pCurrentArrayPtr + 1);
				this->m_itrBegin.m_pCurrent = this->m_itrBegin.m_pBegin;
			}
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		void Deque< T, Allocator, kDequeSubarraySize >::pop_back(void)
		{
			if (this->m_itrEnd.m_pCurrent != this->m_itrEnd.m_pBegin)
			{
				FXD::STD::destruct(--this->m_itrEnd.m_pCurrent);
			}
			else
			{
				this->_free_sub_array(this->m_itrEnd.m_pBegin);
				this->m_itrEnd._set_subarray(this->m_itrEnd.m_pCurrentArrayPtr - 1);
				this->m_itrEnd.m_pCurrent = (this->m_itrEnd.m_pEnd - 1);
				FXD::STD::destruct(this->m_itrEnd.m_pCurrent);
			}
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		void Deque< T, Allocator, kDequeSubarraySize >::clear(void)
		{
			// Destroy all values and all subarrays they belong to, except for the first one, as we need to reserve some space for a valid m_itrBegin/m_itrEnd.
			if (this->m_itrBegin.m_pCurrentArrayPtr != this->m_itrEnd.m_pCurrentArrayPtr)
			{
				for (value_type* pPtr1 = this->m_itrBegin.m_pCurrent; pPtr1 < this->m_itrBegin.m_pEnd; ++pPtr1)
				{
					FXD::STD::destruct(pPtr1);
				}
				for (value_type* pPtr2 = this->m_itrEnd.m_pBegin; pPtr2 < this->m_itrEnd.m_pCurrent; ++pPtr2)
				{
					FXD::STD::destruct(pPtr2);
				}
				this->_free_sub_array(this->m_itrEnd.m_pBegin); // Leave m_itrBegin with a valid subarray.
			}
			else
			{
				for (value_type* pPtr1 = this->m_itrBegin.m_pCurrent; pPtr1 < this->m_itrEnd.m_pCurrent; ++pPtr1)
				{
					FXD::STD::destruct(pPtr1);
				}
			}
			for (value_type** pPtrArray = this->m_itrBegin.m_pCurrentArrayPtr + 1; pPtrArray < this->m_itrEnd.m_pCurrentArrayPtr; ++pPtrArray)
			{
				for (value_type* pPtr1 = *pPtrArray, *pEnd = *pPtrArray + kDequeSubarraySize; pPtr1 < pEnd; ++pPtr1)
				{
					FXD::STD::destruct(pPtr1);
				}
				this->_free_sub_array(*pPtrArray);
			}
			this->m_itrEnd = this->m_itrBegin;
		}

		// Adjust
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		void Deque< T, Allocator, kDequeSubarraySize >::swap(my_type& rhs)
		{
			FXD::STD::swap(this->m_pPtrArray, rhs.m_pPtrArray);
			FXD::STD::swap(this->m_nPtrArraySize, rhs.m_nPtrArraySize);
			FXD::STD::swap(this->m_itrBegin, rhs.m_itrBegin);
			FXD::STD::swap(this->m_itrEnd, rhs.m_itrEnd);
			//FXD::STD::swap(this->m_alloc, rhs.m_alloc);
		}

		// Searches
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline typename Deque< T, Allocator, kDequeSubarraySize >::const_iterator Deque< T, Allocator, kDequeSubarraySize >::find(const value_type& key) const
		{
			return FXD::STD::find(begin(), end(), key);
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		bool Deque< T, Allocator, kDequeSubarraySize >::contains(const value_type& key) const
		{
			return (find(key) != end());
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline bool Deque< T, Allocator, kDequeSubarraySize >::empty(void) const NOEXCEPT
		{
			return (this->m_itrBegin.m_pCurrent == this->m_itrEnd.m_pCurrent);
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline bool Deque< T, Allocator, kDequeSubarraySize >::not_empty(void) const NOEXCEPT
		{
			return (this->m_itrBegin.m_pCurrent != this->m_itrEnd.m_pCurrent);
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline const typename Deque< T, Allocator, kDequeSubarraySize >::size_type Deque< T, Allocator, kDequeSubarraySize >::size(void) const NOEXCEPT
		{
			return (size_type)(this->m_itrEnd - this->m_itrBegin);
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline const typename Deque< T, Allocator, kDequeSubarraySize >::size_type Deque< T, Allocator, kDequeSubarraySize >::count(const value_type& key) const NOEXCEPT
		{
			return (size_type)FXD::STD::count(begin(), end(), key);
		}
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		typename Deque< T, Allocator, kDequeSubarraySize >::allocator_type Deque< T, Allocator, kDequeSubarraySize >::allocator(void) const NOEXCEPT
		{
			return this->m_alloc;
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline void Deque< T, Allocator, kDequeSubarraySize >::set_capacity(size_type nSize)
		{
			if (nSize == 0)
			{
				my_type tmp(this->m_alloc);
				_swap(tmp);
			}
			else if (nSize < size())
			{
				resize(nSize);
			}
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		template < typename Integer >
		void Deque< T, Allocator, kDequeSubarraySize >::_init(Integer nCount, Integer val, FXD::STD::true_type)
		{
			base_type::_init(nCount);
			_init_fill(val);
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		template < typename InputItr >
		void Deque< T, Allocator, kDequeSubarraySize >::_init(InputItr first, InputItr last, FXD::STD::false_type)
		{
			using IC = typename FXD::STD::iterator_traits< InputItr >::iterator_category;
			_init_iterator(first, last, IC());
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		template < typename InputItr >
		void Deque< T, Allocator, kDequeSubarraySize >::_init_iterator(InputItr first, InputItr last, FXD::STD::input_iterator_tag)
		{
			base_type::_init(0);

			for (; first != last; ++first)
			{
				push_back(*first);
			}
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		template < typename ForwardItr >
		void Deque< T, Allocator, kDequeSubarraySize >::_init_iterator(ForwardItr first, ForwardItr last, FXD::STD::forward_iterator_tag)
		{
			const size_type nDist = (size_type)FXD::STD::distance(first, last);
			base_type::_init(nDist);

			for (value_type** pPtrArrayCurrent = this->m_itrBegin.m_pCurrentArrayPtr; pPtrArrayCurrent < this->m_itrEnd.m_pCurrentArrayPtr; ++pPtrArrayCurrent) // Copy to the known-to-be-completely-used subarrays.
			{
				ForwardItr current(first);
				FXD::STD::advance(current, kDequeSubarraySize);
				FXD::STD::uninitialized_copy(first, current, *pPtrArrayCurrent);
				first = current;
			}
			FXD::STD::uninitialized_copy(first, last, this->m_itrEnd.m_pBegin);
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		void Deque< T, Allocator, kDequeSubarraySize >::_init_fill(const value_type& val)
		{
			value_type** pPtrArrayCurrent = this->m_itrBegin.m_pCurrentArrayPtr;

			while (pPtrArrayCurrent < this->m_itrEnd.m_pCurrentArrayPtr)
			{
				FXD::STD::uninitialized_fill(*pPtrArrayCurrent, (*pPtrArrayCurrent + kDequeSubarraySize), val);
				++pPtrArrayCurrent;
			}
			FXD::STD::uninitialized_fill(this->m_itrEnd.m_pBegin, this->m_itrEnd.m_pCurrent, val);
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		template < typename Integer >
		void Deque< T, Allocator, kDequeSubarraySize >::_assign(Integer nCount, Integer nVal, FXD::STD::true_type)
		{
			_assign_values(static_cast< size_type >(nCount), static_cast< value_type >(nVal));
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		template < typename InputItr >
		void Deque< T, Allocator, kDequeSubarraySize >::_assign(InputItr first, InputItr last, FXD::STD::false_type)
		{
			const size_type nDist = (size_type)FXD::STD::distance(first, last);
			const size_type nSize = size();

			if (nDist > nSize) // If we are increasing the size...
			{
				InputItr itrAtEnd(first);

				FXD::STD::advance(itrAtEnd, (difference_type)nSize);
				FXD::STD::copy(first, itrAtEnd, this->m_itrBegin);
				insert(this->m_itrEnd, itrAtEnd, last);
			}
			else // nDist is <= size.
			{
				iterator itrEnd(FXD::STD::copy(first, last, this->m_itrBegin));
				if (nDist < nSize) // If we need to erase any trailing elements...
				{
					erase(itrEnd, this->m_itrEnd);
				}
			}
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		void Deque< T, Allocator, kDequeSubarraySize >::_assign_values(size_type nCount, const value_type& val)
		{
			const size_type nCurSize = size();

			if (nCount > nCurSize) // If we are increasing the size...
			{
				FXD::STD::fill(this->m_itrBegin, this->m_itrEnd, val);
				insert(this->m_itrEnd, (nCount - nCurSize), val);
			}
			else
			{
				erase(this->m_itrBegin + (difference_type)nCount, this->m_itrEnd);
				FXD::STD::fill(this->m_itrBegin, this->m_itrEnd, val);
			}
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		template < typename Integer >
		void Deque< T, Allocator, kDequeSubarraySize >::_insert(const const_iterator& pos, Integer nCount, Integer val, FXD::STD::true_type)
		{
			_insert_values(pos, (size_type)nCount, (value_type)val);
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		template < typename InputItr >
		void Deque< T, Allocator, kDequeSubarraySize >::_insert(const const_iterator& pos, const InputItr& first, const InputItr& last, FXD::STD::false_type)
		{
			using IC = typename FXD::STD::iterator_traits<InputItr>::iterator_category;
			_insert_from_iterator(pos, first, last, IC());
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		template < typename InputItr >
		void Deque< T, Allocator, kDequeSubarraySize >::_insert_from_iterator(const_iterator pos, const InputItr& first, const InputItr& last, FXD::STD::forward_iterator_tag)
		{
			const size_type nDist = (size_type)FXD::STD::distance(first, last);

			if (pos.m_pCurrent == this->m_itrBegin.m_pCurrent) // If inserting at the beginning or into an empty container...
			{
				iterator itrNewBegin(this->_realloc_sub_array(nDist, Internal::E_Side::Front));
				FXD::STD::uninitialized_copy(first, last, itrNewBegin);
				this->m_itrBegin = itrNewBegin;
			}
			else if (pos.m_pCurrent == this->m_itrEnd.m_pCurrent) // If inserting at the end (i.e. appending)...
			{
				const iterator itrNewEnd(this->_realloc_sub_array(nDist, Internal::E_Side::Back));
				FXD::STD::uninitialized_copy(first, last, this->m_itrEnd);
				this->m_itrEnd = itrNewEnd;
			}
			else
			{
				const difference_type nInsertIdx = (pos - this->m_itrBegin);
				const size_type nCurrSize = size();

				if (nInsertIdx < (difference_type)(nCurrSize / 2)) // If the insertion index is in the front half of the Deque... grow the Deque at the front.
				{
					const iterator itrNewBegin(this->_realloc_sub_array(nDist, Internal::E_Side::Front)); // itrNewBegin to m_itrBegin refers to memory that isn't initialized yet; so it's not truly a valid iterator. Or at least not a dereferencable one.
					const iterator itrOldBegin(this->m_itrBegin);// We need to reset this value because the reallocation above can invalidate iterators.
					const iterator itrPosition(this->m_itrBegin + nInsertIdx);

					if (nInsertIdx >= (difference_type)nDist) // If the newly inserted items will be entirely within the old area...
					{
						iterator itrUCopyEnd(this->m_itrBegin + (difference_type)nDist);

						FXD::STD::uninitialized_copy(this->m_itrBegin, itrUCopyEnd, itrNewBegin); // This can throw.
						itrUCopyEnd = FXD::STD::copy(itrUCopyEnd, itrPosition, itrOldBegin); // Recycle 'itrUCopyEnd' to mean something else.
						FXD::STD::copy(first, last, itrUCopyEnd);
					}
					else // Else the newly inserted items are going within the newly allocated area at the front.
					{
						InputItr mid(first);

						FXD::STD::advance(mid, (difference_type)nDist - nInsertIdx);
						FXD::STD::uninitialized_copy_copy(this->m_itrBegin, itrPosition, first, mid, itrNewBegin); // This can throw.
						FXD::STD::copy(mid, last, itrOldBegin);
					}
					this->m_itrBegin = itrNewBegin;
				}
				else
				{
					const iterator itrNewEnd(this->_realloc_sub_array(nDist, Internal::E_Side::Back));
					const iterator itrOldEnd(this->m_itrEnd);
					const difference_type nPushedCount = ((difference_type)nCurrSize - nInsertIdx);
					const iterator itrPos(this->m_itrEnd - nPushedCount);

					if (nPushedCount > (difference_type)nDist)
					{
						const iterator itrUCopyEnd(this->m_itrEnd - (difference_type)nDist);

						FXD::STD::uninitialized_copy(itrUCopyEnd, this->m_itrEnd, this->m_itrEnd);
						FXD::STD::copy_backward(itrPos, itrUCopyEnd, itrOldEnd);
						FXD::STD::copy(first, last, itrPos);
					}
					else
					{
						InputItr mid(first);

						FXD::STD::advance(mid, nPushedCount);
						FXD::STD::uninitialized_copy_copy(mid, last, itrPos, this->m_itrEnd, this->m_itrEnd);
						FXD::STD::copy(first, mid, itrPos);
					}
					this->m_itrEnd = itrNewEnd;
				}
			}
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		void Deque< T, Allocator, kDequeSubarraySize >::_insert_values(const_iterator pos, size_type nCount, const value_type& val)
		{
			if (pos.m_pCurrent == this->m_itrEnd.m_pCurrent) // If inserting at the end (i.e. appending)...
			{
				const iterator itrNewEnd(this->_realloc_sub_array(nCount, Internal::E_Side::Back));
				FXD::STD::uninitialized_fill(this->m_itrEnd, itrNewEnd, val);
				this->m_itrEnd = itrNewEnd;
			}
			else if (pos.m_pCurrent == this->m_itrBegin.m_pCurrent) // If inserting at the beginning...
			{
				const iterator itrNewBegin(this->_realloc_sub_array(nCount, Internal::E_Side::Front));
				FXD::STD::uninitialized_fill(itrNewBegin, this->m_itrBegin, val);
				this->m_itrBegin = itrNewBegin;
			}
			else
			{
				const difference_type nInsertionIndex = (pos - this->m_itrBegin);
				const size_type nSize = size();
				const value_type valSaved(val);

				if (nInsertionIndex < (difference_type)(nSize / 2)) // If the insertion index is in the front half of the Deque... grow the Deque at the front.
				{
					const iterator itrNewBegin(this->_realloc_sub_array(nCount, Internal::E_Side::Front));
					const iterator itrOldBegin(this->m_itrBegin);
					const iterator itrPos(this->m_itrBegin + nInsertionIndex); // We need to reset this value because the reallocation above can invalidate iterators.

					if (nInsertionIndex >= (difference_type)nCount) // If the newly inserted items will be entirely within the old area...
					{
						iterator itrUCopyEnd(this->m_itrBegin + (difference_type)nCount);

						FXD::STD::uninitialized_move_if_noexcept(this->m_itrBegin, itrUCopyEnd, itrNewBegin); // This can throw.
						itrUCopyEnd = FXD::STD::move(itrUCopyEnd, itrPos, itrOldBegin); // Recycle 'itrUCopyEnd' to mean something else.
						FXD::STD::fill(itrUCopyEnd, itrPos, valSaved);
					}
					else // Else the newly inserted items are going within the newly allocated area at the front.
					{
						FXD::STD::uninitialized_move_fill(this->m_itrBegin, itrPos, itrNewBegin, this->m_itrBegin, valSaved); // This can throw.
						FXD::STD::fill(itrOldBegin, itrPos, valSaved);
					}
					this->m_itrBegin = itrNewBegin;
				}
				else // Else the insertion index is in the back half of the Deque, so grow the Deque at the back.
				{
					const iterator itrNewEnd(this->_realloc_sub_array(nCount, Internal::E_Side::Back));
					const iterator itrOldEnd(this->m_itrEnd);
					const difference_type nPushedCount = ((difference_type)nSize - nInsertionIndex);
					const iterator itrPos(this->m_itrEnd - nPushedCount);

					if (nPushedCount > (difference_type)nCount) // If the newly inserted items will be entirely within the old area...
					{
						iterator itrUCopyEnd(this->m_itrEnd - (difference_type)nCount);

						FXD::STD::uninitialized_move_if_noexcept(itrUCopyEnd, this->m_itrEnd, this->m_itrEnd);
						itrUCopyEnd = FXD::STD::move_backward(itrPos, itrUCopyEnd, itrOldEnd);
						FXD::STD::fill(itrPos, itrUCopyEnd, valSaved);
					}
					else // Else the newly inserted items are going within the newly allocated area at the back.
					{
						FXD::STD::uninitialized_fill_move(this->m_itrEnd, itrPos + (difference_type)nCount, valSaved, itrPos, this->m_itrEnd); // This can throw.
						FXD::STD::fill(itrPos, itrOldEnd, valSaved);
					}
					this->m_itrEnd = itrNewEnd;
				}
			}
		}

		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline void Deque< T, Allocator, kDequeSubarraySize >::_swap(my_type& rhs)
		{
			FXD::STD::swap(this->m_pPtrArray, rhs.m_pPtrArray);
			FXD::STD::swap(this->m_nPtrArraySize, rhs.m_nPtrArraySize);
			FXD::STD::swap(this->m_itrBegin, rhs.m_itrBegin);
			FXD::STD::swap(this->m_itrEnd, rhs.m_itrEnd);
		}


		// Non-member comparison functions
		// Equality
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline bool operator==(const Deque< T, Allocator, kDequeSubarraySize >& lhs, const Deque< T, Allocator, kDequeSubarraySize >& rhs)
		{
			return ((lhs.size() == rhs.size()) && equal(lhs.begin(), lhs.end(), rhs.begin()));
		}
		// Inequality
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline bool operator!=(const Deque< T, Allocator, kDequeSubarraySize >& lhs, const Deque< T, Allocator, kDequeSubarraySize >& rhs)
		{
			return ((lhs.size() != rhs.size()) || !equal(lhs.begin(), lhs.end(), rhs.begin()));
		}
		// Less than
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline bool operator<(const Deque< T, Allocator, kDequeSubarraySize >& lhs, const Deque< T, Allocator, kDequeSubarraySize >& rhs)
		{
			return FXD::STD::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
		}
		// Greater than
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline bool operator>(const Deque< T, Allocator, kDequeSubarraySize >& lhs, const Deque< T, Allocator, kDequeSubarraySize >& rhs)
		{
			return (rhs < lhs);
		}
		// Less than or equal to
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline bool operator<=(const Deque< T, Allocator, kDequeSubarraySize >& lhs, const Deque< T, Allocator, kDequeSubarraySize >& rhs)
		{
			return !(rhs < lhs);
		}
		// Greater than or equal to
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline bool operator>=(const Deque< T, Allocator, kDequeSubarraySize >& lhs, const Deque< T, Allocator, kDequeSubarraySize >& rhs)
		{
			return !(lhs < rhs);
		}

		// Inserters and extractors
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		Core::IStreamOut& operator<<(Core::IStreamOut& ostr, const Container::Deque< T, Allocator, kDequeSubarraySize >& list)
		{
			fxd_for(auto& i, list)
			{
				ostr << " " << i;
			}
			return ostr;
		}
	} //namespace Container

	namespace STD
	{
		template < typename T, typename Allocator, FXD::U32 kDequeSubarraySize >
		inline void swap(FXD::Container::Deque< T, Allocator, kDequeSubarraySize >& lhs, FXD::Container::Deque< T, Allocator, kDequeSubarraySize >& rhs)
		{
			lhs.swap(rhs);
		}

		template < typename T, typename Allocator, typename Pred >
		void erase_if(FXD::Container::Deque< T, Allocator >& container, Pred pred)
		{
			container.erase(FXD::STD::remove_if(container.begin(), container.end(), pred), container.end());
		}

		template < typename T, typename Allocator, typename U >
		void erase(FXD::Container::Deque< T, Allocator >& container, const U& val)
		{
			container.erase(FXD::STD::remove(container.begin(), container.end(), val), container.end());
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_DEQUE_H