// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_TUPLE_H
#define FXDENGINE_CONTAINER_TUPLE_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Constants.h"
#include "FXDEngine/Core/Internal/Conjunction.h"
#include "FXDEngine/Core/Internal/Disjunction.h"
#include "FXDEngine/Core/Internal/EnableIf.h"
#include "FXDEngine/Core/Internal/IntegerSequence.h"
#include "FXDEngine/Core/Internal/IsAssignable.h"
#include "FXDEngine/Core/Internal/IsAnyOf.h"
#include "FXDEngine/Core/Internal/IsConstructible.h"
#include "FXDEngine/Core/Internal/IsConvertible.h"
#include "FXDEngine/Core/Internal/IsCopyAssignable.h"
#include "FXDEngine/Core/Internal/IsDefaultConstructible.h"
#include "FXDEngine/Core/Internal/IsImplicitlyDefaultConstructible.h"
#include "FXDEngine/Core/Internal/IsNoThrowAssignable.h"
#include "FXDEngine/Core/Internal/IsNoThrowConstructible.h"
#include "FXDEngine/Core/Internal/IsNoThrowCopyAssignable.h"
#include "FXDEngine/Core/Internal/IsNoThrowCopyConstructible.h"
#include "FXDEngine/Core/Internal/IsNoThrowDefaultConstructible.h"
#include "FXDEngine/Core/Internal/IsNoThrowSwappable.h"
#include "FXDEngine/Core/Internal/IsSame.h"
#include "FXDEngine/Core/Internal/IsSwappable.h"
#include "FXDEngine/Core/Internal/Negation.h"
#include "FXDEngine/Core/Internal/RemoveConst.h"
#include "FXDEngine/Core/Internal/RemoveReference.h"
#include "FXDEngine/Core/Internal/RemoveReferenceWrapper.h"
#include "FXDEngine/Core/Internal/Swap.h"
#include "FXDEngine/Core/Internal/TupleElement.h"
#include "FXDEngine/Core/Internal/TupleSize.h"
#include "FXDEngine/Memory/Internal/AllocatorArgT.h"
#include "FXDEngine/Memory/Internal/UsesAllocator.h"

namespace FXD
{
	namespace Container
	{
		namespace Internal
		{
			// Tuple's implicit constructors
			template < bool IsSame, typename Dst, typename... Src >
			CONSTEXPR bool _tuple_implicit_impl = false;

			template < typename... Dst, typename... Src >
			CONSTEXPR bool _tuple_implicit_impl< true, FXD::Container::Tuple< Dst... >, Src... > = FXD::STD::conjunction< FXD::STD::is_constructible< Dst, Src >..., FXD::STD::is_convertible< Src, Dst >... >::value;

			template < typename Dst, typename... Src >
			struct _tuple_implicit : FXD::STD::integral_constant< bool, Container::Internal::_tuple_implicit_impl< FXD::tuple_size< Dst >::value == sizeof...(Src), Dst, Src... > >
			{};

			// Tuple's EXPLICIT constructors
			template < bool IsSame, typename Dst, typename... Src >
			CONSTEXPR bool _tuple_explicit_impl = false;

			template < typename... Dst, typename... Src >
			CONSTEXPR bool _tuple_explicit_impl< true, FXD::Container::Tuple< Dst... >, Src... > = FXD::STD::conjunction< FXD::STD::is_constructible< Dst, Src >..., FXD::STD::negation< FXD::STD::conjunction< FXD::STD::is_convertible< Src, Dst >... > > >::value;

			template < typename Dst, typename... Src >
			struct _tuple_explicit : FXD::STD::integral_constant< bool, Container::Internal::_tuple_explicit_impl< FXD::tuple_size< Dst >::value == sizeof...(Src), Dst, Src... > >
			{};

			// VARIABLE TEMPLATE _tuple_constructible
			template < bool IsSame, typename Dst, typename... Src >
			CONSTEXPR bool _tuple_constructible_impl = false;

			template < typename... Dst, typename... Src >
			CONSTEXPR bool _tuple_constructible_impl< true, FXD::Container::Tuple< Dst... >, Src... > = FXD::STD::conjunction< FXD::STD::is_constructible< Dst, Src >... >::value;

			template < typename Dst, typename... Src >
			CONSTEXPR bool _tuple_constructible = FXD::Container::Internal::_tuple_constructible_impl< FXD::tuple_size< Dst >::value == sizeof...(Src), Dst, Src... >;

			// VARIABLE TEMPLATE _tuple_nothrow_constructible
			template < bool IsSame, typename Dst, typename... Src >
			CONSTEXPR bool _tuple_nothrow_constructible_impl = false;

			template < typename... Dst, typename... Src >
			CONSTEXPR bool _tuple_nothrow_constructible_impl< true, FXD::Container::Tuple< Dst... >, Src... > = FXD::STD::conjunction< FXD::STD::is_nothrow_constructible< Dst, Src >... >::value;

			template < typename Dst, typename... Src >
			CONSTEXPR bool _tuple_nothrow_constructible = FXD::Container::Internal::_tuple_nothrow_constructible_impl< FXD::tuple_size< Dst >::value == sizeof...(Src), Dst, Src... >;

			// VARIABLE TEMPLATE _tuple_assignable_v
			template < bool IsSame, typename Dst, typename... Src >
			CONSTEXPR bool _tuple_assignable_impl = false;

			template < typename... Dst, typename... Src >
			CONSTEXPR bool _tuple_assignable_impl< true, FXD::Container::Tuple< Dst... >, Src... > = FXD::STD::conjunction< FXD::STD::is_assignable< Dst&, Src >... >::value;

			template < typename Dst, typename... Src >
			CONSTEXPR bool _tuple_assignable_v = FXD::Container::Internal::_tuple_assignable_impl< FXD::tuple_size< Dst >::value == sizeof...(Src), Dst, Src... >;

			template < typename Dst, typename... Src >
			struct _tuple_assignable : FXD::STD::integral_constant< bool, _tuple_assignable_v< Dst, Src... > >
			{};

			// VARIABLE TEMPLATE _tuple_nothrow_assignable
			template < bool IsSame, typename Dst, typename... Src >
			CONSTEXPR bool _tuple_nothrow_assignable_impl = false;

			template < typename... Dst, typename... Src >
			CONSTEXPR bool _tuple_nothrow_assignable_impl< true, FXD::Container::Tuple< Dst... >, Src... > = FXD::STD::conjunction< FXD::STD::is_nothrow_assignable< Dst&, Src >... >::value;

			template < typename Dst, typename... Src >
			CONSTEXPR bool _tuple_nothrow_assignable = FXD::Container::Internal::_tuple_nothrow_assignable_impl< FXD::tuple_size< Dst >::value == sizeof...(Src), Dst, Src... >;
			
			// Tuple's converting copy constructor
			template < typename ThisType, typename... Other >
			struct _tuple_convert_copy : FXD::STD::true_type
			{};

			template < typename ThisType, typename Other >
			struct _tuple_convert_copy< FXD::Container::Tuple< ThisType >, Other > : FXD::STD::integral_constant< bool, !FXD::STD::disjunction< FXD::STD::is_same< ThisType, Other >, FXD::STD::is_constructible< ThisType, const FXD::Container::Tuple< Other >& >, FXD::STD::is_convertible< const FXD::Container::Tuple< Other >&, ThisType > >::value >
			{};

			// Tuple's converting move constructor
			template < typename ThisType, typename... Other >
			struct _tuple_convert_move : FXD::STD::true_type
			{};

			template < typename ThisType, typename Other >
			struct _tuple_convert_move< FXD::Container::Tuple< ThisType >, Other > : FXD::STD::integral_constant< bool, !FXD::STD::disjunction< FXD::STD::is_same< ThisType, Other >, FXD::STD::is_constructible< ThisType, FXD::Container::Tuple< Other > >, FXD::STD::is_convertible< FXD::Container::Tuple< Other >, ThisType > >::value >
			{};

			// Tuple's perfect forwarding constructor
			template < typename ThisType, typename Other, typename... Args >
			struct _tuple_perfect : FXD::STD::true_type
			{};

			template < typename ThisType, typename Other >
			struct _tuple_perfect< ThisType, Other > : FXD::STD::integral_constant< bool, !FXD::STD::is_same< ThisType, FXD::STD::remove_const_t< FXD::STD::remove_reference_t< Other > > >::value >
			{};

			// Tag to disambiguate construction (from one arg per element)
			struct _Exact_args_t
			{
				EXPLICIT _Exact_args_t() = default;
			};

			// Tag to disambiguate construction (from unpacking a Tuple/Pair)
			struct _Unpack_tuple_t
			{
				EXPLICIT _Unpack_tuple_t() = default;
			}; 

			// Tag to disambiguate construction (from an allocator and one arg per element)
			struct _Alloc_exact_args_t
			{
				EXPLICIT _Alloc_exact_args_t() = default;
			};
			
			// Tag to disambiguate construction (from an allocator and unpacking a Tuple/pair)
			struct _Alloc_unpack_tuple_t
			{
				EXPLICIT _Alloc_unpack_tuple_t() = default;
			};

			// STRUCT _ignore
			struct _ignore
			{
				// struct that ignores assignments
				template < typename T >
				CONSTEXPR const _ignore& operator=(const T&) const NOEXCEPT
				{
					return (*this);
				}
			};

			// ------
			// _TupleNode
			// -
			// ------
			template < typename T >
			struct _TupleNode
			{
			public:
				CONSTEXPR _TupleNode(void)
					: m_val()
				{}

				template < typename Other >
				CONSTEXPR _TupleNode(Other&& arg)
					: m_val(std::forward< Other >(arg))
				{}

				template < typename Allocator, typename... Other, FXD::STD::enable_if_t< !FXD::STD::uses_allocator_v< T, Allocator >, FXD::S32 > = 0 >
				CONSTEXPR _TupleNode(const Allocator&, FXD::STD::allocator_arg_t, Other&&... arg)
					: m_val(std::forward< Other >(arg)...)
				{}

				template < typename Allocator, typename... Other, FXD::STD::enable_if_t< FXD::STD::conjunction< FXD::STD::uses_allocator< T, Allocator >, FXD::STD::is_constructible< T, FXD::STD::allocator_arg_t, const Allocator&, Other... > >::value, FXD::S32 > = 0 >
				CONSTEXPR _TupleNode(const Allocator& alloc, FXD::STD::allocator_arg_t, Other&&... arg)
					: m_val(FXD::STD::allocator_arg, alloc, std::forward< Other >(arg)...)
				{}

				template < typename Allocator, typename... Other, FXD::STD::enable_if_t< FXD::STD::conjunction< FXD::STD::uses_allocator< T, Allocator >, FXD::STD::negation< FXD::STD::is_constructible< T, FXD::STD::allocator_arg_t, const Allocator&, Other... > > >::value, FXD::S32 > = 0 >
				CONSTEXPR _TupleNode(const Allocator& alloc, FXD::STD::allocator_arg_t, Other&&... arg)
					: m_val(std::forward< Other >(arg)..., alloc)
				{}

			public:
				T m_val;
			};
		} //namespace Internal


		CONSTEXPR FXD::Container::Internal::_ignore ignore
		{};

		template < typename... Args >
		class Tuple;

		// ------
		// Tuple<>
		// -
		// ------
		template <>
		class Tuple<>
		{
		public:
			CONSTEXPR Tuple(void) NOEXCEPT
			{}

			CONSTEXPR14 Tuple(const Tuple&) NOEXCEPT
			{}

			template < typename Allocator >
			Tuple(FXD::STD::allocator_arg_t, const Allocator&) NOEXCEPT
			{}

			template < typename Allocator >
			Tuple(FXD::STD::allocator_arg_t, const Allocator&, const Tuple&) NOEXCEPT
			{}

			template < typename Tag, FXD::STD::enable_if_t< FXD::STD::is_same< Tag, FXD::Container::Internal::_Exact_args_t >::value, FXD::S32 > = 0 >
			CONSTEXPR14 Tuple(Tag) NOEXCEPT
			{}

			template < typename Tag, typename Allocator, FXD::STD::enable_if_t< FXD::STD::is_same< Tag, FXD::Container::Internal::_Alloc_exact_args_t >::value, FXD::S32 > = 0 >
			CONSTEXPR14 Tuple(Tag, const Allocator&) NOEXCEPT
			{}

			CONSTEXPR14 Tuple& operator=(const Tuple&) = default;

			void swap(Tuple&) NOEXCEPT
			{}

		protected:
			CONSTEXPR14 bool _equals(const Tuple&) const NOEXCEPT
			{
				return true;
			}

			CONSTEXPR14 bool _less(const Tuple&) const NOEXCEPT
			{
				return false;
			}
		};

		// ------
		// Tuple
		// -
		// ------
		template < typename ThisType, typename... RestArg >
		class Tuple< ThisType, RestArg... > : private Container::Tuple< RestArg... >
		{
		public:
			using this_type = ThisType;
			using base_type = Container::Tuple< RestArg... >;

			template < typename Tag, typename ThisType2, typename... RestArg2, FXD::STD::enable_if_t< FXD::STD::is_same< Tag, Container::Internal::_Exact_args_t >::value, FXD::S32 > = 0 >
			CONSTEXPR14 Tuple(Tag, ThisType2&& thisArg, RestArg2&&... restArg)
				: base_type(Container::Internal::_Exact_args_t{}, std::forward< RestArg2 >(restArg)...)
				, m_first(std::forward< ThisType2 >(thisArg))
			{}

			template < typename Tag, typename Tup, size_t... IDX, FXD::STD::enable_if_t< FXD::STD::is_same< Tag, Container::Internal::_Unpack_tuple_t >::value, FXD::S32 > = 0 >
			CONSTEXPR14 Tuple(Tag, Tup&& rhs, FXD::STD::index_sequence< IDX... >) : Tuple(Container::Internal::_Exact_args_t{}, FXD::get< IDX >(std::forward< Tup >(rhs))...)
			{}

			template < typename Tag, typename Tup, FXD::STD::enable_if_t< FXD::STD::is_same< Tag, Container::Internal::_Unpack_tuple_t >::value, FXD::S32 > = 0 >
			CONSTEXPR14 Tuple(Tag, Tup&& rhs) : Tuple(Container::Internal::_Unpack_tuple_t{}, std::forward< Tup >(rhs), FXD::STD::make_index_sequence< FXD::tuple_size< FXD::STD::remove_reference_t< Tup > >::value >{})
			{}

			template < typename Tag, typename Allocator, typename ThisType2, typename... RestArg2, FXD::STD::enable_if_t< FXD::STD::is_same< Tag, Container::Internal::_Alloc_exact_args_t >::value, FXD::S32 > = 0 >
			CONSTEXPR14 Tuple(Tag, const Allocator& alloc, ThisType2&& thisArg, RestArg2&&... restArg)
				: base_type(Container::Internal::_Alloc_exact_args_t{}, alloc, std::forward< RestArg2 >(restArg)...), m_first(alloc, FXD::STD::allocator_arg, std::forward< ThisType2 >(thisArg))
			{}

			template < typename Tag, typename Allocator, typename Tup, size_t... IDX, FXD::STD::enable_if_t< FXD::STD::is_same< Tag, Container::Internal::_Alloc_unpack_tuple_t >::value, FXD::S32 > = 0 >
			CONSTEXPR14 Tuple(Tag, const Allocator& alloc, Tup&& rhs, FXD::STD::index_sequence< IDX... >) : Tuple(Container::Internal::_Alloc_exact_args_t{}, alloc, FXD::get< IDX >(std::forward< Tup >(rhs))...)
			{}

			template < typename Tag, typename Allocator, typename Tup, FXD::STD::enable_if_t< FXD::STD::is_same< Tag, Container::Internal::_Alloc_unpack_tuple_t >::value, FXD::S32 > = 0 >
			CONSTEXPR14 Tuple(Tag, const Allocator& alloc, Tup&& rhs)
				: Tuple(Container::Internal::_Alloc_unpack_tuple_t{}, alloc, std::forward< Tup >(rhs), FXD::STD::make_index_sequence< FXD::tuple_size< FXD::STD::remove_reference_t< Tup > >::value >{})
			{}

			template < typename ThisType2 = ThisType, FXD::STD::enable_if_t< FXD::STD::conjunction< FXD::STD::is_default_constructible< ThisType2 >, FXD::STD::is_default_constructible< RestArg >..., FXD::STD::is_implicitly_default_constructible< ThisType2 >, FXD::STD::is_implicitly_default_constructible< RestArg >... >::value, FXD::S32 > = 0 >
			CONSTEXPR14 Tuple(void) NOEXCEPT(FXD::STD::conjunction< FXD::STD::is_nothrow_default_constructible< ThisType2 >, FXD::STD::is_nothrow_default_constructible< RestArg >... >::value)
				: base_type()
				, m_first()
			{}

			template < typename ThisType2 = ThisType, FXD::STD::enable_if_t< FXD::STD::conjunction< FXD::STD::is_default_constructible< ThisType2 >, FXD::STD::is_default_constructible< RestArg >..., FXD::STD::negation< FXD::STD::conjunction< FXD::STD::is_implicitly_default_constructible< ThisType2 >, FXD::STD::is_implicitly_default_constructible< RestArg >... > > >::value, FXD::S32 > = 0 >
			CONSTEXPR14 EXPLICIT Tuple(void) NOEXCEPT(FXD::STD::conjunction< FXD::STD::is_nothrow_default_constructible< ThisType2 >, FXD::STD::is_nothrow_default_constructible< RestArg >... >::value)
				: base_type()
				, m_first()
			{}

			template < typename ThisType2 = ThisType, FXD::STD::enable_if_t< Container::Internal::_tuple_implicit< Tuple, const ThisType2&, const RestArg&... >::value, FXD::S32 > = 0 >
			CONSTEXPR14 Tuple(const ThisType& thisArg, const RestArg&... restArg) NOEXCEPT(FXD::STD::conjunction< FXD::STD::is_nothrow_copy_constructible< ThisType2 >, FXD::STD::is_nothrow_copy_constructible< RestArg >... >::value)
				: Tuple(Container::Internal::_Exact_args_t{}, thisArg, restArg...)
			{}

			template < typename ThisType2 = ThisType, FXD::STD::enable_if_t< Container::Internal::_tuple_explicit< Tuple, const ThisType2&, const RestArg&... >::value, FXD::S32 > = 0 >
			CONSTEXPR14 EXPLICIT Tuple(const ThisType& thisArg, const RestArg&... restArg) NOEXCEPT(FXD::STD::conjunction< FXD::STD::is_nothrow_copy_constructible< ThisType2 >, FXD::STD::is_nothrow_copy_constructible< RestArg >... >::value)
				: Tuple(Container::Internal::_Exact_args_t{}, thisArg, restArg...)
				{}

			template < typename ThisType2, typename... RestArg2, FXD::STD::enable_if_t< FXD::STD::conjunction< Container::Internal::_tuple_perfect< Tuple, ThisType2, RestArg2... >, Container::Internal::_tuple_implicit< Tuple , ThisType2, RestArg2... > >::value, FXD::S32 > = 0 >
			CONSTEXPR14 Tuple(ThisType2&& thisArg, RestArg2&&... restArg) NOEXCEPT(Container::Internal::_tuple_nothrow_constructible< Tuple, ThisType2, RestArg2... >)
				: Tuple(Container::Internal::_Exact_args_t{}, std::forward< ThisType2 >(thisArg), std::forward< RestArg2 >(restArg)...)
			{}

			template < typename ThisType2, typename... RestArg2, FXD::STD::enable_if_t< FXD::STD::conjunction< Container::Internal::_tuple_perfect< Tuple, ThisType2, RestArg2... >, Container::Internal::_tuple_explicit< Tuple, ThisType2, RestArg2... > >::value, FXD::S32 > = 0 >
			CONSTEXPR14 EXPLICIT Tuple(ThisType2&& thisArg, RestArg2&&... restArg) NOEXCEPT(Container::Internal::_tuple_nothrow_constructible< Tuple, ThisType2, RestArg2... >)
				: Tuple(Container::Internal::_Exact_args_t{}, std::forward< ThisType2 >(thisArg), std::forward< RestArg2 >(restArg)...)
			{}

			Tuple(const Tuple&) = default;
			Tuple(Tuple&&) = default;

			template < typename... Other, FXD::STD::enable_if_t< FXD::STD::conjunction< Container::Internal::_tuple_implicit< Tuple, const Other&... >, Container::Internal::_tuple_convert_copy< Tuple, Other... > >::value, FXD::S32 > = 0 >
			CONSTEXPR14 Tuple(const Tuple< Other... >& rhs) NOEXCEPT(Container::Internal::_tuple_nothrow_constructible< Tuple, const Other&... >)
				: Tuple(Container::Internal::_Unpack_tuple_t{}, rhs)
			{}

			template < typename... Other, FXD::STD::enable_if_t< FXD::STD::conjunction< Container::Internal::_tuple_explicit< Tuple, const Other&... >, Container::Internal::_tuple_convert_copy< Tuple, Other... > >::value, FXD::S32 > = 0 >
			CONSTEXPR14 EXPLICIT Tuple(const Tuple< Other... >& rhs) NOEXCEPT(Container::Internal::_tuple_nothrow_constructible< Tuple, const Other&... >)
				: Tuple(Container::Internal::_Unpack_tuple_t{}, rhs)
			{}

			template < typename... Other, FXD::STD::enable_if_t< FXD::STD::conjunction< Container::Internal::_tuple_implicit< Tuple, Other... >, Container::Internal::_tuple_convert_move< Tuple, Other... > >::value, FXD::S32 > = 0 >
			CONSTEXPR14 Tuple(Tuple< Other... >&& rhs) NOEXCEPT(Container::Internal::_tuple_nothrow_constructible< Tuple, Other... >)
				: Tuple(Container::Internal::_Unpack_tuple_t{}, std::move(rhs))
			{}

			template < typename... Other, FXD::STD::enable_if_t< FXD::STD::conjunction< Container::Internal::_tuple_explicit< Tuple, Other... >, Container::Internal::_tuple_convert_move< Tuple, Other... > >::value, FXD::S32 > = 0 >
			CONSTEXPR14 EXPLICIT Tuple(Tuple< Other... >&& rhs) NOEXCEPT(Container::Internal::_tuple_nothrow_constructible< Tuple, Other... >)
				: Tuple(Container::Internal::_Unpack_tuple_t{}, std::move(rhs))
			{}

			template < typename T1, typename T2, FXD::STD::enable_if_t< Container::Internal::_tuple_implicit< Tuple, const T1&, const T2& >::value, FXD::S32 > = 0 >
			CONSTEXPR14 Tuple(const Core::Pair< T1, T2 >& rhs) NOEXCEPT(Container::Internal::_tuple_nothrow_constructible< Tuple, const T1&, const T2& >)
				: Tuple(Container::Internal::_Unpack_tuple_t{}, rhs)
			{}

			template < typename T1, typename T2, FXD::STD::enable_if_t< Container::Internal::_tuple_explicit< Tuple, const T1&, const T2& >::value, FXD::S32 > = 0 >
			CONSTEXPR14 EXPLICIT Tuple(const Core::Pair< T1, T2 >& rhs) NOEXCEPT(Container::Internal::_tuple_nothrow_constructible< Tuple, const T1&, const T2& >)
				: Tuple(Container::Internal::_Unpack_tuple_t{}, rhs)
			{}

			template < typename T1, typename T2, FXD::STD::enable_if_t< Container::Internal::_tuple_implicit< Tuple, T1, T2 >::value, FXD::S32 > = 0 >
			CONSTEXPR14 Tuple(Core::Pair< T1, T2 >&& rhs) NOEXCEPT(Container::Internal::_tuple_nothrow_constructible< Tuple, T1, T2 >)
				: Tuple(Container::Internal::_Unpack_tuple_t{}, std::move(rhs))
			{}

			template < typename T1, typename T2, FXD::STD::enable_if_t< Container::Internal::_tuple_explicit< Tuple, T1, T2 >::value, FXD::S32 > = 0 >
			CONSTEXPR14 EXPLICIT Tuple(Core::Pair< T1, T2 >&& rhs) NOEXCEPT(Container::Internal::_tuple_nothrow_constructible< Tuple, T1, T2 >)
				: Tuple(Container::Internal::_Unpack_tuple_t{}, std::move(rhs))
			{}

			template < typename Allocator, typename ThisType2 = ThisType, FXD::STD::enable_if_t< FXD::STD::conjunction< FXD::STD::is_default_constructible< ThisType2 >, FXD::STD::is_default_constructible< RestArg >..., FXD::STD::is_implicitly_default_constructible< ThisType2 >, FXD::STD::is_implicitly_default_constructible< RestArg >... >::value, FXD::S32 > = 0 >
			Tuple(FXD::STD::allocator_arg_t, const Allocator& alloc)
				: base_type(FXD::STD::allocator_arg, alloc)
				, m_first(alloc, FXD::STD::allocator_arg)
			{}

			template < typename Allocator, typename ThisType2 = ThisType, FXD::STD::enable_if_t< FXD::STD::conjunction< FXD::STD::is_default_constructible< ThisType2 >, FXD::STD::is_default_constructible< RestArg >..., FXD::STD::negation< FXD::STD::conjunction< FXD::STD::is_implicitly_default_constructible< ThisType2 >, FXD::STD::is_implicitly_default_constructible< RestArg >... > > >::value, FXD::S32 > = 0 >
			EXPLICIT Tuple(FXD::STD::allocator_arg_t, const Allocator& alloc)
				: base_type(FXD::STD::allocator_arg, alloc)
				, m_first(alloc, FXD::STD::allocator_arg)
			{}

			template < typename Allocator, typename ThisType2 = ThisType, FXD::STD::enable_if_t< Container::Internal::_tuple_implicit< Tuple, const ThisType2&, const RestArg&... >::value, FXD::S32 > = 0 >
			Tuple(FXD::STD::allocator_arg_t, const Allocator& alloc, const ThisType& thisArg, const RestArg&... restArg)
				: Tuple(Container::Internal::_Alloc_exact_args_t{}, alloc, thisArg, restArg...)
			{}

			template < typename Allocator, typename ThisType2 = ThisType, FXD::STD::enable_if_t< Container::Internal::_tuple_explicit< Tuple, const ThisType2&, const RestArg&... >::value, FXD::S32 > = 0 >
			EXPLICIT Tuple(FXD::STD::allocator_arg_t, const Allocator& alloc, const ThisType& thisArg, const RestArg&... restArg)
				: Tuple(Container::Internal::_Alloc_exact_args_t{}, alloc, thisArg, restArg...)
			{}

			template < typename Allocator, typename ThisType2, typename... RestArg2, FXD::STD::enable_if_t< FXD::STD::conjunction< Container::Internal::_tuple_perfect< Tuple, ThisType2, RestArg2... >, Container::Internal::_tuple_implicit< Tuple, ThisType2, RestArg2... > >::value, FXD::S32 > = 0 >
			Tuple(FXD::STD::allocator_arg_t, const Allocator& alloc, ThisType2&& thisArg, RestArg2&&... restArg)
				: Tuple(Container::Internal::_Alloc_exact_args_t{}, alloc, std::forward< ThisType2 >(thisArg), std::forward< RestArg2 >(restArg)...)
			{}

			template < typename Allocator, typename ThisType2, typename... RestArg2, FXD::STD::enable_if_t< FXD::STD::conjunction< Container::Internal::_tuple_perfect< Tuple, ThisType2, RestArg2... >, Container::Internal::_tuple_explicit< Tuple, ThisType2, RestArg2... > >::value, FXD::S32 > = 0 >
			EXPLICIT Tuple(FXD::STD::allocator_arg_t, const Allocator& alloc, ThisType2&& thisArg, RestArg2&&... restArg)
				: Tuple(Container::Internal::_Alloc_exact_args_t{}, alloc, std::forward< ThisType2 >(thisArg), std::forward< RestArg >(restArg)...)
			{}

			template < typename Allocator, typename ThisType2 = ThisType, FXD::STD::enable_if_t< Container::Internal::_tuple_constructible< Tuple, const ThisType2&, const RestArg&... >, FXD::S32 > = 0 >
			Tuple(FXD::STD::allocator_arg_t, const Allocator& alloc, const Tuple& rhs)
				: Tuple(Container::Internal::_Alloc_unpack_tuple_t{}, alloc, rhs)
			{}

			template < typename Allocator, typename ThisType2 = ThisType, FXD::STD::enable_if_t< Container::Internal::_tuple_constructible< Tuple, ThisType2, RestArg... >, FXD::S32 > = 0 >
			Tuple(FXD::STD::allocator_arg_t, const Allocator& alloc, Tuple&& rhs)
				: Tuple(Container::Internal::_Alloc_unpack_tuple_t{}, alloc, std::move(rhs))
			{}

			template < typename Allocator, typename... Other, FXD::STD::enable_if_t< FXD::STD::conjunction< Container::Internal::_tuple_implicit< Tuple, const Other&... >, Container::Internal::_tuple_convert_copy< Tuple, Other... > >::value, FXD::S32 > = 0 >
			Tuple(FXD::STD::allocator_arg_t, const Allocator& alloc, const Tuple< Other... >& rhs)
				: Tuple(Container::Internal::_Alloc_unpack_tuple_t{}, alloc, rhs)
			{}

			template < typename Allocator, typename... Other, FXD::STD::enable_if_t< FXD::STD::conjunction< Container::Internal::_tuple_explicit< Tuple, const Other&... >, Container::Internal::_tuple_convert_copy< Tuple, Other... > >::value, FXD::S32 > = 0 >
			EXPLICIT Tuple(FXD::STD::allocator_arg_t, const Allocator& alloc, const Tuple< Other... >& rhs)
				: Tuple(Container::Internal::_Alloc_unpack_tuple_t{}, alloc, rhs)
			{}

			template < typename Allocator, typename... Other, FXD::STD::enable_if_t< FXD::STD::conjunction< Container::Internal::_tuple_implicit< Tuple, Other... >, Container::Internal::_tuple_convert_move< Tuple, Other... > >::value, FXD::S32 > = 0 >
			Tuple(FXD::STD::allocator_arg_t, const Allocator& alloc, Tuple< Other... >&& rhs)
				: Tuple(Container::Internal::_Alloc_unpack_tuple_t{}, alloc, std::move(rhs))
			{}

			template < typename Allocator, typename... Other, FXD::STD::enable_if_t< FXD::STD::conjunction< Container::Internal::_tuple_explicit< Tuple, Other... >, Container::Internal::_tuple_convert_move< Tuple, Other... > >::value, FXD::S32 > = 0 >
			EXPLICIT Tuple(FXD::STD::allocator_arg_t, const Allocator& alloc, Tuple< Other... >&& rhs)
				: Tuple(Container::Internal::_Alloc_unpack_tuple_t{}, alloc, std::move(rhs))
			{}

			template < typename Allocator, typename T1, typename T2, FXD::STD::enable_if_t< Container::Internal::_tuple_implicit< Tuple, const T1&, const T2& >::value, FXD::S32 > = 0 >
			Tuple(FXD::STD::allocator_arg_t, const Allocator& alloc, const Core::Pair< T1, T2 >& rhs)
				: Tuple(Container::Internal::_Alloc_unpack_tuple_t{}, alloc, rhs)
			{}

			template < typename Allocator, typename T1, typename T2, FXD::STD::enable_if_t< Container::Internal::_tuple_explicit< Tuple, const T1&, const T2& >::value, FXD::S32 > = 0 >
			EXPLICIT Tuple(FXD::STD::allocator_arg_t, const Allocator& alloc, const Core::Pair< T1, T2 >& rhs)
				: Tuple(Container::Internal::_Alloc_unpack_tuple_t{}, alloc, rhs)
			{}

			template < typename Allocator, typename T1, typename T2, FXD::STD::enable_if_t< Container::Internal::_tuple_implicit< Tuple, T1, T2 >::value, FXD::S32 > = 0 >
			Tuple(FXD::STD::allocator_arg_t, const Allocator& alloc, Core::Pair< T1, T2 >&& rhs)
				: Tuple(Container::Internal::_Alloc_unpack_tuple_t{}, alloc, std::move(rhs))
			{}

			template < typename Allocator, typename T1, typename T2, FXD::STD::enable_if_t< Container::Internal::_tuple_explicit< Tuple, T1, T2 >::value, FXD::S32 > = 0 >
			EXPLICIT Tuple(FXD::STD::allocator_arg_t, const Allocator& alloc, Core::Pair< T1, T2 >&& rhs)
				: Tuple(Container::Internal::_Alloc_unpack_tuple_t{}, alloc, std::move(rhs))
			{}

			Tuple& operator=(const volatile Tuple&) = delete;

			template < typename MyType = Tuple, typename ThisType2 = ThisType, FXD::STD::enable_if_t< FXD::STD::conjunction< FXD::STD::is_copy_assignable< ThisType2 >, FXD::STD::is_copy_assignable< RestArg >... >::value, FXD::S32 > = 0 >
			Tuple& operator=(typename FXD::STD::type_identity< const MyType& >::type rhs) NOEXCEPT_COND(FXD::STD::conjunction< FXD::STD::is_nothrow_copy_assignable< ThisType2 >, FXD::STD::is_nothrow_copy_assignable< RestArg >... >::value)
			{
				m_first.m_val = rhs.m_first.m_val;
				_get_rest() = rhs._get_rest();
				return (*this);
			}

			template < typename MyType = Tuple, typename ThisType2 = ThisType, FXD::STD::enable_if_t< FXD::STD::conjunction< FXD::STD::is_move_assignable< ThisType2 >, FXD::STD::is_move_assignable< RestArg >... >::value, FXD::S32 > = 0>
			Tuple& operator=(typename FXD::STD::type_identity< MyType&& >::type rhs) NOEXCEPT_COND(FXD::STD::conjunction< FXD::STD::is_nothrow_move_assignable< ThisType2 >, FXD::STD::is_nothrow_move_assignable< RestArg >... >::value)
			{
				m_first.m_val = std::forward< ThisType >(rhs.m_first.m_val);
				_get_rest() = std::forward< base_type >(rhs._get_rest());
				return (*this);
			}

			template < typename... Other, FXD::STD::enable_if_t< FXD::STD::conjunction< FXD::STD::negation< FXD::STD::is_same< Tuple, Tuple< Other... > > >, Container::Internal::_tuple_assignable< Tuple, const Other&... > >::value, FXD::S32 > = 0 >
			Tuple& operator=(const Tuple< Other... >& rhs) NOEXCEPT(Container::Internal::_tuple_nothrow_assignable< Tuple, const Other&... >)
			{
				m_first.m_val = rhs.m_first.m_val;
				_get_rest() = rhs._get_rest();
				return (*this);
			}

			template < typename... Other, FXD::STD::enable_if_t< FXD::STD::conjunction< FXD::STD::negation< FXD::STD::is_same< Tuple, Tuple< Other... > > >, Container::Internal::_tuple_assignable< Tuple, Other... > >::value, FXD::S32 > = 0 >
			Tuple& operator=(Tuple< Other... >&& rhs) NOEXCEPT(Container::Internal::_tuple_nothrow_assignable< Tuple, Other... >)
			{
				m_first.m_val = std::forward< typename Tuple< Other... >::this_type >(rhs.m_first.m_val);
				_get_rest() = std::forward< typename Tuple< Other... >::base_type >(rhs._get_rest());
				return (*this);
			}

			template < typename T1, typename T2, FXD::STD::enable_if_t< Container::Internal::_tuple_assignable_v< Tuple, const T1&, const T2& >, FXD::S32 > = 0 >
			Tuple& operator=(const Core::Pair< T1, T2 >& rhs) NOEXCEPT(Container::Internal::_tuple_nothrow_assignable< Tuple, const T1&, const T2& >)
			{
				m_first.m_val = rhs.first;
				_get_rest().m_first.m_val = rhs.second;
				return (*this);
			}

			template < typename T1, typename T2, FXD::STD::enable_if_t< Container::Internal::_tuple_assignable_v< Tuple, T1, T2 >, FXD::S32 > = 0 >
			Tuple& operator=(Core::Pair< T1, T2 >&& rhs) NOEXCEPT(Container::Internal::_tuple_nothrow_assignable< Tuple, T1, T2 >)
			{
				m_first.m_val = std::forward< T1 >(rhs.first);
				_get_rest().m_first.m_val = std::forward< T2 >(rhs.second);
				return (*this);
			}

			void swap(Tuple& rhs) NOEXCEPT(FXD::STD::conjunction< FXD::STD::is_nothrow_swappable< ThisType >, FXD::STD::is_nothrow_swappable< RestArg >... >::value)
			{
				FXD::STD::swap(m_first.m_val, rhs.m_first.m_val);
				base_type::swap(rhs._get_rest());
			}

			template < typename... Other >
			CONSTEXPR14 bool _equals(const Tuple< Other... >& rhs) const
			{
				return ((m_first.m_val == rhs.m_first.m_val) && base_type::_equals(rhs._get_rest()));
			}

			template < typename... Other >
			CONSTEXPR14 bool _less(const Tuple< Other... >& rhs) const
			{
				return ((m_first.m_val < rhs.m_first.m_val) || (!(rhs.m_first.m_val < m_first.m_val) && base_type::_less(rhs._get_rest())));
			}

			CONSTEXPR14 base_type& _get_rest(void) NOEXCEPT
			{
				return (*this);
			}

			CONSTEXPR14 const base_type& _get_rest(void) const NOEXCEPT
			{
				return (*this);
			}

			template < size_t IDX, typename... Args >
			friend CONSTEXPR14 FXD::tuple_element_t< IDX, Container::Tuple< Args... > >& get(Container::Tuple< Args... >& tuple) NOEXCEPT;

			template < size_t IDX, typename... Args >
			friend CONSTEXPR14 const FXD::tuple_element_t< IDX, Container::Tuple< Args... > >& get(const Container::Tuple< Args... >& tuple) NOEXCEPT;

			template < size_t IDX, typename... Args >
			friend CONSTEXPR14 FXD::tuple_element_t< IDX, Container::Tuple< Args... > >&& get(Container::Tuple< Args... >&& tuple) NOEXCEPT;

			template < size_t IDX, typename... Args >
			friend CONSTEXPR14 const FXD::tuple_element_t< IDX, Container::Tuple< Args... > >&& get(const Container::Tuple< Args... >&& tuple) NOEXCEPT;

			template < typename T, typename... Args >
			friend CONSTEXPR14 T& get(Container::Tuple< Args... >& tuple) NOEXCEPT;

			template < typename T, typename... Args >
			friend CONSTEXPR14 const T& get(const Container::Tuple< Args... >& tuple) NOEXCEPT;

			template < typename T, typename... Args >
			friend CONSTEXPR14 T&& get(Container::Tuple< Args... >&& tuple) NOEXCEPT;

			template < typename T, typename... Args >
			friend CONSTEXPR14 const T&& get(const Container::Tuple< Args... >&& tuple) NOEXCEPT;

			Container::Internal::_TupleNode< ThisType > m_first;
		};

		/*
		#if _HAS_CXX17
			template < typename... Args >
			Tuple(Args...)->Tuple< Args... >;

			template < typename _Ty1, typename _Ty2 >
			Tuple(Core::Pair< _Ty1, _Ty2 >)->Tuple< _Ty1, _Ty2 >;

			template < typename _Alloc, typename... Args >
			Tuple(FXD::STD::allocator_arg_t, _Alloc, Args...)->Tuple< Args... >;

			template < typename _Alloc, typename _Ty1, typename _Ty2 >
			Tuple(FXD::STD::allocator_arg_t, _Alloc, Core::Pair< _Ty1, _Ty2 >)->Tuple< _Ty1, _Ty2 >;

			template < typename _Alloc, typename... Args >
			Tuple(FXD::STD::allocator_arg_t, _Alloc, Tuple< Args... >)->Tuple< Args... >;
		#endif // _HAS_CXX17
		*/

		// OPERATORS FOR Tuple
		template < typename... LHS, typename... RHS >
		CONSTEXPR14 bool operator==(const Container::Tuple< LHS... >& lhs, const Container::Tuple< RHS... >& rhs)
		{
			static_assert(sizeof...(LHS) == sizeof...(RHS), "cannot compare tuples of different sizes");
			return lhs._equals(rhs);
		}

		template < typename... LHS, typename... RHS >
		CONSTEXPR14 bool operator!=(const Container::Tuple< LHS... >& lhs, const Container::Tuple< RHS... >& rhs)
		{
			return !(lhs == rhs);
		}

		template < typename... LHS, typename... RHS >
		CONSTEXPR14 bool operator<(const Container::Tuple< LHS... >& lhs, const Container::Tuple< RHS... >& rhs)
		{
			static_assert(sizeof...(LHS) == sizeof...(RHS), "cannot compare tuples of different sizes");
			return lhs._less(rhs);
		}

		template < typename... LHS, typename... RHS >
		CONSTEXPR14 bool operator>=(const Container::Tuple< LHS... >& lhs, const Container::Tuple< RHS... >& rhs)
		{
			return !(lhs < rhs);
		}

		template < typename... LHS, typename... RHS >
		CONSTEXPR14 bool operator>(const Container::Tuple< LHS... >& lhs, const Container::Tuple< RHS... >& rhs)
		{
			return rhs < lhs;
		}

		template < typename... LHS, typename... RHS >
		CONSTEXPR14 bool operator<=(const Container::Tuple< LHS... >& lhs, const Container::Tuple< RHS... >& rhs)
		{
			return !(rhs < lhs);
		}
	} //namespace Container

	namespace STD
	{
		template < typename... Args, FXD::STD::enable_if_t< FXD::STD::conjunction< FXD::STD::is_swappable< Args >... >::value, FXD::S32 > = 0 >
		void swap(Container::Tuple< Args... >& lhs, Container::Tuple< Args... >& rhs) NOEXCEPT(NOEXCEPT(lhs.swap(rhs)))
		{
			return lhs.swap(rhs);
		}
	} //namespace STD

	namespace Internal
	{
		template < typename T, typename Tup >
		struct _tuple_element
		{};

		template < typename ThisType, typename... RestArg >
		struct _tuple_element< ThisType, Container::Tuple< ThisType, RestArg... > >
		{
			using tuple_type = Container::Tuple< ThisType, RestArg... >;
			static_assert(!FXD::STD::is_any_of< ThisType, RestArg... >::value, "duplicate type T in get<T>(Tuple)");
		};

		template < typename T, typename ThisType, typename... RestArg >
		struct _tuple_element< T, Container::Tuple< ThisType, RestArg... > >
		{
			using tuple_type = typename Internal::_tuple_element< T, Container::Tuple< RestArg... > >::tuple_type;
		};

		// STRUCT TEMPLATE _view_as_tuple
		template < typename T, typename... _For_array >
		struct _view_as_tuple
		{
			static_assert(FXD::STD::Internal::_Always_false< T >, "Unsupported tuple_cat arguments.");
		};

		template < typename... Args >
		struct _view_as_tuple< Container::Tuple< Args... > >
		{
			using type = Container::Tuple< Args... >;
		};

		template < typename T1, typename T2 >
		struct _view_as_tuple< Core::Pair< T1, T2 > >
		{
			using type = Container::Tuple< T1, T2 >;
		};

		template < typename T, typename... Args >
		struct _view_as_tuple< Container::Array< T, 0 >, Args... >
		{
			using type = Container::Tuple< Args... >;
		};

		template < typename T, size_t SIZE, typename... Args >
		struct _view_as_tuple< Container::Array< T, SIZE >, Args... > : Internal::_view_as_tuple< Container::Array< T, SIZE - 1 >, T, Args... >
		{};

		template < typename SeqType1, typename SeqType2 >
		struct _cat_sequences;

		template < size_t... INDICIES1, size_t... INDICIES2 >
		struct _cat_sequences< FXD::STD::index_sequence< INDICIES1... >, FXD::STD::index_sequence< INDICIES2... > >
		{
			using type = FXD::STD::index_sequence< INDICIES1..., INDICIES2... >;
		};

		template < size_t IDX, typename T >
		struct _tup_repeat_for : FXD::STD::integral_constant< size_t, IDX >
		{};

		// FUNCTION TEMPLATE tuple_cat
		template < typename Ret, typename KXArg, typename IXArg, size_t IXNEXT, typename... Tuples >
		struct _tuple_cat_type_impl
		{
			CTC_ASSERT((sizeof...(Tuples) == 0), "Unsupported tuple_cat arguments.");
			using type = Ret;
			using _Kx_arg_seq = KXArg;
			using _Ix_arg_seq = IXArg;
		};

		template < typename... Types1, typename KXArg, size_t... INDICES, size_t INDICESNEXT, typename... Types2, typename... RestArg >
		struct _tuple_cat_type_impl< Container::Tuple< Types1... >, KXArg, FXD::STD::index_sequence< INDICES... >, INDICESNEXT, Container::Tuple< Types2... >, RestArg... >
			: Internal::_tuple_cat_type_impl< Container::Tuple< Types1..., Types2... >, typename Internal::_cat_sequences< KXArg, FXD::STD::index_sequence_for< Types2... > >::type, FXD::STD::index_sequence< INDICES..., Internal::_tup_repeat_for< INDICESNEXT, Types2 >::value... >, INDICESNEXT + 1, RestArg... >
		{};

		template < typename... Tuples >
		struct _tuple_cat_type : Internal::_tuple_cat_type_impl< Container::Tuple<>, FXD::STD::index_sequence<>, FXD::STD::index_sequence<>, 0, typename Internal::_view_as_tuple< FXD::STD::decay_t< Tuples > >::type... >
		{};

		template < typename Ret, size_t... KX, size_t... IDX, typename T >
		CONSTEXPR14 Ret _tuple_cat(FXD::STD::index_sequence< KX... >, FXD::STD::index_sequence< IDX... >, T&& _Arg)
		{ // concatenate tuples
			return Ret(FXD::get< KX >(FXD::get< IDX >(std::forward< T >(_Arg)))...);
		}
	} //namespace Internal

	// FUNCTION TEMPLATE get (by index)
	template < size_t IDX, typename... Args >
	CONSTEXPR14 FXD::tuple_element_t< IDX, Container::Tuple< Args... > >& get(Container::Tuple< Args... >& tuple) NOEXCEPT
	{
		using tuple_type = typename FXD::tuple_element< IDX, Container::Tuple< Args... > >::tuple_type;
		return (((tuple_type&)tuple).m_first.m_val);
	}

	template < size_t IDX, typename... Args >
	CONSTEXPR14 const FXD::tuple_element_t< IDX, Container::Tuple< Args... > >& get(const Container::Tuple< Args... >& tuple) NOEXCEPT
	{
		using tuple_type = typename FXD::tuple_element< IDX, Container::Tuple< Args... > >::tuple_type;
		return (((const tuple_type&)tuple).m_first.m_val);
	}

	template < size_t IDX, typename... Args >
	CONSTEXPR14 FXD::tuple_element_t< IDX, Container::Tuple< Args... > >&& get(Container::Tuple< Args... >&& tuple) NOEXCEPT
	{
		using T = FXD::tuple_element_t< IDX, Container::Tuple< Args... > >&&;
		using tuple_type = typename FXD::tuple_element< IDX, Container::Tuple< Args... > >::tuple_type;

		return (std::forward< T >(((tuple_type&)tuple).m_first.m_val));
	}

	template < size_t IDX, typename... Args >
	CONSTEXPR14 const FXD::tuple_element_t< IDX, Container::Tuple< Args... > >&& get(const Container::Tuple< Args... >&& tuple) NOEXCEPT
	{
		using T = FXD::tuple_element_t< IDX, Container::Tuple< Args... > >;
		using tuple_type = typename FXD::tuple_element< IDX, Container::Tuple< Args... > >::tuple_type;

		return static_cast< const T&& >(static_cast< const tuple_type& >(tuple).m_first.m_val);
	}

	// FUNCTION TEMPLATE get (by type)
	template < typename T, typename... Args >
	CONSTEXPR14 T& get(Container::Tuple< Args... >& tuple) NOEXCEPT
	{
		using tuple_type = typename Internal::_tuple_element< T, Container::Tuple< Args... > >::tuple_type;
		return (((tuple_type&)tuple).m_first.m_val);
	}

	template < typename T, typename... Args >
	CONSTEXPR14 const T& get(const Container::Tuple< Args... >& tuple) NOEXCEPT
	{
		using tuple_type = typename Internal::_tuple_element< T, Container::Tuple< Args... > >::tuple_type;
		return (((const tuple_type&)tuple).m_first.m_val);
	}

	template < typename T, typename... Args >
	CONSTEXPR14 T&& get(Container::Tuple< Args... >&& tuple) NOEXCEPT
	{
		using tuple_type = typename Internal::_tuple_element< T, Container::Tuple< Args... > >::tuple_type;
		return (std::forward< T&& >(((tuple_type&)tuple).m_first.m_val));
	}

	template < typename T, typename... Args >
	CONSTEXPR14 const T&& get(const Container::Tuple< Args... >&& tuple) NOEXCEPT
	{
		using tuple_type = typename Internal::_tuple_element< T, Container::Tuple< Args... > >::tuple_type;
		return (std::forward< const T&& >(((tuple_type&)tuple).m_first.m_val));
	}

	template < typename... Args >
	CONSTEXPR14 Container::Tuple< typename FXD::STD::remove_reference_wrapper< typename FXD::STD::decay< Args >::type >::type... > make_tuple(Args&&... args)
	{
		using tuple_type = Container::Tuple< typename FXD::STD::remove_reference_wrapper< typename FXD::STD::decay< Args >::type >::type... >;
		return tuple_type(std::forward< Args >(args)...);
	}

	template < typename... Args >
	CONSTEXPR14 Container::Tuple< Args&... > tie(Args&... args) NOEXCEPT
	{
		using tuple_type = Container::Tuple< Args&... >;
		return tuple_type(args...);
	}

	template < typename... Args >
	CONSTEXPR14 Container::Tuple< Args&&... > forward_as_tuple(Args&&... args) NOEXCEPT
	{
		return Container::Tuple< Args&&... >(std::forward< Args >(args)...);
	}

	template < typename... Tuples >
	CONSTEXPR14 typename Internal::_tuple_cat_type< Tuples... >::type tuple_cat(Tuples&&... tuples)
	{
		using _Cat1 = Internal::_tuple_cat_type< Tuples... >;
		return Internal::_tuple_cat< typename _Cat1::type >(typename _Cat1::_Kx_arg_seq(), typename _Cat1::_Ix_arg_seq(), FXD::forward_as_tuple(std::forward< Tuples >(tuples)...));
	}

	#if IsCPP17()
	namespace Internal
	{
		template < typename C, typename Tup, size_t... INDICES >
		CONSTEXPR17 decltype(auto) _apply(C&& callable, Tup&& tuple, FXD::STD::index_sequence< INDICES... >)
		{
			return FXD::invoke(std::forward< C >(callable), FXD::get< INDICES >(std::forward< Tup >(tuple))...);
		}

		template < typename T, typename Tup, size_t... INDICES >
		CONSTEXPR17 T _make_from_tuple(Tup&& tuple, FXD::STD::index_sequence< INDICES... >)
		{
			return T(FXD::get< INDICES >(std::forward< Tup >(tuple))...);
		}
	} //namespace Internal

	template < typename C, typename Tup >
	CONSTEXPR17 decltype(auto) apply(C&& callable, Tup&& tuple)
	{
		return Internal::_apply(std::forward< C >(callable), std::forward< Tup >(tuple), FXD::STD::make_index_sequence< FXD::tuple_size< FXD::STD::remove_reference_t< Tup > >::value >{});
	}

	template < typename T, typename Tup >
	CONSTEXPR17 T make_from_tuple(Tup&& tuple)
	{
		return Internal::_make_from_tuple< T >(std::forward< Tup >(tuple), FXD::STD::make_index_sequence< FXD::tuple_size< FXD::STD::remove_reference_t< Tup > >::value >{});
	}
	#endif // IsCPP17()

	/*
	template < size_t _Idx, typename T, FXD::U32 SIZE >
	CONSTEXPR14 T& get(Container::Array< T, SIZE >& array) NOEXCEPT;

	template < size_t _Idx, typename T, FXD::U32 SIZE >
	CONSTEXPR14 const T& get(const Container::Array< T, SIZE >& array) NOEXCEPT;

	template < size_t _Idx, typename T, FXD::U32 SIZE >
	CONSTEXPR14 T&& get(Container::Array< T, SIZE >&& array) NOEXCEPT;

	template < size_t _Idx, typename T, FXD::U32 SIZE >
	CONSTEXPR14 const T&& get(const Container::Array< T, SIZE >&& array) NOEXCEPT;

	// TEMPLATE CONSTRUCTOR pair::pair(Tuple, Tuple, sequence, sequence)
	template < class _Ty1, class _Ty2 >
	template < class _Tuple1, class _Tuple2, size_t... _Indexes1, size_t... _Indexes2 >
	constexpr FXD::Core::Pair< _Ty1, _Ty2 >::pair(_Tuple1& _Val1, _Tuple2& _Val2, FXD::STD::index_sequence< _Indexes1... >, FXD::STD::index_sequence< _Indexes2... >)
		: first(FXD::get< _Indexes1 >(std::move(_Val1))...), second(FXD::get< _Indexes2 >(std::move(_Val2))...)
	{}

	// TEMPLATE CONSTRUCTOR pair::pair(piecewise_construct_t, Tuple, Tuple)
	template < class _Ty1, class _Ty2 >
	template < class... _Types1, class... _Types2 >
	_CONSTEXPR20 FXD::Core::Pair< _Ty1, _Ty2 >::pair(FXD::STD::piecewise_construct_t, Tuple< _Types1... > _Val1, Tuple< _Types2... > _Val2)
		: pair(_Val1, _Val2, index_sequence_for< _Types1... >{}, index_sequence_for< _Types2... >{})
	{}
	*/

	namespace STD
	{
		// STRUCT TEMPLATE uses_allocator
		template < typename... Args, typename Alloc >
		struct uses_allocator< Container::Tuple< Args... >, Alloc > : FXD::STD::true_type
		{}; // true_type if container allocator enabled

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_TUPLE_H