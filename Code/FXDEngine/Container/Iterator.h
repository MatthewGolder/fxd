// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_ITERATOR_H
#define FXDENGINE_CONTAINER_ITERATOR_H

#include "FXDEngine/Core/Types.h"
#include "FXDEngine/Container/Internal/Advance.h"
#include "FXDEngine/Container/Internal/BackInsertIterator.h"
#include "FXDEngine/Container/Internal/Begin.h"
#include "FXDEngine/Container/Internal/CBegin.h"
#include "FXDEngine/Container/Internal/CEnd.h"
#include "FXDEngine/Container/Internal/CRBegin.h"
#include "FXDEngine/Container/Internal/CREnd.h"
#include "FXDEngine/Container/Internal/Data.h"
#include "FXDEngine/Container/Internal/Distance.h"
#include "FXDEngine/Container/Internal/Empty.h"
#include "FXDEngine/Container/Internal/End.h"
#include "FXDEngine/Container/Internal/FrontInsertIterator.h"
#include "FXDEngine/Container/Internal/GenericIterator.h"
#include "FXDEngine/Container/Internal/InsertIterator.h"
#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Container/Internal/IteratorWrapper.h"
#include "FXDEngine/Container/Internal/MoveIterator.h"
#include "FXDEngine/Container/Internal/Next.h"
#include "FXDEngine/Container/Internal/Prev.h"
#include "FXDEngine/Container/Internal/RBegin.h"
#include "FXDEngine/Container/Internal/REnd.h"
#include "FXDEngine/Container/Internal/Reverse.h"
#include "FXDEngine/Container/Internal/Size.h"
#include "FXDEngine/Container/Internal/Tags.h"
#include "FXDEngine/Container/Internal/UnwrapIterator.h"
#include "FXDEngine/Core/Internal/Void_t.h"

// For loops
#if !defined(fxd_for)
	#define fxd_for(var, cont) for (var : cont)
#endif

#if !defined(fxd_for_reverse)
	#define fxd_for_reverse(var, cont) (var : FXD::Container::reverse(cont))
#endif

#if !defined(fxd_for_itr)
	#define fxd_for_itr(var, cont) for (auto var = (cont).begin(); var != (cont).end(); ++var)
#endif

#if !defined(fxd_for_itr_reverse)
	#define fxd_for_itr_reverse(var, cont) for (auto var = (cont).rbegin(); var != (cont).rend(); ++var)
#endif

namespace FXD
{
	namespace Internal
	{
		template < class T, class = void >
		struct _Is_iterator : FXD::STD::false_type
		{};

		template < class T >
		struct _Is_iterator< T, FXD::STD::void_t< typename FXD::STD::iterator_traits< T >::iterator_category > > : FXD::STD::true_type
		{};
	} //namespace Internal
} //namespace FXD

#endif //FXDENGINE_CONTAINER_ITERATOR_H