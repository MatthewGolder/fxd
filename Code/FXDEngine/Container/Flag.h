// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_FLAG_H
#define FXDENGINE_CONTAINER_FLAG_H

#include "FXDEngine/Core/Types.h"
#include "FXDEngine/Core/TypeTraits.h"

namespace FXD
{
	namespace Container
	{
		// ------
		// EnumFlag
		// -
		// ------
		template < typename Enum >
		class EnumFlag
		{
		protected:
			using u_type = typename FXD::STD::underlying_type< Enum >::type;

		public:
			EnumFlag(void)
				: m_nBits(0)
			{}
			EnumFlag(Enum t)
				: m_nBits(0)
			{
				set_flag(t);
			}

			// Test/set.
			inline void set_flag(Enum eType)									{ m_nBits |= ((u_type)1 << (u_type)eType); }
			inline void clear_flag(Enum eType)								{ m_nBits &= ~((u_type)1 << (u_type)eType); }
			inline bool is_flag_set(Enum eType) const						{ return (m_nBits & ((u_type)1 << (u_type)eType)) != 0; }
			inline FXD::U64 get_bits(void) const							{ return m_nBits; }
			inline bool are_all_flags_set(const EnumFlag& rhs) const	{ return (m_nBits & rhs.m_nBits) == rhs.m_nBits; }
			inline bool is_any_flag_set(void) const						{ return (m_nBits != 0); }
			inline bool is_any_flag_set(const EnumFlag& rhs) const	{ return (m_nBits & rhs.m_nBits) != 0; }

			// Array access.
			inline bool operator[](Enum eType) const						{ return (is_flag_set(eType)); }

			// Clear flags.
			inline void clearFlags(void)										{ m_nBits = 0; }

			// Logic operations.
			inline void operator|=(const EnumFlag& rhs)					{ m_nBits |= rhs.m_nBits; }

			inline EnumFlag operator|(const EnumFlag& rhs) const	{ EnumFlag ret; ret.m_nBits = (m_nBits | rhs.m_nBits); return ret; }
			inline EnumFlag operator&(const EnumFlag& rhs) const	{ EnumFlag ret; ret.m_nBits = (m_nBits & rhs.m_nBits); return ret; }
			inline EnumFlag operator^(const EnumFlag& rhs) const	{ EnumFlag ret; ret.m_nBits = (m_nBits ^ rhs.m_nBits); return ret; }
			inline EnumFlag operator-(const EnumFlag& rhs) const	{ EnumFlag ret; ret.m_nBits = (m_nBits & ~rhs.m_nBits); return ret; }

			inline bool operator==(const EnumFlag& rhs) const	{ return (m_nBits == rhs.m_nBits); }
			inline bool operator!=(const EnumFlag& rhs) const	{ return (m_nBits != rhs.m_nBits); }
			inline void operator+=(const EnumFlag& rhs)			{ m_nBits |= rhs.m_nBits; }
			inline void operator-=(const EnumFlag& rhs)			{ m_nBits &= ~rhs.m_nBits; }

		private:
			u_type m_nBits;
		};
	} //namespace Container
} //namespace FXD

#define ENUM_CLASS_FLAGS(Enum) \
	inline FXD::Container::EnumFlag< Enum > operator| (Enum lhs, Enum rhs){ return FXD::Container::EnumFlag< Enum >(lhs) | rhs; } \
	inline FXD::Container::EnumFlag< Enum > operator& (Enum lhs, Enum rhs){ return FXD::Container::EnumFlag< Enum >(lhs) & rhs; } \
	inline FXD::Container::EnumFlag< Enum > operator^ (Enum lhs, Enum rhs){ return FXD::Container::EnumFlag< Enum >(lhs) ^ rhs; }

#endif //FXDENGINE_CONTAINER_FLAG_H