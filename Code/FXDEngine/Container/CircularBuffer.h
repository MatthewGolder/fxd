// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_CIRCULARBUFFER_H
#define FXDENGINE_CONTAINER_CIRCULARBUFFER_H

#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Memory/Allocator/Aligned.h"
#include "FXDEngine/Memory/Memory.h"

namespace FXD
{
	namespace Container
	{
		namespace Internal
		{
			// ------
			// _circular_buffer_iterator
			// -
			// ------
			template < typename Container, typename T, typename Pointer, typename Reference >
			class _circular_buffer_iterator
			{
			public:
				using my_type = _circular_buffer_iterator< Container, T, Pointer, Reference >;
				using container = Container;
				using value_type = T;
				using reference = Reference;
				using pointer = Pointer;
				using size_type = FXD::U32;
				using difference_type = std::ptrdiff_t;

				using iterator_category = FXD::STD::random_access_iterator_tag;
				using iterator = _circular_buffer_iterator< Container, T, T*, T& >;

				_circular_buffer_iterator(container& parent, size_type nID)
					: parent(parent)
					, m_nID(nID)
				{}

				my_type& operator++()
				{
					++m_nID;
					return *this;
				}
				my_type operator++(int) // postincrement
				{
					self_type old(*this);
					operator++();
					return old;
				}
				my_type& operator--()
				{
					--m_nID;
					return (*this);
				}
				my_type operator--(int) // postdecrement
				{
					self_type old(*this);
					operator--();
					return old;
				}

				reference operator*()
				{
					return parent[m_nID];
				}
				pointer operator->()
				{
					return &(parent[m_nID]);
				}

				bool operator==(const my_type& rhs) const
				{
					return ((&parent == &rhs.parent) && (m_nID == rhs.m_nID));
				}
				bool operator!=(const my_type& rhs) const
				{
					return !(rhs == *this);
				}

			private:
				container& parent;
				size_type m_nID;
			};
		} //namespace Container

		// ------
		// CircularBuffer
		// -
		// ------
		template <
			typename T,
			typename Allocator = Memory::Alloc::AlignedType< Memory::Alloc::Heap, T > >
		class CircularBuffer
		{
		public:
			using my_type = CircularBuffer< T, Allocator >;
			using value_type = T;
			using allocator_type = Allocator;
			using pointer = T * ;
			using const_pointer = const T*;
			using reference = T & ;
			using const_reference = const T&;
			using size_type = FXD::U32;
			using difference_type = std::ptrdiff_t;
			using allocator_type = Allocator;
			using iterator = Internal::_circular_buffer_iterator< my_type, T, T*, T& >;

			EXPLICIT CircularBuffer(size_type nCapacity, const allocator_type& alloc = allocator_type());
			~CircularBuffer(void);

			// Iterator Support
			iterator begin(void);
			iterator end(void);

			// Access
			reference operator[](size_type nID);
			const_reference operator[](size_type nID) const;
			reference at(size_type nID);
			const_reference at(size_type nID) const;

			reference front(void);
			const_reference front(void) const;
			reference back(void);
			const_reference back(void) const;

			// Searches
			bool empty(void) const;
			bool not_empty(void) const;

			size_type size(void) const;
			size_type max_size(void) const;
			size_type capacity(void) const;
			allocator_type get_allocator(void) const;

			// Insert
			bool push_back(const value_type& val);
			void pop_front(void);

			// Erase
			void clear(void);

		private:
			NO_COPY(CircularBuffer);
			NO_ASSIGNMENT(CircularBuffer);

			value_type* _wrap(value_type* pPtr) const;

		private:
			size_type m_nCapacity;
			allocator_type m_alloc;
			pointer m_pBuffer;
			pointer m_pFront;
			pointer m_pBack; // points to next unused item
		};

		template < typename T, typename Allocator >
		inline CircularBuffer< T, Allocator >::CircularBuffer(size_type nCapacity, const allocator_type& alloc)
			: m_nCapacity(nCapacity)
			, m_alloc(alloc)
			, m_pBuffer(nullptr)
			, m_pFront(nullptr)
			, m_pBack(nullptr)
		{
			m_pBuffer = (value_type*)m_alloc.allocate(nCapacity * sizeof(value_type));
			m_pBack = m_pBuffer;
			PRINT_COND_ASSERT((nCapacity > 0), "Container: Capacity is invalid");
		}

		template < typename T, typename Allocator >
		inline CircularBuffer< T, Allocator >::~CircularBuffer(void)
		{
			clear(); // deallocates all objects
			m_alloc.deallocate(m_pBuffer, (m_nCapacity * sizeof(value_type)));
		}

		// Iterator Support
		template < typename T, typename Allocator >
		typename CircularBuffer< T, Allocator >::iterator CircularBuffer< T, Allocator >::begin(void)
		{
			return iterator(*this, 0);
		}

		template < typename T, typename Allocator >
		typename CircularBuffer< T, Allocator >::iterator CircularBuffer< T, Allocator >::end(void)
		{
			return iterator(*this, size());
		}

		// Access
		template < typename T, typename Allocator >
		inline typename CircularBuffer< T, Allocator >::reference CircularBuffer< T, Allocator >::operator[](size_type nID)
		{
			return *_wrap(m_pFront + nID);
		}

		template < typename T, typename Allocator >
		inline typename CircularBuffer< T, Allocator >::const_reference CircularBuffer< T, Allocator >::operator[](size_type nID) const
		{
			return *_wrap(m_pFront + nID);
		}

		template < typename T, typename Allocator >
		inline typename CircularBuffer< T, Allocator >::reference CircularBuffer< T, Allocator >::at(size_type nID)
		{
			if (nID >= size())
			{
				PRINT_ASSERT << "Container: Parameter out of range";
			}
			return (*this)[nID];
		}

		template < typename T, typename Allocator >
		inline typename CircularBuffer< T, Allocator >::const_reference CircularBuffer< T, Allocator >::at(size_type nID) const
		{
			if (nID >= size())
			{
				PRINT_ASSERT << "Container: Parameter out of range";
			}
			return (*this)[nID];
		}

		template < typename T, typename Allocator >
		inline typename CircularBuffer< T, Allocator >::reference CircularBuffer< T, Allocator >::front(void)
		{
			PRINT_COND_ASSERT((m_pFront != nullptr), "Container: Parameter out of range");
			return *m_pFront;
		}

		template < typename T, typename Allocator >
		inline typename CircularBuffer< T, Allocator >::const_reference CircularBuffer< T, Allocator >::front(void) const
		{
			PRINT_COND_ASSERT((m_pFront != nullptr), "Container: Front is invalid");
			return *m_pFront;
		}

		template < typename T, typename Allocator >
		inline typename CircularBuffer< T, Allocator >::reference CircularBuffer< T, Allocator >::back(void)
		{
			PRINT_COND_ASSERT((m_pFront != nullptr), "Container: Parameter out of range");
			return *_wrap(m_pBack - 1);
		}

		template < typename T, typename Allocator >
		inline typename CircularBuffer< T, Allocator >::const_reference CircularBuffer< T, Allocator >::back(void) const
		{
			PRINT_COND_ASSERT((m_pFront != nullptr), "Container: Parameter out of range");
			return *_wrap(m_pBack - 1);
		}

		// Searches
		template < typename T, typename Allocator >
		inline bool CircularBuffer< T, Allocator >::empty(void) const
		{
			return (m_pFront == nullptr);
		}

		template < typename T, typename Allocator >
		inline bool CircularBuffer< T, Allocator >::not_empty(void) const
		{
			return (m_pFront != nullptr);
		}

		template < typename T, typename Allocator >
		inline typename CircularBuffer< T, Allocator >::size_type CircularBuffer< T, Allocator >::size(void) const
		{
			return (m_pFront == nullptr) ? 0 : ((m_pBack > m_pFront) ? m_pBack : (m_pBack + m_nCapacity)) - m_pFront;
		}

		template < typename T, typename Allocator >
		inline typename CircularBuffer< T, Allocator >::size_type CircularBuffer< T, Allocator >::max_size(void) const
		{
			return m_nCapacity;
		}

		template < typename T, typename Allocator >
		inline typename CircularBuffer< T, Allocator >::size_type CircularBuffer< T, Allocator >::capacity(void) const
		{
			return m_nCapacity;
		}

		template < typename T, typename Allocator >
		inline typename CircularBuffer< T, Allocator >::allocator_type CircularBuffer< T, Allocator >::get_allocator(void) const
		{
			return m_alloc;
		}

		// Insert
		template < typename T, typename Allocator >
		inline bool CircularBuffer< T, Allocator >::push_back(const value_type& val)
		{
			if ((m_pFront != nullptr) && (m_pFront == m_pBack))
			{
				FXD::STD::destruct(m_pBack);
			}

			FXD_PLACEMENT_NEW(this->m_pBack, value_type)(val);
			//m_alloc.construct(m_pBack, val);

			value_type* const pNext = _wrap(m_pBack + 1);
			if (m_pFront == nullptr)
			{
				// first entry in the buffer
				m_pFront = m_pBack;
				m_pBack = pNext;
				return true;
			}
			else if (m_pFront == m_pBack)
			{
				// buffer is full already, throw something away
				m_pFront = (m_pBack = pNext);
				return false;
			}
			else
			{
				m_pBack = pNext;
				return true;
			}
		}

		template < typename T, typename Allocator >
		inline void CircularBuffer< T, Allocator >::pop_front(void)
		{
			PRINT_COND_ASSERT((m_pFront != nullptr), "Container: Parameter out of range");

			FXD::STD::destruct(m_pFront);
			value_type* const pNext = _wrap(m_pFront + 1);
			if (pNext == m_pBack)
			{
				m_pFront = nullptr;
			}
			else
			{
				m_pFront = pNext;
			}
		}

		// Erase
		template < typename T, typename Allocator >
		inline void CircularBuffer< T, Allocator >::clear(void)
		{
			if (m_pFront != nullptr)
			{
				do
				{
					FXD::STD::destruct(m_pFront);
					m_pFront = _wrap(m_pFront + 1);
				} while (m_pFront != m_pBack);
			}
			m_pFront = nullptr;
		}

		template < typename T, typename Allocator >
		inline typename CircularBuffer< T, Allocator >::value_type* CircularBuffer< T, Allocator >::_wrap(value_type* pPtr) const
		{
			PRINT_COND_ASSERT((pPtr < m_pBuffer + m_nCapacity * 2), "Container: Parameter out of range");
			PRINT_COND_ASSERT((pPtr > m_pBuffer - m_nCapacity), "Container: Parameter out of range");

			if (pPtr >= m_pBuffer + m_nCapacity)
			{
				return (pPtr - m_nCapacity);
			}
			else if (pPtr < m_pBuffer)
			{
				return (pPtr + m_nCapacity);
			}
			else
			{
				return pPtr;
			}
		}

		// Inserters and extractors
		template < typename T, typename Allocator >
		Core::IStreamOut& operator<<(Core::IStreamOut& ostr, const Container::CircularBuffer< T, Allocator >& cont)
		{
			fxd_for(auto& i, cont)
			{
				ostr << " " << i;
			}
			return ostr;
		}
	} //namespace Container
} //namespace FXD
#endif //FXDENGINE_CONTAINER_CIRCULARBUFFER_H