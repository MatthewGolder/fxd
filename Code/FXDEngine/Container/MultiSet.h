// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_MULTISET_H
#define FXDENGINE_CONTAINER_MULTISET_H

#include "FXDEngine/Container/RedBlackTree.h"
#include "FXDEngine/Memory/Allocator/Aligned.h"

namespace FXD
{
	namespace Container
	{
		// ------
		// Multiset
		// -
		// ------
		template <
			typename Key,
			typename Compare = FXD::STD::less< Key >,
			typename Allocator = Memory::Alloc::AlignedType< Memory::Alloc::Heap, Key > >
		class Multiset : public Container::RBTree< Key, Key, Compare, Allocator, FXD::STD::UseSelf< Key >, false, false >
		{
		public:
			using my_type = Multiset< Key, Compare, Allocator >;
			using base_type = Container::RBTree< Key, Key, Compare, Allocator, FXD::STD::UseSelf< Key >, false, false >;

			using key_type = typename base_type::key_type;
			using value_type = typename base_type::value_type;
			using size_type = typename base_type::size_type;
			using iterator = typename base_type::iterator;
			using const_iterator = typename base_type::const_iterator;
			using reverse_iterator = typename base_type::reverse_iterator;
			using const_reverse_iterator = typename base_type::const_reverse_iterator;
			using allocator_type = typename base_type::allocator_type;
			using value_compare = Compare;

		public:
			Multiset(void);
			Multiset(const allocator_type& allocator);
			Multiset(const value_compare& compare, const allocator_type& allocator = allocator_type());

			template < typename InputItr, class = typename FXD::STD::enable_if< FXD::Internal::_Is_iterator< InputItr >::value, void >::type >
			Multiset(InputItr first, InputItr last, const allocator_type& allocator);
			template < typename InputItr, class = typename FXD::STD::enable_if< FXD::Internal::_Is_iterator< InputItr >::value, void >::type >
			Multiset(InputItr first, InputItr last, const value_compare& compare = value_compare(), const allocator_type& allocator = allocator_type());

			Multiset(std::initializer_list<value_type> iList, const allocator_type& allocator);
			Multiset(std::initializer_list<value_type> iList, const value_compare& compare = value_compare(), const allocator_type& allocator = allocator_type());

			~Multiset(void);

			Multiset(const my_type& rhs);
			Multiset(my_type&& rhs);

			my_type& operator=(const my_type& rhs);
			my_type& operator=(my_type&& rhs);
			my_type& operator=(std::initializer_list< value_type > iList);

			value_compare value_comp(void) const;

			size_type count(const key_type& key) const;

			Core::Pair< iterator, iterator > equal_range_small(const key_type& key);
			Core::Pair< const_iterator, const_iterator > equal_range_small(const key_type& key) const;
		};


		template < typename Key, typename Compare, typename Allocator >
		inline Multiset< Key, Compare, Allocator >::Multiset(void)
		{}

		template < typename Key, typename Compare, typename Allocator >
		inline Multiset< Key, Compare, Allocator >::Multiset(const allocator_type& allocator)
			: base_type(allocator)
		{}

		template < typename Key, typename Compare, typename Allocator >
		inline Multiset< Key, Compare, Allocator >::Multiset(const value_compare& compare, const allocator_type& allocator)
			: base_type(compare, allocator)
		{}

		template < typename Key, typename Compare, typename Allocator >
		template < typename InputItr, class >
		inline Multiset< Key, Compare, Allocator >::Multiset(InputItr first, InputItr last, const allocator_type& allocator)
			: base_type(first, last, value_compare(), allocator)
		{}

		template < typename Key, typename Compare, typename Allocator >
		template < typename InputItr, class >
		inline Multiset< Key, Compare, Allocator >::Multiset(InputItr first, InputItr last, const value_compare& compare, const allocator_type& allocator)
			: base_type(first, last, compare, allocator)
		{}

		template < typename Key, typename Compare, typename Allocator >
		inline Multiset< Key, Compare, Allocator >::Multiset(std::initializer_list< value_type > iList, const allocator_type& allocator)
			: base_type(iList.begin(), iList.end(), value_compare(), allocator)
		{}

		template < typename Key, typename Compare, typename Allocator >
		inline Multiset< Key, Compare, Allocator >::Multiset(std::initializer_list< value_type > iList, const value_compare& compare, const allocator_type& allocator)
			: base_type(iList.begin(), iList.end(), compare, allocator)
		{}

		template < typename Key, typename Compare, typename Allocator >
		inline Multiset< Key, Compare, Allocator >::~Multiset(void)
		{}

		template < typename Key, typename Compare, typename Allocator >
		inline Multiset< Key, Compare, Allocator >::Multiset(const my_type& rhs)
			: base_type(rhs)
		{}

		template < typename Key, typename Compare, typename Allocator >
		inline Multiset< Key, Compare, Allocator >::Multiset(my_type&& rhs)
			: base_type(std::move(rhs))
		{}

		template < typename Key, typename Compare, typename Allocator >
		inline typename Multiset< Key, Compare, Allocator >::my_type& Multiset< Key, Compare, Allocator >::operator=(const my_type& rhs)
		{
			return (my_type&)base_type::operator=(rhs);
		}
		template < typename Key, typename Compare, typename Allocator >
		inline typename Multiset< Key, Compare, Allocator >::my_type& Multiset< Key, Compare, Allocator >::operator=(my_type&& rhs)
		{
			return (my_type&)base_type::operator=(std::move(rhs));
		}
		template < typename Key, typename Compare, typename Allocator >
		inline typename Multiset< Key, Compare, Allocator >::my_type& Multiset< Key, Compare, Allocator >::operator=(std::initializer_list< value_type > iList)
		{
			return (my_type&)base_type::operator=(iList);
		}

		template < typename Key, typename Compare, typename Allocator >
		inline typename Multiset< Key, Compare, Allocator >::value_compare Multiset< Key, Compare, Allocator >::value_comp(void) const
		{
			return this->m_compare;
		}

		template < typename Key, typename Compare, typename Allocator >
		inline typename Multiset< Key, Compare, Allocator >::size_type Multiset< Key, Compare, Allocator >::count(const key_type& key) const
		{
			const Core::Pair< const_iterator, const_iterator > range(this->equal_range(key));
			return (size_type)FXD::STD::distance(range.first, range.second);
		}

		template < typename Key, typename Compare, typename Allocator >
		inline Core::Pair< typename Multiset< Key, Compare, Allocator >::iterator, typename Multiset< Key, Compare, Allocator >::iterator > Multiset< Key, Compare, Allocator >::equal_range_small(const key_type& key)
		{
			const iterator itLower(this->lower_bound(key));
			iterator itUpper(itLower);
			while ((itUpper != this->end()) && (!this->m_compare(key, itUpper.m_pNode->m_val)))
			{
				++itUpper;
			}
			return Core::Pair< iterator, iterator >(itLower, itUpper);
		}

		template < typename Key, typename Compare, typename Allocator >
		inline Core::Pair< typename Multiset< Key, Compare, Allocator >::const_iterator, typename Multiset< Key, Compare, Allocator >::const_iterator > Multiset< Key, Compare, Allocator >::equal_range_small(const key_type& key) const
		{
			const const_iterator itLower(this->lower_bound(key));
			const_iterator itUpper(itLower);
			while ((itUpper != this->end()) && (!this->m_compare(key, *itUpper)))
			{
				++itUpper;
			}
			return Core::Pair< const_iterator, const_iterator >(itLower, itUpper);
		}
	} //namespace Container

	namespace STD
	{
		template < typename Key, typename Compare, typename Allocator, typename Pred >
		void erase_if(FXD::Container::Multiset< Key, Compare, Allocator >& container, Pred pred)
		{
			for (auto i = &container.begin(), last = &container.end(); i != last;)
			{
				if (pred(*i))
				{
					i = container.erase(i);
				}
				else
				{
					++i;
				}
			}
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_MULTISET_H