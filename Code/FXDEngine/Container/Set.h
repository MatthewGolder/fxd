// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_SET_H
#define FXDENGINE_CONTAINER_SET_H

#include "FXDEngine/Container/RedBlackTree.h"
#include "FXDEngine/Memory/Allocator/Aligned.h"

namespace FXD
{
	namespace Container
	{
		// ------
		// Set
		// -
		// ------
		template <
			typename Key,
			typename Compare = FXD::STD::less< Key >,
			typename Allocator = Memory::Alloc::AlignedType< Memory::Alloc::Heap, Key > >
		class Set : public Container::RBTree< Key, Key, Compare, Allocator, FXD::STD::UseSelf< Key >, false, true >
		{
		public:
			using my_type = Set< Key, Compare, Allocator >;
			using base_type = Container::RBTree< Key, Key, Compare, Allocator, FXD::STD::UseSelf< Key >, false, true >;

			using key_type = typename base_type::key_type;
			using value_type = typename base_type::value_type;
			using size_type = typename base_type::size_type;
			using iterator = typename base_type::iterator;
			using const_iterator = typename base_type::const_iterator;
			using reverse_iterator = typename base_type::reverse_iterator;
			using const_reverse_iterator = typename base_type::const_reverse_iterator;
			using allocator_type = typename base_type::allocator_type;
			using value_compare = Compare;

		public:
			Set(void);
			Set(const allocator_type& allocator);
			Set(const value_compare& compare, const allocator_type& allocator = allocator_type());

			template < typename InputItr, class = typename FXD::STD::enable_if< FXD::Internal::_Is_iterator< InputItr >::value, void >::type >
			Set(InputItr first, InputItr last, const allocator_type& allocator);
			template < typename InputItr, class = typename FXD::STD::enable_if< FXD::Internal::_Is_iterator< InputItr >::value, void >::type >
			Set(InputItr first, InputItr last, const value_compare& compare = value_compare(), const allocator_type& allocator = allocator_type());

			Set(std::initializer_list< value_type > iList, const allocator_type& allocator);
			Set(std::initializer_list< value_type > iList, const value_compare& compare = value_compare(), const allocator_type& allocator = allocator_type());

			~Set(void);

			Set(const my_type& rhs);
			Set(my_type&& rhs);

			my_type& operator=(const my_type& rhs);
			my_type& operator=(my_type&& rhs);
			my_type& operator=(std::initializer_list< value_type > iList);

			value_compare value_comp(void) const;

			size_type count(const key_type& key) const;
		};


		template < typename Key, typename Compare, typename Allocator >
		inline Set< Key, Compare, Allocator >::Set(void)
		{}

		template < typename Key, typename Compare, typename Allocator >
		inline Set< Key, Compare, Allocator >::Set(const allocator_type& allocator)
			: base_type(allocator)
		{}

		template < typename Key, typename Compare, typename Allocator >
		inline Set< Key, Compare, Allocator >::Set(const value_compare& compare, const allocator_type& allocator)
			: base_type(compare, allocator)
		{}

		template < typename Key, typename Compare, typename Allocator >
		template < typename InputItr, class >
		inline Set< Key, Compare, Allocator >::Set(InputItr first, InputItr last, const allocator_type& allocator)
			: base_type(first, last, value_compare(), allocator)
		{}

		template < typename Key, typename Compare, typename Allocator >
		template < typename InputItr, class >
		inline Set< Key, Compare, Allocator >::Set(InputItr first, InputItr last, const value_compare& compare, const allocator_type& allocator)
			: base_type(first, last, compare, allocator)
		{}

		template < typename Key, typename Compare, typename Allocator >
		inline Set< Key, Compare, Allocator >::Set(std::initializer_list< value_type > iList, const allocator_type& allocator)
			: base_type(iList.begin(), iList.end(), value_compare(), allocator)
		{}

		template < typename Key, typename Compare, typename Allocator >
		inline Set< Key, Compare, Allocator >::Set(std::initializer_list< value_type > iList, const value_compare& compare, const allocator_type& allocator)
			: base_type(iList.begin(), iList.end(), compare, allocator)
		{}

		template < typename Key, typename Compare, typename Allocator >
		inline Set< Key, Compare, Allocator >::~Set(void)
		{}

		template < typename Key, typename Compare, typename Allocator >
		inline Set< Key, Compare, Allocator >::Set(const my_type& rhs)
			: base_type(rhs)
		{}

		template < typename Key, typename Compare, typename Allocator >
		inline Set< Key, Compare, Allocator >::Set(my_type&& rhs)
			: base_type(std::move(rhs))
		{}

		template < typename Key, typename Compare, typename Allocator >
		inline typename Set< Key, Compare, Allocator >::my_type& Set< Key, Compare, Allocator >::operator=(const my_type& rhs)
		{
			return (my_type&)base_type::operator=(rhs);
		}
		template < typename Key, typename Compare, typename Allocator >
		inline typename Set< Key, Compare, Allocator >::my_type& Set< Key, Compare, Allocator >::operator=(my_type&& rhs)
		{
			return (my_type&)base_type::operator=(std::move(rhs));
		}
		template < typename Key, typename Compare, typename Allocator >
		inline typename Set< Key, Compare, Allocator >::my_type& Set< Key, Compare, Allocator >::operator=(std::initializer_list< value_type > iList)
		{
			return (my_type&)base_type::operator=(iList);
		}

		template < typename Key, typename Compare, typename Allocator >
		inline typename Set< Key, Compare, Allocator >::value_compare Set< Key, Compare, Allocator >::value_comp(void) const
		{
			return this->m_compare;
		}

		template < typename Key, typename Compare, typename Allocator >
		inline typename Set< Key, Compare, Allocator >::size_type Set< Key, Compare, Allocator >::count(const key_type& key) const
		{
			const const_iterator itr(this->find(key));
			return (itr != this->end()) ? (size_type)1 : (size_type)0;
		}
	} //namespace Container

	namespace STD
	{
		template < typename Key, typename Compare, typename Allocator, typename Pred >
		void erase_if(FXD::Container::Set< Key, Compare, Allocator >& container, Pred pred)
		{
			for (auto i = &container.begin(), last = &container.end(); i != last;)
			{
				if (pred(*i))
				{
					i = container.erase(i);
				}
				else
				{
					++i;
				}
			}
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_SET_H