// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_MAP_H
#define FXDENGINE_CONTAINER_MAP_H

#include "FXDEngine/Container/RedBlackTree.h"
#include "FXDEngine/Memory/Allocator/Aligned.h"

namespace FXD
{
	namespace Container
	{
		// ------
		// Map
		// -
		// ------
		template <
			typename Key,
			typename T,
			typename Compare = FXD::STD::less< Key >,
			typename Allocator = Memory::Alloc::AlignedType< Memory::Alloc::Heap, Core::Pair< Key, T > > >
		class Map : public Container::RBTree< Key, Core::Pair< const Key, T >, Compare, Allocator, FXD::STD::UseFirst< Core::Pair< const Key, T > >, true, true >
		{
		public:
			using my_type = Map< Key, T, Compare, Allocator >;
			using base_type = Container::RBTree< Key, Core::Pair< const Key, T >, Compare, Allocator, FXD::STD::UseFirst< Core::Pair< const Key, T> >, true, true >;

			using key_type = typename base_type::key_type;
			using value_type = typename base_type::value_type;
			using size_type = typename base_type::size_type;
			using mapped_type = T;
			using node_type = typename base_type::node_type;
			using iterator = typename base_type::iterator;
			using const_iterator = typename base_type::const_iterator;
			using allocator_type = typename base_type::allocator_type;

		public:
			class value_compare
			{
			protected:
				friend class Map;

			public:
				typedef bool result_type;
				typedef value_type first_argument_type;
				typedef value_type second_argument_type;

			protected:
				value_compare(Compare c)
					: m_compare(c)
				{}

			public:
				bool operator()(const value_type& lhs, const value_type& rhs) const
				{
					return m_compare(lhs.first, rhs.first);
				}

			protected:
				Compare m_compare;
			};

		public:
			Map(void);
			EXPLICIT Map(const allocator_type& allocator);
			EXPLICIT Map(const Compare& compare, const allocator_type& allocator = allocator_type());

			template < typename InputItr, class = typename FXD::STD::enable_if< FXD::Internal::_Is_iterator< InputItr >::value, void >::type >
			Map(InputItr first, InputItr last, const allocator_type& allocator);
			template < typename InputItr, class = typename FXD::STD::enable_if< FXD::Internal::_Is_iterator< InputItr >::value, void >::type >
			Map(InputItr first, InputItr last, const Compare& compare = Compare(), const allocator_type& allocator = allocator_type());

			Map(std::initializer_list< value_type > iList, const allocator_type& allocator);
			Map(std::initializer_list< value_type > iList, const Compare& compare = Compare(), const allocator_type& allocator = allocator_type());

			~Map(void);

			Map(const my_type& rhs);
			Map(my_type&& rhs);

			my_type& operator=(const my_type& rhs);
			my_type& operator=(my_type&& rhs);
			my_type& operator=(std::initializer_list< value_type > iList);

			value_compare value_comp(void) const;

			size_type count(const key_type& key) const;

			T& operator[](const key_type& key);
			T& operator[](key_type&& key);

			T& at(const key_type& key);
			const T& at(const key_type& key) const;

			bool try_get_value(const key_type& key, T& out) const
			{
				auto itr = this->find(key);
				if (itr != this->end())
				{
					out = itr->second;
					return true;
				}
				return false;
			}
		};

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline Map< Key, T, Compare, Allocator >::Map(void)
		{}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline Map< Key, T, Compare, Allocator >::Map(const allocator_type& allocator)
			: base_type(allocator)
		{}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline Map< Key, T, Compare, Allocator >::Map(const Compare& compare, const allocator_type& allocator)
			: base_type(compare, allocator)
		{}

		template < typename Key, typename T, typename Compare, typename Allocator >
		template < typename InputItr, class >
		inline Map< Key, T, Compare, Allocator >::Map(InputItr first, InputItr last, const allocator_type& allocator)
			: base_type(first, last, Compare(), allocator)
		{}

		template < typename Key, typename T, typename Compare, typename Allocator >
		template < typename InputItr, class >
		inline Map< Key, T, Compare, Allocator >::Map(InputItr first, InputItr last, const Compare& compare, const allocator_type& allocator)
			: base_type(first, last, compare, allocator)
		{}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline Map< Key, T, Compare, Allocator >::Map(std::initializer_list< value_type > iList, const allocator_type& allocator)
			: base_type(iList.begin(), iList.end(), Compare(), allocator)
		{}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline Map< Key, T, Compare, Allocator >::Map(std::initializer_list< value_type > iList, const Compare& compare, const allocator_type& allocator)
			: base_type(iList.begin(), iList.end(), compare, allocator)
		{}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline Map< Key, T, Compare, Allocator >::~Map(void)
		{}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline Map< Key, T, Compare, Allocator >::Map(const my_type& rhs)
			: base_type(rhs)
		{}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline Map< Key, T, Compare, Allocator >::Map(my_type&& rhs)
			: base_type(std::move(rhs))
		{}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline typename Map< Key, T, Compare, Allocator >::my_type& Map< Key, T, Compare, Allocator >::operator=(const my_type& rhs)
		{
			return (my_type&)base_type::operator=(rhs);
		}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline typename Map< Key, T, Compare, Allocator >::my_type& Map< Key, T, Compare, Allocator >::operator=(my_type&& rhs)
		{
			return (my_type&)base_type::operator=(std::move(rhs));
		}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline typename Map< Key, T, Compare, Allocator >::my_type& Map< Key, T, Compare, Allocator >::operator=(std::initializer_list< value_type > iList)
		{
			return (my_type&)base_type::operator=(iList);
		}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline typename Map< Key, T, Compare, Allocator >::size_type Map< Key, T, Compare, Allocator >::count(const key_type& key) const
		{
			const const_iterator itr(this->find(key));
			return (itr != this->end()) ? 1 : 0;
		}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline typename Map< Key, T, Compare, Allocator >::value_compare Map<Key, T, Compare, Allocator>::value_comp(void) const
		{
			return value_compare(this->m_compare);
		}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline T& Map< Key, T, Compare, Allocator >::operator[](const key_type& key)
		{
			iterator itLower(this->lower_bound(key));
			if ((itLower == this->end()) || (this->m_compare(key, (*itLower).first)))
			{
				itLower = base_type::_insert_key(FXD::STD::true_type(), itLower, key);
			}
			return (*itLower).second;
		}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline T& Map< Key, T, Compare, Allocator >::operator[](key_type&& key)
		{
			iterator itLower(this->lower_bound(key));
			if ((itLower == this->end()) || (this->m_compare(key, (*itLower).first)))
			{
				itLower = base_type::_insert_key(FXD::STD::true_type(), itLower, std::move(key));
			}
			return (*itLower).second;
		}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline T& Map< Key, T, Compare, Allocator >::at(const key_type& key)
		{
			iterator itLower(this->lower_bound(key));
			if (itLower == this->end())
			{
				PRINT_ASSERT << "Container: Key does not exist in map";
			}
			return (*itLower).second;
		}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline const T& Map< Key, T, Compare, Allocator >::at(const key_type& key) const
		{
			const_iterator itLower(this->lower_bound(key));
			if (itLower == this->end())
			{
				PRINT_ASSERT << "Container: Key does not exist in map";
			}
			return (*itLower).second;
		}
	} //namespace Container

	namespace STD
	{
		template < typename Key, typename T, typename Compare, typename Allocator, typename Pred >
		void erase_if(FXD::Container::Map< Key, T, Compare, Allocator >& container, Pred pred)
		{
			for (auto itr = container.begin(), last = container.end(); container != last;)
			{
				if (pred(*itr))
				{
					itr = container.erase(itr);
				}
				else
				{
					++itr;
				}
			}
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_MAP_H