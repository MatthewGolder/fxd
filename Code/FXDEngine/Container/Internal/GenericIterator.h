// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_GENERICITERATOR_H
#define FXDENGINE_CONTAINER_INTERNAL_GENERICITERATOR_H

#include "FXDEngine/Container/Internal/IteratorWrapper.h"

namespace FXD
{
	namespace STD
	{
		// generic_iterator
#		define FXD_SUPPORTS_GENERIC_ITERATOR 1

		template < typename Iterator, typename Container = void >
		class generic_iterator
		{
		public:
			using iterator_type = Iterator;
			using wrapped_iterator_type = iterator_type;
			using iterator_category = typename FXD::STD::iterator_traits< Iterator >::iterator_category;
			using traits_type = iterator_traits< Iterator >;
			using value_type = typename traits_type::value_type;
			using difference_type = typename traits_type::difference_type;
			using reference = typename traits_type::reference;
			using pointer = typename traits_type::pointer;
			using container_type = Container;
			using my_type = FXD::STD::generic_iterator< Iterator, Container >;

			generic_iterator(void)
				: m_itr(iterator_type())
			{}

			EXPLICIT generic_iterator(const iterator_type& rhs)
				: m_itr(rhs)
			{}

			my_type& operator=(const iterator_type& rhs)
			{
				m_itr = rhs;
				return (*this);
			}

			template < typename Iterator2 >
			generic_iterator(const FXD::STD::generic_iterator< Iterator2, Container >& rhs)
				: m_itr(rhs.base())
			{}

			reference operator*(void) const
			{
				return *m_itr;
			}

			pointer operator->(void) const
			{
				return m_itr;
			}

			my_type& operator++(void)
			{
				++m_itr;
				return (*this);
			}

			my_type operator++(FXD::S32)
			{
				return my_type(m_itr++);
			}

			my_type& operator--()
			{
				--m_itr;
				return (*this);
			}

			my_type operator--(FXD::S32)
			{
				return my_type(m_itr--);
			}

			reference operator[](const difference_type& nOff) const
			{
				return m_itr[nOff];
			}

			my_type& operator+=(const difference_type& nOff)
			{
				m_itr += nOff;
				return (*this);
			}

			my_type operator+(const difference_type& nOff) const
			{
				return my_type(m_itr + nOff);
			}

			my_type& operator-=(const difference_type& nOff)
			{
				m_itr -= nOff;
				return (*this);
			}

			my_type operator-(const difference_type& nOff) const
			{
				return my_type(m_itr - nOff);
			}

			const iterator_type& base(void) const
			{
				return m_itr;
			}

		protected:
			Iterator m_itr;
		};

		template < typename IteratorL, typename IteratorRHS, typename Container >
		inline bool operator==(const FXD::STD::generic_iterator< IteratorL, Container >& lhs, const FXD::STD::generic_iterator< IteratorRHS, Container >& rhs)
		{
			return lhs.base() == rhs.base();
		}

		template < typename Iterator, typename Container >
		inline bool operator==(const FXD::STD::generic_iterator< Iterator, Container >& lhs, const FXD::STD::generic_iterator< Iterator, Container >& rhs)
		{
			return lhs.base() == rhs.base();
		}

		template < typename IteratorL, typename IteratorRHS, typename Container >
		inline bool operator!=(const FXD::STD::generic_iterator< IteratorL, Container >& lhs, const FXD::STD::generic_iterator< IteratorRHS, Container >& rhs)
		{
			return lhs.base() != rhs.base();
		}

		template < typename Iterator, typename Container >
		inline bool operator!=(const FXD::STD::generic_iterator< Iterator, Container >& lhs, const FXD::STD::generic_iterator< Iterator, Container >& rhs)
		{
			return lhs.base() != rhs.base();
		}

		template < typename IteratorL, typename IteratorRHS, typename Container >
		inline bool operator<(const FXD::STD::generic_iterator< IteratorL, Container >& lhs, const FXD::STD::generic_iterator< IteratorRHS, Container >& rhs)
		{
			return lhs.base() < rhs.base();
		}

		template < typename Iterator, typename Container >
		inline bool operator<(const FXD::STD::generic_iterator< Iterator, Container >& lhs, const FXD::STD::generic_iterator< Iterator, Container >& rhs)
		{
			return lhs.base() < rhs.base();
		}

		template < typename IteratorL, typename IteratorRHS, typename Container >
		inline bool operator>(const FXD::STD::generic_iterator< IteratorL, Container >& lhs, const FXD::STD::generic_iterator< IteratorRHS, Container >& rhs)
		{
			return lhs.base() > rhs.base();
		}

		template < typename Iterator, typename Container >
		inline bool operator>(const FXD::STD::generic_iterator< Iterator, Container >& lhs, const FXD::STD::generic_iterator< Iterator, Container >& rhs)
		{
			return lhs.base() > rhs.base();
		}

		template < typename IteratorL, typename IteratorRHS, typename Container >
		inline bool operator<=(const FXD::STD::generic_iterator< IteratorL, Container >& lhs, const FXD::STD::generic_iterator< IteratorRHS, Container >& rhs)
		{
			return lhs.base() <= rhs.base();
		}

		template < typename Iterator, typename Container >
		inline bool operator<=(const FXD::STD::generic_iterator< Iterator, Container >& lhs, const FXD::STD::generic_iterator< Iterator, Container >& rhs)
		{
			return lhs.base() <= rhs.base();
		}

		template < typename IteratorL, typename IteratorRHS, typename Container >
		inline bool operator>=(const FXD::STD::generic_iterator< IteratorL, Container >& lhs, const FXD::STD::generic_iterator< IteratorRHS, Container >& rhs)
		{
			return lhs.base() >= rhs.base();
		}

		template < typename Iterator, typename Container >
		inline bool operator>=(const FXD::STD::generic_iterator< Iterator, Container >& lhs, const FXD::STD::generic_iterator< Iterator, Container >& rhs)
		{
			return lhs.base() >= rhs.base();
		}

		template < typename IteratorL, typename IteratorRHS, typename Container >
		inline typename FXD::STD::generic_iterator< IteratorL, Container >::difference_type operator-(const FXD::STD::generic_iterator< IteratorL, Container >& lhs, const FXD::STD::generic_iterator< IteratorRHS, Container >& rhs)
		{
			return lhs.base() - rhs.base();
		}

		template < typename Iterator, typename Container >
		inline FXD::STD::generic_iterator< Iterator, Container > operator+(typename FXD::STD::generic_iterator< Iterator, Container >::difference_type n, const FXD::STD::generic_iterator< Iterator, Container >& x)
		{
			return FXD::STD::generic_iterator< Iterator, Container >(x.base() + n);
		}

		template < typename Iterator >
		struct is_generic_iterator : public FXD::STD::false_type
		{};

		template < typename Iterator, typename Container >
		struct is_generic_iterator< FXD::STD::generic_iterator< Iterator, Container > > : public FXD::STD::true_type
		{};

		template < typename Iterator >
		inline typename FXD::STD::Internal::_is_iterator_wrapper< Iterator, FXD::STD::is_generic_iterator< Iterator >::value >::iterator_type unwrap_generic_iterator(Iterator itr)
		{
			return FXD::STD::Internal::_is_iterator_wrapper< Iterator, FXD::STD::is_generic_iterator< Iterator >::value >::get_base(itr);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_GENERICITERATOR_H