// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_INSERTITERATOR_H
#define FXDENGINE_CONTAINER_INTERNAL_INSERTITERATOR_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Core/TypeTraits.h"

namespace FXD
{
	namespace STD
	{
		// insert_iterator
#		define FXD_SUPPORTS_INSERT_ITERATOR 1

		template < typename Container >
		class insert_iterator : public FXD::STD::iterator< FXD::STD::output_iterator_tag, void, void, void, void >
		{
		public:
			using container_type = Container;
			using iterator_type = typename Container::iterator;
			using const_reference = typename Container::const_reference;

		public:
			insert_iterator& operator=(const FXD::STD::insert_iterator< container_type >& rhs)
			{
				PRINT_COND_ASSERT((&rhs.container == &container), "Container: Containers cannot be the same");
				itr = rhs.itr;
				return (*this);
			}

			insert_iterator(container_type& containerNew, iterator_type itrNew)
				: container(containerNew)
				, itr(itrNew)
			{}

			insert_iterator& operator=(const_reference val)
			{
				itr = container.insert(itr, val);
				++itr;
				return (*this);
			}

			insert_iterator& operator*()
			{
				return (*this);
			}

			insert_iterator& operator++()
			{
				return (*this);
			}

			insert_iterator& operator++(int)
			{
				return (*this);
			}

		protected:
			Container& container;
			iterator_type itr;
		};

		template < typename Container, typename Iterator >
		inline FXD::STD::insert_iterator< Container > inserter(Container& container, Iterator itr)
		{
			using iterator = typename Container::iterator;
			return FXD::STD::insert_iterator< Container >(container, iterator(itr));
		}

		template < typename T >
		struct is_insert_iterator : public FXD::STD::false_type
		{};

		template < typename Iterator >
		struct is_insert_iterator< FXD::STD::insert_iterator< Iterator > > : public FXD::STD::true_type
		{};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_INSERTITERATOR_H