// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_DATA_H
#define FXDENGINE_CONTAINER_INTERNAL_DATA_H

#include "FXDEngine/Core/Internal/Config.h"
#include <initializer_list>

namespace FXD
{
	namespace STD
	{
		// data
#		define FXD_SUPPORTS_DATA 1

		template < typename Container >
		CONSTEXPR14 auto data(Container& cont)->decltype(cont.data())
		{
			return cont.data();
		}

		template < typename Container >
		CONSTEXPR14 auto data(const Container& cont)->decltype(cont.data())
		{
			return cont.data();
		}

		template < typename T, size_t SIZE >
		CONSTEXPR14 T* data(T(&arr)[SIZE]) NOEXCEPT
		{
			return (arr);
		}

		template < typename T >
		CONSTEXPR14 const T* data(std::initializer_list< T > iList) NOEXCEPT
		{
			return iList.begin();
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_DATA_H