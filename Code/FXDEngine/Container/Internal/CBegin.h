// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_CBEGIN_H
#define FXDENGINE_CONTAINER_INTERNAL_CBEGIN_H

#include "FXDEngine/Container/Internal/Begin.h"

namespace FXD
{
	namespace STD
	{
		// cbegin
#		define FXD_SUPPORTS_CBEGIN 1

		template < typename Container >
		CONSTEXPR14 auto cbegin(const Container& cont)NOEXCEPT_IF(NOEXCEPT_IF(FXD::STD::begin(cont)))->decltype(FXD::STD::begin(cont))
		{
			return (FXD::STD::begin(cont));
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_CBEGIN_H