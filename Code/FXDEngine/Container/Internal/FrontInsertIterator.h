// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_FRONTINSERTITERATOR_H
#define FXDENGINE_CONTAINER_INTERNAL_FRONTINSERTITERATOR_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// front_insert_iterator
#		define FXD_SUPPORTS_FRONT_INSERT_ITERATOR 1

		template < typename Container >
		class front_insert_iterator : public FXD::STD::iterator< FXD::STD::output_iterator_tag, void, void, void, void >
		{
		public:
			using my_type = FXD::STD::front_insert_iterator< Container >;
			using container_type = Container;
			using const_reference = typename Container::const_reference;

		public:
			EXPLICIT front_insert_iterator(Container& rhs)
				: container(&rhs)
			{}

			front_insert_iterator& operator=(const_reference val)
			{
				container.push_front(val);
				return (*this);
			}

			front_insert_iterator& operator*()
			{
				return (*this);
			}

			front_insert_iterator& operator++()
			{
				return (*this);
			}

			front_insert_iterator operator++(int)
			{
				return (*this);
			}

		protected:
			void operator=(const my_type& /*rhs*/)
			{}

		protected:
			Container& container;

		};

		template < typename Container >
		inline FXD::STD::front_insert_iterator< Container > front_inserter(Container& rhs)
		{
			return FXD::STD::front_insert_iterator< Container >(rhs);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_FRONTINSERTITERATOR_H