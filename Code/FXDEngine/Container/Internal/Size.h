// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_SIZE_H
#define FXDENGINE_CONTAINER_INTERNAL_SIZE_H

#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// size
#		define FXD_SUPPORTS_SIZE 1

		template < typename Container >
		CONSTEXPR17 auto size(const Container& cont)->decltype(cont.size())
		{
			return cont.size();
		}

		template < typename T, size_t SIZE >
		CONSTEXPR17 size_t size(const T(&arr)[SIZE]) NOEXCEPT
		{
			return SIZE;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_SIZE_H