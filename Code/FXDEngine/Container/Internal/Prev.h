// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_PREV_H
#define FXDENGINE_CONTAINER_INTERNAL_PREV_H

#include "FXDEngine/Container/Internal/Advance.h"
#include "FXDEngine/Container/Internal/IteratorTraits.h"

namespace FXD
{
	namespace STD
	{
		// prev
#		define FXD_SUPPORTS_PREV 1

		template < typename InputItr >
		CONSTEXPR17 InputItr prev(InputItr itr, typename FXD::STD::iterator_traits< InputItr >::difference_type nDist = 1)
		{
			FXD::STD::advance(itr, -nDist);
			return itr;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_PREV_H