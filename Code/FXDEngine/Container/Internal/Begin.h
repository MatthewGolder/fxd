// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_BEGIN_H
#define FXDENGINE_CONTAINER_INTERNAL_BEGIN_H

#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// begin
#		define FXD_SUPPORTS_BEGIN 1

		template < typename Container >
		CONSTEXPR17 auto begin(Container& cont)->decltype(cont.begin())
		{
			return (cont.begin());
		}

		template < typename Container >
		CONSTEXPR17 auto begin(const Container& cont)->decltype(cont.begin())
		{
			return (cont.begin());
		}

		template < typename T, size_t SIZE >
		CONSTEXPR14 T* begin(T(&arr)[SIZE]) NOEXCEPT
		{
			return (arr);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_BEGIN_H