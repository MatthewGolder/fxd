// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_MOVEITERATOR_H
#define FXDENGINE_CONTAINER_INTERNAL_MOVEITERATOR_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/Config.h"
#include <utility>

namespace FXD
{
	namespace STD
	{
		// move_iterator
#		define FXD_SUPPORTS_MOVE_ITERATOR 1

		template < typename Iterator >
		class move_iterator
		{
		public:
			using iterator_type = Iterator;
			using wrapped_iterator_type = iterator_type;
			using traits_type = FXD::STD::iterator_traits< Iterator >;
			using iterator_category = typename traits_type::iterator_category;
			using value_type = typename traits_type::value_type;
			using difference_type = typename traits_type::difference_type;
			using pointer = Iterator;
			using reference = value_type && ;

		public:
			move_iterator(void)
				: m_itr()
			{}

			EXPLICIT move_iterator(iterator_type rhs)
				: m_itr(rhs)
			{}

			~move_iterator(void)
			{}

			template < typename U >
			move_iterator(const move_iterator< U >& rhs)
				: m_itr(rhs.base())
			{}

			iterator_type base(void) const
			{
				return m_itr;
			}

			move_iterator& operator++()
			{
				++m_itr;
				return (*this);
			}
			move_iterator operator++(FXD::S32)
			{
				move_iterator temp = (*this);
				++m_itr;
				return temp;
			}

			move_iterator& operator--()
			{
				--m_itr;
				return (*this);
			}
			move_iterator operator--(FXD::S32)
			{
				move_iterator temp = (*this);
				--m_itr;
				return temp;
			}

			move_iterator& operator+=(difference_type nOff)
			{
				m_itr += nOff;
				return (*this);
			}
			move_iterator operator+(difference_type nOff) const
			{
				return move_iterator(m_itr + nOff);
			}

			move_iterator& operator-=(difference_type nOff)
			{
				m_itr -= nOff;
				return (*this);
			}
			move_iterator operator-(difference_type nOff) const
			{
				return move_iterator(m_itr - nOff);
			}

			reference operator*(void) const
			{
				return std::move(*m_itr);
			}

			pointer operator->() const
			{
				return m_itr;
			}

			reference operator[](difference_type nIdx) const
			{
				return std::move(m_itr[nIdx]);
			}

		protected:
			iterator_type m_itr;
		};

		template < typename IteratorLHS, typename IteratorRHS >
		inline bool operator==(const FXD::STD::move_iterator< IteratorLHS >& lhs, const FXD::STD::move_iterator< IteratorRHS >& rhs)
		{
			return (lhs.base() == rhs.base());
		}

		template < typename IteratorLHS, typename IteratorRHS >
		inline bool operator!=(const FXD::STD::move_iterator< IteratorLHS >& lhs, const FXD::STD::move_iterator< IteratorRHS >& rhs)
		{
			return !(lhs == rhs);
		}

		template < typename IteratorLHS, typename IteratorRHS >
		inline bool operator<(const FXD::STD::move_iterator< IteratorLHS >& lhs, const FXD::STD::move_iterator< IteratorRHS >& rhs)
		{
			return (lhs.base() < rhs.base());
		}

		template < typename IteratorLHS, typename IteratorRHS >
		inline bool operator<=(const FXD::STD::move_iterator< IteratorLHS >& lhs, const FXD::STD::move_iterator< IteratorRHS >& rhs)
		{
			return !(rhs < lhs);
		}

		template < typename IteratorLHS, typename IteratorRHS >
		inline bool operator>(const FXD::STD::move_iterator< IteratorLHS >& lhs, const FXD::STD::move_iterator< IteratorRHS >& rhs)
		{
			return (rhs < lhs);
		}

		template < typename IteratorLHS, typename IteratorRHS >
		inline bool operator>=(const FXD::STD::move_iterator< IteratorLHS >& lhs, const FXD::STD::move_iterator< IteratorRHS >& rhs)
		{
			return !(lhs < rhs);
		}

		template < typename IteratorLHS, typename IteratorRHS >
		inline auto operator-(const FXD::STD::move_iterator< IteratorLHS >& lhs, const FXD::STD::move_iterator< IteratorRHS >& rhs)->decltype(lhs.base() - rhs.base())
		{
			return (lhs.base() - rhs.base());
		}

		template < typename Iterator >
		inline FXD::STD::move_iterator< Iterator > operator+(typename FXD::STD::move_iterator< Iterator >::difference_type nDiff, const FXD::STD::move_iterator< Iterator >& lhs)
		{
			return (lhs + nDiff);
		}

		template < typename T >
		struct is_move_iterator : public FXD::STD::false_type
		{};

		template < typename Iterator >
		struct is_move_iterator< FXD::STD::move_iterator< Iterator > > : public FXD::STD::true_type
		{};

		template < typename Iterator >
		inline FXD::STD::move_iterator< Iterator > make_move_iterator(Iterator itr)
		{
			return FXD::STD::move_iterator< Iterator >(itr);
		}

		template < typename Iterator >
		inline FXD::STD::move_iterator< Iterator > make_move_if_noexcept_iterator(Iterator itr)
		{
			return FXD::STD::move_iterator< Iterator >(itr);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_MOVEITERATOR_H