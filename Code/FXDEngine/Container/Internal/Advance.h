// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_ADVANCE_H
#define FXDENGINE_CONTAINER_INTERNAL_ADVANCE_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"

namespace FXD
{
	namespace STD
	{
		// advance
#		define FXD_SUPPORTS_ADVANCE 1

		namespace Internal
		{
			template < typename InputItr, typename NumType, typename IteratorTag >
			struct _advance
			{
				static void impl(InputItr& itr, NumType nDist)
				{
					for (; 0 < nDist; --nDist)
					{
						++itr;
					}
				}
			};

			template < typename BidirItr, typename NumType >
			struct _advance< BidirItr, NumType, FXD::STD::bidirectional_iterator_tag >
			{
				static void impl(BidirItr& itr, NumType nDist)
				{
					// increment iterator by offset, bidirectional iterators
					for (; 0 < nDist; --nDist)
					{
						++itr;
					}
					for (; nDist < 0; ++nDist)
					{
						--itr;
					}
				}
			};
			template < typename RandomAccessItr, typename NumType >
			struct _advance< RandomAccessItr, NumType, FXD::STD::random_access_iterator_tag >
			{
				static void impl(RandomAccessItr& itr, NumType nDist)
				{
					itr += nDist;
				}
			};
		} //namespace Internal

		template < typename InputItr, typename NumType >
		inline void advance(InputItr& itr, NumType nDist)
		{
			using IC = typename FXD::STD::iterator_traits< InputItr >::iterator_category;

			return FXD::STD::Internal::_advance< InputItr, NumType, IC >::impl(itr, nDist);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_ADVANCE_H