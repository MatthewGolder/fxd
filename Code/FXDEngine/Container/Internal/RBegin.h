// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_RBEGIN_H
#define FXDENGINE_CONTAINER_INTERNAL_RBEGIN_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Container/Internal/Reverse.h"
#include <initializer_list>

namespace FXD
{
	namespace STD
	{
		// rbegin
#		define FXD_SUPPORTS_RBEGIN 1

		template < typename Container >
		CONSTEXPR17 auto rbegin(Container& cont)->decltype(cont.rbegin())
		{
			return (cont.rbegin());
		}

		template < typename Container >
		CONSTEXPR17 auto rbegin(const Container& cont)->decltype(cont.rbegin())
		{
			return (cont.rbegin());
		}

		template < typename T, size_t SIZE >
		CONSTEXPR17 FXD::STD::reverse_iterator< T* > rbegin(T(&arr)[SIZE])
		{
			return (FXD::STD::reverse_iterator< T* >(arr + SIZE));
		}

		template < typename T >
		CONSTEXPR17 FXD::STD::reverse_iterator< const T* > rbegin(std::initializer_list< T > iList)
		{
			return (FXD::STD::reverse_iterator< const T* >(iList.end()));
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_RBEGIN_H