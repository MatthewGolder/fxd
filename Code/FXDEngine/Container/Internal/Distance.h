// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_DISTANCE_H
#define FXDENGINE_CONTAINER_INTERNAL_DISTANCE_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"

namespace FXD
{
	namespace STD
	{
		// distance
#		define FXD_SUPPORTS_DISTANCE 1

		namespace Internal
		{
			template < typename InputItr, typename IteratorTag >
			struct _distance
			{
				static typename FXD::STD::iterator_traits< InputItr >::difference_type impl(InputItr first, InputItr last)
				{
					typename FXD::STD::iterator_traits< InputItr >::difference_type nRet = 0;
					while (first != last)
					{
						++first;
						++nRet;
					}
					return nRet;
				}
			};

			template < typename RandomAccessItr >
			struct _distance< RandomAccessItr, FXD::STD::random_access_iterator_tag >
			{
				static typename FXD::STD::iterator_traits< RandomAccessItr >::difference_type impl(RandomAccessItr first, RandomAccessItr last)
				{
					return (last - first);
				}
			};
		} //namespace Internal

		template < typename InputItr >
		inline typename FXD::STD::iterator_traits< InputItr >::difference_type distance(InputItr first, InputItr last)
		{
			using IC = typename FXD::STD::iterator_traits< InputItr >::iterator_category;

			return FXD::STD::Internal::_distance< InputItr, IC >::impl(first, last);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_DISTANCE_H