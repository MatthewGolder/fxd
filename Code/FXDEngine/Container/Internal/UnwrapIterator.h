// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_UNWRAPITERATOR_H
#define FXDENGINE_CONTAINER_INTERNAL_UNWRAPITERATOR_H

#include "FXDEngine/Container/Internal/IteratorWrapper.h"

namespace FXD
{
	namespace STD
	{
		// unwrap_iterator
#		define FXD_SUPPORTS_UNWRAP_ITERATOR 1

		template < typename Iterator >
		inline typename FXD::STD::Internal::_is_iterator_wrapper< Iterator, FXD::STD::is_iterator_wrapper< Iterator >::value >::iterator_type unwrap_iterator(Iterator itr)
		{
			return FXD::STD::Internal::_is_iterator_wrapper< Iterator, FXD::STD::is_iterator_wrapper< Iterator >::value >::get_base(itr);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_UNWRAPITERATOR_H