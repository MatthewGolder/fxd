// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_NEXT_H
#define FXDENGINE_CONTAINER_INTERNAL_NEXT_H

#include "FXDEngine/Container/Internal/Advance.h"

namespace FXD
{
	namespace STD
	{
		// next
#		define FXD_SUPPORTS_NEXT 1

		template < typename InputItr >
		CONSTEXPR17 InputItr next(InputItr itr, typename FXD::STD::iterator_traits< InputItr >::difference_type nDist = 1)
		{
			FXD::STD::advance(itr, nDist);
			return itr;
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_NEXT_H