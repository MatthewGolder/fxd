// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_END_H
#define FXDENGINE_CONTAINER_INTERNAL_END_H

#include "FXDEngine/Core/Internal/Config.h"

namespace FXD
{
	namespace STD
	{
		// end
#		define FXD_SUPPORTS_END 1

		template < typename Container >
		CONSTEXPR17 auto end(Container& cont)->decltype(cont.end())
		{
			return (cont.end());
		}

		template < typename Container >
		CONSTEXPR17 auto end(const Container& cont)->decltype(cont.end())
		{
			return (cont.end());
		}

		template < typename T, size_t SIZE >
		CONSTEXPR14 T* end(T(&arr)[SIZE]) NOEXCEPT
		{
			return (arr + SIZE);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_END_H