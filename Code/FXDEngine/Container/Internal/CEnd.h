// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_CEND_H
#define FXDENGINE_CONTAINER_INTERNAL_CEND_H

#include "FXDEngine/Container/Internal/End.h"

namespace FXD
{
	namespace STD
	{
		// cend
#		define FXD_SUPPORTS_CEND 1

		template < typename Container >
		CONSTEXPR14 auto cend(const Container& cont)NOEXCEPT_IF(NOEXCEPT_IF(FXD::STD::end(cont)))->decltype(FXD::STD::end(cont))
		{
			return (FXD::STD::end(cont));
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_CEND_H