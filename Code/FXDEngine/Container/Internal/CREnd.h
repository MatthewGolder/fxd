// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_CREND_H
#define FXDENGINE_CONTAINER_INTERNAL_CREND_H

#include "FXDEngine/Container/Internal/REnd.h"

namespace FXD
{
	namespace STD
	{
		// cend
#		define FXD_SUPPORTS_CEND 1

		template < typename Container >
		CONSTEXPR14 auto crend(const Container& cont)->decltype(FXD::STD::rend(cont))
		{
			return (FXD::STD::rend(cont));
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_CREND_H