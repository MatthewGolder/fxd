// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_EMPTY_H
#define FXDENGINE_CONTAINER_INTERNAL_EMPTY_H

#include "FXDEngine/Core/Internal/Config.h"
#include <initializer_list>

namespace FXD
{
	namespace STD
	{
		// empty
#		define FXD_SUPPORTS_EMPTY 1

		template < typename Container >
		CONSTEXPR14 auto empty(const Container& cont)->decltype(cont.empty())
		{
			return cont.empty();
		}

		template < typename T, size_t SIZE >
		CONSTEXPR14 bool empty(const T(&arr)[SIZE]) NOEXCEPT
		{
			return (false);
		}

		template < typename T >
		CONSTEXPR14 bool empty(std::initializer_list< T > iList) NOEXCEPT
		{
			return (iList.size() == 0);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_EMPTY_H