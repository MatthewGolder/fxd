// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_ITERATORTRAITS_H
#define FXDENGINE_CONTAINER_INTERNAL_ITERATORTRAITS_H

#include "FXDEngine/Container/Internal/Tags.h"
#include "FXDEngine/Core/Internal/Void_t.h"
#include <cstddef>

namespace FXD
{
	namespace STD
	{
		// iterator
		template < typename Category, typename T, typename Distance = std::ptrdiff_t, typename Pointer = T * , typename Reference = T & >
		struct iterator
		{
			using iterator_category = Category;
			using value_type = T;
			using difference_type = Distance;
			using pointer = Pointer;
			using reference = Reference;
		};

		namespace Internal
		{
			template < class, class = void >
			struct _iterator_traits
			{};

			template < class Iterator >
			struct _iterator_traits< Iterator, FXD::STD::void_t<
				typename Iterator::iterator_category,
				typename Iterator::value_type,
				typename Iterator::difference_type,
				typename Iterator::pointer,
				typename Iterator::reference
			> >
			{
				using iterator_category = typename Iterator::iterator_category;
				using value_type = typename Iterator::value_type;
				using difference_type = typename Iterator::difference_type;
				using pointer = typename Iterator::pointer;
				using reference = typename Iterator::reference;
			};
		} //namespace Internal

		// ------
		// iterator_traits
		// -
		// ------
		template < typename Iterator >
		struct iterator_traits : FXD::STD::Internal::_iterator_traits< Iterator >
		{};

		template < typename T >
		struct iterator_traits< T* >
		{
			using iterator_category = FXD::STD::random_access_iterator_tag;
			using value_type = T;
			using difference_type = std::ptrdiff_t;
			using pointer = T * ;
			using reference = T & ;
		};

		template < typename T >
		struct iterator_traits< const T* >
		{
			using iterator_category = FXD::STD::random_access_iterator_tag;
			using value_type = T;
			using difference_type = std::ptrdiff_t;
			using pointer = const T*;
			using reference = const T&;
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_ITERATORTRAITS_H