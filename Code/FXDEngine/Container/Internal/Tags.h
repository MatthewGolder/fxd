// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_TAGS_H
#define FXDENGINE_CONTAINER_INTERNAL_TAGS_H

namespace FXD
{
	namespace STD
	{
		struct input_iterator_tag
		{};

		struct output_iterator_tag
		{};

		struct forward_iterator_tag : public FXD::STD::input_iterator_tag
		{};

		struct bidirectional_iterator_tag : public FXD::STD::forward_iterator_tag
		{};

		struct random_access_iterator_tag : public FXD::STD::bidirectional_iterator_tag
		{};

		struct contiguous_iterator_tag : public FXD::STD::random_access_iterator_tag
		{};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_TAGS_H