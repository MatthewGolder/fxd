// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_CRBEGIN_H
#define FXDENGINE_CONTAINER_INTERNAL_CRBEGIN_H

#include "FXDEngine/Container/Internal/RBegin.h"

namespace FXD
{
	namespace STD
	{
		// crbegin
#		define FXD_SUPPORTS_CRBEGIN 1

		template < typename Container >
		CONSTEXPR14 auto crbegin(const Container& cont)->decltype(FXD::STD::rbegin(cont))
		{
			return (FXD::STD::rbegin(cont));
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_CRBEGIN_H