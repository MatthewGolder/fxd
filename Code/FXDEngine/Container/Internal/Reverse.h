// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_REVERSE_H
#define FXDENGINE_CONTAINER_INTERNAL_REVERSE_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/Constants.h"

namespace FXD
{
	namespace STD
	{
		// reverse_iterator
#		define FXD_SUPPORTS_REVERSE_ITERATOR 1

		template < typename Iterator >
		class reverse_iterator : public FXD::STD::iterator<
			typename FXD::STD::iterator_traits< Iterator >::iterator_category,
			typename FXD::STD::iterator_traits< Iterator >::value_type,
			typename FXD::STD::iterator_traits< Iterator >::difference_type,
			typename FXD::STD::iterator_traits< Iterator >::pointer,
			typename FXD::STD::iterator_traits< Iterator >::reference >
		{
		public:
			using iterator_type = Iterator;
			using wrapped_iterator_type = iterator_type;
			using pointer = typename FXD::STD::iterator_traits< Iterator >::pointer;
			using reference = typename FXD::STD::iterator_traits< Iterator >::reference;
			using difference_type = typename FXD::STD::iterator_traits< Iterator >::difference_type;

		public:
			CONSTEXPR14 reverse_iterator(void)
				: m_itr()
			{}

			CONSTEXPR14 EXPLICIT reverse_iterator(iterator_type rhs)
				: m_itr(rhs)
			{}

			CONSTEXPR14 reverse_iterator(const reverse_iterator& rhs)
				: m_itr(rhs.base())
			{}

			template < typename U >
			CONSTEXPR14 reverse_iterator(const reverse_iterator< U >& rhs)
				: m_itr(rhs.base())
			{}

			template < typename U >
			CONSTEXPR14 reverse_iterator< Iterator >& operator=(const reverse_iterator< U >& rhs)
			{
				m_itr = rhs.base();
				return (*this);
			}

			CONSTEXPR14 iterator_type base(void) const
			{
				return m_itr;
			}

			CONSTEXPR14 reverse_iterator& operator++()
			{
				--m_itr;
				return (*this);
			}
			CONSTEXPR14 reverse_iterator operator++(int)
			{
				reverse_iterator ri(*this);
				--m_itr;
				return ri;
			}

			CONSTEXPR14 reverse_iterator& operator--(void)
			{
				++m_itr;
				return (*this);
			}
			CONSTEXPR14 reverse_iterator operator--(int)
			{
				reverse_iterator ri(*this);
				++m_itr;
				return ri;
			}

			CONSTEXPR14 reverse_iterator& operator+=(difference_type nOff) NOEXCEPT
			{
				m_itr -= nOff;
				return (*this);
			}
			CONSTEXPR14 reverse_iterator operator+(difference_type nOff) const
			{
				return reverse_iterator(m_itr - nOff);
			}
			CONSTEXPR14 difference_type operator+(const reverse_iterator& rhs)
			{
				return (rhs.base() + base());
			}

			CONSTEXPR14 reverse_iterator& operator-=(difference_type nOff) NOEXCEPT
			{
				m_itr += nOff;
				return (*this);
			}
			CONSTEXPR14 reverse_iterator operator-(difference_type nOff) const
			{
				return reverse_iterator(m_itr + nOff);
			}
			CONSTEXPR14 difference_type operator-(const reverse_iterator& rhs)
			{
				return (rhs.base() - base());
			}
			CONSTEXPR14 reference operator[](difference_type nID) const
			{
				return m_itr[-nID - 1];
			}

			CONSTEXPR14 reference operator*(void) const
			{
				iterator_type itr(m_itr);
				return (*(--itr));
			}
			CONSTEXPR14 pointer operator->(void) const
			{
				return &(operator*());
			}

		protected:
			Iterator m_itr;
		};

		template < typename IteratorLHS, typename IteratorRHS >
		CONSTEXPR14 inline bool operator==(const FXD::STD::reverse_iterator< IteratorLHS >& lhs, const FXD::STD::reverse_iterator< IteratorRHS >& rhs)
		{
			return (lhs.base() == rhs.base());
		}

		template < typename IteratorLHS, typename IteratorRHS >
		CONSTEXPR14 inline bool operator<(const FXD::STD::reverse_iterator< IteratorLHS >& lhs, const FXD::STD::reverse_iterator< IteratorRHS >& rhs)
		{
			return (lhs.base() > rhs.base());
		}

		template < typename IteratorLHS, typename IteratorRHS >
		CONSTEXPR14 inline bool operator!=(const FXD::STD::reverse_iterator< IteratorLHS >& lhs, const FXD::STD::reverse_iterator< IteratorRHS >& rhs)
		{
			return (lhs.base() != rhs.base());
		}

		template < typename IteratorLHS, typename IteratorRHS >
		CONSTEXPR14 inline bool operator>(const FXD::STD::reverse_iterator< IteratorLHS >& lhs, const FXD::STD::reverse_iterator< IteratorRHS >& rhs)
		{
			return (lhs.base() < rhs.base());
		}

		template < typename IteratorLHS, typename IteratorRHS >
		inline bool operator<=(const FXD::STD::reverse_iterator< IteratorLHS >& lhs, const FXD::STD::reverse_iterator< IteratorRHS >& rhs)
		{
			return (lhs.base() >= rhs.base());
		}

		template < typename IteratorLHS, typename IteratorRHS >
		CONSTEXPR14 inline bool operator>=(const FXD::STD::reverse_iterator< IteratorLHS >& lhs, const FXD::STD::reverse_iterator< IteratorRHS >& rhs)
		{
			return (lhs.base() <= rhs.base());
		}

		template < typename IteratorLHS, typename IteratorRHS >
		CONSTEXPR14 inline typename FXD::STD::reverse_iterator< IteratorLHS >::difference_type operator-(const FXD::STD::reverse_iterator< IteratorLHS >& lhs, const FXD::STD::reverse_iterator< IteratorRHS >& rhs)
		{
			return (rhs.base() - lhs.base());
		}

		template < typename Iterator>
		inline FXD::STD::reverse_iterator< Iterator > operator+(typename FXD::STD::reverse_iterator< Iterator >::difference_type n, const FXD::STD::reverse_iterator< Iterator >& lhs)
		{
			return FXD::STD::reverse_iterator< Iterator >(lhs.base() - n);
		}

		template < typename T >
		struct is_reverse_iterator : public FXD::STD::false_type
		{};

		template < typename Iterator >
		struct is_reverse_iterator< FXD::STD::reverse_iterator< Iterator > > : public FXD::STD::true_type
		{};

		template < class Iterator >
		inline FXD::STD::reverse_iterator< Iterator > make_reverse_iterator(Iterator itr)
		{
			return FXD::STD::reverse_iterator< Iterator >(itr);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_REVERSE_H