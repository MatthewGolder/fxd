// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_BACKINSERTITERATOR_H
#define FXDENGINE_CONTAINER_INTERNAL_BACKINSERTITERATOR_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/Config.h"
#include <utility>

namespace FXD
{
	namespace STD
	{
		// back_insert_iterator
#		define FXD_SUPPORTS_BACK_INSERT_ITERATOR 1

		template < typename Container >
		class back_insert_iterator : public FXD::STD::iterator< FXD::STD::output_iterator_tag, void, void, void, void >
		{
		public:
			using my_type = FXD::STD::back_insert_iterator< Container >;
			using container_type = Container;
			using const_reference = typename Container::const_reference;

		public:
			EXPLICIT back_insert_iterator(Container& rhs)
				: container(&rhs)
			{}

			back_insert_iterator& operator=(const_reference val)
			{
				container->push_back(val);
				return (*this);
			}
			back_insert_iterator& operator=(typename Container::value_type&& val)
			{
				container->push_back(std::move(val));
				return (*this);
			}

			back_insert_iterator& operator*()
			{
				return (*this);
			}

			back_insert_iterator& operator++()
			{
				return (*this);
			}

			back_insert_iterator operator++(int)
			{
				return (*this);
			}

			void operator=(const my_type& rhs)
			{
				container = rhs.container;
			}

		protected:
			Container* container;
		};

		template < typename Container >
		inline FXD::STD::back_insert_iterator< Container > back_inserter(Container& rhs)
		{
			return FXD::STD::back_insert_iterator< Container >(rhs);
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_BACKINSERTITERATOR_H