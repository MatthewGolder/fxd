// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_ITERATORWRAPPER_H
#define FXDENGINE_CONTAINER_INTERNAL_ITERATORWRAPPER_H

#include "FXDEngine/Container/Internal/IteratorTraits.h"
#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Core/Internal/EnableIf.h"
#include "FXDEngine/Core/Internal/IsClass.h"

namespace FXD
{
	namespace STD
	{
		// is_iterator_wrapper
#		define FXD_SUPPORTS_IS_ITERATOR_WRAPPER 1

		namespace Internal
		{
			template < typename Iterator, bool isWrapper >
			struct _is_iterator_wrapper
			{
				using iterator_type = Iterator;

				static iterator_type get_base(Iterator itr)
				{
					return itr;
				}
			};


			template < typename Iterator >
			struct _is_iterator_wrapper< Iterator, true >
			{
				using iterator_type = typename Iterator::iterator_type;

				static iterator_type get_base(Iterator itr)
				{
					return itr.base();
				}
			};
		} //namespace Internal

		template < typename Iterator >
		class is_iterator_wrapper
		{
			template < typename >
			static FXD::STD::Internal::no_type test(...);

			template < typename U >
			static FXD::STD::Internal::yes_type test(typename U::wrapped_iterator_type*, typename FXD::STD::enable_if< FXD::STD::is_class< U >::value >::type* = 0);

		public:
			static const bool value = (sizeof(test< Iterator >(nullptr)) == sizeof(FXD::STD::Internal::yes_type));
		};

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_ITERATORWRAPPER_H