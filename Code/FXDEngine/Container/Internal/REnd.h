// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_INTERNAL_REND_H
#define FXDENGINE_CONTAINER_INTERNAL_REND_H

#include "FXDEngine/Core/Internal/Config.h"
#include "FXDEngine/Container/Internal/Reverse.h"
#include <initializer_list>

namespace FXD
{
	namespace STD
	{
		// rend
#		define FXD_SUPPORTS_REND 1

		template < typename Container >
		CONSTEXPR17 auto rend(Container& cont)->decltype(cont.rend())
		{
			return (cont.rend());
		}

		template < typename Container >
		CONSTEXPR17 auto rend(const Container& cont)->decltype(cont.rend())
		{
			return (cont.rend());
		}

		template < class T, size_t SIZE >
		CONSTEXPR17 FXD::STD::reverse_iterator< T* > rend(T(&arr)[SIZE])
		{
			return (FXD::STD::reverse_iterator< T* >(arr));
		}

		template < typename T >
		CONSTEXPR17 FXD::STD::reverse_iterator< const T* > rend(std::initializer_list< T > iList)
		{
			return (FXD::STD::reverse_iterator< const T* >(iList.begin()));
		}

	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_INTERNAL_REND_H