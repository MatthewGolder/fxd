// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_DIRTYTYPE_H
#define FXDENGINE_CONTAINER_DIRTYTYPE_H

#include "FXDEngine/Core/Types.h"

namespace FXD
{
	namespace Container
	{
		// ------
		// DirtyType
		// - 
		// ------
		template < typename T >
		class DirtyType
		{
		public:
			DirtyType(const T& type, bool bDirty = true);
			~DirtyType(void);

			DirtyType(const DirtyType< T >& rhs);

			DirtyType< T >& operator=(const DirtyType< T >& rhs);

			bool operator==(const T& rhs) const;
			bool operator!=(const T& rhs) const;

			operator T(void) const;

			void set(const T& t, bool bDirty = true);
			const T& get(void) const;

			bool is_dirty(void) const;
			void mark_clean(void);

		public:
			T m_type;
			bool m_bDirty;
		};

		// ------
		// DirtyType
		// -
		// ------
		template < typename T >
		inline DirtyType< T >::DirtyType(const T& type, bool bDirty)
			: m_bDirty(bDirty)
		{
			set(type, bDirty);
		}
		template < typename T >
		inline DirtyType< T >::~DirtyType(void)
		{
		}
		template < typename T >
		inline DirtyType< T >::DirtyType(const DirtyType< T >& rhs)
		{
			set(rhs.m_type);
		}
		template < typename T >
		inline DirtyType< T >& DirtyType< T >::operator=(const DirtyType< T >& rhs)
		{
			if (this != &rhs)
			{
				set(rhs.m_type);
			}
			return (*this);
		}
		template < typename T >
		inline bool DirtyType< T >::operator==(const T& rhs) const
		{
			return (this->m_type == rhs);
		}
		template < typename T >
		inline bool DirtyType< T >::operator!=(const T& rhs) const
		{
			return (this->m_type != rhs);
		}
		template < typename T >
		inline DirtyType< T >::operator T(void) const
		{
			return get();
		}
		template < typename T >
		inline void DirtyType< T >::set(const T& t, bool bDirty)
		{
			m_type = t;
			m_bDirty = bDirty;
		}
		template < typename T >
		inline const T& DirtyType< T >::get(void) const
		{
			return m_type;
		}
		template < typename T >
		inline bool DirtyType< T >::is_dirty(void) const
		{
			return m_bDirty;
		}
		template < typename T >
		inline void DirtyType< T >::mark_clean(void)
		{
			m_bDirty = false;
		}

	} //namespace Container
} //namespace FXD
#endif //FXDENGINE_CONTAINER_DIRTYTYPE_H
