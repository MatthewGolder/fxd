// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_VECTOR_H
#define FXDENGINE_CONTAINER_VECTOR_H

#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Core/TypeTraits.h"
#include "FXDEngine/Core/Internal/CopyBackward.h"
#include "FXDEngine/Core/Internal/Count.h"
#include "FXDEngine/Core/Internal/Find.h"
#include "FXDEngine/Core/Internal/LexicographicalCompare.h"
#include "FXDEngine/Core/Internal/Max.h"
#include "FXDEngine/Core/Internal/Move.h"
#include "FXDEngine/Core/Internal/MoveBackward.h"
#include "FXDEngine/Core/Internal/Sort.h"
#include "FXDEngine/Core/Internal/Remove.h"
#include "FXDEngine/Core/Internal/RemoveIf.h"
#include "FXDEngine/Container/Iterator.h"
#include "FXDEngine/Memory/Allocator/Aligned.h"
#include "FXDEngine/Memory/Memory.h"
#include <initializer_list>

namespace FXD
{
	namespace Container
	{
		namespace Internal
		{
			// ------
			// _vector_iterator
			// -
			// ------
			template < typename Container, typename T, typename Pointer, typename Reference >
			class _vector_iterator
			{
			public:
				using my_type = _vector_iterator< Container, T, Pointer, Reference >;
				using container = Container;
				using value_type = T;
				using reference = Reference;
				using pointer = Pointer;
				using size_type = FXD::U32;
				using difference_type = std::ptrdiff_t;
		
				using iterator_category = FXD::STD::random_access_iterator_tag;
				using iterator = _vector_iterator< Container, T, T*, T& >;
				using const_iterator = _vector_iterator< Container, T, const T*, const T& >;

			public:
				_vector_iterator(void);
				_vector_iterator(container* pCont, pointer pType);
				_vector_iterator(const iterator& rhs);

				my_type& operator++();
				my_type operator++(int);

				my_type& operator--();
				my_type operator--(int);

				my_type& operator+=(difference_type nOff) NOEXCEPT;
				my_type& operator-=(difference_type nOff) NOEXCEPT;

				my_type operator+(difference_type nOff) const;
				my_type operator-(difference_type nOff) const;

				difference_type operator-(const my_type& rhs) const;

				reference operator*(void) const;
				pointer operator->(void) const;

			protected:
				bool _is_valid(void) const NOEXCEPT;

				// Non-member comparison functions
				// Equality
				template < typename Container2, typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB >
				friend bool operator==(const _vector_iterator< Container2, T2, PtrA, RefA >& lhs, const _vector_iterator< Container2, T2, PtrB, RefB >& rhs) NOEXCEPT;
				template < typename Container2, typename T2, typename PtrA, typename RefA >
				friend bool operator==(const _vector_iterator< Container2, T2, PtrA, RefA >& lhs, const _vector_iterator< Container2, T2, PtrA, RefA >& rhs) NOEXCEPT;
				// Inequality
				template < typename Container2, typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB >
				friend bool operator!=(const _vector_iterator< Container2, T2, PtrA, RefA >& lhs, const _vector_iterator< Container2, T2, PtrB, RefB >& rhs) NOEXCEPT;
				template < typename Container2, typename T2, typename PtrA, typename RefA >
				friend bool operator!=(const _vector_iterator< Container2, T2, PtrA, RefA >& lhs, const _vector_iterator< Container2, T2, PtrA, RefA >& rhs) NOEXCEPT;
				// Less than
				template < typename Container2, typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB >
				friend bool operator<(const _vector_iterator< Container2, T2, PtrA, RefA >& lhs, const _vector_iterator< Container2, T2, PtrB, RefB >& rhs) NOEXCEPT;
				// Greater than
				template < typename Container2, typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB >
				friend bool operator>(const _vector_iterator< Container2, T2, PtrA, RefA >& lhs, const _vector_iterator< Container2, T2, PtrB, RefB >& rhs) NOEXCEPT;
				// Less than or equal to
				template < typename Container2, typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB >
				friend bool operator<=(const _vector_iterator< Container2, T2, PtrA, RefA >& lhs, const _vector_iterator< Container2, T2, PtrB, RefB >& rhs) NOEXCEPT;
				// Greater than or equal to
				template < typename Container2, typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB >
				friend bool operator>=(const _vector_iterator< Container2, T2, PtrA, RefA >& lhs, const _vector_iterator< Container2, T2, PtrB, RefB >& rhs) NOEXCEPT;

				template < typename Container2, typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB >
				friend difference_type operator+(const _vector_iterator< Container2, T2, PtrA, RefA >& lhs, const _vector_iterator< Container2, T2, PtrB, RefB >& rhs) NOEXCEPT;
				template < typename Container2, typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB >
				friend difference_type operator-(const _vector_iterator< Container2, T2, PtrA, RefA >& lhs, const _vector_iterator< Container2, T2, PtrB, RefB >& rhs) NOEXCEPT;

			public:
				Container* m_pCont;
				Pointer m_pType;
			};

			// ------
			// _vector_iterator
			// -
			// ------
			template < typename Container, typename T, typename Pointer, typename Reference >
			_vector_iterator< Container, T, Pointer, Reference >::_vector_iterator(void)
				: m_pCont(nullptr)
				, m_pType(nullptr)
			{}
			template < typename Container, typename T, typename Pointer, typename Reference >
			_vector_iterator< Container, T, Pointer, Reference >::_vector_iterator(container* pCont, pointer pType)
				: m_pCont(pCont)
				, m_pType(pType)
			{}
			template < typename Container, typename T, typename Pointer, typename Reference >
			_vector_iterator< Container, T, Pointer, Reference >::_vector_iterator(const iterator& rhs)
				: m_pCont(const_cast<Container*>(rhs.m_pCont))
				, m_pType(const_cast<T*>(rhs.m_pType))
			{}
			template < typename Container, typename T, typename Pointer, typename Reference >
			typename _vector_iterator< Container, T, Pointer, Reference >::my_type& _vector_iterator< Container, T, Pointer, Reference >::operator++()
			{
				PRINT_COND_ASSERT((((m_pCont == nullptr) || (m_pType == nullptr) || (m_pCont->_last() <= m_pType)) == false), "Container: Iterator is invalid");
				++m_pType;
				return (*this);
			}
			template < typename Container, typename T, typename Pointer, typename Reference >
			typename _vector_iterator< Container, T, Pointer, Reference >::my_type _vector_iterator< Container, T, Pointer, Reference >::operator++(int)
			{
				const my_type tmp(*this);
				operator++();
				return tmp;
			}

			template < typename Container, typename T, typename Pointer, typename Reference >
			typename _vector_iterator< Container, T, Pointer, Reference >::my_type& _vector_iterator< Container, T, Pointer, Reference >::operator--()
			{
				PRINT_COND_ASSERT((((m_pCont == nullptr) || (m_pType == nullptr) || (m_pType <= m_pCont->_first())) == false), "Container: Iterator is invalid");
				--m_pType;
				return (*this);
			}
			template < typename Container, typename T, typename Pointer, typename Reference >
			typename _vector_iterator< Container, T, Pointer, Reference >::my_type _vector_iterator< Container, T, Pointer, Reference >::operator--(int)
			{
				const my_type tmp(*this);
				operator--();
				return tmp;
			}

			template < typename Container, typename T, typename Pointer, typename Reference >
			typename _vector_iterator< Container, T, Pointer, Reference >::my_type& _vector_iterator< Container, T, Pointer, Reference >::operator+=(difference_type nOff) NOEXCEPT
			{
				m_pType += nOff;
				return (*this);
			}
			template < typename Container, typename T, typename Pointer, typename Reference >
			typename _vector_iterator< Container, T, Pointer, Reference >::my_type& _vector_iterator< Container, T, Pointer, Reference >::operator-=(difference_type nOff) NOEXCEPT
			{
				m_pType -= nOff;
				return (*this);
			}

			template < typename Container, typename T, typename Pointer, typename Reference >
			typename _vector_iterator< Container, T, Pointer, Reference >::my_type _vector_iterator< Container, T, Pointer, Reference >::operator+(difference_type nOff) const
			{
				return FXD::STD::next((*this), nOff);
			}

			template < typename Container, typename T, typename Pointer, typename Reference >
			typename _vector_iterator< Container, T, Pointer, Reference >::my_type _vector_iterator< Container, T, Pointer, Reference >::operator-(difference_type nOff) const
			{
				return FXD::STD::prev((*this), nOff);
			}

			template < typename Container, typename T, typename Pointer, typename Reference >
			typename _vector_iterator< Container, T, Pointer, Reference >::difference_type _vector_iterator< Container, T, Pointer, Reference >::operator-(const my_type& rhs) const
			{
				return (m_pType - rhs.m_pType);
			}

			template < typename Container, typename T, typename Pointer, typename Reference >
			typename _vector_iterator< Container, T, Pointer, Reference >::reference _vector_iterator< Container, T, Pointer, Reference >::operator*(void) const
			{
				PRINT_COND_ASSERT((_is_valid()), "Container: Iterator is invalid");
				return (*m_pType);
			}
			template < typename Container, typename T, typename Pointer, typename Reference >
			typename _vector_iterator< Container, T, Pointer, Reference >::pointer _vector_iterator< Container, T, Pointer, Reference >::operator->(void) const
			{
				return &(operator*());
			}

			template < typename Container, typename T, typename Pointer, typename Reference >
			bool _vector_iterator< Container, T, Pointer, Reference >::_is_valid(void) const NOEXCEPT
			{
				return !((m_pCont == nullptr) || (m_pType == nullptr) || (m_pType < m_pCont->_first()) || (m_pCont->_last() <= m_pType));
			}

			// Non-member comparison functions
			// Equality
			template < typename Container, typename T, typename PtrA, typename RefA, typename PtrB, typename RefB >
			inline bool operator==(const _vector_iterator< Container, T, PtrA, RefA >& lhs, const _vector_iterator< Container, T, PtrB, RefB >& rhs) NOEXCEPT
			{
				return (lhs.m_pCont == rhs.m_pCont) && (lhs.m_pType == rhs.m_pType);
			}
			template < typename Container, typename T, typename PtrA, typename RefA >
			inline bool operator==(const _vector_iterator< Container, T, PtrA, RefA >& lhs, const _vector_iterator< Container, T, PtrA, RefA >& rhs) NOEXCEPT
			{
				return (lhs.m_pCont == rhs.m_pCont) && (lhs.m_pType == rhs.m_pType);
			}
			// Inequality
			template < typename Container, typename T, typename PtrA, typename RefA, typename PtrB, typename RefB >
			inline bool operator!=(const _vector_iterator< Container, T, PtrA, RefA >& lhs, const _vector_iterator< Container, T, PtrB, RefB >& rhs) NOEXCEPT
			{
				return (lhs.m_pCont != rhs.m_pCont) || (lhs.m_pType != rhs.m_pType);
			}
			template < typename Container, typename T, typename PtrA, typename RefA >
			inline bool operator!=(const _vector_iterator< Container, T, PtrA, RefA >& lhs, const _vector_iterator< Container, T, PtrA, RefA >& rhs) NOEXCEPT
			{
				return (lhs.m_pCont != rhs.m_pCont) || (lhs.m_pType != rhs.m_pType);
			}
			// Less than
			template < typename Container, typename T, typename PtrA, typename RefA, typename PtrB, typename RefB >
			inline bool operator<(const _vector_iterator< Container, T, PtrA, RefA >& lhs, const _vector_iterator< Container, T, PtrB, RefB >& rhs) NOEXCEPT
			{
				return (lhs.m_pType < rhs.m_pType);
			}
			// Greater than
			template < typename Container, typename T, typename PtrA, typename RefA, typename PtrB, typename RefB >
			inline bool operator>(const _vector_iterator< Container, T, PtrA, RefA >& lhs, const _vector_iterator< Container, T, PtrB, RefB >& rhs) NOEXCEPT
			{
				return (lhs.m_pType > rhs.m_pType);
			}
			// Less than or equal to
			template < typename Container, typename T, typename PtrA, typename RefA, typename PtrB, typename RefB >
			inline bool operator<=(const _vector_iterator< Container, T, PtrA, RefA >& lhs, const _vector_iterator< Container, T, PtrB, RefB >& rhs) NOEXCEPT
			{
				return (lhs.m_pType <= rhs.m_pType);
			}
			// Greater than or equal to
			template < typename Container, typename T, typename PtrA, typename RefA, typename PtrB, typename RefB >
			inline bool operator>=(const _vector_iterator< Container, T, PtrA, RefA >& lhs, const _vector_iterator< Container, T, PtrB, RefB >& rhs) NOEXCEPT
			{
				return (lhs.m_pType >= rhs.m_pType);
			}

			template < typename Container, typename T, typename PtrA, typename RefA, typename PtrB, typename RefB >
			inline typename _vector_iterator< Container, T, PtrA, RefA >::difference_type operator+(const _vector_iterator< Container, T, PtrA, RefA >& lhs, const _vector_iterator< Container, T, PtrB, RefB >& rhs) NOEXCEPT
			{
				return (lhs.m_pType + rhs.m_pType);
			}
			template < typename Container, typename T, typename PtrA, typename RefA, typename PtrB, typename RefB >
			inline typename _vector_iterator< Container, T, PtrA, RefA >::difference_type operator-(const _vector_iterator< Container, T, PtrA, RefA >& lhs, const _vector_iterator< Container, T, PtrB, RefB >& rhs) NOEXCEPT
			{
				return (lhs.m_pType - rhs.m_pType);
			}

			// ------
			// _VectorBase
			// -
			// ------
			template <
				typename T,
				typename Allocator = Memory::Alloc::AlignedType< Memory::Alloc::Heap, T > >
			class _VectorBase
			{
			protected:
				using my_type = _VectorBase< T, Allocator >;
				using value_type = T;
				using size_type = FXD::U32;
				using difference_type = std::ptrdiff_t;
				using allocator_type = Allocator;

				static const size_type npos = (size_type)-1;		// 'npos' means non-valid position or simply non-position.
				static const size_type kMaxSize = (size_type)-2;	// -1 is reserved for 'npos'. It also happens to be slightly beneficial that kMaxSize is a value less than -1, as it helps us deal with potential integer wraparound issues.

			public:
				_VectorBase(void);
				_VectorBase(const allocator_type& alloc);
				_VectorBase(size_type nCount, const allocator_type& alloc = allocator_type());
				virtual ~_VectorBase(void);

			protected:
				value_type* _allocate(size_type nCount);
				void _free(size_type nCount);
				size_type _get_new_capacity(size_type currentCapacity);

			protected:
				value_type* m_pBegin;
				value_type* m_pEnd;
				value_type* m_pCapacity;
				allocator_type m_alloc;
			};

			// ------
			// _VectorBase
			// -
			// ------
			template < typename T, typename Allocator >
			_VectorBase< T, Allocator >::_VectorBase(void)
				: m_pBegin(nullptr)
				, m_pEnd(nullptr)
				, m_pCapacity(nullptr)
			{}
			template < typename T, typename Allocator >
			_VectorBase< T, Allocator >::_VectorBase(const allocator_type& alloc)
				: m_pBegin(nullptr)
				, m_pEnd(nullptr)
				, m_pCapacity(nullptr)
				, m_alloc(alloc)
			{}
			template < typename T, typename Allocator >
			_VectorBase< T, Allocator >::_VectorBase(size_type nCount, const allocator_type& alloc)
				: m_pBegin(nullptr)
				, m_pEnd(nullptr)
				, m_pCapacity(nullptr)
				, m_alloc(alloc)
			{
				m_pBegin = _allocate(nCount);
				m_pEnd = m_pBegin;
				m_pCapacity = (m_pBegin + nCount);
			}

			template < typename T, typename Allocator >
			_VectorBase< T, Allocator >::~_VectorBase(void)
			{
				_free((size_type)(m_pCapacity - m_pBegin));
			}

			template < typename T, typename Allocator >
			typename _VectorBase< T, Allocator >::value_type* _VectorBase< T, Allocator >::_allocate(size_type nCount)
			{
				value_type* pData = nullptr;
				if (nCount > 0)
				{
					pData = (value_type*)m_alloc.allocate(nCount * sizeof(T));
					PRINT_COND_ASSERT((pData != nullptr), "Core: nullptr generated. Failed to allocate memory");
				}
				return pData;
			}
			template < typename T, typename Allocator >
			void _VectorBase< T, Allocator >::_free(size_type nCount)
			{
				if (m_pBegin != nullptr)
				{
					m_alloc.deallocate(m_pBegin, (nCount * sizeof(value_type)));
				}
				m_pBegin = nullptr;
				m_pEnd = nullptr;
				m_pCapacity = nullptr;
			}
			template < typename T, typename Allocator >
			typename _VectorBase< T, Allocator >::size_type _VectorBase< T, Allocator >::_get_new_capacity(size_type nCurrentCapacity)
			{
				return (nCurrentCapacity > 0) ? (2 * nCurrentCapacity) : 1;
			}
		} //namespace Internal

		// ------
		// Vector
		// -
		// ------
		template <
			typename T,
			typename Allocator = Memory::Alloc::AlignedType< Memory::Alloc::Heap, T > >
		class Vector : public Internal::_VectorBase< T, Allocator >
		{
		public:
			using my_type = Vector< T, Allocator >;
			using base_type = Internal::_VectorBase< T, Allocator >;
			using value_type = T;
			using pointer = T*;
			using const_pointer = const T*;
			using reference = T&;
			using const_reference = const T&;
			using size_type = typename base_type::size_type;
			using difference_type = typename base_type::difference_type;
			using allocator_type = typename base_type::allocator_type;
			using iterator = Internal::_vector_iterator< my_type, T, T*, T& >;
			using const_iterator = Internal::_vector_iterator< my_type, T, const T*, const T& >;
			using reverse_iterator = FXD::STD::reverse_iterator< iterator >;
			using const_reverse_iterator = FXD::STD::reverse_iterator< const_iterator >;

			friend iterator;
			friend const_iterator;
			friend reverse_iterator;
			friend const_reverse_iterator;

		public:
			Vector(void);
			EXPLICIT Vector(const allocator_type& alloc);
			EXPLICIT Vector(size_type nCount, const allocator_type& alloc = allocator_type());
			Vector(size_type nCount, const value_type& val, const allocator_type& alloc = allocator_type());

			template < typename InputItr, typename = typename FXD::STD::enable_if< FXD::Internal::_Is_iterator< InputItr >::value, void >::type >
			Vector(InputItr first, InputItr last, const allocator_type& alloc = allocator_type());

			Vector(std::initializer_list< value_type > iList, const allocator_type& alloc = allocator_type());

			~Vector(void);

			Vector(const my_type& rhs);
			Vector(const my_type& rhs, const allocator_type& alloc);
			Vector(my_type&& rhs);
			Vector(my_type&& rhs, const allocator_type& alloc);

			my_type& operator=(const my_type& rhs);
			my_type& operator=(my_type&& rhs);
			my_type& operator=(std::initializer_list< value_type > iList);

			// Iterator Support
			iterator begin(void) NOEXCEPT;
			const_iterator begin(void) const NOEXCEPT;
			const_iterator cbegin(void) const NOEXCEPT;
			iterator end(void) NOEXCEPT;
			const_iterator end(void) const NOEXCEPT;
			const_iterator cend(void) const NOEXCEPT;

			reverse_iterator rbegin(void) NOEXCEPT;
			const_reverse_iterator rbegin(void) const NOEXCEPT;
			const_reverse_iterator crbegin(void) const NOEXCEPT;
			reverse_iterator rend(void) NOEXCEPT;
			const_reverse_iterator rend(void) const NOEXCEPT;
			const_reverse_iterator crend(void) const NOEXCEPT;

			// Access
			reference operator[](size_type nID);
			const_reference operator[](size_type nID) const;
			reference at(size_type nID);
			const_reference at(size_type nID) const;

			reference front(void);
			const_reference front(void) const;
			reference back(void);
			const_reference back(void) const;

			pointer data(void) NOEXCEPT;
			const_pointer data(void) const NOEXCEPT;

			// Assign
			void assign(size_type nCount, const value_type& val);
			template < typename InputItr, class = typename FXD::STD::enable_if< FXD::Internal::_Is_iterator< InputItr >::value, void >::type >
			void assign(InputItr first, InputItr last);
			void assign(std::initializer_list< value_type > iList);

			// Resize
			void resize(size_type nCount, const value_type& val);
			void resize(size_type nCount);
			void reserve(size_type nCount);
			void shrink_to_fit(void);

			// Insert
			iterator insert(const_iterator pos, const value_type& val);
			iterator insert(const_iterator pos, value_type&& val);
			iterator insert(const_iterator pos, size_type nCount, const value_type& val);
			iterator insert(const_iterator pos, std::initializer_list< value_type > iList);
			template < typename InputItr, class = typename FXD::STD::enable_if< FXD::Internal::_Is_iterator< InputItr >::value, void >::type >
			iterator insert(const_iterator pos, InputItr first, InputItr last);

			reference push_back(void);
			void push_back(const value_type& val);
			void push_back(value_type&& val);

			void push_front(void);
			void push_front(const value_type& val);
			void push_front(value_type&& val);

			template < class... Args >
			iterator emplace(const_iterator pos, Args&&... args);
			template < class... Args >
			reference emplace_back(Args&&... args);

			// Erase
			iterator erase(const_iterator pos);
			iterator erase(const_iterator first, const_iterator last);
			iterator erase_unsorted(const_iterator pos);
			reverse_iterator erase(const_reverse_iterator pos);
			reverse_iterator erase(const_reverse_iterator first, const_reverse_iterator last);
			reverse_iterator erase_unsorted(const_reverse_iterator pos);
			size_type find_erase(const value_type& key);

			void pop_front(void);
			void pop_back(void);
			void clear(void) NOEXCEPT;

			// Adjust
			bool bring_to_front(const value_type& key);
			bool bring_to_back(const value_type& key);

			void sort(void);
			template < typename Pr >
			void sort(Pr cmp);
			void swap(my_type& rhs);

			// Searches
			const_iterator find(const value_type& key) const;
			bool contains(const value_type& key) const;

			bool empty(void) const NOEXCEPT;
			bool not_empty(void) const NOEXCEPT;
			bool validate(void) const NOEXCEPT;
			size_type size(void) const NOEXCEPT;
			size_type capacity(void) const NOEXCEPT;
			size_type count(const value_type& key) const NOEXCEPT;
			allocator_type allocator(void) const NOEXCEPT;

		private:
			struct should_copy_tag {}; struct should_move_tag : public should_copy_tag {};

			pointer _first(void) NOEXCEPT;
			const_pointer _first(void) const NOEXCEPT;
			pointer _last(void) NOEXCEPT;
			const_pointer _last(void) const NOEXCEPT;

			template < typename Integer >
			void _init(Integer nDist, Integer val, FXD::STD::true_type);

			template < typename InputItr >
			void _init(InputItr first, InputItr last, FXD::STD::false_type);

			template < typename InputItr >
			void _init_iterator(InputItr first, InputItr last, FXD::STD::input_iterator_tag);

			template < typename ForwardItr >
			void _init_iterator(ForwardItr first, ForwardItr last, FXD::STD::forward_iterator_tag);

			template < typename Integer, bool bMove >
			void _assign(Integer n, Integer val, FXD::STD::true_type);

			template < typename InputItr, bool bMove >
			void _assign(InputItr first, InputItr last, FXD::STD::false_type);

			template < typename InputItr, bool bMove >
			void _assign_from_iterator(InputItr first, InputItr last, FXD::STD::input_iterator_tag);

			template < typename RandomAccessItr, bool bMove >
			void _assign_from_iterator(RandomAccessItr first, RandomAccessItr last, FXD::STD::random_access_iterator_tag);

			void _assign_values(size_type nCount, const value_type& val);

			template < typename Integer >
			void _insert(const_iterator pos, Integer n, Integer val, FXD::STD::true_type);
			template < typename InputItr >
			void _insert(const_iterator pos, InputItr first, InputItr last, FXD::STD::false_type);

			template < typename InputItr >
			void _insert_from_iterator(const_iterator pos, InputItr first, InputItr last, FXD::STD::input_iterator_tag);
			template < typename BidirItr >
			void _insert_from_iterator(const_iterator pos, BidirItr first, BidirItr last, FXD::STD::bidirectional_iterator_tag);

			template < typename... Args >
			void _insert_value(const_iterator pos, Args&&... args);

			template < typename... Args >
			void _insert_value_end(Args&&... args);

			void _insert_values(const_iterator pos, size_type nCount, const value_type& val);

			void _insert_values_end(size_type nCount);
			void _insert_values_end(size_type nCount, const value_type& val);

			void _grow(size_type nCount);

			template < typename ForwardItr > // Allocates a pointer of array count n and copy-constructs it with [first,last).
			pointer _realloc(size_type nCount, ForwardItr first, ForwardItr last, should_copy_tag);

			template < typename ForwardItr > // Allocates a pointer of array count n and copy-constructs it with [first,last).
			pointer _realloc(size_type nCount, ForwardItr first, ForwardItr last, should_move_tag);

			void _swap(my_type& rhs);
		};

		// ------
		// Vector
		// -
		// ------
		template < typename T, typename Allocator >
		Vector< T, Allocator >::Vector(void)
			: base_type()
		{}
		template < typename T, typename Allocator >
		Vector< T, Allocator >::Vector(const allocator_type& alloc)
			: base_type(alloc)
		{}
		template < typename T, typename Allocator >
		Vector< T, Allocator >::Vector(size_type nCount, const allocator_type& alloc)
			: base_type(nCount, alloc)
		{
			FXD::STD::uninitialized_default_fill_ptr_n(this->m_pBegin, nCount);
			this->m_pEnd = (this->m_pBegin + nCount);
		}
		template < typename T, typename Allocator >
		Vector< T, Allocator >::Vector(size_type nCount, const value_type& val, const allocator_type& alloc)
			: base_type(nCount, alloc)
		{
			FXD::STD::uninitialized_fill_ptr_n(this->m_pBegin, nCount, val);
			this->m_pEnd = (this->m_pBegin + nCount);
		}
		template < typename T, typename Allocator >
		Vector< T, Allocator >::Vector(std::initializer_list< value_type > iList, const allocator_type& alloc)
			: base_type(alloc)
		{
			_init(iList.begin(), iList.end(), FXD::STD::false_type());
		}
		template < typename T, typename Allocator >
		template < typename InputItr, typename >
		Vector< T, Allocator >::Vector(InputItr first, InputItr last, const allocator_type& alloc)
			: base_type(alloc)
		{
			_init(first, last, FXD::STD::is_integral< InputItr >());
		}

		template < typename T, typename Allocator >
		Vector< T, Allocator >::~Vector(void)
		{
			FXD::STD::destruct(this->m_pBegin, this->m_pEnd);
		}

		template < typename T, typename Allocator >
		Vector< T, Allocator >::Vector(const my_type& rhs)
			: base_type(rhs.size(), rhs.m_alloc)
		{
			this->m_pEnd = FXD::STD::uninitialized_copy_ptr(rhs.m_pBegin, rhs.m_pEnd, this->m_pBegin);
		}
		template < typename T, typename Allocator >
		Vector< T, Allocator >::Vector(const my_type& rhs, const allocator_type& alloc)
			: base_type(rhs.size(), alloc)
		{
			this->m_pEnd = FXD::STD::uninitialized_copy_ptr(rhs.m_pBegin, rhs.m_pEnd, this->m_pBegin);
		}
		template < typename T, typename Allocator >
		Vector< T, Allocator >::Vector(my_type&& rhs)
			: base_type(std::move(rhs.m_alloc))
		{
			_swap(rhs);
		}
		template < typename T, typename Allocator >
		Vector< T, Allocator >::Vector(my_type&& rhs, const allocator_type& alloc)
			: base_type(alloc)
		{
			if (this->m_alloc == rhs.m_alloc)
			{
				_swap(rhs);
			}
			else
			{
				my_type temp(std::move(*this));
				temp.swap(rhs);
			}
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::my_type& Vector< T, Allocator >::operator=(const my_type& rhs)
		{
			if (this != &rhs)
			{
				_assign< const_iterator, false >(rhs.begin(), rhs.end(), FXD::STD::false_type());
			}
			return (*this);
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::my_type& Vector< T, Allocator >::operator=(my_type&& rhs)
		{
			if (this != &rhs)
			{
				clear();
				swap(rhs);
			}
			return (*this);
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::my_type& Vector< T, Allocator >::operator=(std::initializer_list< value_type > iList)
		{
			using InputItr = typename std::initializer_list< value_type >::iterator;
			using IC = typename FXD::STD::iterator_traits< InputItr >::iterator_category;
			_assign_from_iterator< InputItr, false >(iList.begin(), iList.end(), IC());
			return (*this);
		}

		// Iterator Support
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::iterator Vector< T, Allocator >::begin(void) NOEXCEPT
		{
			return iterator(this, _first());
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::const_iterator Vector< T, Allocator >::begin(void) const NOEXCEPT
		{
			return const_iterator(const_cast<my_type*>(this), _first());
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::const_iterator Vector< T, Allocator >::cbegin(void) const NOEXCEPT
		{
			return const_iterator(const_cast<my_type*>(this), _first());
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::iterator Vector< T, Allocator >::end(void) NOEXCEPT
		{
			return iterator(this, _last());
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::const_iterator Vector< T, Allocator >::end(void) const NOEXCEPT
		{
			return const_iterator(const_cast<my_type*>(this), _last());
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::const_iterator Vector< T, Allocator >::cend(void) const NOEXCEPT
		{
			return const_iterator(const_cast<my_type*>(this), _last());
		}

		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::reverse_iterator Vector< T, Allocator >::rbegin(void) NOEXCEPT
		{
			return reverse_iterator(iterator(end()));
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::const_reverse_iterator Vector< T, Allocator >::rbegin(void) const NOEXCEPT
		{
			return const_reverse_iterator(const_iterator(end()));
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::const_reverse_iterator Vector< T, Allocator >::crbegin(void) const NOEXCEPT
		{
			return const_reverse_iterator(const_iterator(end()));
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::reverse_iterator Vector< T, Allocator >::rend(void) NOEXCEPT
		{
			return reverse_iterator(iterator(begin()));
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::const_reverse_iterator Vector< T, Allocator >::rend(void) const NOEXCEPT
		{
			return const_reverse_iterator(const_iterator(begin()));
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::const_reverse_iterator Vector< T, Allocator >::crend(void) const NOEXCEPT
		{
			return const_reverse_iterator(const_iterator(begin()));
		}

		// Access
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::reference Vector< T, Allocator >::operator[](size_type nID)
		{
			return *(this->m_pBegin + nID);
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::const_reference Vector< T, Allocator >::operator[](size_type nID) const
		{
			return *(this->m_pBegin + nID);
		}

		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::reference Vector< T, Allocator >::at(size_type nID)
		{
			return *(this->m_pBegin + nID);
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::const_reference Vector< T, Allocator >::at(size_type nID) const
		{
			return *(this->m_pBegin + nID);
		}

		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::reference Vector< T, Allocator >::front(void)
		{
			return *(this->m_pBegin);
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::const_reference Vector< T, Allocator >::front(void) const
		{
			return *(this->m_pBegin);
		}

		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::reference Vector< T, Allocator >::back(void)
		{
			return *(this->m_pEnd - 1);
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::const_reference Vector< T, Allocator >::back(void) const
		{
			return *(this->m_pEnd - 1);
		}

		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::pointer Vector< T, Allocator >::data(void) NOEXCEPT
		{
			return this->m_pBegin;
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::const_pointer Vector< T, Allocator >::data(void) const NOEXCEPT
		{
			return this->m_pBegin;
		}

		// Assign
		template < typename T, typename Allocator >
		void Vector< T, Allocator >::assign(size_type nCount, const value_type& val)
		{
			_assign_values(nCount, val);
		}
		template < typename T, typename Allocator >
		template < typename InputItr, class >
		void Vector< T, Allocator >::assign(InputItr first, InputItr last)
		{
			_assign< InputItr, false >(first, last, FXD::STD::is_integral< InputItr >());
		}
		template < typename T, typename Allocator >
		void Vector< T, Allocator >::assign(std::initializer_list< value_type > iList)
		{
			using InputItr = typename std::initializer_list< value_type >::iterator;
			using IC = typename FXD::STD::iterator_traits< InputItr >::iterator_category;
			_assign_from_iterator< InputItr, false >(iList.begin(), iList.end(), IC());
		}

		// Resize
		template < typename T, typename Allocator >
		void Vector< T, Allocator >::resize(size_type nCount, const value_type& val)
		{
			if (nCount > (size_type)(this->m_pEnd - this->m_pBegin))
			{
				_insert_values_end(nCount - ((size_type)(this->m_pEnd - this->m_pBegin)), val);
			}
			else
			{
				FXD::STD::destruct((this->m_pBegin + nCount), this->m_pEnd);
				this->m_pEnd = (this->m_pBegin + nCount);
			}
		}
		template < typename T, typename Allocator >
		void Vector< T, Allocator >::resize(size_type nCount)
		{
			resize(nCount, value_type());
		}

		template < typename T, typename Allocator >
		void Vector< T, Allocator >::reserve(size_type nCount)
		{
			if (nCount > capacity())
			{
				_grow(nCount);
			}
		}

		template < typename T, typename Allocator >
		inline void Vector< T, Allocator >::shrink_to_fit(void)
		{
			// This is the simplest way to accomplish this, and it is as efficient as any other.
			my_type tmp = my_type(FXD::STD::move_iterator< iterator >(begin()), FXD::STD::move_iterator< iterator >(end()), this->m_alloc);
			_swap(tmp);
		}

		// Insert
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::iterator Vector< T, Allocator >::insert(const_iterator pos, const value_type& val)
		{
			const difference_type nDist = FXD::STD::distance(cbegin(), pos);

			if ((this->m_pEnd == this->m_pCapacity) || (pos != end()))
			{
				_insert_value(pos, val);
			}
			else
			{
				FXD_PLACEMENT_NEW(this->m_pEnd, value_type)(val);
				++this->m_pEnd;
			}
			return (begin() + nDist);
		}

		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::iterator Vector< T, Allocator >::insert(const_iterator pos, value_type&& val)
		{
			return emplace(pos, std::move(val));
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::iterator Vector< T, Allocator >::insert(const_iterator pos, size_type nCount, const value_type& val)
		{
			const difference_type nDist = FXD::STD::distance(cbegin(), pos);

			_insert_values(pos, nCount, val);
			return (begin() + nDist);
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::iterator Vector< T, Allocator >::insert(const_iterator pos, std::initializer_list< value_type > iList)
		{
			const difference_type nDist = FXD::STD::distance(cbegin(), pos);

			_insert(pos, iList.begin(), iList.end(), FXD::STD::false_type());
			return (begin() + nDist);
		}
		template < typename T, typename Allocator >
		template < typename InputItr, class >
		typename Vector< T, Allocator >::iterator Vector< T, Allocator >::insert(const_iterator pos, InputItr first, InputItr last)
		{
			const difference_type nDist = FXD::STD::distance(cbegin(), pos);

			_insert(pos, first, last, FXD::STD::is_integral< InputItr >());
			return (begin() + nDist);
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::reference Vector< T, Allocator >::push_back(void)
		{
			if (this->m_pEnd < this->m_pCapacity)
			{
				FXD_PLACEMENT_NEW(this->m_pEnd++, value_type)(value_type());
			}
			else
			{
				_insert_value(end(), value_type());
			}
			return back();
		}
		template < typename T, typename Allocator >
		void Vector< T, Allocator >::push_back(const value_type& val)
		{
			if (this->m_pEnd < this->m_pCapacity)
			{
				FXD_PLACEMENT_NEW(this->m_pEnd++, value_type)(val);
			}
			else
			{
				_insert_value(end(), val);
			}
		}
		template < typename T, typename Allocator >
		void Vector< T, Allocator >::push_back(value_type&& val)
		{
			if (this->m_pEnd < this->m_pCapacity)
			{
				FXD_PLACEMENT_NEW(this->m_pEnd++, value_type)(std::move(val));
			}
			else
			{
				_insert_value(end(), std::move(val));
			}
		}

		template < typename T, typename Allocator >
		void Vector< T, Allocator >::push_front(void)
		{
			insert(begin(), value_type());
		}
		template < typename T, typename Allocator >
		void Vector< T, Allocator >::push_front(const value_type& val)
		{
			insert(begin(), val);
		}
		template < typename T, typename Allocator >
		void Vector< T, Allocator >::push_front(value_type&& val)
		{
			insert(begin(), std::move(val));
		}

		template < typename T, typename Allocator >
		template < class... Args >
		typename Vector< T, Allocator >::iterator Vector< T, Allocator >::emplace(const_iterator pos, Args&&... args)
		{
			const difference_type nDist = FXD::STD::distance(cbegin(), pos);

			if ((this->m_pEnd == this->m_pCapacity) || (pos != end()))
			{
				_insert_value(pos, std::forward< Args >(args)...);
			}
			else
			{
				FXD_PLACEMENT_NEW(this->m_pEnd, value_type)(std::forward< Args >(args)...);
				++this->m_pEnd;
			}
			return (begin() + nDist);
		}
		template < typename T, typename Allocator >
		template < class... Args >
		typename Vector< T, Allocator >::reference Vector< T, Allocator >::emplace_back(Args&&... args)
		{
			if (this->m_pEnd < this->m_pCapacity)
			{
				FXD_PLACEMENT_NEW(this->m_pEnd, value_type)(std::forward< Args >(args)...);
				++this->m_pEnd;
			}
			else
			{
				_insert_value_end(std::forward< Args >(args)...);
			}
			return back();
		}

		// Erase
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::iterator Vector< T, Allocator >::erase(const_iterator pos)
		{
			pointer pDstPos = const_cast< value_type* >(pos.m_pType);
			if ((pos + 1) < end())
			{
				FXD::STD::move((pDstPos + 1), this->m_pEnd, pDstPos);
			}
			--this->m_pEnd;
			FXD::STD::destroy_at(this->m_pEnd);
			return iterator(this, pDstPos);
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::iterator Vector< T, Allocator >::erase(const_iterator first, const_iterator last)
		{
			if (first != last)
			{
				pointer const pPos = const_cast< value_type* >(FXD::STD::move(const_cast< value_type* >(last.m_pType), const_cast< value_type* >(this->m_pEnd), const_cast< value_type* >(first.m_pType)));
				FXD::STD::destruct(pPos, this->m_pEnd);
				this->m_pEnd -= (last - first);
			}
			return iterator(this, const_cast< value_type* >(first.m_pType));
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::iterator Vector< T, Allocator >::erase_unsorted(const_iterator pos)
		{
			pointer pDstPosition = const_cast< value_type* >(pos.m_pType);
			(*pDstPosition) = std::move(*(this->m_pEnd - 1));

			--this->m_pEnd;
			FXD::STD::destroy_at(this->m_pEnd);
			return iterator(this, pDstPosition);
		}

		// Erase
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::reverse_iterator Vector< T, Allocator >::erase(const_reverse_iterator pos)
		{
			return reverse_iterator(erase((++pos).base()));
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::reverse_iterator Vector< T, Allocator >::erase(const_reverse_iterator first, const_reverse_iterator last)
		{
			return reverse_iterator(erase(last.base(), first.base()));
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::reverse_iterator Vector< T, Allocator >::erase_unsorted(const_reverse_iterator pos)
		{
			return reverse_iterator(erase_unsorted((++pos).base()));
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::size_type Vector< T, Allocator >::find_erase(const value_type& key)
		{
			const_iterator itr = find(key);
			if (itr != end())
			{
				erase(itr);
				return 1;
			}
			return 0;
		}

		template < typename T, typename Allocator >
		void Vector< T, Allocator >::pop_front(void)
		{
			erase(begin());
		}
		template < typename T, typename Allocator >
		void Vector< T, Allocator >::pop_back(void)
		{
			erase(rbegin());
		}
		template < typename T, typename Allocator >
		void Vector< T, Allocator >::clear(void) NOEXCEPT
		{
			FXD::STD::destruct(this->m_pBegin, this->m_pEnd);
			this->m_pEnd = this->m_pBegin;
		}

		// Adjust
		template < typename T, typename Allocator >
		bool Vector< T, Allocator >::bring_to_front(const value_type& key)
		{
			for (auto itr = begin(); itr != end(); ++itr)
			{
				if ((*itr) == key)
				{
					auto itrMove = std::move(*itr);
					erase(itr);
					push_front(itrMove);
					return true;
				}
			}
			return false;
		}
		template < typename T, typename Allocator >
		bool Vector< T, Allocator >::bring_to_back(const value_type& key)
		{
			for (auto itr = begin(); itr != end(); ++itr)
			{
				if ((*itr) == key)
				{
					if (itr != end())
					{
						auto itrMove = std::move(*itr);
						erase(itr);
						push_back(itrMove);
					}
					return true;
				}
			}
			return false;
		}

		template < typename T, typename Allocator >
		void Vector< T, Allocator >::sort(void)
		{
			sort(FXD::STD::less< T >());
		}
		template < typename T, typename Allocator >
		template < typename Pr >
		void Vector< T, Allocator >::sort(Pr cmp)
		{
			FXD::STD::sort(begin(), end(), cmp, size());
		}

		template < typename T, typename Allocator >
		void Vector< T, Allocator >::swap(my_type& rhs)
		{
			if (this->m_alloc == rhs.m_alloc)
			{
				_swap(rhs);
			}
			else // else swap the contents.
			{
				const my_type temp(*this);
				(*this) = rhs;
				rhs = temp;
			}
		}

		// Searches
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::const_iterator Vector< T, Allocator >::find(const value_type& key) const
		{
			return FXD::STD::find(begin(), end(), key);
		}
		template < typename T, typename Allocator >
		bool Vector< T, Allocator >::contains(const value_type& key) const
		{
			return (find(key) != end());
		}
		template < typename T, typename Allocator >
		bool Vector< T, Allocator >::empty(void) const NOEXCEPT
		{
			return (this->m_pBegin == this->m_pEnd);
		}
		template < typename T, typename Allocator >
		bool Vector< T, Allocator >::not_empty(void) const NOEXCEPT
		{
			return (this->m_pBegin != this->m_pEnd);
		}
		template < typename T, typename Allocator >
		inline bool Vector< T, Allocator >::validate(void) const NOEXCEPT
		{
			if (this->m_pEnd < this->m_pBegin)
			{
				return false;
			}
			if (this->m_pCapacity < this->m_pEnd)
			{
				return false;
			}
			return true;
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::size_type Vector< T, Allocator >::size(void) const NOEXCEPT
		{
			return (size_type)(this->m_pEnd - this->m_pBegin);
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::size_type Vector< T, Allocator >::capacity(void) const NOEXCEPT
		{
			return (size_type)(this->m_pCapacity - this->m_pBegin);
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::size_type Vector< T, Allocator >::count(const value_type& key) const NOEXCEPT
		{
			return (size_type)FXD::STD::count(begin(), end(), key);
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::allocator_type Vector< T, Allocator >::allocator(void) const NOEXCEPT
		{
			return this->m_alloc;
		}

		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::pointer Vector< T, Allocator >::_first(void) NOEXCEPT
		{
			return this->m_pBegin;
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::const_pointer Vector< T, Allocator >::_first(void) const NOEXCEPT
		{
			return this->m_pBegin;
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::pointer Vector< T, Allocator >::_last(void) NOEXCEPT
		{
			return this->m_pEnd;
		}
		template < typename T, typename Allocator >
		typename Vector< T, Allocator >::const_pointer Vector< T, Allocator >::_last(void) const NOEXCEPT
		{
			return this->m_pEnd;
		}

		template < typename T, typename Allocator >
		template < typename Integer >
		void Vector< T, Allocator >::_init(Integer nDist, Integer val, FXD::STD::true_type)
		{
			this->m_pBegin = _allocate(nDist);
			this->m_pEnd = (this->m_pBegin + nDist);
			this->m_pCapacity = this->m_pEnd;

			FXD::STD::uninitialized_fill_ptr_n< value_type, Integer >(this->m_pBegin, nDist, val);
		}
		template < typename T, typename Allocator >
		template < typename InputItr >
		void Vector< T, Allocator >::_init(InputItr first, InputItr last, FXD::STD::false_type)
		{
			using IC = typename FXD::STD::iterator_traits< InputItr >::iterator_category;
			_init_iterator(first, last, IC());
		}

		template < typename T, typename Allocator >
		template < typename InputItr >
		void Vector< T, Allocator >::_init_iterator(InputItr first, InputItr last, FXD::STD::input_iterator_tag)
		{
			for (; first != last; ++first)
			{
				push_back((*first));
			}
		}
		template < typename T, typename Allocator >
		template < typename ForwardItr >
		void Vector< T, Allocator >::_init_iterator(ForwardItr first, ForwardItr last, FXD::STD::forward_iterator_tag)
		{
			const size_type nDist = (size_type)FXD::STD::distance(first, last);
			this->m_pBegin = this->_allocate(nDist);
			this->m_pEnd = (this->m_pBegin + nDist);
			this->m_pCapacity = this->m_pEnd;

			FXD::STD::uninitialized_copy_ptr(first, last, this->m_pBegin);
		}

		template < typename T, typename Allocator >
		template < typename Integer, bool bMove >
		void Vector< T, Allocator >::_assign(Integer nCount, Integer val, FXD::STD::true_type)
		{
			_assign_values(static_cast<size_type>(nCount), static_cast<value_type>(val));
		}
		template < typename T, typename Allocator >
		template < typename InputItr, bool bMove >
		void Vector< T, Allocator >::_assign(InputItr first, InputItr last, FXD::STD::false_type)
		{
			using IC = typename FXD::STD::iterator_traits< InputItr >::iterator_category;
			_assign_from_iterator< InputItr, bMove >(first, last, IC());
		}

		template < typename T, typename Allocator >
		template < typename InputItr, bool bMove >
		void Vector< T, Allocator >::_assign_from_iterator(InputItr first, InputItr last, FXD::STD::input_iterator_tag)
		{
			iterator pos = begin();
			while ((pos != end()) && (first != last))
			{
				(*pos) = (*first);
				++first;
				++pos;
			}
			if (first == last)
			{
				erase(pos, end());
			}
			else
			{
				insert(end(), first, last);
			}
		}
		template < typename T, typename Allocator >
		template < typename RandomAccessItr, bool bMove >
		void Vector< T, Allocator >::_assign_from_iterator(RandomAccessItr first, RandomAccessItr last, FXD::STD::random_access_iterator_tag)
		{
			const size_type nDist = (size_type)FXD::STD::distance(first, last);
			if (nDist > FXD::STD::distance(this->m_pBegin, this->m_pCapacity)) // If nDist > capacity ...
			{
				pointer const pNewData = _realloc(nDist, first, last, should_copy_tag());
				FXD::STD::destruct(this->m_pBegin, this->m_pEnd);
				this->_free(capacity());

				this->m_pBegin = pNewData;
				this->m_pEnd = (this->m_pBegin + nDist);
				this->m_pCapacity = this->m_pEnd;
			}
			else if (nDist <= FXD::STD::distance(this->m_pBegin, this->m_pEnd)) // If nDist <= size ...
			{
				pointer const pNewEnd = FXD::STD::copy(first, last, this->m_pBegin); // Since we are copying to m_pBegin, we don't have to worry about needing copy_backward or a memmove-like copy (as opposed to memcpy-like copy).
				FXD::STD::destruct(pNewEnd, this->m_pEnd);
				this->m_pEnd = pNewEnd;
			}
			else // else size < nDist <= capacity
			{
				RandomAccessItr pos = (first + FXD::STD::distance(this->m_pBegin, this->m_pEnd));
				FXD::STD::copy(first, pos, this->m_pBegin);
				this->m_pEnd = FXD::STD::uninitialized_copy_ptr(pos, last, this->m_pEnd);
			}
		}

		template < typename T, typename Allocator >
		void Vector< T, Allocator >::_assign_values(size_type nCount, const value_type& val)
		{
			if (nCount > FXD::STD::distance(this->m_pBegin, this->m_pCapacity)) // If nDist > capacity ...
			{
				my_type tmp(nCount, val, this->m_alloc); // We have little choice but to reallocate with new memory.
				swap(tmp);
			}
			else if (nCount > FXD::STD::distance(this->m_pBegin, this->m_pEnd)) // If nCount > size ...
			{
				FXD::STD::fill(this->m_pBegin, this->m_pEnd, val);
				FXD::STD::uninitialized_fill_ptr_n(this->m_pEnd, (nCount - size_type(this->m_pEnd - this->m_pBegin)), val);
				this->m_pEnd += (nCount - FXD::STD::distance(this->m_pBegin, this->m_pEnd));
			}
			else // else 0 <= nCount <= size
			{
				FXD::STD::fill_n(this->m_pBegin, nCount, val);
				erase(const_iterator(this, (this->m_pBegin + nCount)), const_iterator(this, this->m_pEnd));
			}
		}

		template < typename T, typename Allocator >
		template < typename Integer >
		void Vector< T, Allocator >::_insert(const_iterator pos, Integer nCount, Integer val, FXD::STD::true_type)
		{
			_insert_values(pos, static_cast< size_type >(nCount), static_cast< value_type >(val));
		}
		template < typename T, typename Allocator >
		template < typename InputItr >
		void Vector< T, Allocator >::_insert(const_iterator pos, InputItr first, InputItr last, FXD::STD::false_type)
		{
			using IC = typename FXD::STD::iterator_traits< InputItr >::iterator_category;
			_insert_from_iterator(pos, first, last, IC());
		}

		template < typename T, typename Allocator >
		template < typename InputItr >
		void Vector< T, Allocator >::_insert_from_iterator(const_iterator pos, InputItr first, InputItr last, FXD::STD::input_iterator_tag)
		{
			for (; first != last; ++first, ++pos)
			{
				pos = insert(pos, *first);
			}
		}
		template < typename T, typename Allocator >
		template < typename BidirItr >
		void Vector< T, Allocator >::_insert_from_iterator(const_iterator pos, BidirItr first, BidirItr last, FXD::STD::bidirectional_iterator_tag)
		{
			pointer pDstPos = const_cast< value_type* >(pos.m_pType);
			if (first != last)
			{
				const size_type nDist = (size_type)FXD::STD::distance(first, last);

				if (nDist <= FXD::STD::distance(this->m_pEnd, this->m_pCapacity)) // If n fits within the existing capacity...
				{
					const size_type nExtra = FXD::STD::distance(pDstPos, this->m_pEnd);

					if (nDist < nExtra)
					{
						//FXD::STD::uninitialized_move_ptr((this->m_pEnd - nDist), this->m_pEnd, this->m_pEnd);
						//FXD::STD::copy_backward(pDstPos, (this->m_pEnd - nDist), this->m_pEnd);
						FXD::STD::uninitialized_move_ptr(this->m_pEnd - nDist, this->m_pEnd, this->m_pEnd);
						FXD::STD::move_backward(pDstPos, this->m_pEnd - nDist, this->m_pEnd); // We need move_backward because of potential overlap issues.
						FXD::STD::copy(first, last, pDstPos);
					}
					else
					{
						BidirItr iTemp = first;
						FXD::STD::advance(iTemp, nExtra);
						FXD::STD::uninitialized_copy_ptr(iTemp, last, this->m_pEnd);
						FXD::STD::uninitialized_move_ptr(pDstPos, this->m_pEnd, this->m_pEnd + nDist - nExtra);
						FXD::STD::copy_backward(first, iTemp, pDstPos + nExtra);
					}
					this->m_pEnd += nDist;
				}
				else // else we need to expand our capacity.
				{
					const size_type nPrevSize = FXD::STD::distance(this->m_pBegin, this->m_pEnd);
					const size_type nGrowSize = _get_new_capacity(nPrevSize);
					const size_type nNewSize = (nGrowSize > (nPrevSize + nDist)) ? nGrowSize : (nPrevSize + nDist);
					pointer const pNewData = _allocate(nNewSize);

					pointer pNewEnd = FXD::STD::uninitialized_move_ptr(this->m_pBegin, pDstPos, pNewData);
					pNewEnd = FXD::STD::uninitialized_copy_ptr(first, last, pNewEnd);
					pNewEnd = FXD::STD::uninitialized_move_ptr(pDstPos, this->m_pEnd, pNewEnd);

					FXD::STD::destruct(this->m_pBegin, this->m_pEnd);
					_free(capacity());

					this->m_pBegin = pNewData;
					this->m_pEnd = pNewEnd;
					this->m_pCapacity = (pNewData + nNewSize);
				}
			}
		}

		template < typename T, typename Allocator >
		template < typename... Args >
		void Vector< T, Allocator >::_insert_value(const_iterator pos, Args&&... args)
		{
			pointer pDstPos = const_cast< value_type* >(pos.m_pType);

			if (this->m_pEnd != this->m_pCapacity) // If size < capacity ...
			{
				PRINT_COND_ASSERT((pos < end()), "Container: Iterator is invalid");

				value_type val(std::forward< Args >(args)...);
				FXD_PLACEMENT_NEW(static_cast< void* >(this->m_pEnd), value_type)(std::move(*(this->m_pEnd - 1)));

				FXD::STD::move_backward(pDstPos, (this->m_pEnd - 1), this->m_pEnd);
				FXD::STD::destruct(pDstPos);
				FXD_PLACEMENT_NEW(static_cast< void* >(pDstPos), value_type)(std::move(val));
				++this->m_pEnd;
			}
			else // else (size == capacity)
			{
				const size_type nPosSize = size_type(pDstPos - this->m_pBegin); // Index of the insertion pos.
				const size_type nPrevSize = size_type(this->m_pEnd - this->m_pBegin);
				const size_type nNewSize = this->_get_new_capacity(nPrevSize);
				pointer const pNewData = this->_allocate(nNewSize);

				FXD_PLACEMENT_NEW((void*)(pNewData + nPosSize), value_type)(std::forward< Args >(args)...);
				pointer pNewEnd = FXD::STD::uninitialized_move_ptr(this->m_pBegin, pDstPos, pNewData);
				pNewEnd = FXD::STD::uninitialized_move_ptr(pDstPos, this->m_pEnd, ++pNewEnd);

				FXD::STD::destruct(this->m_pBegin, this->m_pEnd);
				this->_free(capacity());

				this->m_pBegin = pNewData;
				this->m_pEnd = pNewEnd;
				this->m_pCapacity = (pNewData + nNewSize);
			}
		}

		template < typename T, typename Allocator >
		template < typename... Args >
		void Vector< T, Allocator >::_insert_value_end(Args&&... args)
		{
			const size_type nPrevSize = size_type(this->m_pEnd - this->m_pBegin);
			const size_type nNewSize = this->_get_new_capacity(nPrevSize);
			pointer const pNewData = this->_allocate(nNewSize);

			pointer pNewEnd = FXD::STD::uninitialized_move_ptr(this->m_pBegin, this->m_pEnd, pNewData);
			FXD_PLACEMENT_NEW((void*)pNewEnd, value_type)(std::forward< Args >(args)...);
			pNewEnd++;

			FXD::STD::destruct(this->m_pBegin, this->m_pEnd);
			this->_free(capacity());

			this->m_pBegin = pNewData;
			this->m_pEnd = pNewEnd;
			this->m_pCapacity = (pNewData + nNewSize);
		}

		template < typename T, typename Allocator >
		void Vector< T, Allocator >::_insert_values(const_iterator pos, size_type nCount, const value_type& val)
		{
			pointer pDstPosition = const_cast< value_type* >(pos.m_pType);

			if (nCount <= size_type(this->m_pCapacity - this->m_pEnd)) // If nCount is <= capacity...
			{
				if (nCount > 0)
				{
					const value_type tmp = val;
					const size_type nExtra = size_type(this->m_pEnd - pDstPosition);
					if (nCount < nExtra)
					{
						FXD::STD::uninitialized_move_ptr((this->m_pEnd - nCount), this->m_pEnd, this->m_pEnd);
						FXD::STD::move_backward(pDstPosition, (this->m_pEnd - nCount), this->m_pEnd);
						FXD::STD::fill(pDstPosition, (pDstPosition + nCount), tmp);
					}
					else
					{
						FXD::STD::uninitialized_fill_ptr_n(this->m_pEnd, (nCount - nExtra), tmp);
						FXD::STD::uninitialized_move_ptr(pDstPosition, this->m_pEnd, this->m_pEnd + nCount - nExtra);
						FXD::STD::fill(pDstPosition, this->m_pEnd, tmp);
					}
					this->m_pEnd += nCount;
				}
			}
			else // else nCount > capacity
			{
				const size_type nPrevSize = size_type(this->m_pEnd - this->m_pBegin);
				const size_type nGrowSize = _get_new_capacity(nPrevSize);
				const size_type nNewSize = nGrowSize > (nPrevSize + nCount) ? nGrowSize : (nPrevSize + nCount);
				pointer const pNewData = _allocate(nNewSize);

				pointer pNewEnd = FXD::STD::uninitialized_move_ptr(this->m_pBegin, pDstPosition, pNewData);
				FXD::STD::uninitialized_fill_ptr_n(pNewEnd, nCount, val);
				pNewEnd = FXD::STD::uninitialized_move_ptr(pDstPosition, this->m_pEnd, (pNewEnd + nCount));

				FXD::STD::destruct(this->m_pBegin, this->m_pEnd);
				_free(capacity());

				this->m_pBegin = pNewData;
				this->m_pEnd = pNewEnd;
				this->m_pCapacity = pNewData + nNewSize;
			}
		}

		template < typename T, typename Allocator >
		void Vector< T, Allocator >::_insert_values_end(size_type nCount)
		{
			_insert_values_end(nCount, value_type());
		}
		template < typename T, typename Allocator >
		void Vector< T, Allocator >::_insert_values_end(size_type nCount, const value_type& val)
		{
			if (nCount > size_type(this->m_pCapacity - this->m_pEnd))
			{
				const size_type nPrevSize = size_type(this->m_pEnd - this->m_pBegin);
				const size_type nGrowSize = this->_get_new_capacity(nPrevSize);
				const size_type nNewSize = FXD::STD::Max(nGrowSize, nPrevSize + nCount);
				pointer const pNewData = this->_allocate(nNewSize);

				pointer pNewEnd = FXD::STD::uninitialized_move_ptr(this->m_pBegin, this->m_pEnd, pNewData);
				FXD::STD::uninitialized_fill_ptr_n(pNewEnd, nCount, val);
				pNewEnd += nCount;

				FXD::STD::destruct(this->m_pBegin, this->m_pEnd);
				this->_free(capacity());

				this->m_pBegin = pNewData;
				this->m_pEnd = pNewEnd;
				this->m_pCapacity = (pNewData + nNewSize);
			}
			else
			{
				FXD::STD::uninitialized_fill_ptr_n(this->m_pEnd, nCount, val);
				this->m_pEnd += nCount;
			}
		}

		template < typename T, typename Allocator >
		void Vector< T, Allocator >::_grow(size_type nCount)
		{
			pointer const pNewData = _allocate(nCount);

			pointer pNewEnd = FXD::STD::uninitialized_move_ptr(this->m_pBegin, this->m_pEnd, pNewData);

			FXD::STD::destruct(this->m_pBegin, this->m_pEnd);
			_free(capacity());

			this->m_pBegin = pNewData;
			this->m_pEnd = pNewEnd;
			this->m_pCapacity = (pNewData + nCount);
		}

		template < typename T, typename Allocator >
		template < typename ForwardItr >
		typename Vector< T, Allocator >::pointer Vector< T, Allocator >::_realloc(size_type nCount, ForwardItr first, ForwardItr last, should_copy_tag)
		{
			T* const pData = this->_allocate(nCount);						// pData is of type T* but is not constructed. 
			FXD::STD::uninitialized_copy_ptr(first, last, pData);	// copy-constructs p from [first,last).
			return pData;
		}

		template < typename T, typename Allocator >
		template < typename ForwardItr >
		typename Vector< T, Allocator >::pointer Vector< T, Allocator >::_realloc(size_type nCount, ForwardItr first, ForwardItr last, should_move_tag)
		{
			T* const pData = _allocate(nCount);						// pData is of type T* but is not constructed. 
			FXD::STD::uninitialized_move_ptr(first, last, pData);	// move-constructs p from [first,last).
			return pData;
		}

		template < typename T, typename Allocator >
		void Vector< T, Allocator >::_swap(my_type& rhs)
		{
			FXD::STD::swap(this->m_pBegin, rhs.m_pBegin);
			FXD::STD::swap(this->m_pEnd, rhs.m_pEnd);
			FXD::STD::swap(this->m_pCapacity, rhs.m_pCapacity);
			//FXD::STD::swap(m_alloc, rhs.m_alloc);
		}

		// Non-member comparison functions
		// Equality
		template < typename T, typename Allocator >
		bool operator==(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)
		{
			return ((lhs.size() == rhs.size()) && (FXD::STD::equal(lhs.begin(), lhs.end(), rhs.begin())));
		}
		// Inequality
		template < typename T, typename Allocator >
		bool operator!=(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)
		{
			return ((lhs.size() != rhs.size()) || (FXD::STD::equal(lhs.begin(), lhs.end(), rhs.begin()) == false));
		}
		// Less than
		template < typename T, typename Allocator >
		bool operator<(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)
		{
			return FXD::STD::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
		}
		// Greater than
		template < typename T, typename Allocator >
		bool operator>(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)
		{
			return (rhs < lhs);
		}
		// Less than or equal to
		template < typename T, typename Allocator >
		bool operator<=(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)
		{
			return !(rhs < lhs);
		}
		// Greater than or equal to
		template < typename T, typename Allocator >
		bool operator>=(const Vector< T, Allocator >& lhs, const Vector< T, Allocator >& rhs)
		{
			return !(lhs < rhs);
		}

		// Inserters and extractors
		template < typename T, typename Allocator >
		Core::IStreamOut& operator<<(Core::IStreamOut& ostr, const Container::Vector< T, Allocator >& cont)
		{
			fxd_for(auto& i, cont)
			{
				ostr << " " << i;
			}
			return ostr;
		}
	} //namespace Container

	namespace STD
	{
		template < typename T, typename Allocator >
		inline void swap(FXD::Container::Vector< T, Allocator >& lhs, FXD::Container::Vector< T, Allocator >& rhs)
		{
			lhs.swap(rhs);
		}

		template < typename T, typename Allocator, typename Pred >
		void erase_if(FXD::Container::Vector< T, Allocator >& container, Pred pred)
		{
			container.erase(FXD::STD::remove_if(container.begin(), container.end(), pred), container.end());
		}

		template < typename T, typename Allocator, typename U >
		void erase(FXD::Container::Vector< T, Allocator >& container, const U& val)
		{
			container.erase(FXD::STD::remove(container.begin(), container.end(), val), container.end());
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_VECTOR_H