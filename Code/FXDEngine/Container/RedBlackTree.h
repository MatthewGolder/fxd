// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_REDBLACKTREE_H
#define FXDENGINE_CONTAINER_REDBLACKTREE_H

#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/Core/TypeTraits.h"
#include "FXDEngine/Core/Utility.h"
#include "FXDEngine/Container/Iterator.h"
#include "FXDEngine/Memory/Allocator/Aligned.h"
#include "FXDEngine/Memory/Memory.h"

#include <initializer_list>

namespace FXD
{
	namespace Container
	{
		namespace Internal
		{
			enum class E_RBNodeColor : FXD::U8
			{
				Red,
				Black
			};
			enum class E_RBNodeSide : FXD::U8
			{
				Left,
				Right
			};

			// ------
			// _RBTreeNodeBase
			// -
			// ------
			class _RBTreeNodeBase
			{
			public:
				Internal::_RBTreeNodeBase* m_pNodeRight;
				Internal::_RBTreeNodeBase* m_pNodeLeft;
				Internal::_RBTreeNodeBase* m_pNodeParent;
				Internal::E_RBNodeColor m_eColour;
			};

			// ------
			// _RBTreeNode
			// -
			// ------
			template < typename Value >
			class _RBTreeNode : public Internal::_RBTreeNodeBase
			{
			public:
				NO_COPY(_RBTreeNode);

			public:
				Value m_val;
			};

			extern Internal::_RBTreeNodeBase* RBTreeRotateLeft(Internal::_RBTreeNodeBase* pNode, Internal::_RBTreeNodeBase* pNodeRoot);
			extern Internal::_RBTreeNodeBase* RBTreeRotateRight(Internal::_RBTreeNodeBase* pNode, Internal::_RBTreeNodeBase* pNodeRoot);
			extern Internal::_RBTreeNodeBase* RBTreeNext(const Internal::_RBTreeNodeBase* pNode);
			extern Internal::_RBTreeNodeBase* RBTreePrev(const Internal::_RBTreeNodeBase* pNode);
			extern Internal::_RBTreeNodeBase* RBTreeGetMinChild(const Internal::_RBTreeNodeBase* pNode);
			extern Internal::_RBTreeNodeBase* RBTreeGetMaxChild(const Internal::_RBTreeNodeBase* pNode);
			extern FXD::U32 RBTreeGetBlackCount(const Internal::_RBTreeNodeBase* pNodeTop, const Internal::_RBTreeNodeBase* pNodeBottom);
			extern void RBTreeInsert(Internal::_RBTreeNodeBase* pNode, Internal::_RBTreeNodeBase* pNodeParent, Internal::_RBTreeNodeBase* pNodeAnchor, Internal::E_RBNodeSide eSide);
			extern void RBTreeErase(Internal::_RBTreeNodeBase* pNode, Internal::_RBTreeNodeBase* pNodeAnchor);
		} //namespace Internal

		// ------
		// _rbtree_iterator
		// -
		// ------
		template < typename T, typename Pointer, typename Reference >
		class _rbtree_iterator
		{
		public:
			using my_type = _rbtree_iterator< T, Pointer, Reference >;
			using node_type = Internal::_RBTreeNode< T >;
			using value_type = T;
			using pointer = Pointer;
			using reference = Reference;
			using size_type = FXD::U32;
			using difference_type = std::ptrdiff_t;
			using iterator_category = FXD::STD::bidirectional_iterator_tag;
			using iterator = _rbtree_iterator< T, T*, T& >;
			using const_iterator = _rbtree_iterator< T, const T*, const T& >;

		public:
			_rbtree_iterator(void);
			_rbtree_iterator(const node_type* pNode);
			_rbtree_iterator(const iterator& rhs);

			my_type& operator++();
			my_type operator++(int);

			my_type& operator--();
			my_type operator--(int);

			reference operator*(void) const;
			pointer operator->(void) const;

		protected:
			// Non-member comparison functions
			// Equality
			template < typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB >
			friend bool operator==(const _rbtree_iterator< T2, PtrA, RefA >& lhs, const _rbtree_iterator< T2, PtrB, RefB >& rhs) NOEXCEPT;
			template < typename T2, typename PtrA, typename RefA >
			friend bool operator==(const _rbtree_iterator< T2, PtrA, RefA >& lhs, const _rbtree_iterator< T2, PtrA, RefA >& rhs) NOEXCEPT;
			// Inequality
			template < typename T2, typename PtrA, typename RefA, typename PtrB, typename RefB >
			friend bool operator!=(const _rbtree_iterator< T2, PtrA, RefA >& lhs, const _rbtree_iterator< T2, PtrB, RefB >& rhs) NOEXCEPT;
			template < typename T2, typename PtrA, typename RefA >
			friend bool operator!=(const _rbtree_iterator< T2, PtrA, RefA >& lhs, const _rbtree_iterator< T2, PtrA, RefA >& rhs) NOEXCEPT;

		public:
			node_type* m_pNode;
		};

		// ------
		// _rbtree_iterator
		// -
		// ------
		template < typename T, typename Pointer, typename Reference >
		_rbtree_iterator< T, Pointer, Reference >::_rbtree_iterator(void)
			: m_pNode(nullptr)
		{}

		template < typename T, typename Pointer, typename Reference >
		_rbtree_iterator< T, Pointer, Reference >::_rbtree_iterator(const node_type* pNode)
			: m_pNode(static_cast< node_type* >(const_cast< node_type* >(pNode)))
		{}

		template < typename T, typename Pointer, typename Reference >
		_rbtree_iterator< T, Pointer, Reference >::_rbtree_iterator(const iterator& rhs)
			: m_pNode(rhs.m_pNode)
		{}

		template < typename T, typename Pointer, typename Reference >
		typename _rbtree_iterator< T, Pointer, Reference >::my_type& _rbtree_iterator< T, Pointer, Reference >::operator++()
		{
			m_pNode = static_cast< node_type* >(RBTreeNext(m_pNode));
			return (*this);
		}
		template < typename T, typename Pointer, typename Reference >
		typename _rbtree_iterator< T, Pointer, Reference >::my_type _rbtree_iterator< T, Pointer, Reference >::operator++(int)
		{
			const my_type tmp(*this);
			operator++();
			return (tmp);
		}

		template < typename T, typename Pointer, typename Reference >
		typename _rbtree_iterator< T, Pointer, Reference >::my_type& _rbtree_iterator< T, Pointer, Reference >::operator--()
		{
			m_pNode = static_cast< node_type* >(RBTreePrev(m_pNode));
			return (*this);
		}
		template < typename T, typename Pointer, typename Reference >
		typename _rbtree_iterator< T, Pointer, Reference >::my_type _rbtree_iterator< T, Pointer, Reference >::operator--(int)
		{
			const my_type tmp(*this);
			operator--();
			return (tmp);
		}

		template < typename T, typename Pointer, typename Reference >
		typename _rbtree_iterator< T, Pointer, Reference >::reference _rbtree_iterator< T, Pointer, Reference >::operator*(void) const
		{
			return m_pNode->m_val;
		}
		template < typename T, typename Pointer, typename Reference >
		typename _rbtree_iterator< T, Pointer, Reference >::pointer _rbtree_iterator< T, Pointer, Reference >::operator->(void) const
		{
			return &(operator*());
		}

		// Non-member comparison functions
		// Equality
		template < typename T, typename PtrA, typename RefA, typename PtrB, typename RefB >
		inline bool operator==(const _rbtree_iterator< T, PtrA, RefA >& lhs, const _rbtree_iterator< T, PtrB, RefB >& rhs) NOEXCEPT
		{
			return (lhs.m_pNode == rhs.m_pNode);
		}
		template < typename T, typename PtrA, typename RefA >
		inline bool operator==(const _rbtree_iterator< T, PtrA, RefA >& lhs, const _rbtree_iterator< T, PtrA, RefA >& rhs) NOEXCEPT
		{
			return (lhs.m_pNode == rhs.m_pNode);
		}
		// Inequality
		template < typename T, typename PtrA, typename RefA, typename PtrB, typename RefB >
		inline bool operator!=(const _rbtree_iterator< T, PtrA, RefA >& lhs, const _rbtree_iterator< T, PtrB, RefB >& rhs) NOEXCEPT
		{
			return (lhs.m_pNode != rhs.m_pNode);
		}
		template < typename T, typename PtrA, typename RefA >
		inline bool operator!=(const _rbtree_iterator< T, PtrA, RefA >& lhs, const _rbtree_iterator< T, PtrA, RefA >& rhs) NOEXCEPT
		{
			return (lhs.m_pNode != rhs.m_pNode);
		}

		namespace Internal
		{
			// ------
			// RBBase
			// - This specialization is used for 'Set'.
			// ------
			template < typename Key, typename Value, typename Compare, typename ExtractKey, bool bUniqueKeys, typename RBTree >
			struct _RBBase
			{
			public:
				using extract_key = ExtractKey;

			public:
				_RBBase(void)
					: m_compare()
				{}

				_RBBase(const Compare& compare)
					: m_compare(compare)
				{}

			public:
				Compare m_compare;
			};

			// ------
			// _RBBase
			// - This specialization is used for 'Multiset'.
			// ------
			template < typename Key, typename Value, typename Compare, typename ExtractKey, typename RBTree >
			struct _RBBase< Key, Value, Compare, ExtractKey, false, RBTree >
			{
			public:
				using extract_key = ExtractKey;

			public:
				_RBBase(void)
					: m_compare()
				{}

				_RBBase(const Compare& compare)
					: m_compare(compare)
				{}

			public:
				Compare m_compare;
			};

			// ------
			// _RBBase
			// - This specialization is used for 'Map'.
			// ------
			template < typename Key, typename Pair, typename Compare, typename RBTree >
			struct _RBBase< Key, Pair, Compare, FXD::STD::UseFirst< Pair >, true, RBTree >
			{
			public:
				using extract_key = FXD::STD::UseFirst< Pair >;

			public:
				_RBBase(void)
					: m_compare()
				{}

				_RBBase(const Compare& compare)
					: m_compare(compare)
				{}

			public:
				Compare m_compare;
			};

			// ------
			// _RBBase
			// - This specialization is used for 'multimap'.
			// ------
			template < typename Key, typename Pair, typename Compare, typename RBTree >
			struct _RBBase< Key, Pair, Compare, FXD::STD::UseFirst< Pair >, false, RBTree >
			{
			public:
				using extract_key = FXD::STD::UseFirst< Pair >;

			public:
				_RBBase(void)
					: m_compare()
				{}

				_RBBase(const Compare& compare)
					: m_compare(compare)
				{}

			public:
				Compare m_compare;
			};
		} //namespace Internal

		// ------
		// RBTree
		// - 
		// ------
		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		class RBTree : public Internal::_RBBase< Key, Value, Compare, ExtractKey, bUniqueKeys, RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys > >
		{
		public:
			using size_type = FXD::U32;
			using key_type = Key;
			using value_type = Value;
			using node_type = Internal::_RBTreeNode< value_type >;
			using reference = value_type& ;
			using const_reference = const value_type&;
			using pointer = value_type* ;
			using const_pointer = const value_type*;
			using difference_type = std::ptrdiff_t;
			using iterator = _rbtree_iterator< value_type, value_type*, value_type& >;
			using const_iterator = _rbtree_iterator< value_type, const value_type*, const value_type& >;
			using reverse_iterator = FXD::STD::reverse_iterator< iterator >;
			using const_reverse_iterator = FXD::STD::reverse_iterator< const_iterator >;
			using allocator_type = Allocator;
			using key_compare = Compare;
			using insert_return_type = typename FXD::STD::conditional< bUniqueKeys, Core::Pair< iterator, bool >, iterator >::type;
			using my_type = RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >;
			using base_type = Internal::_RBBase< Key, Value, Compare, ExtractKey, bUniqueKeys, my_type >;
			using has_unique_keys_type = FXD::STD::integral_constant< bool, bUniqueKeys >;
			using extract_key = typename base_type::extract_key;

		public:
			RBTree(void);
			RBTree(const allocator_type& allocator);
			RBTree(const key_compare& compare, const allocator_type& allocator = allocator_type());
			template < typename InputItr, class = typename FXD::STD::enable_if< FXD::Internal::_Is_iterator< InputItr >::value, void >::type >
			RBTree(InputItr first, InputItr last, const key_compare& compare, const allocator_type& allocator = allocator_type());

			~RBTree(void);

			RBTree(const my_type& rhs);
			RBTree(my_type&& rhs);

			my_type& operator=(const my_type& rhs);
			my_type& operator=(my_type&& rhs);
			my_type& operator=(std::initializer_list< value_type > iList);

			void swap(my_type& rhs);

			// Iterator Support
			iterator begin(void) NOEXCEPT;
			const_iterator begin(void) const NOEXCEPT;
			const_iterator cbegin(void) const NOEXCEPT;

			iterator end(void) NOEXCEPT;
			const_iterator end(void) const NOEXCEPT;
			const_iterator cend(void) const NOEXCEPT;

			reverse_iterator rbegin(void) NOEXCEPT;
			const_reverse_iterator rbegin(void) const NOEXCEPT;
			const_reverse_iterator crbegin(void) const NOEXCEPT;

			reverse_iterator rend(void) NOEXCEPT;
			const_reverse_iterator rend(void) const NOEXCEPT;
			const_reverse_iterator crend(void) const NOEXCEPT;

			// Access
			//template < typename P, typename = typename FXD::enable_if< FXD::is_constructible< value_type, P&& >::value >::type >
			insert_return_type insert(const value_type& val);
			template < typename P >
			insert_return_type insert(P&& val);
			insert_return_type insert(value_type&& val);
			iterator insert(const_iterator hint, const value_type& val);
			template < class P >
			iterator insert(const_iterator hint, P&& val);
			iterator insert(const_iterator hint, value_type&& val);
			template < typename InputItr, class = typename FXD::STD::enable_if< FXD::Internal::_Is_iterator< InputItr >::value, void >::type >
			void insert(InputItr first, InputItr last);
			void insert(std::initializer_list< value_type > iList);
			/*
			insert_return_type insert(node_type&& nh);
			iterator insert(const_iterator hint, node_type&& nh);
			*/

			template < class M >
			Core::Pair< iterator, bool > insert_or_assign(const key_type& key, M&& obj);
			template < class M >
			Core::Pair< iterator, bool > insert_or_assign(key_type&& key, M&& obj);
			template < class M >
			iterator insert_or_assign(const_iterator hint, const key_type& key, M&& obj);
			template < class M >
			iterator insert_or_assign(const_iterator hint, key_type&& key, M&& obj);

			template < class... Args >
			insert_return_type emplace(Args&&... args);

			template < class... Args >
			iterator emplace_hint(const_iterator hint, Args&&... args);

			template < class... Args >
			Core::Pair< iterator, bool > try_emplace(const key_type& key, Args&&... args);
			template < class... Args >
			Core::Pair< iterator, bool > try_emplace(key_type&& key, Args&&... args);
			template < class... Args >
			iterator try_emplace(const_iterator hint, const key_type& key, Args&&... args);
			template < class... Args >
			iterator try_emplace(const_iterator hint, key_type&& key, Args&&... args);

			// Erase
			iterator erase(const_iterator pos);
			iterator erase(const_iterator first, const_iterator last);
			reverse_iterator erase(const_reverse_iterator pos);
			reverse_iterator erase(const_reverse_iterator first, const_reverse_iterator last);
			void erase(const key_type* pFirst, const key_type* pLast);
			size_type find_erase(const key_type& key);

			void clear(void);
			void reset_lose_memory(void);

			// Searches
			iterator find(const key_type& key);
			const_iterator find(const key_type& key) const;
			bool contains(const key_type& key) const;

			iterator lower_bound(const key_type& key);
			const_iterator lower_bound(const key_type& key) const;

			iterator upper_bound(const key_type& key);
			const_iterator upper_bound(const key_type& key) const;

			Core::Pair< iterator, iterator > equal_range(const key_type& key);
			Core::Pair< const_iterator, const_iterator > equal_range(const key_type& key) const;

			bool empty(void) const NOEXCEPT;
			bool not_empty(void) const NOEXCEPT;
			bool validate(void) const NOEXCEPT;
			size_type size(void) const NOEXCEPT;
			allocator_type allocator(void) const NOEXCEPT;

			const key_compare& key_comp(void) const;
			key_compare& key_comp(void);

		protected:
			node_type* _allocate_node(void);
			void _free_node(node_type* pNode);

			node_type* _create_node_from_key(const key_type& keinsy);

			template<class... Args>
			node_type* _create_node(Args&&... args);
			node_type* _create_node(const value_type& val);
			node_type* _create_node(value_type&& val);
			node_type* _create_node(const node_type* pNodeSource, node_type* pNodeParent);

			node_type* _copy_subtree(const node_type* pNodeSource, node_type* pNodeDest);
			void _nuke_subtree(node_type* pNode);

			template < typename... Args >
			Core::Pair< iterator, bool > _insert_value(FXD::STD::true_type, Args&&... args);

			template < typename... Args >
			iterator _insert_value(FXD::STD::false_type, Args&&... args);

			template < typename... Args >
			iterator _insert_value_impl(node_type* pNodeParent, bool bForceToLeft, const key_type& key, Args&&... args);
			iterator _insert_value_impl(node_type* pNodeParent, bool bForceToLeft, const key_type& key, node_type* pNodeNew);

			Core::Pair< iterator, bool > _insert_key(FXD::STD::true_type, const key_type& key);
			iterator _insert_key(FXD::STD::false_type, const key_type& key);

			template < typename... Args >
			iterator _insert_value_hint(FXD::STD::true_type, const_iterator pos, Args&&... args);
			template < typename... Args >
			iterator _insert_value_hint(FXD::STD::false_type, const_iterator pos, Args&&... args);

			iterator _insert_value_hint(FXD::STD::true_type, const_iterator pos, value_type&& val);
			iterator _insert_value_hint(FXD::STD::false_type, const_iterator pos, value_type&& val);

			iterator _insert_key(FXD::STD::true_type, const_iterator pos, const key_type& key);
			iterator _insert_key(FXD::STD::false_type, const_iterator pos, const key_type& key);
			iterator _insert_key_impl(node_type* pNodeParent, bool bForceToLeft, const key_type& key);

			node_type* _get_key_position_unique_keys(bool& canInsert, const key_type& key);
			node_type* _get_key_position_nonunique_keys(const key_type& key);

			node_type* _get_key_position_unique_keys_hint(const_iterator pos, bool& bForceToLeft, const key_type& key);
			node_type* _get_key_position_nonunique_keys_hint(const_iterator pos, bool& bForceToLeft, const key_type& key);


		protected:
			Internal::_RBTreeNodeBase m_anchor;
			size_type m_nSize;
			allocator_type m_alloc;
		};

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::RBTree(void)
			: m_anchor()
			, m_nSize(0)
		{
			reset_lose_memory();
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::RBTree(const allocator_type& allocator)
			: m_anchor()
			, m_nSize(0)
			, m_alloc(allocator)
		{
			reset_lose_memory();
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::RBTree(const key_compare& compare, const allocator_type& allocator)
			: base_type(compare)
			, m_anchor()
			, m_nSize(0)
			, m_alloc(allocator)
		{
			reset_lose_memory();
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		template < typename InputItr, typename >
		inline RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::RBTree(InputItr first, InputItr last, const key_compare& compare, const allocator_type& allocator)
			: base_type(compare)
			, m_anchor()
			, m_nSize(0)
			, m_alloc(allocator)
		{
			reset_lose_memory();
			for (; first != last; ++first)
			{
				insert((*first));
			}
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::~RBTree(void)
		{
			_nuke_subtree((node_type*)m_anchor.m_pNodeParent);
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::RBTree(const my_type& rhs)
			: base_type(rhs.m_compare)
			, m_anchor()
			, m_nSize(0)
			, m_alloc(rhs.m_alloc)
		{
			reset_lose_memory();

			if (rhs.m_anchor.m_pNodeParent != nullptr)
			{
				m_anchor.m_pNodeParent = _copy_subtree((const node_type*)rhs.m_anchor.m_pNodeParent, (node_type*)&m_anchor);
				m_anchor.m_pNodeRight = RBTreeGetMaxChild(m_anchor.m_pNodeParent);
				m_anchor.m_pNodeLeft = RBTreeGetMinChild(m_anchor.m_pNodeParent);
				m_nSize = rhs.m_nSize;
			}
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::RBTree(my_type&& rhs)
			: base_type(rhs.m_compare)
			, m_anchor()
			, m_nSize(0)
			, m_alloc(rhs.m_alloc)
		{
			reset_lose_memory();
			swap(rhs);
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::my_type&
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::operator=(const my_type& rhs)
		{
			if (this != &rhs)
			{
				clear();
				base_type::m_compare = rhs.m_compare;
				if (rhs.m_anchor.m_pNodeParent != nullptr)
				{
					m_anchor.m_pNodeParent = _copy_subtree((const node_type*)rhs.m_anchor.m_pNodeParent, (node_type*)&m_anchor);
					m_anchor.m_pNodeRight = RBTreeGetMaxChild(m_anchor.m_pNodeParent);
					m_anchor.m_pNodeLeft = RBTreeGetMinChild(m_anchor.m_pNodeParent);
					m_nSize = rhs.m_nSize;
				}
			}
			return (*this);
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::my_type&
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::operator=(my_type&& rhs)
		{
			if (this != &rhs)
			{
				clear();
				swap(rhs);
			}
			return (*this);
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::my_type& RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::operator=(std::initializer_list< value_type > iList)
		{
			clear();
			for (typename std::initializer_list< value_type >::iterator itr = iList.begin(), itrEnd = iList.end(); itr != itrEnd; ++itr)
			{
				_insert_value(has_unique_keys_type(), std::move(*itr));
			}
			return (*this);
		}


		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		void RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::swap(my_type& rhs)
		{
			const my_type tmp(*this);
			(*this) = rhs;
			rhs = tmp;
		}

		// Iterator Support
		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::begin(void) NOEXCEPT
		{
			return iterator(static_cast< node_type* >(m_anchor.m_pNodeLeft));
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::const_iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::begin(void) const NOEXCEPT
		{
			return const_iterator(static_cast< node_type* >(const_cast<Internal::_RBTreeNodeBase* >(m_anchor.m_pNodeLeft)));
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::const_iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::cbegin(void) const NOEXCEPT
		{
			return const_iterator(static_cast< node_type* >(const_cast<Internal::_RBTreeNodeBase* >(m_anchor.m_pNodeLeft)));
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::end(void) NOEXCEPT
		{
			return iterator(static_cast< node_type* >(&m_anchor));
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::const_iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::end(void) const NOEXCEPT
		{
			return const_iterator(static_cast< node_type* >(const_cast< Internal::_RBTreeNodeBase* >(&m_anchor)));
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::const_iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::cend(void) const NOEXCEPT
		{
			return const_iterator(static_cast< node_type* >(const_cast< Internal::_RBTreeNodeBase* >(&m_anchor)));
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::reverse_iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::rbegin(void) NOEXCEPT
		{
			return reverse_iterator(end());
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::const_reverse_iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::rbegin(void) const NOEXCEPT
		{
			return const_reverse_iterator(end());
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::const_reverse_iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::crbegin(void) const NOEXCEPT
		{
			return const_reverse_iterator(end());
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::reverse_iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::rend(void) NOEXCEPT
		{
			return reverse_iterator(begin());
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::const_reverse_iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::rend(void) const NOEXCEPT
		{
			return const_reverse_iterator(begin());
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::const_reverse_iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::crend(void) const NOEXCEPT
		{
			return const_reverse_iterator(begin());
		}

		// Access
		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::insert_return_type
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::insert(const value_type& val)
		{
			return _insert_value(has_unique_keys_type(), val);
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		template < typename P >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::insert_return_type
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::insert(P&& val)
		{
			return emplace(std::forward< P >(val));
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::insert_return_type
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::insert(value_type&& val)
		{
			return _insert_value(has_unique_keys_type(), std::forward< value_type >(val));
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::insert(const_iterator hint, const value_type& val)
		{
			return _insert_value_hint(has_unique_keys_type(), hint, val);
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		template < typename P >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::insert(const_iterator hint, P&& val)
		{
			return emplace_hint(hint, std::forward< P >(val));
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::insert(const_iterator hint, value_type&& val)
		{
			return _insert_value_hint(has_unique_keys_type(), hint, value_type(std::move(val)));
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		template < typename InputItr, typename >
		void RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::insert(InputItr first, InputItr last)
		{
			for (; first != last; ++first)
			{
				_insert_value(has_unique_keys_type(), (*first));
			}
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		void RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::insert(std::initializer_list< value_type > iList)
		{
			insert(iList.begin(), iList.end());
		}

		/*
		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::insert_return_type RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::insert(node_type&& nh)
		{
		}
		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::insert(const_iterator hint, node_type&& nh)
		{
		}
		*/

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		template < typename M >
		Core::Pair< typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator, bool >
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::insert_or_assign(const key_type& key, M&& obj)
		{
			auto itr = find(key);
			if (itr == end())
			{
				return insert(value_type(FXD::STD::piecewise_construct, FXD::forward_as_tuple(key), FXD::forward_as_tuple(std::forward< M >(obj))));
			}
			else
			{
				itr->second = std::forward< M >(obj);
				return{ itr, false };
			}
		}
		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		template < typename M >
		Core::Pair< typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator, bool >
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::insert_or_assign(key_type&& key, M&& obj)
		{
			auto itr = find(key);
			if (itr == end())
			{
				return insert(value_type(FXD::STD::piecewise_construct, FXD::forward_as_tuple(std::move(key)), FXD::forward_as_tuple(std::forward< M >(obj))));
			}
			else
			{
				itr->second = std::forward< M >(obj);
				return{ itr, false };
			}
		}
		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		template < typename M >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::insert_or_assign(const_iterator hint, const key_type& key, M&& obj)
		{
			auto itr = find(key);
			if (itr == end())
			{
				return insert(hint, value_type(FXD::STD::piecewise_construct, FXD::forward_as_tuple(key), FXD::forward_as_tuple(std::forward< M >(obj))));
			}
			else
			{
				itr->second = std::forward< M >(obj);
				return itr;
			}
		}
		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		template < typename M >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::insert_or_assign(const_iterator hint, key_type&& key, M&& obj)
		{
			auto itr = find(key);
			if (itr == end())
			{
				return insert(hint, value_type(FXD::STD::piecewise_construct, FXD::forward_as_tuple(std::move(key)), FXD::forward_as_tuple(std::forward< M >(obj))));
			}
			else
			{
				itr->second = std::forward< M >(obj);
				return itr;
			}
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		template < typename... Args >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::insert_return_type
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::emplace(Args&&... args)
		{
			return _insert_value(has_unique_keys_type(), std::forward< Args >(args)...);
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		template < typename... Args >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::emplace_hint(const_iterator hint, Args&&... args)
		{
			return _insert_value_hint(has_unique_keys_type(), hint, std::forward< Args >(args)...);
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		template < typename... Args >
		Core::Pair< typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator, bool >
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::try_emplace(const key_type& key, Args&&... args)
		{
			return _insert_value(has_unique_keys_type(), FXD::STD::piecewise_construct, FXD::forward_as_tuple(key), FXD::forward_as_tuple(std::forward< Args >(args)...));
		}
		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		template < typename... Args >
		Core::Pair< typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator, bool >
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::try_emplace(key_type&& key, Args&&... args)
		{
			return _insert_value(has_unique_keys_type(), FXD::STD::piecewise_construct, FXD::forward_as_tuple(std::move(key)), FXD::forward_as_tuple(std::forward< Args >(args)...));
		}
		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		template < typename... Args >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::try_emplace(const_iterator hint, const key_type& key, Args&&... args)
		{
			return _insert_value_hint(has_unique_keys_type(), hint, value_type(FXD::STD::piecewise_construct, FXD::forward_as_tuple(key), FXD::forward_as_tuple(std::forward< Args >(args)...)));
		}
		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		template < typename... Args >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::try_emplace(const_iterator hint, key_type&& key, Args&&... args)
		{
			return _insert_value_hint(has_unique_keys_type(), hint, value_type(FXD::STD::piecewise_construct, FXD::forward_as_tuple(key), FXD::forward_as_tuple(std::forward< Args >(args)...)));
		}

		// Erase
		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::erase(const_iterator pos)
		{
			const iterator itrErase(pos.m_pNode);
			--m_nSize;
			++pos;
			RBTreeErase(itrErase.m_pNode, &m_anchor);
			_free_node(itrErase.m_pNode);
			return iterator(pos.m_pNode);
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::erase(const_iterator first, const_iterator last)
		{
			if ((first.m_pNode != m_anchor.m_pNodeLeft) || (last.m_pNode != &m_anchor))
			{
				while (first != last)
				{
					first = erase(first);
				}
				return iterator(first.m_pNode);
			}
			clear();
			return end();
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::reverse_iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::erase(const_reverse_iterator pos)
		{
			return reverse_iterator(erase((++pos).base()));
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::reverse_iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::erase(const_reverse_iterator first, const_reverse_iterator last)
		{
			return reverse_iterator(erase((++last).base(), (++first).base()));
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline void RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::erase(const key_type* pFirst, const key_type* pLast)
		{
			while (pFirst != pLast)
			{
				erase(*pFirst++);
			}
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::size_type
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::find_erase(const key_type& key)
		{
			const Core::Pair< const_iterator, const_iterator > pair = equal_range(key);
			size_type nNum = FXD::STD::distance(pair.first, pair.second);
			erase(pair.first, pair.second);
			return nNum;
		}


		// Searches
		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::find(const key_type& key)
		{
			extract_key extractKey;
			node_type* pCurrent = (node_type*)m_anchor.m_pNodeParent;
			node_type* pRangeEnd = (node_type*)&m_anchor;

			while (pCurrent != nullptr)
			{
				if (!this->m_compare(extractKey(pCurrent->m_val), key))
				{
					pRangeEnd = pCurrent;
					pCurrent = (node_type*)pCurrent->m_pNodeLeft;
				}
				else
				{
					pCurrent = (node_type*)pCurrent->m_pNodeRight;
				}
			}

			if ((pRangeEnd != &m_anchor) && (!this->m_compare(key, extractKey(pRangeEnd->m_val))))
			{
				return iterator(pRangeEnd);
			}
			return end();
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::const_iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::find(const key_type& key) const
		{
			using rbtree_type = RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >;
			return const_iterator(const_cast<rbtree_type*>(this)->find(key));
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		bool RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::contains(const key_type& key) const
		{
			return (find(key) != end());
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline void RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::clear(void)
		{
			_nuke_subtree((node_type*)m_anchor.m_pNodeParent);
			reset_lose_memory();
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline void RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::reset_lose_memory(void)
		{
			m_anchor.m_pNodeRight = &m_anchor;
			m_anchor.m_pNodeLeft = &m_anchor;
			m_anchor.m_pNodeParent = nullptr;
			m_anchor.m_eColour = Internal::E_RBNodeColor::Red;
			m_nSize = 0;
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::lower_bound(const key_type& key)
		{
			extract_key extractKey;

			node_type* pCurrent = (node_type*)m_anchor.m_pNodeParent;
			node_type* pRangeEnd = (node_type*)&m_anchor;

			while (pCurrent != nullptr)
			{
				if (this->m_compare(extractKey(pCurrent->m_val), key) == false)
				{
					pRangeEnd = pCurrent;
					pCurrent = (node_type*)pCurrent->m_pNodeLeft;
				}
				else
				{
					pCurrent = (node_type*)pCurrent->m_pNodeRight;
				}
			}
			return iterator(pRangeEnd);
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::const_iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::lower_bound(const key_type& key) const
		{
			using rbtree_type = RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >;
			return const_iterator(const_cast<rbtree_type*>(this)->lower_bound(key));
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::upper_bound(const key_type& key)
		{
			extract_key extractKey;

			node_type* pCurrent = (node_type*)m_anchor.m_pNodeParent;
			node_type* pRangeEnd = (node_type*)&m_anchor;

			while (pCurrent != nullptr)
			{
				if (this->m_compare(key, extractKey(pCurrent->m_val)))
				{
					pRangeEnd = pCurrent;
					pCurrent = (node_type*)pCurrent->m_pNodeLeft;
				}
				else
				{
					pCurrent = (node_type*)pCurrent->m_pNodeRight;
				}
			}
			return iterator(pRangeEnd);
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::const_iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::upper_bound(const key_type& key) const
		{
			using rbtree_type = RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >;
			return const_iterator(const_cast< rbtree_type* >(this)->upper_bound(key));
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline Core::Pair< typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator, typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator >
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::equal_range(const key_type& key)
		{
			return Core::Pair< iterator, iterator >(lower_bound(key), upper_bound(key));
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline Core::Pair< typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::const_iterator, typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::const_iterator >
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::equal_range(const key_type& key) const
		{
			return Core::Pair< const_iterator, const_iterator >(lower_bound(key), upper_bound(key));
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline bool RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::empty(void) const NOEXCEPT
		{
			return (m_nSize == 0);
		}
		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline bool RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::not_empty(void) const NOEXCEPT
		{
			return !empty();
		}
		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline bool RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::validate(void) const NOEXCEPT
		{
			// Red-black trees have the following canonical properties which we validate here:
			// 1 Every node is either red or black.
			// 2 Every leaf (NULL) is black by defintion. Any number of black nodes may appear in a sequence. 
			// 3 If a node is red, then both its children are black. Thus, on any path from the root to a leaf, red nodes must not be adjacent.
			// 4 Every simple path from a node to a descendant leaf contains the same number of black nodes.
			// 5 The mnSize member of the tree must equal the number of nodes in the tree.
			// 6 The tree is sorted as per a conventional binary tree.
			// 7 The comparison function is sane; it obeys strict weak ordering. If mCompare(a,b) is true, then mCompare(b,a) must be false. Both cannot be true.

			extract_key extractKey;
			if (size() != 0)
			{
				if (m_anchor.m_pNodeLeft != RBTreeGetMinChild(m_anchor.m_pNodeParent))
				{
					return false;
				}
				if (m_anchor.m_pNodeRight != RBTreeGetMaxChild(m_anchor.m_pNodeParent))
				{
					return false;
				}

				const size_t nBlackCount = RBTreeGetBlackCount(m_anchor.m_pNodeParent, m_anchor.m_pNodeLeft);
				size_type nIteratedSize = 0;

				for (const_iterator itr = begin(); itr != end(); ++itr, ++nIteratedSize)
				{
					const node_type* const pNode = (const node_type*)itr.m_pNode;
					const node_type* const pNodeRight = (const node_type*)pNode->m_pNodeRight;
					const node_type* const pNodeLeft = (const node_type*)pNode->m_pNodeLeft;

					// Verify #7 above.
					if (pNodeRight && this->m_compare(extractKey(pNodeRight->m_val), extractKey(pNode->m_val)) && this->m_compare(extractKey(pNode->m_val), extractKey(pNodeRight->m_val))) // Validate that the compare function is sane.
					{
						return false;
					}
					// Verify #7 above.
					if (pNodeLeft && this->m_compare(extractKey(pNodeLeft->m_val), extractKey(pNode->m_val)) && this->m_compare(extractKey(pNode->m_val), extractKey(pNodeLeft->m_val))) // Validate that the compare function is sane.
					{
						return false;
					}
					// Verify item #1 above.
					if ((pNode->m_eColour != Internal::E_RBNodeColor::Red) && (pNode->m_eColour != Internal::E_RBNodeColor::Black))
					{
						return false;
					}
					// Verify item #3 above.
					if (pNode->m_eColour == Internal::E_RBNodeColor::Red)
					{
						if ((pNodeRight && (pNodeRight->m_eColour == Internal::E_RBNodeColor::Red)) || (pNodeLeft && (pNodeLeft->m_eColour == Internal::E_RBNodeColor::Red)))
						{
							return false;
						}
					}

					// Verify item #6 above.
					if (pNodeRight && this->m_compare(extractKey(pNodeRight->m_val), extractKey(pNode->m_val)))
					{
						return false;
					}
					if (pNodeLeft && this->m_compare(extractKey(pNode->m_val), extractKey(pNodeLeft->m_val)))
					{
						return false;
					}
					if (!pNodeRight && !pNodeLeft) // If we are at a bottom node of the tree...
					{
						// Verify item #4 above.
						if (RBTreeGetBlackCount(m_anchor.m_pNodeParent, pNode) != nBlackCount)
						{
							return false;
						}
					}
				}

				// Verify item #5 above.
				if (nIteratedSize != size())
				{
					return false;
				}
				return true;
			}
			else
			{
				if ((m_anchor.m_pNodeLeft != &m_anchor) || (m_anchor.m_pNodeRight != &m_anchor))
				{
					return false;
				}
			}
			return true;
		}
		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::size_type
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::size(void) const NOEXCEPT
		{
			return m_nSize;
		}
		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::allocator_type
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::allocator(void) const NOEXCEPT
		{
			return m_alloc;
		}
		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		const inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::key_compare&
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::key_comp(void) const
		{
			return this->m_compare;
		}
		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::key_compare&
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::key_comp(void)
		{
			return this->m_compare;
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::node_type*
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_allocate_node(void)
		{
			auto* pNode = (node_type*)m_alloc.allocate(sizeof(node_type));
			PRINT_COND_ASSERT((pNode != nullptr), "Container: nullptr generated. Failed to allocate memory");
			return pNode;
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline void RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_free_node(node_type* pNode)
		{
			FXD::STD::destruct(pNode);
			m_alloc.deallocate(pNode, sizeof(node_type));
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::node_type*
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_create_node_from_key(const key_type& key)
		{
			node_type* const pNode = _allocate_node();
			FXD_PLACEMENT_NEW(&pNode->m_val, value_type)(key);

			pNode->m_pNodeRight = nullptr;
			pNode->m_pNodeLeft = nullptr;
			pNode->m_pNodeParent = nullptr;
			pNode->m_eColour = Internal::E_RBNodeColor::Black;
			return pNode;
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		template < typename... Args >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::node_type*
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_create_node(Args&&... args)
		{
			node_type* const pNode = _allocate_node();
			FXD_PLACEMENT_NEW(FXD::STD::addressof(pNode->m_val), value_type)(std::forward< Args >(args)...);

			pNode->m_pNodeRight = nullptr;
			pNode->m_pNodeLeft = nullptr;
			pNode->m_pNodeParent = nullptr;
			pNode->m_eColour = Internal::E_RBNodeColor::Black;
			return pNode;
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::node_type*
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_create_node(const value_type& val)
		{
			node_type* const pNode = _allocate_node();
			FXD_PLACEMENT_NEW(FXD::STD::addressof(pNode->m_val), value_type)(val);

			pNode->m_pNodeRight = nullptr;
			pNode->m_pNodeLeft = nullptr;
			pNode->m_pNodeParent = nullptr;
			pNode->m_eColour = Internal::E_RBNodeColor::Black;
			return pNode;
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::node_type*
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_create_node(value_type&& val)
		{
			node_type* const pNode = _allocate_node();
			FXD_PLACEMENT_NEW(&pNode->m_val, value_type)(std::move(val));

			pNode->m_pNodeRight = nullptr;
			pNode->m_pNodeLeft = nullptr;
			pNode->m_pNodeParent = nullptr;
			pNode->m_eColour = Internal::E_RBNodeColor::Black;
			return pNode;
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::node_type*
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_create_node(const node_type* pNodeSource, node_type* pNodeParent)
		{
			node_type* const pNode = _create_node(pNodeSource->m_val);

			pNode->m_pNodeRight = nullptr;
			pNode->m_pNodeLeft = nullptr;
			pNode->m_pNodeParent = pNodeParent;
			pNode->m_eColour = pNodeSource->m_eColour;
			return pNode;
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::node_type*
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_copy_subtree(const node_type* pNodeSource, node_type* pNodeDest)
		{
			node_type* const pNewNodeRoot = _create_node(pNodeSource, pNodeDest);

			// Copy the right side of the tree recursively.
			if (pNodeSource->m_pNodeRight != nullptr)
			{
				pNewNodeRoot->m_pNodeRight = _copy_subtree((const node_type*)pNodeSource->m_pNodeRight, pNewNodeRoot);
			}

			node_type* pNewNodeLeft;
			for (pNodeSource = (node_type*)pNodeSource->m_pNodeLeft, pNodeDest = pNewNodeRoot; pNodeSource; pNodeSource = (node_type*)pNodeSource->m_pNodeLeft, pNodeDest = pNewNodeLeft)
			{
				pNewNodeLeft = _create_node(pNodeSource, pNodeDest);
				pNodeDest->m_pNodeLeft = pNewNodeLeft;

				// Copy the right side of the tree recursively.
				if (pNodeSource->m_pNodeRight != nullptr)
				{
					pNewNodeLeft->m_pNodeRight = _copy_subtree((const node_type*)pNodeSource->m_pNodeRight, pNewNodeLeft);
				}
			}
			return pNewNodeRoot;
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		void RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_nuke_subtree(node_type* pNode)
		{
			while (pNode != nullptr)
			{
				_nuke_subtree((node_type*)pNode->m_pNodeRight);
				node_type* const pNodeLeft = (node_type*)pNode->m_pNodeLeft;
				_free_node(pNode);
				pNode = pNodeLeft;
			}
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		template < typename... Args >
		Core::Pair< typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator, bool >
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_insert_value(FXD::STD::true_type, Args&&... args) // FXD::STD::true_type means keys are unique.
		{
			value_type val(std::forward< Args >(args)...);

			extract_key extractKey;
			key_type key(extractKey(val));
			bool bCanInsert;
			node_type* pPosition = _get_key_position_unique_keys(bCanInsert, key);

			if (bCanInsert)
			{
				const iterator itr(_insert_value_impl(pPosition, false, key, std::move(val)));
				return Core::Pair< iterator, bool >(itr, true);
			}
			return Core::Pair< iterator, bool >(iterator(pPosition), false);
		}


		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		template < typename... Args >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_insert_value(FXD::STD::false_type, Args&&... args) // FXD::STD::false_type means keys are not unique.
		{
			value_type val(std::forward< Args >(args)...);
			extract_key extractKey;
			key_type key(extractKey(val));
			node_type* pPosition = _get_key_position_nonunique_keys(key);

			return _insert_value_impl(pPosition, false, key, std::move(val));
		}


		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		template < typename... Args >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_insert_value_impl(node_type* pNodeParent, bool bForceToLeft, const key_type& key, Args&&... args)
		{
			node_type* const pNodeNew = _create_node(std::forward< Args >(args)...); // Note that pNodeNew->mpLeft, mpRight, mpParent, will be uninitialized.
			return _insert_value_impl(pNodeParent, bForceToLeft, key, pNodeNew);
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_insert_value_impl(node_type* pNodeParent, bool bForceToLeft, const key_type& key, node_type* pNodeNew)
		{
			PRINT_COND_INFO((pNodeNew != nullptr), "node to insert to the rbtree must not be null");

			Internal::E_RBNodeSide eSide;
			extract_key extractKey;
			if ((bForceToLeft) || (pNodeParent == &m_anchor) || this->m_compare(key, extractKey(pNodeParent->m_val)))
			{
				eSide = Internal::E_RBNodeSide::Left;
			}
			else
			{
				eSide = Internal::E_RBNodeSide::Right;
			}

			RBTreeInsert(pNodeNew, pNodeParent, &m_anchor, eSide);
			m_nSize++;
			return iterator(pNodeNew);
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		Core::Pair< typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator, bool>
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_insert_key(FXD::STD::true_type, const key_type& key) // true_type means keys are unique.
		{
			bool bCanInsert = true;
			node_type* pPosition = _get_key_position_unique_keys(bCanInsert, key);

			if (bCanInsert)
			{
				const iterator itResult(_insert_key_impl(pPosition, false, key));
				return Core::Pair< iterator, bool >(itResult, true);
			}
			return Core::Pair< iterator, bool >(iterator(pPosition), false);
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_insert_key(FXD::STD::false_type, const key_type& key) // FXD::STD::false_type means keys are not unique.
		{
			return _insert_key_impl(_get_key_position_nonunique_keys(key), false, key);
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		template < typename... Args>
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_insert_value_hint(FXD::STD::true_type, const_iterator pos, Args&&... args) // FXD::STD::true_type means keys are unique.
		{
			node_type* pNodeNew = _create_node(std::forward< Args >(args)...); // Note that pNodeNew->mpLeft, mpRight, mpParent, will be uninitialized.
			const key_type& key(extract_key{}(pNodeNew->m_val));

			bool bForceToLeft = false;
			node_type* pPos = _get_key_position_unique_keys_hint(pos, bForceToLeft, key);

			if (pPos == nullptr)
			{
				bool bCanInsert = false;
				pPos = _get_key_position_unique_keys(bCanInsert, key);

				if (bCanInsert == false)
				{
					_free_node(pNodeNew);
					return iterator(pPos);
				}

				bForceToLeft = false;
			}

			return _insert_value_impl(pPos, bForceToLeft, key, pNodeNew);
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		template < typename... Args>
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_insert_value_hint(FXD::STD::false_type, const_iterator position, Args&&... args) // FXD::STD::false_type means keys are not unique.
		{
			node_type* pNodeNew = _create_node(std::forward< Args >(args)...); // Note that pNodeNew->mpLeft, mpRight, mpParent, will be uninitialized.
			const key_type& key(extract_key{}(pNodeNew->m_val));

			bool bForceToLeft = false;
			node_type* pPos = _get_key_position_nonunique_keys_hint(position, bForceToLeft, key);

			if (pPos == nullptr)
			{
				pPos = _get_key_position_nonunique_keys(key);
				bForceToLeft = false;
			}

			return _insert_value_impl(pPos, bForceToLeft, key, pNodeNew);
		}


		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator 
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_insert_value_hint(FXD::STD::true_type, const_iterator pos, value_type&& val) // FXD::STD::true_type means keys are unique.
		{
			extract_key extractKey;
			key_type key(extractKey(val));
			bool bForceToLeft = true;
			node_type* pPos = _get_key_position_unique_keys_hint(pos, bForceToLeft, key);
			
			if (pPos != nullptr)
			{
				return _insert_value_impl(pPos, bForceToLeft, key, std::move(val));
			}
			else
			{
				return _insert_value(has_unique_keys_type(), std::move(val)).first;
			}
		}


		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator 
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_insert_value_hint(FXD::STD::false_type, const_iterator pos, value_type&& val) // FXD::STD::false_type means keys are not unique.
		{
			extract_key extractKey;
			key_type key(extractKey(val));
			bool bForceToLeft = true;
			node_type* pPos = _get_key_position_nonunique_keys_hint(pos, bForceToLeft, key);

			if (pPos != nullptr)
			{
				return _insert_value_impl(pPos, bForceToLeft, key, std::move(val));
			}
			else
			{
				return _insert_value(has_unique_keys_type(), std::move(val));
			}
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_insert_key(FXD::STD::true_type, const_iterator pos, const key_type& key) // FXD::STD::true_type means keys are unique.
		{
			bool bForceToLeft;
			node_type* pPosition = _get_key_position_unique_keys_hint(pos, bForceToLeft, key);

			return (pPosition != nullptr) ? _insert_key_impl(pPosition, bForceToLeft, key) : _insert_key(has_unique_keys_type(), key).first;
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_insert_key(FXD::STD::false_type, const_iterator pos, const key_type& key) // FXD::STD::false_type means keys are not unique.
		{
			bool bForceToLeft;
			node_type* pPosition = _get_key_position_nonunique_keys_hint(pos, bForceToLeft, key);

			return (pPosition != nullptr) ? _insert_key_impl(pPosition, bForceToLeft, key) : _insert_key(has_unique_keys_type(), key);
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::iterator
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_insert_key_impl(node_type* pNodeParent, bool bForceToLeft, const key_type& key)
		{
			Internal::E_RBNodeSide eSide;
			extract_key extractKey;
			if ((bForceToLeft) || (pNodeParent == &m_anchor) || (this->m_compare(key, extractKey(pNodeParent->m_val))))
			{
				eSide = Internal::E_RBNodeSide::Left;
			}
			else
			{
				eSide = Internal::E_RBNodeSide::Right;
			}

			node_type* const pNodeNew = _create_node_from_key(key);
			RBTreeInsert(pNodeNew, pNodeParent, &m_anchor, eSide);
			m_nSize++;
			return iterator(pNodeNew);
		}


		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::node_type*
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_get_key_position_unique_keys(bool& bCanInsert, const key_type& key)
		{
			extract_key extractKey;
			node_type* pLowerBound = (node_type*)&m_anchor;
			node_type* pCurrent = (node_type*)m_anchor.m_pNodeParent;

			bool bValueLessThanNode = true;
			while (pCurrent != nullptr)
			{
				bValueLessThanNode = this->m_compare(key, extractKey(pCurrent->m_val));
				pLowerBound = pCurrent;

				if (bValueLessThanNode)
				{
					pCurrent = (node_type*)pCurrent->m_pNodeLeft;
				}
				else
				{
					pCurrent = (node_type*)pCurrent->m_pNodeRight;
				}
			}

			node_type* pParent = pLowerBound;
			if (bValueLessThanNode)
			{
				if (pLowerBound != (node_type*)m_anchor.m_pNodeLeft)
				{
					pLowerBound = (node_type*)RBTreePrev(pLowerBound);
				}
				else
				{
					bCanInsert = true;
					return pLowerBound;
				}
			}

			if (this->m_compare(extractKey(pLowerBound->m_val), key))
			{
				bCanInsert = true;
				return pParent;
			}

			bCanInsert = false;
			return pLowerBound;
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::node_type*
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_get_key_position_nonunique_keys(const key_type& key)
		{
			node_type* pCurrent = (node_type*)m_anchor.m_pNodeParent;
			node_type* pRangeEnd = (node_type*)&m_anchor;
			extract_key extractKey;
			while (pCurrent != nullptr)
			{
				pRangeEnd = pCurrent;
				if (this->m_compare(key, extractKey(pCurrent->m_val)))
				{
					pCurrent = (node_type*)pCurrent->m_pNodeLeft;
				}
				else
				{
					pCurrent = (node_type*)pCurrent->m_pNodeRight;
				}
			}
			return pRangeEnd;
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::node_type*
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_get_key_position_unique_keys_hint(const_iterator pos, bool& bForceToLeft, const key_type& key)
		{
			extract_key extractKey;
			if ((pos.m_pNode != m_anchor.m_pNodeRight) && (pos.m_pNode != &m_anchor))
			{
				iterator itNext(pos.m_pNode);
				++itNext;

				const bool bPositionLessThanValue = this->m_compare(extractKey(pos.m_pNode->m_val), key);
				if (bPositionLessThanValue)
				{
					const bool bValueLessThanNext = this->m_compare(key, extractKey(itNext.m_pNode->m_val));
					if (bValueLessThanNext)
					{
						if (pos.m_pNode->m_pNodeRight != nullptr)
						{
							bForceToLeft = true;
							return itNext.m_pNode;
						}
						bForceToLeft = false;
						return pos.m_pNode;
					}
				}
				bForceToLeft = false;
				return nullptr;
			}

			if ((m_nSize != 0) && (this->m_compare(extractKey(((node_type*)m_anchor.m_pNodeRight)->m_val), key)))
			{
				bForceToLeft = false;
				return (node_type*)m_anchor.m_pNodeRight;
			}
			bForceToLeft = false;
			return nullptr;
		}

		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		typename RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::node_type*
			RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >::_get_key_position_nonunique_keys_hint(const_iterator pos, bool& bForceToLeft, const key_type& key)
		{
			extract_key extractKey;
			if ((pos.m_pNode != m_anchor.m_pNodeRight) && (pos.m_pNode != &m_anchor))
			{
				iterator itNext(pos.m_pNode);
				++itNext;

				if ((!this->m_compare(key, extractKey(pos.m_pNode->m_val))) && (!this->m_compare(extractKey(itNext.m_pNode->m_val), key)))
				{
					if (pos.m_pNode->m_pNodeRight != nullptr)
					{
						bForceToLeft = true;
						return itNext.m_pNode;
					}
					bForceToLeft = false;
					return pos.m_pNode;
				}
				bForceToLeft = false;
				return nullptr;
			}

			if ((m_nSize != 0) && (this->m_compare(key, extractKey(((node_type*)m_anchor.m_pNodeRight)->m_val)) == false))
			{
				bForceToLeft = false;
				return (node_type*)m_anchor.m_pNodeRight;
			}
			bForceToLeft = false;
			return nullptr;
		}
	} //namespace Container

	namespace STD
	{
		template < typename Key, typename Value, typename Compare, typename Allocator, typename ExtractKey, bool bMutableIterators, bool bUniqueKeys >
		inline void swap(FXD::Container::RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >& lhs, FXD::Container::RBTree< Key, Value, Compare, Allocator, ExtractKey, bMutableIterators, bUniqueKeys >& rhs)
		{
			lhs.swap(rhs);
		}
	} //namespace STD

} //namespace FXD
#endif //FXDENGINE_CONTAINER_REDBLACKTREE_H