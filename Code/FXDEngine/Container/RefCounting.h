// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_REFCOUNTING_H
#define FXDENGINE_CONTAINER_REFCOUNTING_H

namespace FXD
{
	namespace Container
	{
		// ------
		// Ptr
		// -
		// ------
		template < typename ReferencedType >
		class Ptr
		{
			using ReferenceType = ReferencedType*;

		public:
			inline Ptr(void)
				: m_pPtr(nullptr)
			{}
			inline Ptr(ReferencedType* pReference, bool bAddRef = true)
				: m_pPtr(pReference)
			{
				if (m_pPtr != nullptr && bAddRef)
				{
					m_pPtr->AddRef();
				}
			}
			inline Ptr(const Ptr& rhs)
				: m_pPtr(rhs.m_pPtr)
			{
				if (m_pPtr != nullptr)
				{
					m_pPtr->AddRef();
				}
			}
			inline Ptr(Ptr&& rhs)
				: m_pPtr(rhs.m_pPtr)
			{
				rhs.m_pPtr = nullptr;
			}
			inline ~Ptr(void)
			{
				if (m_pPtr != nullptr)
				{
					m_pPtr->Release();
				}
			}

			Ptr& operator=(ReferenceType pReference)
			{
				ReferenceType pOldReference = m_pPtr;
				m_pPtr = pReference;
				if (m_pPtr != nullptr)
				{
					m_pPtr->AddRef();
				}
				if (pOldReference != nullptr)
				{
					pOldReference->Release();
				}
				return (*this);
			}
			
			inline Ptr& operator=(const Ptr& rhs)
			{
				m_pPtr = rhs.m_pPtr;
				return (*this);
			}

			Ptr& operator=(Ptr&& rhs)
			{
				if (this != &rhs)
				{
					ReferenceType pOldReference = m_pPtr;
					m_pPtr = rhs.m_pPtr;
					rhs.m_pPtr = nullptr;
					if (pOldReference != nullptr)
					{
						pOldReference->Release();
					}
				}
				return (*this);
			}

			inline ReferenceType operator->() const
			{
				return m_pPtr;
			}

			inline operator ReferenceType() const
			{
				return m_pPtr;
			}
			inline operator void**()
			{
				return (void**)&m_pPtr;
			}
			inline operator ReferenceType*()
			{
				return &m_pPtr;
			}

			inline operator bool() const
			{
				return (m_pPtr != nullptr);
			}
			inline bool operator!() const
			{
				return (m_pPtr == nullptr);
			}

			ReferenceType get(void) const
			{
				return m_pPtr;
			}

		private:
			ReferenceType m_pPtr;
		};

		template < typename ReferencedType >
		bool operator==(const Ptr< ReferencedType >& lhs, const Ptr< ReferencedType >& rhs)
		{
			return (lhs.GetReference() == rhs.GetReference());
		}
		template < typename ReferencedType >
		bool operator==(const Ptr<ReferencedType>& lhs, ReferencedType* rhs)
		{
			return (lhs.GetReference() == rhs);
		}
		template < typename ReferencedType >
		bool operator==(ReferencedType* lhs, const Ptr< ReferencedType >& rhs)
		{
			return (lhs == rhs.GetReference());
		}

		template < typename ReferencedType >
		bool operator!=(const Ptr< ReferencedType >& lhs, const Ptr< ReferencedType >& rhs)
		{
			return (lhs.GetReference() != rhs.GetReference());
		}
		template < typename ReferencedType >
		bool operator!=(const Ptr<ReferencedType>& lhs, ReferencedType* rhs)
		{
			return (lhs.GetReference() != rhs);
		}
		template < typename ReferencedType >
		bool operator!=(ReferencedType* lhs, const Ptr< ReferencedType >& rhs)
		{
			return (lhs != rhs.GetReference());
		}

	} //namespace Container
} //namespace FXD
#endif //FXDENGINE_CONTAINER_REFCOUNTING_H