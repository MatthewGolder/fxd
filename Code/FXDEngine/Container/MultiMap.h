// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_CONTAINER_MULTIMAP2_H
#define FXDENGINE_CONTAINER_MULTIMAP2_H

#include "FXDEngine/Container/RedBlackTree.h"
#include "FXDEngine/Memory/Allocator/Aligned.h"

namespace FXD
{
	namespace Container
	{
		// ------
		// MultiMap
		// -
		// ------
		template <
			typename Key,
			typename T,
			typename Compare = FXD::STD::less< Key >,
			typename Allocator = Memory::Alloc::AlignedType< Memory::Alloc::Heap, Core::Pair< Key, T > > >
		class MultiMap : public Container::RBTree< Key, Core::Pair< const Key, T >, Compare, Allocator, FXD::STD::UseFirst< Core::Pair< const Key, T > >, true, false >
		{
		public:
			using my_type = MultiMap< Key, T, Compare, Allocator >;
			using base_type = Container::RBTree< Key, Core::Pair< const Key, T >, Compare, Allocator, FXD::STD::UseFirst< Core::Pair< const Key, T> >, true, false >;

			using key_type = typename base_type::key_type;
			using value_type = typename base_type::value_type;
			using size_type = typename base_type::size_type;
			using mapped_type = T;
			using node_type = typename base_type::node_type;
			using iterator = typename base_type::iterator;
			using const_iterator = typename base_type::const_iterator;
			using allocator_type = typename base_type::allocator_type;

		public:
			class value_compare
			{
			protected:
				friend class MultiMap;

			public:
				typedef bool result_type;
				typedef value_type first_argument_type;
				typedef value_type second_argument_type;

			protected:
				value_compare(Compare c)
					: m_compare(c)
				{}

			public:
				bool operator()(const value_type& lhs, const value_type& rhs) const
				{
					return m_compare(lhs.first, rhs.first);
				}

			protected:
				Compare m_compare;
			};

		public:
			MultiMap(void);
			MultiMap(const allocator_type& allocator);
			MultiMap(const Compare& compare, const allocator_type& allocator = allocator_type());

			template < typename InputItr, class = typename FXD::STD::enable_if< FXD::Internal::_Is_iterator< InputItr >::value, void >::type >
			MultiMap(InputItr first, InputItr last, const allocator_type& allocator);
			template < typename InputItr, class = typename FXD::STD::enable_if< FXD::Internal::_Is_iterator< InputItr >::value, void >::type >
			MultiMap(InputItr first, InputItr last, const Compare& compare = Compare(), const allocator_type& allocator = allocator_type());

			MultiMap(std::initializer_list< value_type > iList, const allocator_type& allocator);
			MultiMap(std::initializer_list< value_type > iList, const Compare& compare = Compare(), const allocator_type& allocator = allocator_type());
			~MultiMap(void);

			MultiMap(const my_type& rhs);
			MultiMap(my_type&& rhs);

			my_type& operator=(const my_type& rhs);
			my_type& operator=(my_type&& rhs);
			my_type& operator=(std::initializer_list< value_type > iList);

			value_compare value_comp(void) const;

			size_type count(const key_type& key) const;
		};

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline MultiMap< Key, T, Compare, Allocator >::MultiMap(void)
		{}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline MultiMap< Key, T, Compare, Allocator >::MultiMap(const allocator_type& allocator)
			: base_type(allocator)
		{}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline MultiMap< Key, T, Compare, Allocator >::MultiMap(const Compare& compare, const allocator_type& allocator)
			: base_type(compare, allocator)
		{}

		template < typename Key, typename T, typename Compare, typename Allocator >
		template < typename InputItr, class >
		inline MultiMap< Key, T, Compare, Allocator >::MultiMap(InputItr first, InputItr last, const allocator_type& allocator)
			: base_type(first, last, Compare(), allocator)
		{}

		template < typename Key, typename T, typename Compare, typename Allocator >
		template < typename InputItr, class >
		inline MultiMap< Key, T, Compare, Allocator >::MultiMap(InputItr first, InputItr last, const Compare& compare, const allocator_type& allocator)
			: base_type(first, last, compare, allocator)
		{}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline MultiMap< Key, T, Compare, Allocator >::MultiMap(std::initializer_list< value_type > iList, const allocator_type& allocator)
			: base_type(iList.begin(), iList.end(), Compare(), allocator)
		{}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline MultiMap< Key, T, Compare, Allocator >::MultiMap(std::initializer_list< value_type > iList, const Compare& compare, const allocator_type& allocator)
			: base_type(iList.begin(), iList.end(), compare, allocator)
		{}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline MultiMap< Key, T, Compare, Allocator >::~MultiMap(void)
		{}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline MultiMap< Key, T, Compare, Allocator >::MultiMap(const my_type& rhs)
			: base_type(rhs)
		{}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline MultiMap< Key, T, Compare, Allocator >::MultiMap(my_type&& rhs)
			: base_type(std::move(rhs))
		{}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline typename MultiMap< Key, T, Compare, Allocator >::my_type& MultiMap< Key, T, Compare, Allocator >::operator=(const my_type& rhs)
		{
			return (my_type&)base_type::operator=(rhs);
		}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline typename MultiMap< Key, T, Compare, Allocator >::my_type& MultiMap< Key, T, Compare, Allocator >::operator=(my_type&& rhs)
		{
			return (my_type&)base_type::operator=(std::move(rhs));
		}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline typename MultiMap< Key, T, Compare, Allocator >::my_type& MultiMap< Key, T, Compare, Allocator >::operator=(std::initializer_list< value_type > iList)
		{
			return (my_type&)base_type::operator=(iList);
		}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline typename MultiMap< Key, T, Compare, Allocator >::value_compare MultiMap<Key, T, Compare, Allocator>::value_comp(void) const
		{
			return value_compare(this->m_compare);
		}

		template < typename Key, typename T, typename Compare, typename Allocator >
		inline typename MultiMap< Key, T, Compare, Allocator >::size_type MultiMap< Key, T, Compare, Allocator >::count(const key_type& key) const
		{
			const Core::Pair< const_iterator, const_iterator > range(this->equal_range(key));
			return (size_type)FXD::STD::distance(range.first, range.second);
		}
	} //namespace Container

	namespace STD
	{
		template < typename Key, typename T, typename Compare, typename Allocator, typename Pred >
		void erase_if(FXD::Container::MultiMap< Key, T, Compare, Allocator >& container, Pred pred)
		{
			for (auto i = container.begin(), last = container.end(); container != last;)
			{
				if (pred(*i))
				{
					i = container.erase(i);
				}
				else
				{
					++i;
				}
			}
		}
	} //namespace STD
} //namespace FXD
#endif //FXDENGINE_CONTAINER_MULTIMAP2_H