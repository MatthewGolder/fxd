// Creator - MatthewGolder
#include "FXDEngine/Container/Singleton.h"

using namespace FXD;
using namespace Container;

Thread::CSLock Container::ISingleton::g_singletonCS;

// ------
// ISingleton
// -
// ------
ISingleton::ISingleton(void)
{
	Thread::CSLock::LockGuard lock(g_singletonCS);
}

ISingleton::~ISingleton(void)
{
	Thread::CSLock::LockGuard lock(g_singletonCS);
}