// Creator - MatthewGolder
#include "FXDEngine/Container/RedBlackTree.h"
#include "FXDEngine/Core/Algorithm.h"
#include "FXDEngine/Core/Debugger.h"

using namespace FXD;
using namespace Container;

Container::Internal::_RBTreeNodeBase* FXD::Container::Internal::RBTreeRotateLeft(Container::Internal::_RBTreeNodeBase* pNode, Container::Internal::_RBTreeNodeBase* pNodeRoot)
{
	Container::Internal::_RBTreeNodeBase* const pNodeTemp = pNode->m_pNodeRight;
	pNode->m_pNodeRight = pNodeTemp->m_pNodeLeft;

	if (pNodeTemp->m_pNodeLeft != nullptr)
	{
		pNodeTemp->m_pNodeLeft->m_pNodeParent = pNode;
	}
	pNodeTemp->m_pNodeParent = pNode->m_pNodeParent;

	if (pNode == pNodeRoot)
	{
		pNodeRoot = pNodeTemp;
	}
	else if (pNode == pNode->m_pNodeParent->m_pNodeLeft)
	{
		pNode->m_pNodeParent->m_pNodeLeft = pNodeTemp;
	}
	else
	{
		pNode->m_pNodeParent->m_pNodeRight = pNodeTemp;
	}
	pNodeTemp->m_pNodeLeft = pNode;
	pNode->m_pNodeParent = pNodeTemp;

	return pNodeRoot;
}

Container::Internal::_RBTreeNodeBase* FXD::Container::Internal::RBTreeRotateRight(Container::Internal::_RBTreeNodeBase* pNode, Container::Internal::_RBTreeNodeBase* pNodeRoot)
{
	Container::Internal::_RBTreeNodeBase* const pNodeTemp = pNode->m_pNodeLeft;
	pNode->m_pNodeLeft = pNodeTemp->m_pNodeRight;

	if (pNodeTemp->m_pNodeRight != nullptr)
	{
		pNodeTemp->m_pNodeRight->m_pNodeParent = pNode;
	}
	pNodeTemp->m_pNodeParent = pNode->m_pNodeParent;

	if (pNode == pNodeRoot)
	{
		pNodeRoot = pNodeTemp;
	}
	else if (pNode == pNode->m_pNodeParent->m_pNodeRight)
	{
		pNode->m_pNodeParent->m_pNodeRight = pNodeTemp;
	}
	else
	{
		pNode->m_pNodeParent->m_pNodeLeft = pNodeTemp;
	}
	pNodeTemp->m_pNodeRight = pNode;
	pNode->m_pNodeParent = pNodeTemp;

	return pNodeRoot;
}

Container::Internal::_RBTreeNodeBase* FXD::Container::Internal::RBTreeNext(const Container::Internal::_RBTreeNodeBase* pNode)
{
	if (pNode->m_pNodeRight != nullptr)
	{
		pNode = pNode->m_pNodeRight;
		while (pNode->m_pNodeLeft != nullptr)
		{
			pNode = pNode->m_pNodeLeft;
		}
	}
	else
	{
		Container::Internal::_RBTreeNodeBase* pNodeTemp = pNode->m_pNodeParent;
		while (pNode == pNodeTemp->m_pNodeRight)
		{
			pNode = pNodeTemp;
			pNodeTemp = pNodeTemp->m_pNodeParent;
		}

		if (pNode->m_pNodeRight != pNodeTemp)
		{
			pNode = pNodeTemp;
		}
	}
	return const_cast<Container::Internal::_RBTreeNodeBase* >(pNode);
}

Container::Internal::_RBTreeNodeBase* FXD::Container::Internal::RBTreePrev(const Container::Internal::_RBTreeNodeBase* pNode)
{
	if ((pNode->m_pNodeParent->m_pNodeParent == pNode) && (pNode->m_eColour == Container::Internal::E_RBNodeColor::Red))
	{
		return pNode->m_pNodeRight;
	}
	else if (pNode->m_pNodeLeft != nullptr)
	{
		Container::Internal::_RBTreeNodeBase* pNodeTemp = pNode->m_pNodeLeft;
		while (pNodeTemp->m_pNodeRight)
		{
			pNodeTemp = pNodeTemp->m_pNodeRight;
		}
		return pNodeTemp;
	}

	Container::Internal::_RBTreeNodeBase* pNodeTemp = pNode->m_pNodeParent;
	while (pNode == pNodeTemp->m_pNodeLeft)
	{
		pNode = pNodeTemp;
		pNodeTemp = pNodeTemp->m_pNodeParent;
	}
	return const_cast< Container::Internal::_RBTreeNodeBase* >(pNodeTemp);
}

Container::Internal::_RBTreeNodeBase* FXD::Container::Internal::RBTreeGetMinChild(const Container::Internal::_RBTreeNodeBase* pNode)
{
	while (pNode->m_pNodeLeft != nullptr)
	{
		pNode = pNode->m_pNodeLeft;
	}
	return const_cast< Internal::_RBTreeNodeBase* >(pNode);
}

Container::Internal::_RBTreeNodeBase* FXD::Container::Internal::RBTreeGetMaxChild(const Container::Internal::_RBTreeNodeBase* pNode)
{
	while (pNode->m_pNodeRight != nullptr)
	{
		pNode = pNode->m_pNodeRight;
	}
	return const_cast< Container::Internal::_RBTreeNodeBase* >(pNode);
}

FXD::U32 FXD::Container::Internal::RBTreeGetBlackCount(const Internal::_RBTreeNodeBase* pNodeTop, const Internal::_RBTreeNodeBase* pNodeBottom)
{
	FXD::U32 nCount = 0;
	for (; pNodeBottom; pNodeBottom = pNodeBottom->m_pNodeParent)
	{
		if (pNodeBottom->m_eColour == Internal::E_RBNodeColor::Black)
		{
			++nCount;
		}
		if (pNodeBottom == pNodeTop)
		{
			break;
		}
	}
	return nCount;
}

void FXD::Container::Internal::RBTreeInsert(Container::Internal::_RBTreeNodeBase* pNode, Container::Internal::_RBTreeNodeBase* pNodeParent, Container::Internal::_RBTreeNodeBase* pNodeAnchor, Container::Internal::E_RBNodeSide eSide)
{
	Container::Internal::_RBTreeNodeBase*& pNodeRootRef = pNodeAnchor->m_pNodeParent;

	// Initialize fields in new node to insert.
	pNode->m_pNodeParent = pNodeParent;
	pNode->m_pNodeRight = nullptr;
	pNode->m_pNodeLeft = nullptr;
	pNode->m_eColour = Container::Internal::E_RBNodeColor::Red;

	// Insert the node.
	if (eSide == Container::Internal::E_RBNodeSide::Left)
	{
		pNodeParent->m_pNodeLeft = pNode;
		if (pNodeParent == pNodeAnchor)
		{
			pNodeAnchor->m_pNodeParent = pNode;
			pNodeAnchor->m_pNodeRight = pNode;
		}
		else if (pNodeParent == pNodeAnchor->m_pNodeLeft)
		{
			pNodeAnchor->m_pNodeLeft = pNode;
		}
	}
	else
	{
		pNodeParent->m_pNodeRight = pNode;
		if (pNodeParent == pNodeAnchor->m_pNodeRight)
		{
			pNodeAnchor->m_pNodeRight = pNode;
		}
	}

	// Rebalance the tree.
	while ((pNode != pNodeRootRef) && (pNode->m_pNodeParent->m_eColour == Container::Internal::E_RBNodeColor::Red))
	{
		Container::Internal::_RBTreeNodeBase* const pNodeParentParent = pNode->m_pNodeParent->m_pNodeParent;

		if (pNode->m_pNodeParent == pNodeParentParent->m_pNodeLeft)
		{
			Container::Internal::_RBTreeNodeBase* const pNodeTemp = pNodeParentParent->m_pNodeRight;

			if (pNodeTemp && (pNodeTemp->m_eColour == Container::Internal::E_RBNodeColor::Red))
			{
				pNode->m_pNodeParent->m_eColour = Container::Internal::E_RBNodeColor::Black;
				pNodeTemp->m_eColour = Container::Internal::E_RBNodeColor::Black;
				pNodeParentParent->m_eColour = Container::Internal::E_RBNodeColor::Red;
				pNode = pNodeParentParent;
			}
			else
			{
				if (pNode->m_pNodeParent && pNode == pNode->m_pNodeParent->m_pNodeRight)
				{
					pNode = pNode->m_pNodeParent;
					pNodeRootRef = RBTreeRotateLeft(pNode, pNodeRootRef);
				}

				pNode->m_pNodeParent->m_eColour = Container::Internal::E_RBNodeColor::Black;
				pNodeParentParent->m_eColour = Container::Internal::E_RBNodeColor::Red;
				pNodeRootRef = RBTreeRotateRight(pNodeParentParent, pNodeRootRef);
			}
		}
		else
		{
			Container::Internal::_RBTreeNodeBase* const pNodeTemp = pNodeParentParent->m_pNodeLeft;

			if (pNodeTemp && (pNodeTemp->m_eColour == Container::Internal::E_RBNodeColor::Red))
			{
				pNode->m_pNodeParent->m_eColour = Container::Internal::E_RBNodeColor::Black;
				pNodeTemp->m_eColour = Container::Internal::E_RBNodeColor::Black;
				pNodeParentParent->m_eColour = Container::Internal::E_RBNodeColor::Red;
				pNode = pNodeParentParent;
			}
			else
			{
				if (pNode == pNode->m_pNodeParent->m_pNodeLeft)
				{
					pNode = pNode->m_pNodeParent;
					pNodeRootRef = RBTreeRotateRight(pNode, pNodeRootRef);
				}

				pNode->m_pNodeParent->m_eColour = Container::Internal::E_RBNodeColor::Black;
				pNodeParentParent->m_eColour = Container::Internal::E_RBNodeColor::Red;
				pNodeRootRef = RBTreeRotateLeft(pNodeParentParent, pNodeRootRef);
			}
		}
	}
	pNodeRootRef->m_eColour = Container::Internal::E_RBNodeColor::Black;
}

void FXD::Container::Internal::RBTreeErase(Container::Internal::_RBTreeNodeBase* pNode, Container::Internal::_RBTreeNodeBase* pNodeAnchor)
{
	Container::Internal::_RBTreeNodeBase*& pNodeRootRef = pNodeAnchor->m_pNodeParent;
	Container::Internal::_RBTreeNodeBase*& pNodeLeftmostRef = pNodeAnchor->m_pNodeLeft;
	Container::Internal::_RBTreeNodeBase*& pNodeRightmostRef = pNodeAnchor->m_pNodeRight;
	Container::Internal::_RBTreeNodeBase* pNodeSuccessor = pNode;
	Container::Internal::_RBTreeNodeBase* pNodeChild = nullptr;
	Container::Internal::_RBTreeNodeBase* pNodeChildParent = nullptr;

	if (pNodeSuccessor->m_pNodeLeft == nullptr)
	{
		pNodeChild = pNodeSuccessor->m_pNodeRight;
	}
	else if (pNodeSuccessor->m_pNodeRight == nullptr)
	{
		pNodeChild = pNodeSuccessor->m_pNodeLeft;
	}
	else
	{
		pNodeSuccessor = pNodeSuccessor->m_pNodeRight;
		while (pNodeSuccessor->m_pNodeLeft)
		{
			pNodeSuccessor = pNodeSuccessor->m_pNodeLeft;
		}
		pNodeChild = pNodeSuccessor->m_pNodeRight;
	}

	if (pNodeSuccessor == pNode)
	{
		pNodeChildParent = pNodeSuccessor->m_pNodeParent;

		if (pNodeChild != nullptr)
		{
			pNodeChild->m_pNodeParent = pNodeSuccessor->m_pNodeParent;
		}
		if (pNode == pNodeRootRef)
		{
			pNodeRootRef = pNodeChild;
		}
		else
		{
			if (pNode == pNode->m_pNodeParent->m_pNodeLeft)
			{
				pNode->m_pNodeParent->m_pNodeLeft = pNodeChild;
			}
			else
			{
				pNode->m_pNodeParent->m_pNodeRight = pNodeChild;
			}
		}

		if (pNode == pNodeLeftmostRef)
		{
			if (pNode->m_pNodeRight && pNodeChild)
			{
				PRINT_COND_ASSERT((pNodeChild != nullptr), "Container: Logically pNodeChild should always be valid");
				pNodeLeftmostRef = RBTreeGetMinChild(pNodeChild);
			}
			else
			{
				pNodeLeftmostRef = pNode->m_pNodeParent;
			}
		}

		if (pNode == pNodeRightmostRef)
		{
			if (pNode->m_pNodeLeft && pNodeChild)
			{
				PRINT_COND_ASSERT((pNodeChild != nullptr), "Container: Logically pNodeChild should always be valid");
				pNodeRightmostRef = RBTreeGetMaxChild(pNodeChild);
			}
			else
			{
				pNodeRightmostRef = pNode->m_pNodeParent;
			}
		}
	}
	else
	{
		pNode->m_pNodeLeft->m_pNodeParent = pNodeSuccessor;
		pNodeSuccessor->m_pNodeLeft = pNode->m_pNodeLeft;

		if (pNodeSuccessor == pNode->m_pNodeRight)
		{
			pNodeChildParent = pNodeSuccessor;
		}
		else
		{
			pNodeChildParent = pNodeSuccessor->m_pNodeParent;

			if (pNodeChild != nullptr)
			{
				pNodeChild->m_pNodeParent = pNodeChildParent;
			}
			pNodeChildParent->m_pNodeLeft = pNodeChild;

			pNodeSuccessor->m_pNodeRight = pNode->m_pNodeRight;
			pNode->m_pNodeRight->m_pNodeParent = pNodeSuccessor;
		}

		if (pNode == pNodeRootRef)
		{
			pNodeRootRef = pNodeSuccessor;
		}
		else if (pNode == pNode->m_pNodeParent->m_pNodeLeft)
		{
			pNode->m_pNodeParent->m_pNodeLeft = pNodeSuccessor;
		}
		else
		{
			pNode->m_pNodeParent->m_pNodeRight = pNodeSuccessor;
		}

		pNodeSuccessor->m_pNodeParent = pNode->m_pNodeParent;
		FXD::STD::swap(pNodeSuccessor->m_eColour, pNode->m_eColour);
	}

	// Here we do tree balancing as per the conventional red-black tree algorithm.
	if (pNode->m_eColour == Container::Internal::E_RBNodeColor::Black)
	{
		while ((pNodeChild != pNodeRootRef) && ((pNodeChild == nullptr) || (pNodeChild->m_eColour == Container::Internal::E_RBNodeColor::Black)))
		{
			if (pNodeChild == pNodeChildParent->m_pNodeLeft)
			{
				Container::Internal::_RBTreeNodeBase* pNodeTemp = pNodeChildParent->m_pNodeRight;
				if (pNodeTemp->m_eColour == Container::Internal::E_RBNodeColor::Red)
				{
					pNodeTemp->m_eColour = Container::Internal::E_RBNodeColor::Black;
					pNodeChildParent->m_eColour = Container::Internal::E_RBNodeColor::Red;
					pNodeRootRef = RBTreeRotateLeft(pNodeChildParent, pNodeRootRef);
					pNodeTemp = pNodeChildParent->m_pNodeRight;
				}

				if (((pNodeTemp->m_pNodeLeft == nullptr) || (pNodeTemp->m_pNodeLeft->m_eColour == Container::Internal::E_RBNodeColor::Black)) &&
					((pNodeTemp->m_pNodeRight == nullptr) || (pNodeTemp->m_pNodeRight->m_eColour == Container::Internal::E_RBNodeColor::Black)))
				{
					pNodeTemp->m_eColour = Container::Internal::E_RBNodeColor::Red;
					pNodeChild = pNodeChildParent;
					pNodeChildParent = pNodeChildParent->m_pNodeParent;
				}
				else
				{
					if ((pNodeTemp->m_pNodeRight == nullptr) || (pNodeTemp->m_pNodeRight->m_eColour == Container::Internal::E_RBNodeColor::Black))
					{
						pNodeTemp->m_pNodeLeft->m_eColour = Container::Internal::E_RBNodeColor::Black;
						pNodeTemp->m_eColour = Container::Internal::E_RBNodeColor::Red;
						pNodeRootRef = RBTreeRotateRight(pNodeTemp, pNodeRootRef);
						pNodeTemp = pNodeChildParent->m_pNodeRight;
					}

					pNodeTemp->m_eColour = pNodeChildParent->m_eColour;
					pNodeChildParent->m_eColour = Container::Internal::E_RBNodeColor::Black;

					if (pNodeTemp->m_pNodeRight != nullptr)
					{
						pNodeTemp->m_pNodeRight->m_eColour = Container::Internal::E_RBNodeColor::Black;
					}
					pNodeRootRef = RBTreeRotateLeft(pNodeChildParent, pNodeRootRef);
					break;
				}
			}
			else
			{
				Container::Internal::_RBTreeNodeBase* pNodeTemp = pNodeChildParent->m_pNodeLeft;
				if (pNodeTemp->m_eColour == Container::Internal::E_RBNodeColor::Red)
				{
					pNodeTemp->m_eColour = Container::Internal::E_RBNodeColor::Black;
					pNodeChildParent->m_eColour = Container::Internal::E_RBNodeColor::Red;

					pNodeRootRef = RBTreeRotateRight(pNodeChildParent, pNodeRootRef);
					pNodeTemp = pNodeChildParent->m_pNodeLeft;
				}

				if (((pNodeTemp->m_pNodeRight == nullptr) || (pNodeTemp->m_pNodeRight->m_eColour == Container::Internal::E_RBNodeColor::Black)) &&
					((pNodeTemp->m_pNodeLeft == nullptr) || (pNodeTemp->m_pNodeLeft->m_eColour == Container::Internal::E_RBNodeColor::Black)))
				{
					pNodeTemp->m_eColour = Container::Internal::E_RBNodeColor::Red;
					pNodeChild = pNodeChildParent;
					pNodeChildParent = pNodeChildParent->m_pNodeParent;
				}
				else
				{
					if ((pNodeTemp->m_pNodeLeft == nullptr) || (pNodeTemp->m_pNodeLeft->m_eColour == Container::Internal::E_RBNodeColor::Black))
					{
						pNodeTemp->m_pNodeRight->m_eColour = Container::Internal::E_RBNodeColor::Black;
						pNodeTemp->m_eColour = Container::Internal::E_RBNodeColor::Red;

						pNodeRootRef = RBTreeRotateLeft(pNodeTemp, pNodeRootRef);
						pNodeTemp = pNodeChildParent->m_pNodeLeft;
					}

					pNodeTemp->m_eColour = pNodeChildParent->m_eColour;
					pNodeChildParent->m_eColour = Container::Internal::E_RBNodeColor::Black;

					if (pNodeTemp->m_pNodeLeft != nullptr)
					{
						pNodeTemp->m_pNodeLeft->m_eColour = Container::Internal::E_RBNodeColor::Black;
					}
					pNodeRootRef = RBTreeRotateRight(pNodeChildParent, pNodeRootRef);
					break;
				}
			}
		}
		if (pNodeChild != nullptr)
		{
			pNodeChild->m_eColour = Container::Internal::E_RBNodeColor::Black;
		}
	}
}