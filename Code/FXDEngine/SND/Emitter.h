// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_EMITTER_H
#define FXDENGINE_SND_EMITTER_H

#include "FXDEngine/Math/Matrix4.h"
#include "FXDEngine/Math/Vector3F.h"
#include "FXDEngine/SND/Types.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// IEmitter
		// -
		// ------
		class IEmitter
		{
		public:
			friend class INodeVoice;

		public:
			EXPLICIT IEmitter(const SND::ISndAdapter* pSndAdapter, const SND::INodeVoice* pNodeVoice);
			virtual ~IEmitter(void);

			bool create_emitter(void);
			bool release_emitter(void);

			Math::Vector3F position(void) const;
			Math::Vector3F up(void) const;
			Math::Vector3F forward(void) const;
			Math::Vector3F velocity(void) const;
			FXD::F32 min_distance(void) const;
			FXD::F32 max_distance(void) const;
			FXD::F32 falloff(void) const;

			void set(const Math::Matrix4F& value);
			void set_position(const Math::Vector3F& pos);
			void set_up(const Math::Vector3F& up);
			void set_forward(const Math::Vector3F& forward);
			void set_velocity(const Math::Vector3F& velocity);
			void set_min_max_distance(FXD::F32 fMin, FXD::F32 fMax);

		protected:
			virtual bool _create_emitter(void) PURE;
			virtual bool _release_emitter(void) PURE;

			virtual void _update(const SND::Listener& listener) PURE;
			virtual void _update_distance_model(void) PURE;
		
			Math::Vector3F _position(void) const;
			Math::Vector3F _up(void) const;
			Math::Vector3F _forward(void) const;
			Math::Vector3F _velocity(void) const;
			FXD::F32 _min_distance(void) const;
			FXD::F32 _max_distance(void) const;
			FXD::F32 _falloff(void) const;

			void _set(const Math::Matrix4F& value);
			void _set_position(const Math::Vector3F& pos);
			void _set_up(const Math::Vector3F& up);
			void _setForward(const Math::Vector3F& forward);
			void _set_velocity(const Math::Vector3F& velocity);
			void _set_min_max_distance(FXD::F32 fMin, FXD::F32 fMax);
			
			// Data Members
			const SND::ISndAdapter* m_pSndAdapter;
			const SND::INodeVoice* m_pNodeVoice;
			FXD::F32 m_fMinDistance;
			FXD::F32 m_fMaxDistance;
			Math::Matrix4F m_matrix;
			Math::Vector3F m_velocity;
		};

	} //namespace SND
} //namespace FXD
#endif //FXDENGINE_SND_EMITTER_H