// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_LISTENER_H
#define FXDENGINE_SND_LISTENER_H

#include "FXDEngine/Container/DirtyType.h"
#include "FXDEngine/Math/Matrix4.h"
#include "FXDEngine/Math/Vector3F.h"
#include "FXDEngine/SND/Types.h"
#include "FXDEngine/Thread/RWLock.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// IListener
		// -
		// ------
		class IListener
		{
		public:
			friend class ISndAdapter;

			// ------
			// _ListenerData
			// -
			// ------
			class _ListenerData
			{
			public:
				_ListenerData(void);
				~_ListenerData(void);

			public:
				Math::Matrix4F m_matrix;
				Math::Vector3F m_velocity;
				Container::DirtyType< SND::E_SndDistanceModel > m_eDistanceModel;
				Container::DirtyType< FXD::F32 > m_fDopplerFactor;
				Container::DirtyType< FXD::F32 > m_fRolloffFactor;
			};

		public:
			EXPLICIT IListener(const SND::ISndAdapter* pSndAdapter);
			virtual ~IListener(void);

			bool create(void);
			bool release(void);

			void set_position(const Math::Vector3F& pos);
			void set_up(const Math::Vector3F& up);
			void set_forward(const Math::Vector3F& forward);
			void set_velocity(const Math::Vector3F& velocity);
			void set_distance_model(SND::E_SndDistanceModel eDistanceModel);
			void set_doppler_factor(FXD::F32 fFactor);
			void set_rolloff_factor(FXD::F32 fFactor);

			Math::Vector3F position(void) const;
			Math::Vector3F up(void) const;
			Math::Vector3F forward(void) const;
			Math::Vector3F velocity(void) const;
			SND::E_SndDistanceModel distance_model(void) const;
			FXD::F32 doppler_factor(void) const;
			FXD::F32 rolloff_factor(void) const;

		protected:
			virtual bool _create(void) PURE;
			virtual bool _release(void) PURE;

			virtual void _update(FXD::F32 dt) PURE;

			virtual void _set_distance_model(SND::E_SndDistanceModel eDistanceModel) PURE;
			virtual void _set_doppler_factor(FXD::F32 fFactor) PURE;
			virtual void _set_rolloff_factor(FXD::F32 fFactor) PURE;

		protected:
			const SND::ISndAdapter* m_pSndAdapter;
			Thread::RWLockData< _ListenerData > m_listenerData;
		};

	} //namespace SND
} //namespace FXD
#endif //FXDENGINE_SND_LISTENER_H