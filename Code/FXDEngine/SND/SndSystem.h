// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_SNDSYSTEM_H
#define FXDENGINE_SND_SNDSYSTEM_H

#include "FXDEngine/SND/SndApi.h"
#include "FXDEngine/IO/FileCache.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// ISndSystem
		// - 
		// ------
		class ISndSystem FINAL : public Core::NonCopyable
		{
		public:
			friend class ISndJPThread;

		public:
			ISndSystem(void);
			~ISndSystem(void);

			Job::Future< bool > create_system(void);
			Job::Future< bool > release_system(void);

			Job::Future< SND::SndAdapter > create_adapter(const App::AdapterDesc& adapterDesc);
			Job::Future< bool > release_adapter(SND::SndAdapter& sndAdapter);

			Job::Future< SND::Audio > load_audio(const IO::Path& ioPath);
			void load_audio_async(const IO::Path& ioPath, FXD::STD::function< void(SND::Audio) > callback);

			GET_REF_W(SND::SndApi, sndApi, snd_api);
			GET_REF_W(IO::FileCache< SND::Audio >, audioCache, audio_cache);

		private:
			bool _create_system(void);
			bool _release_system(void);

			SND::SndAdapter _create_adapter(const App::AdapterDesc& adapterDesc);
			bool _release_adapter(SND::SndAdapter& sndAdapter);

			void _process(FXD::F32 dt);

		private:
			SND::SndApi m_sndApi;
			IO::FileCache< SND::Audio > m_audioCache;
			Container::Vector< SND::SndAdapter > m_sndAdapters;
		};

		namespace Funcs
		{
			extern FXD::S64 VolumeToDeciBels(FXD::F32 fVolume);									// Converts and volume (0.0f to 1.0f) to a deciBel value
			extern FXD::S64 VolumeToMilliBels(FXD::F32 fVolume, FXD::S32 nMaxMilliBels);	// Converts and volume (0.0f to 1.0f) to a MilliBel value (a Hundredth of a deciBel)
		} //namespace Funcs

	} //namespace SND
} //namespace FXD
#endif //FXDENGINE_SND_SNDSYSTEM_H