// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_EFFECT_H
#define FXDENGINE_SND_EFFECT_H

#include "FXDEngine/Container/Vector.h"
#include "FXDEngine/Core/StreamIn.h"
#include "FXDEngine/SND/Types.h"

namespace FXD
{
	namespace SND
	{
		// Default Values
		static const FXD::F32 kReverbDefaultWetDryMix = 100.0f;		// [0, 100] (percentage)
		static const FXD::F32 kReverbGain = 0.32f;						// [0, 1]
		static const FXD::F32 kReverbGainHF = 0.89f;						// [0, 1]
		static const FXD::F32 kReverbRoomRollOff = 0.0f;				// [0.0, 10.0]
		static const FXD::F32 kReverbDecayTime = 1.49f;					// [0.1, 20.0] (seconds)
		static const FXD::F32 kReverbDecayTimeHF = 0.83f;				// [0.1, 2.0]
		static const FXD::F32 kReverbReflectionGain = 0.05f;			// [0.0, 3.16]
		static const FXD::F32 kReverbReflectionDelay = 0.007f;		// [0.1, 3.0] (seconds)
		static const FXD::F32 kReverbLateGain = 1.26f;					// [0.0, 10.0]
		static const FXD::F32 kReverbLateDelay = 0.011f;				// [0.0, 0.1] (seconds)
		static const FXD::F32 kReverbLateDiffusion = 1.0f;				// [0.0, 1.0]
		static const FXD::F32 kReverbLateDensity = 1.0f;				// [0.0, 1.0]
		static const FXD::F32 kReverbLateAirAbsorption = 0.994f;		// [0.0892, 1.0]
		static const FXD::F32 kReverbLateRolloffFactor = 0.0f;		// [0.0, 10.00]

		// ------
		// EFFECT_DESC
		// -
		// ------
		struct EFFECT_DESC
		{
		public:
			// Default Presets
			static const EFFECT_DESC kPresetGeneric;
			static const EFFECT_DESC kPresetPaddedCell;
			static const EFFECT_DESC kPresetRoom;
			static const EFFECT_DESC kPresetBathroom;
			static const EFFECT_DESC kPresetLivingRoom;
			static const EFFECT_DESC kPresetStoneRoom;
			static const EFFECT_DESC kPresetAuditorium;
			static const EFFECT_DESC kPresetConcertHall;
			static const EFFECT_DESC kPresetCave;
			static const EFFECT_DESC kPresetArena;
			static const EFFECT_DESC kPresetHangar;
			static const EFFECT_DESC kPresetCarpetedHallway;
			static const EFFECT_DESC kPresetHallway;
			static const EFFECT_DESC kPresetStoneCorridor;
			static const EFFECT_DESC kPresetAlley;
			static const EFFECT_DESC kPresetForest;
			static const EFFECT_DESC kPresetCity;
			static const EFFECT_DESC kPresetMountains;
			static const EFFECT_DESC kPresetQuarry;
			static const EFFECT_DESC kPresetPlain;
			static const EFFECT_DESC kPresetParkingLot;
			static const EFFECT_DESC kPresetSewerPipe;
			static const EFFECT_DESC kPresetUnderwater;
			static const EFFECT_DESC kPresetSmallRoom;
			static const EFFECT_DESC kPresetMediumRoom;
			static const EFFECT_DESC kPresetLargeRoom;
			static const EFFECT_DESC kPresetMedumHall;
			static const EFFECT_DESC kPresetLargeHall;
			static const EFFECT_DESC kPresetPlate;
			static const EFFECT_DESC kPresetDrugged;
			static const EFFECT_DESC kPresetDizzy;
			static const EFFECT_DESC kPresetPsychotic;

			// Sports Presets
			static const EFFECT_DESC kPresetSportEmptyStadium;
			static const EFFECT_DESC kPresetSportFullStadium;
			static const EFFECT_DESC kPresetSportSquashCourt;
			static const EFFECT_DESC kPresetSportSmallSwimmingPool;
			static const EFFECT_DESC kPresetSportLargeSwimmingPool;
			static const EFFECT_DESC kPresetSportGymnasium;
			static const EFFECT_DESC kPresetSportStadiumTannoy;

			// Prefab Presets
			static const EFFECT_DESC kPresetPrefabWorkshop;
			static const EFFECT_DESC kPresetPrefabSchoolRoom;
			static const EFFECT_DESC kPresetPrefabPractiseRoom;
			static const EFFECT_DESC kPresetPrefabOuthouse;
			static const EFFECT_DESC kPresetPrefabCaravan;

			// Mood Presets
			static const EFFECT_DESC kPresetMoodHeaven;
			static const EFFECT_DESC kPresetMoodHell;
			static const EFFECT_DESC kPresetMoodMemory;

			// Driving Presets
			static const EFFECT_DESC kPresetDrivingCommentator;
			static const EFFECT_DESC kPresetDrivingPitGarage;
			static const EFFECT_DESC kPresetDrivingInCarRacer;
			static const EFFECT_DESC kPresetDrivingInCarSports;
			static const EFFECT_DESC kPresetDrivingInCarLuxury;
			static const EFFECT_DESC kPresetDrivingEmptyStand;
			static const EFFECT_DESC kPresetDrivingFullStand;
			static const EFFECT_DESC kPresetDrivingTunnel;

			// Misc. Presets
			static const EFFECT_DESC kPresetDustyRoom;
			static const EFFECT_DESC kPresetChapel;
			static const EFFECT_DESC kPresetSmallWaterRoom;

			EFFECT_DESC(
				bool bEnabled = true,
				FXD::F32 fWetDryMix = kReverbDefaultWetDryMix,
				FXD::F32 fRoom = kReverbGain,
				FXD::F32 fRoomHF = kReverbGainHF,
				FXD::F32 fDecayTime = kReverbDecayTime,
				FXD::F32 fDecayHFRatio = kReverbDecayTimeHF,
				FXD::F32 fReflections = kReverbReflectionGain,
				FXD::F32 fReflectionsDelay = kReverbReflectionDelay,
				FXD::F32 fReverb = kReverbLateGain,
				FXD::F32 fReverbDelay = kReverbLateDelay,
				FXD::F32 fDiffusion = kReverbLateDiffusion,
				FXD::F32 fDensity = kReverbLateDensity,
				FXD::F32 fAirAbsorption = kReverbLateAirAbsorption,
				FXD::F32 fRoomRolloffFactor = kReverbRoomRollOff);

			bool Enabled;
			FXD::F32 WetDryMix;				// 0.0 to 1.0
			FXD::F32 Volume;					// 0.0 to 1.0
			FXD::F32 Gain;						// 0.0 < 0.32 < 1.0 
			FXD::F32 GainHF;					// 0.0 < 0.89 < 1.0
			FXD::F32 DecayTime;				// 0.1 < 1.49 < 20.0	Seconds
			FXD::F32 DecayHFRatio;			// 0.1 < 0.83 < 2.0
			FXD::F32 ReflectionsGain;		// 0.0 < 0.05 < 3.16
			FXD::F32 ReflectionsDelay;		// 0.0 < 0.007 < 0.3	Seconds
			FXD::F32 LateGain;				// 0.0 < 1.26 < 10.0
			FXD::F32 LateDelay;				// 0.0 < 0.011 < 0.1	Seconds
			FXD::F32 Diffusion;				// 0.0 < 1.0 < 1.0
			FXD::F32 Density;					// 0.0 < 1.0 < 1.0
			FXD::F32 AirAbsorptionGainHF;	// 0.892 < 0.994 < 1.0
			FXD::F32 RoomRolloffFactor;	// 0.0 < 0.0 < 10.0
		};

		// ------
		// IEffect
		// -
		// ------
		class IEffect
		{
		public:
			friend class IEffectChain;
			friend class IEffectChainXA2;

		public:
			EXPLICIT IEffect(const EFFECT_DESC& effectDesc);
			virtual ~IEffect(void);

			SET(bool, bDestroyed, destroyed);

			GET_W(bool, bDestroyed, destroyed);
			GET_REF_W(EFFECT_DESC, effectDesc, effect_desc);

		protected:
			bool m_bDestroyed;
			SND::EFFECT_DESC m_effectDesc;
		};

		// ------
		// IEffectChain
		// -
		// ------
		class IEffectChain
		{
		public:
			friend class INodeMixer;
			static bool FromStream(Core::StreamIn& stream, SND::IEffectChain* pEffectChain);

		public:
			EXPLICIT IEffectChain(const SND::ISndAdapter* pSndAdapter, SND::INodeMixer* pNodeMixer);
			virtual ~IEffectChain(void);

			bool create_effect_chain(void);
			bool release_effect_chain(void);

			void add_effects(const Container::Vector< SND::EFFECT_DESC >& effectDescs);
			void add_effect(const SND::EFFECT_DESC& effectDesc);
			void remove_effects(void);
			void remove_effect(FXD::U16 nSlot);

			void set_effect(const SND::EFFECT_DESC& effectDef, FXD::U16 nSlot);
			void enable_effect(FXD::U16 nSlot, bool bEnable);
			bool is_effect_enabled(FXD::U16 nSlot) const;

		protected:
			virtual bool _create_effect_chain(void) PURE;
			virtual bool _release_effect_chain(void) PURE;

			virtual void _add_effects(const Container::Vector< SND::EFFECT_DESC >& effectDescs) PURE;
			void _remove_effects(void);
			void _remove_effect(FXD::U16 nSlot);

			void _set_effect(const SND::EFFECT_DESC& effectDesc, FXD::U16 nSlot);
			virtual void _enable_effect(FXD::U16 nSlot, bool bEnable) PURE;
			virtual bool _is_effect_enabled(FXD::U16 nSlot) const PURE;

			virtual void _update(FXD::F32 dt) PURE;
			virtual void _recreate(void) PURE;

		protected:
			const SND::ISndAdapter* m_pSndAdapter;
			SND::INodeMixer* m_pNodeMixer;
			bool m_bNeedsUpdate;
			bool m_bNeedsRecreate;
			Container::Vector< SND::Effect > m_effects;
		};

	} //namespace SND
} //namespace FXD
#endif //FXDENGINE_SND_EFFECT_H