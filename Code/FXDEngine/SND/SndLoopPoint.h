// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_LOOPPOINT_H
#define FXDENGINE_SND_LOOPPOINT_H

#include "FXDEngine/Container/Map.h"
#include "FXDEngine/SND/Types.h"
#include "FXDEngine/Core/String.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// LOOP_POINT_DESC
		// -
		// ------
		struct LOOP_POINT_DESC
		{
			friend class SndLoopPointTagCB;

			bool isInfinate(void) const;
						
			FXD::S32 RepeatMax = 1;
			FXD::F32 StartPosition = 0.0f;
			FXD::F32 EndPosition = 0.0f;
			Core::String8 Name;
			Core::String8 NextName;
		};

		// ------
		// LoopPointHandle
		// -
		// ------
		class LoopPointHandle
		{
		public:
			friend class ISndLoopGroup;

		public:
			LoopPointHandle(void);
			~LoopPointHandle(void);

		private:
			void _incrementPlayed(void);
			void _resetPlayedCount(void);
			bool _is_finished(void) const;

		public:
			const SND::LOOP_POINT_DESC* m_pLoopPoint;
			FXD::S32 m_nRepeatCount;
			FXD::U32 m_nLoopPointBytesRead;
		};

		// ------
		// ISndLoopGroup
		// -
		// ------
		class ISndLoopGroup FINAL : public Core::NonCopyable
		{
		public:
			friend class SndLoopPointTagCB;

		public:
			static SND::SndLoopGroup FromStream(Core::StreamIn& stream);

		public:
			ISndLoopGroup(void);
			virtual ~ISndLoopGroup(void);

			void addLoopPoint(const SND::LOOP_POINT_DESC& loopDesc);

			SND::LoopPointHandle begin(void) const;
			SND::LoopPointHandle next(SND::LoopPointHandle& loopPointHandle) const;

			SET_REF(Core::String8, strVoiceName, voiceName);
			SET_REF(Core::String8, strFirstLoopPointName, firstLoopPointName);

			GET_REF_R(Core::String8, strVoiceName, voiceName);
			GET_REF_R(Core::String8, strFirstLoopPointName, firstLoopPointName);

			Core::String8 m_strVoiceName;
			Core::String8 m_strFirstLoopPointName;
			Container::Map< const FXD::HashValue, SND::LOOP_POINT_DESC > m_loopDescs;
		};

	} //namespace SND
} //namespace FXD
#endif //FXDENGINE_SND_LOOPPOINT_H