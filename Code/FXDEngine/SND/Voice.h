// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_VOICE_H
#define FXDENGINE_SND_VOICE_H

#include "FXDEngine/Memory/MemHandle.h"
#include "FXDEngine/SND/SndLoopPoint.h"

namespace FXD
{
	namespace SND
	{
		struct VOICE_DESC
		{
			SND::Audio Audio = SND::Audio();
			SND::SndLoopGroup LoopGroup = SND::SndLoopGroup();
			SND::E_CreateFlags CreateFlag;
		};

		// ------
		// IVoice
		// -
		// ------
		class IVoice : public Core::NonCopyable
		{
		public:
			friend class INodeMixer;
			friend class INodeVoice;
			friend class INodeVoiceOAL;
			friend class INodeVoiceOSL;
			friend class INodeVoiceXA2;

		protected:
			EXPLICIT IVoice(const SND::ISndAdapter* pSndAdapter, SND::INodeVoice* pNodeVoice);
			virtual ~IVoice(void);

			virtual bool create(const SND::VOICE_DESC& voiceDesc) PURE;
			virtual void release(void) PURE;

			virtual void play(void) PURE;
			virtual void pause(void) PURE;
			virtual void stop(void) PURE;

			bool is_finished(void) const;

			virtual FXD::U32 tell_sample_position(void) const PURE;
			FXD::F32 tell_playback_position(void) const;
			FXD::F32 get_playback_length(void) const;
			Core::String8 tell_playback_position_string(void) const;

			void _update(FXD::F32 dt);
			virtual void _update_buffers(void) PURE;
			void _update_loop_point(void);

			virtual void _buffer_fill(void) PURE;
			virtual void _buffer_submit(FXD::U32 nSize) PURE;
			virtual void _buffers_flush(void) PURE;
			virtual FXD::U32 _buffer_queued_count(void) const PURE;
			virtual void _buffers_finished(void) PURE;

			void _ensure_loop_points_defined(const SND::E_CreateFlags& eCreateFlag, const SND::Audio& audio);

		public:
			const SND::ISndAdapter* m_pSndAdapter;
			SND::INodeVoice* m_pVoiceNode;
			SND::LoopPointHandle m_loopHandle;
			SND::VOICE_DESC m_voiceDesc;
			FXD::U32 m_nBytesStartPos;
			FXD::U32 m_nBytesSize;
			bool m_bStreamComplete;
		};

		// ------
		// IVoiceSFX
		// -
		// ------
		class IVoiceSFX : public SND::IVoice
		{
		public:
			static const FXD::U32 BufferCount = kAudioSFXBufferCount;

		public:
			EXPLICIT IVoiceSFX(const SND::ISndAdapter* pSndAdapter, SND::INodeVoice* pNodeVoice);
			virtual ~IVoiceSFX(void);

		protected:
			virtual bool create(const SND::VOICE_DESC& voiceDesc) PURE;
			virtual void release(void) PURE;

			virtual void play(void) PURE;
			virtual void pause(void) PURE;
			virtual void stop(void) PURE;

			virtual FXD::U32 tell_sample_position(void) const PURE;

			virtual void _update_buffers(void) PURE;

			virtual void _buffer_fill(void) PURE;
			virtual void _buffer_submit(FXD::U32 nSize) PURE;
			virtual void _buffers_flush(void) PURE;
			virtual FXD::U32 _buffer_queued_count(void) const PURE;
			virtual void _buffers_finished(void) PURE;

		protected:
			FXD::U32 m_nCurrentBuffer;
			//FXD::U8* m_pBuffer[kAudioMusicBufferCount];
			Memory::MemHandle m_dataBuffer;
		};

		// ------
		// IVoiceMusic
		// -
		// ------
		class IVoiceMusic : public SND::IVoice
		{
		public:
			static const FXD::U32 BufferCount = kAudioMusicBufferCount;

		public:
			EXPLICIT IVoiceMusic(const SND::ISndAdapter* pSndAdapter, SND::INodeVoice* pNodeVoice);
			virtual ~IVoiceMusic(void);

		protected:
			virtual bool create(const SND::VOICE_DESC& voiceDesc) PURE;
			virtual void release(void) PURE;

			virtual void play(void) PURE;
			virtual void pause(void) PURE;
			virtual void stop(void) PURE;

			virtual FXD::U32 tell_sample_position(void) const PURE;

			virtual void _update_buffers(void) PURE;

			virtual void _buffer_fill(void) PURE;
			virtual void _buffer_submit(FXD::U32 nSize) PURE;
			virtual void _buffers_flush(void) PURE;
			virtual FXD::U32 _buffer_queued_count(void) const PURE;
			virtual void _buffers_finished(void) PURE;

		protected:
			FXD::U32 m_nCurrentBuffer;
			Memory::MemHandle m_memBuffers[kAudioMusicBufferCount];
		};

	} //namespace SND
} //namespace FXD
#endif //FXDENGINE_SND_VOICE_H