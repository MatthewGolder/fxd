// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_OAL_VOICEOAL_H
#define FXDENGINE_SND_OAL_VOICEOAL_H

#include "FXDEngine/SND/Types.h"

#if IsSndOpenAL()
#include "FXDEngine/SND/Voice.h"
#include "FXDEngine/SND/OAL/OpenAL.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// FXDBUFFEROAL
		// -
		// ------
		class FXDBUFFEROAL
		{
		public:
			enum class State
			{
				Free,
				Playing,
				Finished,
				Count,
				Unknown = 0xffff
			};

		public:
			FXDBUFFEROAL(void)
				: m_eState(State::Free)
				, m_nID(0)
				, m_nSampleStart(0)
				, m_nSampleOffset(0)
				, m_alBuffer(0)
			{
			}
			~FXDBUFFEROAL(void)
			{
				PRINT_COND_ASSERT((m_alBuffer == 0), "SND: m_alBuffer was not cleaned up before shutdown");
			}

		public:
			State m_eState;
			FXD::U32 m_nID;
			FXD::U32 m_nSampleStart;
			FXD::U32 m_nSampleOffset;
			ALuint m_alBuffer;
		};

		// ------
		// IVoiceSFXOAL
		// -
		// ------
		class IVoiceSFXOAL FINAL : public SND::IVoiceSFX
		{
		public:
			EXPLICIT IVoiceSFXOAL(const SND::ISndAdapter* pSndAdapter, SND::INodeVoice* pNodeVoice);
			~IVoiceSFXOAL(void);

		protected:
			bool create(const SND::VOICE_DESC& voiceDesc) FINAL;
			void release(void) FINAL;

			void play(void) FINAL;
			void pause(void) FINAL;
			void stop(void) FINAL;

			FXD::U32 tell_sample_position(void) const FINAL;

			void _update_buffers(void) FINAL;

			void _buffer_fill(void) FINAL;
			void _buffer_submit(FXD::U32 nSize) FINAL;
			void _buffers_flush(void) FINAL;
			FXD::U32 _buffer_queued_count(void) const FINAL;
			void _buffers_finished(void) FINAL;

		protected:
			FXDBUFFEROAL m_buffers[BufferCount];
			Container::Vector< FXDBUFFEROAL* > m_queue;
		};

		// ------
		// IVoiceMusicOAL
		// -
		// ------
		class IVoiceMusicOAL FINAL : public SND::IVoiceMusic
		{
		public:
			EXPLICIT IVoiceMusicOAL(const SND::ISndAdapter* pSndAdapter, SND::INodeVoice* pNodeVoice);
			~IVoiceMusicOAL(void);

		protected:
			bool create(const SND::VOICE_DESC& voiceDesc) FINAL;
			void release(void) FINAL;

			void play(void) FINAL;
			void pause(void) FINAL;
			void stop(void) FINAL;

			FXD::U32 tell_sample_position(void) const FINAL;

			void _update_buffers(void) FINAL;

			void _buffer_fill(void) FINAL;
			void _buffer_submit(FXD::U32 nSize) FINAL;
			void _buffers_flush(void) FINAL;
			FXD::U32 _buffer_queued_count(void) const FINAL;
			void _buffers_finished(void) FINAL;

		protected:
			FXDBUFFEROAL m_buffers[BufferCount];
			Container::Vector< FXDBUFFEROAL* > m_queue;
		};

	} //namespace SND
} //namespace FXD
#endif //IsSndOpenAL()
#endif //FXDENGINE_SND_OAL_VOICEOAL_H