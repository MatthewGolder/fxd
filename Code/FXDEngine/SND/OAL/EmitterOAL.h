// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_OAL_EMITTEROAL_H
#define FXDENGINE_SND_OAL_EMITTEROAL_H

#include "FXDEngine/SND/Types.h"

#if IsSndOpenAL()
#include "FXDEngine/SND/Emitter.h"
#include "FXDEngine/SND/OAL/OpenAL.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// IEmitterOAL
		// -
		// ------
		class IEmitterOAL FINAL : public IEmitter
		{
		public:
			EXPLICIT IEmitterOAL(const SND::ISndAdapter* pSndAdapter, const SND::INodeVoice* pNodeVoice);
			~IEmitterOAL(void);

		protected:
			bool _create_emitter(void) FINAL;
			bool _release_emitter(void) FINAL;

			void _update(const SND::Listener& listener) FINAL;
			void _update_distance_model(void) FINAL;
		};

	} //namespace SND
} //namespace FXD
#endif //IsSndOpenAL()
#endif //FXDENGINE_SND_OAL_EMITTEROAL_H