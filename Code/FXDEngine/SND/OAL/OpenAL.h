// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_OAL_OPENAL_H
#define FXDENGINE_SND_OAL_OPENAL_H

#include "FXDEngine/SND/Types.h"

#if IsOSPC() && IsSndOpenAL()
#define AL_NO_PROTOTYPES 1
#define ALC_NO_PROTOTYPES 1
#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alext.h>
#include <AL/efx.h>
#include <AL/efx-creative.h>
#include <AL/efx-presets.h>
#if defined(_WIN64)
	#pragma comment(lib,"OpenAL32.lib")
	#pragma comment(lib,"EFX-Util.lib")
#else
	#pragma comment(lib,"OpenAL32.lib")
	#pragma comment(lib,"EFX-Util.lib")
#endif

#endif

namespace FXD
{
	namespace SND
	{
		// ------
		// CheckOALResult
		// -
		// ------
		class CheckOALResult
		{
		public:
			CheckOALResult(const FXD::UTF8* pText, const FXD::UTF8* pFile, FXD::U32 nLine);

			bool operator()();

			GET_R(ALenum, alError, alError);

		private:
			ALenum m_alError;
			FXD::U32 m_nLine;
			const FXD::UTF8* m_pText;
			const FXD::UTF8* m_pFile;
		};
		#define OALResultCheckFail(c) FXD::SND::CheckOALResult(#c, __FILE__, __LINE__)()
		#define OALResultCheckSuccess() !OALResultCheckFail(c)

		enum ALCaps
		{
			ALCapture = 0,
			ALEFX,
			ALOffset,
			ALLinearDistance,
			ALExponentDistance,
			ALEAX2,
			ALEAX3,
			ALEAX4,
			ALEAX5,
			ALEAXRAM
		};

		class ISndAdapterOAL;
		class ISndAdapterDescOAL;
		typedef std::shared_ptr< ISndAdapterOAL > SndAdapterOAL;
		typedef std::shared_ptr< ISndAdapterDescOAL > SndAdapterDescOAL;

		class IVoiceSFXOAL;
		class IVoiceMusicOAL;
		typedef std::shared_ptr< IVoiceSFXOAL > VoiceSFXOAL;
		typedef std::shared_ptr< IVoiceMusicOAL > VoiceMusicOAL;

		class INodeDataOAL;
		class INodeMixerOAL;
		class INodeVoiceOAL;
		typedef std::shared_ptr< INodeDataOAL > NodeDataOAL;
		typedef std::shared_ptr< INodeMixerOAL > NodeMixerOAL;
		typedef std::shared_ptr< INodeVoiceOAL > NodeVoiceOAL;

		class IListenerOAL;
		typedef std::shared_ptr< IListenerOAL > ListenerOAL;

		class IEmitterOAL;
		typedef std::shared_ptr< IEmitterOAL > EmitterOAL;

		class IEffectOAL;
		class IEffectChainOAL;
		typedef std::shared_ptr< IEffectOAL > EffectOAL;
		typedef std::shared_ptr< IEffectChainOAL > EffectChainOAL;

		// ------
		// OAL
		// -
		// ------
		namespace OAL
		{
			extern ALenum GetALFormat(const SND::SndFormat& fmt);
			extern bool CheckExtension(ALCdevice* pDevice, const FXD::UTF8* pExtension);
			extern void PrintExtension(ALCdevice* pDevice);
		} //namespace OAL

	} //namespace SND
} //namespace FXD
#endif //FXDENGINE_SND_OAL_OPENAL_H