// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_OAL_LISTENEROAL_H
#define FXDENGINE_SND_OAL_LISTENEROAL_H

#include "FXDEngine/SND/Types.h"

#if IsSndOpenAL()
#include "FXDEngine/SND/Listener.h"
#include "FXDEngine/SND/OAL/OpenAL.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// IListenerOAL
		// -
		// ------
		class IListenerOAL FINAL : public IListener
		{
		public:
			EXPLICIT IListenerOAL(const SND::ISndAdapter* pSndAdapter);
			~IListenerOAL(void);

		private:
			bool _create(void) FINAL;
			bool _release(void) FINAL;

			void _update(FXD::F32 dt) FINAL;

			void _set_distance_model(SND::E_SndDistanceModel eDistanceModel) FINAL;
			void _set_doppler_factor(FXD::F32 fFactor) FINAL;
			void _set_rolloff_factor(FXD::F32 fFactor) FINAL;
			void _set_rolloff_factorInternal(FXD::F32 fFactor);

		private:
			FXD::F32 m_fUserRolloffFactor;
		};

	} //namespace SND
} //namespace FXD
#endif //IsSndOpenAL()
#endif //FXDENGINE_SND_OAL_LISTENEROAL_H