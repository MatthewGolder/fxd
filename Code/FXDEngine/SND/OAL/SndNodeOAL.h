// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_OAL_SNDNODEOAL_H
#define FXDENGINE_SND_OAL_SNDNODEOAL_H

#include "FXDEngine/SND/Types.h"

#if IsSndOpenAL()
#include "FXDEngine/SND/SndNode.h"
#include "FXDEngine/SND/OAL/OpenAL.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// INodeDataOAL
		// -
		// ------
		class INodeDataOAL FINAL : public SND::INodeData
		{
		public:
			INodeDataOAL(void);
			virtual ~INodeDataOAL(void);

		public:
			ALuint m_alEffect;
		};

		// ------
		// INodeMasterOAL
		// -
		// ------
		class INodeMixerOAL FINAL : public SND::INodeMixer
		{
		public:
			EXPLICIT INodeMixerOAL(const SND::ISndAdapter* pSndAdapter, const SND::INodeMixer* pNodeParent);
			virtual ~INodeMixerOAL(void);

		protected:
			void _create(const SND::NODE_DESC& nodeDesc) FINAL;
			void _release(void) FINAL;			

			void _update_volume(FXD::F32 dt) FINAL;
		};

		// ------
		// INodeVoiceOAL
		// -
		// ------
		class INodeVoiceOAL FINAL : public SND::INodeVoice
		{
		public:
			EXPLICIT INodeVoiceOAL(const SND::ISndAdapter* pSndAdapter, const SND::INodeMixer* pNodeParent);
			virtual ~INodeVoiceOAL(void);

		protected:
			void _create(const SND::NODE_DESC& nodeDesc, const SND::NODE_VOICE_DESC& nodeVoiceDesc) FINAL;
			void _release(void) FINAL;

			bool _is_valid(void) const FINAL;

			void _update_volume(FXD::F32 dt) FINAL;
			void _update_frequency(FXD::F32 dt) FINAL;

		public:
			ALuint m_alSource;
		};

	} //namespace SND
} //namespace FXD
#endif //IsSndOpenAL()
#endif //FXDENGINE_SND_OAL_SNDNODEOAL_H