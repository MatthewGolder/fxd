// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_OAL_EFFECTOAL_H
#define FXDENGINE_SND_OAL_EFFECTOAL_H

#include "FXDEngine/SND/Types.h"

#if IsSndOpenAL()
#include "FXDEngine/SND/Effect.h"
#include "FXDEngine/SND/OAL/OpenAL.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// IEffectOAL
		// -
		// ------
		class IEffectOAL FINAL : public IEffect
		{
		public:
			EXPLICIT IEffectOAL(const EFFECT_DESC& effectDesc);
			~IEffectOAL(void);

		public:
			ALuint m_alEffectSlot;
		};

		// ------
		// IEffectChainOAL
		// -
		// ------
		class IEffectChainOAL FINAL : public IEffectChain
		{
		public:
			EXPLICIT IEffectChainOAL(const SND::ISndAdapter* pSndAdapter, SND::INodeMixer* pNodeMixer);
			~IEffectChainOAL(void);

		protected:
			bool _create_effect_chain(void) FINAL;
			bool _release_effect_chain(void) FINAL;

			void _add_effects(const Container::Vector< SND::EFFECT_DESC >& effectDescs) FINAL;
			void _enable_effect(FXD::U16 nSlot, bool bEnable) FINAL;
			bool _is_effect_enabled(FXD::U16 nSlot) const FINAL;

			void _update(FXD::F32 dt) FINAL;
			void _recreate(void) FINAL;
		};

	} //namespace SND
} //namespace FXD
#endif //IsSndOpenAL()
#endif //FXDENGINE_SND_OAL_EFFECTOAL_H