// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndOpenAL()
#include "FXDEngine/SND/OAL/SndApiOAL.h"
#include "FXDEngine/SND/OAL/SndAdapterOAL.h"

// Effect objects
LPALGENEFFECTS alGenEffects = nullptr;
LPALDELETEEFFECTS alDeleteEffects = nullptr;
LPALISEFFECT alIsEffect = nullptr;
LPALEFFECTI alEffecti = nullptr;
LPALEFFECTIV alEffectiv = nullptr;
LPALEFFECTF alEffectf = nullptr;
LPALEFFECTFV alEffectfv = nullptr;
LPALGETEFFECTI alGetEffecti = nullptr;
LPALGETEFFECTIV alGetEffectiv = nullptr;
LPALGETEFFECTF alGetEffectf = nullptr;
LPALGETEFFECTFV alGetEffectfv = nullptr;

//Filter objects
LPALGENFILTERS alGenFilters = nullptr;
LPALDELETEFILTERS alDeleteFilters = nullptr;
LPALISFILTER alIsFilter = nullptr;
LPALFILTERI alFilteri = nullptr;
LPALFILTERIV alFilteriv = nullptr;
LPALFILTERF alFilterf = nullptr;
LPALFILTERFV alFilterfv = nullptr;
LPALGETFILTERI alGetFilteri = nullptr;
LPALGETFILTERIV alGetFilteriv = nullptr;
LPALGETFILTERF alGetFilterf = nullptr;
LPALGETFILTERFV alGetFilterfv = nullptr;

// Auxiliary slot object
LPALGENAUXILIARYEFFECTSLOTS alGenAuxiliaryEffectSlots = nullptr;
LPALDELETEAUXILIARYEFFECTSLOTS alDeleteAuxiliaryEffectSlots = nullptr;
LPALISAUXILIARYEFFECTSLOT alIsAuxiliaryEffectSlot = nullptr;
LPALAUXILIARYEFFECTSLOTI alAuxiliaryEffectSloti = nullptr;
LPALAUXILIARYEFFECTSLOTIV alAuxiliaryEffectSlotiv = nullptr;
LPALAUXILIARYEFFECTSLOTF alAuxiliaryEffectSlotf = nullptr;
LPALAUXILIARYEFFECTSLOTFV alAuxiliaryEffectSlotfv = nullptr;
LPALGETAUXILIARYEFFECTSLOTI alGetAuxiliaryEffectSloti = nullptr;
LPALGETAUXILIARYEFFECTSLOTIV alGetAuxiliaryEffectSlotiv = nullptr;
LPALGETAUXILIARYEFFECTSLOTF alGetAuxiliaryEffectSlotf = nullptr;
LPALGETAUXILIARYEFFECTSLOTFV alGetAuxiliaryEffectSlotfv = nullptr;

using namespace FXD;
using namespace SND;

// ------
// ISndApiOAL
// - 
// ------
ISndApiOAL::ISndApiOAL(void)
	: ISndApi(SND::E_SndApi::OpenAL)
{
}

ISndApiOAL::~ISndApiOAL(void)
{
}

#define GET_AL_PROC(v, p, b) v = (p)alGetProcAddress(UTF_8(#v)); if (v == nullptr) { b = false; }

bool ISndApiOAL::create_api(void)
{
	// 1. Load the OpenAL functions
	bool bRetVal = true;
	// Effect objects
	GET_AL_PROC(alGenEffects, LPALGENEFFECTS, bRetVal);
	GET_AL_PROC(alDeleteEffects, LPALDELETEEFFECTS, bRetVal);
	GET_AL_PROC(alIsEffect, LPALISEFFECT, bRetVal);
	GET_AL_PROC(alEffecti, LPALEFFECTI, bRetVal);
	GET_AL_PROC(alEffectiv, LPALEFFECTIV, bRetVal);
	GET_AL_PROC(alEffectf, LPALEFFECTF, bRetVal);
	GET_AL_PROC(alEffectfv, LPALEFFECTFV, bRetVal);
	GET_AL_PROC(alGetEffecti, LPALGETEFFECTI, bRetVal);
	GET_AL_PROC(alGetEffectiv, LPALGETEFFECTIV, bRetVal);
	GET_AL_PROC(alGetEffectf, LPALGETEFFECTF, bRetVal);
	GET_AL_PROC(alGetEffectfv, LPALGETEFFECTFV, bRetVal);

	//Filter objects
	GET_AL_PROC(alGenFilters, LPALGENFILTERS, bRetVal);
	GET_AL_PROC(alDeleteFilters, LPALDELETEFILTERS, bRetVal);
	GET_AL_PROC(alIsFilter, LPALISFILTER, bRetVal);
	GET_AL_PROC(alFilteri, LPALFILTERI, bRetVal);
	GET_AL_PROC(alFilteriv, LPALFILTERIV, bRetVal);
	GET_AL_PROC(alFilterf, LPALFILTERF, bRetVal);
	GET_AL_PROC(alFilterfv, LPALFILTERFV, bRetVal);
	GET_AL_PROC(alGetFilteri, LPALGETFILTERI, bRetVal);
	GET_AL_PROC(alGetFilteriv, LPALGETFILTERIV, bRetVal);
	GET_AL_PROC(alGetFilterf, LPALGETFILTERF, bRetVal);
	GET_AL_PROC(alGetFilterfv, LPALGETFILTERFV, bRetVal);

	// Auxiliary slot object
	GET_AL_PROC(alGenAuxiliaryEffectSlots, LPALGENAUXILIARYEFFECTSLOTS, bRetVal);
	GET_AL_PROC(alDeleteAuxiliaryEffectSlots, LPALDELETEAUXILIARYEFFECTSLOTS, bRetVal);
	GET_AL_PROC(alIsAuxiliaryEffectSlot, LPALISAUXILIARYEFFECTSLOT, bRetVal);
	GET_AL_PROC(alAuxiliaryEffectSloti, LPALAUXILIARYEFFECTSLOTI, bRetVal);
	GET_AL_PROC(alAuxiliaryEffectSlotiv, LPALAUXILIARYEFFECTSLOTIV, bRetVal);
	GET_AL_PROC(alAuxiliaryEffectSlotf, LPALAUXILIARYEFFECTSLOTF, bRetVal);
	GET_AL_PROC(alAuxiliaryEffectSlotfv, LPALAUXILIARYEFFECTSLOTFV, bRetVal);
	GET_AL_PROC(alGetAuxiliaryEffectSloti, LPALGETAUXILIARYEFFECTSLOTI, bRetVal);
	GET_AL_PROC(alGetAuxiliaryEffectSlotiv, LPALGETAUXILIARYEFFECTSLOTIV, bRetVal);
	GET_AL_PROC(alGetAuxiliaryEffectSlotf, LPALGETAUXILIARYEFFECTSLOTF, bRetVal);
	GET_AL_PROC(alGetAuxiliaryEffectSlotfv, LPALGETAUXILIARYEFFECTSLOTFV, bRetVal);
	GET_AL_PROC(alGetAuxiliaryEffectSlotfv, LPALGETAUXILIARYEFFECTSLOTFV, bRetVal);
	GET_AL_PROC(alGetAuxiliaryEffectSlotfv, LPALGETAUXILIARYEFFECTSLOTFV, bRetVal);

	// 2. Create the list of adapters
	_create_adapter_list();

	return bRetVal;
}

bool ISndApiOAL::release_api(void)
{
	alGenEffects = nullptr;
	alDeleteEffects = nullptr;
	alIsEffect = nullptr;
	alEffecti = nullptr;
	alEffectiv = nullptr;
	alEffectf = nullptr;
	alEffectfv = nullptr;
	alGetEffecti = nullptr;
	alGetEffectiv = nullptr;
	alGetEffectf = nullptr;
	alGetEffectfv = nullptr;

	alGenFilters = nullptr;
	alDeleteFilters = nullptr;
	alIsFilter = nullptr;
	alFilteri = nullptr;
	alFilteriv = nullptr;
	alFilterf = nullptr;
	alFilterfv = nullptr;
	alGetFilteri = nullptr;
	alGetFilteriv = nullptr;
	alGetFilterf = nullptr;
	alGetFilterfv = nullptr;

	alGenAuxiliaryEffectSlots = nullptr;
	alDeleteAuxiliaryEffectSlots = nullptr;
	alIsAuxiliaryEffectSlot = nullptr;
	alAuxiliaryEffectSloti = nullptr;
	alAuxiliaryEffectSlotiv = nullptr;
	alAuxiliaryEffectSlotf = nullptr;
	alAuxiliaryEffectSlotfv = nullptr;
	alGetAuxiliaryEffectSloti = nullptr;
	alGetAuxiliaryEffectSlotiv = nullptr;
	alGetAuxiliaryEffectSlotf = nullptr;
	alGetAuxiliaryEffectSlotfv = nullptr;
	return true;
}

FXD::U32 ALGetMaxNumSources(void)
{
	// Clear AL Error Code
	alGetError();

	// Generate up to 256 Sources, checking for any errors
	FXD::U32 nSourceCount = 0;
	ALuint nSources[256];
	Memory::MemZero(nSources, sizeof(nSources));
	for (nSourceCount = 0; nSourceCount < 256; nSourceCount++)
	{
		alGenSources(1, &nSources[nSourceCount]);
		if (alGetError() != AL_NO_ERROR)
		{
			break;
		}
	}

	// Release the Sources
	alDeleteSources(nSourceCount, nSources);
	if (alGetError() != AL_NO_ERROR)
	{
		for (FXD::U32 n1 = 0; n1 < 256; n1++)
			alDeleteSources(1, &nSources[n1]);
	}
	return nSourceCount;
}

void ISndApiOAL::_create_adapter_list(void)
{
	if (alcIsExtensionPresent(NULL, "ALC_ENUMERATION_EXT"))
	{
		ALCchar* pDevices = (ALCchar*)alcGetString(NULL, ALC_DEVICE_SPECIFIER);
		ALCchar* pDefaultDeviceName = (ALCchar*)alcGetString(NULL, ALC_DEFAULT_DEVICE_SPECIFIER);

		FXD::U32 nID = 0;
		while (*pDevices != NULL)
		{
			ALCdevice* pDevice = alcOpenDevice(pDevices);
			if (pDevice != nullptr)
			{
				ALint attribs[4] = { 0 };
				attribs[0] = ALC_MAX_AUXILIARY_SENDS;
				attribs[1] = 4;
				ALCcontext* pContext = alcCreateContext(pDevice, attribs);
				if (pContext != nullptr)
				{
					if (alcMakeContextCurrent(pContext) == ALC_TRUE)
					{
						bool bNewName = true;
						ALCchar* pActualDeviceName = (ALCchar*)alcGetString(pDevice, ALC_DEVICE_SPECIFIER);
						fxd_for(const auto& adapterDesc, m_adapterDescs)
						{
							if (adapterDesc.first.compare(pActualDeviceName) == 0)
							{
								bNewName = false;
							}
						}

						if ((bNewName) && (pActualDeviceName != nullptr) && (Core::CharTraits< FXD::UTF8 >::length(pActualDeviceName) > 0))
						{
							ALint nMajor = 0; alcGetIntegerv(pDevice, ALC_MAJOR_VERSION, sizeof(ALint), &nMajor);
							ALint nMinor = 0; alcGetIntegerv(pDevice, ALC_MINOR_VERSION, sizeof(ALint), &nMinor);
							ALCint nEFXMajor = 0; alcGetIntegerv(pDevice, ALC_EFX_MAJOR_VERSION, sizeof(ALCint), &nEFXMajor);
							ALCint nEFXMinor = 0; alcGetIntegerv(pDevice, ALC_EFX_MINOR_VERSION, sizeof(ALCint), &nEFXMinor);
							ALint nCaps = 0;
							// Check for ALC Extensions
							if (alcIsExtensionPresent(pDevice, "ALC_EXT_CAPTURE") == AL_TRUE)
								nCaps |= ALCapture;
							if (alcIsExtensionPresent(pDevice, "ALC_EXT_EFX") == AL_TRUE)
								nCaps |= ALEFX;
							// Check for AL Extensions
							if (alIsExtensionPresent("AL_EXT_OFFSET") == AL_TRUE)
								nCaps |= ALOffset;
							if (alIsExtensionPresent("AL_EXT_LINEAR_DISTANCE") == AL_TRUE)
								nCaps |= ALLinearDistance;
							if (alIsExtensionPresent("AL_EXT_EXPONENT_DISTANCE") == AL_TRUE)
								nCaps |= ALExponentDistance;
							if (alIsExtensionPresent("EAX2.0") == AL_TRUE)
								nCaps |= ALEAX2;
							if (alIsExtensionPresent("EAX3.0") == AL_TRUE)
								nCaps |= ALEAX3;
							if (alIsExtensionPresent("EAX4.0") == AL_TRUE)
								nCaps |= ALEAX4;
							if (alIsExtensionPresent("EAX5.0") == AL_TRUE)
								nCaps |= ALEAX5;
							if (alIsExtensionPresent("EAX-RAM") == AL_TRUE)
								nCaps |= ALEAXRAM;
							FXD::S32 nEax = 0;
							for (FXD::S32 n1 = ALEAX2; n1 < ALEAXRAM; n1++)
							{
								nEax += (int)nCaps & n1;
							}

							SND::SndAdapterDescOAL info = std::make_shared< SND::ISndAdapterDescOAL >(nID);
							info->driver(SND::Funcs::ToString(E_SndApi::OpenAL).c_str() + Core::toString8(nMajor) + Core::toString8(nMinor));
							info->name((const FXD::UTF8*)pDevices);
							info->isDefault(Core::CharTraits< FXD::UTF8 >::compare_no_case(pDefaultDeviceName, pDevices) == 0);
							info->majorVersion(nMajor);
							info->minorVersion(nMinor);
							info->efxMajorVersion(nEFXMajor);
							info->efxMinorVersion(nEFXMinor);
							info->capsFlags(nCaps);
							info->has_hardware(nEax > 0);
							info->max_sources(ALGetMaxNumSources());
							m_adapterDescs[info->name()] = info;
						}
					}
					alcMakeContextCurrent(NULL);
					alcDestroyContext(pContext);
					//pContext = nullptr;
				}
				alcCloseDevice(pDevice);
				//pDevice = nullptr;
			}

			pDevices += Core::CharTraits< FXD::UTF8 >::length(pDevices) + 1;
			nID++;
		}
	}
}
#endif //IsSndOpenAL()
