// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndOpenAL()
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/OAL/EffectOAL.h"
#include "FXDEngine/SND/OAL/SndAdapterOAL.h"
#include "FXDEngine/SND/OAL/SndApiOAL.h"
#include "FXDEngine/SND/OAL/SndNodeOAL.h"

using namespace FXD;
using namespace SND;

// ------
// OAL
// -
// ------
namespace OAL
{
	bool createReverb(const SND::ISndAdapter* pSndAdapter, SND::IEffect* pEffect)
	{
		if (pSndAdapter->is_vaild())
		{
			SND::IEffectOAL* pEffectOAL = static_cast<SND::IEffectOAL*>(pEffect);
			if (pEffectOAL->m_alEffectSlot == 0)
			{
				alGenAuxiliaryEffectSlots(1, &pEffectOAL->m_alEffectSlot);
				OALResultCheckFail("alGenAuxiliaryEffectSlots");
				if (pEffectOAL->m_alEffectSlot == 0)
				{
					return false;
				}
			}
		}
		return true;
	}

	void shutdownReverb(const SND::ISndAdapter* pSndAdapter, SND::IEffect* pEffect)
	{
		if (pSndAdapter->is_vaild())
		{
			SND::IEffectOAL* pEffectOAL = static_cast< SND::IEffectOAL* >(pEffect);
			if (pEffectOAL->m_alEffectSlot != 0)
			{
				alDeleteAuxiliaryEffectSlots(1, &pEffectOAL->m_alEffectSlot);
				pEffectOAL->m_alEffectSlot = 0;
			}
		}
	}
} //namespace OAL

// ------
// IEffectOAL
// - 
// ------
IEffectOAL::IEffectOAL(const SND::EFFECT_DESC& effectDesc)
	: IEffect(effectDesc)
	, m_alEffectSlot(0)
{
}
IEffectOAL::~IEffectOAL(void)
{
	PRINT_COND_ASSERT((m_alEffectSlot == 0), "SND: m_alEffectSlot has not been shutdown");
}


// ------
// IEffectChainOAL
// - 
// ------
IEffectChainOAL::IEffectChainOAL(const SND::ISndAdapter* pSndAdapter, SND::INodeMixer* pNodeMixer)
	: IEffectChain(pSndAdapter, pNodeMixer)
{
}
IEffectChainOAL::~IEffectChainOAL(void)
{
}

bool IEffectChainOAL::_create_effect_chain(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");
	return true;
}

bool IEffectChainOAL::_release_effect_chain(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	fxd_for(auto& effect, m_effects)
	{
		effect->destroyed(true);
	}
	_recreate();
	return true;
}

void IEffectChainOAL::_add_effects(const Container::Vector< SND::EFFECT_DESC >& effectDescs)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	fxd_for(auto& effectDesc, effectDescs)
	{
		m_effects.push_back(std::make_shared< IEffectOAL >(effectDesc));
	}
	m_bNeedsRecreate = true;
}

void IEffectChainOAL::_enable_effect(FXD::U16 nSlot, bool bEnable)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	m_effects[nSlot]->effect_desc().Enabled = bEnable;
}

bool IEffectChainOAL::_is_effect_enabled(FXD::U16 nSlot) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	bool bState = m_effects[nSlot]->effect_desc().Enabled;
	return bState;
}

void IEffectChainOAL::_update(FXD::F32 dt)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (m_bNeedsRecreate)
	{
		_recreate();
	}
	if ((!m_bNeedsRecreate) && (m_bNeedsUpdate))
	{
		FXD::U32 nCount = 0;
		fxd_for(auto& effect, m_effects)
		{
			SND::IEffectOAL* pEffectOAL = static_cast< SND::IEffectOAL* >(effect.get());

			ALuint alEffect;
			alGenEffects(1, &alEffect);
			if (alEffect == 0)
			{
				return;
			}
			alEffecti(alEffect, AL_EFFECT_TYPE, AL_EFFECT_REVERB);

			alEffectf(alEffect, AL_REVERB_GAIN, effect->effect_desc().Gain);
			alEffectf(alEffect, AL_REVERB_GAINHF, effect->effect_desc().GainHF);
			alEffectf(alEffect, AL_REVERB_DECAY_TIME, effect->effect_desc().DecayTime);
			alEffectf(alEffect, AL_REVERB_DECAY_HFRATIO, effect->effect_desc().DecayHFRatio);
			alEffectf(alEffect, AL_REVERB_REFLECTIONS_GAIN, effect->effect_desc().ReflectionsGain);
			alEffectf(alEffect, AL_REVERB_REFLECTIONS_DELAY, effect->effect_desc().ReflectionsDelay);
			alEffectf(alEffect, AL_REVERB_LATE_REVERB_GAIN, effect->effect_desc().LateGain);
			alEffectf(alEffect, AL_REVERB_LATE_REVERB_DELAY, effect->effect_desc().LateDelay);
			alEffectf(alEffect, AL_REVERB_DIFFUSION, effect->effect_desc().Diffusion);
			alEffectf(alEffect, AL_REVERB_DENSITY, effect->effect_desc().Density);
			alEffectf(alEffect, AL_REVERB_AIR_ABSORPTION_GAINHF, effect->effect_desc().AirAbsorptionGainHF);
			alEffectf(alEffect, AL_REVERB_ROOM_ROLLOFF_FACTOR, effect->effect_desc().RoomRolloffFactor);
			alEffecti(alEffect, AL_REVERB_DECAY_HFLIMIT, 0x1);

			alAuxiliaryEffectSloti(pEffectOAL->m_alEffectSlot, AL_EFFECTSLOT_EFFECT, alEffect);
			OALResultCheckFail("alAuxiliaryEffectSloti");

			alDeleteEffects(1, &alEffect);
			nCount++;
		}
		m_bNeedsUpdate = false;
	}
}

void IEffectChainOAL::_recreate(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1. Remove the effects on all of the mixer voices
	FXD::U32 nCount = 0;
	fxd_for(auto& effect, m_effects)
	{
		IEffectOAL* pEffectOAL = (IEffectOAL*)effect.get();
		if (pEffectOAL->m_alEffectSlot != 0)
		{
			fxd_for(auto& voice, m_pNodeMixer->voices())
			{
				INodeVoiceOAL* pNodeVoiceOAL = (INodeVoiceOAL*)voice.get();
				alSource3i(pNodeVoiceOAL->m_alSource, AL_EFFECTSLOT_NULL, pEffectOAL->m_alEffectSlot, 0, AL_FILTER_NULL);
			}
		}
		nCount++;
	}

	// 2. Remove destroyed effects
	fxd_for_itr(itr, m_effects)
	{
		if ((*itr)->destroyed())
		{
			::OAL::shutdownReverb(m_pSndAdapter, (*itr).get());
			itr = m_effects.erase(itr);
		}
	}

	nCount = 0;
	if (m_effects.not_empty())
	{
		// 3. Create the new effects
		fxd_for(auto& effect, m_effects)
		{
			SND::IEffectOAL* pEffectOAL = static_cast< SND::IEffectOAL* >(effect.get());
			::OAL::createReverb(m_pSndAdapter, pEffectOAL);

			fxd_for(auto& voice, m_pNodeMixer->voices())
			{
				SND::INodeVoiceOAL* pNodeVoiceOAL = static_cast< SND::INodeVoiceOAL* >(voice.get());
				alSource3i(pNodeVoiceOAL->m_alSource, AL_AUXILIARY_SEND_FILTER, pEffectOAL->m_alEffectSlot, 0, AL_FILTER_NULL);
				OALResultCheckFail("alSource3i");
			}
			nCount++;
		}
		m_bNeedsUpdate = true;
	}
	m_bNeedsRecreate = false;
	return;
}
#endif //IsSndOpenAL()
