// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndOpenAL()
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/OAL/ListenerOAL.h"
#include "FXDEngine/SND/OAL/SndAdapterOAL.h"
#include "FXDEngine/SND/OAL/SndNodeOAL.h"

using namespace FXD;
using namespace SND;

// ------
// ISndAdapterDescOAL
// - 
// ------
ISndAdapterDescOAL::ISndAdapterDescOAL(const App::DeviceIndex nDeviceID)
	: ISndAdapterDesc(nDeviceID)
	, m_nMajorVersion(0)
	, m_nMinorVersion(0)
	, m_nEFXMajorVersion(0)
	, m_nEFXMinorVersion(0)
	, m_nCapsFlags(0)
{
}

ISndAdapterDescOAL::~ISndAdapterDescOAL(void)
{
}

// ------
// ISndAdapterOAL
// - 
// ------
ISndAdapterOAL::ISndAdapterOAL(const App::AdapterDesc& adapterDesc, const App::IApi* pApi)
	: ISndAdapter(adapterDesc, pApi)
	, m_pDevice(nullptr)
	, m_pContext(nullptr)
{
}

ISndAdapterOAL::~ISndAdapterOAL(void)
{
	PRINT_COND_ASSERT((m_pContext == nullptr), "SND: m_pContext has not been shutdown");
	PRINT_COND_ASSERT((m_pDevice == nullptr), "SND: m_pDevice has not been shutdown");
}

bool ISndAdapterOAL::_create(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1. Create the adapter
	const ALCchar* pDeviceName = alcGetString(NULL, ALC_DEFAULT_DEVICE_SPECIFIER);
	m_pDevice = alcOpenDevice(pDeviceName);
	if (m_pDevice == nullptr)
	{
		PRINT_WARN << "SND: alcOpenDevice() failed";
		return false;
	}

	// 2. Create the context
	ALint attribs[4] = { 0 };
	attribs[0] = ALC_MAX_AUXILIARY_SENDS;
	attribs[1] = 4;
	m_pContext = alcCreateContext(m_pDevice, attribs);
	if (m_pContext == nullptr)
	{
		PRINT_WARN << "SND: alcCreateContext() failed";
		return false;
	}
	PRINT_COND_ASSERT((alcMakeContextCurrent(m_pContext) == ALC_TRUE), "SND: alcMakeContextCurrent() failed");

	// 4. Create the master node
	NODE_DESC nodeDesc;
	nodeDesc.Name = UTF_8("Master");
	m_masterNode = std::make_shared< SND::INodeMixerOAL >(this, nullptr);
	m_masterNode->create(nodeDesc);

	// 5. Create the listener
	m_listener = std::make_shared< SND::IListenerOAL >(this);
	m_listener->create();
	return true;
}

bool ISndAdapterOAL::_release(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1. Release the listener
	FXD_RELEASE(m_listener, release());

	// 2. Release the master node
	FXD_RELEASE(m_masterNode, release());

	// 3. Release the Al Context
	alcMakeContextCurrent(NULL);
	alcDestroyContext(m_pContext);
	m_pContext = nullptr;
	alcCloseDevice(m_pDevice);
	m_pDevice = nullptr;
	return true;
}


bool ISndAdapterOAL::_is_vaild(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	bool bRetVal = ((m_pDevice != nullptr) && (m_pContext != nullptr));
	return bRetVal;
}
#endif //IsSndOpenAL()
