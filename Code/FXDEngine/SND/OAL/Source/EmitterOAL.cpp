// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndOpenAL()
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/OAL/EmitterOAL.h"
#include "FXDEngine/SND/OAL/ListenerOAL.h"
#include "FXDEngine/SND/OAL/SndAdapterOAL.h"
#include "FXDEngine/SND/OAL/SndNodeOAL.h"
#include "FXDEngine/SND/OAL/VoiceOAL.h"

using namespace FXD;
using namespace SND;

// ------
// IEmitterOAL
// - 
// ------
IEmitterOAL::IEmitterOAL(const SND::ISndAdapter* pSndAdapter, const SND::INodeVoice* pNodeVoice)
	: IEmitter(pSndAdapter, pNodeVoice)
{
}

IEmitterOAL::~IEmitterOAL(void)
{
}

bool IEmitterOAL::_create_emitter(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1.
	set(Math::Matrix4F::kIdentity);
	set_velocity(Math::Vector3F::kZero);
	set_min_max_distance(m_fMinDistance, m_fMaxDistance);

	// 2.
	return true;
}

bool IEmitterOAL::_release_emitter(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	return true;
}

void IEmitterOAL::_update(const SND::Listener& listener)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	Math::Vector3F vPos = position();
	Math::Vector3F vUp = up();
	Math::Vector3F vForward = forward();
	Math::Vector3F vVelocity = velocity();

	FXD::F32 fPosition[3] = { vPos.x(), vPos.y(), -vPos.z() };
	FXD::F32 fVelocity[3] = { vVelocity.x(), vVelocity.y(), -vVelocity.z() };
	FXD::F32 fOrient[6] = { vForward.x(), vForward.y(), -vForward.z(), vUp.x(), vUp.y(), -vUp.z() };

	SND::INodeVoiceOAL* pVoiceNodeOAL = (SND::INodeVoiceOAL*)(m_pNodeVoice);
	alSourcefv(pVoiceNodeOAL->m_alSource, AL_POSITION, fPosition);
	alSourcefv(pVoiceNodeOAL->m_alSource, AL_VELOCITY, fVelocity);
	alSourcefv(pVoiceNodeOAL->m_alSource, AL_ORIENTATION, fOrient);

	alSourcef(pVoiceNodeOAL->m_alSource, AL_REFERENCE_DISTANCE, m_fMinDistance);
	alSourcef(pVoiceNodeOAL->m_alSource, AL_MAX_DISTANCE, m_fMaxDistance);
}

void IEmitterOAL::_update_distance_model(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	SND::INodeVoiceOAL* pVoiceNodeOAL = (SND::INodeVoiceOAL*)(m_pNodeVoice);
	alSourcef(pVoiceNodeOAL->m_alSource, AL_REFERENCE_DISTANCE, m_fMinDistance);
	alSourcef(pVoiceNodeOAL->m_alSource, AL_MAX_DISTANCE, m_fMaxDistance);
}
#endif //IsSndOpenAL()
