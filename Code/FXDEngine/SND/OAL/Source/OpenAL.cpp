// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndOpenAL()
#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/OAL/OpenAL.h"

using namespace FXD;
using namespace SND;

// ------
// CheckOALResult
// -
// ------
inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, CheckOALResult const& rhs)
{
	switch (rhs.alError())
	{
#define HANDLE_STR(h) case h: ostr << #h; break;
		HANDLE_STR(AL_INVALID_NAME)
		HANDLE_STR(AL_INVALID_ENUM)
		HANDLE_STR(AL_INVALID_VALUE)
		HANDLE_STR(AL_INVALID_OPERATION)
		HANDLE_STR(AL_OUT_OF_MEMORY)
#undef HANDLE_STR
	default:
		FXD_ERROR("Unrecognised ALenum - please find the correct string and add to this case");
		break;
	}
	return ostr;
}

CheckOALResult::CheckOALResult(const FXD::UTF8* pText, const FXD::UTF8* pFile, FXD::U32 nLine)
	: m_alError(alGetError())
	, m_nLine(nLine)
	, m_pText(pText)
	, m_pFile(pFile)
{
}

bool CheckOALResult::operator()()
{
	if (m_alError == AL_NO_ERROR)
	{
		return false;
	}
	PRINT_WARN << "[" << m_pFile << " , " << m_nLine << "] - failed with error: " << (*this);
	return true;
}

// ------
// OAL
// -
// ------
ALenum FXD::SND::OAL::GetALFormat(const SND::SndFormat& fmt)
{
	switch (fmt.Channels)
	{
		case 1:
		{
			if (fmt.BitsPerSample == 8)
				return AL_FORMAT_MONO8;
			if (fmt.BitsPerSample == 16)
				return AL_FORMAT_MONO16;
			if (fmt.BitsPerSample == 32)
				return AL_FORMAT_MONO_FLOAT32;
			return AL_NONE;
		}
		case 2:
		{
			if (fmt.BitsPerSample == 8)
				return AL_FORMAT_STEREO8;
			if (fmt.BitsPerSample == 16)
				return AL_FORMAT_STEREO16;
			if (fmt.BitsPerSample == 32)
				return AL_FORMAT_STEREO_FLOAT32;
			return AL_NONE;
		}
		case 4:
		{
			if (fmt.BitsPerSample == 8)
				return AL_FORMAT_QUAD8;
			if (fmt.BitsPerSample == 16)
				return AL_FORMAT_QUAD16;
			if (fmt.BitsPerSample == 32)
				return AL_FORMAT_QUAD32;
			return AL_NONE;
		}
		case 6:
		{
			if (fmt.BitsPerSample == 8)
				return AL_FORMAT_51CHN8;
			if (fmt.BitsPerSample == 16)
				return AL_FORMAT_51CHN16;
			if (fmt.BitsPerSample == 32)
				return AL_FORMAT_51CHN32;
			return AL_NONE;
		}
		case 7:
		{
			if (fmt.BitsPerSample == 8)
				return AL_FORMAT_61CHN8;
			if (fmt.BitsPerSample == 16)
				return AL_FORMAT_61CHN16;
			if (fmt.BitsPerSample == 32)
				return AL_FORMAT_61CHN32;
			return AL_NONE;
		}
		case 8:
		{
			if (fmt.BitsPerSample == 8)
				return AL_FORMAT_71CHN8;
			if (fmt.BitsPerSample == 16)
				return AL_FORMAT_71CHN16;
			if (fmt.BitsPerSample == 32)
				return AL_FORMAT_71CHN32;
			return AL_NONE;
		}
		default:
		{
			return AL_NONE;
		}
	}
}

bool FXD::SND::OAL::CheckExtension(ALCdevice* pDevice, const FXD::UTF8* pExtension)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (pExtension != nullptr)
	{
		const FXD::UTF8* pExtensionList = (const FXD::UTF8*)alGetString(AL_EXTENSIONS);
		if (pExtensionList != nullptr)
		{
			bool retVal = (strstr(pExtensionList, pExtension) != nullptr);
			if (retVal)
			{
				return true;
			}
		}

		pExtensionList = (const FXD::UTF8*)alcGetString(pDevice, ALC_EXTENSIONS);
		if (pExtensionList != nullptr)
		{
			bool retVal = (strstr(pExtensionList, pExtension) != nullptr);
			if (retVal)
			{
				return true;
			}
		}
	}
	return false;
}

void FXD::SND::OAL::PrintExtension(ALCdevice* pDevice)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	PRINT_INFO << "SND: AL_EXTENSIONS";
	const FXD::UTF8* pExtensionList = (const FXD::UTF8*)alGetString(AL_EXTENSIONS);
	if (pExtensionList != nullptr)
	{
		Core::String8 strExt = (FXD::UTF8*)pExtensionList;
		FXD::U32 nPosBegin = 0;
		FXD::U32 nPosEnd = 0;
		FXD::U32 nLength = strExt.length();
		bool bFinished = false;
		while (!bFinished)
		{
			nPosEnd = strExt.find(" ", nPosBegin);
			if (nPosEnd == Core::String8::npos)
			{
				nPosEnd = nLength;
				bFinished = true;
			}

			const FXD::UTF8* pStr = strExt.c_str();
			Core::String8 ss(&pStr[nPosBegin], nPosEnd - nPosBegin);
			PRINT_INFO << ss;

			nPosBegin = nPosEnd + 1;
		}
	}

	PRINT_INFO << "SND: ALC_EXTENSIONS";
	pExtensionList = (const FXD::UTF8*)alcGetString(pDevice, ALC_EXTENSIONS);
	if (pExtensionList != nullptr)
	{
		Core::String8 strExt = (FXD::UTF8*)pExtensionList;
		FXD::U32 nPosBegin = 0;
		FXD::U32 nPosEnd = 0;
		FXD::U32 nLength = strExt.length();
		bool bFinished = false;
		while (!bFinished)
		{
			nPosEnd = strExt.find(" ", nPosBegin);
			if (nPosEnd == Core::String8::npos)
			{
				nPosEnd = nLength;
				bFinished = true;
			}

			const FXD::UTF8* pStr = strExt.c_str();
			Core::String8 ss(&pStr[nPosBegin], nPosEnd - nPosBegin);
			PRINT_INFO << ss;

			nPosBegin = nPosEnd + 1;
		}
	}
}
#endif //IsSndOpenAL()