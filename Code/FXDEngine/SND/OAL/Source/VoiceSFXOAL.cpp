// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndOpenAL()
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/OAL/ListenerOAL.h"
#include "FXDEngine/SND/OAL/SndAdapterOAL.h"
#include "FXDEngine/SND/OAL/SndNodeOAL.h"
#include "FXDEngine/SND/OAL/VoiceOAL.h"
#include "FXDEngine/SND/Decoders/Audio.h"

using namespace FXD;
using namespace SND;

// ------
// IVoiceSFXOAL
// - 
// ------
IVoiceSFXOAL::IVoiceSFXOAL(const SND::ISndAdapter* pSndAdapter, SND::INodeVoice* pNodeVoice)
	: IVoiceSFX(pSndAdapter, pNodeVoice)
{
	for (FXD::U16 n1 = 0; n1 < BufferCount; n1++)
	{
		m_buffers[n1].m_nID = n1;
		m_buffers[n1].m_alBuffer = 0;
	}
}

IVoiceSFXOAL::~IVoiceSFXOAL(void)
{
}

bool IVoiceSFXOAL::create(const SND::VOICE_DESC& voiceDesc)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");
	PRINT_COND_ASSERT((voiceDesc.Audio->is_sequenceable()), "SND: Stream is not sequenceable");

	// 1. Store Local Information and construct memory buffers 
	m_voiceDesc = voiceDesc;
	m_voiceDesc.Audio->seek_begin();
	_ensure_loop_points_defined(m_voiceDesc.CreateFlag, m_voiceDesc.Audio);

	m_dataBuffer = Memory::MemHandle::read_to_end((*m_voiceDesc.Audio), true);
	m_nCurrentBuffer = 0;
	for (FXD::U16 n1 = 0; n1 < BufferCount; n1++)
	{
		alGenBuffers(1, &m_buffers[n1].m_alBuffer);
	}
	return true;
}

void IVoiceSFXOAL::release(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	stop();
	for (FXD::U16 n1 = 0; n1 < BufferCount; n1++)
	{
		alDeleteBuffers(1, &m_buffers[n1].m_alBuffer);
		m_buffers[n1].m_alBuffer = 0;
	}
	m_voiceDesc = SND::VOICE_DESC();
}

void IVoiceSFXOAL::play(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!m_pVoiceNode->is_valid())
	{
		return;
	}

	SND::INodeVoiceOAL* pVoiceNodeOAL = (SND::INodeVoiceOAL*)(m_pVoiceNode);
	alSourcePlay(pVoiceNodeOAL->m_alSource);
	OALResultCheckFail("alSourcePlay");

	m_bStreamComplete = false;
}

void IVoiceSFXOAL::pause(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!m_pVoiceNode->is_valid())
	{
		return;
	}

	SND::INodeVoiceOAL* pVoiceNodeOAL = (SND::INodeVoiceOAL*)(m_pVoiceNode);
	alSourcePause(pVoiceNodeOAL->m_alSource);
	OALResultCheckFail("alSourcePause");
}

void IVoiceSFXOAL::stop(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!m_pVoiceNode->is_valid())
	{
		return;
	}

	SND::INodeVoiceOAL* pVoiceNodeOAL = (SND::INodeVoiceOAL*)(m_pVoiceNode);
	alSourceStop(pVoiceNodeOAL->m_alSource);
	OALResultCheckFail("alSourceStop");

	_buffers_flush();
	m_loopHandle = LoopPointHandle();
}

FXD::U32 IVoiceSFXOAL::tell_sample_position(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!m_pVoiceNode->is_valid())
	{
		return 0;
	}
	auto itr = m_queue.begin();
	if (itr == m_queue.end())
	{
		return 0;
	}

	SND::INodeVoiceOAL* pVoiceNodeOAL = (SND::INodeVoiceOAL*)(m_pVoiceNode);
	ALint alSampleOffset = 0;
	alGetSourcei(pVoiceNodeOAL->m_alSource, AL_SAMPLE_OFFSET, &alSampleOffset);

	FXD::U32 nSampleStart = ((*itr)->m_nSampleStart + (*itr)->m_nSampleOffset) / m_voiceDesc.Audio->source_format().getBytesPerSample();
	return (nSampleStart + alSampleOffset);
}

void IVoiceSFXOAL::_update_buffers(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1. Early out if we are not playing
	if ((!m_pVoiceNode->is_valid()) || (!m_pVoiceNode->is_playing()))
	{
		return;
	}

	// 2. Check for buffers that have finished
	_buffers_finished();

	// 3. If we have finished streaming data then check when we can stop
	if (m_bStreamComplete)
	{
		if (_buffer_queued_count() == 0)
		{
			m_pVoiceNode->stop();
		}
		return;
	}

	// 3. If we have a fee buffer then fill a new one
	FXD::U32 nQueuedBuffers = _buffer_queued_count();
	if (nQueuedBuffers < BufferCount)
	{
		_buffer_fill();
	}

	// 4. OpenAL fix
	SND::INodeVoiceOAL* pVoiceNodeOAL = (SND::INodeVoiceOAL*)(m_pVoiceNode);
	ALint alState = 0;
	alGetSourcei(pVoiceNodeOAL->m_alSource, AL_SOURCE_STATE, &alState);

	FXD::U32 nAttempts = 0;
	while ((alState != AL_PLAYING) && (nAttempts < 5))
	{
		alSourcePlay(pVoiceNodeOAL->m_alSource);
		alGetSourcei(pVoiceNodeOAL->m_alSource, AL_SOURCE_STATE, &alState);
		nAttempts++;
	}
}

void IVoiceSFXOAL::_buffer_fill(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1. Return if we are finished
	if (m_bStreamComplete)
	{
		return;
	}

	// 2. Make sure we have a buffer to start
	if (!m_loopHandle.m_pLoopPoint)
	{
		_update_loop_point();
	}

	// 3. Submit the buffer data
	const FXD::U32 nAmountRead = m_nBytesSize;
	_buffer_submit(nAmountRead);

	// 4. Move to next loop point
	_update_loop_point();
}

void IVoiceSFXOAL::_buffer_submit(FXD::U32 nSize)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1. Get a pointer to the memory 
	Memory::MemHandle::LockGuard memHandleLock(m_dataBuffer);
	const FXD::U8* pMemPointer = (const FXD::U8*)memHandleLock.get_mem();

	// 2. Construct a local buffer to store
	m_buffers[m_nCurrentBuffer].m_nSampleStart = m_nBytesStartPos;
	m_buffers[m_nCurrentBuffer].m_nSampleOffset = m_loopHandle.m_nLoopPointBytesRead;
	m_buffers[m_nCurrentBuffer].m_eState = FXDBUFFEROAL::State::Playing;
	m_queue.push_back(&m_buffers[m_nCurrentBuffer]);
	m_loopHandle.m_nLoopPointBytesRead += nSize;

	// 3. Create and submit current api buffer
	ALvoid* pMemBegin = (ALvoid*)(pMemPointer + m_nBytesStartPos);
	alBufferData(m_buffers[m_nCurrentBuffer].m_alBuffer, SND::OAL::GetALFormat(m_voiceDesc.Audio->source_format()), pMemBegin, nSize, m_voiceDesc.Audio->source_format().SampleRate);
	OALResultCheckFail("alBufferData");

	SND::INodeVoiceOAL* pVoiceNodeOAL = (SND::INodeVoiceOAL*)(m_pVoiceNode);
	alSourceQueueBuffers(pVoiceNodeOAL->m_alSource, 1, &m_buffers[m_nCurrentBuffer].m_alBuffer);
	OALResultCheckFail("alSourceQueueBuffers");

	// 4. Increment to the next buffer
	m_nCurrentBuffer++;
	m_nCurrentBuffer %= BufferCount;
}

void IVoiceSFXOAL::_buffers_flush(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (m_pVoiceNode->is_valid())
	{
		SND::INodeVoiceOAL* pVoiceNodeOAL = (SND::INodeVoiceOAL*)(m_pVoiceNode);
		for (FXD::U16 n1 = 0; n1 < BufferCount; n1++)
		{
			alSourceUnqueueBuffers(pVoiceNodeOAL->m_alSource, 1, &m_buffers[m_nCurrentBuffer].m_alBuffer);
			OALResultCheckFail("alSourceUnqueueBuffers");
		}
	}

	m_nBytesStartPos = 0;
	m_nBytesSize = 0;
	m_bStreamComplete = true;
}

FXD::U32 IVoiceSFXOAL::_buffer_queued_count(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!m_pVoiceNode->is_valid())
	{
		return 0;
	}

	SND::INodeVoiceOAL* pVoiceNodeOAL = (SND::INodeVoiceOAL*)(m_pVoiceNode);
	ALint alBuffersQueued = 0;
	alGetSourcei(pVoiceNodeOAL->m_alSource, AL_BUFFERS_QUEUED, &alBuffersQueued);
	return (FXD::U32)alBuffersQueued;
}

void IVoiceSFXOAL::_buffers_finished(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!m_pVoiceNode->is_valid())
	{
		return;
	}

	SND::INodeVoiceOAL* pVoiceNodeOAL = (SND::INodeVoiceOAL*)(m_pVoiceNode);
	ALint nNumprocessed = 0;
	alGetSourcei(pVoiceNodeOAL->m_alSource, AL_BUFFERS_PROCESSED, &nNumprocessed);
	while (nNumprocessed != 0)
	{
		ALuint alBuffer = 0;
		alSourceUnqueueBuffers(pVoiceNodeOAL->m_alSource, 1, &alBuffer);
		OALResultCheckFail("alSourceUnqueueBuffers");
		nNumprocessed--;

		for (FXD::U16 n1 = 0; n1 < BufferCount; n1++)
		{
			if (m_buffers[n1].m_alBuffer == alBuffer)
			{
				m_buffers[n1].m_eState = FXDBUFFEROAL::State::Finished;
				m_queue.find_erase(&m_buffers[n1]);
				break;
			}
		}
	}

	for (FXD::U16 n1 = 0; n1 < BufferCount; n1++)
	{
		if (m_buffers[n1].m_eState == FXDBUFFEROAL::State::Finished)
		{
			m_buffers[n1].m_eState = FXDBUFFEROAL::State::Free;
			m_queue.find_erase(&m_buffers[n1]);
		}
	}
}
#endif //IsSndOpenAL()