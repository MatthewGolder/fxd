// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_OAL_SNDADAPTEROAL_H
#define FXDENGINE_SND_OAL_SNDADAPTEROAL_H

#include "FXDEngine/SND/Types.h"

#if IsSndOpenAL()
#include "FXDEngine/SND/SndAdapter.h"
#include "FXDEngine/SND/OAL/OpenAL.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// ISndAdapterDescOAL
		// - 
		// ------
		class ISndAdapterDescOAL FINAL : public SND::ISndAdapterDesc
		{
		public:
			ISndAdapterDescOAL(const App::DeviceIndex nDeviceID);
			~ISndAdapterDescOAL(void);

			SET(ALint, nMajorVersion, majorVersion);
			SET(ALint, nMinorVersion, minorVersion);
			SET(ALint, nEFXMajorVersion, efxMajorVersion);
			SET(ALint, nEFXMinorVersion, efxMinorVersion);
			SET(ALint, nCapsFlags, capsFlags);

			GET_R(ALint, nMajorVersion, majorVersion);
			GET_R(ALint, nMinorVersion, minorVersion);
			GET_R(ALint, nEFXMajorVersion, efxMajorVersion);
			GET_R(ALint, nEFXMinorVersion, efxMinorVersion);
			GET_R(ALint, nCapsFlags, capsFlags);

		private:
			ALint m_nMajorVersion;
			ALint m_nMinorVersion;
			ALint m_nEFXMajorVersion;
			ALint m_nEFXMinorVersion;
			ALint m_nCapsFlags;
		};

		// ------
		// ISndAdapterOAL
		// -
		// ------
		class ISndAdapterOAL FINAL : public SND::ISndAdapter
		{
		public:
			EXPLICIT ISndAdapterOAL(const App::AdapterDesc& adapterDesc, const App::IApi* pApi);
			~ISndAdapterOAL(void);

		protected:
			bool _create(void) FINAL;
			bool _release(void) FINAL;

			bool _is_vaild(void) const FINAL;

		protected:
			ALCdevice* m_pDevice;
			ALCcontext* m_pContext;
		};

	} //namespace SND
} //namespace FXD
#endif //IsSndOpenAL()
#endif //FXDENGINE_SND_OAL_SNDADAPTEROAL_H