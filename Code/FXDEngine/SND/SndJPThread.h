// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_SNDJPTHREAD_H
#define FXDENGINE_SND_SNDJPTHREAD_H

#include "FXDEngine/Core/FPS.h"
#include "FXDEngine/Core/Timer.h"
#include "FXDEngine/SND/Types.h"
#include "FXDEngine/Job/AsyncJob.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// ISndJPThread
		// -
		// ------
		class ISndJPThread : public Job::IAsyncTaskPoolThread
		{
		public:
			ISndJPThread(void);
			virtual ~ISndJPThread(void);

			const Core::StringView8 display_name(void) const FINAL { return UTF_8("SndThread"); }

			GET_REF_W(SND::SndSystem, sndSystem, snd_system);

		protected:
			bool init_pool(void) FINAL;
			bool shutdown_pool(void) FINAL;
			void idle_process(void) FINAL;
			bool is_thread_restricted(void) const FINAL;

		private:
			Core::FPS m_fps;
			Core::Timer m_timer;
			SND::SndSystem m_sndSystem;
		};

		namespace Funcs
		{
			extern bool IsSndThreadRestricted(void);
			extern void RestrictSndThread(void);
			extern void UnRestrictSndThread(void);
		} //namespace Funcs

	} //namespace SND
} //namespace FXD
#endif //FXDENGINE_SND_SNDJPTHREAD_H