// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_SNDADAPTER_H
#define FXDENGINE_SND_SNDADAPTER_H

#include "FXDEngine/App/Adapter.h"
#include "FXDEngine/SND/Types.h"
#include "FXDEngine/IO/Types.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// ISndAdapterDesc
		// - 
		// ------
		class ISndAdapterDesc : public App::IAdapterDesc
		{
		public:
			ISndAdapterDesc(const App::DeviceIndex nDeviceID)
				: IAdapterDesc(nDeviceID)
				, m_bHasHardware(false)
				, m_nMaxSources(-1)
			{
			}
			virtual ~ISndAdapterDesc(void)
			{
			}

			SET(bool, bHasHardware, has_hardware);
			GET_R(bool, bHasHardware, has_hardware);

			SET(FXD::S32, nMaxSources, max_sources);
			GET_R(FXD::S32, nMaxSources, max_sources);

		protected:
			bool m_bHasHardware;
			FXD::S32 m_nMaxSources;
		};

		// ------
		// ISndAdapter
		// -
		// ------
		class ISndAdapter : public App::IAdapter
		{
		public:
			friend class ISndSystem;
			friend class INodeVoiceOAL;
			friend class INodeVoiceOSL;
			friend class INodeVoiceXA2;

		public:
			EXPLICIT ISndAdapter(const App::AdapterDesc& adapterDesc, const App::IApi* pApi);
			virtual ~ISndAdapter(void);

			Job::Future< bool > create(void) FINAL;
			Job::Future< bool > release(void) FINAL;

			Job::Future< SND::SndLoopGroup > load_loop_points(const IO::Path& ioPath);
			Job::Future< SND::SndLoopGroup > load_loop_points_stream(Core::StreamIn& stream);

			Job::Future< SND::Playlist > load_playlist(const IO::Path& ioPath, const SND::NodeMixer& nodeMixer);
			Job::Future< SND::Playlist > load_playlist_stream(Core::StreamIn& stream, const IO::Path& ioPath, const SND::NodeMixer& nodeMixer);

			Job::Future< void > register_playlist(const SND::Playlist& playlist);
			Job::Future< void > unregister_playlist(const SND::Playlist& playlist);

			bool is_vaild(void) const;

			GET_REF_RW(SND::Listener, listener, listener);
			GET_REF_RW(SND::NodeMixer, masterNode, master_node);
			GET_REF_W(Container::Vector< SND::Playlist >, playlists, playlists);

		protected:
			void _update(FXD::F32 dt);
			void _update_listener(FXD::F32 dt);
			void _update_nodes(FXD::F32 dt);
			void _update_playlists(FXD::F32 dt);
			void _update_priorities(FXD::F32 dt);

			virtual bool _create(void) PURE;
			virtual bool _release(void) PURE;

			SND::SndLoopGroup _load_loop_points(const IO::Path& ioPath);
			SND::SndLoopGroup _load_loop_points_stream(Core::StreamIn stream);

			SND::Playlist _load_playlist(const IO::Path& ioPath, const SND::NodeMixer& nodeMixer);
			SND::Playlist _load_playlist_stream(Core::StreamIn& stream, const IO::Path& ioPath, const SND::NodeMixer& nodeMixer);
			void _register_playlist(const SND::Playlist& playlist);
			void _unregister_playlist(const SND::Playlist& playlist);

			virtual bool _is_vaild(void) const PURE;

		protected:
			SND::Listener m_listener;
			SND::NodeMixer m_masterNode;
			Container::Vector< SND::Playlist > m_playlists;
			mutable Container::Vector< SND::INodeVoice* > m_voices;
		};

	} //namespace SND
} //namespace FXD
#endif //FXDE;NGINE_SND_SNDADAPTER_H