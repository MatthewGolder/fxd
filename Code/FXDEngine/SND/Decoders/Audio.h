// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_DECODERS_AUDIO_H
#define FXDENGINE_SND_DECODERS_AUDIO_H

#include "FXDEngine/IO/FileCache.h"
#include "FXDEngine/SND/Types.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// E_AudioAsset
		// -
		// ------
		enum class E_AudioAsset
		{
			Ogg,
			Wav,
			Procedural,
			Count,
			Unknown = 0xffff
		};

		// ------
		// IAudio
		// -
		// ------
		class IAudio : public IO::IAsset, public Core::IStreamIn
		{
		public:
			IAudio(SND::E_AudioAsset eAsset, Core::StreamIn& streamIn, const Core::String8& strAssetName);
			virtual ~IAudio(void);

			virtual bool is_sequenceable(void) const PURE;
			virtual bool is_streamable(void) const PURE;
			virtual bool is_platform_supported(void) const PURE;

			virtual FXD::U64 get_total_samples(void) PURE;
			virtual FXD::F32 get_playback_length(void) PURE;

			virtual void seek_time(FXD::F32 fVal) PURE;

			const Core::StringView8 get_asset_type(void) const FINAL { return UTF_8("Audio"); }

			virtual FXD::U64 length(void) PURE;
			virtual FXD::S32 tell(void) PURE;
			virtual FXD::U64 seek_to(FXD::U64 nOffset, Core::E_SeekOffset eFrom) PURE;

			GET_R(SND::E_AudioAsset, eAsset, eAsset);
			GET_R(SND::SndFormat, sourceFormat, source_format);

		protected:
			SND::E_AudioAsset m_eAsset;
			SND::SndFormat m_sourceFormat;
			Core::StreamIn m_streamIn;
		};

		namespace File
		{
			extern const Core::StringView8 ToString(SND::E_AudioAsset eAsset);
			extern SND::E_AudioAsset ToFileType(const Core::StringView8 strType);
		} //namespace File

	} //namespace SND
} //namespace FXD
#endif //FXDENGINE_SND_DECODERS_AUDIO_H