// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_DECODERS_AUDIOOGG_H
#define FXDENGINE_SND_DECODERS_AUDIOOGG_H

#include "FXDEngine/SND/Decoders/Audio.h"

#include <3rdParty/ogg/ogg.h>
#include <3rdParty/vorbis/vorbisfile.h>

namespace FXD
{
	namespace SND
	{
		// ------
		// IAudioOgg
		// -
		// ------
		class IAudioOgg FINAL : public SND::IAudio
		{
		public:
			IAudioOgg(Core::StreamIn& streamIn, const Core::String8& strAssetName);
			~IAudioOgg(void);

			bool is_sequenceable(void) const FINAL;
			bool is_streamable(void) const FINAL;
			bool is_platform_supported(void) const FINAL;

			FXD::U64 get_total_samples(void) FINAL;
			FXD::F32 get_playback_length(void) FINAL;

			void seek_time(FXD::F32 fVal) FINAL;

			FXD::U64 read_size(void* pDst, const FXD::U64 nSize) FINAL;				// Read data from the stream, and return number of bytes read.

		private:
			FXD::U64 length(void) FINAL;														// Get the total length of the stream.
			FXD::S32 tell(void) FINAL;															// Return current stream position (or -1 on error).
			FXD::U64 seek_to(FXD::U64 nOffset, Core::E_SeekOffset eFrom) FINAL;	// Seek to a position in the stream.

		private:
			OggVorbis_File m_oggFile;
			vorbis_info* m_pVorbisInfo;
			vorbis_comment* m_pVorbisComment;
		};

	} //namespace SND
} //namespace FXD
#endif //FXDENGINE_SND_DECODERS_AUDIOOGG_H