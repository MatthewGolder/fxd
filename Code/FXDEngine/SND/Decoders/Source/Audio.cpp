// Creator - MatthewGolder
#include "FXDEngine/SND/Decoders/Audio.h"

using namespace FXD;
using namespace SND;

// ------
// IAudio
// -
// ------
IAudio::IAudio(SND::E_AudioAsset eAsset, Core::StreamIn& streamIn, const Core::String8& strAssetName)
	: IAsset(strAssetName)
	, IStreamIn(nullptr, Core::CompressParams::Default)
	, m_eAsset(eAsset)
	, m_streamIn(streamIn)
{
}

IAudio::~IAudio(void)
{
}

// ------
// File
// -
// ------
const Core::StringView8 sSndFileTypes[] =
{
	"Ogg",			// SND::E_AudioAsset::Ogg
	"Wav",			// SND::E_AudioAsset::Wav
	"Procedural",	// SND::E_AudioAsset::Procedural
	"Count"			// SND::E_AudioAsset::Count
};

const Core::StringView8 FXD::SND::File::ToString(SND::E_AudioAsset eAsset)
{
	return sSndFileTypes[FXD::STD::to_underlying(eAsset)];
}

SND::E_AudioAsset FXD::SND::File::ToFileType(const Core::StringView8 strType)
{
	for (FXD::U16 n1 = 0; n1 < FXD::STD::to_underlying(SND::E_AudioAsset::Count); ++n1)
	{
		if (strType.ends_with_no_case(sSndFileTypes[n1]))
		{
			return (SND::E_AudioAsset)n1;
		}
	}
	return SND::E_AudioAsset::Unknown;
}