// Creator - MatthewGolder
#include "FXDEngine/SND/Decoders/AudioWav.h"
#include "FXDEngine/Core/OS.h"
#include "FXDEngine/Memory/StreamMemHandle.h"

using namespace FXD;
using namespace SND;

// ------
// FormatChunk
// -
// ------
class FormatChunk
{
public:
	FXD::S32 nChunkDataSize;
	FXD::S16 nCompressionCode;
	FXD::S16 nNumberOfChannels;
	FXD::S32 nSampleRate;
	FXD::S32 nAverageBytesPerSecond;
	FXD::S16 nBlockAlign;
	FXD::S16 nSignificantBitsPerSample;
};

// ------
// DataChunk
// -
// ------
class DataChunk
{
public:
	FXD::S32 nSize;
};

// ------
// IAudioWav
// -
// ------
IAudioWav::IAudioWav(Core::StreamIn& streamIn, const Core::String8& strAssetName)
	: IAudio(SND::E_AudioAsset::Wav, streamIn, strAssetName)
	, m_nStartOffset(0)
{
	FormatChunk formatChunk = {};
	Memory::MemZero_T(formatChunk);
	DataChunk dataChunk = {};
	Memory::MemZero_T(dataChunk);

	auto RIFFChunk = [&]()
	{
		FXD::S32 nFileSize = 0;
		(*streamIn) >> nFileSize;
	};
	auto WAVChunk = [&]()
	{
	};
	auto JUNKChunk = [&]()
	{
		FXD::S32 nSize = 0;
		(*streamIn) >> nSize;
		streamIn->seek_to(nSize, Core::E_SeekOffset::Current);
	};
	auto BEXTChunk = [&]()
	{
		FXD::S32 nSize = 0;
		(*streamIn) >> nSize;
		streamIn->seek_to(nSize, Core::E_SeekOffset::Current);
	};
	auto FMTChunk = [&]()
	{
		(*streamIn) >> formatChunk.nChunkDataSize;
		(*streamIn) >> formatChunk.nCompressionCode;
		(*streamIn) >> formatChunk.nNumberOfChannels;
		(*streamIn) >> formatChunk.nSampleRate;
		(*streamIn) >> formatChunk.nAverageBytesPerSecond;
		(*streamIn) >> formatChunk.nBlockAlign;
		(*streamIn) >> formatChunk.nSignificantBitsPerSample;
	};
	auto DATAChunk = [&]()
	{
		(*streamIn) >> dataChunk.nSize;
	};

	bool bFinished = false;
	while (!bFinished)
	{
		FXD::UTF8 chunkID[4];
		streamIn->read_size(chunkID, 4);
		if (Core::CharTraits< FXD::UTF8 >::compare_no_case(chunkID, "RIFF", 4) == 0)
		{
			RIFFChunk();
		}
		else if (Core::CharTraits< FXD::UTF8 >::compare_no_case(chunkID, "WAVE", 4) == 0)
		{
			WAVChunk();
		}
		else if (Core::CharTraits< FXD::UTF8 >::compare_no_case(chunkID, "BEXT", 4) == 0)
		{
			BEXTChunk();
		}
		else if (Core::CharTraits< FXD::UTF8 >::compare_no_case(chunkID, "JUNK", 4) == 0)
		{
			JUNKChunk();
		}
		else if (Core::CharTraits< FXD::UTF8 >::compare_no_case(chunkID, "FMT ", 4) == 0)
		{
			FMTChunk();
		}
		else if (Core::CharTraits< FXD::UTF8 >::compare_no_case(chunkID, "DATA", 4) == 0)
		{
			DATAChunk();
			m_nStartOffset = streamIn->tell();
			m_sourceFormat.Format = formatChunk.nCompressionCode;
			m_sourceFormat.Channels = formatChunk.nNumberOfChannels;
			m_sourceFormat.SampleRate = formatChunk.nSampleRate;
			m_sourceFormat.BytesPerSec = formatChunk.nAverageBytesPerSecond;
			m_sourceFormat.BitsPerSample = formatChunk.nSignificantBitsPerSample;
			bFinished = true;
		}
		else
		{
			PRINT_ASSERT << "SND: Input file found unknown chunk " << chunkID << "";
		}
	}
}

IAudioWav::~IAudioWav(void)
{
}

bool IAudioWav::is_sequenceable(void) const
{
	return true;
}

bool IAudioWav::is_streamable(void) const
{
	return true;
}

bool IAudioWav::is_platform_supported(void) const
{
	const Core::E_OS eCurrPlatform = FXD::Core::OS::OSType();
	switch (eCurrPlatform)
	{
		case Core::E_OS::Windows:
		{
			return true;
		}
		case Core::E_OS::Android:
		{
			return true;
		}
		default:
		{
			return false;
		}
	}
}

FXD::U64 IAudioWav::get_total_samples(void)
{
	FXD::U64 nSampleCount = 0.0f;
	if (m_streamIn)
	{
		FXD::U32 nDataSize = length();
		FXD::U32 nBytesPerSample = m_sourceFormat.getBytesPerSample();
		nSampleCount = (nDataSize / (m_sourceFormat.Channels * nBytesPerSample));
	}
	return nSampleCount;
}

FXD::F32 IAudioWav::get_playback_length(void)
{
	FXD::U64 nNumSamples = get_total_samples();
	FXD::F32 fLength = ((FXD::F32)nNumSamples / (FXD::F32)m_sourceFormat.SampleRate);
	return fLength;
}

void IAudioWav::seek_time(FXD::F32 fVal)
{
	FXD::F32 fTime = ((FXD::F32)m_sourceFormat.BytesPerSec * fVal);
	seek_to(fTime, Core::E_SeekOffset::Begin);
}

FXD::U64 IAudioWav::read_size(void* pDst, const FXD::U64 nSize)
{
	FXD::U64 nAmountRead = 0;
	FXD::U64 nResult = 0;

	while (nAmountRead < nSize)
	{
		nResult = m_streamIn->read_size(pDst, nSize);
		if (nResult > 0)
		{
			nAmountRead += nResult;
		}
		else if (nResult < 0)
		{
			PRINT_ASSERT << "SND: ";
		}
		else
		{
			break;
		}
	}
	return nAmountRead;
}

FXD::U64 IAudioWav::length(void)
{
	if (m_streamIn)
	{
		return (m_streamIn->length() - m_nStartOffset);
	}
	return 0;
}

FXD::S32 IAudioWav::tell(void)
{
	if (m_streamIn)
	{
		return (m_streamIn->tell() - m_nStartOffset);
	}
	return 0;
}

FXD::U64 IAudioWav::seek_to(FXD::U64 nOffset, Core::E_SeekOffset eFrom)
{
	if (m_streamIn)
	{
		FXD::U64 nSeek = m_streamIn->seek_to((nOffset + m_nStartOffset), eFrom);
		return (nSeek - m_nStartOffset);
	}
	return 0;
}