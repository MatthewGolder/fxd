// Creator - MatthewGolder
#include "FXDEngine/SND/Decoders/AudioOgg.h"
#include "FXDEngine/Core/OS.h"

using namespace FXD;
using namespace SND;

// ------
// SNDOgg
// -
// ------
namespace SNDOgg
{
	size_t read_func(void* pPtr, size_t nSize, size_t nNmemb, void* pDatasource)
	{
		Core::IStreamIn* pStreamIn = (Core::IStreamIn*)pDatasource;
		size_t nBytesRead = 0;
		if (pStreamIn != nullptr)
		{
			nBytesRead = pStreamIn->read_size(pPtr, (nSize * nNmemb));
		}
		return nBytesRead;
	}
	int seek_func(void* pData, ogg_int64_t nOffset, int nWhence)
	{
		Core::E_SeekOffset eSeekType = Core::E_SeekOffset::Begin;
		switch (nWhence)
		{
			case SEEK_SET:
			{
				eSeekType = Core::E_SeekOffset::Begin;
				break;
			}
			case SEEK_CUR:
			{
				eSeekType = Core::E_SeekOffset::Current;
				break;
			}
			case SEEK_END:
			{
				eSeekType = Core::E_SeekOffset::End;
				break;
			}
			default:
			{
				break;
			}
		}

		Core::IStreamIn* pStreamIn = (Core::IStreamIn*)pData;
		FXD::U32 nRetVal = 0;
		if (pStreamIn != nullptr)
		{
			nRetVal = pStreamIn->seek_to(nOffset, eSeekType);
		}
		return (int)nRetVal;
	}
	int close_func(void* pData)
	{
		Core::IStreamIn* pStreamIn = (Core::IStreamIn*)pData;
		if (pStreamIn != nullptr)
		{
			return 0;
		}
		return 0;
	}
	long tell_func(void* pData)
	{
		Core::IStreamIn* pStreamIn = (Core::IStreamIn*)pData;
		FXD::S32 nTellVal = 0;
		if (pStreamIn != nullptr)
		{
			nTellVal = pStreamIn->tell();
		}
		return nTellVal;
	}
} //namespace SNDOgg

// ------
// IAudioOgg
// -
// ------
IAudioOgg::IAudioOgg(Core::StreamIn& streamIn, const Core::String8& strAssetName)
	: IAudio(SND::E_AudioAsset::Ogg, streamIn, strAssetName)
	, m_pVorbisInfo(nullptr)
	, m_pVorbisComment(nullptr)
{
	ov_callbacks oggCallbacks = {};
	Memory::MemZero_T(oggCallbacks);
	oggCallbacks.read_func = &SNDOgg::read_func;
	oggCallbacks.close_func = &SNDOgg::close_func;
	oggCallbacks.seek_func = &SNDOgg::seek_func;
	oggCallbacks.tell_func = &SNDOgg::tell_func;
	if (ov_open_callbacks(m_streamIn.get(), &m_oggFile, nullptr, 0, oggCallbacks) < 0)
	{
		PRINT_ASSERT << "SND: ov_open_callbacks() failed";
	}

	m_pVorbisInfo = ov_info(&m_oggFile, -1);
	m_pVorbisComment = ov_comment(&m_oggFile, -1);

	m_sourceFormat.Format = 1;
	m_sourceFormat.Channels = (FXD::U16)m_pVorbisInfo->channels;
	m_sourceFormat.SampleRate = m_pVorbisInfo->rate;
	m_sourceFormat.BytesPerSec = (m_pVorbisInfo->rate * m_pVorbisInfo->channels * 2);
	m_sourceFormat.BitsPerSample = 16;
}

IAudioOgg::~IAudioOgg(void)
{
	vorbis_comment_clear(m_pVorbisComment);
	m_pVorbisComment = nullptr;

	vorbis_info_clear(m_pVorbisInfo);
	m_pVorbisInfo = nullptr;

	ov_clear(&m_oggFile);
}

bool IAudioOgg::is_sequenceable(void) const
{
	return false;
}

bool IAudioOgg::is_streamable(void) const
{
	return true;
}

bool IAudioOgg::is_platform_supported(void) const
{
	const Core::E_OS eCurrPlatform = FXD::Core::OS::OSType();
	switch (eCurrPlatform)
	{
		case Core::E_OS::Windows:
		{
			return true;
		}
		case Core::E_OS::Android:
		{
			return true;
		}
		default:
		{
			return false;
		}
	}
}

FXD::U64 IAudioOgg::get_total_samples(void)
{
	ogg_int64_t nRetSamples = ov_pcm_total(&m_oggFile, -1);
	return nRetSamples;
}

FXD::F32 IAudioOgg::get_playback_length(void)
{
	FXD::U64 nNumSamples = get_total_samples();
	FXD::F32 fLength = ((FXD::F32)nNumSamples / (FXD::F32)m_sourceFormat.SampleRate);
	return fLength;
}

void IAudioOgg::seek_time(FXD::F32 fVal)
{
	ov_time_seek(&m_oggFile, (double)fVal);
}

FXD::U64 IAudioOgg::read_size(void* pDst, const FXD::U64 nSize)
{
	FXD::U64 nAmountRead = 0;
	FXD::S32 nSection = 0;
	FXD::S32 nResult = 0;
	char* pBuffer = (char*)pDst;
	while (nAmountRead < nSize)
	{
		nResult = ov_read(&m_oggFile, pBuffer + nAmountRead, nSize - nAmountRead, 0, 2, 1, &nSection);
		if (nResult > 0)
		{
			nAmountRead += nResult;
		}
		else if (nResult == 0)
		{
			break;
		}
		else
		{
			switch (nResult)
			{
				case OV_HOLE:
				{
					PRINT_ASSERT << "SND: ov_read() indicates there was an interruption in the data";
					break;
				}
				case OV_EBADLINK:
				{
					PRINT_ASSERT << "SND: ov_read() indicates that an invalid stream section was supplied to libvorbisfile, or the requested link is corrupt";
					break;
				}
				case OV_EINVAL:
				{
					PRINT_ASSERT << "SND: ov_read() indicates the initial file headers couldn't be read or are corrupt, or that the initial open call for vf failed";
					break;
				}
				default:
				{
					PRINT_ASSERT << "SND: ov_read() with unknown value " << nResult << "";
					break;
				}
			}
		}
	}
	return nAmountRead;
}

FXD::U64 IAudioOgg::length(void)
{
	if (m_streamIn)
	{
		return m_streamIn->length();
	}
	return 0;
}

FXD::S32 IAudioOgg::tell(void)
{
	PRINT_ASSERT << "SND: IAudioOgg::tell()";
	return 0;
}

FXD::U64 IAudioOgg::seek_to(FXD::U64 nOffset, Core::E_SeekOffset eFrom)
{
	FXD::F64 fTime = ((FXD::F64)nOffset / (FXD::F64)m_sourceFormat.getBytesPerSample());
	switch (eFrom)
	{
		case Core::E_SeekOffset::Begin:
		{
			ov_time_seek(&m_oggFile, (double)fTime);
			break;
		}
		case Core::E_SeekOffset::Current:
		{
			FXD::F64 fTell = ov_time_tell(&m_oggFile);
			ov_time_seek(&m_oggFile, (double)(fTime + fTell));
			break;
		}
		case Core::E_SeekOffset::End:
		{
			FXD::F64 fTell = get_playback_length();
			ov_time_seek(&m_oggFile, (double)(fTime + fTell));
			break;
		}
	}
	return nOffset;
}