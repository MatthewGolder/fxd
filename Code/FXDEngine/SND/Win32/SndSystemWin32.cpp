// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsOSPC()
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/SND/SndSystem.h"
#include "FXDEngine/SND/OAL/SndAdapterOAL.h"
#include "FXDEngine/SND/OAL/SndApiOAL.h"
#include "FXDEngine/SND/XA2/SndAdapterXA2.h"
#include "FXDEngine/SND/XA2/SndApiXA2.h"

using namespace FXD;
using namespace SND;

// ------
// ISndSystem
// - 
// ------
bool ISndSystem::_create_system(void)
{
	// 1. Early out if the api already exists
	if (m_sndApi)
	{
		return false;
	}

	// 2. Create the desired api
	SND::E_SndApi eApi = SND::Funcs::ToApiType(FXDPlatform()->cmd_args().get_value(UTF_8("sndapi")).c_str());
	FXDPlatform()->globals()[UTF_8("sndapi")] = eApi;
#if IsSndXAudio2()
	if ((eApi == E_SndApi::XAudio2) || (eApi == E_SndApi::Default) || (eApi == E_SndApi::Unknown))
	{
		m_sndApi = std::make_unique< SND::ISndApiXA2 >();
	}
#endif //IsSndXAudio2()
#if IsSndOpenAL()
	if (eApi == E_SndApi::OpenAL) 
	{
		m_sndApi = std::make_unique< SND::ISndApiOAL >();
	}
#endif //IsSndOpenAL()

	// 3. Create the desired api 
	bool bRetVal = m_sndApi->create_api();
	return bRetVal;
}

bool ISndSystem::_release_system(void)
{
	FXD_RELEASE(m_sndApi, release_api());
	return true;
}

SND::SndAdapter ISndSystem::_create_adapter(const App::AdapterDesc& adapterDesc)
{
	// 1. Create the desired adapter
	SND::E_SndApi eApi = m_sndApi->sndApiType();
	SND::SndAdapter sndAdapter = {};
#if IsSndOpenAL()
	if (eApi == E_SndApi::OpenAL)
	{
		sndAdapter = std::make_shared< SND::ISndAdapterOAL >(adapterDesc, m_sndApi.get());
	}
#endif //IsSndOpenAL()
#if IsSndXAudio2()
	if ((eApi == E_SndApi::XAudio2) || (eApi == E_SndApi::Default) || (eApi == E_SndApi::Unknown))
	{
		sndAdapter = std::make_shared< SND::ISndAdapterXA2 >(adapterDesc, m_sndApi.get());
	}
#endif //IsSndXAudio2()

	// 2. Create the desired adapter 
	bool bRetVal = sndAdapter->create().get();
	if (!bRetVal)
	{
		return nullptr;
	}

	m_sndAdapters.push_back(sndAdapter);
	return sndAdapter;
}

bool ISndSystem::_release_adapter(SND::SndAdapter& sndAdapter)
{
	if (sndAdapter)
	{
		sndAdapter->release().get();
		m_sndAdapters.find_erase(sndAdapter);
	}
	return true;
}
#endif //IsOSPC()