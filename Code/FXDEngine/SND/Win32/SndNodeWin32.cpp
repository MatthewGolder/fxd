// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsOSPC()
#include "FXDEngine/SND/SndAdapter.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/App/Platform.h"
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/SndNode.h"
#include "FXDEngine/SND/XA2/SndNodeXA2.h"
#include "FXDEngine/SND/OAL/SndNodeOAL.h"
#include "FXDEngine/SND/Decoders/Audio.h"

using namespace FXD;
using namespace SND;

// ------
// INodeMixer
// -
// ------
SND::NodeMixer INodeMixer::_add_mixer(const SND::NODE_DESC& nodeDesc)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");
	PRINT_COND_ASSERT((128 > m_mixers.size()), "SND: Cannot add any more children nodes to this node");
	PRINT_COND_ASSERT(!m_mixers.contains(nodeDesc.Name), "SND: Duplicate node added");

	SND::E_SndApi eApi = (*STD::any_cast< SND::E_SndApi >(&FXD::FXDPlatform()->globals()[UTF_8("sndapi")]));
	SND::NodeMixer node = {};
#if IsSndOpenAL()
	if (eApi == E_SndApi::OpenAL)
	{
		node = std::make_shared< SND::INodeMixerOAL >(m_pSndAdapter, this);
	}
#endif //IsSndOpenAL()
#if IsSndXAudio2()
	if ((eApi == E_SndApi::XAudio2) || (eApi == E_SndApi::Default) || (eApi == E_SndApi::Unknown))
	{
		node = std::make_shared< SND::INodeMixerXA2 >(m_pSndAdapter, this);
	}
#endif //IsSndXAudio2()

	node->create(nodeDesc);
	m_mixers[nodeDesc.Name] = node;
	return node;
}

SND::NodeVoice INodeMixer::_add_voice(const SND::NODE_VOICE_DESC& nodeVoiceDesc)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "Sound is not allowed on current thread");
	PRINT_COND_ASSERT((512 > m_voices.size()), "SND: Cannot add any more children nodes to this node");

	SND::E_SndApi eApi = (*STD::any_cast< SND::E_SndApi >(&FXD::FXDPlatform()->globals()[UTF_8("sndapi")]));
	SND::NodeVoice nodeVoice = {};
#if IsSndOpenAL()
	if (eApi == E_SndApi::OpenAL)
	{
		nodeVoice = std::make_shared< SND::INodeVoiceOAL >(m_pSndAdapter, this);
	}
#endif //IsSndOpenAL()
#if IsSndXAudio2()
	if ((eApi == E_SndApi::XAudio2) || (eApi == E_SndApi::Default) || (eApi == E_SndApi::Unknown))
	{
		nodeVoice = std::make_shared< SND::INodeVoiceXA2 >(m_pSndAdapter, this);
	}
#endif //IsSndXAudio2()

	SND::NODE_DESC nodeDesc = {};
	nodeDesc.Volume = 1.0f;
	nodeDesc.OutChannels = nodeVoiceDesc.Audio->source_format().Channels;
	nodeDesc.SampleRate = nodeVoiceDesc.Audio->source_format().SampleRate;
	nodeVoice->create(nodeDesc, nodeVoiceDesc);
	m_voices.push_back(nodeVoice);

	return nodeVoice;
}
#endif //IsOSPC()