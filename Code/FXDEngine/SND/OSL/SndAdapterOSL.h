// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_OSL_SNDADAPTEROSL_H
#define FXDENGINE_SND_OSL_SNDADAPTEROSL_H

#include "FXDEngine/SND/Types.h"

#if IsSndOpenSLES()
#include "FXDEngine/SND/SndAdapter.h"
#include "FXDEngine/SND/OSL/OpenSLES.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// ISndAdapterDescOSL
		// - 
		// ------
		class ISndAdapterDescOSL FINAL : public SND::ISndAdapterDesc
		{
		public:
			ISndAdapterDescOSL(const App::DeviceIndex nDeviceID);
			~ISndAdapterDescOSL(void);

		public:
			//ALint m_nIMajorVersion;
			//ALint m_nIMinorVersion;
			//ALint m_nICapsFlags;
		};

		// ------
		// ISndAdapterOSL
		// -
		// ------
		class ISndAdapterOSL FINAL : public SND::ISndAdapter
		{
		public:
			EXPLICIT ISndAdapterOSL(const App::AdapterDesc& adapterDesc, const App::IApi* pApi);
			~ISndAdapterOSL(void);

		protected:
			bool _create(void) FINAL;
			bool _release(void) FINAL;

			bool _is_vaild(void) const FINAL;

		protected:
			//ALCdevice* m_pDevice;
			//ALCcontext* m_pContext;
		};

	} //namespace SND
} //namespace FXD
#endif //IsSndOpenSLES()
#endif //FXDENGINE_SND_OSL_SNDADAPTEROSL_H