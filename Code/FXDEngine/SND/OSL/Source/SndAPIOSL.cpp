// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndOpenSLES()
#include "FXDEngine/SND/OSL/SndApiOSL.h"
#include "FXDEngine/SND/OSL/SndAdapterOSL.h"

using namespace FXD;
using namespace SND;

// ------
// ISndApiOSL
// - 
// ------
ISndApiOSL::ISndApiOSL(void)
	: ISndApi(SND::E_SndApi::OpenAL)
{
}

ISndApiOSL::~ISndApiOSL(void)
{
}

#define GET_AL_PROC(v, p, b) v = (p)alGetProcAddress(UTF_8(#v)); if (v == nullptr) { b = false; }

bool ISndApiOSL::create_api(void)
{
	// 1. Create the list of adapters
	_create_adapter_list();

	bool bRetVal = true;
	return bRetVal;
}

bool ISndApiOSL::release_api(void)
{
	return true;
}

void ISndApiOSL::_create_adapter_list(void)
{
}
#endif //IsSndOpenSLES()