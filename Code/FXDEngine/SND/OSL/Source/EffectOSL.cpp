// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndOpenSLES()
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/OSL/EffectOSL.h"
#include "FXDEngine/SND/OSL/SndAdapterOSL.h"
#include "FXDEngine/SND/OSL/SndApiOSL.h"
#include "FXDEngine/SND/OSL/SndNodeOSL.h"

using namespace FXD;
using namespace SND;

// ------
// OSL
// -
// ------
namespace OSL
{
	bool createReverb(const SND::ISndAdapter* pSndAdapter, SND::IEffect* pEffect)
	{
		if (pSndAdapter->is_vaild())
		{
			SND::IEffectOSL* pEffectOSL = static_cast<SND::IEffectOSL*>(pEffect);
			/*
			if (pEffectOSL->m_alEffectSlot == 0)
			{
				alGenAuxiliaryEffectSlots(1, &pEffectOSL->m_alEffectSlot);
				OSLResultCheckFail("alGenAuxiliaryEffectSlots");
				if (pEffectOSL->m_alEffectSlot == 0)
				{
					return false;
				}
			}
			*/
		}
		return true;
	}

	void shutdownReverb(const SND::ISndAdapter* pSndAdapter, SND::IEffect* pEffect)
	{
		if (pSndAdapter->is_vaild())
		{
			SND::IEffectOSL* pEffectOSL = static_cast< SND::IEffectOSL* >(pEffect);
			/*
			if (pEffectOSL->m_alEffectSlot != 0)
			{
				alDeleteAuxiliaryEffectSlots(1, &pEffectOSL->m_alEffectSlot);
				pEffectOSL->m_alEffectSlot = 0;
			}
			*/
		}
	}
} //namespace OSL

// ------
// IEffectOSL
// - 
// ------
IEffectOSL::IEffectOSL(const SND::EFFECT_DESC& effectDesc)
	: IEffect(effectDesc)
	//, m_alEffectSlot(0)
{
}
IEffectOSL::~IEffectOSL(void)
{
	//PRINT_COND_ASSERT((m_alEffectSlot == 0), "SND: m_alEffectSlot has not been shutdown");
}


// ------
// IEffectChainOSL
// - 
// ------
IEffectChainOSL::IEffectChainOSL(const SND::ISndAdapter* pSndAdapter, SND::INodeMixer* pNodeMixer)
	: IEffectChain(pSndAdapter, pNodeMixer)
{
}
IEffectChainOSL::~IEffectChainOSL(void)
{
}

bool IEffectChainOSL::_create_effect_chain(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");
	return true;
}

bool IEffectChainOSL::_release_effect_chain(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	fxd_for(auto& effect, m_effects)
	{
		effect->destroyed(true);
	}
	_recreate();
	return true;
}

void IEffectChainOSL::_add_effects(const Container::Vector< SND::EFFECT_DESC >& effectDescs)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	fxd_for(auto& effectDesc, effectDescs)
	{
		m_effects.push_back(std::make_shared< IEffectOSL >(effectDesc));
	}
	m_bNeedsRecreate = true;
}

void IEffectChainOSL::_enable_effect(FXD::U16 nSlot, bool bEnable)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	m_effects[nSlot]->effect_desc().Enabled = bEnable;
}

bool IEffectChainOSL::_is_effect_enabled(FXD::U16 nSlot) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	bool bState = m_effects[nSlot]->effect_desc().Enabled;
	return bState;
}

void IEffectChainOSL::_update(FXD::F32 dt)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (m_bNeedsRecreate)
	{
		_recreate();
	}
	if ((!m_bNeedsRecreate) && (m_bNeedsUpdate))
	{
		FXD::U32 nCount = 0;
		fxd_for(auto& effect, m_effects)
		{
			SND::IEffectOSL* pEffectOSL = static_cast< SND::IEffectOSL* >(effect.get());

			/*
			ALuint alEffect;
			alGenEffects(1, &alEffect);
			if (alEffect == 0)
			{
				return;
			}
			alEffecti(alEffect, AL_EFFECT_TYPE, AL_EFFECT_REVERB);

			alEffectf(alEffect, AL_REVERB_GAIN, effect->effect_desc().Gain);
			alEffectf(alEffect, AL_REVERB_GAINHF, effect->effect_desc().GainHF);
			alEffectf(alEffect, AL_REVERB_DECAY_TIME, effect->effect_desc().DecayTime);
			alEffectf(alEffect, AL_REVERB_DECAY_HFRATIO, effect->effect_desc().DecayHFRatio);
			alEffectf(alEffect, AL_REVERB_REFLECTIONS_GAIN, effect->effect_desc().ReflectionsGain);
			alEffectf(alEffect, AL_REVERB_REFLECTIONS_DELAY, effect->effect_desc().ReflectionsDelay);
			alEffectf(alEffect, AL_REVERB_LATE_REVERB_GAIN, effect->effect_desc().LateGain);
			alEffectf(alEffect, AL_REVERB_LATE_REVERB_DELAY, effect->effect_desc().LateDelay);
			alEffectf(alEffect, AL_REVERB_DIFFUSION, effect->effect_desc().Diffusion);
			alEffectf(alEffect, AL_REVERB_DENSITY, effect->effect_desc().Density);
			alEffectf(alEffect, AL_REVERB_AIR_ABSORPTION_GAINHF, effect->effect_desc().AirAbsorptionGainHF);
			alEffectf(alEffect, AL_REVERB_ROOM_ROLLOFF_FACTOR, effect->effect_desc().RoomRolloffFactor);
			alEffecti(alEffect, AL_REVERB_DECAY_HFLIMIT, 0x1);

			alAuxiliaryEffectSloti(pEffectOSL->m_alEffectSlot, AL_EFFECTSLOT_EFFECT, alEffect);
			OSLResultCheckFail("alAuxiliaryEffectSloti");

			alDeleteEffects(1, &alEffect);
			*/
			nCount++;
		}
		m_bNeedsUpdate = false;
	}
}

void IEffectChainOSL::_recreate(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1. Remove the effects on all of the mixer voices
	FXD::U32 nCount = 0;
	fxd_for(auto& effect, m_effects)
	{
		IEffectOSL* pEffectOSL = (IEffectOSL*)effect.get();
		/*
		if (pEffectOSL->m_alEffectSlot != 0)
		{
			fxd_for(auto& voice, m_pNodeMixer->voices())
			{
				INodeVoiceOSL* pNodeVoiceOSL = (INodeVoiceOSL*)voice.get();
				alSource3i(pNodeVoiceOSL->m_alSource, AL_EFFECTSLOT_NULL, pEffectOSL->m_alEffectSlot, 0, AL_FILTER_NULL);
			}
		}
		*/
		nCount++;
	}

	// 2. Remove destroyed effects
	fxd_for_itr(itr, m_effects)
	{
		if ((*itr)->destroyed())
		{
			::OSL::shutdownReverb(m_pSndAdapter, (*itr).get());
			itr = m_effects.erase(itr);
		}
	}

	nCount = 0;
	if (m_effects.not_empty())
	{
		// 3. Create the new effects
		fxd_for(auto& effect, m_effects)
		{
			SND::IEffectOSL* pEffectOSL = static_cast< SND::IEffectOSL* >(effect.get());
			::OSL::createReverb(m_pSndAdapter, pEffectOSL);

			fxd_for(auto& voice, m_pNodeMixer->voices())
			{
				/*
				SND::INodeVoiceOSL* pNodeVoiceOSL = static_cast< SND::INodeVoiceOSL* >(voice.get());
				alSource3i(pNodeVoiceOSL->m_alSource, AL_AUXILIARY_SEND_FILTER, pEffectOSL->m_alEffectSlot, 0, AL_FILTER_NULL);
				OSLResultCheckFail("alSource3i");
				*/
			}
			nCount++;
		}
		m_bNeedsUpdate = true;
	}
	m_bNeedsRecreate = false;
	return;
}
#endif //IsSndOpenSLES()