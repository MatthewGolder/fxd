// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndOpenSLES()
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/OSL/EmitterOSL.h"
#include "FXDEngine/SND/OSL/ListenerOSL.h"
#include "FXDEngine/SND/OSL/SndAdapterOSL.h"
#include "FXDEngine/SND/OSL/SndNodeOSL.h"
#include "FXDEngine/SND/OSL/VoiceOSL.h"

using namespace FXD;
using namespace SND;

// ------
// IEmitterOSL
// - 
// ------
IEmitterOSL::IEmitterOSL(const SND::ISndAdapter* pSndAdapter, const SND::INodeVoice* pNodeVoice)
	: IEmitter(pSndAdapter, pNodeVoice)
{
}

IEmitterOSL::~IEmitterOSL(void)
{
}

bool IEmitterOSL::_create_emitter(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1.
	set(Math::Matrix4F::kIdentity);
	set_velocity(Math::Vector3F::kZero);
	set_min_max_distance(m_fMinDistance, m_fMaxDistance);

	// 2.
	//SND::INodeVoiceOSL* pVoiceNodeOSL = (SND::INodeVoiceOSL*)(m_pNodeVoice);
	//alSourcei(pVoiceNodeOSL->m_alSource, AL_SOURCE_RELATIVE, ((b3D) ? AL_FALSE : AL_TRUE));
	//if (b3D)
	//	alSourcef(pVoiceNodeOSL->m_alSource, AL_ROLLOFF_FACTOR, m_pSndAdapter->listener()->rolloff_factor());
	return true;
}

bool IEmitterOSL::_release_emitter(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	return true;
}

void IEmitterOSL::_update(const SND::Listener& listener)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	Math::Vector3F vPos = position();
	Math::Vector3F vUp = up();
	Math::Vector3F vForward = forward();
	Math::Vector3F vVelocity = velocity();

	FXD::F32 fPosition[3] = { vPos.x(), vPos.y(), -vPos.z() };
	FXD::F32 fVelocity[3] = { vVelocity.x(), vVelocity.y(), -vVelocity.z() };
	FXD::F32 fOrient[6] = { vForward.x(), vForward.y(), -vForward.z(), vUp.x(), vUp.y(), -vUp.z() };

	//SND::INodeVoiceOSL* pVoiceNodeOSL = (SND::INodeVoiceOSL*)(m_pNodeVoice);
	//alSourcefv(pVoiceNodeOSL->m_alSource, AL_POSITION, fPosition);
	//alSourcefv(pVoiceNodeOSL->m_alSource, AL_VELOCITY, fVelocity);
	//alSourcefv(pVoiceNodeOSL->m_alSource, AL_ORIENTATION, fOrient);

	//alSourcef(pVoiceNodeOSL->m_alSource, AL_REFERENCE_DISTANCE, m_fMinDistance);
	//alSourcef(pVoiceNodeOSL->m_alSource, AL_MAX_DISTANCE, m_fMaxDistance);
}

void IEmitterOSL::_update_distance_model(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	//SND::INodeVoiceOSL* pVoiceNodeOSL = (SND::INodeVoiceOSL*)(m_pNodeVoice);
	//alSourcef(pVoiceNodeOSL->m_alSource, AL_REFERENCE_DISTANCE, m_fMinDistance);
	//alSourcef(pVoiceNodeOSL->m_alSource, AL_MAX_DISTANCE, m_fMaxDistance);
}
#endif //IsSndOpenSLES()