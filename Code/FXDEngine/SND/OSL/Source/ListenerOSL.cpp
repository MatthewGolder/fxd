// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndOpenSLES()
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/OSL/ListenerOSL.h"
#include "FXDEngine/SND/OSL/SndAdapterOSL.h"

using namespace FXD;
using namespace SND;

// ------
// OAL
// -
// ------
//extern FXD::Container::Vector< ALint > m_sourceLog;
namespace OSL
{
	void set_rolloff_factorInternal(FXD::F32 fFactor)
	{
		/*
		fxd_for(const auto& source, m_sourceLog)
		{
			ALint nSource = source;
			alSourcef(nSource, AL_ROLLOFF_FACTOR, fFactor);
		}
		*/
	}
} //namespace OSL

// ------
// IListenerOSL
// - 
// ------
IListenerOSL::IListenerOSL(const SND::ISndAdapter* pSndAdapter)
	: IListener(pSndAdapter)
	, m_fUserRolloffFactor(1.0f)
{
}

IListenerOSL::~IListenerOSL(void)
{
}

bool IListenerOSL::_create(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1.
	set_position(Math::Vector3F::kZero);
	set_up(Math::Vector3F::kUnitY);
	set_forward(Math::Vector3F::kUnitZ);
	set_velocity(Math::Vector3F::kZero);
	set_distance_model(E_SndDistanceModel::Linear);
	set_doppler_factor(1.0f);
	set_rolloff_factor(1.0f);
	return true;
}

bool IListenerOSL::_release(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	return true;
}

void IListenerOSL::_update(FXD::F32 dt)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	{
		Thread::RWLockData< _ListenerData >::ScopedWriteLock wLock(m_listenerData);
		if (wLock->m_eDistanceModel.is_dirty())
			_set_distance_model(wLock->m_eDistanceModel.get());
		if (wLock->m_fDopplerFactor.is_dirty())
			_set_doppler_factor(wLock->m_fDopplerFactor.get());
		if (wLock->m_fRolloffFactor.is_dirty())
			_set_rolloff_factor(wLock->m_fRolloffFactor.get());
	}

	Math::Vector3F pos = this->position();
	Math::Vector3F up = this->up();
	Math::Vector3F forward = this->forward();
	Math::Vector3F velocity = this->velocity();

	FXD::F32 fPosition[3] = { pos.x(), pos.y(), -pos.z() };
	FXD::F32 fVelocity[3] = { velocity.x(), velocity.y(), -velocity.z() };
	FXD::F32 fOrient[6] = { forward.x(), forward.y(), -forward.z(), up.x(), up.y(), -up.z() };

	//alListenerfv(AL_POSITION, fPosition);
	//alListenerfv(AL_ORIENTATION, fOrient);
	//alListenerfv(AL_VELOCITY, fVelocity);
}

void IListenerOSL::_set_distance_model(SND::E_SndDistanceModel eDistanceModel)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	Thread::RWLockData< _ListenerData >::ScopedWriteLock wLock(m_listenerData);

	switch (eDistanceModel)
	{
	case E_SndDistanceModel::Linear:
	{
		//alDistanceModel(AL_LINEAR_DISTANCE_CLAMPED);
		if (wLock->m_fRolloffFactor != 1.0f)
			_set_rolloff_factorInternal(1.0f); // No rolloff on linear.
		break;
	}
	case E_SndDistanceModel::Logarithmic:
	{
		//alDistanceModel(AL_INVERSE_DISTANCE_CLAMPED);
		if (m_fUserRolloffFactor != wLock->m_fRolloffFactor.get())
			_set_rolloff_factorInternal(m_fUserRolloffFactor);
		break;
	}
	default:
		PRINT_ASSERT << "SND: set_distance_model - distance model not implemented";
	}
	wLock->m_eDistanceModel = eDistanceModel;
	wLock->m_eDistanceModel.mark_clean();
}

void IListenerOSL::_set_doppler_factor(FXD::F32 fFactor)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	Thread::RWLockData< _ListenerData >::ScopedWriteLock wLock(m_listenerData);

	//alDopplerFactor(fFactor);
	wLock->m_fDopplerFactor = fFactor;
	wLock->m_fDopplerFactor.mark_clean();
}

void IListenerOSL::_set_rolloff_factor(FXD::F32 fFactor)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	Thread::RWLockData< _ListenerData >::ScopedWriteLock wLock(m_listenerData);

	if ((wLock->m_eDistanceModel == E_SndDistanceModel::Linear) && (fFactor != 1.0f))
		PRINT_ASSERT << "SND: set_rolloff_factor - rolloff factor <> 1.0f ignored in linear distance model";
	else
		_set_rolloff_factorInternal(fFactor);
	wLock->m_fRolloffFactor = fFactor;
	wLock->m_fRolloffFactor.mark_clean();
}

void IListenerOSL::_set_rolloff_factorInternal(FXD::F32 fFactor)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	Thread::RWLockData< _ListenerData >::ScopedWriteLock wLock(m_listenerData);

	::OSL::set_rolloff_factorInternal(fFactor);
	wLock->m_fRolloffFactor = fFactor;
}
#endif //IsSndOpenSLES()