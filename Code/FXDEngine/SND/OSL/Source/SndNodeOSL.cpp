// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndOpenSLES()
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/Listener.h"
#include "FXDEngine/SND/OSL/EffectOSL.h"
#include "FXDEngine/SND/OSL/EmitterOSL.h"
#include "FXDEngine/SND/OSL/SndAdapterOSL.h"
#include "FXDEngine/SND/OSL/SndNodeOSL.h"
#include "FXDEngine/SND/OSL/VoiceOSL.h"
#include "FXDEngine/SND/Decoders/Audio.h"

using namespace FXD;
using namespace SND;

// ------
// OSL
// -
// ------
//FXD::Container::Vector< ALint > m_sourceLog;
namespace OSL
{

	bool createMasterNode(const SND::ISndAdapter* /*pSndAdapter*/, SND::INodeMixer* /*pNodeMixer*/)
	{
		// 1. Create CreateMasteringVoice
		return true;
	}

	bool createSubmixNode(const SND::ISndAdapter* /*pSndAdapter*/, SND::INodeMixer* /*pNodeMixer*/)
	{
		// 1. Create CreateMasteringVoice
		return true;
	}

	bool createVoiceNode(const SND::ISndAdapter* pSndAdapter, SND::INodeVoice* pNodeVoice, const SND::SndFormat& /*sndFormat*/, bool b3D)
	{
		// 1. Create alGenSources
		/*
		INodeVoiceOAL* pNodeOAL = (SND::INodeVoiceOAL*)pNodeVoice;

		alGenSources(1, &pNodeOAL->m_alSource);
		if (pNodeOAL->m_alSource == 0)
		{
			PRINT_WARN << "SND: alGenSources() failed";
			return false;
		}

		alSourcei(pNodeOAL->m_alSource, AL_SOURCE_RELATIVE, ((b3D) ? AL_FALSE : AL_TRUE));
		if (b3D)
		{
			alSourcef(pNodeOAL->m_alSource, AL_ROLLOFF_FACTOR, pSndAdapter->listener()->rolloff_factor());
		}

		m_sourceLog.push_back(pNodeOAL->m_alSource);
		*/
		return true;
	}

	void shutdownMixerNode(SND::INodeMixer* /*pNodeMixer*/)
	{
		// 1. Destroy mixer
	}

	void shutdownVoiceNode(SND::INodeVoice* pNodeVoice)
	{
		// 1. Destroy voice
		/*
		INodeVoiceOAL* pNodeOAL = (SND::INodeVoiceOAL*)pNodeVoice;

		m_sourceLog.find_erase(pNodeOAL->m_alSource);
		if (pNodeOAL->m_alSource != 0)
		{
			alDeleteSources(1, &pNodeOAL->m_alSource);
			pNodeOAL->m_alSource = 0;
		}
		*/
	}

	bool linkNodes(const SND::ISndAdapter* /*pSndAdapter*/, const SND::INodeMixer* /*pParentNode*/, SND::INode* /*pNode*/)
	{
		return true;
	}
} //namespace OSL

// ------
// INodeDataOSL
// -
// ------
INodeDataOSL::INodeDataOSL(void)
//: m_alEffect(0)
{
}

INodeDataOSL:: ~INodeDataOSL(void)
{
	//PRINT_COND_ASSERT((m_alEffect == 0), "SND: m_alEffect was not cleaned up before shutdown");
}

// ------
// INodeMixerOSL
// -
// ------
INodeMixerOSL::INodeMixerOSL(const SND::ISndAdapter* pSndAdapter, const SND::INodeMixer* pNodeParent)
	: INodeMixer(pSndAdapter, pNodeParent)
{
}

INodeMixerOSL::~INodeMixerOSL(void)
{
}

void INodeMixerOSL::_create(const SND::NODE_DESC& nodeDesc)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1. Set variables
	m_nodeDesc = nodeDesc;
	m_nodeData = std::make_shared< INodeDataOSL >();

	// 2. Generic Construction Stuff
	if (m_pNodeParent == nullptr)
	{
		::OSL::createMasterNode(m_pSndAdapter, this);
	}
	else
	{
		::OSL::createSubmixNode(m_pSndAdapter, this);
	}

	if (m_nodeDesc.Auxiliary)
	{
		m_effectChain = std::make_shared< SND::IEffectChainOSL >(m_pSndAdapter, this);
		m_effectChain->create_effect_chain();
	}

	// 3. Set Volume and Frequency
	set_volume(m_nodeDesc.Volume);
}

void INodeMixerOSL::_release(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	FXD_RELEASE(m_effectChain, release_effect_chain());

	// 1. Shutdown submix nodes
	fxd_for(auto& mixer, m_mixers)
	{
		mixer.second->release();
	}
	m_mixers.clear();

	// 2. Shutdown voice nodes
	fxd_for(auto& voice, m_voices)
	{
		voice->release();
	}
	m_voices.clear();

	// 3. Shutdown node
	::OSL::shutdownMixerNode(this);
}

void INodeMixerOSL::_update_volume(FXD::F32 dt)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	m_nodeDesc.Volume.update(dt);
	fxd_for(auto& mixer, m_mixers)
	{
		mixer.second->set_volume(mixer.second->get_node_desc().Volume);
	}
	fxd_for(auto& voice, m_voices)
	{
		voice->set_volume(voice->get_node_desc().Volume);
	}
}

// ------
// INodeVoiceOSL
// -
// ------
INodeVoiceOSL::INodeVoiceOSL(const SND::ISndAdapter* pSndAdapter, const SND::INodeMixer* pNodeParent)
	: INodeVoice(pSndAdapter, pNodeParent)
	//, m_alSource(0)
{
}

INodeVoiceOSL::~INodeVoiceOSL(void)
{
	//PRINT_COND_ASSERT((m_alSource == 0), "SND: m_alSource was not cleaned up before shutdown");
}

void INodeVoiceOSL::_create(const SND::NODE_DESC& nodeDesc, const SND::NODE_VOICE_DESC& nodeVoiceDesc)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1. Sanity Check
	if ((nodeVoiceDesc.Flags.is_flag_set(E_CreateFlag::Is3D)) && (nodeVoiceDesc.Audio->source_format().Channels > 1))
	{
		PRINT_ASSERT << "SND: We don't support multi-channel 3d sounds";
	}

	// 2. Initialise the default variables
	m_nodeDesc = nodeDesc;
	m_nodeData = std::make_shared< INodeDataOSL >();
	if (nodeVoiceDesc.Flags.is_flag_set(E_CreateFlag::Streamed))
	{
		m_voice = std::make_shared< SND::IVoiceMusicOSL >(m_pSndAdapter, this);
	}
	else
	{
		m_voice = std::make_shared< SND::IVoiceSFXOSL >(m_pSndAdapter, this);
	}

	SND::VOICE_DESC voiceDesc = {};
	voiceDesc.Audio = nodeVoiceDesc.Audio;
	voiceDesc.LoopGroup = nodeVoiceDesc.LoopGroup;
	voiceDesc.CreateFlag = nodeVoiceDesc.Flags;
	m_voice->create(voiceDesc);

	// 3. Create and link nodes
	::OSL::createVoiceNode(m_pSndAdapter, this, nodeVoiceDesc.Audio->source_format(), nodeVoiceDesc.Flags.is_flag_set(E_CreateFlag::Is3D));
	::OSL::linkNodes(m_pSndAdapter, m_pNodeParent, this);

	// 4. Initialise the emitter
	if (nodeVoiceDesc.Flags.is_flag_set(E_CreateFlag::Is3D))
	{
		m_emitter = std::make_shared< SND::IEmitterOSL >(m_pSndAdapter, this);
		m_emitter->create_emitter();
	}

	// 5. Set Volume, Frequency and Priority
	set_volume(m_nodeDesc.Volume);
	set_priority(nodeVoiceDesc.Priority);
	set_frequency(nodeVoiceDesc.Ratio);

	m_pSndAdapter->m_voices.push_back(this);
}

void INodeVoiceOSL::_release(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	m_pSndAdapter->m_voices.find_erase(this);

	FXD_RELEASE(m_emitter, release_emitter());
	FXD_RELEASE(m_voice, release());

	::OSL::shutdownVoiceNode(this);
}

bool INodeVoiceOSL::_is_valid(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	//return (m_alSource != 0);
	return true;
}

void INodeVoiceOSL::_update_volume(FXD::F32 dt)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	m_nodeDesc.Volume.update(dt);

	FXD::F32 fVolFinal = m_nodeDesc.Volume;

	const SND::INodeMixer* pNode = m_pNodeParent;
	while (pNode != nullptr)
	{
		fVolFinal *= pNode->get_node_desc().Volume;
		pNode = pNode->get_node_parent();
	}

	//if (m_alSource != 0)
	//	alSourcef(m_alSource, AL_GAIN, fVolFinal);
}


void INodeVoiceOSL::_update_frequency(FXD::F32 dt)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	m_fRatio.update(dt);

	//if (m_alSource != 0)
	//	alSourcef(m_alSource, AL_PITCH, m_fRatio);
}
#endif //IsSndOpenSLES()