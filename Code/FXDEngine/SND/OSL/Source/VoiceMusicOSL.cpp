// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndOpenSLES()
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/OSL/ListenerOSL.h"
#include "FXDEngine/SND/OSL/SndAdapterOSL.h"
#include "FXDEngine/SND/OSL/SndNodeOSL.h"
#include "FXDEngine/SND/OSL/VoiceOSL.h"
#include "FXDEngine/SND/Decoders/Audio.h"

using namespace FXD;
using namespace SND;

// ------
// IVoiceMusicOSL
// - 
// ------
IVoiceMusicOSL::IVoiceMusicOSL(const SND::ISndAdapter* pSndAdapter, SND::INodeVoice* pNodeVoice)
	: IVoiceMusic(pSndAdapter, pNodeVoice)
{
	for (FXD::U16 n1 = 0; n1 < BufferCount; n1++)
	{
		/*
		m_buffers[n1].m_nID = n1;
		m_buffers[n1].m_alBuffer = 0;
		*/
	}
}

IVoiceMusicOSL::~IVoiceMusicOSL(void)
{
}

bool IVoiceMusicOSL::create(const SND::VOICE_DESC& voiceDesc)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");
	PRINT_COND_ASSERT((voiceDesc.Audio->is_streamable()), "SND: Stream is not streamable");

	// 2. Store Local Information and construct memory buffers 
	m_voiceDesc = voiceDesc;
	m_voiceDesc.Audio->seek_begin();
	_ensure_loop_points_defined(m_voiceDesc.CreateFlag, m_voiceDesc.Audio);

	m_nCurrentBuffer = 0;
	for (FXD::U16 n1 = 0; n1 < BufferCount; n1++)
	{
		m_memBuffers[n1] = Memory::MemHandle::allocate(kAudioMusicBufferSize);
		/*
		alGenBuffers(1, &m_buffers[n1].m_alBuffer);
		*/
	}
	return true;
}

void IVoiceMusicOSL::release(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	stop();
	for (FXD::U16 n1 = 0; n1 < BufferCount; n1++)
	{
		/*
		alDeleteBuffers(1, &m_buffers[n1].m_alBuffer);
		m_buffers[n1].m_alBuffer = 0;
		*/
		m_memBuffers[n1].clear_memory();
	}
	m_voiceDesc = SND::VOICE_DESC();
}

void IVoiceMusicOSL::play(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!m_pVoiceNode->is_valid())
	{
		return;
	}

	/*
	SND::INodeVoiceOSL* pVoiceNodeOSL = (SND::INodeVoiceOSL*)(m_pVoiceNode);
	alSourcePlay(pVoiceNodeOSL->m_alSource);
	*/
	OSLResultCheckFail("alSourcePlay");

	m_bStreamComplete = false;
}

void IVoiceMusicOSL::pause(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!m_pVoiceNode->is_valid())
	{
		return;
	}

	/*
	SND::INodeVoiceOSL* pVoiceNodeOSL = (SND::INodeVoiceOSL*)(m_pVoiceNode);
	alSourcePause(pVoiceNodeOSL->m_alSource);
	*/
	OSLResultCheckFail("alSourcePause");
}

void IVoiceMusicOSL::stop(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!m_pVoiceNode->is_valid())
	{
		return;
	}

	/*
	SND::INodeVoiceOSL* pVoiceNodeOSL = (SND::INodeVoiceOSL*)(m_pVoiceNode);
	alSourceStop(pVoiceNodeOSL->m_alSource);
	*/
	OSLResultCheckFail("alSourceStop");

	_buffers_flush();
	m_loopHandle = LoopPointHandle();
}

FXD::U32 IVoiceMusicOSL::tell_sample_position(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!m_pVoiceNode->is_valid())
	{
		return 0;
	}

	auto itr = m_queue.begin();
	if (itr == m_queue.end())
	{
		return 0;
	}

	/*
	SND::INodeVoiceOSL* pVoiceNodeOSL = (SND::INodeVoiceOSL*)(m_pVoiceNode);
	ALint alSampleOffset = 0;
	alGetSourcei(pVoiceNodeOSL->m_alSource, AL_SAMPLE_OFFSET, &alSampleOffset);
	OSLResultCheckFail("alGetSourcei");

	FXD::U32 nSampleStart = ((*itr)->m_nSampleStart + (*itr)->m_nSampleOffset) / m_voiceDesc.Audio->source_format().getBytesPerSample();
	return (nSampleStart + alSampleOffset);
	*/
	return 0;
}

void IVoiceMusicOSL::_update_buffers(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1. Early out if we are not playing
	if ((!m_pVoiceNode->is_valid()) || (!m_pVoiceNode->is_playing()))
	{
		return;
	}

	// 2. Check for buffers that have finished
	_buffers_finished();

	// 3. If we have finished streaming data then check when we can stop
	if (m_bStreamComplete)
	{
		if (_buffer_queued_count() == 0)
		{
			m_pVoiceNode->stop();
		}
		return;
	}

	// 3. If we have a fee buffer then fill a new one
	FXD::U32 nQueuedBuffers = _buffer_queued_count();
	if (nQueuedBuffers < BufferCount)
	{
		_buffer_fill();
	}

	// 4. OpenAL fix
	/*
	SND::INodeVoiceOSL* pVoiceNodeOSL = (SND::INodeVoiceOSL*)(m_pVoiceNode);
	ALint alState = 0;
	alGetSourcei(pVoiceNodeOSL->m_alSource, AL_SOURCE_STATE, &alState);

	FXD::U32 nAttempts = 0;
	while ((alState != AL_PLAYING) && (nAttempts < 5))
	{
		alSourcePlay(pVoiceNodeOSL->m_alSource);
		alGetSourcei(pVoiceNodeOSL->m_alSource, AL_SOURCE_STATE, &alState);
		nAttempts++;
	}
	*/
}

void IVoiceMusicOSL::_buffer_fill(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1. Return if we are finished
	if (m_bStreamComplete)
	{
		return;
	}

	// 2. Make sure we have a buffer to start
	if (!m_loopHandle.m_pLoopPoint)
	{
		_update_loop_point();
	}

	// 3. Seek to the correct point in the stream
	const FXD::U32 nTell = m_loopHandle.m_nLoopPointBytesRead + (m_voiceDesc.Audio->source_format().BytesPerSec * m_loopHandle.m_pLoopPoint->StartPosition);
	const FXD::U32 nSeek = m_voiceDesc.Audio->seek_to(nTell, Core::E_SeekOffset::Begin);

	// 4. Construct buffer data to submit
	FXD::U32 nTotalBytesRead = 0;
	const FXD::U32 nMaxBufferBytes = m_nBytesSize;
	Memory::MemHandle::LockGuard mhLock(m_memBuffers[m_nCurrentBuffer]);

	while (true)
	{
		// 4.1. Have we have finshed filling the buffer
		PRINT_COND_ASSERT((nTotalBytesRead <= kAudioMusicBufferSize), "SND: The buffer has too much data");
		if (nTotalBytesRead == kAudioMusicBufferSize)
		{
			break;
		}

		// 4.2. Calculate amount to read
		FXD::U32 nRequestBytes = (kAudioMusicBufferSize - nTotalBytesRead);
		const FXD::U32 nBufferSum = (m_loopHandle.m_nLoopPointBytesRead + nRequestBytes);
		if (nBufferSum > nMaxBufferBytes)
		{
			nRequestBytes -= (nBufferSum - nMaxBufferBytes);
		}

		// 4.3. Fill the buffers
		void* pMHData = (void*)((FXD::U8*)mhLock.get_mem() + nTotalBytesRead);
		const FXD::U32 nBytesRead = m_voiceDesc.Audio->read_size(pMHData, nRequestBytes);
		PRINT_COND_ASSERT((nBytesRead == nRequestBytes), "SND: Amount read is not the same as requested");
		nTotalBytesRead += nBytesRead;

		// 4.4. We have finished reading this loop point 
		const FXD::U32 nCurrBytesTotal = m_loopHandle.m_nLoopPointBytesRead + nTotalBytesRead;
		if (nCurrBytesTotal >= nMaxBufferBytes)
		{
			break;
		}
	}

	// 5. Submit the buffer data
	_buffer_submit(nTotalBytesRead);

	// 6. Move to next loop point
	if (m_loopHandle.m_nLoopPointBytesRead >= nMaxBufferBytes)
	{
		_update_loop_point();
	}
}

void IVoiceMusicOSL::_buffer_submit(FXD::U32 nSize)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1. Get a pointer to the memory 
	Memory::MemHandle::LockGuard memHandleLock(m_memBuffers[m_nCurrentBuffer]);
	const FXD::U8* pMemPointer = (const FXD::U8*)memHandleLock.get_mem();

	// 2. Construct a buffer to submit
	/*
	m_buffers[m_nCurrentBuffer].m_nSampleStart = m_nBytesStartPos;
	m_buffers[m_nCurrentBuffer].m_nSampleOffset = m_loopHandle.m_nLoopPointBytesRead;
	m_buffers[m_nCurrentBuffer].m_eState = FXDBUFFEROAL::State::Playing;
	m_queue.push_back(&m_buffers[m_nCurrentBuffer]);
	m_loopHandle.m_nLoopPointBytesRead += nSize;

	// 3. Create and submit current api buffer
	ALvoid* pMemBegin = (ALvoid*)(pMemPointer);
	alBufferData(m_buffers[m_nCurrentBuffer].m_alBuffer, SND::OAL::GetALFormat(m_voiceDesc.Audio->source_format()), pMemBegin, nSize, m_voiceDesc.Audio->source_format().SampleRate);
	OALResultCheckFail("alBufferData");

	SND::INodeVoiceOAL* pVoiceNodeOAL = (SND::INodeVoiceOAL*)(m_pVoiceNode);
	alSourceQueueBuffers(pVoiceNodeOAL->m_alSource, 1, &m_buffers[m_nCurrentBuffer].m_alBuffer);
	OALResultCheckFail("alSourceQueueBuffers");
	*/

	// 4. Increment to the next buffer
	m_nCurrentBuffer++;
	m_nCurrentBuffer %= BufferCount;
}

void IVoiceMusicOSL::_buffers_flush(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (m_pVoiceNode->is_valid())
	{
		/*
		SND::INodeVoiceOSL* pVoiceNodeOSL = (SND::INodeVoiceOSL*)(m_pVoiceNode);
		for (FXD::U16 n1 = 0; n1 < BufferCount; n1++)
		{
			alSourceUnqueueBuffers(pVoiceNodeOSL->m_alSource, 1, &m_buffers[m_nCurrentBuffer].m_alBuffer);
			OSLResultCheckFail("alSourceUnqueueBuffers");
		}
		*/
	}

	for (FXD::U16 n1 = 0; n1 < BufferCount; n1++)
	{
		m_memBuffers[n1].clear_memory();
	}
	m_nBytesStartPos = 0;
	m_nBytesSize = 0;
	m_bStreamComplete = true;
}

FXD::U32 IVoiceMusicOSL::_buffer_queued_count(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!m_pVoiceNode->is_valid())
	{
		return 0;
	}

	/*
	SND::INodeVoiceOSL* pVoiceNodeOSL = (SND::INodeVoiceOSL*)(m_pVoiceNode);
	ALint alBuffersQueued = 0;
	alGetSourcei(pVoiceNodeOSL->m_alSource, AL_BUFFERS_QUEUED, &alBuffersQueued);
	return (FXD::U32)alBuffersQueued;
	*/
	return 0;
}

void IVoiceMusicOSL::_buffers_finished(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!m_pVoiceNode->is_valid())
	{
		return;
	}

	/*
	SND::INodeVoiceOSL* pVoiceNodeOSL = (SND::INodeVoiceOSL*)(m_pVoiceNode);

	ALint nNumprocessed = 0;
	alGetSourcei(pVoiceNodeOSL->m_alSource, AL_BUFFERS_PROCESSED, &nNumprocessed);
	while (nNumprocessed != 0)
	{
		ALuint alBuffer = 0;
		alSourceUnqueueBuffers(pVoiceNodeOSL->m_alSource, 1, &alBuffer);
		OSLResultCheckFail("alSourceUnqueueBuffers");

		ALint nSize = 0;
		alGetBufferi(alBuffer, AL_SIZE, &nSize);
		nNumprocessed--;

		for (FXD::U16 n1 = 0; n1 < BufferCount; n1++)
		{
			if (m_buffers[n1].m_alBuffer == alBuffer)
			{
				m_buffers[n1].m_eState = FXDBUFFEROSL::State::Finished;
				m_queue.find_erase(&m_buffers[n1]);
				break;
			}
		}
	}

	for (FXD::U16 n1 = 0; n1 < BufferCount; n1++)
	{
		if (m_buffers[n1].m_eState == FXDBUFFEROSL::State::Finished)
		{
			m_buffers[n1].m_eState = FXDBUFFEROSL::State::Free;
			m_queue.find_erase(&m_buffers[n1]);
		}
	}
	*/
}
#endif //IsSndOpenSLES()