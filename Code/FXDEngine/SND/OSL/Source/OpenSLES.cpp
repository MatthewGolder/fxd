// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndOpenSLES()
#include "FXDEngine/Core/Debugger.h"
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/OSL/OpenSLES.h"

using namespace FXD;
using namespace SND;

// ------
// CheckOSLResult
// -
// ------
inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, CheckOSLResult const& rhs)
{
	/*
	switch (rhs.alError())
	{
#define HANDLE_STR(h) case h: ostr << #h; break;
#undef HANDLE_STR
	default:
		FXD_ERROR("Unrecognised - please find the correct string and add to this case");
		break;
	}
	*/
	return ostr;
}

CheckOSLResult::CheckOSLResult(const FXD::UTF8* pText, const FXD::UTF8* pFile, FXD::U32 nLine)
	: m_nLine(nLine)
	, m_pText(pText)
	, m_pFile(pFile)
{
}

bool CheckOSLResult::operator()()
{
	PRINT_WARN << "[" << m_pFile << " , " << m_nLine << "] - failed with error: " << (*this);
	return true;
}
#endif //IsSndOpenSLES()