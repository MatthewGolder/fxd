// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_OSL_EMITTEROSL_H
#define FXDENGINE_SND_OSL_EMITTEROSL_H

#include "FXDEngine/SND/Types.h"

#if IsSndOpenSLES()
#include "FXDEngine/SND/Emitter.h"
#include "FXDEngine/SND/OSL/OpenSLES.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// IEmitterOSL
		// -
		// ------
		class IEmitterOSL FINAL : public IEmitter
		{
		public:
			EXPLICIT IEmitterOSL(const SND::ISndAdapter* pSndAdapter, const SND::INodeVoice* pNodeVoice);
			~IEmitterOSL(void);

		protected:
			bool _create_emitter(void) FINAL;
			bool _release_emitter(void) FINAL;

			void _update(const SND::Listener& listener) FINAL;
			void _update_distance_model(void) FINAL;
		};

	} //namespace SND
} //namespace FXD
#endif //IsSndOpenSLES()
#endif //FXDENGINE_SND_OSL_EMITTEROSL_H