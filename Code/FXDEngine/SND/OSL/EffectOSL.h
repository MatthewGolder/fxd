// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_OSL_EFFECTOSL_H
#define FXDENGINE_SND_OSL_EFFECTOSL_H

#include "FXDEngine/SND/Types.h"

#if IsSndOpenSLES()
#include "FXDEngine/SND/Effect.h"
#include "FXDEngine/SND/OSL/OpenSLES.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// IEffectOSL
		// -
		// ------
		class IEffectOSL FINAL : public IEffect
		{
		public:
			EXPLICIT IEffectOSL(const EFFECT_DESC& effectDesc);
			~IEffectOSL(void);

		public:
			//ALuint m_alEffect;
		};

		// ------
		// IEffectChainOSL
		// -
		// ------
		class IEffectChainOSL FINAL : public IEffectChain
		{
		public:
			EXPLICIT IEffectChainOSL(const SND::ISndAdapter* pSndAdapter, SND::INodeMixer* pNodeMixer);
			~IEffectChainOSL(void);

		protected:
			bool _create_effect_chain(void) FINAL;
			bool _release_effect_chain(void) FINAL;

			void _add_effects(const Container::Vector< SND::EFFECT_DESC >& effectDescs) FINAL;
			void _enable_effect(FXD::U16 nSlot, bool bEnable) FINAL;
			bool _is_effect_enabled(FXD::U16 nSlot) const FINAL;

			void _update(FXD::F32 dt) FINAL;
			void _recreate(void) FINAL;
		};

	} //namespace SND
} //namespace FXD
#endif //IsSndOpenSLES()
#endif //FXDENGINE_SND_OSL_EFFECTOSL_H