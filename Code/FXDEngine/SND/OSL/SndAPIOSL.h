// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_OSL_SNDAPIOSL_H
#define FXDENGINE_SND_OSL_SNDAPIOSL_H

#include "FXDEngine/SND/Types.h"

#if IsSndOpenSLES()
#include "FXDEngine/SND/SndApi.h"
#include "FXDEngine/SND/OSL/OpenSLES.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// ISndApiOSL
		// -
		// ------
		class ISndApiOSL FINAL : public ISndApi
		{
		public:
			ISndApiOSL(void);
			~ISndApiOSL(void);

			bool create_api(void) FINAL;
			bool release_api(void) FINAL;

		private:
			void _create_adapter_list(void) FINAL;
		};

	} //namespace SND
} //namespace FXD
#endif //IsSndOpenSLES()
#endif //FXDENGINE_SND_OSL_SNDAPIOSL_H