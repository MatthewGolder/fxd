// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_OSL_LISTENEROSL_H
#define FXDENGINE_SND_OSL_LISTENEROSL_H

#include "FXDEngine/SND/Types.h"

#if IsSndOpenSLES()
#include "FXDEngine/SND/Listener.h"
#include "FXDEngine/SND/OSL/OpenSLES.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// IListenerOSL
		// -
		// ------
		class IListenerOSL FINAL : public IListener
		{
		public:
			EXPLICIT IListenerOSL(const SND::ISndAdapter* pSndAdapter);
			~IListenerOSL(void);

		private:
			bool _create(void) FINAL;
			bool _release(void) FINAL;

			void _update(FXD::F32 dt) FINAL;

			void _set_distance_model(SND::E_SndDistanceModel eDistanceModel) FINAL;
			void _set_doppler_factor(FXD::F32 fFactor) FINAL;
			void _set_rolloff_factor(FXD::F32 fFactor) FINAL;
			void _set_rolloff_factorInternal(FXD::F32 fFactor);

		private:
			FXD::F32 m_fUserRolloffFactor;
		};

	} //namespace SND
} //namespace FXD
#endif //IsSndOpenSLES()
#endif //FXDENGINE_SND_OSL_LISTENEROSL_H