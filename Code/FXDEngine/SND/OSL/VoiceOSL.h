// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_OSL_VOICEOSL_H
#define FXDENGINE_SND_OSL_VOICEOSL_H

#include "FXDEngine/SND/Types.h"

#if IsSndOpenSLES()
#include "FXDEngine/SND/Voice.h"
#include "FXDEngine/SND/OSL/OpenSLES.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// FXDBUFFEROSL
		// -
		// ------
		class FXDBUFFEROSL
		{
		public:
			enum class State
			{
				Free,
				Playing,
				Finished,
				Count,
				Unknown = 0xffff
			};

		public:
			FXDBUFFEROSL(void)
				: m_nSampleStart(0)
				, m_nSampleOffset(0)
				, m_nSampleSize(0)
				, m_nID(0)
				, m_eState(State::Free)
				//, m_alBuffer(0)
			{
			}
			~FXDBUFFEROSL(void)
			{
				//PRINT_COND_ASSERT((m_alBuffer == 0), "SND: m_alBuffer was not cleaned up before shutdown");
			}

		public:
			FXD::U32 m_nSampleStart;
			FXD::U32 m_nSampleOffset;
			FXD::U32 m_nSampleSize;
			FXD::U32 m_nID;
			State m_eState;
			//ALuint m_alBuffer;
		};

		// ------
		// IVoiceSFXOSL
		// -
		// ------
		class IVoiceSFXOSL FINAL : public SND::IVoiceSFX
		{
		public:
			EXPLICIT IVoiceSFXOSL(const SND::ISndAdapter* pSndAdapter, SND::INodeVoice* pNodeVoice);
			~IVoiceSFXOSL(void);

		protected:
			bool create(const SND::VOICE_DESC& voiceDesc) FINAL;
			void release(void) FINAL;

			void play(void) FINAL;
			void pause(void) FINAL;
			void stop(void) FINAL;

			FXD::U32 tell_sample_position(void) const FINAL;

			void _update_buffers(void) FINAL;

			void _buffer_fill(void) FINAL;
			void _buffer_submit(FXD::U32 nSize) FINAL;
			void _buffers_flush(void) FINAL;
			FXD::U32 _buffer_queued_count(void) const FINAL;
			void _buffers_finished(void) FINAL;

		protected:
			FXDBUFFEROSL m_buffers[BufferCount];
			Container::Vector< FXDBUFFEROSL* > m_queue;
		};

		// ------
		// IVoiceMusicOSL
		// -
		// ------
		class IVoiceMusicOSL FINAL : public SND::IVoiceMusic
		{
		public:
			EXPLICIT IVoiceMusicOSL(const SND::ISndAdapter* pSndAdapter, SND::INodeVoice* pNodeVoice);
			~IVoiceMusicOSL(void);

		protected:
			bool create(const SND::VOICE_DESC& voiceDesc) FINAL;
			void release(void) FINAL;

			void play(void) FINAL;
			void pause(void) FINAL;
			void stop(void) FINAL;

			FXD::U32 tell_sample_position(void) const FINAL;

			void _update_buffers(void) FINAL;

			void _buffer_fill(void) FINAL;
			void _buffer_submit(FXD::U32 nSize) FINAL;
			void _buffers_flush(void) FINAL;
			FXD::U32 _buffer_queued_count(void) const FINAL;
			void _buffers_finished(void) FINAL;

		protected:
			FXDBUFFEROSL m_buffers[BufferCount];
			Container::Vector< FXDBUFFEROSL* > m_queue;
		};

	} //namespace SND
} //namespace FXD
#endif //IsSndOpenSLES()
#endif //FXDENGINE_SND_OSL_VOICEOSL_H