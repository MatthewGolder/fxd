// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_OSL_OPENSLES_H
#define FXDENGINE_SND_OSL_OPENSLES_H

#include "FXDEngine/SND/Types.h"

#if IsSndOpenSLES()
#if IsOSAndroid()
	#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>
#endif //IsOSAndroid()

namespace FXD
{
	namespace SND
	{
		// ------
		// CheckOSLResult
		// -
		// ------
		class CheckOSLResult
		{
		public:
			CheckOSLResult(const FXD::UTF8* pText, const FXD::UTF8* pFile, FXD::U32 nLine);

			bool operator()();

		private:
			FXD::U32 m_nLine;
			const FXD::UTF8* m_pText;
			const FXD::UTF8* m_pFile;
		};
		#define OSLResultCheckFail(c) FXD::SND::CheckOSLResult(#c, __FILE__, __LINE__)()
		#define OSLResultCheckSuccess() !OSLResultCheckFail(c)

		class ISndAdapterOSL;
		class ISndAdapterDescOSL;
		typedef std::shared_ptr< ISndAdapterOSL > SndAdapterOSL;
		typedef std::shared_ptr< ISndAdapterDescOSL > SndAdapterDescOSL;

		class IVoiceSFXOSL;
		class IVoiceMusicOSL;
		typedef std::shared_ptr< IVoiceSFXOSL > VoiceSFXOSL;
		typedef std::shared_ptr< IVoiceMusicOSL > VoiceMusicOSL;

		class INodeDataOSL;
		class INodeMixerOSL;
		class INodeVoiceOSL;
		typedef std::shared_ptr< INodeDataOSL > NodeDataOSL;
		typedef std::shared_ptr< INodeMixerOSL > NodeMixerOSL;
		typedef std::shared_ptr< INodeVoiceOSL > NodeVoiceOSL;

		class INodeVoiceOSL;
		typedef std::shared_ptr< INodeVoiceOSL > NodeVoiceOSL;

		class IListenerOSL;
		typedef std::shared_ptr< IListenerOSL > ListenerOSL;

		class IEmitterOSL;
		typedef std::shared_ptr< IEmitterOSL > EmitterOSL;

		class IEffectChainOSL;
		typedef std::shared_ptr< IEffectChainOSL > EffectChainOSL;

	} //namespace SND
} //namespace FXD
#endif //IsSndOpenSLES()
#endif //FXDENGINE_SND_OSL_OPENSLES_H