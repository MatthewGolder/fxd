// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_OSL_SNDNODEOSL_H
#define FXDENGINE_SND_OSL_SNDNODEOSL_H

#include "FXDEngine/SND/Types.h"

#if IsSndOpenSLES()
#include "FXDEngine/SND/SndNode.h"
#include "FXDEngine/SND/OSL/OpenSLES.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// INodeDataOSL
		// -
		// ------
		class INodeDataOSL FINAL : public SND::INodeData
		{
		public:
			INodeDataOSL(void);
			virtual ~INodeDataOSL(void);

		public:
			//ALuint m_alEffect;
		};

		// ------
		// INodeMasterOSL
		// -
		// ------
		class INodeMixerOSL FINAL : public SND::INodeMixer
		{
		public:
			EXPLICIT INodeMixerOSL(const SND::ISndAdapter* pSndAdapter, const SND::INodeMixer* pNodeParent);
			virtual ~INodeMixerOSL(void);

		protected:
			void _create(const SND::NODE_DESC& nodeDesc) FINAL;
			void _release(void) FINAL;

			void _update_volume(FXD::F32 dt) FINAL;
		};

		// ------
		// INodeVoiceOSL
		// -
		// ------
		class INodeVoiceOSL FINAL : public SND::INodeVoice
		{
		public:
			EXPLICIT INodeVoiceOSL(const SND::ISndAdapter* pSndAdapter, const SND::INodeMixer* pNodeParent);
			virtual ~INodeVoiceOSL(void);

		protected:
			void _create(const SND::NODE_DESC& nodeDesc, const SND::NODE_VOICE_DESC& nodeVoiceDesc) FINAL;
			void _release(void) FINAL;

			bool _is_valid(void) const FINAL;

			void _update_volume(FXD::F32 dt) FINAL;
			void _update_frequency(FXD::F32 dt) FINAL;

		public:
			//ALuint m_alSource;
		};

	} //namespace SND
} //namespace FXD
#endif //IsSndOpenSLES()
#endif //FXDENGINE_SND_OSL_SNDNODEOSL_H