// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_TYPES_H
#define FXDENGINE_SND_TYPES_H

#include "FXDEngine/Container/Flag.h"
#include "FXDEngine/Core/Types.h"
#include "FXDEngine/Math/Limits.h"

#include <memory>

#if IsOSPC()
	#define IsSndXAudio2() 1
	#define IsSndOpenAL() 0
	#define IsSndOpenSLES() 0
#elif IsOSAndroid()
	#define IsSndXAudio2() 0
	#define IsSndOpenAL() 0
	#define IsSndOpenSLES() 1
#endif

CONSTEXPR FXD::U32 kAudioSFXBufferCount = 2;
CONSTEXPR FXD::U32 kAudioMusicBufferCount = 4;
CONSTEXPR FXD::U32 kAudioMusicBufferSize = 16384;

namespace FXD
{
	namespace SND
	{
		// ------
		// E_SndApi
		// -
		// ------
		enum class E_SndApi
		{
			Default,
			OpenAL,
			OpenSL,
			XAudio2,
			Count,
			Unknown = 0xffff
		};

		// ------
		// E_SndDistanceModel
		// -
		// ------
		enum class E_SndDistanceModel
		{
			Linear,
			Logarithmic,
			Count,
			Unknown = 0xffff
		};

		// ------
		// SndFormat
		// -
		// ------
		struct SndFormat
		{
		public:
			FXD::U16 getBytesPerSample(void) const { return (BitsPerSample / 8); }
			FXD::U16 getBlockAlign(void) const { return (Channels * getBytesPerSample()); }

		public:
			FXD::U16 Format = 0;
			FXD::U16 Channels = 0;
			FXD::U32 SampleRate = 0;
			FXD::U32 BytesPerSec = 0;
			FXD::U16 BitsPerSample = 0;
		};

		#define DEFAULT_HIGH_FREQUENCY 5000.0f

		// ------
		// E_VoiceState
		// -
		// ------
		enum class E_VoiceState
		{
			Stopped,
			Paused,
			Playing,
			Count,
			Unknown = 0xffff
		};

		// ------
		// E_CreateFlag
		// -
		// ------
		enum class E_CreateFlag
		{
			Streamed,
			Looped,
			Is3D,
			Count,
			Unknown = 0xffff
		};
		typedef Container::EnumFlag< SND::E_CreateFlag > E_CreateFlags;
		ENUM_CLASS_FLAGS(SND::E_CreateFlag);

		class ISndApi;
		typedef std::unique_ptr< ISndApi > SndApi;

		class ISndSystem;
		typedef std::unique_ptr< ISndSystem > SndSystem;

		class ISndAdapterDesc;
		typedef std::shared_ptr< ISndAdapterDesc > SndAdapterDesc;

		class ISndAdapter;
		typedef std::shared_ptr< ISndAdapter > SndAdapter;

		class IVoice;
		class IVoiceSFX;
		class IVoiceMusic;
		typedef std::shared_ptr< IVoice > Voice;

		class INodeData;
		typedef std::shared_ptr< INodeData > NodeData;

		class INode;
		class INodeMixer;
		class INodeVoice;
		typedef std::shared_ptr< INode > SndNode;
		typedef std::shared_ptr< INodeMixer > NodeMixer;
		typedef std::shared_ptr< INodeVoice > NodeVoice;

		class IAudio;
		class IAudioOgg;
		class IAudioWav;
		typedef std::shared_ptr< IAudio > Audio;

		//class SndLoopPoint;
		class ISndLoopGroup;
		typedef std::shared_ptr< ISndLoopGroup > SndLoopGroup;

		class IPlaylist;
		typedef std::shared_ptr< IPlaylist > Playlist;

		class IListener;
		typedef std::shared_ptr< IListener > Listener;

		class IEmitter;
		typedef std::shared_ptr< IEmitter > Emitter;

		class IEffect;
		class IEffectChain;
		typedef std::shared_ptr< IEffect > Effect;
		typedef std::shared_ptr< IEffectChain > EffectChain;

		class CreateVoice;

	} //namespace SND
} //namespace FXD

class EffectChainTagCB;
class SndLoopPointTagCB;
#endif //FXDENGINE_SND_TYPES_H