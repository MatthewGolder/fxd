// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_PLAYLIST_H
#define FXDENGINE_SND_PLAYLIST_H

#include "FXDEngine/Core/String.h"
#include "FXDEngine/IO/Path.h"
#include "FXDEngine/SND/Types.h"
#include "FXDEngine/Thread/CriticalSection.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// PlaylistTrack
		// -
		// ------
		class PlaylistTrack
		{
		public:
			PlaylistTrack(const Core::String8& strTrackName = UTF_8(""), bool bStreamed = true);
			~PlaylistTrack(void);

			void add_genres(const Container::Vector< Core::String8 >& genres);
			void add_genre(const Core::String8& strGenre);
			void remove_genre(const Core::String8& strGenre);
			void clear_genres(void);
			bool contains_genre(const Core::String8& strGenre) const;

			SET(bool, bStreamed, streamed);
			SET(Core::String8, strTrackName, trackName);

			GET_R(bool, bStreamed, streamed);
			GET_R(Core::String8, strTrackName, trackName);

		private:
			bool m_bStreamed;
			Core::String8 m_strTrackName;
			Container::Vector< Core::String8 > m_genres;
		};

		// ------
		// IPlaylist
		// -
		// ------
		class IPlaylist FINAL : public Core::NonCopyable
		{
		public:
			friend class ISndAdapter;

			// ------
			// _Data
			// -
			// ------
			class _Data
			{
			public:
				_Data(const IO::Path& ioPath, const SND::NodeMixer& sndNode);
				~_Data(void);

			public:
				SND::NodeVoice m_currentVoice;
				SND::NodeMixer m_sndNode;
				IO::Path m_ioPath;
				SND::E_VoiceState m_eState;
				FXD::U16 m_nIndex;
				Container::Vector< SND::PlaylistTrack > m_trackList;
				Container::Vector< Core::String8 > m_disabledGenres;
			};

		public:
			static SND::Playlist FromStream(SND::ISndAdapter* pSndAdapter, Core::StreamIn& stream, const IO::Path& ioPath, const SND::NodeMixer& sndNode);

		public:
			IPlaylist(SND::ISndAdapter* pSndAdapter, const IO::Path& ioPath, const SND::NodeMixer& sndNode);
			virtual ~IPlaylist(void);

			void play(void);
			void pause(void);
			void stop(void);

			bool is_playing(void) const;

			void add_track(const SND::PlaylistTrack& track);
			void add_tracks(const Container::Vector< SND::PlaylistTrack >& tracks);

			void skip_track(void);
			void prev_track(void);
			void clear_tracks(void);
			void shuffle_tracks(void);

			void enable_genre(const Core::String8& strGenre);
			void disable_genre(const Core::String8& strGenre);

		private:
			void _process(FXD::F32 dt);

			FXD::U16 _find_next_track_index(void) const;
			FXD::U16 _find_previous_track_index(void) const;
			bool _is_current_track_disabled(void) const;
			bool _is_track_disabled(const PlaylistTrack& track) const;

		private:
			SND::ISndAdapter* m_pSndAdapter;
			mutable Thread::CSLockData< _Data > m_playlistData;
		};

	} //namespace SND
} //namespace FXD
#endif //FXDENGINE_SND_PLAYLIST_H