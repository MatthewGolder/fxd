// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_SNDAPI_H
#define FXDENGINE_SND_SNDAPI_H

#include "FXDEngine/App/Api.h"
#include "FXDEngine/Core/StringView.h"
#include "FXDEngine/SND/Types.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// ISndApi
		// -
		// ------
		class ISndApi : public App::IApi
		{
		public:
			ISndApi(SND::E_SndApi eSndApi);
			virtual ~ISndApi(void);

			virtual bool create_api(void) PURE;
			virtual bool release_api(void) PURE;

			GET_R(SND::E_SndApi, eSndApi, sndApiType);

		protected:
			virtual void _create_adapter_list(void) PURE;

		protected:
			SND::E_SndApi m_eSndApi;
		};

		namespace Funcs
		{
			extern const Core::StringView8 ToString(const SND::E_SndApi eType);
			extern SND::E_SndApi ToApiType(const Core::StringView8 strType);
		} //namespace Funcs

	} //namespace SND
} //namespace FXD
#endif //FXDENGINE_SND_SNDAPI_H