// Creator - MatthewGolder
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/SndSystem.h"

using namespace FXD;
using namespace SND;

// 0 - Not Initialised
// 1 - Initialised
// 2 - ShutDown
FXD::S32 g_nAudioStatus = 0;

// ------
// ISndJPThread
// -
// ------
ISndJPThread::ISndJPThread(void)
	: IAsyncTaskPoolThread(display_name())
	, m_sndSystem(nullptr)
{
	if (g_nAudioStatus == 2)
	{
		PRINT_ASSERT << "SND: ISndJPThread accessed after shutdown";
	}
}

ISndJPThread::~ISndJPThread(void)
{
	stop_and_wait_for_exit();
}

bool ISndJPThread::init_pool(void)
{
	SND::Funcs::RestrictSndThread();
	PRINT_INFO << "SND: ISndJPThread::init_pool()";

	m_sndSystem = std::make_unique< SND::ISndSystem >();
	m_timer.start_counter();
	g_nAudioStatus = 1;
	m_shared.get_quick_write_lock()->m_eState = Job::E_AsyncState::Running;
	return true;
}

bool ISndJPThread::shutdown_pool(void)
{
	m_sndSystem = nullptr;
	SND::Funcs::UnRestrictSndThread();
	PRINT_INFO << "SND: ISndJPThread::shutdown_pool()";

	g_nAudioStatus = 2;
	m_shared.get_quick_write_lock()->m_eState = Job::E_AsyncState::ShutDown;
	return true;
}

void ISndJPThread::idle_process(void)
{
	if (is_shuttingdown())
	{
		return;
	}

	const FXD::S32 nProcessFrequency = 10;

	// 1. Get initial time stamp.
	FXD::F32 ts1 = (FXD::F32)Core::Funcs::GetTimeSecsF();

	FXD::F32 dt = m_timer.elapsed_time();
	m_fps.update(dt);
	if (m_sndSystem)
	{
		m_sndSystem->_process(dt);
	}

	// 2. Get final time stamp and wait for next update interval.
	FXD::F32 ts2 = (FXD::F32)Core::Funcs::GetTimeSecsF();
	FXD::S32 nToWait = nProcessFrequency - (FXD::S32)((ts2 - ts1) * 1000.0f);
	if (nToWait > 0)
	{
		Thread::Funcs::Sleep(nToWait);
	}
}

bool ISndJPThread::is_thread_restricted(void) const
{
	return SND::Funcs::IsSndThreadRestricted();
}

// ------
// Funcs
// -
// ------
static FXD::S32 g_ContextThread = 0;
bool FXD::SND::Funcs::IsSndThreadRestricted(void)
{
	return (g_ContextThread != 0) && (g_ContextThread != Thread::Funcs::GetCurrentThreadID());
}

void FXD::SND::Funcs::RestrictSndThread(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread" << Thread::Funcs::GetCurrentThreadID());
	g_ContextThread = Thread::Funcs::GetCurrentThreadID();
}

void FXD::SND::Funcs::UnRestrictSndThread(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread" << Thread::Funcs::GetCurrentThreadID());
	g_ContextThread = 0;
}