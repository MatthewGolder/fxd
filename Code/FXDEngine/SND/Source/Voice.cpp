// Creator - MatthewGolder
#include "FXDEngine/SND/Voice.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Job/Async.h"
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/SndNode.h"
#include "FXDEngine/SND/Decoders/Audio.h"

using namespace FXD;
using namespace SND;

// ------
// IVoice
// -
// ------
IVoice::IVoice(const SND::ISndAdapter* pSndAdapter, SND::INodeVoice* pNodeVoice)
	: m_pSndAdapter(pSndAdapter)
	, m_pVoiceNode(pNodeVoice)
	, m_nBytesStartPos(0)
	, m_nBytesSize(0)
	, m_bStreamComplete(false)
{
}

IVoice::~IVoice(void)
{
}

bool IVoice::is_finished(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!m_pVoiceNode->is_valid())
	{
		return true;
	}
	if (!m_pVoiceNode->is_stopped())
	{
		return false;
	}
	if (!m_bStreamComplete)
	{
		return false;
	}
	return (_buffer_queued_count() == 0);
}

FXD::F32 IVoice::tell_playback_position(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	FXD::U32 nSamplesPlayed = tell_sample_position();
	FXD::F32 fRetVal = (FXD::F32)nSamplesPlayed / (FXD::F32)m_voiceDesc.Audio->source_format().SampleRate;
	return fRetVal;
}

FXD::F32 IVoice::get_playback_length(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	FXD::F32 fRetVal = m_voiceDesc.Audio->get_playback_length();
	return fRetVal;
}

Core::String8 IVoice::tell_playback_position_string(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	return Core::StringBuilder8::format_time(tell_playback_position());
}

void IVoice::_update(FXD::F32 /*dt*/)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	_update_buffers();
}

void IVoice::_update_loop_point(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	const SND::SndFormat& fmt = m_voiceDesc.Audio->source_format();
	if (!m_loopHandle.m_pLoopPoint)
	{
		m_loopHandle = m_voiceDesc.LoopGroup->begin();
	}
	else
	{
		m_loopHandle = m_voiceDesc.LoopGroup->next(m_loopHandle);
	}

	if (!m_loopHandle.m_pLoopPoint)
	{
		m_nBytesStartPos = 0;
		m_nBytesSize = 0;
		m_bStreamComplete = true;
	}
	else
	{
		m_nBytesStartPos = Math::CeilI((FXD::F32)m_loopHandle.m_pLoopPoint->StartPosition * (FXD::F32)fmt.BytesPerSec);
		m_nBytesSize = Math::CeilI((FXD::F32)m_loopHandle.m_pLoopPoint->EndPosition * (FXD::F32)fmt.BytesPerSec) - m_nBytesStartPos;
		m_bStreamComplete = false;
	}
}

void IVoice::_ensure_loop_points_defined(const SND::E_CreateFlags& eCreateFlag, const SND::Audio& audio)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!m_voiceDesc.LoopGroup)
	{
		PRINT_WARN << "SND: _ensure_loop_points_defined() no loop points defined. [" << audio->get_asset_name() << "]";

		// 1. New LoopGroup
		m_voiceDesc.LoopGroup = std::make_shared< SND::ISndLoopGroup >();
		m_voiceDesc.LoopGroup->voiceName(audio->get_asset_name());
		m_voiceDesc.LoopGroup->firstLoopPointName("start");

		// 2. New LoopPoint
		SND::LOOP_POINT_DESC loopDesc;
		loopDesc.EndPosition = audio->get_playback_length();
		loopDesc.Name = "start";
		if (eCreateFlag.is_flag_set(SND::E_CreateFlag::Looped))
		{
			loopDesc.RepeatMax = 0;
		}
		m_voiceDesc.LoopGroup->addLoopPoint(loopDesc);
	}
	else
	{
		if (m_voiceDesc.LoopGroup->voiceName() != audio->get_asset_name())
		{
			PRINT_WARN << "SND: AudioCueName and SndLoopPoints Name do not match";
		}
	}
}

// ------
// IVoiceSFX
// -
// ------
IVoiceSFX::IVoiceSFX(const SND::ISndAdapter* pSndAdapter, SND::INodeVoice* pNodeVoice)
	: IVoice(pSndAdapter, pNodeVoice)
	, m_nCurrentBuffer(0)
{
}

IVoiceSFX::~IVoiceSFX(void)
{
}


// ------
// IVoiceMusic
// -
// ------
IVoiceMusic::IVoiceMusic(const SND::ISndAdapter* pSndAdapter, SND::INodeVoice* pNodeVoice)
	: IVoice(pSndAdapter, pNodeVoice)
	, m_nCurrentBuffer(0)
{
}

IVoiceMusic::~IVoiceMusic(void)
{
}