// Creator - MatthewGolder
#include "FXDEngine/SND/Listener.h"
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace SND;

// ------
// IListener::_ListenerData
// - 
// ------
IListener::_ListenerData::_ListenerData(void)
	: m_matrix(Math::Matrix4F::kIdentity)
	, m_velocity(Math::Vector3F::kZero)
	, m_eDistanceModel(E_SndDistanceModel::Linear)
	, m_fDopplerFactor(1.0f)
	, m_fRolloffFactor(1.0f)
{
}

IListener::_ListenerData::~_ListenerData(void)
{
}

// ------
// IListener
// - 
// ------
IListener::IListener(const SND::ISndAdapter* pSndAdapter)
	: m_pSndAdapter(pSndAdapter)
{
}

IListener::~IListener(void)
{
}

bool IListener::create(void)
{
	auto funcRef = [&](void) { return _create(); };

	return Job::Async(FXDSND(), [=]() { return funcRef(); }).get();
}

bool IListener::release(void)
{
	auto funcRef = [&](void) { return _release(); };

	return Job::Async(FXDSND(), [=]() { return funcRef(); }).get();
}

void IListener::set_position(const Math::Vector3F& pos)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	m_listenerData.get_quick_write_lock()->m_matrix.set_translation(pos);
}

void IListener::set_up(const Math::Vector3F& up)
{
	m_listenerData.get_quick_write_lock()->m_matrix.set_up(up);
}

void IListener::set_forward(const Math::Vector3F& forward)
{
	m_listenerData.get_quick_write_lock()->m_matrix.set_forward(forward);
}

void IListener::set_velocity(const Math::Vector3F& velocity)
{
	m_listenerData.get_quick_write_lock()->m_velocity = velocity;
}

void IListener::set_distance_model(SND::E_SndDistanceModel eDistanceModel)
{
	m_listenerData.get_quick_write_lock()->m_eDistanceModel = eDistanceModel;
}

void IListener::set_doppler_factor(FXD::F32 fFactor)
{
	m_listenerData.get_quick_write_lock()->m_fDopplerFactor = fFactor;
}

void IListener::set_rolloff_factor(FXD::F32 fFactor)
{
	m_listenerData.get_quick_write_lock()->m_fRolloffFactor = fFactor;
}

Math::Vector3F IListener::position(void) const
{
	return m_listenerData.get_quick_read_lock()->m_matrix.get_translation();
}

Math::Vector3F IListener::up(void) const
{
	return m_listenerData.get_quick_read_lock()->m_matrix.get_up();
}

Math::Vector3F IListener::forward(void) const
{
	return m_listenerData.get_quick_read_lock()->m_matrix.get_forward();
}

Math::Vector3F IListener::velocity(void) const
{
	return m_listenerData.get_quick_read_lock()->m_velocity;
}

SND::E_SndDistanceModel IListener::distance_model(void) const
{
	return m_listenerData.get_quick_read_lock()->m_eDistanceModel;
}

FXD::F32 IListener::doppler_factor(void) const
{
	return m_listenerData.get_quick_read_lock()->m_fDopplerFactor;
}

FXD::F32 IListener::rolloff_factor(void) const
{
	return m_listenerData.get_quick_read_lock()->m_fRolloffFactor;
}