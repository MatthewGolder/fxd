// Creator - MatthewGolder
#include "FXDEngine/SND/Emitter.h"
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/SndAdapter.h"
#include "FXDEngine/SND/Listener.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace SND;

// ------
// IEmitter
// - 
// ------
IEmitter::IEmitter(const SND::ISndAdapter* pSndAdapter, const SND::INodeVoice* pNodeVoice)
	: m_pSndAdapter(pSndAdapter)
	, m_pNodeVoice(pNodeVoice)
	, m_fMinDistance(1.0f)
	, m_fMaxDistance(100.0f)
	, m_matrix(Math::Matrix4F::kIdentity)
	, m_velocity(Math::Vector3F::kZero)
{
}

IEmitter::~IEmitter(void)
{
}

bool IEmitter::create_emitter(void)
{
	auto funcRef = [&](void) { return _create_emitter(); };

	return Job::Async(FXDSND(), [=]() { return funcRef(); }).get();
}

bool IEmitter::release_emitter(void)
{
	auto funcRef = [&](void) { return _release_emitter(); };

	return Job::Async(FXDSND(), [=]() { return funcRef(); }).get();
}

Math::Vector3F IEmitter::position(void) const
{
	auto funcRef = [&](void) { return _position(); };

	return Job::Async(FXDSND(), [=]() { return funcRef(); }).get();
}

Math::Vector3F IEmitter::up(void) const
{
	auto funcRef = [&](void) { return _up(); };

	return Job::Async(FXDSND(), [=]() { return funcRef(); }).get();
}

Math::Vector3F IEmitter::forward(void) const
{
	auto funcRef = [&](void) { return _forward(); };

	return Job::Async(FXDSND(), [=]() { return funcRef(); }).get();
}

Math::Vector3F IEmitter::velocity(void) const
{
	auto funcRef = [&](void) { return _velocity(); };

	return Job::Async(FXDSND(), [=]() { return funcRef(); }).get();
}

FXD::F32 IEmitter::min_distance(void) const
{
	auto funcRef = [&](void) { return _min_distance(); };

	return Job::Async(FXDSND(), [=]() { return funcRef(); }).get();
}

FXD::F32 IEmitter::max_distance(void) const
{
	auto funcRef = [&](void) { return _max_distance(); };

	return Job::Async(FXDSND(), [=]() { return funcRef(); }).get();
}

FXD::F32 IEmitter::falloff(void) const
{
	auto funcRef = [&](void) { return _falloff(); };

	return Job::Async(FXDSND(), [=]() { return funcRef(); }).get();
}

void IEmitter::set(const Math::Matrix4F& value)
{
	auto funcRef = [&](const Math::Matrix4F& value) { _set(value); };

	Job::Async(FXDSND(), [=]() { funcRef(value); }).get();
}

void IEmitter::set_position(const Math::Vector3F& pos)
{
	auto funcRef = [&](const Math::Vector3F& pos) { _set_position(pos); };

	Job::Async(FXDSND(), [=]() { funcRef(pos); }).get();
}

void IEmitter::set_up(const Math::Vector3F& up)
{
	auto funcRef = [&](const Math::Vector3F& up) { _set_up(up); };

	Job::Async(FXDSND(), [=]() { funcRef(up); }).get();
}

void IEmitter::set_forward(const Math::Vector3F& forward)
{
	auto funcRef = [&](const Math::Vector3F& forward) { _setForward(forward); };

	Job::Async(FXDSND(), [=]() { funcRef(forward); }).get();
}

void IEmitter::set_velocity(const Math::Vector3F& velocity)
{
	auto funcRef = [&](const Math::Vector3F& velocity) { _set_velocity(velocity); };

	Job::Async(FXDSND(), [=]() { funcRef(velocity); }).get();
}

void IEmitter::set_min_max_distance(FXD::F32 fMin, FXD::F32 fMax)
{
	auto funcRef = [&](FXD::F32 fMin, FXD::F32 fMax) { _set_min_max_distance(fMin, fMax); };

	Job::Async(FXDSND(), [=]() { funcRef(fMin, fMax); }).get();
}

Math::Vector3F IEmitter::_position(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	return m_matrix.get_translation();
}

Math::Vector3F IEmitter::_up(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	return m_matrix.get_up();
}

Math::Vector3F IEmitter::_forward(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	return m_matrix.get_forward();
}

Math::Vector3F IEmitter::_velocity(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	return m_velocity;
}

FXD::F32 IEmitter::_min_distance(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	return m_fMinDistance;
}

FXD::F32 IEmitter::_max_distance(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	return m_fMaxDistance;
}

FXD::F32 IEmitter::_falloff(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	FXD::F32 fRolloff = m_pSndAdapter->listener()->rolloff_factor();
	FXD::F32 fDistance = m_pSndAdapter->listener()->position().distance(position());

	if (m_pSndAdapter->listener()->distance_model() == E_SndDistanceModel::Linear)
	{
		return (1.0f - fRolloff * (fDistance - m_fMinDistance) / (m_fMaxDistance - m_fMinDistance));
	}
	else
	{
		return (m_fMinDistance / (m_fMinDistance + fRolloff * (fDistance - m_fMinDistance)));
	}
}

void IEmitter::_set(const Math::Matrix4F& value)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	set_position(value.get_translation());
	set_up(value.get_up());
	set_forward(value.get_forward());
}

void IEmitter::_set_position(const Math::Vector3F& pos)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	m_matrix.set_translation(pos);
}

void IEmitter::_set_up(const Math::Vector3F& up)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	//m_matrix.set_up(up);
}

void IEmitter::_setForward(const Math::Vector3F& forward)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	//m_matrix.set_forward(forward);
}

void IEmitter::_set_velocity(const Math::Vector3F& velocity)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	m_velocity = velocity;
}

void IEmitter::_set_min_max_distance(FXD::F32 fMin, FXD::F32 fMax)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	m_fMinDistance = fMin;
	m_fMaxDistance = fMax;

	_update_distance_model();
}