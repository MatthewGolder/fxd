// Creator - MatthewGolder
#include "FXDEngine/SND/SndNode.h"
#include "FXDEngine/SND/Effect.h"
#include "FXDEngine/SND/Emitter.h"
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/Voice.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/IO/IODevice.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOSystem.h"
#include "FXDEngine/IO/Path.h"
#include "FXDEngine/IO/XML/XMLSaxParser.h"
#include "FXDEngine/Job/Async.h"
#include "FXDEngine/Memory/StreamMemHandle.h"
#include "FXDEngine/Core/Algorithm.h"

using namespace FXD;
using namespace SND;

// ------
// SndNodeTagCB
// -
// ------
class SndNodeTagCB FINAL : public IO::XMLSaxParser::ITagCB
{
public:
	enum class E_Tag
	{
		Group = 0,
		Mixer = 1,
		Count,
		Unknown = 0xffff
	};

	// ------
	// SndNodeCreate
	// -
	// ------
	class SndNodeCreate
	{
	public:
		SndNodeCreate(SndNodeCreate* pParent)
			: m_pParent(pParent)
		{}
		~SndNodeCreate(void)
		{
			fxd_for(auto& child, m_children)
			{
				FXD_SAFE_DELETE(child);
			}
		}

		void construct(SND::INodeMixer* pNodeMaster)
		{
			fxd_for(auto& pChild, m_children)
			{
				SND::NodeMixer nodeMixer = pNodeMaster->add_mixer(pChild->m_nodeDesc);
				pChild->construct(pNodeMaster);
			}
		}

	public:
		NODE_DESC m_nodeDesc;
		SndNodeTagCB::SndNodeCreate* m_pParent;
		Container::Vector< SndNodeTagCB::SndNodeCreate* > m_children;
	};

public:
	SndNodeTagCB(void)
		: m_eTag(E_Tag::Unknown)
		, m_pCurrentCreate(nullptr)
		, m_parent(nullptr)
	{}
	~SndNodeTagCB(void)
	{}

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL
	{
		pTagCB = this;
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("Group"))
				{
					m_eTag = E_Tag::Group;
				}
				break;
			}
			case E_Tag::Group:
			{
				if (strName == UTF_8("MixerNode"))
				{
					m_eTag = E_Tag::Mixer;
					m_parent.m_children.push_back(FXD_NEW(SndNodeTagCB::SndNodeCreate(m_pCurrentCreate)));
					m_pCurrentCreate = m_parent.m_children.back();
				}
				break;
			}
			default:
			{
				PRINT_ASSERT << "SND: Unknown tag";
				break;
			}
		}
	}
	void end_tag(const Core::String8& strName) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Group:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			case E_Tag::Mixer:
			{
				m_pCurrentCreate = m_pCurrentCreate->m_pParent;
				if (m_pCurrentCreate == nullptr)
				{
					m_eTag = E_Tag::Group;
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}

	void handle_text(const Core::String8& /*strText*/, const IO::StreamParser::Position& /*pos*/) FINAL
	{
	}

	void handle_attribute(const Core::String8& strName, const Core::String8& strValue) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Mixer:
			{
				if (strName == UTF_8("name"))
				{
					m_pCurrentCreate->m_nodeDesc.Name = strValue;
					break;
				}
				else if (strName == UTF_8("volume"))
				{
					m_pCurrentCreate->m_nodeDesc.Volume = strValue.to_F32();
					break;
				}
				else if (strName == UTF_8("sample_rate"))
				{
					m_pCurrentCreate->m_nodeDesc.SampleRate = strValue.to_S32();
					break;
				}
				else if (strName == UTF_8("channels"))
				{
					m_pCurrentCreate->m_nodeDesc.OutChannels = strValue.to_S32();
					break;
				}
				else if (strName == UTF_8("auxiliary"))
				{
					m_pCurrentCreate->m_nodeDesc.Auxiliary = strValue.to_bool();
					break;
				}
				else
				{
					PRINT_ASSERT << "SND: Unknown tag";
					break;
				}
			}
			default:
			{
				PRINT_ASSERT << "SND: Unknown tag";
				break;
			}
		}
	}

public:
	E_Tag m_eTag;
	SndNodeCreate* m_pCurrentCreate;
	SndNodeCreate m_parent;
};

// ------
// INodeData
// -
// ------
INodeData::INodeData(void)
{
}

INodeData::~INodeData(void)
{
}

// ------
// INode
// -
// ------
INode::INode(const SND::ISndAdapter* pSndAdapter, const SND::INodeMixer* pNodeParent)
	: m_pSndAdapter(pSndAdapter)
	, m_pNodeParent(pNodeParent)
{
}

INode::~INode(void)
{
}

SND::NODE_DESC INode::get_node_desc(void) const
{
	auto funcRef = [&](void)
	{
		return _get_node_desc();
	};

	return Job::Async(FXDSND(), [=]()
	{
		return funcRef();
	}).get();
}

SND::NodeData INode::get_node_data(void) const
{
	auto funcRef = [&](void)
	{
		return _get_node_data();
	};

	return Job::Async(FXDSND(), [=]()
	{
		return funcRef();
	}).get();
}

const SND::INodeMixer* INode::get_node_parent(void) const
{
	auto funcRef = [&](void)
	{
		return _get_node_parent();
	};

	return Job::Async(FXDSND(), [=]()
	{
		return funcRef();
	}).get();
}

void INode::_set_volume(FXD::F32 fVolume, FXD::F32 fDuration)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	m_nodeDesc.Volume.set(fVolume, fDuration);
}

SND::NODE_DESC INode::_get_node_desc(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");
	return m_nodeDesc;
}

SND::NodeData INode::_get_node_data(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");
	return m_nodeData;
}

const SND::INodeMixer* INode::_get_node_parent(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");
	return m_pNodeParent;
}

void INode::set_volume(FXD::F32 fVolume, FXD::F32 fDuration)
{
	auto funcRef = [&](FXD::F32 fVolume, FXD::F32 fDuration)
	{
		_set_volume(fVolume, fDuration);
	};

	Job::Async(FXDSND(), [=]()
	{
		funcRef(fVolume, fDuration);
	}).get();
}

// ------
// INodeMixer
// -
// ------
bool INodeMixer::FromStream(Core::StreamIn& stream, SND::INodeMixer* pNodeMaster)
{
	if (stream)
	{
		SndNodeTagCB callback;
		IO::StreamParser streamParser(stream);
		IO::XMLSaxParser::parse(streamParser, callback);

		callback.m_parent.construct(pNodeMaster);
		return true;
	}
	return false;
}


INodeMixer::INodeMixer(const SND::ISndAdapter* pSndAdapter, const SND::INodeMixer* pNodeParent)
	: INode(pSndAdapter, pNodeParent)
{
}

INodeMixer::~INodeMixer(void)
{
}

void INodeMixer::create(const NODE_DESC& nodeDesc)
{
	auto funcRef = [&](const NODE_DESC& nodeDesc)
	{
		_create(nodeDesc);
	};

	Job::Async(FXDSND(), [=]()
	{
		funcRef(nodeDesc);
	}).get();
}

void INodeMixer::release(void)
{
	auto funcRef = [&](void)
	{
		_release();
	};

	Job::Async(FXDSND(), [=]()
	{
		funcRef();
	}).get();
}

bool INodeMixer::load_nodes(const IO::Path& ioPath)
{
	auto funcRef = [&](const IO::Path& ioPath)
	{
		return _load_nodes(ioPath);
	};

	return Job::Async(FXDSND(), [=]()
	{
		return funcRef(ioPath);
	}).get();
}

bool INodeMixer::load_nodes_stream(Core::StreamIn& stream)
{
	auto funcRef = [&](Core::StreamIn stream)
	{
		return _load_nodes_stream(stream);
	};

	return Job::Async(FXDSND(), [=]()
	{
		return funcRef(stream);
	}).get();
}

bool INodeMixer::load_effects(const IO::Path& ioPath)
{
	auto funcRef = [&](const IO::Path& ioPath)
	{
		return _load_effects(ioPath);
	};

	return Job::Async(FXDSND(), [=]()
	{
		return funcRef(ioPath);
	}).get();
}

bool INodeMixer::load_effects_stream(Core::StreamIn& stream)
{
	auto funcRef = [&](Core::StreamIn stream)
	{
		return _load_effects_stream(stream);
	};

	return Job::Async(FXDSND(), [=]()
	{
		return funcRef(stream);
	}).get();
}

NodeMixer INodeMixer::add_mixer(const SND::NODE_DESC& nodeDesc)
{
	auto funcRef = [&](const SND::NODE_DESC& nodeDesc)
	{
		return _add_mixer(nodeDesc);
	};

	return Job::Async(FXDSND(), [=]()
	{
		return funcRef(nodeDesc);
	}).get();
}

SND::NodeVoice INodeMixer::add_voice(const SND::NODE_VOICE_DESC& nodeVoiceDesc)
{
	auto funcRef = [&](const SND::NODE_VOICE_DESC& nodeVoiceDesc)
	{
		return _add_voice(nodeVoiceDesc);
	};

	return Job::Async(FXDSND(), [=]()
	{
		return funcRef(nodeVoiceDesc);
	}).get();
}

SND::NodeMixer INodeMixer::find_mixer(const Core::String8& strNodeName)
{
	auto funcRef = [&](const Core::String8& strNodeName)
	{
		return _find_mixer(strNodeName);
	};

	return Job::Async(FXDSND(), [=]()
	{
		return funcRef(strNodeName);
	}).get();
}

void INodeMixer::_update(FXD::F32 dt, const SND::Listener& listener)
{
	_remove_finished_voices();
	_update_volume(dt);
	if (m_effectChain)
	{
		m_effectChain->_update(dt);
	}

	FXD::STD::for_each(m_mixers.begin(), m_mixers.end(), [&](auto& mixer)
	{
		mixer.second->_update(dt, listener);
	});

	FXD::STD::for_each(m_voices.begin(), m_voices.end(), [&](auto& voice)
	{
		voice->_update(dt, listener);
	});
}

void INodeMixer::_remove_finished_voices(void)
{
	FXD::STD::for_each(m_mixers.begin(), m_mixers.end(), [&](auto& mixer)
	{
		mixer.second->_remove_finished_voices();
	});

	fxd_for_itr(itr, m_voices)
	{
		if (((*itr).use_count() <= 1) && (!(*itr)->is_playing()))
		{
			(*itr)->release();
			//asda = m_voices.remove(itr);
		}
	}
}

bool INodeMixer::_load_nodes(const IO::Path& ioPath)
{
	Core::StreamIn stream = FXD::FXDIO()->io_system()->io_device()->file_open_memory_stream(ioPath, false, { }).get();
	return _load_nodes_stream(stream);
}

bool INodeMixer::_load_nodes_stream(Core::StreamIn& stream)
{
	return SND::INodeMixer::FromStream(stream, this);
}

bool INodeMixer::_load_effects(const IO::Path& ioPath)
{
	Core::StreamIn stream = FXD::FXDIO()->io_system()->io_device()->file_open_memory_stream(ioPath, false, { }).get();
	return _load_effects_stream(stream);
}

bool INodeMixer::_load_effects_stream(Core::StreamIn& stream)
{
	return SND::IEffectChain::FromStream(stream, m_effectChain.get());
}

SND::NodeMixer INodeMixer::_find_mixer(const Core::String8& strNodeName)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	auto itr = m_mixers.find(strNodeName);
	if (itr != m_mixers.end())
	{
		return (*itr).second;
	}
	return SND::NodeMixer();
}

// ------
// INodeVoice
// -
// ------
INodeVoice::INodeVoice(const SND::ISndAdapter* pSndAdapter, const SND::INodeMixer* pNodeParent)
	: INode(pSndAdapter, pNodeParent)
	, m_voice(nullptr)
	, m_emitter(nullptr)
	, m_nPriority(Math::NumericLimits< FXD::U8 >::max())
	, m_fRatio(1.0f)
	, m_eVoiceState(E_VoiceState::Stopped, false)
{
}

INodeVoice::~INodeVoice(void)
{
}

void INodeVoice::create(const SND::NODE_DESC& nodeDesc, const SND::NODE_VOICE_DESC& nodeVoiceDesc)
{
	auto funcRef = [&](const SND::NODE_DESC& nodeDesc, const SND::NODE_VOICE_DESC& nodeVoiceDesc)
	{
		_create(nodeDesc, nodeVoiceDesc);
	};

	Job::Async(FXDSND(), [=]()
	{
		funcRef(nodeDesc, nodeVoiceDesc);
	}).get();
}

void INodeVoice::release(void)
{
	auto funcRef = [&](void)
	{
		_release();
	};

	Job::Async(FXDSND(), [=]()
	{
		funcRef();
	}).get();
}


void INodeVoice::play(void)
{
	if (!is_playing())
	{
		m_eVoiceState = E_VoiceState::Playing;
	}
}

void INodeVoice::pause(void)
{
	if (is_playing())
	{
		m_eVoiceState = E_VoiceState::Paused;
	}
	else
	{
		m_eVoiceState = E_VoiceState::Playing; //Unless we tell it otherwise the voice resumes playback from its last position
	}
}

void INodeVoice::stop(void)
{
	if (!is_stopped())
	{
		m_eVoiceState = E_VoiceState::Stopped;
	}
}

bool INodeVoice::is_playing(void) const
{
	return (m_eVoiceState == E_VoiceState::Playing);
}

bool INodeVoice::is_paused(void) const
{
	return (m_eVoiceState == E_VoiceState::Paused);
}

bool INodeVoice::is_stopped(void) const
{
	return (m_eVoiceState == E_VoiceState::Stopped);
}

bool INodeVoice::is_finished(void) const
{
	auto funcRef = [&](void)
	{
		return m_voice->is_finished();
	};

	return Job::Async(FXDSND(), [=]()
	{
		return funcRef();
	}).get();
}

bool INodeVoice::is_valid(void) const
{
	auto funcRef = [&](void)
	{
		return _is_valid();
	};

	return Job::Async(FXDSND(), [=]()
	{
		return funcRef();
	}).get();
}

FXD::U32 INodeVoice::tell_sample_position(void) const
{
	auto funcRef = [&](void)
	{
		return m_voice->tell_sample_position();
	};

	return Job::Async(FXDSND(), [=]()
	{
		return funcRef();
	}).get();
}

FXD::F32 INodeVoice::tell_playback_position(void) const
{
	auto funcRef = [&](void)
	{
		return m_voice->tell_playback_position();
	};

	return Job::Async(FXDSND(), [=]()
	{
		return funcRef();
	}).get();
}

FXD::F32 INodeVoice::get_playback_length(void) const
{
	auto funcRef = [&](void)
	{
		return m_voice->get_playback_length();
	};

	return Job::Async(FXDSND(), [=]()
	{
		return funcRef();
	}).get();
}

Core::String8 INodeVoice::tell_playback_position_string(void) const
{
	auto funcRef = [&](void)
	{
		return m_voice->tell_playback_position_string();
	};

	return Job::Async(FXDSND(), [=]()
	{
		return funcRef();
	}).get();
}

void INodeVoice::set_frequency(FXD::F32 fRatio, FXD::F32 fDuration)
{
	auto funcRef = [&](FXD::F32 fRatio, FXD::F32 fDuration)
	{
		_set_frequency(fRatio, fDuration);
	};

	Job::Async(FXDSND(), [=]()
	{
		funcRef(fRatio, fDuration);
	}).get();
}

void INodeVoice::set_priority(FXD::U8 nPriority)
{
	auto funcRef = [&](FXD::U8 nPriority)
	{
		_set_priority(nPriority);
	};

	Job::Async(FXDSND(), [=]()
	{
		funcRef(nPriority);
	}).get();
}

FXD::F32 INodeVoice::get_frequency(void) const
{
	auto funcRef = [&](void)
	{
		return _get_frequency();
	};

	return Job::Async(FXDSND(), [=]()
	{
		return funcRef();
	}).get();
}

FXD::F32 INodeVoice::get_importance(void) const
{
	auto funcRef = [&](void)
	{
		return _get_importance();
	};

	return Job::Async(FXDSND(), [=]()
	{
		return funcRef();
	}).get();
}

void INodeVoice::_update(FXD::F32 dt, const SND::Listener& listener)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	_update_volume(dt);
	_update_frequency(dt);
	_update_emitter(dt, listener);
	_update_voice(dt, listener);

	/*
	FXD::F32 m_fUpdateTime;
	m_fUpdateTime -= dt;
	if (m_fUpdateTime <= 0.0f)
	{
	m_fUpdateTime = 0.1f;
	}
	*/
}

void INodeVoice::_update_emitter(FXD::F32 /*dt*/, const SND::Listener& listener)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!is_playing())
	{
		return;
	}

	if (m_emitter)
	{
		m_emitter->_update(listener);
	}
}

void INodeVoice::_update_voice(FXD::F32 dt, const SND::Listener& listener)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// Update voice state 
	if (m_eVoiceState.is_dirty())
	{
		if (is_valid())
		{
			switch (m_eVoiceState)
			{
			case E_VoiceState::Playing:
			{
				m_voice->play();
				break;
			}
			case E_VoiceState::Paused:
			{
				m_voice->pause();
				break;
			}
			case E_VoiceState::Stopped:
			{
				m_voice->stop();
				break;
			}
			default:
			{
				PRINT_ASSERT << "SND: Unknown State Type";
				break;
			}
			}
			m_eVoiceState.mark_clean();
		}
	}

	if (m_voice)
	{
		m_voice->_update(dt);
	}
}

void INodeVoice::_set_frequency(FXD::F32 fRatio, FXD::F32 fDuration)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	m_fRatio.set(fRatio, fDuration);
}

void INodeVoice::_set_priority(FXD::U8 nPriority)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (m_emitter)
	{
		m_nPriority = nPriority;
	}
}

FXD::F32 INodeVoice::_get_frequency(void) const
{
	return m_fRatio;
}

FXD::F32 INodeVoice::_get_importance(void) const
{
	FXD::F32 fImportance = m_nodeDesc.Volume;
	const SND::INodeMixer* pNode = m_pNodeParent;
	while (pNode != nullptr)
	{
		fImportance *= pNode->get_node_desc().Volume;
		pNode = pNode->get_node_parent();
	}

	if (m_emitter)
	{
		fImportance *= FXD::STD::clamp(m_emitter->falloff(), 0.0f, 1.0f);
	}
	return (((FXD::F32)m_nPriority) * fImportance);
}