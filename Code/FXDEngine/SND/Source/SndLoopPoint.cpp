// Creator - MatthewGolder
#include "FXDEngine/SND/SndLoopPoint.h"
#include "FXDEngine/IO/XML/XMLSaxParser.h"

using namespace FXD;
using namespace SND;

// ------
// SndLoopPointTagCB
// -
// ------
class SndLoopPointTagCB FINAL : public IO::XMLSaxParser::ITagCB, public Core::NonCopyable
{
public:
	enum class E_Tag
	{
		LoopGroup = 0,
		LoopPoint,
		Count,
		Unknown = 0xffff
	};

public:
	SndLoopPointTagCB(SND::SndLoopGroup& loopGroup)
		: m_eTag(SndLoopPointTagCB::E_Tag::Unknown)
		, m_loopGroup(loopGroup)
	{}
	~SndLoopPointTagCB(void)
	{}

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB)FINAL
	{
		pTagCB = this;
		switch (m_eTag)
		{
			case SndLoopPointTagCB::E_Tag::Unknown:
			{
				if (strName == Core::String8("SndLoopGroup"))
				{
					m_eTag = SndLoopPointTagCB::E_Tag::LoopGroup;
				}
				break;
			}
			case SndLoopPointTagCB::E_Tag::LoopGroup:
			{
				if (strName == Core::String8("SndLoopPoint"))
				{
					m_eTag = SndLoopPointTagCB::E_Tag::LoopPoint;
					m_currentDesc = SND::LOOP_POINT_DESC();
				}
				break;
			}
			default:
			{
				PRINT_ASSERT << "SND: Unknown XML Tag in SndLoopGroup";
				break;
			}
		}
	}
	void end_tag(const Core::String8& strName)FINAL
	{
		switch (m_eTag)
		{
			case SndLoopPointTagCB::E_Tag::LoopGroup:
			{
				m_eTag = SndLoopPointTagCB::E_Tag::Unknown;
				break;
			}
			case SndLoopPointTagCB::E_Tag::LoopPoint:
			{
				m_loopGroup->addLoopPoint(m_currentDesc);
				m_currentDesc = SND::LOOP_POINT_DESC();
				m_eTag = SndLoopPointTagCB::E_Tag::LoopGroup;
				break;
			}
			default:
			{
				break;
			}
		}
	}
	
	void handle_text(const Core::String8& /*strText*/, const FXD::IO::StreamParser::Position& /*pos*/)FINAL
	{
	}

	void handle_attribute(const Core::String8& strName, const Core::String8& strValue)FINAL
	{
		switch (m_eTag)
		{
			case SndLoopPointTagCB::E_Tag::LoopGroup:
			{
				if (strName == Core::String8("voice_name"))
				{
					m_loopGroup->m_strVoiceName = strValue;
				}
				else if (strName == Core::String8("first_loop_point"))
				{
					m_loopGroup->m_strFirstLoopPointName = strValue;
				}
				else
				{
					PRINT_ASSERT << "SND: Unknown XML Attribute";
				}
				break;
			}
			case SndLoopPointTagCB::E_Tag::LoopPoint:
			{
				if (strName == Core::String8("loop_point_name"))
				{
					m_currentDesc.Name = strValue;
				}
				else if (strName == Core::String8("next_loop_point_name"))
				{
					m_currentDesc.NextName = strValue;
				}
				else if (strName == Core::String8("repeat_max"))
				{
					m_currentDesc.RepeatMax = strValue.to_S32();
				}
				else if (strName == Core::String8("start_position"))
				{
					m_currentDesc.StartPosition = strValue.to_F32();
				}
				else if (strName == Core::String8("end_position"))
				{
					m_currentDesc.EndPosition = strValue.to_F32();
				}
				else
				{
					PRINT_ASSERT << "SND: Unknown XML Attribute";
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}

private:
	SndLoopPointTagCB::E_Tag m_eTag;
	SND::SndLoopGroup& m_loopGroup;
	SND::LOOP_POINT_DESC m_currentDesc;
};


LoopPointHandle::LoopPointHandle(void)
	: m_pLoopPoint(nullptr)
	, m_nRepeatCount(0)
	, m_nLoopPointBytesRead(0)
{
}

LoopPointHandle::~LoopPointHandle(void)
{
}

void LoopPointHandle::_incrementPlayed(void)
{
	if (m_pLoopPoint->isInfinate())
	{
		return;
	}
	else
	{
		m_nRepeatCount++;
		PRINT_COND_ASSERT((m_nRepeatCount <= m_pLoopPoint->RepeatMax), "SND: Incremented above max repeat count");
	}
}

void LoopPointHandle::_resetPlayedCount(void)
{
	m_nRepeatCount = 0;
	m_nLoopPointBytesRead = 0;
}

bool LoopPointHandle::_is_finished(void) const
{
	if (m_pLoopPoint->isInfinate())
	{
		return false;
	}
	return (m_pLoopPoint->RepeatMax == m_nRepeatCount);
}

// ------
// LOOP_POINT_DESC
// - 
// ------
bool LOOP_POINT_DESC::isInfinate(void) const
{
	return (RepeatMax == 0);
}

// ------
// SndLoopGroup
// - 
// ------
SND::SndLoopGroup ISndLoopGroup::FromStream(Core::StreamIn& stream)
{
	if (stream)
	{
		SND::SndLoopGroup loopGroup = std::make_shared< SND::ISndLoopGroup >();

		SndLoopPointTagCB callback(loopGroup);
		IO::StreamParser streamParser(stream);
		IO::XMLSaxParser::parse(streamParser, callback);
		return loopGroup;
	}
	return SND::SndLoopGroup();
}

ISndLoopGroup::ISndLoopGroup(void)
{
}

ISndLoopGroup::~ISndLoopGroup(void)
{
}

void ISndLoopGroup::addLoopPoint(const SND::LOOP_POINT_DESC& loopDesc)
{
	fxd_for(auto& itr, m_loopDescs)
	{
		PRINT_COND_ASSERT((itr.first != loopDesc.Name.to_hash()), "SND: PointName already in use");
	}
	m_loopDescs[loopDesc.Name.to_hash()] = loopDesc;
}

SND::LoopPointHandle ISndLoopGroup::begin(void) const
{
	fxd_for(auto& itr, m_loopDescs)
	{
		if (itr.first == m_strFirstLoopPointName.to_hash())
		{
			SND::LoopPointHandle loopPointHandle;
			loopPointHandle.m_nRepeatCount = 0;
			loopPointHandle.m_pLoopPoint = &itr.second;
			return loopPointHandle;
		}
	}

	PRINT_WARN << "SND: Cannot find first loop point - " << m_strFirstLoopPointName << "";
	return SND::LoopPointHandle();
}

SND::LoopPointHandle ISndLoopGroup::next(SND::LoopPointHandle& loopPointHandle) const
{
	loopPointHandle._incrementPlayed();

	bool bFinished = loopPointHandle._is_finished();
	if (!bFinished)
	{
		SND::LoopPointHandle nextPointHandle = loopPointHandle;
		nextPointHandle.m_nLoopPointBytesRead = 0;
		return nextPointHandle;
	}

	const Core::String8& strNextName = loopPointHandle.m_pLoopPoint->NextName;
	if (strNextName.empty())
	{
		return SND::LoopPointHandle();
	}

	fxd_for(auto& itr, m_loopDescs)
	{
		if (itr.first == strNextName.to_hash())
		{
			LoopPointHandle nextPointHandle;
			nextPointHandle.m_nRepeatCount = 0;
			nextPointHandle.m_pLoopPoint = &itr.second;
			return nextPointHandle;
		}
	}
	PRINT_WARN << "SND: Cannot find next loop point - " << strNextName << "";
	return SND::LoopPointHandle();
}