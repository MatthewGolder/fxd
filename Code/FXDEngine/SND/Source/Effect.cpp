// Creator - MatthewGolder
#include "FXDEngine/SND/Effect.h"
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/IO/XML/XMLSaxParser.h"
#include "FXDEngine/Job/Async.h"

using namespace FXD;
using namespace SND;

// ------
// EffectChainTagCB
// -
// ------
class EffectChainTagCB FINAL : public IO::XMLSaxParser::ITagCB
{
public:
	enum class E_Tag
	{
		EffectChain = 0,
		Effect,
		Count,
		Unknown = 0xffff
	};

public:
	EffectChainTagCB(SND::IEffectChain* pEffectChain)
		: m_eTag(E_Tag::Unknown)
		, m_pEffectChain(pEffectChain)
	{
	}
	~EffectChainTagCB(void)
	{
	}

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL;
	void end_tag(const Core::String8& strName) FINAL;
	void handle_text(const Core::String8& strText, const IO::StreamParser::Position& pos) FINAL;
	void handle_attribute(const Core::String8& strName, const Core::String8& strValue) FINAL;

protected:
	E_Tag m_eTag;
	SND::EFFECT_DESC m_effectDesc;
	SND::IEffectChain* m_pEffectChain;
};

void EffectChainTagCB::handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB)
{
	pTagCB = this;
	switch (m_eTag)
	{
		case E_Tag::Unknown:
		{
			if (strName == Core::String8("EffectChain"))
			{
				m_eTag = E_Tag::EffectChain;
			}
			break;
		}
		case E_Tag::EffectChain:
		{
			if (strName == Core::String8("Effect"))
			{
				m_eTag = E_Tag::Effect;
				m_effectDesc = SND::EFFECT_DESC();
			}
			break;
		}
		default:
		{
			PRINT_ASSERT << "SND: Unknown XML Tag in EffectChainTagCB";
			break;
		}
	}
}

void EffectChainTagCB::end_tag(const Core::String8& strName)
{
	switch (m_eTag)
	{
		case E_Tag::EffectChain:
		{
			m_eTag = E_Tag::Unknown;
			break;
		}
		case E_Tag::Effect:
		{
			m_pEffectChain->add_effect(m_effectDesc);
			m_eTag = E_Tag::EffectChain;
			break;
		}
		default:
		{
			break;
		}
	}
}

void EffectChainTagCB::handle_text(const Core::String8& strText, const IO::StreamParser::Position& pos)
{
}

void EffectChainTagCB::handle_attribute(const Core::String8& strName, const Core::String8& strValue)
{
	switch (m_eTag)
	{
		case E_Tag::EffectChain:
		{
			break;
		}
		case E_Tag::Effect:
		{
			/*
			if (strName == Core::String8("enabled"))
				m_effectDef.enabled(strValue.to_bool());
			else if (strName == Core::String8("wet_dry_mix"))
				m_effectDef.wetDryMix(strValue.to_F32());
			else if (strName == Core::String8("room_size"))
				m_effectDef.roomSize(strValue.to_S32());
			else if (strName == Core::String8("room_hf"))
				m_effectDef.roomHF(strValue.to_S32());
			else if (strName == Core::String8("room_rolloff"))
				m_effectDef.roomRolloff(strValue.to_F32());
			else if (strName == Core::String8("decay_time"))
				m_effectDef.decayTime(strValue.to_F32());
			else if (strName == Core::String8("decay_hf_ratio"))
				m_effectDef.decayHFRatio(strValue.to_F32());
			else if (strName == Core::String8("reflections"))
				m_effectDef.reflections(strValue.to_S32());
			else if (strName == Core::String8("reflections_delay"))
				m_effectDef.reflectionsDelay(strValue.to_F32());
			else if (strName == Core::String8("reverb"))
				m_effectDef.reverb(strValue.to_S32());
			else if (strName == Core::String8("reverb_delay"))
				m_effectDef.reverbDelay(strValue.to_F32());
			else if (strName == Core::String8("diffusion"))
				m_effectDef.diffusion(strValue.to_F32());
			else if (strName == Core::String8("density"))
				m_effectDef.density(strValue.to_F32());
			else if (strName == Core::String8("hf_reference"))
				m_effectDef.hfReference(strValue.to_F32());
			break;
			*/
		}
		default:
		{
			break;
		}
	}
}

// ------
// EFFECT_DESC
// -
// ------
// | Enabled | WetDry | Gain | GainHQ | DecayTime | DecayHQ | Reflections | ReflectionsDelay | Reverb | ReverbDelay | Difussion | Density | AirAbsorbtion | RoomRollOff |

// Default Presets
const EFFECT_DESC EFFECT_DESC::kPresetGeneric						(true, 100.0f, 0.316200f, 0.891300f, 1.490000f, 0.830000f, 0.050000f, 0.007000f, 1.258900f, 0.011000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetPaddedCell					(true, 100.0f, 0.316200f, 0.001000f, 0.170000f, 0.100000f, 0.250000f, 0.001000f, 1.269100f, 0.002000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetRoom							(true, 100.0f, 0.316200f, 0.592900f, 0.400000f, 0.830000f, 0.150300f, 0.002000f, 1.062900f, 0.003000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetBathroom						(true, 100.0f, 0.316200f, 0.251200f, 1.490000f, 0.540000f, 0.653100f, 0.007000f, 3.273400f, 0.011000f, 1.000000f, 0.600000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetLivingRoom					(true, 100.0f, 0.316200f, 0.001000f, 0.500000f, 0.100000f, 0.205100f, 0.003000f, 0.280500f, 0.004000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetStoneRoom						(true, 100.0f, 0.316200f, 0.707900f, 2.310000f, 0.640000f, 0.441100f, 0.012000f, 1.100300f, 0.017000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetAuditorium					(true, 100.0f, 0.316200f, 0.578100f, 4.320000f, 0.590000f, 0.403200f, 0.020000f, 0.717000f, 0.030000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetConcertHall					(true, 100.0f, 0.316200f, 0.562300f, 3.920000f, 0.700000f, 0.242700f, 0.020000f, 0.997700f, 0.029000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetCave							(true, 100.0f, 0.316200f, 1.000000f, 2.910000f, 1.300000f, 0.500000f, 0.015000f, 0.706300f, 0.022000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetArena							(true, 100.0f, 0.316200f, 0.447700f, 7.240000f, 0.330000f, 0.261200f, 0.020000f, 1.018600f, 0.030000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetHangar							(true, 100.0f, 0.316200f, 0.316200f, 10.05000f, 0.230000f, 0.500000f, 0.020000f, 1.256000f, 0.030000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetCarpetedHallway				(true, 100.0f, 0.316200f, 0.010000f, 0.300000f, 0.100000f, 0.121500f, 0.002000f, 0.153100f, 0.030000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetHallway						(true, 100.0f, 0.316200f, 0.707900f, 1.490000f, 0.590000f, 0.245800f, 0.007000f, 1.661500f, 0.011000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetStoneCorridor				(true, 100.0f, 0.316200f, 0.761200f, 2.700000f, 0.790000f, 0.247200f, 0.013000f, 1.575800f, 0.020000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetAlley							(true, 100.0f, 0.316200f, 0.732800f, 1.490000f, 0.860000f, 0.250000f, 0.007000f, 0.995400f, 0.011000f, 0.300000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetForest							(true, 100.0f, 0.316200f, 0.022400f, 1.490000f, 0.540000f, 0.052500f, 0.162000f, 0.768200f, 0.088000f, 0.300000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetCity							(true, 100.0f, 0.316200f, 0.398100f, 1.490000f, 0.670000f, 0.073000f, 0.007000f, 0.142700f, 0.011000f, 0.500000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetMountains						(true, 100.0f, 0.316200f, 0.056200f, 1.490000f, 0.210000f, 0.040700f, 0.300000f, 0.191900f, 0.100000f, 0.270000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetQuarry							(true, 100.0f, 0.316200f, 0.316200f, 1.490000f, 0.830000f, 0.000000f, 0.061000f, 1.778300f, 0.025000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetPlain							(true, 100.0f, 0.316200f, 0.100000f, 1.490000f, 0.500000f, 0.058500f, 0.179000f, 0.108900f, 0.100000f, 0.210000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetParkingLot					(true, 100.0f, 0.316200f, 1.000000f, 1.650000f, 1.500000f, 0.208200f, 0.008000f, 0.265200f, 0.012000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetSewerPipe						(true, 100.0f, 0.316200f, 0.316200f, 2.810000f, 0.140000f, 1.638700f, 0.014000f, 3.247100f, 0.021000f, 0.800000f, 0.600000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetUnderwater					(true, 100.0f, 0.316200f, 0.010000f, 1.490000f, 0.100000f, 0.596300f, 0.007000f, 7.079500f, 0.011000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetSmallRoom						(true, 100.0f, 0.316200f, 0.501000f, 1.100000f, 0.830000f, 0.630500f, 0.005000f, 1.780000f, 0.011000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetMediumRoom					(true, 100.0f, 0.316200f, 0.501000f, 1.300000f, 0.830000f, 0.316000f, 0.010000f, 0.794100f, 0.020000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetLargeRoom						(true, 100.0f, 0.316200f, 0.501000f, 1.500000f, 0.830000f, 0.158400f, 0.020000f, 0.316200f, 0.040000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetMedumHall						(true, 100.0f, 0.316200f, 0.501000f, 1.800000f, 0.700000f, 0.223800f, 0.015000f, 0.397700f, 0.030000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetLargeHall						(true, 100.0f, 0.316200f, 0.501000f, 1.800000f, 0.700000f, 0.100000f, 0.030000f, 0.199500f, 0.060000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetPlate							(true, 100.0f, 0.316200f, 0.501000f, 1.300000f, 0.900000f, 1.000000f, 0.002000f, 1.000000f, 0.010000f, 1.000000f, 0.750000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetDrugged						(true, 100.0f, 0.316200f, 1.000000f, 8.390000f, 1.390000f, 0.876000f, 0.002000f, 3.108100f, 0.030000f, 0.500000f, 0.428700f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetDizzy							(true, 100.0f, 0.316200f, 0.631000f, 17.23000f, 0.560000f, 0.139200f, 0.020000f, 0.493700f, 0.030000f, 0.600000f, 0.364500f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetPsychotic						(true, 100.0f, 0.316200f, 0.840400f, 7.560000f, 0.910000f, 0.486400f, 0.020000f, 2.437800f, 0.030000f, 0.500000f, 0.062500f, 0.994300f, 0.000000);

// Sports Presets
const EFFECT_DESC EFFECT_DESC::kPresetSportEmptyStadium			(true, 100.0f, 0.316200f, 0.446700f, 6.260000f, 0.510000f, 0.063100f, 0.183000f, 0.398100f, 0.038000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetSportFullStadium			(true, 100.0f, 0.316200f, 0.070800f, 5.250000f, 0.170000f, 0.100000f, 0.188000f, 0.281800f, 0.038000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetSportSquashCourt			(true, 100.0f, 0.316200f, 0.316200f, 2.220000f, 0.910000f, 0.446700f, 0.007000f, 0.794300f, 0.011000f, 0.750000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetSportSmallSwimmingPool	(true, 100.0f, 0.316200f, 0.794300f, 2.760000f, 1.250000f, 0.631000f, 0.020000f, 0.794300f, 0.030000f, 0.700000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetSportLargeSwimmingPool	(true, 100.0f, 0.316200f, 0.794300f, 5.490000f, 1.310000f, 0.446700f, 0.039000f, 0.501200f, 0.049000f, 0.820000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetSportGymnasium				(true, 100.0f, 0.316200f, 0.446700f, 3.140000f, 1.060000f, 0.398100f, 0.029000f, 0.562300f, 0.045000f, 0.810000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetSportStadiumTannoy			(true, 100.0f, 0.316200f, 0.562300f, 2.530000f, 0.880000f, 0.281800f, 0.230000f, 0.501200f, 0.063000f, 0.780000f, 1.000000f, 0.994300f, 0.000000);

// Prefab Presets
const EFFECT_DESC EFFECT_DESC::kPresetPrefabWorkshop				(true, 100.0f, 0.316200f, 0.141300f, 0.760000f, 1.000000f, 1.000000f, 0.012000f, 1.122000f, 0.012000f, 1.000000f, 0.428700f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetPrefabSchoolRoom			(true, 100.0f, 0.316200f, 0.631000f, 0.980000f, 0.450000f, 1.412500f, 0.017000f, 1.412500f, 0.015000f, 0.690000f, 0.402200f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetPrefabPractiseRoom			(true, 100.0f, 0.316200f, 0.398100f, 1.120000f, 0.560000f, 1.258900f, 0.010000f, 1.412500f, 0.011000f, 0.870000f, 0.402200f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetPrefabOuthouse				(true, 100.0f, 0.316200f, 0.112200f, 1.380000f, 0.380000f, 0.891300f, 0.024000f, 0.631000f, 0.044000f, 0.820000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetPrefabCaravan				(true, 100.0f, 0.316200f, 0.089100f, 0.430000f, 1.500000f, 1.000000f, 0.012000f, 1.995300f, 0.012000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);

// Mood Presets
const EFFECT_DESC EFFECT_DESC::kPresetMoodHeaven					(true, 100.0f, 0.316200f, 0.794300f, 5.040000f, 1.120000f, 0.242700f, 0.020000f, 1.258900f, 0.029000f, 0.940000f, 1.000000f, 0.997700f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetMoodHell						(true, 100.0f, 0.316200f, 0.354800f, 3.570000f, 0.490000f, 0.000000f, 0.020000f, 1.412500f, 0.030000f, 0.570000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetMoodMemory					(true, 100.0f, 0.316200f, 0.631000f, 4.060000f, 0.820000f, 0.039800f, 0.000000f, 1.122000f, 0.000000f, 0.850000f, 1.000000f, 0.988600f, 0.000000);

// Driving Presets
const EFFECT_DESC EFFECT_DESC::kPresetDrivingCommentator			(true, 100.0f, 0.316200f, 0.562300f, 2.420000f, 0.880000f, 0.199500f, 0.093000f, 0.251200f, 0.017000f, 0.000000f, 1.000000f, 0.988600f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetDrivingPitGarage			(true, 100.0f, 0.316200f, 0.707900f, 1.720000f, 0.930000f, 0.562300f, 0.000000f, 1.258900f, 0.016000f, 0.590000f, 0.428700f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetDrivingInCarRacer			(true, 100.0f, 0.316200f, 1.000000f, 0.170000f, 2.000000f, 1.778300f, 0.007000f, 0.707900f, 0.015000f, 0.800000f, 0.083200f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetDrivingInCarSports			(true, 100.0f, 0.316200f, 0.631000f, 0.170000f, 0.750000f, 1.000000f, 0.010000f, 0.562300f, 0.000000f, 0.800000f, 0.083200f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetDrivingInCarLuxury			(true, 100.0f, 0.316200f, 0.100000f, 0.130000f, 0.410000f, 0.794300f, 0.010000f, 1.584900f, 0.010000f, 1.000000f, 0.256000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetDrivingFullStand			(true, 100.0f, 0.316200f, 0.281800f, 3.010000f, 1.370000f, 0.354800f, 0.090000f, 0.177800f, 0.049000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetDrivingEmptyStand		(true, 100.0f, 0.316200f, 1.000000f, 4.620000f, 1.750000f, 0.208200f, 0.090000f, 0.251200f, 0.049000f, 1.000000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetDrivingTunnel				(true, 100.0f, 0.316200f, 0.398100f, 3.420000f, 0.940000f, 0.707900f, 0.051000f, 0.707900f, 0.047000f, 0.810000f, 1.000000f, 0.994300f, 0.000000);

// Misc. Presets
const EFFECT_DESC EFFECT_DESC::kPresetDustyRoom						(true, 100.0f, 0.316200f, 0.794300f, 1.790000f, 0.380000f, 0.501200f, 0.002000f, 1.258900f, 0.006000f, 0.560000f, 0.364500f, 0.988600f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetChapel							(true, 100.0f, 0.316200f, 0.562300f, 4.620000f, 0.640000f, 0.446700f, 0.032000f, 0.794300f, 0.049000f, 0.840000f, 1.000000f, 0.994300f, 0.000000);
const EFFECT_DESC EFFECT_DESC::kPresetSmallWaterRoom				(true, 100.0f, 0.316200f, 0.447700f, 1.510000f, 1.250000f, 0.891300f, 0.020000f, 1.412500f, 0.030000f, 0.700000f, 1.000000f, 0.992000f, 0.000000);



EFFECT_DESC::EFFECT_DESC(bool bEnabled, FXD::F32 fWetDryMix, FXD::F32 fRoom, FXD::F32 fRoomHF, FXD::F32 fDecayTime, FXD::F32 fDecayHFRatio, FXD::F32 fReflections, FXD::F32 fReflectionsDelay, FXD::F32 fReverb, FXD::F32 fReverbDelay, FXD::F32 fDiffusion, FXD::F32 fDensity, FXD::F32 fAirAbsorption, FXD::F32 fRoomRolloffFactor)
	: Enabled(bEnabled)
	, WetDryMix(fWetDryMix)
	, Volume(1.0f)
	, Gain(fRoom)
	, GainHF(fRoomHF)
	, DecayTime(fDecayTime)
	, DecayHFRatio(fDecayHFRatio)
	, ReflectionsGain(fReflections)
	, ReflectionsDelay(fReflectionsDelay)
	, LateGain(fReverb)
	, LateDelay(fReverbDelay)
	, Diffusion(fDiffusion)
	, Density(fDensity)
	, AirAbsorptionGainHF(fAirAbsorption)
	, RoomRolloffFactor(fRoomRolloffFactor)
{
}

// ------
// IEffect
// -
// ------
IEffect::IEffect(const EFFECT_DESC& effectDesc)
	: m_bDestroyed(false)
	, m_effectDesc(effectDesc)
{
}
IEffect::~IEffect(void)
{
}

// ------
// IEffectChain
// -
// ------
bool IEffectChain::FromStream(Core::StreamIn& stream, SND::IEffectChain* pEffectChain)
{
	if (stream)
	{
		EffectChainTagCB callback(pEffectChain);
		IO::StreamParser streamParser(stream);
		IO::XMLSaxParser::parse(streamParser, callback);
		return true;
	}
	return false;
}

IEffectChain::IEffectChain(const SND::ISndAdapter* pSndAdapter, SND::INodeMixer* pNodeMixer)
	: m_pSndAdapter(pSndAdapter)
	, m_pNodeMixer(pNodeMixer)
	, m_bNeedsUpdate(false)
	, m_bNeedsRecreate(false)
{
}

IEffectChain::~IEffectChain(void)
{
}

bool IEffectChain::create_effect_chain(void)
{
	auto funcRef = [&](void) { return _create_effect_chain(); };

	return Job::Async(FXDSND(), [=]() { return funcRef(); }).get();
}

bool IEffectChain::release_effect_chain(void)
{
	auto funcRef = [&](void) { return _release_effect_chain(); };

	return Job::Async(FXDSND(), [=]() { return funcRef(); }).get();
}

void IEffectChain::add_effects(const Container::Vector< SND::EFFECT_DESC >& effectDescs)
{
	auto funcRef = [&](const Container::Vector< SND::EFFECT_DESC >& effectDefs) { return _add_effects(effectDescs); };

	return Job::Async(FXDSND(), [=]() { return funcRef(effectDescs); }).get();
}

void IEffectChain::add_effect(const SND::EFFECT_DESC& effectDesc)
{
	Container::Vector< SND::EFFECT_DESC > effects;
	effects.push_back(effectDesc);
	add_effects(effects);
}

void IEffectChain::remove_effects(void)
{
	auto funcRef = [&](void) { _remove_effects(); };

	Job::Async(FXDSND(), [=]() { funcRef(); });
}

void IEffectChain::remove_effect(FXD::U16 nSlot)
{
	auto funcRef = [&](FXD::U16 nSlot) { _remove_effect(nSlot); };

	Job::Async(FXDSND(), [=]() { funcRef(nSlot); }).get();
}

void IEffectChain::set_effect(const SND::EFFECT_DESC& effectDesc, FXD::U16 nSlot)
{
	auto funcRef = [&](const SND::EFFECT_DESC& effectDef, FXD::U16 nSlot) { _set_effect(effectDesc, nSlot); };

	Job::Async(FXDSND(), [=]() { funcRef(effectDesc, nSlot); }).get();
}

void IEffectChain::enable_effect(FXD::U16 nSlot, bool bEnable)
{
	auto funcRef = [&](FXD::U16 nSlot, bool bEnable) { _set_effect(nSlot, bEnable); };

	Job::Async(FXDSND(), [=]() { funcRef(nSlot, bEnable); }).get();
}

bool IEffectChain::is_effect_enabled(FXD::U16 nSlot) const
{
	auto funcRef = [&](FXD::U16 nSlot) { return _is_effect_enabled(nSlot); };

	return Job::Async(FXDSND(), [=]() { return funcRef(nSlot); }).get();
}

void IEffectChain::_remove_effects(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	fxd_for(auto& effect, m_effects)
	{
		effect->destroyed(true);
	}
	m_bNeedsRecreate = true;
}

void IEffectChain::_remove_effect(FXD::U16 nSlot)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	m_effects[nSlot]->destroyed(true);
	m_bNeedsRecreate = true;
}

void IEffectChain::_set_effect(const SND::EFFECT_DESC& effectDesc, FXD::U16 nSlot)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	m_effects[nSlot]->effect_desc() = effectDesc;
	m_bNeedsUpdate = true;
}