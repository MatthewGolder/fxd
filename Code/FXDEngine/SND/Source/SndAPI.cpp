// Creator - MatthewGolder
#include "FXDEngine/SND/SndApi.h"

using namespace FXD;
using namespace SND;

// ------
// ISndApi
// -
// ------
ISndApi::ISndApi(SND::E_SndApi eSndApi)
	: IApi(App::E_Api::SND)
	, m_eSndApi(eSndApi)
{
}

ISndApi::~ISndApi(void)
{
}

// ------
// Funcs
// -
// ------
const Core::StringView8 sSndApis[] =
{
	"Default",	// E_SndApi::Default
	"OpenAL",	// E_SndApi::OpenAL
	"OpenSL",	// E_SndApi::OpenSL
	"XAudio2",	// E_SndApi::XAudio2
	"Count"		// E_SndApi::Count
};

const Core::StringView8 FXD::SND::Funcs::ToString(const SND::E_SndApi eApi)
{
	return sSndApis[FXD::STD::to_underlying(eApi)];
}

SND::E_SndApi FXD::SND::Funcs::ToApiType(const Core::StringView8 strType)
{
	for (FXD::S32 n1 = 0; n1 < FXD::STD::to_underlying(SND::E_SndApi::Count); ++n1)
	{
		if (strType.compare_no_case(sSndApis[n1]) == 0)
		{
			return (SND::E_SndApi)n1;
		}
	}
	return SND::E_SndApi::Unknown;
}