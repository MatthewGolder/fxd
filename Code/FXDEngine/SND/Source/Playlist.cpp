// Creator - MatthewGolder
#include "FXDEngine/SND/Playlist.h"
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/SndNode.h"
#include "FXDEngine/SND/SndSystem.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/IO/XML/XMLSaxParser.h"

#include <algorithm>

using namespace FXD;
using namespace SND;

// ------
// PlaylistTagCB
// -
// ------
class PlaylistTagCB FINAL : public IO::XMLSaxParser::ITagCB
{
public:
	enum class E_Tag
	{
		Playlist = 0,
		Track,
		Count,
		Unknown = 0xffff
	};

public:
	PlaylistTagCB(SND::Playlist& sndPlaylist);
	~PlaylistTagCB(void);

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL;
	void end_tag(const Core::String8& strName) FINAL;
	void handle_text(const Core::String8& strText, const IO::StreamParser::Position& pos) FINAL;
	void handle_attribute(const Core::String8& strName, const Core::String8& strValue) FINAL;

	void creates(SND::SndNode& parentNode);

protected:
	E_Tag m_eTag;
	SND::Playlist& m_sndPlaylist;
	Container::Vector< PlaylistTrack > m_tracks;
};

PlaylistTagCB::PlaylistTagCB(SND::Playlist& sndPlaylist)
	: m_eTag(E_Tag::Unknown)
	, m_sndPlaylist(sndPlaylist)
{
}

PlaylistTagCB::~PlaylistTagCB(void)
{
}

void PlaylistTagCB::handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB)
{
	pTagCB = this;
	switch (m_eTag)
	{
		case E_Tag::Unknown:
		{
			if (strName == Core::String8("Playlist"))
				m_eTag = E_Tag::Playlist;
			break;
		}
		case E_Tag::Playlist:
		{
			if (strName == Core::String8("Track"))
			{
				m_tracks.push_back(SND::PlaylistTrack());
				m_eTag = E_Tag::Track;
			}
			break;
		}
		default:
		{
			PRINT_ASSERT << "SND: Unknown XML Tag in PlaylistTagCB";
			break;
		}
	}
}

void PlaylistTagCB::end_tag(const Core::String8& strName)
{
	switch (m_eTag)
	{
		case E_Tag::Playlist:
		{
			m_eTag = E_Tag::Unknown;
			break;
		}
		case E_Tag::Track:
		{
			if (m_sndPlaylist)
				m_sndPlaylist->add_track(m_tracks.back());
			m_eTag = E_Tag::Playlist;
			break;
		}
		default:
		{
			break;
		}
	}
}

void PlaylistTagCB::handle_text(const Core::String8& /*strText*/, const IO::StreamParser::Position& /*pos*/)
{
}

void PlaylistTagCB::handle_attribute(const Core::String8& strName, const Core::String8& strValue)
{
	switch (m_eTag)
	{
		case E_Tag::Playlist:
		{
			if (strName == Core::String8("disabled_genres"))
			{
				Container::Vector< Core::String8 > genres = strValue.to_str_array(UTF8('|'));
				fxd_for(const auto& genre, genres)
				{
					m_sndPlaylist->disable_genre(genre);
				}
			}
			break;
		}
		case E_Tag::Track:
		{
			if (strName == Core::String8("filename"))
			{
				m_tracks.back().trackName(strValue);
			}
			else if (strName == Core::String8("streamed"))
			{
				m_tracks.back().streamed(strValue.to_bool());
			}
			else if (strName == Core::String8("genres"))
			{
				Container::Vector< Core::String8 > genres = strValue.to_str_array(UTF8('|'));
				m_tracks.back().add_genres(genres);
			}
			break;
		}
		default:
		{
			break;
		}
	}
}

// ------
// IPlaylist
// - 
// ------
PlaylistTrack::PlaylistTrack(const Core::String8& strTrackName, bool bStreamed)
	: m_bStreamed(bStreamed)
	, m_strTrackName(strTrackName)
{
}

PlaylistTrack::~PlaylistTrack(void)
{
}

void PlaylistTrack::add_genres(const Container::Vector< Core::String8 >& genres)
{
	fxd_for(const auto& genre, genres)
	{
		add_genre(genre);
	}
}

void PlaylistTrack::add_genre(const Core::String8& strGenre)
{
	if (contains_genre(strGenre) == false)
	{
		m_genres.push_back(strGenre);
	}
}

void PlaylistTrack::remove_genre(const Core::String8& strGenre)
{
	m_genres.find_erase(strGenre);
}

void PlaylistTrack::clear_genres(void)
{
	m_genres.clear();
}

bool PlaylistTrack::contains_genre(const Core::String8& strGenre) const
{
	bool bRetVal = m_genres.contains(strGenre);
	return bRetVal;
}


// ------
// IPlaylist::_Data
// - 
// ------
IPlaylist::_Data::_Data(const IO::Path& ioPath, const SND::NodeMixer& sndNode)
	: m_sndNode(sndNode)
	, m_ioPath(ioPath)
	, m_eState(E_VoiceState::Stopped)
	, m_nIndex(0)
{
}

IPlaylist::_Data::~_Data(void)
{
}


// ------
// IPlaylist
// - 
// ------
SND::Playlist IPlaylist::FromStream(SND::ISndAdapter* pSndAdapter, Core::StreamIn& stream, const IO::Path& ioPath, const SND::NodeMixer& sndNode)
{
	if (stream)
	{
		SND::Playlist playlist = std::make_shared< SND::IPlaylist >(pSndAdapter, ioPath.parent_path(), sndNode);

		PlaylistTagCB callback(playlist);
		IO::StreamParser streamParser(stream);
		IO::XMLSaxParser::parse(streamParser, callback);
		return playlist;
	}
	return SND::Playlist();
}

IPlaylist::IPlaylist(SND::ISndAdapter* pSndAdapter, const IO::Path& ioPath, const SND::NodeMixer& sndNode)
	: m_pSndAdapter(pSndAdapter)
	, m_playlistData(ioPath, sndNode)
{
}

IPlaylist::~IPlaylist(void)
{
}

void IPlaylist::play(void)
{
	Thread::CSLockData< _Data >::LockGuard lock(m_playlistData);

	if (lock->m_currentVoice)
	{
		lock->m_currentVoice->play();
	}
	lock->m_eState = E_VoiceState::Playing;
}

void IPlaylist::pause(void)
{
	Thread::CSLockData< _Data >::LockGuard lock(m_playlistData);

	if (lock->m_currentVoice)
	{
		lock->m_currentVoice->pause();
	}
	lock->m_eState = E_VoiceState::Paused;
}

void IPlaylist::stop(void)
{
	Thread::CSLockData< _Data >::LockGuard lock(m_playlistData);

	if (lock->m_currentVoice)
	{
		lock->m_currentVoice->stop();
		lock->m_currentVoice = SND::NodeVoice();
	}
	lock->m_eState = E_VoiceState::Stopped;
}

bool IPlaylist::is_playing(void) const
{
	return (m_playlistData.get_quick_read_lock()->m_eState == E_VoiceState::Playing);
}

void IPlaylist::add_track(const SND::PlaylistTrack& track)
{
	Thread::CSLockData< _Data >::LockGuard lock(m_playlistData);

	fxd_for(const auto& track, lock->m_trackList)
	{
		if (track.trackName() == track.trackName())
		{
			return;
		}
	}
	lock->m_trackList.push_back(track);
}

void IPlaylist::add_tracks(const Container::Vector< SND::PlaylistTrack >& tracks)
{
	fxd_for(const auto& track, tracks)
	{
		add_track(track);
	}
}

void IPlaylist::skip_track(void)
{
	Thread::CSLockData< _Data >::LockGuard lock(m_playlistData);

	E_VoiceState eState = lock->m_eState;

	stop();
	lock->m_nIndex = _find_next_track_index();

	lock->m_eState = eState;
}

void IPlaylist::prev_track(void)
{
	Thread::CSLockData< _Data >::LockGuard lock(m_playlistData);

	E_VoiceState eState = lock->m_eState;

	stop();
	lock->m_nIndex = _find_previous_track_index();

	lock->m_eState = eState;
}

void IPlaylist::clear_tracks(void)
{
	Thread::CSLockData< _Data >::LockGuard lock(m_playlistData);

	stop();
	lock->m_trackList.clear();
}

void IPlaylist::shuffle_tracks(void)
{
	Thread::CSLockData< _Data >::LockGuard lock(m_playlistData);

	if (!lock->m_trackList.empty())
	{
		//FXD::STD::random_shuffle(lock->m_trackList.first(), lock->m_trackList.last());
	}
}


void IPlaylist::enable_genre(const Core::String8& strGenre)
{
	Thread::CSLockData< _Data >::LockGuard lock(m_playlistData);

	bool bExisted = lock->m_disabledGenres.find_erase(strGenre);
}

void IPlaylist::disable_genre(const Core::String8& strGenre)
{
	Thread::CSLockData< _Data >::LockGuard lock(m_playlistData);

	E_VoiceState eState = lock->m_eState;
	bool bExists = lock->m_disabledGenres.contains(strGenre);
	if (!bExists)
	{
		lock->m_disabledGenres.push_back(strGenre);
		if (lock->m_trackList.not_empty())
		{
			if (_is_current_track_disabled())
			{
				stop();
			}
		}
		lock->m_eState = eState;
	}
}

void IPlaylist::_process(FXD::F32 /*dt*/)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	Thread::CSLockData< _Data >::LockGuard lock(m_playlistData);

	if (is_playing())
	{
		if (lock->m_currentVoice)
		{
			if (!lock->m_currentVoice->is_finished())
			{
				return; // We are playing the current track so do nothing and return.
			}
			else
			{
				// We are finished playing the current track so stop it and find the next one.
				stop();
				lock->m_eState = E_VoiceState::Playing;
				lock->m_nIndex = _find_next_track_index();
			}
		}

		if (lock->m_trackList.not_empty())
		{
			if (_is_current_track_disabled())
			{
				lock->m_nIndex = _find_next_track_index(); // The current track has a disabled genre so find the next one.
			}
			else
			{
				IO::Path path = (lock->m_ioPath / lock->m_trackList[lock->m_nIndex].trackName().c_str());

				auto audio = FXDSND()->snd_system()->load_audio(path).get();
				SND::E_CreateFlags eFlags;
				if (lock->m_trackList[lock->m_nIndex].streamed())
				{
					eFlags |= SND::E_CreateFlag::Streamed;
				}

				SND::NODE_VOICE_DESC nodeVoiceDesc = {};
				nodeVoiceDesc.Flags = eFlags;
				nodeVoiceDesc.Audio = audio;
				lock->m_currentVoice = lock->m_sndNode->add_voice(nodeVoiceDesc);
				lock->m_currentVoice->play();
			}
		}
	}
}

FXD::U16 IPlaylist::_find_next_track_index(void) const
{
	Thread::CSLockData< _Data >::LockGuard lock(m_playlistData);

	FXD::U16 nRetIndex = lock->m_nIndex;
	while (true)
	{
		nRetIndex++;
		if (nRetIndex >= lock->m_trackList.size())
		{
			nRetIndex = 0;
		}
		if (lock->m_nIndex == nRetIndex)
		{
			break;
		}
		if (_is_track_disabled(lock->m_trackList[nRetIndex]) == false)
		{
			break;
		}
	}
	return nRetIndex;
}

FXD::U16 IPlaylist::_find_previous_track_index(void) const
{
	Thread::CSLockData< _Data >::LockGuard lock(m_playlistData);

	FXD::U16 nRetIndex = lock->m_nIndex;
	while (true)
	{
		nRetIndex--;
		if (nRetIndex < 0)
		{
			if (lock->m_trackList.empty())
			{
				nRetIndex = 0;
			}
			else
			{
				nRetIndex = (lock->m_trackList.size() - 1);
			}
		}
		if (lock->m_nIndex == nRetIndex)
		{
			break;
		}
		if (!_is_track_disabled(lock->m_trackList[nRetIndex]))
		{
			break;
		}
	}
	return nRetIndex;
}

bool IPlaylist::_is_current_track_disabled(void) const
{
	Thread::CSLockData< _Data >::LockGuard lock(m_playlistData);

	PRINT_COND_ASSERT((lock->m_nIndex < lock->m_trackList.size()), "SND: Attempting to access invalid track index");
	return _is_track_disabled(lock->m_trackList[lock->m_nIndex]);
}

bool IPlaylist::_is_track_disabled(const PlaylistTrack& track) const
{
	Thread::CSLockData< _Data >::LockGuard lock(m_playlistData);

	fxd_for(const auto& genre, lock->m_disabledGenres)
	{
		if (track.contains_genre(genre))
		{
			return true;
		}
	}
	return false;
}