// Creator - MatthewGolder
#include "FXDEngine/SND/SndAdapter.h"
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/SndLoopPoint.h"
#include "FXDEngine/SND/SndNode.h"
#include "FXDEngine/SND/Listener.h"
#include "FXDEngine/SND/Playlist.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/IO/IODevice.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOSystem.h"
#include "FXDEngine/Job/Async.h"
#include "FXDEngine/Memory/StreamMemHandle.h"

using namespace FXD;
using namespace SND;

// ------
// ISndAdapter
// - 
// ------
ISndAdapter::ISndAdapter(const App::AdapterDesc& adapterDesc, const App::IApi* pApi)
	: IAdapter(adapterDesc, pApi)
{
}

ISndAdapter::~ISndAdapter(void)
{
}

Job::Future< bool > ISndAdapter::create(void)
{
	auto funcRef = [&](void) { return _create(); };

	return Job::Async(FXDSND(), [=]() { return funcRef(); });
}

Job::Future< bool > ISndAdapter::release(void)
{
	auto funcRef = [&](void) { return _release(); };

	return Job::Async(FXDSND(), [=]() { return funcRef(); });
}

Job::Future< SND::SndLoopGroup > ISndAdapter::load_loop_points(const IO::Path& ioPath)
{
	auto funcRef = [&](const IO::Path& ioPath) { return _load_loop_points(ioPath); };

	return Job::Async(FXDAsync(), [=]() { return funcRef(ioPath); });
}

Job::Future< SND::SndLoopGroup > ISndAdapter::load_loop_points_stream(Core::StreamIn& stream)
{
	auto funcRef = [&](Core::StreamIn stream) { return _load_loop_points_stream(stream); };

	return Job::Async(FXDAsync(), [=]() { return funcRef(stream); });
}

Job::Future< SND::Playlist > ISndAdapter::load_playlist(const IO::Path& ioPath, const SND::NodeMixer& nodeMixer)
{
	auto funcRef = [&](const IO::Path& ioPath, const SND::NodeMixer& nodeMixer) { return _load_playlist(ioPath, nodeMixer); };

	return Job::Async(FXDAsync(), [=]() { return funcRef(ioPath, nodeMixer); });
}

Job::Future< SND::Playlist > ISndAdapter::load_playlist_stream(Core::StreamIn& stream, const IO::Path& ioPath, const SND::NodeMixer& nodeMixer)
{
	auto funcRef = [&](Core::StreamIn stream, const IO::Path& ioPath, const SND::NodeMixer& nodeMixer) { return _load_playlist_stream(stream, ioPath, nodeMixer); };

	return Job::Async(FXDAsync(), [=]() { return funcRef(stream, ioPath, nodeMixer); });
}

Job::Future< void > ISndAdapter::register_playlist(const SND::Playlist& playlist)
{
	auto funcRef = [&](const SND::Playlist& playlist) { _register_playlist(playlist); };

	return Job::Async(FXDSND(), [=]() { return funcRef(playlist); });
}

Job::Future< void > ISndAdapter::unregister_playlist(const SND::Playlist& playlist)
{
	auto funcRef = [&](const SND::Playlist& playlist) { _unregister_playlist(playlist); };

	return Job::Async(FXDSND(), [=]() { return funcRef(playlist); });
}

bool ISndAdapter::is_vaild(void) const
{
	auto funcRef = [&](void) { return _is_vaild(); };

	return Job::Async(FXDSND(), [=]() { return funcRef(); }).get();
}

void ISndAdapter::_update(FXD::F32 dt)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	_update_listener(dt);
	_update_nodes(dt);
	_update_playlists(dt);
	_update_priorities(dt);
}

void ISndAdapter::_update_listener(FXD::F32 dt)
{
	if (m_listener)
	{
		m_listener->_update(dt);
	}
}

void ISndAdapter::_update_nodes(FXD::F32 dt)
{
	if (m_masterNode)
	{
		m_masterNode->_update(dt, m_listener);
	}
}

void ISndAdapter::_update_playlists(FXD::F32 dt)
{
	fxd_for(auto& playlist, m_playlists)
	{
		playlist->_process(dt);
	}
}
void ISndAdapter::_update_priorities(FXD::F32 /*dt*/)
{
	m_voices.sort([](auto& lhs, auto& rhs)
	{
		return ((*lhs).get_importance() > (*rhs).get_importance());
	});

	/*
	SND::SndAdapterDesc sndDesc = std::dynamic_pointer_cast< ISndAdapterDesc >(m_adapterDesc);

	for (FXD::U32 n1 = sndDesc->max_sources(); n1 < m_voices.size(); n1++)
	{
		if (m_voices[n1]->is_valid())
		{
			//float f = 0;
			//NodeHelper::shutdownVoiceNode(m_voices[n1]);
		}
	}
	for (FXD::U32 n1 = 0; n1 < m_voices.size() && n1 < sndDesc->max_sources(); n1++)
	{
		if ((!m_voices[n1]->is_valid()) && (m_voices[n1]->is_playing()))
		{
			//float f = 0;
			//NodeHelper::createVoiceNode(this, m_voices[n1], m_voices[n1]->audioVoice()->m_voiceDesc.Audio->getSourceFormat());
			//NodeHelper::linkNodes(this, m_voices[n1]->getPatentMixer(), m_voices[n1]);
		}
	}
	*/
}

SND::SndLoopGroup ISndAdapter::_load_loop_points(const IO::Path& ioPath)
{
	Core::StreamIn stream = FXD::FXDIO()->io_system()->io_device()->file_open_memory_stream(ioPath, false, { }).get();
	return _load_loop_points_stream(stream);
}

SND::SndLoopGroup ISndAdapter::_load_loop_points_stream(Core::StreamIn stream)
{
	return SND::ISndLoopGroup::FromStream(stream);
}

SND::Playlist ISndAdapter::_load_playlist(const IO::Path& ioPath, const SND::NodeMixer& nodeMixer)
{
	Core::StreamIn stream = FXD::FXDIO()->io_system()->io_device()->file_open_memory_stream(ioPath, false, { }).get();
	return _load_playlist_stream(stream, ioPath, nodeMixer);
}

SND::Playlist ISndAdapter::_load_playlist_stream(Core::StreamIn& stream, const IO::Path& ioPath, const SND::NodeMixer& nodeMixer)
{
	return IPlaylist::FromStream(this, stream, ioPath, nodeMixer);
}

void ISndAdapter::_register_playlist(const SND::Playlist& playlist)
{
	if (!playlist)
	{
		return;
	}

	auto itr = m_playlists.find(playlist);
	if (itr == m_playlists.end())
	{
		m_playlists.emplace_back(playlist);
	}
	return;
}

void ISndAdapter::_unregister_playlist(const SND::Playlist& playlist)
{
	if (playlist)
	{
		playlist->stop();
		m_playlists.find_erase(playlist);
	}
}