// Creator - MatthewGolder
#include "FXDEngine/SND/SndAdapter.h"
#include "FXDEngine/SND/SndSystem.h"
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/Decoders/AudioOgg.h"
#include "FXDEngine/SND/Decoders/AudioWav.h"
#include "FXDEngine/App/FXDApi.h"
#include "FXDEngine/Core/Algorithm.h"
#include "FXDEngine/Core/WatchDogThread.h"
#include "FXDEngine/IO/IODevice.h"
#include "FXDEngine/IO/IOJPThread.h"
#include "FXDEngine/IO/IOSystem.h"
#include "FXDEngine/Job/Async.h"
#include "FXDEngine/Math/Math.h"
#include "FXDEngine/Memory/StreamMemHandle.h"

using namespace FXD;
using namespace SND;

SND::Audio AudioLoadFunc(const IO::Path& ioPath)
{
	// 1. Open the audio file.
	auto futStream = FXDIO()->io_system()->io_device()->file_open_memory_stream(ioPath, false, { });

	auto futAudio = futStream.then(FXDSND(), [&](auto& fut)
	{
		Core::String8 strFileName = ioPath.filename();
		Core::StreamIn stream = fut.get();

		// 2. Parse the audio file.
		SND::Audio audio;
		if (strFileName.ends_with_no_case(UTF_8(".ogg")))
		{
			audio = std::make_shared< SND::IAudioOgg >(stream, strFileName);
		}
		else if (strFileName.ends_with_no_case(UTF_8(".wav")))
		{
			audio = std::make_shared< SND::IAudioWav >(stream, strFileName);
		}
		else
		{
			PRINT_WARN << "SND: Unknown file format tag";
		}
		return audio;
	});

	return futAudio.get();
}

// ------
// ISndSystem
// - 
// ------
ISndSystem::ISndSystem(void)
	: m_sndApi(nullptr)
{
}

ISndSystem::~ISndSystem(void)
{
	PRINT_COND_ASSERT((m_sndApi == nullptr), "SND: m_sndApi is not shutdown");
	PRINT_COND_ASSERT((m_sndAdapters.size() == 0), "SND: m_sndAdapters are not shutdown");
}

Job::Future< bool > ISndSystem::create_system(void)
{
	auto funcRef = [&](void) { return _create_system(); };

	return Job::Async(FXDSND(), [=]() { return funcRef(); });
}

Job::Future< bool > ISndSystem::release_system(void)
{
	auto funcRef = [&](void) { return _release_system(); };

	return Job::Async(FXDSND(), [=]() { return funcRef(); });
}

Job::Future< SND::SndAdapter > ISndSystem::create_adapter(const App::AdapterDesc& adapterDesc)
{
	auto funcRef = [&](const App::AdapterDesc& adapterDesc) { return _create_adapter(adapterDesc); };

	return Job::Async(FXDSND(), [=]() { return funcRef(adapterDesc); });
}

Job::Future< bool > ISndSystem::release_adapter(SND::SndAdapter& sndAdapter)
{
	auto funcRef = [&](SND::SndAdapter sndAdapter) { return _release_adapter(sndAdapter); };

	return Job::Async(FXDSND(), [=]() { return funcRef(sndAdapter); });
}

Job::Future< SND::Audio > ISndSystem::load_audio(const IO::Path& ioPath)
{
	auto funcRef = [&](const IO::Path& ioPath) { return audio_cache().load(ioPath, AudioLoadFunc); };

	return Job::Async(FXDAsync(), [=]() { return funcRef(ioPath); });
}

void ISndSystem::load_audio_async(const IO::Path& ioPath, FXD::STD::function< void(SND::Audio) > callback)
{
	callback(load_audio(ioPath).get());
}

void ISndSystem::_process(FXD::F32 dt)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

#if defined(FXD_ENABLE_WATCHDOG)
	static Core::WatchDogThread sWDThread("Sound - WatchDog", 500);
	sWDThread.increment();
#endif

	fxd_for(auto& sndAdapter, m_sndAdapters)
	{
		sndAdapter->_update(dt);
	}
	m_audioCache.remove_unique();
}

// ------
// Funcs
// -
// ------
FXD::S64 FXD::SND::Funcs::VolumeToDeciBels(FXD::F32 fVolume)
{
	const FXD::S64 nDeciBels = -100;
	if (fVolume > 0.0f)
	{
		return FXD::STD::clamp< FXD::S64 >((FXD::S64)(20.0f * Math::Log10F(fVolume)), nDeciBels, 0);
	}
	return nDeciBels;
}

FXD::S64 FXD::SND::Funcs::VolumeToMilliBels(FXD::F32 fVolume, FXD::S32 nMaxMilliBels)
{
	const FXD::S64 nMilliBels = -10000;
	if (fVolume > 0.0f)
	{
		return FXD::STD::clamp< FXD::S64 >((FXD::S64)(2000.0f * Math::Log10F(fVolume)), nMilliBels, nMaxMilliBels);
	}
	return nMilliBels;
}