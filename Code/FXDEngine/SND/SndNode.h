// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_SNDNODE_H
#define FXDENGINE_SND_SNDNODE_H

#include "FXDEngine/Core/String.h"
#include "FXDEngine/Container/DirtyType.h"
#include "FXDEngine/Container/Map.h"
#include "FXDEngine/IO/Types.h"
#include "FXDEngine/Math/Lerpable.h"
#include "FXDEngine/SND/Types.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// NODE_DESC
		// -
		// ------
		struct NODE_DESC
		{
			Core::String8 Name;
			FXD::Math::LerpF32 Volume = 1.0f;
			FXD::U32 SampleRate = 48000;
			FXD::U32 OutChannels = 2;
			bool Auxiliary = false;
		};

		// ------
		// NODE_VOICE_DESC
		// -
		// ------
		struct NODE_VOICE_DESC
		{
			FXD::F32 Ratio = 1.0f;
			FXD::U8 Priority = Math::NumericLimits< FXD::U8 >::max();
			SND::Audio Audio = SND::Audio();
			SND::SndLoopGroup LoopGroup = SND::SndLoopGroup();
			SND::E_CreateFlags Flags;
		};

		// ------
		// INodeData
		// -
		// ------
		class INodeData : public Core::NonCopyable
		{
		protected:
			INodeData(void);
			virtual ~INodeData(void);
		};

		// ------
		// INode
		// -
		// ------
		class INode : public Core::NonCopyable
		{
		protected:
			EXPLICIT INode(const SND::ISndAdapter* pSndAdapter, const SND::INodeMixer* pNodeParent);
			virtual ~INode(void);

		public:
			void set_volume(FXD::F32 fVolume, FXD::F32 fDuration = 0.0f);
			SND::NODE_DESC get_node_desc(void) const;
			SND::NodeData get_node_data(void) const;
			const SND::INodeMixer* get_node_parent(void) const;

		protected:
			virtual void _update(FXD::F32 dt, const SND::Listener& listener) PURE;
			virtual void _update_volume(FXD::F32 dt) PURE;

			void _set_volume(FXD::F32 fVolume, FXD::F32 fDuration);

			SND::NODE_DESC _get_node_desc(void) const;
			SND::NodeData _get_node_data(void) const;
			const SND::INodeMixer* _get_node_parent(void) const;

		protected:
			const SND::ISndAdapter* m_pSndAdapter;
			const SND::INodeMixer* m_pNodeParent;
			SND::NODE_DESC m_nodeDesc;
			SND::NodeData m_nodeData;
		};

		// ------
		// INodeMixer
		// -
		// ------
		class INodeMixer : public SND::INode
		{
		public:
			friend class ISndAdapter;
			static bool FromStream(Core::StreamIn& stream, SND::INodeMixer* pNodeMaster);

		protected:
			EXPLICIT INodeMixer(const SND::ISndAdapter* pSndAdapter, const SND::INodeMixer* pNodeParent);
			virtual ~INodeMixer(void);

		public:
			void create(const NODE_DESC& nodeDesc);
			void release(void);

			bool load_nodes(const IO::Path& ioPath);
			bool load_nodes_stream(Core::StreamIn& stream);

			bool load_effects(const IO::Path& ioPath);
			bool load_effects_stream(Core::StreamIn& stream);

			SND::NodeMixer add_mixer(const SND::NODE_DESC& nodeDesc);
			SND::NodeVoice add_voice(const SND::NODE_VOICE_DESC& nodeVoiceDesc);

			SND::NodeMixer find_mixer(const Core::String8& strNodeName);

			GET_REF_W(SND::EffectChain, effectChain, effect_chain);
			GET_REF_W(Container::Vector< SND::NodeVoice >, voices, voices);

		protected:
			void _update(FXD::F32 dt, const SND::Listener& listener);
			virtual void _update_volume(FXD::F32 dt) PURE;
			void _remove_finished_voices(void);

			virtual void _create(const SND::NODE_DESC& nodeDesc) PURE;
			virtual void _release(void) PURE;

			bool _load_nodes(const IO::Path& ioPath);
			bool _load_nodes_stream(Core::StreamIn& stream);

			bool _load_effects(const IO::Path& ioPath);
			bool _load_effects_stream(Core::StreamIn& stream);

			SND::NodeMixer _add_mixer(const SND::NODE_DESC& nodeDesc);
			SND::NodeVoice _add_voice(const SND::NODE_VOICE_DESC& nodeVoiceDesc);

			SND::NodeMixer _find_mixer(const Core::String8& strNodeName);

		protected:
			SND::EffectChain m_effectChain;
			Container::Vector< SND::NodeVoice > m_voices;
			Container::Map< Core::String8, SND::NodeMixer > m_mixers;
		};

		// ------
		// INodeVoice
		// -
		// ------
		class INodeVoice : public SND::INode
		{
		public:
			friend class ISndAdapter;
			friend class INodeMixer;

		protected:
			EXPLICIT INodeVoice(const SND::ISndAdapter* pSndAdapter, const SND::INodeMixer* pNodeParent);
			virtual ~INodeVoice(void);

		public:
			void create(const SND::NODE_DESC& nodeDesc, const SND::NODE_VOICE_DESC& nodeVoiceDesc);
			void release(void);

			void play(void);
			void pause(void);
			void stop(void);

			bool is_playing(void) const;
			bool is_paused(void) const;
			bool is_stopped(void) const;
			bool is_finished(void) const;
			bool is_valid(void) const;

			FXD::U32 tell_sample_position(void) const;
			FXD::F32 tell_playback_position(void) const;
			FXD::F32 get_playback_length(void) const;
			Core::String8 tell_playback_position_string(void) const;

			void set_frequency(FXD::F32 fRatio, FXD::F32 fDuration = 0.0f);
			void set_priority(FXD::U8 nPriority);

			FXD::F32 get_frequency(void) const;
			FXD::F32 get_importance(void) const;

			GET_REF_W(SND::Voice, voice, voice);
			GET_REF_W(SND::Emitter, emitter, emitter);

		protected:
			virtual void _create(const SND::NODE_DESC& nodeDesc, const SND::NODE_VOICE_DESC& nodeVoiceDesc) PURE;
			virtual void _release(void) PURE;

			virtual bool _is_valid(void) const PURE;

			void _update(FXD::F32 dt, const SND::Listener& listener);
			virtual void _update_volume(FXD::F32 dt) PURE;
			virtual void _update_frequency(FXD::F32 dt) PURE;
			void _update_emitter(FXD::F32 dt, const SND::Listener& listener);
			void _update_voice(FXD::F32 dt, const SND::Listener& listener);

			void _set_frequency(FXD::F32 fRatio, FXD::F32 fDuration);
			void _set_priority(FXD::U8 nPriority);

			FXD::F32 _get_frequency(void) const;
			FXD::F32 _get_importance(void) const;

		protected:
			SND::Voice m_voice;
			SND::Emitter m_emitter;
			FXD::U8 m_nPriority;
			Math::LerpF32 m_fRatio;
			Container::DirtyType< SND::E_VoiceState > m_eVoiceState;
		};

	} //namespace SND
} //namespace FXD
#endif //FXDENGINE_SND_SNDNODE_H