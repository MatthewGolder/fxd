// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_XA2_XAUDIO2_H
#define FXDENGINE_SND_XA2_XAUDIO2_H

#include "FXDEngine/SND/Types.h"

#if	IsSndXAudio2()
#	if IsOSPC()
#		include "FXDEngine/Core/Win32/Win32.h"
#		//define FXD_FORCE_JUNE_2010_SDK
#		if (_WIN32_WINNT >= 0x0602) && !defined(FXD_FORCE_JUNE_2010_SDK)
#			if defined(_MSC_VER) && (_MSC_VER < 1700)
#				error DirectX Tool Kit for Audio does not support VS without the DirectX SDK 
#			endif
#			include <xaudio2.h>
#			include <xaudio2fx.h>
#			include <x3daudio.h>
#			pragma comment(lib, "xaudio2.lib")
#		else
			// Using XAudio 2.7 requires the DirectX SDK
#			define FXD_XAUDIO2_JUNE_2010_SDK
#			include <comdecl.h>
#			include <xaudio2.h>
#			include <xaudio2fx.h>
#			include <x3daudio.h>
#			pragma comment(lib, "X3DAudio.lib")
#		endif
#	endif

namespace FXD
{
	namespace SND
	{
		// ------
		// CheckXA2Result
		// -
		// ------
		class CheckXA2Result
		{
		public:
			CheckXA2Result(HRESULT hr, const FXD::UTF8* pText, const FXD::UTF8* pFile, FXD::U32 nLine);

			bool operator()();

		public:
			HRESULT m_hr;
			FXD::U32 m_nLine;
			const FXD::UTF8* m_pText;
			const FXD::UTF8* m_pFile;
		};
#		define XA2ResultCheckFail(c) FXD::SND::CheckXA2Result(c, #c, __FILE__, __LINE__)()
#		define XA2ResultCheckSuccess(c) !XA2ResultCheckFail(c)

		class ISndAdapterXA2;
		class ISndAdapterDescXA2;
		typedef std::shared_ptr< ISndAdapterXA2 > SndAdapterXA2;
		typedef std::shared_ptr< ISndAdapterDescXA2 > SndAdapterDescXA2;

		class IVoiceSFXXA2;
		class IVoiceMusicXA2;
		typedef std::shared_ptr< IVoiceSFXXA2 > VoiceSFXXA2;
		typedef std::shared_ptr< IVoiceMusicXA2 > VoiceMusicXA2;

		class INodeDataXA2;
		class INodeMixerXA2;
		class INodeVoiceXA2;
		typedef std::shared_ptr< INodeDataXA2 > SndNodeDataXA2;
		typedef std::shared_ptr< INodeMixerXA2 > NodeMixerXA2;
		typedef std::shared_ptr< INodeVoiceXA2 > NodeVoiceXA2;

		class IListenerXA2;
		typedef std::shared_ptr< IListenerXA2 > ListenerXA2;

		class IEmitterXA2;
		typedef std::shared_ptr< IEmitterXA2 > EmitterXA2;

		class IEffectXA2;
		class IEffectChainXA2;
		typedef std::shared_ptr< IEffectXA2 > EffectXA2;
		typedef std::shared_ptr< IEffectChainXA2 > EffectChainXA2;

	} //namespace SND
} //namespace FXD
#endif //IsSndXAudio2()
#endif //FXDENGINE_SND_XA2_XAUDIO2_H