// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_XA2_EFFECTXA2_H
#define FXDENGINE_SND_XA2_EFFECTXA2_H

#include "FXDEngine/SND/Types.h"

#if IsSndXAudio2()
#include "FXDEngine/SND/Effect.h"
#include "FXDEngine/SND/XA2/XAudio2.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// IEffectChainXA2
		// -
		// ------
		class IEffectXA2 FINAL : public IEffect
		{
		public:
			EXPLICIT IEffectXA2(const SND::EFFECT_DESC& effectDesc);
			~IEffectXA2(void);

		public:
			IUnknown* m_pReverb;
		};

		// ------
		// IEffectChainXA2
		// -
		// ------
		class IEffectChainXA2 FINAL : public IEffectChain
		{
		public:
			EXPLICIT IEffectChainXA2(const SND::ISndAdapter* pSndAdapter, SND::INodeMixer* pNodeMixer);
			~IEffectChainXA2(void);

		protected:
			bool _create_effect_chain(void) FINAL;
			bool _release_effect_chain(void) FINAL;

			void _add_effects(const Container::Vector< SND::EFFECT_DESC >& effectDescs) FINAL;
			void _enable_effect(FXD::U16 nSlot, bool bEnable) FINAL;
			bool _is_effect_enabled(FXD::U16 nSlot) const FINAL;

			void _update(FXD::F32 dt) FINAL;
			void _recreate(void) FINAL;

		public:
		};

	} //namespace SND
} //namespace FXD
#endif //IsSndXAudio2()
#endif //FXDENGINE_SND_XA2_EFFECTXA2_H