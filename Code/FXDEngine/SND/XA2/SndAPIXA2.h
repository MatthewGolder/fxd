// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_XA2_SNDAPIXA2_H
#define FXDENGINE_SND_XA2_SNDAPIXA2_H

#include "FXDEngine/SND/Types.h"

#if IsSndXAudio2()
#include "FXDEngine/SND/SndApi.h"
#include "FXDEngine/SND/XA2/XAudio2.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// ISndApiXA2
		// -
		// ------
		class ISndApiXA2 FINAL : public ISndApi
		{
		public:
			ISndApiXA2(void);
			~ISndApiXA2(void);

			bool create_api(void) FINAL;
			bool release_api(void) FINAL;

		private:
			void _create_adapter_list(void) FINAL;

		private:
#if defined(FXD_XAUDIO2_JUNE_2010_SDK)
			HMODULE m_XAudioDLL;
#endif
		};

	} //namespace SND
} //namespace FXD
#endif //IsSndXAudio2()
#endif //FXDENGINE_SND_XA2_SNDAPIXA2_H