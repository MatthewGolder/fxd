// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndXAudio2()
#include "FXDEngine/Memory/Memory.h"
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/XA2/ListenerXA2.h"
#include "FXDEngine/SND/XA2/SndAdapterXA2.h"
#include "FXDEngine/SND/XA2/SndNodeXA2.h"
#include "FXDEngine/SND/XA2/VoiceXA2.h"
#include "FXDEngine/SND/Decoders/Audio.h"

using namespace FXD;
using namespace SND;

// ------
// IVoiceSFXXA2
// - 
// ------
IVoiceSFXXA2::IVoiceSFXXA2(const SND::ISndAdapter* pSndAdapter, SND::INodeVoice* pNodeVoice)
	: IVoiceSFX(pSndAdapter, pNodeVoice)
	, m_nSamplesPlayedOffset(0)
{
	for (FXD::U16 n1 = 0; n1 < BufferCount; n1++)
	{
		m_buffers[n1].m_nID = n1;
		Memory::MemZero(&m_buffers[n1].m_xa2Buffer, sizeof(XAUDIO2_BUFFER));
	}
}

IVoiceSFXXA2::~IVoiceSFXXA2(void)
{
}

bool IVoiceSFXXA2::create(const SND::VOICE_DESC& voiceDesc)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");
	PRINT_COND_ASSERT((voiceDesc.Audio->is_sequenceable()), "SND: Stream is not sequenceable");

	// 1. Store Local Information and construct memory buffers 
	m_voiceDesc = voiceDesc;
	m_voiceDesc.Audio->seek_begin();
	_ensure_loop_points_defined(m_voiceDesc.CreateFlag, m_voiceDesc.Audio);

	m_nCurrentBuffer = 0;
	m_dataBuffer = Memory::MemHandle::read_to_end((*m_voiceDesc.Audio), true);
	return true;
}

void IVoiceSFXXA2::release(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	stop();
	for (FXD::U16 n1 = 0; n1 < BufferCount; n1++)
	{
		Memory::MemZero(&m_buffers[n1].m_xa2Buffer, sizeof(XAUDIO2_BUFFER));
	}
	m_voiceDesc = SND::VOICE_DESC();
}

void IVoiceSFXXA2::play(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!m_pVoiceNode->is_valid())
	{
		return;
	}

	IXAudio2SourceVoice* pVoice = (IXAudio2SourceVoice*)((INodeDataXA2*)m_pVoiceNode->get_node_data().get())->m_pVoice;
	if (XA2ResultCheckFail(pVoice->Start(0, 0)))
	{
		PRINT_ASSERT << "SND: voiceNode->Start() failed";
	}

	m_bStreamComplete = false;
}

void IVoiceSFXXA2::pause(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!m_pVoiceNode->is_valid())
	{
		return;
	}

	IXAudio2SourceVoice* pVoice = (IXAudio2SourceVoice*)((INodeDataXA2*)m_pVoiceNode->get_node_data().get())->m_pVoice;
	if (XA2ResultCheckFail(pVoice->Stop(0, 0)))
	{
		PRINT_ASSERT << "SND: voiceNode->Stop() failed";
	}
}

void IVoiceSFXXA2::stop(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!m_pVoiceNode->is_valid())
	{
		return;
	}

	IXAudio2SourceVoice* pVoice = (IXAudio2SourceVoice*)((INodeDataXA2*)m_pVoiceNode->get_node_data().get())->m_pVoice;
	if (XA2ResultCheckFail(pVoice->Stop(0)))
	{
		PRINT_ASSERT << "SND: voiceNode->Stop() failed";
	}

	_buffers_flush();
	m_loopHandle = LoopPointHandle();
}

FXD::U32 IVoiceSFXXA2::tell_sample_position(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!m_pVoiceNode->is_valid())
	{
		return 0;
	}

	auto itr = m_bufferQueue.begin();
	if (itr == m_bufferQueue.end())
	{
		return 0;
	}

	IXAudio2SourceVoice* pXA2Voice = (IXAudio2SourceVoice*)((INodeDataXA2*)m_pVoiceNode->get_node_data().get())->m_pVoice;
	XAUDIO2_VOICE_STATE xa2VoiceState = {};
	pXA2Voice->GetState(&xa2VoiceState);

	FXD::U32 nXA2SampleOffset = (xa2VoiceState.SamplesPlayed - m_nSamplesPlayedOffset);
	FXD::U32 nSampleStart = ((*itr)->m_nSampleStart + (*itr)->m_nSampleOffset) / m_voiceDesc.Audio->source_format().getBytesPerSample();
	return (nSampleStart + nXA2SampleOffset);
}

void IVoiceSFXXA2::_update_buffers(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1. Early out if we are not playing
	if ((!m_pVoiceNode->is_valid()) || (!m_pVoiceNode->is_playing()))
	{
		return;
	}

	// 2. Check for buffers that have finished
	_buffers_finished();

	// 3. If we have finished streaming data then check when we can stop
	if (m_bStreamComplete)
	{
		if (_buffer_queued_count() == 0)
		{
			m_pVoiceNode->stop();
		}
		return;
	}

	// 4. If we have a fee buffer then fill a new one
	FXD::U32 nQueuedBuffers = _buffer_queued_count();
	if (nQueuedBuffers < BufferCount)
	{
		_buffer_fill();
	}
}

void IVoiceSFXXA2::_buffer_fill(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1. Return if we are finished
	if (m_bStreamComplete)
	{
		return;
	}

	// 2. Make sure we have a buffer to start
	if (!m_loopHandle.m_pLoopPoint)
	{
		_update_loop_point();
	}

	// 3. Submit the buffer data
	const FXD::U32 nAmountRead = m_nBytesSize;
	_buffer_submit(nAmountRead);

	// 4. Move to next loop point
	_update_loop_point();
}

void IVoiceSFXXA2::_buffer_submit(FXD::U32 nSize)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");
	PRINT_COND_ASSERT((nSize < XAUDIO2_MAX_BUFFER_BYTES), "SND: Buffer too big to play");

	// 1. Get a pointer to the memory 
	Memory::MemHandle::LockGuard memHandleLock(m_dataBuffer);
	const FXD::U8* pMemPointer = (const FXD::U8*)memHandleLock.get_mem();

	// 2. Construct a local buffer to store
	m_buffers[m_nCurrentBuffer].m_nSampleStart = m_nBytesStartPos;
	m_buffers[m_nCurrentBuffer].m_nSampleOffset = m_loopHandle.m_nLoopPointBytesRead;
	m_buffers[m_nCurrentBuffer].m_eState = FXDBUFFERXA2::State::Playing;
	m_bufferQueue.push_back(&m_buffers[m_nCurrentBuffer]);
	m_loopHandle.m_nLoopPointBytesRead += nSize;

	// 3. Create and submit current api buffer
	const FXD::U8* pMemBegin = (const FXD::U8*)(pMemPointer + m_nBytesStartPos);
	Memory::MemZero_T(m_buffers[m_nCurrentBuffer].m_xa2Buffer);
	m_buffers[m_nCurrentBuffer].m_xa2Buffer.PlayBegin = 0;
	m_buffers[m_nCurrentBuffer].m_xa2Buffer.PlayLength = 0;
	m_buffers[m_nCurrentBuffer].m_xa2Buffer.pAudioData = pMemBegin;
	m_buffers[m_nCurrentBuffer].m_xa2Buffer.AudioBytes = (UINT32)nSize;
	m_buffers[m_nCurrentBuffer].m_xa2Buffer.Flags = 0;
	m_buffers[m_nCurrentBuffer].m_xa2Buffer.pContext = &(m_buffers[m_nCurrentBuffer]);

	IXAudio2SourceVoice* pVoice = (IXAudio2SourceVoice*)((INodeDataXA2*)m_pVoiceNode->get_node_data().get())->m_pVoice;
	if (XA2ResultCheckFail(pVoice->SubmitSourceBuffer(&m_buffers[m_nCurrentBuffer].m_xa2Buffer)))
	{
		PRINT_ASSERT << "SND: m_pVoice->SubmitSourceBuffer() failed";
	}

	// 4. Increment to the next buffer
	m_nCurrentBuffer++;
	m_nCurrentBuffer %= BufferCount;
}

void IVoiceSFXXA2::_buffers_flush(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (m_pVoiceNode->is_valid())
	{
		IXAudio2SourceVoice* pVoice = (IXAudio2SourceVoice*)((INodeDataXA2*)m_pVoiceNode->get_node_data().get())->m_pVoice;
		pVoice->FlushSourceBuffers();
		while (_buffer_queued_count() > 0)
		{
		}
	}

	m_nBytesStartPos = 0;
	m_nBytesSize = 0;
	m_bStreamComplete = true;
}

FXD::U32 IVoiceSFXXA2::_buffer_queued_count(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!m_pVoiceNode->is_valid())
	{
		return 0;
	}

	FXD::U32 nCount = 0;
	for (FXD::U16 n1 = 0; n1 < BufferCount; n1++)
	{
		if (m_buffers[n1].m_eState == FXDBUFFERXA2::State::Playing)
		{
			nCount++;
		}
	}
	return nCount;
}

void IVoiceSFXXA2::_buffers_finished(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (!m_pVoiceNode->is_valid())
	{
		return;
	}

	bool bRemoved = false;
	for (FXD::U16 n1 = 0; n1 < BufferCount; n1++)
	{
		if (m_buffers[n1].m_eState == FXDBUFFERXA2::State::Finished)
		{
			m_buffers[n1].m_eState = FXDBUFFERXA2::State::Free;
			m_bufferQueue.find_erase(&m_buffers[n1]);
			bRemoved = true;
		}
	}
	if (bRemoved)
	{
		IXAudio2SourceVoice* pXA2Voice = (IXAudio2SourceVoice*)((INodeDataXA2*)m_pVoiceNode->get_node_data().get())->m_pVoice;
		XAUDIO2_VOICE_STATE xa2VoiceState = {};
		pXA2Voice->GetState(&xa2VoiceState);
		m_nSamplesPlayedOffset = (FXD::S32)xa2VoiceState.SamplesPlayed;
	}
}
#endif //IsSndXAudio2()