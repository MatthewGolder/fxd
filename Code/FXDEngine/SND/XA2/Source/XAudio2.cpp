// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndXAudio2()
#include "FXDEngine/SND/XA2/XAudio2.h"
#include "FXDEngine/Core/Debugger.h"

using namespace FXD;
using namespace SND;

// ------
// CheckXA2Result
// -
// ------
inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, CheckXA2Result const& rhs)
{
	switch (rhs.m_hr)
	{
#define HANDLE_STR(h) case h: ostr << #h; break;
		HANDLE_STR(XAUDIO2_E_INVALID_CALL)
#undef HANDLE_STR
	default:
		{
			FXD_ERROR("Unrecognised HResult - please find the correct string and add to this case");
			break;
		}
	}
	return ostr;
}

CheckXA2Result::CheckXA2Result(HRESULT hr, const FXD::UTF8* pText, const FXD::UTF8* pFile, FXD::U32 nLine)
	: m_hr(hr)
	, m_nLine(nLine)
	, m_pText(pText)
	, m_pFile(pFile)
{
}

bool CheckXA2Result::operator()()
{
	if (SUCCEEDED(m_hr))
	{
		return false;
	}
	PRINT_WARN << "[" << m_pFile << " , " << m_nLine << "] " << m_pText << " - failed with error: " << (*this);
	return true;
}
#endif //IsSndXAudio2()