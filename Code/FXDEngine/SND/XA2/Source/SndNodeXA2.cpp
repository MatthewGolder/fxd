// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndXAudio2()
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/XA2/EffectXA2.h"
#include "FXDEngine/SND/XA2/EmitterXA2.h"
#include "FXDEngine/SND/XA2/SndAdapterXA2.h"
#include "FXDEngine/SND/XA2/SndNodeXA2.h"
#include "FXDEngine/SND/XA2/VoiceXA2.h"
#include "FXDEngine/SND/Decoders/Audio.h"

using namespace FXD;
using namespace SND;

// ------
// XA2
// -
// ------
namespace XA2
{
	bool createMasterNode(const SND::ISndAdapter* pSndAdapter, SND::INodeMixer* pNodeMixer)
	{
		// 1. Create CreateMasteringVoice
		if (pSndAdapter->is_vaild())
		{
			SND::ISndAdapterXA2* pSndAdapterXA2 = (SND::ISndAdapterXA2*)(pSndAdapter);
			SND::INodeDataXA2* pNodeData = (SND::INodeDataXA2*)pNodeMixer->get_node_data().get();

#if defined(FXD_XAUDIO2_JUNE_2010_SDK)
			UINT32 nPreferredDevice = 0;
			HRESULT hr = pSndAdapterXA2->m_pEngine->CreateMasteringVoice((IXAudio2MasteringVoice**)&pNodeData->m_pVoice, XAUDIO2_DEFAULT_CHANNELS, XAUDIO2_DEFAULT_SAMPLERATE, pSndAdapter->adapter_desc()->deviceID(), nPreferredDevice, nullptr);
#else
			WCHAR* pPreferredDevice = nullptr;
			HRESULT hr = pSndAdapterXA2->m_pEngine->CreateMasteringVoice((IXAudio2MasteringVoice**)&pNodeData->m_pVoice, XAUDIO2_DEFAULT_CHANNELS, XAUDIO2_DEFAULT_SAMPLERATE, pSndAdapter->adapter_desc()->deviceID(), pPreferredDevice, nullptr);
#endif
			if (XA2ResultCheckFail(hr))
			{
				PRINT_WARN << "SND: CreateMasteringVoice() failed";
				pNodeData->m_pVoice = nullptr;
				return false;
			}
		}
		return true;
	}

	bool createSubmixNode(const SND::ISndAdapter* pSndAdapter, SND::INodeMixer* pNodeMixer)
	{
		// 1. Create CreateSubmixVoice
		if (pSndAdapter->is_vaild())
		{
			SND::ISndAdapterXA2* pSndAdapterXA2 = (SND::ISndAdapterXA2*)(pSndAdapter);
			SND::INodeDataXA2* pNodeData = (SND::INodeDataXA2*)pNodeMixer->get_node_data().get();

			if (XA2ResultCheckFail(pSndAdapterXA2->m_pEngine->CreateSubmixVoice((IXAudio2SubmixVoice**)&pNodeData->m_pVoice, pNodeMixer->get_node_desc().OutChannels, pNodeMixer->get_node_desc().SampleRate, 0, 0/*, &audioSends, nullptr*/)))
			{
				PRINT_WARN << "SND: CreateSubmixVoice() failed";
				pNodeData->m_pVoice = nullptr;
				return false;
			}
		}
		return true;
	}

	bool createVoiceNode(const SND::ISndAdapter* pSndAdapter, SND::INodeVoice* pNodeVoice, const SND::SndFormat& sndFormat)
	{
		// 1. Setup WAVEFORMATEX
		WAVEFORMATEX wfx;
		wfx.wFormatTag = sndFormat.Format;
		wfx.nChannels = sndFormat.Channels;
		wfx.nSamplesPerSec = sndFormat.SampleRate;
		wfx.wBitsPerSample = sndFormat.BitsPerSample;
		wfx.nBlockAlign = (sndFormat.Channels * sndFormat.BitsPerSample / 8);
		wfx.nAvgBytesPerSec = sndFormat.BytesPerSec;
		wfx.cbSize = sizeof(WAVEFORMATEX);

		// 2. Create CreateSourceVoice
		if (pSndAdapter->is_vaild())
		{
			SND::ISndAdapterXA2* pSndAdapterXA2 = (SND::ISndAdapterXA2*)(pSndAdapter);
			SND::INodeDataXA2* pNodeData = (SND::INodeDataXA2*)pNodeVoice->get_node_data().get();

			IXAudio2VoiceCallback* pCallback = (dynamic_cast< IXAudio2VoiceCallback* >(pNodeVoice->voice().get()));
			if (XA2ResultCheckFail(pSndAdapterXA2->m_pEngine->CreateSourceVoice((IXAudio2SourceVoice**)&pNodeData->m_pVoice, &wfx, 0, XAUDIO2_DEFAULT_FREQ_RATIO, pCallback/*, &audioSends, nullptr*/)))
			{
				pNodeData->m_pVoice = nullptr;
				PRINT_WARN << "SND: CreateSourceVoice() failed";
				return false;
			}
		}
		return true;
	}

	void shutdownMixerNode(SND::INodeMixer* pNodeMixer)
	{
		// 1. Destroy mixer
		INodeDataXA2* pNodeData = (INodeDataXA2*)pNodeMixer->get_node_data().get();
		if (pNodeData->m_pVoice != nullptr)
		{
			pNodeData->m_pVoice->DestroyVoice();
			pNodeData->m_pVoice = nullptr;
		}
	}

	void shutdownVoiceNode(SND::INodeVoice* pNodeVoice)
	{
		// 1. Destroy voice
		INodeDataXA2* pNodeData = (INodeDataXA2*)pNodeVoice->get_node_data().get();
		if (pNodeData->m_pVoice != nullptr)
		{
			pNodeData->m_pVoice->DestroyVoice();
			pNodeData->m_pVoice = nullptr;
		}
	}

	bool linkNodes(const SND::ISndAdapter* /*pSndAdapter*/, const SND::INodeMixer* pParentNode, SND::INode* pNode)
	{
		// 1. Get Parent
		INodeDataXA2* pParentNodeData = (INodeDataXA2*)pParentNode->get_node_data().get();

		// 2. Setup XAUDIO2_VOICE_SENDS
		XAUDIO2_VOICE_SENDS audioSends;
#ifdef X2GENERIC
		audioSends.OutputCount = 1;
		audioSends.pOutputVoices = (IXAudio2Voice**)&pParentNodeData->m_pVoice;
#else
		XAUDIO2_SEND_DESCRIPTOR sendDesc;
		sendDesc.Flags = 0;
		sendDesc.pOutputVoice = pParentNodeData->m_pVoice;
		audioSends.SendCount = 1;
		audioSends.pSends = &sendDesc;
#endif

		// 3. Link Nodes
		INodeDataXA2* pNodeData = (INodeDataXA2*)pNode->get_node_data().get();
		if (XA2ResultCheckFail(pNodeData->m_pVoice->SetOutputVoices(&audioSends)))
		{
			pNodeData->m_pVoice = nullptr;
			PRINT_WARN << "SND: SetOutputVoices() failed";
			return false;
		}
		return true;
	}
} //namespace XA2

// ------
// INodeDataXA2
// -
// ------
INodeDataXA2::INodeDataXA2(void)
	: m_pVoice(nullptr)
{
}

INodeDataXA2::~INodeDataXA2(void)
{
	PRINT_COND_ASSERT((m_pVoice == nullptr), "SND: m_pVoice was not cleaned up before shutdown");
}

// ------
// INodeMixerXA2
// -
// ------
INodeMixerXA2::INodeMixerXA2(const SND::ISndAdapter* pSndAdapter, const SND::INodeMixer* pNodeParent)
	: INodeMixer(pSndAdapter, pNodeParent)
{
}

INodeMixerXA2::~INodeMixerXA2(void)
{
}

void INodeMixerXA2::_create(const SND::NODE_DESC& nodeDesc)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1. Set variables
	m_nodeDesc = nodeDesc;
	m_nodeData = std::make_shared< INodeDataXA2 >();

	// 2. Create and link nodes
	if (m_pNodeParent == nullptr)
	{
		::XA2::createMasterNode(m_pSndAdapter, this);
	}
	else
	{
		::XA2::createSubmixNode(m_pSndAdapter, this);
		::XA2::linkNodes(m_pSndAdapter, m_pNodeParent, this);
	}

	if (m_nodeDesc.Auxiliary)
	{
		m_effectChain = std::make_shared< SND::IEffectChainXA2 >(m_pSndAdapter, this);
		m_effectChain->create_effect_chain();
	}

	// 3. Set Volume
	set_volume(m_nodeDesc.Volume);
}

void INodeMixerXA2::_release(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	FXD_RELEASE(m_effectChain, release_effect_chain());

	// 1. Shutdown submix nodes
	fxd_for(auto& mixer, m_mixers)
	{
		mixer.second->release();
	}
	m_mixers.clear();

	// 2. Shutdown voice nodes
	fxd_for(auto& voice, m_voices)
	{
		voice->release();
	}
	m_voices.clear();

	// 3. Shutdown node
	::XA2::shutdownMixerNode(this);
}

void INodeMixerXA2::_update_volume(FXD::F32 dt)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	m_nodeDesc.Volume.update(dt);

	INodeDataXA2* pNodeData = (INodeDataXA2*)m_nodeData.get();
	if (pNodeData->m_pVoice != nullptr)
	{
		pNodeData->m_pVoice->SetVolume(m_nodeDesc.Volume);
	}
}

// ------
// INodeVoiceXA2
// -
// ------
INodeVoiceXA2::INodeVoiceXA2(const SND::ISndAdapter* pSndAdapter, const SND::INodeMixer* pNodeParent)
	: INodeVoice(pSndAdapter, pNodeParent)
{
}

INodeVoiceXA2::~INodeVoiceXA2(void)
{
}

void INodeVoiceXA2::_create(const SND::NODE_DESC& nodeDesc, const SND::NODE_VOICE_DESC& nodeVoiceDesc)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1. Sanity Check
	if ((nodeVoiceDesc.Flags.is_flag_set(E_CreateFlag::Is3D)) && (nodeVoiceDesc.Audio->source_format().Channels > 1))
	{
		PRINT_ASSERT << "SND: We don't support multi-channel 3d sounds";
	}

	// 2. Initialise the voice
	m_nodeDesc = nodeDesc;
	m_nodeData = std::make_shared< INodeDataXA2 >();
	if (nodeVoiceDesc.Flags.is_flag_set(E_CreateFlag::Streamed))
	{
		m_voice = std::make_shared< SND::IVoiceMusicXA2 >(m_pSndAdapter, this);
	}
	else
	{
		m_voice = std::make_shared< SND::IVoiceSFXXA2 >(m_pSndAdapter, this);
	}

	SND::VOICE_DESC voiceDesc = {};
	voiceDesc.Audio = nodeVoiceDesc.Audio;
	voiceDesc.LoopGroup = nodeVoiceDesc.LoopGroup;
	voiceDesc.CreateFlag = nodeVoiceDesc.Flags;
	m_voice->create(voiceDesc);

	// 3. Create and link nodes
	::XA2::createVoiceNode(m_pSndAdapter, this, nodeVoiceDesc.Audio->source_format());
	::XA2::linkNodes(m_pSndAdapter, m_pNodeParent, this);

	// 4. Initialise the emitter
	if (nodeVoiceDesc.Flags.is_flag_set(E_CreateFlag::Is3D))
	{
		m_emitter = std::make_shared< SND::IEmitterXA2 >(m_pSndAdapter, this);
		m_emitter->create_emitter();
	}

	// 5. Set Volume, Frequency and Priority
	set_volume(m_nodeDesc.Volume);
	set_priority(nodeVoiceDesc.Priority);
	set_frequency(nodeVoiceDesc.Ratio);

	m_pSndAdapter->m_voices.push_back(this);
}

void INodeVoiceXA2::_release(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	m_pSndAdapter->m_voices.find_erase(this);

	FXD_RELEASE(m_emitter, release_emitter());
	FXD_RELEASE(m_voice, release());

	::XA2::shutdownVoiceNode(this);
}

bool INodeVoiceXA2::_is_valid(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	IXAudio2Voice* pVoice = ((INodeDataXA2*)get_node_data().get())->m_pVoice;
	if (pVoice == nullptr)
	{
		return false;
	}
	return true;
}

void INodeVoiceXA2::_update_volume(FXD::F32 dt)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	m_nodeDesc.Volume.update(dt);

	INodeDataXA2* pNodeData = (INodeDataXA2*)m_nodeData.get();
	if (pNodeData->m_pVoice != nullptr)
	{
		pNodeData->m_pVoice->SetVolume(m_nodeDesc.Volume);
	}
}

void INodeVoiceXA2::_update_frequency(FXD::F32 dt)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	m_fRatio.update(dt);

	INodeDataXA2* pNodeData = (INodeDataXA2*)m_nodeData.get();
	if (pNodeData->m_pVoice != nullptr)
	{
		((IXAudio2SourceVoice*)pNodeData->m_pVoice)->SetFrequencyRatio(m_fRatio, XAUDIO2_COMMIT_NOW);
	}
}
#endif //IsSndXAudio2()