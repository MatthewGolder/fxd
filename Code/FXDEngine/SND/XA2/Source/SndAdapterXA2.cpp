// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndXAudio2()
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/XA2/ListenerXA2.h"
#include "FXDEngine/SND/XA2/SndAdapterXA2.h"
#include "FXDEngine/SND/XA2/SndNodeXA2.h"

using namespace FXD;
using namespace SND;

// ------
// ISndAdapterDescXA2
// - 
// ------
ISndAdapterDescXA2::ISndAdapterDescXA2(const App::DeviceIndex nDeviceID)
	: ISndAdapterDesc(nDeviceID)
	, m_nDeviceIndex(-1)
{
#if defined(FXD_XAUDIO2_JUNE_2010_SDK)
	Memory::MemZero_T(m_role);
#endif
}

ISndAdapterDescXA2::~ISndAdapterDescXA2(void)
{
}

// ------
// ISndAdapterXA2
// - 
// ------
ISndAdapterXA2::ISndAdapterXA2(const App::AdapterDesc& adapterDesc, const App::IApi* pApi)
	: ISndAdapter(adapterDesc, pApi)
	, m_pEngine(nullptr)
{
}

ISndAdapterXA2::~ISndAdapterXA2(void)
{
	PRINT_COND_ASSERT((m_pEngine == nullptr), "SND: Engine has not been shutdown");
}

bool ISndAdapterXA2::_create(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1. Initialize Stuff
	if (XA2ResultCheckFail(CoInitializeEx(NULL, COINIT_MULTITHREADED)))
	{
		PRINT_WARN << "SND: CoInitializeEx() failed";
		return false;
	}

	// 2. Create the audio engine
	DWORD nCreateFlags = 0;
#if defined(FXD_XAUDIO2_JUNE_2010_SDK) && defined(FXD_DEBUG)
	nCreateFlags |= XAUDIO2_DEBUG_ENGINE;
#endif

	if (XA2ResultCheckFail(XAudio2Create(&m_pEngine, nCreateFlags, XAUDIO2_DEFAULT_PROCESSOR)))
	{
		PRINT_WARN << "SND: XAudio2Create() failed";
		return false;
	}

	// 3. Create the master node
	NODE_DESC nodeDesc;
	nodeDesc.Name = UTF_8("Master");
	m_masterNode = std::make_shared< SND::INodeMixerXA2 >(this, nullptr);
	m_masterNode->create(nodeDesc);

	// 4. Create the listener
	m_listener = std::make_shared< SND::IListenerXA2 >(this);
	m_listener->create();
	return true;
}

bool ISndAdapterXA2::_release(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1. Release the listener
	FXD_RELEASE(m_listener, release());

	// 2. Release the master node
	FXD_RELEASE(m_masterNode, release());

	// 3. Release the Engine
	if (m_pEngine != nullptr)
	{
		m_pEngine->Release();
		m_pEngine = nullptr;
	}
	CoUninitialize();
	return true;
}

bool ISndAdapterXA2::_is_vaild(void) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	bool bRetVal = (m_pEngine != nullptr);
	return bRetVal;
}
#endif //IsSndXAudio2()