// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndXAudio2()
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/XA2/EmitterXA2.h"
#include "FXDEngine/SND/XA2/ListenerXA2.h"
#include "FXDEngine/SND/XA2/SndAdapterXA2.h"
#include "FXDEngine/SND/XA2/SndNodeXA2.h"
#include "FXDEngine/SND/XA2/VoiceXA2.h"

using namespace FXD;
using namespace SND;

// ------
// IListenerXA2
// - 
// ------
IEmitterXA2::IEmitterXA2(const SND::ISndAdapter* pSndAdapter, const SND::INodeVoice* pNodeVoice)
	: IEmitter(pSndAdapter, pNodeVoice)
{
	Memory::MemZero_T(m_xa2Emitter);
	m_xa2Emitter.DopplerScaler = 1.0f;
}

IEmitterXA2::~IEmitterXA2(void)
{
}

bool IEmitterXA2::_create_emitter(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1.
	set(Math::Matrix4F::kIdentity);
	set_velocity(Math::Vector3F::kZero);
	set_min_max_distance(m_fMinDistance, m_fMaxDistance);

	// 2.
	m_xa2Emitter.ChannelCount = 1;// m_pNodeVoice->m_voiceDesc.Audio->source_format().Channels;
	return true;
}

bool IEmitterXA2::_release_emitter(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (m_xa2Emitter.pVolumeCurve != nullptr)
	{
		FXD_DELETE_ARRAY(m_xa2Emitter.pVolumeCurve->pPoints);
		m_xa2Emitter.pVolumeCurve->pPoints = nullptr;
		FXD_DELETE(m_xa2Emitter.pVolumeCurve);
		m_xa2Emitter.pVolumeCurve = nullptr;
	}
	return true;
}

void IEmitterXA2::_update(const SND::Listener& listener)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	SND::ListenerXA2 listenerXA2 = std::dynamic_pointer_cast< SND::IListenerXA2 >(listener);

	Math::Vector3F vPos = position();
	Math::Vector3F vUp = up();
	Math::Vector3F vForward = forward();
	Math::Vector3F vVelocity = velocity();

	m_xa2Emitter.Position.x = vPos.x();
	m_xa2Emitter.Position.y = vPos.y();
	m_xa2Emitter.Position.z = vPos.z();
	m_xa2Emitter.OrientTop.x = vUp.x();
	m_xa2Emitter.OrientTop.y = vUp.y();
	m_xa2Emitter.OrientTop.z = vUp.z();
	m_xa2Emitter.OrientFront.x = vForward.x();
	m_xa2Emitter.OrientFront.y = vForward.y();
	m_xa2Emitter.OrientFront.z = vForward.z();
	m_xa2Emitter.Velocity.x = vVelocity.x();
	m_xa2Emitter.Velocity.y = vVelocity.y();
	m_xa2Emitter.Velocity.z = vVelocity.z();

	X3DAUDIO_DSP_SETTINGS dspSettings = {};
	Memory::MemZero_T(dspSettings);

	FLOAT32 matrix[12] = { 0 };
	dspSettings.SrcChannelCount = m_xa2Emitter.ChannelCount;
	dspSettings.DstChannelCount = m_pSndAdapter->master_node()->get_node_desc().OutChannels;
	dspSettings.pMatrixCoefficients = matrix;
	dspSettings.DopplerFactor = 1.0f;

	X3DAudioCalculate(listenerXA2->m_pX3DInstance, &listenerXA2->m_xa2Listener, &m_xa2Emitter, (X3DAUDIO_CALCULATE_MATRIX | X3DAUDIO_CALCULATE_DOPPLER), &dspSettings);

	IXAudio2SourceVoice* pVoice = (IXAudio2SourceVoice*)((INodeDataXA2*)m_pNodeVoice->get_node_data().get())->m_pVoice;
	if (pVoice != nullptr)
	{
		IXAudio2Voice* pParent = nullptr;
		pVoice->SetOutputMatrix(pParent, dspSettings.SrcChannelCount, dspSettings.DstChannelCount, dspSettings.pMatrixCoefficients, 4321);
		pVoice->SetFrequencyRatio(dspSettings.DopplerFactor * m_pNodeVoice->get_frequency(), 4321);
	}

	SND::ISndAdapterXA2* pSndAdapterXA2 = (SND::ISndAdapterXA2*)(m_pSndAdapter);
	if (pSndAdapterXA2->m_pEngine)
	{
		pSndAdapterXA2->m_pEngine->CommitChanges(4321);
	}
}

void IEmitterXA2::_update_distance_model(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1. Set the overall volume curve scale.
	m_xa2Emitter.CurveDistanceScaler = m_fMaxDistance;

	// 2. The curve uses normalized distances, so figure out the normalized min distance.
	FXD::F32 fNormMin = 0.0f;
	if (m_fMinDistance > 0.0f)
	{
		fNormMin = (m_fMinDistance / m_fMaxDistance);
	}

	// See what type of curve we are supposed to generate.
	const bool bLinear = (m_pSndAdapter->listener()->distance_model() == E_SndDistanceModel::Linear);

	// Have we setup the curve yet?
	if ((m_xa2Emitter.pVolumeCurve == nullptr) || ((bLinear) && (m_xa2Emitter.pVolumeCurve->PointCount != 2)) || ((bLinear == false) && (m_xa2Emitter.pVolumeCurve->PointCount != 6)))
	{
		if (m_xa2Emitter.pVolumeCurve == nullptr)
		{
			m_xa2Emitter.pVolumeCurve = FXD_NEW(X3DAUDIO_DISTANCE_CURVE);
		}
		else
		{
			FXD_DELETE_ARRAY(m_xa2Emitter.pVolumeCurve->pPoints);
			m_xa2Emitter.pVolumeCurve->pPoints = nullptr;
		}

		// We use 6 points for logarithmic volume curves and 2 for linear volume curves.
		if (bLinear)
		{
			m_xa2Emitter.pVolumeCurve->pPoints = FXD_NEW_ARRAY(X3DAUDIO_DISTANCE_CURVE_POINT, 2);
			m_xa2Emitter.pVolumeCurve->PointCount = 2;
		}
		else
		{
			m_xa2Emitter.pVolumeCurve->pPoints = FXD_NEW_ARRAY(X3DAUDIO_DISTANCE_CURVE_POINT, 6);
			m_xa2Emitter.pVolumeCurve->PointCount = 6;
		}

		// The first and last points are known and will not change.
		m_xa2Emitter.pVolumeCurve->pPoints[0].Distance = 0.0f;
		m_xa2Emitter.pVolumeCurve->pPoints[0].DSPSetting = 1.0f;
		m_xa2Emitter.pVolumeCurve->pPoints[bLinear ? 1 : 5].Distance = 1.0f;
		m_xa2Emitter.pVolumeCurve->pPoints[bLinear ? 1 : 5].DSPSetting = 0.0f;
	}

	if (!bLinear)
	{
		// Set the second point of the curve.
		m_xa2Emitter.pVolumeCurve->pPoints[1].Distance = fNormMin;
		m_xa2Emitter.pVolumeCurve->pPoints[1].DSPSetting = 1.0f;

		// The next three points are calculated to give the sound a rough logarithmic falloff.
		FXD::F32 fDistStep = (1.0f - fNormMin) / 4.0f;
		for (FXD::U32 n1 = 0; n1 < 3; n1++)
		{
			FXD::U32 nIndex = 2 + n1;
			FXD::F32 fDist = fNormMin + (fDistStep * (FXD::F32)(n1 + 1));
			m_xa2Emitter.pVolumeCurve->pPoints[nIndex].Distance = fDist;
			m_xa2Emitter.pVolumeCurve->pPoints[nIndex].DSPSetting = 1.0f - Math::Log10F(fDist * 10.0f);
		}
	}
}
#endif //IsSndXAudio2()