// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndXAudio2()
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/SndSystem.h"
#include "FXDEngine/SND/XA2/EffectXA2.h"
#include "FXDEngine/SND/XA2/SndAdapterXA2.h"
#include "FXDEngine/SND/XA2/SndNodeXA2.h"

using namespace FXD;
using namespace SND;

// ------
// XA2
// -
// ------
namespace XA2
{
	bool createReverb(const SND::ISndAdapter* pSndAdapter, SND::IEffect* pEffect)
	{
		if (pSndAdapter->is_vaild())
		{
			SND::IEffectXA2* pEffectXA2 = (SND::IEffectXA2*)pEffect;
			if (pEffectXA2->m_pReverb == nullptr)
			{
				DWORD nFlags = 0;
#if defined(FXD_XAUDIO2_JUNE_2010_SDK) && defined(FXD_DEBUG)
				nFlags |= XAUDIO2_DEBUG_ENGINE;
#endif
				if (XA2ResultCheckFail(XAudio2CreateReverb(&pEffectXA2->m_pReverb, nFlags)))
				{
					return false;
				}
			}
		}
		return true;
	}

	void shutdownReverb(const SND::ISndAdapter* pSndAdapter, SND::IEffect* pEffect)
	{
		if (pSndAdapter->is_vaild())
		{
			SND::IEffectXA2* pEffectXA2 = static_cast< SND::IEffectXA2* >(pEffect);
			if (pEffectXA2->m_pReverb != nullptr)
			{
				pEffectXA2->m_pReverb->Release();
				pEffectXA2->m_pReverb = nullptr;
			}
		}
	}
} //namespace XA2

// ------
// IEffectChainXA2
// - 
// ------
IEffectXA2::IEffectXA2(const SND::EFFECT_DESC& effectDesc)
	: IEffect(effectDesc)
	, m_pReverb(nullptr)
{
}
IEffectXA2::~IEffectXA2(void)
{
	PRINT_COND_ASSERT((m_pReverb == nullptr), "SND: m_pReverb was not cleaned up before shutdown");
}

// ------
// IEffectChainXA2
// - 
// ------
IEffectChainXA2::IEffectChainXA2(const SND::ISndAdapter* pSndAdapter, SND::INodeMixer* pNodeMixer)
	: IEffectChain(pSndAdapter, pNodeMixer)
{
}
IEffectChainXA2::~IEffectChainXA2(void)
{
}

bool IEffectChainXA2::_create_effect_chain(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	return true;
}

bool IEffectChainXA2::_release_effect_chain(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	fxd_for(auto& effect, m_effects)
	{
		effect->destroyed(true);
	}
	_recreate();
	return true;
}

void IEffectChainXA2::_add_effects(const Container::Vector< SND::EFFECT_DESC >& effectDescs)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	fxd_for(auto& effectDesc, effectDescs)
	{
		m_effects.push_back(std::make_shared< IEffectXA2 >(effectDesc));
	}
	m_bNeedsRecreate = true;
}

void IEffectChainXA2::_enable_effect(FXD::U16 nSlot, bool bEnable)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	INodeDataXA2* pNodeData = (INodeDataXA2*)m_pNodeMixer->get_node_data().get();

	m_effects[nSlot]->effect_desc().Enabled = bEnable;
	if (bEnable)
	{
		pNodeData->m_pVoice->EnableEffect(nSlot);
	}
	else
	{
		pNodeData->m_pVoice->DisableEffect(nSlot);
	}
}

bool IEffectChainXA2::_is_effect_enabled(FXD::U16 nSlot) const
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	bool bState = m_effects[nSlot]->effect_desc().Enabled;
	return bState;
}

void IEffectChainXA2::_update(FXD::F32 /*dt*/)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	if (m_bNeedsRecreate)
	{
		_recreate();
	}
	if ((!m_bNeedsRecreate) && (m_bNeedsUpdate))
	{
		FXD::U32 nCount = 0;
		fxd_for(auto& effect, m_effects)
		{
			XAUDIO2FX_REVERB_I3DL2_PARAMETERS xa2I3DL2Parameters = {};
			Memory::MemZero_T(xa2I3DL2Parameters);
			xa2I3DL2Parameters.WetDryMix = 100.0f;
			xa2I3DL2Parameters.Room = (INT32)SND::Funcs::VolumeToMilliBels(effect->effect_desc().Volume * effect->effect_desc().Gain, 0);
			xa2I3DL2Parameters.RoomHF = (INT32)SND::Funcs::VolumeToMilliBels(effect->effect_desc().GainHF, -45);
			xa2I3DL2Parameters.DecayTime = effect->effect_desc().DecayTime;
			xa2I3DL2Parameters.DecayHFRatio = effect->effect_desc().DecayHFRatio;
			xa2I3DL2Parameters.Reflections = (INT32)SND::Funcs::VolumeToMilliBels(effect->effect_desc().ReflectionsGain, 1000);
			xa2I3DL2Parameters.ReflectionsDelay = effect->effect_desc().ReflectionsDelay;
			xa2I3DL2Parameters.Reverb = (INT32)SND::Funcs::VolumeToMilliBels(effect->effect_desc().LateGain, 2000);
			xa2I3DL2Parameters.ReverbDelay = effect->effect_desc().LateDelay;
			xa2I3DL2Parameters.Diffusion = effect->effect_desc().Diffusion * 100.0f;
			xa2I3DL2Parameters.Density = effect->effect_desc().Density * 100.0f;
			xa2I3DL2Parameters.RoomRolloffFactor = effect->effect_desc().RoomRolloffFactor;
			xa2I3DL2Parameters.HFReference = DEFAULT_HIGH_FREQUENCY;

			XAUDIO2FX_REVERB_PARAMETERS xa2ReverbParameters = {};
			ReverbConvertI3DL2ToNative(&xa2I3DL2Parameters, &xa2ReverbParameters);

			INodeDataXA2* pNodeData = (INodeDataXA2*)m_pNodeMixer->get_node_data().get();
			if (XA2ResultCheckFail(pNodeData->m_pVoice->SetEffectParameters(nCount, &xa2ReverbParameters, sizeof(xa2ReverbParameters))))
			{
				return;
			}
			nCount++;
		}
		m_bNeedsUpdate = false;
	}
}

void IEffectChainXA2::_recreate(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	INodeDataXA2* pNodeData = (INodeDataXA2*)m_pNodeMixer->get_node_data().get();

	// 1. Stop the current effect Chain
	if (XA2ResultCheckFail(pNodeData->m_pVoice->SetEffectChain(nullptr)))
	{
		return;
	}

	// 2. Remove destroyed effects
	fxd_for_itr(itr, m_effects)
	{
		if ((*itr)->destroyed())
		{
			::XA2::shutdownReverb(m_pSndAdapter, (*itr).get());
			itr = m_effects.erase(itr);
		}
	}

	if (m_effects.not_empty())
	{
		// 3. Create the new effects
		fxd_for(auto& effect, m_effects)
		{
			::XA2::createReverb(m_pSndAdapter, effect.get());
		}

		// 4. Create descriptors for the effect Chain
		XAUDIO2_EFFECT_DESCRIPTOR* pDescriptors = FXD_NEW_ARRAY(XAUDIO2_EFFECT_DESCRIPTOR, m_effects.size());
		FXD::U32 nCount = 0;
		fxd_for(auto& effect, m_effects)
		{
			SND::IEffectXA2* pEffectXA2 = (SND::IEffectXA2*)effect.get();
			pDescriptors[nCount].InitialState = effect->effect_desc().Enabled;
			pDescriptors[nCount].OutputChannels = m_pNodeMixer->get_node_desc().OutChannels;
			pDescriptors[nCount].pEffect = pEffectXA2->m_pReverb;
			nCount++;
		}

		// 5. Set current effect Chain
		XAUDIO2_EFFECT_CHAIN xa2EffectChain = {};
		xa2EffectChain.EffectCount = m_effects.size();
		xa2EffectChain.pEffectDescriptors = pDescriptors;
		if (XA2ResultCheckFail(pNodeData->m_pVoice->SetEffectChain(&xa2EffectChain)))
		{
			FXD_SAFE_DELETE(pDescriptors);
			return;
		}
		FXD_SAFE_DELETE(pDescriptors);
		m_bNeedsUpdate = true;
	}
	m_bNeedsRecreate = false;
	return;
}
#endif //IsSndXAudio2()