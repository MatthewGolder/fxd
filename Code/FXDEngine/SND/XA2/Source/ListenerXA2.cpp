// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndXAudio2()
#include "FXDEngine/SND/SndJPThread.h"
#include "FXDEngine/SND/XA2/ListenerXA2.h"
#include "FXDEngine/SND/XA2/SndAdapterXA2.h"
#include "FXDEngine/SND/XA2/SndNodeXA2.h"

using namespace FXD;
using namespace SND;

// ------
// IListenerXA2
// - 
// ------
IListenerXA2::IListenerXA2(const SND::ISndAdapter* pSndAdapter)
	: IListener(pSndAdapter)
{
	Memory::MemZero_T(m_xa2Listener);
}

IListenerXA2::~IListenerXA2(void)
{
}

bool IListenerXA2::_create(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	// 1.
	DWORD dwChannelMask;
	UINT32 nChannels;
#if defined(FXD_XAUDIO2_JUNE_2010_SDK)
	SND::ISndAdapterXA2* pSndAdapter = (SND::ISndAdapterXA2*)(m_pSndAdapter);
	XAUDIO2_DEVICE_DETAILS xa2DeviceDetails = {};
	if (XA2ResultCheckFail(pSndAdapter->m_pEngine->GetDeviceDetails(0, &xa2DeviceDetails)))
	{
		PRINT_WARN << "SND: XAudio2Create() failed";
		return false;
	}
	dwChannelMask = xa2DeviceDetails.OutputFormat.dwChannelMask;
	nChannels = xa2DeviceDetails.OutputFormat.Format.nChannels;
#else
	SND::INodeDataXA2* pNodeData = (SND::INodeDataXA2*)m_pSndAdapter->master_node()->get_node_data().get();
	((IXAudio2MasteringVoice*)pNodeData->m_pVoice)->GetChannelMask(&dwChannelMask);

	XAUDIO2_VOICE_DETAILS xa2VoiceDetails = {};
	((IXAudio2MasteringVoice*)pNodeData->m_pVoice)->GetVoiceDetails(&xa2VoiceDetails);
	nChannels = xa2VoiceDetails.InputChannels;
#endif

	// 2.
	X3DAudioInitialize(dwChannelMask, X3DAUDIO_SPEED_OF_SOUND, m_pX3DInstance);

	// 3.
	set_position(Math::Vector3F::kZero);
	set_up(Math::Vector3F::kUnitY);
	set_forward(Math::Vector3F::kUnitZ);
	set_velocity(Math::Vector3F::kZero);
	set_distance_model(E_SndDistanceModel::Linear);
	set_doppler_factor(1.0f);
	set_rolloff_factor(1.0f);
	return true;
}

bool IListenerXA2::_release(void)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	return true;
}

void IListenerXA2::_update(FXD::F32 /*dt*/)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	{
		Thread::RWLockData< _ListenerData >::ScopedWriteLock wLock(m_listenerData);
		if (wLock->m_eDistanceModel.is_dirty())
		{
			_set_distance_model(wLock->m_eDistanceModel);
		}
		if (wLock->m_fDopplerFactor.is_dirty())
		{
			_set_doppler_factor(wLock->m_fDopplerFactor);
		}
		if (wLock->m_fRolloffFactor.is_dirty())
		{
			_set_rolloff_factor(wLock->m_fRolloffFactor);
		}
	}

	Math::Vector3F vPos = position();
	Math::Vector3F vUp = up();
	Math::Vector3F vForward = forward();
	Math::Vector3F vVelocity = velocity();

	m_xa2Listener.Position.x = vPos.x();
	m_xa2Listener.Position.y = vPos.y();
	m_xa2Listener.Position.z = vPos.z();
	m_xa2Listener.OrientFront.x = vForward.x();
	m_xa2Listener.OrientFront.y = vForward.y();
	m_xa2Listener.OrientFront.z = vForward.z();
	m_xa2Listener.OrientTop.x = vUp.x();
	m_xa2Listener.OrientTop.y = vUp.y();
	m_xa2Listener.OrientTop.z = vUp.z();
	m_xa2Listener.Velocity.x = vVelocity.x();
	m_xa2Listener.Velocity.y = vVelocity.y();
	m_xa2Listener.Velocity.z = vVelocity.z();

	/*
	Memory::MemCopy(&m_xa2ListenerCone, &X3DAudioDefault_DirectionalCone, sizeof(m_xa2ListenerCone));
	m_xa2ListenerCone.InnerAngle = 1.4f;
	m_xa2ListenerCone.OuterAngle = (2.0f * 1.4f);
	m_listener.pCone = &m_xa2ListenerCone;
	*/
}

void IListenerXA2::_set_distance_model(SND::E_SndDistanceModel eDistanceModel)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	Thread::RWLockData< _ListenerData >::ScopedWriteLock wLock(m_listenerData);

	wLock->m_eDistanceModel = eDistanceModel;
	wLock->m_eDistanceModel.mark_clean();
}

void IListenerXA2::_set_doppler_factor(FXD::F32 fFactor)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	Thread::RWLockData< _ListenerData >::ScopedWriteLock wLock(m_listenerData);

	wLock->m_fDopplerFactor = fFactor;
	wLock->m_fDopplerFactor.mark_clean();
}

void IListenerXA2::_set_rolloff_factor(FXD::F32 fFactor)
{
	PRINT_COND_ASSERT((!SND::Funcs::IsSndThreadRestricted()), "SND: Not allowed on current thread");

	Thread::RWLockData< _ListenerData >::ScopedWriteLock wLock(m_listenerData);

	wLock->m_fRolloffFactor = fFactor;
	wLock->m_fRolloffFactor.mark_clean();
}
#endif //IsSndXAudio2()