// Creator - MatthewGolder
#include "FXDEngine/SND/Types.h"

#if IsSndXAudio2()
#include "FXDEngine/SND/XA2/SndApiXA2.h"
#include "FXDEngine/SND/XA2/SndAdapterXA2.h"

using namespace FXD;
using namespace SND;

// ------
// ISndApiXA2
// - 
// ------
ISndApiXA2::ISndApiXA2(void)
	: ISndApi(SND::E_SndApi::XAudio2)
#if defined(FXD_XAUDIO2_JUNE_2010_SDK)
#if defined(FXD_DEBUG)
	, m_XAudioDLL(nullptr)
#endif
#endif
{
}

ISndApiXA2::~ISndApiXA2(void)
{
}

bool ISndApiXA2::create_api(void)
{
	// 1. Create the list of adapters
	_create_adapter_list();

	bool bRetVal = true;
	// 2. Xaudio2 2.7 fix
#if defined(FXD_XAUDIO2_JUNE_2010_SDK)
#if defined(FXD_DEBUG)
	m_XAudioDLL = LoadLibraryExW(L"XAudioD2_7.DLL", nullptr, 0x00000800 /*LOAD_LIBRARY_SEARCH_SYSTEM32*/);
#else
	m_XAudioDLL = LoadLibraryExW(L"XAudio2_7.DLL", nullptr, 0x00000800 /*LOAD_LIBRARY_SEARCH_SYSTEM32*/);
#endif
	if (m_XAudioDLL == nullptr)
	{
		PRINT_WARN << "SND: m_XAudioDLL failed to load";
		bRetVal = false;
	}
#endif
	return bRetVal;
}

bool ISndApiXA2::release_api(void)
{
#if defined(FXD_XAUDIO2_JUNE_2010_SDK)
	if (m_XAudioDLL != nullptr)
	{
		FreeLibrary(m_XAudioDLL);
		m_XAudioDLL = nullptr;
	}
#endif
	return true;
}

void ISndApiXA2::_create_adapter_list(void)
{
	// Create a temp XAudio object for device enumeration.
	IXAudio2* pEngine = nullptr;

	// 1. Initialize Stuff
	if (XA2ResultCheckFail(CoInitializeEx(NULL, COINIT_MULTITHREADED)))
	{
		PRINT_WARN << "SND: CoInitializeEx() failed";
		return;
	}

	// 2. Create the audio engine
	DWORD nCreateFlags = 0;
#if defined(FXD_XAUDIO2_JUNE_2010_SDK) && defined(FXD_DEBUG)
	nCreateFlags |= XAUDIO2_DEBUG_ENGINE;
#endif

	if (XA2ResultCheckFail(XAudio2Create(&pEngine, nCreateFlags, XAUDIO2_DEFAULT_PROCESSOR)))
	{
		PRINT_WARN << "SND: XAudio2Create() failed";
		return;
	}

	// Add the adapters to the info list.
#if defined(FXD_XAUDIO2_JUNE_2010_SDK)
	UINT32 nDeviceCount = 0;
	if (XA2ResultCheckFail(pEngine->GetDeviceCount(&nDeviceCount)))
	{
		PRINT_ASSERT << "SND: GetDeviceCount() failed";
	}

	for (UINT32 n1 = 0; n1 < nDeviceCount; ++n1)
	{
		XAUDIO2_DEVICE_DETAILS xa2DeviceDetails = {};
		if (XA2ResultCheckFail(pEngine->GetDeviceDetails(n1, &xa2DeviceDetails)))
		{
			continue;
		}
		// Add a adapter to the info list.
		SND::SndAdapterDescXA2 info = std::make_shared< ISndAdapterDescXA2 >(n1);
		info->driver(UTF_8("XAudio"));
		info->isDefault((xa2DeviceDetails.Role == GlobalDefaultDevice));
		info->name(Core::StringBuilder8::to_string((const FXD::UTF16*)xa2DeviceDetails.DisplayName));
		info->has_hardware(false);
		info->max_sources(64);
		info->m_nDeviceIndex = n1;
		info->m_role = xa2DeviceDetails.Role;
		m_adapterDescs[info->name()] = info;
	}
#else
	IXAudio2MasteringVoice* pMasterVoice = nullptr;
	WCHAR* pPreferredDevice = nullptr;
	if (XA2ResultCheckFail(pEngine->CreateMasteringVoice(&pMasterVoice, XAUDIO2_DEFAULT_CHANNELS, XAUDIO2_DEFAULT_SAMPLERATE, 0, pPreferredDevice, nullptr, AudioCategory_Other)))
	{
		PRINT_ASSERT << "SND: CreateMasteringVoice() failed";
	}

	XAUDIO2_VOICE_DETAILS vdetails = {};
	pMasterVoice->GetVoiceDetails(&vdetails);

	SND::SndAdapterDescXA2 info = std::make_shared< ISndAdapterDescXA2 >(0);
	info->driver(UTF_8("XAudio"));
	info->isDefault(true);
	//info->name(Core::StringBuilder8::to_string((const FXD::UTF16*)xa2DeviceDetails.DisplayName));
	info->has_hardware(false);
	info->max_sources(64);
	info->m_nDeviceIndex = 0;
	m_adapterDescs[info->name()] = info;

	if (pMasterVoice != nullptr)
	{
		pMasterVoice->DestroyVoice();
		pMasterVoice = nullptr;
	}
#endif

	// We're done with XAudio for now.
	if (pEngine != nullptr)
	{
		pEngine->Release();
		pEngine = nullptr;
	}
}
#endif //IsSndXAudio2()