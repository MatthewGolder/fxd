// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_XA2_VOICEXA2_H
#define FXDENGINE_SND_XA2_VOICEXA2_H

#include "FXDEngine/SND/Types.h"

#if IsSndXAudio2()
#include "FXDEngine/SND/Voice.h"
#include "FXDEngine/SND/XA2/XAudio2.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// FXDBUFFERXA2
		// -
		// ------
		class FXDBUFFERXA2
		{
		public:
			enum class State
			{
				Free,
				Playing,
				Finished,
				Count,
				Unknown = 0xffff
			};

		public:
			FXDBUFFERXA2(void)
				: m_eState(State::Free)
				, m_nID(0)
				, m_nSampleStart(0)
				, m_nSampleOffset(0)
			{
				Memory::MemZero_T(m_xa2Buffer);
			}
			~FXDBUFFERXA2(void)
			{}

		public:
			State m_eState;
			FXD::U32 m_nID;
			FXD::U32 m_nSampleStart;
			FXD::U32 m_nSampleOffset;
			XAUDIO2_BUFFER m_xa2Buffer;
		};

		// ------
		// IVoiceSFXXA2
		// -
		// ------
		class IVoiceSFXXA2 FINAL : public SND::IVoiceSFX, public IXAudio2VoiceCallback
		{
		public:
			EXPLICIT IVoiceSFXXA2(const SND::ISndAdapter* pSndAdapter, SND::INodeVoice* pNodeVoice);
			~IVoiceSFXXA2(void);

		protected:
			bool create(const SND::VOICE_DESC& voiceDesc) FINAL;
			void release(void) FINAL;

			void play(void) FINAL;
			void pause(void) FINAL;
			void stop(void) FINAL;

			FXD::U32 tell_sample_position(void) const FINAL;

			void _update_buffers(void) FINAL;

			void _buffer_fill(void) FINAL;
			void _buffer_submit(FXD::U32 nSize) FINAL;
			void _buffers_flush(void) FINAL;
			FXD::U32 _buffer_queued_count(void) const FINAL;
			void _buffers_finished(void) FINAL;

			STDMETHOD_(void, OnVoiceProcessingPassStart) (THIS_ UINT32 /*BytesRequired*/) {}
			STDMETHOD_(void, OnVoiceProcessingPassEnd) (THIS) {}
			STDMETHOD_(void, OnStreamEnd) (THIS) {}
			STDMETHOD_(void, OnBufferStart) (THIS_ void* /*pBufferContext*/) {}
			STDMETHOD_(void, OnBufferEnd) (THIS_ void* pBufferContext)
			{
				FXDBUFFERXA2* pFXDBuffer = (FXDBUFFERXA2*)pBufferContext;
				pFXDBuffer->m_eState = FXDBUFFERXA2::State::Finished;
			}
			STDMETHOD_(void, OnLoopEnd) (THIS_ void* /*pBufferContext*/) {}
			STDMETHOD_(void, OnVoiceError) (THIS_ void* /*pBufferContext*/, HRESULT /*Error*/) {}

		protected:
			FXD::S32 m_nSamplesPlayedOffset;
			FXDBUFFERXA2 m_buffers[BufferCount];
			Container::Vector< FXDBUFFERXA2* > m_bufferQueue;
		};

		// ------
		// IVoiceMusicXA2
		// -
		// ------
		class IVoiceMusicXA2 FINAL : public SND::IVoiceMusic, public IXAudio2VoiceCallback
		{
		public:
			EXPLICIT IVoiceMusicXA2(const SND::ISndAdapter* pSndAdapter, SND::INodeVoice* pNodeVoice);
			~IVoiceMusicXA2(void);

		protected:
			bool create(const SND::VOICE_DESC& voiceDesc) FINAL;
			void release(void) FINAL;

			void play(void) FINAL;
			void pause(void) FINAL;
			void stop(void) FINAL;

			FXD::U32 tell_sample_position(void) const FINAL;

			void _update_buffers(void) FINAL;

			void _buffer_fill(void) FINAL;
			void _buffer_submit(FXD::U32 nSize) FINAL;
			void _buffers_flush(void) FINAL;
			FXD::U32 _buffer_queued_count(void) const FINAL;
			void _buffers_finished(void) FINAL;

			STDMETHOD_(void, OnVoiceProcessingPassStart) (THIS_ UINT32 /*BytesRequired*/) {}
			STDMETHOD_(void, OnVoiceProcessingPassEnd) (THIS) {}
			STDMETHOD_(void, OnStreamEnd) (THIS) {}
			STDMETHOD_(void, OnBufferStart) (THIS_ void* /*pBufferContext*/) {}
			STDMETHOD_(void, OnBufferEnd) (THIS_ void* pBufferContext)
			{
				FXDBUFFERXA2* pFXDBuffer = (FXDBUFFERXA2*)pBufferContext;
				pFXDBuffer->m_eState = FXDBUFFERXA2::State::Finished;
			}
			STDMETHOD_(void, OnLoopEnd) (THIS_ void* /*pBufferContext*/) {}
			STDMETHOD_(void, OnVoiceError) (THIS_ void* /*pBufferContext*/, HRESULT /*Error*/) {}
		
		protected:
			FXD::S32 m_nSamplesPlayedOffset;
			FXDBUFFERXA2 m_buffers[BufferCount];
			Container::Vector< FXDBUFFERXA2* > m_bufferQueue;
		};

	} //namespace SND
} //namespace FXD
#endif //IsSndXAudio2()
#endif //FXDENGINE_SND_XA2_VOICEXA2_H