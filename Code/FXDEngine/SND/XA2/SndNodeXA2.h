// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_XA2_SNDNODEXA2_H
#define FXDENGINE_SND_XA2_SNDNODEXA2_H

#include "FXDEngine/SND/Types.h"

#if IsSndXAudio2()
#include "FXDEngine/SND/SndNode.h"
#include "FXDEngine/SND/XA2/XAudio2.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// INodeDataXA2
		// -
		// ------
		class INodeDataXA2 FINAL : public SND::INodeData
		{
		public:
			INodeDataXA2(void);
			virtual ~INodeDataXA2(void);

		public:
			IXAudio2Voice* m_pVoice;
		};

		// ------
		// INodeMixerXA2
		// -
		// ------
		class INodeMixerXA2 FINAL : public SND::INodeMixer
		{
		public:
			EXPLICIT INodeMixerXA2(const SND::ISndAdapter* pSndAdapter, const SND::INodeMixer* pNodeParent);
			virtual ~INodeMixerXA2(void);

		protected:
			void _create(const SND::NODE_DESC& nodeDesc) FINAL;
			void _release(void) FINAL;

			void _update_volume(FXD::F32 dt) FINAL;
		};

		// ------
		// INodeVoiceXA2
		// -
		// ------
		class INodeVoiceXA2 FINAL : public SND::INodeVoice
		{
		public:
			EXPLICIT INodeVoiceXA2(const SND::ISndAdapter* pSndAdapter, const SND::INodeMixer* pNodeParent);
			virtual ~INodeVoiceXA2(void);

		protected:
			void _create(const SND::NODE_DESC& nodeDesc, const SND::NODE_VOICE_DESC& nodeVoiceDesc) FINAL;
			void _release(void) FINAL;

			bool _is_valid(void) const FINAL;

			void _update_volume(FXD::F32 dt) FINAL;
			void _update_frequency(FXD::F32 dt) FINAL;
		};

	} //namespace SND
} //namespace FXD
#endif //IsSndXAudio2()
#endif //FXDENGINE_SND_XA2_SNDNODEXA2_H