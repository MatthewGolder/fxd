// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_XA2_SNDADAPTERXA2_H
#define FXDENGINE_SND_XA2_SNDADAPTERXA2_H

#include "FXDEngine/SND/Types.h"

#if IsSndXAudio2()
#include "FXDEngine/SND/SndAdapter.h"
#include "FXDEngine/SND/XA2/XAudio2.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// ISndAdapterDescXA2
		// - 
		// ------
		class ISndAdapterDescXA2 FINAL : public SND::ISndAdapterDesc
		{
		public:
			ISndAdapterDescXA2(const App::DeviceIndex nDeviceID);
			~ISndAdapterDescXA2(void);

		public:
			UINT32 m_nDeviceIndex;
#if defined(FXD_XAUDIO2_JUNE_2010_SDK)
			XAUDIO2_DEVICE_ROLE m_role;
#endif
		};

		// ------
		// ISndAdapterXA2
		// -
		// ------
		class ISndAdapterXA2 FINAL : public SND::ISndAdapter
		{
		public:
			EXPLICIT ISndAdapterXA2(const App::AdapterDesc& adapterDesc, const App::IApi* pApi);
			~ISndAdapterXA2(void);

		protected:
			bool _create(void) FINAL;
			bool _release(void) FINAL;

			bool _is_vaild(void) const FINAL;

		public:
			IXAudio2* m_pEngine;
		};

	} //namespace SND
} //namespace FXD
#endif //IsSndXAudio2()
#endif //FXDENGINE_SND_XA2_SNDADAPTERXA2_H