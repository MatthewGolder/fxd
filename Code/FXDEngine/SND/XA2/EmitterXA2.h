// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_XA2_EMITTERXA2_H
#define FXDENGINE_SND_XA2_EMITTERXA2_H

#include "FXDEngine/SND/Types.h"

#if IsSndXAudio2()
#include "FXDEngine/SND/Emitter.h"
#include "FXDEngine/SND/XA2/XAudio2.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// IEmitterXA2
		// -
		// ------
		class IEmitterXA2 FINAL : public IEmitter
		{
		public:
			EXPLICIT IEmitterXA2(const SND::ISndAdapter* pSndAdapter, const SND::INodeVoice* pNodeVoice);
			~IEmitterXA2(void);

		protected:
			bool _create_emitter(void) FINAL;
			bool _release_emitter(void) FINAL;

			void _update(const SND::Listener& listener) FINAL;
			void _update_distance_model(void) FINAL;

		public:
			X3DAUDIO_EMITTER m_xa2Emitter;
		};

	} //namespace SND
} //namespace FXD
#endif //IsSndXAudio2()
#endif //FXDENGINE_SND_XA2_EMITTERXA2_H