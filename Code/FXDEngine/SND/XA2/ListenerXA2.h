// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SND_XA2_LISTENERXA2_H
#define FXDENGINE_SND_XA2_LISTENERXA2_H

#include "FXDEngine/SND/Types.h"

#if IsSndXAudio2()
#include "FXDEngine/SND/Listener.h"
#include "FXDEngine/SND/XA2/XAudio2.h"

namespace FXD
{
	namespace SND
	{
		// ------
		// IListenerXA2
		// -
		// ------
		class IListenerXA2 FINAL : public IListener
		{
		public:
			EXPLICIT IListenerXA2(const SND::ISndAdapter* pSndAdapter);
			~IListenerXA2(void);

		private:
			bool _create(void) FINAL;
			bool _release(void) FINAL;

			void _update(FXD::F32 dt) FINAL;

			void _set_distance_model(SND::E_SndDistanceModel eDistanceModel) FINAL;
			void _set_doppler_factor(FXD::F32 fFactor) FINAL;
			void _set_rolloff_factor(FXD::F32 fFactor) FINAL;

		public:
			X3DAUDIO_HANDLE m_pX3DInstance;
			X3DAUDIO_LISTENER m_xa2Listener;
			//X3DAUDIO_CONE m_xa2ListenerCone;
		};

	} //namespace SND
} //namespace FXD
#endif //IsSndXAudio2()
#endif //FXDENGINE_SND_XA2_LISTENERXA2_H