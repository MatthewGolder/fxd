// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_MATRIX4_H
#define FXDENGINE_MATH_MATRIX4_H

#include "FXDEngine/Math/Types.h"
#include "FXDEngine/Core/StreamOut.h"

namespace FXD
{
	namespace Math
	{
		// ------
		// Matrix4F
		// -
		// ------
		class Matrix4F
		{
		public:
			friend class Matrix3F;

		public:
			static const Matrix4F kZero;
			static const Matrix4F kIdentity;

		public:
			Matrix4F(void);
			Matrix4F(FXD::F32 m00, FXD::F32 m01, FXD::F32 m02, FXD::F32 m03, FXD::F32 m10, FXD::F32 m11, FXD::F32 m12, FXD::F32 m13, FXD::F32 m20, FXD::F32 m21, FXD::F32 m22, FXD::F32 m23, FXD::F32 m30, FXD::F32 m31, FXD::F32 m32, FXD::F32 m33);
			Matrix4F(const Matrix4F& mat);
			Matrix4F(const Matrix3F& mat);
			~Matrix4F(void);

			FXD::F32 operator()(FXD::U32 nRow, FXD::U32 nCol) const;

			friend bool operator==(const Matrix4F& lhs, const Matrix4F& rhs) NOEXCEPT;
			friend bool operator!=(const Matrix4F& lhs, const Matrix4F& rhs) NOEXCEPT;

			Matrix4F operator+(const Matrix4F& rhs) const;
			Matrix4F operator-(const Matrix4F& rhs) const;
			Matrix4F operator*(const Matrix4F& rhs) const;
			Vector3F operator*(const Vector3F& rhs) const;
			Vector4F operator*(const Vector4F& rhs) const;
			Matrix4F& operator+=(const Matrix4F& rhs);
			Matrix4F& operator-=(const Matrix4F& rhs);
			Matrix4F& operator*=(const Matrix4F& rhs);
			Matrix4F operator-() const;

			FXD::F32 detrament(void) const;
			void negate(void);
			void transpose(void);
			void inverse(void);
			void set_row(FXD::U32 nCol, const Vector4F& vec);
			void set_row(FXD::U32 nRow, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ, FXD::F32 fW);
			void set_column(FXD::U32 nCol, const Vector4F& vec);
			void set_column(FXD::U32 nCol, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ, FXD::F32 fW);
			void set_translation(const Vector3F& vec);
			void set_translation(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			void set_right(const Vector3F& vec);
			void set_right(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			void set_up(const Vector3F& vec);
			void set_up(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			void set_forward(const Vector3F& vec);
			void set_forward(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			void set_scale(const Vector3F& vec);
			void set_scale(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			void set_rotationX(FXD::F32 fAngleX);
			void set_rotationY(FXD::F32 fAngleY);
			void set_rotationZ(FXD::F32 fAngleZ);
			void set_rotation_pitch_yaw_roll(Math::Radian rPitch, Math::Radian rYaw, Math::Radian rRoll, Math::EulerAngleOrder eOrder = Math::EulerAngleOrder::XYZ);
			void set_rotation_pitch_yaw_roll(const Vector3F& angle, Math::EulerAngleOrder eOrder = Math::EulerAngleOrder::XYZ);
			void set_rotation_quaternion(const QuaternionF& quat);
			void trs(const Vector3F& translation, const QuaternionF& rotation, const Vector3F& scale);
			void lookat_lh(const Vector3F& eye, const Vector3F& forward, const Vector3F& up);
			void perspective_lh(FXD::F32 fViewWidth, FXD::F32 fViewHeight, FXD::F32 fNearZ, FXD::F32 fFarZ);
			void perspective_fov_lh(FXD::F32 fFOVAngleY, FXD::F32 fAspectHByW, FXD::F32 fNearZ, FXD::F32 fFarZ);
			void orthographic_lh(FXD::F32 fViewWidth, FXD::F32 fViewHeight, FXD::F32 fNearZ, FXD::F32 fFarZ);

			Matrix4F get_negate(void) const;
			Matrix4F get_transpose(void) const;
			Matrix4F get_inverse(void) const;
			Vector4F get_row(FXD::U32 nRow) const;
			Vector4F get_column(FXD::U32 nCol) const;
			Vector3F get_translation(void) const;
			Vector3F get_right(void) const;
			Vector3F get_up(void) const;
			Vector3F get_forward(void) const;
			Vector3F get_scale(void) const;

			static void set_negate(Matrix4F& dst);
			static void set_transpose(Matrix4F& dst);
			static void set_inverse(Matrix4F& dst);
			static void set_translation(Matrix4F& dst, const Vector3F& vec);
			static void set_translation(Matrix4F& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			static void set_right(Matrix4F& dst, const Vector3F& vec);
			static void set_right(Matrix4F& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			static void set_up(Matrix4F& dst, const Vector3F& vec);
			static void set_up(Matrix4F& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			static void set_forward(Matrix4F& dst, const Vector3F& vec);
			static void set_forward(Matrix4F& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			static void set_scale(Matrix4F& dst, const Vector3F& vec);
			static void set_scale(Matrix4F& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			static void set_rotationX(Matrix4F& dst, FXD::F32 fAngleX);
			static void set_rotationY(Matrix4F& dst, FXD::F32 fAngleY);
			static void set_rotationZ(Matrix4F& dst, FXD::F32 fAngleZ);
			static void set_rotation_pitch_yaw_roll(Matrix4F& dst, Math::Radian rPitch, Math::Radian rYaw, Math::Radian rRoll, Math::EulerAngleOrder eOrder = Math::EulerAngleOrder::XYZ);
			static void set_rotation_pitch_yaw_roll(Matrix4F& dst, const Vector3F& angle, Math::EulerAngleOrder eOrder = Math::EulerAngleOrder::XYZ);
			static void set_rotation_quaternion(Matrix4F& dst, const QuaternionF& quat);
			static void set_trs(Matrix4F& dst, const Vector3F& translation, const QuaternionF& rotation, const Vector3F& scale);
			static void set_lookat_lh(Matrix4F& dst, const Vector3F& eye, const Vector3F& forward, const Vector3F& up);
			static void set_perspective_lh(Matrix4F& dst, FXD::F32 fViewWidth, FXD::F32 fViewHeight, FXD::F32 fNearZ, FXD::F32 fFarZ);
			static void set_perspective_fov_lh(Matrix4F& dst, FXD::F32 fFOVAngleY, FXD::F32 fAspectHByW, FXD::F32 fNearZ, FXD::F32 fFarZ);
			static void set_orthographic_lh(Matrix4F& dst, FXD::F32 fViewWidth, FXD::F32 fViewHeight, FXD::F32 fNearZ, FXD::F32 fFarZ);

			static FXD::F32 get_detrament(const Matrix4F& mat);
			static Matrix4F get_negate(const Matrix4F& mat);
			static Matrix4F get_transpose(const Matrix4F& mat);
			static Matrix4F get_inverse(const Matrix4F& mat);
			static Vector4F get_row(const Matrix4F& mat, FXD::U32 nRow);
			static Vector4F get_column(const Matrix4F& mat, FXD::U32 nCol);
			static Vector3F get_translation(const Matrix4F& mat);
			static Vector3F get_right(const Matrix4F& mat);
			static Vector3F get_up(const Matrix4F& mat);
			static Vector3F get_forward(const Matrix4F& mat);
			static Vector3F get_scale(const Matrix4F& mat);

			static Matrix4F create_translation(const Vector3F& vec);
			static Matrix4F create_translation(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			static Matrix4F create_up(const Vector3F& vec);
			static Matrix4F create_up(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			static Matrix4F create_forward(const Vector3F& vec);
			static Matrix4F create_forward(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			static Matrix4F create_scale(const Vector3F& vec);
			static Matrix4F create_scale(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			static Matrix4F create_rotationX(FXD::F32 fAngleX);
			static Matrix4F create_rotationY(FXD::F32 fAngleY);
			static Matrix4F create_rotationZ(FXD::F32 fAngleZ);
			static Matrix4F create_rotation_pitch_yaw_roll(Math::Radian rPitch, Math::Radian rYaw, Math::Radian rRoll, Math::EulerAngleOrder eOrder = Math::EulerAngleOrder::XYZ);
			static Matrix4F create_rotation_pitch_yaw_roll(const Vector3F& angle, Math::EulerAngleOrder eOrder = Math::EulerAngleOrder::XYZ);
			static Matrix4F create_rotation_quaternion(const QuaternionF& quat);
			static Matrix4F create_trs(const Vector3F& translation, const QuaternionF& rotation, const Vector3F& scale);
			static Matrix4F create_lookat_lh(const Vector3F& eye, const Vector3F& forward, const Vector3F& up);
			static Matrix4F create_perspective_lh(FXD::F32 fViewWidth, FXD::F32 fViewHeight, FXD::F32 fNearZ, FXD::F32 fFarZ);
			static Matrix4F create_perspective_fov_lh(FXD::F32 fFOVAngleY, FXD::F32 fAspectHByW, FXD::F32 fNearZ, FXD::F32 fFarZ);
			static Matrix4F create_orthographic_lh(FXD::F32 fViewWidth, FXD::F32 fViewHeight, FXD::F32 fNearZ, FXD::F32 fFarZ);

		private:
			_matrix4Type m_data;
		};

		// Comparison operators
		// Equality
		inline bool operator==(const Matrix4F& lhs, const Matrix4F& rhs) NOEXCEPT
		{
			return Mat4Compare(lhs.m_data, rhs.m_data);
		}

		// Inequality
		inline bool operator!=(const Matrix4F& lhs, const Matrix4F& rhs) NOEXCEPT
		{
			return !Mat4Compare(lhs.m_data, rhs.m_data);
		}

		// Core::IStreamOut
		inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, Matrix4F const& rhs)
		{
			return ostr
				<< rhs(0, 0) << ' ' << rhs(0, 1) << ' ' << rhs(0, 2) << ' ' << rhs(0, 3) << ' '
				<< rhs(1, 0) << ' ' << rhs(1, 1) << ' ' << rhs(1, 2) << ' ' << rhs(1, 3) << ' '
				<< rhs(2, 0) << ' ' << rhs(2, 1) << ' ' << rhs(2, 2) << ' ' << rhs(2, 3) << ' '
				<< rhs(3, 0) << ' ' << rhs(3, 1) << ' ' << rhs(3, 2) << ' ' << rhs(3, 3);
		}

		/*
		// ------
		// Matrix4
		// -
		// ------
		class Matrix4
		{
		public:
			static const Matrix4 kZero;
			static const Matrix4 kIdentity;

		public:
			Matrix4(void);
			Matrix4(FXD::F32 m00, FXD::F32 m01, FXD::F32 m02, FXD::F32 m03, FXD::F32 m10, FXD::F32 m11, FXD::F32 m12, FXD::F32 m13, FXD::F32 m20, FXD::F32 m21, FXD::F32 m22, FXD::F32 m23, FXD::F32 m30, FXD::F32 m31, FXD::F32 m32, FXD::F32 m33);
			Matrix4(const Matrix4& mat);
			EXPLICIT Matrix4(const Matrix3& mat3);
			~Matrix4(void);

			FXD::F32* operator[](FXD::U32 nRow);
			const FXD::F32* operator[](FXD::U32 nRow) const;

			Matrix4 operator+(const Matrix4& rhs) const;
			Matrix4 operator-(const Matrix4& rhs) const;
			Matrix4 operator*(const Matrix4& rhs) const;
			Matrix4 operator*(FXD::F32 rhs) const;

			bool operator==(const Matrix4& rhs) const;
			bool operator!=(const Matrix4& rhs) const;

			Vector3F get_up(void) const;
			Vector3F get_forward(void) const;
			Vector3F get_translation(void) const;

			void set_column(FXD::U32 nID, const Vector4F& column);
			void set_row(FXD::U32 nID, const Vector4F& column);
			void set_up(const Vector3F& v);
			void set_forward(const Vector3F& v);
			void set_position(const Vector3F& v);

			void swap(Matrix4& rhs);

			bool isAffine(void) const;																										// Check whether or not the matrix is affine matrix.

			void extract3x3Matrix(Matrix3& m3x3) const;																				// Extracts the rotation/scaling part of the matrix as a 3x3 matrix.
			Matrix4 transpose(void) const;																									// Returns a transpose of the matrix (switched columns and rows).
			Matrix4 adjoint(void) const;																									// Calculates the adjoint of the matrix.
			FXD::F32 determinant(void) const;																								// Calculates the determinant of the matrix.
			FXD::F32 determinant3x3(void) const;																							// Calculates the determinant of the 3x3 sub-matrix.
			Matrix4 inverse(void) const;																									// Calculates the inverse of the matrix.
			Matrix4 inverseAffine(void) const;																							// Returns the inverse of the affine matrix.
			Matrix4 concatenateAffine(const Matrix4& other) const;																	// Concatenate two affine matrices.
			void decomposition(Vector3F& position, Quaternion& rotation, Vector3F& scale) const;							// Decompose a Matrix4 to translation, rotation and scale.

			void set_trs(const Vector3F& translation, const Quaternion& rotation, const Vector3F& scale);				// Creates a matrix from translation, rotation and scale.
			void setInverseTRS(const Vector3F& translation, const Quaternion& rotation, const Vector3F& scale);	// Creates a matrix from inverse translation, rotation and scale.

			Vector3F multiply(const Vector3F& v) const;											// Transform a 3D point by this matrix.
			Vector4F multiply(const Vector4F& v) const;											// Transform a 4D vector by this matrix
			Vector3F multiplyAffine(const Vector3F& v) const;									// Transform a 3D point by this matrix.
			Vector4F multiplyAffine(const Vector4F& v) const;									// Transform a 4D vector by this matrix.
			Vector3F multiplyDirection(const Vector3F& v) const;								// Transform a 3D direction by this matrix.
			Plane multiplyAffine(const Plane& p) const;											// Transform a plane by this matrix.


			void makeView(const Vector3F& position, const Quaternion& orientation, const Matrix4* PreflectMatrix = nullptr);					// Creates a view matrix and applies optional reflection.
			void makeProjectionOrtho(FXD::F32 left, FXD::F32 right, FXD::F32 top, FXD::F32 bottom, FXD::F32 fNear, FXD::F32 fFar);			// Creates an ortographic projection matrix.

			static Matrix4 translation(const Vector3F& translation);																		// Creates a 4x4 transformation matrix that performs translation.
			static Matrix4 scaling(const Vector3F& scale);																					// Creates a 4x4 transformation matrix that performs scaling.
			static Matrix4 scaling(FXD::F32 scale);																							// Creates a 4x4 transformation matrix that performs uniform scaling.
			static Matrix4 rotation(const Quaternion& rotation);																			// Creates a 4x4 transformation matrix that performs rotation.
			static Matrix4 TRS(const Vector3F& translation, const Quaternion& rotation, const Vector3F& scale);			// Creates a matrix from translation, rotation and scale.
			static Matrix4 inverseTRS(const Vector3F& translation, const Quaternion& rotation, const Vector3F& scale);	// Creates a matrix from inverse translation, rotation and scale.

		private:
			FXD::F32 m_matrix[4][4];
		};

		// ------
		// Matrix4
		// -
		// ------
		inline Matrix4::Matrix4(void)
		{
		}
		inline Matrix4::Matrix4(
			FXD::F32 m00, FXD::F32 m01, FXD::F32 m02, FXD::F32 m03,
			FXD::F32 m10, FXD::F32 m11, FXD::F32 m12, FXD::F32 m13,
			FXD::F32 m20, FXD::F32 m21, FXD::F32 m22, FXD::F32 m23,
			FXD::F32 m30, FXD::F32 m31, FXD::F32 m32, FXD::F32 m33)
		{
			m_matrix[0][0] = m00;
			m_matrix[0][1] = m01;
			m_matrix[0][2] = m02;
			m_matrix[0][3] = m03;
			m_matrix[1][0] = m10;
			m_matrix[1][1] = m11;
			m_matrix[1][2] = m12;
			m_matrix[1][3] = m13;
			m_matrix[2][0] = m20;
			m_matrix[2][1] = m21;
			m_matrix[2][2] = m22;
			m_matrix[2][3] = m23;
			m_matrix[3][0] = m30;
			m_matrix[3][1] = m31;
			m_matrix[3][2] = m32;
			m_matrix[3][3] = m33;
		}
		inline Matrix4::Matrix4(const Matrix4& mat)
		{
			//Memory::MemCopy(m_matrix, mat.m_matrix, 16 * sizeof(FXD::F32));
		}
		inline Matrix4::Matrix4(const Matrix3& mat3)
		{
			m_matrix[0][0] = mat3.m_matrix[0][0];
			m_matrix[0][1] = mat3.m_matrix[0][1];
			m_matrix[0][2] = mat3.m_matrix[0][2];
			m_matrix[0][3] = 0.0f;
			m_matrix[1][0] = mat3.m_matrix[1][0];
			m_matrix[1][1] = mat3.m_matrix[1][1];
			m_matrix[1][2] = mat3.m_matrix[1][2];
			m_matrix[1][3] = 0.0f;
			m_matrix[2][0] = mat3.m_matrix[2][0];
			m_matrix[2][1] = mat3.m_matrix[2][1];
			m_matrix[2][2] = mat3.m_matrix[2][2];
			m_matrix[2][3] = 0.0f;
			m_matrix[3][0] = 0.0f;
			m_matrix[3][1] = 0.0f;
			m_matrix[3][2] = 0.0f;
			m_matrix[3][3] = 1.0f;
		}
		inline Matrix4::~Matrix4(void)
		{
		}
		inline FXD::F32* Matrix4::operator[](FXD::U32 nRow)
		{
			PRINT_COND_ASSERT((nRow < 4), "Math:");
			return m_matrix[nRow];
		}
		inline const FXD::F32* Matrix4::operator[](FXD::U32 nRow) const
		{
			PRINT_COND_ASSERT((nRow < 4), "Math:");
			return m_matrix[nRow];
		}
		inline Matrix4 Matrix4::operator+(const Matrix4& rhs) const
		{
			Matrix4 r;
			r.m_matrix[0][0] = (m_matrix[0][0] + rhs.m_matrix[0][0]);
			r.m_matrix[0][1] = (m_matrix[0][1] + rhs.m_matrix[0][1]);
			r.m_matrix[0][2] = (m_matrix[0][2] + rhs.m_matrix[0][2]);
			r.m_matrix[0][3] = (m_matrix[0][3] + rhs.m_matrix[0][3]);

			r.m_matrix[1][0] = (m_matrix[1][0] + rhs.m_matrix[1][0]);
			r.m_matrix[1][1] = (m_matrix[1][1] + rhs.m_matrix[1][1]);
			r.m_matrix[1][2] = (m_matrix[1][2] + rhs.m_matrix[1][2]);
			r.m_matrix[1][3] = (m_matrix[1][3] + rhs.m_matrix[1][3]);

			r.m_matrix[2][0] = (m_matrix[2][0] + rhs.m_matrix[2][0]);
			r.m_matrix[2][1] = (m_matrix[2][1] + rhs.m_matrix[2][1]);
			r.m_matrix[2][2] = (m_matrix[2][2] + rhs.m_matrix[2][2]);
			r.m_matrix[2][3] = (m_matrix[2][3] + rhs.m_matrix[2][3]);

			r.m_matrix[3][0] = (m_matrix[3][0] + rhs.m_matrix[3][0]);
			r.m_matrix[3][1] = (m_matrix[3][1] + rhs.m_matrix[3][1]);
			r.m_matrix[3][2] = (m_matrix[3][2] + rhs.m_matrix[3][2]);
			r.m_matrix[3][3] = (m_matrix[3][3] + rhs.m_matrix[3][3]);
			return r;
		}
		inline Matrix4 Matrix4::operator-(const Matrix4& rhs) const
		{
			Matrix4 r;
			r.m_matrix[0][0] = (m_matrix[0][0] - rhs.m_matrix[0][0]);
			r.m_matrix[0][1] = (m_matrix[0][1] - rhs.m_matrix[0][1]);
			r.m_matrix[0][2] = (m_matrix[0][2] - rhs.m_matrix[0][2]);
			r.m_matrix[0][3] = (m_matrix[0][3] - rhs.m_matrix[0][3]);

			r.m_matrix[1][0] = (m_matrix[1][0] - rhs.m_matrix[1][0]);
			r.m_matrix[1][1] = (m_matrix[1][1] - rhs.m_matrix[1][1]);
			r.m_matrix[1][2] = (m_matrix[1][2] - rhs.m_matrix[1][2]);
			r.m_matrix[1][3] = (m_matrix[1][3] - rhs.m_matrix[1][3]);

			r.m_matrix[2][0] = (m_matrix[2][0] - rhs.m_matrix[2][0]);
			r.m_matrix[2][1] = (m_matrix[2][1] - rhs.m_matrix[2][1]);
			r.m_matrix[2][2] = (m_matrix[2][2] - rhs.m_matrix[2][2]);
			r.m_matrix[2][3] = (m_matrix[2][3] - rhs.m_matrix[2][3]);

			r.m_matrix[3][0] = (m_matrix[3][0] - rhs.m_matrix[3][0]);
			r.m_matrix[3][1] = (m_matrix[3][1] - rhs.m_matrix[3][1]);
			r.m_matrix[3][2] = (m_matrix[3][2] - rhs.m_matrix[3][2]);
			r.m_matrix[3][3] = (m_matrix[3][3] - rhs.m_matrix[3][3]);
			return r;
		}
		inline Matrix4 Matrix4::operator*(const Matrix4& rhs) const
		{
			Matrix4 r;
			r.m_matrix[0][0] = (m_matrix[0][0] * rhs.m_matrix[0][0] + m_matrix[0][1] * rhs.m_matrix[1][0] + m_matrix[0][2] * rhs.m_matrix[2][0] + m_matrix[0][3] * rhs.m_matrix[3][0]);
			r.m_matrix[0][1] = (m_matrix[0][0] * rhs.m_matrix[0][1] + m_matrix[0][1] * rhs.m_matrix[1][1] + m_matrix[0][2] * rhs.m_matrix[2][1] + m_matrix[0][3] * rhs.m_matrix[3][1]);
			r.m_matrix[0][2] = (m_matrix[0][0] * rhs.m_matrix[0][2] + m_matrix[0][1] * rhs.m_matrix[1][2] + m_matrix[0][2] * rhs.m_matrix[2][2] + m_matrix[0][3] * rhs.m_matrix[3][2]);
			r.m_matrix[0][3] = (m_matrix[0][0] * rhs.m_matrix[0][3] + m_matrix[0][1] * rhs.m_matrix[1][3] + m_matrix[0][2] * rhs.m_matrix[2][3] + m_matrix[0][3] * rhs.m_matrix[3][3]);

			r.m_matrix[1][0] = (m_matrix[1][0] * rhs.m_matrix[0][0] + m_matrix[1][1] * rhs.m_matrix[1][0] + m_matrix[1][2] * rhs.m_matrix[2][0] + m_matrix[1][3] * rhs.m_matrix[3][0]);
			r.m_matrix[1][1] = (m_matrix[1][0] * rhs.m_matrix[0][1] + m_matrix[1][1] * rhs.m_matrix[1][1] + m_matrix[1][2] * rhs.m_matrix[2][1] + m_matrix[1][3] * rhs.m_matrix[3][1]);
			r.m_matrix[1][2] = (m_matrix[1][0] * rhs.m_matrix[0][2] + m_matrix[1][1] * rhs.m_matrix[1][2] + m_matrix[1][2] * rhs.m_matrix[2][2] + m_matrix[1][3] * rhs.m_matrix[3][2]);
			r.m_matrix[1][3] = (m_matrix[1][0] * rhs.m_matrix[0][3] + m_matrix[1][1] * rhs.m_matrix[1][3] + m_matrix[1][2] * rhs.m_matrix[2][3] + m_matrix[1][3] * rhs.m_matrix[3][3]);

			r.m_matrix[2][0] = (m_matrix[2][0] * rhs.m_matrix[0][0] + m_matrix[2][1] * rhs.m_matrix[1][0] + m_matrix[2][2] * rhs.m_matrix[2][0] + m_matrix[2][3] * rhs.m_matrix[3][0]);
			r.m_matrix[2][1] = (m_matrix[2][0] * rhs.m_matrix[0][1] + m_matrix[2][1] * rhs.m_matrix[1][1] + m_matrix[2][2] * rhs.m_matrix[2][1] + m_matrix[2][3] * rhs.m_matrix[3][1]);
			r.m_matrix[2][2] = (m_matrix[2][0] * rhs.m_matrix[0][2] + m_matrix[2][1] * rhs.m_matrix[1][2] + m_matrix[2][2] * rhs.m_matrix[2][2] + m_matrix[2][3] * rhs.m_matrix[3][2]);
			r.m_matrix[2][3] = (m_matrix[2][0] * rhs.m_matrix[0][3] + m_matrix[2][1] * rhs.m_matrix[1][3] + m_matrix[2][2] * rhs.m_matrix[2][3] + m_matrix[2][3] * rhs.m_matrix[3][3]);

			r.m_matrix[3][0] = (m_matrix[3][0] * rhs.m_matrix[0][0] + m_matrix[3][1] * rhs.m_matrix[1][0] + m_matrix[3][2] * rhs.m_matrix[2][0] + m_matrix[3][3] * rhs.m_matrix[3][0]);
			r.m_matrix[3][1] = (m_matrix[3][0] * rhs.m_matrix[0][1] + m_matrix[3][1] * rhs.m_matrix[1][1] + m_matrix[3][2] * rhs.m_matrix[2][1] + m_matrix[3][3] * rhs.m_matrix[3][1]);
			r.m_matrix[3][2] = (m_matrix[3][0] * rhs.m_matrix[0][2] + m_matrix[3][1] * rhs.m_matrix[1][2] + m_matrix[3][2] * rhs.m_matrix[2][2] + m_matrix[3][3] * rhs.m_matrix[3][2]);
			r.m_matrix[3][3] = (m_matrix[3][0] * rhs.m_matrix[0][3] + m_matrix[3][1] * rhs.m_matrix[1][3] + m_matrix[3][2] * rhs.m_matrix[2][3] + m_matrix[3][3] * rhs.m_matrix[3][3]);
			return r;
		}
		inline Matrix4 Matrix4::operator*(FXD::F32 rhs) const
		{
			return Matrix4(
				(rhs * m_matrix[0][0]), (rhs * m_matrix[0][1]), (rhs * m_matrix[0][2]), (rhs * m_matrix[0][3]),
				(rhs * m_matrix[1][0]), (rhs * m_matrix[1][1]), (rhs * m_matrix[1][2]), (rhs * m_matrix[1][3]),
				(rhs * m_matrix[2][0]), (rhs * m_matrix[2][1]), (rhs * m_matrix[2][2]), (rhs * m_matrix[2][3]),
				(rhs * m_matrix[3][0]), (rhs * m_matrix[3][1]), (rhs * m_matrix[3][2]), (rhs * m_matrix[3][3])
			);
		}
		inline bool Matrix4::operator==(const Matrix4& rhs) const
		{
			if (
				(m_matrix[0][0] != rhs.m_matrix[0][0]) || (m_matrix[0][1] != rhs.m_matrix[0][1]) || (m_matrix[0][2] != rhs.m_matrix[0][2]) || (m_matrix[0][3] != rhs.m_matrix[0][3]) ||
				(m_matrix[1][0] != rhs.m_matrix[1][0]) || (m_matrix[1][1] != rhs.m_matrix[1][1]) || (m_matrix[1][2] != rhs.m_matrix[1][2]) || (m_matrix[1][3] != rhs.m_matrix[1][3]) ||
				(m_matrix[2][0] != rhs.m_matrix[2][0]) || (m_matrix[2][1] != rhs.m_matrix[2][1]) || (m_matrix[2][2] != rhs.m_matrix[2][2]) || (m_matrix[2][3] != rhs.m_matrix[2][3]) ||
				(m_matrix[3][0] != rhs.m_matrix[3][0]) || (m_matrix[3][1] != rhs.m_matrix[3][1]) || (m_matrix[3][2] != rhs.m_matrix[3][2]) || (m_matrix[3][3] != rhs.m_matrix[3][3]))
			{
				return false;
			}
			return true;
		}
		inline bool Matrix4::operator!=(const Matrix4& rhs) const
		{
			return !operator==(rhs);
		}
		inline Vector3F Matrix4::get_up(void) const
		{
			return Vector3F(m_matrix[0][1], m_matrix[1][1], m_matrix[2][1]);
		}
		inline Vector3F Matrix4::get_forward(void) const
		{
			return Vector3F(m_matrix[0][2], m_matrix[1][2], m_matrix[2][2]);
		}
		inline Vector3F Matrix4::get_translation(void) const
		{
			return Vector3F(m_matrix[0][3], m_matrix[1][3], m_matrix[2][3]);
		}
		inline void Matrix4::set_column(FXD::U32 nID, const Vector4F& column)
		{
			m_matrix[0][nID] = column.x();
			m_matrix[1][nID] = column.y();
			m_matrix[2][nID] = column.z();
			m_matrix[3][nID] = column.w();
		}
		inline void Matrix4::set_row(FXD::U32 nID, const Vector4F& column)
		{
			m_matrix[nID][0] = column.x();
			m_matrix[nID][1] = column.y();
			m_matrix[nID][2] = column.z();
			m_matrix[nID][3] = column.w();
		}
		inline void Matrix4::set_up(const Vector3F& v)
		{
			set_column(1, Vector4F(v[0], v[1], v[2], 0.0f));
		}
		inline void Matrix4::set_forward(const Vector3F& v)
		{
			set_column(2, Vector4F(v[0], v[1], v[2], 0.0f));
		}
		inline void Matrix4::set_position(const Vector3F& v)
		{
			set_column(3, Vector4F(v[0], v[1], v[2], 0.0f));
		}
		inline void Matrix4::swap(Matrix4& rhs)
		{
			FXD::STD::swap(m_matrix[0][0], rhs.m_matrix[0][0]);
			FXD::STD::swap(m_matrix[0][1], rhs.m_matrix[0][1]);
			FXD::STD::swap(m_matrix[0][2], rhs.m_matrix[0][2]);
			FXD::STD::swap(m_matrix[0][3], rhs.m_matrix[0][3]);
			FXD::STD::swap(m_matrix[1][0], rhs.m_matrix[1][0]);
			FXD::STD::swap(m_matrix[1][1], rhs.m_matrix[1][1]);
			FXD::STD::swap(m_matrix[1][2], rhs.m_matrix[1][2]);
			FXD::STD::swap(m_matrix[1][3], rhs.m_matrix[1][3]);
			FXD::STD::swap(m_matrix[2][0], rhs.m_matrix[2][0]);
			FXD::STD::swap(m_matrix[2][1], rhs.m_matrix[2][1]);
			FXD::STD::swap(m_matrix[2][2], rhs.m_matrix[2][2]);
			FXD::STD::swap(m_matrix[2][3], rhs.m_matrix[2][3]);
			FXD::STD::swap(m_matrix[3][0], rhs.m_matrix[3][0]);
			FXD::STD::swap(m_matrix[3][1], rhs.m_matrix[3][1]);
			FXD::STD::swap(m_matrix[3][2], rhs.m_matrix[3][2]);
			FXD::STD::swap(m_matrix[3][3], rhs.m_matrix[3][3]);
		}
		inline bool Matrix4::isAffine(void) const
		{
			return ((m_matrix[3][0] == 0) && (m_matrix[3][1] == 0) && (m_matrix[3][2] == 0) && (m_matrix[3][3] == 1));
		}*/

	} //namespace Math
} //namespace FXD
#endif //FXDENGINE_MATH_MATRIX4_H