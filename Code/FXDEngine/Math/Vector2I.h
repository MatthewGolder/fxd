// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_VECTOR2I_H
#define FXDENGINE_MATH_VECTOR2I_H

#include "FXDEngine/Math/Types.h"
#include "FXDEngine/Core/StreamOut.h"

namespace FXD
{
	namespace Math
	{
		// ------
		// Vector2I
		// -
		// ------
		class Vector2I
		{
		public:
			static const Vector2I kZero;
			static const Vector2I kOne;
			static const Vector2I kLeft;
			static const Vector2I kRight;
			static const Vector2I kUp;
			static const Vector2I kDown;

		public:
			Vector2I(void);
			Vector2I(FXD::S32 val);
			Vector2I(FXD::S32 x, FXD::S32 y);
			~Vector2I(void);

			FXD::S32 operator[](FXD::U32 nID) const;
			FXD::F32 operator()(FXD::U32 nID) const;

			Vector2I& operator=(const Vector2I& rhs);
			Vector2I& operator=(FXD::S32 rhs);

			friend bool operator==(const Vector2I& lhs, const Vector2I& rhs) NOEXCEPT;
			friend bool operator!=(const Vector2I& lhs, const Vector2I& rhs) NOEXCEPT;

			friend Vector2I operator+(const Vector2I& lhs, FXD::S32 rhs) NOEXCEPT;
			friend Vector2I operator+(const Vector2I& lhs, const Vector2I& rhs) NOEXCEPT;
			friend Vector2I operator-(const Vector2I& lhs, FXD::S32 rhs) NOEXCEPT;
			friend Vector2I operator-(const Vector2I& lhs, const Vector2I& rhs) NOEXCEPT;
			friend Vector2I operator*(const Vector2I& lhs, FXD::S32 rhs) NOEXCEPT;
			friend Vector2I operator*(const Vector2I& lhs, const Vector2I& rhs) NOEXCEPT;
			friend Vector2I operator/(const Vector2I& lhs, FXD::S32 rhs) NOEXCEPT;
			friend Vector2I operator/(const Vector2I& lhs, const Vector2I& rhs) NOEXCEPT;

			friend Vector2I& operator+=(Vector2I& lhs, FXD::S32 rhs) NOEXCEPT;
			friend Vector2I& operator+=(Vector2I& lhs, const Vector2I& rhs) NOEXCEPT;
			friend Vector2I& operator-=(Vector2I& lhs, FXD::S32 rhs) NOEXCEPT;
			friend Vector2I& operator-=(Vector2I& lhs, const Vector2I& rhs) NOEXCEPT;
			friend Vector2I& operator*=(Vector2I& lhs, FXD::S32 rhs) NOEXCEPT;
			friend Vector2I& operator*=(Vector2I& lhs, const Vector2I& rhs) NOEXCEPT;
			friend Vector2I& operator/=(Vector2I& lhs, FXD::S32 rhs) NOEXCEPT;
			friend Vector2I& operator/=(Vector2I& lhs, const Vector2I& rhs) NOEXCEPT;

			Vector2I operator-() const;

			FXD::S32 x(void) const;
			FXD::S32 y(void) const;
			FXD::S32 index(FXD::U32 nID);

			void set(FXD::S32 x, FXD::S32 xy);
			void x(FXD::S32 x);
			void y(FXD::S32 y);
			void index(FXD::U32 nID, FXD::S32 nVal);

			Vector2I midpoint(const Vector2I& rhs) const;

			void negate(void);
			void minimum(const Vector2I& min);
			void maximum(const Vector2I& max);
			void clamp(const Vector2I& min, const Vector2I& max);
			void abs(void);
			Vector2I get_negate(void) const;
			Vector2I get_minimum(const Vector2I& min) const;
			Vector2I get_maximum(const Vector2I& max) const;
			Vector2I get_clamp(const Vector2I& min, const Vector2I& max) const;
			Vector2I get_abs(void) const;

			static Vector2I midpoint(const Vector2I& lhs, const Vector2I& rhs);

			static void set_negate(Vector2I& vec);
			static void set_minimum(Vector2I& vec, const Vector2I& min);
			static void set_maximum(Vector2I& vec, const Vector2I& max);
			static void set_clamp(Vector2I& vec, const Vector2I& min, const Vector2I& max);
			static void set_abs(Vector2I& val);
			static Vector2I get_negate(const Vector2I& vec);
			static Vector2I get_minimum(const Vector2I& vec, const Vector2I& min);
			static Vector2I get_maximum(const Vector2I& vec, const Vector2I& max);
			static Vector2I get_clamp(const Vector2I& vec, const Vector2I& min, const Vector2I& max);
			static Vector2I get_abs(const Vector2I& val);

		private:
			_vector2IType m_data;
		};

		// Comparison operators
		// Equality
		inline bool operator==(const Vector2I& lhs, const Vector2I& rhs) NOEXCEPT
		{
			return Math::Vec2ICompare(lhs.m_data, rhs.m_data);
		}

		// Inequality
		inline bool operator!=(const Vector2I& lhs, const Vector2I& rhs) NOEXCEPT
		{
			return !Math::Vec2ICompare(lhs.m_data, rhs.m_data);
		}

		// Arithmetic operators
		// Add
		inline Vector2I operator+(const Vector2I& lhs, FXD::S32 rhs) NOEXCEPT
		{
			Vector2I out;
			Math::Vec2IAdd(out.m_data, lhs.m_data, rhs);
			return out;
		}
		inline Vector2I operator+(const Vector2I& lhs, const Vector2I& rhs) NOEXCEPT
		{
			Vector2I out;
			Math::Vec2IAdd(out.m_data, lhs.m_data, rhs.m_data);
			return out;
		}

		// Subtract
		inline Vector2I operator-(const Vector2I& lhs, FXD::S32 rhs) NOEXCEPT
		{
			Vector2I out;
			Math::Vec2ISub(out.m_data, lhs.m_data, rhs);
			return out;
		}
		inline Vector2I operator-(const Vector2I& lhs, const Vector2I& rhs) NOEXCEPT
		{
			Vector2I out;
			Math::Vec2ISub(out.m_data, lhs.m_data, rhs.m_data);
			return out;
		}

		// Multiplication
		inline Vector2I operator*(const Vector2I& lhs, FXD::S32 rhs) NOEXCEPT
		{
			Vector2I out;
			Math::Vec2IMul(out.m_data, lhs.m_data, rhs);
			return out;
		}
		inline Vector2I operator*(const Vector2I& lhs, const Vector2I& rhs) NOEXCEPT
		{
			Vector2I out;
			Vec2IMul(out.m_data, lhs.m_data, rhs.m_data);
			return out;
		}

		// Division
		inline Vector2I operator/(const Vector2I& lhs, FXD::S32 rhs) NOEXCEPT
		{
			Vector2I out;
			Math::Vec2IDiv(out.m_data, lhs.m_data, rhs);
			return out;
		}
		inline Vector2I operator/(const Vector2I& lhs, const Vector2I& rhs) NOEXCEPT
		{
			Vector2I out;
			Math::Vec2IDiv(out.m_data, lhs.m_data, rhs.m_data);
			return out;
		}

		// Assignment operators
		// Add
		inline Vector2I& operator+=(Vector2I& lhs, FXD::S32 rhs) NOEXCEPT
		{
			Math::Vec2IAdd(lhs.m_data, lhs.m_data, rhs);
			return lhs;
		}
		inline Vector2I& operator+=(Vector2I& lhs, const Vector2I& rhs) NOEXCEPT
		{
			Math::Vec2IAdd(lhs.m_data, lhs.m_data, rhs.m_data);
			return lhs;
		}

		// Subtract
		inline Vector2I& operator-=(Vector2I& lhs, FXD::S32 rhs) NOEXCEPT
		{
			Math::Vec2ISub(lhs.m_data, lhs.m_data, rhs);
			return lhs;
		}
		inline Vector2I& operator-=(Vector2I& lhs, const Vector2I& rhs) NOEXCEPT
		{
			Math::Vec2ISub(lhs.m_data, lhs.m_data, rhs.m_data);
			return lhs;
		}

		// Multiplication
		inline Vector2I& operator*=(Vector2I& lhs, FXD::S32 rhs) NOEXCEPT
		{
			Math::Vec2IMul(lhs.m_data, lhs.m_data, rhs);
			return lhs;
		}
		inline Vector2I& operator*=(Vector2I& lhs, const Vector2I& rhs) NOEXCEPT
		{
			Math::Vec2IMul(lhs.m_data, lhs.m_data, rhs.m_data);
			return lhs;
		}

		// Division
		inline Vector2I& operator/=(Vector2I& lhs, FXD::S32 rhs) NOEXCEPT
		{
			Math::Vec2IDiv(lhs.m_data, lhs.m_data, rhs);
			return lhs;
		}
		inline Vector2I& operator/=(Vector2I& lhs, const Vector2I& rhs) NOEXCEPT
		{
			Math::Vec2IDiv(lhs.m_data, lhs.m_data, rhs.m_data);
			return lhs;
		}

		// Core::IStreamOut
		inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, Vector2I const& rhs)
		{
			return ostr << rhs(0) << ' ' << rhs(1);
		}

	} //namespace Math
} //namespace FXD
#endif //FXDENGINE_MATH_VECTOR2I_H