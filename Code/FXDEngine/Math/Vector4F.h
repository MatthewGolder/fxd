// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_VECTOR4F_H
#define FXDENGINE_MATH_VECTOR4F_H

#include "FXDEngine/Math/Types.h"
#include "FXDEngine/Core/StreamOut.h"

namespace FXD
{
	namespace Math
	{
		// ------
		// Vector4F
		// -
		// ------
		class FXD_ALIGN_AS(16) Vector4F
		{
		public:
			friend class Matrix4F;

		public:
			static const Vector4F kZero;
			static const Vector4F kOne;
			static const Vector4F kInf;
			static const Vector4F kNegInf;
			static const Vector4F kUnitX;
			static const Vector4F kUnitY;
			static const Vector4F kUnitZ;
			static const Vector4F kUnitW;

		public:
			Vector4F(void);
			Vector4F(FXD::F32 val);
			Vector4F(FXD::F32 x, FXD::F32 y, FXD::F32 z, FXD::F32 w);
			~Vector4F(void);

			FXD::F32 operator[](FXD::U32 nID) const;
			FXD::F32 operator()(FXD::U32 nID) const;

			Vector4F& operator=(const Vector4F& rhs);
			Vector4F& operator=(FXD::F32 rhs);

			friend bool operator==(const Vector4F& lhs, const Vector4F& rhs) NOEXCEPT;
			friend bool operator!=(const Vector4F& lhs, const Vector4F& rhs) NOEXCEPT;

			friend Vector4F operator+(const Vector4F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector4F operator+(const Vector4F& lhs, const Vector4F& rhs) NOEXCEPT;
			friend Vector4F operator-(const Vector4F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector4F operator-(const Vector4F& lhs, const Vector4F& rhs) NOEXCEPT;
			friend Vector4F operator*(const Vector4F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector4F operator*(const Vector4F& lhs, const Vector4F& rhs) NOEXCEPT;
			friend Vector4F operator/(const Vector4F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector4F operator/(const Vector4F& lhs, const Vector4F& rhs) NOEXCEPT;

			friend Vector4F& operator+=(Vector4F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector4F& operator+=(Vector4F& lhs, const Vector4F& rhs) NOEXCEPT;
			friend Vector4F& operator-=(Vector4F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector4F& operator-=(Vector4F& lhs, const Vector4F& rhs) NOEXCEPT;
			friend Vector4F& operator*=(Vector4F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector4F& operator*=(Vector4F& lhs, const Vector4F& rhs) NOEXCEPT;
			friend Vector4F& operator/=(Vector4F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector4F& operator/=(Vector4F& lhs, const Vector4F& rhs) NOEXCEPT;

			Vector4F operator-() const;

			FXD::F32 x(void) const;
			FXD::F32 y(void) const;
			FXD::F32 z(void) const;
			FXD::F32 w(void) const;
			FXD::F32 index(FXD::U32 nID);

			void x(FXD::F32 x);
			void y(FXD::F32 y);
			void z(FXD::F32 z);
			void w(FXD::F32 w);
			void index(FXD::U32 nID, FXD::F32 fVal);

			FXD::F32 dot(const Vector4F& rhs) const;
			FXD::F32 length(void) const;
			FXD::F32 squared_length(void) const;
			FXD::F32 distance(const Vector4F& rhs) const;
			FXD::F32 sqrd_distance(const Vector4F& rhs) const;
			Vector4F midpoint(const Vector4F& rhs) const;

			void negate(void);
			void minimum(const Vector4F& min);
			void maximum(const Vector4F& max);
			void grid_snap(const FXD::F32 fGridSize);
			void clamp(const Vector4F& min, const Vector4F& max);
			void normalise(void);
			void abs(void);
			void ceil(void);
			void floor(void);
			Vector4F get_negate(void) const;
			Vector4F get_minimum(const Vector4F& min) const;
			Vector4F get_maximum(const Vector4F& max) const;
			Vector4F get_grid_snap(const FXD::F32 fGridSize);
			Vector4F get_clamp(const Vector4F& min, const Vector4F& max) const;
			Vector4F get_normalise(void) const;
			Vector4F get_abs(void) const;
			Vector4F get_ceil(void) const;
			Vector4F get_floor(void) const;

			static FXD::F32 dot(const Vector4F& lhs, const Vector4F& rhs);
			static FXD::F32 distance(const Vector4F& lhs, const Vector4F& rhs);
			static FXD::F32 sqrd_distance(const Vector4F& lhs, const Vector4F& rhs);
			static Vector4F midpoint(const Vector4F& lhs, const Vector4F& rhs);

			static void set_negate(Vector4F& vec);
			static void set_minimum(Vector4F& vec, const Vector4F& min);
			static void set_maximum(Vector4F& vec, const Vector4F& max);
			static void set_grid_snap(Vector4F& vec, const FXD::F32 fGridSize);
			static void set_clamp(Vector4F& vec, const Vector4F& min, const Vector4F& max);
			static void set_normalise(Vector4F& val);
			static void set_abs(Vector4F& val);
			static void set_ceil(Vector4F& val);
			static void set_floor(Vector4F& val);
			static void set_lerp(Vector4F& val, const Vector4F& lhs, const Vector4F& rhs, FXD::F32 fVal);
			static Vector4F get_negate(const Vector4F& vec);
			static Vector4F get_minimum(const Vector4F& vec, const Vector4F& min);
			static Vector4F get_maximum(const Vector4F& vec, const Vector4F& max);
			static Vector4F get_grid_snap(const Vector4F& vec, const FXD::F32 fGridSize);
			static Vector4F get_clamp(const Vector4F& vec, const Vector4F& min, const Vector4F& max);
			static Vector4F get_normalise(const Vector4F& val);
			static Vector4F get_abs(const Vector4F& val);
			static Vector4F get_ceil(const Vector4F& val);
			static Vector4F get_floor(const Vector4F& val);
			static Vector4F get_lerp(const Vector4F& lhs, const Vector4F& rhs, FXD::F32 fVal);

		private:
			_vector4FType m_data;
		};

		// Comparison operators
		// Equality
		inline bool operator==(const Vector4F& lhs, const Vector4F& rhs) NOEXCEPT
		{
			return Vec4FCompare(lhs.m_data, rhs.m_data);
		}

		// Inequality
		inline bool operator!=(const Vector4F& lhs, const Vector4F& rhs) NOEXCEPT
		{
			return !Vec4FCompare(lhs.m_data, rhs.m_data);
		}

		// Arithmetic operators
		// Add
		inline Vector4F operator+(const Vector4F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vector4F out;
			Vec4FAdd(out.m_data, lhs.m_data, rhs);
			return out;
		}
		inline Vector4F operator+(const Vector4F& lhs, const Vector4F& rhs) NOEXCEPT
		{
			Vector4F out;
			Vec4FAdd(out.m_data, lhs.m_data, rhs.m_data);
			return out;
		}

		// Subtract
		inline Vector4F operator-(const Vector4F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vector4F out;
			Vec4FSub(out.m_data, lhs.m_data, rhs);
			return out;
		}
		inline Vector4F operator-(const Vector4F& lhs, const Vector4F& rhs) NOEXCEPT
		{
			Vector4F out;
			Vec4FSub(out.m_data, lhs.m_data, rhs.m_data);
			return out;
		}

		// Multiplication
		inline Vector4F operator*(const Vector4F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vector4F out;
			Vec4FMul(out.m_data, lhs.m_data, rhs);
			return out;
		}
		inline Vector4F operator*(const Vector4F& lhs, const Vector4F& rhs) NOEXCEPT
		{
			Vector4F out;
			Vec4FMul(out.m_data, lhs.m_data, rhs.m_data);
			return out;
		}

		// Division
		inline Vector4F operator/(const Vector4F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vector4F out;
			Vec4FDiv(out.m_data, lhs.m_data, rhs);
			return out;
		}
		inline Vector4F operator/(const Vector4F& lhs, const Vector4F& rhs) NOEXCEPT
		{
			Vector4F out;
			Vec4FDiv(out.m_data, lhs.m_data, rhs.m_data);
			return out;
		}

		// Assignment operators
		// Add
		inline Vector4F& operator+=(Vector4F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vec4FAdd(lhs.m_data, lhs.m_data, rhs);
			return lhs;
		}
		inline Vector4F& operator+=(Vector4F& lhs, const Vector4F& rhs) NOEXCEPT
		{
			Vec4FAdd(lhs.m_data, lhs.m_data, rhs.m_data);
			return lhs;
		}

		// Subtract
		inline Vector4F& operator-=(Vector4F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vec4FSub(lhs.m_data, lhs.m_data, rhs);
			return lhs;
		}
		inline Vector4F& operator-=(Vector4F& lhs, const Vector4F& rhs) NOEXCEPT
		{
			Vec4FSub(lhs.m_data, lhs.m_data, rhs.m_data);
			return lhs;
		}

		// Multiplication
		inline Vector4F& operator*=(Vector4F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vec4FMul(lhs.m_data, lhs.m_data, rhs);
			return lhs;
		}
		inline Vector4F& operator*=(Vector4F& lhs, const Vector4F& rhs) NOEXCEPT
		{
			Vec4FMul(lhs.m_data, lhs.m_data, rhs.m_data);
			return lhs;
		}

		// Division
		inline Vector4F& operator/=(Vector4F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vec4FDiv(lhs.m_data, lhs.m_data, rhs);
			return lhs;
		}
		inline Vector4F& operator/=(Vector4F& lhs, const Vector4F& rhs) NOEXCEPT
		{
			Vec4FDiv(lhs.m_data, lhs.m_data, rhs.m_data);
			return lhs;
		}

		// Core::IStreamOut
		inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, Vector4F const& rhs)
		{
			return ostr << rhs(0) << ' ' << rhs(1) << ' ' << rhs(2) << ' ' << rhs(3);
		}

	} //namespace Math
} //namespace FXD
#endif //FXDENGINE_MATH_VECTOR4F_H
