// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_VECTOR2F_H
#define FXDENGINE_MATH_VECTOR2F_H

#include "FXDEngine/Math/Types.h"
#include "FXDEngine/Core/StreamOut.h"

namespace FXD
{
	namespace Math
	{
		// ------
		// Vector2F
		// -
		// ------
		class Vector2F
		{
		public:
			static const Vector2F kZero;
			static const Vector2F kOne;
			static const Vector2F kInf;
			static const Vector2F kNegInf;
			static const Vector2F kLeft;
			static const Vector2F kRight;
			static const Vector2F kUp;
			static const Vector2F kDown;
			static const Vector2F kUnitX;
			static const Vector2F kUnitY;
			static const Vector2F kAnchorMiddle;
			static const Vector2F kAnchorBottomLeft;
			static const Vector2F kAnchorTopLeft;
			static const Vector2F kAnchorBottomRight;
			static const Vector2F kAnchorTopRight;
			static const Vector2F kAnchorMiddleRight;
			static const Vector2F kAnchorMiddleLeft;
			static const Vector2F kAnchorMiddleTop;
			static const Vector2F kAnchorMiddleBottom;

		public:
			Vector2F(void);
			Vector2F(FXD::F32 val);
			Vector2F(FXD::F32 x, FXD::F32 y);
			~Vector2F(void);

			FXD::F32 operator[](FXD::U32 nID) const;
			FXD::F32 operator()(FXD::U32 nID) const;

			Vector2F& operator=(const Vector2F& rhs);
			Vector2F& operator=(FXD::F32 rhs);

			friend bool operator==(const Vector2F& lhs, const Vector2F& rhs) NOEXCEPT;
			friend bool operator!=(const Vector2F& lhs, const Vector2F& rhs) NOEXCEPT;

			friend Vector2F operator+(const Vector2F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector2F operator+(const Vector2F& lhs, const Vector2F& rhs) NOEXCEPT;
			friend Vector2F operator-(const Vector2F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector2F operator-(const Vector2F& lhs, const Vector2F& rhs) NOEXCEPT;
			friend Vector2F operator*(const Vector2F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector2F operator*(const Vector2F& lhs, const Vector2F& rhs) NOEXCEPT;
			friend Vector2F operator/(const Vector2F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector2F operator/(const Vector2F& lhs, const Vector2F& rhs) NOEXCEPT;

			friend Vector2F& operator+=(Vector2F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector2F& operator+=(Vector2F& lhs, const Vector2F& rhs) NOEXCEPT;
			friend Vector2F& operator-=(Vector2F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector2F& operator-=(Vector2F& lhs, const Vector2F& rhs) NOEXCEPT;
			friend Vector2F& operator*=(Vector2F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector2F& operator*=(Vector2F& lhs, const Vector2F& rhs) NOEXCEPT;
			friend Vector2F& operator/=(Vector2F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector2F& operator/=(Vector2F& lhs, const Vector2F& rhs) NOEXCEPT;

			Vector2F operator-() const;

			FXD::F32 x(void) const;
			FXD::F32 y(void) const;
			FXD::F32 index(FXD::U32 nID);

			void x(FXD::F32 x);
			void y(FXD::F32 y);
			void index(FXD::U32 nID, FXD::F32 fVal);

			FXD::F32 dot(const Vector2F& rhs) const;
			FXD::F32 cross(const Vector2F& rhs) const;
			FXD::F32 length(void) const;
			FXD::F32 squared_length(void) const;
			FXD::F32 distance(const Vector2F& rhs) const;
			FXD::F32 sqrd_distance(const Vector2F& rhs) const;
			Vector2F midpoint(const Vector2F& rhs) const;
			Vector2F perpendicular(void) const;

			void negate(void);
			void minimum(const Vector2F& min);
			void maximum(const Vector2F& max);
			void grid_snap(const FXD::F32 fGridSize);
			void clamp(const Vector2F& min, const Vector2F& max);
			void normalise(void);
			void abs(void);
			void ceil(void);
			void floor(void);
			Vector2F get_negate(void) const;
			Vector2F get_minimum(const Vector2F& min) const;
			Vector2F get_maximum(const Vector2F& max) const;
			Vector2F get_grid_snap(const FXD::F32 fGridSize);
			Vector2F get_clamp(const Vector2F& min, const Vector2F& max) const;
			Vector2F get_normalise(void) const;
			Vector2F get_abs(void) const;
			Vector2F get_ceil(void) const;
			Vector2F get_floor(void) const;

			static FXD::F32 dot(const Vector2F& lhs, const Vector2F& rhs);
			static FXD::F32 cross(const Vector2F& lhs, const Vector2F& rhs);
			static FXD::F32 distance(const Vector2F& lhs, const Vector2F& rhs);
			static FXD::F32 sqrd_distance(const Vector2F& lhs, const Vector2F& rhs);
			static Vector2F midpoint(const Vector2F& lhs, const Vector2F& rhs);

			static void set_negate(Vector2F& vec);
			static void set_minimum(Vector2F& vec, const Vector2F& min);
			static void set_maximum(Vector2F& vec, const Vector2F& max);
			static void set_grid_snap(Vector2F& vec, const FXD::F32 fGridSize);
			static void set_clamp(Vector2F& vec, const Vector2F& min, const Vector2F& max);
			static void set_normalise(Vector2F& val);
			static void set_abs(Vector2F& val);
			static void set_ceil(Vector2F& val);
			static void set_floor(Vector2F& val);
			static void set_lerp(Vector2F& val, const Vector2F& lhs, const Vector2F& rhs, FXD::F32 fVal);
			static Vector2F get_negate(const Vector2F& vec);
			static Vector2F get_minimum(const Vector2F& vec, const Vector2F& min);
			static Vector2F get_maximum(const Vector2F& vec, const Vector2F& max);
			static Vector2F get_grid_snap(const Vector2F& vec, const FXD::F32 fGridSize);
			static Vector2F get_clamp(const Vector2F& vec, const Vector2F& min, const Vector2F& max);
			static Vector2F get_normalise(const Vector2F& val);
			static Vector2F get_abs(const Vector2F& val);
			static Vector2F get_ceil(const Vector2F& val);
			static Vector2F get_floor(const Vector2F& val);
			static Vector2F get_lerp(const Vector2F& lhs, const Vector2F& rhs, FXD::F32 fVal);

		private:
			_vector2FType m_data;
		};

		// Comparison operators
		// Equality
		inline bool operator==(const Vector2F& lhs, const Vector2F& rhs) NOEXCEPT
		{
			return Vec2FCompare(lhs.m_data, rhs.m_data);
		}

		// Inequality
		inline bool operator!=(const Vector2F& lhs, const Vector2F& rhs) NOEXCEPT
		{
			return !Vec2FCompare(lhs.m_data, rhs.m_data);
		}

		// Arithmetic operators
		// Add
		inline Vector2F operator+(const Vector2F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vector2F out;
			Vec2FAdd(out.m_data, lhs.m_data, rhs);
			return out;
		}
		inline Vector2F operator+(const Vector2F& lhs, const Vector2F& rhs) NOEXCEPT
		{
			Vector2F out;
			Vec2FAdd(out.m_data, lhs.m_data, rhs.m_data);
			return out;
		}

		// Subtract
		inline Vector2F operator-(const Vector2F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vector2F out;
			Vec2FSub(out.m_data, lhs.m_data, rhs);
			return out;
		}
		inline Vector2F operator-(const Vector2F& lhs, const Vector2F& rhs) NOEXCEPT
		{
			Vector2F out;
			Vec2FSub(out.m_data, lhs.m_data, rhs.m_data);
			return out;
		}

		// Multiplication
		inline Vector2F operator*(const Vector2F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vector2F out;
			Vec2FMul(out.m_data, lhs.m_data, rhs);
			return out;
		}
		inline Vector2F operator*(const Vector2F& lhs, const Vector2F& rhs) NOEXCEPT
		{
			Vector2F out;
			Vec2FMul(out.m_data, lhs.m_data, rhs.m_data);
			return out;
		}

		// Division
		inline Vector2F operator/(const Vector2F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vector2F out;
			Vec2FDiv(out.m_data, lhs.m_data, rhs);
			return out;
		}
		inline Vector2F operator/(const Vector2F& lhs, const Vector2F& rhs) NOEXCEPT
		{
			Vector2F out;
			Vec2FDiv(out.m_data, lhs.m_data, rhs.m_data);
			return out;
		}

		// Assignment operators
		// Add
		inline Vector2F& operator+=(Vector2F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vec2FAdd(lhs.m_data, lhs.m_data, rhs);
			return lhs;
		}
		inline Vector2F& operator+=(Vector2F& lhs, const Vector2F& rhs) NOEXCEPT
		{
			Vec2FAdd(lhs.m_data, lhs.m_data, rhs.m_data);
			return lhs;
		}

		// Subtract
		inline Vector2F& operator-=(Vector2F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vec2FSub(lhs.m_data, lhs.m_data, rhs);
			return lhs;
		}
		inline Vector2F& operator-=(Vector2F& lhs, const Vector2F& rhs) NOEXCEPT
		{
			Vec2FSub(lhs.m_data, lhs.m_data, rhs.m_data);
			return lhs;
		}

		// Multiplication
		inline Vector2F& operator*=(Vector2F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vec2FMul(lhs.m_data, lhs.m_data, rhs);
			return lhs;
		}
		inline Vector2F& operator*=(Vector2F& lhs, const Vector2F& rhs) NOEXCEPT
		{
			Vec2FMul(lhs.m_data, lhs.m_data, rhs.m_data);
			return lhs;
		}

		// Division
		inline Vector2F& operator/=(Vector2F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vec2FDiv(lhs.m_data, lhs.m_data, rhs);
			return lhs;
		}
		inline Vector2F& operator/=(Vector2F& lhs, const Vector2F& rhs) NOEXCEPT
		{
			Vec2FDiv(lhs.m_data, lhs.m_data, rhs.m_data);
			return lhs;
		}

		// Core::IStreamOut
		inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, Vector2F const& rhs)
		{
			return ostr << rhs(0) << ' ' << rhs(1);
		}

	} //namespace Math
} //namespace FXD
#endif //FXDENGINE_MATH_VECTOR2F_H