// Creator - MatthewGolder
#include "FXDEngine/Math/Types.h"
#include "FXDEngine/Math/Math.h"

#if IsMathSSE()
namespace FXD
{
	namespace Math
	{
		void Vec2FSet(_vector2FType& dst, FXD::F32 x, FXD::F32 y)
		{
			dst.vector2[0] = x;
			dst.vector2[1] = y;
		}
		void Vec2FSet(_vector2FType& dst, FXD::F32 fVal)
		{
			Math::Vec2FSet(dst, fVal, fVal);
		}

		void Vec2FSetIndex(_vector2FType& dst, FXD::U32 nID, FXD::F32 fVal)
		{
			dst.vector2[nID] = fVal;
		}
		void Vec2FSetX(_vector2FType& dst, FXD::F32 fVal)
		{
			Math::Vec2FSetIndex(dst, 0, fVal);
		}
		void Vec2FSetY(_vector2FType& dst, FXD::F32 fVal)
		{
			Math::Vec2FSetIndex(dst, 1, fVal);
		}

		FXD::F32 Vec2FGetIndex(const _vector2FType& vec, FXD::U32 nID)
		{
			return vec.vector2[nID];
		}
		FXD::F32 Vec2FGetX(const _vector2FType& vec)
		{
			return Math::Vec2FGetIndex(vec, 0);
		}
		FXD::F32 Vec2FGetY(const _vector2FType& vec)
		{
			return Math::Vec2FGetIndex(vec, 1);
		}

		bool Vec2FCompare(const _vector2FType& lhs, const _vector2FType& rhs)
		{
			return (
				(Math::Vec2FGetX(lhs) == Math::Vec2FGetX(rhs)) &&
				(Math::Vec2FGetY(lhs) == Math::Vec2FGetY(rhs)));
		}
		void Vec2FCopy(_vector2FType& dst, const _vector2FType& src)
		{
			Math::Vec2FSetX(dst, Math::Vec2FGetX(src));
			Math::Vec2FSetY(dst, Math::Vec2FGetY(src));
		}

		void Vec2FAdd(_vector2FType& dst, const _vector2FType& lhs, FXD::F32 fX, FXD::F32 fY)
		{
			Math::Vec2FSetX(dst, (Math::Vec2FGetX(lhs) + fX));
			Math::Vec2FSetY(dst, (Math::Vec2FGetY(lhs) + fY));
		}
		void Vec2FAdd(_vector2FType& dst, const _vector2FType& lhs, const _vector2FType& rhs)
		{
			Math::Vec2FAdd(dst, lhs, Math::Vec2FGetX(rhs), Math::Vec2FGetY(rhs));
		}
		void Vec2FAdd(_vector2FType& dst, const _vector2FType& lhs, FXD::F32 fVal)
		{
			Math::Vec2FAdd(dst, lhs, fVal, fVal);
		}

		void Vec2FSub(_vector2FType& dst, const _vector2FType& lhs, FXD::F32 fX, FXD::F32 fY)
		{
			Math::Vec2FSetX(dst, (Math::Vec2FGetX(lhs) - fX));
			Math::Vec2FSetY(dst, (Math::Vec2FGetY(lhs) - fY));
		}
		void Vec2FSub(_vector2FType& dst, const _vector2FType& lhs, const _vector2FType& rhs)
		{
			Math::Vec2FSub(dst, lhs, Math::Vec2FGetX(rhs), Math::Vec2FGetY(rhs));
		}
		void Vec2FSub(_vector2FType& dst, const _vector2FType& lhs, FXD::F32 fVal)
		{
			Math::Vec2FSub(dst, lhs, fVal, fVal);
		}

		void Vec2FMul(_vector2FType& dst, const _vector2FType& lhs, FXD::F32 fX, FXD::F32 fY)
		{
			Math::Vec2FSetX(dst, (Math::Vec2FGetX(lhs) * fX));
			Math::Vec2FSetY(dst, (Math::Vec2FGetY(lhs) * fY));
		}
		void Vec2FMul(_vector2FType& dst, const _vector2FType& lhs, const _vector2FType& rhs)
		{
			Math::Vec2FMul(dst, lhs, Math::Vec2FGetX(rhs), Math::Vec2FGetY(rhs));
		}
		void Vec2FMul(_vector2FType& dst, const _vector2FType& lhs, FXD::F32 fVal)
		{
			Math::Vec2FMul(dst, lhs, fVal, fVal);
		}

		void Vec2FDiv(_vector2FType& dst, const _vector2FType& lhs, FXD::F32 fX, FXD::F32 fY)
		{
			Math::Vec2FSetX(dst, (Math::Vec2FGetX(lhs) / fX));
			Math::Vec2FSetY(dst, (Math::Vec2FGetY(lhs) / fY));
		}
		void Vec2FDiv(_vector2FType& dst, const _vector2FType& lhs, const _vector2FType& rhs)
		{
			Math::Vec2FDiv(dst, lhs, Math::Vec2FGetX(rhs), Math::Vec2FGetY(rhs));
		}
		void Vec2FDiv(_vector2FType& dst, const _vector2FType& lhs, FXD::F32 fVal)
		{
			Math::Vec2FDiv(dst, lhs, fVal, fVal);
		}

		FXD::F32 Vec2FSum(const _vector2FType& vec)
		{
			return (Math::Vec2FGetX(vec) + Math::Vec2FGetY(vec));
		}
		FXD::F32 Vec2FDot(const _vector2FType& lhs, const _vector2FType& rhs)
		{
			_vector2FType vecDot;
			Math::Vec2FMul(vecDot, lhs, rhs);
			return Math::Vec2FSum(vecDot);
		}
		FXD::F32 Vec2FCross(const _vector2FType& lhs, const _vector2FType& rhs)
		{
			return (Math::Vec2FGetX(lhs) * Math::Vec2FGetY(rhs)) - (Math::Vec2FGetY(lhs) * Math::Vec2FGetX(rhs));
		}
		FXD::F32 Vec2FSquaredLength(const _vector2FType& vec)
		{
			return Math::Vec2FDot(vec, vec);
		}
		FXD::F32 Vec2FSquaredDistance(const _vector2FType& lhs, const _vector2FType& rhs)
		{
			_vector2FType out;
			Math::Vec2FSub(out, lhs, rhs);
			return Vec2FSquaredLength(out);
		}
		void Vec2FMidPoint(_vector2FType& out, const _vector2FType& lhs, const _vector2FType& rhs)
		{
			Math::Vec2FAdd(out, lhs, rhs);
			Math::Vec2FDiv(out, out, 2.0f);
		}

		void Vec2FNegate(_vector2FType& vec)
		{
			Math::Vec2FSetX(vec, -Math::Vec2FGetX(vec));
			Math::Vec2FSetY(vec, -Math::Vec2FGetY(vec));
		}
		void Vec2FMinimum(_vector2FType& out, const _vector2FType min)
		{
			// Clamp the x value.
			if (Math::Vec2FGetX(out) > Math::Vec2FGetX(min))
			{
				Math::Vec2FSetX(out, Math::Vec2FGetX(min));
			}
			// Clamp the y value.
			if (Math::Vec2FGetY(out) > Math::Vec2FGetY(min))
			{
				Math::Vec2FSetY(out, Math::Vec2FGetY(min));
			}
		}
		void Vec2FMaximum(_vector2FType& out, const _vector2FType max)
		{
			// Clamp the x value.
			if (Math::Vec2FGetX(out) < Math::Vec2FGetX(max))
			{
				Math::Vec2FSetY(out, Math::Vec2FGetX(max));
			}
			// Clamp the y value.
			if (Math::Vec2FGetX(out) < Math::Vec2FGetX(max))
			{
				Math::Vec2FSetY(out, Math::Vec2FGetX(max));
			}
		}
		void Vec2FClamp(_vector2FType& out, const _vector2FType& min, const _vector2FType& max)
		{
			Math::Vec2FMinimum(out, max);
			Math::Vec2FMaximum(out, min);
		}
		void Vec2FNormalise(_vector2FType& out)
		{
			FXD::F32 fLength = Math::Sqrt(Math::Vec2FSquaredLength(out));
			if (fLength != 0.0f)
			{
				Math::Vec2FDiv(out, out, fLength);
			}
		}
		void Vec2FAbs(_vector2FType& out)
		{
			Math::Vec2FSetX(out, Math::Abs(Math::Vec2FGetX(out)));
			Math::Vec2FSetY(out, Math::Abs(Math::Vec2FGetY(out)));
		}
		void Vec2FCeil(_vector2FType& out)
		{
			Math::Vec2FSetX(out, Math::Ceil(Math::Vec2FGetX(out)));
			Math::Vec2FSetY(out, Math::Ceil(Math::Vec2FGetY(out)));
		}
		void Vec2FFloor(_vector2FType& out)
		{
			Math::Vec2FSetX(out, Math::Floor(Math::Vec2FGetX(out)));
			Math::Vec2FSetY(out, Math::Floor(Math::Vec2FGetY(out)));
		}

	} //namespace Math
} //namespace FXD
#endif //IsMathSSE()