// Creator - MatthewGolder
#include "FXDEngine/Math/Types.h"
#include "FXDEngine/Math/Math.h"

#if IsMathSSE()
namespace FXD
{
	namespace Math
	{
		void QuatFRotationPitchYawRollF((_vector4FType& dst, FXD::F32 fPitch, FXD::F32 fYaw, FXD::F32 fRoll, Math::EulerAngleOrder eOrder)
		{
			const EulerAngleOrderData& l = Math::kEALookup[(FXD::U32)eOrder];

			FXD::F32 fXOver2 = (fPitch * 0.5f);
			FXD::F32 fYOver2 = (fYaw * 0.5f);
			FXD::F32 fZOver2 = (fRoll * 0.5f);

			FXD::F32 fSinX = Math::Sin(fXOver2);
			FXD::F32 fCosX = Math::Cos(fXOver2);

			FXD::F32 fSinY = Math::Sin(fYOver2);
			FXD::F32 fCosY = Math::Cos(fYOver2);

			FXD::F32 fSinZ = Math::Sin(fZOver2);
			FXD::F32 fCosZ = Math::Cos(fZOver2);

			_vector4FType quats[3];
			Math::Vec4FSet(quats[0], fSinX, 0.0f, 0.0f, fCosX);
			Math::Vec4FSet(quats[1], 0.0f, fSinY, 0.0f, fCosY);
			Math::Vec4FSet(quats[2], 0.0f, 0.0f, fSinZ, fCosZ);

			_vector4FType temp;
			Math::QuatFMul(temp, quats[l.a], quats[l.b]);
			Math::QuatFMul(dst, temp, quats[l.c]);
		}
		void QuatFRotationPitchYawRollV(_vector4FType& dst, const _vector3FType& rhs, Math::EulerAngleOrder eOrder)
		{
			Math::QuatFRotationPitchYawRollF(dst, Math::Vec3FGetX(rhs), Math::Vec3FGetY(rhs), Math::Vec3FGetZ(rhs), eOrder);
		}
		void QuatFToRotation(_matrix3Type& dst, const _vector4FType& src)
		{
			_vector4FType mul;
			Math::Vec4FMul(mul, src, Math::Vec4FGetX(src));

			FXD::F32 xx = Math::Vec4FGetX(mul);
			FXD::F32 xy = Math::Vec4FGetY(mul);
			FXD::F32 xz = Math::Vec4FGetZ(mul);
			FXD::F32 xw = Math::Vec4FGetW(mul);

			FXD::F32 yy = Math::Vec4FGetY(src) * Math::Vec4FGetY(src);
			FXD::F32 yz = Math::Vec4FGetY(src) * Math::Vec4FGetZ(src);
			FXD::F32 yw = Math::Vec4FGetY(src) * Math::Vec4FGetW(src);

			FXD::F32 zz = Math::Vec4FGetZ(src) * Math::Vec4FGetZ(src);
			FXD::F32 zw = Math::Vec4FGetZ(src) * Math::Vec4FGetW(src);

			Math::Mat3SetRow(dst, 0, (1.0f - (2.0f * (yy + zz))), (2.0f * (xy + zw)), (2.0f * (xz - yw)));
			Math::Mat3SetRow(dst, 1, (2.0f * (xy - zw)), (1.0f - (2.0f * (xx + zz))), (2.0f * (yz + xw)));
			Math::Mat3SetRow(dst, 2, (2.0f * (xz + yw)), (2.0f * (yz - xw)), (1.0f - (2.0f * (xx + yy))));
		}

		void QuatFMul(_vector4FType& dst, const _vector4FType& lhs, const _vector4FType& rhs)
		{
			__m128 vQ1 = lhs.vector4;
			__m128 vQ2 = rhs.vector4;

			__m128 A1 = FXD_pshufd_ps(vQ1, FXD_SHUFFLE(0, 1, 2, 0));
			__m128 B1 = FXD_pshufd_ps(vQ2, FXD_SHUFFLE(3, 3, 3, 0));
			A1 = _mm_mul_ps(A1, B1);

			__m128 A2 = FXD_pshufd_ps(vQ1, FXD_SHUFFLE(1, 2, 0, 1));
			__m128 B2 = FXD_pshufd_ps(vQ2, FXD_SHUFFLE(2, 0, 1, 1));
			A2 = _mm_mul_ps(A2, B2);

			B1 = FXD_pshufd_ps(vQ1, FXD_SHUFFLE(2, 0, 1, 2));
			B2 = FXD_pshufd_ps(vQ2, FXD_SHUFFLE(1, 2, 0, 2));
			B1 = _mm_mul_ps(B1, B2);

			vQ1 = FXD_splat_ps(vQ1, 3);
			vQ1 = _mm_mul_ps(vQ1, vQ2);

			A1 = _mm_add_ps(A1, A2);
			vQ1 = _mm_sub_ps(vQ1, B1);

			A1 = _mm_xor_ps(A1, vPPPM);
			dst.vector4 = _mm_add_ps(vQ1, A1);
		}

		//void QuatFMul(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fVal)
		//{
		//}

	} //namespace Math
} //namespace FXD
#endif //IsMathSSE()