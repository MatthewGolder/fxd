// Creator - MatthewGolder
#include "FXDEngine/Math/Types.h"
#include "FXDEngine/Math/Math.h"

#if IsMathSSE()
namespace FXD
{
	namespace Math
	{
		void Vec2ISet(_vector2IType& dst, FXD::S32 x, FXD::S32 y)
		{
			dst.vector2[0] = x;
			dst.vector2[1] = y;
		}
		void Vec2ISet(_vector2IType& dst, FXD::S32 nVal)
		{
			Math::Vec2ISet(dst, nVal, nVal);
		}

		void Vec2ISetIndex(_vector2IType& dst, FXD::U32 nID, FXD::S32 nVal)
		{
			dst.vector2[nID] = nVal;
		}
		void Vec2ISetX(_vector2IType& dst, FXD::S32 nVal)
		{
			Math::Vec2ISetIndex(dst, 0, nVal);
		}
		void Vec2ISetY(_vector2IType& dst, FXD::S32 nVal)
		{
			Math::Vec2ISetIndex(dst, 1, nVal);
		}

		FXD::S32 Vec2IGetIndex(const _vector2IType& vec, FXD::U32 nID)
		{
			return vec.vector2[nID];
		}
		FXD::S32 Vec2IGetX(const _vector2IType& vec)
		{
			return Math::Vec2IGetIndex(vec, 0);
		}
		FXD::S32 Vec2IGetY(const _vector2IType& vec)
		{
			return Math::Vec2IGetIndex(vec, 1);
		}

		bool Vec2ICompare(const _vector2IType& lhs, const _vector2IType& rhs)
		{
			return (
				(Math::Vec2IGetX(lhs) == Math::Vec2IGetX(rhs)) &&
				(Math::Vec2IGetY(lhs) == Math::Vec2IGetY(rhs)));
		}
		void Vec2ICopy(_vector2IType& dst, const _vector2IType& src)
		{
			Math::Vec2ISetX(dst, Math::Vec2IGetX(src));
			Math::Vec2ISetY(dst, Math::Vec2IGetX(src));
		}

		void Vec2IAdd(_vector2IType& dst, const _vector2IType& lhs, FXD::S32 nX, FXD::S32 nY)
		{
			Math::Vec2ISetX(dst, (Math::Vec2IGetX(lhs) + nX));
			Math::Vec2ISetY(dst, (Math::Vec2IGetY(lhs) + nY));
		}
		void Vec2IAdd(_vector2IType& dst, const _vector2IType& lhs, const _vector2IType& rhs)
		{
			Math::Vec2IAdd(dst, lhs, Math::Vec2IGetX(rhs), Math::Vec2IGetY(rhs));
		}
		void Vec2IAdd(_vector2IType& dst, const _vector2IType& lhs, FXD::S32 nVal)
		{
			Math::Vec2IAdd(dst, lhs, nVal, nVal);
		}

		void Vec2ISub(_vector2IType& dst, const _vector2IType& lhs, FXD::S32 nX, FXD::S32 nY)
		{
			Math::Vec2ISetX(dst, (Math::Vec2IGetX(lhs) - nX));
			Math::Vec2ISetY(dst, (Math::Vec2IGetY(lhs) - nY));
		}
		void Vec2ISub(_vector2IType& dst, const _vector2IType& lhs, const _vector2IType& rhs)
		{
			Math::Vec2ISub(dst, lhs, Math::Vec2IGetX(rhs), Math::Vec2IGetY(rhs));
		}
		void Vec2ISub(_vector2IType& dst, const _vector2IType& lhs, FXD::S32 nVal)
		{
			Math::Vec2ISub(dst, lhs, nVal, nVal);
		}

		void Vec2IMul(_vector2IType& dst, const _vector2IType& lhs, FXD::S32 nX, FXD::S32 nY)
		{
			Math::Vec2ISetX(dst, (Math::Vec2IGetX(lhs) * nX));
			Math::Vec2ISetY(dst, (Math::Vec2IGetY(lhs) * nY));
		}
		void Vec2IMul(_vector2IType& dst, const _vector2IType& lhs, const _vector2IType& rhs)
		{
			Math::Vec2IMul(dst, lhs, Math::Vec2IGetX(rhs), Math::Vec2IGetY(rhs));
		}
		void Vec2IMul(_vector2IType& dst, const _vector2IType& lhs, FXD::S32 nVal)
		{
			Math::Vec2IMul(dst, lhs, nVal, nVal);
		}

		void Vec2IDiv(_vector2IType& dst, const _vector2IType& lhs, FXD::S32 nX, FXD::S32 nY)
		{
			Math::Vec2ISetX(dst, (Math::Vec2IGetX(lhs) / nX));
			Math::Vec2ISetY(dst, (Math::Vec2IGetY(lhs) / nY));
		}
		void Vec2IDiv(_vector2IType& dst, const _vector2IType& lhs, const _vector2IType& rhs)
		{
			Math::Vec2IDiv(dst, lhs, Math::Vec2IGetX(rhs), Math::Vec2IGetY(rhs));
		}
		void Vec2IDiv(_vector2IType& dst, const _vector2IType& lhs, FXD::S32 nVal)
		{
			Math::Vec2IDiv(dst, lhs, nVal, nVal);
		}
		void Vec2IMidPoint(_vector2IType& out, const _vector2IType& lhs, const _vector2IType& rhs)
		{
			Math::Vec2IAdd(out, lhs, rhs);
			Math::Vec2IDiv(out, out, 2);
		}

		void Vec2INegate(_vector2IType& vec)
		{
			Math::Vec2ISetX(vec, -Math::Vec2IGetX(vec));
			Math::Vec2ISetY(vec, -Math::Vec2IGetY(vec));
		}
		void Vec2IMinimum(_vector2IType& out, const _vector2IType min)
		{
			// Clamp the x value.
			if (Math::Vec2IGetX(out) > Math::Vec2IGetX(min))
			{
				Math::Vec2ISetX(out, Math::Vec2IGetX(min));
			}
			// Clamp the y value.
			if (Math::Vec2IGetY(out) > Math::Vec2IGetY(min))
			{
				Math::Vec2ISetY(out, Math::Vec2IGetY(min));
			}
		}
		void Vec2IMaximum(_vector2IType& out, const _vector2IType max)
		{
			// Clamp the x value.
			if (Math::Vec2IGetX(out) < Math::Vec2IGetX(max))
			{
				Math::Vec2ISetY(out, Math::Vec2IGetX(max));
			}
			// Clamp the y value.
			if (Math::Vec2IGetX(out) < Math::Vec2IGetX(max))
			{
				Math::Vec2ISetY(out, Math::Vec2IGetX(max));
			}
		}
		void Vec2IClamp(_vector2IType& out, const _vector2IType& min, const _vector2IType& max)
		{
			Math::Vec2IMinimum(out, max);
			Math::Vec2IMaximum(out, min);
		}
		void Vec2IAbs(_vector2IType& out)
		{
			Math::Vec2ISetX(out, Math::Abs(Math::Vec2IGetX(out)));
			Math::Vec2ISetY(out, Math::Abs(Math::Vec2IGetY(out)));
		}

	} //namespace Math
} //namespace FXD
#endif //IsMathSSE()