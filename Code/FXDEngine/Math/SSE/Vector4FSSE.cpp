// Creator - MatthewGolder
#include "FXDEngine/Math/Types.h"
#include "FXDEngine/Math/Math.h"

#if IsMathSSE()
namespace FXD
{
	namespace Math
	{
		void Vec4FSet(_vector4FType& dst, FXD::F32 x, FXD::F32 y, FXD::F32 z, FXD::F32 w)
		{
			dst.vector4 = _mm_set_ps(w, z, y, x);
		}
		void Vec4FSet(_vector4FType& dst, FXD::F32 fVal)
		{
			Math::Vec4FSet(dst, fVal, fVal, fVal, fVal);
		}

		void Vec4FSetIndex(_vector4FType& dst, FXD::U32 nID, FXD::F32 fVal)
		{
			((FXD::F32*)&dst.vector4)[nID] = fVal;
		}
		void Vec4FSetX(_vector4FType& dst, FXD::F32 fVal)
		{
			Math::Vec4FSetIndex(dst, 0, fVal);
		}
		void Vec4FSetY(_vector4FType& dst, FXD::F32 fVal)
		{
			Math::Vec4FSetIndex(dst, 1, fVal);
		}
		void Vec4FSetZ(_vector4FType& dst, FXD::F32 fVal)
		{
			Math::Vec4FSetIndex(dst, 2, fVal);
		}
		void Vec4FSetW(_vector4FType& dst, FXD::F32 fVal)
		{
			Math::Vec4FSetIndex(dst, 3, fVal);
		}

		FXD::F32 Vec4FGetIndex(const _vector4FType& vec, const FXD::U32 nID)
		{
			return ((FXD::F32*)&vec.vector4)[nID];
		}
		FXD::F32 Vec4FGetX(const _vector4FType& vec)
		{
			return Math::Vec4FGetIndex(vec, 0);
		}
		FXD::F32 Vec4FGetY(const _vector4FType& vec)
		{
			return Math::Vec4FGetIndex(vec, 1);
		}
		FXD::F32 Vec4FGetZ(const _vector4FType& vec)
		{
			return Math::Vec4FGetIndex(vec, 2);
		}
		FXD::F32 Vec4FGetW(const _vector4FType& vec)
		{
			return Math::Vec4FGetIndex(vec, 3);
		}

		bool Vec4FCompare(const _vector4FType& lhs, const _vector4FType& rhs)
		{
			__m128 vcmp = (__m128)_mm_cmpneq_ps(lhs.vector4, rhs.vector4);
			FXD::S32 nRes = _mm_movemask_ps(vcmp);
			return (nRes == 0);
		}
		void Vec4FCopy(_vector4FType& dst, const _vector4FType& src)
		{
			dst.vector4 = src.vector4;
		}
		void Vec4FCopy(_vector4FType& dst, const _vector3FType& src)
		{
			dst.vector4 = src.vector3;
		}

		void Vec4FAdd(_vector4FType& dst, const _vector4FType& lhs, const _vector4FType& rhs)
		{
			dst.vector4 = _mm_add_ps(lhs.vector4, rhs.vector4);
		}
		void Vec4FAdd(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ, FXD::F32 fW)
		{
			_vector4FType vec;
			Math::Vec4FSet(vec, fX, fY, fZ, fW);
			Math::Vec4FAdd(dst, lhs, vec);
		}
		void Vec4FAdd(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fVal)
		{
			Math::Vec4FAdd(dst, lhs, fVal, fVal, fVal, fVal);
		}

		void Vec4FSub(_vector4FType& dst, const _vector4FType& lhs, const _vector4FType& rhs)
		{
			dst.vector4 = _mm_sub_ps(lhs.vector4, rhs.vector4);
		}
		void Vec4FSub(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ, FXD::F32 fW)
		{
			_vector4FType vec;
			Math::Vec4FSet(vec, fX, fY, fZ, fW);
			Math::Vec4FSub(dst, lhs, vec);
		}
		void Vec4FSub(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fVal)
		{
			Math::Vec4FSub(dst, lhs, fVal, fVal, fVal, fVal);
		}

		void Vec4FMul(_vector4FType& dst, const _vector4FType& lhs, const _vector4FType& rhs)
		{
			dst.vector4 = _mm_mul_ps(lhs.vector4, rhs.vector4);
		}
		void Vec4FMul(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ, FXD::F32 fW)
		{
			_vector4FType vec;
			Math::Vec4FSet(vec, fX, fY, fZ, fW);
			Math::Vec4FMul(dst, lhs, vec);
		}
		void Vec4FMul(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fVal)
		{
			Math::Vec4FMul(dst, lhs, fVal, fVal, fVal, fVal);
		}

		void Vec4FDiv(_vector4FType& dst, const _vector4FType& lhs, const _vector4FType& rhs)
		{
			dst.vector4 = _mm_div_ps(lhs.vector4, rhs.vector4);
		}
		void Vec4FDiv(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ, FXD::F32 fW)
		{
			_vector4FType vec;
			Math::Vec4FSet(vec, fX, fY, fZ, fW);
			Math::Vec4FDiv(dst, lhs, vec);
		}
		void Vec4FDiv(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fVal)
		{
			Math::Vec4FDiv(dst, lhs, fVal, fVal, fVal, fVal);
		}

		FXD::F32 Vec4FSum(const _vector4FType& vec)
		{
			return (Math::Vec4FGetX(vec) + Math::Vec4FGetY(vec) + Math::Vec4FGetZ(vec) + Math::Vec4FGetW(vec));
		}
		FXD::F32 Vec4FDot(const _vector4FType& lhs, const _vector4FType& rhs)
		{
			_vector4FType vecDot;
			Math::Vec4FMul(vecDot, lhs, rhs);
			return Vec4FSum(vecDot);
		}
		FXD::F32 Vec4FSquaredLength(const _vector4FType& vec)
		{
			return Math::Vec4FDot(vec, vec);
		}
		FXD::F32 Vec4FSquaredDistance(const _vector4FType& lhs, const _vector4FType& rhs)
		{
			_vector4FType out;
			Math::Vec4FSub(out, lhs, rhs);
			return Math::Vec4FSquaredLength(out);
		}
		void Vec4FMidPoint(_vector4FType& out, const _vector4FType& lhs, const _vector4FType& rhs)
		{
			Math::Vec4FAdd(out, lhs, rhs);
			Math::Vec4FDiv(out, out, 2.0f);
		}

		void Vec4FNegate(_vector4FType& vec)
		{
			vec.vector4 = _mm_xor_ps(vec.vector4, s_vecNegateBits);
		}
		void Vec4FMinimum(_vector4FType& out, const _vector4FType min)
		{
			out.vector4 = _mm_min_ps(out.vector4, min.vector4);
		}
		void Vec4FMaximum(_vector4FType& out, const _vector4FType max)
		{
			out.vector4 = _mm_max_ps(out.vector4, max.vector4);
		}
		void Vec4FClamp(_vector4FType& out, const _vector4FType& min, const _vector4FType& max)
		{
			Math::Vec4FMinimum(out, max);
			Math::Vec4FMaximum(out, min);
		}
		void Vec4FNormalise(_vector4FType& out)
		{
			FXD::F32 fLength = Math::Sqrt(Math::Vec4FSquaredLength(out));
			if (fLength != 0)
			{
				Vec4FDiv(out, out, fLength);
			}
		}
		void Vec4FAbs(_vector4FType& out)
		{
			out.vector4 = _mm_andnot_ps(s_vecAbsBits, out.vector4);
		}
		void Vec4FCeil(_vector4FType& out)
		{
			__m128i v0 = _mm_setzero_si128();
			__m128i v1 = _mm_cmpeq_epi32(v0, v0);
			__m128i ji = _mm_srli_epi32(v1, 25);
			__m128 j = *(__m128*)&_mm_slli_epi32(ji, 23); //create vector 1.0f
			__m128i i = _mm_cvttps_epi32(out.vector4);
			__m128 fi = _mm_cvtepi32_ps(i);
			__m128 igx = _mm_cmplt_ps(fi, out.vector4);
			j = _mm_and_ps(igx, j);
			out.vector4 = _mm_add_ps(fi, j);
		}
		void Vec4FFloor(_vector4FType& out)
		{
			__m128i v0 = _mm_setzero_si128();
			__m128i v1 = _mm_cmpeq_epi32(v0, v0);
			__m128i ji = _mm_srli_epi32(v1, 25);
			__m128 j = *(__m128*)&_mm_slli_epi32(ji, 23); //create vector 1.0f
			__m128i i = _mm_cvttps_epi32(out.vector4);
			__m128 fi = _mm_cvtepi32_ps(i);
			__m128 igx = _mm_cmpgt_ps(fi, out.vector4);
			j = _mm_and_ps(igx, j);
			out.vector4 = _mm_sub_ps(fi, j);
		}

	} //namespace Math
} //namespace FXD
#endif //IsMathSSE()