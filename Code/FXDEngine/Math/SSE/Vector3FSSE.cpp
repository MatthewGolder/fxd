// Creator - MatthewGolder
#include "FXDEngine/Math/Types.h"
#include "FXDEngine/Math/Math.h"
#include "FXDEngine/Core/Algorithm.h"
#include "FXDEngine/Core/Debugger.h"

#if IsMathSSE()
namespace FXD
{
	namespace Math
	{
		void Vec3FSet(_vector3FType& dst, FXD::F32 x, FXD::F32 y, FXD::F32 z)
		{
			dst.vector3 = _mm_set_ps(0.0f, z, y, x);
		}
		void Vec3FSet(_vector3FType& dst, FXD::F32 fVal)
		{
			Math::Vec3FSet(dst, fVal, fVal, fVal);
		}

		void Vec3FSetIndex(_vector3FType& dst, FXD::U32 nID, FXD::F32 fVal)
		{
			((FXD::F32*)&dst.vector3)[nID] = fVal;
		}
		void Vec3FSetX(_vector3FType& dst, FXD::F32 fVal)
		{
			Math::Vec3FSetIndex(dst, 0, fVal);
		}
		void Vec3FSetY(_vector3FType& dst, FXD::F32 fVal)
		{
			Math::Vec3FSetIndex(dst, 1, fVal);
		}
		void Vec3FSetZ(_vector3FType& dst, FXD::F32 fVal)
		{
			Math::Vec3FSetIndex(dst, 2, fVal);
		}
		void Vec3FSetW(_vector3FType& dst, FXD::F32 fVal)
		{
			Math::Vec3FSetIndex(dst, 3, fVal);
		}

		FXD::F32 Vec3FGetIndex(const _vector3FType& vec, FXD::U32 nID)
		{
			return ((FXD::F32*)&vec.vector3)[nID];
		}
		FXD::F32 Vec3FGetX(const _vector3FType& vec)
		{
			return Math::Vec3FGetIndex(vec, 0);
		}
		FXD::F32 Vec3FGetY(const _vector3FType& vec)
		{
			return Math::Vec3FGetIndex(vec, 1);
		}
		FXD::F32 Vec3FGetZ(const _vector3FType& vec)
		{
			return Math::Vec3FGetIndex(vec, 2);
		}
		FXD::F32 Vec3FGetW(const _vector3FType& vec)
		{
			return Math::Vec3FGetIndex(vec, 3);
		}

		bool Vec3FCompare(const _vector3FType& lhs, const _vector3FType& rhs)
		{
			__m128 vcmp = (__m128)_mm_cmpneq_ps(lhs.vector3, rhs.vector3);
			FXD::S32 nRes = _mm_movemask_ps(vcmp);
			return (nRes == 0);
		}
		void Vec3FCopy(_vector3FType& dst, const _vector3FType& src)
		{
			dst.vector3 = src.vector3;
		}
		void Vec3FCopy(_vector3FType& dst, const _vector4FType& src)
		{
			dst.vector3 = src.vector4;
		}

		void Vec3FAdd(_vector3FType& dst, const _vector3FType& lhs, const _vector3FType& rhs)
		{
			dst.vector3 = _mm_add_ps(lhs.vector3, rhs.vector3);
		}
		void Vec3FAdd(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
		{
			_vector3FType vec;
			Math::Vec3FSet(vec, fX, fY, fZ);
			Math::Vec3FAdd(dst, lhs, vec);
		}
		void Vec3FAdd(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fVal)
		{
			Math::Vec3FAdd(dst, lhs, fVal, fVal, fVal);
		}

		void Vec3FSub(_vector3FType& dst, const _vector3FType& lhs, const _vector3FType& rhs)
		{
			dst.vector3 = _mm_sub_ps(lhs.vector3, rhs.vector3);
		}
		void Vec3FSub(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
		{
			_vector3FType vec;
			Math::Vec3FSet(vec, fX, fY, fZ);
			Math::Vec3FSub(dst, lhs, vec);
		}
		void Vec3FSub(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fVal)
		{
			Math::Vec3FSub(dst, lhs, fVal, fVal, fVal);
		}

		void Vec3FMul(_vector3FType& dst, const _vector3FType& lhs, const _vector3FType& rhs)
		{
			dst.vector3 = _mm_mul_ps(lhs.vector3, rhs.vector3);
		}
		void Vec3FMul(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
		{
			_vector3FType vec;
			Math::Vec3FSet(vec, fX, fY, fZ);
			Math::Vec3FMul(dst, lhs, vec);
		}
		void Vec3FMul(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fVal)
		{
			Math::Vec3FMul(dst, lhs, fVal, fVal, fVal);
		}

		void Vec3FDiv(_vector3FType& dst, const _vector3FType& lhs, const _vector3FType& rhs)
		{
			dst.vector3 = _mm_div_ps(lhs.vector3, rhs.vector3);
		}
		void Vec3FDiv(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
		{
			_vector3FType vec;
			Math::Vec3FSet(vec, fX, fY, fZ);
			Math::Vec3FDiv(dst, lhs, vec);
		}
		void Vec3FDiv(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fVal)
		{
			Math::Vec3FDiv(dst, lhs, fVal, fVal, fVal);
		}

		FXD::F32 Vec3FSum(const _vector3FType& vec)
		{
			return (Math::Vec3FGetX(vec) + Math::Vec3FGetY(vec) + Math::Vec3FGetZ(vec));
		}
		FXD::F32 Vec3FDot(const _vector3FType& lhs, const _vector3FType& rhs)
		{
			_vector3FType vecDot;
			Math::Vec3FMul(vecDot, lhs, rhs);
			return Math::Vec3FSum(vecDot);
		}
		_vector3FType Vec3FCross(const _vector3FType& lhs, const _vector3FType& rhs)
		{
			_vector3FType retVec;
			__m128 t0 = _mm_shuffle_ps(lhs.vector3, lhs.vector3, _MM_SHUFFLE(3, 0, 2, 1));
			__m128 t1 = _mm_shuffle_ps(rhs.vector3, rhs.vector3, _MM_SHUFFLE(3, 1, 0, 2));
			__m128 m0 = _mm_mul_ps(t0, t1);

			__m128 t2 = _mm_shuffle_ps(lhs.vector3, lhs.vector3, _MM_SHUFFLE(3, 1, 0, 2));
			__m128 t3 = _mm_shuffle_ps(rhs.vector3, rhs.vector3, _MM_SHUFFLE(3, 0, 2, 1));
			__m128 m1 = _mm_mul_ps(t2, t3);

			retVec.vector3 = _mm_sub_ps(m0, m1);
			return retVec;
		}
		FXD::F32 Vec3FSquaredLength(const _vector3FType& vec)
		{
			return Math::Vec3FDot(vec, vec);
		}
		FXD::F32 Vec3FSquaredDistance(const _vector3FType& lhs, const _vector3FType& rhs)
		{
			_vector3FType out;
			Math::Vec3FSub(out, lhs, rhs);
			return Vec3FSquaredLength(out);
		}
		void Vec3FMidPoint(_vector3FType& out, const _vector3FType& lhs, const _vector3FType& rhs)
		{
			Math::Vec3FAdd(out, lhs, rhs);
			Math::Vec3FDiv(out, out, 2.0f);
		}
		FXD::F32 Vec3FAngleBetween(const _vector3FType& lhs, const _vector3FType& rhs)
		{
			FXD::F32 fLenProduct = (Math::Sqr(Math::Vec3FSquaredLength(lhs)) * Math::Sqr(Math::Vec3FSquaredLength(rhs)));

			// Divide by zero check
			if (fLenProduct < 1e-6f)
			{
				fLenProduct = 1e-6f;
			}

			FXD::F32 fVal = (Math::Vec3FDot(lhs, rhs) / fLenProduct);
			fVal = FXD::STD::clamp(fVal, -1.0f, 1.0f);
			return Math::FuncsMath::AcosF(fVal);
		}
		void Vec3FNegate(_vector3FType& vec)
		{
			vec.vector3 = _mm_xor_ps(vec.vector3, s_vecNegateBits);
		}
		void Vec3FMinimum(_vector3FType& out, const _vector3FType min)
		{
			out.vector3 = _mm_min_ps(out.vector3, min.vector3);
		}
		void Vec3FMaximum(_vector3FType& out, const _vector3FType max)
		{
			out.vector3 = _mm_max_ps(out.vector3, max.vector3);
		}
		void Vec3FClamp(_vector3FType& out, const _vector3FType& min, const _vector3FType& max)
		{
			Math::Vec3FMinimum(out, max);
			Math::Vec3FMaximum(out, min);
		}
		void Vec3FNormalise(_vector3FType& out)
		{
			FXD::F32 fLength = Math::Sqrt(Math::Vec3FSquaredLength(out));
			if (fLength != 0)
			{
				Math::Vec3FDiv(out, out, fLength);
			}
		}
		void Vec3FAbs(_vector3FType& out)
		{
			out.vector3 = _mm_andnot_ps(s_vecAbsBits, out.vector3);
		}
		void Vec3FCeil(_vector3FType& out)
		{
			__m128i v0 = _mm_setzero_si128();
			__m128i v1 = _mm_cmpeq_epi32(v0, v0);
			__m128i ji = _mm_srli_epi32(v1, 25);
			__m128 j = *(__m128*)&_mm_slli_epi32(ji, 23); //create vector 1.0f
			__m128i i = _mm_cvttps_epi32(out.vector3);
			__m128 fi = _mm_cvtepi32_ps(i);
			__m128 igx = _mm_cmplt_ps(fi, out.vector3);
			j = _mm_and_ps(igx, j);
			out.vector3 = _mm_add_ps(fi, j);
		}
		void Vec3FFloor(_vector3FType& out)
		{
			__m128i v0 = _mm_setzero_si128();
			__m128i v1 = _mm_cmpeq_epi32(v0, v0);
			__m128i ji = _mm_srli_epi32(v1, 25);
			__m128 j = *(__m128*)&_mm_slli_epi32(ji, 23); //create vector 1.0f
			__m128i i = _mm_cvttps_epi32(out.vector3);
			__m128 fi = _mm_cvtepi32_ps(i);
			__m128 igx = _mm_cmpgt_ps(fi, out.vector3);
			j = _mm_and_ps(igx, j);
			out.vector3 = _mm_sub_ps(fi, j);
		}

	} //namespace Math
} //namespace FXD
#endif //IsMathSSE()