// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_SSE_MATHSSE_H
#define FXDENGINE_MATH_SSE_MATHSSE_H

#if IsMathSSE()
#include <xmmintrin.h>
//#include <tmmintrin.h>

namespace FXD
{
	namespace Math
	{
		struct _vector2FType
		{
			FXD::F32 vector2[2];
		};
		struct _vector2IType
		{
			FXD::S32 vector2[2];
		};
		struct _vector3FType
		{
			__m128 vector3;
		};
		struct _vector4FType
		{
			__m128 vector4;
		};
		struct _matrix3Type
		{
			_vector3FType mtrx3[3];
		};
		struct _matrix4Type
		{
			_vector4FType mtrx4[4];
		};

		_MM_ALIGN16 const FXD::U32 s_vecNegateBitsInit[4] = { 0x80000000, 0x80000000, 0x80000000, 0x80000000 };
		const __m128 s_vecNegateBits = _mm_load_ps((const FXD::F32*)s_vecNegateBitsInit);

		_MM_ALIGN16 const FXD::U32 s_vecAbsBitsInit[4] = { 0x7fffffff, 0x7fffffff, 0x7fffffff, 0x7fffffff };
		const __m128 s_vecAbsBits = _mm_load_ps((const FXD::F32*)s_vecAbsBitsInit);

		_MM_ALIGN16 const FXD::F32 s_XMNoFractionInit[4] = { 8388608.0f, 8388608.0f, 8388608.0f, 8388608.0f };
		const __m128 s_XMNoFraction = _mm_load_ps((const FXD::F32*)s_XMNoFractionInit);


		#define vPPPM (_mm_set_ps(-0.0f, +0.0f, +0.0f, +0.0f))

		#define FXD_SHUFFLE(x, y, z, w) ((w)<<6 | (z)<<4 | (y)<<2 | (x))
		#define FXD_pshufd_ps(_a, _mask) _mm_shuffle_ps((_a), (_a), (_mask))
		#define FXD_splat_ps(_a, _i) FXD_pshufd_ps((_a), FXD_SHUFFLE(_i,_i,_i,_i))

	} //namespace Math
} //namespace FXD
#endif //IsMathSSE()
#endif //FXDENGINE_MATH_SSE_MATHSSE_H