// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_QUATERNION_H
#define FXDENGINE_MATH_QUATERNION_H

#include "FXDEngine/Math/Types.h"
#include "FXDEngine/Core/StreamOut.h"
#include "FXDEngine/Math/Vector3F.h"

namespace FXD
{
	namespace Math
	{
		// ------
		// QuaternionF
		// -
		// ------
		class FXD_ALIGN_AS(16) QuaternionF
		{
		public:
			friend class Matrix3F;
			friend class Matrix4F;

		public:
			static const QuaternionF kIdentity;

		public:
			QuaternionF(void);
			QuaternionF(FXD::F32 x, FXD::F32 y, FXD::F32 z, FXD::F32 w);
			QuaternionF(const Math::Radian& rPitch, const Math::Radian& rYaw, const Math::Radian& rRoll, Math::EulerAngleOrder eOrder = Math::EulerAngleOrder::XYZ);
			QuaternionF(const Math::Degree& dPitch, const Math::Degree& dYaw, const Math::Degree& dRoll, Math::EulerAngleOrder eOrder = Math::EulerAngleOrder::XYZ);
			QuaternionF(const Matrix3F& rot);
			~QuaternionF(void);

			bool operator==(const QuaternionF& rhs) const;
			bool operator!=(const QuaternionF& rhs) const;

			FXD::F32 operator[](FXD::U32 nID) const;
			FXD::F32 operator()(FXD::U32 nID) const;

			QuaternionF operator*(const QuaternionF& rhs) const;
			//QuaternionF operator*(FXD::F32 rhs) const;

			void from_euler_angles(const Math::Radian& rPitch, const Math::Radian& rYaw, const Math::Radian& rRoll, Math::EulerAngleOrder eOrder = Math::EulerAngleOrder::XYZ);

			static void set_euler_angles(QuaternionF& dst, const Math::Radian& rPitch, const Math::Radian& rYaw, const Math::Radian& rRoll, Math::EulerAngleOrder eOrder = Math::EulerAngleOrder::XYZ);

			static QuaternionF create_euler_angles(const Math::Radian& rPitch, const Math::Radian& rYaw, const Math::Radian& rRoll, Math::EulerAngleOrder eOrder = Math::EulerAngleOrder::XYZ);

		private:
			_vector4FType m_data;
		};

		// IStreamOut
		inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, QuaternionF const& rhs)
		{
			return ostr << rhs(0) << ' ' << rhs(1) << ' ' << rhs(2) << ' ' << rhs(3);
		}

		/*
		// ------
		// Quaternion
		// -
		// ------
		class Quaternion
		{
		public:
			static const FXD::F32 kEpsilon;
			static const Quaternion kZero;
			static const Quaternion kIdentity;

			private:
			struct EulerAngleOrderData
			{
				FXD::S32 a, b, c;
			};

		public:
			Quaternion(FXD::F32 w = 1.0f, FXD::F32 x = 0.0f, FXD::F32 y = 0.0f, FXD::F32 z = 0.0f);
			EXPLICIT Quaternion(const Matrix3F& rot);
			EXPLICIT Quaternion(const Vector3F& axis, const Degree& deg);
			EXPLICIT Quaternion(const Vector3F& axis, const Radian& rad);
			EXPLICIT Quaternion(const Vector3F& xaxis, const Vector3F& yaxis, const Vector3F& zaxis);
			EXPLICIT Quaternion(const Radian& xAngle, const Radian& yAngle, const Radian& zAngle);
			~Quaternion(void);

			Quaternion& operator=(const Quaternion& rhs);

			FXD::F32 operator[](const FXD::U32 i) const;
			FXD::F32& operator[](const FXD::U32 i);

			Quaternion operator+(const Quaternion& rhs) const;
			Quaternion operator-() const;
			Quaternion operator-(const Quaternion& rhs) const;
			Quaternion operator*(const Quaternion& rhs) const;
			Quaternion operator*(FXD::F32 rhs) const;

			bool operator==(const Quaternion& rhs) const;
			bool operator!=(const Quaternion& rhs) const;

			friend Quaternion operator*(FXD::F32 lhs, const Quaternion& rhs) { return Quaternion((lhs * rhs.w()), (lhs * rhs.x()), (lhs * rhs.y()), (lhs * rhs.z())); }

			FXD::F32 w(void) const { return m_w; }
			FXD::F32 x(void) const { return m_x; }
			FXD::F32 y(void) const { return m_y; }
			FXD::F32 z(void) const { return m_z; }

			void swap(Quaternion& rhs);																					// Exchange the contents of this quaternion with another.
			FXD::F32 dot(const Quaternion& other) const;																// Calculates the dot product of this quaternion and another.
			FXD::F32 normalise(void);																						// Normalizes this quaternion, and returns the previous length.
			Quaternion inverse(void) const;																				// Gets the inverse.
			Vector3F rotate(const Vector3F& vec) const;																// Rotates the provided vector.
			void fromRotationMatrix(const Matrix3F& mat);															// Initializes the quaternion from a 3x3 rotation matrix.
			void fromAxisAngle(const Vector3F& axis, const Degree& angle);										// Initializes the quaternion from an angle axis pair. Quaternion will represent a rotation of "angle" radians around "axis".
			void fromAxisAngle(const Vector3F& axis, const Radian& angle);										// Initializes the quaternion from an angle axis pair. Quaternion will represent a rotation of "angle" radians around "axis".
			void fromAxes(const Vector3F& xAxis, const Vector3F& yAxis, const Vector3F& zAxis);			// Initializes the quaternion from orthonormal set of axes. Quaternion will represent a rotation from base axes to the specified set of axes.
			void fromEulerAngles(const Degree& xAngle, const Degree& yAngle, const Degree& zAngle);	// Creates a quaternion from the provided Pitch/Yaw/Roll angles.
			void fromEulerAngles(const Radian& xAngle, const Radian& yAngle, const Radian& zAngle);	// Creates a quaternion from the provided Pitch/Yaw/Roll angles.
			void toRotationMatrix(Matrix3F& mat) const;																// Converts a quaternion to a rotation matrix.
			void toAxisAngle(Vector3F& axis, Degree& angle) const;													// Converts a quaternion to an angle axis pair.
			void toAxisAngle(Vector3F& axis, Radian& angle) const;													// Converts a quaternion to an angle axis pair.
			void toAxes(Vector3F& xAxis, Vector3F& yAxis, Vector3F& zAxis) const;								// Converts a quaternion to an orthonormal set of axes.
			bool toEulerAngles(Degree& xAngle, Degree& yAngle, Degree& zAngle) const;						// Extracts Pitch/Yaw/Roll rotations from this quaternion.
			bool toEulerAngles(Radian& xAngle, Radian& yAngle, Radian& zAngle) const;						// Extracts Pitch/Yaw/Roll rotations from this quaternion.
			Vector3F xAxis(void) const;																						// Gets the positive x-axis of the coordinate system transformed by this quaternion.
			Vector3F yAxis(void) const;																						// Gets the positive y-axis of the coordinate system transformed by this quaternion.
			Vector3F zAxis(void) const;																						// Gets the positive z-axis of the coordinate system transformed by this quaternion.
			void lookRotation(const Vector3F& forwardDir);															// Orients the quaternion so its negative z axis points to the provided direction.
			void lookRotation(const Vector3F& forwardDir, const Vector3F& upDir);							// Orients the quaternion so its negative z axis points to the provided direction.

			static Quaternion slerp(FXD::F32 t, const Quaternion& p, const Quaternion& q, bool bShortestPath = true);								// Performs spherical interpolation between two quaternions. Spherical interpolation neatly interpolates between two rotations without modifying the size of the vector it is applied to (unlike linear interpolation).
			static Quaternion getRotationFromTo(const Vector3F& from, const Vector3F& dst, const Vector3F& fallbackAxis = Vector3F::kZero);	// Gets the shortest arc quaternion to rotate this vector to the destination vector.

			private:
			FXD::F32 m_x, m_y, m_z, m_w;
			static const EulerAngleOrderData kEALookup[6];
		};

		// ------
		// Quaternion
		// -
		// ------
		inline Quaternion::Quaternion(FXD::F32 w, FXD::F32 x, FXD::F32 y, FXD::F32 z)
		: m_x(x)
		, m_y(y)
		, m_z(z)
		, m_w(w)
		{
		}
		inline Quaternion::Quaternion(const Matrix3F& rot)
		{
			fromRotationMatrix(rot);
		}
		inline Quaternion::Quaternion(const Vector3F& axis, const Degree& deg)
		{
			fromAxisAngle(axis, Radian(deg));
		}
		inline Quaternion::Quaternion(const Vector3F& axis, const Radian& rad)
		{
			fromAxisAngle(axis, rad);
		}
		inline Quaternion::Quaternion(const Vector3F& xaxis, const Vector3F& yaxis, const Vector3F& zaxis)
		{
			fromAxes(xaxis, yaxis, zaxis);
		}
		inline Quaternion::Quaternion(const Radian& xAngle, const Radian& yAngle, const Radian& zAngle)
		{
			fromEulerAngles(xAngle, yAngle, zAngle);
		}
		inline Quaternion::~Quaternion(void)
		{
		}

		inline Quaternion& Quaternion::operator=(const Quaternion& rhs) { m_x = rhs.m_x; m_y = rhs.m_y; m_z = rhs.m_z; m_w = rhs.m_w; return (*this); }
		inline FXD::F32 Quaternion::operator[](const FXD::U32 i) const { PRINT_COND_ASSERT((i < 4), "Math:"); return *(&m_x + i); }
		inline FXD::F32& Quaternion::operator[](const FXD::U32 i) { PRINT_COND_ASSERT((i < 4), "Math:"); return *(&m_x + i); }
		inline Quaternion Quaternion::operator+(const Quaternion& rhs) const { return Quaternion((m_w + rhs.w()), (m_x + rhs.x()), (m_y + rhs.y()), (m_z + rhs.z())); }
		inline Quaternion Quaternion::operator-() const { return Quaternion(-m_w, -m_x, -m_y, -m_z); }
		inline Quaternion Quaternion::operator-(const Quaternion& rhs) const { return Quaternion((m_w - rhs.w()), (m_x - rhs.x()), (m_y - rhs.y()), (m_z - rhs.z())); }
		inline Quaternion Quaternion::operator*(const Quaternion& rhs) const
		{
			return Quaternion(
				(m_w * rhs.w()) - (m_x * rhs.x()) - (m_y * rhs.y()) - (m_z * rhs.z()),
				(m_w * rhs.x()) + (m_x * rhs.w()) + (m_y * rhs.z()) - (m_z * rhs.y()),
				(m_w * rhs.y()) + (m_y * rhs.w()) + (m_z * rhs.x()) - (m_x * rhs.z()),
				(m_w * rhs.z()) + (m_z * rhs.w()) + (m_x * rhs.y()) - (m_y * rhs.x())
				);
		}
		inline Quaternion Quaternion::operator*(FXD::F32 rhs) const { return Quaternion((rhs * m_w), (rhs * m_x), (rhs * m_y), (rhs * m_z)); }
		inline bool Quaternion::operator==(const Quaternion& rhs) const { return ((rhs.m_x == m_x) && (rhs.m_y == m_y) && (rhs.m_z == m_z) && (rhs.m_w == m_w)); }
		inline bool Quaternion::operator!=(const Quaternion& rhs) const { return !operator==(rhs); }
		*/
	} //namespace Math
} //namespace FXD
#endif //FXDENGINE_MATH_QUATERNION_H
