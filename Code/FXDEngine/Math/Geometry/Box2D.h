// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_GEOMETRY_BOX2D_H
#define FXDENGINE_MATH_GEOMETRY_BOX2D_H

#include "FXDEngine/Math/Vector2F.h"

namespace FXD
{
	namespace Math
	{
		// ------
		// Box2D
		// -
		// ------
		class Box2D
		{
		public:
			static const Box2D kBoxEmpty;
			static const Box2D kBoxUnit;
			static const Box2D kBoxOne;

		public:
			Box2D(void);
			Box2D(const Box2D& rhs);
			Box2D(const Vector2F& min, const Vector2F& max);
			~Box2D(void);

			Box2D& operator=(const Box2D& rhs);

			bool operator==(const Box2D& rhs) const;
			bool operator!=(const Box2D& rhs) const;

			const FXD::F32 get_left(void) const		{ return m_min.x(); }

			const FXD::F32 get_right(void) const		{ return m_max.x(); }
				
			const FXD::F32 getTop(void) const			{ return m_min.y(); }

			const FXD::F32 get_bottom(void) const	{ return m_max.y(); }

			const FXD::F32 get_width(void) const		{ return (get_right() - get_left()); }

			const FXD::F32 get_height(void) const	{ return (get_bottom() - getTop()); }

			// Gets the corner of the box with minimum values (opposite to maximum corner).
			Vector2F& get_min(void)					{ return m_min; }
			const Vector2F& get_min(void) const	{ return m_min; }

			// Gets the corner of the box with maximum values (opposite to minimum corner).
			Vector2F& get_max(void)					{ return m_max; }
			const Vector2F& get_max(void) const	{ return m_max; }

			void set_left(FXD::F32 fVal)			{ m_min.x(fVal); }
			void set_right(FXD::F32 fVal)			{ m_max.x(fVal); }
			void set_top(FXD::F32 fVal)			{ m_min.y(fVal); }
			void set_bottom(FXD::F32 fVal)		{ m_max.y(fVal); }

			// Sets the corner of the box with minimum values (opposite to maximum corner).
			void set_min(const Vector2F& vec)	{ m_min = vec; }

			// Sets the corner of the box with maximum values (opposite to minimum corner).
			void set_max(const Vector2F& vec)	{ m_max = vec; }

			// Sets the minimum and maximum corners.
			void set_extents(const Vector2F& min, const Vector2F& max);

			// Scales the box around the center by multiplying its extents with the provided scale.
			void scale(const Vector2F& s);

			// Merges the two boxes, creating a new bounding box that encapsulates them both.
			void merge(const Box2D& rhs);

			// Expands the bounding box so it includes the provided point.
			void merge(const Vector2F& point);

			// Returns true if this and the provided box intersect.
			bool intersects(const Box2D& rhs) const;

			// Returns true if this and the provided box intersect.
			bool contains_completely(const Box2D& rhs) const;

			// Center of the box.
			Vector2F get_center(void) const;

			// Size of the box (difference between minimum and maximum corners)
			Vector2F get_size(void) const;

			// Extents of the box (distance from center to one of the corners)
			Vector2F get_half_size(void) const;

			// Radius of a sphere that fully encompasses the box.
			FXD::F32 get_radius(void) const;

			// Size of the volume in the box.
			FXD::F32 get_volume(void) const;

			// Returns true if the provided point is inside the bounding box.
			bool contains(const Vector2F& vec) const;

			// Returns true if the provided bounding box is completely inside the bounding box.
			bool contains(const Box2D& rhs) const;

		protected:
			Vector2F m_min;
			Vector2F m_max;
		};

	} //namespace Math
} //namespace FXD
#endif //FXDENGINE_MATH_GEOMETRY_BOX2D_H