// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_GEOMETRY_AABOX_H
#define FXDENGINE_MATH_GEOMETRY_AABOX_H

#include "FXDEngine/Math/Vector3F.h"
#include "FXDEngine/Core/Utility.h"

namespace FXD
{
	namespace Math
	{
		// ------
		// AABox
		// -
		// ------
		class AABox
		{
		public:
			static const AABox kBoxEmpty;
			static const AABox kBoxUnit;
			static const AABox kBoxOne;

		public:
			// Different corners of a box.
			/*
			1-----2
			/|    /|
			/ |   / |
			5-----4  |
			|  0--|--3
			| /   | /
			|/    |/
			6-----7
			*/
			// ------
			// E_AABBCorner
			// -
			// ------
			enum class E_AABBCorner : FXD::U32
			{
				Far_Left_Bottom = 0,
				Far_Left_Top = 1,
				Far_Right_Top = 2,
				Far_Right_Bottom = 3,
				Near_Right_Top = 4,
				Near_Left_Top = 5,
				Near_Left_Bottom = 6,
				Near_Right_Bottom = 7,
				Count,
				Unknown = 0xffff
			};

			AABox(void);
			AABox(const AABox& rhs);
			AABox(const Vector3F& min, const Vector3F& max);
			~AABox(void);

			AABox& operator=(const AABox& rhs);

			bool operator==(const AABox& rhs) const;
			bool operator!=(const AABox& rhs) const;

			const Vector3F& get_min(void) const { return m_minimum; }
			const Vector3F& get_max(void) const { return m_maximum; }
			Vector3F get_corner(AABox::E_AABBCorner eCorner) const;
			Vector3F get_center(void) const;
			Vector3F get_size(void) const;
			Vector3F get_half_size(void) const;
			FXD::F32 get_radius(void) const;
			FXD::F32 get_volume(void) const;

			void set_min(const Vector3F& vec) { m_minimum = vec; }
			void set_max(const Vector3F& vec) { m_maximum = vec; }
			void set_extents(const Vector3F& min, const Vector3F& max);

			void swap(AABox& rhs);
			void scale(const Vector3F& scale);

			void merge(const Vector3F& point);
			//void merge(const Sphere& sphere);
			void merge(const AABox& rhs);

			bool contains(const Vector3F& v) const;
			bool contains(const AABox& rhs) const;

			bool intersects(const AABox& rhs) const;
			bool intersects(const Sphere& sphere) const;
			bool intersects(const Plane& plane) const;
            Core::Pair< bool, FXD::F32 > intersects(const Ray& ray) const;
			bool intersects(const Ray& ray, FXD::F32& d1, FXD::F32& d2) const;

		protected:
			Vector3F m_minimum;
			Vector3F m_maximum;
		};

	} //namespace Math
} //namespace FXD
#endif //FXDENGINE_MATH_GEOMETRY_AABOX_H
