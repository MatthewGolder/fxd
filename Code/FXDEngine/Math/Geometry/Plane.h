// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_GEOMETRY_PLANE_H
#define FXDENGINE_MATH_GEOMETRY_PLANE_H

#include "FXDEngine/Container/Vector.h"
#include "FXDEngine/Math/Vector3F.h"
#include "FXDEngine/Core/Utility.h"

namespace FXD
{
	namespace Math
	{
		// ------
		// Plane
		// -
		// ------
		class Plane
		{
		public:
			/*
			* The "positive side" of the plane is the half space to which the plane normal points. The "negative side" is the
			* other half space. The flag "no side" indicates the plane itself.
			*/
			// ------
			// E_PlaneSide
			// -
			// ------
			enum class E_PlaneSide
			{
				None,
				Positive,
				Negative,
				Both,
				Count,
				Unknown = 0xffff
			};

		public:
			Plane(void);
			Plane(const Plane& rhs);
			Plane(const Vector3F& normal, FXD::F32 fDistance);
			Plane(FXD::F32 a, FXD::F32 b, FXD::F32 c, FXD::F32 fDistance);
			Plane(const Vector3F& normal, const Vector3F& point);
			Plane(const Vector3F& point0, const Vector3F& point1, const Vector3F& point2);
			~Plane(void);

			Plane& operator=(const Plane& rhs);

			bool operator==(const Plane& rhs) const;
			bool operator!=(const Plane& rhs) const;

			const Vector3F& get_normal(void) const		{ return m_normal; }
			FXD::F32 get_distance(void) const				{ return m_fDistance; }
			void set_normal(const Vector3F& normal)	{ m_normal = normal; }
			void set_distance(FXD::F32 dist)				{ m_fDistance = dist; }

			E_PlaneSide get_slide(const Vector3F& point) const;					// Returns the side of the plane where the point is located on.
			E_PlaneSide get_slide(const AABox& box) const;						// Returns the side where the alignedBox is. The flag Side_Both indicates an intersecting box. One corner ON the plane is sufficient to consider the box and the plane intersecting.
			E_PlaneSide get_slide(const Sphere& sphere) const;					// Returns the side where the sphere is. The flag Side_Both indicates an intersecting sphere.
			FXD::F32 get_distance(const Vector3F& point) const;					// Returns a distance from point to plane.


			void swap(Plane& rhs);
			Vector3F project_vector(const Vector3F& v) const;				// Project a vector onto the plane.
			FXD::F32 normalise(void);												// Normalizes the plane's normal and the length scale of d.


			bool intersects(const AABox& box) const;
			bool intersects(const Sphere& sphere) const;
			Core::Pair< bool, FXD::F32 > intersects(const Ray& ray) const;

		private:
			Vector3F m_normal;
			FXD::F32 m_fDistance;
		};
		using PlaneList = Container::Vector< Plane >;

	} //namespace Math
} //namespace FXD
#endif //FXDENGINE_MATH_GEOMETRY_PLANE_H
