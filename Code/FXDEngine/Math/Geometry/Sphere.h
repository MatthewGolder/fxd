// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_GEOMETRY_SPHERE_H
#define FXDENGINE_MATH_GEOMETRY_SPHERE_H

#include "FXDEngine/Math/Vector3F.h"
#include "FXDEngine/Core/Utility.h"

namespace FXD
{
	namespace Math
	{
		// ------
		// Sphere
		// -
		// ------
		class Sphere
		{
		public:
			Sphere(void);
			Sphere(const Vector3F& center, FXD::F32 fRadius);
			~Sphere(void);

			Sphere& operator=(const Sphere& rhs);

			bool operator==(const Sphere& rhs) const;
			bool operator!=(const Sphere& rhs) const;

			FXD::F32 get_radius(void) const				{ return m_fRadius; }
			const Vector3F& get_center(void) const		{ return m_center; }

			void set_radius(FXD::F32 fRadius)			{ m_fRadius = fRadius; }
			void set_center(const Vector3F& center)	{ m_center = center; }

			void swap(Sphere& rhs);
	
			void merge(const Vector3F& point);
			void merge(const Sphere& rhs);
			void merge(const AABox& box);

			bool contains(const Vector3F& point) const;
			
			bool intersects(const Sphere& sphere) const;
			bool intersects(const AABox& box) const;
			bool intersects(const Plane& plane) const;	
			Core::Pair<bool, FXD::F32> intersects(const Ray& ray, bool bDiscardInside = true) const;

		private:
			FXD::F32 m_fRadius;
			Vector3F m_center;
		};

	} //namespace Math
} //namespace FXD
#endif //FXDENGINE_MATH_GEOMETRY_SPHERE_H
