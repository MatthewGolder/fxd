// Creator - MatthewGolder
#include "FXDEngine/Math/Geometry/AABox.h"
#include "FXDEngine/Math/Geometry/Plane.h"
#include "FXDEngine/Math/Geometry/Ray.h"
#include "FXDEngine/Math/Geometry/Sphere.h"
#include "FXDEngine/Math/Math.h"
#include "FXDEngine/Math/Matrix4.h"

using namespace FXD;
using namespace Math;

// ------
// AABox
// -
// ------
const AABox AABox::kBoxEmpty(Vector3F(0.0f, 0.0f, 0.0f), Vector3F(0.0f, 0.0f, 0.0f));
const AABox AABox::kBoxUnit(Vector3F(0.0f, 0.0f, 0.0f), Vector3F(1.0f, 1.0f, 1.0f));
const AABox AABox::kBoxOne(Vector3F(1.0f, 1.0f, 1.0f), Vector3F(1.0f, 1.0f, 1.0f));

AABox::AABox(void)
	: m_minimum(Vector3F::kZero)
	, m_maximum(Vector3F::kOne)
{
	// Default to a null box 
	set_min(Vector3F(-0.5f, -0.5f, -0.5f));
	set_max(Vector3F(0.5f, 0.5f, 0.5f));
}

AABox::AABox(const AABox& copy)
	: m_minimum(Vector3F::kZero)
	, m_maximum(Vector3F::kOne)
{
	set_extents(copy.m_minimum, copy.m_maximum);
}

AABox::AABox(const Vector3F& min, const Vector3F& max)
	: m_minimum(Vector3F::kZero)
	, m_maximum(Vector3F::kOne)
{
	set_extents(min, max);
}

AABox::~AABox(void)
{
}

AABox& AABox::operator=(const AABox& rhs)
{
	set_extents(rhs.m_minimum, rhs.m_maximum);
	return (*this);
}

bool AABox::operator==(const AABox& rhs) const
{
	return
		(m_minimum == rhs.m_minimum) &&
		(m_maximum == rhs.m_maximum);
}

bool AABox::operator!=(const AABox& rhs) const
{
	return !((*this) == rhs);
}

Vector3F AABox::get_corner(AABox::E_AABBCorner eCorner) const
{
	switch (eCorner)
	{
		case AABox::E_AABBCorner::Far_Left_Bottom:
		{
			return m_minimum;
		}
		case AABox::E_AABBCorner::Far_Left_Top:
		{
			return Vector3F(m_minimum.x(), m_maximum.y(), m_minimum.z());
		}
		case AABox::E_AABBCorner::Far_Right_Top:
		{
			return Vector3F(m_maximum.x(), m_maximum.y(), m_minimum.z());
		}
		case AABox::E_AABBCorner::Far_Right_Bottom:
		{
			return Vector3F(m_maximum.x(), m_minimum.y(), m_minimum.z());
		}
		case AABox::E_AABBCorner::Near_Right_Bottom:
		{
			return Vector3F(m_maximum.x(), m_minimum.y(), m_maximum.z());
		}
		case AABox::E_AABBCorner::Near_Left_Bottom:
		{
			return Vector3F(m_minimum.x(), m_minimum.y(), m_maximum.z());
		}
		case AABox::E_AABBCorner::Near_Left_Top:
		{
			return Vector3F(m_minimum.x(), m_maximum.y(), m_maximum.z());
		}
		case AABox::E_AABBCorner::Near_Right_Top:
		{
			return m_maximum;
		}
		default:
		{
			return Vector3F::kZero;
		}
	}
}

Vector3F AABox::get_center(void) const
{
	return Vector3F(
		(m_maximum.x() + m_minimum.x()) * 0.5f,
		(m_maximum.y() + m_minimum.y()) * 0.5f,
		(m_maximum.z() + m_minimum.z()) * 0.5f);
}

Vector3F AABox::get_size(void) const
{
	return (m_maximum - m_minimum);
}

Vector3F AABox::get_half_size(void) const
{
	return (m_maximum - m_minimum) * 0.5f;
}

FXD::F32 AABox::get_radius(void) const
{
	return (m_maximum - m_minimum).length();
}

FXD::F32 AABox::get_volume(void) const
{
	Vector3F diff = (m_maximum - m_minimum);
	return (diff.x() * diff.y() * diff.z());
}

void AABox::set_extents(const Vector3F& min, const Vector3F& max)
{
	m_minimum = min;
	m_maximum = max;
}

void AABox::swap(AABox& rhs)
{
	FXD::STD::swap(m_minimum, rhs.m_minimum);
	FXD::STD::swap(m_maximum, rhs.m_maximum);
}

void AABox::scale(const Vector3F& scale)
{
	Vector3F center = get_center();
	Vector3F min = (center + (m_minimum - center) * scale);
	Vector3F max = (center + (m_maximum - center) * scale);
	set_extents(min, max);
}

void AABox::merge(const Vector3F& point)
{
	m_maximum.get_maximum(point);
	m_minimum.get_minimum(point);
}

void AABox::merge(const AABox& rhs)
{
	Vector3F min = m_minimum;
	Vector3F max = m_maximum;
	max.get_maximum(rhs.m_maximum);
	min.get_minimum(rhs.m_minimum);

	set_extents(min, max);
}

bool AABox::contains(const Vector3F& v) const
{
	return
		(m_minimum.x() <= v.x()) && (v.x() <= m_maximum.x()) &&
		(m_minimum.y() <= v.y()) && (v.y() <= m_maximum.y()) &&
		(m_minimum.z() <= v.z()) && (v.z() <= m_maximum.z());
}

bool AABox::contains(const AABox& rhs) const
{
	return
		(m_minimum.x() <= rhs.m_minimum.x()) &&
		(m_minimum.y() <= rhs.m_minimum.y()) &&
		(m_minimum.z() <= rhs.m_minimum.z()) &&
		(rhs.m_maximum.x() <= m_maximum.x()) &&
		(rhs.m_maximum.y() <= m_maximum.y()) &&
		(rhs.m_maximum.z() <= m_maximum.z());
}

bool AABox::intersects(const AABox& rhs) const
{
	// Use up to 6 separating planes
	if (m_maximum.x() < rhs.m_minimum.x())
	{
		return false;
	}
	if (m_maximum.y() < rhs.m_minimum.y())
	{
		return false;
	}
	if (m_maximum.z() < rhs.m_minimum.z())
	{
		return false;
	}
	if (m_minimum.x() > rhs.m_maximum.x())
	{
		return false;
	}
	if (m_minimum.y() > rhs.m_maximum.y())
	{
		return false;
	}
	if (m_minimum.z() > rhs.m_maximum.z())
	{
		return false;
	}
	return true;
}

bool AABox::intersects(const Sphere& sphere) const
{
	// Use splitting planes
	const Vector3F& center = sphere.get_center();
	FXD::F32 fRadius = sphere.get_radius();
	const Vector3F& min = get_min();
	const Vector3F& max = get_max();

	// Arvo's algorithm
	FXD::F32 fD = 0.0f;
	for (FXD::S32 n1 = 0; n1 < 3; ++n1)
	{
		if (center[n1] < min[n1])
		{
			FXD::F32 fS = (center[n1] - min[n1]);
			fD += (fS * fS);
		}
		else if (center[n1] > max[n1])
		{
			FXD::F32 fS = (center[n1] - max[n1]);
			fD += (fS * fS);
		}
	}
	return (fD <= (fRadius * fRadius));
}

bool AABox::intersects(const Plane& plane) const
{
	return (plane.get_slide(*this) == Plane::E_PlaneSide::Both);
}

Core::Pair< bool, FXD::F32 > AABox::intersects(const Ray& ray) const
{
	FXD::F32 fLowt = 0.0f;
	bool bHit = false;
	const Vector3F& min = get_min();
	const Vector3F& max = get_max();
	const Vector3F& rayorig = ray.get_origin();
	const Vector3F& raydir = ray.get_direction();

	// Check origin inside first
	if (((rayorig.x() > min.x()) && (rayorig.y() > min.y()) && (rayorig.z() > min.z())) && ((rayorig.x() < max.x()) && (rayorig.y() < max.y()) && (rayorig.z() < max.z())))
	{
		return Core::Pair< bool, FXD::F32 >(true, 0.0f);
	}

	// Check each face in turn, only check closest 3
	// Min x
	if ((rayorig.x() <= min.x()) && (raydir.x() > 0.0f))
	{
		FXD::F32 fT = (min.x() - rayorig.x()) / raydir.x();
		if (fT >= 0.0f)
		{
			// Substitute t back into ray and check bounds and dist
			Vector3F hitpoint = (rayorig + raydir * fT);
			if ((hitpoint.y() >= min.y()) && (hitpoint.y() <= max.y()) && (hitpoint.z() >= min.z()) && (hitpoint.z() <= max.z()) && ((!bHit) || (fT < fLowt)))
			{
				bHit = true;
				fLowt = fT;
			}
		}
	}
	// Max x
	if ((rayorig.x() >= max.x()) && (raydir.x() < 0.0f))
	{
		FXD::F32 fT = (max.x() - rayorig.x()) / raydir.x();
		if (fT >= 0.0f)
		{
			// Substitute t back into ray and check bounds and dist
			Vector3F hitpoint = (rayorig + raydir * fT);
			if ((hitpoint.y() >= min.y()) && (hitpoint.y() <= max.y()) && (hitpoint.z() >= min.z()) && (hitpoint.z() <= max.z()) && ((!bHit) || (fT < fLowt)))
			{
				bHit = true;
				fLowt = fT;
			}
		}
	}
	// Min y
	if ((rayorig.y() <= min.y()) && (raydir.y() > 0.0f))
	{
		FXD::F32 fT = (min.y() - rayorig.y()) / raydir.y();
		if (fT >= 0.0f)
		{
			// Substitute t back into ray and check bounds and dist
			Vector3F hitpoint = (rayorig + raydir * fT);
			if ((hitpoint.x() >= min.x()) && (hitpoint.x() <= max.x()) && (hitpoint.z() >= min.z()) && (hitpoint.z() <= max.z()) && ((!bHit) || (fT < fLowt)))
			{
				bHit = true;
				fLowt = fT;
			}
		}
	}
	// Max y
	if ((rayorig.y() >= max.y()) && (raydir.y() < 0.0f))
	{
		FXD::F32 fT = (max.y() - rayorig.y()) / raydir.y();
		if (fT >= 0.0f)
		{
			// Substitute t back into ray and check bounds and dist
			Vector3F hitpoint = (rayorig + raydir * fT);
			if ((hitpoint.x() >= min.x()) && (hitpoint.x() <= max.x()) && (hitpoint.z() >= min.z()) && (hitpoint.z() <= max.z()) && ((!bHit) || (fT < fLowt)))
			{
				bHit = true;
				fLowt = fT;
			}
		}
	}
	// Min z
	if ((rayorig.z() <= min.z()) && (raydir.z() > 0.0f))
	{
		FXD::F32 fT = (min.z() - rayorig.z()) / raydir.z();
		if (fT >= 0.0f)
		{
			// Substitute t back into ray and check bounds and dist
			Vector3F hitpoint = (rayorig + raydir * fT);
			if ((hitpoint.x() >= min.x()) && (hitpoint.x() <= max.x()) && (hitpoint.y() >= min.y()) && (hitpoint.y() <= max.y()) && ((!bHit) || (fT < fLowt)))
			{
				bHit = true;
				fLowt = fT;
			}
		}
	}
	// Max z
	if ((rayorig.z() >= max.z()) && (raydir.z() < 0.0f))
	{
		FXD::F32 fT = (max.z() - rayorig.z()) / raydir.z();
		if (fT >= 0.0f)
		{
			// Substitute t back into ray and check bounds and dist
			Vector3F hitpoint = (rayorig + raydir * fT);
			if ((hitpoint.x() >= min.x()) && (hitpoint.x() <= max.x()) && (hitpoint.y() >= min.y()) && (hitpoint.y() <= max.y()) && ((!bHit) || (fT < fLowt)))
			{
				bHit = true;
				fLowt = fT;
			}
		}
	}
	return Core::Pair< bool, FXD::F32 >(bHit, fLowt);
}

bool AABox::intersects(const Ray& ray, FXD::F32& d1, FXD::F32& d2) const
{
	const Vector3F& min = get_min();
	const Vector3F& max = get_max();
	const Vector3F& rayorig = ray.get_origin();
	const Vector3F& raydir = ray.get_direction();

	Vector3F absDir(Math::Abs(raydir[0]), Math::Abs(raydir[1]), Math::Abs(raydir[2]));

	// Sort the axis, ensure check minimise floating error axis first
	FXD::S32 nMax = 0;
	FXD::S32 nMid = 1;
	FXD::S32 nMin = 2;
	if (absDir[0] < absDir[2])
	{
		nMax = 2;
		nMin = 0;
	}
	if (absDir[1] < absDir[nMin])
	{
		nMid = nMin;
		nMin = 1;
	}
	else if (absDir[1] > absDir[nMax])
	{
		nMid = nMax;
		nMax = 1;
	}

	FXD::F32 fStart = 0.0f;
	FXD::F32 fEnd = Math::NumericLimits< FXD::F32 >::infinity();

#define _CALC_AXIS(i)															\
do {																					\
	FXD::F32 fDenom = (1.0f / raydir[i]);									\
	FXD::F32 fNewstart = (min[i] - rayorig[i]) * fDenom;				\
	FXD::F32 fNewend = (max[i] - rayorig[i]) * fDenom;					\
	if (fNewstart > fNewend) FXD::STD::swap(fNewstart, fNewend);	\
	if (fNewstart > fEnd || fNewend < fStart) return false;			\
	if (fNewstart > fStart) fStart = fNewstart;							\
	if (fNewend < fEnd) fEnd = fNewend;										\
} while(0)

	// Check each axis in turn
	_CALC_AXIS(nMax);
	if (absDir[nMid] < Math::NumericLimits< FXD::F32 >::epsilon())
	{
		// Parallel with middle and minimise axis, check bounds only
		if ((rayorig[nMid] < min[nMid]) || (rayorig[nMid] > max[nMid]) || (rayorig[nMin] < min[nMin]) || (rayorig[nMin] > max[nMin]))
		{
			return false;
		}
	}
	else
	{
		_CALC_AXIS(nMid);
		if (absDir[nMin] < Math::NumericLimits< FXD::F32 >::epsilon())
		{
			// Parallel with minimise axis, check bounds only
			if ((rayorig[nMin] < min[nMin]) || (rayorig[nMin] > max[nMin]))
			{
				return false;
			}
		}
		else
		{
			_CALC_AXIS(nMin);
		}
	}
#undef _CALC_AXIS

	d1 = fStart;
	d2 = fEnd;
	return true;
}