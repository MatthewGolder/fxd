// Creator - MatthewGolder
#include "FXDEngine/Math/Geometry/Box2D.h"

using namespace FXD;
using namespace Math;

// ------
// Box2D
// -
// ------
const Box2D Box2D::kBoxEmpty(Vector2F(0.0f, 0.0f), Vector2F(0.0f, 0.0f));
const Box2D Box2D::kBoxUnit(Vector2F(0.0f, 0.0f), Vector2F(1.0f, 1.0f));
const Box2D Box2D::kBoxOne(Vector2F(1.0f, 1.0f), Vector2F(1.0f, 1.0f));

Box2D::Box2D(void)
{
}
Box2D::Box2D(const Box2D& rhs)
	: m_min(rhs.m_min)
	, m_max(rhs.m_max)
{
}
Box2D::Box2D(const Vector2F& min, const Vector2F& max)
	: m_min(min)
	, m_max(max)
{
}
Box2D::~Box2D(void)
{
}

Box2D& Box2D::operator=(const Box2D& rhs)
{
	m_min = rhs.m_min;
	m_max = rhs.m_max;
	return (*this);
}

bool Box2D::operator==(const Box2D& rhs) const
{
	return
		(m_min == rhs.m_min) &&
		(m_max == rhs.m_max);
}
bool Box2D::operator!=(const Box2D& rhs) const
{
	return !((*this) == rhs);
}