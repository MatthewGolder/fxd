// Creator - MatthewGolder
#include "FXDEngine/Math/Geometry/Sphere.h"
#include "FXDEngine/Math/Geometry/AABox.h"
#include "FXDEngine/Math/Geometry/Plane.h"
#include "FXDEngine/Math/Geometry/Ray.h"
#include "FXDEngine/Math/Math.h"
#include "FXDEngine/Math/Matrix4.h"
#include "FXDEngine/Core/Algorithm.h"

using namespace FXD;
using namespace Math;

// ------
// Sphere
// -
// ------
Sphere::Sphere(void)
	: m_fRadius(1.0f)
	, m_center(Vector3F::kZero)
{
}

Sphere::Sphere(const Vector3F& center, FXD::F32 fRadius)
	: m_fRadius(fRadius)
	, m_center(center)
{
}

Sphere::~Sphere(void)
{
}

Sphere& Sphere::operator=(const Sphere& rhs)
{
	m_fRadius = rhs.m_fRadius;
	m_center = rhs.m_center;
	return (*this);
}

bool Sphere::operator==(const Sphere& rhs) const
{
	return ((rhs.m_fRadius == m_fRadius) && (rhs.m_center == m_center));
}

bool Sphere::operator!=(const Sphere& rhs) const
{
	return ((rhs.m_fRadius != m_fRadius) || (rhs.m_center != m_center));
}

void Sphere::swap(Sphere& rhs)
{
	FXD::STD::swap(m_fRadius, rhs.m_fRadius);
	FXD::STD::swap(m_center, rhs.m_center);
}

void Sphere::merge(const Vector3F& point)
{
	FXD::F32 fDist = point.distance(m_center);
	m_fRadius = FXD::STD::Max(m_fRadius, fDist);
}

void Sphere::merge(const Sphere& rhs)
{
	Vector3F newCenter = (m_center + rhs.m_center) * 0.5f;
	FXD::F32 fNewRadiusA = (newCenter.distance(m_center) + get_radius());
	FXD::F32 fNewRadiusB = (newCenter.distance(rhs.m_center) + rhs.get_radius());

	m_center = newCenter;
	m_fRadius = FXD::STD::Max(fNewRadiusA, fNewRadiusB);
}

void Sphere::merge(const AABox& box)
{
	for (FXD::U32 n1 = 0; n1 < FXD::STD::to_underlying(AABox::E_AABBCorner::Count); n1++)
	{
		merge(box.get_corner((AABox::E_AABBCorner)n1));
	}
}

bool Sphere::contains(const Vector3F& point) const
{
	return ((point - m_center).squared_length() <= Math::Sqr(m_fRadius));
}

bool Sphere::intersects(const Sphere& sphere) const
{
	return (sphere.m_center - m_center).squared_length() <= Math::Sqr(sphere.m_fRadius + m_fRadius);
}

bool Sphere::intersects(const AABox& box) const
{
	return box.intersects(*this);
}

bool Sphere::intersects(const Plane& plane) const
{
	return (Math::Abs(plane.get_distance(get_center())) <= get_radius());
}

Core::Pair< bool, FXD::F32 > Sphere::intersects(const Ray& ray, bool bDiscardInside) const
{
	const Vector3F& raydir = ray.get_direction();
	const Vector3F& rayorig = (ray.get_origin() - get_center());
	FXD::F32 fRadius = get_radius();

	// Check origin inside first
	if ((rayorig.squared_length() <= (fRadius * fRadius)) && (bDiscardInside))
	{
		return Core::Pair< bool, FXD::F32 >(true, 0.0f);
	}

	// t = (-fB +/- sqrt(fB * fB + (4.0f * fA * fC))) / 2a
	FXD::F32 fA = raydir.dot(raydir);
	FXD::F32 fB = (2.0f * rayorig.dot(raydir));
	FXD::F32 fC = (rayorig.dot(rayorig) - (fRadius * fRadius));

	// Determinant
	FXD::F32 fDet = (fB * fB) - (4.0f * fA * fC);
	if (fDet < 0.0f)
	{
		// No intersection
		return Core::Pair< bool, FXD::F32 >(false, 0.0f);
	}
	else
	{
		// If d == 0 there is one intersection, if d > 0 there are 2.
		// We only return the first one.
		FXD::F32 t = (-fB - Math::Sqrt(fDet)) / (2.0f * fA);
		if (t < 0.0f)
		{
			t = (-fB + Math::Sqrt(fDet)) / (2.0f * fA);
		}
		return Core::Pair< bool, FXD::F32 >(true, t);
	}
}