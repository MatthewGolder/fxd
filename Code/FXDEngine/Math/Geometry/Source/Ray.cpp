// Creator - MatthewGolder
#include "FXDEngine/Math/Geometry/Ray.h"
#include "FXDEngine/Math/Geometry/AABox.h"
#include "FXDEngine/Math/Geometry/Plane.h"
#include "FXDEngine/Math/Geometry/Sphere.h"
#include "FXDEngine/Math/Math.h"
#include "FXDEngine/Math/Matrix4.h"

using namespace FXD;
using namespace Math;

// ------
// Ray
// -
// ------
Ray::Ray(void)
	: m_origin(Vector3F::kZero)
	, m_direction(Vector3F::kUnitZ)
{
}

Ray::Ray(const Vector3F& origin, const Vector3F& direction)
	: m_origin(origin)
	, m_direction(direction)
{
}

Ray::~Ray(void)
{
}

Ray& Ray::operator=(const Ray& rhs)
{
	m_origin = rhs.m_origin;
	m_direction = rhs.m_direction;
	return (*this);
}

bool Ray::operator==(const Ray& rhs) const
{
	return ((rhs.m_origin == m_origin) && (rhs.m_direction == m_direction));
}

bool Ray::operator!=(const Ray& rhs) const
{
	return ((rhs.m_origin != m_origin) || (rhs.m_direction != m_direction));
}

void Ray::swap(Ray& rhs)
{
	FXD::STD::swap(m_origin, rhs.m_origin);
	FXD::STD::swap(m_direction, rhs.m_direction);
}

void Ray::transform(const Matrix4F& matrix)
{
	Vector3F vEnd = get_point(1.0f);
	m_direction = matrix * m_origin;
	vEnd = matrix * vEnd;
	m_direction = Vector3F::get_normalise(vEnd - m_origin);
}

Core::Pair< bool, FXD::F32 > Ray::intersects(const Plane& plane) const
{
	return plane.intersects(*this);
}

Core::Pair< bool, FXD::F32 > Ray::intersects(const Sphere& sphere) const
{
	return sphere.intersects(*this);
}

Core::Pair< bool, FXD::F32 > Ray::intersects(const AABox& box) const
{
	return box.intersects(*this);
}

Core::Pair< bool, FXD::F32 > Ray::intersects(const Vector3F& a, const Vector3F& b, const Vector3F& c, const Vector3F& normal, bool bPositiveSide, bool bNegativeSide) const
{
	// Calculate intersection with plane.
	FXD::F32 t;
	{
		// Check intersect side
		FXD::F32 fDenom = normal.dot(get_direction());
		if (fDenom > +Math::NumericLimits< FXD::F32 >::epsilon())
		{
			if (!bNegativeSide)
			{
				return Core::Pair< bool, FXD::F32 >(false, 0.0f);
			}
		}
		else if (fDenom < -Math::NumericLimits< FXD::F32 >::epsilon())
		{
			if (!bPositiveSide)
			{
				return Core::Pair< bool, FXD::F32 >(false, 0.0f);
			}
		}
		else
		{
			// Parallel or triangle area is close to zero when
			// the plane normal not normalized.
			return Core::Pair< bool, FXD::F32 >(false, 0.0f);
		}

		t = (normal.dot(a - get_origin()) / fDenom);
		if (t < 0.0f)
		{
			// Intersection is behind origin
			return Core::Pair< bool, FXD::F32 >(false, 0.0f);
		}
	}

	// Calculate the largest area projection plane in X, Y or Z.
	FXD::U32 nI0 = 1;
	FXD::U32 nI1 = 2;
	{
		FXD::F32 n0 = Math::Abs(normal[0]);
		FXD::F32 n1 = Math::Abs(normal[1]);
		FXD::F32 n2 = Math::Abs(normal[2]);
		if (n1 > n2)
		{
			if (n1 > n0)
			{
				nI0 = 0;
			}
		}
		else
		{
			if (n2 > n0)
			{
				nI1 = 0;
			}
		}
	}

	// Check the intersection point is inside the triangle.
	{
		FXD::F32 u1 = b[nI0] - a[nI0];
		FXD::F32 v1 = b[nI1] - a[nI1];
		FXD::F32 u2 = c[nI0] - a[nI0];
		FXD::F32 v2 = c[nI1] - a[nI1];
		FXD::F32 u0 = t * get_direction()[nI0] + get_origin()[nI0] - a[nI0];
		FXD::F32 v0 = t * get_direction()[nI1] + get_origin()[nI1] - a[nI1];

		FXD::F32 fAlpha = ((u0 * v2) - (u2 * v0));
		FXD::F32 fBeta = ((u1 * v0) - (u0 * v1));
		FXD::F32 fArea = ((u1 * v2) - (u2 * v1));

		// epsilon to avoid FXD::F32 precision errors.
		const FXD::F32 kEpsilon = 1e-6f;
		FXD::F32 fTolerance = (-kEpsilon * fArea);
		if (fArea > 0.0f)
		{
			if ((fAlpha < fTolerance) || (fBeta < fTolerance) || ((fAlpha + fBeta) >(fArea - fTolerance)))
			{
				return Core::Pair< bool, FXD::F32 >(false, 0.0f);
			}
		}
		else
		{
			if ((fAlpha < fTolerance) || (fBeta < fTolerance) || ((fAlpha + fBeta) < (fArea - fTolerance)))
			{
				return Core::Pair< bool, FXD::F32 >(false, 0.0f);
			}
		}
	}
	return Core::Pair< bool, FXD::F32 >(true, t);
}