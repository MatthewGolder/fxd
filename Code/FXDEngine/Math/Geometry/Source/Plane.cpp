// Creator - MatthewGolder
#include "FXDEngine/Math/Geometry/Plane.h"
#include "FXDEngine/Math/Geometry/AABox.h"
#include "FXDEngine/Math/Geometry/Ray.h"
#include "FXDEngine/Math/Geometry/Sphere.h"
#include "FXDEngine/Math/Math.h"
#include "FXDEngine/Math/Matrix3.h"

using namespace FXD;
using namespace Math;

// ------
// Plane
// -
// ------
Plane::Plane(void)
	: m_normal(Vector3F::kZero)
	, m_fDistance(0.0f)
{
}

Plane::Plane(const Plane& rhs)
	: m_normal(rhs.m_normal)
	, m_fDistance(rhs.m_fDistance)
{
}

Plane::Plane(const Vector3F& normal, FXD::F32 fDistance)
	: m_normal(normal)
	, m_fDistance(-fDistance)
{
}

Plane::Plane(FXD::F32 a, FXD::F32 b, FXD::F32 c, FXD::F32 fDistance)
	: m_normal(a, b, c)
	, m_fDistance(fDistance)
{
}

Plane::Plane(const Vector3F& normal, const Vector3F& point)
{
	m_normal = normal;
	m_fDistance = -normal.dot(point);
}

Plane::Plane(const Vector3F& point0, const Vector3F& point1, const Vector3F& point2)
{
	Vector3F kEdge1 = (point1 - point0);
	Vector3F kEdge2 = (point2 - point0);
	m_normal = kEdge1.cross(kEdge2);
	m_normal.normalise();
	m_fDistance = -m_normal.dot(point0);
}

Plane::~Plane(void)
{
}

Plane& Plane::operator=(const Plane& rhs)
{
	m_fDistance = rhs.m_fDistance;
	m_normal = rhs.m_normal;
	return (*this);
}

bool Plane::operator==(const Plane& rhs) const
{
	return ((rhs.m_fDistance == m_fDistance) && (rhs.m_normal == m_normal));
}

bool Plane::operator!=(const Plane& rhs) const
{
	return ((rhs.m_fDistance != m_fDistance) || (rhs.m_normal != m_normal));
}

FXD::F32 Plane::get_distance(const Vector3F& point) const
{
	return (m_normal.dot(point) + m_fDistance);
}

Plane::E_PlaneSide Plane::get_slide(const Vector3F& point) const
{
	FXD::F32 fDistance = get_distance(point);

	if (fDistance < 0.0f)
	{
		return Plane::E_PlaneSide::Negative;
	}
	if (fDistance > 0.0f)
	{
		return Plane::E_PlaneSide::Positive;
	}
	return Plane::E_PlaneSide::None;
}

Plane::E_PlaneSide Plane::get_slide(const AABox& box) const
{
	// Calculate the distance between box centre and the plane
	FXD::F32 fDistance = get_distance(box.get_center());

	// Calculate the maximize allows absolute distance for
	// the distance between box centre and plane
	Vector3F halfSize = box.get_half_size();
	FXD::F32 fMaxAbsDist = Math::Abs(m_normal.x() * halfSize.x()) + Math::Abs(m_normal.y() * halfSize.y()) + Math::Abs(m_normal.z() * halfSize.z());

	if (fDistance < -fMaxAbsDist)
	{
		return Plane::E_PlaneSide::Negative;
	}

	if (fDistance > +fMaxAbsDist)
	{
		return Plane::E_PlaneSide::Positive;
	}
	return Plane::E_PlaneSide::Both;
}

Plane::E_PlaneSide Plane::get_slide(const Sphere& sphere) const
{
	// Calculate the distance between box centre and the plane
	FXD::F32 fDistance = get_distance(sphere.get_center());
	FXD::F32 fRadius = sphere.get_radius();

	if (fDistance < -fRadius)
	{
		return Plane::E_PlaneSide::Negative;
	}
	if (fDistance > +fRadius)
	{
		return Plane::E_PlaneSide::Positive;
	}
	return Plane::E_PlaneSide::Both;
}

void Plane::swap(Plane& rhs)
{
	FXD::STD::swap(m_fDistance, rhs.m_fDistance);
	FXD::STD::swap(m_normal, rhs.m_normal);
}

Vector3F Plane::project_vector(const Vector3F& point) const
{
	// We know plane normal is unit length, so use simple method
	/*
	Matrix3F xform;
	xform[0][0] = 1.0f - m_normal.x() * m_normal.x();
	xform[0][1] = -m_normal.x() * m_normal.y();
	xform[0][2] = -m_normal.x() * m_normal.z();
	xform[1][0] = -m_normal.y() * m_normal.x();
	xform[1][1] = 1.0f - m_normal.y() * m_normal.y();
	xform[1][2] = -m_normal.y() * m_normal.z();
	xform[2][0] = -m_normal.z() * m_normal.x();
	xform[2][1] = -m_normal.z() * m_normal.y();
	xform[2][2] = 1.0f - m_normal.z() * m_normal.z();
	return xform.transform(point);
	*/
	return Vector3F();
}

FXD::F32 Plane::normalise(void)
{
	FXD::F32 fLength = m_normal.length();

	// Will also work for zero-sized vectors, but will change nothing
	if (fLength > 1e-08f)
	{
		FXD::F32 fInvLength = 1.0f / fLength;
		m_normal *= fInvLength;
		m_fDistance *= fInvLength;
	}
	return fLength;
}

bool Plane::intersects(const AABox& box) const
{
	return box.intersects(*this);
}

bool Plane::intersects(const Sphere& sphere) const
{
	return sphere.intersects(*this);
}

Core::Pair< bool, FXD::F32 > Plane::intersects(const Ray& ray) const
{
	FXD::F32 fDenom = m_normal.dot(ray.get_direction());
	if (Math::Abs(fDenom) < Math::NumericLimits< FXD::F32 >::epsilon())
	{
		// Parallel
		return Core::Pair< bool, FXD::F32 >(false, 0.0f);
	}
	else
	{
		FXD::F32 fNom = m_normal.dot(ray.get_origin()) + m_fDistance;
		FXD::F32 ft = -(fNom / fDenom);
		return Core::Pair< bool, FXD::F32 >(ft >= 0.0f, ft);
	}
}