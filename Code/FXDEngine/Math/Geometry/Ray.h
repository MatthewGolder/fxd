// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_GEOMETRY_RAY_H
#define FXDENGINE_MATH_GEOMETRY_RAY_H

#include "FXDEngine/Math/Vector3F.h"
#include "FXDEngine/Core/Utility.h"

namespace FXD
{
	namespace Math
	{
		// ------
		// Ray
		// -
		// ------
		class Ray
		{
		public:
			Ray(void);
			Ray(const Vector3F& origin, const Vector3F& direction);
			~Ray(void);

			Ray& operator=(const Ray& rhs);

			bool operator==(const Ray& rhs) const;
			bool operator!=(const Ray& rhs) const;

			Vector3F operator*(FXD::F32 t) const { return get_point(t); }

			const Vector3F& get_origin(void) const { return m_origin; }
			const Vector3F& get_direction(void) const { return m_direction; }
			Vector3F get_point(FXD::F32 t) const { return Vector3F(m_origin + (m_direction * t)); }

			void set_origin(const Vector3F& origin) { m_origin = origin; }
			void set_direction(const Vector3F& dir) { m_direction = dir; }

			void swap(Ray& rhs);

			void transform(const Matrix4F& matrix);

			Core::Pair< bool, FXD::F32 > intersects(const Plane& plane) const;
			Core::Pair< bool, FXD::F32 > intersects(const Sphere& sphere) const;
			Core::Pair< bool, FXD::F32 > intersects(const AABox& box) const;
			Core::Pair< bool, FXD::F32 > intersects(const Vector3F& a, const Vector3F& b, const Vector3F& c, const Vector3F& normal, bool bPositiveSide = true, bool bNegativeSide = true) const;

		private:
			Vector3F m_origin;
			Vector3F m_direction;
		};

	} //namespace Math
} //namespace FXD
#endif //FXDENGINE_MATH_GEOMETRY_RAY_H
