// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_RADIAN_H
#define FXDENGINE_MATH_RADIAN_H

#include "FXDEngine/Math/Types.h"

namespace FXD
{
	namespace Math
	{
		// ------
		// Radian
		// -
		// ------
		class Radian
		{
		public:
			EXPLICIT Radian(FXD::F32 fRad = 0.0f);
			Radian(const Degree& deg);
			~Radian(void);

			Radian& operator=(FXD::F32 rhs);
			Radian& operator=(const Radian& rhs);
			Radian& operator=(const Degree& rhs);

			const Radian& operator+() const;
			Radian operator+(FXD::F32 fRad) const;
			Radian operator+(const Radian& rad) const;
			Radian operator+(const Degree& deg) const;

			Radian operator-() const;
			Radian operator-(FXD::F32 fRad) const;
			Radian operator-(const Radian& rad) const;
			Radian operator-(const Degree& deg) const;

			Radian operator*(FXD::F32 fRad) const;
			Radian operator*(const Radian& rad) const;
			Radian operator*(const Degree& deg) const;

			Radian operator/(FXD::F32 fRad) const;
			Radian operator/(const Radian& rad) const;
			Radian operator/(const Degree& deg) const;

			Radian& operator+=(FXD::F32 fRad);
			Radian& operator+=(const Radian& rad);
			Radian& operator+=(const Degree& deg);

			Radian& operator-=(FXD::F32 fRad);
			Radian& operator-=(const Radian& rad);
			Radian& operator-=(const Degree& deg);

			Radian& operator*=(FXD::F32 fRad);
			Radian& operator*=(const Radian& rad);
			Radian& operator*=(const Degree& deg);

			Radian& operator/=(FXD::F32 fRad);
			Radian& operator/=(const Radian& rad);
			Radian& operator/=(const Degree& deg);

			bool operator< (const Radian& rad) const { return (m_fRad < rad.m_fRad); }
			bool operator<= (const Radian& rad) const { return (m_fRad <= rad.m_fRad); }
			bool operator> (const Radian& rad) const { return (m_fRad > rad.m_fRad); }
			bool operator>= (const Radian& rad) const { return (m_fRad >= rad.m_fRad); }
			bool operator== (const Radian& rad) const { return (m_fRad == rad.m_fRad); }
			bool operator!= (const Radian& rad) const { return (m_fRad != rad.m_fRad); }

			friend Radian operator* (FXD::F32 lhs, const Radian& rhs) { return Radian(lhs * rhs.m_fRad); }
			friend Radian operator/ (FXD::F32 lhs, const Radian& rhs) { return Radian(lhs / rhs.m_fRad); }
			friend Radian operator+ (Radian& lhs, FXD::F32 rhs) { return Radian(lhs.m_fRad + rhs); }
			friend Radian operator+ (FXD::F32 lhs, const Radian& rhs) { return Radian(lhs + rhs.m_fRad); }
			friend Radian operator- (const Radian& lhs, FXD::F32 rhs) { return Radian(lhs.m_fRad - rhs); }
			friend Radian operator- (const FXD::F32 lhs, const Radian& rhs) { return Radian(lhs - rhs.m_fRad); }

			operator FXD::F32(void) const;

			FXD::F32 to_degrees(void) const;
			FXD::F32 to_radians(void) const;

			Radian wrap(void);

		private:
			FXD::F32 m_fRad;
		};
	} //namespace Math
} //namespace FXD
#endif //FXDENGINE_MATH_RADIAN_H
