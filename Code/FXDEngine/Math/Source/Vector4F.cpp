// Creator - MatthewGolder
#include "FXDEngine/Math/Vector4F.h"
#include "FXDEngine/Math/Math.h"
#include "FXDEngine/Core/Debugger.h"

using namespace FXD;
using namespace Math;

// ------
// Vector4F
// -
// ------
const Vector4F Vector4F::kZero(0.0f);
const Vector4F Vector4F::kOne(1.0f);
const Vector4F Vector4F::kInf(Math::NumericLimits< FXD::F32 >::infinity());
const Vector4F Vector4F::kNegInf(-Math::NumericLimits< FXD::F32 >::infinity());
const Vector4F Vector4F::kUnitX(1.0f, 0.0f, 0.0f, 0.0f);
const Vector4F Vector4F::kUnitY(0.0f, 1.0f, 0.0f, 0.0f);
const Vector4F Vector4F::kUnitZ(0.0f, 0.0f, 1.0f, 0.0f);
const Vector4F Vector4F::kUnitW(0.0f, 0.0f, 0.0f, 1.0f);

Vector4F::Vector4F(void)
{
	Vec4FSet(m_data, 0.0f);
}
Vector4F::Vector4F(FXD::F32 val)
{
	Vec4FSet(m_data, val);
}
Vector4F::Vector4F(FXD::F32 x, FXD::F32 y, FXD::F32 z, FXD::F32 w)
{
	Vec4FSet(m_data, x, y, z, w);
}
Vector4F::~Vector4F(void)
{
}

FXD::F32 Vector4F::operator[](FXD::U32 nID) const
{
	PRINT_COND_ASSERT((nID < 4), "Math: Index out of bounds");
	return Vec4FGetIndex(m_data, nID);
}
FXD::F32 Vector4F::operator()(FXD::U32 nID) const
{
	PRINT_COND_ASSERT((nID < 4), "Math: Index out of bounds");
	return Vec4FGetIndex(m_data, nID);
}

Vector4F& Vector4F::operator=(const Vector4F& rhs)
{
	Vec4FCopy(m_data, rhs.m_data);
	return (*this);
}
Vector4F& Vector4F::operator=(FXD::F32 rhs)
{
	Vec4FSet(m_data, rhs);
	return (*this);
}

Vector4F Vector4F::operator-() const
{
	Vector4F out = (*this);
	out.negate();
	return out;
}

FXD::F32 Vector4F::x(void) const
{
	return Vec4FGetX(m_data);
}
FXD::F32 Vector4F::y(void) const
{
	return Vec4FGetY(m_data);
}
FXD::F32 Vector4F::z(void) const
{
	return Vec4FGetZ(m_data);
}
FXD::F32 Vector4F::w(void) const
{
	return Vec4FGetW(m_data);
}
FXD::F32 Vector4F::index(FXD::U32 nID)
{
	PRINT_COND_ASSERT((nID < 4), "Math: Index out of bounds");
	return Vec4FGetIndex(m_data, nID);
}

void Vector4F::x(FXD::F32 x)
{
	Vec4FSetX(m_data, x);
}
void Vector4F::y(FXD::F32 y)
{
	Vec4FSetY(m_data, y);
}
void Vector4F::z(FXD::F32 z)
{
	Vec4FSetZ(m_data, z);
}
void Vector4F::w(FXD::F32 w)
{
	Vec4FSetW(m_data, w);
}
void Vector4F::index(FXD::U32 nID, FXD::F32 fVal)
{
	PRINT_COND_ASSERT((nID < 4), "Math: Index out of bounds");
	Vec4FSetIndex(m_data, nID, fVal);
}

FXD::F32 Vector4F::dot(const Vector4F& rhs) const
{
	return Vec4FDot(m_data, rhs.m_data);
}
FXD::F32 Vector4F::length(void) const
{
	return Math::Sqrt(squared_length());
}
FXD::F32 Vector4F::squared_length(void) const
{
	return Vec4FSquaredLength(m_data);
}
FXD::F32 Vector4F::distance(const Vector4F& rhs) const
{
	return Math::Sqrt(sqrd_distance(rhs));
}
FXD::F32 Vector4F::sqrd_distance(const Vector4F& rhs) const
{
	return Vec4FSquaredDistance(m_data, rhs.m_data);
}
Vector4F Vector4F::midpoint(const Vector4F& rhs) const
{
	Vector4F retVec;
	Vec4FMidPoint(retVec.m_data, m_data, rhs.m_data);
	return retVec;
}

void Vector4F::negate(void)
{
	Vec4FNegate(m_data);
}
void Vector4F::minimum(const Vector4F& min)
{
	Vec4FMinimum(m_data, min.m_data);
}
void Vector4F::maximum(const Vector4F& max)
{
	Vec4FMaximum(m_data, max.m_data);
}
void Vector4F::grid_snap(const FXD::F32 fGridSize)
{
	Vec4FSet(m_data, Math::GridSnap(x(), fGridSize), Math::GridSnap(y(), fGridSize), Math::GridSnap(z(), fGridSize), Math::GridSnap(w(), fGridSize));
}
void Vector4F::clamp(const Vector4F& min, const Vector4F& max)
{
	Vec4FClamp(m_data, min.m_data, max.m_data);
}
void Vector4F::normalise(void)
{
	Vec4FNormalise(m_data);
}
void Vector4F::abs(void)
{
	Vec4FAbs(m_data);
}
void Vector4F::ceil(void)
{
	Vec4FCeil(m_data);
}
void Vector4F::floor(void)
{
	Vec4FFloor(m_data);
}

Vector4F Vector4F::get_negate(void) const
{
	Vector4F retVec = (*this);
	retVec.negate();
	return retVec;
}
Vector4F Vector4F::get_minimum(const Vector4F& min) const
{
	Vector4F retVec = (*this);
	retVec.minimum(min);
	return retVec;
}
Vector4F Vector4F::get_maximum(const Vector4F& max) const
{
	Vector4F retVec = (*this);
	retVec.maximum(max);
	return retVec;
}
Vector4F Vector4F::get_grid_snap(const FXD::F32 fGridSize)
{
	Vector4F retVec = (*this);
	retVec.grid_snap(fGridSize);
	return retVec;
}
Vector4F Vector4F::get_clamp(const Vector4F& min, const Vector4F& max) const
{
	Vector4F retVec = (*this);
	retVec.clamp(min, max);
	return retVec;
}
Vector4F Vector4F::get_normalise(void) const
{
	Vector4F retVec = (*this);
	retVec.normalise();
	return retVec;
}
Vector4F Vector4F::get_abs(void) const
{
	Vector4F retVec = (*this);
	retVec.abs();
	return retVec;
}
Vector4F Vector4F::get_ceil(void) const
{
	Vector4F retVec = (*this);
	retVec.ceil();
	return retVec;
}
Vector4F Vector4F::get_floor(void) const
{
	Vector4F retVec = (*this);
	retVec.floor();
	return retVec;
}

// STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS 
FXD::F32 Vector4F::dot(const Vector4F& lhs, const Vector4F& rhs)
{
	return lhs.dot(rhs);
}
FXD::F32 Vector4F::distance(const Vector4F& lhs, const Vector4F& rhs)
{
	return lhs.distance(rhs);
}
FXD::F32 Vector4F::sqrd_distance(const Vector4F& lhs, const Vector4F& rhs)
{
	return lhs.sqrd_distance(rhs);
}
Vector4F Vector4F::midpoint(const Vector4F& lhs, const Vector4F& rhs)
{
	return lhs.midpoint(rhs);

}

void Vector4F::set_negate(Vector4F& vec)
{
	vec.negate();
}
void Vector4F::set_minimum(Vector4F& vec, const Vector4F& min)
{
	vec.minimum(min);
}
void Vector4F::set_maximum(Vector4F& vec, const Vector4F& max)
{
	vec.maximum(max);
}
void Vector4F::set_grid_snap(Vector4F& vec, const FXD::F32 fGridSize)
{
	vec.grid_snap(fGridSize);
}
void Vector4F::set_clamp(Vector4F& vec, const Vector4F& min, const Vector4F& max)
{
	vec.clamp(min, max);
}
void Vector4F::set_normalise(Vector4F& vec)
{
	vec.normalise();
}
void Vector4F::set_abs(Vector4F& vec)
{
	vec.abs();
}
void Vector4F::set_ceil(Vector4F& vec)
{
	vec.ceil();
}
void Vector4F::set_floor(Vector4F& vec)
{
	vec.floor();
}
void Vector4F::set_lerp(Vector4F& vec, const Vector4F& lhs, const Vector4F& rhs, FXD::F32 fVal)
{
	vec = lhs + (rhs - lhs) * fVal;
}

Vector4F Vector4F::get_negate(const Vector4F& vec)
{
	Vector4F retVec = vec;
	Vector4F::set_negate(retVec);
	return retVec;
}
Vector4F Vector4F::get_minimum(const Vector4F& vec, const Vector4F& min)
{
	Vector4F retVec = vec;
	Vector4F::set_minimum(retVec, min);
	return retVec;
}
Vector4F Vector4F::get_maximum(const Vector4F& vec, const Vector4F& max)
{
	Vector4F retVec = vec;
	Vector4F::set_maximum(retVec, max);
	return retVec;
}
Vector4F Vector4F::get_grid_snap(const Vector4F& vec, const FXD::F32 fGridSize)
{
	Vector4F retVec = vec;
	Vector4F::set_grid_snap(retVec, fGridSize);
	return retVec;
}
Vector4F Vector4F::get_clamp(const Vector4F& vec, const Vector4F& min, const Vector4F& max)
{
	Vector4F retVec = vec;
	Vector4F::set_clamp(retVec, min, max);
	return retVec;
}
Vector4F Vector4F::get_normalise(const Vector4F& vec)
{
	Vector4F retVec = vec;
	Vector4F::set_normalise(retVec);
	return retVec;
}
Vector4F Vector4F::get_abs(const Vector4F& vec)
{
	Vector4F retVec = vec;
	Vector4F::set_abs(retVec);
	return retVec;
}
Vector4F Vector4F::get_ceil(const Vector4F& vec)
{
	Vector4F retVec = vec;
	Vector4F::set_ceil(retVec);
	return retVec;
}
Vector4F Vector4F::get_floor(const Vector4F& vec)
{
	Vector4F retVec = vec;
	Vector4F::set_floor(retVec);
	return retVec;
}
Vector4F Vector4F::get_lerp(const Vector4F& lhs, const Vector4F& rhs, FXD::F32 fVal)
{
	Vector4F retVec;
	Vector4F::set_lerp(retVec, lhs, rhs, fVal);
	return retVec;
}
