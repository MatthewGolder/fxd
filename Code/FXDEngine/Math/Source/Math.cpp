// Creator - MatthewGolder
#include "FXDEngine/Math/Math.h"
#include "FXDEngine/Math/Constants.h"
#include "FXDEngine/Math/Radian.h"
#include <cstdlib>

using namespace FXD;
using namespace Math;

// Random 
FXD::S32 FXD::Math::Rand(void)
{
	return std::rand();
}

FXD::F64 FXD::Math::RandomUnit(void)
{
	return ((FXD::F64)FXD::Math::Rand() / RAND_MAX);
}

FXD::F32 FXD::Math::RandomUnsignedFloat(void)
{
	FXD::U32 nRand = FXD::Math::Rand();
	return (FXD::F32)((FXD::F64)nRand / (FXD::F64)RAND_MAX);
}

FXD::F32 FXD::Math::RandomSignedFloat(void)
{
	return (FXD::Math::RandomUnsignedFloat() * 2.0f) - 1.0f;
}

FXD::F32 FXD::Math::RandomRange(const FXD::F32 fMin, const FXD::F32 fMax)
{
	return (fMin + (fMax - fMin) * FXD::Math::RandomUnsignedFloat());
}

// ------
// FuncsMath
// -
// ------
FXD::F32 FuncsMath::CosRad(const Math::Radian& val)
{
	return Math::Cos(val.to_radians());
}

FXD::F32 FuncsMath::SinRad(const Math::Radian& val)
{
	return Math::Sin(val.to_radians());
}

FXD::F32 FuncsMath::TanRad(const Math::Radian& val)
{
	return Math::Tan(val.to_radians());
}

Math::Radian FuncsMath::AcosFRad(const FXD::F32 fVal)
{
	if (-1.0f < fVal)
	{
		return (fVal < 1.0f) ? Radian(Math::Acos(fVal)) : Math::Radian(0.0f);
	}
	else
	{
		return Radian(PI);
	}
}
Math::Radian FuncsMath::AsinFRad(const FXD::F32 fVal)
{
	if (-1.0f < fVal)
	{
		return (fVal < 1.0f) ? Radian(Math::Asin(fVal)) : Math::Radian(PI_HALF);
	}
	else
	{
		return Radian(-PI_HALF);
	}
}