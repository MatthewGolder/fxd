// Creator - MatthewGolder
#include "FXDEngine/Math/UnitHelpers.h"

using namespace FXD;
using namespace Math;
using namespace Weight;
using namespace Distance;
using namespace Temperature;

// ------
// Kilograms
// -
// ------
Kilograms::Kilograms(const Weight::Pounds& lb)
	: m_fVal(lb.m_fVal * 0.4535923703803783)
{
}
Kilograms::Kilograms(const Weight::Stone& st)
	: m_fVal(st.m_fVal * 6.35029317991006)
{
}

// ------
// Pounds
// -
// ------
Pounds::Pounds(const Weight::Kilograms& kg)
	: m_fVal(kg.m_fVal * 2.20462262)
{
}
Pounds::Pounds(const Weight::Stone& st)
	: m_fVal(st.m_fVal * 14.0)
{
}

// ------
// Stone
// -
// ------
Stone::Stone(const Weight::Kilograms& kg)
	: m_fVal(kg.m_fVal * 0.15747304442)
{
}
Stone::Stone(const Weight::Pounds& lb)
	: m_fVal(lb.m_fVal * 0.07142857148948241)
{
}

// ------
// Meters
// -
// ------
Meters::Meters(const Distance::Miles& mi)
	: m_fVal(mi.m_fVal * 1609.344)
{
}
Meters::Meters(const Distance::Inches& in)
	: m_fVal(in.m_fVal * 0.0254)
{
}

// ------
// Inches
// -
// ------
Inches::Inches(const Distance::Meters& me)
	: m_fVal(me.m_fVal * 39.37007874)
{
}
Inches::Inches(const Distance::Miles& mi)
	: m_fVal(mi.m_fVal * 63360.0)
{
}

// ------
// Miles
// -
// ------
Miles::Miles(const Distance::Meters& me)
	: m_fVal(me.m_fVal * 0.000621371)
{
}
Miles::Miles(const Distance::Inches& in)
	: m_fVal(in.m_fVal * 0.0000157828283)
{
}

// ------
// Fahrenheit
// -
// ------
Fahrenheit::Fahrenheit(const Temperature::Celsius& ce)
	: m_fVal(ce.m_fVal * 9.0 / 5.0 + 32.0)
{
}
Fahrenheit::Fahrenheit(const Temperature::Kelvin& ke)
	: m_fVal(ke.m_fVal * 9.0 / 5.0 - 459.67)
{
}

// ------
// Celsius
// -
// ------
Celsius::Celsius(const Temperature::Fahrenheit& fe)
	: m_fVal((fe.m_fVal - 32.0) * 5.0 / 9.0)
{
}
Celsius::Celsius(const Temperature::Kelvin& ke)
	: m_fVal(ke.m_fVal - 273.15)
{
}

// ------
// Kelvin
// -
// ------
Kelvin::Kelvin(const Temperature::Fahrenheit& fe)
	: m_fVal((fe.m_fVal + 459.67) * 5.0 / 9.0)
{
}
Kelvin::Kelvin(const Temperature::Celsius& ce)
	: m_fVal(ce.m_fVal + 273.15)
{
}