// Creator - MatthewGolder
#include "FXDEngine/Math/Matrix4.h"
#include "FXDEngine/Math/Matrix3.h"
#include "FXDEngine/Math/Vector3F.h"
#include "FXDEngine/Math/Vector4F.h"
#include "FXDEngine/Math/Quaternion.h"
#include "FXDEngine/Math/Geometry/Plane.h"

using namespace FXD;
using namespace Math;

const Matrix4F Matrix4F::kZero = Matrix4F(
	0.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 0.0f);
const Matrix4F Matrix4F::kIdentity = Matrix4F(
	1.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f);

// ------
// Matrix4F
// -
// ------
Matrix4F::Matrix4F(void)
{
	Mat4Copy(m_data, Matrix4F::kIdentity.m_data);
}

Matrix4F::Matrix4F(FXD::F32 m00, FXD::F32 m01, FXD::F32 m02, FXD::F32 m03, FXD::F32 m10, FXD::F32 m11, FXD::F32 m12, FXD::F32 m13, FXD::F32 m20, FXD::F32 m21, FXD::F32 m22, FXD::F32 m23, FXD::F32 m30, FXD::F32 m31, FXD::F32 m32, FXD::F32 m33)
{
	Mat4Set(m_data, m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32, m33);
}

Matrix4F::Matrix4F(const Matrix4F& mat)
{
	Mat4Copy(m_data, mat.m_data);
}

Matrix4F::Matrix4F(const Matrix3F& mat)
{
	Mat4Copy(m_data, mat.m_data);
}

Matrix4F::~Matrix4F(void)
{
}

FXD::F32 Matrix4F::operator()(FXD::U32 nRow, FXD::U32 nCol) const
{
	return get_row(nRow)[nCol];
}

Matrix4F Matrix4F::operator+(const Matrix4F& rhs) const
{
	Matrix4F out;
	Mat4Add(out.m_data, m_data, rhs.m_data);
	return out;
}
Matrix4F Matrix4F::operator-(const Matrix4F& rhs) const
{
	Matrix4F out;
	Mat4Sub(out.m_data, m_data, rhs.m_data);
	return out;
}
Matrix4F Matrix4F::operator*(const Matrix4F& rhs) const
{
	Matrix4F out;
	Mat4Mul(out.m_data, m_data, rhs.m_data);
	return out;
}

Vector3F Matrix4F::operator*(const Vector3F& rhs) const
{
	Vector3F out;
	Mat4Mul(out.m_data, m_data, rhs.m_data);
	return out;
}

Vector4F Matrix4F::operator*(const Vector4F& rhs) const
{
	Vector4F out;
	Mat4Mul(out.m_data, m_data, rhs.m_data);
	return out;
}

Matrix4F& Matrix4F::operator+=(const Matrix4F& rhs)
{
	Mat4Add(m_data, m_data, rhs.m_data);
	return (*this);
}
Matrix4F& Matrix4F::operator-=(const Matrix4F& rhs)
{
	Mat4Sub(m_data, m_data, rhs.m_data);
	return (*this);
}
Matrix4F& Matrix4F::operator*=(const Matrix4F& rhs)
{
	Matrix4F tmp;
	Mat4Mul(tmp.m_data, m_data, rhs.m_data);
	Mat4Copy(m_data, tmp.m_data);
	return (*this);
}
Matrix4F Matrix4F::operator-() const
{
	Matrix4F out = (*this);
	out.negate();
	return out;
}

FXD::F32 Matrix4F::detrament(void) const
{
	return Mat4Detrament(m_data);
}

void Matrix4F::negate(void)
{
	Mat4Negate(m_data);
}

void Matrix4F::transpose(void)
{
	_matrix4Type matTemp;
	Mat4Transpose(matTemp, m_data);
	Mat4Copy(m_data, matTemp);
}

void Matrix4F::inverse(void)
{
	_matrix4Type matTemp;
	Mat4Inverse(matTemp, m_data);
	Mat4Copy(m_data, matTemp);
}

void Matrix4F::set_row(FXD::U32 nRow, const Vector4F& vec)
{
	Mat4SetRow(m_data, nRow, vec.m_data);
}

void Matrix4F::set_row(FXD::U32 nRow, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ, FXD::F32 fW)
{
	Mat4SetRow(m_data, nRow, fX, fY, fZ, fW);
}

void Matrix4F::set_column(FXD::U32 nCol, const Vector4F& vec)
{
	Mat4SetColumn(m_data, nCol, vec.m_data);
}

void Matrix4F::set_column(FXD::U32 nCol, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ, FXD::F32 fW)
{
	Mat4SetColumn(m_data, nCol, fX, fY, fZ, fW);
}

void Matrix4F::set_translation(const Vector3F& vec)
{
	Mat4SetTranslation(m_data, vec.m_data);
}

void Matrix4F::set_translation(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	Mat4SetTranslation(m_data, fX, fY, fZ);
}

void Matrix4F::set_right(const Vector3F& vec)
{
	Mat4SetRight(m_data, vec.m_data);
}

void Matrix4F::set_right(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	Mat4SetRight(m_data, fX, fY, fZ);
}

void Matrix4F::set_up(const Vector3F& vec)
{
	Mat4SetUp(m_data, vec.m_data);
}

void Matrix4F::set_up(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	Mat4SetUp(m_data, fX, fY, fZ);
}
void Matrix4F::set_forward(const Vector3F& vec)
{
	Mat4SetForward(m_data, vec.m_data);
}

void Matrix4F::set_forward(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	Mat4SetForward(m_data, fX, fY, fZ);
}

void Matrix4F::set_scale(const Vector3F& vec)
{
	Mat4SetScale(m_data, vec.m_data);
}

void Matrix4F::set_scale(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	Mat4SetScale(m_data, fX, fY, fZ);
}

void Matrix4F::set_rotationX(FXD::F32 fAngleX)
{
	Mat4SetRotationX(m_data, fAngleX);
}

void Matrix4F::set_rotationY(FXD::F32 fAngleY)
{
	Mat4SetRotationY(m_data, fAngleY);
}

void Matrix4F::set_rotationZ(FXD::F32 fAngleZ)
{
	Mat4SetRotationZ(m_data, fAngleZ);
}

void Matrix4F::set_rotation_pitch_yaw_roll(Math::Radian rPitch, Math::Radian rYaw, Math::Radian rRoll, Math::EulerAngleOrder eOrder)
{
	Mat4SetRotationPitchYawRoll(m_data, rPitch, rYaw, rRoll, eOrder);
}

void Matrix4F::set_rotation_pitch_yaw_roll(const Vector3F& angle, Math::EulerAngleOrder eOrder)
{
	Mat4SetRotationPitchYawRoll(m_data, angle.m_data, eOrder);
}

void Matrix4F::set_rotation_quaternion(const QuaternionF& quat)
{
	Mat4SetRotationQuaternion(m_data, quat.m_data);
}

void Matrix4F::trs(const Vector3F& translation, const QuaternionF& rotation, const Vector3F& scale)
{
	Mat4SetTRS(m_data, translation.m_data, rotation.m_data, scale.m_data);
}

void Matrix4F::lookat_lh(const Vector3F& eye, const Vector3F& forward, const Vector3F& up)
{
	Mat4LookAtLH(m_data, eye.m_data, forward.m_data, up.m_data);
}

void Matrix4F::perspective_lh(FXD::F32 fViewWidth, FXD::F32 fViewHeight, FXD::F32 fNearZ, FXD::F32 fFarZ)
{
	Mat4PerspectiveLH(m_data, fViewWidth, fViewHeight, fNearZ, fFarZ);
}

void Matrix4F::perspective_fov_lh(FXD::F32 fFOVAngleY, FXD::F32 fAspectHByW, FXD::F32 fNearZ, FXD::F32 fFarZ)
{
	Mat4PerspectiveFOVLH(m_data, fFOVAngleY, fAspectHByW, fNearZ, fFarZ);
}

void Matrix4F::orthographic_lh(FXD::F32 fViewWidth, FXD::F32 fViewHeight, FXD::F32 fNearZ, FXD::F32 fFarZ)
{
	Mat4OrthographicLH(m_data, fViewWidth, fViewHeight, fNearZ, fFarZ);
}

Matrix4F Matrix4F::get_negate(void) const
{
	Matrix4F retMat = (*this);
	Mat4Negate(retMat.m_data);
	return retMat;
}

Matrix4F Matrix4F::get_transpose(void) const
{
	Matrix4F retMat;
	Mat4Transpose(retMat.m_data, m_data);
	return retMat;
}

Matrix4F Matrix4F::get_inverse(void) const
{
	Matrix4F retMat;
	Mat4Inverse(retMat.m_data, m_data);
	return retMat;
}

Vector4F Matrix4F::get_row(FXD::U32 nRow) const
{
	Vector4F retVec;
	Mat4GetRow(retVec.m_data, m_data, nRow);
	return retVec;
}

Vector4F Matrix4F::get_column(FXD::U32 nCol) const
{
	Vector4F retVec;
	Mat4GetColumn(retVec.m_data, m_data, nCol);
	return retVec;
}

Vector3F Matrix4F::get_translation(void) const
{
	Vector3F retVec;
	Mat4GetTranslation(retVec.m_data, m_data);
	return retVec;
}

Vector3F Matrix4F::get_right(void) const
{
	Vector3F retVec;
	Mat4GetRight(retVec.m_data, m_data);
	return retVec;
}

Vector3F Matrix4F::get_up(void) const
{
	Vector3F retVec;
	Mat4GetUp(retVec.m_data, m_data);
	return retVec;
}

Vector3F Matrix4F::get_forward(void) const
{
	Vector3F retVec;
	Mat4GetForward(retVec.m_data, m_data);
	return retVec;
}

Vector3F Matrix4F::get_scale(void) const
{
	Vector3F retVec;
	Mat4GetScale(retVec.m_data, m_data);
	return retVec;
}

// STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS 
void Matrix4F::set_negate(Matrix4F& dst)
{
	dst.negate();
}

void Matrix4F::set_transpose(Matrix4F& dst)
{
	dst.transpose();
}

void Matrix4F::set_inverse(Matrix4F& dst)
{
	dst.inverse();
}

void Matrix4F::set_translation(Matrix4F& dst, const Vector3F& vec)
{
	dst.set_translation(vec);
}

void Matrix4F::set_translation(Matrix4F& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	dst.set_translation(fX, fY, fZ);
}

void Matrix4F::set_right(Matrix4F& dst, const Vector3F& vec)
{
	dst.set_right(vec);
}

void Matrix4F::set_right(Matrix4F& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	dst.set_right(fX, fY, fZ);
}

void Matrix4F::set_up(Matrix4F& dst, const Vector3F& vec)
{
	dst.set_up(vec);
}

void Matrix4F::set_up(Matrix4F& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	dst.set_up(fX, fY, fZ);
}

void Matrix4F::set_forward(Matrix4F& dst, const Vector3F& vec)
{
	dst.set_forward(vec);
}

void Matrix4F::set_forward(Matrix4F& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	dst.set_forward(fX, fY, fZ);
}

void Matrix4F::set_scale(Matrix4F& dst, const Vector3F& vec)
{
	dst.set_scale(vec);
}

void Matrix4F::set_scale(Matrix4F& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	dst.set_scale(fX, fY, fZ);
}

void Matrix4F::set_rotationX(Matrix4F& dst, FXD::F32 fAngleX)
{
	dst.set_rotationX(fAngleX);
}

void Matrix4F::set_rotationY(Matrix4F& dst, FXD::F32 fAngleY)
{
	dst.set_rotationY(fAngleY);
}

void Matrix4F::set_rotationZ(Matrix4F& dst, FXD::F32 fAngleZ)
{
	dst.set_rotationZ(fAngleZ);
}

void Matrix4F::set_rotation_pitch_yaw_roll(Matrix4F& dst, Math::Radian rPitch, Math::Radian rYaw, Math::Radian rRoll, Math::EulerAngleOrder eOrder)
{
	dst.set_rotation_pitch_yaw_roll(rPitch, rYaw, rRoll, eOrder);
}

void Matrix4F::set_rotation_pitch_yaw_roll(Matrix4F& dst, const Vector3F& angle, Math::EulerAngleOrder eOrder)
{
	dst.set_rotation_pitch_yaw_roll(angle, eOrder);
}

void Matrix4F::set_rotation_quaternion(Matrix4F& dst, const QuaternionF& quat)
{
	dst.set_rotation_quaternion(quat);
}

void Matrix4F::set_trs(Matrix4F& dst, const Vector3F& translation, const QuaternionF& rotation, const Vector3F& scale)
{
	dst.trs(translation, rotation, scale);
}

void Matrix4F::set_lookat_lh(Matrix4F& dst, const Vector3F& eye, const Vector3F& forward, const Vector3F& up)
{
	dst.lookat_lh(eye, forward, up);
}

void Matrix4F::set_perspective_lh(Matrix4F& dst, FXD::F32 fViewWidth, FXD::F32 fViewHeight, FXD::F32 fNearZ, FXD::F32 fFarZ)
{
	dst.perspective_lh(fViewWidth, fViewHeight, fNearZ, fFarZ);
}

void Matrix4F::set_perspective_fov_lh(Matrix4F& dst, FXD::F32 fFOVAngleY, FXD::F32 fAspectHByW, FXD::F32 fNearZ, FXD::F32 fFarZ)
{
	dst.perspective_fov_lh(fFOVAngleY, fAspectHByW, fNearZ, fFarZ);
}

void Matrix4F::set_orthographic_lh(Matrix4F& dst, FXD::F32 fViewWidth, FXD::F32 fViewHeight, FXD::F32 fNearZ, FXD::F32 fFarZ)
{
	dst.orthographic_lh(fViewWidth, fViewHeight, fNearZ, fFarZ);
}

FXD::F32 Matrix4F::get_detrament(const Matrix4F& mat)
{
	return mat.detrament();
}

Matrix4F Matrix4F::get_negate(const Matrix4F& mat)
{
	Matrix4F retMat = mat;
	Matrix4F::set_negate(retMat);
	return retMat;
}

Matrix4F Matrix4F::get_transpose(const Matrix4F& mat)
{
	Matrix4F retMat = mat;
	Matrix4F::set_transpose(retMat);
	return retMat;
}

Matrix4F Matrix4F::get_inverse(const Matrix4F& mat)
{
	Matrix4F retMat = mat;
	Matrix4F::set_inverse(retMat);
	return retMat;
}
Vector4F Matrix4F::get_row(const Matrix4F& mat, FXD::U32 nRow)
{
	return mat.get_row(nRow);
}

Vector4F Matrix4F::get_column(const Matrix4F& mat, FXD::U32 nCol)
{
	return mat.get_column(nCol);
}

Vector3F Matrix4F::get_translation(const Matrix4F& mat)
{
	return mat.get_translation();
}

Vector3F Matrix4F::get_right(const Matrix4F& mat)
{
	return mat.get_right();
}

Vector3F Matrix4F::get_up(const Matrix4F& mat)
{
	return mat.get_up();
}

Vector3F Matrix4F::get_forward(const Matrix4F& mat)
{
	return mat.get_forward();
}

Vector3F Matrix4F::get_scale(const Matrix4F& mat)
{
	return mat.get_scale();
}

Matrix4F Matrix4F::create_translation(const Vector3F& vec)
{
	Matrix4F retMat;
	Matrix4F::set_translation(retMat, vec);
	return retMat;
}

Matrix4F Matrix4F::create_translation(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	Matrix4F retMat;
	Matrix4F::set_translation(retMat, fX, fY, fZ);
	return retMat;
}

Matrix4F Matrix4F::create_up(const Vector3F& vec)
{
	Matrix4F retMat;
	Matrix4F::set_up(retMat, vec);
	return retMat;
}

Matrix4F Matrix4F::create_up(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	Matrix4F retMat;
	Matrix4F::set_up(retMat, fX, fY, fZ);
	return retMat;
}

Matrix4F Matrix4F::create_forward(const Vector3F& vec)
{
	Matrix4F retMat;
	Matrix4F::set_forward(retMat, vec);
	return retMat;
}

Matrix4F Matrix4F::create_forward(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	Matrix4F retMat;
	Matrix4F::set_forward(retMat, fX, fY, fZ);
	return retMat;
}

Matrix4F Matrix4F::create_scale(const Vector3F& vec)
{
	Matrix4F retMat;
	Matrix4F::set_scale(retMat, vec);
	return retMat;
}

Matrix4F Matrix4F::create_scale(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	Matrix4F retMat;
	Matrix4F::set_scale(retMat, fX, fY, fZ);
	return retMat;
}

Matrix4F Matrix4F::create_rotationX(FXD::F32 fAngleX)
{
	Matrix4F retMat;
	Matrix4F::set_rotationX(retMat, fAngleX);
	return retMat;
}

Matrix4F Matrix4F::create_rotationY(FXD::F32 fAngleY)
{
	Matrix4F retMat;
	Matrix4F::set_rotationY(retMat, fAngleY);
	return retMat;
}

Matrix4F Matrix4F::create_rotationZ(FXD::F32 fAngleZ)
{
	Matrix4F retMat;
	Matrix4F::set_rotationZ(retMat, fAngleZ);
	return retMat;
}

Matrix4F Matrix4F::create_rotation_pitch_yaw_roll(Math::Radian rPitch, Math::Radian rYaw, Math::Radian rRoll, Math::EulerAngleOrder eOrder)
{
	Matrix4F retMat;
	Matrix4F::set_rotation_pitch_yaw_roll(retMat, rPitch, rYaw, rRoll, eOrder);
	return retMat;
}

Matrix4F Matrix4F::create_rotation_pitch_yaw_roll(const Vector3F& angle, Math::EulerAngleOrder eOrder)
{
	Matrix4F retMat;
	Matrix4F::set_rotation_pitch_yaw_roll(retMat, angle, eOrder);
	return retMat;
}

Matrix4F Matrix4F::create_rotation_quaternion(const QuaternionF& quat)
{
	Matrix4F retMat;
	Matrix4F::set_rotation_quaternion(retMat, quat);
	return retMat;
}

Matrix4F Matrix4F::create_trs(const Vector3F& translation, const QuaternionF& rotation, const Vector3F& scale)
{
	Matrix4F retMat;
	Matrix4F::set_trs(retMat, translation, rotation, scale);
	return retMat;
}

Matrix4F Matrix4F::create_lookat_lh(const Vector3F& eye, const Vector3F& forward, const Vector3F& up)
{
	Matrix4F retMat;
	Matrix4F::set_lookat_lh(retMat, eye, forward, up);
	return retMat;
}

Matrix4F Matrix4F::create_perspective_lh(FXD::F32 fViewWidth, FXD::F32 fViewHeight, FXD::F32 fNearZ, FXD::F32 fFarZ)
{
	Matrix4F retMat;
	Matrix4F::set_perspective_lh(retMat, fViewWidth, fViewHeight, fNearZ, fFarZ);
	return retMat;
}

Matrix4F Matrix4F::create_perspective_fov_lh(FXD::F32 fFOVAngleY, FXD::F32 fAspectHByW, FXD::F32 fNearZ, FXD::F32 fFarZ)
{
	Matrix4F retMat;
	Matrix4F::set_perspective_fov_lh(retMat, fFOVAngleY, fAspectHByW, fNearZ, fFarZ);
	return retMat;
}

Matrix4F Matrix4F::create_orthographic_lh(FXD::F32 fViewWidth, FXD::F32 fViewHeight, FXD::F32 fNearZ, FXD::F32 fFarZ)
{
	Matrix4F retMat;
	Matrix4F::set_orthographic_lh(retMat, fViewWidth, fViewHeight, fNearZ, fFarZ);
	return retMat;
}

/*
// ------
// Matrix4
// -
// ------
const Matrix4 Matrix4::kZero(
	0.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 0.0f);

const Matrix4 Matrix4::kIdentity(
	1.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f);

static FXD::F32 MINOR(const Matrix4& m_matrix, const FXD::U32 r0, const FXD::U32 r1, const FXD::U32 r2, const FXD::U32 c0, const FXD::U32 c1, const FXD::U32 c2)
{
	return
		m_matrix[r0][c0] * (m_matrix[r1][c1] * m_matrix[r2][c2] - m_matrix[r2][c1] * m_matrix[r1][c2]) -
		m_matrix[r0][c1] * (m_matrix[r1][c0] * m_matrix[r2][c2] - m_matrix[r2][c0] * m_matrix[r1][c2]) +
		m_matrix[r0][c2] * (m_matrix[r1][c0] * m_matrix[r2][c1] - m_matrix[r2][c0] * m_matrix[r1][c1]);
}

void Matrix4::extract3x3Matrix(Matrix3& m3x3) const
{
	m3x3.m_matrix[0][0] = m_matrix[0][0];
	m3x3.m_matrix[0][1] = m_matrix[0][1];
	m3x3.m_matrix[0][2] = m_matrix[0][2];
	m3x3.m_matrix[1][0] = m_matrix[1][0];
	m3x3.m_matrix[1][1] = m_matrix[1][1];
	m3x3.m_matrix[1][2] = m_matrix[1][2];
	m3x3.m_matrix[2][0] = m_matrix[2][0];
	m3x3.m_matrix[2][1] = m_matrix[2][1];
	m3x3.m_matrix[2][2] = m_matrix[2][2];
}

Matrix4 Matrix4::transpose(void) const
{
	return Matrix4(
		m_matrix[0][0], m_matrix[1][0], m_matrix[2][0], m_matrix[3][0],
		m_matrix[0][1], m_matrix[1][1], m_matrix[2][1], m_matrix[3][1],
		m_matrix[0][2], m_matrix[1][2], m_matrix[2][2], m_matrix[3][2],
		m_matrix[0][3], m_matrix[1][3], m_matrix[2][3], m_matrix[3][3]
		);
}

Matrix4 Matrix4::adjoint(void) const
{
	return Matrix4(
		MINOR(*this, 1, 2, 3, 1, 2, 3),
		-MINOR(*this, 0, 2, 3, 1, 2, 3),
		MINOR(*this, 0, 1, 3, 1, 2, 3),
		-MINOR(*this, 0, 1, 2, 1, 2, 3),

		-MINOR(*this, 1, 2, 3, 0, 2, 3),
		MINOR(*this, 0, 2, 3, 0, 2, 3),
		-MINOR(*this, 0, 1, 3, 0, 2, 3),
		MINOR(*this, 0, 1, 2, 0, 2, 3),

		MINOR(*this, 1, 2, 3, 0, 1, 3),
		-MINOR(*this, 0, 2, 3, 0, 1, 3),
		MINOR(*this, 0, 1, 3, 0, 1, 3),
		-MINOR(*this, 0, 1, 2, 0, 1, 3),

		-MINOR(*this, 1, 2, 3, 0, 1, 2),
		MINOR(*this, 0, 2, 3, 0, 1, 2),
		-MINOR(*this, 0, 1, 3, 0, 1, 2),
		MINOR(*this, 0, 1, 2, 0, 1, 2)
		);
}

FXD::F32 Matrix4::determinant(void) const
{
	return
		m_matrix[0][0] * MINOR(*this, 1, 2, 3, 1, 2, 3) -
		m_matrix[0][1] * MINOR(*this, 1, 2, 3, 0, 2, 3) +
		m_matrix[0][2] * MINOR(*this, 1, 2, 3, 0, 1, 3) -
		m_matrix[0][3] * MINOR(*this, 1, 2, 3, 0, 1, 2);
}

FXD::F32 Matrix4::determinant3x3(void) const
{
	FXD::F32 fCofactor00 = ((m_matrix[1][1] * m_matrix[2][2]) - (m_matrix[1][2] * m_matrix[2][1]));
	FXD::F32 fCofactor10 = ((m_matrix[1][2] * m_matrix[2][0]) - (m_matrix[1][0] * m_matrix[2][2]));
	FXD::F32 fCofactor20 = ((m_matrix[1][0] * m_matrix[2][1]) - (m_matrix[1][1] * m_matrix[2][0]));

	FXD::F32 fDet = ((m_matrix[0][0] * fCofactor00) + (m_matrix[0][1] * fCofactor10) + (m_matrix[0][2] * fCofactor20));
	return fDet;
}

Matrix4 Matrix4::inverse(void) const
{
	FXD::F32 m00 = m_matrix[0][0], m01 = m_matrix[0][1], m02 = m_matrix[0][2], m03 = m_matrix[0][3];
	FXD::F32 m10 = m_matrix[1][0], m11 = m_matrix[1][1], m12 = m_matrix[1][2], m13 = m_matrix[1][3];
	FXD::F32 m20 = m_matrix[2][0], m21 = m_matrix[2][1], m22 = m_matrix[2][2], m23 = m_matrix[2][3];
	FXD::F32 m30 = m_matrix[3][0], m31 = m_matrix[3][1], m32 = m_matrix[3][2], m33 = m_matrix[3][3];

	FXD::F32 v0 = ((m20 * m31) - (m21 * m30));
	FXD::F32 v1 = ((m20 * m32) - (m22 * m30));
	FXD::F32 v2 = ((m20 * m33) - (m23 * m30));
	FXD::F32 v3 = ((m21 * m32) - (m22 * m31));
	FXD::F32 v4 = ((m21 * m33) - (m23 * m31));
	FXD::F32 v5 = ((m22 * m33) - (m23 * m32));

	FXD::F32 t00 = +(v5 * m11 - v4 * m12 + v3 * m13);
	FXD::F32 t10 = -(v5 * m10 - v2 * m12 + v1 * m13);
	FXD::F32 t20 = +(v4 * m10 - v2 * m11 + v0 * m13);
	FXD::F32 t30 = -(v3 * m10 - v1 * m11 + v0 * m12);

	FXD::F32 fInvDet = (1.0f / (t00 * m00 + t10 * m01 + t20 * m02 + t30 * m03));

	FXD::F32 d00 = (t00 * fInvDet);
	FXD::F32 d10 = (t10 * fInvDet);
	FXD::F32 d20 = (t20 * fInvDet);
	FXD::F32 d30 = (t30 * fInvDet);

	FXD::F32 d01 = -(v5 * m01 - v4 * m02 + v3 * m03) * fInvDet;
	FXD::F32 d11 = +(v5 * m00 - v2 * m02 + v1 * m03) * fInvDet;
	FXD::F32 d21 = -(v4 * m00 - v2 * m01 + v0 * m03) * fInvDet;
	FXD::F32 d31 = +(v3 * m00 - v1 * m01 + v0 * m02) * fInvDet;

	v0 = ((m10 * m31) - (m11 * m30));
	v1 = ((m10 * m32) - (m12 * m30));
	v2 = ((m10 * m33) - (m13 * m30));
	v3 = ((m11 * m32) - (m12 * m31));
	v4 = ((m11 * m33) - (m13 * m31));
	v5 = ((m12 * m33) - (m13 * m32));

	FXD::F32 d02 = +(v5 * m01 - v4 * m02 + v3 * m03) * fInvDet;
	FXD::F32 d12 = -(v5 * m00 - v2 * m02 + v1 * m03) * fInvDet;
	FXD::F32 d22 = +(v4 * m00 - v2 * m01 + v0 * m03) * fInvDet;
	FXD::F32 d32 = -(v3 * m00 - v1 * m01 + v0 * m02) * fInvDet;

	v0 = ((m21 * m10) - (m20 * m11));
	v1 = ((m22 * m10) - (m20 * m12));
	v2 = ((m23 * m10) - (m20 * m13));
	v3 = ((m22 * m11) - (m21 * m12));
	v4 = ((m23 * m11) - (m21 * m13));
	v5 = ((m23 * m12) - (m22 * m13));

	FXD::F32 d03 = -(v5 * m01 - v4 * m02 + v3 * m03) * fInvDet;
	FXD::F32 d13 = +(v5 * m00 - v2 * m02 + v1 * m03) * fInvDet;
	FXD::F32 d23 = -(v4 * m00 - v2 * m01 + v0 * m03) * fInvDet;
	FXD::F32 d33 = +(v3 * m00 - v1 * m01 + v0 * m02) * fInvDet;

	return Matrix4(
		d00, d01, d02, d03,
		d10, d11, d12, d13,
		d20, d21, d22, d23,
		d30, d31, d32, d33
		);
}

Matrix4 Matrix4::inverseAffine(void) const
{
	PRINT_COND_ASSERT(isAffine(), "Math:");

	FXD::F32 m10 = m_matrix[1][0];
	FXD::F32 m11 = m_matrix[1][1];
	FXD::F32 m12 = m_matrix[1][2];
	FXD::F32 m20 = m_matrix[2][0];
	FXD::F32 m21 = m_matrix[2][1];
	FXD::F32 m22 = m_matrix[2][2];

	FXD::F32 t00 = ((m22 * m11) - (m21 * m12));
	FXD::F32 t10 = ((m20 * m12) - (m22 * m10));
	FXD::F32 t20 = ((m21 * m10) - (m20 * m11));

	FXD::F32 m00 = m_matrix[0][0];
	FXD::F32 m01 = m_matrix[0][1];
	FXD::F32 m02 = m_matrix[0][2];

	FXD::F32 fInvDet = (1.0f / (m00 * t00 + m01 * t10 + m02 * t20));

	t00 *= fInvDet;
	t10 *= fInvDet;
	t20 *= fInvDet;
	m00 *= fInvDet;
	m01 *= fInvDet;
	m02 *= fInvDet;

	FXD::F32 r00 = t00;
	FXD::F32 r01 = ((m02 * m21) - (m01 * m22));
	FXD::F32 r02 = ((m01 * m12) - (m02 * m11));

	FXD::F32 r10 = t10;
	FXD::F32 r11 = ((m00 * m22) - (m02 * m20));
	FXD::F32 r12 = ((m02 * m10) - (m00 * m12));

	FXD::F32 r20 = t20;
	FXD::F32 r21 = ((m01 * m20) - (m00 * m21));
	FXD::F32 r22 = ((m00 * m11) - (m01 * m10));

	FXD::F32 m03 = m_matrix[0][3];
	FXD::F32 m13 = m_matrix[1][3];
	FXD::F32 m23 = m_matrix[2][3];

	FXD::F32 r03 = -((r00 * m03) + (r01 * m13) + (r02 * m23));
	FXD::F32 r13 = -((r10 * m03) + (r11 * m13) + (r12 * m23));
	FXD::F32 r23 = -((r20 * m03) + (r21 * m13) + (r22 * m23));

	return Matrix4(
		r00, r01, r02, r03,
		r10, r11, r12, r13,
		r20, r21, r22, r23,
		0.0f, 0.0f, 0.0f, 1.0f
		);
}

Matrix4 Matrix4::concatenateAffine(const Matrix4& rhs) const
{
	PRINT_COND_ASSERT(((isAffine()) && (rhs.isAffine())), "Math:");

	return Matrix4(
		m_matrix[0][0] * rhs.m_matrix[0][0] + m_matrix[0][1] * rhs.m_matrix[1][0] + m_matrix[0][2] * rhs.m_matrix[2][0],
		m_matrix[0][0] * rhs.m_matrix[0][1] + m_matrix[0][1] * rhs.m_matrix[1][1] + m_matrix[0][2] * rhs.m_matrix[2][1],
		m_matrix[0][0] * rhs.m_matrix[0][2] + m_matrix[0][1] * rhs.m_matrix[1][2] + m_matrix[0][2] * rhs.m_matrix[2][2],
		m_matrix[0][0] * rhs.m_matrix[0][3] + m_matrix[0][1] * rhs.m_matrix[1][3] + m_matrix[0][2] * rhs.m_matrix[2][3] + m_matrix[0][3],

		m_matrix[1][0] * rhs.m_matrix[0][0] + m_matrix[1][1] * rhs.m_matrix[1][0] + m_matrix[1][2] * rhs.m_matrix[2][0],
		m_matrix[1][0] * rhs.m_matrix[0][1] + m_matrix[1][1] * rhs.m_matrix[1][1] + m_matrix[1][2] * rhs.m_matrix[2][1],
		m_matrix[1][0] * rhs.m_matrix[0][2] + m_matrix[1][1] * rhs.m_matrix[1][2] + m_matrix[1][2] * rhs.m_matrix[2][2],
		m_matrix[1][0] * rhs.m_matrix[0][3] + m_matrix[1][1] * rhs.m_matrix[1][3] + m_matrix[1][2] * rhs.m_matrix[2][3] + m_matrix[1][3],

		m_matrix[2][0] * rhs.m_matrix[0][0] + m_matrix[2][1] * rhs.m_matrix[1][0] + m_matrix[2][2] * rhs.m_matrix[2][0],
		m_matrix[2][0] * rhs.m_matrix[0][1] + m_matrix[2][1] * rhs.m_matrix[1][1] + m_matrix[2][2] * rhs.m_matrix[2][1],
		m_matrix[2][0] * rhs.m_matrix[0][2] + m_matrix[2][1] * rhs.m_matrix[1][2] + m_matrix[2][2] * rhs.m_matrix[2][2],
		m_matrix[2][0] * rhs.m_matrix[0][3] + m_matrix[2][1] * rhs.m_matrix[1][3] + m_matrix[2][2] * rhs.m_matrix[2][3] + m_matrix[2][3],

		0.0f,
		0.0f,
		0.0f,
		1.0f);
}

void Matrix4::decomposition(Vector3F& position, Quaternion& rotation, Vector3F& scale) const
{
	Matrix3 m3x3;
	extract3x3Matrix(m3x3);

	Matrix3 matQ;
	Vector3F vecU;
	m3x3.QDUDecomposition(matQ, scale, vecU);

	rotation = Quaternion(matQ);
	position = Vector3F(m_matrix[0][3], m_matrix[1][3], m_matrix[2][3]);
}

void Matrix4::set_trs(const Vector3F& translation, const Quaternion& rotation, const Vector3F& scale)
{
	Matrix3 rot3x3;
	rotation.toRotationMatrix(rot3x3);

	m_matrix[0][0] = (scale.x() * rot3x3[0][0]);
	m_matrix[0][1] = (scale.y() * rot3x3[0][1]);
	m_matrix[0][2] = (scale.z() * rot3x3[0][2]);
	m_matrix[0][3] = translation.x();

	m_matrix[1][0] = (scale.x() * rot3x3[1][0]);
	m_matrix[1][1] = (scale.y() * rot3x3[1][1]);
	m_matrix[1][2] = (scale.z() * rot3x3[1][2]);
	m_matrix[1][3] = translation.y();

	m_matrix[2][0] = (scale.x() * rot3x3[2][0]);
	m_matrix[2][1] = (scale.y() * rot3x3[2][1]);
	m_matrix[2][2] = (scale.z() * rot3x3[2][2]);
	m_matrix[2][3] = translation.z();

	// No projection term
	m_matrix[3][0] = 0.0f;
	m_matrix[3][1] = 0.0f;
	m_matrix[3][2] = 0.0f;
	m_matrix[3][3] = 1.0f;
}

void Matrix4::setInverseTRS(const Vector3F& translation, const Quaternion& rotation, const Vector3F& scale)
{
	// Invert the parameters
	Vector3F invTranslate = -translation;
	Vector3F invScale(1 / scale.x(), 1 / scale.y(), 1 / scale.z());
	Quaternion invRot = rotation.inverse();

	// Because we're inverting, order is translation, rotation, scale
	// So make translation relative to scale & rotation
	invTranslate = invRot.rotate(invTranslate);
	invTranslate *= invScale;

	// Next, make a 3x3 rotation matrix
	Matrix3 rot3x3;
	invRot.toRotationMatrix(rot3x3);

	// Set up final matrix with scale, rotation and translation
	m_matrix[0][0] = (invScale.x() * rot3x3[0][0]);
	m_matrix[0][1] = (invScale.x() * rot3x3[0][1]);
	m_matrix[0][2] = (invScale.x() * rot3x3[0][2]);
	m_matrix[0][3] = invTranslate.x();
	m_matrix[1][0] = (invScale.y() * rot3x3[1][0]);
	m_matrix[1][1] = (invScale.y() * rot3x3[1][1]);
	m_matrix[1][2] = (invScale.y() * rot3x3[1][2]);
	m_matrix[1][3] = invTranslate.y();
	m_matrix[2][0] = (invScale.z() * rot3x3[2][0]);
	m_matrix[2][1] = (invScale.z() * rot3x3[2][1]);
	m_matrix[2][2] = (invScale.z() * rot3x3[2][2]);
	m_matrix[2][3] = invTranslate.z();

	// No projection term
	m_matrix[3][0] = 0.0f;
	m_matrix[3][1] = 0.0f;
	m_matrix[3][2] = 0.0f;
	m_matrix[3][3] = 1.0f;
}

Vector3F Matrix4::multiply(const Vector3F& v) const
{
	Vector3F r;
	FXD::F32 fInvW = (1.0f / (m_matrix[3][0] * v.x() + m_matrix[3][1] * v.y() + m_matrix[3][2] * v.z() + m_matrix[3][3]));

	return Vector3F(
		((m_matrix[0][0] * v.x() + m_matrix[0][1] * v.y() + m_matrix[0][2] * v.z() + m_matrix[0][3]) * fInvW),
		((m_matrix[1][0] * v.x() + m_matrix[1][1] * v.y() + m_matrix[1][2] * v.z() + m_matrix[1][3]) * fInvW),
		((m_matrix[2][0] * v.x() + m_matrix[2][1] * v.y() + m_matrix[2][2] * v.z() + m_matrix[2][3]) * fInvW)
		);
}

Vector4F Matrix4::multiply(const Vector4F& v) const
{
	return Vector4F(
		(m_matrix[0][0] * v.x() + m_matrix[0][1] * v.y() + m_matrix[0][2] * v.z() + m_matrix[0][3] * v.w()),
		(m_matrix[1][0] * v.x() + m_matrix[1][1] * v.y() + m_matrix[1][2] * v.z() + m_matrix[1][3] * v.w()),
		(m_matrix[2][0] * v.x() + m_matrix[2][1] * v.y() + m_matrix[2][2] * v.z() + m_matrix[2][3] * v.w()),
		(m_matrix[3][0] * v.x() + m_matrix[3][1] * v.y() + m_matrix[3][2] * v.z() + m_matrix[3][3] * v.w())
		);
}

Vector3F Matrix4::multiplyAffine(const Vector3F& v) const
{
	return Vector3F(
		(m_matrix[0][0] * v.x() + m_matrix[0][1] * v.y() + m_matrix[0][2] * v.z() + m_matrix[0][3]),
		(m_matrix[1][0] * v.x() + m_matrix[1][1] * v.y() + m_matrix[1][2] * v.z() + m_matrix[1][3]),
		(m_matrix[2][0] * v.x() + m_matrix[2][1] * v.y() + m_matrix[2][2] * v.z() + m_matrix[2][3])
		);
}

Vector4F Matrix4::multiplyAffine(const Vector4F& v) const
{
	return Vector4F(
		(m_matrix[0][0] * v.x() + m_matrix[0][1] * v.y() + m_matrix[0][2] * v.z() + m_matrix[0][3] * v.w()),
		(m_matrix[1][0] * v.x() + m_matrix[1][1] * v.y() + m_matrix[1][2] * v.z() + m_matrix[1][3] * v.w()),
		(m_matrix[2][0] * v.x() + m_matrix[2][1] * v.y() + m_matrix[2][2] * v.z() + m_matrix[2][3] * v.w()),
		v.w()
		);
}

Vector3F Matrix4::multiplyDirection(const Vector3F& v) const
{
	return Vector3F(
		(m_matrix[0][0] * v.x() + m_matrix[0][1] * v.y() + m_matrix[0][2] * v.z()),
		(m_matrix[1][0] * v.x() + m_matrix[1][1] * v.y() + m_matrix[1][2] * v.z()),
		(m_matrix[2][0] * v.x() + m_matrix[2][1] * v.y() + m_matrix[2][2] * v.z())
		);
}

Plane Matrix4::multiplyAffine(const Plane& p) const
{
	Vector4F localNormal(p.get_normal().x(), p.get_normal().y(), p.get_normal().z(), 0.0f);
	Vector4F localPoint = localNormal * p.get_distance();
	localPoint.w(1.0f);

	Matrix4 itMat = inverse().transpose();
	Vector4F worldNormal = itMat.multiplyAffine(localNormal);
	Vector4F worldPoint = multiplyAffine(localPoint);

	FXD::F32 d = worldNormal.dot(worldPoint);
	return Plane(worldNormal.x(), worldNormal.y(), worldNormal.z(), d);
}

void Matrix4::makeView(const Vector3F& position, const Quaternion& orientation, const Matrix4* reflectMatrix)
{
	// View matrix is:
	// [Lx Uy Dz Tx]
	// [Lx Uy Dz Ty]
	// [Lx Uy Dz Tz]
	// [0  0  0  1 ]
	// Where T = -(Transposed(Rot) * Pos)

	// This is most efficiently done using 3x3 Matrices
	Matrix3 rot;
	orientation.toRotationMatrix(rot);

	// Make the translation relative to new axes
	Matrix3 rotT = rot.transpose();
	Vector3F trans = (-rotT).transform(position);

	// Make final matrix
	(*this) = Matrix4(rotT);
	m_matrix[0][3] = trans.x();
	m_matrix[1][3] = trans.y();
	m_matrix[2][3] = trans.z();

	// Deal with reflections
	if (reflectMatrix)
	{
		(*this) = (*this) * (*reflectMatrix);
	}
}

void Matrix4::makeProjectionOrtho(FXD::F32 left, FXD::F32 right, FXD::F32 top, FXD::F32 bottom, FXD::F32 fNear, FXD::F32 fFar)
{
	// Create a matrix that transforms coordinate to normalized device coordinate in range:
	// Left -1 - Right 1
	// Bottom -1 - Top 1
	// Near -1 - Far 1

	FXD::F32 deltaX = (right - left);
	FXD::F32 deltaY = (bottom - top);
	FXD::F32 deltaZ = (fFar - fNear);

	m_matrix[0][0] = (2.0F / deltaX);
	m_matrix[0][1] = 0.0f;
	m_matrix[0][2] = 0.0f;
	m_matrix[0][3] = -(right + left) / deltaX;

	m_matrix[1][0] = 0.0f;
	m_matrix[1][1] = (-2.0F / deltaY);
	m_matrix[1][2] = 0.0f;
	m_matrix[1][3] = (top + bottom) / deltaY;

	m_matrix[2][0] = 0.0f;
	m_matrix[2][1] = 0.0f;
	m_matrix[2][2] = (-2.0F / deltaZ);
	m_matrix[2][3] = -(fFar + fNear) / deltaZ;

	m_matrix[3][0] = 0.0f;
	m_matrix[3][1] = 0.0f;
	m_matrix[3][2] = 0.0f;
	m_matrix[3][3] = 1.0f;
}

Matrix4 Matrix4::translation(const Vector3F& translation)
{
	Matrix4 mat;
	mat[0][0] = 1.0f;
	mat[0][1] = 0.0f;
	mat[0][2] = 0.0f;
	mat[0][3] = translation.x();
	mat[1][0] = 0.0f;
	mat[1][1] = 1.0f;
	mat[1][2] = 0.0f;
	mat[1][3] = translation.y();
	mat[2][0] = 0.0f;
	mat[2][1] = 0.0f;
	mat[2][2] = 1.0f;
	mat[2][3] = translation.z();
	mat[3][0] = 0.0f;
	mat[3][1] = 0.0f;
	mat[3][2] = 0.0f;
	mat[3][3] = 1.0f;
	return mat;
}

Matrix4 Matrix4::scaling(const Vector3F& scale)
{
	Matrix4 mat;
	mat[0][0] = scale.x();
	mat[0][1] = 0.0f;
	mat[0][2] = 0.0f;
	mat[0][3] = 0.0f;
	mat[1][0] = 0.0f;
	mat[1][1] = scale.y();
	mat[1][2] = 0.0f;
	mat[1][3] = 0.0f;
	mat[2][0] = 0.0f;
	mat[2][1] = 0.0f;
	mat[2][2] = scale.z();
	mat[2][3] = 0.0f;
	mat[3][0] = 0.0f;
	mat[3][1] = 0.0f;
	mat[3][2] = 0.0f;
	mat[3][3] = 1.0f;
	return mat;
}

Matrix4 Matrix4::scaling(FXD::F32 scale)
{
	Matrix4 mat;
	mat[0][0] = scale;
	mat[0][1] = 0.0f;
	mat[0][2] = 0.0f;
	mat[0][3] = 0.0f;
	mat[1][0] = 0.0f;
	mat[1][1] = scale;
	mat[1][2] = 0.0f;
	mat[1][3] = 0.0f;
	mat[2][0] = 0.0f;
	mat[2][1] = 0.0f;
	mat[2][2] = scale;
	mat[2][3] = 0.0f;
	mat[3][0] = 0.0f;
	mat[3][1] = 0.0f;
	mat[3][2] = 0.0f;
	mat[3][3] = 1.0f;
	return mat;
}

Matrix4 Matrix4::rotation(const Quaternion& rotation)
{
	Matrix3 mat;
	rotation.toRotationMatrix(mat);
	return Matrix4(mat);
}

Matrix4 Matrix4::TRS(const Vector3F& translation, const Quaternion& rotation, const Vector3F& scale)
{
	Matrix4 mat;
	mat.set_trs(translation, rotation, scale);
	return mat;
}

Matrix4 Matrix4::inverseTRS(const Vector3F& translation, const Quaternion& rotation, const Vector3F& scale)
{
	Matrix4 mat;
	mat.setInverseTRS(translation, rotation, scale);
	return mat;
}
*/