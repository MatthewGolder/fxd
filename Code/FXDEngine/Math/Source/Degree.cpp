// Creator - MatthewGolder
#include "FXDEngine/Math/Degree.h"
#include "FXDEngine/Math/Constants.h"
#include "FXDEngine/Math/Math.h"
#include "FXDEngine/Math/Radian.h"

using namespace FXD;
using namespace Math;

// ------
// Degree
// -
// ------
Degree::Degree(FXD::F32 fDeg)
	: m_fDeg(fDeg)
{
}

Degree::Degree(const Degree& rhs)
	: m_fDeg(rhs.m_fDeg)
{
}

Degree::Degree(const Radian& rhs)
	: m_fDeg(rhs.to_degrees())
{
}

Degree::~Degree(void)
{
}

Degree& Degree::operator=(const FXD::F32& fDeg)
{
	m_fDeg = fDeg;
	return (*this);
}

Degree& Degree::operator=(const Degree& deg)
{
	m_fDeg = deg.m_fDeg;
	return (*this);
}

Degree& Degree::operator=(const Radian& rad)
{
	m_fDeg = rad.to_degrees();
	return (*this);
}

const Degree& Degree::operator+ () const				{ return (*this); }
Degree Degree::operator+(FXD::F32 fDeg) const			{ return Degree(m_fDeg + fDeg); }
Degree Degree::operator+(const Degree& deg) const	{ return Degree(m_fDeg + deg.m_fDeg); }
Degree Degree::operator+(const Radian& rad) const	{ return Degree(m_fDeg + rad.to_degrees()); }

Degree Degree::operator-() const							{ return Degree(-m_fDeg); }
Degree Degree::operator-(FXD::F32 fDeg) const			{ return Degree(m_fDeg - fDeg); }
Degree Degree::operator-(const Degree& deg) const	{ return Degree(m_fDeg - deg.m_fDeg); }
Degree Degree::operator-(const Radian& rad) const	{ return Degree(m_fDeg - rad.to_degrees()); }

Degree Degree::operator*(FXD::F32 fDeg) const			{ return Degree(m_fDeg * fDeg); }
Degree Degree::operator*(const Degree& deg) const	{ return Degree(m_fDeg * deg.m_fDeg); }
Degree Degree::operator*(const Radian& rad) const	{ return Degree(m_fDeg + rad.to_degrees()); }

Degree Degree::operator/(FXD::F32 fDeg) const			{ return Degree(m_fDeg / fDeg); }
Degree Degree::operator/(const Degree& deg) const	{ return Degree(m_fDeg / deg.m_fDeg); }
Degree Degree::operator/(const Radian& rad) const	{ return Degree(m_fDeg / rad.to_degrees()); }

Degree& Degree::operator+=(FXD::F32 fDeg)			{ m_fDeg += fDeg; return (*this); }
Degree& Degree::operator+=(const Degree& deg)	{ m_fDeg += deg.m_fDeg; return (*this); }
Degree& Degree::operator+=(const Radian& rad)	{ m_fDeg += rad.to_degrees(); return (*this); }

Degree& Degree::operator-=(FXD::F32 fDeg)			{ m_fDeg -= fDeg; return (*this); }
Degree& Degree::operator-=(const Degree& deg)	{ m_fDeg -= deg.m_fDeg; return (*this); }
Degree& Degree::operator-=(const Radian& rad)	{ m_fDeg -= rad.to_degrees(); return (*this); }

Degree& Degree::operator*=(FXD::F32 fDeg)			{ m_fDeg *= fDeg; return (*this); }
Degree& Degree::operator*=(const Degree& deg)	{ m_fDeg *= deg.m_fDeg; return (*this); }
Degree& Degree::operator*=(const Radian& rad)	{ m_fDeg *= rad.to_degrees(); return (*this); }

Degree& Degree::operator/=(FXD::F32 fDeg)			{ m_fDeg /= fDeg; return (*this); }
Degree& Degree::operator/=(const Degree& deg)	{ m_fDeg /= deg.m_fDeg; return (*this); }
Degree& Degree::operator/=(const Radian& rad)	{ m_fDeg /= rad.to_degrees(); return (*this); }

Math::Degree::operator FXD::F32(void) const
{
	return m_fDeg;
}

FXD::F32 Degree::to_degrees(void) const
{
	return m_fDeg;
}

FXD::F32 Degree::to_radians(void) const
{
	return (m_fDeg * Math::DEG2RAD);
}

Degree Degree::wrap(void)
{
	m_fDeg = Math::FMod(m_fDeg, 360.0f);

	if (m_fDeg < 0)
		m_fDeg += 360.0f;
	return (*this);
}