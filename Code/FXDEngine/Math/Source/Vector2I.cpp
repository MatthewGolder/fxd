// Creator - MatthewGolder
#include "FXDEngine/Math/Vector2I.h"
#include "FXDEngine/Math/Math.h"
#include "FXDEngine/Core/Debugger.h"

using namespace FXD;
using namespace Math;

// ------
// Vector2I
// -
// ------
const Vector2I Vector2I::kZero(0);
const Vector2I Vector2I::kOne(1);
const Vector2I Vector2I::kLeft(-1, 0);
const Vector2I Vector2I::kRight(1, 0);
const Vector2I Vector2I::kUp(0, 1);
const Vector2I Vector2I::kDown(0, -1);

Vector2I::Vector2I(void)
{
	Vec2ISet(m_data, 0);
}
Vector2I::Vector2I(FXD::S32 val)
{
	Vec2ISet(m_data, val);
}
Vector2I::Vector2I(FXD::S32 x, FXD::S32 y)
{
	Vec2ISet(m_data, x, y);
}
Vector2I::~Vector2I(void)
{
}

FXD::S32 Vector2I::operator[](FXD::U32 nID) const
{
	PRINT_COND_ASSERT((nID < 2), "Math: Index out of bounds");
	return Vec2IGetIndex(m_data, nID);
}
FXD::F32 Vector2I::operator()(FXD::U32 nID) const
{
	PRINT_COND_ASSERT((nID < 2), "Math: Index out of bounds");
	return Vec2IGetIndex(m_data, nID);
}

Vector2I& Vector2I::operator=(const Vector2I& rhs)
{
	Vec2ICopy(m_data, rhs.m_data);
	return (*this);
}
Vector2I& Vector2I::operator=(FXD::S32 rhs)
{
	Vec2ISet(m_data, rhs);
	return (*this);
}

Vector2I Vector2I::operator-() const
{
	Vector2I out = (*this);
	out.negate();
	return out;
}

FXD::S32 Vector2I::x(void) const
{
	return Vec2IGetX(m_data);
}
FXD::S32 Vector2I::y(void) const
{
	return Vec2IGetY(m_data);
}
FXD::S32 Vector2I::index(FXD::U32 nID)
{
	PRINT_COND_ASSERT((nID < 2), "Math: Index out of bounds");
	return Vec2IGetIndex(m_data, nID);
}

void Vector2I::set(FXD::S32 x, FXD::S32 y)
{
	Vec2ISet(m_data, x, y);
}
void Vector2I::x(FXD::S32 x)
{
	Vec2ISetX(m_data, x);
}
void Vector2I::y(FXD::S32 y)
{
	Vec2ISetY(m_data, y);
}
void Vector2I::index(FXD::U32 nID, FXD::S32 nVal)
{
	PRINT_COND_ASSERT((nID < 2), "Math: Index out of bounds");
	Vec2ISetIndex(m_data, nID, nVal);
}

Vector2I Vector2I::midpoint(const Vector2I& rhs) const
{
	Vector2I retVec;
	Vec2IMidPoint(retVec.m_data, m_data, rhs.m_data);
	return retVec;
}

void Vector2I::negate(void)
{
	Vec2INegate(m_data);
}
void Vector2I::minimum(const Vector2I& min)
{
	Vec2IMinimum(m_data, min.m_data);
}
void Vector2I::maximum(const Vector2I& max)
{
	Vec2IMaximum(m_data, max.m_data);
}
void Vector2I::clamp(const Vector2I& min, const Vector2I& max)
{
	Vec2IClamp(m_data, min.m_data, max.m_data);
}
void Vector2I::abs(void)
{
	Vec2IAbs(m_data);
}

Vector2I Vector2I::get_negate(void) const
{
	Vector2I retVec = (*this);
	retVec.negate();
	return retVec;
}
Vector2I Vector2I::get_minimum(const Vector2I& min) const
{
	Vector2I retVec = (*this);
	retVec.minimum(min);
	return retVec;
}
Vector2I Vector2I::get_maximum(const Vector2I& max) const
{
	Vector2I retVec = (*this);
	retVec.maximum(max);
	return retVec;
}
Vector2I Vector2I::get_clamp(const Vector2I& min, const Vector2I& max) const
{
	Vector2I retVec = (*this);
	retVec.clamp(min, max);
	return retVec;
}
Vector2I Vector2I::get_abs(void) const
{
	Vector2I retVec = (*this);
	retVec.abs();
	return retVec;
}

// STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS 
Vector2I Vector2I::midpoint(const Vector2I& lhs, const Vector2I& rhs)
{
	return lhs.midpoint(rhs);
}

void Vector2I::set_negate(Vector2I& vec)
{
	vec.negate();
}
void Vector2I::set_minimum(Vector2I& vec, const Vector2I& min)
{
	vec.minimum(min);
}
void Vector2I::set_maximum(Vector2I& vec, const Vector2I& max)
{
	vec.maximum(max);
}
void Vector2I::set_clamp(Vector2I& vec, const Vector2I& min, const Vector2I& max)
{
	vec.clamp(min, max);
}
void Vector2I::set_abs(Vector2I& vec)
{
	vec.abs();
}

Vector2I Vector2I::get_negate(const Vector2I& vec)
{
	Vector2I retVec = vec;
	Vector2I::set_negate(retVec);
	return retVec;
}
Vector2I Vector2I::get_minimum(const Vector2I& vec, const Vector2I& min)
{
	Vector2I retVec = vec;
	Vector2I::set_minimum(retVec, min);
	return retVec;
}
Vector2I Vector2I::get_maximum(const Vector2I& vec, const Vector2I& max)
{
	Vector2I retVec = vec;
	Vector2I::set_maximum(retVec, max);
	return retVec;
}
Vector2I Vector2I::get_clamp(const Vector2I& vec, const Vector2I& min, const Vector2I& max)
{
	Vector2I retVec = vec;
	Vector2I::set_clamp(retVec, min, max);
	return retVec;
}
Vector2I Vector2I::get_abs(const Vector2I& vec)
{
	Vector2I retVec = vec;
	Vector2I::set_abs(retVec);
	return retVec;
}