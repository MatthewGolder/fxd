// Creator - MatthewGolder
#include "FXDEngine/Math/Radian.h"
#include "FXDEngine/Math/Constants.h"
#include "FXDEngine/Math/Degree.h"
#include "FXDEngine/Math/Math.h"

using namespace FXD;
using namespace Math;

// ------
// Radian
// -
// ------
Radian::Radian(FXD::F32 fRad)
	: m_fRad(fRad)
{
}

Radian::Radian(const Degree& deg)
	: m_fRad(deg.to_radians())
{
}

Radian::~Radian(void)
{
}

Radian& Radian::operator=(FXD::F32 rhs)
{
	m_fRad = rhs;
	return (*this);
}

Radian& Radian::operator=(const Radian& rhs)
{
	m_fRad = rhs.m_fRad;
	return (*this);
}

Radian& Radian::operator=(const Degree& rhs)
{
	m_fRad = rhs.to_radians();
	return (*this);
}

const Radian& Radian::operator+() const					{ return (*this); }
Radian Radian::operator+(FXD::F32 fRad) const			{ return Radian(m_fRad + fRad); }
Radian Radian::operator+(const Radian& rad) const	{ return Radian(m_fRad + rad.m_fRad); }
Radian Radian::operator+(const Degree& deg) const	{ return Radian(m_fRad + deg.to_radians()); }

Radian Radian::operator-() const							{ return Radian(-m_fRad); }
Radian Radian::operator-(FXD::F32 fRad) const			{ return Radian(m_fRad - fRad); }
Radian Radian::operator-(const Radian& rad) const	{ return Radian(m_fRad - rad.m_fRad); }
Radian Radian::operator-(const Degree& deg) const	{ return Radian(m_fRad - deg.to_radians()); }

Radian Radian::operator*(FXD::F32 fRad) const			{ return Radian(m_fRad * fRad); }
Radian Radian::operator*(const Radian& rad) const	{ return Radian(m_fRad * rad.m_fRad); }
Radian Radian::operator*(const Degree& deg) const	{ return Radian(m_fRad * deg.to_radians()); }

Radian Radian::operator/(FXD::F32 fRad) const			{ return Radian(m_fRad / fRad); }
Radian Radian::operator/(const Radian& rad) const	{ return Radian(m_fRad / rad.m_fRad); }
Radian Radian::operator/(const Degree& deg) const	{ return Radian(m_fRad / deg.to_radians()); }

Radian& Radian::operator+=(FXD::F32 fRad)			{ m_fRad += fRad; return (*this); }
Radian& Radian::operator+=(const Radian& rad)	{ m_fRad += rad.m_fRad; return (*this); }
Radian& Radian::operator+=(const Degree& deg)	{ m_fRad += deg.to_radians(); return (*this); }

Radian& Radian::operator-=(FXD::F32 fRad)			{ m_fRad -= fRad; return (*this); }
Radian& Radian::operator-=(const Radian& rad)	{ m_fRad -= rad.m_fRad; return (*this); }
Radian& Radian::operator-=(const Degree& deg)	{ m_fRad -= deg.to_radians(); return (*this); }

Radian& Radian::operator*=(FXD::F32 fRad)			{ m_fRad *= fRad; return (*this); }
Radian& Radian::operator*=(const Radian& rad)	{ m_fRad *= rad.m_fRad; return (*this); }
Radian& Radian::operator*=(const Degree& deg)	{ m_fRad *= deg.to_radians(); return (*this); }

Radian& Radian::operator/=(FXD::F32 fRad)			{ m_fRad /= fRad; return (*this); }
Radian& Radian::operator/=(const Radian& rad)	{ m_fRad /= rad.m_fRad; return (*this); }
Radian& Radian::operator/=(const Degree& deg)	{ m_fRad /= deg.to_radians(); return (*this); }

Math::Radian::operator FXD::F32(void) const
{
	return m_fRad;
}

FXD::F32 Radian::to_degrees(void) const
{
	return (m_fRad * Math::RAD2DEG);
}

FXD::F32 Radian::to_radians(void) const
{
	return m_fRad;
}

Radian Radian::wrap(void)
{
	m_fRad = Math::FMod(m_fRad, Math::PI_TWO);

	if (m_fRad < 0.0f)
		m_fRad += Math::PI_TWO;
	return (*this);
}