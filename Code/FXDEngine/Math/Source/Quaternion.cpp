// Creator - MatthewGolder
#include "FXDEngine/Math/Quaternion.h"
#include "FXDEngine/Math/Degree.h"
#include "FXDEngine/Math/Math.h"
#include "FXDEngine/Math/Matrix3.h"
#include "FXDEngine/Math/Radian.h"
#include "FXDEngine/Math/Vector3F.h"
#include "FXDEngine/Core/Debugger.h"

using namespace FXD;
using namespace Math;

// ------
// QuaternionF
// -
// ------
const QuaternionF QuaternionF::kIdentity(0.0f, 0.0f, 0.0f, 1.0f);

QuaternionF::QuaternionF(void)
{
	Vec4FCopy(m_data, QuaternionF::kIdentity.m_data);
}

QuaternionF::QuaternionF(FXD::F32 x, FXD::F32 y, FXD::F32 z, FXD::F32 w)
{
	Vec4FSet(m_data, x, y, z, w);
}

QuaternionF::QuaternionF(const Math::Radian& rPitch, const Math::Radian& rYaw, const Math::Radian& rRoll, Math::EulerAngleOrder eOrder)
{
	from_euler_angles(rPitch, rYaw, rRoll, eOrder);
}

QuaternionF::QuaternionF(const Math::Degree& dPitch, const Math::Degree& dYaw, const Math::Degree& dRoll, Math::EulerAngleOrder eOrder)
{
	from_euler_angles(Radian(dPitch), Radian(dYaw), Radian(dRoll), eOrder);
}

QuaternionF::QuaternionF(const Matrix3F& rot)
{
}

QuaternionF::~QuaternionF(void)
{
}

bool QuaternionF::operator==(const QuaternionF& rhs) const
{
	return Vec4FCompare(m_data, rhs.m_data);
}
bool QuaternionF::operator!=(const QuaternionF& rhs) const
{
	return !Vec4FCompare(m_data, rhs.m_data);
}

FXD::F32 QuaternionF::operator[](FXD::U32 nID) const
{
	PRINT_COND_ASSERT((nID < 4), "Math: Index out of bounds");
	return Vec4FGetIndex(m_data, nID);
}

FXD::F32 QuaternionF::operator()(FXD::U32 nID) const
{
	PRINT_COND_ASSERT((nID < 4), "Math: Index out of bounds");
	return Vec4FGetIndex(m_data, nID);
}

QuaternionF QuaternionF::operator*(const QuaternionF& rhs) const
{
	QuaternionF out;
	QuatFMul(out.m_data, m_data, rhs.m_data);
	return out;
}

//QuaternionF QuaternionF::operator*(FXD::F32 rhs) const
//{
//	QuaternionF out;
//	QuatFMul(out.m_data, m_data, rhs);
//	return out;
//}

void QuaternionF::from_euler_angles(const Math::Radian& rPitch, const Math::Radian& rYaw, const Math::Radian& rRoll, Math::EulerAngleOrder eOrder)
{
	QuatFRotationPitchYawRollF(m_data, rPitch, rYaw, rRoll, eOrder);
}

void QuaternionF::set_euler_angles(QuaternionF& dst, const Math::Radian& rPitch, const Math::Radian& rYaw, const Math::Radian& rRoll, Math::EulerAngleOrder eOrder)
{
	dst.from_euler_angles(rPitch, rYaw, rRoll, eOrder);
}

QuaternionF QuaternionF::create_euler_angles(const Math::Radian& rPitch, const Math::Radian& rYaw, const Math::Radian& rRoll, Math::EulerAngleOrder eOrder)
{
	QuaternionF retQuat;
	QuaternionF::set_euler_angles(retQuat, rPitch, rYaw, rRoll, eOrder);
	return retQuat;
}

/*
// ------
// Quaternion
// -
// ------
const FXD::F32 Quaternion::kEpsilon = 1e-03f;
const Quaternion Quaternion::kZero(0.0f, 0.0f, 0.0f, 0.0f);
const Quaternion Quaternion::kIdentity(1.0f, 0.0f, 0.0f, 0.0f);
const Quaternion::EulerAngleOrderData Quaternion::kEALookup[6] =
{ { 0, 1, 2 }, { 0, 2, 1 }, { 1, 0, 2 },
{ 1, 2, 0 }, { 2, 0, 1 }, { 2, 1, 0 } };;


void Quaternion::swap(Quaternion& rhs)
{
	FXD::STD::swap(m_w, rhs.m_w);
	FXD::STD::swap(m_x, rhs.m_x);
	FXD::STD::swap(m_y, rhs.m_y);
	FXD::STD::swap(m_z, rhs.m_z);
}

FXD::F32 Quaternion::dot(const Quaternion& other) const
{
	return (m_w * other.w()) + (m_x * other.x()) + (m_y * other.y()) + (m_z * other.z());
}

FXD::F32 Quaternion::normalise(void)
{
	FXD::F32 fLen = (m_w * m_w) + (m_x * m_x) + (m_y * m_y) + (m_z * m_z);
	FXD::F32 fFactor = (1.0f / Math::Sqrt(fLen));
	(*this) = ((*this) * fFactor);
	return fLen;
}

Quaternion Quaternion::inverse(void) const
{
	FXD::F32 fNorm = (m_w * m_w) + (m_x * m_x) + (m_y * m_y) + (m_z * m_z);
	if (fNorm > 0.0f)
	{
		FXD::F32 fInvNorm = (1.0f / fNorm);
		return Quaternion((m_w * fInvNorm), (-m_x * fInvNorm), (-m_y * fInvNorm), (-m_z * fInvNorm));
	}
	else
	{
		// Return an invalid result to flag the error
		return Quaternion::kZero;
	}
}

Vector3F Quaternion::rotate(const Vector3F& v) const
{
	// Note: Does compiler generate fast code here? Perhaps its better to pull all code locally without constructing an intermediate matrix.
	Matrix3 rot;
	toRotationMatrix(rot);
	return rot.transform(v);
	return Vector3F();
}


void Quaternion::fromRotationMatrix(const Matrix3F& mat)
{
	// Algorithm in Ken Shoemake's article in 1987 SIGGRAPH course notes
	// article "Quaternion Calculus and Fast Animation".

	FXD::F32 fTrace = (mat[0][0] + mat[1][1] + mat[2][2]);
	FXD::F32 fRoot = 0.0f;

	if (fTrace > 0.0f)
	{
		// |w| > 1/2, may as well choose w > 1/2
		fRoot = Math::Sqrt(fTrace + 1.0f); // 2w
		m_w = (0.5f * fRoot);
		fRoot = (0.5f / fRoot); // 1/(4w)
		m_x = (mat[2][1] - mat[1][2]) * fRoot;
		m_y = (mat[0][2] - mat[2][0]) * fRoot;
		m_z = (mat[1][0] - mat[0][1]) * fRoot;
	}
	else
	{
		// |w| <= 1/2
		static FXD::U32 nextLookup[3] = { 1, 2, 0 };
		FXD::U32 nI = 0;
		if (mat[1][1] > mat[0][0])
		{
			nI = 1;
		}

		if (mat[2][2] > mat[nI][nI])
		{
			nI = 2;
		}

		FXD::U32 nJ = nextLookup[nI];
		FXD::U32 nK = nextLookup[nJ];

		fRoot = Math::Sqrt(mat[nI][nI] - mat[nJ][nJ] - mat[nK][nK] + 1.0f);

		FXD::F32* fCmpntLookup[3] = { &m_x, &m_y, &m_z };
		*fCmpntLookup[nI] = (0.5f * fRoot);
		fRoot = (0.5f / fRoot);

		m_w = (mat[nK][nJ] - mat[nJ][nK]) * fRoot;
		*fCmpntLookup[nJ] = (mat[nJ][nI] + mat[nI][nJ]) * fRoot;
		*fCmpntLookup[nK] = (mat[nK][nI] + mat[nI][nK]) * fRoot;
	}
	normalise();
}

void Quaternion::fromAxisAngle(const Vector3F& axis, const Degree& angle)
{
	fromAxisAngle(axis, Radian(angle));
}

void Quaternion::fromAxisAngle(const Vector3F& axis, const Radian& angle)
{
	Radian halfAngle(0.5f * angle);
	FXD::F32 fSin = Math::FuncsMath::SinRad(halfAngle);

	m_w = Math::FuncsMath::CosRad(halfAngle);
	m_x = (fSin * axis.x());
	m_y = (fSin * axis.y());
	m_z = (fSin * axis.z());
}

void Quaternion::fromAxes(const Vector3F& xaxis, const Vector3F& yaxis, const Vector3F& zaxis)
{
	Matrix3 kRot;

	kRot[0][0] = xaxis.x();
	kRot[1][0] = xaxis.y();
	kRot[2][0] = xaxis.z();

	kRot[0][1] = yaxis.x();
	kRot[1][1] = yaxis.y();
	kRot[2][1] = yaxis.z();

	kRot[0][2] = zaxis.x();
	kRot[1][2] = zaxis.y();
	kRot[2][2] = zaxis.z();

	fromRotationMatrix(kRot);
}

void Quaternion::fromEulerAngles(const Degree& xAngle, const Degree& yAngle, const Degree& zAngle)
{
	fromEulerAngles(Radian(xAngle), Radian(yAngle), Radian(zAngle));
}

void Quaternion::fromEulerAngles(const Radian& xAngle, const Radian& yAngle, const Radian& zAngle)
{
	Radian halfXAngle = (xAngle * 0.5f);
	Radian halfYAngle = (yAngle * 0.5f);
	Radian halfZAngle = (zAngle * 0.5f);

	FXD::F32 fCX = Math::FuncsMath::CosRad(halfXAngle);
	FXD::F32 fSX = Math::FuncsMath::SinRad(halfXAngle);

	FXD::F32 fCY = Math::FuncsMath::CosRad(halfYAngle);
	FXD::F32 fSY = Math::FuncsMath::SinRad(halfYAngle);

	FXD::F32 fcZ = Math::FuncsMath::CosRad(halfZAngle);
	FXD::F32 fSZ = Math::FuncsMath::SinRad(halfZAngle);

	Quaternion quatX(fCX, fSX, 0.0f, 0.0f);
	Quaternion quatY(fCY, 0.0f, fSY, 0.0f);
	Quaternion quatZ(fcZ, 0.0f, 0.0f, fSZ);

	(*this) = (quatY * quatX * quatZ);
}

void Quaternion::toRotationMatrix(Matrix3F& mat) const
{
	FXD::F32 fTX = (m_x + m_x);
	FXD::F32 fTY = (m_y + m_y);
	FXD::F32 fTZ = (m_z + m_z);
	FXD::F32 twx = (fTX * m_w);
	FXD::F32 twy = (fTY * m_w);
	FXD::F32 twz = (fTZ * m_w);
	FXD::F32 txx = (fTX * m_x);
	FXD::F32 txy = (fTY * m_x);
	FXD::F32 txz = (fTZ * m_x);
	FXD::F32 tyy = (fTY * m_y);
	FXD::F32 tyz = (fTZ * m_y);
	FXD::F32 tzz = (fTZ * m_z);

	mat[0][0] = (1.0f - (tyy + tzz));
	mat[0][1] = (txy - twz);
	mat[0][2] = (txz + twy);
	mat[1][0] = (txy + twz);
	mat[1][1] = (1.0f - (txx + tzz));
	mat[1][2] = (tyz - twx);
	mat[2][0] = (txz - twy);
	mat[2][1] = (tyz + twx);
	mat[2][2] = (1.0f - (txx + tyy));
}

void Quaternion::toAxisAngle(Vector3F& axis, Degree& angle) const
{
	Radian tempRad(angle);
	toAxisAngle(axis, tempRad);
}

void Quaternion::toAxisAngle(Vector3F& axis, Radian& angle) const
{
	FXD::F32 sqrLength = ((m_x * m_x) + (m_y * m_y) + (m_z * m_z));
	if (sqrLength > 0.0f)
	{
		angle = (2.0f * Math::FuncsMath::AcosFRad(m_w));
		FXD::F32 fInvLength = Math::InvSqrt(sqrLength);
		axis.x(m_x * fInvLength);
		axis.y(m_y * fInvLength);
		axis.z(m_z * fInvLength);
	}
	else
	{
		// Angle is 0 (mod 2*pi), so any axis will do
		angle = Radian(0.0f);
		axis.x(1.0f);
		axis.y(0.0f);
		axis.z(0.0f);
	}
}

void Quaternion::toAxes(Vector3F& xaxis, Vector3F& yaxis, Vector3F& zaxis) const
{
	Matrix3 matRot;
	toRotationMatrix(matRot);

	xaxis.x(matRot[0][0]);
	xaxis.y(matRot[1][0]);
	xaxis.z(matRot[2][0]);

	yaxis.x(matRot[0][1]);
	yaxis.y(matRot[1][1]);
	yaxis.z(matRot[2][1]);

	zaxis.x(matRot[0][2]);
	zaxis.y(matRot[1][2]);
	zaxis.z(matRot[2][2]);
}

bool Quaternion::toEulerAngles(Degree& xAngle, Degree& yAngle, Degree& zAngle) const
{
	Radian tempXAngle(xAngle);
	Radian tempYAngle(yAngle);
	Radian tempZAngle(zAngle);
	return toEulerAngles(tempXAngle, tempYAngle, tempZAngle);
}

bool Quaternion::toEulerAngles(Radian& xAngle, Radian& yAngle, Radian& zAngle) const
{
	Matrix3 matRot;
	toRotationMatrix(matRot);
	return matRot.toEulerAngles(xAngle, yAngle, zAngle);
	return false;
}

Vector3F Quaternion::xAxis(void) const
{
	FXD::F32 fTY = (2.0f * m_y);
	FXD::F32 fTZ = (2.0f * m_z);
	FXD::F32 fTWY = (fTY * m_w);
	FXD::F32 fTWZ = (fTZ * m_w);
	FXD::F32 fTXY = (fTY * m_x);
	FXD::F32 fTXZ = (fTZ * m_x);
	FXD::F32 fTYY = (fTY * m_y);
	FXD::F32 fTZZ = (fTZ * m_z);
	return Vector3F(1.0f - (fTYY + fTZZ), fTXY + fTWZ, fTXZ - fTWY);
}

Vector3F Quaternion::yAxis(void) const
{
	FXD::F32 fTX = (2.0f * m_x);
	FXD::F32 fTY = (2.0f * m_y);
	FXD::F32 fTZ = (2.0f * m_z);
	FXD::F32 fTWX = (fTX * m_w);
	FXD::F32 fTWZ = (fTZ * m_w);
	FXD::F32 fTXX = (fTX * m_x);
	FXD::F32 fTXY = (fTY * m_x);
	FXD::F32 fTYZ = (fTZ * m_y);
	FXD::F32 fTZZ = (fTZ * m_z);
	return Vector3F(fTXY - fTWZ, 1.0f - (fTXX + fTZZ), fTYZ + fTWX);
}

Vector3F Quaternion::zAxis(void) const
{
	FXD::F32 fTX = (2.0f * m_x);
	FXD::F32 fTY = (2.0f * m_y);
	FXD::F32 fTZ = (2.0f * m_z);
	FXD::F32 fTWX = (fTX * m_w);
	FXD::F32 fTWY = (fTY * m_w);
	FXD::F32 fTXX = (fTX * m_x);
	FXD::F32 fTXZ = (fTZ * m_x);
	FXD::F32 fTYY = (fTY * m_y);
	FXD::F32 fTYZ = (fTZ * m_y);

	return Vector3F(fTXZ + fTWY, fTYZ - fTWX, 1.0f - (fTXX + fTYY));
}

void Quaternion::lookRotation(const Vector3F& forwardDir)
{
	if (forwardDir == Vector3F::kZero)
	{
		return;
	}

	Vector3F nrmForwardDir = Vector3F::get_normalise(forwardDir);
	Vector3F currentForwardDir = -zAxis();

	Quaternion targetRotation;
	if (((nrmForwardDir + currentForwardDir).squared_length()) < 0.00005f)
	{
		// Oops, a 180 degree turn (infinite possible rotation axes)
		// Default to yaw i.e. use current kUp
		(*this) = Quaternion(-m_y, -m_z, m_w, m_x);
	}
	else
	{
		// Derive shortest arc to new direction
		Quaternion rotQuat = getRotationFromTo(currentForwardDir, nrmForwardDir);
		(*this) = (rotQuat * (*this));
	}
}

void Quaternion::lookRotation(const Vector3F& forwardDir, const Vector3F& upDir)
{
	Vector3F forward = Vector3F::get_normalise(forwardDir);
	Vector3F up = Vector3F::get_normalise(upDir);

	if (Math::ApproxEquals(Vector3F::dot(forward, up), 1.0f))
	{
		lookRotation(forward);
		return;
	}

	Vector3F x = Vector3F::cross(forward, up);
	Vector3F y = Vector3F::cross(x, forward);

	x.normalise();
	y.normalise();

	(*this) = Quaternion(x, y, -forward);
}

Quaternion Quaternion::slerp(FXD::F32 t, const Quaternion& p, const Quaternion& q, bool bShortestPath)
{
	FXD::F32 fCos = p.dot(q);
	Quaternion quat;

	if ((fCos < 0.0f) && (bShortestPath))
	{
		fCos = -fCos;
		quat = -q;
	}
	else
	{
		quat = q;
	}

	if (Math::Abs(fCos) < (1.0f - kEpsilon))
	{
		// Standard case (slerp)
		FXD::F32 fSin = Math::Sqrt(1 - Math::Sqr(fCos));
		Radian angle = Math::FuncsMath::Atan2Rad(fSin, fCos);
		FXD::F32 fInvSin = (1.0f / fSin);
		FXD::F32 fCoeff0 = (Math::FuncsMath::SinRad((1.0f - t) * angle) * fInvSin);
		FXD::F32 fCoeff1 = (Math::FuncsMath::SinRad(t * angle) * fInvSin);
		return (fCoeff0 * p + fCoeff1 * quat);
	}
	else
	{
		// There are two situations:
		// 1. "p" and "q" are very close (fCos ~= +1), so we can do a linear
		//		interpolation safely.
		// 2. "p" and "q" are almost inverse of each other (fCos ~= -1), there
		//		are an infinite number of possibilities interpolation. but we haven't
		//		have method to fix this case, so just use linear interpolation here.
		Quaternion ret = (1.0f - t) * p + t * quat;

		// Taking the complement requires renormalization
		ret.normalise();
		return ret;
	}
}

Quaternion Quaternion::getRotationFromTo(const Vector3F& from, const Vector3F& dst, const Vector3F& fallbackAxis)
{
	// Based on Stan Melax's article in Game Programming Gems
	Quaternion q;
	Vector3F v0 = Vector3F::get_normalise(from);
	Vector3F v1 = Vector3F::get_normalise(dst);

	FXD::F32 fDot = v0.dot(v1);
	// If dot == 1.0f, vectors are the same
	if (fDot >= 1.0f)
	{
		return Quaternion::kIdentity;
	}

	if (fDot < (1e-6f - 1.0f))
	{
		if (fallbackAxis != Vector3F::kZero)
		{
			// Rotate 180 degrees about the fallback axis
			q.fromAxisAngle(fallbackAxis, Radian(Math::PI));
		}
		else
		{
			// Generate an axis
			Vector3F axis = Vector3F::kUnitX.cross(from);
			//if (axis.isZeroLength()) // Pick another if colinear
			//{
			//	axis = Vector3F::kUnitY.cross(from);
			//}
			axis.normalise();
			q.fromAxisAngle(axis, Radian(Math::PI));
		}
	}
	else
	{
		FXD::F32 fSqrt = Math::Sqrt((1 + fDot) * 2);
		FXD::F32 fInvs = (1.0f / fSqrt);

		Vector3F c = v0.cross(v1);
		q.m_x = (c.x() * fInvs);
		q.m_y = (c.y() * fInvs);
		q.m_z = (c.z() * fInvs);
		q.m_w = (fSqrt * 0.5f);
		q.normalise();
	}
	return q;
}
*/