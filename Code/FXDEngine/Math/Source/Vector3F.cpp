// Creator - MatthewGolder
#include "FXDEngine/Math/Vector3F.h"
#include "FXDEngine/Math/Vector4F.h"
#include "FXDEngine/Math/Math.h"
#include "FXDEngine/Core/Algorithm.h"
#include "FXDEngine/Core/Debugger.h"

using namespace FXD;
using namespace Math;

// ------
// Vector3F
// -
// ------
const Vector3F Vector3F::kZero(0.0f);
const Vector3F Vector3F::kOne(1.0f);
const Vector3F Vector3F::kInf(Math::NumericLimits< FXD::F32 >::infinity());
const Vector3F Vector3F::kNegInf(-Math::NumericLimits< FXD::F32 >::infinity());
const Vector3F Vector3F::kLeft(-1.0F, 0.0f, 0.0f);
const Vector3F Vector3F::kRight(1.0F, 0.0f, 0.0f);
const Vector3F Vector3F::kUp(0.0f, 1.0f, 0.0f);
const Vector3F Vector3F::kDown(0.0f, -1.0f, 0.0f);
const Vector3F Vector3F::kForward(0.0f, 0.0f, 1.0f);
const Vector3F Vector3F::kBack(0.0f, 0.0f, -1.0f);
const Vector3F Vector3F::kUnitX(1.0f, 0.0f, 0.0f);
const Vector3F Vector3F::kUnitY(0.0f, 1.0f, 0.0f);
const Vector3F Vector3F::kUnitZ(0.0f, 0.0f, 1.0f);

Vector3F::Vector3F(void)
{
	Vec3FSet(m_data, 0.0f);
}
Vector3F::Vector3F(FXD::F32 val)
{
	Vec3FSet(m_data, val);
}
Vector3F::Vector3F(FXD::F32 x, FXD::F32 y, FXD::F32 z)
{
	Vec3FSet(m_data, x, y, z);
}
Vector3F::~Vector3F(void)
{
}

FXD::F32 Vector3F::operator[](FXD::U32 nID) const
{
	PRINT_COND_ASSERT((nID < 3), "Math: Index out of bounds");
	return Vec3FGetIndex(m_data, nID);
}
FXD::F32 Vector3F::operator()(FXD::U32 nID) const
{
	PRINT_COND_ASSERT((nID < 3), "Math: Index out of bounds");
	return Vec3FGetIndex(m_data, nID);
}

Vector3F& Vector3F::operator=(const Vector3F& rhs)
{
	Vec3FCopy(m_data, rhs.m_data);
	return (*this);
}
Vector3F& Vector3F::operator=(FXD::F32 rhs)
{
	Vec3FSet(m_data, rhs);
	return (*this);
}

Vector3F Vector3F::operator-() const
{
	Vector3F out = (*this);
	out.negate();
	return out;
}

FXD::F32 Vector3F::x(void) const
{
	return Vec3FGetX(m_data);
}
FXD::F32 Vector3F::y(void) const
{
	return Vec3FGetY(m_data);
}
FXD::F32 Vector3F::z(void) const
{
	return Vec3FGetZ(m_data);
}
FXD::F32 Vector3F::index(FXD::U32 nID)
{
	PRINT_COND_ASSERT((nID < 3), "Math: Index out of bounds");
	return Vec3FGetIndex(m_data, nID);
}

void Vector3F::x(FXD::F32 x)
{
	Vec3FSetX(m_data, x);
}
void Vector3F::y(FXD::F32 y)
{
	Vec3FSetY(m_data, y);
}
void Vector3F::z(FXD::F32 z)
{
	Vec3FSetZ(m_data, z);
}
void Vector3F::index(FXD::U32 nID, FXD::F32 fVal)
{
	PRINT_COND_ASSERT((nID < 3), "Math: Index out of bounds");
	Vec3FSetIndex(m_data, nID, fVal);
}

FXD::F32 Vector3F::dot(const Vector3F& rhs) const
{
	return Vec3FDot(m_data, rhs.m_data);
}
Vector3F Vector3F::cross(const Vector3F& rhs) const
{
	Vector3F retVec;
	retVec.m_data = Vec3FCross(m_data, rhs.m_data);
	return retVec;
}
FXD::F32 Vector3F::length(void) const
{
	return Math::Sqrt(squared_length());
}
FXD::F32 Vector3F::squared_length(void) const
{
	return Vec3FSquaredLength(m_data);
}
FXD::F32 Vector3F::distance(const Vector3F& rhs) const
{
	return Math::Sqrt(sqrd_distance(rhs));
}
FXD::F32 Vector3F::sqrd_distance(const Vector3F& rhs) const
{
	return Vec3FSquaredDistance(m_data, rhs.m_data);
}
Vector3F Vector3F::midpoint(const Vector3F& rhs) const
{
	Vector3F retVec;
	Vec3FMidPoint(retVec.m_data, m_data, rhs.m_data);
	return retVec;
}
FXD::F32 Vector3F::angle_between(const Vector3F& dst)
{
	return Vec3FAngleBetween(m_data, dst.m_data);
}

void Vector3F::negate(void)
{
	Vec3FNegate(m_data);
}
void Vector3F::minimum(const Vector3F& min)
{
	Vec3FMinimum(m_data, min.m_data);
}
void Vector3F::maximum(const Vector3F& max)
{
	Vec3FMaximum(m_data, max.m_data);
}
void Vector3F::grid_snap(const FXD::F32 fGridSize)
{
	Vec3FSet(m_data, Math::GridSnap(x(), fGridSize), Math::GridSnap(y(), fGridSize), Math::GridSnap(z(), fGridSize));
}
void Vector3F::clamp(const Vector3F& min, const Vector3F& max)
{
	Vec3FClamp(m_data, min.m_data, max.m_data);
}
void Vector3F::normalise(void)
{
	Vec3FNormalise(m_data);
}
void Vector3F::abs(void)
{
	Vec3FAbs(m_data);
}
void Vector3F::ceil(void)
{
	Vec3FCeil(m_data);
}
void Vector3F::floor(void)
{
	Vec3FFloor(m_data);
}

Vector3F Vector3F::get_negate(void) const
{
	Vector3F retVec = (*this);
	retVec.negate();
	return retVec;
}
Vector3F Vector3F::get_minimum(const Vector3F& min) const
{
	Vector3F retVec = (*this);
	retVec.minimum(min);
	return retVec;
}
Vector3F Vector3F::get_maximum(const Vector3F& max) const
{
	Vector3F retVec = (*this);
	retVec.maximum(max);
	return retVec;
}
Vector3F Vector3F::get_grid_snap(const FXD::F32 fGridSize)
{
	Vector3F retVec = (*this);
	retVec.grid_snap(fGridSize);
	return retVec;
}
Vector3F Vector3F::get_clamp(const Vector3F& min, const Vector3F& max) const
{
	Vector3F retVec = (*this);
	retVec.clamp(min, max);
	return retVec;
}
Vector3F Vector3F::get_normalise(void) const
{
	Vector3F retVec = (*this);
	retVec.normalise();
	return retVec;
}
Vector3F Vector3F::get_abs(void) const
{
	Vector3F retVec = (*this);
	retVec.abs();
	return retVec;
}
Vector3F Vector3F::get_ceil(void) const
{
	Vector3F retVec = (*this);
	retVec.ceil();
	return retVec;
}
Vector3F Vector3F::get_floor(void) const
{
	Vector3F retVec = (*this);
	retVec.floor();
	return retVec;
}

// STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS 
FXD::F32 Vector3F::dot(const Vector3F& lhs, const Vector3F& rhs)
{
	return lhs.dot(rhs);
}
Vector3F Vector3F::cross(const Vector3F& lhs, const Vector3F& rhs)
{
	return lhs.dot(lhs, rhs);
}
FXD::F32 Vector3F::distance(const Vector3F& lhs, const Vector3F& rhs)
{
	return lhs.distance(rhs);
}
FXD::F32 Vector3F::sqrd_distance(const Vector3F& lhs, const Vector3F& rhs)
{
	return lhs.sqrd_distance(rhs);
}
Vector3F Vector3F::midpoint(const Vector3F& lhs, const Vector3F& rhs)
{
	return lhs.midpoint(rhs);
}

void Vector3F::set_negate(Vector3F& vec)
{
	vec.negate();
}
void Vector3F::set_minimum(Vector3F& vec, const Vector3F& min)
{
	vec.minimum(min);
}
void Vector3F::set_maximum(Vector3F& vec, const Vector3F& max)
{
	vec.maximum(max);
}
void Vector3F::set_grid_snap(Vector3F& vec, const FXD::F32 fGridSize)
{
	vec.grid_snap(fGridSize);
}
void Vector3F::set_clamp(Vector3F& vec, const Vector3F& min, const Vector3F& max)
{
	vec.clamp(min, max);
}
void Vector3F::set_normalise(Vector3F& vec)
{
	vec.normalise();
}
void Vector3F::set_abs(Vector3F& vec)
{
	vec.abs();
}
void Vector3F::set_ceil(Vector3F& vec)
{
	vec.ceil();
}
void Vector3F::set_floor(Vector3F& vec)
{
	vec.floor();
}
void Vector3F::set_lerp(Vector3F& vec, const Vector3F& lhs, const Vector3F& rhs, FXD::F32 fVal)
{
	vec = lhs + (rhs - lhs) * fVal;
}

Vector3F Vector3F::get_negate(const Vector3F& vec)
{
	Vector3F retVec = vec;
	Vector3F::set_negate(retVec);
	return retVec;
}
Vector3F Vector3F::get_minimum(const Vector3F& vec, const Vector3F& min)
{
	Vector3F retVec = vec;
	Vector3F::set_minimum(retVec, min);
	return retVec;
}
Vector3F Vector3F::get_maximum(const Vector3F& vec, const Vector3F& max)
{
	Vector3F retVec = vec;
	Vector3F::set_maximum(retVec, max);
	return retVec;
}
Vector3F Vector3F::get_grid_snap(const Vector3F& vec, const FXD::F32 fGridSize)
{
	Vector3F retVec = vec;
	Vector3F::set_grid_snap(retVec, fGridSize);
	return retVec;
}
Vector3F Vector3F::get_clamp(const Vector3F& vec, const Vector3F& min, const Vector3F& max)
{
	Vector3F retVec = vec;
	Vector3F::set_clamp(retVec, min, max);
	return retVec;
}
Vector3F Vector3F::get_normalise(const Vector3F& vec)
{
	Vector3F retVec = vec;
	Vector3F::set_normalise(retVec);
	return retVec;
}
Vector3F Vector3F::get_abs(const Vector3F& vec)
{
	Vector3F retVec = vec;
	Vector3F::set_abs(retVec);
	return retVec;
}
Vector3F Vector3F::get_ceil(const Vector3F& vec)
{
	Vector3F retVec = vec;
	Vector3F::set_ceil(retVec);
	return retVec;
}
Vector3F Vector3F::get_floor(const Vector3F& vec)
{
	Vector3F retVec = vec;
	Vector3F::set_floor(retVec);
	return retVec;
}
Vector3F Vector3F::get_lerp(const Vector3F& lhs, const Vector3F& rhs, FXD::F32 fVal)
{
	Vector3F retVec;
	Vector3F::set_lerp(retVec, lhs, rhs, fVal);
	return retVec;
}

/*
// ------
// Vector3
// -
// ------
const Vector3 Vector3::kZero(0.0f);
const Vector3 Vector3::kOne(1.0f, 1.0f, 1.0f);
const Vector3 Vector3::kInf(Math::NumericLimits< FXD::F32 >::infinity());
const Vector3 Vector3::kUnitX(1.0f, 0.0f, 0.0f);
const Vector3 Vector3::kUnitY(0.0f, 1.0f, 0.0f);
const Vector3 Vector3::kUnitZ(0.0f, 0.0f, 1.0f);

Vector3::Vector3(void)
	: m_x(0.0f)
	, m_y(0.0f)
	, m_z(0.0f)
{
}

Vector3::Vector3(FXD::F32 x, FXD::F32 y, FXD::F32 z)
	: m_x(x)
	, m_y(y)
	, m_z(z)
{
}

Vector3::Vector3(const Vector4F& vec)
	: m_x(vec.x())
	, m_y(vec.y())
	, m_z(vec.z())
{
}

Vector3::~Vector3(void)
{
}

Vector3& Vector3::operator=(const Vector3& rhs)
{
	m_x = rhs.m_x;
	m_y = rhs.m_y;
	m_z = rhs.m_z;
	return (*this);
}

Vector3& Vector3::operator=(FXD::F32 rhs)
{
	m_x = rhs;
	m_y = rhs;
	m_z = rhs;
	return (*this);
}


FXD::F32 Vector3::operator[](FXD::U32 i) const
{
	PRINT_COND_ASSERT((i < 3), "Math:");
	return *(&m_x + i);
}

FXD::F32& Vector3::operator[](FXD::U32 i)
{
	PRINT_COND_ASSERT((i < 3), "Math:");
	return *(&m_x + i);
}

const Vector3& Vector3::operator+() const
{
	return (*this);
}
Vector3 Vector3::operator+(const Vector3& rhs) const
{
	return Vector3((m_x + rhs.m_x), (m_y + rhs.m_y), (m_z + rhs.m_z));
}
Vector3 Vector3::operator+(FXD::F32 rhs) const
{
	return Vector3((m_x + rhs), (m_y + rhs), (m_z + rhs));
}
Vector3 Vector3::operator-() const
{
	return Vector3(-m_x, -m_y, -m_z);
}
Vector3 Vector3::operator-(const Vector3& rhs) const
{
	return Vector3((m_x - rhs.m_x), (m_y - rhs.m_y), (m_z - rhs.m_z));
}
Vector3 Vector3::operator-(FXD::F32 rhs) const
{
	return Vector3((m_x - rhs), (m_y - rhs), (m_z - rhs));
}
Vector3 Vector3::operator*(const Vector3& rhs) const
{
	return Vector3((m_x * rhs.m_x), (m_y * rhs.m_y), (m_z * rhs.m_z));
}
Vector3 Vector3::operator*(FXD::F32 rhs) const
{
	return Vector3((m_x * rhs), (m_y * rhs), (m_z * rhs));
}
Vector3 Vector3::operator/(const Vector3& rhs) const
{
	return Vector3((m_x / rhs.m_x), (m_y / rhs.m_y), (m_z / rhs.m_z));
}
Vector3 Vector3::operator/(FXD::F32 val) const
{
	PRINT_COND_ASSERT((val != 0.0f), "Math:");
	FXD::F32 fInv = 1.0f / val;
	return Vector3((m_x * fInv), (m_y * fInv), (m_z * fInv));
}

Vector3& Vector3::operator+=(const Vector3& rhs)
{
	m_x += rhs.m_x;
	m_y += rhs.m_y;
	m_z += rhs.m_z;
	return (*this);
}
Vector3& Vector3::operator+=(FXD::F32 rhs)
{
	m_x += rhs;
	m_y += rhs;
	m_z += rhs;
	return (*this);
}
Vector3& Vector3::operator-=(const Vector3& rhs)
{
	m_x -= rhs.m_x;
	m_y -= rhs.m_y;
	m_z -= rhs.m_z;
	return (*this);
}
Vector3& Vector3::operator-=(FXD::F32 rhs)
{
	m_x -= rhs;
	m_y -= rhs;
	m_z -= rhs;
	return (*this);
}
Vector3& Vector3::operator*=(const Vector3& rhs)
{
	m_x *= rhs.m_x;
	m_y *= rhs.m_y;
	m_z *= rhs.m_z;
	return (*this);
}
Vector3& Vector3::operator*=(FXD::F32 rhs)
{
	m_x *= rhs;
	m_y *= rhs;
	m_z *= rhs;
	return (*this);
}
Vector3& Vector3::operator/=(const Vector3& rhs)
{
	m_x /= rhs.m_x;
	m_y /= rhs.m_y;
	m_z /= rhs.m_z;
	return (*this);
}
Vector3& Vector3::operator/=(FXD::F32 rhs)
{
	PRINT_COND_ASSERT((rhs != 0.0f), "Math:");
	FXD::F32 fInv = (1.0f / rhs);
	m_x *= fInv;
	m_y *= fInv;
	m_z *= fInv;
	return (*this);
}

bool Vector3::operator==(const Vector3& rhs) const
{
	return ((m_x == rhs.m_x) && (m_y == rhs.m_y) && (m_z == rhs.m_z));
}

bool Vector3::operator!=(const Vector3& rhs) const
{
	return ((m_x != rhs.m_x) || (m_y != rhs.m_y) || (m_z != rhs.m_z));
}


void Vector3::normalise(void)
{
	(*this) = Vector3::normalise((*this));
}

void Vector3::floor(const Vector3& cmp)
{
	(*this) = Vector3::floor((*this), cmp);
}

void Vector3::ceil(const Vector3& cmp)
{
	(*this) = Vector3::ceil((*this), cmp);
}

FXD::F32 Vector3::length(void) const
{
	return Math::Sqrt(squared_length());
}

FXD::F32 Vector3::squared_length(void) const
{
	return (m_x * m_x) + (m_y * m_y) + (m_z * m_z);
}

FXD::F32 Vector3::distance(const Vector3& rhs) const
{
	return (*this - rhs).length();
}

FXD::F32 Vector3::squaredDistance(const Vector3& rhs) const
{
	return (*this - rhs).squared_length();
}

FXD::F32 Vector3::dot(const Vector3& vec) const
{
	return Vector3::dot((*this), vec);
}

Vector3 Vector3::perpendicular(void) const
{
	static const FXD::F32 fSquareZero = (FXD::F32)(1e-06 * 1e-06);
	Vector3 perp = cross(Vector3::kUnitX);
	if (perp.squared_length() < fSquareZero)
	{
		perp = cross(Vector3::kUnitY);
	}
	perp.normalise();
	return perp;
}

Vector3 Vector3::cross(const Vector3& rhs) const
{
	return Vector3::cross((*this), rhs);
}

bool Vector3::isZeroLength(void) const
{
	FXD::F32 fSqlen = squared_length();
	return (fSqlen < (1e-06 * 1e-06));
}

Vector3 Vector3::reflect(const Vector3& normal) const
{
	return Vector3((*this) - (2.0f * dot(normal) * normal));
}

Radian Vector3::angle_between(const Vector3& dst) const
{
	FXD::F32 fLenProduct = (length() * dst.length());

	// Divide by zero check
	if (fLenProduct < 1e-6f)
	{
		fLenProduct = 1e-6f;
	}

	FXD::F32 fVal = (dot(dst) / fLenProduct);
	fVal = FXD::STD::clamp(fVal, -1.0f, 1.0f);
	return Math::FuncsMath::AcosFRad(fVal);
}

void Vector3::orthogonalComplement(Vector3& lhs, Vector3& rhs)
{
	if (Math::Abs(m_x) > Math::Abs(m_y))
		lhs = Vector3(-m_z, 0.0f, m_x);
	else
		lhs = Vector3(0.0f, m_z, -m_y);
	rhs = cross(lhs);
	orthonormalize(*this, lhs, rhs);
}

void Vector3::orthonormalize(Vector3& vec0, Vector3& vec1, Vector3& vec2)
{
	vec0.normalise();

	FXD::F32 fDot0 = vec0.dot(vec1);
	vec1 -= (fDot0 * vec0);
	vec1.normalise();

	FXD::F32 fDot1 = vec1.dot(vec2);
	fDot0 = vec0.dot(vec2);
	vec2 -= ((fDot0 * vec0) + (fDot1 * vec1));
	vec2.normalise();
}

Vector3 Vector3::normalise(const Vector3& val)
{
	FXD::F32 fLen = val.length();
	// Will also work for zero-sized vectors, but will change nothing
	if (fLen > 1e-08)
	{
		FXD::F32 fInvLen = (1.0f / fLen);
		Vector3 normalizedVec;
		normalizedVec.m_x = (val.m_x * fInvLen);
		normalizedVec.m_y = (val.m_y * fInvLen);
		normalizedVec.m_z = (val.m_z * fInvLen);
		return normalizedVec;
	}
	else
		return val;
}

Vector3 Vector3::floor(const Vector3& val, const Vector3& cmp)
{
	Vector3 ret = val;
	if (cmp.m_x < ret.m_x)
		ret.m_x = cmp.m_x;
	if (cmp.m_y < ret.m_y)
		ret.m_y = cmp.m_y;
	if (cmp.m_z < ret.m_z)
		ret.m_z = cmp.m_z;
	return ret;
}

Vector3 Vector3::ceil(const Vector3& val, const Vector3& cmp)
{
	Vector3 ret = val;
	if (cmp.m_x > ret.m_x)
		ret.m_x = cmp.m_x;
	if (cmp.m_y > ret.m_y)
		ret.m_y = cmp.m_y;
	if (cmp.m_z > ret.m_z)
		ret.m_z = cmp.m_z;
	return ret;
}

FXD::F32 Vector3::dot(const Vector3& lhs, const Vector3& rhs)
{
	return ((lhs.m_x * rhs.m_x) + (lhs.m_y * rhs.m_y) + (lhs.m_z * rhs.m_z));
}

Vector3 Vector3::cross(const Vector3& lhs, const Vector3& rhs)
{
	return Vector3(
		(lhs.m_y * rhs.m_z) - (lhs.m_z * rhs.m_y),
		(lhs.m_z * rhs.m_x) - (lhs.m_x * rhs.m_z),
		(lhs.m_x * rhs.m_y) - (lhs.m_y * rhs.m_x));
}
*/