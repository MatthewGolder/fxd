// Creator - MatthewGolder
#include "FXDEngine/Math/Matrix3.h"
#include "FXDEngine/Math/Matrix4.h"
#include "FXDEngine/Math/Vector3F.h"
#include "FXDEngine/Math/Quaternion.h"

using namespace FXD;
using namespace Math;

const Matrix3F Matrix3F::kZero = Matrix3F(
	0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f);
const Matrix3F Matrix3F::kIdentity = Matrix3F(
	1.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 1.0f);

// ------
// Matrix3F
// -
// ------
Matrix3F::Matrix3F(void)
{
	Mat3Copy(m_data, Matrix3F::kIdentity.m_data);
}

Matrix3F::Matrix3F(FXD::F32 m00, FXD::F32 m01, FXD::F32 m02, FXD::F32 m10, FXD::F32 m11, FXD::F32 m12, FXD::F32 m20, FXD::F32 m21, FXD::F32 m22)
{
	Mat3Set(m_data, m00, m01, m02, m10, m11, m12, m20, m21, m22);
}

Matrix3F::Matrix3F(const Matrix3F& mat)
{
	Mat3Copy(m_data, mat.m_data);
}

Matrix3F::Matrix3F(const Matrix4F& mat)
{
	Mat3Copy(m_data, mat.m_data);
}

Matrix3F::~Matrix3F(void)
{
}

FXD::F32 Matrix3F::operator()(FXD::U32 nRow, FXD::U32 nCol) const
{
	return get_row(nRow)[nCol];
}

Matrix3F Matrix3F::operator-() const
{
	Matrix3F out = (*this);
	out.negate();
	return out;
}

FXD::F32 Matrix3F::detrament(void) const
{
	return Mat3Detrament(m_data);
}

void Matrix3F::negate(void)
{
	Mat3Negate(m_data);
}

void Matrix3F::transpose(void)
{
	_matrix3Type matTemp;
	Mat3Transpose(matTemp, m_data);
	Mat3Copy(m_data, matTemp);
}

void Matrix3F::inverse(void)
{
	_matrix3Type matTemp;
	Mat3Inverse(matTemp, m_data);
	Mat3Copy(m_data, matTemp);
}

void Matrix3F::set_row(FXD::U32 nRow, const Vector3F& vec)
{
	Mat3SetRow(m_data, nRow, vec.m_data);
}

void Matrix3F::set_row(FXD::U32 nRow, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	Mat3SetRow(m_data, nRow, fX, fY, fZ);
}

void Matrix3F::set_column(FXD::U32 nCol, const Vector3F& vec)
{
	Mat3SetColumn(m_data, nCol, vec.m_data);
}

void Matrix3F::set_column(FXD::U32 nCol, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	Mat3SetColumn(m_data, nCol, fX, fY, fZ);
}

void Matrix3F::set_right(const Vector3F& vec)
{
	Mat3SetRight(m_data, vec.m_data);
}

void Matrix3F::set_right(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	Mat3SetRight(m_data, fX, fY, fZ);
}

void Matrix3F::set_up(const Vector3F& vec)
{
	Mat3SetUp(m_data, vec.m_data);
}

void Matrix3F::set_up(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	Mat3SetUp(m_data, fX, fY, fZ);
}

void Matrix3F::set_forward(const Vector3F& vec)
{
	Mat3SetForward(m_data, vec.m_data);
}

void Matrix3F::set_forward(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	Mat3SetForward(m_data, fX, fY, fZ);
}

void Matrix3F::set_scale(const Vector3F& vec)
{
	Mat3SetScale(m_data, vec.m_data);
}

void Matrix3F::set_scale(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	Mat3SetScale(m_data, fX, fY, fZ);
}
void Matrix3F::set_rotationX(FXD::F32 fAngleX)
{
	Mat3SetRotationX(m_data, fAngleX);
}

void Matrix3F::set_rotationY(FXD::F32 fAngleY)
{
	Mat3SetRotationY(m_data, fAngleY);
}

void Matrix3F::set_rotationZ(FXD::F32 fAngleZ)
{
	Mat3SetRotationZ(m_data, fAngleZ);
}

void Matrix3F::set_rotation_pitch_yaw_roll(Math::Radian rPitch, Math::Radian rYaw, Math::Radian rRoll, Math::EulerAngleOrder eOrder)
{
	Mat3SetRotationPitchYawRoll(m_data, rPitch, rYaw, rRoll, eOrder);
}

void Matrix3F::set_rotation_pitch_yaw_roll(const Vector3F& angle, Math::EulerAngleOrder eOrder)
{
	Mat3SetRotationPitchYawRoll(m_data, angle.m_data, eOrder);
}

void Matrix3F::set_rotation_quaternion(const QuaternionF& quat)
{
	Mat3SetRotationQuaternion(m_data, quat.m_data);
}

Matrix3F Matrix3F::get_negate(void) const
{
	Matrix3F retMat = (*this);
	Mat3Negate(retMat.m_data);
	return retMat;
}

Matrix3F Matrix3F::get_transpose(void) const
{
	Matrix3F retMat;
	Mat3Transpose(retMat.m_data, m_data);
	return retMat;
}

Matrix3F Matrix3F::get_inverse(void) const
{
	Matrix3F retMat;
	Mat3Inverse(retMat.m_data, m_data);
	return retMat;
}

Vector3F Matrix3F::get_row(FXD::U32 nRow) const
{
	Vector3F retVec;
	Mat3GetRow(retVec.m_data, m_data, nRow);
	return retVec;
}

Vector3F Matrix3F::get_column(FXD::U32 nCol) const
{
	Vector3F retVec;
	Mat3GetColumn(retVec.m_data, m_data, nCol);
	return retVec;
}

Vector3F Matrix3F::get_right(void) const
{
	Vector3F retVec;
	Mat3GetRight(retVec.m_data, m_data);
	return retVec;
}

Vector3F Matrix3F::get_up(void) const
{
	Vector3F retVec;
	Mat3GetUp(retVec.m_data, m_data);
	return retVec;
}

Vector3F Matrix3F::get_forward(void) const
{
	Vector3F retVec;
	Mat3GetForward(retVec.m_data, m_data);
	return retVec;
}

Vector3F Matrix3F::get_scale(void) const
{
	Vector3F retVec;
	Mat3GetScale(retVec.m_data, m_data);
	return retVec;
}

// STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS 
void Matrix3F::set_negate(Matrix3F& dst)
{
	dst.negate();
}

void Matrix3F::set_transpose(Matrix3F& dst)
{
	dst.transpose();
}

void Matrix3F::set_inverse(Matrix3F& dst)
{
	dst.inverse();
}

void Matrix3F::set_right(Matrix3F& dst, const Vector3F& vec)
{
	dst.set_right(vec);
}

void Matrix3F::set_right(Matrix3F& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	dst.set_right(fX, fY, fZ);
}

void Matrix3F::set_up(Matrix3F& dst, const Vector3F& vec)
{
	dst.set_up(vec);
}

void Matrix3F::set_up(Matrix3F& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	dst.set_up(fX, fY, fZ);
}

void Matrix3F::set_forward(Matrix3F& dst, const Vector3F& vec)
{
	dst.set_forward(vec);
}

void Matrix3F::set_forward(Matrix3F& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	dst.set_forward(fX, fY, fZ);
}

void Matrix3F::set_scale(Matrix3F& dst, const Vector3F& vec)
{
	dst.set_scale(vec);
}

void Matrix3F::set_scale(Matrix3F& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	dst.set_scale(fX, fY, fZ);
}

void Matrix3F::set_rotationX(Matrix3F& dst, FXD::F32 fAngleX)
{
	dst.set_rotationX(fAngleX);
}

void Matrix3F::set_rotationY(Matrix3F& dst, FXD::F32 fAngleY)
{
	dst.set_rotationY(fAngleY);
}

void Matrix3F::set_rotationZ(Matrix3F& dst, FXD::F32 fAngleZ)
{
	dst.set_rotationZ(fAngleZ);
}

void Matrix3F::set_rotation_pitch_yaw_roll(Matrix3F& dst, Math::Radian rPitch, Math::Radian rYaw, Math::Radian rRoll, Math::EulerAngleOrder eOrder)
{
	dst.set_rotation_pitch_yaw_roll(rPitch, rYaw, rRoll, eOrder);
}

void Matrix3F::set_rotation_pitch_yaw_roll(Matrix3F& dst, const Vector3F& angle, Math::EulerAngleOrder eOrder)
{
	dst.set_rotation_pitch_yaw_roll(angle, eOrder);
}

void Matrix3F::set_rotation_quaternion(Matrix3F& dst, const QuaternionF& quat)
{
	dst.set_rotation_quaternion(quat);
}

FXD::F32 Matrix3F::get_detrament(const Matrix3F& mat)
{
	return mat.detrament();
}

Matrix3F Matrix3F::get_negate(const Matrix3F& mat)
{
	Matrix3F retMat = mat;
	Matrix3F::set_negate(retMat);
	return retMat;
}

Matrix3F Matrix3F::get_transpose(const Matrix3F& mat)
{
	Matrix3F retMat = mat;
	Matrix3F::set_transpose(retMat);
	return retMat;
}

Matrix3F Matrix3F::get_inverse(const Matrix3F& mat)
{
	Matrix3F retMat = mat;
	Matrix3F::set_inverse(retMat);
	return retMat;
}

Vector3F Matrix3F::get_row(const Matrix3F& mat, FXD::U32 nRow)
{
	return mat.get_row(nRow);
}

Vector3F Matrix3F::get_column(const Matrix3F& mat, FXD::U32 nCol)
{
	return mat.get_column(nCol);
}

Vector3F Matrix3F::get_right(const Matrix3F& mat)
{
	return mat.get_right();
}

Vector3F Matrix3F::get_up(const Matrix3F& mat)
{
	return mat.get_up();
}

Vector3F Matrix3F::get_forward(const Matrix3F& mat)
{
	return mat.get_forward();
}

Vector3F Matrix3F::get_scale(const Matrix3F& mat)
{
	return mat.get_scale();
}

Matrix3F Matrix3F::create_up(const Vector3F& vec)
{
	Matrix3F retMat;
	Matrix3F::set_up(retMat, vec);
	return retMat;
}

Matrix3F Matrix3F::create_up(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	Matrix3F retMat;
	Matrix3F::set_up(retMat, fX, fY, fZ);
	return retMat;
}

Matrix3F Matrix3F::create_forward(const Vector3F& vec)
{
	Matrix3F retMat;
	Matrix3F::set_forward(retMat, vec);
	return retMat;
}

Matrix3F Matrix3F::create_forward(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	Matrix3F retMat;
	Matrix3F::set_forward(retMat, fX, fY, fZ);
	return retMat;
}

Matrix3F Matrix3F::create_scale(const Vector3F& vec)
{
	Matrix3F retMat;
	Matrix3F::set_scale(retMat, vec);
	return retMat;
}

Matrix3F Matrix3F::create_scale(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
{
	Matrix3F retMat;
	Matrix3F::set_scale(retMat, fX, fY, fZ);
	return retMat;
}

Matrix3F Matrix3F::create_rotationX(FXD::F32 fAngleX)
{
	Matrix3F retMat;
	Matrix3F::set_rotationX(retMat, fAngleX);
	return retMat;
}

Matrix3F Matrix3F::create_rotationY(FXD::F32 fAngleY)
{
	Matrix3F retMat;
	Matrix3F::set_rotationY(retMat, fAngleY);
	return retMat;
}

Matrix3F Matrix3F::create_rotationZ(FXD::F32 fAngleZ)
{
	Matrix3F retMat;
	Matrix3F::set_rotationZ(retMat, fAngleZ);
	return retMat;
}

Matrix3F Matrix3F::create_rotation_pitch_yaw_roll(Math::Radian rPitch, Math::Radian rYaw, Math::Radian rRoll, Math::EulerAngleOrder eOrder)
{
	Matrix3F retMat;
	Matrix3F::set_rotation_pitch_yaw_roll(retMat, rPitch, rYaw, rRoll, eOrder);
	return retMat;
}

Matrix3F Matrix3F::create_rotation_pitch_yaw_roll(const Vector3F& angle, Math::EulerAngleOrder eOrder)
{
	Matrix3F retMat;
	Matrix3F::set_rotation_pitch_yaw_roll(retMat, angle, eOrder);
	return retMat;
}

Matrix3F Matrix3F::create_rotation_quaternion(const QuaternionF& quat)
{
	Matrix3F retMat;
	Matrix3F::set_rotation_quaternion(retMat, quat);
	return retMat;
}

/*
// ------
// Matrix3
// -
// ------
const FXD::F32 Matrix3::kEpsilon = 1e-06f;
const Matrix3 Matrix3::kZero(
	0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f);

const Matrix3 Matrix3::kIdentity(
	1.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 1.0f);

const FXD::F32 Matrix3::kSVDEpsilon = 1e-04f;
const FXD::U32 Matrix3::kSVDMaxItrs = 32;
const Matrix3::EulerAngleOrderData Matrix3::kEALookup[6] =
{ { 0, 1, 2, 1.0f }, { 0, 2, 1, -1.0f }, { 1, 0, 2, -1.0f },
{ 1, 2, 0, 1.0f }, { 2, 0, 1, 1.0f }, { 2, 1, 0, -1.0f } };;


Vector3F Matrix3::transform(const Vector3F& vec) const
{
	Vector3F prod;
	for (FXD::U32 nRow = 0; nRow < 3; nRow++)
	{
		prod.index(nRow, ((m_matrix[nRow][0] * vec[0]) + (m_matrix[nRow][1] * vec[1]) + (m_matrix[nRow][2] * vec[2])));
	}
	return prod;
}

Matrix3 Matrix3::transpose(void) const
{
	Matrix3 matTranspose;
	for (FXD::U32 nRow = 0; nRow < 3; nRow++)
	{
		for (FXD::U32 nCol = 0; nCol < 3; nCol++)
		{
			matTranspose[nRow][nCol] = m_matrix[nCol][nRow];
		}
	}
	return matTranspose;
}

bool Matrix3::inverse(Matrix3& matInv, FXD::F32 tolerance) const
{
	matInv[0][0] = ((m_matrix[1][1] * m_matrix[2][2]) - (m_matrix[1][2] * m_matrix[2][1]));
	matInv[0][1] = ((m_matrix[0][2] * m_matrix[2][1]) - (m_matrix[0][1] * m_matrix[2][2]));
	matInv[0][2] = ((m_matrix[0][1] * m_matrix[1][2]) - (m_matrix[0][2] * m_matrix[1][1]));
	matInv[1][0] = ((m_matrix[1][2] * m_matrix[2][0]) - (m_matrix[1][0] * m_matrix[2][2]));
	matInv[1][1] = ((m_matrix[0][0] * m_matrix[2][2]) - (m_matrix[0][2] * m_matrix[2][0]));
	matInv[1][2] = ((m_matrix[0][2] * m_matrix[1][0]) - (m_matrix[0][0] * m_matrix[1][2]));
	matInv[2][0] = ((m_matrix[1][0] * m_matrix[2][1]) - (m_matrix[1][1] * m_matrix[2][0]));
	matInv[2][1] = ((m_matrix[0][1] * m_matrix[2][0]) - (m_matrix[0][0] * m_matrix[2][1]));
	matInv[2][2] = ((m_matrix[0][0] * m_matrix[1][1]) - (m_matrix[0][1] * m_matrix[1][0]));

	FXD::F32 fDet = ((m_matrix[0][0] * matInv[0][0]) + (m_matrix[0][1] * matInv[1][0]) + (m_matrix[0][2] * matInv[2][0]));
	if (Math::FuncsMath::Abs(fDet) <= tolerance)
	{
		return false;
	}

	FXD::F32 fInvDet = (1.0f / fDet);
	for (FXD::U32 nRow = 0; nRow < 3; nRow++)
	{
		for (FXD::U32 nCol = 0; nCol < 3; nCol++)
		{
			matInv[nRow][nCol] *= fInvDet;
		}
	}
	return true;
}

Matrix3 Matrix3::inverse(FXD::F32 tolerance) const
{
	Matrix3 matInv = Matrix3::kZero;
	inverse(matInv, tolerance);
	return matInv;
}

FXD::F32 Matrix3::determinant(void) const
{
	FXD::F32 fCofactor00 = ((m_matrix[1][1] * m_matrix[2][2]) - (m_matrix[1][2] * m_matrix[2][1]));
	FXD::F32 fCofactor10 = ((m_matrix[1][2] * m_matrix[2][0]) - (m_matrix[1][0] * m_matrix[2][2]));
	FXD::F32 fCofactor20 = ((m_matrix[1][0] * m_matrix[2][1]) - (m_matrix[1][1] * m_matrix[2][0]));

	FXD::F32 fDet = ((m_matrix[0][0] * fCofactor00) + (m_matrix[0][1] * fCofactor10) + (m_matrix[0][2] * fCofactor20));
	return fDet;
}

void Matrix3::decomposition(Quaternion& rotation, Vector3F& scale) const
{
	Matrix3 matQ;
	Vector3F vecU;
	QDUDecomposition(matQ, scale, vecU);
	rotation = Quaternion(matQ);
}

void Matrix3::singularValueDecomposition(Matrix3& matL, Vector3F& matS, Matrix3& matR) const
{
	Matrix3 mat = (*this);
	bidiagonalize(mat, matL, matR);

	for (FXD::U32 n1 = 0; n1 < kSVDMaxItrs; n1++)
	{
		FXD::F32 fTmp0 = 0.0f;
		FXD::F32 fTmp1 = 0.0f;
		bool bTest1 = (Math::Abs(mat[0][1]) <= kSVDEpsilon * (Math::Abs(mat[0][0]) + Math::Abs(mat[1][1])));
		bool bTest2 = (Math::Abs(mat[1][2]) <= kSVDEpsilon * (Math::Abs(mat[1][1]) + Math::Abs(mat[2][2])));
		if (bTest1)
		{
			if (bTest2)
			{
				matS.x(mat[0][0]);
				matS.y(mat[1][1]);
				matS.z(mat[2][2]);
				break;
			}
			else
			{
				// 2x2 closed form factorization
				FXD::F32 fTmp = ((mat[1][1] * mat[1][1]) - (mat[2][2] * mat[2][2]) + (mat[1][2] * mat[1][2])) / (mat[1][2] * mat[2][2]);
				FXD::F32 fTan0 = (0.5f * (fTmp + Math::Sqrt(fTmp * fTmp + 4.0f)));
				FXD::F32 fCos0 = Math::InvSqrt(1.0f + fTan0 * fTan0);
				FXD::F32 fSin0 = (fTan0 * fCos0);

				for (FXD::U32 nCol = 0; nCol < 3; nCol++)
				{
					fTmp0 = matL[nCol][1];
					fTmp1 = matL[nCol][2];
					matL[nCol][1] = ((fCos0 * fTmp0) - (fSin0 * fTmp1));
					matL[nCol][2] = ((fSin0 * fTmp0) + (fCos0 * fTmp1));
				}

				FXD::F32 fTan1 = ((mat[1][2] - (mat[2][2] * fTan0)) / mat[1][1]);
				FXD::F32 fCos1 = Math::InvSqrt(1.0f + (fTan1 * fTan1));
				FXD::F32 fSin1 = (-fTan1 * fCos1);

				for (FXD::U32 nRow = 0; nRow < 3; nRow++)
				{
					fTmp0 = matR[1][nRow];
					fTmp1 = matR[2][nRow];
					matR[1][nRow] = ((fCos1 * fTmp0) - (fSin1 * fTmp1));
					matR[2][nRow] = ((fSin1 * fTmp0) + (fCos1 * fTmp1));
				}

				matS.x(mat[0][0]);
				matS.y(fCos0 * fCos1 * mat[1][1] - fSin1 * (fCos0 * mat[1][2] - fSin0 * mat[2][2]));
				matS.z(fSin0 * fSin1 * mat[1][1] + fCos1 * (fSin0 * mat[1][2] + fCos0 * mat[2][2]));
				break;
			}
		}
		else
		{
			if (bTest2)
			{
				// 2x2 closed form factorization
				FXD::F32 fTmp = ((mat[0][0] * mat[0][0]) + (mat[1][1] * mat[1][1]) - (mat[0][1] * mat[0][1])) / (mat[0][1] * mat[1][1]);
				FXD::F32 fTan0 = (0.5f * (-fTmp + Math::Sqrt(fTmp * fTmp + 4.0f)));
				FXD::F32 fCos0 = Math::InvSqrt(1.0f + fTan0 * fTan0);
				FXD::F32 fSin0 = (fTan0 * fCos0);
				for (FXD::U32 nCol = 0; nCol < 3; nCol++)
				{
					fTmp0 = matL[nCol][0];
					fTmp1 = matL[nCol][1];
					matL[nCol][0] = ((fCos0 * fTmp0) - (fSin0 * fTmp1));
					matL[nCol][1] = ((fSin0 * fTmp0) + (fCos0 * fTmp1));
				}

				FXD::F32 fTan1 = (mat[0][1] - mat[1][1] * fTan0) / mat[0][0];
				FXD::F32 fCos1 = Math::InvSqrt(1.0f + fTan1 * fTan1);
				FXD::F32 fSin1 = (-fTan1 * fCos1);
				for (FXD::U32 nRow = 0; nRow < 3; nRow++)
				{
					fTmp0 = matR[0][nRow];
					fTmp1 = matR[1][nRow];
					matR[0][nRow] = ((fCos1 * fTmp0) - (fSin1 * fTmp1));
					matR[1][nRow] = ((fSin1 * fTmp0) + (fCos1 * fTmp1));
				}

				matS.x((fCos0 * fCos1 * mat[0][0] - fSin1 * (fCos0 * mat[0][1] - fSin0 * mat[1][1])));
				matS.y((fSin0 * fSin1 * mat[0][0] + fCos1 * (fSin0 * mat[0][1] + fCos0 * mat[1][1])));
				matS.z(mat[2][2]);
				break;
			}
			else
			{
				golubKahanStep(mat, matL, matR);
			}
		}
	}

	// Positize diagonal
	for (FXD::U32 nRow = 0; nRow < 3; nRow++)
	{
		if (matS[nRow] < 0.0f)
		{
			matS.index(nRow, -matS[nRow]);
			for (FXD::U32 nCol = 0; nCol < 3; nCol++)
			{
				matR[nRow][nCol] = -matR[nRow][nCol];
			}
		}
	}
}

void Matrix3::QDUDecomposition(Matrix3& matQ, Vector3F& vecD, Vector3F& vecU) const
{
	// Build orthogonal matrix Q
	FXD::F32 fInvLength = Math::InvSqrt((m_matrix[0][0] * m_matrix[0][0]) + (m_matrix[1][0] * m_matrix[1][0]) + (m_matrix[2][0] * m_matrix[2][0]));
	matQ[0][0] = (m_matrix[0][0] * fInvLength);
	matQ[1][0] = (m_matrix[1][0] * fInvLength);
	matQ[2][0] = (m_matrix[2][0] * fInvLength);

	FXD::F32 fDot = (matQ[0][0] * m_matrix[0][1]) + (matQ[1][0] * m_matrix[1][1]) + (matQ[2][0] * m_matrix[2][1]);
	matQ[0][1] = (m_matrix[0][1] - (fDot * matQ[0][0]));
	matQ[1][1] = (m_matrix[1][1] - (fDot * matQ[1][0]));
	matQ[2][1] = (m_matrix[2][1] - (fDot * matQ[2][0]));

	fInvLength = Math::InvSqrt((matQ[0][1] * matQ[0][1]) + (matQ[1][1] * matQ[1][1]) + (matQ[2][1] * matQ[2][1]));
	matQ[0][1] *= fInvLength;
	matQ[1][1] *= fInvLength;
	matQ[2][1] *= fInvLength;

	fDot = ((matQ[0][0] * m_matrix[0][2]) + (matQ[1][0] * m_matrix[1][2]) + (matQ[2][0] * m_matrix[2][2]));
	matQ[0][2] = (m_matrix[0][2] - (fDot * matQ[0][0]));
	matQ[1][2] = (m_matrix[1][2] - (fDot * matQ[1][0]));
	matQ[2][2] = (m_matrix[2][2] - (fDot * matQ[2][0]));

	fDot = ((matQ[0][1] * m_matrix[0][2]) + (matQ[1][1] * m_matrix[1][2]) + (matQ[2][1] * m_matrix[2][2]));
	matQ[0][2] -= (fDot * matQ[0][1]);
	matQ[1][2] -= (fDot * matQ[1][1]);
	matQ[2][2] -= (fDot * matQ[2][1]);

	fInvLength = Math::InvSqrt((matQ[0][2] * matQ[0][2]) + (matQ[1][2] * matQ[1][2]) + (matQ[2][2] * matQ[2][2]));
	matQ[0][2] *= fInvLength;
	matQ[1][2] *= fInvLength;
	matQ[2][2] *= fInvLength;

	// Guarantee that orthogonal matrix has determinant 1 (no reflections)
	FXD::F32 fDet =
		matQ[0][0] * matQ[1][1] * matQ[2][2] + matQ[0][1] * matQ[1][2] * matQ[2][0] +
		matQ[0][2] * matQ[1][0] * matQ[2][1] - matQ[0][2] * matQ[1][1] * matQ[2][0] -
		matQ[0][1] * matQ[1][0] * matQ[2][2] - matQ[0][0] * matQ[1][2] * matQ[2][1];

	if (fDet < 0.0f)
	{
		for (FXD::U32 nRow = 0; nRow < 3; nRow++)
		{
			for (FXD::U32 nCol = 0; nCol < 3; nCol++)
			{
				matQ[nRow][nCol] = -matQ[nRow][nCol];
			}
		}
	}

	// Build "right" matrix R
	Matrix3 matRight;
	matRight[0][0] = (matQ[0][0] * m_matrix[0][0]) + (matQ[1][0] * m_matrix[1][0]) + (matQ[2][0] * m_matrix[2][0]);
	matRight[0][1] = (matQ[0][0] * m_matrix[0][1]) + (matQ[1][0] * m_matrix[1][1]) + (matQ[2][0] * m_matrix[2][1]);
	matRight[1][1] = (matQ[0][1] * m_matrix[0][1]) + (matQ[1][1] * m_matrix[1][1]) + (matQ[2][1] * m_matrix[2][1]);
	matRight[0][2] = (matQ[0][0] * m_matrix[0][2]) + (matQ[1][0] * m_matrix[1][2]) + (matQ[2][0] * m_matrix[2][2]);
	matRight[1][2] = (matQ[0][1] * m_matrix[0][2]) + (matQ[1][1] * m_matrix[1][2]) + (matQ[2][1] * m_matrix[2][2]);
	matRight[2][2] = (matQ[0][2] * m_matrix[0][2]) + (matQ[1][2] * m_matrix[1][2]) + (matQ[2][2] * m_matrix[2][2]);

	// The scaling component
	vecD.x(matRight[0][0]);
	vecD.y(matRight[1][1]);
	vecD.z(matRight[2][2]);

	// The shear component
	FXD::F32 fInvD0 = (1.0f / vecD[0]);
	vecU.x((matRight[0][1] * fInvD0));
	vecU.y((matRight[0][2] * fInvD0));
	vecU.z((matRight[1][2] / vecD[1]));
}

void Matrix3::orthonormalize(void)
{
	// Compute q0
	FXD::F32 fInvLength = Math::InvSqrt((m_matrix[0][0] * m_matrix[0][0]) + (m_matrix[1][0] * m_matrix[1][0]) + (m_matrix[2][0] * m_matrix[2][0]));

	m_matrix[0][0] *= fInvLength;
	m_matrix[1][0] *= fInvLength;
	m_matrix[2][0] *= fInvLength;

	// Compute q1
	FXD::F32 fDot0 = (m_matrix[0][0] * m_matrix[0][1]) + (m_matrix[1][0] * m_matrix[1][1]) + (m_matrix[2][0] * m_matrix[2][1]);

	m_matrix[0][1] -= (fDot0 * m_matrix[0][0]);
	m_matrix[1][1] -= (fDot0 * m_matrix[1][0]);
	m_matrix[2][1] -= (fDot0 * m_matrix[2][0]);

	fInvLength = Math::InvSqrt(m_matrix[0][1] * m_matrix[0][1] + m_matrix[1][1] * m_matrix[1][1] + m_matrix[2][1] * m_matrix[2][1]);

	m_matrix[0][1] *= fInvLength;
	m_matrix[1][1] *= fInvLength;
	m_matrix[2][1] *= fInvLength;

	// Compute q2
	FXD::F32 fDot1 = ((m_matrix[0][1] * m_matrix[0][2]) + (m_matrix[1][1] * m_matrix[1][2]) + (m_matrix[2][1] * m_matrix[2][2]));
	fDot0 = ((m_matrix[0][0] * m_matrix[0][2]) + (m_matrix[1][0] * m_matrix[1][2]) + (m_matrix[2][0] * m_matrix[2][2]));

	m_matrix[0][2] -= ((fDot0 * m_matrix[0][0]) + (fDot1 * m_matrix[0][1]));
	m_matrix[1][2] -= ((fDot0 * m_matrix[1][0]) + (fDot1 * m_matrix[1][1]));
	m_matrix[2][2] -= ((fDot0 * m_matrix[2][0]) + (fDot1 * m_matrix[2][1]));

	fInvLength = Math::InvSqrt((m_matrix[0][2] * m_matrix[0][2]) + (m_matrix[1][2] * m_matrix[1][2]) + (m_matrix[2][2] * m_matrix[2][2]));

	m_matrix[0][2] *= fInvLength;
	m_matrix[1][2] *= fInvLength;
	m_matrix[2][2] *= fInvLength;
}

void Matrix3::toAxisAngle(Vector3F& axis, Radian& radians) const
{
	FXD::F32 fTrace = m_matrix[0][0] + m_matrix[1][1] + m_matrix[2][2];
	FXD::F32 fCos = (0.5f * (fTrace - 1.0f));
	radians = Math::FuncsMath::AcosFRad(fCos); // In [0, PI]

	if (radians > Radian(0.0f))
	{
		if (radians < Radian(Math::PI))
		{
			axis.x(m_matrix[2][1] - m_matrix[1][2]);
			axis.y(m_matrix[0][2] - m_matrix[2][0]);
			axis.z(m_matrix[1][0] - m_matrix[0][1]);
			axis.normalise();
		}
		else
		{
			// Angle is PI
			FXD::F32 fHalfInverse;
			if (m_matrix[0][0] >= m_matrix[1][1])
			{
				// r00 >= r11
				if (m_matrix[0][0] >= m_matrix[2][2])
				{
					// r00 is maximum diagonal term
					axis.x(0.5f * Math::Sqrt(m_matrix[0][0] - m_matrix[1][1] - m_matrix[2][2] + 1.0f));
					fHalfInverse = (0.5f / axis.x());
					axis.y(fHalfInverse * m_matrix[0][1]);
					axis.z(fHalfInverse * m_matrix[0][2]);
				}
				else
				{
					// r22 is maximum diagonal term
					axis.z(0.5f * Math::Sqrt(m_matrix[2][2] - m_matrix[0][0] - m_matrix[1][1] + 1.0f));
					fHalfInverse = (0.5f / axis.z());
					axis.x(fHalfInverse * m_matrix[0][2]);
					axis.y(fHalfInverse * m_matrix[1][2]);
				}
			}
			else
			{
				// r11 > r00
				if (m_matrix[1][1] >= m_matrix[2][2])
				{
					// r11 is maximum diagonal term
					axis.y(0.5f * Math::Sqrt(m_matrix[1][1] - m_matrix[0][0] - m_matrix[2][2] + 1.0f));
					fHalfInverse = 0.5f / axis.y();
					axis.x(fHalfInverse * m_matrix[0][1]);
					axis.z(fHalfInverse * m_matrix[1][2]);
				}
				else
				{
					// r22 is maximum diagonal term
					axis.z(0.5f * Math::Sqrt(m_matrix[2][2] - m_matrix[0][0] - m_matrix[1][1] + 1.0f));
					fHalfInverse = (0.5f / axis.z());
					axis.x(fHalfInverse * m_matrix[0][2]);
					axis.y(fHalfInverse * m_matrix[1][2]);
				}
			}
		}
	}
	else
	{
		// The angle is 0 and the matrix is the identity. Any axis will work, so just use the x-axis.
		axis.x(1.0f);
		axis.y(0.0f);
		axis.z(0.0f);
	}
}

void Matrix3::toQuaternion(Quaternion& quat) const
{
	quat.fromRotationMatrix(*this);
}

bool Matrix3::toEulerAngles(Radian& xAngle, Radian& yAngle, Radian& zAngle) const
{
	xAngle = -Radian(Math::FuncsMath::AsinFRad(m_matrix[1][2]));
	if (xAngle < Radian(Math::PI_HALF))
	{
		if (xAngle > Radian(-Math::PI_HALF))
		{
			yAngle = Math::FuncsMath::Atan2F(m_matrix[0][2], m_matrix[2][2]);
			zAngle = Math::FuncsMath::Atan2F(m_matrix[1][0], m_matrix[1][1]);
			return true;
		}
		else
		{
			// Note: Not an unique solution.
			xAngle = Radian(-Math::PI_HALF);
			yAngle = Math::FuncsMath::Atan2F(-m_matrix[0][1], m_matrix[0][0]);
			zAngle = Radian(0.0f);
			return false;
		}
	}
	else
	{
		// Note: Not an unique solution.
		xAngle = Radian(Math::PI_HALF);
		yAngle = Math::FuncsMath::Atan2F(m_matrix[0][1], m_matrix[0][0]);
		zAngle = Radian(0.0f);
		return false;
	}
}

void Matrix3::fromAxes(const Vector3F& xAxis, const Vector3F& yAxis, const Vector3F& zAxis)
{
	set_column(0, xAxis);
	set_column(1, yAxis);
	set_column(2, zAxis);
}

void Matrix3::fromAxisAngle(const Vector3F& axis, const Degree& angle)
{
	fromAxisAngle(axis, Radian(angle));
}

void Matrix3::fromAxisAngle(const Vector3F& axis, const Radian& angle)
{
	FXD::F32 fCos = Math::FuncsMath::CosRad(angle);
	FXD::F32 fSin = Math::FuncsMath::SinRad(angle);
	FXD::F32 fOneMinusCos = (1.0f - fCos);
	FXD::F32 fXSqrd = (axis.x() * axis.x());
	FXD::F32 fYSqrd = (axis.y() * axis.y());
	FXD::F32 fZSqrd = (axis.z() * axis.z());
	FXD::F32 fXYM = (axis.x() * axis.y() * fOneMinusCos);
	FXD::F32 fXZM = (axis.x() * axis.z() * fOneMinusCos);
	FXD::F32 fYZM = (axis.y() * axis.z() * fOneMinusCos);
	FXD::F32 fXSin = (axis.x() * fSin);
	FXD::F32 fYSin = (axis.y() * fSin);
	FXD::F32 fZSin = (axis.z() * fSin);

	m_matrix[0][0] = (fXSqrd * fOneMinusCos + fCos);
	m_matrix[0][1] = (fXYM - fZSin);
	m_matrix[0][2] = (fXZM + fYSin);
	m_matrix[1][0] = (fXYM + fZSin);
	m_matrix[1][1] = (fYSqrd * fOneMinusCos + fCos);
	m_matrix[1][2] = (fYZM - fXSin);
	m_matrix[2][0] = (fXZM - fYSin);
	m_matrix[2][1] = (fYZM + fXSin);
	m_matrix[2][2] = (fZSqrd * fOneMinusCos + fCos);
}

void Matrix3::fromQuaternion(const Quaternion& quat)
{
	quat.toRotationMatrix(*this);
}

void Matrix3::fromEulerAngles(const Degree& xAngle, const Degree& yAngle, const Degree& zAngle)
{
	fromEulerAngles(Radian(xAngle), Radian(yAngle), Radian(zAngle));
}

void Matrix3::fromEulerAngles(const Radian& xAngle, const Radian& yAngle, const Radian& zAngle)
{
	FXD::F32 fCosX = Math::FuncsMath::CosRad(xAngle);
	FXD::F32 fSinX = Math::FuncsMath::SinRad(xAngle);

	FXD::F32 fCosY = Math::FuncsMath::CosRad(yAngle);
	FXD::F32 fSinY = Math::FuncsMath::SinRad(yAngle);

	FXD::F32 fCosZ = Math::FuncsMath::CosRad(zAngle);
	FXD::F32 fSinZ = Math::FuncsMath::SinRad(zAngle);

	m_matrix[0][0] = (fCosY * fCosZ + fSinX * fSinY * fSinZ);
	m_matrix[0][1] = (fCosZ * fSinX * fSinY - fCosY * fSinZ);
	m_matrix[0][2] = (fCosX * fSinY);

	m_matrix[1][0] = (fCosX * fSinZ);
	m_matrix[1][1] = (fCosX * fCosZ);
	m_matrix[1][2] = (-fSinX);

	m_matrix[2][0] = (-fCosZ * fSinY + fCosY * fSinX * fSinZ);
	m_matrix[2][1] = (fCosY * fCosZ * fSinX + fSinY * fSinZ);
	m_matrix[2][2] = (fCosX * fCosY);
}

void Matrix3::eigenSolveSymmetric(FXD::F32 eigenValues[3], Vector3F eigenVectors[3]) const
{
	Matrix3 mat = (*this);
	FXD::F32 subDiag[3];
	mat.tridiagonal(eigenValues, subDiag);
	mat.QLAlgorithm(eigenValues, subDiag);

	for (FXD::U32 n1 = 0; n1 < 3; n1++)
	{
		eigenVectors[n1].x(mat[0][n1]);
		eigenVectors[n1].y(mat[1][n1]);
		eigenVectors[n1].z(mat[2][n1]);
	}

	// Make eigenvectors form a right--handed system
	Vector3F cross = eigenVectors[1].cross(eigenVectors[2]);
	FXD::F32 fDet = eigenVectors[0].dot(cross);
	if (fDet < 0.0f)
	{
		eigenVectors[2].x(-eigenVectors[2][0]);
		eigenVectors[2].y(-eigenVectors[2][1]);
		eigenVectors[2].z(-eigenVectors[2][2]);
	}
}

void Matrix3::tridiagonal(FXD::F32 diag[3], FXD::F32 subDiag[3])
{
	//	Householder reduction T = Q^t M Q
	//		Input:
	//		mat, symmetric 3x3 matrix M
	//	Output:
	//		mat, orthogonal matrix Q
	//		diag, diagonal entries of T
	//		subd, subdiagonal entries of T (T is symmetric)

	FXD::F32 fA = m_matrix[0][0];
	FXD::F32 fB = m_matrix[0][1];
	FXD::F32 fC = m_matrix[0][2];
	FXD::F32 fD = m_matrix[1][1];
	FXD::F32 fE = m_matrix[1][2];
	FXD::F32 fF = m_matrix[2][2];

	diag[0] = fA;
	subDiag[2] = 0.0f;
	if (Math::Abs(fC) >= kEpsilon)
	{
		FXD::F32 fLength = Math::Sqrt((fB * fB) + (fC * fC));
		FXD::F32 fInvLength = (1.0f / fLength);
		fB *= fInvLength;
		fC *= fInvLength;
		FXD::F32 fQ = (2.0f * fB * fE + fC * (fF - fD));
		diag[1] = (fD + fC * fQ);
		diag[2] = (fF - fC * fQ);
		subDiag[0] = fLength;
		subDiag[1] = (fE - fB * fQ);
		m_matrix[0][0] = 1.0f;
		m_matrix[0][1] = 0.0f;
		m_matrix[0][2] = 0.0f;
		m_matrix[1][0] = 0.0f;
		m_matrix[1][1] = fB;
		m_matrix[1][2] = fC;
		m_matrix[2][0] = 0.0f;
		m_matrix[2][1] = fC;
		m_matrix[2][2] = -fB;
	}
	else
	{
		diag[1] = fD;
		diag[2] = fF;
		subDiag[0] = fB;
		subDiag[1] = fE;
		m_matrix[0][0] = 1.0f;
		m_matrix[0][1] = 0.0f;
		m_matrix[0][2] = 0.0f;
		m_matrix[1][0] = 0.0f;
		m_matrix[1][1] = 1.0f;
		m_matrix[1][2] = 0.0f;
		m_matrix[2][0] = 0.0f;
		m_matrix[2][1] = 0.0f;
		m_matrix[2][2] = 1.0f;
	}
}

bool Matrix3::QLAlgorithm(FXD::F32 diag[3], FXD::F32 subDiag[3])
{
	// QL iteration with implicit shifting to reduce matrix from tridiagonal to diagonal
	for (FXD::S32 n1 = 0; n1 < 3; n1++)
	{
		const FXD::U32 nMaxIter = 32;
		FXD::U32 nIter;
		for (nIter = 0; nIter < nMaxIter; nIter++)
		{
			FXD::S32 n2;
			for (n2 = n1; n2 <= 1; n2++)
			{
				FXD::F32 sum = Math::Abs(diag[n2]) + Math::Abs(diag[n2 + 1]);
				if (Math::Abs(subDiag[n2]) + sum == sum)
				{
					break;
				}
			}

			if (n2 == n1)
			{
				break;
			}

			FXD::F32 fTmp0 = (diag[n1 + 1] - diag[n1]) / (2.0f * subDiag[n1]);
			FXD::F32 fTmp1 = Math::Sqrt(fTmp0 * fTmp0 + 1.0f);
			if (fTmp0 < 0.0f)
			{
				fTmp0 = diag[n2] - diag[n1] + subDiag[n1] / (fTmp0 - fTmp1);
			}
			else
			{
				fTmp0 = diag[n2] - diag[n1] + subDiag[n1] / (fTmp0 + fTmp1);
			}

			FXD::F32 fSin = 1.0f;
			FXD::F32 fCos = 1.0f;
			FXD::F32 fTmp2 = 0.0f;
			for (FXD::S32 n3 = n2 - 1; n3 >= n1; n3--)
			{
				FXD::F32 fTmp3 = (fSin * subDiag[n3]);
				FXD::F32 fTmp4 = (fCos * subDiag[n3]);
				if (Math::Abs(fTmp3) >= Math::Abs(fTmp0))
				{
					fCos = (fTmp0 / fTmp3);
					fTmp1 = Math::Sqrt(fCos * fCos + 1.0f);
					subDiag[n3 + 1] = (fTmp3 * fTmp1);
					fSin = (1.0f / fTmp1);
					fCos *= fSin;
				}
				else
				{
					fSin = (fTmp3 / fTmp0);
					fTmp1 = Math::Sqrt(fSin * fSin + 1.0f);
					subDiag[n3 + 1] = (fTmp0 * fTmp1);
					fCos = (1.0f / fTmp1);
					fSin *= fCos;
				}

				fTmp0 = (diag[n3 + 1] - fTmp2);
				fTmp1 = ((diag[n3] - fTmp0) * fSin + 2.0f * fTmp4 * fCos);
				fTmp2 = (fSin * fTmp1);
				diag[n3 + 1] = (fTmp0 + fTmp2);
				fTmp0 = (fCos * fTmp1 - fTmp4);

				for (FXD::S32 nRow = 0; nRow < 3; nRow++)
				{
					fTmp3 = m_matrix[nRow][n3 + 1];
					m_matrix[nRow][n3 + 1] = (fSin * m_matrix[nRow][n3] + fCos * fTmp3);
					m_matrix[nRow][n3] = (fCos * m_matrix[nRow][n3] - fSin * fTmp3);
				}
			}
			diag[n1] -= fTmp2;
			subDiag[n1] = fTmp0;
			subDiag[n2] = 0.0f;
		}

		if (nIter == nMaxIter)
		{
			// Should not get here under normal circumstances
			return false;
		}
	}
	return true;
}

void Matrix3::bidiagonalize(Matrix3& matA, Matrix3& matL, Matrix3& matR)
{
	FXD::F32 v[3], w[3];
	bool bIdentity = false;

	// Map first column to (*,0,0)
	FXD::F32 fLength = Math::Sqrt((matA[0][0] * matA[0][0]) + (matA[1][0] * matA[1][0]) + (matA[2][0] * matA[2][0]));
	if (fLength > 0.0f)
	{
		FXD::F32 fSign = (matA[0][0] > 0.0f ? 1.0f : -1.0f);
		FXD::F32 t1 = (matA[0][0] + fSign * fLength);
		FXD::F32 fInvT1 = (1.0f / t1);
		v[1] = (matA[1][0] * fInvT1);
		v[2] = (matA[2][0] * fInvT1);

		FXD::F32 t2 = (-2.0f / (1.0f + v[1] * v[1] + v[2] * v[2]));
		w[0] = (t2 * (matA[0][0] + matA[1][0] * v[1] + matA[2][0] * v[2]));
		w[1] = (t2 * (matA[0][1] + matA[1][1] * v[1] + matA[2][1] * v[2]));
		w[2] = (t2 * (matA[0][2] + matA[1][2] * v[1] + matA[2][2] * v[2]));
		matA[0][0] += w[0];
		matA[0][1] += w[1];
		matA[0][2] += w[2];
		matA[1][1] += (v[1] * w[1]);
		matA[1][2] += (v[1] * w[2]);
		matA[2][1] += (v[2] * w[1]);
		matA[2][2] += (v[2] * w[2]);

		matL[0][0] = (1.0f + t2);
		matL[0][1] = (matL[1][0] = (t2 * v[1]));
		matL[0][2] = (matL[2][0] = (t2 * v[2]));
		matL[1][1] = (1.0f + t2 * v[1] * v[1]);
		matL[1][2] = (matL[2][1] = (t2 * v[1] * v[2]));
		matL[2][2] = (1.0f + t2 * v[2] * v[2]);
		bIdentity = false;
	}
	else
	{
		matL = Matrix3::kIdentity;
		bIdentity = true;
	}

	// Map first nRow to (*,*,0)
	fLength = Math::Sqrt(matA[0][1] * matA[0][1] + matA[0][2] * matA[0][2]);
	if (fLength > 0.0f)
	{
		FXD::F32 fSign = (matA[0][1] > 0.0f ? 1.0f : -1.0f);
		FXD::F32 t1 = matA[0][1] + fSign * fLength;
		v[2] = matA[0][2] / t1;

		FXD::F32 t2 = (-2.0f / (1.0f + v[2] * v[2]));
		w[0] = (t2 * (matA[0][1] + matA[0][2] * v[2]));
		w[1] = (t2 * (matA[1][1] + matA[1][2] * v[2]));
		w[2] = (t2 * (matA[2][1] + matA[2][2] * v[2]));
		matA[0][1] += w[0];
		matA[1][1] += w[1];
		matA[1][2] += (w[1] * v[2]);
		matA[2][1] += w[2];
		matA[2][2] += (w[2] * v[2]);

		matR[0][0] = 1.0f;
		matR[0][1] = matR[1][0] = 0.0f;
		matR[0][2] = matR[2][0] = 0.0f;
		matR[1][1] = 1.0f + t2;
		matR[1][2] = (matR[2][1] = (t2 * v[2]));
		matR[2][2] = (1.0f + t2 * v[2] * v[2]);
	}
	else
	{
		matR = Matrix3::kIdentity;
	}

	// Map second column to (*,*,0)
	fLength = Math::Sqrt(matA[1][1] * matA[1][1] + matA[2][1] * matA[2][1]);
	if (fLength > 0.0)
	{
		FXD::F32 fSign = (matA[1][1] > 0.0f ? 1.0f : -1.0f);
		FXD::F32 t1 = (matA[1][1] + fSign * fLength);
		v[2] = (matA[2][1] / t1);

		FXD::F32 t2 = (-2.0f / (1.0f + v[2] * v[2]));
		w[1] = (t2 * (matA[1][1] + matA[2][1] * v[2]));
		w[2] = (t2 * (matA[1][2] + matA[2][2] * v[2]));
		matA[1][1] += w[1];
		matA[1][2] += w[2];
		matA[2][2] += (v[2] * w[2]);

		FXD::F32 a = (1.0f + t2);
		FXD::F32 b = (t2 * v[2]);
		FXD::F32 c = (1.0f + b * v[2]);

		if (bIdentity)
		{
			matL[0][0] = 1.0f;
			matL[0][1] = matL[1][0] = 0.0f;
			matL[0][2] = matL[2][0] = 0.0f;
			matL[1][1] = a;
			matL[1][2] = matL[2][1] = b;
			matL[2][2] = c;
		}
		else
		{
			for (FXD::S32 nRow = 0; nRow < 3; nRow++)
			{
				FXD::F32 fTmp0 = matL[nRow][1];
				FXD::F32 fTmp1 = matL[nRow][2];
				matL[nRow][1] = ((a * fTmp0) + (b * fTmp1));
				matL[nRow][2] = ((b * fTmp0) + (c * fTmp1));
			}
		}
	}
}

void Matrix3::golubKahanStep(Matrix3& matA, Matrix3& matL, Matrix3& matR)
{
	FXD::F32 f11 = ((matA[0][1] * matA[0][1]) + (matA[1][1] * matA[1][1]));
	FXD::F32 t22 = ((matA[1][2] * matA[1][2]) + (matA[2][2] * matA[2][2]));
	FXD::F32 t12 = (matA[1][1] * matA[1][2]);
	FXD::F32 fTrace = (f11 + t22);
	FXD::F32 fDiff = (f11 - t22);
	FXD::F32 fDiscr = Math::Sqrt(fDiff * fDiff + 4.0f * t12 * t12);
	FXD::F32 fRoot1 = (0.5f * (fTrace + fDiscr));
	FXD::F32 fRoot2 = (0.5f * (fTrace - fDiscr));

	// Adjust right
	FXD::F32 y = matA[0][0] - (Math::Abs(fRoot1 - t22) <= Math::Abs(fRoot2 - t22) ? fRoot1 : fRoot2);
	FXD::F32 z = matA[0][1];
	FXD::F32 fInvLength = Math::InvSqrt(y * y + z * z);
	FXD::F32 fSin = (z * fInvLength);
	FXD::F32 fCos = (-y * fInvLength);

	FXD::F32 fTmp0 = matA[0][0];
	FXD::F32 fTmp1 = matA[0][1];
	matA[0][0] = ((fCos * fTmp0) - (fSin * fTmp1));
	matA[0][1] = ((fSin * fTmp0) + (fCos * fTmp1));
	matA[1][0] = (-fSin * matA[1][1]);
	matA[1][1] *= fCos;

	FXD::U32 nRow;
	for (nRow = 0; nRow < 3; nRow++)
	{
		fTmp0 = matR[0][nRow];
		fTmp1 = matR[1][nRow];
		matR[0][nRow] = ((fCos * fTmp0) - (fSin * fTmp1));
		matR[1][nRow] = ((fSin * fTmp0) + (fCos * fTmp1));
	}

	// Adjust left
	y = matA[0][0];
	z = matA[1][0];
	fInvLength = Math::InvSqrt(y * y + z * z);
	fSin = (z * fInvLength);
	fCos = (-y * fInvLength);

	matA[0][0] = ((fCos * matA[0][0]) - (fSin * matA[1][0]));
	fTmp0 = matA[0][1];
	fTmp1 = matA[1][1];
	matA[0][1] = ((fCos * fTmp0) - (fSin * fTmp1));
	matA[1][1] = ((fSin * fTmp0) + (fCos * fTmp1));
	matA[0][2] = (-fSin * matA[1][2]);
	matA[1][2] *= fCos;

	FXD::U32 nCol;
	for (nCol = 0; nCol < 3; nCol++)
	{
		fTmp0 = matL[nCol][0];
		fTmp1 = matL[nCol][1];
		matL[nCol][0] = ((fCos * fTmp0) - (fSin * fTmp1));
		matL[nCol][1] = ((fSin * fTmp0) + (fCos * fTmp1));
	}

	// Adjust right
	y = matA[0][1];
	z = matA[0][2];
	fInvLength = Math::InvSqrt(y * y + z * z);
	fSin = (z * fInvLength);
	fCos = (-y * fInvLength);

	matA[0][1] = ((fCos * matA[0][1]) - (fSin * matA[0][2]));
	fTmp0 = matA[1][1];
	fTmp1 = matA[1][2];
	matA[1][1] = ((fCos * fTmp0) - (fSin * fTmp1));
	matA[1][2] = ((fSin * fTmp0) + (fCos * fTmp1));
	matA[2][1] = (-fSin * matA[2][2]);
	matA[2][2] *= fCos;

	for (nRow = 0; nRow < 3; nRow++)
	{
		fTmp0 = matR[1][nRow];
		fTmp1 = matR[2][nRow];
		matR[1][nRow] = ((fCos * fTmp0) - (fSin * fTmp1));
		matR[2][nRow] = ((fSin * fTmp0) + (fCos * fTmp1));
	}

	// Adjust left
	y = matA[1][1];
	z = matA[2][1];
	fInvLength = Math::InvSqrt(y * y + z * z);
	fSin = (z * fInvLength);
	fCos = (-y * fInvLength);

	matA[1][1] = ((fCos * matA[1][1]) - (fSin * matA[2][1]));
	fTmp0 = matA[1][2];
	fTmp1 = matA[2][2];
	matA[1][2] = (fCos * fTmp0) - (fSin * fTmp1);
	matA[2][2] = (fSin * fTmp0) + (fCos * fTmp1);

	for (nCol = 0; nCol < 3; nCol++)
	{
		fTmp0 = matL[nCol][1];
		fTmp1 = matL[nCol][2];
		matL[nCol][1] = (fCos * fTmp0) - (fSin * fTmp1);
		matL[nCol][2] = (fSin * fTmp0) + (fCos * fTmp1);
	}
}
*/