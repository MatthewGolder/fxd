// Creator - MatthewGolder
#include "FXDEngine/Math/Vector2F.h"
#include "FXDEngine/Math/Math.h"
#include "FXDEngine/Core/Debugger.h"

using namespace FXD;
using namespace Math;

// ------
// Vector2F
// -
// ------
const Vector2F Vector2F::kZero(0.0f);
const Vector2F Vector2F::kOne(1.0f);
const Vector2F Vector2F::kInf(Math::NumericLimits< FXD::F32 >::infinity());
const Vector2F Vector2F::kNegInf(-Math::NumericLimits< FXD::F32 >::infinity());
const Vector2F Vector2F::kLeft(-1.0f, 0.0f);
const Vector2F Vector2F::kRight(1.0f, 0.0f);
const Vector2F Vector2F::kUp(0.0f, 1.0f);
const Vector2F Vector2F::kDown(0.0f, -1.0f);
const Vector2F Vector2F::kUnitX(1.0f, 0.0f);
const Vector2F Vector2F::kUnitY(0.0f, 1.0f);
const Vector2F Vector2F::kAnchorMiddle(0.5f, 0.5f);
const Vector2F Vector2F::kAnchorBottomLeft(0.0f, 0.0f);
const Vector2F Vector2F::kAnchorTopLeft(0.0f, 1.0f);
const Vector2F Vector2F::kAnchorBottomRight(1.0f, 0.0f);
const Vector2F Vector2F::kAnchorTopRight(1.0f, 1.0f);
const Vector2F Vector2F::kAnchorMiddleRight(1.0f, 0.5f);
const Vector2F Vector2F::kAnchorMiddleLeft(0.0f, 0.5f);
const Vector2F Vector2F::kAnchorMiddleTop(0.5f, 1.0f);
const Vector2F Vector2F::kAnchorMiddleBottom(0.5f, 0.0f);

Vector2F::Vector2F(void)
{
	Vec2FSet(m_data, 0.0f);
}
Vector2F::Vector2F(FXD::F32 val)
{
	Vec2FSet(m_data, val);
}
Vector2F::Vector2F(FXD::F32 x, FXD::F32 y)
{
	Vec2FSet(m_data, x, y);
}
Vector2F::~Vector2F(void)
{
}

FXD::F32 Vector2F::operator[](FXD::U32 nID) const
{
	PRINT_COND_ASSERT((nID < 2), "Math: Index out of bounds");
	return Vec2FGetIndex(m_data, nID);
}

FXD::F32 Vector2F::operator()(FXD::U32 nID) const
{
	PRINT_COND_ASSERT((nID < 2), "Math: Index out of bounds");
	return Vec2FGetIndex(m_data, nID);
}

Vector2F& Vector2F::operator=(const Vector2F& rhs)
{
	Vec2FCopy(m_data, rhs.m_data);
	return (*this);
}
Vector2F& Vector2F::operator=(FXD::F32 rhs)
{
	Vec2FSet(m_data, rhs);
	return (*this);
}

Vector2F Vector2F::operator-() const
{
	Vector2F out = (*this);
	out.negate();
	return out;
}

FXD::F32 Vector2F::x(void) const
{
	return Vec2FGetX(m_data);
}
FXD::F32 Vector2F::y(void) const
{
	return Vec2FGetY(m_data);
}
FXD::F32 Vector2F::index(FXD::U32 nID)
{
	PRINT_COND_ASSERT((nID < 2), "Math: Index out of bounds");
	return Vec2FGetIndex(m_data, nID);
}

void Vector2F::x(FXD::F32 x)
{
	Vec2FSetX(m_data, x);
}
void Vector2F::y(FXD::F32 y)
{
	Vec2FSetY(m_data, y);
}
void Vector2F::index(FXD::U32 nID, FXD::F32 fVal)
{
	PRINT_COND_ASSERT((nID < 2), "Math: Index out of bounds");
	Vec2FSetIndex(m_data, nID, fVal);
}

FXD::F32 Vector2F::dot(const Vector2F& rhs) const
{
	return Vec2FDot(m_data, rhs.m_data);
}
FXD::F32 Vector2F::cross(const Vector2F& rhs) const
{
	return Vec2FCross(m_data, rhs.m_data);
}
FXD::F32 Vector2F::length(void) const
{
	return Math::Sqrt(squared_length());
}
FXD::F32 Vector2F::squared_length(void) const
{
	return Vec2FSquaredLength(m_data);
}
FXD::F32 Vector2F::distance(const Vector2F& rhs) const
{
	return Math::Sqrt(sqrd_distance(rhs));
}
FXD::F32 Vector2F::sqrd_distance(const Vector2F& rhs) const
{
	return Vec2FSquaredDistance(m_data, rhs.m_data);
}
Vector2F Vector2F::midpoint(const Vector2F& rhs) const
{
	Vector2F retVec;
	Vec2FMidPoint(retVec.m_data, m_data, rhs.m_data);
	return retVec;
}
Vector2F Vector2F::perpendicular(void) const
{
	return Vector2F(-y(), x());
}

void Vector2F::negate(void)
{
	Vec2FNegate(m_data);
}
void Vector2F::minimum(const Vector2F& min)
{
	Vec2FMinimum(m_data, min.m_data);
}
void Vector2F::maximum(const Vector2F& max)
{
	Vec2FMaximum(m_data, max.m_data);
}
void Vector2F::grid_snap(const FXD::F32 fGridSize)
{
	Vec2FSet(m_data, Math::GridSnap(x(), fGridSize), Math::GridSnap(y(), fGridSize));
}
void Vector2F::clamp(const Vector2F& min, const Vector2F& max)
{
	Vec2FClamp(m_data, min.m_data, max.m_data);
}
void Vector2F::normalise(void)
{
	Vec2FNormalise(m_data);
}
void Vector2F::abs(void)
{
	Vec2FAbs(m_data);
}
void Vector2F::ceil(void)
{
	Vec2FCeil(m_data);
}
void Vector2F::floor(void)
{
	Vec2FFloor(m_data);
}

Vector2F Vector2F::get_negate(void) const
{
	Vector2F retVec = (*this);
	retVec.negate();
	return retVec;
}
Vector2F Vector2F::get_minimum(const Vector2F& min) const
{
	Vector2F retVec = (*this);
	retVec.minimum(min);
	return retVec;
}
Vector2F Vector2F::get_maximum(const Vector2F& max) const
{
	Vector2F retVec = (*this);
	retVec.maximum(max);
	return retVec;
}
Vector2F Vector2F::get_grid_snap(const FXD::F32 fGridSize)
{
	Vector2F retVec = (*this);
	retVec.grid_snap(fGridSize);
	return retVec;
}
Vector2F Vector2F::get_clamp(const Vector2F& min, const Vector2F& max) const
{
	Vector2F retVec = (*this);
	retVec.clamp(min, max);
	return retVec;
}
Vector2F Vector2F::get_normalise(void) const
{
	Vector2F retVec = (*this);
	retVec.normalise();
	return retVec;
}
Vector2F Vector2F::get_abs(void) const
{
	Vector2F retVec = (*this);
	retVec.abs();
	return retVec;
}
Vector2F Vector2F::get_ceil(void) const
{
	Vector2F retVec = (*this);
	retVec.ceil();
	return retVec;
}
Vector2F Vector2F::get_floor(void) const
{
	Vector2F retVec = (*this);
	retVec.floor();
	return retVec;
}

// STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS STATICS 
FXD::F32 Vector2F::dot(const Vector2F& lhs, const Vector2F& rhs)
{
	return lhs.dot(rhs);
}
FXD::F32 Vector2F::cross(const Vector2F& lhs, const Vector2F& rhs)
{
	return lhs.dot(lhs, rhs);
}
FXD::F32 Vector2F::distance(const Vector2F& lhs, const Vector2F& rhs)
{
	return lhs.distance(rhs);
}
FXD::F32 Vector2F::sqrd_distance(const Vector2F& lhs, const Vector2F& rhs)
{
	return lhs.sqrd_distance(rhs);
}
Vector2F Vector2F::midpoint(const Vector2F& lhs, const Vector2F& rhs)
{
	return lhs.midpoint(rhs);
}

void Vector2F::set_negate(Vector2F& vec)
{
	vec.negate();
}
void Vector2F::set_minimum(Vector2F& vec, const Vector2F& min)
{
	vec.minimum(min);
}
void Vector2F::set_maximum(Vector2F& vec, const Vector2F& max)
{
	vec.maximum(max);
}
void Vector2F::set_grid_snap(Vector2F& vec, const FXD::F32 fGridSize)
{
	vec.grid_snap(fGridSize);
}
void Vector2F::set_clamp(Vector2F& vec, const Vector2F& min, const Vector2F& max)
{
	vec.clamp(min, max);
}
void Vector2F::set_normalise(Vector2F& vec)
{
	vec.normalise();
}
void Vector2F::set_abs(Vector2F& vec)
{
	vec.abs();
}
void Vector2F::set_ceil(Vector2F& vec)
{
	vec.ceil();
}
void Vector2F::set_floor(Vector2F& vec)
{
	vec.floor();
}
void Vector2F::set_lerp(Vector2F& vec, const Vector2F& lhs, const Vector2F& rhs, FXD::F32 fVal)
{
	vec = lhs + (rhs - lhs) * fVal;
}

Vector2F Vector2F::get_negate(const Vector2F& vec)
{
	Vector2F retVec = vec;
	Vector2F::set_negate(retVec);
	return retVec;
}
Vector2F Vector2F::get_minimum(const Vector2F& vec, const Vector2F& min)
{
	Vector2F retVec = vec;
	Vector2F::set_minimum(retVec, min);
	return retVec;
}
Vector2F Vector2F::get_maximum(const Vector2F& vec, const Vector2F& max)
{
	Vector2F retVec = vec;
	Vector2F::set_maximum(retVec, max);
	return retVec;
}
Vector2F Vector2F::get_grid_snap(const Vector2F& vec, const FXD::F32 fGridSize)
{
	Vector2F retVec = vec;
	Vector2F::set_grid_snap(retVec, fGridSize);
	return retVec;
}
Vector2F Vector2F::get_clamp(const Vector2F& vec, const Vector2F& min, const Vector2F& max)
{
	Vector2F retVec = vec;
	Vector2F::set_clamp(retVec, min, max);
	return retVec;
}
Vector2F Vector2F::get_normalise(const Vector2F& vec)
{
	Vector2F retVec = vec;
	Vector2F::set_normalise(retVec);
	return retVec;
}
Vector2F Vector2F::get_abs(const Vector2F& vec)
{
	Vector2F retVec = vec;
	Vector2F::set_abs(retVec);
	return retVec;
}
Vector2F Vector2F::get_ceil(const Vector2F& vec)
{
	Vector2F retVec = vec;
	Vector2F::set_ceil(retVec);
	return retVec;
}
Vector2F Vector2F::get_floor(const Vector2F& vec)
{
	Vector2F retVec = vec;
	Vector2F::set_floor(retVec);
	return retVec;
}
Vector2F Vector2F::get_lerp(const Vector2F& lhs, const Vector2F& rhs, FXD::F32 fVal)
{
	Vector2F retVec;
	Vector2F::set_lerp(retVec, lhs, rhs, fVal);
	return retVec;
}