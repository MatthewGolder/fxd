// Creator - MatthewGolder
#pragma once
#include "FXDEngine/Core/CompileTimeChecks.h"
#include "FXDEngine/Core/TypeTraits.h"
#include <cmath>

namespace FXD
{
	namespace Math
	{
		namespace Detail
		{
			// Basic operations
			template < typename T >
			CONSTEXPR bool IsPrimeHelper(T nVal, T nDiv)
			{
				return (nDiv * nDiv > nVal) ? true :
						(nVal % nDiv == 0) ? false :
						IsPrimeHelper(nVal, nDiv + 2);
			}

			template < typename Number, typename FXD::STD::enable_if< FXD::STD::is_unsigned< Number >::value >::type* = nullptr >
			CONSTEXPR auto AbsHelper(Number nVal)->decltype(auto)
			{
				return nVal;
			}

			template < typename Number, typename FXD::STD::enable_if< !FXD::STD::is_unsigned< Number >::value >::type* = nullptr >
			CONSTEXPR auto AbsHelper(Number nVal)->decltype(auto)
			{
				return (nVal >= 0) ? nVal : -nVal;
			}
		}

		// Basic operations
		template < typename Float >
		CONSTEXPR bool ApproxEquals(Float lhs, Float rhs, Float fTolerance)
		{
			CTC_ASSERT((Math::NumericLimits< Float >::is_integer == false), "Math: Value is not a Float");
			return (Math::Abs(rhs - lhs) <= fTolerance);
		}

		template < typename Integer >
		CONSTEXPR Integer Fractoral(Integer nVal)
		{
			return (nVal > 1) ? (nVal * Fractoral(nVal - 1)) : 1;
		}

		template < typename Integer >
		CONSTEXPR bool IsEven(Integer nVal)
		{
			CTC_ASSERT((Math::NumericLimits< Integer >::is_integer == true), "Math: Value is not a Integer");
			return !(nVal % 2);
		}

		template < typename Integer >
		CONSTEXPR bool IsOdd(Integer nVal)
		{
			CTC_ASSERT((Math::NumericLimits< Integer >::is_integer == true), "Math: Value is not a Integer");
			return !IsEven(nVal);
		}

		template < typename Integer >
		CONSTEXPR bool IsPrime(Integer nVal)
		{
			CTC_ASSERT((Math::NumericLimits< Integer >::is_integer == true), "Math: Value is not a Integer");
			return (nVal < 2) ? false :
					(nVal == 2) ? true :
					(nVal % 2 == 0) ? false :
					Detail::IsPrimeHelper(nVal, 3);
		}

		template < typename Integer >
		CONSTEXPR bool IsPowerOfTwo(Integer nVal)
		{
			CTC_ASSERT((Math::NumericLimits< Integer >::is_integer == true), "Math: Value is not a Integer");
			return !(nVal == 0) && !(nVal & (nVal - 1));
		}

		template < typename Number >
		CONSTEXPR Number Abs(Number nVal)
		{
			return Detail::AbsHelper(nVal);
		}

		template < typename Float >
		CONSTEXPR Float FMod(Float nVal1, Float nVal2)
		{
			CTC_ASSERT((Math::NumericLimits< Float >::is_integer == false), "Math: Value is not a Float");
			return ::fmod(nVal1, nVal2);
		}

		template < typename Number >
		CONSTEXPR Number Dim(const Number lhs, const Number rhs)
		{
			return (lhs > rhs) ? (lhs - rhs) : 0;
		}

		template < typename Number >
		CONSTEXPR Number Lerp(const Number& t1, const Number& t2, FXD::F32 fLerp)
		{
			return t1 + ((t2 - t1) * fLerp);
		}

		template < typename Float >
		CONSTEXPR Float GridSnap(const Float fLocation, const Float fGrid)
		{
			return (fGrid == 0.0f) ? fLocation : (Math::Floor((fLocation + 0.5f * fGrid) / fGrid) * fGrid);
		}

		// Exponential functions
		template < typename Float >
		FXD::F32 Exp(const Float fVal)
		{
			CTC_ASSERT((Math::NumericLimits< Float >::is_integer == false), "Math: Value is not a Float");
			return ::exp(fVal);
		}

		template < typename Float >
		FXD::F32 Exp2(const Float fVal)
		{
			CTC_ASSERT((Math::NumericLimits< Float >::is_integer == false), "Math: Value is not a Float");
			return ::exp2(fVal);
		}

		template < typename Float >
		FXD::F32 LogF(const Float fVal)
		{
			CTC_ASSERT((Math::NumericLimits< Float >::is_integer == false), "Math: Value is not a Float");
			return ::log(fVal);
		}

		template < typename Float >
		FXD::F32 Log2F(const Float fVal)
		{
			CTC_ASSERT((Math::NumericLimits< Float >::is_integer == false), "Math: Value is not a Float");
			return ::log2(fVal);
		}

		template < typename Float >
		FXD::F32 Log10F(const Float fVal)
		{
			CTC_ASSERT((Math::NumericLimits< Float >::is_integer == false), "Math: Value is not a Float");
			return ::log10(fVal);
		}

		// Power functions
		template < typename Float >
		Float Sqrt(Float fVal)
		{
			CTC_ASSERT((Math::NumericLimits< Float >::is_integer == false), "Math: Value is not a Float");
			return ::sqrt(fVal);
		}

		template < typename Float >
		Float InvSqrt(const Float fVal)
		{
			return (1.0f / Math::Sqrt(fVal));
		}

		template < typename Float >
		Float Hypot(Float nVal1, Float nVal2)
		{
			CTC_ASSERT((Math::NumericLimits< Float >::is_integer == false), "Math: Value is not a Float");
			return Math::Sqrt((nVal1 * nVal1) + (nVal2 * nVal2));
		}

		template < typename Number >
		CONSTEXPR Number Sqr(Number nVal)
		{
			return (nVal * nVal);
		}

		template < typename Number >
		CONSTEXPR Number Cube(Number nVal)
		{
			return (nVal * nVal * nVal);
		}

		inline FXD::F32 Pow(FXD::F32 fVal, FXD::F32 fExponent)
		{
			return ::pow(fVal, fExponent);
		}

		inline FXD::F32 PowF(FXD::F32 fVal, FXD::F32 fExponent)
		{
			return ::pow(fVal, fExponent);
		}

		inline FXD::F64 PowL(FXD::F64 fVal, FXD::F64 fExponent)
		{
			return ::pow(fVal, fExponent);
		}

		template < typename T, typename U >
		CONSTEXPR auto Sum(T first, U second)->decltype(auto)
		{
			return (first + second);
		}

		template < typename T, typename U, typename... Rest >
		CONSTEXPR auto Sum(T first, U second, Rest... rest)->decltype(auto)
		{
			return (first + Math::Sum(second, rest...));
		}

		template < typename... Numbers >
		CONSTEXPR auto Mean(Numbers... args)->decltype(auto)
		{
			return (Math::Sum(args...) / sizeof...(args));
		}

		// Trigonometric functions
		template < typename Float >
		Float Sin(const Float fVal)
		{
			return ::sin(fVal);
		}

		template < typename Float >
		Float Cos(const Float fVal)
		{
			return ::cos(fVal);
		}

		template < typename Float >
		Float Tan(const Float fVal)
		{
			return ::tan(fVal);
		}

		template < typename Float >
		Float Asin(const Float fVal)
		{
			return ::asin(fVal);
		}

		template < typename Float >
		Float Acos(const Float fVal)
		{
			return ::acos(fVal);
		}

		template < typename Float >
		Float Atan(const Float fVal)
		{
			return ::atan(fVal);
		}

		template < typename Float >
		Float Atan2(const Float fXVal, const Float fYVal)
		{
			return ::atan2(fYVal, fXVal);
		}


		// Hyperbolic functions
		template < typename Float >
		Float Sinh(const Float fVal)
		{
			return ::sinh(fVal);
		}

		template < typename Float >
		Float Cosh(const Float fVal)
		{
			return ::cosh(fVal);
		}

		template < typename Float >
		Float Tanh(const Float fVal)
		{
			return ::tanh(fVal);
		}

		template < typename Float >
		Float Asinh(const Float fVal)
		{
			return ::asinh(fVal);
		}

		template < typename Float >
		Float Acosh(const Float fVal)
		{
			return ::acosh(fVal);
		}

		template < typename Float >
		Float Atanh(const Float fVal)
		{
			return ::atanh(fVal);
		}


		template < typename Float >
		CONSTEXPR FXD::S32 TruncToInt(Float fVal)
		{
			CTC_ASSERT((Math::NumericLimits< Float >::is_integer == false), "Math: Value is not a Float");
			return (FXD::S32)fVal;
		}

		template < typename Float >
		CONSTEXPR Float Ceil(const Float fVal)
		{
			CTC_ASSERT((Math::NumericLimits< Float >::is_integer == false), "Math: Value is not a Float");
			return (Float)(FXD::S32(fVal) == fVal) ? FXD::S32(fVal) : (fVal >= 0.0) ? FXD::S32(fVal) + 1 : FXD::S32(fVal);
		}

		template < typename Float >
		CONSTEXPR FXD::S32 CeilI(const Float fVal)
		{
			CTC_ASSERT((Math::NumericLimits< Float >::is_integer == false), "Math: Value is not a Float");
			return TruncToInt(Ceil(fVal));
		}

		template < typename Float >
		CONSTEXPR Float Floor(const Float fVal)
		{
			CTC_ASSERT((Math::NumericLimits< Float >::is_integer == false), "Math: Value is not a Float");
			return (Float)(FXD::S32(fVal) == fVal) ? FXD::S32(fVal) : (fVal >= 0.0) ? FXD::S32(fVal) : FXD::S32(fVal) - 1;
		}

		template < typename Float >
		CONSTEXPR FXD::S32 FloorI(const Float fVal)
		{
			CTC_ASSERT((Math::NumericLimits< Float >::is_integer == false), "Math: Value is not a Float");
			return TruncToInt(Floor(fVal));
		}

		template < typename Float >
		CONSTEXPR Float RoundF(const Float fVal)
		{
			CTC_ASSERT((Math::NumericLimits< Float >::is_integer == false), "Math: Value is not a Float");
			return Floor(fVal + 0.5);
		}

		template < typename Float >
		CONSTEXPR FXD::S32 RoundI(const Float fVal)
		{
			CTC_ASSERT((Math::NumericLimits< Float >::is_integer == false), "Math: Value is not a Float");
			return FloorI(fVal + 0.5);
		}
	} //namespace Math
} //namespace FXD