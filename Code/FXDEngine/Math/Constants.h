// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_CONSTANTS_H
#define FXDENGINE_MATH_CONSTANTS_H

#include "FXDEngine/Core/Types.h"

namespace FXD
{
	namespace Math
	{
		static CONSTEXPR const FXD::F32 PI = 3.14159265358979323846f;
		static CONSTEXPR const FXD::F32 PI_HALF = 1.57079632679489661923f;
		static CONSTEXPR const FXD::F32 PI_TWO = 6.28318530717958647692f;
		static CONSTEXPR const FXD::F32 DEG2RAD = (FXD::Math::PI / 180.0f);
		static CONSTEXPR const FXD::F32 RAD2DEG = (180.0f / FXD::Math::PI);

	} //namespace Math
} //namespace FXD
#endif //FXDENGINE_MATH_CONSTANTS_H