// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_DEGREE_H
#define FXDENGINE_MATH_DEGREE_H

#include "FXDEngine/Math/Types.h"

namespace FXD
{
	namespace Math
	{
		// ------
		// Degree
		// -
		// ------
		class Degree
		{
		public:
			EXPLICIT Degree(FXD::F32 fDeg = 0.0f);
			Degree(const Degree& rhs);
			Degree(const Radian& rhs);
			~Degree(void);

			Degree& operator=(const FXD::F32& fDeg);
			Degree& operator=(const Degree& deg);
			Degree& operator=(const Radian& rad);

			const Degree& operator+() const;
			Degree operator+(FXD::F32 fDeg) const;
			Degree operator+(const Degree& deg) const;
			Degree operator+(const Radian& rad) const;

			Degree operator-() const;
			Degree operator-(FXD::F32 fDeg) const;
			Degree operator-(const Degree& deg) const;
			Degree operator-(const Radian& rad) const;

			Degree operator*(FXD::F32 fDeg) const;
			Degree operator*(const Degree& deg) const;
			Degree operator*(const Radian& rad) const;

			Degree operator/(FXD::F32 fDeg) const;
			Degree operator/(const Degree& deg) const;
			Degree operator/(const Radian& rad) const;

			Degree& operator+=(FXD::F32 fDeg);
			Degree& operator+=(const Degree& deg);
			Degree& operator+=(const Radian& rad);

			Degree& operator-=(FXD::F32 fDeg);
			Degree& operator-=(const Degree& deg);
			Degree& operator-=(const Radian& rad);

			Degree& operator*=(FXD::F32 fDeg);
			Degree& operator*=(const Degree& deg);
			Degree& operator*=(const Radian& rad);

			Degree& operator/=(FXD::F32 fDeg);
			Degree& operator/=(const Degree& deg);
			Degree& operator/=(const Radian& rad);

			bool operator< (const Degree& d) const { return (m_fDeg < d.m_fDeg); }
			bool operator<=(const Degree& d) const { return (m_fDeg <= d.m_fDeg); }
			bool operator>	(const Degree& d) const { return (m_fDeg > d.m_fDeg); }
			bool operator>=(const Degree& d) const { return (m_fDeg >= d.m_fDeg); }
			bool operator==(const Degree& d) const { return (m_fDeg == d.m_fDeg); }
			bool operator!=(const Degree& d) const { return (m_fDeg != d.m_fDeg); }

			friend Degree operator* (FXD::F32 lhs, const Degree& rhs) { return Degree(lhs * rhs.m_fDeg); }
			friend Degree operator/ (FXD::F32 lhs, const Degree& rhs) { return Degree(lhs / rhs.m_fDeg); }
			friend Degree operator+ (Degree& lhs, FXD::F32 rhs) { return Degree(lhs.m_fDeg + rhs); }
			friend Degree operator+ (FXD::F32 lhs, const Degree& rhs) { return Degree(lhs + rhs.m_fDeg); }
			friend Degree operator- (const Degree& lhs, FXD::F32 rhs) { return Degree(lhs.m_fDeg - rhs); }
			friend Degree operator- (const FXD::F32 lhs, const Degree& rhs) { return Degree(lhs - rhs.m_fDeg); }

			operator FXD::F32(void) const;

			FXD::F32 to_degrees(void) const;
			FXD::F32 to_radians(void) const;

			Degree wrap(void);

		private:
			FXD::F32 m_fDeg;
		};
	} //namespace Math
} //namespace FXD
#endif //FXDENGINE_MATH_DEGREE_H