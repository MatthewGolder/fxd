// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_LERPABLE_H
#define FXDENGINE_MATH_LERPABLE_H

#include "FXDEngine/Math/Math.h"
#include "FXDEngine/Core/Algorithm.h"

namespace FXD
{
	namespace Math
	{
		// ------
		// Lerpable
		// -
		// ------
		template < typename Type >
		class Lerpable
		{
		public:
			Lerpable(void)
				: Lerpable(Type())
			{}
			Lerpable(const Type& value)
				: m_bDirty(true)
				, m_target(value)
				, m_initial(value)
				, m_fTimer(0.0f)
				, m_fDuration(0.0f)
			{
			}
			~Lerpable(void)
			{}

			operator Type(void) const
			{
				return get();
			}
			void set(const Type& target, FXD::F32 fDuration)
			{
				m_bDirty = true;
				m_target = target;
				m_initial = get();
				m_fTimer = 0.0f;
				m_fDuration = fDuration;
			}
			Type get(void) const
			{
				if (m_fDuration == 0.0f)
				{
					return m_target;
				}
				FXD::F32 fPercentage = FXD::STD::clamp(m_fTimer / m_fDuration, 0.0f, 1.0f);
				return FXD::Math::Lerp(m_initial, m_target, fPercentage);
			}
			void update(FXD::F32 dt)
			{
				FXD::F32 fT = m_fTimer;
				m_fTimer = FXD::STD::clamp(m_fTimer + dt, 0.0f, m_fDuration);
				m_bDirty = (fT != m_fTimer);
			}

		private:
			bool m_bDirty;
			Type m_target;
			Type m_initial;
			FXD::F32 m_fTimer;
			FXD::F32 m_fDuration;
		};
		using LerpF32 = Lerpable< FXD::F32 >;
		using LerpF64 = Lerpable< FXD::F64 >;

	} //namespace Math
} //namespace FXD
#endif //FXDENGINE_MATH_LERPABLE_H