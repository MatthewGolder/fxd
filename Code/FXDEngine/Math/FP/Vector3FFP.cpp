// Creator - MatthewGolder
#include "FXDEngine/Math/Types.h"
#include "FXDEngine/Math/Math.h"
#include "FXDEngine/Core/Algorithm.h"
#include "FXDEngine/Core/Debugger.h"

#if IsMathFP()
namespace FXD
{
	namespace Math
	{
		void Vec3FSet(_vector3FType& dst, FXD::F32 x, FXD::F32 y, FXD::F32 z)
		{
			dst.vector3[0] = x;
			dst.vector3[1] = y;
			dst.vector3[2] = z;
		}
		void Vec3FSet(_vector3FType& dst, FXD::F32 fVal)
		{
			Math::Vec3FSet(dst, fVal, fVal, fVal);
		}

		void Vec3FSetIndex(_vector3FType& dst, FXD::U32 nID, FXD::F32 fVal)
		{
			dst.vector3[nID] = fVal;
		}
		void Vec3FSetX(_vector3FType& dst, FXD::F32 fVal)
		{
			Math::Vec3FSetIndex(dst, 0, fVal);
		}
		void Vec3FSetY(_vector3FType& dst, FXD::F32 fVal)
		{
			Math::Vec3FSetIndex(dst, 1, fVal);
		}
		void Vec3FSetZ(_vector3FType& dst, FXD::F32 fVal)
		{
			Math::Vec3FSetIndex(dst, 2, fVal);
		}
		void Vec3FSetW(_vector3FType& dst, FXD::F32 fVal)
		{
			Math::Vec3FSetIndex(dst, 3, fVal);
		}

		FXD::F32 Vec3FGetIndex(const _vector3FType& vec, FXD::U32 nID)
		{
			return vec.vector3[nID];
		}
		FXD::F32 Vec3FGetX(const _vector3FType& vec)
		{
			return Math::Vec3FGetIndex(vec, 0);
		}
		FXD::F32 Vec3FGetY(const _vector3FType& vec)
		{
			return Math::Vec3FGetIndex(vec, 1);
		}
		FXD::F32 Vec3FGetZ(const _vector3FType& vec)
		{
			return Math::Vec3FGetIndex(vec, 2);
		}
		FXD::F32 Vec3FGetW(const _vector3FType& vec)
		{
			return Math::Vec3FGetIndex(vec, 3);
		}

		bool Vec3FCompare(const _vector3FType& lhs, const _vector3FType& rhs)
		{
			return (
				(Math::Vec3FGetX(lhs) == Math::Vec3FGetX(rhs)) &&
				(Math::Vec3FGetY(lhs) == Math::Vec3FGetY(rhs)) &&
				(Math::Vec3FGetZ(lhs) == Math::Vec3FGetZ(rhs)));
		}
		void Vec3FCopy(_vector3FType& dst, const _vector3FType& src)
		{
			Math::Vec3FSetX(dst, Math::Vec3FGetX(src));
			Math::Vec3FSetY(dst, Math::Vec3FGetY(src));
			Math::Vec3FSetZ(dst, Math::Vec3FGetZ(src));
		}
		void Vec3FCopy(_vector3FType& dst, const _vector4FType& src)
		{
			Math::Vec3FSetX(dst, Math::Vec4FGetX(src));
			Math::Vec3FSetY(dst, Math::Vec4FGetY(src));
			Math::Vec3FSetZ(dst, Math::Vec4FGetZ(src));
		}

		void Vec3FAdd(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
		{
			Math::Vec3FSetX(dst, (Math::Vec3FGetX(lhs) + fX));
			Math::Vec3FSetY(dst, (Math::Vec3FGetY(lhs) + fY));
			Math::Vec3FSetZ(dst, (Math::Vec3FGetZ(lhs) + fZ));
		}
		void Vec3FAdd(_vector3FType& dst, const _vector3FType& lhs, const _vector3FType& rhs)
		{
			Math::Vec3FAdd(dst, lhs, Math::Vec3FGetX(rhs), Math::Vec3FGetY(rhs), Math::Vec3FGetZ(rhs));
		}
		void Vec3FAdd(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fVal)
		{
			Math::Vec3FAdd(dst, lhs, fVal, fVal, fVal);
		}

		void Vec3FSub(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
		{
			Math::Vec3FSetX(dst, (Math::Vec3FGetX(lhs) - fX));
			Math::Vec3FSetY(dst, (Math::Vec3FGetY(lhs) - fY));
			Math::Vec3FSetZ(dst, (Math::Vec3FGetZ(lhs) - fZ));
		}
		void Vec3FSub(_vector3FType& dst, const _vector3FType& lhs, const _vector3FType& rhs)
		{
			Math::Vec3FSub(dst, lhs, Math::Vec3FGetX(rhs), Math::Vec3FGetY(rhs), Math::Vec3FGetZ(rhs));
		}
		void Vec3FSub(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fVal)
		{
			Math::Vec3FSub(dst, lhs, fVal, fVal, fVal);
		}

		void Vec3FMul(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
		{
			Math::Vec3FSetX(dst, (Math::Vec3FGetX(lhs) * fX));
			Math::Vec3FSetY(dst, (Math::Vec3FGetY(lhs) * fY));
			Math::Vec3FSetZ(dst, (Math::Vec3FGetZ(lhs) * fZ));
		}
		void Vec3FMul(_vector3FType& dst, const _vector3FType& lhs, const _vector3FType& rhs)
		{
			Math::Vec3FMul(dst, lhs, Math::Vec3FGetX(rhs), Math::Vec3FGetY(rhs), Math::Vec3FGetZ(rhs));
		}
		void Vec3FMul(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fVal)
		{
			Math::Vec3FMul(dst, lhs, fVal, fVal, fVal);
		}

		void Vec3FDiv(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
		{
			Math::Vec3FSetX(dst, (Math::Vec3FGetX(lhs) / fX));
			Math::Vec3FSetY(dst, (Math::Vec3FGetY(lhs) / fY));
			Math::Vec3FSetZ(dst, (Math::Vec3FGetZ(lhs) / fZ));
		}
		void Vec3FDiv(_vector3FType& dst, const _vector3FType& lhs, const _vector3FType& rhs)
		{
			Math::Vec3FDiv(dst, lhs, Math::Vec3FGetX(rhs), Math::Vec3FGetY(rhs), Math::Vec3FGetZ(rhs));
		}
		void Vec3FDiv(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fVal)
		{
			Math::Vec3FDiv(dst, lhs, fVal, fVal, fVal);
		}

		FXD::F32 Vec3FSum(const _vector3FType& vec)
		{
			return (Math::Vec3FGetX(vec) + Math::Vec3FGetY(vec) + Math::Vec3FGetZ(vec));
		}
		FXD::F32 Vec3FDot(const _vector3FType& lhs, const _vector3FType& rhs)
		{
			_vector3FType vecDot;
			Math::Vec3FMul(vecDot, lhs, rhs);
			return Math::Vec3FSum(vecDot);
		}
		_vector3FType Vec3FCross(const _vector3FType& lhs, const _vector3FType& rhs)
		{
			_vector3FType retVec;
			Math::Vec3FSetX(retVec, (Math::Vec3FGetY(lhs) * Math::Vec3FGetZ(rhs)) - (Math::Vec3FGetZ(lhs) * Math::Vec3FGetY(rhs)));
			Math::Vec3FSetY(retVec, (Math::Vec3FGetZ(lhs) * Math::Vec3FGetX(rhs)) - (Math::Vec3FGetX(lhs) * Math::Vec3FGetZ(rhs)));
			Math::Vec3FSetZ(retVec, (Math::Vec3FGetX(lhs) * Math::Vec3FGetY(rhs)) - (Math::Vec3FGetY(lhs) * Math::Vec3FGetX(rhs)));
			Math::Vec3FSetZ(retVec, 0.0f);
			return retVec;
		}
		FXD::F32 Vec3FSquaredLength(const _vector3FType& vec)
		{
			return Math::Vec3FDot(vec, vec);
		}
		FXD::F32 Vec3FSquaredDistance(const _vector3FType& lhs, const _vector3FType& rhs)
		{
			_vector3FType out;
			Math::Vec3FSub(out, lhs, rhs);
			return Math::Vec3FSquaredLength(out);
		}
		void Vec3FMidPoint(_vector3FType& out, const _vector3FType& lhs, const _vector3FType& rhs)
		{
			Math::Vec3FAdd(out, lhs, rhs);
			Math::Vec3FDiv(out, out, 2.0f);
		}
		FXD::F32 Vec3FAngleBetween(const _vector3FType& lhs, const _vector3FType& rhs)
		{
			FXD::F32 fLenProduct = (Math::Sqr(Math::Vec3FSquaredLength(lhs)) * Math::Sqr(Math::Vec3FSquaredLength(rhs)));

			// Divide by zero check
			if (fLenProduct < 1e-6f)
			{
				fLenProduct = 1e-6f;
			}

			FXD::F32 fVal = (Math::Vec3FDot(lhs, rhs) / fLenProduct);
			fVal = FXD::STD::clamp(fVal, -1.0f, 1.0f);
			return Math::Acos(fVal);
		}
		void Vec3FNegate(_vector3FType& vec)
		{
			Math::Vec3FSetX(vec, -Math::Vec3FGetX(vec));
			Math::Vec3FSetY(vec, -Math::Vec3FGetY(vec));
			Math::Vec3FSetZ(vec, -Math::Vec3FGetZ(vec));
		}
		void Vec3FMinimum(_vector3FType& out, const _vector3FType min)
		{
			// Clamp the x value.
			if (Math::Vec3FGetX(out) > Math::Vec3FGetX(min))
			{
				Math::Vec3FSetX(out, Math::Vec3FGetX(min));
			}
			// Clamp the y value.
			if (Math::Vec3FGetY(out) > Math::Vec3FGetY(min))
			{
				Math::Vec3FSetY(out, Math::Vec3FGetY(min));
			}
			// Clamp the z value.
			if (Math::Vec3FGetZ(out) > Math::Vec3FGetZ(min))
			{
				Math::Vec3FSetZ(out, Math::Vec3FGetZ(min));
			}
		}
		void Vec3FMaximum(_vector3FType& out, const _vector3FType max)
		{
			// Clamp the x value.
			if (Math::Vec3FGetX(out) < Math::Vec3FGetX(max))
			{
				Math::Vec3FSetX(out, Math::Vec3FGetX(max));
			}
			// Clamp the y value.
			if (Math::Vec3FGetY(out) < Math::Vec3FGetY(max))
			{
				Math::Vec3FSetY(out, Math::Vec3FGetY(max));
			}
			// Clamp the z value.
			if (Math::Vec3FGetZ(out) < Math::Vec3FGetZ(max))
			{
				Math::Vec3FSetZ(out, Math::Vec3FGetZ(max));
			}
		}
		void Vec3FClamp(_vector3FType& out, const _vector3FType& min, const _vector3FType& max)
		{
			Math::Vec3FMinimum(out, max);
			Math::Vec3FMaximum(out, min);
		}
		void Vec3FNormalise(_vector3FType& out)
		{
			const FXD::F32 fLength = Math::Sqrt(Math::Vec3FSquaredLength(out));
			if (fLength != 0.0f)
			{
				Math::Vec3FDiv(out, out, fLength);
			}
		}
		void Vec3FAbs(_vector3FType& out)
		{
			Math::Vec3FSetX(out, Math::Abs(Math::Vec3FGetX(out)));
			Math::Vec3FSetY(out, Math::Abs(Math::Vec3FGetY(out)));
			Math::Vec3FSetZ(out, Math::Abs(Math::Vec3FGetZ(out)));
		}
		void Vec3FCeil(_vector3FType& out)
		{
			Math::Vec3FSetX(out, Math::Ceil(Math::Vec3FGetX(out)));
			Math::Vec3FSetY(out, Math::Ceil(Math::Vec3FGetY(out)));
			Math::Vec3FSetZ(out, Math::Ceil(Math::Vec3FGetZ(out)));
		}
		void Vec3FFloor(_vector3FType& out)
		{
			Math::Vec3FSetX(out, Math::Floor(Math::Vec3FGetX(out)));
			Math::Vec3FSetY(out, Math::Floor(Math::Vec3FGetY(out)));
			Math::Vec3FSetZ(out, Math::Floor(Math::Vec3FGetZ(out)));
		}

	} //namespace Math
} //namespace FXD
#endif //IsMathFP()