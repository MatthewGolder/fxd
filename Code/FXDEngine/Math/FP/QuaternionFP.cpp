// Creator - MatthewGolder
#include "FXDEngine/Math/Types.h"
#include "FXDEngine/Math/Math.h"

#if IsMathFP()
namespace FXD
{
	namespace Math
	{
		void QuatFRotationPitchYawRollF(_vector4FType& dst, FXD::F32 fPitch, FXD::F32 fYaw, FXD::F32 fRoll, Math::EulerAngleOrder eOrder)
		{
			const EulerAngleOrderData& l = Math::kEALookup[(FXD::U32)eOrder];

			const FXD::F32 fXOver2 = (fPitch * 0.5f);
			const FXD::F32 fYOver2 = (fYaw * 0.5f);
			const FXD::F32 fZOver2 = (fRoll * 0.5f);

			const FXD::F32 fSinX = Math::Sin(fXOver2);
			const FXD::F32 fCosX = Math::Cos(fXOver2);

			const FXD::F32 fSinY = Math::Sin(fYOver2);
			const FXD::F32 fCosY = Math::Cos(fYOver2);

			const FXD::F32 fSinZ = Math::Sin(fZOver2);
			const FXD::F32 fCosZ = Math::Cos(fZOver2);

			_vector4FType quats[3];
			Math::Vec4FSet(quats[0], fSinX, 0.0f, 0.0f, fCosX);
			Math::Vec4FSet(quats[1], 0.0f, fSinY, 0.0f, fCosY);
			Math::Vec4FSet(quats[2], 0.0f, 0.0f, fSinZ, fCosZ);

			_vector4FType temp;
			Math::QuatFMul(temp, quats[l.a], quats[l.b]);
			Math::QuatFMul(dst, temp, quats[l.c]);
		}
		void QuatFRotationPitchYawRollV(_vector4FType& dst, const _vector3FType& rhs, Math::EulerAngleOrder eOrder)
		{
			Math::QuatFRotationPitchYawRollF(dst, Math::Vec3FGetX(rhs), Math::Vec3FGetY(rhs), Math::Vec3FGetZ(rhs), eOrder);
		}
		void QuatFToRotation(_matrix3Type& dst, const _vector4FType& src)
		{
			_vector4FType mul;
			Math::Vec4FMul(mul, src, Math::Vec4FGetX(src));

			const FXD::F32 xx = Math::Vec4FGetX(mul);
			const FXD::F32 xy = Math::Vec4FGetY(mul);
			const FXD::F32 xz = Math::Vec4FGetZ(mul);
			const FXD::F32 xw = Math::Vec4FGetW(mul);

			const FXD::F32 yy = Math::Vec4FGetY(src) * Math::Vec4FGetY(src);
			const FXD::F32 yz = Math::Vec4FGetY(src) * Math::Vec4FGetZ(src);
			const FXD::F32 yw = Math::Vec4FGetY(src) * Math::Vec4FGetW(src);

			const FXD::F32 zz = Math::Vec4FGetZ(src) * Math::Vec4FGetZ(src);
			const FXD::F32 zw = Math::Vec4FGetZ(src) * Math::Vec4FGetW(src);

			Math::Mat3SetRow(dst, 0, (1.0f - (2.0f * (yy + zz))), (2.0f * (xy + zw)), (2.0f * (xz - yw)));
			Math::Mat3SetRow(dst, 1, (2.0f * (xy - zw)), (1.0f - (2.0f * (xx + zz))), (2.0f * (yz + xw)));
			Math::Mat3SetRow(dst, 2, (2.0f * (xz + yw)), (2.0f * (yz - xw)), (1.0f - (2.0f * (xx + yy))));
		}
		void QuatFMul(_vector4FType& dst, const _vector4FType& lhs, const _vector4FType& rhs)
		{
			Math::Vec4FSet(dst,
				(Math::Vec4FGetW(rhs) * Math::Vec4FGetX(lhs)) + (Math::Vec4FGetX(rhs) * Math::Vec4FGetW(lhs)) + (Math::Vec4FGetY(rhs) * Math::Vec4FGetZ(lhs)) - (Math::Vec4FGetZ(rhs) * Math::Vec4FGetY(lhs)),
				(Math::Vec4FGetW(rhs) * Math::Vec4FGetY(lhs)) - (Math::Vec4FGetX(rhs) * Math::Vec4FGetZ(lhs)) + (Math::Vec4FGetY(rhs) * Math::Vec4FGetW(lhs)) + (Math::Vec4FGetZ(rhs) * Math::Vec4FGetX(lhs)),
				(Math::Vec4FGetW(rhs) * Math::Vec4FGetZ(lhs)) + (Math::Vec4FGetX(rhs) * Math::Vec4FGetY(lhs)) - (Math::Vec4FGetY(rhs) * Math::Vec4FGetX(lhs)) + (Math::Vec4FGetZ(rhs) * Math::Vec4FGetW(lhs)),
				(Math::Vec4FGetW(rhs) * Math::Vec4FGetW(lhs)) - (Math::Vec4FGetX(rhs) * Math::Vec4FGetX(lhs)) - (Math::Vec4FGetY(rhs) * Math::Vec4FGetY(lhs)) - (Math::Vec4FGetZ(rhs) * Math::Vec4FGetZ(lhs)));
		}

		//void QuatFMul(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fVal)
		//{
		//}

	} //namespace Math
} //namespace FXD
#endif //IsMathFP()