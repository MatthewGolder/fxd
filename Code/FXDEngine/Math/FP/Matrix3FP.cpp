// Creator - MatthewGolder
#include "FXDEngine/Math/Types.h"
#include "FXDEngine/Math/Math.h"

#if IsMathFP()
namespace FXD
{
	namespace Math
	{
		void Mat3SetIdentity(_matrix3Type& dst)
		{
			Math::Mat3SetRow(dst, 0, 1.0f, 0.0f, 0.0f);
			Math::Mat3SetRow(dst, 1, 0.0f, 1.0f, 0.0f);
			Math::Mat3SetRow(dst, 2, 0.0f, 0.0f, 1.0f);
		}
		void Mat3Set(_matrix3Type& dst, FXD::F32 m00, FXD::F32 m01, FXD::F32 m02, FXD::F32 m10, FXD::F32 m11, FXD::F32 m12, FXD::F32 m20, FXD::F32 m21, FXD::F32 m22)
		{
			Math::Mat3SetRow(dst, 0, m00, m01, m02);
			Math::Mat3SetRow(dst, 1, m10, m11, m12);
			Math::Mat3SetRow(dst, 2, m20, m21, m22);
		}
		bool Mat3Compare(const _matrix3Type& dst, const _matrix3Type& src)
		{
			return
				(Math::Vec3FCompare(dst.mtrx3[0], src.mtrx3[0])) &&
				(Math::Vec3FCompare(dst.mtrx3[1], src.mtrx3[1])) &&
				(Math::Vec3FCompare(dst.mtrx3[2], src.mtrx3[2]));
		}
		void Mat3Copy(_matrix3Type& dst, const _matrix3Type& src)
		{
			Math::Vec3FCopy(dst.mtrx3[0], src.mtrx3[0]);
			Math::Vec3FCopy(dst.mtrx3[1], src.mtrx3[1]);
			Math::Vec3FCopy(dst.mtrx3[2], src.mtrx3[2]);
		}
		void Mat3Copy(_matrix3Type& dst, const _matrix4Type& src)
		{
			Math::Vec3FCopy(dst.mtrx3[0], src.mtrx4[0]);
			Math::Vec3FCopy(dst.mtrx3[1], src.mtrx4[1]);
			Math::Vec3FCopy(dst.mtrx3[2], src.mtrx4[2]);
		}

		_vector3FType Mat3Dot3(const _matrix3Type& mat, const _vector3FType& vec)
		{
			_vector3FType m0, m1, m2;
			Math::Vec3FMul(m0, mat.mtrx3[0], Math::Vec3FGetX(vec));
			Math::Vec3FMul(m1, mat.mtrx3[1], Math::Vec3FGetY(vec));
			Math::Vec3FMul(m2, mat.mtrx3[2], Math::Vec3FGetZ(vec));

			_vector3FType res;
			Math::Vec3FSetX(res, Math::Vec3FGetX(m0) + Math::Vec3FGetX(m1) + Math::Vec3FGetX(m2));
			Math::Vec3FSetY(res, Math::Vec3FGetY(m0) + Math::Vec3FGetY(m1) + Math::Vec3FGetY(m2));
			Math::Vec3FSetZ(res, Math::Vec3FGetZ(m0) + Math::Vec3FGetZ(m1) + Math::Vec3FGetZ(m2));
			return res;
		}
		void Mat3Add(_matrix3Type& dst, const _matrix3Type& lhs, const _matrix3Type& rhs)
		{
			Math::Vec3FAdd(dst.mtrx3[0], lhs.mtrx3[0], rhs.mtrx3[0]);
			Math::Vec3FAdd(dst.mtrx3[1], lhs.mtrx3[1], rhs.mtrx3[1]);
			Math::Vec3FAdd(dst.mtrx3[2], lhs.mtrx3[2], rhs.mtrx3[2]);
			Math::Vec3FAdd(dst.mtrx3[3], lhs.mtrx3[3], rhs.mtrx3[3]);
		}
		void Mat3Sub(_matrix3Type& dst, const _matrix3Type& lhs, const _matrix3Type& rhs)
		{
			Math::Vec3FSub(dst.mtrx3[0], lhs.mtrx3[0], rhs.mtrx3[0]);
			Math::Vec3FSub(dst.mtrx3[1], lhs.mtrx3[1], rhs.mtrx3[1]);
			Math::Vec3FSub(dst.mtrx3[2], lhs.mtrx3[2], rhs.mtrx3[2]);
			Math::Vec3FSub(dst.mtrx3[3], lhs.mtrx3[3], rhs.mtrx3[3]);
		}
		void Mat3Mul(_matrix3Type& dst, const _matrix3Type& lhs, const _matrix3Type& rhs)
		{
			dst.mtrx3[0] = Math::Mat3Dot3(rhs, lhs.mtrx3[0]);
			dst.mtrx3[1] = Math::Mat3Dot3(rhs, lhs.mtrx3[1]);
			dst.mtrx3[2] = Math::Mat3Dot3(rhs, lhs.mtrx3[2]);
		}

		FXD::F32 Mat3Detrament(const _matrix3Type& src)
		{
			const FXD::F32 fCofactor00 = ((Math::Vec3FGetY(src.mtrx3[1]) * Math::Vec3FGetZ(src.mtrx3[2])) - (Math::Vec3FGetZ(src.mtrx3[1]) * Math::Vec3FGetY(src.mtrx3[2])));
			const FXD::F32 fCofactor10 = ((Math::Vec3FGetZ(src.mtrx3[1]) * Math::Vec3FGetX(src.mtrx3[2])) - (Math::Vec3FGetX(src.mtrx3[1]) * Math::Vec3FGetZ(src.mtrx3[2])));
			const FXD::F32 fCofactor20 = ((Math::Vec3FGetX(src.mtrx3[1]) * Math::Vec3FGetY(src.mtrx3[2])) - (Math::Vec3FGetY(src.mtrx3[1]) * Math::Vec3FGetX(src.mtrx3[2])));
			const FXD::F32 fDet = ((Math::Vec3FGetX(src.mtrx3[0]) * fCofactor00) + (Math::Vec3FGetY(src.mtrx3[0]) * fCofactor10) + (Math::Vec3FGetZ(src.mtrx3[0]) * fCofactor20));
			return fDet;
		}
		void Mat3Negate(_matrix3Type& dst)
		{
			Math::Vec3FNegate(dst.mtrx3[0]);
			Math::Vec3FNegate(dst.mtrx3[1]);
			Math::Vec3FNegate(dst.mtrx3[2]);
		}
		void Mat3Transpose(_matrix3Type& dst, const _matrix3Type& src)
		{
			Mat3Set(dst,
				src.mtrx3[0].vector3[0], src.mtrx3[1].vector3[0], src.mtrx3[2].vector3[0],
				src.mtrx3[0].vector3[1], src.mtrx3[1].vector3[1], src.mtrx3[2].vector3[1],
				src.mtrx3[0].vector3[2], src.mtrx3[1].vector3[2], src.mtrx3[2].vector3[2]
			);
		}
		void Mat3Inverse(_matrix3Type& dst, const _matrix3Type& src)
		{
			const FXD::F32 fdet = Math::Mat3Detrament(src);
			const FXD::F32 fInvdet = (1.0f / fdet);

			Math::Mat3SetRow(dst, 0,
				(Math::Vec3FGetY(src.mtrx3[1]) * Math::Vec3FGetZ(src.mtrx3[2]) - Math::Vec3FGetY(src.mtrx3[2]) * Math::Vec3FGetZ(src.mtrx3[1])) * fInvdet,
				(Math::Vec3FGetZ(src.mtrx3[0]) * Math::Vec3FGetY(src.mtrx3[2]) - Math::Vec3FGetY(src.mtrx3[0]) * Math::Vec3FGetZ(src.mtrx3[2])) * fInvdet,
				(Math::Vec3FGetY(src.mtrx3[0]) * Math::Vec3FGetZ(src.mtrx3[1]) - Math::Vec3FGetZ(src.mtrx3[0]) * Math::Vec3FGetY(src.mtrx3[1])) * fInvdet);

			Math::Mat3SetRow(dst, 1,
				(Math::Vec3FGetZ(src.mtrx3[1]) * Math::Vec3FGetX(src.mtrx3[2]) - Math::Vec3FGetX(src.mtrx3[1]) * Math::Vec3FGetZ(src.mtrx3[2])) * fInvdet,
				(Math::Vec3FGetX(src.mtrx3[0]) * Math::Vec3FGetZ(src.mtrx3[2]) - Math::Vec3FGetZ(src.mtrx3[0]) * Math::Vec3FGetX(src.mtrx3[2])) * fInvdet,
				(Math::Vec3FGetX(src.mtrx3[1]) * Math::Vec3FGetZ(src.mtrx3[0]) - Math::Vec3FGetX(src.mtrx3[0]) * Math::Vec3FGetZ(src.mtrx3[1])) * fInvdet);

			Math::Mat3SetRow(dst, 2,
				(Math::Vec3FGetX(src.mtrx3[1]) * Math::Vec3FGetY(src.mtrx3[2]) - Math::Vec3FGetX(src.mtrx3[2]) * Math::Vec3FGetY(src.mtrx3[1])) * fInvdet,
				(Math::Vec3FGetX(src.mtrx3[2]) * Math::Vec3FGetY(src.mtrx3[0]) - Math::Vec3FGetX(src.mtrx3[0]) * Math::Vec3FGetY(src.mtrx3[2])) * fInvdet,
				(Math::Vec3FGetX(src.mtrx3[0]) * Math::Vec3FGetY(src.mtrx3[1]) - Math::Vec3FGetX(src.mtrx3[1]) * Math::Vec3FGetY(src.mtrx3[0])) * fInvdet);
		}
		void Mat3SetRow(_matrix3Type& dst, FXD::U32 nRow, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
		{
			_vector3FType vec;
			Math::Vec3FSet(vec, fX, fY, fZ);
			Math::Vec3FCopy(dst.mtrx3[nRow], vec);
		}
		void Mat3SetRow(_matrix3Type& dst, FXD::U32 nRow, const _vector3FType& vec)
		{
			Math::Vec3FCopy(dst.mtrx3[nRow], vec);
		}
		void Mat3SetColumn(_matrix3Type& dst, FXD::U32 nCol, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
		{
			Math::Vec3FSetIndex(dst.mtrx3[0], nCol, fX);
			Math::Vec3FSetIndex(dst.mtrx3[1], nCol, fY);
			Math::Vec3FSetIndex(dst.mtrx3[2], nCol, fZ);
		}
		void Mat3SetColumn(_matrix3Type& dst, FXD::U32 nCol, const _vector3FType& vec)
		{
			Math::Vec3FSetIndex(dst.mtrx3[0], nCol, Math::Vec3FGetX(vec));
			Math::Vec3FSetIndex(dst.mtrx3[1], nCol, Math::Vec3FGetY(vec));
			Math::Vec3FSetIndex(dst.mtrx3[2], nCol, Math::Vec3FGetZ(vec));
		}
		void Mat3SetRight(_matrix3Type& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
		{
			_vector3FType vec;
			Math::Vec3FSet(vec, fX, fY, fZ);
			Math::Mat3SetIdentity(dst);
			Math::Mat3SetColumn(dst, 0, vec);
		}
		void Mat3SetRight(_matrix3Type& dst, const _vector3FType& vec)
		{
			Math::Mat3SetRight(dst, Math::Vec3FGetX(vec), Math::Vec3FGetY(vec), Math::Vec3FGetZ(vec));
		}
		void Mat3SetUp(_matrix3Type& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
		{
			_vector3FType vec;
			Math::Vec3FSet(vec, fX, fY, fZ);
			Math::Mat3SetIdentity(dst);
			Math::Mat3SetColumn(dst, 1, vec);
		}
		void Mat3SetUp(_matrix3Type& dst, const _vector3FType& vec)
		{
			Math::Mat3SetUp(dst, Math::Vec3FGetX(vec), Math::Vec3FGetY(vec), Math::Vec3FGetZ(vec));
		}
		void Mat3SetForward(_matrix3Type& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
		{
			_vector3FType vec;
			Math::Vec3FSet(vec, fX, fY, fZ);
			Math::Mat3SetIdentity(dst);
			Math::Mat3SetColumn(dst, 2, vec);
		}
		void Mat3SetForward(_matrix3Type& dst, const _vector3FType& vec)
		{
			Math::Mat3SetForward(dst, Math::Vec3FGetX(vec), Math::Vec3FGetY(vec), Math::Vec3FGetZ(vec));
		}
		void Mat3SetScale(_matrix3Type& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
		{
			Math::Mat3SetRow(dst, 0, fX, 0.0f, 0.0f);
			Math::Mat3SetRow(dst, 1, 0.0f, fY, 0.0f);
			Math::Mat3SetRow(dst, 2, 0.0f, 0.0f, fZ);
		}
		void Mat3SetScale(_matrix3Type& dst, const _vector3FType& vec)
		{
			Math::Mat3SetScale(dst, Math::Vec3FGetX(vec), Math::Vec3FGetY(vec), Math::Vec3FGetZ(vec));
		}
		void Mat3SetRotationX(_matrix3Type& dst, FXD::F32 fAngleX)
		{
			FXD::F32 fSinAngle = Math::Sin(fAngleX);
			FXD::F32 fCosAngle = Math::Cos(fAngleX);

			Math::Mat3SetRow(dst, 0, 1.0f, 0.0f, 0.0f);
			Math::Mat3SetRow(dst, 1, 0.0f, fCosAngle, fSinAngle);
			Math::Mat3SetRow(dst, 2, 0.0f, -fSinAngle, fCosAngle);
		}
		void Mat3SetRotationY(_matrix3Type& dst, FXD::F32 fAngleY)
		{
			const FXD::F32 fSinAngle = Math::Sin(fAngleY);
			const FXD::F32 fCosAngle = Math::Cos(fAngleY);

			Math::Mat3SetRow(dst, 0, fCosAngle, 0.0f, -fSinAngle);
			Math::Mat3SetRow(dst, 1, 0.0f, 1.0f, 0.0f);
			Math::Mat3SetRow(dst, 2, fSinAngle, 0.0f, fCosAngle);
		}
		void Mat3SetRotationZ(_matrix3Type& dst, FXD::F32 fAngleZ)
		{
			const FXD::F32 fSinAngle = Math::Sin(fAngleZ);
			const FXD::F32 fCosAngle = Math::Cos(fAngleZ);

			Math::Vec3FSet(dst.mtrx3[0], fCosAngle, fSinAngle, 0.0f);
			Math::Vec3FSet(dst.mtrx3[1], -fSinAngle, fCosAngle, 0.0f);
			Math::Vec3FSet(dst.mtrx3[2], 0.0f, 0.0f, 1.0f);
		}
		void Mat3SetRotationPitchYawRoll(_matrix3Type& dst, const _vector3FType& angle, Math::EulerAngleOrder eOrder)
		{
			_vector4FType quat;
			Math::QuatFRotationPitchYawRollV(quat, angle, eOrder);
			Math::Mat3SetRotationQuaternion(dst, quat);
		}
		void Mat3SetRotationPitchYawRoll(_matrix3Type& dst, FXD::F32 fPitch, FXD::F32 fYaw, FXD::F32 fRoll, Math::EulerAngleOrder eOrder)
		{
			_vector3FType angle;
			Math::Vec3FSet(angle, fPitch, fYaw, fRoll);
			Math::Mat3SetRotationPitchYawRoll(dst, angle, eOrder);
		}
		void Mat3SetRotationQuaternion(_matrix3Type& dst, const _vector4FType& quat)
		{
			Math::QuatFToRotation(dst, quat);
		}
		void Mat3GetRow(_vector3FType& dst, const _matrix3Type& mat, FXD::U32 nRow)
		{
			Math::Vec3FCopy(dst, mat.mtrx3[nRow]);
		}
		void Mat3GetColumn(_vector3FType& dst, const _matrix3Type& mat, FXD::U32 nCol)
		{
			Math::Vec3FSet(dst, Math::Vec3FGetIndex(mat.mtrx3[0], nCol), Math::Vec3FGetIndex(mat.mtrx3[1], nCol), Math::Vec3FGetIndex(mat.mtrx3[2], nCol));
		}
		void Mat3GetRight(_vector3FType& dst, const _matrix3Type& mat)
		{
			Math::Mat3GetColumn(dst, mat, 0);
		}
		void Mat3GetUp(_vector3FType& dst, const _matrix3Type& mat)
		{
			Math::Mat3GetColumn(dst, mat, 1);
		}
		void Mat3GetForward(_vector3FType& dst, const _matrix3Type& mat)
		{
			Math::Mat3GetColumn(dst, mat, 2);
		}
		void Mat3GetScale(_vector3FType& dst, const _matrix3Type& mat)
		{
			Math::Vec3FSet(dst, Math::Vec3FGetIndex(mat.mtrx3[0], 0), Math::Vec3FGetIndex(mat.mtrx3[1], 1), Math::Vec3FGetIndex(mat.mtrx3[2], 2));
		}
	} //namespace Math
} //namespace FXD
#endif //IsMathFP()