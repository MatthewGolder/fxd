// Creator - MatthewGolder
#include "FXDEngine/Math/Types.h"
#include "FXDEngine/Math/Math.h"
#include "FXDEngine/Core/Debugger.h"

#if IsMathFP()
namespace FXD
{
	namespace Math
	{
		void Mat4SetIdentity(_matrix4Type& dst)
		{
			Math::Mat4SetRow(dst, 0, 1.0f, 0.0f, 0.0f, 0.0f);
			Math::Mat4SetRow(dst, 1, 0.0f, 1.0f, 0.0f, 0.0f);
			Math::Mat4SetRow(dst, 2, 0.0f, 0.0f, 1.0f, 0.0f);
			Math::Mat4SetRow(dst, 3, 0.0f, 0.0f, 0.0f, 1.0f);
		}
		void Mat4Set(_matrix4Type& dst, FXD::F32 m00, FXD::F32 m01, FXD::F32 m02, FXD::F32 m03, FXD::F32 m10, FXD::F32 m11, FXD::F32 m12, FXD::F32 m13, FXD::F32 m20, FXD::F32 m21, FXD::F32 m22, FXD::F32 m23, FXD::F32 m30, FXD::F32 m31, FXD::F32 m32, FXD::F32 m33)
		{
			Math::Mat4SetRow(dst, 0, m00, m01, m02, m03);
			Math::Mat4SetRow(dst, 1, m10, m11, m12, m13);
			Math::Mat4SetRow(dst, 2, m20, m21, m22, m23);
			Math::Mat4SetRow(dst, 3, m30, m31, m32, m33);
		}
		bool Mat4Compare(const _matrix4Type& lhs, const _matrix4Type& rhs)
		{
			return
				(Math::Vec4FCompare(lhs.mtrx4[0], rhs.mtrx4[0])) &&
				(Math::Vec4FCompare(lhs.mtrx4[1], rhs.mtrx4[1])) &&
				(Math::Vec4FCompare(lhs.mtrx4[2], rhs.mtrx4[2])) &&
				(Math::Vec4FCompare(lhs.mtrx4[3], rhs.mtrx4[3]));
		}
		void Mat4Copy(_matrix4Type& dst, const _matrix4Type& src)
		{
			Math::Vec4FCopy(dst.mtrx4[0], src.mtrx4[0]);
			Math::Vec4FCopy(dst.mtrx4[1], src.mtrx4[1]);
			Math::Vec4FCopy(dst.mtrx4[2], src.mtrx4[2]);
			Math::Vec4FCopy(dst.mtrx4[3], src.mtrx4[3]);
		}
		void Mat4Copy(_matrix4Type& dst, const _matrix3Type& src)
		{
			Math::Mat4SetIdentity(dst);
			Math::Vec4FCopy(dst.mtrx4[0], src.mtrx3[0]);
			Math::Vec4FCopy(dst.mtrx4[1], src.mtrx3[1]);
			Math::Vec4FCopy(dst.mtrx4[2], src.mtrx3[2]);
		}

		_vector4FType Mat4Dot4(const _matrix4Type& mat, const _vector4FType& vec)
		{
			_vector4FType m0, m1, m2, m3;
			Math::Vec4FMul(m0, mat.mtrx4[0], Math::Vec4FGetX(vec));
			Math::Vec4FMul(m1, mat.mtrx4[1], Math::Vec4FGetY(vec));
			Math::Vec4FMul(m2, mat.mtrx4[2], Math::Vec4FGetZ(vec));
			Math::Vec4FMul(m3, mat.mtrx4[3], Math::Vec4FGetW(vec));

			_vector4FType res;
			Math::Vec4FSetX(res, Math::Vec4FGetX(m0) + Math::Vec4FGetX(m1) + Math::Vec4FGetX(m2) + Math::Vec4FGetX(m3));
			Math::Vec4FSetY(res, Math::Vec4FGetY(m0) + Math::Vec4FGetY(m1) + Math::Vec4FGetY(m2) + Math::Vec4FGetY(m3));
			Math::Vec4FSetZ(res, Math::Vec4FGetZ(m0) + Math::Vec4FGetZ(m1) + Math::Vec4FGetZ(m2) + Math::Vec4FGetZ(m3));
			Math::Vec4FSetW(res, Math::Vec4FGetW(m0) + Math::Vec4FGetW(m1) + Math::Vec4FGetW(m2) + Math::Vec4FGetW(m3));
			return res;
		}

		void Mat4Add(_matrix4Type& dst, const _matrix4Type& lhs, const _matrix4Type& rhs)
		{
			Math::Vec4FAdd(dst.mtrx4[0], lhs.mtrx4[0], rhs.mtrx4[0]);
			Math::Vec4FAdd(dst.mtrx4[1], lhs.mtrx4[1], rhs.mtrx4[1]);
			Math::Vec4FAdd(dst.mtrx4[2], lhs.mtrx4[2], rhs.mtrx4[2]);
			Math::Vec4FAdd(dst.mtrx4[3], lhs.mtrx4[3], rhs.mtrx4[3]);
		}
		void Mat4Sub(_matrix4Type& dst, const _matrix4Type& lhs, const _matrix4Type& rhs)
		{
			Math::Vec4FSub(dst.mtrx4[0], lhs.mtrx4[0], rhs.mtrx4[0]);
			Math::Vec4FSub(dst.mtrx4[1], lhs.mtrx4[1], rhs.mtrx4[1]);
			Math::Vec4FSub(dst.mtrx4[2], lhs.mtrx4[2], rhs.mtrx4[2]);
			Math::Vec4FSub(dst.mtrx4[3], lhs.mtrx4[3], rhs.mtrx4[3]);
		}
		void Mat4Mul(_matrix4Type& dst, const _matrix4Type& lhs, const _matrix4Type& rhs)
		{
			dst.mtrx4[0] = Mat4Dot4(rhs, lhs.mtrx4[0]);
			dst.mtrx4[1] = Mat4Dot4(rhs, lhs.mtrx4[1]);
			dst.mtrx4[2] = Mat4Dot4(rhs, lhs.mtrx4[2]);
			dst.mtrx4[3] = Mat4Dot4(rhs, lhs.mtrx4[3]);
		}
		void Mat4Mul(_vector3FType& dst, const _matrix4Type& lhs, const _vector3FType& rhs)
		{
			_vector4FType row0, row1, row2, row3;
			Math::Mat4GetRow(row0, lhs, 0);
			Math::Mat4GetRow(row1, lhs, 1);
			Math::Mat4GetRow(row2, lhs, 2);
			Math::Mat4GetRow(row3, lhs, 3);

			_vector3FType row33;
			Math::Vec3FCopy(row33, row3);

			const FXD::F32 fInvW = (1.0f / (Vec3FDot(row33, rhs) + Math::Vec4FGetW(row3)));
			_vector4FType rhs4;
			Math::Vec4FSet(rhs4, Math::Vec3FGetX(rhs), Math::Vec3FGetY(rhs), Math::Vec3FGetZ(rhs), fInvW);

			Math::Vec3FSetX(dst, Math::Vec4FDot(row0, rhs4));
			Math::Vec3FSetY(dst, Math::Vec4FDot(row1, rhs4));
			Math::Vec3FSetZ(dst, Math::Vec4FDot(row2, rhs4));
		}
		void Mat4Mul(_vector4FType& dst, const _matrix4Type& lhs, const _vector4FType& rhs)
		{
			_vector4FType row0, row1, row2, row3;
			Math::Mat4GetRow(row0, lhs, 0);
			Math::Mat4GetRow(row1, lhs, 1);
			Math::Mat4GetRow(row2, lhs, 2);
			Math::Mat4GetRow(row3, lhs, 3);

			Math::Vec4FSetX(dst, Math::Vec4FDot(row0, rhs));
			Math::Vec4FSetY(dst, Math::Vec4FDot(row1, rhs));
			Math::Vec4FSetZ(dst, Math::Vec4FDot(row2, rhs));
			Math::Vec4FSetW(dst, Math::Vec4FDot(row3, rhs));
		}

		FXD::F32 Mat4Detrament(const _matrix4Type& src)
		{
			const FXD::F32 fCofactor00 = Math::Vec4FGetX(src.mtrx4[0]) * (
				Math::Vec4FGetY(src.mtrx4[1]) * (Math::Vec4FGetZ(src.mtrx4[2]) * Math::Vec4FGetW(src.mtrx4[3]) - Math::Vec4FGetW(src.mtrx4[2]) * Math::Vec4FGetZ(src.mtrx4[3])) -
				Math::Vec4FGetY(src.mtrx4[2]) * (Math::Vec4FGetZ(src.mtrx4[1]) * Math::Vec4FGetW(src.mtrx4[3]) - Math::Vec4FGetW(src.mtrx4[1]) * Math::Vec4FGetZ(src.mtrx4[3])) +
				Math::Vec4FGetY(src.mtrx4[3]) * (Math::Vec4FGetZ(src.mtrx4[1]) * Math::Vec4FGetW(src.mtrx4[2]) - Math::Vec4FGetW(src.mtrx4[1]) * Math::Vec4FGetZ(src.mtrx4[2]))
				);
			const FXD::F32 fCofactor01 = Math::Vec4FGetX(src.mtrx4[1]) * (
				Math::Vec4FGetY(src.mtrx4[0]) * (Math::Vec4FGetZ(src.mtrx4[2]) * Math::Vec4FGetW(src.mtrx4[3]) - Math::Vec4FGetW(src.mtrx4[2]) * Math::Vec4FGetZ(src.mtrx4[3])) -
				Math::Vec4FGetY(src.mtrx4[2]) * (Math::Vec4FGetZ(src.mtrx4[0]) * Math::Vec4FGetW(src.mtrx4[3]) - Math::Vec4FGetW(src.mtrx4[0]) * Math::Vec4FGetZ(src.mtrx4[3])) +
				Math::Vec4FGetY(src.mtrx4[3]) * (Math::Vec4FGetZ(src.mtrx4[0]) * Math::Vec4FGetW(src.mtrx4[2]) - Math::Vec4FGetW(src.mtrx4[0]) * Math::Vec4FGetZ(src.mtrx4[2]))
				);
			const FXD::F32 fCofactor02 = Math::Vec4FGetX(src.mtrx4[2]) * (
				Math::Vec4FGetY(src.mtrx4[0]) * (Math::Vec4FGetZ(src.mtrx4[1]) * Math::Vec4FGetW(src.mtrx4[3]) - Math::Vec4FGetW(src.mtrx4[1]) * Math::Vec4FGetZ(src.mtrx4[3])) -
				Math::Vec4FGetY(src.mtrx4[1]) * (Math::Vec4FGetZ(src.mtrx4[0]) * Math::Vec4FGetW(src.mtrx4[3]) - Math::Vec4FGetW(src.mtrx4[0]) * Math::Vec4FGetZ(src.mtrx4[3])) +
				Math::Vec4FGetY(src.mtrx4[3]) * (Math::Vec4FGetZ(src.mtrx4[0]) * Math::Vec4FGetW(src.mtrx4[1]) - Math::Vec4FGetW(src.mtrx4[0]) * Math::Vec4FGetZ(src.mtrx4[1]))
				);
			const FXD::F32 fCofactor03 = Math::Vec4FGetX(src.mtrx4[3]) * (
				Math::Vec4FGetY(src.mtrx4[0]) * (Math::Vec4FGetZ(src.mtrx4[1]) * Math::Vec4FGetW(src.mtrx4[2]) - Math::Vec4FGetW(src.mtrx4[1]) * Math::Vec4FGetZ(src.mtrx4[2])) -
				Math::Vec4FGetY(src.mtrx4[1]) * (Math::Vec4FGetZ(src.mtrx4[0]) * Math::Vec4FGetW(src.mtrx4[2]) - Math::Vec4FGetW(src.mtrx4[0]) * Math::Vec4FGetZ(src.mtrx4[2])) +
				Math::Vec4FGetY(src.mtrx4[2]) * (Math::Vec4FGetZ(src.mtrx4[0]) * Math::Vec4FGetW(src.mtrx4[1]) - Math::Vec4FGetW(src.mtrx4[0]) * Math::Vec4FGetZ(src.mtrx4[1]))
				);

			return (fCofactor00 - fCofactor01 + fCofactor02 - fCofactor03);
		}
		void Mat4Negate(_matrix4Type& dst)
		{
			Math::Vec4FNegate(dst.mtrx4[0]);
			Math::Vec4FNegate(dst.mtrx4[1]);
			Math::Vec4FNegate(dst.mtrx4[2]);
			Math::Vec4FNegate(dst.mtrx4[3]);
		}
		void Mat4Transpose(_matrix4Type& dst, const _matrix4Type& src)
		{
			Math::Mat4Set(dst,
				src.mtrx4[0].vector4[0], src.mtrx4[1].vector4[0], src.mtrx4[2].vector4[0], src.mtrx4[3].vector4[0],
				src.mtrx4[0].vector4[1], src.mtrx4[1].vector4[1], src.mtrx4[2].vector4[1], src.mtrx4[3].vector4[1],
				src.mtrx4[0].vector4[2], src.mtrx4[1].vector4[2], src.mtrx4[2].vector4[2], src.mtrx4[3].vector4[2],
				src.mtrx4[0].vector4[3], src.mtrx4[1].vector4[3], src.mtrx4[2].vector4[3], src.mtrx4[3].vector4[3]
			);
		}
		void Mat4Inverse(_matrix4Type& dst, const _matrix4Type& src)
		{
			_matrix4Type tmp;
			Math::Mat4SetRow(tmp, 0,
				(Math::Vec4FGetZ(src.mtrx4[2]) * Math::Vec4FGetW(src.mtrx4[3]) - Math::Vec4FGetW(src.mtrx4[2]) * Math::Vec4FGetZ(src.mtrx4[3])),
				(Math::Vec4FGetZ(src.mtrx4[1]) * Math::Vec4FGetW(src.mtrx4[3]) - Math::Vec4FGetW(src.mtrx4[1]) * Math::Vec4FGetZ(src.mtrx4[3])),
				(Math::Vec4FGetZ(src.mtrx4[1]) * Math::Vec4FGetW(src.mtrx4[2]) - Math::Vec4FGetW(src.mtrx4[1]) * Math::Vec4FGetZ(src.mtrx4[2])),
				0.0f);
			Math::Mat4SetRow(tmp, 1,
				(Math::Vec4FGetZ(src.mtrx4[2]) * Math::Vec4FGetW(src.mtrx4[3]) - Math::Vec4FGetW(src.mtrx4[2]) * Math::Vec4FGetZ(src.mtrx4[3])),
				(Math::Vec4FGetZ(src.mtrx4[0]) * Math::Vec4FGetW(src.mtrx4[3]) - Math::Vec4FGetW(src.mtrx4[0]) * Math::Vec4FGetZ(src.mtrx4[3])),
				(Math::Vec4FGetZ(src.mtrx4[0]) * Math::Vec4FGetW(src.mtrx4[2]) - Math::Vec4FGetW(src.mtrx4[0]) * Math::Vec4FGetZ(src.mtrx4[2])),
				0.0f);
			Math::Mat4SetRow(tmp, 2,
				(Math::Vec4FGetZ(src.mtrx4[1]) * Math::Vec4FGetW(src.mtrx4[3]) - Math::Vec4FGetW(src.mtrx4[1]) * Math::Vec4FGetZ(src.mtrx4[3])),
				(Math::Vec4FGetZ(src.mtrx4[0]) * Math::Vec4FGetW(src.mtrx4[3]) - Math::Vec4FGetW(src.mtrx4[0]) * Math::Vec4FGetZ(src.mtrx4[3])),
				(Math::Vec4FGetZ(src.mtrx4[0]) * Math::Vec4FGetW(src.mtrx4[1]) - Math::Vec4FGetW(src.mtrx4[0]) * Math::Vec4FGetZ(src.mtrx4[1])),
				0.0f);
			Math::Mat4SetRow(tmp, 3,
				(Math::Vec4FGetZ(src.mtrx4[1]) * Math::Vec4FGetW(src.mtrx4[2]) - Math::Vec4FGetW(src.mtrx4[1]) * Math::Vec4FGetZ(src.mtrx4[2])),
				(Math::Vec4FGetZ(src.mtrx4[0]) * Math::Vec4FGetW(src.mtrx4[2]) - Math::Vec4FGetW(src.mtrx4[0]) * Math::Vec4FGetZ(src.mtrx4[2])),
				(Math::Vec4FGetZ(src.mtrx4[0]) * Math::Vec4FGetW(src.mtrx4[1]) - Math::Vec4FGetW(src.mtrx4[0]) * Math::Vec4FGetZ(src.mtrx4[1])),
				0.0f);

			FXD::F32 fDet[4];
			fDet[0] = Math::Vec4FGetY(src.mtrx4[1]) * Math::Vec4FGetX(tmp.mtrx4[0]) - Math::Vec4FGetY(src.mtrx4[2]) * Math::Vec4FGetY(tmp.mtrx4[0]) + Math::Vec4FGetY(src.mtrx4[3]) * Math::Vec4FGetZ(tmp.mtrx4[0]);
			fDet[1] = Math::Vec4FGetY(src.mtrx4[0]) * Math::Vec4FGetX(tmp.mtrx4[1]) - Math::Vec4FGetY(src.mtrx4[2]) * Math::Vec4FGetY(tmp.mtrx4[1]) + Math::Vec4FGetY(src.mtrx4[3]) * Math::Vec4FGetZ(tmp.mtrx4[1]);
			fDet[2] = Math::Vec4FGetY(src.mtrx4[0]) * Math::Vec4FGetX(tmp.mtrx4[2]) - Math::Vec4FGetY(src.mtrx4[1]) * Math::Vec4FGetY(tmp.mtrx4[2]) + Math::Vec4FGetY(src.mtrx4[3]) * Math::Vec4FGetZ(tmp.mtrx4[2]);
			fDet[3] = Math::Vec4FGetY(src.mtrx4[0]) * Math::Vec4FGetX(tmp.mtrx4[3]) - Math::Vec4FGetY(src.mtrx4[1]) * Math::Vec4FGetY(tmp.mtrx4[3]) + Math::Vec4FGetY(src.mtrx4[2]) * Math::Vec4FGetZ(tmp.mtrx4[3]);

			const FXD::F32 fDeterminant = Math::Vec4FGetX(src.mtrx4[0]) * fDet[0] - Math::Vec4FGetX(src.mtrx4[1]) * fDet[1] + Math::Vec4FGetX(src.mtrx4[2]) * fDet[2] - Math::Vec4FGetX(src.mtrx4[3]) * fDet[3];
			const FXD::F32 fRDet = (1.0f / fDeterminant);

			Mat4SetRow(dst, 0,
				( fRDet * fDet[0]),
				(-fRDet * fDet[1]),
				( fRDet * fDet[2]),
				(-fRDet * fDet[3])
			);
			Math::Mat4SetRow(dst, 1,
				(-fRDet * (Math::Vec4FGetX(src.mtrx4[1]) * Math::Vec4FGetX(tmp.mtrx4[0]) - Math::Vec4FGetX(src.mtrx4[2]) * Math::Vec4FGetY(tmp.mtrx4[0]) + Math::Vec4FGetX(src.mtrx4[3]) * Math::Vec4FGetZ(tmp.mtrx4[0]))),
				( fRDet * (Math::Vec4FGetX(src.mtrx4[0]) * Math::Vec4FGetX(tmp.mtrx4[1]) - Math::Vec4FGetX(src.mtrx4[2]) * Math::Vec4FGetY(tmp.mtrx4[1]) + Math::Vec4FGetX(src.mtrx4[3]) * Math::Vec4FGetZ(tmp.mtrx4[1]))),
				(-fRDet * (Math::Vec4FGetX(src.mtrx4[0]) * Math::Vec4FGetX(tmp.mtrx4[2]) - Math::Vec4FGetX(src.mtrx4[1]) * Math::Vec4FGetY(tmp.mtrx4[2]) + Math::Vec4FGetX(src.mtrx4[3]) * Math::Vec4FGetZ(tmp.mtrx4[2]))),
				( fRDet * (Math::Vec4FGetX(src.mtrx4[0]) * Math::Vec4FGetX(tmp.mtrx4[3]) - Math::Vec4FGetX(src.mtrx4[1]) * Math::Vec4FGetY(tmp.mtrx4[3]) + Math::Vec4FGetX(src.mtrx4[2]) * Math::Vec4FGetZ(tmp.mtrx4[3])))
			);
			Math::Mat4SetRow(dst, 2,
				(fRDet * (
				Math::Vec4FGetX(src.mtrx4[1]) * (Math::Vec4FGetY(src.mtrx4[2]) * Math::Vec4FGetW(src.mtrx4[3]) - Math::Vec4FGetW(src.mtrx4[2]) * Math::Vec4FGetY(src.mtrx4[3])) -
				Math::Vec4FGetX(src.mtrx4[2]) * (Math::Vec4FGetY(src.mtrx4[1]) * Math::Vec4FGetW(src.mtrx4[3]) - Math::Vec4FGetW(src.mtrx4[1]) * Math::Vec4FGetY(src.mtrx4[3])) +
				Math::Vec4FGetX(src.mtrx4[3]) * (Math::Vec4FGetY(src.mtrx4[1]) * Math::Vec4FGetW(src.mtrx4[2]) - Math::Vec4FGetW(src.mtrx4[1]) * Math::Vec4FGetY(src.mtrx4[2])))),
				(-fRDet * (
				Math::Vec4FGetX(src.mtrx4[0]) * (Math::Vec4FGetY(src.mtrx4[2]) * Math::Vec4FGetW(src.mtrx4[3]) - Math::Vec4FGetW(src.mtrx4[2]) * Math::Vec4FGetY(src.mtrx4[3])) -
				Math::Vec4FGetX(src.mtrx4[2]) * (Math::Vec4FGetY(src.mtrx4[0]) * Math::Vec4FGetW(src.mtrx4[3]) - Math::Vec4FGetW(src.mtrx4[0]) * Math::Vec4FGetY(src.mtrx4[3])) +
				Math::Vec4FGetX(src.mtrx4[3]) * (Math::Vec4FGetY(src.mtrx4[0]) * Math::Vec4FGetW(src.mtrx4[2]) - Math::Vec4FGetW(src.mtrx4[0]) * Math::Vec4FGetY(src.mtrx4[2])))),
				(fRDet * (
				Math::Vec4FGetX(src.mtrx4[0]) * (Math::Vec4FGetY(src.mtrx4[1]) * Math::Vec4FGetW(src.mtrx4[3]) - Math::Vec4FGetW(src.mtrx4[1]) * Math::Vec4FGetY(src.mtrx4[3])) -
				Math::Vec4FGetX(src.mtrx4[1]) * (Math::Vec4FGetY(src.mtrx4[0]) * Math::Vec4FGetW(src.mtrx4[3]) - Math::Vec4FGetW(src.mtrx4[0]) * Math::Vec4FGetY(src.mtrx4[3])) +
				Math::Vec4FGetX(src.mtrx4[3]) * (Math::Vec4FGetY(src.mtrx4[0]) * Math::Vec4FGetW(src.mtrx4[1]) - Math::Vec4FGetW(src.mtrx4[0]) * Math::Vec4FGetY(src.mtrx4[1])))),
				(-fRDet * (
				Math::Vec4FGetX(src.mtrx4[0]) * (Math::Vec4FGetY(src.mtrx4[1]) * Math::Vec4FGetW(src.mtrx4[2]) - Math::Vec4FGetW(src.mtrx4[1]) * Math::Vec4FGetY(src.mtrx4[2])) -
				Math::Vec4FGetX(src.mtrx4[1]) * (Math::Vec4FGetY(src.mtrx4[0]) * Math::Vec4FGetW(src.mtrx4[2]) - Math::Vec4FGetW(src.mtrx4[0]) * Math::Vec4FGetY(src.mtrx4[2])) +
				Math::Vec4FGetX(src.mtrx4[2]) * (Math::Vec4FGetY(src.mtrx4[0]) * Math::Vec4FGetW(src.mtrx4[1]) - Math::Vec4FGetW(src.mtrx4[0]) * Math::Vec4FGetY(src.mtrx4[1]))))
			);
			Math::Mat4SetRow(dst, 3,
				(-fRDet * (
				Math::Vec4FGetX(src.mtrx4[1]) * (Math::Vec4FGetY(src.mtrx4[2]) * Math::Vec4FGetZ(src.mtrx4[3]) - Math::Vec4FGetZ(src.mtrx4[2]) * Math::Vec4FGetY(src.mtrx4[3])) -
				Math::Vec4FGetX(src.mtrx4[2]) * (Math::Vec4FGetY(src.mtrx4[1]) * Math::Vec4FGetZ(src.mtrx4[3]) - Math::Vec4FGetZ(src.mtrx4[1]) * Math::Vec4FGetY(src.mtrx4[3])) +
				Math::Vec4FGetX(src.mtrx4[3]) * (Math::Vec4FGetY(src.mtrx4[1]) * Math::Vec4FGetZ(src.mtrx4[2]) - Math::Vec4FGetZ(src.mtrx4[1]) * Math::Vec4FGetY(src.mtrx4[2])))),
				(fRDet * (
				Math::Vec4FGetX(src.mtrx4[0]) * (Math::Vec4FGetY(src.mtrx4[2]) * Math::Vec4FGetZ(src.mtrx4[3]) - Math::Vec4FGetZ(src.mtrx4[2]) * Math::Vec4FGetY(src.mtrx4[3])) -
				Math::Vec4FGetX(src.mtrx4[2]) * (Math::Vec4FGetY(src.mtrx4[0]) * Math::Vec4FGetZ(src.mtrx4[3]) - Math::Vec4FGetZ(src.mtrx4[0]) * Math::Vec4FGetY(src.mtrx4[3])) +
				Math::Vec4FGetX(src.mtrx4[3]) * (Math::Vec4FGetY(src.mtrx4[0]) * Math::Vec4FGetZ(src.mtrx4[2]) - Math::Vec4FGetZ(src.mtrx4[0]) * Math::Vec4FGetY(src.mtrx4[2])))),
				(-fRDet * (
				Math::Vec4FGetX(src.mtrx4[0]) * (Math::Vec4FGetY(src.mtrx4[1]) * Math::Vec4FGetZ(src.mtrx4[3]) - Math::Vec4FGetZ(src.mtrx4[1]) * Math::Vec4FGetY(src.mtrx4[3])) -
				Math::Vec4FGetX(src.mtrx4[1]) * (Math::Vec4FGetY(src.mtrx4[0]) * Math::Vec4FGetZ(src.mtrx4[3]) - Math::Vec4FGetZ(src.mtrx4[0]) * Math::Vec4FGetY(src.mtrx4[3])) +
				Math::Vec4FGetX(src.mtrx4[3]) * (Math::Vec4FGetY(src.mtrx4[0]) * Math::Vec4FGetZ(src.mtrx4[1]) - Math::Vec4FGetZ(src.mtrx4[0]) * Math::Vec4FGetY(src.mtrx4[1])))),
				(fRDet * (
				Math::Vec4FGetX(src.mtrx4[0]) * (Math::Vec4FGetY(src.mtrx4[1]) * Math::Vec4FGetZ(src.mtrx4[2]) - Math::Vec4FGetZ(src.mtrx4[1]) * Math::Vec4FGetY(src.mtrx4[2])) -
				Math::Vec4FGetX(src.mtrx4[1]) * (Math::Vec4FGetY(src.mtrx4[0]) * Math::Vec4FGetZ(src.mtrx4[2]) - Math::Vec4FGetZ(src.mtrx4[0]) * Math::Vec4FGetY(src.mtrx4[2])) +
				Math::Vec4FGetX(src.mtrx4[2]) * (Math::Vec4FGetY(src.mtrx4[0]) * Math::Vec4FGetZ(src.mtrx4[1]) - Math::Vec4FGetZ(src.mtrx4[0]) * Math::Vec4FGetY(src.mtrx4[1]))))
			);
		}
		void Mat4SetRow(_matrix4Type& dst, FXD::U32 nRow, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ, FXD::F32 fW)
		{
			_vector4FType vec;
			Math::Vec4FSet(vec, fX, fY, fZ, fW);
			Math::Vec4FCopy(dst.mtrx4[nRow], vec);
		}
		void Mat4SetRow(_matrix4Type& dst, FXD::U32 nRow, const _vector4FType& vec)
		{
			Math::Vec4FCopy(dst.mtrx4[nRow], vec);
		}
		void Mat4SetColumn(_matrix4Type& dst, FXD::U32 nCol, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ, FXD::F32 fW)
		{
			Math::Vec4FSetIndex(dst.mtrx4[0], nCol, fX);
			Math::Vec4FSetIndex(dst.mtrx4[1], nCol, fY);
			Math::Vec4FSetIndex(dst.mtrx4[2], nCol, fZ);
			Math::Vec4FSetIndex(dst.mtrx4[3], nCol, fW);
		}
		void Mat4SetColumn(_matrix4Type& dst, FXD::U32 nCol, const _vector4FType& vec)
		{
			Math::Vec4FSetIndex(dst.mtrx4[0], nCol, Math::Vec4FGetX(vec));
			Math::Vec4FSetIndex(dst.mtrx4[1], nCol, Math::Vec4FGetY(vec));
			Math::Vec4FSetIndex(dst.mtrx4[2], nCol, Math::Vec4FGetZ(vec));
			Math::Vec4FSetIndex(dst.mtrx4[3], nCol, Math::Vec4FGetW(vec));
		}
		void Mat4SetTranslation(_matrix4Type& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
		{
			_vector4FType vec;
			Math::Vec4FSet(vec, fX, fY, fZ, 1.0f);
			Math::Mat4SetIdentity(dst);
			Math::Mat4SetRow(dst, 3, vec);
		}
		void Mat4SetTranslation(_matrix4Type& dst, const _vector3FType& vec)
		{
			Math::Mat4SetTranslation(dst, Math::Vec3FGetX(vec), Math::Vec3FGetY(vec), Math::Vec3FGetZ(vec));
		}
		void Mat4SetRight(_matrix4Type& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
		{
			_vector4FType vec;
			Math::Vec4FSet(vec, fX, fY, fZ, 0.0f);
			Math::Mat4SetIdentity(dst);
			Math::Mat4SetColumn(dst, 0, vec);
		}
		void Mat4SetRight(_matrix4Type& dst, const _vector3FType& vec)
		{
			Math::Mat4SetRight(dst, Math::Vec3FGetX(vec), Math::Vec3FGetY(vec), Math::Vec3FGetZ(vec));
		}
		void Mat4SetUp(_matrix4Type& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
		{
			_vector4FType vec;
			Math::Vec4FSet(vec, fX, fY, fZ, 0.0f);
			Math::Mat4SetIdentity(dst);
			Math::Mat4SetColumn(dst, 1, vec);
		}
		void Mat4SetUp(_matrix4Type& dst, const _vector3FType& vec)
		{
			Math::Mat4SetUp(dst, Math::Vec3FGetX(vec), Math::Vec3FGetY(vec), Math::Vec3FGetZ(vec));
		}
		void Mat4SetForward(_matrix4Type& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
		{
			_vector4FType vec;
			Math::Vec4FSet(vec, fX, fY, fZ, 0.0f);
			Math::Mat4SetIdentity(dst);
			Math::Mat4SetColumn(dst, 2, vec);
		}
		void Mat4SetForward(_matrix4Type& dst, const _vector3FType& vec)
		{
			Math::Mat4SetForward(dst, Math::Vec3FGetX(vec), Math::Vec3FGetY(vec), Math::Vec3FGetZ(vec));
		}
		void Mat4SetScale(_matrix4Type& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ)
		{
			Math::Mat4SetRow(dst, 0, fX, 0.0f, 0.0f, 0.0f);
			Math::Mat4SetRow(dst, 1, 0.0f, fY, 0.0f, 0.0f);
			Math::Mat4SetRow(dst, 2, 0.0f, 0.0f, fZ, 0.0f);
			Math::Mat4SetRow(dst, 3, 0.0f, 0.0f, 0.0f, 1.0f);
		}
		void Mat4SetScale(_matrix4Type& dst, const _vector3FType& vec)
		{
			Math::Mat4SetScale(dst, Math::Vec3FGetX(vec), Math::Vec3FGetY(vec), Math::Vec3FGetZ(vec));
		}
		void Mat4SetRotationX(_matrix4Type& dst, FXD::F32 fAngleX)
		{
			const FXD::F32 fSinAngle = Math::Sin(fAngleX);
			const FXD::F32 fCosAngle = Math::Cos(fAngleX);

			Math::Mat4SetRow(dst, 0, 1.0f, 0.0f, 0.0f, 0.0f);
			Math::Mat4SetRow(dst, 1, 0.0f, fCosAngle, fSinAngle, 0.0f);
			Math::Mat4SetRow(dst, 2, 0.0f, -fSinAngle, fCosAngle, 0.0f);
			Math::Mat4SetRow(dst, 3, 0.0f, 0.0f, 0.0f, 1.0f);
		}
		void Mat4SetRotationY(_matrix4Type& dst, FXD::F32 fAngleY)
		{
			const FXD::F32 fSinAngle = Math::Sin(fAngleY);
			const FXD::F32 fCosAngle = Math::Cos(fAngleY);

			Math::Mat4SetRow(dst, 0, fCosAngle, 0.0f, -fSinAngle, 0.0f);
			Math::Mat4SetRow(dst, 1, 0.0f, 1.0f, 0.0f, 0.0f);
			Math::Mat4SetRow(dst, 2, fSinAngle, 0.0f, fCosAngle, 0.0f);
			Math::Mat4SetRow(dst, 3, 0.0f, 0.0f, 0.0f, 1.0f);
		}
		void Mat4SetRotationZ(_matrix4Type& dst, FXD::F32 fAngleZ)
		{
			const FXD::F32 fSinAngle = Math::Sin(fAngleZ);
			const FXD::F32 fCosAngle = Math::Cos(fAngleZ);

			Math::Mat4SetRow(dst, 0, fCosAngle, fSinAngle, 0.0f, 0.0f);
			Math::Mat4SetRow(dst, 1, -fSinAngle, fCosAngle, 0.0f, 0.0f);
			Math::Mat4SetRow(dst, 2, 0.0f, 0.0f, 1.0f, 0.0f);
			Math::Mat4SetRow(dst, 3, 0.0f, 0.0f, 0.0f, 1.0f);
		}
		void Mat4SetRotationPitchYawRoll(_matrix4Type& dst, const _vector3FType& angle, Math::EulerAngleOrder eOrder)
		{
			_vector4FType quat;
			Math::QuatFRotationPitchYawRollV(quat, angle, eOrder);
			Math::Mat4SetRotationQuaternion(dst, quat);
		}
		void Mat4SetRotationPitchYawRoll(_matrix4Type& dst, FXD::F32 fPitch, FXD::F32 fYaw, FXD::F32 fRoll, Math::EulerAngleOrder eOrder)
		{
			_vector3FType angle;
			Math::Vec3FSet(angle, fPitch, fYaw, fRoll);
			Math::Mat4SetRotationPitchYawRoll(dst, angle, eOrder);
		}
		void Mat4SetRotationQuaternion(_matrix4Type& dst, const _vector4FType& quat)
		{
			_matrix3Type mat3;
			Math::QuatFToRotation(mat3, quat);
			Math::Mat4Copy(dst, mat3);
		}
		void FXMMatrixLookToLH(_matrix4Type& dst, const _vector3FType& eye, const _vector3FType& forward, const _vector3FType& up)
		{
			/*
			assert(Vec3FCompare(forward, XMVectorZero()) == false);
			assert(XMVector3IsInfinite(forward) == false);
			assert(Vec3FCompare(up, XMVectorZero()) == false);
			assert(XMVector3IsInfinite(up) == false);
			*/

			_vector3FType R2;
			Math::Vec3FCopy(R2, forward);
			Math::Vec3FNormalise(R2);

			_vector3FType R0 = Vec3FCross(up, R2);
			Math::Vec3FNormalise(R0);

			_vector3FType R1 = Vec3FCross(R2, R0);

			_vector3FType NegEyePosition;
			Math::Vec3FCopy(NegEyePosition, eye);
			Math::Vec3FNegate(NegEyePosition);

			_matrix4Type matTemp;
			Math::Mat4SetIdentity(matTemp);
			Math::Mat4SetRow(matTemp, 0, Math::Vec3FGetX(R0), Math::Vec3FGetY(R0), Math::Vec3FGetZ(R0), Vec3FDot(R0, NegEyePosition));
			Math::Mat4SetRow(matTemp, 1, Math::Vec3FGetX(R1), Math::Vec3FGetY(R1), Math::Vec3FGetZ(R1), Vec3FDot(R1, NegEyePosition));
			Math::Mat4SetRow(matTemp, 2, Math::Vec3FGetX(R2), Math::Vec3FGetY(R2), Math::Vec3FGetZ(R2), Vec3FDot(R2, NegEyePosition));

			Math::Mat4Transpose(dst, matTemp);
		}
		void Mat4SetTRS(_matrix4Type& dst, const _vector3FType& translation, const _vector4FType& rotation, const _vector3FType& scale)
		{
			_matrix3Type rot3x3;
			Math::QuatFToRotation(rot3x3, rotation);

			Math::Mat4SetRow(dst, 0, Math::Vec3FGetX(scale) * Math::Vec3FGetX(rot3x3.mtrx3[0]), Math::Vec3FGetY(scale) * Math::Vec3FGetY(rot3x3.mtrx3[0]), Math::Vec3FGetZ(scale) * Math::Vec3FGetZ(rot3x3.mtrx3[0]), Math::Vec3FGetX(translation));
			Math::Mat4SetRow(dst, 1, Math::Vec3FGetX(scale) * Math::Vec3FGetX(rot3x3.mtrx3[1]), Math::Vec3FGetY(scale) * Math::Vec3FGetY(rot3x3.mtrx3[1]), Math::Vec3FGetZ(scale) * Math::Vec3FGetZ(rot3x3.mtrx3[1]), Math::Vec3FGetY(translation));
			Math::Mat4SetRow(dst, 2, Math::Vec3FGetX(scale) * Math::Vec3FGetX(rot3x3.mtrx3[2]), Math::Vec3FGetY(scale) * Math::Vec3FGetY(rot3x3.mtrx3[2]), Math::Vec3FGetZ(scale) * Math::Vec3FGetZ(rot3x3.mtrx3[2]), Math::Vec3FGetZ(translation));
			Math::Mat4SetRow(dst, 3, 0.0f, 0.0f, 0.0f, 1.0f);
		}
		void Mat4LookAtLH(_matrix4Type& dst, const _vector3FType& eye, const _vector3FType& forward, const _vector3FType& up)
		{
			_vector3FType eyeDirection;
			Math::Vec3FSub(eyeDirection, forward, eye);
			FXMMatrixLookToLH(dst, eye, eyeDirection, up);
		}
		void Mat4PerspectiveLH(_matrix4Type& dst, FXD::F32 fViewWidth, FXD::F32 fViewHeight, FXD::F32 fNearZ, FXD::F32 fFarZ)
		{
			PRINT_COND_ASSERT((Math::ApproxEquals(fViewWidth, 0.0f, 0.00001f) == false), "Math:");
			PRINT_COND_ASSERT((Math::ApproxEquals(fViewHeight, 0.0f, 0.00001f) == false), "Math:");
			PRINT_COND_ASSERT((Math::ApproxEquals(fFarZ, fNearZ, 0.00001f) == false), "Math:");

			const FXD::F32 fTwoNearZ = (fNearZ + fNearZ);
			const FXD::F32 fRange = (fFarZ / (fFarZ - fNearZ));

			Math::Mat4SetRow(dst, 0, (fTwoNearZ / fViewWidth), 0.0f, 0.0f, 0.0f);
			Math::Mat4SetRow(dst, 1, 0.0f, (fTwoNearZ / fViewHeight), 0.0f, 0.0f);
			Math::Mat4SetRow(dst, 2, 0.0f, 0.0f, fRange, 1.0f);
			Math::Mat4SetRow(dst, 3, 0.0f, 0.0f, -fRange * fNearZ, 0.0f);
		}
		void Mat4PerspectiveFOVLH(_matrix4Type& dst, FXD::F32 fFOVAngleY, FXD::F32 fAspectHByW, FXD::F32 fNearZ, FXD::F32 fFarZ)
		{
			PRINT_COND_ASSERT((Math::ApproxEquals(fFOVAngleY, 0.0f, 0.00001f * 2.0f) == false), "Math:");
			PRINT_COND_ASSERT((Math::ApproxEquals(fAspectHByW, 0.0f, 0.00001f) == false), "Math:");
			PRINT_COND_ASSERT((Math::ApproxEquals(fFarZ, fNearZ, 0.00001f) == false), "Math:");

			const FXD::F32 fSinFov = Math::Sin(0.5f * fFOVAngleY);
			const FXD::F32 fCosFov = Math::Cos(0.5f * fFOVAngleY);

			const FXD::F32 fHeight = (fCosFov / fSinFov);
			const FXD::F32 fWidth = (fHeight / fAspectHByW);
			const FXD::F32 fRange = (fFarZ / (fFarZ - fNearZ));

			Math::Mat4SetRow(dst, 0, fWidth, 0.0f, 0.0f, 0.0f);
			Math::Mat4SetRow(dst, 1, 0.0f, fHeight, 0.0f, 0.0f);
			Math::Mat4SetRow(dst, 2, 0.0f, 0.0f, fRange, 1.0f);
			Math::Mat4SetRow(dst, 3, 0.0f, 0.0f, -fRange * fNearZ, 0.0f);
		}
		void Mat4OrthographicLH(_matrix4Type& dst, FXD::F32 fViewWidth, FXD::F32 fViewHeight, FXD::F32 fNearZ, FXD::F32 fFarZ)
		{
			PRINT_COND_ASSERT((Math::ApproxEquals(fViewWidth, 0.0f, 0.00001f) == false), "Math:");
			PRINT_COND_ASSERT((Math::ApproxEquals(fViewHeight, 0.0f, 0.00001f) == false), "Math:");
			PRINT_COND_ASSERT((Math::ApproxEquals(fFarZ, fNearZ, 0.00001f) == false), "Math:");

			const FXD::F32 fRange = (1.0f / (fFarZ - fNearZ));

			Math::Mat4SetRow(dst, 0, (2.0f / fViewWidth), 0.0f, 0.0f, 0.0f);
			Math::Mat4SetRow(dst, 1, 0.0f, (2.0f / fViewHeight), 0.0f, 0.0f);
			Math::Mat4SetRow(dst, 2, 0.0f, 0.0f, fRange, 1.0f);
			Math::Mat4SetRow(dst, 3, 0.0f, 0.0f, (-fRange * fNearZ), 0.0f);
		}

		void Mat4GetRow(_vector3FType& dst, const _matrix4Type& mat, FXD::U32 nRow)
		{
			Math::Vec3FCopy(dst, mat.mtrx4[nRow]);
		}
		void Mat4GetRow(_vector4FType& dst, const _matrix4Type& mat, FXD::U32 nRow)
		{
			Math::Vec4FCopy(dst, mat.mtrx4[nRow]);
		}
		void Mat4GetColumn(_vector3FType& dst, const _matrix4Type& mat, FXD::U32 nCol)
		{
			Math::Vec3FSet(dst, Math::Vec4FGetIndex(mat.mtrx4[0], nCol), Math::Vec4FGetIndex(mat.mtrx4[1], nCol), Math::Vec4FGetIndex(mat.mtrx4[2], nCol));
		}
		void Mat4GetColumn(_vector4FType& dst, const _matrix4Type& mat, FXD::U32 nCol)
		{
			Math::Vec4FSet(dst, Math::Vec4FGetIndex(mat.mtrx4[0], nCol), Math::Vec4FGetIndex(mat.mtrx4[1], nCol), Math::Vec4FGetIndex(mat.mtrx4[2], nCol), Math::Vec4FGetIndex(mat.mtrx4[3], nCol));
		}
		void Mat4GetTranslation(_vector3FType& dst, const _matrix4Type& mat)
		{
			Math::Mat4GetRow(dst, mat, 3);
		}
		void Mat4GetRight(_vector3FType& dst, const _matrix4Type& mat)
		{
			Math::Mat4GetColumn(dst, mat, 0);
		}
		void Mat4GetUp(_vector3FType& dst, const _matrix4Type& mat)
		{
			Math::Mat4GetColumn(dst, mat, 1);
		}
		void Mat4GetForward(_vector3FType& dst, const _matrix4Type& mat)
		{
			Math::Mat4GetColumn(dst, mat, 2);
		}
		void Mat4GetScale(_vector3FType& dst, const _matrix4Type& mat)
		{
			Math::Vec3FSet(dst, Math::Vec4FGetIndex(mat.mtrx4[0], 0), Math::Vec4FGetIndex(mat.mtrx4[1], 1), Math::Vec4FGetIndex(mat.mtrx4[2], 2));
		}

	} //namespace Math
} //namespace FXD
#endif //IsMathFP()