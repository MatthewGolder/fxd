// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_FP_MATHFP_H
#define FXDENGINE_MATH_FP_MATHFP_H

#if IsMathFP()
namespace FXD
{
	namespace Math
	{
		struct _vector2FType
		{
			FXD::F32 vector2[2];
		};
		struct _vector2IType
		{
			FXD::S32 vector2[2];
		};
		struct _vector3FType
		{
			FXD::F32 vector3[3];
		};
		struct _vector4FType
		{
			FXD::F32 vector4[4];
		};
		struct _matrix3Type
		{
			_vector3FType mtrx3[3];
		};
		struct _matrix4Type
		{
			_vector4FType mtrx4[4];
		};

	} //namespace Math
} //namespace FXD
#endif //IsMathFP()
#endif //FXDENGINE_MATH_FP_MATHFP_H