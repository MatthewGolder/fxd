// Creator - MatthewGolder
#include "FXDEngine/Math/Types.h"
#include "FXDEngine/Math/Math.h"

#if IsMathFP()
namespace FXD
{
	namespace Math
	{
		void Vec4FSet(_vector4FType& dst, FXD::F32 x, FXD::F32 y, FXD::F32 z, FXD::F32 w)
		{
			dst.vector4[0] = x;
			dst.vector4[1] = y;
			dst.vector4[2] = z;
			dst.vector4[3] = w;
		}
		void Vec4FSet(_vector4FType& dst, FXD::F32 fVal)
		{
			Math::Vec4FSet(dst, fVal, fVal, fVal, fVal);
		}

		void Vec4FSetIndex(_vector4FType& dst, FXD::U32 nID, FXD::F32 fVal)
		{
			dst.vector4[nID] = fVal;
		}
		void Vec4FSetX(_vector4FType& dst, FXD::F32 fVal)
		{
			Math::Vec4FSetIndex(dst, 0, fVal);
		}
		void Vec4FSetY(_vector4FType& dst, FXD::F32 fVal)
		{
			Math::Vec4FSetIndex(dst, 1, fVal);
		}
		void Vec4FSetZ(_vector4FType& dst, FXD::F32 fVal)
		{
			Math::Vec4FSetIndex(dst, 2, fVal);
		}
		void Vec4FSetW(_vector4FType& dst, FXD::F32 fVal)
		{
			Math::Vec4FSetIndex(dst, 3, fVal);
		}

		FXD::F32 Vec4FGetIndex(const _vector4FType& vec, FXD::U32 nID)
		{
			return vec.vector4[nID];
		}
		FXD::F32 Vec4FGetX(const _vector4FType& vec)
		{
			return Math::Vec4FGetIndex(vec, 0);
		}
		FXD::F32 Vec4FGetY(const _vector4FType& vec)
		{
			return Math::Vec4FGetIndex(vec, 1);
		}
		FXD::F32 Vec4FGetZ(const _vector4FType& vec)
		{
			return Math::Vec4FGetIndex(vec, 2);
		}
		FXD::F32 Vec4FGetW(const _vector4FType& vec)
		{
			return Math::Vec4FGetIndex(vec, 3);
		}

		bool Vec4FCompare(const _vector4FType& lhs, const _vector4FType& rhs)
		{
			return (
				(Math::Vec4FGetX(lhs) == Math::Vec4FGetX(rhs)) &&
				(Math::Vec4FGetY(lhs) == Math::Vec4FGetY(rhs)) &&
				(Math::Vec4FGetZ(lhs) == Math::Vec4FGetZ(rhs)) &&
				(Math::Vec4FGetZ(lhs) == Math::Vec4FGetZ(rhs)));
		}
		void Vec4FCopy(_vector4FType& dst, const _vector4FType& src)
		{
			Math::Vec4FSetX(dst, Math::Vec4FGetX(src));
			Math::Vec4FSetY(dst, Math::Vec4FGetY(src));
			Math::Vec4FSetZ(dst, Math::Vec4FGetZ(src));
			Math::Vec4FSetW(dst, Math::Vec4FGetW(src));
		}
		void Vec4FCopy(_vector4FType& dst, const _vector3FType& src)
		{
			Math::Vec4FSetX(dst, Math::Vec3FGetX(src));
			Math::Vec4FSetY(dst, Math::Vec3FGetY(src));
			Math::Vec4FSetZ(dst, Math::Vec3FGetZ(src));
			Math::Vec4FSetW(dst, 0.0f);
		}

		void Vec4FAdd(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ, FXD::F32 fW)
		{
			Math::Vec4FSetX(dst, (Math::Vec4FGetX(lhs) + fX));
			Math::Vec4FSetY(dst, (Math::Vec4FGetY(lhs) + fY));
			Math::Vec4FSetZ(dst, (Math::Vec4FGetZ(lhs) + fZ));
			Math::Vec4FSetW(dst, (Math::Vec4FGetW(lhs) + fW));
		}
		void Vec4FAdd(_vector4FType& dst, const _vector4FType& lhs, const _vector4FType& rhs)
		{
			Math::Vec4FAdd(dst, lhs, Math::Vec4FGetX(rhs), Math::Vec4FGetY(rhs), Math::Vec4FGetZ(rhs), Math::Vec4FGetW(rhs));
		}
		void Vec4FAdd(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fVal)
		{
			Math::Vec4FAdd(dst, lhs, fVal, fVal, fVal, fVal);
		}

		void Vec4FSub(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ, FXD::F32 fW)
		{
			Math::Vec4FSetX(dst, (Math::Vec4FGetX(lhs) - fX));
			Math::Vec4FSetY(dst, (Math::Vec4FGetY(lhs) - fY));
			Math::Vec4FSetZ(dst, (Math::Vec4FGetZ(lhs) - fZ));
			Math::Vec4FSetW(dst, (Math::Vec4FGetW(lhs) - fW));
		}
		void Vec4FSub(_vector4FType& dst, const _vector4FType& lhs, const _vector4FType& rhs)
		{
			Math::Vec4FSub(dst, lhs, Math::Vec4FGetX(rhs), Math::Vec4FGetY(rhs), Math::Vec4FGetZ(rhs), Math::Vec4FGetW(rhs));
		}
		void Vec4FSub(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fVal)
		{
			Math::Vec4FSub(dst, lhs, fVal, fVal, fVal, fVal);
		}

		void Vec4FMul(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ, FXD::F32 fW)
		{
			Math::Vec4FSetX(dst, (Math::Vec4FGetX(lhs) * fX));
			Math::Vec4FSetY(dst, (Math::Vec4FGetY(lhs) * fY));
			Math::Vec4FSetZ(dst, (Math::Vec4FGetZ(lhs) * fZ));
			Math::Vec4FSetW(dst, (Math::Vec4FGetW(lhs) * fW));
		}
		void Vec4FMul(_vector4FType& dst, const _vector4FType& lhs, const _vector4FType& rhs)
		{
			Math::Vec4FMul(dst, lhs, Math::Vec4FGetX(rhs), Math::Vec4FGetY(rhs), Math::Vec4FGetZ(rhs), Math::Vec4FGetW(rhs));
		}
		void Vec4FMul(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fVal)
		{
			Math::Vec4FMul(dst, lhs, fVal, fVal, fVal, fVal);
		}

		void Vec4FDiv(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ, FXD::F32 fW)
		{
			Math::Vec4FSetX(dst, (Math::Vec4FGetX(lhs) / fX));
			Math::Vec4FSetY(dst, (Math::Vec4FGetY(lhs) / fY));
			Math::Vec4FSetZ(dst, (Math::Vec4FGetZ(lhs) / fZ));
			Math::Vec4FSetW(dst, (Math::Vec4FGetW(lhs) / fW));
		}
		void Vec4FDiv(_vector4FType& dst, const _vector4FType& lhs, const _vector4FType& rhs)
		{
			Math::Vec4FDiv(dst, lhs, Math::Vec4FGetX(rhs), Math::Vec4FGetY(rhs), Math::Vec4FGetZ(rhs), Math::Vec4FGetW(rhs));
		}
		void Vec4FDiv(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fVal)
		{
			Math::Vec4FDiv(dst, lhs, fVal, fVal, fVal, fVal);
		}

		FXD::F32 Vec4FSum(const _vector4FType& vec)
		{
			return (Math::Vec4FGetX(vec) + Math::Vec4FGetY(vec) + Math::Vec4FGetZ(vec) + Math::Vec4FGetW(vec));
		}
		FXD::F32 Vec4FDot(const _vector4FType& lhs, const _vector4FType& rhs)
		{
			_vector4FType vecDot;
			Math::Vec4FMul(vecDot, lhs, rhs);
			return Math::Vec4FSum(vecDot);
		}
		FXD::F32 Vec4FSquaredLength(const _vector4FType& vec)
		{
			return Math::Vec4FDot(vec, vec);
		}
		FXD::F32 Vec4FSquaredDistance(const _vector4FType& lhs, const _vector4FType& rhs)
		{
			_vector4FType out;
			Math::Vec4FSub(out, lhs, rhs);
			return Math::Vec4FSquaredLength(out);
		}
		void Vec4FMidPoint(_vector4FType& out, const _vector4FType& lhs, const _vector4FType& rhs)
		{
			Math::Vec4FAdd(out, lhs, rhs);
			Math::Vec4FDiv(out, out, 2.0f);
		}

		void Vec4FNegate(_vector4FType& vec)
		{
			Math::Vec4FSetX(vec, -Math::Vec4FGetX(vec));
			Math::Vec4FSetY(vec, -Math::Vec4FGetY(vec));
			Math::Vec4FSetZ(vec, -Math::Vec4FGetZ(vec));
			Math::Vec4FSetW(vec, -Math::Vec4FGetW(vec));
		}
		void Vec4FMinimum(_vector4FType& out, const _vector4FType min)
		{
			// Clamp the x value.
			if (Math::Vec4FGetX(out) > Math::Vec4FGetX(min))
			{
				Math::Vec4FSetX(out, Math::Vec4FGetX(min));
			}
			// Clamp the y value.
			if (Math::Vec4FGetY(out) > Math::Vec4FGetY(min))
			{
				Math::Vec4FSetY(out, Math::Vec4FGetY(min));
			}
			// Clamp the z value.
			if (Math::Vec4FGetZ(out) > Math::Vec4FGetZ(min))
			{
				Math::Vec4FSetZ(out, Math::Vec4FGetZ(min));
			}
			// Clamp the w value.
			if (Math::Vec4FGetW(out) > Math::Vec4FGetW(min))
			{
				Math::Vec4FSetW(out, Math::Vec4FGetW(min));
			}
		}
		void Vec4FMaximum(_vector4FType& out, const _vector4FType max)
		{
			// Clamp the x value.
			if (Math::Vec4FGetX(out) < Math::Vec4FGetX(max))
			{
				Math::Vec4FSetX(out, Math::Vec4FGetX(max));
			}
			// Clamp the y value.
			if (Math::Vec4FGetY(out) < Math::Vec4FGetY(max))
			{
				Math::Vec4FSetY(out, Math::Vec4FGetY(max));
			}
			// Clamp the z value.
			if (Math::Vec4FGetZ(out) < Math::Vec4FGetZ(max))
			{
				Math::Vec4FSetZ(out, Math::Vec4FGetZ(max));
			}
			// Clamp the w value.
			if (Math::Vec4FGetW(out) < Math::Vec4FGetW(max))
			{
				Math::Vec4FSetW(out, Math::Vec4FGetW(max));
			}
		}
		void Vec4FClamp(_vector4FType& out, const _vector4FType& min, const _vector4FType& max)
		{
			Math::Vec4FMinimum(out, max);
			Math::Vec4FMaximum(out, min);
		}
		void Vec4FNormalise(_vector4FType& out)
		{
			const FXD::F32 fLength = Math::Sqrt(Math::Vec4FSquaredLength(out));
			if (fLength != 0.0f)
			{
				Math::Vec4FDiv(out, out, fLength);
			}
		}
		void Vec4FAbs(_vector4FType& out)
		{
			Math::Vec4FSetX(out, Math::Abs(Math::Vec4FGetX(out)));
			Math::Vec4FSetY(out, Math::Abs(Math::Vec4FGetY(out)));
			Math::Vec4FSetZ(out, Math::Abs(Math::Vec4FGetZ(out)));
			Math::Vec4FSetW(out, Math::Abs(Math::Vec4FGetW(out)));
		}
		void Vec4FCeil(_vector4FType& out)
		{
			Math::Vec4FSetX(out, Math::Ceil(Math::Vec4FGetX(out)));
			Math::Vec4FSetY(out, Math::Ceil(Math::Vec4FGetY(out)));
			Math::Vec4FSetZ(out, Math::Ceil(Math::Vec4FGetZ(out)));
			Math::Vec4FSetW(out, Math::Ceil(Math::Vec4FGetW(out)));
		}
		void Vec4FFloor(_vector4FType& out)
		{
			Math::Vec4FSetX(out, Math::Floor(Math::Vec4FGetX(out)));
			Math::Vec4FSetY(out, Math::Floor(Math::Vec4FGetY(out)));
			Math::Vec4FSetZ(out, Math::Floor(Math::Vec4FGetZ(out)));
			Math::Vec4FSetW(out, Math::Floor(Math::Vec4FGetW(out)));
		}

	} //namespace Math
} //namespace FXD
#endif //IsMathFP()