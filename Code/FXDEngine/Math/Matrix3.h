// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_MATRIX3_H
#define FXDENGINE_MATH_MATRIX3_H

#include "FXDEngine/Math/Types.h"
#include "FXDEngine/Core/StreamOut.h"

namespace FXD
{
	namespace Math
	{
		// ------
		// Matrix3F
		// -
		// ------
		class Matrix3F
		{
		public:
			friend class Matrix4F;

		public:
			static const Matrix3F kZero;
			static const Matrix3F kIdentity;

		public:
			Matrix3F(void);
			Matrix3F(FXD::F32 m00, FXD::F32 m01, FXD::F32 m02, FXD::F32 m10, FXD::F32 m11, FXD::F32 m12, FXD::F32 m20, FXD::F32 m21, FXD::F32 m22);
			Matrix3F(const Matrix3F& mat);
			Matrix3F(const Matrix4F& mat);
			~Matrix3F(void);

			FXD::F32 operator()(FXD::U32 nRow, FXD::U32 nCol) const;

			friend bool operator==(const Matrix3F& lhs, const Matrix3F& rhs) NOEXCEPT;
			friend bool operator!=(const Matrix3F& lhs, const Matrix3F& rhs) NOEXCEPT;

			Matrix3F operator-() const;

			FXD::F32 detrament(void) const;
			void negate(void);
			void transpose(void);
			void inverse(void);
			void set_row(FXD::U32 nCol, const Vector3F& vec);
			void set_row(FXD::U32 nRow, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			void set_column(FXD::U32 nCol, const Vector3F& vec);
			void set_column(FXD::U32 nCol, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			void set_right(const Vector3F& vec);
			void set_right(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			void set_up(const Vector3F& vec);
			void set_up(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			void set_forward(const Vector3F& vec);
			void set_forward(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			void set_scale(const Vector3F& vec);
			void set_scale(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			void set_rotationX(FXD::F32 fAngleX);
			void set_rotationY(FXD::F32 fAngleY);
			void set_rotationZ(FXD::F32 fAngleZ);
			void set_rotation_pitch_yaw_roll(Math::Radian rPitch, Math::Radian rYaw, Math::Radian rRoll, Math::EulerAngleOrder eOrder = Math::EulerAngleOrder::XYZ);
			void set_rotation_pitch_yaw_roll(const Vector3F& angle, Math::EulerAngleOrder eOrder = Math::EulerAngleOrder::XYZ);
			void set_rotation_quaternion(const QuaternionF& quat);

			Matrix3F get_negate(void) const;
			Matrix3F get_transpose(void) const;
			Matrix3F get_inverse(void) const;
			Vector3F get_row(FXD::U32 nRow) const;
			Vector3F get_column(FXD::U32 nCol) const;
			Vector3F get_right(void) const;
			Vector3F get_up(void) const;
			Vector3F get_forward(void) const;
			Vector3F get_scale(void) const;

			static void set_negate(Matrix3F& dst);
			static void set_transpose(Matrix3F& dst);
			static void set_inverse(Matrix3F& dst);
			static void set_right(Matrix3F& dst, const Vector3F& vec);
			static void set_right(Matrix3F& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			static void set_up(Matrix3F& dst, const Vector3F& vec);
			static void set_up(Matrix3F& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			static void set_forward(Matrix3F& dst, const Vector3F& vec);
			static void set_forward(Matrix3F& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			static void set_scale(Matrix3F& dst, const Vector3F& vec);
			static void set_scale(Matrix3F& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			static void set_rotationX(Matrix3F& dst, FXD::F32 fAngleX);
			static void set_rotationY(Matrix3F& dst, FXD::F32 fAngleY);
			static void set_rotationZ(Matrix3F& dst, FXD::F32 fAngleZ);
			static void set_rotation_pitch_yaw_roll(Matrix3F& dst, Math::Radian rPitch, Math::Radian rYaw, Math::Radian rRoll, Math::EulerAngleOrder eOrder = Math::EulerAngleOrder::XYZ);
			static void set_rotation_pitch_yaw_roll(Matrix3F& dst, const Vector3F& angle, Math::EulerAngleOrder eOrder = Math::EulerAngleOrder::XYZ);
			static void set_rotation_quaternion(Matrix3F& dst, const QuaternionF& quat);

			static FXD::F32 get_detrament(const Matrix3F& mat);
			static Matrix3F get_negate(const Matrix3F& mat);
			static Matrix3F get_transpose(const Matrix3F& mat);
			static Matrix3F get_inverse(const Matrix3F& mat);
			static Vector3F get_row(const Matrix3F& mat, FXD::U32 nRow);
			static Vector3F get_column(const Matrix3F& mat, FXD::U32 nCol);
			static Vector3F get_right(const Matrix3F& mat);
			static Vector3F get_up(const Matrix3F& mat);
			static Vector3F get_forward(const Matrix3F& mat);
			static Vector3F get_scale(const Matrix3F& mat);

			static Matrix3F create_up(const Vector3F& vec);
			static Matrix3F create_up(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			static Matrix3F create_forward(const Vector3F& vec);
			static Matrix3F create_forward(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			static Matrix3F create_scale(const Vector3F& vec);
			static Matrix3F create_scale(FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
			static Matrix3F create_rotationX(FXD::F32 fAngleX);
			static Matrix3F create_rotationY(FXD::F32 fAngleY);
			static Matrix3F create_rotationZ(FXD::F32 fAngleZ);
			static Matrix3F create_rotation_pitch_yaw_roll(Math::Radian rPitch, Math::Radian rYaw, Math::Radian rRoll, Math::EulerAngleOrder eOrder = Math::EulerAngleOrder::XYZ);
			static Matrix3F create_rotation_pitch_yaw_roll(const Vector3F& angle, Math::EulerAngleOrder eOrder = Math::EulerAngleOrder::XYZ);
			static Matrix3F create_rotation_quaternion(const QuaternionF& quat);

		private:
			_matrix3Type m_data;
		};

		// Comparison operators
		// Equality
		inline bool operator==(const Matrix3F& lhs, const Matrix3F& rhs) NOEXCEPT
		{
			return Mat3Compare(lhs.m_data, rhs.m_data);
		}

		// Inequality
		inline bool operator!=(const Matrix3F& lhs, const Matrix3F& rhs) NOEXCEPT
		{
			return !Mat3Compare(lhs.m_data, rhs.m_data);
		}

		// Core::IStreamOut
		inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, Matrix3F const& rhs)
		{
			return ostr
				<< rhs(0, 0) << ' ' << rhs(0, 1) << ' ' << rhs(0, 2) << ' '
				<< rhs(1, 0) << ' ' << rhs(1, 1) << ' ' << rhs(1, 2) << ' '
				<< rhs(2, 0) << ' ' << rhs(2, 1) << ' ' << rhs(2, 2);
		}

		/*
		// ------
		// Matrix3
		// -
		// ------
		class Matrix3
		{
		private:
			struct EulerAngleOrderData
			{
				FXD::S32 a, b, c;
				FXD::F32 sign;
			};

		public:
			static const FXD::F32 kEpsilon;
			static const Matrix3 kZero;
			static const Matrix3 kIdentity;
			static const FXD::F32 kSVDEpsilon;
			static const FXD::U32 kSVDMaxItrs;
			static const EulerAngleOrderData kEALookup[6];

		public:
			Matrix3(void);
			Matrix3(const Matrix3& mat);
			Matrix3(FXD::F32 m00, FXD::F32 m01, FXD::F32 m02, FXD::F32 m10, FXD::F32 m11, FXD::F32 m12, FXD::F32 m20, FXD::F32 m21, FXD::F32 m22);
			EXPLICIT Matrix3(const Quaternion& rotation);
			EXPLICIT Matrix3(const Quaternion& rotation, const Vector3F& scale);
			EXPLICIT Matrix3(const Vector3F& axis, const Radian& angle);
			EXPLICIT Matrix3(const Vector3F& xaxis, const Vector3F& yaxis, const Vector3F& zaxis);
			EXPLICIT Matrix3(const Degree& xAngle, const Degree& yAngle, const Degree& zAngle);
			EXPLICIT Matrix3(const Radian& xAngle, const Radian& yAngle, const Radian& zAngle);
			~Matrix3(void);

			Matrix3& operator=(const Matrix3& rhs);

			FXD::F32* operator[](FXD::U32 nRow) const;

			Matrix3 operator+(const Matrix3& rhs) const;
			Matrix3 operator-() const;
			Matrix3 operator-(const Matrix3& rhs) const;
			Matrix3 operator*(const Matrix3& rhs) const;
			Matrix3 operator*(FXD::F32 rhs) const;

			bool operator==(const Matrix3& rhs) const;
			bool operator!=(const Matrix3& rhs) const;

			friend Matrix3 operator*(FXD::F32 lhs, const Matrix3& rhs);

			Vector3F get_row(FXD::U32 nRow) const;
			Vector3F get_column(FXD::U32 nCol) const;
			void set_column(FXD::U32 nCol, const Vector3F& vec);

			void swap(Matrix3& rhs);

			Vector3F transform(const Vector3F& vec) const;															// Transforms the given vector by this matrix and returns the newly transformed vector.
			Matrix3 transpose(void) const;																					// Returns a transpose of the matrix (switched columns and rows).
			bool inverse(Matrix3& mat, FXD::F32 fTolerance = 1e-06f) const;										// Calculates an inverse of the matrix if it exists.
			Matrix3 inverse(FXD::F32 fTolerance = 1e-06f) const;													// Calculates an inverse of the matrix if it exists.		
			FXD::F32 determinant(void) const;																				// Calculates the matrix determinant.
			void decomposition(Quaternion& rotation, Vector3F& scale) const;									// Decompose a Matrix3 to rotation and scale.
			void singularValueDecomposition(Matrix3& matL, Vector3F& matS, Matrix3& matR) const;			// Decomposes the matrix into various useful values.
			void QDUDecomposition(Matrix3& matQ, Vector3F& vecD, Vector3F& vecU) const;						// Decomposes the matrix into a set of values.
			void orthonormalize(void);																						// Gram-Schmidt orthonormalization (applied to columns of rotation matrix)
			void toAxisAngle(Vector3F& axis, Radian& angle) const;													// Converts an orthonormal matrix to axis angle representation.
			void toQuaternion(Quaternion& quat) const;																	// Converts an orthonormal matrix to quaternion representation.
			bool toEulerAngles(Radian& xAngle, Radian& yAngle, Radian& zAngle) const;						// Converts an orthonormal matrix to euler angle (pitch/yaw/roll) representation.
			void fromAxes(const Vector3F& xAxis, const Vector3F& yAxis, const Vector3F& zAxis);			// Creates a matrix from a three axes.
			void fromAxisAngle(const Vector3F& axis, const Degree& angle);										// Creates a rotation matrix from an axis angle representation.
			void fromAxisAngle(const Vector3F& axis, const Radian& angle);										// Creates a rotation matrix from an axis angle representation.
			void fromQuaternion(const Quaternion& quat);																// Creates a rotation matrix from a quaternion representation.
			void fromEulerAngles(const Degree& xAngle, const Degree& yAngle, const Degree& zAngle);	// Creates a rotation matrix from the provided Pitch/Yaw/Roll angles.
			void fromEulerAngles(const Radian& xAngle, const Radian& yAngle, const Radian& zAngle);	// Creates a rotation matrix from the provided Pitch/Yaw/Roll angles.
			void eigenSolveSymmetric(FXD::F32 eigenValues[3], Vector3F eigenVectors[3]) const;			// Eigensolver, matrix must be symmetric.

		protected:
			friend class Matrix4;

			// Support for eigensolver
			void tridiagonal(FXD::F32 diag[3], FXD::F32 subDiag[3]);
			bool QLAlgorithm(FXD::F32 diag[3], FXD::F32 subDiag[3]);

			// Support for singular value decomposition
			static void bidiagonalize(Matrix3& matA, Matrix3& matL, Matrix3& matR);
			static void golubKahanStep(Matrix3& matA, Matrix3& matL, Matrix3& matR);

		private:
			FXD::F32 m_matrix[3][3];
		};

		// ------
		// Matrix3
		// -
		// ------
		inline Matrix3::Matrix3(void)
		{
		}
		inline Matrix3::Matrix3(const Matrix3& mat)
		{
			Memory::MemCopy(m_matrix, mat.m_matrix, 9 * sizeof(FXD::F32));
		}
		inline Matrix3::Matrix3(FXD::F32 m00, FXD::F32 m01, FXD::F32 m02, FXD::F32 m10, FXD::F32 m11, FXD::F32 m12, FXD::F32 m20, FXD::F32 m21, FXD::F32 m22)
		{
			m_matrix[0][0] = m00;
			m_matrix[0][1] = m01;
			m_matrix[0][2] = m02;
			m_matrix[1][0] = m10;
			m_matrix[1][1] = m11;
			m_matrix[1][2] = m12;
			m_matrix[2][0] = m20;
			m_matrix[2][1] = m21;
			m_matrix[2][2] = m22;
		}
		inline Matrix3::Matrix3(const Quaternion& rotation)
		{
			fromQuaternion(rotation);
		}
		inline Matrix3::Matrix3(const Quaternion& rotation, const Vector3F& scale)
		{
			fromQuaternion(rotation);

			for (FXD::U32 nRow = 0; nRow < 3; nRow++)
				for (FXD::U32 nCol = 0; nCol < 3; nCol++)
					m_matrix[nRow][nCol] = (scale[nRow] * m_matrix[nRow][nCol]);
		}
		inline Matrix3::Matrix3(const Vector3F& axis, const Radian& angle)
		{
			fromAxisAngle(axis, angle);
		}
		inline Matrix3::Matrix3(const Vector3F& xaxis, const Vector3F& yaxis, const Vector3F& zaxis)
		{
			fromAxes(xaxis, yaxis, zaxis);
		}
		inline Matrix3::Matrix3(const Degree& xAngle, const Degree& yAngle, const Degree& zAngle)
		{
			fromEulerAngles(Radian(xAngle), Radian(yAngle), Radian(zAngle));
		}
		inline Matrix3::Matrix3(const Radian& xAngle, const Radian& yAngle, const Radian& zAngle)
		{
			fromEulerAngles(xAngle, yAngle, zAngle);
		}
		inline Matrix3::~Matrix3(void)
		{
		}
		inline Matrix3& Matrix3::operator=(const Matrix3& rhs)
		{
			Memory::MemCopy(m_matrix, rhs.m_matrix, 9 * sizeof(FXD::F32));
			return (*this);
		}

		inline FXD::F32* Matrix3::operator[](FXD::U32 nRow) const
		{
			PRINT_COND_ASSERT((nRow < 3), "Math:");
			return (FXD::F32*)m_matrix[nRow];
		}

		inline Matrix3 Matrix3::operator+(const Matrix3& rhs) const
		{
			Matrix3 sum;
			for (FXD::U32 nRow = 0; nRow < 3; nRow++)
				for (FXD::U32 nCol = 0; nCol < 3; nCol++)
					sum.m_matrix[nRow][nCol] = (m_matrix[nRow][nCol] + rhs.m_matrix[nRow][nCol]);
			return sum;
		}
		inline Matrix3 Matrix3::operator-() const
		{
			Matrix3 neg;
			for (FXD::U32 nRow = 0; nRow < 3; nRow++)
				for (FXD::U32 nCol = 0; nCol < 3; nCol++)
					neg[nRow][nCol] = -m_matrix[nRow][nCol];
			return neg;
		}
		inline Matrix3 Matrix3::operator-(const Matrix3& rhs) const
		{
			Matrix3 diff;
			for (FXD::U32 nRow = 0; nRow < 3; nRow++)
				for (FXD::U32 nCol = 0; nCol < 3; nCol++)
					diff.m_matrix[nRow][nCol] = (m_matrix[nRow][nCol] - rhs.m_matrix[nRow][nCol]);
			return diff;
		}
		inline Matrix3 Matrix3::operator*(const Matrix3& rhs) const
		{
			Matrix3 prod;
			for (FXD::U32 nRow = 0; nRow < 3; nRow++)
				for (FXD::U32 nCol = 0; nCol < 3; nCol++)
					prod.m_matrix[nRow][nCol] = ((m_matrix[nRow][0] * rhs.m_matrix[0][nCol]) + (m_matrix[nRow][1] * rhs.m_matrix[1][nCol]) + (m_matrix[nRow][2] * rhs.m_matrix[2][nCol]));
			return prod;
		}
		inline Matrix3 Matrix3::operator*(FXD::F32 rhs) const
		{
			Matrix3 prod;
			for (FXD::U32 nRow = 0; nRow < 3; nRow++)
				for (FXD::U32 nCol = 0; nCol < 3; nCol++)
					prod[nRow][nCol] = (rhs * m_matrix[nRow][nCol]);
			return prod;
		}
		inline bool Matrix3::operator==(const Matrix3& rhs) const
		{
			for (FXD::U32 nRow = 0; nRow < 3; nRow++)
				for (FXD::U32 nCol = 0; nCol < 3; nCol++)
					if (m_matrix[nRow][nCol] != rhs.m_matrix[nRow][nCol])
						return false;
			return true;
		}
		inline bool Matrix3::operator!=(const Matrix3& rhs) const
		{
			return !operator==(rhs);
		}
		inline Matrix3 operator*(FXD::F32 lhs, const Matrix3& rhs)
		{
			Matrix3 prod;
			for (FXD::U32 nRow = 0; nRow < 3; nRow++)
				for (FXD::U32 nCol = 0; nCol < 3; nCol++)
					prod[nRow][nCol] = (lhs * rhs.get_row(nRow)[nCol]);
			return prod;
		}
		inline Vector3F Matrix3::get_row(FXD::U32 nRow) const
		{
			PRINT_COND_ASSERT((nRow < 3), "Math:");
			return Vector3F(m_matrix[nRow][0], m_matrix[nRow][1], m_matrix[nRow][2]);
		}
		inline Vector3F Matrix3::get_column(FXD::U32 nCol) const
		{
			PRINT_COND_ASSERT((nCol < 3), "Math:");
			return Vector3F(m_matrix[0][nCol], m_matrix[1][nCol], m_matrix[2][nCol]);
		}
		inline void Matrix3::set_column(FXD::U32 nCol, const Vector3F& vec)
		{
			PRINT_COND_ASSERT((nCol < 3), "Math:");
			m_matrix[0][nCol] = vec.x();
			m_matrix[1][nCol] = vec.y();
			m_matrix[2][nCol] = vec.z();
		}
		inline void Matrix3::swap(Matrix3& rhs)
		{
			FXD::STD::swap(m_matrix[0][0], rhs.m_matrix[0][0]);
			FXD::STD::swap(m_matrix[0][1], rhs.m_matrix[0][1]);
			FXD::STD::swap(m_matrix[0][2], rhs.m_matrix[0][2]);
			FXD::STD::swap(m_matrix[1][0], rhs.m_matrix[1][0]);
			FXD::STD::swap(m_matrix[1][1], rhs.m_matrix[1][1]);
			FXD::STD::swap(m_matrix[1][2], rhs.m_matrix[1][2]);
			FXD::STD::swap(m_matrix[2][0], rhs.m_matrix[2][0]);
			FXD::STD::swap(m_matrix[2][1], rhs.m_matrix[2][1]);
			FXD::STD::swap(m_matrix[2][2], rhs.m_matrix[2][2]);
		}
		*/
	} //namespace Math
} //namespace FXD
#endif //FXDENGINE_MATH_MATRIX3_H