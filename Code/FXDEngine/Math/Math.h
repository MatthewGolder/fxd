// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_MATH_H
#define FXDENGINE_MATH_MATH_H

#include "FXDEngine/Math/Types.h"
#include "FXDEngine/Math/Limits.h"

namespace FXD
{
	namespace Math
	{
		// Basic operations
		template < typename Float >
		CONSTEXPR bool ApproxEquals(Float lhs, Float rhs, Float fTolerance = Math::NumericLimits< Float >::epsilon());

		template < typename Integer >
		CONSTEXPR Integer Fractoral(Integer nVal);

		template < typename Integer >
		CONSTEXPR bool IsEven(Integer nVal);

		template < typename Integer >
		CONSTEXPR bool IsOdd(Integer nVal);

		template < typename Integer >
		CONSTEXPR bool IsPrime(Integer nVal);

		template < typename Integer >
		CONSTEXPR bool IsPowerOfTwo(Integer nVal);

		template < typename Number >
		CONSTEXPR Number Abs(Number nVal);

		template < typename Float >
		CONSTEXPR Float FMod(Float nVal1, Float nVal2);

		template < typename Number >
		CONSTEXPR Number Dim(const Number lhs, const Number rhs);

		template < typename Number >
		CONSTEXPR Number Lerp(const Number& t1, const Number& t2, FXD::F32 fLerp);

		template < typename Float >
		CONSTEXPR Float GridSnap(const Float fLocation, const Float fGrid);


		// Exponential functions
		template < typename Float >
		FXD::F32 Exp(const Float fVal);

		template < typename Float >
		FXD::F32 Exp2(const Float fVal);

		template < typename Float >
		FXD::F32 LogF(const Float fVal);

		template < typename Float >
		FXD::F32 Log2F(const Float fVal);

		template < typename Float >
		FXD::F32 Log10F(const Float fVal);


		// Power functions
		template < typename Float >
		Float Sqrt(const Float fVal);

		template < typename Float >
		Float InvSqrt(const Float fVal);

		template < typename Float >
		Float Hypot(const Float fVal1, const Float fVal2);

		template < typename Number >
		CONSTEXPR Number Sqr(Number nVal);

		template < typename Number >
		CONSTEXPR Number Cube(Number nVal);

		inline FXD::F32 Pow(FXD::F32 fVal, FXD::F32 fExponent);
		inline FXD::F32 PowF(FXD::F32 fVal, FXD::F32 fExponent);
		inline FXD::F64 PowL(FXD::F64 fVal, FXD::F64 fExponent);

		template < typename T, typename U, typename... Rest >
		CONSTEXPR auto Sum(T first, U second, Rest... rest)->decltype(auto);

		template < typename... Numbers >
		CONSTEXPR auto Mean(Numbers... args)->decltype(auto);


		// Trigonometric functions
		template < typename Float >
		Float Sin(const Float fVal);

		template < typename Float >
		Float Cos(const Float fVal);

		template < typename Float >
		Float Tan(const Float fVal);

		template < typename Float >
		Float Asin(const Float fVal);

		template < typename Float >
		Float Acos(const Float fVal);

		template < typename Float >
		Float Atan(const Float fVal);

		template < typename Float >
		Float Atan2(const Float fXVal, const Float fYVal);


		// Hyperbolic functions
		template < typename Float >
		Float Sinh(const Float fVal);

		template < typename Float >
		Float Cosh(const Float fVal);

		template < typename Float >
		Float Tanh(const Float fVal);

		template < typename Float >
		Float Asinh(const Float fVal);

		template < typename Float >
		Float Acosh(const Float fVal);

		template < typename Float >
		Float Atanh(const Float fVal);


		// Nearest integer floating point operations 
		template < typename Float >
		CONSTEXPR FXD::S32 TruncToInt(Float fVal);

		template < typename Float >
		CONSTEXPR Float Ceil(const Float fVal);

		template < typename Float >
		CONSTEXPR FXD::S32 CeilI(const Float fVal);

		template < typename Float >
		CONSTEXPR Float Floor(const Float fVal);

		template < typename Float >
		CONSTEXPR FXD::S32 FloorI(const Float fVal);

		template < typename Float >
		CONSTEXPR Float RoundF(const Float fVal);

		template < typename Float >
		CONSTEXPR FXD::S32 RoundI(const Float fVal);

		// Random 
		extern FXD::S32 Rand(void);
		extern FXD::F64 RandomUnit(void);
		extern FXD::F32 RandomUnsignedFloat(void);
		extern FXD::F32 RandomSignedFloat(void);
		extern FXD::F32 RandomRange(const FXD::F32 fMin, const FXD::F32 fMax);

		struct FuncsMath
		{
			static FXD::F32 CosRad(const Math::Radian& val);
			static FXD::F32 SinRad(const Math::Radian& val);
			static FXD::F32 TanRad(const Math::Radian& val);
			static Math::Radian AcosFRad(const FXD::F32 fVal);
			static Math::Radian AsinFRad(const FXD::F32 fVal);
		}; //FuncsMath

	} //namespace Math
} //namespace FXD
#include "FXDEngine/Math/Source/Math.inl"
#endif //FXDENGINE_MATH_MATH_H