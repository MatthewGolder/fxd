// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_TYPES_H
#define FXDENGINE_MATH_TYPES_H

#include "FXDEngine/Core/Types.h"

#if IsOSPC()
#	define IsMathFP() 1
#	define IsMathSSE() 0
#	define IsMathNeon() 0
#endif
#if IsOSAndroid()
#	define IsMathFP() 1
#	define IsMathSSE() 0
#	define IsMathNeon() 0
#endif

#if IsMathFP()
	#include "FXDEngine/Math/FP/MathFP.h"
#endif //IsMathFP()
#if IsMathSSE()
	#include "FXDEngine/Math/SSE/MathSSE.h"
#endif //IsMathSSE()

namespace FXD
{
	namespace Math
	{
		class AABox;
		class Plane;
		class Ray;
		class Sphere;
		class Degree;
		class Radian;
		//class Vector;
		//class Vector3;
		//class Vector4;
		class Quaternion;
		class Matrix3;
		class Matrix4;

		class Vector2F;
		class Vector3F;
		class Vector4F;
		class QuaternionF;
		class Matrix3F;
		class Matrix4F;

		// ------
		// EulerAngleOrder
		// -
		// ------
		enum class EulerAngleOrder
		{
			XYZ,
			XZY,
			YXZ,
			YZX,
			ZXY,
			ZYX
		};
		struct EulerAngleOrderData
		{
			FXD::U32 a, b, c;
		};
		static const EulerAngleOrderData kEALookup[6] =
		{
			{ 0, 1, 2 },	// XYZ
			{ 0, 2, 1 },	// XZY
			{ 1, 0, 2 },	// YXZ
			{ 1, 2, 0 },	// YZX
			{ 2, 0, 1 },	// ZXY
			{ 2, 1, 0 }		// ZYX
		};
		
		// ------
		// _matrix3Type
		// -
		// ------
		void Mat3SetIdentity(_matrix3Type& dst);
		void Mat3Set(_matrix3Type& dst, FXD::F32 m00, FXD::F32 m01, FXD::F32 m02, FXD::F32 m10, FXD::F32 m11, FXD::F32 m12, FXD::F32 m20, FXD::F32 m21, FXD::F32 m22);
		bool Mat3Compare(const _matrix3Type& lhs, const _matrix3Type& src);
		void Mat3Copy(_matrix3Type& dst, const _matrix3Type& src);
		void Mat3Copy(_matrix3Type& dst, const _matrix4Type& src);

		_vector3FType Mat3Dot3(const _matrix3Type& mat, const _vector3FType& vec);
		void Mat3Add(_matrix3Type& dst, const _matrix3Type& lhs, const _matrix3Type& rhs);
		void Mat3Sub(_matrix3Type& dst, const _matrix3Type& lhs, const _matrix3Type& rhs);
		void Mat3Mul(_matrix3Type& dst, const _matrix3Type& lhs, const _matrix3Type& rhs);

		FXD::F32 Mat3Detrament(const _matrix3Type& src);
		void Mat3Negate(_matrix3Type& dst);
		void Mat3Transpose(_matrix3Type& dst, const _matrix3Type& src);
		void Mat3Inverse(_matrix3Type& dst, const _matrix3Type& src);
		void Mat3SetRow(_matrix3Type& dst, FXD::U32 nRow, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
		void Mat3SetRow(_matrix3Type& dst, FXD::U32 nRow, const _vector3FType& vec);
		void Mat3SetColumn(_matrix3Type& dst, FXD::U32 nCol, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
		void Mat3SetColumn(_matrix3Type& dst, FXD::U32 nCol, const _vector3FType& vec);
		void Mat3SetRight(_matrix3Type& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
		void Mat3SetRight(_matrix3Type& dst, const _vector3FType& vec);
		void Mat3SetUp(_matrix3Type& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
		void Mat3SetUp(_matrix3Type& dst, const _vector3FType& vec);
		void Mat3SetForward(_matrix3Type& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
		void Mat3SetForward(_matrix3Type& dst, const _vector3FType& vec);
		void Mat3SetScale(_matrix3Type& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
		void Mat3SetScale(_matrix3Type& dst, const _vector3FType& vec);
		void Mat3SetRotationX(_matrix3Type& dst, FXD::F32 fAngleX);
		void Mat3SetRotationY(_matrix3Type& dst, FXD::F32 fAngleY);
		void Mat3SetRotationZ(_matrix3Type& dst, FXD::F32 fAngleZ);
		void Mat3SetRotationPitchYawRoll(_matrix3Type& dst, const _vector3FType& angle, Math::EulerAngleOrder eOrder);
		void Mat3SetRotationPitchYawRoll(_matrix3Type& dst, FXD::F32 fPitch, FXD::F32 fYaw, FXD::F32 fRoll, Math::EulerAngleOrder eOrder);
		void Mat3SetRotationQuaternion(_matrix3Type& dst, const _vector4FType& quat);

		void Mat3GetRow(_vector3FType& dst, const _matrix3Type& mat, FXD::U32 nRow);
		void Mat3GetColumn(_vector3FType& dst, const _matrix3Type& mat, FXD::U32 nCol);
		void Mat3GetRight(_vector3FType& dst, const _matrix3Type& mat);
		void Mat3GetUp(_vector3FType& dst, const _matrix3Type& mat);
		void Mat3GetForward(_vector3FType& dst, const _matrix3Type& mat);
		void Mat3GetScale(_vector3FType& dst, const _matrix3Type& mat);


		// ------
		// _matrix4Type
		// -
		// ------
		void Mat4SetIdentity(_matrix4Type& dst);
		void Mat4Set(_matrix4Type& dst, FXD::F32 m00, FXD::F32 m01, FXD::F32 m02, FXD::F32 m03, FXD::F32 m10, FXD::F32 m11, FXD::F32 m12, FXD::F32 m13, FXD::F32 m20, FXD::F32 m21, FXD::F32 m22, FXD::F32 m23, FXD::F32 m30, FXD::F32 m31, FXD::F32 m32, FXD::F32 m33);
		bool Mat4Compare(const _matrix4Type& lhs, const _matrix4Type& rhs);
		void Mat4Copy(_matrix4Type& dst, const _matrix4Type& src);
		void Mat4Copy(_matrix4Type& dst, const _matrix3Type& src);

		_vector4FType Mat4Dot4(const _matrix4Type& mat, const _vector4FType& vec);

		void Mat4Add(_matrix4Type& dst, const _matrix4Type& lhs, const _matrix4Type& rhs);
		void Mat4Sub(_matrix4Type& dst, const _matrix4Type& lhs, const _matrix4Type& rhs);
		void Mat4Mul(_matrix4Type& dst, const _matrix4Type& lhs, const _matrix4Type& rhs);
		void Mat4Mul(_vector3FType& dst, const _matrix4Type& lhs, const _vector3FType& rhs);
		void Mat4Mul(_vector4FType& dst, const _matrix4Type& lhs, const _vector4FType& rhs);

		FXD::F32 Mat4Detrament(const _matrix4Type& src);
		void Mat4Negate(_matrix4Type& dst);
		void Mat4Transpose(_matrix4Type& dst, const _matrix4Type& src);
		void Mat4Inverse(_matrix4Type& dst, const _matrix4Type& src);
		void Mat4SetRow(_matrix4Type& dst, FXD::U32 nRow, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ, FXD::F32 fW);
		void Mat4SetRow(_matrix4Type& dst, FXD::U32 nRow, const _vector4FType& vec);
		void Mat4SetColumn(_matrix4Type& dst, FXD::U32 nCol, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ, FXD::F32 fW);
		void Mat4SetColumn(_matrix4Type& dst, FXD::U32 nCol, const _vector4FType& vec);
		void Mat4SetTranslation(_matrix4Type& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
		void Mat4SetTranslation(_matrix4Type& dst, const _vector3FType& vec);
		void Mat4SetRight(_matrix4Type& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
		void Mat4SetRight(_matrix4Type& dst, const _vector3FType& vec);
		void Mat4SetUp(_matrix4Type& dst, FXD::F32 fUpX, FXD::F32 fY, FXD::F32 fZ);
		void Mat4SetUp(_matrix4Type& dst, const _vector3FType& vec);
		void Mat4SetForward(_matrix4Type& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
		void Mat4SetForward(_matrix4Type& dst, const _vector3FType& vec);
		void Mat4SetScale(_matrix4Type& dst, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
		void Mat4SetScale(_matrix4Type& dst, const _vector3FType& vec);
		void Mat4SetRotationX(_matrix4Type& dst, FXD::F32 fAngleX);
		void Mat4SetRotationY(_matrix4Type& dst, FXD::F32 fAngleY);
		void Mat4SetRotationZ(_matrix4Type& dst, FXD::F32 fAngleZ);
		void Mat4SetRotationPitchYawRoll(_matrix4Type& dst, const _vector3FType& angle, Math::EulerAngleOrder eOrder);
		void Mat4SetRotationPitchYawRoll(_matrix4Type& dst, FXD::F32 fPitch, FXD::F32 fYaw, FXD::F32 fRoll, Math::EulerAngleOrder eOrder);
		void Mat4SetRotationQuaternion(_matrix4Type& dst, const _vector4FType& quat);
		void FXMMatrixLookToLH(_matrix4Type& dst, const _vector3FType& eye, const _vector3FType& forward, const _vector3FType& up);
		void Mat4SetTRS(_matrix4Type& dst, const _vector3FType& translation, const _vector4FType& rotation, const _vector3FType& scale);
		void Mat4LookAtLH(_matrix4Type& dst, const _vector3FType& eye, const _vector3FType& forward, const _vector3FType& up);
		void Mat4PerspectiveLH(_matrix4Type& dst, FXD::F32 fViewWidth, FXD::F32 fViewHeight, FXD::F32 fNearZ, FXD::F32 fFarZ);
		void Mat4PerspectiveFOVLH(_matrix4Type& dst, FXD::F32 fFOVAngleY, FXD::F32 fAspectHByW, FXD::F32 fNearZ, FXD::F32 fFarZ);
		void Mat4OrthographicLH(_matrix4Type& dst, FXD::F32 fViewWidth, FXD::F32 fViewHeight, FXD::F32 fNearZ, FXD::F32 fFarZ);

		void Mat4GetRow(_vector3FType& dst, const _matrix4Type& mat, FXD::U32 nRow);
		void Mat4GetRow(_vector4FType& dst, const _matrix4Type& mat, FXD::U32 nRow);
		void Mat4GetColumn(_vector3FType& dst, const _matrix4Type& mat, FXD::U32 nCol);
		void Mat4GetColumn(_vector4FType& dst, const _matrix4Type& mat, FXD::U32 nCol);
		void Mat4GetTranslation(_vector3FType& dst, const _matrix4Type& mat);
		void Mat4GetRight(_vector3FType& dst, const _matrix4Type& mat);
		void Mat4GetUp(_vector3FType& dst, const _matrix4Type& mat);
		void Mat4GetForward(_vector3FType& dst, const _matrix4Type& mat);
		void Mat4GetScale(_vector3FType& dst, const _matrix4Type& mat);


		// ------
		// _quaternionType
		// -
		// ------
		void QuatFRotationPitchYawRollF(_vector4FType& dst, FXD::F32 fPitch, FXD::F32 fYaw, FXD::F32 fRoll, Math::EulerAngleOrder eOrder);
		void QuatFRotationPitchYawRollV(_vector4FType& dst, const _vector3FType& rhs, Math::EulerAngleOrder eOrder);
		void QuatFToRotation(_matrix3Type& dst, const _vector4FType& src);
		void QuatFMul(_vector4FType& dst, const _vector4FType& lhs, const _vector4FType& rhs);
		//void QuatFMul(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fVal);


		// ------
		// _vector2FType
		// -
		// ------
		void Vec2FSet(_vector2FType& dst, FXD::F32 x, FXD::F32 y);
		void Vec2FSet(_vector2FType& dst, FXD::F32 fVal);

		void Vec2FSetIndex(_vector2FType& dst, FXD::U32 nID, FXD::F32 fVal);
		void Vec2FSetX(_vector2FType& dst, FXD::F32 fVal);
		void Vec2FSetY(_vector2FType& dst, FXD::F32 fVal);

		FXD::F32 Vec2FGetIndex(const _vector2FType& vec, FXD::U32 nID);
		FXD::F32 Vec2FGetX(const _vector2FType& vec);
		FXD::F32 Vec2FGetY(const _vector2FType& vec);

		bool Vec2FCompare(const _vector2FType& lhs, const _vector2FType& rhs);
		void Vec2FCopy(_vector2FType& dst, const _vector2FType& src);

		void Vec2FAdd(_vector2FType& dst, const _vector2FType& lhs, FXD::F32 fX, FXD::F32 fY);
		void Vec2FAdd(_vector2FType& dst, const _vector2FType& lhs, const _vector2FType& rhs);
		void Vec2FAdd(_vector2FType& dst, const _vector2FType& lhs, FXD::F32 fVal);

		void Vec2FSub(_vector2FType& dst, const _vector2FType& lhs, FXD::F32 fX, FXD::F32 fY);
		void Vec2FSub(_vector2FType& dst, const _vector2FType& lhs, const _vector2FType& rhs);
		void Vec2FSub(_vector2FType& dst, const _vector2FType& lhs, FXD::F32 fVal);

		void Vec2FMul(_vector2FType& dst, const _vector2FType& lhs, FXD::F32 fX, FXD::F32 fY);
		void Vec2FMul(_vector2FType& dst, const _vector2FType& lhs, const _vector2FType& rhs);
		void Vec2FMul(_vector2FType& dst, const _vector2FType& lhs, FXD::F32 fVal);

		void Vec2FDiv(_vector2FType& dst, const _vector2FType& lhs, FXD::F32 fX, FXD::F32 fY);
		void Vec2FDiv(_vector2FType& dst, const _vector2FType& lhs, const _vector2FType& rhs);
		void Vec2FDiv(_vector2FType& dst, const _vector2FType& lhs, FXD::F32 fVal);

		FXD::F32 Vec2FSum(const _vector2FType& vec);
		FXD::F32 Vec2FDot(const _vector2FType& lhs, const _vector2FType& rhs);
		FXD::F32 Vec2FCross(const _vector2FType& lhs, const _vector2FType& rhs);
		FXD::F32 Vec2FSquaredLength(const _vector2FType& vec);
		FXD::F32 Vec2FSquaredDistance(const _vector2FType& lhs, const _vector2FType& rhs);
		void Vec2FMidPoint(_vector2FType& out, const _vector2FType& lhs, const _vector2FType& rhs);

		void Vec2FNegate(_vector2FType& vec);
		void Vec2FMinimum(_vector2FType& out, const _vector2FType min);
		void Vec2FMaximum(_vector2FType& out, const _vector2FType max);
		void Vec2FClamp(_vector2FType& out, const _vector2FType& min, const _vector2FType& max);
		void Vec2FNormalise(_vector2FType& out);
		void Vec2FAbs(_vector2FType& out);
		void Vec2FCeil(_vector2FType& out);
		void Vec2FFloor(_vector2FType& out);


		// ------
		// _vector2FType
		// -
		// ------
		void Vec2ISet(_vector2IType& dst, FXD::S32 x, FXD::S32 y);
		void Vec2ISet(_vector2IType& dst, FXD::S32 nVal);

		void Vec2ISetIndex(_vector2IType& dst, FXD::U32 nID, FXD::S32 nVal);
		void Vec2ISetX(_vector2IType& dst, FXD::S32 nVal);
		void Vec2ISetY(_vector2IType& dst, FXD::S32 nVal);

		FXD::S32 Vec2IGetIndex(const _vector2IType& vec, FXD::U32 nID);
		FXD::S32 Vec2IGetX(const _vector2IType& vec);
		FXD::S32 Vec2IGetY(const _vector2IType& vec);

		bool Vec2ICompare(const _vector2IType& lhs, const _vector2IType& rhs);
		void Vec2ICopy(_vector2IType& dst, const _vector2IType& src);

		void Vec2IAdd(_vector2IType& dst, const _vector2IType& lhs, FXD::S32 nX, FXD::S32 nY);
		void Vec2IAdd(_vector2IType& dst, const _vector2IType& lhs, const _vector2IType& rhs);
		void Vec2IAdd(_vector2IType& dst, const _vector2IType& lhs, FXD::S32 nVal);

		void Vec2ISub(_vector2IType& dst, const _vector2IType& lhs, FXD::S32 nX, FXD::S32 nY);
		void Vec2ISub(_vector2IType& dst, const _vector2IType& lhs, const _vector2IType& rhs);
		void Vec2ISub(_vector2IType& dst, const _vector2IType& lhs, FXD::S32 nVal);

		void Vec2IMul(_vector2IType& dst, const _vector2IType& lhs, FXD::S32 nX, FXD::S32 nY);
		void Vec2IMul(_vector2IType& dst, const _vector2IType& lhs, const _vector2IType& rhs);
		void Vec2IMul(_vector2IType& dst, const _vector2IType& lhs, FXD::S32 nVal);

		void Vec2IDiv(_vector2IType& dst, const _vector2IType& lhs, FXD::S32 nX, FXD::S32 nY);
		void Vec2IDiv(_vector2IType& dst, const _vector2IType& lhs, const _vector2IType& rhs);
		void Vec2IDiv(_vector2IType& dst, const _vector2IType& lhs, FXD::S32 nVal);

		void Vec2IMidPoint(_vector2IType& out, const _vector2IType& lhs, const _vector2IType& rhs);

		void Vec2INegate(_vector2IType& vec);
		void Vec2IMinimum(_vector2IType& out, const _vector2IType min);
		void Vec2IMaximum(_vector2IType& out, const _vector2IType max);
		void Vec2IClamp(_vector2IType& out, const _vector2IType& min, const _vector2IType& max);
		void Vec2IAbs(_vector2IType& out);


		// ------
		// _vector3FType
		// -
		// ------
		void Vec3FSet(_vector3FType& dst, FXD::F32 x, FXD::F32 y, FXD::F32 z);
		void Vec3FSet(_vector3FType& dst, FXD::F32 fVal);

		void Vec3FSetIndex(_vector3FType& dst, FXD::U32 nID, FXD::F32 fVal);
		void Vec3FSetX(_vector3FType& dst, FXD::F32 fVal);
		void Vec3FSetY(_vector3FType& dst, FXD::F32 fVal);
		void Vec3FSetZ(_vector3FType& dst, FXD::F32 fVal);
		void Vec3FSetW(_vector3FType& dst, FXD::F32 fVal);

		FXD::F32 Vec3FGetIndex(const _vector3FType& vec, FXD::U32 nID);
		FXD::F32 Vec3FGetX(const _vector3FType& vec);
		FXD::F32 Vec3FGetY(const _vector3FType& vec);
		FXD::F32 Vec3FGetZ(const _vector3FType& vec);
		FXD::F32 Vec3FGetW(const _vector3FType& vec);

		bool Vec3FCompare(const _vector3FType& lhs, const _vector3FType& rhs);
		void Vec3FCopy(_vector3FType& dst, const _vector3FType& src);
		void Vec3FCopy(_vector3FType& dst, const _vector4FType& src);

		void Vec3FAdd(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
		void Vec3FAdd(_vector3FType& dst, const _vector3FType& lhs, const _vector3FType& rhs);
		void Vec3FAdd(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fVal);

		void Vec3FSub(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
		void Vec3FSub(_vector3FType& dst, const _vector3FType& lhs, const _vector3FType& rhs);
		void Vec3FSub(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fVal);

		void Vec3FMul(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
		void Vec3FMul(_vector3FType& dst, const _vector3FType& lhs, const _vector3FType& rhs);
		void Vec3FMul(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fVal);

		void Vec3FDiv(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ);
		void Vec3FDiv(_vector3FType& dst, const _vector3FType& lhs, const _vector3FType& rhs);
		void Vec3FDiv(_vector3FType& dst, const _vector3FType& lhs, FXD::F32 fVal);

		FXD::F32 Vec3FSum(const _vector3FType& vec);
		FXD::F32 Vec3FDot(const _vector3FType& lhs, const _vector3FType& rhs);
		_vector3FType Vec3FCross(const _vector3FType& lhs, const _vector3FType& rhs);
		FXD::F32 Vec3FSquaredLength(const _vector3FType& vec);
		FXD::F32 Vec3FSquaredDistance(const _vector3FType& lhs, const _vector3FType& rhs);
		void Vec3FMidPoint(_vector3FType& out, const _vector3FType& lhs, const _vector3FType& rhs);
		FXD::F32 Vec3FAngleBetween(const _vector3FType& lhs, const _vector3FType& rhs);
		void Vec3FNegate(_vector3FType& vec);
		void Vec3FMinimum(_vector3FType& out, const _vector3FType min);
		void Vec3FMaximum(_vector3FType& out, const _vector3FType max);
		void Vec3FClamp(_vector3FType& out, const _vector3FType& min, const _vector3FType& max);
		void Vec3FNormalise(_vector3FType& out);
		void Vec3FAbs(_vector3FType& out);
		void Vec3FCeil(_vector3FType& out);
		void Vec3FFloor(_vector3FType& out);


		// ------
		// _vector4FType
		// -
		// ------
		void Vec4FSet(_vector4FType& dst, FXD::F32 x, FXD::F32 y, FXD::F32 z, FXD::F32 w);
		void Vec4FSet(_vector4FType& dst, FXD::F32 fVal);

		void Vec4FSetIndex(_vector4FType& dst, FXD::U32 nID, FXD::F32 fVal);
		void Vec4FSetX(_vector4FType& dst, FXD::F32 fVal);
		void Vec4FSetY(_vector4FType& dst, FXD::F32 fVal);
		void Vec4FSetZ(_vector4FType& dst, FXD::F32 fVal);
		void Vec4FSetW(_vector4FType& dst, FXD::F32 fVal);

		FXD::F32 Vec4FGetIndex(const _vector4FType& vec, const FXD::U32 nID);
		FXD::F32 Vec4FGetX(const _vector4FType& vec);
		FXD::F32 Vec4FGetY(const _vector4FType& vec);
		FXD::F32 Vec4FGetZ(const _vector4FType& vec);
		FXD::F32 Vec4FGetW(const _vector4FType& vec);

		bool Vec4FCompare(const _vector4FType& lhs, const _vector4FType& rhs);
		void Vec4FCopy(_vector4FType& dst, const _vector4FType& src);
		void Vec4FCopy(_vector4FType& dst, const _vector3FType& src);

		void Vec4FAdd(_vector4FType& dst, const _vector4FType& lhs, const _vector4FType& rhs);
		void Vec4FAdd(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ, FXD::F32 fW);
		void Vec4FAdd(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fVal);

		void Vec4FSub(_vector4FType& dst, const _vector4FType& lhs, const _vector4FType& rhs);
		void Vec4FSub(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ, FXD::F32 fW);
		void Vec4FSub(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fVal);

		void Vec4FMul(_vector4FType& dst, const _vector4FType& lhs, const _vector4FType& rhs);
		void Vec4FMul(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ, FXD::F32 fW);
		void Vec4FMul(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fVal);

		void Vec4FDiv(_vector4FType& dst, const _vector4FType& lhs, const _vector4FType& rhs);
		void Vec4FDiv(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fX, FXD::F32 fY, FXD::F32 fZ, FXD::F32 fW);
		void Vec4FDiv(_vector4FType& dst, const _vector4FType& lhs, FXD::F32 fVal);

		FXD::F32 Vec4FSum(const _vector4FType& vec);
		FXD::F32 Vec4FDot(const _vector4FType& lhs, const _vector4FType& rhs);
		FXD::F32 Vec4FSquaredLength(const _vector4FType& vec);
		FXD::F32 Vec4FSquaredDistance(const _vector4FType& lhs, const _vector4FType& rhs);
		void Vec4FMidPoint(_vector4FType& out, const _vector4FType& lhs, const _vector4FType& rhs);

		void Vec4FNegate(_vector4FType& vec);
		void Vec4FMinimum(_vector4FType& out, const _vector4FType min);
		void Vec4FMaximum(_vector4FType& out, const _vector4FType max);
		void Vec4FClamp(_vector4FType& out, const _vector4FType& min, const _vector4FType& max);
		void Vec4FNormalise(_vector4FType& out);
		void Vec4FAbs(_vector4FType& out);
		void Vec4FCeil(_vector4FType& out);
		void Vec4FFloor(_vector4FType& out);

	} //namespace Math
} //namespace FXD
#endif //FXDENGINE_MATH_TYPES_H