// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_LIMITS_H
#define FXDENGINE_MATH_LIMITS_H

#include "FXDEngine/Core/Types.h"
#include <float.h>

#ifdef max
#	undef max
#endif
#ifdef min
#	undef min
#endif

namespace FXD
{
	namespace Math
	{
		static CONSTEXPR const FXD::U8	MIN_U8(0x00);
		static CONSTEXPR const FXD::U16	MIN_U16(0x0000);
		static CONSTEXPR const FXD::U32	MIN_U32(0x00000000);
		static CONSTEXPR const FXD::U64	MIN_U64(0x0000000000000000);
		static CONSTEXPR const FXD::S8	MIN_S8(-128);
		static CONSTEXPR const FXD::S16	MIN_S16(-32768);
		static CONSTEXPR const FXD::S32	MIN_S32(0x80000000);
		static CONSTEXPR const FXD::S64	MIN_S64(0x8000000000000000);

		static CONSTEXPR const FXD::U8	MAX_U8(0xff);
		static CONSTEXPR const FXD::U16	MAX_U16(0xffff);
		static CONSTEXPR const FXD::U32	MAX_U32(0xffffffff);
		static CONSTEXPR const FXD::U64	MAX_U64(0xffffffffffffffff);
		static CONSTEXPR const FXD::S8	MAX_S8(0x7f);
		static CONSTEXPR const FXD::S16	MAX_S16(0x7fff);
		static CONSTEXPR const FXD::S32	MAX_S32(0x7fffffff);
		static CONSTEXPR const FXD::S64	MAX_S64(0x7fffffffffffffff);

		static CONSTEXPR const FXD::F32	MIN_F32(1.175494351e-38F);
		static CONSTEXPR const FXD::F32	MAX_F32(3.402823466e+38F);
		static CONSTEXPR const FXD::F32	EPSILON_F32(1.192092896e-07F);				// Smallest such that 1.0+FLT_EPSILON != 1.0

		static CONSTEXPR const FXD::F64	MIN_F64(2.2250738585072014e-308);
		static CONSTEXPR const FXD::F64	MAX_F64(1.7976931348623158e+308);
		static CONSTEXPR const FXD::F64	EPSILON_F64(2.2204460492503131e-016);		// Smallest such that 1.0+DBL_EPSILON != 1.0

#		ifndef CHAR_BIT
#			define CHAR_BIT 8
#		endif

#		define LIMITS_IS_SIGNED(T) ((T)(-1) < 0)

#		define LIMITS_DIGITS_S(T) ((sizeof(T) * CHAR_BIT) - 1)
#		define LIMITS_DIGITS_U(T) ((sizeof(T) * CHAR_BIT))
#		define LIMITS_DIGITS(T) ((LIMITS_IS_SIGNED(T) ? LIMITS_DIGITS_S(T) : LIMITS_DIGITS_U(T)))

#		define LIMITS_DIGITS10_S(T) ((LIMITS_DIGITS_S(T) * 643L) / 2136) // (643 / 2136) ~= log10(2).
#		define LIMITS_DIGITS10_U(T) ((LIMITS_DIGITS_U(T) * 643L) / 2136)
#		define LIMITS_DIGITS10(T)((LIMITS_IS_SIGNED(T) ? LIMITS_DIGITS10_S(T) : LIMITS_DIGITS10_U(T)))

		namespace Internal
		{
			struct NumericLimitsBase
			{
				static CONSTEXPR const bool is_bounded = false;
				static CONSTEXPR const bool is_signed = false;
				static CONSTEXPR const bool is_integer = false;
				static CONSTEXPR const bool is_exact = false;
				static CONSTEXPR const bool is_modulo = false;
				static CONSTEXPR const bool has_infinity = false;
				static CONSTEXPR const FXD::S32 digits = 0;
				static CONSTEXPR const FXD::S32 digits10 = 0;
				static CONSTEXPR const FXD::S32 max_digits10 = 0;
			};
		} // namespace Internal

		// ------
		// NumericLimits
		// -
		// ------
		template < typename NumericType >
		struct NumericLimits : public Internal::NumericLimitsBase
		{
			static CONSTEXPR const NumericType min(void) { return NumericType(); }
			static CONSTEXPR const NumericType max(void) { return NumericType(); }
			static CONSTEXPR const NumericType lowest(void) { return NumericType(); }
			static CONSTEXPR const NumericType epsilon(void) { return NumericType(); }
			static CONSTEXPR const NumericType infinity(void) { return NumericType(); }
			static CONSTEXPR const NumericType quiet_NaN(void) { return NumericType(); }
		};

		template < typename Type >
		struct NumericLimits< const Type > : public NumericLimits< Type >
		{};

		template < typename Type >
		struct NumericLimits< volatile Type > : public NumericLimits< Type >
		{};

		template < typename Type >
		struct NumericLimits< const volatile Type > : public NumericLimits< Type >
		{};

		template <>
		struct NumericLimits< bool >
		{
			using Type = bool;

			static CONSTEXPR const Type min(void) { return false; }
			static CONSTEXPR const Type max(void) { return true; }
			static CONSTEXPR const Type lowest(void) { return false; }
			static CONSTEXPR const Type epsilon(void) { return false; }
			static CONSTEXPR const Type infinity(void) { return Type(); }
			static CONSTEXPR const Type quiet_NaN(void) { return Type(); }
			static CONSTEXPR const bool is_bounded = true;
			static CONSTEXPR const bool is_signed = false;
			static CONSTEXPR const bool is_integer = true;
			static CONSTEXPR const bool is_exact = true;
			static CONSTEXPR const bool is_modulo = false;
			static CONSTEXPR const bool has_infinity = false;
			static CONSTEXPR const FXD::S32 digits = 1;
			static CONSTEXPR const FXD::S32 digits10 = 0;
			static CONSTEXPR const FXD::S32 max_digits10 = 0;
		};

		template <>
		struct NumericLimits< FXD::UTF8 >
		{
			using Type = FXD::UTF8;

			static CONSTEXPR const Type min(void) { return Math::MIN_U8; }
			static CONSTEXPR const Type max(void) { return Math::MAX_U8; }
			static CONSTEXPR const Type lowest(void) { return NumericLimits< Type >::min(); }
			static CONSTEXPR const Type epsilon(void) { return 0; }
			static CONSTEXPR const Type infinity(void) { return 0; }
			static CONSTEXPR const Type quiet_NaN(void) { return 0; }
			static CONSTEXPR const bool is_bounded = true;
			static CONSTEXPR const bool is_signed = false;
			static CONSTEXPR const bool is_integer = true;
			static CONSTEXPR const bool is_exact = true;
			static CONSTEXPR const bool is_modulo = true;
			static CONSTEXPR const bool has_infinity = false;
			static CONSTEXPR const FXD::S32 digits = LIMITS_DIGITS(Type);
			static CONSTEXPR const FXD::S32 digits10 = LIMITS_DIGITS10(Type);
			static CONSTEXPR const FXD::S32 max_digits10 = 0;
		};

		template <>
		struct NumericLimits< FXD::UTF16 >
		{
			using Type = FXD::UTF16;

			static CONSTEXPR const Type min(void) { return Math::MIN_U16; }
			static CONSTEXPR const Type max(void) { return Math::MAX_U16; }
			static CONSTEXPR const Type lowest(void) { return NumericLimits< Type >::min(); }
			static CONSTEXPR const Type epsilon(void) { return 0; }
			static CONSTEXPR const Type infinity(void) { return 0; }
			static CONSTEXPR const Type quiet_NaN(void) { return 0; }
			static CONSTEXPR const bool is_bounded = true;
			static CONSTEXPR const bool is_signed = false;
			static CONSTEXPR const bool is_integer = true;
			static CONSTEXPR const bool is_exact = true;
			static CONSTEXPR const bool is_modulo = true;
			static CONSTEXPR const bool has_infinity = false;
			static CONSTEXPR const FXD::S32 digits = LIMITS_DIGITS(Type);
			static CONSTEXPR const FXD::S32 digits10 = LIMITS_DIGITS10(Type);
			static CONSTEXPR const FXD::S32 max_digits10 = 0;
		};

		template <>
		struct NumericLimits< FXD::UTF32 >
		{
			using Type = FXD::UTF32;

			static CONSTEXPR const Type min(void) { return Math::MIN_U32; }
			static CONSTEXPR const Type max(void) { return Math::MAX_U32; }
			static CONSTEXPR const Type lowest(void) { return NumericLimits< Type >::min(); }
			static CONSTEXPR const Type epsilon(void) { return 0; }
			static CONSTEXPR const Type infinity(void) { return 0; }
			static CONSTEXPR const Type quiet_NaN(void) { return 0; }
			static CONSTEXPR const bool is_bounded = true;
			static CONSTEXPR const bool is_signed = false;
			static CONSTEXPR const bool is_integer = true;
			static CONSTEXPR const bool is_exact = true;
			static CONSTEXPR const bool is_modulo = true;
			static CONSTEXPR const bool has_infinity = false;
			static CONSTEXPR const FXD::S32 digits = LIMITS_DIGITS(Type);
			static CONSTEXPR const FXD::S32 digits10 = LIMITS_DIGITS10(Type);
			static CONSTEXPR const FXD::S32 max_digits10 = 0;
		};

		template <>
		struct NumericLimits< FXD::U8 >
		{
			using Type = FXD::U8;

			static CONSTEXPR const Type min(void) { return Math::MIN_U8; }
			static CONSTEXPR const Type max(void) { return Math::MAX_U8; }
			static CONSTEXPR const Type lowest(void) { return NumericLimits< Type >::min(); }
			static CONSTEXPR const Type epsilon(void) { return 0; }
			static CONSTEXPR const Type infinity(void) { return 0; }
			static CONSTEXPR const Type quiet_NaN(void) { return 0; }
			static CONSTEXPR const bool is_bounded = true;
			static CONSTEXPR const bool is_signed = false;
			static CONSTEXPR const bool is_integer = true;
			static CONSTEXPR const bool is_exact = true;
			static CONSTEXPR const bool is_modulo = true;
			static CONSTEXPR const bool has_infinity = false;
			static CONSTEXPR const FXD::S32 digits = LIMITS_DIGITS_U(Type);
			static CONSTEXPR const FXD::S32 digits10 = LIMITS_DIGITS10_U(Type);
			static CONSTEXPR const FXD::S32 max_digits10 = 0;
		};

		template <>
		struct NumericLimits< FXD::U16 >
		{
			using Type = FXD::U16;

			static CONSTEXPR const Type min(void) { return Math::MIN_U16; }
			static CONSTEXPR const Type max(void) { return Math::MAX_U16; }
			static CONSTEXPR const Type lowest(void) { return NumericLimits< Type >::min(); }
			static CONSTEXPR const Type epsilon(void) { return 0; }
			static CONSTEXPR const Type infinity(void) { return 0; }
			static CONSTEXPR const Type quiet_NaN(void) { return 0; }
			static CONSTEXPR const bool is_bounded = true;
			static CONSTEXPR const bool is_signed = false;
			static CONSTEXPR const bool is_integer = true;
			static CONSTEXPR const bool is_exact = true;
			static CONSTEXPR const bool is_modulo = true;
			static CONSTEXPR const bool has_infinity = false;
			static CONSTEXPR const FXD::S32 digits = LIMITS_DIGITS_U(Type);
			static CONSTEXPR const FXD::S32 digits10 = LIMITS_DIGITS10_U(Type);
			static CONSTEXPR const FXD::S32 max_digits10 = 0;
		};

		template <>
		struct NumericLimits< FXD::U32 >
		{
			using Type = FXD::U32;

			static CONSTEXPR const Type min(void) { return Math::MIN_U32; }
			static CONSTEXPR const Type max(void) { return Math::MAX_U32; }
			static CONSTEXPR const Type lowest(void) { return NumericLimits< Type >::min(); }
			static CONSTEXPR const Type epsilon(void) { return 0; }
			static CONSTEXPR const Type infinity(void) { return 0; }
			static CONSTEXPR const Type quiet_NaN(void) { return 0; }
			static CONSTEXPR const bool is_bounded = true;
			static CONSTEXPR const bool is_signed = false;
			static CONSTEXPR const bool is_integer = true;
			static CONSTEXPR const bool is_exact = true;
			static CONSTEXPR const bool is_modulo = true;
			static CONSTEXPR const bool has_infinity = false;
			static CONSTEXPR const FXD::S32 digits = LIMITS_DIGITS_U(Type);
			static CONSTEXPR const FXD::S32 digits10 = LIMITS_DIGITS10_U(Type);
			static CONSTEXPR const FXD::S32 max_digits10 = 0;
		};

		template <>
		struct NumericLimits< FXD::U64 >
		{
			using Type = FXD::U64;

			static CONSTEXPR const Type min(void) { return Math::MIN_U64; }
			static CONSTEXPR const Type max(void) { return Math::MAX_U64; }
			static CONSTEXPR const Type lowest(void) { return NumericLimits< Type >::min(); }
			static CONSTEXPR const Type epsilon(void) { return 0; }
			static CONSTEXPR const Type infinity(void) { return 0; }
			static CONSTEXPR const Type quiet_NaN(void) { return 0; }
			static CONSTEXPR const bool is_bounded = true;
			static CONSTEXPR const bool is_signed = false;
			static CONSTEXPR const bool is_integer = true;
			static CONSTEXPR const bool is_exact = true;
			static CONSTEXPR const bool is_modulo = true;
			static CONSTEXPR const bool has_infinity = false;
			static CONSTEXPR const FXD::S32 digits = LIMITS_DIGITS_U(Type);
			static CONSTEXPR const FXD::S32 digits10 = LIMITS_DIGITS10_U(Type);
			static CONSTEXPR const FXD::S32 max_digits10 = 0;
		};

		template <>
		struct NumericLimits< FXD::S8 >
		{
			using Type = FXD::S8;

			static CONSTEXPR const Type min(void) { return Math::MIN_S8; }
			static CONSTEXPR const Type max(void) { return Math::MAX_S8; }
			static CONSTEXPR const Type lowest(void) { return NumericLimits< Type >::min(); }
			static CONSTEXPR const Type epsilon(void) { return 0; }
			static CONSTEXPR const Type infinity(void) { return 0; }
			static CONSTEXPR const Type quiet_NaN(void) { return 0; }
			static CONSTEXPR const bool is_bounded = true;
			static CONSTEXPR const bool is_signed = true;
			static CONSTEXPR const bool is_integer = true;
			static CONSTEXPR const bool is_exact = true;
			static CONSTEXPR const bool is_modulo = true;
			static CONSTEXPR const bool has_infinity = false;
			static CONSTEXPR const FXD::S32 digits = LIMITS_DIGITS_S(Type);
			static CONSTEXPR const FXD::S32 digits10 = LIMITS_DIGITS10_S(Type);
			static CONSTEXPR const FXD::S32 max_digits10 = 0;
		};

		template <>
		struct NumericLimits< FXD::S16 >
		{
			using Type = FXD::S16;

			static CONSTEXPR const Type min(void) { return Math::MIN_S16; }
			static CONSTEXPR const Type max(void) { return Math::MAX_S16; }
			static CONSTEXPR const Type lowest(void) { return NumericLimits< Type >::min(); }
			static CONSTEXPR const Type epsilon(void) { return 0; }
			static CONSTEXPR const Type infinity(void) { return 0; }
			static CONSTEXPR const Type quiet_NaN(void) { return 0; }
			static CONSTEXPR const bool is_bounded = true;
			static CONSTEXPR const bool is_signed = true;
			static CONSTEXPR const bool is_integer = true;
			static CONSTEXPR const bool is_exact = true;
			static CONSTEXPR const bool is_modulo = true;
			static CONSTEXPR const bool has_infinity = false;
			static CONSTEXPR const FXD::S32 digits = LIMITS_DIGITS_S(Type);
			static CONSTEXPR const FXD::S32 digits10 = LIMITS_DIGITS10_S(Type);
			static CONSTEXPR const FXD::S32 max_digits10 = 0;
		};

		template <>
		struct NumericLimits< FXD::S32 >
		{
			using Type = FXD::S32;

			static CONSTEXPR const Type min(void) { return Math::MIN_S32; }
			static CONSTEXPR const Type max(void) { return Math::MAX_S32; }
			static CONSTEXPR const Type lowest(void) { return NumericLimits< Type >::min(); }
			static CONSTEXPR const Type epsilon(void) { return 0; }
			static CONSTEXPR const Type infinity(void) { return 0; }
			static CONSTEXPR const Type quiet_NaN(void) { return 0; }
			static CONSTEXPR const bool is_bounded = true;
			static CONSTEXPR const bool is_signed = true;
			static CONSTEXPR const bool is_integer = true;
			static CONSTEXPR const bool is_exact = true;
			static CONSTEXPR const bool is_modulo = true;
			static CONSTEXPR const bool has_infinity = false;
			static CONSTEXPR const FXD::S32 digits = LIMITS_DIGITS_S(Type);
			static CONSTEXPR const FXD::S32 digits10 = LIMITS_DIGITS10_S(Type);
			static CONSTEXPR const FXD::S32 max_digits10 = 0;
		};

		template <>
		struct NumericLimits< FXD::S64 >
		{
			using Type = FXD::S64;

			static CONSTEXPR const Type min(void) { return Math::MIN_S64; }
			static CONSTEXPR const Type max(void) { return Math::MAX_S64; }
			static CONSTEXPR const Type lowest(void) { return NumericLimits< Type >::min(); }
			static CONSTEXPR const Type epsilon(void) { return 0; }
			static CONSTEXPR const Type infinity(void) { return 0; }
			static CONSTEXPR const Type quiet_NaN(void) { return 0; }
			static CONSTEXPR const bool is_bounded = true;
			static CONSTEXPR const bool is_signed = true;
			static CONSTEXPR const bool is_integer = true;
			static CONSTEXPR const bool is_exact = true;
			static CONSTEXPR const bool is_modulo = true;
			static CONSTEXPR const bool has_infinity = false;
			static CONSTEXPR const FXD::S32 digits = LIMITS_DIGITS_S(Type);
			static CONSTEXPR const FXD::S32 digits10 = LIMITS_DIGITS10_S(Type);
			static CONSTEXPR const FXD::S32 max_digits10 = 0;
		};

		template <>
		struct NumericLimits< FXD::F32 >
		{
			using Type = FXD::F32;

			static CONSTEXPR const Type min(void) { return Math::MIN_F32; }
			static CONSTEXPR const Type max(void) { return Math::MAX_F32; }
			static CONSTEXPR const Type lowest(void) { return -NumericLimits< Type >::max(); }
			static CONSTEXPR const Type epsilon(void) { return Math::EPSILON_F32; }
			static CONSTEXPR const Type infinity(void) { return (__builtin_huge_valf()); }
			static CONSTEXPR const Type quiet_NaN(void) { return (__builtin_nanf("0")); }
			static CONSTEXPR const bool is_bounded = true;
			static CONSTEXPR const bool is_signed = true;
			static CONSTEXPR const bool is_integer = false;
			static CONSTEXPR const bool is_exact = false;
			static CONSTEXPR const bool is_modulo = false;
			static CONSTEXPR const bool has_infinity = true;
			static CONSTEXPR const FXD::S32 digits = FLT_MANT_DIG;
			static CONSTEXPR const FXD::S32 digits10 = FLT_DIG;
			static CONSTEXPR const FXD::S32 max_digits10 = FLT_MANT_DIG;
		};

		template <>
		struct NumericLimits< FXD::F64 >
		{
			using Type = FXD::F64;

			static CONSTEXPR const Type min(void) { return Math::MIN_F64; }
			static CONSTEXPR const Type max(void) { return Math::MAX_F64; }
			static CONSTEXPR const Type lowest(void) { return -NumericLimits< Type >::max(); }
			static CONSTEXPR const Type epsilon(void) { return Math::EPSILON_F64; }
			static CONSTEXPR const Type infinity(void) { return (__builtin_huge_valf()); }
			static CONSTEXPR const Type quiet_NaN(void) { return (__builtin_nanf("0")); }
			static CONSTEXPR const bool is_bounded = true;
			static CONSTEXPR const bool is_signed = true;
			static CONSTEXPR const bool is_integer = false;
			static CONSTEXPR const bool is_exact = false;
			static CONSTEXPR const bool is_modulo = false;
			static CONSTEXPR const bool has_infinity = true;
			static CONSTEXPR const FXD::S32 digits = LDBL_MANT_DIG;
			static CONSTEXPR const FXD::S32 digits10 = LDBL_DIG;
			static CONSTEXPR const FXD::S32 max_digits10 = LDBL_MANT_DIG;
		};

	} //namespace Math
} //namespace FXD
#endif //FXDENGINE_MATH_LIMITS_H