// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_UNITHELPERS_H
#define FXDENGINE_MATH_UNITHELPERS_H

#include "FXDEngine/Core/Types.h"

namespace FXD
{
	namespace Math
	{
		namespace Weight
		{
			class Kilograms;
			class Pounds;
			class Stone;

			// ------
			// Kilograms
			// -
			// ------
			class Kilograms
			{
			public:
				friend class Pounds;
				friend class Stone;

			private:
				EXPLICIT CONSTEXPR Kilograms(FXD::F64 fVal)
					: m_fVal(fVal)
				{
				}

			public:
				EXPLICIT Kilograms(const Weight::Pounds& lb);
				EXPLICIT Kilograms(const Weight::Stone& st);

				friend CONSTEXPR Kilograms operator""_KG(FXD::F64 val);

				operator FXD::F64() { return m_fVal; }

			private:
				FXD::F64 m_fVal;
			};

			// ------
			// Pounds
			// -
			// ------
			class Pounds
			{
			public:
				friend class Kilograms;
				friend class Stone;

			private:
				EXPLICIT CONSTEXPR Pounds(FXD::F64 fVal)
					: m_fVal(fVal)
				{
				}

			public:
				EXPLICIT Pounds(const Weight::Kilograms& kg);
				EXPLICIT Pounds(const Weight::Stone& st);

				friend CONSTEXPR Pounds operator""_LB(FXD::F64 val);

				operator FXD::F64() { return m_fVal; }

			private:
				FXD::F64 m_fVal;
			};

			// ------
			// Stone
			// -
			// ------
			class Stone
			{
			public:
				friend class Kilograms;
				friend class Pounds;

			private:
				EXPLICIT CONSTEXPR Stone(FXD::F64 fVal)
					: m_fVal(fVal)
				{
				}

			public:
				EXPLICIT Stone(const Weight::Kilograms& kg);
				EXPLICIT Stone(const Weight::Pounds& lb);

				friend CONSTEXPR Stone operator""_ST(FXD::F64 val);

				operator FXD::F64() { return m_fVal; }

			private:
				FXD::F64 m_fVal;
			};

			CONSTEXPR Kilograms operator""_KG(FXD::F64 fKG)	{ return Kilograms(fKG); }
			CONSTEXPR Pounds operator""_LB(FXD::F64 fLB)		{ return Pounds(fLB); }
			CONSTEXPR Stone operator""_ST(FXD::F64 fST)		{ return Stone(fST); }

		} //namespace Weight

		namespace Distance
		{
			class Meters;
			class Inches;
			class Miles;

			// ------
			// Meters
			// -
			// ------
			class Meters
			{
			public:
				friend class Inches;
				friend class Miles;

			private:
				EXPLICIT CONSTEXPR Meters(FXD::F64 fVal)
					: m_fVal(fVal)
				{
				}

			public:
				EXPLICIT Meters(const Distance::Miles& mi);
				EXPLICIT Meters(const Distance::Inches& in);

				friend CONSTEXPR Meters operator""_ME(FXD::F64 val);

				operator FXD::F64() { return m_fVal; }

			private:
				FXD::F64 m_fVal;
			};

			// ------
			// Inches
			// -
			// ------
			class Inches
			{
			public:
				friend class Meters;
				friend class Miles;

			private:
				EXPLICIT CONSTEXPR Inches(FXD::F64 fVal)
					: m_fVal(fVal)
				{
				}

			public:
				EXPLICIT Inches(const Distance::Meters& me);
				EXPLICIT Inches(const Distance::Miles& mi);

				friend CONSTEXPR Inches operator""_IN(FXD::F64 val);

				operator FXD::F64() { return m_fVal; }

			private:
				FXD::F64 m_fVal;
			};

			// ------
			// Miles
			// -
			// ------
			class Miles
			{
			public:
				friend class Meters;
				friend class Inches;

			private:
				EXPLICIT CONSTEXPR Miles(FXD::F64 fVal)
					: m_fVal(fVal)
				{
				}

			public:
				EXPLICIT Miles(const Distance::Meters& me);
				EXPLICIT Miles(const Distance::Inches& in);

				friend CONSTEXPR Miles operator""_MI(FXD::F64 val);

				operator FXD::F64() { return m_fVal; }
			
			private:
				FXD::F64 m_fVal;
			};

			CONSTEXPR Meters operator""_ME(FXD::F64 val)		{ return Meters(val); }
			CONSTEXPR Inches operator""_IN(FXD::F64 val)		{ return Inches(val); }
			CONSTEXPR Miles operator""_MI(FXD::F64 val)		{ return Miles(val); }
		} //namespace Distance

		namespace Temperature
		{
			class Fahrenheit;
			class Celsius;
			class Kelvin;

			// ------
			// Fahrenheit
			// -
			// ------
			class Fahrenheit
			{
			public:
				friend class Celsius;
				friend class Kelvin;

			private:
				EXPLICIT CONSTEXPR Fahrenheit(FXD::F64 fVal)
					: m_fVal(fVal)
				{
				}

			public:
				EXPLICIT Fahrenheit(const Temperature::Celsius& ce);
				EXPLICIT Fahrenheit(const Temperature::Kelvin& ke);

				friend CONSTEXPR Fahrenheit operator""_FH(FXD::F64 val);

				operator FXD::F64() { return m_fVal; }

			private:
				FXD::F64 m_fVal;
			};

			// ------
			// Miles
			// -
			// ------
			class Celsius
			{
			public:
				friend class Fahrenheit;
				friend class Kelvin;

			private:
				EXPLICIT CONSTEXPR Celsius(FXD::F64 fVal)
					: m_fVal(fVal)
				{
				}

			public:
				EXPLICIT Celsius(const Temperature::Fahrenheit& fh);
				EXPLICIT Celsius(const Temperature::Kelvin& ke);

				friend CONSTEXPR Celsius operator""_CE(FXD::F64 val);

				operator FXD::F64() { return m_fVal; }

			private:
				FXD::F64 m_fVal;
			};

			// ------
			// Kelvin
			// -
			// ------
			class Kelvin
			{
			public:
				friend class Fahrenheit;
				friend class Celsius;

			private:
				EXPLICIT CONSTEXPR Kelvin(FXD::F64 fVal)
					: m_fVal(fVal)
				{
				}

			public:
				EXPLICIT Kelvin(const Temperature::Fahrenheit& fh);
				EXPLICIT Kelvin(const Temperature::Celsius& ce);

				friend CONSTEXPR Kelvin operator""_KE(FXD::F64 val);

				operator FXD::F64() { return m_fVal; }

			private:
				FXD::F64 m_fVal;
			};

			CONSTEXPR Fahrenheit operator""_FH(FXD::F64 val) { return Fahrenheit(val); }
			CONSTEXPR Celsius operator""_CE(FXD::F64 val) { return Celsius(val); }
			CONSTEXPR Kelvin operator""_KE(FXD::F64 val) { return Kelvin(val); }
		} //namespace Temperature

	} //namespace Math
} //namespace FXD

using FXD::Math::Weight::operator"" _KG;
using FXD::Math::Weight::operator"" _LB;
using FXD::Math::Weight::operator"" _ST;

using FXD::Math::Distance::operator"" _ME;
using FXD::Math::Distance::operator"" _IN;
using FXD::Math::Distance::operator"" _MI;

using FXD::Math::Temperature::operator"" _FH;
using FXD::Math::Temperature::operator"" _CE;
using FXD::Math::Temperature::operator"" _KE;

#endif //FXDENGINE_MATH_UNITHELPERS_H