// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_MATH_VECTOR3F_H
#define FXDENGINE_MATH_VECTOR3F_H

#include "FXDEngine/Math/Types.h"
#include "FXDEngine/Math/Radian.h"
#include "FXDEngine/Core/StreamOut.h"

namespace FXD
{
	namespace Math
	{
		// ------
		// Vector3F
		// -
		// ------
		class /*FXD_ALIGN_AS(16)*/ Vector3F
		{
		public:
			friend class Matrix3F;
			friend class Matrix4F;

			static const Vector3F kZero;
			static const Vector3F kOne;
			static const Vector3F kInf;
			static const Vector3F kNegInf;
			static const Vector3F kLeft;
			static const Vector3F kRight;
			static const Vector3F kUp;
			static const Vector3F kDown;
			static const Vector3F kForward;
			static const Vector3F kBack;
			static const Vector3F kUnitX;
			static const Vector3F kUnitY;
			static const Vector3F kUnitZ;

		public:
			Vector3F(void);
			Vector3F(FXD::F32 val);
			Vector3F(FXD::F32 x, FXD::F32 y, FXD::F32 z);
			~Vector3F(void);

			FXD::F32 operator[](FXD::U32 nID) const;
			FXD::F32 operator()(FXD::U32 nID) const;

			Vector3F& operator=(const Vector3F& rhs);
			Vector3F& operator=(FXD::F32 rhs);

			friend bool operator==(const Vector3F& lhs, const Vector3F& rhs) NOEXCEPT;
			friend bool operator!=(const Vector3F& lhs, const Vector3F& rhs) NOEXCEPT;

			friend Vector3F operator+(const Vector3F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector3F operator+(const Vector3F& lhs, const Vector3F& rhs) NOEXCEPT;
			friend Vector3F operator-(const Vector3F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector3F operator-(const Vector3F& lhs, const Vector3F& rhs) NOEXCEPT;
			friend Vector3F operator*(const Vector3F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector3F operator*(const Vector3F& lhs, const Vector3F& rhs) NOEXCEPT;
			friend Vector3F operator/(const Vector3F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector3F operator/(const Vector3F& lhs, const Vector3F& rhs) NOEXCEPT;

			friend Vector3F& operator+=(Vector3F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector3F& operator+=(Vector3F& lhs, const Vector3F& rhs) NOEXCEPT;
			friend Vector3F& operator-=(Vector3F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector3F& operator-=(Vector3F& lhs, const Vector3F& rhs) NOEXCEPT;
			friend Vector3F& operator*=(Vector3F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector3F& operator*=(Vector3F& lhs, const Vector3F& rhs) NOEXCEPT;
			friend Vector3F& operator/=(Vector3F& lhs, FXD::F32 rhs) NOEXCEPT;
			friend Vector3F& operator/=(Vector3F& lhs, const Vector3F& rhs) NOEXCEPT;

			Vector3F operator-() const;

			FXD::F32 x(void) const;
			FXD::F32 y(void) const;
			FXD::F32 z(void) const;
			FXD::F32 w(void) const;
			FXD::F32 index(FXD::U32 nID);

			void x(FXD::F32 x);
			void y(FXD::F32 y);
			void z(FXD::F32 z);
			void index(FXD::U32 nID, FXD::F32 fVal);

			FXD::F32 dot(const Vector3F& rhs) const;
			Vector3F cross(const Vector3F& rhs) const;
			FXD::F32 length(void) const;
			FXD::F32 squared_length(void) const;
			FXD::F32 distance(const Vector3F& rhs) const;
			FXD::F32 sqrd_distance(const Vector3F& rhs) const;
			Vector3F midpoint(const Vector3F& rhs) const;
			FXD::F32 angle_between(const Vector3F& dst);

			void negate(void);
			void minimum(const Vector3F& min);
			void maximum(const Vector3F& max);
			void grid_snap(const FXD::F32 fGridSize);
			void clamp(const Vector3F& min, const Vector3F& max);
			void normalise(void);
			void abs(void);
			void ceil(void);
			void floor(void);
			Vector3F get_negate(void) const;
			Vector3F get_minimum(const Vector3F& min) const;
			Vector3F get_maximum(const Vector3F& max) const;
			Vector3F get_grid_snap(const FXD::F32 fGridSize);
			Vector3F get_clamp(const Vector3F& min, const Vector3F& max) const;
			Vector3F get_normalise(void) const;
			Vector3F get_abs(void) const;
			Vector3F get_ceil(void) const;
			Vector3F get_floor(void) const;

			static FXD::F32 dot(const Vector3F& lhs, const Vector3F& rhs);
			static Vector3F cross(const Vector3F& lhs, const Vector3F& rhs);
			static FXD::F32 distance(const Vector3F& lhs, const Vector3F& rhs);
			static FXD::F32 sqrd_distance(const Vector3F& lhs, const Vector3F& rhs);
			static Vector3F midpoint(const Vector3F& lhs, const Vector3F& rhs);

			static void set_negate(Vector3F& vec);
			static void set_minimum(Vector3F& vec, const Vector3F& min);
			static void set_maximum(Vector3F& vec, const Vector3F& max);
			static void set_grid_snap(Vector3F& vec, const FXD::F32 fGridSize);
			static void set_clamp(Vector3F& vec, const Vector3F& min, const Vector3F& max);
			static void set_normalise(Vector3F& val);
			static void set_abs(Vector3F& val);
			static void set_ceil(Vector3F& val);
			static void set_floor(Vector3F& val);
			static void set_lerp(Vector3F& val, const Vector3F& lhs, const Vector3F& rhs, FXD::F32 fVal);
			static Vector3F get_negate(const Vector3F& vec);
			static Vector3F get_minimum(const Vector3F& vec, const Vector3F& min);
			static Vector3F get_maximum(const Vector3F& vec, const Vector3F& max);
			static Vector3F get_grid_snap(const Vector3F& vec, const FXD::F32 fGridSize);
			static Vector3F get_clamp(const Vector3F& vec, const Vector3F& min, const Vector3F& max);
			static Vector3F get_normalise(const Vector3F& val);
			static Vector3F get_abs(const Vector3F& val);
			static Vector3F get_ceil(const Vector3F& val);
			static Vector3F get_floor(const Vector3F& val);
			static Vector3F get_lerp(const Vector3F& lhs, const Vector3F& rhs, FXD::F32 fVal);

		private:
			_vector3FType m_data;
		};

		// Comparison operators
		// Equality
		inline bool operator==(const Vector3F& lhs, const Vector3F& rhs) NOEXCEPT
		{
			return Vec3FCompare(lhs.m_data, rhs.m_data);
		}

		// Inequality
		inline bool operator!=(const Vector3F& lhs, const Vector3F& rhs) NOEXCEPT
		{
			return !Vec3FCompare(lhs.m_data, rhs.m_data);
		}

		// Arithmetic operators
		// Add
		inline Vector3F operator+(const Vector3F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vector3F out;
			Vec3FAdd(out.m_data, lhs.m_data, rhs);
			return out;
		}
		inline Vector3F operator+(const Vector3F& lhs, const Vector3F& rhs) NOEXCEPT
		{
			Vector3F out;
			Vec3FAdd(out.m_data, lhs.m_data, rhs.m_data);
			return out;
		}

		// Subtract
		inline Vector3F operator-(const Vector3F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vector3F out;
			Vec3FSub(out.m_data, lhs.m_data, rhs);
			return out;
		}
		inline Vector3F operator-(const Vector3F& lhs, const Vector3F& rhs) NOEXCEPT
		{
			Vector3F out;
			Vec3FSub(out.m_data, lhs.m_data, rhs.m_data);
			return out;
		}

		// Multiplication
		inline Vector3F operator*(const Vector3F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vector3F out;
			Vec3FMul(out.m_data, lhs.m_data, rhs);
			return out;
		}
		inline Vector3F operator*(const Vector3F& lhs, const Vector3F& rhs) NOEXCEPT
		{
			Vector3F out;
			Vec3FMul(out.m_data, lhs.m_data, rhs.m_data);
			return out;
		}

		// Division
		inline Vector3F operator/(const Vector3F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vector3F out;
			Vec3FDiv(out.m_data, lhs.m_data, rhs);
			return out;
		}
		inline Vector3F operator/(const Vector3F& lhs, const Vector3F& rhs) NOEXCEPT
		{
			Vector3F out;
			Vec3FDiv(out.m_data, lhs.m_data, rhs.m_data);
			return out;
		}

		// Assignment operators
		// Add
		inline Vector3F& operator+=(Vector3F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vec3FAdd(lhs.m_data, lhs.m_data, rhs);
			return lhs;
		}
		inline Vector3F& operator+=(Vector3F& lhs, const Vector3F& rhs) NOEXCEPT
		{
			Vec3FAdd(lhs.m_data, lhs.m_data, rhs.m_data);
			return lhs;
		}

		// Subtract
		inline Vector3F& operator-=(Vector3F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vec3FSub(lhs.m_data, lhs.m_data, rhs);
			return lhs;
		}
		inline Vector3F& operator-=(Vector3F& lhs, const Vector3F& rhs) NOEXCEPT
		{
			Vec3FSub(lhs.m_data, lhs.m_data, rhs.m_data);
			return lhs;
		}

		// Multiplication
		inline Vector3F& operator*=(Vector3F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vec3FMul(lhs.m_data, lhs.m_data, rhs);
			return lhs;
		}
		inline Vector3F& operator*=(Vector3F& lhs, const Vector3F& rhs) NOEXCEPT
		{
			Vec3FMul(lhs.m_data, lhs.m_data, rhs.m_data);
			return lhs;
		}

		// Division
		inline Vector3F& operator/=(Vector3F& lhs, FXD::F32 rhs) NOEXCEPT
		{
			Vec3FDiv(lhs.m_data, lhs.m_data, rhs);
			return lhs;
		}
		inline Vector3F& operator/=(Vector3F& lhs, const Vector3F& rhs) NOEXCEPT
		{
			Vec3FDiv(lhs.m_data, lhs.m_data, rhs.m_data);
			return lhs;
		}

		// Core::IStreamOut
		inline Core::IStreamOut& operator<<(Core::IStreamOut& ostr, Vector3F const& rhs)
		{
			return ostr << rhs(0) << ' ' << rhs(1) << ' ' << rhs(2);
		}

		/*
		// ------
		// Vector3
		// -
		// ------
		class Vector3
		{
		public:
			static const Vector3 kZero;
			static const Vector3 kOne;
			static const Vector3 kInf;
			static const Vector3 kUnitX;
			static const Vector3 kUnitY;
			static const Vector3 kUnitZ;

		public:
			Vector3(void);
			Vector3(FXD::F32 x, FXD::F32 y, FXD::F32 z);
			EXPLICIT Vector3(const Vector4F& vec);
			~Vector3(void);

			Vector3& operator=(const Vector3& rhs);
			Vector3& operator=(FXD::F32 rhs);

			FXD::F32 operator[](FXD::U32 i) const;
			FXD::F32& operator[](FXD::U32 i);

			const Vector3& operator+() const;
			Vector3 operator+(const Vector3& rhs) const;
			Vector3 operator+(FXD::F32 rhs) const;
			Vector3 operator-() const;
			Vector3 operator-(const Vector3& rhs) const;
			Vector3 operator-(FXD::F32 rhs) const;
			Vector3 operator*(const Vector3& rhs) const;
			Vector3 operator*(FXD::F32 rhs) const;
			Vector3 operator/(const Vector3& rhs) const;
			Vector3 operator/(FXD::F32 val) const;

			Vector3& operator+=(const Vector3& rhs);
			Vector3& operator+=(FXD::F32 rhs);
			Vector3& operator-=(const Vector3& rhs);
			Vector3& operator-=(FXD::F32 rhs);
			Vector3& operator*=(const Vector3& rhs);
			Vector3& operator*=(FXD::F32 rhs);
			Vector3& operator/=(const Vector3& rhs);
			Vector3& operator/=(FXD::F32 rhs);

			bool operator== (const Vector3& rhs) const;
			bool operator!= (const Vector3& rhs) const;

			friend Vector3 operator+(const Vector3& lhs, FXD::F32 rhs) { return Vector3((lhs.m_x + rhs), (lhs.m_y + rhs), (lhs.m_z + rhs)); }
			friend Vector3 operator+(FXD::F32 lhs, const Vector3& rhs) { return Vector3((lhs + rhs.m_x), (lhs + rhs.m_y), (lhs + rhs.m_z)); }
			friend Vector3 operator-(const Vector3& lhs, FXD::F32 rhs) { return Vector3((lhs.m_x - rhs), (lhs.m_y - rhs), (lhs.m_z - rhs)); }
			friend Vector3 operator-(FXD::F32 lhs, const Vector3& rhs) { return Vector3((lhs - rhs.m_x), (lhs - rhs.m_y), (lhs - rhs.m_z)); }
			friend Vector3 operator*(FXD::F32 lhs, const Vector3& rhs) { return Vector3((lhs * rhs.m_x), (lhs * rhs.m_y), (lhs * rhs.m_z)); }
			friend Vector3 operator/(FXD::F32 lhs, const Vector3& rhs) { return Vector3((lhs / rhs.m_x), (lhs / rhs.m_y), (lhs / rhs.m_z)); }

			FXD::F32 x(void) const { return m_x; }
			FXD::F32 y(void) const { return m_y; }
			FXD::F32 z(void) const { return m_z; }

			void x(FXD::F32 x) { m_x = x; }
			void y(FXD::F32 y) { m_y = y; }
			void z(FXD::F32 z) { m_z = z; }

			FXD::F32* ptr(void) { return &m_x; }
			const FXD::F32* ptr(void) const { return &m_x; }

			void swap(Vector3& rhs);											// Exchange the contents of this vector with another.
			void normalise(void);												// Normalizes the vector.
			void floor(const Vector3& cmp);									// Sets this vector's components to the minimum of its own and the ones of the passed in vector.
			void ceil(const Vector3& cmp);									// Sets this vector's components to the maximum of its own and the ones of the passed in vector.

			FXD::F32 length(void) const;										// Returns the length (magnitude) of the vector.
			FXD::F32 squared_length(void) const;								// Returns the square of the length(magnitude) of the vector.
			FXD::F32 distance(const Vector3& rhs) const;					//	Returns the distance to another vector.
			FXD::F32 squaredDistance(const Vector3& rhs) const;			// Returns the square of the distance to another vector.
			FXD::F32 dot(const Vector3& vec) const;							// Calculates the dot (scalar) product of this vector with another.
			Vector3 perpendicular(void) const;								// Generates a vector perpendicular to this vector.
			Vector3 cross(const Vector3& rhs) const;						// Calculates the cross-product of 2 vectors, that is, the vector that lies perpendicular to them both.
			bool isZeroLength(void) const;										// Returns true if this vector is zero length.
			Vector3 reflect(const Vector3& normal) const;					// Calculates a reflection vector to the plane with the given normal.
			Radian angle_between(const Vector3& dst) const;				// Gets the angle between 2 vectors.
			void orthogonalComplement(Vector3& a, Vector3& b);			// Calculates two vectors orthonormal to the current vector, and normalizes the current vector if not already.


			static void orthonormalize(Vector3& vec0, Vector3& vec1, Vector3& vec2);	// Performs Gram-Schmidt orthonormalization.
			static Vector3 normalise(const Vector3& val);										// Normalizes the provided vector and returns a new normalized instance.
			static Vector3 floor(const Vector3& val, const Vector3& cmp);					// Gets this vector's components to the minimum of its own and the ones of the passed in vector.
			static Vector3 ceil(const Vector3& val, const Vector3& cmp);					// Gets this vector's components to the maximum of its own and the ones of the passed in vector.
			static FXD::F32 dot(const Vector3& lhs, const Vector3& rhs);					// Calculates the dot (scalar) product of two vectors.
			static Vector3 cross(const Vector3& lhs, const Vector3& rhs);					// Calculates the cross-product of 2 vectors, that is, the vector that lies perpendicular to them both.

		private:
			FXD::F32 m_x;
			FXD::F32 m_y;
			FXD::F32 m_z;
		};
		*/

	} //namespace Math
} //namespace FXD
#endif //FXDENGINE_MATH_VECTOR3F_H