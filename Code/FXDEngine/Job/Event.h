// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_JOB_EVENT_H
#define FXDENGINE_JOB_EVENT_H

#include <functional>

namespace FXD
{
	namespace Job
	{
		class EventHandler;

		// ------
		// IEvent
		// -
		// ------
		class IEvent
		{
		protected:
			IEvent(void)
			{}
			virtual ~IEvent(void)
			{}
		};

		// ------
		// IHandler
		// -
		// ------
		class IHandler
		{
		protected:
			IHandler(void)
			{}
			virtual ~IHandler(void)
			{}
		};

		// ------
		// Handler
		// -
		// ------
		template < typename T >
		class Handler : public Job::IHandler
		{
		public:
			friend class Job::EventHandler;

		protected:
			Handler(FXD::STD::function< void(T) >&& func)
				: m_func(std::move(func))
			{}
			~Handler(void)
			{}

		protected:
			FXD::STD::function< void(T) > m_func;
		};

	} //namespace Job
} //namespace FXD
#endif //FXDENGINE_JOB_EVENT_H