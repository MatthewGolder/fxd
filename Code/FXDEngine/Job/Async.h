// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_JOB_ASYNC_H
#define FXDENGINE_JOB_ASYNC_H

#include "FXDEngine/Job/AsyncJob.h"

namespace FXD
{
	namespace Job
	{
		// ------
		// Async
		// -
		// ------
		template < typename AsyncType, typename FuncType, typename... ArgTypes >
		auto Async(AsyncType* pAsync, FuncType&& inFunc, ArgTypes&&... inArgs)->Job::Future< decltype(inFunc(inArgs...)) >
		{
			using RetType = decltype(inFunc(inArgs...));

			FXD::STD::function< RetType(ArgTypes...) > func(inFunc);
			Job::Promise< RetType > promise;
			Job::Future< RetType > future = promise.get_future();
			
			pAsync->push_command(std::move(promise), std::move(func), std::forward< ArgTypes >(inArgs)...);
			return std::move(future);
		}

	} //namespace Job
} //namespace FXD
#endif //FXDENGINE_JOB_ASYNC_H