// Creator - MatthewGolder
#include "FXDEngine/Job/AsyncJob.h"

using namespace FXD;
using namespace Job;

// ------
// AsyncCancel
// -
// ------
AsyncCancel::AsyncCancel(void)
	: m_cancelled(0)
	, m_pParent(nullptr)
{
}

AsyncCancel::~AsyncCancel(void)
{
}

void AsyncCancel::cancel(void)
{
	m_cancelled.increment();
}

void AsyncCancel::uncancel(void)
{
	m_cancelled.decrement();
}

bool AsyncCancel::is_cancelled(void) const
{
	if ((m_pParent != nullptr) && (m_pParent->is_cancelled()))
	{
		return true;
	}
	return (m_cancelled.get_instant_value() != 0);
}

bool AsyncCancel::is_self_cancelled(void) const
{
	return (m_cancelled.get_instant_value() != 0);
}

// ------
// IAsyncTaskPool::Shared
// -
// ------
IAsyncTaskPool::Shared::Shared()
	: m_eState(E_AsyncState::Initializing)
{
}

IAsyncTaskPool::Shared::~Shared()
{
}

// ------
// IAsyncTaskPool
// -
// ------
IAsyncTaskPool::IAsyncTaskPool(void)
{
}

IAsyncTaskPool::~IAsyncTaskPool(void)
{
}

const FXD::S32 IAsyncTaskPool::commands_per_idle(void) const
{
	return 4;
}

bool IAsyncTaskPool::process_pool(void)
{
	FXD::S32 nCount = 0;
	const FXD::S32 nIdleTimeEveryN = commands_per_idle();

	while (true)
	{
		// 1. Check if we are shutting down
		if (m_shared.get_quick_read_lock()->m_eState == E_AsyncState::ShuttingDown)
		{
			return false;
		}

		// 2. Get next command
		Job::IAsyncTask* pTask = nullptr;
		{
			Thread::RWLockData< Job::IAsyncTaskPool::Shared >::ScopedWriteLock lock(m_shared);
			if (lock->m_commands.not_empty())
			{
				pTask = lock->m_commands.front();
				lock->m_commands.pop();
			}
		}

		// 3. Perform next command
		if (pTask != nullptr)
		{
			pTask->run_task();
			delete pTask;
			pTask = nullptr;

			if (nCount++ >= nIdleTimeEveryN)
			{
				idle_process();
				return true;
			}
		}
		else
		{
			idle_process();
			return true;
		}
	}
}

void IAsyncTaskPool::issue_stop_signal(void)
{
	m_shared.get_quick_write_lock()->m_eState = E_AsyncState::ShuttingDown;
}

void IAsyncTaskPool::wait_for_shutdown(void)
{
	while (!is_shutdown())
	{
		Thread::Funcs::Sleep(50);
	}
}

bool IAsyncTaskPool::is_shuttingdown(void) const
{
	return (m_shared.get_quick_read_lock()->m_eState == E_AsyncState::ShuttingDown);
}

bool IAsyncTaskPool::is_shutdown(void) const
{
	return (m_shared.get_quick_read_lock()->m_eState == E_AsyncState::ShutDown);
}

bool IAsyncTaskPool::is_initialized(void) const
{
	return (m_shared.get_quick_read_lock()->m_eState == E_AsyncState::Running);
}

// ------
// IAsyncTaskPoolThread
// -
// ------
IAsyncTaskPoolThread::IAsyncTaskPoolThread(const Core::StringView8 strName)
	: IThread(strName.c_str())
{
	start_thread();
}

IAsyncTaskPoolThread::~IAsyncTaskPoolThread(void)
{
	while (is_active())
	{
		Thread::Funcs::Sleep(0);
	}
}

void IAsyncTaskPoolThread::stop_and_wait_for_exit(void)
{
	while (is_active())
	{
		Thread::Funcs::Sleep(0);
	}
}

bool IAsyncTaskPoolThread::thread_init_func(void)
{
	bool bRetVal = init_pool();
	return bRetVal;
}

bool IAsyncTaskPoolThread::thread_process_func(void)
{
	bool bRetVal = process_pool();
	return bRetVal;
}

bool IAsyncTaskPoolThread::thread_shutdown_func(void)
{
	bool bRetVal = shutdown_pool();
	return bRetVal;
}

// ------
// IAsyncJobThread
// -
// ------
IAsyncJobThread::IAsyncJobThread(void)
	: IThread("IAsyncJobThread")
	, m_eState(E_AsyncState::Initializing)
{
	start_thread();
}

IAsyncJobThread::~IAsyncJobThread(void)
{
	if (!join())
	{
		PRINT_ASSERT << "Job: IAsyncJobThread took too long to shutdown";
	}
}

bool IAsyncJobThread::thread_init_func(void)
{
	m_eState = E_AsyncState::Running;
	return true;
}

bool IAsyncJobThread::thread_process_func(void)
{
	if ((m_eState == E_AsyncState::ShuttingDown) || (m_eState == E_AsyncState::ShutDown))
	{
		return false;
	}
	m_wakeUp.wait_for(100);
	if ((m_eState == E_AsyncState::ShuttingDown) || (m_eState == E_AsyncState::ShutDown))
	{
		return false;
	}
	if (m_pAsyncTask != nullptr)
	{
		m_pAsyncTask->run_task();
		FXD_DELETE(m_pAsyncTask);
		m_pAsyncTask = nullptr;
	}
	return true;
}

bool IAsyncJobThread::thread_shutdown_func(void)
{
	m_eState = E_AsyncState::ShutDown;
	return true;
}

void IAsyncJobThread::set_async_task(Job::IAsyncTask* pAsyncTask)
{
	m_pAsyncTask = pAsyncTask;
	m_wakeUp.set();
}

void IAsyncJobThread::begin_shutdown(void)
{
	m_eState = E_AsyncState::ShuttingDown;
}

bool IAsyncJobThread::is_running(void) const
{
	return (m_pAsyncTask != nullptr);
}

bool IAsyncJobThread::is_shutdown(void) const
{
	return (m_eState == E_AsyncState::ShutDown);
}

// 0 - Not Initialised
// 1 - Initialised
// 2 - ShutDown
FXD::S32 nAsyncTaskMasterStatus = 0;

// ------
// AsyncJobRunner
// -
// ------
AsyncJobRunner::AsyncJobRunner(void)
	: m_bShutDown(false)
{
	if (nAsyncTaskMasterStatus == 2)
	{
		PRINT_ASSERT << "Job: AsyncJobRunner accessed after shutdown";
	}
	nAsyncTaskMasterStatus = 1;
}

AsyncJobRunner::~AsyncJobRunner(void)
{
	Thread::CSLock::LockGuard lock(m_cs);
	m_asyncTaskThreads.clear();
}

void AsyncJobRunner::begin_shutdown(void)
{
	Thread::CSLock::LockGuard lock(m_cs);
	fxd_for(auto& thread, m_asyncTaskThreads)
	{
		thread->begin_shutdown();
	}
	nAsyncTaskMasterStatus = 2;
}

bool AsyncJobRunner::is_shutdown(void)
{
	return m_bShutDown;
}

void AsyncJobRunner::wait_for_shutdown(FXD::U32 nMilliseconds)
{
	bool bFinished = true;
	while (!is_shutdown())
	{
		Thread::Funcs::Sleep(nMilliseconds);

		//FXD::STD::all_of(m_asyncTaskThreads.begin(), m_asyncTaskThreads.end(), [](Job::AsyncJobThread thread)
		//{
		//	return true;
		//});

		bFinished = true;
		fxd_for(const auto& thread, m_asyncTaskThreads)
		{
			if (!thread->is_shutdown())
			{
				bFinished = false;
			}
		}
		if (bFinished)
		{
			m_bShutDown = true;
			return;
		}
	}
}