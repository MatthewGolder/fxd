// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_JOB_ASYNCJOB_H
#define FXDENGINE_JOB_ASYNCJOB_H

#include "FXDEngine/Container/Singleton.h"
#include "FXDEngine/Container/Tuple.h"
#include "FXDEngine/Container/Queue.h"
#include "FXDEngine/Core/StringView.h"
#include "FXDEngine/Thread/AtomicInt.h"
#include "FXDEngine/Thread/Thread.h"
#include "FXDEngine/Thread/RWLock.h"
#include "FXDEngine/Job/Future.h"

#include <functional>

namespace FXD
{
	namespace Job
	{
		namespace Internal
		{
			namespace _Helper
			{
				template < FXD::S32... Is >
				struct index
				{};

				template < FXD::S32 N, FXD::S32... Is >
				struct gen_seq : gen_seq< N - 1, N - 1, Is... >
				{};

				template < FXD::S32... Is >
				struct gen_seq< 0, Is... > : index< Is... >
				{};
			}
		} //namespace Internal

		// ------
		// AsyncCancel
		// -
		// ------
		class AsyncCancel
		{
		public:
			AsyncCancel(void);
			~AsyncCancel(void);

			void cancel(void);
			void uncancel(void);
			bool is_cancelled(void) const;
			bool is_self_cancelled(void) const;

		private:
			NO_COPY(AsyncCancel);
			NO_ASSIGNMENT(AsyncCancel);

		private:
			Thread::AtomicInt m_cancelled;
			const AsyncCancel* m_pParent;
		};

		// ------
		// IAsyncTask
		// -
		// ------
		class IAsyncTask
		{
		public:
			IAsyncTask(void)
			{}
			virtual ~IAsyncTask(void)
			{}

			virtual void run_task(void) PURE;
		};

		// ------
		// AsyncTask
		// -
		// ------
		template < typename RetType, typename... ArgTypes >
		class AsyncTask : public Job::IAsyncTask
		{
		public:
			AsyncTask(Job::Promise< RetType >&& promise, FXD::STD::function< RetType(ArgTypes...) >&& func, ArgTypes&&... inArgs)
				: m_promise(std::move(promise))
				, m_func(std::move(func))
				, m_args(std::forward< ArgTypes >(inArgs)...)
			{}
			~AsyncTask(void)
			{}

			template < typename... Args, int... Is >
			RetType func(Container::Tuple< Args... >& tup, Internal::_Helper::index< Is... >)
			{
				return std::move(m_func(FXD::get< Is >(tup)...));
			}

			template < typename... Args >
			RetType func(Container::Tuple< Args... >& tup)
			{
				return std::move(func(tup, Internal::_Helper::gen_seq< sizeof...(Args) >{}));
			}

			void run_task(void) FINAL
			{
				m_promise.set_value(std::move(func(m_args)));
			}

		protected:
			Job::Promise< RetType > m_promise;
			FXD::STD::function< RetType(ArgTypes...) > m_func;
			Container::Tuple< ArgTypes... > m_args;
		};

		// ------
		// AsyncTask< Void >
		// -
		// ------
		template < typename... ArgTypes >
		class AsyncTask< void, ArgTypes... > : public Job::IAsyncTask
		{
		public:
			AsyncTask(Job::Promise< void >&& promise, FXD::STD::function< void(ArgTypes...) >&& func, ArgTypes&&... inArgs)
				: m_promise(std::move(promise))
				, m_func(std::move(func))
				, m_args(std::forward< ArgTypes >(inArgs)...)
			{}
			~AsyncTask(void)
			{}

			template < typename... Args, int... Is >
			void func(Container::Tuple< Args... >& tup, Internal::_Helper::index< Is... >)
			{
				m_func(FXD::get< Is >(tup)...);
			}

			template < typename... Args >
			void func(Container::Tuple< Args... >& tup)
			{
				func(tup, Internal::_Helper::gen_seq< sizeof...(Args) >{});
			}

			void run_task(void) FINAL
			{
				func(m_args);
				m_promise.set_value();
			}

		protected:
			Job::Promise< void > m_promise;
			FXD::STD::function< void(ArgTypes...) > m_func;
			Container::Tuple< ArgTypes... > m_args;
		};


		// ------
		// E_AsyncState
		// -
		// ------
		enum class E_AsyncState
		{
			Initializing = 0,
			Running,
			ShuttingDown,
			ShutDown,
			Count,
			Unknown = 0xffff
		};


		// ------
		// IAsyncTaskPool
		// -
		// ------
		class IAsyncTaskPool : public Core::NonCopyable
		{
		public:
			// ------
			// Shared
			// -
			// ------
			class Shared
			{
			public:
				Shared(void);
				~Shared(void);

			public:
				Job::E_AsyncState m_eState;
				Container::Queue< Job::IAsyncTask* > m_commands;
			};

		public:
			IAsyncTaskPool(void);
			virtual ~IAsyncTaskPool(void);

			template < typename RetType, typename... ArgTypes >
			void push_command(Job::Promise< RetType >&& promise, FXD::STD::function< RetType(ArgTypes...) >&& func, ArgTypes&&... inArgs)
			{
				if (!is_thread_restricted())
				{
					Job::AsyncTask< RetType, ArgTypes... > task(std::move(promise), std::move(func), std::forward< ArgTypes >(inArgs)...);
					task.run_task();
				}
				else
				{
					Job::AsyncTask< RetType, ArgTypes... >* pAsyncTask = new Job::AsyncTask< RetType, ArgTypes... >(std::move(promise), std::move(func), std::forward< ArgTypes >(inArgs)...);
					m_shared.get_quick_write_lock()->m_commands.push(pAsyncTask);
				}
			}

			void issue_stop_signal(void);
			bool is_initialized(void) const;
			bool is_shuttingdown(void) const;
			bool is_shutdown(void) const;
			void wait_for_shutdown(void);

			virtual const Core::StringView8 display_name(void) const PURE;

		protected:
			virtual const FXD::S32 commands_per_idle(void) const;

			bool process_pool(void);

			virtual bool init_pool(void) PURE;
			virtual bool shutdown_pool(void) PURE;
			virtual void idle_process(void) PURE;
			virtual bool is_thread_restricted(void) const PURE;

		protected:
			Thread::RWLockData< Job::IAsyncTaskPool::Shared > m_shared;
		};
		
		// ------
		// IAsyncTaskPoolThread
		// -
		// ------
		class IAsyncTaskPoolThread : public Job::IAsyncTaskPool, public Thread::IThread
		{
		public:
			IAsyncTaskPoolThread(const Core::StringView8 strName);
			virtual ~IAsyncTaskPoolThread(void);

			void stop_and_wait_for_exit(void);

		protected:
			virtual bool init_pool(void) PURE;
			virtual bool shutdown_pool(void) PURE;
			virtual void idle_process(void) PURE;

			bool thread_init_func(void) FINAL;
			bool thread_process_func(void) FINAL;
			bool thread_shutdown_func(void) FINAL;
		};

		// ------
		// IAsyncJobThread
		// -
		// ------
		class IAsyncJobThread : public Thread::IThread
		{
		public:
			IAsyncJobThread(void);
			virtual ~IAsyncJobThread(void);

			bool thread_init_func(void) FINAL;
			bool thread_process_func(void) FINAL;
			bool thread_shutdown_func(void) FINAL;

			void set_async_task(Job::IAsyncTask* pAsyncTask);
			void begin_shutdown(void);
			bool is_running(void) const;
			bool is_shutdown(void) const;

		protected:
			Job::E_AsyncState m_eState;
			Job::IAsyncTask* m_pAsyncTask;
			Thread::Event m_wakeUp;
		};
		using AsyncJobThread = std::shared_ptr< Job::IAsyncJobThread >;


		// ------
		// AsyncJobRunner
		// -
		// ------
		class AsyncJobRunner FINAL : public Core::NonCopyable
		{
		public:
			AsyncJobRunner(void);
			~AsyncJobRunner(void);

		public:
			void begin_shutdown(void);
			bool is_shutdown(void);
			void wait_for_shutdown(FXD::U32 nMilliseconds = 100);

			template < typename RetType, typename... ArgTypes >
			void push_command(Job::Promise< RetType >&& promise, FXD::STD::function< RetType(ArgTypes...) >&& func, ArgTypes&&... inArgs)
			{
				Thread::CSLock::LockGuard lock(m_cs);
				Job::AsyncTask< RetType, ArgTypes... >* pAsyncTask = new Job::AsyncTask< RetType, ArgTypes... >(std::move(promise), std::move(func), std::forward< ArgTypes >(inArgs)...);
				fxd_for(auto& thread, m_asyncTaskThreads)
				{
					if (!thread->is_running())
					{
						thread->set_async_task(pAsyncTask);
						return;
					}
				}
				Job::AsyncJobThread newThread = std::make_shared< Job::IAsyncJobThread >();
				newThread->set_async_task(pAsyncTask);
				m_asyncTaskThreads.push_back(newThread);
			}

		protected:
			Thread::CSLock m_cs;
			bool m_bShutDown;
			Container::Vector< Job::AsyncJobThread > m_asyncTaskThreads;
		};

	} //namespace Job
} //namespace FXD
#endif //FXDENGINE_JOB_ASYNCJOB_H