// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_JOB_EVENTHANDLER_H
#define FXDENGINE_JOB_EVENTHANDLER_H

#include "FXDEngine/Job/Event.h"
#include "FXDEngine/Container/Map.h"
#include "FXDEngine/Container/Vector.h"

#include <typeindex>

namespace FXD
{
	namespace Job
	{
		// ------
		// EventHandler
		// -
		// ------
		class EventHandler FINAL : public Core::NonCopyable
		{
		public:
			EventHandler(void)
			{}
			~EventHandler(void)
			{}

			template < typename T >
			const Job::Handler< T >* attach(FXD::STD::function< void(T) >&& func)
			{
				CTC_IS_BASE_OF(Job::IEvent, T);

				const FXD::HashValue hash = typeid(T).hash_code();

				auto handlers = m_handlersMap.find(hash);
				if (handlers == m_handlersMap.end())
				{
					m_handlersMap[hash] = Container::Vector< Job::IHandler* >();
					handlers = m_handlersMap.find(hash);
				}

				Job::Handler< T >* pHandler = FXD_NEW(Job::Handler< T >)(std::move(func));
				(*handlers).second.push_back(pHandler);
				return pHandler;
			}

			template < typename T >
			void detach(const Job::Handler< T >* pHandler)
			{
				CTC_IS_BASE_OF(Job::IEvent, T);

				const FXD::HashValue hash = typeid(T).hash_code();

				auto handlers = m_handlersMap.find(hash);
				if (handlers != m_handlersMap.end())
				{
					auto handler = (*handlers).second.find((Job::IHandler*)pHandler);
					if (handler != (*handlers).second.end())
					{
						FXD_SAFE_DELETE(pHandler);
						(*handlers).second.erase(handler);
					}
				}
			}

			template < typename T >
			void raise(const T& type)
			{
				CTC_IS_BASE_OF(Job::IEvent, T);

				const FXD::HashValue hash = typeid(T).hash_code();

				auto handlers = m_handlersMap.find(hash);
				if (handlers != m_handlersMap.end())
				{
					fxd_for(const auto& a, (*handlers).second)
					{
						Job::Handler< T >* pHandler = (Job::Handler< T >*)a;
						if (pHandler != nullptr)
						{
							pHandler->m_func(type);
						}
					}
				}
			}

		private:
			Container::Map< FXD::HashValue, Container::Vector< Job::IHandler* > > m_handlersMap;
		};

	} //namespace Job
} //namespace FXD
#endif //FXDENGINE_JOB_EVENTHANDLER_H