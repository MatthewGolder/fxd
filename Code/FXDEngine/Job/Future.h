// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_JOB_FUTURE_H
#define FXDENGINE_JOB_FUTURE_H

#include "FXDEngine/Thread/Event.h"
#include "FXDEngine/Core/Debugger.h"
//#include "FXDEngine/Job/Async.h"

#include <type_traits>

namespace FXD
{
	namespace Job
	{
		template < typename ResultType >
		class Future;

		template < typename AsyncType, typename FuncType, typename... ArgTypes >
		auto Async(AsyncType* pAsync, FuncType&& inFunc, ArgTypes&&... inArgs)->Job::Future< decltype(inFunc(inArgs...)) >;

		// ------
		// BaseFutureState
		// - Class for the internal state of asynchronous return values (futures).
		// ------
		class BaseFutureState
		{
		public:
			BaseFutureState(void)
				: m_bComplete(false)
				, m_bRetrieved(false)
			{}
			virtual ~BaseFutureState(void)
			{}

		public:
			bool is_complete(void) const
			{
				return m_bComplete;
			}

			bool already_retrieved(void) const
			{
				return m_bRetrieved;
			}

			bool wait_for(FXD::U32 nMilliseconds = 1000) const
			{
				if (m_completionEvent.wait_for(nMilliseconds))
				{
					return true;
				}
				return false;
			}

		protected:
			void mark_complete(void)
			{
				m_bComplete = true;
				m_completionEvent.set();
			}

			void mark_retrieved(void) const
			{
				m_bRetrieved = true;
			}

		private:
			Thread::Event m_completionEvent;
			bool m_bComplete;
			mutable bool m_bRetrieved;
		};

		// ------
		// FutureState
		// - Implements the internal state of asynchronous return values (futures).
		// ------
		template < typename InternalResultType >
		class FutureState : public Job::BaseFutureState
		{
		public:
			FutureState(void)
				: Job::BaseFutureState()
			{}
			~FutureState(void)
			{}

		public:
			const InternalResultType& get_result(void) const
			{
				while (!is_complete())
				{
					wait_for();
				}
				mark_retrieved();
				return m_result;
			}

			void set_result(const InternalResultType& InResult)
			{
				PRINT_COND_ASSERT(!is_complete(), "Job:");
				m_result = InResult;
				mark_complete();
			}

			void set_result(InternalResultType&& InResult)
			{
				PRINT_COND_ASSERT(!is_complete(), "Job:");
				m_result = InResult;
				mark_complete();
			}

		private:
			InternalResultType m_result;
		};


		// ------
		// IFutureBase
		// - Abstract base template for futures and shared futures.
		// ------
		template < typename InternalResultType >
		class IFutureBase
		{
		public:
			bool is_ready(void) const
			{
				return (!m_state == false) ? m_state->is_complete() : false;
			}

			bool is_valid(void) const
			{
				return (!m_state == false) && (m_state->already_retrieved() == false);
			}

			void wait(void) const
			{
				while (!wait_for(1000))
				{}
			}

			bool wait_for(FXD::U32 nMilliseconds = 1000) const
			{
				return (!m_state == false) ? m_state->wait_for(nMilliseconds) : false;
			}

		protected:
			using state_type = std::shared_ptr< Job::FutureState< InternalResultType > >;

			IFutureBase(void)
			{}
			IFutureBase(const state_type& inState)
				: m_state(inState)
			{}
			IFutureBase(IFutureBase&& rhs)
				: m_state(std::move(rhs.m_state))
			{
				rhs.m_state.reset();
			}
			~IFutureBase(void)
			{}

			IFutureBase& operator=(IFutureBase&& rhs)
			{
				m_state = std::move(rhs.m_state);
				rhs.m_state.reset();
				return (*this);
			}

		protected:
			const state_type& get_state(void) const
			{
				PRINT_COND_ASSERT((!m_state == false), "Job: Future has an invalid state");
				return m_state;
			}

		private:
			state_type m_state;
		};

		// ------
		// Future
		// - Template for unshared futures.
		// ------
		template < typename ResultType >
		class Future : public Job::IFutureBase< ResultType >
		{
		public:
			using base_type = Job::IFutureBase< ResultType >;
			using my_type = Future< ResultType >;

		public:
			Future(void)
			{}
			Future(const typename base_type::state_type& inState)
				: base_type(inState)
			{}
			Future(Future&& rhs)
				: base_type(std::move(rhs))
			{}
			~Future(void)
			{}

			Future& operator=(Future&& rhs)
			{
				base_type::operator=(std::move(rhs));
				return (*this);
			}

			ResultType get(void) const
			{
				return std::move(this->get_state()->get_result());
			}

			template < typename AsyncType, typename FuncType >
			auto then(AsyncType* pAsync, FuncType&& func)->Job::Future< decltype(func(*this)) >
			{
				return Job::Async(pAsync, std::move(func), (*this));
			}

		private:
			NO_COPY(Future);
			NO_ASSIGNMENT(Future);
		};

		// ------
		// Future
		// - Template for unshared futures (specialization for reference types).
		// ------
		template < typename ResultType >
		class Future< ResultType& > : public Job::IFutureBase< ResultType >
		{
		public:
			using base_type = Job::IFutureBase< ResultType* >;

		public:
			Future(void)
			{}
			Future(const typename base_type::state_type& inState)
				: base_type(inState)
			{}
			Future(Future&& rhs)
				: base_type(std::move(rhs))
			{}
			~Future(void)
			{}

			Future& operator=(Future&& rhs)
			{
				base_type::operator=(std::move(rhs));
				return (*this);
			}

			ResultType& get(void) const
			{
				return (*this)->get_state()->get_result();
			}

			template < typename AsyncType, typename FuncType >
			auto then(AsyncType* pAsync, FuncType&& func)->Job::Future< decltype(func(*this)) >
			{
				return Job::Async(pAsync, std::move(func), (*this));
			}

		private:
			NO_COPY(Future);
			NO_ASSIGNMENT(Future);
		};

		// ------
		// Future
		// - Template for unshared futures (specialization for void).
		// ------
		template <>
		class Future< void > : public Job::IFutureBase< FXD::S32 >
		{
		public:
			using base_type = Job::IFutureBase< FXD::S32 >;
			using my_type = Future< void >;

		public:
			Future(void)
			{}
			Future(const base_type::state_type& InState)
				: base_type(InState)
			{}
			Future(Future&& rhs)
				: base_type(std::move(rhs))
			{}
			~Future(void)
			{}

			Future& operator=(Future&& rhs)
			{
				base_type::operator=(std::move(rhs));
				return (*this);
			}

			void get(void) const
			{
				get_state()->get_result();
			}

			template < typename AsyncType, typename FuncType >
			auto then(AsyncType* pAsync, FuncType&& func)->Job::Future< decltype(func(*this)) >
			{
				return Job::Async(pAsync, std::move(func), (*this));
			}

		private:
			NO_COPY(Future);
			NO_ASSIGNMENT(Future);
		};


		// ------
		// IPromiseBase
		// -
		// ------
		template < typename InternalResultType >
		class IPromiseBase : FXD::Core::NonCopyable
		{
			using state_type = std::shared_ptr< Job::FutureState< InternalResultType > >;

		public:
			IPromiseBase(void)
				: m_state(std::make_shared< Job::FutureState< InternalResultType > >())
			{}
			IPromiseBase(IPromiseBase&& rhs)
				: m_state(std::move(rhs.m_state))
			{
				rhs.m_state.reset();
			}

			IPromiseBase& operator=(IPromiseBase&& rhs)
			{
				m_state = rhs.m_state;
				rhs.m_state.reset();
				return (*this);
			}

		protected:
			~IPromiseBase(void)
			{
				if (!m_state == false)
				{
					PRINT_COND_ASSERT((m_state->is_complete()), "Job: Promise has not completed");
				}
			}

			const state_type& get_state(void)
			{
				PRINT_COND_ASSERT((!m_state == false), "Job: Promise has an invalid state");
				return m_state;
			}

		private:
			state_type m_state;
		};

		// ------
		// Promise
		// -
		// ------
		template < typename ResultType >
		class Promise : public Job::IPromiseBase< ResultType >
		{
		public:
			using base_type = Job::IPromiseBase< ResultType >;

			Promise(void)
				: base_type()
				, m_bFutureRetrieved(false)
			{}
			Promise(Promise&& rhs)
				: base_type(std::move(rhs))
				, m_bFutureRetrieved(std::move(rhs.m_bFutureRetrieved))
			{}

			Promise& operator=(Promise&& rhs)
			{
				base_type::operator=(std::move(rhs));
				m_bFutureRetrieved = std::move(rhs.m_bFutureRetrieved);
				return this;
			}

		public:
			Job::Future< ResultType > get_future(void)
			{
				PRINT_COND_ASSERT(!m_bFutureRetrieved, "Job:");
				m_bFutureRetrieved = true;
				return Job::Future< ResultType >(this->get_state());
			}

			void set_value(const ResultType& result)
			{
				this->get_state()->set_result(result);
			}
			void set_value(ResultType&& result)
			{
				this->get_state()->set_result(std::move(result));
			}

		private:
			bool m_bFutureRetrieved;
		};

		// ------
		// Promise
		// - Template for promises (specialization for reference types).
		// ------
		template < typename ResultType >
		class Promise< ResultType& > : public Job::IPromiseBase< ResultType* >
		{
			using base_type = Job::IPromiseBase< ResultType* >;

		public:
			Promise(void)
				: base_type()
				, m_bFutureRetrieved(false)
			{}
			Promise(Promise&& rhs)
				: base_type(std::move(rhs))
				, m_bFutureRetrieved(std::move(rhs.m_bFutureRetrieved))
			{}

			Promise& operator=(Promise&& rhs)
			{
				base_type::operator=(std::move(rhs));
				m_bFutureRetrieved = std::move(rhs.m_bFutureRetrieved);
				return this;
			}

		public:
			Job::Future< ResultType& > get_future(void)
			{
				PRINT_COND_ASSERT((!m_bFutureRetrieved), "Job: Future has already been retrieved");
				m_bFutureRetrieved = true;
				return Job::Future< ResultType& >(this->get_state());
			}

			void set_value(ResultType& Result)
			{
				this->get_state()->set_result(Result);
			}

		private:
			bool m_bFutureRetrieved;
		};

		// ------
		// Promise
		// - Template for promises (specialization for void results).
		// ------
		template <>
		class Promise< void > : public Job::IPromiseBase< FXD::S32 >
		{
			using base_type = Job::IPromiseBase< FXD::S32 >;

		public:
			Promise(void)
				: base_type()
				, m_bFutureRetrieved(false)
			{}
			Promise(Promise&& rhs)
				: base_type(std::move(rhs))
				, m_bFutureRetrieved(false)
			{}

			Promise& operator=(Promise&& rhs)
			{
				base_type::operator=(std::move(rhs));
				m_bFutureRetrieved = std::move(rhs.m_bFutureRetrieved);
				return (*this);
			}

		public:
			Job::Future< void > get_future(void)
			{
				PRINT_COND_ASSERT((!m_bFutureRetrieved), "Job: Future has already been retrieved");
				m_bFutureRetrieved = true;
				return Job::Future< void >(get_state());
			}

			void set_value(void)
			{
				get_state()->set_result(0);
			}

		private:
			bool m_bFutureRetrieved;
		};

	} //namespace Job
} //namespace FXD
#endif //FXDENGINE_JOB_FUTURE_H