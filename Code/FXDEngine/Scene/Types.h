// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SCENE_TYPES_H
#define FXDENGINE_SCENE_TYPES_H

#include <memory>

namespace FXD
{
	namespace Scene
	{
		class IGeo;
		class IGeoCollada;
		typedef std::shared_ptr< IGeo > Geo;

	} //namespace Scene
} //namespace FXD
#endif //FXDENGINE_SCENE_TYPES_H