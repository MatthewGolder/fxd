// Creator - MatthewGolder
#include "FXDEngine/Scene/Assets/GeoCollada.h"
#include "FXDEngine/IO/XML/XMLSaxParser.h"
#include "FXDEngine/Container/Map.h"
#include "FXDEngine/Core/Colour.h"
#include "FXDEngine/Core/String.h"
#include "FXDEngine/Memory/Log/AllocStats.h"

using namespace FXD;
using namespace Scene;

struct ImageData
{
	Core::String8 id;
	Core::String8 name;
	Core::String8 from;
};

struct EffectData
{
	struct Param
	{
		struct Sampler2D
		{
			Core::String8 source;
			Core::String8 min_filter;
			Core::String8 mag_filter;
		};

		struct Surface
		{
			Core::String8 from;
			Core::String8 type;
			Core::String8 format;
		};

		Core::String8 sid;
		Container::Vector< Surface > surfaces;
		Container::Vector< Sampler2D > samplers;
	};

	struct Technique
	{
		Core::String8 sid;
		Core::String8 texture;
		Core::String8 texcoord;
		Core::String8 colour;
	};

	Core::String8 id;
	Core::String8 name;
	Container::Vector< Param > params;
	Container::Vector< Technique > techniques;
};

struct MaterialData
{
	Core::String8 id;
	Core::String8 name;
	Core::String8 url;
};

struct GeoData
{
	struct MeshData
	{
		struct Source
		{
			Core::String8 id;
			FXD::U32 stride;
			FXD::U32 count;
			Container::Vector< FXD::F32 > array;
		};

		struct Verts
		{
			struct Inputs
			{
				Core::String8 semantic;
				Core::String8 source;
			};
			Core::String8 id;
			Container::Vector< Inputs > inputs;
		};

		struct Tris
		{
			struct Inputs
			{
				Core::String8 semantic;
				Core::String8 source;
				FXD::U32 set = 0;
				FXD::U32 offest = 0;
			};
			Core::String8 material;
			FXD::U32 count = 0;
			Container::Vector< Inputs > inputs;
			Container::Vector< FXD::S16 > indices;
		};

		Container::Vector< Tris > tris;
		Container::Vector< Verts > verts;
		Container::Vector< Source > sources;
	};

	Core::String8 id;
	Core::String8 name;
	Container::Vector< MeshData > meshes;
};

struct SceneDataData
{
};

struct ColladaData
{
	Container::Vector< ImageData > images;
	Container::Vector< EffectData > effects;
	Container::Vector< MaterialData > materials;
	Container::Vector< GeoData > geos;
	//Container::Vector< SceneDataData > scenes;
};

// ------
// ColladaSurfaceTagCB
// -
// ------
class ColladaSurfaceTagCB FINAL : public IO::XMLSaxParser::ITagCB, public Core::NonCopyable
{
public:
	enum class E_Tag
	{
		InitFrom = 0,
		Format,
		Unknown = 0xffff
	};

public:
	ColladaSurfaceTagCB(IO::XMLSaxParser::ITagCB* pParent, EffectData::Param::Surface& surface)
		: m_eTag(ColladaSurfaceTagCB::E_Tag::Unknown)
		, m_pParent(pParent)
		, m_surface(surface)
	{
	}
	~ColladaSurfaceTagCB(void)
	{
	}

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL
	{
		pTagCB = this;
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("init_from"))
				{
					m_eTag = E_Tag::InitFrom;
				}
				else if (strName == UTF_8("format"))
				{
					m_eTag = E_Tag::Format;
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void end_tag(const Core::String8& /*strName*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::InitFrom:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			case E_Tag::Format:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_text(const Core::String8& strText, const IO::StreamParser::Position& /*pos*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::InitFrom:
			{
				m_surface.from = strText;
				break;
			}
			case E_Tag::Format:
			{
				m_surface.format = strText;
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_attribute(const Core::String8& strName, const Core::String8& strValue) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == "type")
				{
					m_surface.type = strValue;
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}

public:
	ColladaSurfaceTagCB::E_Tag m_eTag;
	IO::XMLSaxParser::ITagCB* m_pParent;
	EffectData::Param::Surface& m_surface;
};

// ------
// ColladaSampler2DTagCB
// -
// ------
class ColladaSampler2DTagCB FINAL : public IO::XMLSaxParser::ITagCB, public Core::NonCopyable
{
public:
	enum class E_Tag
	{
		Source = 0,
		MinFilter,
		MagFilter,
		Unknown = 0xffff
	};

public:
	ColladaSampler2DTagCB(IO::XMLSaxParser::ITagCB* pParent, EffectData::Param::Sampler2D& sampler2D)
		: m_eTag(ColladaSampler2DTagCB::E_Tag::Unknown)
		, m_pParent(pParent)
		, m_sampler2D(sampler2D)
	{
	}
	~ColladaSampler2DTagCB(void)
	{
	}

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL
	{
		pTagCB = this;
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == "source")
				{
					m_eTag = E_Tag::Source;
				}
				else if (strName == "minfilter")
				{
					m_eTag = E_Tag::MinFilter;
				}
				else if (strName == "magfilter")
				{
					m_eTag = E_Tag::MagFilter;
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void end_tag(const Core::String8& /*strName*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Source:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			case E_Tag::MinFilter:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			case E_Tag::MagFilter:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_text(const Core::String8& strText, const IO::StreamParser::Position& /*pos*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Source:
			{
				m_sampler2D.source = strText;
				break;
			}
			case E_Tag::MinFilter:
			{
				m_sampler2D.min_filter = strText;
				break;
			}
			case E_Tag::MagFilter:
			{
				m_sampler2D.mag_filter = strText;
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_attribute(const Core::String8& /*strName*/, const Core::String8& /*strValue*/) FINAL
	{
		switch (m_eTag)
		{
			default:
			{
				break;
			}
		}
	}

public:
	ColladaSampler2DTagCB::E_Tag m_eTag;
	IO::XMLSaxParser::ITagCB* m_pParent;
	EffectData::Param::Sampler2D& m_sampler2D;
};

// ------
// ColladaParamTagCB
// -
// ------
class ColladaParamTagCB FINAL : public IO::XMLSaxParser::ITagCB, public Core::NonCopyable
{
public:
	enum class E_Tag
	{
		Surface = 0,
		Sampler2D,
		Unknown = 0xffff
	};

public:
	ColladaParamTagCB(IO::XMLSaxParser::ITagCB* pParent, EffectData::Param& paramData)
		: m_pChild(nullptr)
		, m_pParent(pParent)
		, m_eTag(ColladaParamTagCB::E_Tag::Unknown)
		, m_paramData(paramData)
	{
	}
	~ColladaParamTagCB(void)
	{
	}

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL
	{
		pTagCB = this;
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == "surface")
				{
					m_eTag = E_Tag::Surface;
					m_pChild = pTagCB = FXD_NEW(ColladaSurfaceTagCB)(this, m_paramData.surfaces.emplace_back());
				}
				else if (strName == "sampler2D")
				{
					m_eTag = E_Tag::Sampler2D;
					m_pChild = pTagCB = FXD_NEW(ColladaSampler2DTagCB)(this, m_paramData.samplers.emplace_back());
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void end_tag(const Core::String8& /*strName*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				break;
			}
			case E_Tag::Surface:
			{
				m_eTag = E_Tag::Unknown;
				FXD_SAFE_DELETE(m_pChild);
				break;
			}
			case E_Tag::Sampler2D:
			{
				m_eTag = E_Tag::Unknown;
				FXD_SAFE_DELETE(m_pChild);
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_text(const Core::String8& /*strText*/, const IO::StreamParser::Position& /*pos*/) FINAL
	{
		switch (m_eTag)
		{
			default:
			{
				break;
			}
		}
	}
	void handle_attribute(const Core::String8& strName, const Core::String8& strValue) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == "sid")
				{
					m_paramData.sid = strValue;
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}

public:
	IO::XMLSaxParser::ITagCB* m_pChild;
	IO::XMLSaxParser::ITagCB* m_pParent;
	ColladaParamTagCB::E_Tag m_eTag;
	EffectData::Param& m_paramData;
};

// ------
// ColladaTechniqueTagCB
// -
// ------
class ColladaTechniqueTagCB FINAL : public IO::XMLSaxParser::ITagCB, public Core::NonCopyable
{
public:
	enum class E_Tag
	{
		Phong = 0,
		Diffuse,
		Texture,
		Color,
		Unknown = 0xffff
	};

public:
	ColladaTechniqueTagCB(IO::XMLSaxParser::ITagCB* pParent, EffectData::Technique& technique)
		: m_eTag(ColladaTechniqueTagCB::E_Tag::Unknown)
		, m_pParent(pParent)
		, m_technique(technique)
	{
	}
	~ColladaTechniqueTagCB(void)
	{
	}

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL
	{
		pTagCB = this;
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == "phong")
				{
					m_eTag = E_Tag::Phong;
				}
				break;
			}
			case E_Tag::Phong:
			{
				if (strName == "diffuse")
				{
					m_eTag = E_Tag::Diffuse;
				}
				break;
			}
			case E_Tag::Diffuse:
			{
				if (strName == "texture")
				{
					m_eTag = E_Tag::Texture;
				}
				else if (strName == "color")
				{
					m_eTag = E_Tag::Color;
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void end_tag(const Core::String8& /*strName*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				break;
			}
			case E_Tag::Phong:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			case E_Tag::Diffuse:
			{
				m_eTag = E_Tag::Phong;
				break;
			}
			case E_Tag::Texture:
			{
				m_eTag = E_Tag::Diffuse;
				break;
			}
			case E_Tag::Color:
			{
				m_eTag = E_Tag::Diffuse;
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_text(const Core::String8& strText, const IO::StreamParser::Position& /*pos*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Color:
			{
				m_technique.colour = strText;
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_attribute(const Core::String8& strName, const Core::String8& strValue) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == "sid")
				{
					m_technique.sid = strValue;
				}
				break;
			}
			case E_Tag::Texture:
			{
				if (strName == "texture")
				{
					m_technique.texture = strValue;
				}
				else if (strName == "texcoord")
				{
					m_technique.texcoord = strValue;
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}

public:
	ColladaTechniqueTagCB::E_Tag m_eTag;
	IO::XMLSaxParser::ITagCB* m_pParent;
	EffectData::Technique& m_technique;
};

// ------
// ColladaTrianglesSourceFileTagCB
// -
// ------
class ColladaTrianglesSourceFileTagCB FINAL : public IO::XMLSaxParser::ITagCB, public Core::NonCopyable
{
public:
	enum class E_Tag
	{
		Input = 0,
		P,
		Unknown = 0xffff
	};

public:
	ColladaTrianglesSourceFileTagCB(IO::XMLSaxParser::ITagCB* pParent, GeoData::MeshData::Tris& tris)
		: m_eTag(ColladaTrianglesSourceFileTagCB::E_Tag::Unknown)
		, m_pParent(pParent)
		, m_tris(tris)
	{
	}
	~ColladaTrianglesSourceFileTagCB(void)
	{
	}

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL
	{
		pTagCB = this;
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("input"))
				{
					m_tris.inputs.emplace_back();
					m_eTag = E_Tag::Input;
				}
				else if (strName == UTF_8("p"))
				{
					m_eTag = E_Tag::P;
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void end_tag(const Core::String8& /*strName*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Input:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			case E_Tag::P:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_text(const Core::String8& strText, const IO::StreamParser::Position& /*pos*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::P:
			{
				FXD::U32 nCount = (m_tris.count * m_tris.inputs.size() * 3);
				m_tris.indices.resize(nCount);
				strText.to_S16_array(' ', m_tris.indices.data(), nCount);
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_attribute(const Core::String8& strName, const Core::String8& strValue) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("material"))
				{
					m_tris.material = strValue;
				}
				else if (strName == UTF_8("count"))
				{
					m_tris.count = strValue.to_S32();
				}
				break;
			}
			case E_Tag::Input:
			{
				if (strName == UTF_8("semantic"))
				{
					m_tris.inputs.back().semantic = strValue;
				}
				else if (strName == UTF_8("source"))
				{
					m_tris.inputs.back().source = strValue;
				}
				else if (strName == UTF_8("set"))
				{
					m_tris.inputs.back().set = strValue.to_S32();
				}
				else if (strName == UTF_8("offset"))
				{
					m_tris.inputs.back().offest = strValue.to_S32();
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}

public:
	ColladaTrianglesSourceFileTagCB::E_Tag m_eTag;
	IO::XMLSaxParser::ITagCB* m_pParent;
	GeoData::MeshData::Tris& m_tris;
};

// ------
// ColladaVertsSourceFileTagCB
// -
// ------
class ColladaVertsSourceFileTagCB FINAL : public IO::XMLSaxParser::ITagCB, public Core::NonCopyable
{
public:
	enum class E_Tag
	{
		Input = 0,
		Unknown = 0xffff
	};

public:
	ColladaVertsSourceFileTagCB(IO::XMLSaxParser::ITagCB* pParent, GeoData::MeshData::Verts& verts)
		: m_eTag(ColladaVertsSourceFileTagCB::E_Tag::Unknown)
		, m_pParent(pParent)
		, m_verts(verts)
	{
	}
	~ColladaVertsSourceFileTagCB(void)
	{
	}

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL
	{
		pTagCB = this;
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("input"))
				{
					m_eTag = E_Tag::Input;
					m_verts.inputs.emplace_back();
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void end_tag(const Core::String8& /*strName*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Input:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_text(const Core::String8& /*strText*/, const IO::StreamParser::Position& /*pos*/) FINAL
	{
		switch (m_eTag)
		{
			default:
			{
				break;
			}
		}
	}
	void handle_attribute(const Core::String8& strName, const Core::String8& strValue) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("id"))
				{
					m_verts.id = strValue;
				}
				break;
			}
			case E_Tag::Input:
			{
				if (strName == UTF_8("semantic"))
				{
					m_verts.inputs.back().semantic = strValue;
				}
				else if (strName == UTF_8("source"))
				{
					m_verts.inputs.back().source = strValue;
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}

public:
	ColladaVertsSourceFileTagCB::E_Tag m_eTag;
	IO::XMLSaxParser::ITagCB* m_pParent;
	GeoData::MeshData::Verts& m_verts;
};

// ------
// ColladaMeshSourceFileTagCB
// -
// ------
class ColladaMeshSourceFileTagCB FINAL : public IO::XMLSaxParser::ITagCB, public Core::NonCopyable
{
public:
	enum class E_Tag
	{
		FloatArray = 0,
		TechniqueCommon,
		Accessor,
		Param,
		Unknown = 0xffff
	};

public:
	ColladaMeshSourceFileTagCB(IO::XMLSaxParser::ITagCB* pParent, GeoData::MeshData::Source& source)
		: m_eTag(ColladaMeshSourceFileTagCB::E_Tag::Unknown)
		, m_pParent(pParent)
		, m_source(source)
	{}
	~ColladaMeshSourceFileTagCB(void)
	{}

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL
	{
		pTagCB = this;
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("float_array"))
				{
					m_eTag = E_Tag::FloatArray;
				}
				else if (strName == UTF_8("technique_common"))
				{
					m_eTag = E_Tag::TechniqueCommon;
				}
				break;
			}
			case E_Tag::TechniqueCommon:
			{
				if (strName == UTF_8("accessor"))
				{
					m_eTag = E_Tag::Accessor;
				}
				break;
			}
			case E_Tag::Accessor:
			{
				if (strName == UTF_8("param"))
				{
					m_eTag = E_Tag::Param;
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void end_tag(const Core::String8& /*strName*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::FloatArray:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			case E_Tag::TechniqueCommon:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			case E_Tag::Accessor:
			{
				m_eTag = E_Tag::TechniqueCommon;
				break;
			}
			case E_Tag::Param:
			{
				m_eTag = E_Tag::Accessor;
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_text(const Core::String8& strText, const IO::StreamParser::Position& /*pos*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::FloatArray:
			{
				strText.to_F32_array(' ', m_source.array.data(), m_source.count);
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_attribute(const Core::String8& strName, const Core::String8& strValue) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("id"))
				{
					m_source.id = strValue;
				}
				break;
			}
			case E_Tag::FloatArray:
			{
				if (strName == UTF_8("id"))
				{
					m_source.id = strValue;
				}
				else if (strName == UTF_8("stride"))
				{
					m_source.stride = strValue.to_S32();
				}
				else if (strName == UTF_8("count"))
				{
					m_source.count = strValue.to_S32();
					m_source.array.resize(m_source.count);
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}

public:
	ColladaMeshSourceFileTagCB::E_Tag m_eTag;
	IO::XMLSaxParser::ITagCB* m_pParent;
	GeoData::MeshData::Source& m_source;
};

// ------
// ColladaMeshFileTagCB
// -
// ------
class ColladaMeshFileTagCB FINAL : public IO::XMLSaxParser::ITagCB, public Core::NonCopyable
{
public:
	enum class E_Tag
	{
		Source = 0,
		Vertices,
		Triangles,
		Unknown = 0xffff
	};

public:
	ColladaMeshFileTagCB(IO::XMLSaxParser::ITagCB* pParent, GeoData::MeshData& mesh)
		: m_pChild(nullptr)
		, m_pParent(pParent)
		, m_eTag(ColladaMeshFileTagCB::E_Tag::Unknown)
		, m_mesh(mesh)
	{
	}
	~ColladaMeshFileTagCB(void)
	{
	}

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL
	{
		pTagCB = this;
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("source"))
				{
					m_eTag = E_Tag::Source;
					m_pChild = pTagCB = FXD_NEW(ColladaMeshSourceFileTagCB)(this, m_mesh.sources.emplace_back());
				}
				else if (strName == UTF_8("vertices"))
				{
					m_eTag = E_Tag::Vertices;
					m_pChild = pTagCB = FXD_NEW(ColladaVertsSourceFileTagCB)(this, m_mesh.verts.emplace_back());
				}
				else if (strName == UTF_8("triangles"))
				{
					m_eTag = E_Tag::Triangles;
					m_pChild = pTagCB = FXD_NEW(ColladaTrianglesSourceFileTagCB)(this, m_mesh.tris.emplace_back());
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void end_tag(const Core::String8& /*strName*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Source:
			{
				m_eTag = E_Tag::Unknown;
				FXD_SAFE_DELETE(m_pChild);
				break;
			}
			case E_Tag::Vertices:
			{
				m_eTag = E_Tag::Unknown;
				FXD_SAFE_DELETE(m_pChild);
				break;
			}
			case E_Tag::Triangles:
			{
				m_eTag = E_Tag::Unknown;
				FXD_SAFE_DELETE(m_pChild);
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_text(const Core::String8& /*strText*/, const IO::StreamParser::Position& /*pos*/) FINAL
	{
		switch (m_eTag)
		{
			default:
			{
				break;
			}
		}
	}
	void handle_attribute(const Core::String8& /*strName*/, const Core::String8& /*strValue*/) FINAL
	{
		switch (m_eTag)
		{
			default:
			{
				break;
			}
		}
	}

public:
	IO::XMLSaxParser::ITagCB* m_pChild;
	IO::XMLSaxParser::ITagCB* m_pParent;
	ColladaMeshFileTagCB::E_Tag m_eTag;
	GeoData::MeshData& m_mesh;
};

// ------
// ColladaGeometryTagCB
// -
// ------
class ColladaGeometryTagCB FINAL : public IO::XMLSaxParser::ITagCB, public Core::NonCopyable
{
public:
	enum class E_Tag
	{
		Mesh = 0,
		Unknown = 0xffff
	};

public:
	ColladaGeometryTagCB(IO::XMLSaxParser::ITagCB* pParent, GeoData& geoData)
		: m_pChild(nullptr)
		, m_pParent(pParent)
		, m_eTag(ColladaGeometryTagCB::E_Tag::Unknown)
		, m_geoData(geoData)
	{
	}
	~ColladaGeometryTagCB(void)
	{
	}

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL
	{
		pTagCB = this;
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("mesh"))
				{
					m_eTag = E_Tag::Mesh;
					m_pChild = pTagCB = FXD_NEW(ColladaMeshFileTagCB)(this, m_geoData.meshes.emplace_back());
				}
				break;
			}
		}
	}
	void end_tag(const Core::String8& /*strName*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Mesh:
			{
				m_eTag = E_Tag::Unknown;
				FXD_SAFE_DELETE(m_pChild);
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_text(const Core::String8& /*strText*/, const IO::StreamParser::Position& /*pos*/) FINAL
	{
		switch (m_eTag)
		{
			default:
			{
				break;
			}
		}
	}
	void handle_attribute(const Core::String8& strName, const Core::String8& strValue) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("id"))
				{
					m_geoData.id = strValue;
				}
				else if (strName == UTF_8("name"))
				{
					m_geoData.name = strValue;
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}

public:
	IO::XMLSaxParser::ITagCB* m_pChild;
	IO::XMLSaxParser::ITagCB* m_pParent;
	ColladaGeometryTagCB::E_Tag m_eTag;
	GeoData& m_geoData;
};

// ------
// ColladaMaterialTagCB
// -
// ------
class ColladaMaterialTagCB FINAL : public IO::XMLSaxParser::ITagCB, public Core::NonCopyable
{
public:
	enum class E_Tag
	{
		InstanceEffect = 0,
		Unknown = 0xffff
	};

public:
	ColladaMaterialTagCB(IO::XMLSaxParser::ITagCB* pParent, MaterialData& materialData)
		: m_eTag(ColladaMaterialTagCB::E_Tag::Unknown)
		, m_pParent(pParent)
		, m_materialData(materialData)
	{
	}
	~ColladaMaterialTagCB(void)
	{
	}

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL
	{
		pTagCB = this;
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("instance_effect"))
				{
					m_eTag = E_Tag::InstanceEffect;
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void end_tag(const Core::String8& /*strName*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::InstanceEffect:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_text(const Core::String8& /*strText*/, const IO::StreamParser::Position& /*pos*/) FINAL
	{
		switch (m_eTag)
		{
			default:
			{
				break;
			}
		}
	}
	void handle_attribute(const Core::String8& strName, const Core::String8& strValue) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("id"))
				{
					m_materialData.id = strValue;
				}
				else if (strName == UTF_8("name"))
				{
					m_materialData.name = strValue;
				}
				break;
			}
			case E_Tag::InstanceEffect:
			{
				if (strName == UTF_8("url"))
				{
					m_materialData.url = strValue;
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}

public:
	ColladaMaterialTagCB::E_Tag m_eTag;
	IO::XMLSaxParser::ITagCB* m_pParent;
	MaterialData& m_materialData;
};

// ------
// ColladaEffectTagCB
// -
// ------
class ColladaEffectTagCB FINAL : public IO::XMLSaxParser::ITagCB, public Core::NonCopyable
{
public:
	enum class E_Tag
	{
		Effect = 0,
		Profile_COMMON,
		NewParam,
		Technique,
		Unknown = 0xffff
	};

public:
	ColladaEffectTagCB(IO::XMLSaxParser::ITagCB* pParent, EffectData& effectdata)
		: m_pChild(nullptr)
		, m_pParent(pParent)
		, m_eTag(ColladaEffectTagCB::E_Tag::Unknown)
		, m_effectdata(effectdata)
	{
	}
	~ColladaEffectTagCB(void)
	{
	}

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL
	{
		pTagCB = this;
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("profile_COMMON"))
				{
					m_eTag = E_Tag::Profile_COMMON;
				}
				break;
			}
			case E_Tag::Profile_COMMON:
			{
				if (strName == UTF_8("newparam"))
				{
					m_eTag = E_Tag::NewParam;
					m_pChild = pTagCB = FXD_NEW(ColladaParamTagCB)(this, m_effectdata.params.emplace_back());
				}
				else if (strName == UTF_8("technique"))
				{
					m_eTag = E_Tag::Technique;
					m_pChild = pTagCB = FXD_NEW(ColladaTechniqueTagCB)(this, m_effectdata.techniques.emplace_back());
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void end_tag(const Core::String8& /*strName*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Effect:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			case E_Tag::Profile_COMMON:
			{
				m_eTag = E_Tag::Effect;
				break;
			}
			case E_Tag::NewParam:
			{
				m_eTag = E_Tag::Profile_COMMON;
				FXD_SAFE_DELETE(m_pChild);
				break;
			}
			case E_Tag::Technique:
			{
				m_eTag = E_Tag::Profile_COMMON;
				FXD_SAFE_DELETE(m_pChild);
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_text(const Core::String8& /*strText*/, const IO::StreamParser::Position& /*pos*/) FINAL
	{
		switch (m_eTag)
		{
			default:
			{
				break;
			}
		}
	}
	void handle_attribute(const Core::String8& strName, const Core::String8& strValue) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("id"))
				{
					m_effectdata.id = strValue;
				}
				else if (strName == UTF_8("name"))
				{
					m_effectdata.name = strValue;
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}

public:
	IO::XMLSaxParser::ITagCB* m_pChild;
	IO::XMLSaxParser::ITagCB* m_pParent;
	ColladaEffectTagCB::E_Tag m_eTag;
	EffectData& m_effectdata;
};

// ------
// ColladaLibraryImagesTagCB
// -
// ------
class ColladaImageTagCB FINAL : public IO::XMLSaxParser::ITagCB, public Core::NonCopyable
{
public:
	enum class E_Tag
	{
		InitFrom = 0,
		Unknown = 0xffff
	};

public:
	ColladaImageTagCB(IO::XMLSaxParser::ITagCB* pParent, ImageData& image)
		: m_eTag(ColladaImageTagCB::E_Tag::Unknown)
		, m_pParent(pParent)
		, m_image(image)
	{
	}
	~ColladaImageTagCB(void)
	{
	}

	void handle_tag(const Core::String8& /*strName*/, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL
	{
		pTagCB = this;
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				m_eTag = E_Tag::InitFrom;
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void end_tag(const Core::String8& /*strName*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				break;
			}
			case E_Tag::InitFrom:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_text(const Core::String8& strText, const IO::StreamParser::Position& /*pos*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::InitFrom:
			{
				m_image.from = strText;
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_attribute(const Core::String8& strName, const Core::String8& strValue) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("id"))
				{
					m_image.id = strValue;
				}
				else if (strName == UTF_8("name"))
				{
					m_image.name = strValue;
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}

public:
	ColladaImageTagCB::E_Tag m_eTag;
	IO::XMLSaxParser::ITagCB* m_pParent;
	ImageData& m_image;

};

// ------
// ColladaAssetTagCB
// -
// ------
class ColladaAssetTagCB FINAL : public IO::XMLSaxParser::ITagCB, public Core::NonCopyable
{
public:
	enum class E_Tag
	{
		Created = 0,
		Modified,
		Unit,
		UpAaxis,
		Unknown = 0xffff
	};

public:
	ColladaAssetTagCB(IO::XMLSaxParser::ITagCB* pParent, ColladaData& colladaData)
		: m_eTag(ColladaAssetTagCB::E_Tag::Unknown)
		, m_pParent(pParent)
		, m_colladaData(colladaData)
	{
	}
	~ColladaAssetTagCB(void)
	{
	}

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL
	{
		pTagCB = this;
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("created"))
				{
					m_eTag = E_Tag::Created;
				}
				else if (strName == UTF_8("modified"))
				{
					m_eTag = E_Tag::Modified;
				}
				else if (strName == UTF_8("unit"))
				{
					m_eTag = E_Tag::Unit;
				}
				else if (strName == UTF_8("up_axis"))
				{
					m_eTag = E_Tag::UpAaxis;
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void end_tag(const Core::String8& /*strName*/) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Created:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			case E_Tag::Modified:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			case E_Tag::Unit:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			case E_Tag::UpAaxis:
			{
				m_eTag = E_Tag::Unknown;
				break;
			}
			default:
			{
				break;
			}
		}
	}
	void handle_text(const Core::String8& /*strName*/, const IO::StreamParser::Position& /*pos*/) FINAL
	{
		switch (m_eTag)
		{
			default:
			{
				break;
			}
		}
	}
	void handle_attribute(const Core::String8& /*strName*/, const Core::String8& /*strValue*/) FINAL
	{
		switch (m_eTag)
		{
			default:
			{
				break;
			}
		}
	}

public:
	ColladaAssetTagCB::E_Tag m_eTag;
	IO::XMLSaxParser::ITagCB* m_pParent;
	ColladaData& m_colladaData;
};

// ------
// ColladaFileTagCB
// -
// ------
class ColladaFileTagCB FINAL : public IO::XMLSaxParser::ITagCB, public Core::NonCopyable
{
public:
	enum class E_Tag
	{
		Collada = 0,
		Asset,
		Image,
		Effect,
		Material,
		Geometry,
		VisualScene,
		Scene,
		Count,
		Unknown = 0xffff
	};

public:
	ColladaFileTagCB(void)
		: m_pChild(nullptr)
		, m_eTag(ColladaFileTagCB::E_Tag::Unknown)
	{
	}
	~ColladaFileTagCB(void)
	{
	}

	void handle_tag(const Core::String8& strName, IO::XMLSaxParser::ITagCB*& pTagCB) FINAL
	{
		pTagCB = this;
		switch (m_eTag)
		{
			case E_Tag::Unknown:
			{
				if (strName == UTF_8("COLLADA"))
				{
					m_eTag = E_Tag::Collada;
				}
				break;
			}
			case E_Tag::Collada:
			{
				if (strName == UTF_8("asset"))
				{
					m_eTag = E_Tag::Asset;
					m_pChild = pTagCB = FXD_NEW(ColladaAssetTagCB)(this, m_colladaData);
				}
				else if (strName == UTF_8("image"))
				{
					m_eTag = E_Tag::Image;
					m_pChild = pTagCB = FXD_NEW(ColladaImageTagCB)(this, m_colladaData.images.emplace_back());
				}
				else if (strName == UTF_8("effect"))
				{
					m_eTag = E_Tag::Effect;
					m_pChild = pTagCB = FXD_NEW(ColladaEffectTagCB)(this, m_colladaData.effects.emplace_back());
				}
				else if (strName == UTF_8("material"))
				{
					m_eTag = E_Tag::Material;
					m_pChild = pTagCB = FXD_NEW(ColladaMaterialTagCB)(this, m_colladaData.materials.emplace_back());
				}
				else if (strName == UTF_8("geometry"))
				{
					m_eTag = E_Tag::Geometry;
					m_pChild = pTagCB = FXD_NEW(ColladaGeometryTagCB)(this, m_colladaData.geos.emplace_back());
				}
				else if (strName == UTF_8("visual_scene"))
				{
					m_eTag = E_Tag::VisualScene;
					//m_pChild = pTagCB = FXD_NEW(ColladaVisualSceneTagCB)(this, m_colladaData.geos.emplace_back());
				}
				else if (strName == UTF_8("scene"))
				{
					m_eTag = E_Tag::Scene;
					//m_pChild = pTagCB = FXD_NEW(ColladaSceneTagCB)(this, m_colladaData.geos.emplace_back());
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}

	void end_tag(const Core::String8& strName) FINAL
	{
		switch (m_eTag)
		{
			case E_Tag::Collada:
			{
				if (strName == "COLLADA")
				{
					m_eTag = E_Tag::Unknown;
				}
				break;
			}
			case E_Tag::Asset:
			{
				m_eTag = E_Tag::Collada;
				FXD_SAFE_DELETE(m_pChild);
				break;
			}
			case E_Tag::Image:
			{
				m_eTag = E_Tag::Collada;
				FXD_SAFE_DELETE(m_pChild);
				break;
			}
			case E_Tag::Effect:
			{
				m_eTag = E_Tag::Collada;
				FXD_SAFE_DELETE(m_pChild);
				break;
			}
			case E_Tag::Material:
			{
				m_eTag = E_Tag::Collada;
				FXD_SAFE_DELETE(m_pChild);
				break;
			}
			case E_Tag::Geometry:
			{
				m_eTag = E_Tag::Collada;
				FXD_SAFE_DELETE(m_pChild);
				break;
			}
			case E_Tag::VisualScene:
			{
				m_eTag = E_Tag::Collada;
				FXD_SAFE_DELETE(m_pChild);
				break;
			}
			case E_Tag::Scene:
			{
				m_eTag = E_Tag::Collada;
				FXD_SAFE_DELETE(m_pChild);
				break;
			}
			default:
			{
				break;
			}
		}
	}

	void handle_text(const Core::String8& /*strText*/, const IO::StreamParser::Position& /*pos*/) FINAL
	{
		switch (m_eTag)
		{
			default:
			{
				break;
			}
		}
	}
	void handle_attribute(const Core::String8& /*strName*/, const Core::String8& /*strValue*/) FINAL
	{
		switch (m_eTag)
		{
			default:
			{
				break;
			}
		}
	}

public:
	IO::XMLSaxParser::ITagCB* m_pChild;
	ColladaFileTagCB::E_Tag m_eTag;
	ColladaData m_colladaData;
};

// ------
// IGeoCollada
// -
// ------
IGeoCollada::IGeoCollada(Core::StreamIn& streamIn, const Core::String8& strFileName)
	: IGeo(Scene::E_GeoAsset::Collada, strFileName)
{
	PRINT_INFO << FXD::Memory::Log::GetTotalStats().total_bytes_allocated();
	{
		ColladaFileTagCB callback;
		IO::StreamParser streamParser(streamIn);
		IO::XMLSaxParser::parse(streamParser, callback);

		fxd_for(auto geo, callback.m_colladaData.geos)
		{
		}
	}
	PRINT_INFO << FXD::Memory::Log::GetTotalStats().total_bytes_allocated();
}

IGeoCollada::~IGeoCollada(void)
{
}

bool IGeoCollada::is_platform_supported(void) const
{
	return true;
}