// Creator - MatthewGolder
#include "FXDEngine/Scene/Assets/Geo.h"

using namespace FXD;
using namespace Scene;

// ------
// IGeo
// - 
// ------
IGeo::IGeo(const Scene::E_GeoAsset eAsset, const Core::String8& strFileName)
	: IAsset(strFileName)
	, m_eAsset(eAsset)
{
}

IGeo::~IGeo(void)
{
}

// ------
// File
// -
// ------
const Core::StringView8 sGeoFileTypes[] =
{
	"Collada",	// E_GeoAsset::Collada
	"Count"		// E_GeoAsset::Count
};

const Core::StringView8 FXD::Scene::File::ToString(Scene::E_GeoAsset eGeoType)
{
	return sGeoFileTypes[FXD::STD::to_underlying(eGeoType)];
}

Scene::E_GeoAsset FXD::Scene::File::ToFileType(const Core::StringView8 strType)
{
	for (FXD::U16 n1 = 0; n1 < FXD::STD::to_underlying(Scene::E_GeoAsset::Count); ++n1)
	{
		if (strType.ends_with_no_case(sGeoFileTypes[n1]))
		{
			return (Scene::E_GeoAsset)n1;
		}
	}
	return Scene::E_GeoAsset::Unknown;
}