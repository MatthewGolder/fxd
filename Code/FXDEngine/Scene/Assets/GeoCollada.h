// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SCENE_ASSETS_GEOCOLLADA_H
#define FXDENGINE_SCENE_ASSETS_GEOCOLLADA_H

#include "FXDEngine/Scene/Assets/Geo.h"

namespace FXD
{
	namespace Scene
	{
		// ------
		// IGeoCollada
		// -
		// ------
		class IGeoCollada FINAL : public Scene::IGeo
		{
		public:
			IGeoCollada(Core::StreamIn& streamIn, const Core::String8& strFileName);
			~IGeoCollada(void);

			bool is_platform_supported(void) const FINAL;
		};

	} //namespace Scene
} //namespace FXD
#endif //FXDENGINE_SCENE_ASSETS_GEOCOLLADA_H