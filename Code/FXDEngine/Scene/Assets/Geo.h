// Creator - MatthewGolder
#pragma once
#ifndef FXDENGINE_SCENE_ASSETS_GEO_H
#define FXDENGINE_SCENE_ASSETS_GEO_H

#include "FXDEngine/IO/FileCache.h"
#include "FXDEngine/Scene/Types.h"

namespace FXD
{
	namespace Scene
	{
		// ------
		// E_GeoAsset
		// -
		// ------
		enum class E_GeoAsset
		{
			Collada,
			Count,
			Unknown = 0xffff
		};

		// ------
		// IGeo
		// -
		// ------
		class IGeo : public IO::IAsset
		{
		public:
			IGeo(const Scene::E_GeoAsset eAsset, const Core::String8& strFileName);
			virtual ~IGeo(void);

			virtual bool is_platform_supported(void) const PURE;
			const Core::StringView8 get_asset_type(void) const FINAL { return UTF_8("Geo"); }

			GET_R(Scene::E_GeoAsset, eAsset, eAsset);

		protected:
			Scene::E_GeoAsset m_eAsset;
		};

		namespace File
		{
			extern const Core::StringView8 ToString(Scene::E_GeoAsset eGeoType);
			extern Scene::E_GeoAsset ToFileType(const Core::StringView8 strType);
		} //namespace File

	} //namespace Scene
} //namespace FXD
#endif //FXDENGINE_SCENE_ASSETS_GEO_H